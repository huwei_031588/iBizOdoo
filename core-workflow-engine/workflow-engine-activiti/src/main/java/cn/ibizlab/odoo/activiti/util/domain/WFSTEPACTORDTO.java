package cn.ibizlab.odoo.activiti.util.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 服务DTO对象[WFSTEPACTORDTO]
 */
public class WFSTEPACTORDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WFSTEPACTORID]
     *
     */
    private String WFSTEPACTORId;

    @JsonIgnore
    private boolean WFSTEPACTORIdDirtyFlag;

    /**
     * 属性 [WFSTEPACTORNAME]
     *
     */
    private String WFSTEPACTORName;

    @JsonIgnore
    private boolean WFSTEPACTORNameDirtyFlag;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    private Timestamp UpdateDate;

    @JsonIgnore
    private boolean UpdateDateDirtyFlag;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    private String UpdateMan;

    @JsonIgnore
    private boolean UpdateManDirtyFlag;

    /**
     * 属性 [CREATEDATE]
     *
     */
    private Timestamp CreateDate;

    @JsonIgnore
    private boolean CreateDateDirtyFlag;

    /**
     * 属性 [CREATEMAN]
     *
     */
    private String CreateMan;

    @JsonIgnore
    private boolean CreateManDirtyFlag;

    /**
     * 属性 [WFUSERID]
     *
     */
    private String WFUSERId;

    @JsonIgnore
    private boolean WFUSERIdDirtyFlag;

    /**
     * 属性 [WFSTEPID]
     *
     */
    private String WFSTEPId;

    @JsonIgnore
    private boolean WFSTEPIdDirtyFlag;

    /**
     * 属性 [LOGICNAME]
     *
     */
    private String Logicname;

    @JsonIgnore
    private boolean LogicnameDirtyFlag;

    /**
     * 属性 [WFUSERNAME]
     *
     */
    private String WFUSERName;

    @JsonIgnore
    private boolean WFUSERNameDirtyFlag;


    /**
     * 获取 [WFSTEPACTORID]
     */
    public String getWFSTEPACTORId(){
        return WFSTEPACTORId ;
    }

    /**
     * 设置 [WFSTEPACTORID]
     */
    public void setWFSTEPACTORId(String  wFSTEPACTORId){
        this.WFSTEPACTORId = wFSTEPACTORId ;
        this.WFSTEPACTORIdDirtyFlag = true ;
    }

    /**
     * 获取 [WFSTEPACTORID]脏标记
     */
    @JsonIgnore
    public boolean getWFSTEPACTORIdDirtyFlag(){
        return WFSTEPACTORIdDirtyFlag ;
    }

    /**
     * 获取 [WFSTEPACTORNAME]
     */
    public String getWFSTEPACTORName(){
        return WFSTEPACTORName ;
    }

    /**
     * 设置 [WFSTEPACTORNAME]
     */
    public void setWFSTEPACTORName(String  wFSTEPACTORName){
        this.WFSTEPACTORName = wFSTEPACTORName ;
        this.WFSTEPACTORNameDirtyFlag = true ;
    }

    /**
     * 获取 [WFSTEPACTORNAME]脏标记
     */
    @JsonIgnore
    public boolean getWFSTEPACTORNameDirtyFlag(){
        return WFSTEPACTORNameDirtyFlag ;
    }

    /**
     * 获取 [UPDATEDATE]
     */
    public Timestamp getUpdateDate(){
        return UpdateDate ;
    }

    /**
     * 设置 [UPDATEDATE]
     */
    public void setUpdateDate(Timestamp  updateDate){
        this.UpdateDate = updateDate ;
        this.UpdateDateDirtyFlag = true ;
    }

    /**
     * 获取 [UPDATEDATE]脏标记
     */
    @JsonIgnore
    public boolean getUpdateDateDirtyFlag(){
        return UpdateDateDirtyFlag ;
    }

    /**
     * 获取 [UPDATEMAN]
     */
    public String getUpdateMan(){
        return UpdateMan ;
    }

    /**
     * 设置 [UPDATEMAN]
     */
    public void setUpdateMan(String  updateMan){
        this.UpdateMan = updateMan ;
        this.UpdateManDirtyFlag = true ;
    }

    /**
     * 获取 [UPDATEMAN]脏标记
     */
    @JsonIgnore
    public boolean getUpdateManDirtyFlag(){
        return UpdateManDirtyFlag ;
    }

    /**
     * 获取 [CREATEDATE]
     */
    public Timestamp getCreateDate(){
        return CreateDate ;
    }

    /**
     * 设置 [CREATEDATE]
     */
    public void setCreateDate(Timestamp  createDate){
        this.CreateDate = createDate ;
        this.CreateDateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATEDATE]脏标记
     */
    @JsonIgnore
    public boolean getCreateDateDirtyFlag(){
        return CreateDateDirtyFlag ;
    }

    /**
     * 获取 [CREATEMAN]
     */
    public String getCreateMan(){
        return CreateMan ;
    }

    /**
     * 设置 [CREATEMAN]
     */
    public void setCreateMan(String  createMan){
        this.CreateMan = createMan ;
        this.CreateManDirtyFlag = true ;
    }

    /**
     * 获取 [CREATEMAN]脏标记
     */
    @JsonIgnore
    public boolean getCreateManDirtyFlag(){
        return CreateManDirtyFlag ;
    }

    /**
     * 获取 [WFUSERID]
     */
    public String getWFUSERId(){
        return WFUSERId ;
    }

    /**
     * 设置 [WFUSERID]
     */
    public void setWFUSERId(String  wFUSERId){
        this.WFUSERId = wFUSERId ;
        this.WFUSERIdDirtyFlag = true ;
    }

    /**
     * 获取 [WFUSERID]脏标记
     */
    @JsonIgnore
    public boolean getWFUSERIdDirtyFlag(){
        return WFUSERIdDirtyFlag ;
    }

    /**
     * 获取 [WFSTEPID]
     */
    public String getWFSTEPId(){
        return WFSTEPId ;
    }

    /**
     * 设置 [WFSTEPID]
     */
    public void setWFSTEPId(String  wFSTEPId){
        this.WFSTEPId = wFSTEPId ;
        this.WFSTEPIdDirtyFlag = true ;
    }

    /**
     * 获取 [WFSTEPID]脏标记
     */
    @JsonIgnore
    public boolean getWFSTEPIdDirtyFlag(){
        return WFSTEPIdDirtyFlag ;
    }

    /**
     * 获取 [LOGICNAME]
     */
    public String getLogicname(){
        return Logicname ;
    }

    /**
     * 设置 [LOGICNAME]
     */
    public void setLogicname(String  logicname){
        this.Logicname = logicname ;
        this.LogicnameDirtyFlag = true ;
    }

    /**
     * 获取 [LOGICNAME]脏标记
     */
    @JsonIgnore
    public boolean getLogicnameDirtyFlag(){
        return LogicnameDirtyFlag ;
    }

    /**
     * 获取 [WFUSERNAME]
     */
    public String getWFUSERName(){
        return WFUSERName ;
    }

    /**
     * 设置 [WFUSERNAME]
     */
    public void setWFUSERName(String  wFUSERName){
        this.WFUSERName = wFUSERName ;
        this.WFUSERNameDirtyFlag = true ;
    }

    /**
     * 获取 [WFUSERNAME]脏标记
     */
    @JsonIgnore
    public boolean getWFUSERNameDirtyFlag(){
        return WFUSERNameDirtyFlag ;
    }



}
