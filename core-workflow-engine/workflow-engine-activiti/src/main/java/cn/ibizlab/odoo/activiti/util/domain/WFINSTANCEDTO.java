package cn.ibizlab.odoo.activiti.util.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

//import com.ibiz.core.base.valuerule.anno.wfinstance.*;

/**
 * 服务DTO对象[WFINSTANCEDTO]
 */
public class WFINSTANCEDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATEMAN]
     *
     */
    private String CreateMan;

    @JsonIgnore
    private boolean CreateManDirtyFlag;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    private String WFINSTANCEId;

    @JsonIgnore
    private boolean WFINSTANCEIdDirtyFlag;

    /**
     * 属性 [WFINSTANCENAME]
     *
     */
    private String WFINSTANCEName;

    @JsonIgnore
    private boolean WFINSTANCENameDirtyFlag;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    private String UpdateMan;

    @JsonIgnore
    private boolean UpdateManDirtyFlag;

    /**
     * 属性 [CREATEDATE]
     *
     */
    private Timestamp CreateDate;

    @JsonIgnore
    private boolean CreateDateDirtyFlag;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    private Timestamp UpdateDate;

    @JsonIgnore
    private boolean UpdateDateDirtyFlag;

    /**
     * 属性 [WFENGINEID]
     *
     */
    private String WFENGINEId;

    @JsonIgnore
    private boolean WFENGINEIdDirtyFlag;

    /**
     * 属性 [PSWFVERSIONID]
     *
     */
    private String PSWFVERSIONId;

    @JsonIgnore
    private boolean PSWFVERSIONIdDirtyFlag;

    /**
     * 属性 [PSWORKFLOWID]
     *
     */
    private String PSWORKFLOWId;

    @JsonIgnore
    private boolean PSWORKFLOWIdDirtyFlag;

    /**
     * 属性 [WFENGINEINSTANCEID]
     *
     */
    private String Wfengineinstanceid;

    @JsonIgnore
    private boolean WfengineinstanceidDirtyFlag;

    /**
     * 属性 [LOGICNAME]
     *
     */
    private String Logicname;

    @JsonIgnore
    private boolean LogicnameDirtyFlag;

    /**
     * 属性 [ISCLOSED]
     *
     */
    private Integer Isclosed;

    @JsonIgnore
    private boolean IsclosedDirtyFlag;

    /**
     * 属性 [WFSTATE]
     *
     */
    private String Wfstate;

    @JsonIgnore
    private boolean WfstateDirtyFlag;

    /**
     * 属性 [STARTTIME]
     *
     */
    private Timestamp Starttime;

    @JsonIgnore
    private boolean StarttimeDirtyFlag;

    /**
     * 属性 [ENDTIME]
     *
     */
    private Timestamp Endtime;

    @JsonIgnore
    private boolean EndtimeDirtyFlag;

    /**
     * 属性 [OWNER]
     *
     */
    private String Owner;

    @JsonIgnore
    private boolean OwnerDirtyFlag;

    /**
     * 属性 [CURSTEP]
     *
     */
    private String Curstep;

    @JsonIgnore
    private boolean CurstepDirtyFlag;

    /**
     * 属性 [PREVSTEP]
     *
     */
    private String Prevstep;

    @JsonIgnore
    private boolean PrevstepDirtyFlag;

    /**
     * 属性 [BSDATA]
     *
     */
    private String Bsdata;

    @JsonIgnore
    private boolean BsdataDirtyFlag;

    /**
     * 属性 [BSWFDATA]
     *
     */
    private String Bswfdata;

    @JsonIgnore
    private boolean BswfdataDirtyFlag;


    /**
     * 获取 [CREATEMAN]
     */
    public String getCreateMan(){
        return CreateMan ;
    }

    /**
     * 设置 [CREATEMAN]
     */
    public void setCreateMan(String  createMan){
        this.CreateMan = createMan ;
        this.CreateManDirtyFlag = true ;
    }

    /**
     * 获取 [CREATEMAN]脏标记
     */
    @JsonIgnore
    public boolean getCreateManDirtyFlag(){
        return CreateManDirtyFlag ;
    }

    /**
     * 获取 [WFINSTANCEID]
     */
    public String getWFINSTANCEId(){
        return WFINSTANCEId ;
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWFINSTANCEId(String  wFINSTANCEId){
        this.WFINSTANCEId = wFINSTANCEId ;
        this.WFINSTANCEIdDirtyFlag = true ;
    }

    /**
     * 获取 [WFINSTANCEID]脏标记
     */
    @JsonIgnore
    public boolean getWFINSTANCEIdDirtyFlag(){
        return WFINSTANCEIdDirtyFlag ;
    }

    /**
     * 获取 [WFINSTANCENAME]
     */
    public String getWFINSTANCEName(){
        return WFINSTANCEName ;
    }

    /**
     * 设置 [WFINSTANCENAME]
     */
    public void setWFINSTANCEName(String  wFINSTANCEName){
        this.WFINSTANCEName = wFINSTANCEName ;
        this.WFINSTANCENameDirtyFlag = true ;
    }

    /**
     * 获取 [WFINSTANCENAME]脏标记
     */
    @JsonIgnore
    public boolean getWFINSTANCENameDirtyFlag(){
        return WFINSTANCENameDirtyFlag ;
    }

    /**
     * 获取 [UPDATEMAN]
     */
    public String getUpdateMan(){
        return UpdateMan ;
    }

    /**
     * 设置 [UPDATEMAN]
     */
    public void setUpdateMan(String  updateMan){
        this.UpdateMan = updateMan ;
        this.UpdateManDirtyFlag = true ;
    }

    /**
     * 获取 [UPDATEMAN]脏标记
     */
    @JsonIgnore
    public boolean getUpdateManDirtyFlag(){
        return UpdateManDirtyFlag ;
    }

    /**
     * 获取 [CREATEDATE]
     */
    public Timestamp getCreateDate(){
        return CreateDate ;
    }

    /**
     * 设置 [CREATEDATE]
     */
    public void setCreateDate(Timestamp  createDate){
        this.CreateDate = createDate ;
        this.CreateDateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATEDATE]脏标记
     */
    @JsonIgnore
    public boolean getCreateDateDirtyFlag(){
        return CreateDateDirtyFlag ;
    }

    /**
     * 获取 [UPDATEDATE]
     */
    public Timestamp getUpdateDate(){
        return UpdateDate ;
    }

    /**
     * 设置 [UPDATEDATE]
     */
    public void setUpdateDate(Timestamp  updateDate){
        this.UpdateDate = updateDate ;
        this.UpdateDateDirtyFlag = true ;
    }

    /**
     * 获取 [UPDATEDATE]脏标记
     */
    @JsonIgnore
    public boolean getUpdateDateDirtyFlag(){
        return UpdateDateDirtyFlag ;
    }

    /**
     * 获取 [WFENGINEID]
     */
    public String getWFENGINEId(){
        return WFENGINEId ;
    }

    /**
     * 设置 [WFENGINEID]
     */
    public void setWFENGINEId(String  wFENGINEId){
        this.WFENGINEId = wFENGINEId ;
        this.WFENGINEIdDirtyFlag = true ;
    }

    /**
     * 获取 [WFENGINEID]脏标记
     */
    @JsonIgnore
    public boolean getWFENGINEIdDirtyFlag(){
        return WFENGINEIdDirtyFlag ;
    }

    /**
     * 获取 [PSWFVERSIONID]
     */
    public String getPSWFVERSIONId(){
        return PSWFVERSIONId ;
    }

    /**
     * 设置 [PSWFVERSIONID]
     */
    public void setPSWFVERSIONId(String  pSWFVERSIONId){
        this.PSWFVERSIONId = pSWFVERSIONId ;
        this.PSWFVERSIONIdDirtyFlag = true ;
    }

    /**
     * 获取 [PSWFVERSIONID]脏标记
     */
    @JsonIgnore
    public boolean getPSWFVERSIONIdDirtyFlag(){
        return PSWFVERSIONIdDirtyFlag ;
    }

    /**
     * 获取 [PSWORKFLOWID]
     */
    public String getPSWORKFLOWId(){
        return PSWORKFLOWId ;
    }

    /**
     * 设置 [PSWORKFLOWID]
     */
    public void setPSWORKFLOWId(String  pSWORKFLOWId){
        this.PSWORKFLOWId = pSWORKFLOWId ;
        this.PSWORKFLOWIdDirtyFlag = true ;
    }

    /**
     * 获取 [PSWORKFLOWID]脏标记
     */
    @JsonIgnore
    public boolean getPSWORKFLOWIdDirtyFlag(){
        return PSWORKFLOWIdDirtyFlag ;
    }

    /**
     * 获取 [WFENGINEINSTANCEID]
     */
    public String getWfengineinstanceid(){
        return Wfengineinstanceid ;
    }

    /**
     * 设置 [WFENGINEINSTANCEID]
     */
    public void setWfengineinstanceid(String  wfengineinstanceid){
        this.Wfengineinstanceid = wfengineinstanceid ;
        this.WfengineinstanceidDirtyFlag = true ;
    }

    /**
     * 获取 [WFENGINEINSTANCEID]脏标记
     */
    @JsonIgnore
    public boolean getWfengineinstanceidDirtyFlag(){
        return WfengineinstanceidDirtyFlag ;
    }

    /**
     * 获取 [LOGICNAME]
     */
    public String getLogicname(){
        return Logicname ;
    }

    /**
     * 设置 [LOGICNAME]
     */
    public void setLogicname(String  logicname){
        this.Logicname = logicname ;
        this.LogicnameDirtyFlag = true ;
    }

    /**
     * 获取 [LOGICNAME]脏标记
     */
    @JsonIgnore
    public boolean getLogicnameDirtyFlag(){
        return LogicnameDirtyFlag ;
    }

    /**
     * 获取 [ISCLOSED]
     */
    public Integer getIsclosed(){
        return Isclosed ;
    }

    /**
     * 设置 [ISCLOSED]
     */
    public void setIsclosed(Integer  isclosed){
        this.Isclosed = isclosed ;
        this.IsclosedDirtyFlag = true ;
    }

    /**
     * 获取 [ISCLOSED]脏标记
     */
    @JsonIgnore
    public boolean getIsclosedDirtyFlag(){
        return IsclosedDirtyFlag ;
    }

    /**
     * 获取 [WFSTATE]
     */
    public String getWfstate(){
        return Wfstate ;
    }

    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(String  wfstate){
        this.Wfstate = wfstate ;
        this.WfstateDirtyFlag = true ;
    }

    /**
     * 获取 [WFSTATE]脏标记
     */
    @JsonIgnore
    public boolean getWfstateDirtyFlag(){
        return WfstateDirtyFlag ;
    }

    /**
     * 获取 [STARTTIME]
     */
    public Timestamp getStarttime(){
        return Starttime ;
    }

    /**
     * 设置 [STARTTIME]
     */
    public void setStarttime(Timestamp  starttime){
        this.Starttime = starttime ;
        this.StarttimeDirtyFlag = true ;
    }

    /**
     * 获取 [STARTTIME]脏标记
     */
    @JsonIgnore
    public boolean getStarttimeDirtyFlag(){
        return StarttimeDirtyFlag ;
    }

    /**
     * 获取 [ENDTIME]
     */
    public Timestamp getEndtime(){
        return Endtime ;
    }

    /**
     * 设置 [ENDTIME]
     */
    public void setEndtime(Timestamp  endtime){
        this.Endtime = endtime ;
        this.EndtimeDirtyFlag = true ;
    }

    /**
     * 获取 [ENDTIME]脏标记
     */
    @JsonIgnore
    public boolean getEndtimeDirtyFlag(){
        return EndtimeDirtyFlag ;
    }

    /**
     * 获取 [OWNER]
     */
    public String getOwner(){
        return Owner ;
    }

    /**
     * 设置 [OWNER]
     */
    public void setOwner(String  owner){
        this.Owner = owner ;
        this.OwnerDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER]脏标记
     */
    @JsonIgnore
    public boolean getOwnerDirtyFlag(){
        return OwnerDirtyFlag ;
    }

    /**
     * 获取 [CURSTEP]
     */
    public String getCurstep(){
        return Curstep ;
    }

    /**
     * 设置 [CURSTEP]
     */
    public void setCurstep(String  curstep){
        this.Curstep = curstep ;
        this.CurstepDirtyFlag = true ;
    }

    /**
     * 获取 [CURSTEP]脏标记
     */
    @JsonIgnore
    public boolean getCurstepDirtyFlag(){
        return CurstepDirtyFlag ;
    }

    /**
     * 获取 [PREVSTEP]
     */
    public String getPrevstep(){
        return Prevstep ;
    }

    /**
     * 设置 [PREVSTEP]
     */
    public void setPrevstep(String  prevstep){
        this.Prevstep = prevstep ;
        this.PrevstepDirtyFlag = true ;
    }

    /**
     * 获取 [PREVSTEP]脏标记
     */
    @JsonIgnore
    public boolean getPrevstepDirtyFlag(){
        return PrevstepDirtyFlag ;
    }

    /**
     * 获取 [BSDATA]
     */
    public String getBsdata(){
        return Bsdata ;
    }

    /**
     * 设置 [BSDATA]
     */
    public void setBsdata(String  bsdata){
        this.Bsdata = bsdata ;
        this.BsdataDirtyFlag = true ;
    }

    /**
     * 获取 [BSDATA]脏标记
     */
    @JsonIgnore
    public boolean getBsdataDirtyFlag(){
        return BsdataDirtyFlag ;
    }

    /**
     * 获取 [BSWFDATA]
     */
    public String getBswfdata(){
        return Bswfdata ;
    }

    /**
     * 设置 [BSWFDATA]
     */
    public void setBswfdata(String  bswfdata){
        this.Bswfdata = bswfdata ;
        this.BswfdataDirtyFlag = true ;
    }

    /**
     * 获取 [BSWFDATA]脏标记
     */
    @JsonIgnore
    public boolean getBswfdataDirtyFlag(){
        return BswfdataDirtyFlag ;
    }




}
