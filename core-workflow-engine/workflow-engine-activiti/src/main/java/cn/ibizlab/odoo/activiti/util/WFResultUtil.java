package cn.ibizlab.odoo.activiti.util;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WFResultUtil {

    public static JSONObject success(String msg){

        JSONObject successObj=new JSONObject();
        successObj.put("rst", 1);
        successObj.put("msg",msg);
        return successObj;
    }

    public static JSONObject error(String msg){

        JSONObject failObj=new JSONObject();
        failObj.put("rst",0);
        failObj.put("msg",msg);
        return failObj;
    }
}
