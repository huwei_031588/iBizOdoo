package cn.ibizlab.odoo.util.valuerule;

import cn.ibizlab.odoo.util.valuerule.condition.VRGroupCondition;

import java.util.ArrayList;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ValueRule<T> {
    protected String name;

    protected String ruleInfo;
    //属性名
    protected String field;
    //值
    protected T value;

    protected VRGroupCondition<T> groupCondition;

    public ValueRule(String name, String ruleInfo, T value) {
        this.name = name;
        this.value = value;
        this.ruleInfo = ruleInfo;
        this.groupCondition = new VRGroupCondition<>("默认组", false, ruleInfo, value).init(true);
    }

    public ValueRule(String name, String ruleInfo, String field, T value) {
        this.name = name;
        this.field = field;
        this.value = value;
        this.ruleInfo = ruleInfo;
        this.groupCondition = new VRGroupCondition<>("默认组", false, ruleInfo, value).init(true);
    }

    public boolean isValid() {
        return groupCondition.isValid();
    }
}
