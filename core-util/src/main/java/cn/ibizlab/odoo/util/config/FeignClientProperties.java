package cn.ibizlab.odoo.util.config;

public interface FeignClientProperties {

	public String getTokenUrl();

	public String getClientId();

	public String getClientSecret();

	public String getServiceUrl();

    public String getServiceId();

}
