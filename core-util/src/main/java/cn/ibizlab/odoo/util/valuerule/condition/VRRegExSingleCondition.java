package cn.ibizlab.odoo.util.valuerule.condition;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import cn.ibizlab.odoo.util.valuerule.VRSingleCondition;
import cn.ibizlab.odoo.util.log.IBIZLog;

import lombok.extern.slf4j.Slf4j;

/**
 * 正则条件（REGEX）
 * @param <T> 当前成员变量类型
 */
@Slf4j
@Data
@IBIZLog
public class VRRegExSingleCondition<T> extends VRSingleCondition<T> {
    //正则式
    private String regex;

    public VRRegExSingleCondition(String name, Boolean isNotMode, String ruleInfo, T value) {
        super(name, isNotMode, ruleInfo, value);
    }

    public VRRegExSingleCondition<T> init(String regex){
        this.regex = regex;
        return this;
    }

    @Override
    public boolean validate() {
        String valuestr = String.valueOf(value == null ? "" : value);
        return valuestr.matches(regex);
    }
}
