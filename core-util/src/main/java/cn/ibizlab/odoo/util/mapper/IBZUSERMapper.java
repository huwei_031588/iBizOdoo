package cn.ibizlab.odoo.util.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.ibizlab.odoo.util.domain.IBZUSER;

public interface IBZUSERMapper extends BaseMapper<IBZUSER>{

}