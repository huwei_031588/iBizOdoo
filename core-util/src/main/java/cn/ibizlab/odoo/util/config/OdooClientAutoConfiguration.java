package cn.ibizlab.odoo.util.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(OdooClientConfiguration.class)
@ConditionalOnWebApplication
@EnableConfigurationProperties(OdooClientProperties.class)

public class OdooClientAutoConfiguration {
}
