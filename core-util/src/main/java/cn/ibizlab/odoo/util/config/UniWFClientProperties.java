package cn.ibizlab.odoo.util.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.wf.uni")
@Data
public class UniWFClientProperties {

	private String tokenUrl;

	private String clientId;

	private String clientSecret;

	private String serviceUrl;

    private String serviceId;

}
