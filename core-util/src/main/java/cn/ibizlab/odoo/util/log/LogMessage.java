package cn.ibizlab.odoo.util.log;

import com.alibaba.fastjson.JSON;

public enum LogMessage {
    //方法开始
    FUNCTION_START("Begin. "),
    //方法正常结束
    FUNCTION_END("Normal end. "),
    //方法入参
    PARAMS("Params:{}. "),
    //方法返回值
    RETURNED_INFO("Returned info:{}. "),
    //方法返回值
    RETURNED_VALUE("Returned values:{}. "),
    //抛出检查（可预期的）异常。
    FUNCTION_EXCEPTION_END ("Abnormal end. "),
    //抛出意外运行时异常
    FUCNTION_ERROR_END("Exception end. "),
    //抛出运行时异常对应的信息。
    EXCEPTION_MSG("Exception message:{}. "),
    //耗时检查
    TIME_COST("Cost {} ms. "),
    //循环
    FOR("For loop, condition on({}) "),
    WHILE("While loop, condition on({}) "),
    //swich语句
    SWITCH("Switch，case on({})" );

    public String msg;

    LogMessage(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public static String getStr(Object params) {
        String paramStr = null;
        try {
            paramStr = JSON.toJSONString(params);
        } catch (Exception e) {
            paramStr = params + "";
        }
        return paramStr;
    }
}
