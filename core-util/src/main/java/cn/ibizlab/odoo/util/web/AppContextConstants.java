package cn.ibizlab.odoo.util.web;

public class AppContextConstants {

	/**
	 * 应用上下文，用户标识
	 */
	final public static String CONTEXT_USERID = "srfuserid";

	/**
	 * 应用上下文，用户名称
	 */
	final public static String CONTEXT_USERNAME = "srfusername";

	/**
	 * 应用上下文，用户图像路径
	 */
	final public static String CONTEXT_USERICONPATH = "srfusericonpath";

	/**
	 * 应用上下文，当前的用户模式
	 */
	final public static String CONTEXT_USERMODE = "srfusermode";

	/**
	 * 应用上下文，登录名称
	 */
	public final static String CONTEXT_LOGINNAME = "srfloginname";

	/**
	 * 应用上下文，本地化（Spring变量）
	 */
	public final static String CONTEXT_LOCALE = "srflocale";

	/**
	 * 应用上下文，当前用户时区标识
	 */
	public final static String CONTEXT_TIMEZONE = "srftimezone";

	/**
	 * 应用上下文，当前用户组织标识
	 */
	public final static String CONTEXT_ORGID = "srforgid";

	/**
	 * 应用上下文，当前用户组织名称
	 */
	public final static String CONTEXT_ORGNAME = "srforgname";

	/**
	 * 应用上下文，当前用户组织部门标识
	 */
	public final static String CONTEXT_ORGSECTORID = "srforgsectorid";

	/**
	 * 应用上下文，当前用户组织部门名称
	 */
	public final static String CONTEXT_ORGSECTORNAME = "srforgsectorname";

	/**
	 * 应用上下文，当前用户条线代码
	 */
	public final static String CONTEXT_ORGSECTORBC = "srfsectorbc";
	
	
	/**
	 * 相关部门
	 */
	public final static String CONTEXT_ORGSECTORS = "srforgsectors";

}


