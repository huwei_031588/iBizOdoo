package cn.ibizlab.odoo.util.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

import cn.ibizlab.odoo.util.config.FeignClientProperties;

@ConfigurationProperties(prefix = "client.wf.engine.activiti")
@Data
public class WFActivitiEngineClientProperties implements FeignClientProperties {

	private String tokenUrl;

	private String clientId;

	private String clientSecret;

	private String serviceUrl;

    private String serviceId;

}
