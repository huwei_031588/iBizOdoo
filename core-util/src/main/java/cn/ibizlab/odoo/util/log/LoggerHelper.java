package cn.ibizlab.odoo.util.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static cn.ibizlab.odoo.util.log.LogMessage.*;

public class LoggerHelper {
    //用于记录方法开始时间戳
    public static ThreadLocal<Long> startSet = new ThreadLocal<>();

    /**
     * 打印日志：开始方法（业务调用）
     *
     * @param params 调用原方法入参
     */
    public static void startLog(Object... params) {
        String loggerName = getClassName();
        Logger log = LoggerFactory.getLogger(loggerName);
        log.info(FUNCTION_START.getMsg(), loggerName);
        log.debug(PARAMS.getMsg(), getStr(params));
        startSet.set(System.currentTimeMillis());
    }

    /**
     * 打印日志：结束方法（业务调用）
     *
     * @param params 调用工具类的原方法返回值。
     */
    public static void endLog(Object... params) {
        Long start = startSet.get();
        String loggerName = getClassName();
        Logger log = LoggerFactory.getLogger(loggerName);
        log.info(FUNCTION_END.getMsg(), loggerName);
        log.debug(RETURNED_VALUE.getMsg(), getStr(params));

        if (start != null) {
            startSet.remove();
            log.debug(TIME_COST.getMsg(), System.currentTimeMillis() - start);
        }
    }
    public static void info(LogMessage message,Object... params){
        LoggerFactory.getLogger(getClassName()).info(message.getMsg(),params);
    }
    public static void debug(LogMessage message,Object...params){
        LoggerFactory.getLogger(getClassName()).debug(message.getMsg(),params);
    }
    public static void error(LogMessage message,Object...params){
        LoggerFactory.getLogger(getClassName()).error(message.getMsg(),params);
    }
    public static void warn(LogMessage message,Object...params){
        LoggerFactory.getLogger(getClassName()).warn(message.getMsg(),params);
    }
    public static void error(String msg) {
        LoggerFactory.getLogger(getClassName()).error(msg);
    }

    public static void error(String msg, Object... obj) {
        LoggerFactory.getLogger(getClassName()).error(msg, obj);
    }

    public static void warn(String msg) {
        LoggerFactory.getLogger(getClassName()).error(msg);
    }

    public static void warn(String msg, Object... obj) {
        LoggerFactory.getLogger(getClassName()).error(msg, obj);
    }

    public static void info(String msg) {
        LoggerFactory.getLogger(getClassName()).info(msg);
    }

    public static void info(String msg, Object... obj) {
        LoggerFactory.getLogger(getClassName()).info(msg, obj);
    }

    public static void debug(String msg) {
        LoggerFactory.getLogger(getClassName()).debug(msg);
    }

    public static void debug(String msg, Object... obj) {
        LoggerFactory.getLogger(getClassName()).debug(msg, obj);
    }

    /**
     * 获取调用 LoggerUtil工具静态类的类名
     *
     * @return 长类名.方法名(行数)
     * ps: 获取【行数】影响性能，生产环境中不建议打印行数。
     */
    private static String getClassName() {
        StackTraceElement element = Thread.currentThread().getStackTrace()[3];
        //从程序栈中获取定位信息
        String className = element.getClassName();
        String methodName = element.getMethodName();
        int lineNum = element.getLineNumber();
//        return className + "." + methodName;
        return String.format("%s.%s(%s)",className ,methodName,lineNum);
    }

}