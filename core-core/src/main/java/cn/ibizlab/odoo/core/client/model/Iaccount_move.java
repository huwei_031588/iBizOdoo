package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_move] 对象
 */
public interface Iaccount_move {

    /**
     * 获取 [金额]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [金额]
     */
    public Double getAmount();

    /**
     * 获取 [金额]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [自动撤销]
     */
    public void setAuto_reverse(String auto_reverse);
    
    /**
     * 设置 [自动撤销]
     */
    public String getAuto_reverse();

    /**
     * 获取 [自动撤销]脏标记
     */
    public boolean getAuto_reverseDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [日期]
     */
    public void setDate(Timestamp date);
    
    /**
     * 设置 [日期]
     */
    public Timestamp getDate();

    /**
     * 获取 [日期]脏标记
     */
    public boolean getDateDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [科目]
     */
    public void setDummy_account_id(Integer dummy_account_id);
    
    /**
     * 设置 [科目]
     */
    public Integer getDummy_account_id();

    /**
     * 获取 [科目]脏标记
     */
    public boolean getDummy_account_idDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id(Integer journal_id);
    
    /**
     * 设置 [日记账]
     */
    public Integer getJournal_id();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_idDirtyFlag();
    /**
     * 获取 [日记账]
     */
    public void setJournal_id_text(String journal_id_text);
    
    /**
     * 设置 [日记账]
     */
    public String getJournal_id_text();

    /**
     * 获取 [日记账]脏标记
     */
    public boolean getJournal_id_textDirtyFlag();
    /**
     * 获取 [日记账项目]
     */
    public void setLine_ids(String line_ids);
    
    /**
     * 设置 [日记账项目]
     */
    public String getLine_ids();

    /**
     * 获取 [日记账项目]脏标记
     */
    public boolean getLine_idsDirtyFlag();
    /**
     * 获取 [匹配百分比]
     */
    public void setMatched_percentage(Double matched_percentage);
    
    /**
     * 设置 [匹配百分比]
     */
    public Double getMatched_percentage();

    /**
     * 获取 [匹配百分比]脏标记
     */
    public boolean getMatched_percentageDirtyFlag();
    /**
     * 获取 [号码]
     */
    public void setName(String name);
    
    /**
     * 设置 [号码]
     */
    public String getName();

    /**
     * 获取 [号码]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [内部备注]
     */
    public void setNarration(String narration);
    
    /**
     * 设置 [内部备注]
     */
    public String getNarration();

    /**
     * 获取 [内部备注]脏标记
     */
    public boolean getNarrationDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [业务伙伴]
     */
    public Integer getPartner_id();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [业务伙伴]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [业务伙伴]
     */
    public String getPartner_id_text();

    /**
     * 获取 [业务伙伴]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [参考]
     */
    public void setRef(String ref);
    
    /**
     * 设置 [参考]
     */
    public String getRef();

    /**
     * 获取 [参考]脏标记
     */
    public boolean getRefDirtyFlag();
    /**
     * 获取 [撤销日期]
     */
    public void setReverse_date(Timestamp reverse_date);
    
    /**
     * 设置 [撤销日期]
     */
    public Timestamp getReverse_date();

    /**
     * 获取 [撤销日期]脏标记
     */
    public boolean getReverse_dateDirtyFlag();
    /**
     * 获取 [撤销分录]
     */
    public void setReverse_entry_id(Integer reverse_entry_id);
    
    /**
     * 设置 [撤销分录]
     */
    public Integer getReverse_entry_id();

    /**
     * 获取 [撤销分录]脏标记
     */
    public boolean getReverse_entry_idDirtyFlag();
    /**
     * 获取 [撤销分录]
     */
    public void setReverse_entry_id_text(String reverse_entry_id_text);
    
    /**
     * 设置 [撤销分录]
     */
    public String getReverse_entry_id_text();

    /**
     * 获取 [撤销分录]脏标记
     */
    public boolean getReverse_entry_id_textDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [库存移动]
     */
    public void setStock_move_id(Integer stock_move_id);
    
    /**
     * 设置 [库存移动]
     */
    public Integer getStock_move_id();

    /**
     * 获取 [库存移动]脏标记
     */
    public boolean getStock_move_idDirtyFlag();
    /**
     * 获取 [库存移动]
     */
    public void setStock_move_id_text(String stock_move_id_text);
    
    /**
     * 设置 [库存移动]
     */
    public String getStock_move_id_text();

    /**
     * 获取 [库存移动]脏标记
     */
    public boolean getStock_move_id_textDirtyFlag();
    /**
     * 获取 [税率现金收付制分录]
     */
    public void setTax_cash_basis_rec_id(Integer tax_cash_basis_rec_id);
    
    /**
     * 设置 [税率现金收付制分录]
     */
    public Integer getTax_cash_basis_rec_id();

    /**
     * 获取 [税率现金收付制分录]脏标记
     */
    public boolean getTax_cash_basis_rec_idDirtyFlag();
    /**
     * 获取 [税类别域]
     */
    public void setTax_type_domain(String tax_type_domain);
    
    /**
     * 设置 [税类别域]
     */
    public String getTax_type_domain();

    /**
     * 获取 [税类别域]脏标记
     */
    public boolean getTax_type_domainDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
