package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [sale_order_line] 对象
 */
public interface sale_order_line {

    public String getAnalytic_line_ids();

    public void setAnalytic_line_ids(String analytic_line_ids);

    public String getAnalytic_tag_ids();

    public void setAnalytic_tag_ids(String analytic_tag_ids);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getCurrency_id();

    public void setCurrency_id(Integer currency_id);

    public String getCurrency_id_text();

    public void setCurrency_id_text(String currency_id_text);

    public Double getCustomer_lead();

    public void setCustomer_lead(Double customer_lead);

    public Double getDiscount();

    public void setDiscount(Double discount);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getDisplay_type();

    public void setDisplay_type(String display_type);

    public Integer getEvent_id();

    public void setEvent_id(Integer event_id);

    public String getEvent_id_text();

    public void setEvent_id_text(String event_id_text);

    public String getEvent_ok();

    public void setEvent_ok(String event_ok);

    public Integer getEvent_ticket_id();

    public void setEvent_ticket_id(Integer event_ticket_id);

    public String getEvent_ticket_id_text();

    public void setEvent_ticket_id_text(String event_ticket_id_text);

    public Integer getId();

    public void setId(Integer id);

    public String getInvoice_lines();

    public void setInvoice_lines(String invoice_lines);

    public String getInvoice_status();

    public void setInvoice_status(String invoice_status);

    public String getIs_downpayment();

    public void setIs_downpayment(String is_downpayment);

    public String getIs_expense();

    public void setIs_expense(String is_expense);

    public Integer getLinked_line_id();

    public void setLinked_line_id(Integer linked_line_id);

    public String getLinked_line_id_text();

    public void setLinked_line_id_text(String linked_line_id_text);

    public String getMove_ids();

    public void setMove_ids(String move_ids);

    public String getName();

    public void setName(String name);

    public String getName_short();

    public void setName_short(String name_short);

    public String getOption_line_ids();

    public void setOption_line_ids(String option_line_ids);

    public Integer getOrder_id();

    public void setOrder_id(Integer order_id);

    public String getOrder_id_text();

    public void setOrder_id_text(String order_id_text);

    public Integer getOrder_partner_id();

    public void setOrder_partner_id(Integer order_partner_id);

    public String getOrder_partner_id_text();

    public void setOrder_partner_id_text(String order_partner_id_text);

    public Double getPrice_reduce();

    public void setPrice_reduce(Double price_reduce);

    public Double getPrice_reduce_taxexcl();

    public void setPrice_reduce_taxexcl(Double price_reduce_taxexcl);

    public Double getPrice_reduce_taxinc();

    public void setPrice_reduce_taxinc(Double price_reduce_taxinc);

    public Double getPrice_subtotal();

    public void setPrice_subtotal(Double price_subtotal);

    public Double getPrice_tax();

    public void setPrice_tax(Double price_tax);

    public Double getPrice_total();

    public void setPrice_total(Double price_total);

    public Double getPrice_unit();

    public void setPrice_unit(Double price_unit);

    public String getProduct_custom_attribute_value_ids();

    public void setProduct_custom_attribute_value_ids(String product_custom_attribute_value_ids);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public byte[] getProduct_image();

    public void setProduct_image(byte[] product_image);

    public String getProduct_no_variant_attribute_value_ids();

    public void setProduct_no_variant_attribute_value_ids(String product_no_variant_attribute_value_ids);

    public Integer getProduct_packaging();

    public void setProduct_packaging(Integer product_packaging);

    public String getProduct_packaging_text();

    public void setProduct_packaging_text(String product_packaging_text);

    public Integer getProduct_uom();

    public void setProduct_uom(Integer product_uom);

    public Double getProduct_uom_qty();

    public void setProduct_uom_qty(Double product_uom_qty);

    public String getProduct_uom_text();

    public void setProduct_uom_text(String product_uom_text);

    public String getProduct_updatable();

    public void setProduct_updatable(String product_updatable);

    public Integer getPurchase_line_count();

    public void setPurchase_line_count(Integer purchase_line_count);

    public String getPurchase_line_ids();

    public void setPurchase_line_ids(String purchase_line_ids);

    public Double getQty_delivered();

    public void setQty_delivered(Double qty_delivered);

    public Double getQty_delivered_manual();

    public void setQty_delivered_manual(Double qty_delivered_manual);

    public String getQty_delivered_method();

    public void setQty_delivered_method(String qty_delivered_method);

    public Double getQty_invoiced();

    public void setQty_invoiced(Double qty_invoiced);

    public Double getQty_to_invoice();

    public void setQty_to_invoice(Double qty_to_invoice);

    public Integer getRoute_id();

    public void setRoute_id(Integer route_id);

    public String getRoute_id_text();

    public void setRoute_id_text(String route_id_text);

    public Integer getSalesman_id();

    public void setSalesman_id(Integer salesman_id);

    public String getSalesman_id_text();

    public void setSalesman_id_text(String salesman_id_text);

    public String getSale_order_option_ids();

    public void setSale_order_option_ids(String sale_order_option_ids);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public String getState();

    public void setState(String state);

    public String getTax_id();

    public void setTax_id(String tax_id);

    public Double getUntaxed_amount_invoiced();

    public void setUntaxed_amount_invoiced(Double untaxed_amount_invoiced);

    public Double getUntaxed_amount_to_invoice();

    public void setUntaxed_amount_to_invoice(Double untaxed_amount_to_invoice);

    public String getWarning_stock();

    public void setWarning_stock(String warning_stock);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
