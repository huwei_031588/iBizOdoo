package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mailSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_mailService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_event.client.event_mailOdooClient;
import cn.ibizlab.odoo.core.odoo_event.clientmodel.event_mailClientModel;

/**
 * 实体[自动发邮件] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_mailServiceImpl implements IEvent_mailService {

    @Autowired
    event_mailOdooClient event_mailOdooClient;


    @Override
    public boolean create(Event_mail et) {
        event_mailClientModel clientModel = convert2Model(et,null);
		event_mailOdooClient.create(clientModel);
        Event_mail rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_mail> list){
    }

    @Override
    public boolean remove(Integer id) {
        event_mailClientModel clientModel = new event_mailClientModel();
        clientModel.setId(id);
		event_mailOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Event_mail et) {
        event_mailClientModel clientModel = convert2Model(et,null);
		event_mailOdooClient.update(clientModel);
        Event_mail rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Event_mail> list){
    }

    @Override
    public Event_mail get(Integer id) {
        event_mailClientModel clientModel = new event_mailClientModel();
        clientModel.setId(id);
		event_mailOdooClient.get(clientModel);
        Event_mail et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Event_mail();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_mail> searchDefault(Event_mailSearchContext context) {
        List<Event_mail> list = new ArrayList<Event_mail>();
        Page<event_mailClientModel> clientModelList = event_mailOdooClient.search(context);
        for(event_mailClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Event_mail>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public event_mailClientModel convert2Model(Event_mail domain , event_mailClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new event_mailClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("mail_registration_idsdirtyflag"))
                model.setMail_registration_ids(domain.getMailRegistrationIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("interval_nbrdirtyflag"))
                model.setInterval_nbr(domain.getIntervalNbr());
            if((Boolean) domain.getExtensionparams().get("donedirtyflag"))
                model.setDone(domain.getDone());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("interval_typedirtyflag"))
                model.setInterval_type(domain.getIntervalType());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("mail_sentdirtyflag"))
                model.setMail_sent(domain.getMailSent());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("scheduled_datedirtyflag"))
                model.setScheduled_date(domain.getScheduledDate());
            if((Boolean) domain.getExtensionparams().get("interval_unitdirtyflag"))
                model.setInterval_unit(domain.getIntervalUnit());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("event_id_textdirtyflag"))
                model.setEvent_id_text(domain.getEventIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("template_id_textdirtyflag"))
                model.setTemplate_id_text(domain.getTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("event_iddirtyflag"))
                model.setEvent_id(domain.getEventId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("template_iddirtyflag"))
                model.setTemplate_id(domain.getTemplateId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Event_mail convert2Domain( event_mailClientModel model ,Event_mail domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Event_mail();
        }

        if(model.getMail_registration_idsDirtyFlag())
            domain.setMailRegistrationIds(model.getMail_registration_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getInterval_nbrDirtyFlag())
            domain.setIntervalNbr(model.getInterval_nbr());
        if(model.getDoneDirtyFlag())
            domain.setDone(model.getDone());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getInterval_typeDirtyFlag())
            domain.setIntervalType(model.getInterval_type());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMail_sentDirtyFlag())
            domain.setMailSent(model.getMail_sent());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getScheduled_dateDirtyFlag())
            domain.setScheduledDate(model.getScheduled_date());
        if(model.getInterval_unitDirtyFlag())
            domain.setIntervalUnit(model.getInterval_unit());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getEvent_id_textDirtyFlag())
            domain.setEventIdText(model.getEvent_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getTemplate_id_textDirtyFlag())
            domain.setTemplateIdText(model.getTemplate_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getEvent_idDirtyFlag())
            domain.setEventId(model.getEvent_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getTemplate_idDirtyFlag())
            domain.setTemplateId(model.getTemplate_id());
        return domain ;
    }

}

    



