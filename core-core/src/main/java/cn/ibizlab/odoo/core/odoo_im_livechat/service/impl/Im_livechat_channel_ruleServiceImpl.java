package cn.ibizlab.odoo.core.odoo_im_livechat.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_channel_ruleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_im_livechat.client.im_livechat_channel_ruleOdooClient;
import cn.ibizlab.odoo.core.odoo_im_livechat.clientmodel.im_livechat_channel_ruleClientModel;

/**
 * 实体[实时聊天频道规则] 服务对象接口实现
 */
@Slf4j
@Service
public class Im_livechat_channel_ruleServiceImpl implements IIm_livechat_channel_ruleService {

    @Autowired
    im_livechat_channel_ruleOdooClient im_livechat_channel_ruleOdooClient;


    @Override
    public Im_livechat_channel_rule get(Integer id) {
        im_livechat_channel_ruleClientModel clientModel = new im_livechat_channel_ruleClientModel();
        clientModel.setId(id);
		im_livechat_channel_ruleOdooClient.get(clientModel);
        Im_livechat_channel_rule et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Im_livechat_channel_rule();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Im_livechat_channel_rule et) {
        im_livechat_channel_ruleClientModel clientModel = convert2Model(et,null);
		im_livechat_channel_ruleOdooClient.update(clientModel);
        Im_livechat_channel_rule rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Im_livechat_channel_rule> list){
    }

    @Override
    public boolean create(Im_livechat_channel_rule et) {
        im_livechat_channel_ruleClientModel clientModel = convert2Model(et,null);
		im_livechat_channel_ruleOdooClient.create(clientModel);
        Im_livechat_channel_rule rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Im_livechat_channel_rule> list){
    }

    @Override
    public boolean remove(Integer id) {
        im_livechat_channel_ruleClientModel clientModel = new im_livechat_channel_ruleClientModel();
        clientModel.setId(id);
		im_livechat_channel_ruleOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Im_livechat_channel_rule> searchDefault(Im_livechat_channel_ruleSearchContext context) {
        List<Im_livechat_channel_rule> list = new ArrayList<Im_livechat_channel_rule>();
        Page<im_livechat_channel_ruleClientModel> clientModelList = im_livechat_channel_ruleOdooClient.search(context);
        for(im_livechat_channel_ruleClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Im_livechat_channel_rule>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public im_livechat_channel_ruleClientModel convert2Model(Im_livechat_channel_rule domain , im_livechat_channel_ruleClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new im_livechat_channel_ruleClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("auto_popup_timerdirtyflag"))
                model.setAuto_popup_timer(domain.getAutoPopupTimer());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("regex_urldirtyflag"))
                model.setRegex_url(domain.getRegexUrl());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("actiondirtyflag"))
                model.setAction(domain.getAction());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("country_idsdirtyflag"))
                model.setCountry_ids(domain.getCountryIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("channel_id_textdirtyflag"))
                model.setChannel_id_text(domain.getChannelIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("channel_iddirtyflag"))
                model.setChannel_id(domain.getChannelId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Im_livechat_channel_rule convert2Domain( im_livechat_channel_ruleClientModel model ,Im_livechat_channel_rule domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Im_livechat_channel_rule();
        }

        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getAuto_popup_timerDirtyFlag())
            domain.setAutoPopupTimer(model.getAuto_popup_timer());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getRegex_urlDirtyFlag())
            domain.setRegexUrl(model.getRegex_url());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getActionDirtyFlag())
            domain.setAction(model.getAction());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCountry_idsDirtyFlag())
            domain.setCountryIds(model.getCountry_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getChannel_id_textDirtyFlag())
            domain.setChannelIdText(model.getChannel_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getChannel_idDirtyFlag())
            domain.setChannelId(model.getChannel_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



