package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.im_livechat_report_operator;

/**
 * 实体[im_livechat_report_operator] 服务对象接口
 */
public interface im_livechat_report_operatorRepository{


    public im_livechat_report_operator createPO() ;
        public void createBatch(im_livechat_report_operator im_livechat_report_operator);

        public void updateBatch(im_livechat_report_operator im_livechat_report_operator);

        public List<im_livechat_report_operator> search();

        public void removeBatch(String id);

        public void remove(String id);

        public void create(im_livechat_report_operator im_livechat_report_operator);

        public void update(im_livechat_report_operator im_livechat_report_operator);

        public void get(String id);


}
