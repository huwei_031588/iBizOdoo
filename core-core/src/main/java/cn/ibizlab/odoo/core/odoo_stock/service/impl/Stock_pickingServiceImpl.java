package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_pickingSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_pickingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_pickingOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_pickingClientModel;

/**
 * 实体[调拨] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_pickingServiceImpl implements IStock_pickingService {

    @Autowired
    stock_pickingOdooClient stock_pickingOdooClient;


    @Override
    public Stock_picking get(Integer id) {
        stock_pickingClientModel clientModel = new stock_pickingClientModel();
        clientModel.setId(id);
		stock_pickingOdooClient.get(clientModel);
        Stock_picking et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_picking();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        stock_pickingClientModel clientModel = new stock_pickingClientModel();
        clientModel.setId(id);
		stock_pickingOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Stock_picking et) {
        stock_pickingClientModel clientModel = convert2Model(et,null);
		stock_pickingOdooClient.update(clientModel);
        Stock_picking rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_picking> list){
    }

    @Override
    public boolean create(Stock_picking et) {
        stock_pickingClientModel clientModel = convert2Model(et,null);
		stock_pickingOdooClient.create(clientModel);
        Stock_picking rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_picking> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_picking> searchDefault(Stock_pickingSearchContext context) {
        List<Stock_picking> list = new ArrayList<Stock_picking>();
        Page<stock_pickingClientModel> clientModelList = stock_pickingOdooClient.search(context);
        for(stock_pickingClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_picking>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_pickingClientModel convert2Model(Stock_picking domain , stock_pickingClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_pickingClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("package_level_idsdirtyflag"))
                model.setPackage_level_ids(domain.getPackageLevelIds());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("printeddirtyflag"))
                model.setPrinted(domain.getPrinted());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("move_typedirtyflag"))
                model.setMove_type(domain.getMoveType());
            if((Boolean) domain.getExtensionparams().get("backorder_idsdirtyflag"))
                model.setBackorder_ids(domain.getBackorderIds());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("has_packagesdirtyflag"))
                model.setHas_packages(domain.getHasPackages());
            if((Boolean) domain.getExtensionparams().get("origindirtyflag"))
                model.setOrigin(domain.getOrigin());
            if((Boolean) domain.getExtensionparams().get("is_lockeddirtyflag"))
                model.setIs_locked(domain.getIsLocked());
            if((Boolean) domain.getExtensionparams().get("show_operationsdirtyflag"))
                model.setShow_operations(domain.getShowOperations());
            if((Boolean) domain.getExtensionparams().get("group_iddirtyflag"))
                model.setGroup_id(domain.getGroupId());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("move_line_ids_without_packagedirtyflag"))
                model.setMove_line_ids_without_package(domain.getMoveLineIdsWithoutPackage());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("move_line_idsdirtyflag"))
                model.setMove_line_ids(domain.getMoveLineIds());
            if((Boolean) domain.getExtensionparams().get("package_level_ids_detailsdirtyflag"))
                model.setPackage_level_ids_details(domain.getPackageLevelIdsDetails());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("date_donedirtyflag"))
                model.setDate_done(domain.getDateDone());
            if((Boolean) domain.getExtensionparams().get("has_trackingdirtyflag"))
                model.setHas_tracking(domain.getHasTracking());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("immediate_transferdirtyflag"))
                model.setImmediate_transfer(domain.getImmediateTransfer());
            if((Boolean) domain.getExtensionparams().get("has_scrap_movedirtyflag"))
                model.setHas_scrap_move(domain.getHasScrapMove());
            if((Boolean) domain.getExtensionparams().get("move_ids_without_packagedirtyflag"))
                model.setMove_ids_without_package(domain.getMoveIdsWithoutPackage());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("show_validatedirtyflag"))
                model.setShow_validate(domain.getShowValidate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("prioritydirtyflag"))
                model.setPriority(domain.getPriority());
            if((Boolean) domain.getExtensionparams().get("move_linesdirtyflag"))
                model.setMove_lines(domain.getMoveLines());
            if((Boolean) domain.getExtensionparams().get("purchase_iddirtyflag"))
                model.setPurchase_id(domain.getPurchaseId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("show_mark_as_tododirtyflag"))
                model.setShow_mark_as_todo(domain.getShowMarkAsTodo());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("move_line_existdirtyflag"))
                model.setMove_line_exist(domain.getMoveLineExist());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("scheduled_datedirtyflag"))
                model.setScheduled_date(domain.getScheduledDate());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("show_check_availabilitydirtyflag"))
                model.setShow_check_availability(domain.getShowCheckAvailability());
            if((Boolean) domain.getExtensionparams().get("show_lots_textdirtyflag"))
                model.setShow_lots_text(domain.getShowLotsText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("backorder_id_textdirtyflag"))
                model.setBackorder_id_text(domain.getBackorderIdText());
            if((Boolean) domain.getExtensionparams().get("picking_type_id_textdirtyflag"))
                model.setPicking_type_id_text(domain.getPickingTypeIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("location_dest_id_textdirtyflag"))
                model.setLocation_dest_id_text(domain.getLocationDestIdText());
            if((Boolean) domain.getExtensionparams().get("picking_type_codedirtyflag"))
                model.setPicking_type_code(domain.getPickingTypeCode());
            if((Boolean) domain.getExtensionparams().get("picking_type_entire_packsdirtyflag"))
                model.setPicking_type_entire_packs(domain.getPickingTypeEntirePacks());
            if((Boolean) domain.getExtensionparams().get("owner_id_textdirtyflag"))
                model.setOwner_id_text(domain.getOwnerIdText());
            if((Boolean) domain.getExtensionparams().get("sale_id_textdirtyflag"))
                model.setSale_id_text(domain.getSaleIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("picking_type_iddirtyflag"))
                model.setPicking_type_id(domain.getPickingTypeId());
            if((Boolean) domain.getExtensionparams().get("sale_iddirtyflag"))
                model.setSale_id(domain.getSaleId());
            if((Boolean) domain.getExtensionparams().get("location_dest_iddirtyflag"))
                model.setLocation_dest_id(domain.getLocationDestId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("owner_iddirtyflag"))
                model.setOwner_id(domain.getOwnerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("backorder_iddirtyflag"))
                model.setBackorder_id(domain.getBackorderId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_picking convert2Domain( stock_pickingClientModel model ,Stock_picking domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_picking();
        }

        if(model.getPackage_level_idsDirtyFlag())
            domain.setPackageLevelIds(model.getPackage_level_ids());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getPrintedDirtyFlag())
            domain.setPrinted(model.getPrinted());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getMove_typeDirtyFlag())
            domain.setMoveType(model.getMove_type());
        if(model.getBackorder_idsDirtyFlag())
            domain.setBackorderIds(model.getBackorder_ids());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getHas_packagesDirtyFlag())
            domain.setHasPackages(model.getHas_packages());
        if(model.getOriginDirtyFlag())
            domain.setOrigin(model.getOrigin());
        if(model.getIs_lockedDirtyFlag())
            domain.setIsLocked(model.getIs_locked());
        if(model.getShow_operationsDirtyFlag())
            domain.setShowOperations(model.getShow_operations());
        if(model.getGroup_idDirtyFlag())
            domain.setGroupId(model.getGroup_id());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMove_line_ids_without_packageDirtyFlag())
            domain.setMoveLineIdsWithoutPackage(model.getMove_line_ids_without_package());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMove_line_idsDirtyFlag())
            domain.setMoveLineIds(model.getMove_line_ids());
        if(model.getPackage_level_ids_detailsDirtyFlag())
            domain.setPackageLevelIdsDetails(model.getPackage_level_ids_details());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getDate_doneDirtyFlag())
            domain.setDateDone(model.getDate_done());
        if(model.getHas_trackingDirtyFlag())
            domain.setHasTracking(model.getHas_tracking());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getImmediate_transferDirtyFlag())
            domain.setImmediateTransfer(model.getImmediate_transfer());
        if(model.getHas_scrap_moveDirtyFlag())
            domain.setHasScrapMove(model.getHas_scrap_move());
        if(model.getMove_ids_without_packageDirtyFlag())
            domain.setMoveIdsWithoutPackage(model.getMove_ids_without_package());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getShow_validateDirtyFlag())
            domain.setShowValidate(model.getShow_validate());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPriorityDirtyFlag())
            domain.setPriority(model.getPriority());
        if(model.getMove_linesDirtyFlag())
            domain.setMoveLines(model.getMove_lines());
        if(model.getPurchase_idDirtyFlag())
            domain.setPurchaseId(model.getPurchase_id());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getShow_mark_as_todoDirtyFlag())
            domain.setShowMarkAsTodo(model.getShow_mark_as_todo());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getMove_line_existDirtyFlag())
            domain.setMoveLineExist(model.getMove_line_exist());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getScheduled_dateDirtyFlag())
            domain.setScheduledDate(model.getScheduled_date());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getShow_check_availabilityDirtyFlag())
            domain.setShowCheckAvailability(model.getShow_check_availability());
        if(model.getShow_lots_textDirtyFlag())
            domain.setShowLotsText(model.getShow_lots_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getBackorder_id_textDirtyFlag())
            domain.setBackorderIdText(model.getBackorder_id_text());
        if(model.getPicking_type_id_textDirtyFlag())
            domain.setPickingTypeIdText(model.getPicking_type_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getLocation_dest_id_textDirtyFlag())
            domain.setLocationDestIdText(model.getLocation_dest_id_text());
        if(model.getPicking_type_codeDirtyFlag())
            domain.setPickingTypeCode(model.getPicking_type_code());
        if(model.getPicking_type_entire_packsDirtyFlag())
            domain.setPickingTypeEntirePacks(model.getPicking_type_entire_packs());
        if(model.getOwner_id_textDirtyFlag())
            domain.setOwnerIdText(model.getOwner_id_text());
        if(model.getSale_id_textDirtyFlag())
            domain.setSaleIdText(model.getSale_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getPicking_type_idDirtyFlag())
            domain.setPickingTypeId(model.getPicking_type_id());
        if(model.getSale_idDirtyFlag())
            domain.setSaleId(model.getSale_id());
        if(model.getLocation_dest_idDirtyFlag())
            domain.setLocationDestId(model.getLocation_dest_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getOwner_idDirtyFlag())
            domain.setOwnerId(model.getOwner_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getBackorder_idDirtyFlag())
            domain.setBackorderId(model.getBackorder_id());
        return domain ;
    }

}

    



