package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [hr_leave_report] 对象
 */
public interface Ihr_leave_report {

    /**
     * 获取 [员工标签]
     */
    public void setCategory_id(Integer category_id);
    
    /**
     * 设置 [员工标签]
     */
    public Integer getCategory_id();

    /**
     * 获取 [员工标签]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [员工标签]
     */
    public void setCategory_id_text(String category_id_text);
    
    /**
     * 设置 [员工标签]
     */
    public String getCategory_id_text();

    /**
     * 获取 [员工标签]脏标记
     */
    public boolean getCategory_id_textDirtyFlag();
    /**
     * 获取 [开始日期]
     */
    public void setDate_from(Timestamp date_from);
    
    /**
     * 设置 [开始日期]
     */
    public Timestamp getDate_from();

    /**
     * 获取 [开始日期]脏标记
     */
    public boolean getDate_fromDirtyFlag();
    /**
     * 获取 [结束日期]
     */
    public void setDate_to(Timestamp date_to);
    
    /**
     * 设置 [结束日期]
     */
    public Timestamp getDate_to();

    /**
     * 获取 [结束日期]脏标记
     */
    public boolean getDate_toDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id(Integer department_id);
    
    /**
     * 设置 [部门]
     */
    public Integer getDepartment_id();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_idDirtyFlag();
    /**
     * 获取 [部门]
     */
    public void setDepartment_id_text(String department_id_text);
    
    /**
     * 设置 [部门]
     */
    public String getDepartment_id_text();

    /**
     * 获取 [部门]脏标记
     */
    public boolean getDepartment_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id(Integer employee_id);
    
    /**
     * 设置 [员工]
     */
    public Integer getEmployee_id();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_idDirtyFlag();
    /**
     * 获取 [员工]
     */
    public void setEmployee_id_text(String employee_id_text);
    
    /**
     * 设置 [员工]
     */
    public String getEmployee_id_text();

    /**
     * 获取 [员工]脏标记
     */
    public boolean getEmployee_id_textDirtyFlag();
    /**
     * 获取 [休假类型]
     */
    public void setHoliday_status_id(Integer holiday_status_id);
    
    /**
     * 设置 [休假类型]
     */
    public Integer getHoliday_status_id();

    /**
     * 获取 [休假类型]脏标记
     */
    public boolean getHoliday_status_idDirtyFlag();
    /**
     * 获取 [休假类型]
     */
    public void setHoliday_status_id_text(String holiday_status_id_text);
    
    /**
     * 设置 [休假类型]
     */
    public String getHoliday_status_id_text();

    /**
     * 获取 [休假类型]脏标记
     */
    public boolean getHoliday_status_id_textDirtyFlag();
    /**
     * 获取 [分配模式]
     */
    public void setHoliday_type(String holiday_type);
    
    /**
     * 设置 [分配模式]
     */
    public String getHoliday_type();

    /**
     * 获取 [分配模式]脏标记
     */
    public boolean getHoliday_typeDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setName(String name);
    
    /**
     * 设置 [说明]
     */
    public String getName();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [天数]
     */
    public void setNumber_of_days(Double number_of_days);
    
    /**
     * 设置 [天数]
     */
    public Double getNumber_of_days();

    /**
     * 获取 [天数]脏标记
     */
    public boolean getNumber_of_daysDirtyFlag();
    /**
     * 获取 [反映在最近的工资单中]
     */
    public void setPayslip_status(String payslip_status);
    
    /**
     * 设置 [反映在最近的工资单中]
     */
    public String getPayslip_status();

    /**
     * 获取 [反映在最近的工资单中]脏标记
     */
    public boolean getPayslip_statusDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [申请类型]
     */
    public void setType(String type);
    
    /**
     * 设置 [申请类型]
     */
    public String getType();

    /**
     * 获取 [申请类型]脏标记
     */
    public boolean getTypeDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
