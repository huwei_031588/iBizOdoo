package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_industry;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_industrySearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_industryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_partner_industryOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_partner_industryClientModel;

/**
 * 实体[工业] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_partner_industryServiceImpl implements IRes_partner_industryService {

    @Autowired
    res_partner_industryOdooClient res_partner_industryOdooClient;


    @Override
    public boolean update(Res_partner_industry et) {
        res_partner_industryClientModel clientModel = convert2Model(et,null);
		res_partner_industryOdooClient.update(clientModel);
        Res_partner_industry rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_partner_industry> list){
    }

    @Override
    public boolean create(Res_partner_industry et) {
        res_partner_industryClientModel clientModel = convert2Model(et,null);
		res_partner_industryOdooClient.create(clientModel);
        Res_partner_industry rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_partner_industry> list){
    }

    @Override
    public boolean remove(Integer id) {
        res_partner_industryClientModel clientModel = new res_partner_industryClientModel();
        clientModel.setId(id);
		res_partner_industryOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Res_partner_industry get(Integer id) {
        res_partner_industryClientModel clientModel = new res_partner_industryClientModel();
        clientModel.setId(id);
		res_partner_industryOdooClient.get(clientModel);
        Res_partner_industry et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_partner_industry();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_partner_industry> searchDefault(Res_partner_industrySearchContext context) {
        List<Res_partner_industry> list = new ArrayList<Res_partner_industry>();
        Page<res_partner_industryClientModel> clientModelList = res_partner_industryOdooClient.search(context);
        for(res_partner_industryClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_partner_industry>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_partner_industryClientModel convert2Model(Res_partner_industry domain , res_partner_industryClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_partner_industryClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("full_namedirtyflag"))
                model.setFull_name(domain.getFullName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_partner_industry convert2Domain( res_partner_industryClientModel model ,Res_partner_industry domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_partner_industry();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getFull_nameDirtyFlag())
            domain.setFullName(model.getFull_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



