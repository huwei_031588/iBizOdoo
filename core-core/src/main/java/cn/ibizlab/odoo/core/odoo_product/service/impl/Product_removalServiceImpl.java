package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_removal;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_removalSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_removalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_removalOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_removalClientModel;

/**
 * 实体[下架策略] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_removalServiceImpl implements IProduct_removalService {

    @Autowired
    product_removalOdooClient product_removalOdooClient;


    @Override
    public boolean remove(Integer id) {
        product_removalClientModel clientModel = new product_removalClientModel();
        clientModel.setId(id);
		product_removalOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Product_removal et) {
        product_removalClientModel clientModel = convert2Model(et,null);
		product_removalOdooClient.update(clientModel);
        Product_removal rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_removal> list){
    }

    @Override
    public boolean create(Product_removal et) {
        product_removalClientModel clientModel = convert2Model(et,null);
		product_removalOdooClient.create(clientModel);
        Product_removal rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_removal> list){
    }

    @Override
    public Product_removal get(Integer id) {
        product_removalClientModel clientModel = new product_removalClientModel();
        clientModel.setId(id);
		product_removalOdooClient.get(clientModel);
        Product_removal et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_removal();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_removal> searchDefault(Product_removalSearchContext context) {
        List<Product_removal> list = new ArrayList<Product_removal>();
        Page<product_removalClientModel> clientModelList = product_removalOdooClient.search(context);
        for(product_removalClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_removal>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_removalClientModel convert2Model(Product_removal domain , product_removalClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_removalClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("methoddirtyflag"))
                model.setMethod(domain.getMethod());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_removal convert2Domain( product_removalClientModel model ,Product_removal domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_removal();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMethodDirtyFlag())
            domain.setMethod(model.getMethod());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



