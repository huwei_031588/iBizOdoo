package cn.ibizlab.odoo.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_purchase.client.purchase_orderOdooClient;
import cn.ibizlab.odoo.core.odoo_purchase.clientmodel.purchase_orderClientModel;

/**
 * 实体[采购订单] 服务对象接口实现
 */
@Slf4j
@Service
public class Purchase_orderServiceImpl implements IPurchase_orderService {

    @Autowired
    purchase_orderOdooClient purchase_orderOdooClient;


    @Override
    public Purchase_order get(Integer id) {
        purchase_orderClientModel clientModel = new purchase_orderClientModel();
        clientModel.setId(id);
		purchase_orderOdooClient.get(clientModel);
        Purchase_order et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Purchase_order();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        purchase_orderClientModel clientModel = new purchase_orderClientModel();
        clientModel.setId(id);
		purchase_orderOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Purchase_order et) {
        purchase_orderClientModel clientModel = convert2Model(et,null);
		purchase_orderOdooClient.update(clientModel);
        Purchase_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Purchase_order> list){
    }

    @Override
    public boolean create(Purchase_order et) {
        purchase_orderClientModel clientModel = convert2Model(et,null);
		purchase_orderOdooClient.create(clientModel);
        Purchase_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Purchase_order> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Purchase_order> searchDefault(Purchase_orderSearchContext context) {
        List<Purchase_order> list = new ArrayList<Purchase_order>();
        Page<purchase_orderClientModel> clientModelList = purchase_orderOdooClient.search(context);
        for(purchase_orderClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Purchase_order>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public purchase_orderClientModel convert2Model(Purchase_order domain , purchase_orderClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new purchase_orderClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("is_shippeddirtyflag"))
                model.setIs_shipped(domain.getIsShipped());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("access_tokendirtyflag"))
                model.setAccess_token(domain.getAccessToken());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("order_linedirtyflag"))
                model.setOrder_line(domain.getOrderLine());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("notesdirtyflag"))
                model.setNotes(domain.getNotes());
            if((Boolean) domain.getExtensionparams().get("amount_totaldirtyflag"))
                model.setAmount_total(domain.getAmountTotal());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("amount_taxdirtyflag"))
                model.setAmount_tax(domain.getAmountTax());
            if((Boolean) domain.getExtensionparams().get("access_urldirtyflag"))
                model.setAccess_url(domain.getAccessUrl());
            if((Boolean) domain.getExtensionparams().get("invoice_countdirtyflag"))
                model.setInvoice_count(domain.getInvoiceCount());
            if((Boolean) domain.getExtensionparams().get("amount_untaxeddirtyflag"))
                model.setAmount_untaxed(domain.getAmountUntaxed());
            if((Boolean) domain.getExtensionparams().get("picking_idsdirtyflag"))
                model.setPicking_ids(domain.getPickingIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("access_warningdirtyflag"))
                model.setAccess_warning(domain.getAccessWarning());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("picking_countdirtyflag"))
                model.setPicking_count(domain.getPickingCount());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("invoice_idsdirtyflag"))
                model.setInvoice_ids(domain.getInvoiceIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("date_orderdirtyflag"))
                model.setDate_order(domain.getDateOrder());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("default_location_dest_id_usagedirtyflag"))
                model.setDefault_location_dest_id_usage(domain.getDefaultLocationDestIdUsage());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("group_iddirtyflag"))
                model.setGroup_id(domain.getGroupId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("invoice_statusdirtyflag"))
                model.setInvoice_status(domain.getInvoiceStatus());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("date_approvedirtyflag"))
                model.setDate_approve(domain.getDateApprove());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("partner_refdirtyflag"))
                model.setPartner_ref(domain.getPartnerRef());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("date_planneddirtyflag"))
                model.setDate_planned(domain.getDatePlanned());
            if((Boolean) domain.getExtensionparams().get("origindirtyflag"))
                model.setOrigin(domain.getOrigin());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("fiscal_position_id_textdirtyflag"))
                model.setFiscal_position_id_text(domain.getFiscalPositionIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("dest_address_id_textdirtyflag"))
                model.setDest_address_id_text(domain.getDestAddressIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("payment_term_id_textdirtyflag"))
                model.setPayment_term_id_text(domain.getPaymentTermIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("picking_type_id_textdirtyflag"))
                model.setPicking_type_id_text(domain.getPickingTypeIdText());
            if((Boolean) domain.getExtensionparams().get("incoterm_id_textdirtyflag"))
                model.setIncoterm_id_text(domain.getIncotermIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("dest_address_iddirtyflag"))
                model.setDest_address_id(domain.getDestAddressId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("payment_term_iddirtyflag"))
                model.setPayment_term_id(domain.getPaymentTermId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("incoterm_iddirtyflag"))
                model.setIncoterm_id(domain.getIncotermId());
            if((Boolean) domain.getExtensionparams().get("fiscal_position_iddirtyflag"))
                model.setFiscal_position_id(domain.getFiscalPositionId());
            if((Boolean) domain.getExtensionparams().get("picking_type_iddirtyflag"))
                model.setPicking_type_id(domain.getPickingTypeId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Purchase_order convert2Domain( purchase_orderClientModel model ,Purchase_order domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Purchase_order();
        }

        if(model.getIs_shippedDirtyFlag())
            domain.setIsShipped(model.getIs_shipped());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAccess_tokenDirtyFlag())
            domain.setAccessToken(model.getAccess_token());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getOrder_lineDirtyFlag())
            domain.setOrderLine(model.getOrder_line());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getNotesDirtyFlag())
            domain.setNotes(model.getNotes());
        if(model.getAmount_totalDirtyFlag())
            domain.setAmountTotal(model.getAmount_total());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getAmount_taxDirtyFlag())
            domain.setAmountTax(model.getAmount_tax());
        if(model.getAccess_urlDirtyFlag())
            domain.setAccessUrl(model.getAccess_url());
        if(model.getInvoice_countDirtyFlag())
            domain.setInvoiceCount(model.getInvoice_count());
        if(model.getAmount_untaxedDirtyFlag())
            domain.setAmountUntaxed(model.getAmount_untaxed());
        if(model.getPicking_idsDirtyFlag())
            domain.setPickingIds(model.getPicking_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getAccess_warningDirtyFlag())
            domain.setAccessWarning(model.getAccess_warning());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getPicking_countDirtyFlag())
            domain.setPickingCount(model.getPicking_count());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getInvoice_idsDirtyFlag())
            domain.setInvoiceIds(model.getInvoice_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getDate_orderDirtyFlag())
            domain.setDateOrder(model.getDate_order());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getDefault_location_dest_id_usageDirtyFlag())
            domain.setDefaultLocationDestIdUsage(model.getDefault_location_dest_id_usage());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getGroup_idDirtyFlag())
            domain.setGroupId(model.getGroup_id());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getInvoice_statusDirtyFlag())
            domain.setInvoiceStatus(model.getInvoice_status());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getDate_approveDirtyFlag())
            domain.setDateApprove(model.getDate_approve());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getPartner_refDirtyFlag())
            domain.setPartnerRef(model.getPartner_ref());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getDate_plannedDirtyFlag())
            domain.setDatePlanned(model.getDate_planned());
        if(model.getOriginDirtyFlag())
            domain.setOrigin(model.getOrigin());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getFiscal_position_id_textDirtyFlag())
            domain.setFiscalPositionIdText(model.getFiscal_position_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getDest_address_id_textDirtyFlag())
            domain.setDestAddressIdText(model.getDest_address_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPayment_term_id_textDirtyFlag())
            domain.setPaymentTermIdText(model.getPayment_term_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getPicking_type_id_textDirtyFlag())
            domain.setPickingTypeIdText(model.getPicking_type_id_text());
        if(model.getIncoterm_id_textDirtyFlag())
            domain.setIncotermIdText(model.getIncoterm_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getDest_address_idDirtyFlag())
            domain.setDestAddressId(model.getDest_address_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getPayment_term_idDirtyFlag())
            domain.setPaymentTermId(model.getPayment_term_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getIncoterm_idDirtyFlag())
            domain.setIncotermId(model.getIncoterm_id());
        if(model.getFiscal_position_idDirtyFlag())
            domain.setFiscalPositionId(model.getFiscal_position_id());
        if(model.getPicking_type_idDirtyFlag())
            domain.setPickingTypeId(model.getPicking_type_id());
        return domain ;
    }

}

    



