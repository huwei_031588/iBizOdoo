package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;

/**
 * 实体 [车辆相关的费用] 存储模型
 */
public interface Fleet_vehicle_cost{

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 总价
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [总价]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 里程表数值
     */
    Double getOdometer();

    void setOdometer(Double odometer);

    /**
     * 获取 [里程表数值]脏标记
     */
    boolean getOdometerDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 费用所属类别
     */
    String getCost_type();

    void setCost_type(String cost_type);

    /**
     * 获取 [费用所属类别]脏标记
     */
    boolean getCost_typeDirtyFlag();

    /**
     * 成本说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [成本说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 自动生成
     */
    String getAuto_generated();

    void setAuto_generated(String auto_generated);

    /**
     * 获取 [自动生成]脏标记
     */
    boolean getAuto_generatedDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 包括服务
     */
    String getCost_ids();

    void setCost_ids(String cost_ids);

    /**
     * 获取 [包括服务]脏标记
     */
    boolean getCost_idsDirtyFlag();

    /**
     * 合同
     */
    String getContract_id_text();

    void setContract_id_text(String contract_id_text);

    /**
     * 获取 [合同]脏标记
     */
    boolean getContract_id_textDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 上级
     */
    String getParent_id_text();

    void setParent_id_text(String parent_id_text);

    /**
     * 获取 [上级]脏标记
     */
    boolean getParent_id_textDirtyFlag();

    /**
     * 类型
     */
    String getCost_subtype_id_text();

    void setCost_subtype_id_text(String cost_subtype_id_text);

    /**
     * 获取 [类型]脏标记
     */
    boolean getCost_subtype_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 单位
     */
    String getOdometer_unit();

    void setOdometer_unit(String odometer_unit);

    /**
     * 获取 [单位]脏标记
     */
    boolean getOdometer_unitDirtyFlag();

    /**
     * 里程表
     */
    String getOdometer_id_text();

    void setOdometer_id_text(String odometer_id_text);

    /**
     * 获取 [里程表]脏标记
     */
    boolean getOdometer_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 类型
     */
    Integer getCost_subtype_id();

    void setCost_subtype_id(Integer cost_subtype_id);

    /**
     * 获取 [类型]脏标记
     */
    boolean getCost_subtype_idDirtyFlag();

    /**
     * 合同
     */
    Integer getContract_id();

    void setContract_id(Integer contract_id);

    /**
     * 获取 [合同]脏标记
     */
    boolean getContract_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 上级
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 车辆
     */
    Integer getVehicle_id();

    void setVehicle_id(Integer vehicle_id);

    /**
     * 获取 [车辆]脏标记
     */
    boolean getVehicle_idDirtyFlag();

    /**
     * 里程表
     */
    Integer getOdometer_id();

    void setOdometer_id(Integer odometer_id);

    /**
     * 获取 [里程表]脏标记
     */
    boolean getOdometer_idDirtyFlag();

}
