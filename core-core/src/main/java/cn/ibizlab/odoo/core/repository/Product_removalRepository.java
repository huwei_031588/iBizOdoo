package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_removal;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_removalSearchContext;

/**
 * 实体 [下架策略] 存储对象
 */
public interface Product_removalRepository extends Repository<Product_removal> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_removal> searchDefault(Product_removalSearchContext context);

    Product_removal convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_removal domain , Product_removal po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_removal convert2Domain( Product_removal po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_removal domain) ;

}
