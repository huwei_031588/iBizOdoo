package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_partner;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_partner.Res_partnerPartner_shareDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_partner
 * 属性：Partner_share
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_partnerPartner_shareDefaultValidator.class})
public @interface Res_partnerPartner_shareDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
