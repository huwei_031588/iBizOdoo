package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [凭证录入] 对象
 */
@Data
public class Account_move extends EntityClient implements Serializable {

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 内部备注
     */
    @JSONField(name = "narration")
    @JsonProperty("narration")
    private String narration;

    /**
     * 匹配百分比
     */
    @DEField(name = "matched_percentage")
    @JSONField(name = "matched_percentage")
    @JsonProperty("matched_percentage")
    private Double matchedPercentage;

    /**
     * 税类别域
     */
    @JSONField(name = "tax_type_domain")
    @JsonProperty("tax_type_domain")
    private String taxTypeDomain;

    /**
     * 号码
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 参考
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;

    /**
     * 科目
     */
    @JSONField(name = "dummy_account_id")
    @JsonProperty("dummy_account_id")
    private Integer dummyAccountId;

    /**
     * 自动撤销
     */
    @DEField(name = "auto_reverse")
    @JSONField(name = "auto_reverse")
    @JsonProperty("auto_reverse")
    private String autoReverse;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 金额
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 撤销日期
     */
    @DEField(name = "reverse_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "reverse_date" , format="yyyy-MM-dd")
    @JsonProperty("reverse_date")
    private Timestamp reverseDate;

    /**
     * 日记账项目
     */
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    private String lineIds;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 业务伙伴
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 撤销分录
     */
    @JSONField(name = "reverse_entry_id_text")
    @JsonProperty("reverse_entry_id_text")
    private String reverseEntryIdText;

    /**
     * 库存移动
     */
    @JSONField(name = "stock_move_id_text")
    @JsonProperty("stock_move_id_text")
    private String stockMoveIdText;

    /**
     * 日记账
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 库存移动
     */
    @DEField(name = "stock_move_id")
    @JSONField(name = "stock_move_id")
    @JsonProperty("stock_move_id")
    private Integer stockMoveId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 税率现金收付制分录
     */
    @DEField(name = "tax_cash_basis_rec_id")
    @JSONField(name = "tax_cash_basis_rec_id")
    @JsonProperty("tax_cash_basis_rec_id")
    private Integer taxCashBasisRecId;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 撤销分录
     */
    @DEField(name = "reverse_entry_id")
    @JSONField(name = "reverse_entry_id")
    @JsonProperty("reverse_entry_id")
    private Integer reverseEntryId;


    /**
     * 
     */
    @JSONField(name = "odoojournal")
    @JsonProperty("odoojournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JSONField(name = "odooreverseentry")
    @JsonProperty("odooreverseentry")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_move odooReverseEntry;

    /**
     * 
     */
    @JSONField(name = "odootaxcashbasisrec")
    @JsonProperty("odootaxcashbasisrec")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile odooTaxCashBasisRec;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoostockmove")
    @JsonProperty("odoostockmove")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move odooStockMove;




    /**
     * 设置 [内部备注]
     */
    public void setNarration(String narration){
        this.narration = narration ;
        this.modify("narration",narration);
    }
    /**
     * 设置 [匹配百分比]
     */
    public void setMatchedPercentage(Double matchedPercentage){
        this.matchedPercentage = matchedPercentage ;
        this.modify("matched_percentage",matchedPercentage);
    }
    /**
     * 设置 [号码]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [参考]
     */
    public void setRef(String ref){
        this.ref = ref ;
        this.modify("ref",ref);
    }
    /**
     * 设置 [自动撤销]
     */
    public void setAutoReverse(String autoReverse){
        this.autoReverse = autoReverse ;
        this.modify("auto_reverse",autoReverse);
    }
    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }
    /**
     * 设置 [撤销日期]
     */
    public void setReverseDate(Timestamp reverseDate){
        this.reverseDate = reverseDate ;
        this.modify("reverse_date",reverseDate);
    }
    /**
     * 设置 [库存移动]
     */
    public void setStockMoveId(Integer stockMoveId){
        this.stockMoveId = stockMoveId ;
        this.modify("stock_move_id",stockMoveId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [日记账]
     */
    public void setJournalId(Integer journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }
    /**
     * 设置 [税率现金收付制分录]
     */
    public void setTaxCashBasisRecId(Integer taxCashBasisRecId){
        this.taxCashBasisRecId = taxCashBasisRecId ;
        this.modify("tax_cash_basis_rec_id",taxCashBasisRecId);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [撤销分录]
     */
    public void setReverseEntryId(Integer reverseEntryId){
        this.reverseEntryId = reverseEntryId ;
        this.modify("reverse_entry_id",reverseEntryId);
    }

}


