package cn.ibizlab.odoo.core.odoo_web_editor.valuerule.validator.web_editor_converter_test;

import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.odoo.util.helper.SpringContextHolder;
import cn.ibizlab.odoo.util.valuerule.SysValueRule;
import cn.ibizlab.odoo.util.valuerule.StringLengthValueRule;
import cn.ibizlab.odoo.util.SearchFieldFilter;
import cn.ibizlab.odoo.util.enums.SearchFieldType;
import cn.ibizlab.odoo.core.odoo_web_editor.valuerule.anno.web_editor_converter_test.Web_editor_converter_testCharDefault;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

/**
 * 值规则注解解析类
 * 实体：Web_editor_converter_test
 * 属性：Char
 * 值规则：Default
 */
@Slf4j
@Component
public class Web_editor_converter_testCharDefaultValidator implements ConstraintValidator<Web_editor_converter_testCharDefault, String> {
     String message;
    /**
     * 用来完成将注解中的内容初始化
    */
    @Override
    public void initialize(Web_editor_converter_testCharDefault constraintAnnotation) {
        this.message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean isValid = true;

        {   //组条件：默认组
            //组合条件操作：AND
            boolean groupValid = true;
            {   //字符长度（STRINGLENGTH）:默认字符串长度
                Integer minlength = null;
                Integer maxlength = 100;

                boolean isInRange = StringLengthValueRule.isValid(value, minlength, maxlength, false, true);
                groupValid = groupValid && isInRange;
            }

            isValid = isValid && groupValid;
        }

        if(!isValid){
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message)
                    .addConstraintViolation();
        }
        return isValid;
    }
}

