package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;

/**
 * 实体 [采购 & 账单] 存储模型
 */
public interface Purchase_bill_union{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 来源
     */
    String getReference();

    void setReference(String reference);

    /**
     * 获取 [来源]脏标记
     */
    boolean getReferenceDirtyFlag();

    /**
     * 参考
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [参考]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 采购订单
     */
    String getPurchase_order_id_text();

    void setPurchase_order_id_text(String purchase_order_id_text);

    /**
     * 获取 [采购订单]脏标记
     */
    boolean getPurchase_order_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 供应商
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 供应商账单
     */
    String getVendor_bill_id_text();

    void setVendor_bill_id_text(String vendor_bill_id_text);

    /**
     * 获取 [供应商账单]脏标记
     */
    boolean getVendor_bill_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 供应商账单
     */
    Integer getVendor_bill_id();

    void setVendor_bill_id(Integer vendor_bill_id);

    /**
     * 获取 [供应商账单]脏标记
     */
    boolean getVendor_bill_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 采购订单
     */
    Integer getPurchase_order_id();

    void setPurchase_order_id(Integer purchase_order_id);

    /**
     * 获取 [采购订单]脏标记
     */
    boolean getPurchase_order_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 供应商
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getPartner_idDirtyFlag();

}
