package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_product.filter.Product_categorySearchContext;

/**
 * 实体 [产品种类] 存储模型
 */
public interface Product_category{

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 库存计价科目
     */
    Integer getProperty_stock_valuation_account_id();

    void setProperty_stock_valuation_account_id(Integer property_stock_valuation_account_id);

    /**
     * 获取 [库存计价科目]脏标记
     */
    boolean getProperty_stock_valuation_account_idDirtyFlag();

    /**
     * 下级类别
     */
    String getChild_id();

    void setChild_id(String child_id);

    /**
     * 获取 [下级类别]脏标记
     */
    boolean getChild_idDirtyFlag();

    /**
     * # 产品
     */
    Integer getProduct_count();

    void setProduct_count(Integer product_count);

    /**
     * 获取 [# 产品]脏标记
     */
    boolean getProduct_countDirtyFlag();

    /**
     * 路线
     */
    String getRoute_ids();

    void setRoute_ids(String route_ids);

    /**
     * 获取 [路线]脏标记
     */
    boolean getRoute_idsDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 库存计价
     */
    String getProperty_valuation();

    void setProperty_valuation(String property_valuation);

    /**
     * 获取 [库存计价]脏标记
     */
    boolean getProperty_valuationDirtyFlag();

    /**
     * 父级路径
     */
    String getParent_path();

    void setParent_path(String parent_path);

    /**
     * 获取 [父级路径]脏标记
     */
    boolean getParent_pathDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 完整名称
     */
    String getComplete_name();

    void setComplete_name(String complete_name);

    /**
     * 获取 [完整名称]脏标记
     */
    boolean getComplete_nameDirtyFlag();

    /**
     * 价格差异科目
     */
    Integer getProperty_account_creditor_price_difference_categ();

    void setProperty_account_creditor_price_difference_categ(Integer property_account_creditor_price_difference_categ);

    /**
     * 获取 [价格差异科目]脏标记
     */
    boolean getProperty_account_creditor_price_difference_categDirtyFlag();

    /**
     * 库存出货科目
     */
    Integer getProperty_stock_account_output_categ_id();

    void setProperty_stock_account_output_categ_id(Integer property_stock_account_output_categ_id);

    /**
     * 获取 [库存出货科目]脏标记
     */
    boolean getProperty_stock_account_output_categ_idDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 路线合计
     */
    String getTotal_route_ids();

    void setTotal_route_ids(String total_route_ids);

    /**
     * 获取 [路线合计]脏标记
     */
    boolean getTotal_route_idsDirtyFlag();

    /**
     * 成本方法
     */
    String getProperty_cost_method();

    void setProperty_cost_method(String property_cost_method);

    /**
     * 获取 [成本方法]脏标记
     */
    boolean getProperty_cost_methodDirtyFlag();

    /**
     * 收入科目
     */
    Integer getProperty_account_income_categ_id();

    void setProperty_account_income_categ_id(Integer property_account_income_categ_id);

    /**
     * 获取 [收入科目]脏标记
     */
    boolean getProperty_account_income_categ_idDirtyFlag();

    /**
     * 费用科目
     */
    Integer getProperty_account_expense_categ_id();

    void setProperty_account_expense_categ_id(Integer property_account_expense_categ_id);

    /**
     * 获取 [费用科目]脏标记
     */
    boolean getProperty_account_expense_categ_idDirtyFlag();

    /**
     * 库存日记账
     */
    Integer getProperty_stock_journal();

    void setProperty_stock_journal(Integer property_stock_journal);

    /**
     * 获取 [库存日记账]脏标记
     */
    boolean getProperty_stock_journalDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 库存进货科目
     */
    Integer getProperty_stock_account_input_categ_id();

    void setProperty_stock_account_input_categ_id(Integer property_stock_account_input_categ_id);

    /**
     * 获取 [库存进货科目]脏标记
     */
    boolean getProperty_stock_account_input_categ_idDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 上级类别
     */
    String getParent_id_text();

    void setParent_id_text(String parent_id_text);

    /**
     * 获取 [上级类别]脏标记
     */
    boolean getParent_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 强制下架策略
     */
    String getRemoval_strategy_id_text();

    void setRemoval_strategy_id_text(String removal_strategy_id_text);

    /**
     * 获取 [强制下架策略]脏标记
     */
    boolean getRemoval_strategy_id_textDirtyFlag();

    /**
     * 上级类别
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级类别]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 强制下架策略
     */
    Integer getRemoval_strategy_id();

    void setRemoval_strategy_id(Integer removal_strategy_id);

    /**
     * 获取 [强制下架策略]脏标记
     */
    boolean getRemoval_strategy_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
