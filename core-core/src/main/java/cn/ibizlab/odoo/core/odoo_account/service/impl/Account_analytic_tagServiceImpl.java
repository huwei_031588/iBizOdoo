package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_tag;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_tagSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_tagService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_analytic_tagOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_analytic_tagClientModel;

/**
 * 实体[分析标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_analytic_tagServiceImpl implements IAccount_analytic_tagService {

    @Autowired
    account_analytic_tagOdooClient account_analytic_tagOdooClient;


    @Override
    public Account_analytic_tag get(Integer id) {
        account_analytic_tagClientModel clientModel = new account_analytic_tagClientModel();
        clientModel.setId(id);
		account_analytic_tagOdooClient.get(clientModel);
        Account_analytic_tag et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_analytic_tag();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_analytic_tagClientModel clientModel = new account_analytic_tagClientModel();
        clientModel.setId(id);
		account_analytic_tagOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_analytic_tag et) {
        account_analytic_tagClientModel clientModel = convert2Model(et,null);
		account_analytic_tagOdooClient.create(clientModel);
        Account_analytic_tag rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_analytic_tag> list){
    }

    @Override
    public boolean update(Account_analytic_tag et) {
        account_analytic_tagClientModel clientModel = convert2Model(et,null);
		account_analytic_tagOdooClient.update(clientModel);
        Account_analytic_tag rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_analytic_tag> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_analytic_tag> searchDefault(Account_analytic_tagSearchContext context) {
        List<Account_analytic_tag> list = new ArrayList<Account_analytic_tag>();
        Page<account_analytic_tagClientModel> clientModelList = account_analytic_tagOdooClient.search(context);
        for(account_analytic_tagClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_analytic_tag>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_analytic_tagClientModel convert2Model(Account_analytic_tag domain , account_analytic_tagClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_analytic_tagClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("analytic_distribution_idsdirtyflag"))
                model.setAnalytic_distribution_ids(domain.getAnalyticDistributionIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("active_analytic_distributiondirtyflag"))
                model.setActive_analytic_distribution(domain.getActiveAnalyticDistribution());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_analytic_tag convert2Domain( account_analytic_tagClientModel model ,Account_analytic_tag domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_analytic_tag();
        }

        if(model.getAnalytic_distribution_idsDirtyFlag())
            domain.setAnalyticDistributionIds(model.getAnalytic_distribution_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getActive_analytic_distributionDirtyFlag())
            domain.setActiveAnalyticDistribution(model.getActive_analytic_distribution());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        return domain ;
    }

}

    



