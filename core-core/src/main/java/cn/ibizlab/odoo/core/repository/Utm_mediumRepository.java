package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Utm_medium;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mediumSearchContext;

/**
 * 实体 [UTM媒体] 存储对象
 */
public interface Utm_mediumRepository extends Repository<Utm_medium> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Utm_medium> searchDefault(Utm_mediumSearchContext context);

    Utm_medium convert2PO(cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium domain , Utm_medium po) ;

    cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium convert2Domain( Utm_medium po ,cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium domain) ;

}
