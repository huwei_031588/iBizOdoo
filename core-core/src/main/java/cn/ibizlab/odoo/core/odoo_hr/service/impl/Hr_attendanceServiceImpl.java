package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_attendanceSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_attendanceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_attendanceOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_attendanceClientModel;

/**
 * 实体[出勤] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_attendanceServiceImpl implements IHr_attendanceService {

    @Autowired
    hr_attendanceOdooClient hr_attendanceOdooClient;


    @Override
    public boolean remove(Integer id) {
        hr_attendanceClientModel clientModel = new hr_attendanceClientModel();
        clientModel.setId(id);
		hr_attendanceOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Hr_attendance et) {
        hr_attendanceClientModel clientModel = convert2Model(et,null);
		hr_attendanceOdooClient.create(clientModel);
        Hr_attendance rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_attendance> list){
    }

    @Override
    public boolean update(Hr_attendance et) {
        hr_attendanceClientModel clientModel = convert2Model(et,null);
		hr_attendanceOdooClient.update(clientModel);
        Hr_attendance rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_attendance> list){
    }

    @Override
    public Hr_attendance get(Integer id) {
        hr_attendanceClientModel clientModel = new hr_attendanceClientModel();
        clientModel.setId(id);
		hr_attendanceOdooClient.get(clientModel);
        Hr_attendance et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_attendance();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_attendance> searchDefault(Hr_attendanceSearchContext context) {
        List<Hr_attendance> list = new ArrayList<Hr_attendance>();
        Page<hr_attendanceClientModel> clientModelList = hr_attendanceOdooClient.search(context);
        for(hr_attendanceClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_attendance>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_attendanceClientModel convert2Model(Hr_attendance domain , hr_attendanceClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_attendanceClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("worked_hoursdirtyflag"))
                model.setWorked_hours(domain.getWorkedHours());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("check_indirtyflag"))
                model.setCheck_in(domain.getCheckIn());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("check_outdirtyflag"))
                model.setCheck_out(domain.getCheckOut());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("department_iddirtyflag"))
                model.setDepartment_id(domain.getDepartmentId());
            if((Boolean) domain.getExtensionparams().get("employee_id_textdirtyflag"))
                model.setEmployee_id_text(domain.getEmployeeIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("employee_iddirtyflag"))
                model.setEmployee_id(domain.getEmployeeId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_attendance convert2Domain( hr_attendanceClientModel model ,Hr_attendance domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_attendance();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWorked_hoursDirtyFlag())
            domain.setWorkedHours(model.getWorked_hours());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCheck_inDirtyFlag())
            domain.setCheckIn(model.getCheck_in());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCheck_outDirtyFlag())
            domain.setCheckOut(model.getCheck_out());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getDepartment_idDirtyFlag())
            domain.setDepartmentId(model.getDepartment_id());
        if(model.getEmployee_id_textDirtyFlag())
            domain.setEmployeeIdText(model.getEmployee_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getEmployee_idDirtyFlag())
            domain.setEmployeeId(model.getEmployee_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



