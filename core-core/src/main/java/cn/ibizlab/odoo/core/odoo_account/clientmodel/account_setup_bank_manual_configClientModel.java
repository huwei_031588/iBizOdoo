package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_setup_bank_manual_config] 对象
 */
public class account_setup_bank_manual_configClientModel implements Serializable{

    /**
     * 账户持有人名称
     */
    public String acc_holder_name;

    @JsonIgnore
    public boolean acc_holder_nameDirtyFlag;
    
    /**
     * 账户号码
     */
    public String acc_number;

    @JsonIgnore
    public boolean acc_numberDirtyFlag;
    
    /**
     * 类型
     */
    public String acc_type;

    @JsonIgnore
    public boolean acc_typeDirtyFlag;
    
    /**
     * 银行识别代码
     */
    public String bank_bic;

    @JsonIgnore
    public boolean bank_bicDirtyFlag;
    
    /**
     * 银行
     */
    public Integer bank_id;

    @JsonIgnore
    public boolean bank_idDirtyFlag;
    
    /**
     * 名称
     */
    public String bank_name;

    @JsonIgnore
    public boolean bank_nameDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建或链接选项
     */
    public String create_or_link_option;

    @JsonIgnore
    public boolean create_or_link_optionDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 会计日记账
     */
    public String journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 日记账
     */
    public Integer linked_journal_id;

    @JsonIgnore
    public boolean linked_journal_idDirtyFlag;
    
    /**
     * 代码
     */
    public String new_journal_code;

    @JsonIgnore
    public boolean new_journal_codeDirtyFlag;
    
    /**
     * 新科目名
     */
    public String new_journal_name;

    @JsonIgnore
    public boolean new_journal_nameDirtyFlag;
    
    /**
     * 账户持有人
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * ‎有所有必需的参数‎
     */
    public String qr_code_valid;

    @JsonIgnore
    public boolean qr_code_validDirtyFlag;
    
    /**
     * 科目类型
     */
    public String related_acc_type;

    @JsonIgnore
    public boolean related_acc_typeDirtyFlag;
    
    /**
     * 业务伙伴银行账户
     */
    public Integer res_partner_bank_id;

    @JsonIgnore
    public boolean res_partner_bank_idDirtyFlag;
    
    /**
     * 核对银行账号
     */
    public String sanitized_acc_number;

    @JsonIgnore
    public boolean sanitized_acc_numberDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [账户持有人名称]
     */
    @JsonProperty("acc_holder_name")
    public String getAcc_holder_name(){
        return this.acc_holder_name ;
    }

    /**
     * 设置 [账户持有人名称]
     */
    @JsonProperty("acc_holder_name")
    public void setAcc_holder_name(String  acc_holder_name){
        this.acc_holder_name = acc_holder_name ;
        this.acc_holder_nameDirtyFlag = true ;
    }

     /**
     * 获取 [账户持有人名称]脏标记
     */
    @JsonIgnore
    public boolean getAcc_holder_nameDirtyFlag(){
        return this.acc_holder_nameDirtyFlag ;
    }   

    /**
     * 获取 [账户号码]
     */
    @JsonProperty("acc_number")
    public String getAcc_number(){
        return this.acc_number ;
    }

    /**
     * 设置 [账户号码]
     */
    @JsonProperty("acc_number")
    public void setAcc_number(String  acc_number){
        this.acc_number = acc_number ;
        this.acc_numberDirtyFlag = true ;
    }

     /**
     * 获取 [账户号码]脏标记
     */
    @JsonIgnore
    public boolean getAcc_numberDirtyFlag(){
        return this.acc_numberDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("acc_type")
    public String getAcc_type(){
        return this.acc_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("acc_type")
    public void setAcc_type(String  acc_type){
        this.acc_type = acc_type ;
        this.acc_typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getAcc_typeDirtyFlag(){
        return this.acc_typeDirtyFlag ;
    }   

    /**
     * 获取 [银行识别代码]
     */
    @JsonProperty("bank_bic")
    public String getBank_bic(){
        return this.bank_bic ;
    }

    /**
     * 设置 [银行识别代码]
     */
    @JsonProperty("bank_bic")
    public void setBank_bic(String  bank_bic){
        this.bank_bic = bank_bic ;
        this.bank_bicDirtyFlag = true ;
    }

     /**
     * 获取 [银行识别代码]脏标记
     */
    @JsonIgnore
    public boolean getBank_bicDirtyFlag(){
        return this.bank_bicDirtyFlag ;
    }   

    /**
     * 获取 [银行]
     */
    @JsonProperty("bank_id")
    public Integer getBank_id(){
        return this.bank_id ;
    }

    /**
     * 设置 [银行]
     */
    @JsonProperty("bank_id")
    public void setBank_id(Integer  bank_id){
        this.bank_id = bank_id ;
        this.bank_idDirtyFlag = true ;
    }

     /**
     * 获取 [银行]脏标记
     */
    @JsonIgnore
    public boolean getBank_idDirtyFlag(){
        return this.bank_idDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("bank_name")
    public String getBank_name(){
        return this.bank_name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("bank_name")
    public void setBank_name(String  bank_name){
        this.bank_name = bank_name ;
        this.bank_nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getBank_nameDirtyFlag(){
        return this.bank_nameDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建或链接选项]
     */
    @JsonProperty("create_or_link_option")
    public String getCreate_or_link_option(){
        return this.create_or_link_option ;
    }

    /**
     * 设置 [创建或链接选项]
     */
    @JsonProperty("create_or_link_option")
    public void setCreate_or_link_option(String  create_or_link_option){
        this.create_or_link_option = create_or_link_option ;
        this.create_or_link_optionDirtyFlag = true ;
    }

     /**
     * 获取 [创建或链接选项]脏标记
     */
    @JsonIgnore
    public boolean getCreate_or_link_optionDirtyFlag(){
        return this.create_or_link_optionDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [会计日记账]
     */
    @JsonProperty("journal_id")
    public String getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [会计日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(String  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [会计日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("linked_journal_id")
    public Integer getLinked_journal_id(){
        return this.linked_journal_id ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("linked_journal_id")
    public void setLinked_journal_id(Integer  linked_journal_id){
        this.linked_journal_id = linked_journal_id ;
        this.linked_journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getLinked_journal_idDirtyFlag(){
        return this.linked_journal_idDirtyFlag ;
    }   

    /**
     * 获取 [代码]
     */
    @JsonProperty("new_journal_code")
    public String getNew_journal_code(){
        return this.new_journal_code ;
    }

    /**
     * 设置 [代码]
     */
    @JsonProperty("new_journal_code")
    public void setNew_journal_code(String  new_journal_code){
        this.new_journal_code = new_journal_code ;
        this.new_journal_codeDirtyFlag = true ;
    }

     /**
     * 获取 [代码]脏标记
     */
    @JsonIgnore
    public boolean getNew_journal_codeDirtyFlag(){
        return this.new_journal_codeDirtyFlag ;
    }   

    /**
     * 获取 [新科目名]
     */
    @JsonProperty("new_journal_name")
    public String getNew_journal_name(){
        return this.new_journal_name ;
    }

    /**
     * 设置 [新科目名]
     */
    @JsonProperty("new_journal_name")
    public void setNew_journal_name(String  new_journal_name){
        this.new_journal_name = new_journal_name ;
        this.new_journal_nameDirtyFlag = true ;
    }

     /**
     * 获取 [新科目名]脏标记
     */
    @JsonIgnore
    public boolean getNew_journal_nameDirtyFlag(){
        return this.new_journal_nameDirtyFlag ;
    }   

    /**
     * 获取 [账户持有人]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [账户持有人]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [账户持有人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [‎有所有必需的参数‎]
     */
    @JsonProperty("qr_code_valid")
    public String getQr_code_valid(){
        return this.qr_code_valid ;
    }

    /**
     * 设置 [‎有所有必需的参数‎]
     */
    @JsonProperty("qr_code_valid")
    public void setQr_code_valid(String  qr_code_valid){
        this.qr_code_valid = qr_code_valid ;
        this.qr_code_validDirtyFlag = true ;
    }

     /**
     * 获取 [‎有所有必需的参数‎]脏标记
     */
    @JsonIgnore
    public boolean getQr_code_validDirtyFlag(){
        return this.qr_code_validDirtyFlag ;
    }   

    /**
     * 获取 [科目类型]
     */
    @JsonProperty("related_acc_type")
    public String getRelated_acc_type(){
        return this.related_acc_type ;
    }

    /**
     * 设置 [科目类型]
     */
    @JsonProperty("related_acc_type")
    public void setRelated_acc_type(String  related_acc_type){
        this.related_acc_type = related_acc_type ;
        this.related_acc_typeDirtyFlag = true ;
    }

     /**
     * 获取 [科目类型]脏标记
     */
    @JsonIgnore
    public boolean getRelated_acc_typeDirtyFlag(){
        return this.related_acc_typeDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴银行账户]
     */
    @JsonProperty("res_partner_bank_id")
    public Integer getRes_partner_bank_id(){
        return this.res_partner_bank_id ;
    }

    /**
     * 设置 [业务伙伴银行账户]
     */
    @JsonProperty("res_partner_bank_id")
    public void setRes_partner_bank_id(Integer  res_partner_bank_id){
        this.res_partner_bank_id = res_partner_bank_id ;
        this.res_partner_bank_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴银行账户]脏标记
     */
    @JsonIgnore
    public boolean getRes_partner_bank_idDirtyFlag(){
        return this.res_partner_bank_idDirtyFlag ;
    }   

    /**
     * 获取 [核对银行账号]
     */
    @JsonProperty("sanitized_acc_number")
    public String getSanitized_acc_number(){
        return this.sanitized_acc_number ;
    }

    /**
     * 设置 [核对银行账号]
     */
    @JsonProperty("sanitized_acc_number")
    public void setSanitized_acc_number(String  sanitized_acc_number){
        this.sanitized_acc_number = sanitized_acc_number ;
        this.sanitized_acc_numberDirtyFlag = true ;
    }

     /**
     * 获取 [核对银行账号]脏标记
     */
    @JsonIgnore
    public boolean getSanitized_acc_numberDirtyFlag(){
        return this.sanitized_acc_numberDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("acc_holder_name") instanceof Boolean)&& map.get("acc_holder_name")!=null){
			this.setAcc_holder_name((String)map.get("acc_holder_name"));
		}
		if(!(map.get("acc_number") instanceof Boolean)&& map.get("acc_number")!=null){
			this.setAcc_number((String)map.get("acc_number"));
		}
		if(!(map.get("acc_type") instanceof Boolean)&& map.get("acc_type")!=null){
			this.setAcc_type((String)map.get("acc_type"));
		}
		if(!(map.get("bank_bic") instanceof Boolean)&& map.get("bank_bic")!=null){
			this.setBank_bic((String)map.get("bank_bic"));
		}
		if(!(map.get("bank_id") instanceof Boolean)&& map.get("bank_id")!=null){
			Object[] objs = (Object[])map.get("bank_id");
			if(objs.length > 0){
				this.setBank_id((Integer)objs[0]);
			}
		}
		if(!(map.get("bank_name") instanceof Boolean)&& map.get("bank_name")!=null){
			this.setBank_name((String)map.get("bank_name"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_or_link_option") instanceof Boolean)&& map.get("create_or_link_option")!=null){
			this.setCreate_or_link_option((String)map.get("create_or_link_option"));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 0){
				Integer[] journal_id = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setJournal_id(Arrays.toString(journal_id).replace(" ",""));
			}
		}
		if(!(map.get("linked_journal_id") instanceof Boolean)&& map.get("linked_journal_id")!=null){
			Object[] objs = (Object[])map.get("linked_journal_id");
			if(objs.length > 0){
				this.setLinked_journal_id((Integer)objs[0]);
			}
		}
		if(!(map.get("new_journal_code") instanceof Boolean)&& map.get("new_journal_code")!=null){
			this.setNew_journal_code((String)map.get("new_journal_code"));
		}
		if(!(map.get("new_journal_name") instanceof Boolean)&& map.get("new_journal_name")!=null){
			this.setNew_journal_name((String)map.get("new_journal_name"));
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(map.get("qr_code_valid") instanceof Boolean){
			this.setQr_code_valid(((Boolean)map.get("qr_code_valid"))? "true" : "false");
		}
		if(!(map.get("related_acc_type") instanceof Boolean)&& map.get("related_acc_type")!=null){
			this.setRelated_acc_type((String)map.get("related_acc_type"));
		}
		if(!(map.get("res_partner_bank_id") instanceof Boolean)&& map.get("res_partner_bank_id")!=null){
			Object[] objs = (Object[])map.get("res_partner_bank_id");
			if(objs.length > 0){
				this.setRes_partner_bank_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sanitized_acc_number") instanceof Boolean)&& map.get("sanitized_acc_number")!=null){
			this.setSanitized_acc_number((String)map.get("sanitized_acc_number"));
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAcc_holder_name()!=null&&this.getAcc_holder_nameDirtyFlag()){
			map.put("acc_holder_name",this.getAcc_holder_name());
		}else if(this.getAcc_holder_nameDirtyFlag()){
			map.put("acc_holder_name",false);
		}
		if(this.getAcc_number()!=null&&this.getAcc_numberDirtyFlag()){
			map.put("acc_number",this.getAcc_number());
		}else if(this.getAcc_numberDirtyFlag()){
			map.put("acc_number",false);
		}
		if(this.getAcc_type()!=null&&this.getAcc_typeDirtyFlag()){
			map.put("acc_type",this.getAcc_type());
		}else if(this.getAcc_typeDirtyFlag()){
			map.put("acc_type",false);
		}
		if(this.getBank_bic()!=null&&this.getBank_bicDirtyFlag()){
			map.put("bank_bic",this.getBank_bic());
		}else if(this.getBank_bicDirtyFlag()){
			map.put("bank_bic",false);
		}
		if(this.getBank_id()!=null&&this.getBank_idDirtyFlag()){
			map.put("bank_id",this.getBank_id());
		}else if(this.getBank_idDirtyFlag()){
			map.put("bank_id",false);
		}
		if(this.getBank_name()!=null&&this.getBank_nameDirtyFlag()){
			map.put("bank_name",this.getBank_name());
		}else if(this.getBank_nameDirtyFlag()){
			map.put("bank_name",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_or_link_option()!=null&&this.getCreate_or_link_optionDirtyFlag()){
			map.put("create_or_link_option",this.getCreate_or_link_option());
		}else if(this.getCreate_or_link_optionDirtyFlag()){
			map.put("create_or_link_option",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getJournal_id()!=null&&this.getJournal_idDirtyFlag()){
			map.put("journal_id",this.getJournal_id());
		}else if(this.getJournal_idDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getLinked_journal_id()!=null&&this.getLinked_journal_idDirtyFlag()){
			map.put("linked_journal_id",this.getLinked_journal_id());
		}else if(this.getLinked_journal_idDirtyFlag()){
			map.put("linked_journal_id",false);
		}
		if(this.getNew_journal_code()!=null&&this.getNew_journal_codeDirtyFlag()){
			map.put("new_journal_code",this.getNew_journal_code());
		}else if(this.getNew_journal_codeDirtyFlag()){
			map.put("new_journal_code",false);
		}
		if(this.getNew_journal_name()!=null&&this.getNew_journal_nameDirtyFlag()){
			map.put("new_journal_name",this.getNew_journal_name());
		}else if(this.getNew_journal_nameDirtyFlag()){
			map.put("new_journal_name",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getQr_code_valid()!=null&&this.getQr_code_validDirtyFlag()){
			map.put("qr_code_valid",Boolean.parseBoolean(this.getQr_code_valid()));		
		}		if(this.getRelated_acc_type()!=null&&this.getRelated_acc_typeDirtyFlag()){
			map.put("related_acc_type",this.getRelated_acc_type());
		}else if(this.getRelated_acc_typeDirtyFlag()){
			map.put("related_acc_type",false);
		}
		if(this.getRes_partner_bank_id()!=null&&this.getRes_partner_bank_idDirtyFlag()){
			map.put("res_partner_bank_id",this.getRes_partner_bank_id());
		}else if(this.getRes_partner_bank_idDirtyFlag()){
			map.put("res_partner_bank_id",false);
		}
		if(this.getSanitized_acc_number()!=null&&this.getSanitized_acc_numberDirtyFlag()){
			map.put("sanitized_acc_number",this.getSanitized_acc_number());
		}else if(this.getSanitized_acc_numberDirtyFlag()){
			map.put("sanitized_acc_number",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
