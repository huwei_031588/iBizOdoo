package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheet_register_payment_wizardSearchContext;

/**
 * 实体 [费用登记付款向导] 存储模型
 */
public interface Hr_expense_sheet_register_payment_wizard{

    /**
     * 付款金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [付款金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 备忘
     */
    String getCommunication();

    void setCommunication(String communication);

    /**
     * 获取 [备忘]脏标记
     */
    boolean getCommunicationDirtyFlag();

    /**
     * 隐藏付款方式
     */
    String getHide_payment_method();

    void setHide_payment_method(String hide_payment_method);

    /**
     * 获取 [隐藏付款方式]脏标记
     */
    boolean getHide_payment_methodDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 显示合作伙伴银行账户
     */
    String getShow_partner_bank_account();

    void setShow_partner_bank_account(String show_partner_bank_account);

    /**
     * 获取 [显示合作伙伴银行账户]脏标记
     */
    boolean getShow_partner_bank_accountDirtyFlag();

    /**
     * 付款日期
     */
    Timestamp getPayment_date();

    void setPayment_date(Timestamp payment_date);

    /**
     * 获取 [付款日期]脏标记
     */
    boolean getPayment_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 付款方法
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [付款方法]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 付款类型
     */
    String getPayment_method_id_text();

    void setPayment_method_id_text(String payment_method_id_text);

    /**
     * 获取 [付款类型]脏标记
     */
    boolean getPayment_method_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 付款类型
     */
    Integer getPayment_method_id();

    void setPayment_method_id(Integer payment_method_id);

    /**
     * 获取 [付款类型]脏标记
     */
    boolean getPayment_method_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 收款银行账号
     */
    Integer getPartner_bank_account_id();

    void setPartner_bank_account_id(Integer partner_bank_account_id);

    /**
     * 获取 [收款银行账号]脏标记
     */
    boolean getPartner_bank_account_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 付款方法
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [付款方法]脏标记
     */
    boolean getJournal_idDirtyFlag();

}
