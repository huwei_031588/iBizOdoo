package cn.ibizlab.odoo.core.odoo_web_editor.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_test_subSearchContext;
import cn.ibizlab.odoo.core.odoo_web_editor.service.IWeb_editor_converter_test_subService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_web_editor.client.web_editor_converter_test_subOdooClient;
import cn.ibizlab.odoo.core.odoo_web_editor.clientmodel.web_editor_converter_test_subClientModel;

/**
 * 实体[Web编辑器转换器子测试] 服务对象接口实现
 */
@Slf4j
@Service
public class Web_editor_converter_test_subServiceImpl implements IWeb_editor_converter_test_subService {

    @Autowired
    web_editor_converter_test_subOdooClient web_editor_converter_test_subOdooClient;


    @Override
    public boolean update(Web_editor_converter_test_sub et) {
        web_editor_converter_test_subClientModel clientModel = convert2Model(et,null);
		web_editor_converter_test_subOdooClient.update(clientModel);
        Web_editor_converter_test_sub rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Web_editor_converter_test_sub> list){
    }

    @Override
    public Web_editor_converter_test_sub get(Integer id) {
        web_editor_converter_test_subClientModel clientModel = new web_editor_converter_test_subClientModel();
        clientModel.setId(id);
		web_editor_converter_test_subOdooClient.get(clientModel);
        Web_editor_converter_test_sub et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Web_editor_converter_test_sub();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Web_editor_converter_test_sub et) {
        web_editor_converter_test_subClientModel clientModel = convert2Model(et,null);
		web_editor_converter_test_subOdooClient.create(clientModel);
        Web_editor_converter_test_sub rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Web_editor_converter_test_sub> list){
    }

    @Override
    public boolean remove(Integer id) {
        web_editor_converter_test_subClientModel clientModel = new web_editor_converter_test_subClientModel();
        clientModel.setId(id);
		web_editor_converter_test_subOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Web_editor_converter_test_sub> searchDefault(Web_editor_converter_test_subSearchContext context) {
        List<Web_editor_converter_test_sub> list = new ArrayList<Web_editor_converter_test_sub>();
        Page<web_editor_converter_test_subClientModel> clientModelList = web_editor_converter_test_subOdooClient.search(context);
        for(web_editor_converter_test_subClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Web_editor_converter_test_sub>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public web_editor_converter_test_subClientModel convert2Model(Web_editor_converter_test_sub domain , web_editor_converter_test_subClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new web_editor_converter_test_subClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Web_editor_converter_test_sub convert2Domain( web_editor_converter_test_subClientModel model ,Web_editor_converter_test_sub domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Web_editor_converter_test_sub();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



