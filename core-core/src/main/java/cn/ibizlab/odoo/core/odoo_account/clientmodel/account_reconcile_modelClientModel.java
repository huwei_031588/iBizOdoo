package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_reconcile_model] 对象
 */
public class account_reconcile_modelClientModel implements Serializable{

    /**
     * 科目
     */
    public Integer account_id;

    @JsonIgnore
    public boolean account_idDirtyFlag;
    
    /**
     * 科目
     */
    public String account_id_text;

    @JsonIgnore
    public boolean account_id_textDirtyFlag;
    
    /**
     * 核销金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 金额类型
     */
    public String amount_type;

    @JsonIgnore
    public boolean amount_typeDirtyFlag;
    
    /**
     * 分析账户
     */
    public Integer analytic_account_id;

    @JsonIgnore
    public boolean analytic_account_idDirtyFlag;
    
    /**
     * 分析账户
     */
    public String analytic_account_id_text;

    @JsonIgnore
    public boolean analytic_account_id_textDirtyFlag;
    
    /**
     * 分析标签
     */
    public String analytic_tag_ids;

    @JsonIgnore
    public boolean analytic_tag_idsDirtyFlag;
    
    /**
     * 自动验证
     */
    public String auto_reconcile;

    @JsonIgnore
    public boolean auto_reconcileDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 第二含税价‎
     */
    public String force_second_tax_included;

    @JsonIgnore
    public boolean force_second_tax_includedDirtyFlag;
    
    /**
     * 含税价
     */
    public String force_tax_included;

    @JsonIgnore
    public boolean force_tax_includedDirtyFlag;
    
    /**
     * 添加第二行
     */
    public String has_second_line;

    @JsonIgnore
    public boolean has_second_lineDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 第二含税价‎
     */
    public String is_second_tax_price_included;

    @JsonIgnore
    public boolean is_second_tax_price_includedDirtyFlag;
    
    /**
     * 含税价
     */
    public String is_tax_price_included;

    @JsonIgnore
    public boolean is_tax_price_includedDirtyFlag;
    
    /**
     * 日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 日记账
     */
    public String journal_id_text;

    @JsonIgnore
    public boolean journal_id_textDirtyFlag;
    
    /**
     * 日记账项目标签
     */
    public String label;

    @JsonIgnore
    public boolean labelDirtyFlag;
    
    /**
     * 金额
     */
    public String match_amount;

    @JsonIgnore
    public boolean match_amountDirtyFlag;
    
    /**
     * 参数最大金额
     */
    public Double match_amount_max;

    @JsonIgnore
    public boolean match_amount_maxDirtyFlag;
    
    /**
     * 参数最小金额
     */
    public Double match_amount_min;

    @JsonIgnore
    public boolean match_amount_minDirtyFlag;
    
    /**
     * 凭证类型
     */
    public String match_journal_ids;

    @JsonIgnore
    public boolean match_journal_idsDirtyFlag;
    
    /**
     * 标签
     */
    public String match_label;

    @JsonIgnore
    public boolean match_labelDirtyFlag;
    
    /**
     * 标签参数
     */
    public String match_label_param;

    @JsonIgnore
    public boolean match_label_paramDirtyFlag;
    
    /**
     * 数量性质
     */
    public String match_nature;

    @JsonIgnore
    public boolean match_natureDirtyFlag;
    
    /**
     * 已经匹配合作伙伴
     */
    public String match_partner;

    @JsonIgnore
    public boolean match_partnerDirtyFlag;
    
    /**
     * 限制合作伙伴类别为
     */
    public String match_partner_category_ids;

    @JsonIgnore
    public boolean match_partner_category_idsDirtyFlag;
    
    /**
     * 限制合作伙伴为
     */
    public String match_partner_ids;

    @JsonIgnore
    public boolean match_partner_idsDirtyFlag;
    
    /**
     * 同币种匹配
     */
    public String match_same_currency;

    @JsonIgnore
    public boolean match_same_currencyDirtyFlag;
    
    /**
     * 会计匹配
     */
    public String match_total_amount;

    @JsonIgnore
    public boolean match_total_amountDirtyFlag;
    
    /**
     * 会计匹配%
     */
    public Double match_total_amount_param;

    @JsonIgnore
    public boolean match_total_amount_paramDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 类型
     */
    public String rule_type;

    @JsonIgnore
    public boolean rule_typeDirtyFlag;
    
    /**
     * 第二科目
     */
    public Integer second_account_id;

    @JsonIgnore
    public boolean second_account_idDirtyFlag;
    
    /**
     * 第二科目
     */
    public String second_account_id_text;

    @JsonIgnore
    public boolean second_account_id_textDirtyFlag;
    
    /**
     * 第二核销金额
     */
    public Double second_amount;

    @JsonIgnore
    public boolean second_amountDirtyFlag;
    
    /**
     * 第二金额类型
     */
    public String second_amount_type;

    @JsonIgnore
    public boolean second_amount_typeDirtyFlag;
    
    /**
     * 第二分析帐户
     */
    public Integer second_analytic_account_id;

    @JsonIgnore
    public boolean second_analytic_account_idDirtyFlag;
    
    /**
     * 第二分析帐户
     */
    public String second_analytic_account_id_text;

    @JsonIgnore
    public boolean second_analytic_account_id_textDirtyFlag;
    
    /**
     * 第二分析标签
     */
    public String second_analytic_tag_ids;

    @JsonIgnore
    public boolean second_analytic_tag_idsDirtyFlag;
    
    /**
     * 第二个分录
     */
    public Integer second_journal_id;

    @JsonIgnore
    public boolean second_journal_idDirtyFlag;
    
    /**
     * 第二个分录
     */
    public String second_journal_id_text;

    @JsonIgnore
    public boolean second_journal_id_textDirtyFlag;
    
    /**
     * 第二个分录项目标签
     */
    public String second_label;

    @JsonIgnore
    public boolean second_labelDirtyFlag;
    
    /**
     * 第二税率类别
     */
    public String second_tax_amount_type;

    @JsonIgnore
    public boolean second_tax_amount_typeDirtyFlag;
    
    /**
     * 第二个税
     */
    public Integer second_tax_id;

    @JsonIgnore
    public boolean second_tax_idDirtyFlag;
    
    /**
     * 第二个税
     */
    public String second_tax_id_text;

    @JsonIgnore
    public boolean second_tax_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 税率类别
     */
    public String tax_amount_type;

    @JsonIgnore
    public boolean tax_amount_typeDirtyFlag;
    
    /**
     * 税率
     */
    public Integer tax_id;

    @JsonIgnore
    public boolean tax_idDirtyFlag;
    
    /**
     * 税率
     */
    public String tax_id_text;

    @JsonIgnore
    public boolean tax_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [科目]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

     /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }   

    /**
     * 获取 [科目]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return this.account_id_text ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return this.account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [核销金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [核销金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [核销金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [金额类型]
     */
    @JsonProperty("amount_type")
    public String getAmount_type(){
        return this.amount_type ;
    }

    /**
     * 设置 [金额类型]
     */
    @JsonProperty("amount_type")
    public void setAmount_type(String  amount_type){
        this.amount_type = amount_type ;
        this.amount_typeDirtyFlag = true ;
    }

     /**
     * 获取 [金额类型]脏标记
     */
    @JsonIgnore
    public boolean getAmount_typeDirtyFlag(){
        return this.amount_typeDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("analytic_account_id")
    public Integer getAnalytic_account_id(){
        return this.analytic_account_id ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("analytic_account_id")
    public void setAnalytic_account_id(Integer  analytic_account_id){
        this.analytic_account_id = analytic_account_id ;
        this.analytic_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_idDirtyFlag(){
        return this.analytic_account_idDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("analytic_account_id_text")
    public String getAnalytic_account_id_text(){
        return this.analytic_account_id_text ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("analytic_account_id_text")
    public void setAnalytic_account_id_text(String  analytic_account_id_text){
        this.analytic_account_id_text = analytic_account_id_text ;
        this.analytic_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_id_textDirtyFlag(){
        return this.analytic_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return this.analytic_tag_ids ;
    }

    /**
     * 设置 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [分析标签]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return this.analytic_tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [自动验证]
     */
    @JsonProperty("auto_reconcile")
    public String getAuto_reconcile(){
        return this.auto_reconcile ;
    }

    /**
     * 设置 [自动验证]
     */
    @JsonProperty("auto_reconcile")
    public void setAuto_reconcile(String  auto_reconcile){
        this.auto_reconcile = auto_reconcile ;
        this.auto_reconcileDirtyFlag = true ;
    }

     /**
     * 获取 [自动验证]脏标记
     */
    @JsonIgnore
    public boolean getAuto_reconcileDirtyFlag(){
        return this.auto_reconcileDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [第二含税价‎]
     */
    @JsonProperty("force_second_tax_included")
    public String getForce_second_tax_included(){
        return this.force_second_tax_included ;
    }

    /**
     * 设置 [第二含税价‎]
     */
    @JsonProperty("force_second_tax_included")
    public void setForce_second_tax_included(String  force_second_tax_included){
        this.force_second_tax_included = force_second_tax_included ;
        this.force_second_tax_includedDirtyFlag = true ;
    }

     /**
     * 获取 [第二含税价‎]脏标记
     */
    @JsonIgnore
    public boolean getForce_second_tax_includedDirtyFlag(){
        return this.force_second_tax_includedDirtyFlag ;
    }   

    /**
     * 获取 [含税价]
     */
    @JsonProperty("force_tax_included")
    public String getForce_tax_included(){
        return this.force_tax_included ;
    }

    /**
     * 设置 [含税价]
     */
    @JsonProperty("force_tax_included")
    public void setForce_tax_included(String  force_tax_included){
        this.force_tax_included = force_tax_included ;
        this.force_tax_includedDirtyFlag = true ;
    }

     /**
     * 获取 [含税价]脏标记
     */
    @JsonIgnore
    public boolean getForce_tax_includedDirtyFlag(){
        return this.force_tax_includedDirtyFlag ;
    }   

    /**
     * 获取 [添加第二行]
     */
    @JsonProperty("has_second_line")
    public String getHas_second_line(){
        return this.has_second_line ;
    }

    /**
     * 设置 [添加第二行]
     */
    @JsonProperty("has_second_line")
    public void setHas_second_line(String  has_second_line){
        this.has_second_line = has_second_line ;
        this.has_second_lineDirtyFlag = true ;
    }

     /**
     * 获取 [添加第二行]脏标记
     */
    @JsonIgnore
    public boolean getHas_second_lineDirtyFlag(){
        return this.has_second_lineDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [第二含税价‎]
     */
    @JsonProperty("is_second_tax_price_included")
    public String getIs_second_tax_price_included(){
        return this.is_second_tax_price_included ;
    }

    /**
     * 设置 [第二含税价‎]
     */
    @JsonProperty("is_second_tax_price_included")
    public void setIs_second_tax_price_included(String  is_second_tax_price_included){
        this.is_second_tax_price_included = is_second_tax_price_included ;
        this.is_second_tax_price_includedDirtyFlag = true ;
    }

     /**
     * 获取 [第二含税价‎]脏标记
     */
    @JsonIgnore
    public boolean getIs_second_tax_price_includedDirtyFlag(){
        return this.is_second_tax_price_includedDirtyFlag ;
    }   

    /**
     * 获取 [含税价]
     */
    @JsonProperty("is_tax_price_included")
    public String getIs_tax_price_included(){
        return this.is_tax_price_included ;
    }

    /**
     * 设置 [含税价]
     */
    @JsonProperty("is_tax_price_included")
    public void setIs_tax_price_included(String  is_tax_price_included){
        this.is_tax_price_included = is_tax_price_included ;
        this.is_tax_price_includedDirtyFlag = true ;
    }

     /**
     * 获取 [含税价]脏标记
     */
    @JsonIgnore
    public boolean getIs_tax_price_includedDirtyFlag(){
        return this.is_tax_price_includedDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日记账项目标签]
     */
    @JsonProperty("label")
    public String getLabel(){
        return this.label ;
    }

    /**
     * 设置 [日记账项目标签]
     */
    @JsonProperty("label")
    public void setLabel(String  label){
        this.label = label ;
        this.labelDirtyFlag = true ;
    }

     /**
     * 获取 [日记账项目标签]脏标记
     */
    @JsonIgnore
    public boolean getLabelDirtyFlag(){
        return this.labelDirtyFlag ;
    }   

    /**
     * 获取 [金额]
     */
    @JsonProperty("match_amount")
    public String getMatch_amount(){
        return this.match_amount ;
    }

    /**
     * 设置 [金额]
     */
    @JsonProperty("match_amount")
    public void setMatch_amount(String  match_amount){
        this.match_amount = match_amount ;
        this.match_amountDirtyFlag = true ;
    }

     /**
     * 获取 [金额]脏标记
     */
    @JsonIgnore
    public boolean getMatch_amountDirtyFlag(){
        return this.match_amountDirtyFlag ;
    }   

    /**
     * 获取 [参数最大金额]
     */
    @JsonProperty("match_amount_max")
    public Double getMatch_amount_max(){
        return this.match_amount_max ;
    }

    /**
     * 设置 [参数最大金额]
     */
    @JsonProperty("match_amount_max")
    public void setMatch_amount_max(Double  match_amount_max){
        this.match_amount_max = match_amount_max ;
        this.match_amount_maxDirtyFlag = true ;
    }

     /**
     * 获取 [参数最大金额]脏标记
     */
    @JsonIgnore
    public boolean getMatch_amount_maxDirtyFlag(){
        return this.match_amount_maxDirtyFlag ;
    }   

    /**
     * 获取 [参数最小金额]
     */
    @JsonProperty("match_amount_min")
    public Double getMatch_amount_min(){
        return this.match_amount_min ;
    }

    /**
     * 设置 [参数最小金额]
     */
    @JsonProperty("match_amount_min")
    public void setMatch_amount_min(Double  match_amount_min){
        this.match_amount_min = match_amount_min ;
        this.match_amount_minDirtyFlag = true ;
    }

     /**
     * 获取 [参数最小金额]脏标记
     */
    @JsonIgnore
    public boolean getMatch_amount_minDirtyFlag(){
        return this.match_amount_minDirtyFlag ;
    }   

    /**
     * 获取 [凭证类型]
     */
    @JsonProperty("match_journal_ids")
    public String getMatch_journal_ids(){
        return this.match_journal_ids ;
    }

    /**
     * 设置 [凭证类型]
     */
    @JsonProperty("match_journal_ids")
    public void setMatch_journal_ids(String  match_journal_ids){
        this.match_journal_ids = match_journal_ids ;
        this.match_journal_idsDirtyFlag = true ;
    }

     /**
     * 获取 [凭证类型]脏标记
     */
    @JsonIgnore
    public boolean getMatch_journal_idsDirtyFlag(){
        return this.match_journal_idsDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("match_label")
    public String getMatch_label(){
        return this.match_label ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("match_label")
    public void setMatch_label(String  match_label){
        this.match_label = match_label ;
        this.match_labelDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getMatch_labelDirtyFlag(){
        return this.match_labelDirtyFlag ;
    }   

    /**
     * 获取 [标签参数]
     */
    @JsonProperty("match_label_param")
    public String getMatch_label_param(){
        return this.match_label_param ;
    }

    /**
     * 设置 [标签参数]
     */
    @JsonProperty("match_label_param")
    public void setMatch_label_param(String  match_label_param){
        this.match_label_param = match_label_param ;
        this.match_label_paramDirtyFlag = true ;
    }

     /**
     * 获取 [标签参数]脏标记
     */
    @JsonIgnore
    public boolean getMatch_label_paramDirtyFlag(){
        return this.match_label_paramDirtyFlag ;
    }   

    /**
     * 获取 [数量性质]
     */
    @JsonProperty("match_nature")
    public String getMatch_nature(){
        return this.match_nature ;
    }

    /**
     * 设置 [数量性质]
     */
    @JsonProperty("match_nature")
    public void setMatch_nature(String  match_nature){
        this.match_nature = match_nature ;
        this.match_natureDirtyFlag = true ;
    }

     /**
     * 获取 [数量性质]脏标记
     */
    @JsonIgnore
    public boolean getMatch_natureDirtyFlag(){
        return this.match_natureDirtyFlag ;
    }   

    /**
     * 获取 [已经匹配合作伙伴]
     */
    @JsonProperty("match_partner")
    public String getMatch_partner(){
        return this.match_partner ;
    }

    /**
     * 设置 [已经匹配合作伙伴]
     */
    @JsonProperty("match_partner")
    public void setMatch_partner(String  match_partner){
        this.match_partner = match_partner ;
        this.match_partnerDirtyFlag = true ;
    }

     /**
     * 获取 [已经匹配合作伙伴]脏标记
     */
    @JsonIgnore
    public boolean getMatch_partnerDirtyFlag(){
        return this.match_partnerDirtyFlag ;
    }   

    /**
     * 获取 [限制合作伙伴类别为]
     */
    @JsonProperty("match_partner_category_ids")
    public String getMatch_partner_category_ids(){
        return this.match_partner_category_ids ;
    }

    /**
     * 设置 [限制合作伙伴类别为]
     */
    @JsonProperty("match_partner_category_ids")
    public void setMatch_partner_category_ids(String  match_partner_category_ids){
        this.match_partner_category_ids = match_partner_category_ids ;
        this.match_partner_category_idsDirtyFlag = true ;
    }

     /**
     * 获取 [限制合作伙伴类别为]脏标记
     */
    @JsonIgnore
    public boolean getMatch_partner_category_idsDirtyFlag(){
        return this.match_partner_category_idsDirtyFlag ;
    }   

    /**
     * 获取 [限制合作伙伴为]
     */
    @JsonProperty("match_partner_ids")
    public String getMatch_partner_ids(){
        return this.match_partner_ids ;
    }

    /**
     * 设置 [限制合作伙伴为]
     */
    @JsonProperty("match_partner_ids")
    public void setMatch_partner_ids(String  match_partner_ids){
        this.match_partner_ids = match_partner_ids ;
        this.match_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [限制合作伙伴为]脏标记
     */
    @JsonIgnore
    public boolean getMatch_partner_idsDirtyFlag(){
        return this.match_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [同币种匹配]
     */
    @JsonProperty("match_same_currency")
    public String getMatch_same_currency(){
        return this.match_same_currency ;
    }

    /**
     * 设置 [同币种匹配]
     */
    @JsonProperty("match_same_currency")
    public void setMatch_same_currency(String  match_same_currency){
        this.match_same_currency = match_same_currency ;
        this.match_same_currencyDirtyFlag = true ;
    }

     /**
     * 获取 [同币种匹配]脏标记
     */
    @JsonIgnore
    public boolean getMatch_same_currencyDirtyFlag(){
        return this.match_same_currencyDirtyFlag ;
    }   

    /**
     * 获取 [会计匹配]
     */
    @JsonProperty("match_total_amount")
    public String getMatch_total_amount(){
        return this.match_total_amount ;
    }

    /**
     * 设置 [会计匹配]
     */
    @JsonProperty("match_total_amount")
    public void setMatch_total_amount(String  match_total_amount){
        this.match_total_amount = match_total_amount ;
        this.match_total_amountDirtyFlag = true ;
    }

     /**
     * 获取 [会计匹配]脏标记
     */
    @JsonIgnore
    public boolean getMatch_total_amountDirtyFlag(){
        return this.match_total_amountDirtyFlag ;
    }   

    /**
     * 获取 [会计匹配%]
     */
    @JsonProperty("match_total_amount_param")
    public Double getMatch_total_amount_param(){
        return this.match_total_amount_param ;
    }

    /**
     * 设置 [会计匹配%]
     */
    @JsonProperty("match_total_amount_param")
    public void setMatch_total_amount_param(Double  match_total_amount_param){
        this.match_total_amount_param = match_total_amount_param ;
        this.match_total_amount_paramDirtyFlag = true ;
    }

     /**
     * 获取 [会计匹配%]脏标记
     */
    @JsonIgnore
    public boolean getMatch_total_amount_paramDirtyFlag(){
        return this.match_total_amount_paramDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("rule_type")
    public String getRule_type(){
        return this.rule_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("rule_type")
    public void setRule_type(String  rule_type){
        this.rule_type = rule_type ;
        this.rule_typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getRule_typeDirtyFlag(){
        return this.rule_typeDirtyFlag ;
    }   

    /**
     * 获取 [第二科目]
     */
    @JsonProperty("second_account_id")
    public Integer getSecond_account_id(){
        return this.second_account_id ;
    }

    /**
     * 设置 [第二科目]
     */
    @JsonProperty("second_account_id")
    public void setSecond_account_id(Integer  second_account_id){
        this.second_account_id = second_account_id ;
        this.second_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [第二科目]脏标记
     */
    @JsonIgnore
    public boolean getSecond_account_idDirtyFlag(){
        return this.second_account_idDirtyFlag ;
    }   

    /**
     * 获取 [第二科目]
     */
    @JsonProperty("second_account_id_text")
    public String getSecond_account_id_text(){
        return this.second_account_id_text ;
    }

    /**
     * 设置 [第二科目]
     */
    @JsonProperty("second_account_id_text")
    public void setSecond_account_id_text(String  second_account_id_text){
        this.second_account_id_text = second_account_id_text ;
        this.second_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [第二科目]脏标记
     */
    @JsonIgnore
    public boolean getSecond_account_id_textDirtyFlag(){
        return this.second_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [第二核销金额]
     */
    @JsonProperty("second_amount")
    public Double getSecond_amount(){
        return this.second_amount ;
    }

    /**
     * 设置 [第二核销金额]
     */
    @JsonProperty("second_amount")
    public void setSecond_amount(Double  second_amount){
        this.second_amount = second_amount ;
        this.second_amountDirtyFlag = true ;
    }

     /**
     * 获取 [第二核销金额]脏标记
     */
    @JsonIgnore
    public boolean getSecond_amountDirtyFlag(){
        return this.second_amountDirtyFlag ;
    }   

    /**
     * 获取 [第二金额类型]
     */
    @JsonProperty("second_amount_type")
    public String getSecond_amount_type(){
        return this.second_amount_type ;
    }

    /**
     * 设置 [第二金额类型]
     */
    @JsonProperty("second_amount_type")
    public void setSecond_amount_type(String  second_amount_type){
        this.second_amount_type = second_amount_type ;
        this.second_amount_typeDirtyFlag = true ;
    }

     /**
     * 获取 [第二金额类型]脏标记
     */
    @JsonIgnore
    public boolean getSecond_amount_typeDirtyFlag(){
        return this.second_amount_typeDirtyFlag ;
    }   

    /**
     * 获取 [第二分析帐户]
     */
    @JsonProperty("second_analytic_account_id")
    public Integer getSecond_analytic_account_id(){
        return this.second_analytic_account_id ;
    }

    /**
     * 设置 [第二分析帐户]
     */
    @JsonProperty("second_analytic_account_id")
    public void setSecond_analytic_account_id(Integer  second_analytic_account_id){
        this.second_analytic_account_id = second_analytic_account_id ;
        this.second_analytic_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [第二分析帐户]脏标记
     */
    @JsonIgnore
    public boolean getSecond_analytic_account_idDirtyFlag(){
        return this.second_analytic_account_idDirtyFlag ;
    }   

    /**
     * 获取 [第二分析帐户]
     */
    @JsonProperty("second_analytic_account_id_text")
    public String getSecond_analytic_account_id_text(){
        return this.second_analytic_account_id_text ;
    }

    /**
     * 设置 [第二分析帐户]
     */
    @JsonProperty("second_analytic_account_id_text")
    public void setSecond_analytic_account_id_text(String  second_analytic_account_id_text){
        this.second_analytic_account_id_text = second_analytic_account_id_text ;
        this.second_analytic_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [第二分析帐户]脏标记
     */
    @JsonIgnore
    public boolean getSecond_analytic_account_id_textDirtyFlag(){
        return this.second_analytic_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [第二分析标签]
     */
    @JsonProperty("second_analytic_tag_ids")
    public String getSecond_analytic_tag_ids(){
        return this.second_analytic_tag_ids ;
    }

    /**
     * 设置 [第二分析标签]
     */
    @JsonProperty("second_analytic_tag_ids")
    public void setSecond_analytic_tag_ids(String  second_analytic_tag_ids){
        this.second_analytic_tag_ids = second_analytic_tag_ids ;
        this.second_analytic_tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [第二分析标签]脏标记
     */
    @JsonIgnore
    public boolean getSecond_analytic_tag_idsDirtyFlag(){
        return this.second_analytic_tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [第二个分录]
     */
    @JsonProperty("second_journal_id")
    public Integer getSecond_journal_id(){
        return this.second_journal_id ;
    }

    /**
     * 设置 [第二个分录]
     */
    @JsonProperty("second_journal_id")
    public void setSecond_journal_id(Integer  second_journal_id){
        this.second_journal_id = second_journal_id ;
        this.second_journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [第二个分录]脏标记
     */
    @JsonIgnore
    public boolean getSecond_journal_idDirtyFlag(){
        return this.second_journal_idDirtyFlag ;
    }   

    /**
     * 获取 [第二个分录]
     */
    @JsonProperty("second_journal_id_text")
    public String getSecond_journal_id_text(){
        return this.second_journal_id_text ;
    }

    /**
     * 设置 [第二个分录]
     */
    @JsonProperty("second_journal_id_text")
    public void setSecond_journal_id_text(String  second_journal_id_text){
        this.second_journal_id_text = second_journal_id_text ;
        this.second_journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [第二个分录]脏标记
     */
    @JsonIgnore
    public boolean getSecond_journal_id_textDirtyFlag(){
        return this.second_journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [第二个分录项目标签]
     */
    @JsonProperty("second_label")
    public String getSecond_label(){
        return this.second_label ;
    }

    /**
     * 设置 [第二个分录项目标签]
     */
    @JsonProperty("second_label")
    public void setSecond_label(String  second_label){
        this.second_label = second_label ;
        this.second_labelDirtyFlag = true ;
    }

     /**
     * 获取 [第二个分录项目标签]脏标记
     */
    @JsonIgnore
    public boolean getSecond_labelDirtyFlag(){
        return this.second_labelDirtyFlag ;
    }   

    /**
     * 获取 [第二税率类别]
     */
    @JsonProperty("second_tax_amount_type")
    public String getSecond_tax_amount_type(){
        return this.second_tax_amount_type ;
    }

    /**
     * 设置 [第二税率类别]
     */
    @JsonProperty("second_tax_amount_type")
    public void setSecond_tax_amount_type(String  second_tax_amount_type){
        this.second_tax_amount_type = second_tax_amount_type ;
        this.second_tax_amount_typeDirtyFlag = true ;
    }

     /**
     * 获取 [第二税率类别]脏标记
     */
    @JsonIgnore
    public boolean getSecond_tax_amount_typeDirtyFlag(){
        return this.second_tax_amount_typeDirtyFlag ;
    }   

    /**
     * 获取 [第二个税]
     */
    @JsonProperty("second_tax_id")
    public Integer getSecond_tax_id(){
        return this.second_tax_id ;
    }

    /**
     * 设置 [第二个税]
     */
    @JsonProperty("second_tax_id")
    public void setSecond_tax_id(Integer  second_tax_id){
        this.second_tax_id = second_tax_id ;
        this.second_tax_idDirtyFlag = true ;
    }

     /**
     * 获取 [第二个税]脏标记
     */
    @JsonIgnore
    public boolean getSecond_tax_idDirtyFlag(){
        return this.second_tax_idDirtyFlag ;
    }   

    /**
     * 获取 [第二个税]
     */
    @JsonProperty("second_tax_id_text")
    public String getSecond_tax_id_text(){
        return this.second_tax_id_text ;
    }

    /**
     * 设置 [第二个税]
     */
    @JsonProperty("second_tax_id_text")
    public void setSecond_tax_id_text(String  second_tax_id_text){
        this.second_tax_id_text = second_tax_id_text ;
        this.second_tax_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [第二个税]脏标记
     */
    @JsonIgnore
    public boolean getSecond_tax_id_textDirtyFlag(){
        return this.second_tax_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [税率类别]
     */
    @JsonProperty("tax_amount_type")
    public String getTax_amount_type(){
        return this.tax_amount_type ;
    }

    /**
     * 设置 [税率类别]
     */
    @JsonProperty("tax_amount_type")
    public void setTax_amount_type(String  tax_amount_type){
        this.tax_amount_type = tax_amount_type ;
        this.tax_amount_typeDirtyFlag = true ;
    }

     /**
     * 获取 [税率类别]脏标记
     */
    @JsonIgnore
    public boolean getTax_amount_typeDirtyFlag(){
        return this.tax_amount_typeDirtyFlag ;
    }   

    /**
     * 获取 [税率]
     */
    @JsonProperty("tax_id")
    public Integer getTax_id(){
        return this.tax_id ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("tax_id")
    public void setTax_id(Integer  tax_id){
        this.tax_id = tax_id ;
        this.tax_idDirtyFlag = true ;
    }

     /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getTax_idDirtyFlag(){
        return this.tax_idDirtyFlag ;
    }   

    /**
     * 获取 [税率]
     */
    @JsonProperty("tax_id_text")
    public String getTax_id_text(){
        return this.tax_id_text ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("tax_id_text")
    public void setTax_id_text(String  tax_id_text){
        this.tax_id_text = tax_id_text ;
        this.tax_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getTax_id_textDirtyFlag(){
        return this.tax_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("account_id") instanceof Boolean)&& map.get("account_id")!=null){
			Object[] objs = (Object[])map.get("account_id");
			if(objs.length > 0){
				this.setAccount_id((Integer)objs[0]);
			}
		}
		if(!(map.get("account_id") instanceof Boolean)&& map.get("account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("account_id");
			if(objs.length > 1){
				this.setAccount_id_text((String)objs[1]);
			}
		}
		if(!(map.get("amount") instanceof Boolean)&& map.get("amount")!=null){
			this.setAmount((Double)map.get("amount"));
		}
		if(!(map.get("amount_type") instanceof Boolean)&& map.get("amount_type")!=null){
			this.setAmount_type((String)map.get("amount_type"));
		}
		if(!(map.get("analytic_account_id") instanceof Boolean)&& map.get("analytic_account_id")!=null){
			Object[] objs = (Object[])map.get("analytic_account_id");
			if(objs.length > 0){
				this.setAnalytic_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("analytic_account_id") instanceof Boolean)&& map.get("analytic_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("analytic_account_id");
			if(objs.length > 1){
				this.setAnalytic_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("analytic_tag_ids") instanceof Boolean)&& map.get("analytic_tag_ids")!=null){
			Object[] objs = (Object[])map.get("analytic_tag_ids");
			if(objs.length > 0){
				Integer[] analytic_tag_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAnalytic_tag_ids(Arrays.toString(analytic_tag_ids).replace(" ",""));
			}
		}
		if(map.get("auto_reconcile") instanceof Boolean){
			this.setAuto_reconcile(((Boolean)map.get("auto_reconcile"))? "true" : "false");
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("force_second_tax_included") instanceof Boolean){
			this.setForce_second_tax_included(((Boolean)map.get("force_second_tax_included"))? "true" : "false");
		}
		if(map.get("force_tax_included") instanceof Boolean){
			this.setForce_tax_included(((Boolean)map.get("force_tax_included"))? "true" : "false");
		}
		if(map.get("has_second_line") instanceof Boolean){
			this.setHas_second_line(((Boolean)map.get("has_second_line"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_second_tax_price_included") instanceof Boolean){
			this.setIs_second_tax_price_included(((Boolean)map.get("is_second_tax_price_included"))? "true" : "false");
		}
		if(map.get("is_tax_price_included") instanceof Boolean){
			this.setIs_tax_price_included(((Boolean)map.get("is_tax_price_included"))? "true" : "false");
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 0){
				this.setJournal_id((Integer)objs[0]);
			}
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 1){
				this.setJournal_id_text((String)objs[1]);
			}
		}
		if(!(map.get("label") instanceof Boolean)&& map.get("label")!=null){
			this.setLabel((String)map.get("label"));
		}
		if(!(map.get("match_amount") instanceof Boolean)&& map.get("match_amount")!=null){
			this.setMatch_amount((String)map.get("match_amount"));
		}
		if(!(map.get("match_amount_max") instanceof Boolean)&& map.get("match_amount_max")!=null){
			this.setMatch_amount_max((Double)map.get("match_amount_max"));
		}
		if(!(map.get("match_amount_min") instanceof Boolean)&& map.get("match_amount_min")!=null){
			this.setMatch_amount_min((Double)map.get("match_amount_min"));
		}
		if(!(map.get("match_journal_ids") instanceof Boolean)&& map.get("match_journal_ids")!=null){
			Object[] objs = (Object[])map.get("match_journal_ids");
			if(objs.length > 0){
				Integer[] match_journal_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMatch_journal_ids(Arrays.toString(match_journal_ids).replace(" ",""));
			}
		}
		if(!(map.get("match_label") instanceof Boolean)&& map.get("match_label")!=null){
			this.setMatch_label((String)map.get("match_label"));
		}
		if(!(map.get("match_label_param") instanceof Boolean)&& map.get("match_label_param")!=null){
			this.setMatch_label_param((String)map.get("match_label_param"));
		}
		if(!(map.get("match_nature") instanceof Boolean)&& map.get("match_nature")!=null){
			this.setMatch_nature((String)map.get("match_nature"));
		}
		if(map.get("match_partner") instanceof Boolean){
			this.setMatch_partner(((Boolean)map.get("match_partner"))? "true" : "false");
		}
		if(!(map.get("match_partner_category_ids") instanceof Boolean)&& map.get("match_partner_category_ids")!=null){
			Object[] objs = (Object[])map.get("match_partner_category_ids");
			if(objs.length > 0){
				Integer[] match_partner_category_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMatch_partner_category_ids(Arrays.toString(match_partner_category_ids).replace(" ",""));
			}
		}
		if(!(map.get("match_partner_ids") instanceof Boolean)&& map.get("match_partner_ids")!=null){
			Object[] objs = (Object[])map.get("match_partner_ids");
			if(objs.length > 0){
				Integer[] match_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMatch_partner_ids(Arrays.toString(match_partner_ids).replace(" ",""));
			}
		}
		if(map.get("match_same_currency") instanceof Boolean){
			this.setMatch_same_currency(((Boolean)map.get("match_same_currency"))? "true" : "false");
		}
		if(map.get("match_total_amount") instanceof Boolean){
			this.setMatch_total_amount(((Boolean)map.get("match_total_amount"))? "true" : "false");
		}
		if(!(map.get("match_total_amount_param") instanceof Boolean)&& map.get("match_total_amount_param")!=null){
			this.setMatch_total_amount_param((Double)map.get("match_total_amount_param"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("rule_type") instanceof Boolean)&& map.get("rule_type")!=null){
			this.setRule_type((String)map.get("rule_type"));
		}
		if(!(map.get("second_account_id") instanceof Boolean)&& map.get("second_account_id")!=null){
			Object[] objs = (Object[])map.get("second_account_id");
			if(objs.length > 0){
				this.setSecond_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("second_account_id") instanceof Boolean)&& map.get("second_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("second_account_id");
			if(objs.length > 1){
				this.setSecond_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("second_amount") instanceof Boolean)&& map.get("second_amount")!=null){
			this.setSecond_amount((Double)map.get("second_amount"));
		}
		if(!(map.get("second_amount_type") instanceof Boolean)&& map.get("second_amount_type")!=null){
			this.setSecond_amount_type((String)map.get("second_amount_type"));
		}
		if(!(map.get("second_analytic_account_id") instanceof Boolean)&& map.get("second_analytic_account_id")!=null){
			Object[] objs = (Object[])map.get("second_analytic_account_id");
			if(objs.length > 0){
				this.setSecond_analytic_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("second_analytic_account_id") instanceof Boolean)&& map.get("second_analytic_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("second_analytic_account_id");
			if(objs.length > 1){
				this.setSecond_analytic_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("second_analytic_tag_ids") instanceof Boolean)&& map.get("second_analytic_tag_ids")!=null){
			Object[] objs = (Object[])map.get("second_analytic_tag_ids");
			if(objs.length > 0){
				Integer[] second_analytic_tag_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setSecond_analytic_tag_ids(Arrays.toString(second_analytic_tag_ids).replace(" ",""));
			}
		}
		if(!(map.get("second_journal_id") instanceof Boolean)&& map.get("second_journal_id")!=null){
			Object[] objs = (Object[])map.get("second_journal_id");
			if(objs.length > 0){
				this.setSecond_journal_id((Integer)objs[0]);
			}
		}
		if(!(map.get("second_journal_id") instanceof Boolean)&& map.get("second_journal_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("second_journal_id");
			if(objs.length > 1){
				this.setSecond_journal_id_text((String)objs[1]);
			}
		}
		if(!(map.get("second_label") instanceof Boolean)&& map.get("second_label")!=null){
			this.setSecond_label((String)map.get("second_label"));
		}
		if(!(map.get("second_tax_amount_type") instanceof Boolean)&& map.get("second_tax_amount_type")!=null){
			this.setSecond_tax_amount_type((String)map.get("second_tax_amount_type"));
		}
		if(!(map.get("second_tax_id") instanceof Boolean)&& map.get("second_tax_id")!=null){
			Object[] objs = (Object[])map.get("second_tax_id");
			if(objs.length > 0){
				this.setSecond_tax_id((Integer)objs[0]);
			}
		}
		if(!(map.get("second_tax_id") instanceof Boolean)&& map.get("second_tax_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("second_tax_id");
			if(objs.length > 1){
				this.setSecond_tax_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("tax_amount_type") instanceof Boolean)&& map.get("tax_amount_type")!=null){
			this.setTax_amount_type((String)map.get("tax_amount_type"));
		}
		if(!(map.get("tax_id") instanceof Boolean)&& map.get("tax_id")!=null){
			Object[] objs = (Object[])map.get("tax_id");
			if(objs.length > 0){
				this.setTax_id((Integer)objs[0]);
			}
		}
		if(!(map.get("tax_id") instanceof Boolean)&& map.get("tax_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("tax_id");
			if(objs.length > 1){
				this.setTax_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccount_id()!=null&&this.getAccount_idDirtyFlag()){
			map.put("account_id",this.getAccount_id());
		}else if(this.getAccount_idDirtyFlag()){
			map.put("account_id",false);
		}
		if(this.getAccount_id_text()!=null&&this.getAccount_id_textDirtyFlag()){
			//忽略文本外键account_id_text
		}else if(this.getAccount_id_textDirtyFlag()){
			map.put("account_id",false);
		}
		if(this.getAmount()!=null&&this.getAmountDirtyFlag()){
			map.put("amount",this.getAmount());
		}else if(this.getAmountDirtyFlag()){
			map.put("amount",false);
		}
		if(this.getAmount_type()!=null&&this.getAmount_typeDirtyFlag()){
			map.put("amount_type",this.getAmount_type());
		}else if(this.getAmount_typeDirtyFlag()){
			map.put("amount_type",false);
		}
		if(this.getAnalytic_account_id()!=null&&this.getAnalytic_account_idDirtyFlag()){
			map.put("analytic_account_id",this.getAnalytic_account_id());
		}else if(this.getAnalytic_account_idDirtyFlag()){
			map.put("analytic_account_id",false);
		}
		if(this.getAnalytic_account_id_text()!=null&&this.getAnalytic_account_id_textDirtyFlag()){
			//忽略文本外键analytic_account_id_text
		}else if(this.getAnalytic_account_id_textDirtyFlag()){
			map.put("analytic_account_id",false);
		}
		if(this.getAnalytic_tag_ids()!=null&&this.getAnalytic_tag_idsDirtyFlag()){
			map.put("analytic_tag_ids",this.getAnalytic_tag_ids());
		}else if(this.getAnalytic_tag_idsDirtyFlag()){
			map.put("analytic_tag_ids",false);
		}
		if(this.getAuto_reconcile()!=null&&this.getAuto_reconcileDirtyFlag()){
			map.put("auto_reconcile",Boolean.parseBoolean(this.getAuto_reconcile()));		
		}		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getForce_second_tax_included()!=null&&this.getForce_second_tax_includedDirtyFlag()){
			map.put("force_second_tax_included",Boolean.parseBoolean(this.getForce_second_tax_included()));		
		}		if(this.getForce_tax_included()!=null&&this.getForce_tax_includedDirtyFlag()){
			map.put("force_tax_included",Boolean.parseBoolean(this.getForce_tax_included()));		
		}		if(this.getHas_second_line()!=null&&this.getHas_second_lineDirtyFlag()){
			map.put("has_second_line",Boolean.parseBoolean(this.getHas_second_line()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_second_tax_price_included()!=null&&this.getIs_second_tax_price_includedDirtyFlag()){
			map.put("is_second_tax_price_included",Boolean.parseBoolean(this.getIs_second_tax_price_included()));		
		}		if(this.getIs_tax_price_included()!=null&&this.getIs_tax_price_includedDirtyFlag()){
			map.put("is_tax_price_included",Boolean.parseBoolean(this.getIs_tax_price_included()));		
		}		if(this.getJournal_id()!=null&&this.getJournal_idDirtyFlag()){
			map.put("journal_id",this.getJournal_id());
		}else if(this.getJournal_idDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getJournal_id_text()!=null&&this.getJournal_id_textDirtyFlag()){
			//忽略文本外键journal_id_text
		}else if(this.getJournal_id_textDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getLabel()!=null&&this.getLabelDirtyFlag()){
			map.put("label",this.getLabel());
		}else if(this.getLabelDirtyFlag()){
			map.put("label",false);
		}
		if(this.getMatch_amount()!=null&&this.getMatch_amountDirtyFlag()){
			map.put("match_amount",this.getMatch_amount());
		}else if(this.getMatch_amountDirtyFlag()){
			map.put("match_amount",false);
		}
		if(this.getMatch_amount_max()!=null&&this.getMatch_amount_maxDirtyFlag()){
			map.put("match_amount_max",this.getMatch_amount_max());
		}else if(this.getMatch_amount_maxDirtyFlag()){
			map.put("match_amount_max",false);
		}
		if(this.getMatch_amount_min()!=null&&this.getMatch_amount_minDirtyFlag()){
			map.put("match_amount_min",this.getMatch_amount_min());
		}else if(this.getMatch_amount_minDirtyFlag()){
			map.put("match_amount_min",false);
		}
		if(this.getMatch_journal_ids()!=null&&this.getMatch_journal_idsDirtyFlag()){
			map.put("match_journal_ids",this.getMatch_journal_ids());
		}else if(this.getMatch_journal_idsDirtyFlag()){
			map.put("match_journal_ids",false);
		}
		if(this.getMatch_label()!=null&&this.getMatch_labelDirtyFlag()){
			map.put("match_label",this.getMatch_label());
		}else if(this.getMatch_labelDirtyFlag()){
			map.put("match_label",false);
		}
		if(this.getMatch_label_param()!=null&&this.getMatch_label_paramDirtyFlag()){
			map.put("match_label_param",this.getMatch_label_param());
		}else if(this.getMatch_label_paramDirtyFlag()){
			map.put("match_label_param",false);
		}
		if(this.getMatch_nature()!=null&&this.getMatch_natureDirtyFlag()){
			map.put("match_nature",this.getMatch_nature());
		}else if(this.getMatch_natureDirtyFlag()){
			map.put("match_nature",false);
		}
		if(this.getMatch_partner()!=null&&this.getMatch_partnerDirtyFlag()){
			map.put("match_partner",Boolean.parseBoolean(this.getMatch_partner()));		
		}		if(this.getMatch_partner_category_ids()!=null&&this.getMatch_partner_category_idsDirtyFlag()){
			map.put("match_partner_category_ids",this.getMatch_partner_category_ids());
		}else if(this.getMatch_partner_category_idsDirtyFlag()){
			map.put("match_partner_category_ids",false);
		}
		if(this.getMatch_partner_ids()!=null&&this.getMatch_partner_idsDirtyFlag()){
			map.put("match_partner_ids",this.getMatch_partner_ids());
		}else if(this.getMatch_partner_idsDirtyFlag()){
			map.put("match_partner_ids",false);
		}
		if(this.getMatch_same_currency()!=null&&this.getMatch_same_currencyDirtyFlag()){
			map.put("match_same_currency",Boolean.parseBoolean(this.getMatch_same_currency()));		
		}		if(this.getMatch_total_amount()!=null&&this.getMatch_total_amountDirtyFlag()){
			map.put("match_total_amount",Boolean.parseBoolean(this.getMatch_total_amount()));		
		}		if(this.getMatch_total_amount_param()!=null&&this.getMatch_total_amount_paramDirtyFlag()){
			map.put("match_total_amount_param",this.getMatch_total_amount_param());
		}else if(this.getMatch_total_amount_paramDirtyFlag()){
			map.put("match_total_amount_param",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getRule_type()!=null&&this.getRule_typeDirtyFlag()){
			map.put("rule_type",this.getRule_type());
		}else if(this.getRule_typeDirtyFlag()){
			map.put("rule_type",false);
		}
		if(this.getSecond_account_id()!=null&&this.getSecond_account_idDirtyFlag()){
			map.put("second_account_id",this.getSecond_account_id());
		}else if(this.getSecond_account_idDirtyFlag()){
			map.put("second_account_id",false);
		}
		if(this.getSecond_account_id_text()!=null&&this.getSecond_account_id_textDirtyFlag()){
			//忽略文本外键second_account_id_text
		}else if(this.getSecond_account_id_textDirtyFlag()){
			map.put("second_account_id",false);
		}
		if(this.getSecond_amount()!=null&&this.getSecond_amountDirtyFlag()){
			map.put("second_amount",this.getSecond_amount());
		}else if(this.getSecond_amountDirtyFlag()){
			map.put("second_amount",false);
		}
		if(this.getSecond_amount_type()!=null&&this.getSecond_amount_typeDirtyFlag()){
			map.put("second_amount_type",this.getSecond_amount_type());
		}else if(this.getSecond_amount_typeDirtyFlag()){
			map.put("second_amount_type",false);
		}
		if(this.getSecond_analytic_account_id()!=null&&this.getSecond_analytic_account_idDirtyFlag()){
			map.put("second_analytic_account_id",this.getSecond_analytic_account_id());
		}else if(this.getSecond_analytic_account_idDirtyFlag()){
			map.put("second_analytic_account_id",false);
		}
		if(this.getSecond_analytic_account_id_text()!=null&&this.getSecond_analytic_account_id_textDirtyFlag()){
			//忽略文本外键second_analytic_account_id_text
		}else if(this.getSecond_analytic_account_id_textDirtyFlag()){
			map.put("second_analytic_account_id",false);
		}
		if(this.getSecond_analytic_tag_ids()!=null&&this.getSecond_analytic_tag_idsDirtyFlag()){
			map.put("second_analytic_tag_ids",this.getSecond_analytic_tag_ids());
		}else if(this.getSecond_analytic_tag_idsDirtyFlag()){
			map.put("second_analytic_tag_ids",false);
		}
		if(this.getSecond_journal_id()!=null&&this.getSecond_journal_idDirtyFlag()){
			map.put("second_journal_id",this.getSecond_journal_id());
		}else if(this.getSecond_journal_idDirtyFlag()){
			map.put("second_journal_id",false);
		}
		if(this.getSecond_journal_id_text()!=null&&this.getSecond_journal_id_textDirtyFlag()){
			//忽略文本外键second_journal_id_text
		}else if(this.getSecond_journal_id_textDirtyFlag()){
			map.put("second_journal_id",false);
		}
		if(this.getSecond_label()!=null&&this.getSecond_labelDirtyFlag()){
			map.put("second_label",this.getSecond_label());
		}else if(this.getSecond_labelDirtyFlag()){
			map.put("second_label",false);
		}
		if(this.getSecond_tax_amount_type()!=null&&this.getSecond_tax_amount_typeDirtyFlag()){
			map.put("second_tax_amount_type",this.getSecond_tax_amount_type());
		}else if(this.getSecond_tax_amount_typeDirtyFlag()){
			map.put("second_tax_amount_type",false);
		}
		if(this.getSecond_tax_id()!=null&&this.getSecond_tax_idDirtyFlag()){
			map.put("second_tax_id",this.getSecond_tax_id());
		}else if(this.getSecond_tax_idDirtyFlag()){
			map.put("second_tax_id",false);
		}
		if(this.getSecond_tax_id_text()!=null&&this.getSecond_tax_id_textDirtyFlag()){
			//忽略文本外键second_tax_id_text
		}else if(this.getSecond_tax_id_textDirtyFlag()){
			map.put("second_tax_id",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getTax_amount_type()!=null&&this.getTax_amount_typeDirtyFlag()){
			map.put("tax_amount_type",this.getTax_amount_type());
		}else if(this.getTax_amount_typeDirtyFlag()){
			map.put("tax_amount_type",false);
		}
		if(this.getTax_id()!=null&&this.getTax_idDirtyFlag()){
			map.put("tax_id",this.getTax_id());
		}else if(this.getTax_idDirtyFlag()){
			map.put("tax_id",false);
		}
		if(this.getTax_id_text()!=null&&this.getTax_id_textDirtyFlag()){
			//忽略文本外键tax_id_text
		}else if(this.getTax_id_textDirtyFlag()){
			map.put("tax_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
