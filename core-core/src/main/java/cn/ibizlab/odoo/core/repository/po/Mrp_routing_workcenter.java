package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routing_workcenterSearchContext;

/**
 * 实体 [工作中心使用情况] 存储模型
 */
public interface Mrp_routing_workcenter{

    /**
     * 基于
     */
    Integer getTime_mode_batch();

    void setTime_mode_batch(Integer time_mode_batch);

    /**
     * 获取 [基于]脏标记
     */
    boolean getTime_mode_batchDirtyFlag();

    /**
     * 待处理的数量
     */
    Double getBatch_size();

    void setBatch_size(Double batch_size);

    /**
     * 获取 [待处理的数量]脏标记
     */
    boolean getBatch_sizeDirtyFlag();

    /**
     * 工单
     */
    String getWorkorder_ids();

    void setWorkorder_ids(String workorder_ids);

    /**
     * 获取 [工单]脏标记
     */
    boolean getWorkorder_idsDirtyFlag();

    /**
     * # 工单
     */
    Integer getWorkorder_count();

    void setWorkorder_count(Integer workorder_count);

    /**
     * 获取 [# 工单]脏标记
     */
    boolean getWorkorder_countDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 下一作业
     */
    String getBatch();

    void setBatch(String batch);

    /**
     * 获取 [下一作业]脏标记
     */
    boolean getBatchDirtyFlag();

    /**
     * 人工时长
     */
    Double getTime_cycle_manual();

    void setTime_cycle_manual(Double time_cycle_manual);

    /**
     * 获取 [人工时长]脏标记
     */
    boolean getTime_cycle_manualDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 说明
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 时长计算
     */
    String getTime_mode();

    void setTime_mode(String time_mode);

    /**
     * 获取 [时长计算]脏标记
     */
    boolean getTime_modeDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 操作
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [操作]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 时长
     */
    Double getTime_cycle();

    void setTime_cycle(Double time_cycle);

    /**
     * 获取 [时长]脏标记
     */
    boolean getTime_cycleDirtyFlag();

    /**
     * 工作记录表
     */
    byte[] getWorksheet();

    void setWorksheet(byte[] worksheet);

    /**
     * 获取 [工作记录表]脏标记
     */
    boolean getWorksheetDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 工作中心
     */
    String getWorkcenter_id_text();

    void setWorkcenter_id_text(String workcenter_id_text);

    /**
     * 获取 [工作中心]脏标记
     */
    boolean getWorkcenter_id_textDirtyFlag();

    /**
     * 父级工艺路线
     */
    String getRouting_id_text();

    void setRouting_id_text(String routing_id_text);

    /**
     * 获取 [父级工艺路线]脏标记
     */
    boolean getRouting_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 父级工艺路线
     */
    Integer getRouting_id();

    void setRouting_id(Integer routing_id);

    /**
     * 获取 [父级工艺路线]脏标记
     */
    boolean getRouting_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 工作中心
     */
    Integer getWorkcenter_id();

    void setWorkcenter_id(Integer workcenter_id);

    /**
     * 获取 [工作中心]脏标记
     */
    boolean getWorkcenter_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
