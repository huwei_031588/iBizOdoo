package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_automation;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automationSearchContext;

/**
 * 实体 [自动动作] 存储对象
 */
public interface Base_automationRepository extends Repository<Base_automation> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_automation> searchDefault(Base_automationSearchContext context);

    Base_automation convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Base_automation domain , Base_automation po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Base_automation convert2Domain( Base_automation po ,cn.ibizlab.odoo.core.odoo_base.domain.Base_automation domain) ;

}
