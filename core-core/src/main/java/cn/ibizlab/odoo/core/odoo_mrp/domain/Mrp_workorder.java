package cn.ibizlab.odoo.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [工单] 对象
 */
@Data
public class Mrp_workorder extends EntityClient implements Serializable {

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 时间
     */
    @JSONField(name = "time_ids")
    @JsonProperty("time_ids")
    private String timeIds;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 在此工单工作的用户
     */
    @JSONField(name = "working_user_ids")
    @JsonProperty("working_user_ids")
    private String workingUserIds;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 数量
     */
    @DEField(name = "qty_produced")
    @JSONField(name = "qty_produced")
    @JsonProperty("qty_produced")
    private Double qtyProduced;

    /**
     * 实际时长
     */
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 报废转移
     */
    @JSONField(name = "scrap_count")
    @JsonProperty("scrap_count")
    private Integer scrapCount;

    /**
     * 安排的开始日期
     */
    @DEField(name = "date_planned_start")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned_start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned_start")
    private Timestamp datePlannedStart;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 当前用户正在工作吗？
     */
    @JSONField(name = "is_user_working")
    @JsonProperty("is_user_working")
    private String isUserWorking;

    /**
     * 移动
     */
    @JSONField(name = "move_raw_ids")
    @JsonProperty("move_raw_ids")
    private String moveRawIds;

    /**
     * 上一个在此工单工作的用户
     */
    @JSONField(name = "last_working_user_id")
    @JsonProperty("last_working_user_id")
    private String lastWorkingUserId;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 待追踪的产品
     */
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;

    /**
     * 工单
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * Is the first WO to produce
     */
    @JSONField(name = "is_first_wo")
    @JsonProperty("is_first_wo")
    private String isFirstWo;

    /**
     * 已生产
     */
    @JSONField(name = "is_produced")
    @JsonProperty("is_produced")
    private String isProduced;

    /**
     * 实际开始日期
     */
    @DEField(name = "date_start")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_start")
    private Timestamp dateStart;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 颜色
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 每单位时长
     */
    @DEField(name = "duration_unit")
    @JSONField(name = "duration_unit")
    @JsonProperty("duration_unit")
    private Double durationUnit;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 容量
     */
    @JSONField(name = "capacity")
    @JsonProperty("capacity")
    private Double capacity;

    /**
     * 操作凭证行
     */
    @JSONField(name = "active_move_line_ids")
    @JsonProperty("active_move_line_ids")
    private String activeMoveLineIds;

    /**
     * 安排的完工日期
     */
    @DEField(name = "date_planned_finished")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned_finished" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned_finished")
    private Timestamp datePlannedFinished;

    /**
     * 实际结束日期
     */
    @DEField(name = "date_finished")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_finished" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_finished")
    private Timestamp dateFinished;

    /**
     * 将被生产的数量
     */
    @JSONField(name = "qty_remaining")
    @JsonProperty("qty_remaining")
    private Double qtyRemaining;

    /**
     * 报废
     */
    @JSONField(name = "scrap_ids")
    @JsonProperty("scrap_ids")
    private String scrapIds;

    /**
     * 时长偏差(%)
     */
    @DEField(name = "duration_percent")
    @JSONField(name = "duration_percent")
    @JsonProperty("duration_percent")
    private Integer durationPercent;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 预计时长
     */
    @DEField(name = "duration_expected")
    @JSONField(name = "duration_expected")
    @JsonProperty("duration_expected")
    private Double durationExpected;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 当前的已生产数量
     */
    @DEField(name = "qty_producing")
    @JSONField(name = "qty_producing")
    @JsonProperty("qty_producing")
    private Double qtyProducing;

    /**
     * 生产日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "production_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("production_date")
    private Timestamp productionDate;

    /**
     * 工作记录表
     */
    @JSONField(name = "worksheet")
    @JsonProperty("worksheet")
    private byte[] worksheet;

    /**
     * 工作中心
     */
    @JSONField(name = "workcenter_id_text")
    @JsonProperty("workcenter_id_text")
    private String workcenterIdText;

    /**
     * 批次/序列号码
     */
    @JSONField(name = "final_lot_id_text")
    @JsonProperty("final_lot_id_text")
    private String finalLotIdText;

    /**
     * 工作中心状态
     */
    @JSONField(name = "working_state")
    @JsonProperty("working_state")
    private String workingState;

    /**
     * 原始生产数量
     */
    @JSONField(name = "qty_production")
    @JsonProperty("qty_production")
    private Double qtyProduction;

    /**
     * 计量单位
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 材料可用性
     */
    @JSONField(name = "production_availability")
    @JsonProperty("production_availability")
    private String productionAvailability;

    /**
     * 状态
     */
    @JSONField(name = "production_state")
    @JsonProperty("production_state")
    private String productionState;

    /**
     * 下一工单
     */
    @JSONField(name = "next_work_order_id_text")
    @JsonProperty("next_work_order_id_text")
    private String nextWorkOrderIdText;

    /**
     * 操作
     */
    @JSONField(name = "operation_id_text")
    @JsonProperty("operation_id_text")
    private String operationIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 制造订单
     */
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    private String productionIdText;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 追踪
     */
    @JSONField(name = "product_tracking")
    @JsonProperty("product_tracking")
    private String productTracking;

    /**
     * 批次/序列号码
     */
    @DEField(name = "final_lot_id")
    @JSONField(name = "final_lot_id")
    @JsonProperty("final_lot_id")
    private Integer finalLotId;

    /**
     * 工作中心
     */
    @DEField(name = "workcenter_id")
    @JSONField(name = "workcenter_id")
    @JsonProperty("workcenter_id")
    private Integer workcenterId;

    /**
     * 制造订单
     */
    @DEField(name = "production_id")
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Integer productionId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 操作
     */
    @DEField(name = "operation_id")
    @JSONField(name = "operation_id")
    @JsonProperty("operation_id")
    private Integer operationId;

    /**
     * 下一工单
     */
    @DEField(name = "next_work_order_id")
    @JSONField(name = "next_work_order_id")
    @JsonProperty("next_work_order_id")
    private Integer nextWorkOrderId;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odooproduction")
    @JsonProperty("odooproduction")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production odooProduction;

    /**
     * 
     */
    @JSONField(name = "odoooperation")
    @JsonProperty("odoooperation")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing_workcenter odooOperation;

    /**
     * 
     */
    @JSONField(name = "odooworkcenter")
    @JsonProperty("odooworkcenter")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter odooWorkcenter;

    /**
     * 
     */
    @JSONField(name = "odoonextworkorder")
    @JsonProperty("odoonextworkorder")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder odooNextWorkOrder;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoofinallot")
    @JsonProperty("odoofinallot")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot odooFinalLot;




    /**
     * 设置 [数量]
     */
    public void setQtyProduced(Double qtyProduced){
        this.qtyProduced = qtyProduced ;
        this.modify("qty_produced",qtyProduced);
    }
    /**
     * 设置 [实际时长]
     */
    public void setDuration(Double duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [安排的开始日期]
     */
    public void setDatePlannedStart(Timestamp datePlannedStart){
        this.datePlannedStart = datePlannedStart ;
        this.modify("date_planned_start",datePlannedStart);
    }
    /**
     * 设置 [工单]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [实际开始日期]
     */
    public void setDateStart(Timestamp dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }
    /**
     * 设置 [每单位时长]
     */
    public void setDurationUnit(Double durationUnit){
        this.durationUnit = durationUnit ;
        this.modify("duration_unit",durationUnit);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [容量]
     */
    public void setCapacity(Double capacity){
        this.capacity = capacity ;
        this.modify("capacity",capacity);
    }
    /**
     * 设置 [安排的完工日期]
     */
    public void setDatePlannedFinished(Timestamp datePlannedFinished){
        this.datePlannedFinished = datePlannedFinished ;
        this.modify("date_planned_finished",datePlannedFinished);
    }
    /**
     * 设置 [实际结束日期]
     */
    public void setDateFinished(Timestamp dateFinished){
        this.dateFinished = dateFinished ;
        this.modify("date_finished",dateFinished);
    }
    /**
     * 设置 [时长偏差(%)]
     */
    public void setDurationPercent(Integer durationPercent){
        this.durationPercent = durationPercent ;
        this.modify("duration_percent",durationPercent);
    }
    /**
     * 设置 [预计时长]
     */
    public void setDurationExpected(Double durationExpected){
        this.durationExpected = durationExpected ;
        this.modify("duration_expected",durationExpected);
    }
    /**
     * 设置 [当前的已生产数量]
     */
    public void setQtyProducing(Double qtyProducing){
        this.qtyProducing = qtyProducing ;
        this.modify("qty_producing",qtyProducing);
    }
    /**
     * 设置 [批次/序列号码]
     */
    public void setFinalLotId(Integer finalLotId){
        this.finalLotId = finalLotId ;
        this.modify("final_lot_id",finalLotId);
    }
    /**
     * 设置 [工作中心]
     */
    public void setWorkcenterId(Integer workcenterId){
        this.workcenterId = workcenterId ;
        this.modify("workcenter_id",workcenterId);
    }
    /**
     * 设置 [制造订单]
     */
    public void setProductionId(Integer productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }
    /**
     * 设置 [操作]
     */
    public void setOperationId(Integer operationId){
        this.operationId = operationId ;
        this.modify("operation_id",operationId);
    }
    /**
     * 设置 [下一工单]
     */
    public void setNextWorkOrderId(Integer nextWorkOrderId){
        this.nextWorkOrderId = nextWorkOrderId ;
        this.modify("next_work_order_id",nextWorkOrderId);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

}


