package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_fiscal_position_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_taxSearchContext;

/**
 * 实体 [税率的科目调整] 存储对象
 */
public interface Account_fiscal_position_taxRepository extends Repository<Account_fiscal_position_tax> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_fiscal_position_tax> searchDefault(Account_fiscal_position_taxSearchContext context);

    Account_fiscal_position_tax convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax domain , Account_fiscal_position_tax po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax convert2Domain( Account_fiscal_position_tax po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax domain) ;

}
