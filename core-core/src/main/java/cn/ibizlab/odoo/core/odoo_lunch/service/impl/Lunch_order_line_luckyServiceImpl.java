package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line_lucky;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_line_luckySearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_order_line_luckyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_order_line_luckyOdooClient;
import cn.ibizlab.odoo.core.odoo_lunch.clientmodel.lunch_order_line_luckyClientModel;

/**
 * 实体[幸运工作餐订单明细行] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_order_line_luckyServiceImpl implements ILunch_order_line_luckyService {

    @Autowired
    lunch_order_line_luckyOdooClient lunch_order_line_luckyOdooClient;


    @Override
    public Lunch_order_line_lucky get(Integer id) {
        lunch_order_line_luckyClientModel clientModel = new lunch_order_line_luckyClientModel();
        clientModel.setId(id);
		lunch_order_line_luckyOdooClient.get(clientModel);
        Lunch_order_line_lucky et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Lunch_order_line_lucky();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Lunch_order_line_lucky et) {
        lunch_order_line_luckyClientModel clientModel = convert2Model(et,null);
		lunch_order_line_luckyOdooClient.create(clientModel);
        Lunch_order_line_lucky rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_order_line_lucky> list){
    }

    @Override
    public boolean update(Lunch_order_line_lucky et) {
        lunch_order_line_luckyClientModel clientModel = convert2Model(et,null);
		lunch_order_line_luckyOdooClient.update(clientModel);
        Lunch_order_line_lucky rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Lunch_order_line_lucky> list){
    }

    @Override
    public boolean remove(Integer id) {
        lunch_order_line_luckyClientModel clientModel = new lunch_order_line_luckyClientModel();
        clientModel.setId(id);
		lunch_order_line_luckyOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_order_line_lucky> searchDefault(Lunch_order_line_luckySearchContext context) {
        List<Lunch_order_line_lucky> list = new ArrayList<Lunch_order_line_lucky>();
        Page<lunch_order_line_luckyClientModel> clientModelList = lunch_order_line_luckyOdooClient.search(context);
        for(lunch_order_line_luckyClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Lunch_order_line_lucky>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public lunch_order_line_luckyClientModel convert2Model(Lunch_order_line_lucky domain , lunch_order_line_luckyClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new lunch_order_line_luckyClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("is_max_budgetdirtyflag"))
                model.setIs_max_budget(domain.getIsMaxBudget());
            if((Boolean) domain.getExtensionparams().get("supplier_idsdirtyflag"))
                model.setSupplier_ids(domain.getSupplierIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("max_budgetdirtyflag"))
                model.setMax_budget(domain.getMaxBudget());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Lunch_order_line_lucky convert2Domain( lunch_order_line_luckyClientModel model ,Lunch_order_line_lucky domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Lunch_order_line_lucky();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIs_max_budgetDirtyFlag())
            domain.setIsMaxBudget(model.getIs_max_budget());
        if(model.getSupplier_idsDirtyFlag())
            domain.setSupplierIds(model.getSupplier_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMax_budgetDirtyFlag())
            domain.setMaxBudget(model.getMax_budget());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



