package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partner_massSearchContext;

/**
 * 实体 [转化线索为商机（批量）] 存储对象
 */
public interface Crm_lead2opportunity_partner_massRepository extends Repository<Crm_lead2opportunity_partner_mass> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_lead2opportunity_partner_mass> searchDefault(Crm_lead2opportunity_partner_massSearchContext context);

    Crm_lead2opportunity_partner_mass convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass domain , Crm_lead2opportunity_partner_mass po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass convert2Domain( Crm_lead2opportunity_partner_mass po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass domain) ;

}
