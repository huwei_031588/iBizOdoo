package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_lineSearchContext;

/**
 * 实体 [日记账项目] 存储模型
 */
public interface Account_move_line{

    /**
     * 分析标签
     */
    String getAnalytic_tag_ids();

    void setAnalytic_tag_ids(String analytic_tag_ids);

    /**
     * 获取 [分析标签]脏标记
     */
    boolean getAnalytic_tag_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 标签
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [标签]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 余额
     */
    Double getBalance();

    void setBalance(Double balance);

    /**
     * 获取 [余额]脏标记
     */
    boolean getBalanceDirtyFlag();

    /**
     * 采用的税
     */
    String getTax_ids();

    void setTax_ids(String tax_ids);

    /**
     * 获取 [采用的税]脏标记
     */
    boolean getTax_idsDirtyFlag();

    /**
     * 货币金额
     */
    Double getAmount_currency();

    void setAmount_currency(Double amount_currency);

    /**
     * 获取 [货币金额]脏标记
     */
    boolean getAmount_currencyDirtyFlag();

    /**
     * 旧税额
     */
    String getTax_line_grouping_key();

    void setTax_line_grouping_key(String tax_line_grouping_key);

    /**
     * 获取 [旧税额]脏标记
     */
    boolean getTax_line_grouping_keyDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 已核销
     */
    String getReconciled();

    void setReconciled(String reconciled);

    /**
     * 获取 [已核销]脏标记
     */
    boolean getReconciledDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 现金余额
     */
    Double getBalance_cash_basis();

    void setBalance_cash_basis(Double balance_cash_basis);

    /**
     * 获取 [现金余额]脏标记
     */
    boolean getBalance_cash_basisDirtyFlag();

    /**
     * 贷方现金基础
     */
    Double getCredit_cash_basis();

    void setCredit_cash_basis(Double credit_cash_basis);

    /**
     * 获取 [贷方现金基础]脏标记
     */
    boolean getCredit_cash_basisDirtyFlag();

    /**
     * 数量
     */
    Double getQuantity();

    void setQuantity(Double quantity);

    /**
     * 获取 [数量]脏标记
     */
    boolean getQuantityDirtyFlag();

    /**
     * 残值金额
     */
    Double getAmount_residual();

    void setAmount_residual(Double amount_residual);

    /**
     * 获取 [残值金额]脏标记
     */
    boolean getAmount_residualDirtyFlag();

    /**
     * 重新计算税额
     */
    String getRecompute_tax_line();

    void setRecompute_tax_line(String recompute_tax_line);

    /**
     * 获取 [重新计算税额]脏标记
     */
    boolean getRecompute_tax_lineDirtyFlag();

    /**
     * 出现在VAT报告
     */
    String getTax_exigible();

    void setTax_exigible(String tax_exigible);

    /**
     * 获取 [出现在VAT报告]脏标记
     */
    boolean getTax_exigibleDirtyFlag();

    /**
     * 外币残余金额
     */
    Double getAmount_residual_currency();

    void setAmount_residual_currency(Double amount_residual_currency);

    /**
     * 获取 [外币残余金额]脏标记
     */
    boolean getAmount_residual_currencyDirtyFlag();

    /**
     * 基本金额
     */
    Double getTax_base_amount();

    void setTax_base_amount(Double tax_base_amount);

    /**
     * 获取 [基本金额]脏标记
     */
    boolean getTax_base_amountDirtyFlag();

    /**
     * 上级状态
     */
    String getParent_state();

    void setParent_state(String parent_state);

    /**
     * 获取 [上级状态]脏标记
     */
    boolean getParent_stateDirtyFlag();

    /**
     * 无催款
     */
    String getBlocked();

    void setBlocked(String blocked);

    /**
     * 获取 [无催款]脏标记
     */
    boolean getBlockedDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 匹配的贷方
     */
    String getMatched_credit_ids();

    void setMatched_credit_ids(String matched_credit_ids);

    /**
     * 获取 [匹配的贷方]脏标记
     */
    boolean getMatched_credit_idsDirtyFlag();

    /**
     * 分析明细行
     */
    String getAnalytic_line_ids();

    void setAnalytic_line_ids(String analytic_line_ids);

    /**
     * 获取 [分析明细行]脏标记
     */
    boolean getAnalytic_line_idsDirtyFlag();

    /**
     * 到期日期
     */
    Timestamp getDate_maturity();

    void setDate_maturity(Timestamp date_maturity);

    /**
     * 获取 [到期日期]脏标记
     */
    boolean getDate_maturityDirtyFlag();

    /**
     * 借方
     */
    Double getDebit();

    void setDebit(Double debit);

    /**
     * 获取 [借方]脏标记
     */
    boolean getDebitDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 对方
     */
    String getCounterpart();

    void setCounterpart(String counterpart);

    /**
     * 获取 [对方]脏标记
     */
    boolean getCounterpartDirtyFlag();

    /**
     * 匹配的借记卡
     */
    String getMatched_debit_ids();

    void setMatched_debit_ids(String matched_debit_ids);

    /**
     * 获取 [匹配的借记卡]脏标记
     */
    boolean getMatched_debit_idsDirtyFlag();

    /**
     * 借记现金基础
     */
    Double getDebit_cash_basis();

    void setDebit_cash_basis(Double debit_cash_basis);

    /**
     * 获取 [借记现金基础]脏标记
     */
    boolean getDebit_cash_basisDirtyFlag();

    /**
     * 贷方
     */
    Double getCredit();

    void setCredit(Double credit);

    /**
     * 获取 [贷方]脏标记
     */
    boolean getCreditDirtyFlag();

    /**
     * 记叙
     */
    String getNarration();

    void setNarration(String narration);

    /**
     * 获取 [记叙]脏标记
     */
    boolean getNarrationDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 类型
     */
    String getUser_type_id_text();

    void setUser_type_id_text(String user_type_id_text);

    /**
     * 获取 [类型]脏标记
     */
    boolean getUser_type_id_textDirtyFlag();

    /**
     * 发起人付款
     */
    String getPayment_id_text();

    void setPayment_id_text(String payment_id_text);

    /**
     * 获取 [发起人付款]脏标记
     */
    boolean getPayment_id_textDirtyFlag();

    /**
     * 发起人税
     */
    String getTax_line_id_text();

    void setTax_line_id_text(String tax_line_id_text);

    /**
     * 获取 [发起人税]脏标记
     */
    boolean getTax_line_id_textDirtyFlag();

    /**
     * 匹配号码
     */
    String getFull_reconcile_id_text();

    void setFull_reconcile_id_text(String full_reconcile_id_text);

    /**
     * 获取 [匹配号码]脏标记
     */
    boolean getFull_reconcile_id_textDirtyFlag();

    /**
     * 费用
     */
    String getExpense_id_text();

    void setExpense_id_text(String expense_id_text);

    /**
     * 获取 [费用]脏标记
     */
    boolean getExpense_id_textDirtyFlag();

    /**
     * 日记账分录
     */
    String getMove_id_text();

    void setMove_id_text(String move_id_text);

    /**
     * 获取 [日记账分录]脏标记
     */
    boolean getMove_id_textDirtyFlag();

    /**
     * 日记账
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 公司货币
     */
    String getCompany_currency_id_text();

    void setCompany_currency_id_text(String company_currency_id_text);

    /**
     * 获取 [公司货币]脏标记
     */
    boolean getCompany_currency_id_textDirtyFlag();

    /**
     * 发票
     */
    String getInvoice_id_text();

    void setInvoice_id_text(String invoice_id_text);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_id_textDirtyFlag();

    /**
     * 分析账户
     */
    String getAnalytic_account_id_text();

    void setAnalytic_account_id_text(String analytic_account_id_text);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAnalytic_account_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 参考
     */
    String getRef();

    void setRef(String ref);

    /**
     * 获取 [参考]脏标记
     */
    boolean getRefDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 报告
     */
    String getStatement_id_text();

    void setStatement_id_text(String statement_id_text);

    /**
     * 获取 [报告]脏标记
     */
    boolean getStatement_id_textDirtyFlag();

    /**
     * 用该分录核销的银行核销单明细
     */
    String getStatement_line_id_text();

    void setStatement_line_id_text(String statement_line_id_text);

    /**
     * 获取 [用该分录核销的银行核销单明细]脏标记
     */
    boolean getStatement_line_id_textDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 科目
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [科目]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 计量单位
     */
    String getProduct_uom_id_text();

    void setProduct_uom_id_text(String product_uom_id_text);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 日记账分录
     */
    Integer getMove_id();

    void setMove_id(Integer move_id);

    /**
     * 获取 [日记账分录]脏标记
     */
    boolean getMove_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 发起人付款
     */
    Integer getPayment_id();

    void setPayment_id(Integer payment_id);

    /**
     * 获取 [发起人付款]脏标记
     */
    boolean getPayment_idDirtyFlag();

    /**
     * 公司货币
     */
    Integer getCompany_currency_id();

    void setCompany_currency_id(Integer company_currency_id);

    /**
     * 获取 [公司货币]脏标记
     */
    boolean getCompany_currency_idDirtyFlag();

    /**
     * 日记账
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * 分析账户
     */
    Integer getAnalytic_account_id();

    void setAnalytic_account_id(Integer analytic_account_id);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAnalytic_account_idDirtyFlag();

    /**
     * 发票
     */
    Integer getInvoice_id();

    void setInvoice_id(Integer invoice_id);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_idDirtyFlag();

    /**
     * 发起人税
     */
    Integer getTax_line_id();

    void setTax_line_id(Integer tax_line_id);

    /**
     * 获取 [发起人税]脏标记
     */
    boolean getTax_line_idDirtyFlag();

    /**
     * 匹配号码
     */
    Integer getFull_reconcile_id();

    void setFull_reconcile_id(Integer full_reconcile_id);

    /**
     * 获取 [匹配号码]脏标记
     */
    boolean getFull_reconcile_idDirtyFlag();

    /**
     * 用该分录核销的银行核销单明细
     */
    Integer getStatement_line_id();

    void setStatement_line_id(Integer statement_line_id);

    /**
     * 获取 [用该分录核销的银行核销单明细]脏标记
     */
    boolean getStatement_line_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 科目
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [科目]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 报告
     */
    Integer getStatement_id();

    void setStatement_id(Integer statement_id);

    /**
     * 获取 [报告]脏标记
     */
    boolean getStatement_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 类型
     */
    Integer getUser_type_id();

    void setUser_type_id(Integer user_type_id);

    /**
     * 获取 [类型]脏标记
     */
    boolean getUser_type_idDirtyFlag();

    /**
     * 费用
     */
    Integer getExpense_id();

    void setExpense_id(Integer expense_id);

    /**
     * 获取 [费用]脏标记
     */
    boolean getExpense_idDirtyFlag();

}
