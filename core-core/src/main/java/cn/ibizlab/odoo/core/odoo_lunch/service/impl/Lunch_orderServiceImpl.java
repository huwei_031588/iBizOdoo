package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_orderOdooClient;
import cn.ibizlab.odoo.core.odoo_lunch.clientmodel.lunch_orderClientModel;

/**
 * 实体[午餐订单] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_orderServiceImpl implements ILunch_orderService {

    @Autowired
    lunch_orderOdooClient lunch_orderOdooClient;


    @Override
    public Lunch_order get(Integer id) {
        lunch_orderClientModel clientModel = new lunch_orderClientModel();
        clientModel.setId(id);
		lunch_orderOdooClient.get(clientModel);
        Lunch_order et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Lunch_order();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Lunch_order et) {
        lunch_orderClientModel clientModel = convert2Model(et,null);
		lunch_orderOdooClient.create(clientModel);
        Lunch_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_order> list){
    }

    @Override
    public boolean remove(Integer id) {
        lunch_orderClientModel clientModel = new lunch_orderClientModel();
        clientModel.setId(id);
		lunch_orderOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Lunch_order et) {
        lunch_orderClientModel clientModel = convert2Model(et,null);
		lunch_orderOdooClient.update(clientModel);
        Lunch_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Lunch_order> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_order> searchDefault(Lunch_orderSearchContext context) {
        List<Lunch_order> list = new ArrayList<Lunch_order>();
        Page<lunch_orderClientModel> clientModelList = lunch_orderOdooClient.search(context);
        for(lunch_orderClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Lunch_order>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public lunch_orderClientModel convert2Model(Lunch_order domain , lunch_orderClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new lunch_orderClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("previous_order_widgetdirtyflag"))
                model.setPrevious_order_widget(domain.getPreviousOrderWidget());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("order_line_idsdirtyflag"))
                model.setOrder_line_ids(domain.getOrderLineIds());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("balance_visibledirtyflag"))
                model.setBalance_visible(domain.getBalanceVisible());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("alertsdirtyflag"))
                model.setAlerts(domain.getAlerts());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("cash_move_balancedirtyflag"))
                model.setCash_move_balance(domain.getCashMoveBalance());
            if((Boolean) domain.getExtensionparams().get("previous_order_idsdirtyflag"))
                model.setPrevious_order_ids(domain.getPreviousOrderIds());
            if((Boolean) domain.getExtensionparams().get("totaldirtyflag"))
                model.setTotal(domain.getTotal());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Lunch_order convert2Domain( lunch_orderClientModel model ,Lunch_order domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Lunch_order();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getPrevious_order_widgetDirtyFlag())
            domain.setPreviousOrderWidget(model.getPrevious_order_widget());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getOrder_line_idsDirtyFlag())
            domain.setOrderLineIds(model.getOrder_line_ids());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getBalance_visibleDirtyFlag())
            domain.setBalanceVisible(model.getBalance_visible());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getAlertsDirtyFlag())
            domain.setAlerts(model.getAlerts());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCash_move_balanceDirtyFlag())
            domain.setCashMoveBalance(model.getCash_move_balance());
        if(model.getPrevious_order_idsDirtyFlag())
            domain.setPreviousOrderIds(model.getPrevious_order_ids());
        if(model.getTotalDirtyFlag())
            domain.setTotal(model.getTotal());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



