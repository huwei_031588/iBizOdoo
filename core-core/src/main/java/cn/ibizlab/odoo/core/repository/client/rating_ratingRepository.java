package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.rating_rating;

/**
 * 实体[rating_rating] 服务对象接口
 */
public interface rating_ratingRepository{


    public rating_rating createPO() ;
        public void get(String id);

        public void update(rating_rating rating_rating);

        public void create(rating_rating rating_rating);

        public void remove(String id);

        public void updateBatch(rating_rating rating_rating);

        public void removeBatch(String id);

        public List<rating_rating> search();

        public void createBatch(rating_rating rating_rating);


}
