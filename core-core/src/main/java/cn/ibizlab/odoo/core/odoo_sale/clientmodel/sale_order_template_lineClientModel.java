package cn.ibizlab.odoo.core.odoo_sale.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[sale_order_template_line] 对象
 */
public class sale_order_template_lineClientModel implements Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 折扣(%)
     */
    public Double discount;

    @JsonIgnore
    public boolean discountDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 显示类型
     */
    public String display_type;

    @JsonIgnore
    public boolean display_typeDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 说明
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 单价
     */
    public Double price_unit;

    @JsonIgnore
    public boolean price_unitDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 计量单位
     */
    public Integer product_uom_id;

    @JsonIgnore
    public boolean product_uom_idDirtyFlag;
    
    /**
     * 计量单位
     */
    public String product_uom_id_text;

    @JsonIgnore
    public boolean product_uom_id_textDirtyFlag;
    
    /**
     * 数量
     */
    public Double product_uom_qty;

    @JsonIgnore
    public boolean product_uom_qtyDirtyFlag;
    
    /**
     * 报价单模板参考
     */
    public Integer sale_order_template_id;

    @JsonIgnore
    public boolean sale_order_template_idDirtyFlag;
    
    /**
     * 报价单模板参考
     */
    public String sale_order_template_id_text;

    @JsonIgnore
    public boolean sale_order_template_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [折扣(%)]
     */
    @JsonProperty("discount")
    public Double getDiscount(){
        return this.discount ;
    }

    /**
     * 设置 [折扣(%)]
     */
    @JsonProperty("discount")
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.discountDirtyFlag = true ;
    }

     /**
     * 获取 [折扣(%)]脏标记
     */
    @JsonIgnore
    public boolean getDiscountDirtyFlag(){
        return this.discountDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [显示类型]
     */
    @JsonProperty("display_type")
    public String getDisplay_type(){
        return this.display_type ;
    }

    /**
     * 设置 [显示类型]
     */
    @JsonProperty("display_type")
    public void setDisplay_type(String  display_type){
        this.display_type = display_type ;
        this.display_typeDirtyFlag = true ;
    }

     /**
     * 获取 [显示类型]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_typeDirtyFlag(){
        return this.display_typeDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [单价]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return this.price_unit ;
    }

    /**
     * 设置 [单价]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

     /**
     * 获取 [单价]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return this.price_unitDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return this.product_uom_id ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return this.product_uom_idDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return this.product_uom_id_text ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return this.product_uom_id_textDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return this.product_uom_qty ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return this.product_uom_qtyDirtyFlag ;
    }   

    /**
     * 获取 [报价单模板参考]
     */
    @JsonProperty("sale_order_template_id")
    public Integer getSale_order_template_id(){
        return this.sale_order_template_id ;
    }

    /**
     * 设置 [报价单模板参考]
     */
    @JsonProperty("sale_order_template_id")
    public void setSale_order_template_id(Integer  sale_order_template_id){
        this.sale_order_template_id = sale_order_template_id ;
        this.sale_order_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [报价单模板参考]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_idDirtyFlag(){
        return this.sale_order_template_idDirtyFlag ;
    }   

    /**
     * 获取 [报价单模板参考]
     */
    @JsonProperty("sale_order_template_id_text")
    public String getSale_order_template_id_text(){
        return this.sale_order_template_id_text ;
    }

    /**
     * 设置 [报价单模板参考]
     */
    @JsonProperty("sale_order_template_id_text")
    public void setSale_order_template_id_text(String  sale_order_template_id_text){
        this.sale_order_template_id_text = sale_order_template_id_text ;
        this.sale_order_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [报价单模板参考]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_id_textDirtyFlag(){
        return this.sale_order_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("discount") instanceof Boolean)&& map.get("discount")!=null){
			this.setDiscount((Double)map.get("discount"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("display_type") instanceof Boolean)&& map.get("display_type")!=null){
			this.setDisplay_type((String)map.get("display_type"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("price_unit") instanceof Boolean)&& map.get("price_unit")!=null){
			this.setPrice_unit((Double)map.get("price_unit"));
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 0){
				this.setProduct_uom_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_id") instanceof Boolean)&& map.get("product_uom_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_uom_id");
			if(objs.length > 1){
				this.setProduct_uom_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_uom_qty") instanceof Boolean)&& map.get("product_uom_qty")!=null){
			this.setProduct_uom_qty((Double)map.get("product_uom_qty"));
		}
		if(!(map.get("sale_order_template_id") instanceof Boolean)&& map.get("sale_order_template_id")!=null){
			Object[] objs = (Object[])map.get("sale_order_template_id");
			if(objs.length > 0){
				this.setSale_order_template_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sale_order_template_id") instanceof Boolean)&& map.get("sale_order_template_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("sale_order_template_id");
			if(objs.length > 1){
				this.setSale_order_template_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDiscount()!=null&&this.getDiscountDirtyFlag()){
			map.put("discount",this.getDiscount());
		}else if(this.getDiscountDirtyFlag()){
			map.put("discount",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getDisplay_type()!=null&&this.getDisplay_typeDirtyFlag()){
			map.put("display_type",this.getDisplay_type());
		}else if(this.getDisplay_typeDirtyFlag()){
			map.put("display_type",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPrice_unit()!=null&&this.getPrice_unitDirtyFlag()){
			map.put("price_unit",this.getPrice_unit());
		}else if(this.getPrice_unitDirtyFlag()){
			map.put("price_unit",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_uom_id()!=null&&this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",this.getProduct_uom_id());
		}else if(this.getProduct_uom_idDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getProduct_uom_id_text()!=null&&this.getProduct_uom_id_textDirtyFlag()){
			//忽略文本外键product_uom_id_text
		}else if(this.getProduct_uom_id_textDirtyFlag()){
			map.put("product_uom_id",false);
		}
		if(this.getProduct_uom_qty()!=null&&this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",this.getProduct_uom_qty());
		}else if(this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",false);
		}
		if(this.getSale_order_template_id()!=null&&this.getSale_order_template_idDirtyFlag()){
			map.put("sale_order_template_id",this.getSale_order_template_id());
		}else if(this.getSale_order_template_idDirtyFlag()){
			map.put("sale_order_template_id",false);
		}
		if(this.getSale_order_template_id_text()!=null&&this.getSale_order_template_id_textDirtyFlag()){
			//忽略文本外键sale_order_template_id_text
		}else if(this.getSale_order_template_id_textDirtyFlag()){
			map.put("sale_order_template_id",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
