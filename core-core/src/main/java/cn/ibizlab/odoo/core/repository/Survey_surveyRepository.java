package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Survey_survey;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_surveySearchContext;

/**
 * 实体 [问卷] 存储对象
 */
public interface Survey_surveyRepository extends Repository<Survey_survey> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Survey_survey> searchDefault(Survey_surveySearchContext context);

    Survey_survey convert2PO(cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey domain , Survey_survey po) ;

    cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey convert2Domain( Survey_survey po ,cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey domain) ;

}
