package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Im_livechat_channel_rule;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;

/**
 * 实体 [实时聊天频道规则] 存储对象
 */
public interface Im_livechat_channel_ruleRepository extends Repository<Im_livechat_channel_rule> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Im_livechat_channel_rule> searchDefault(Im_livechat_channel_ruleSearchContext context);

    Im_livechat_channel_rule convert2PO(cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule domain , Im_livechat_channel_rule po) ;

    cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule convert2Domain( Im_livechat_channel_rule po ,cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule domain) ;

}
