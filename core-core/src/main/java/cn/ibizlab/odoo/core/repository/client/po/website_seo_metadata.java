package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [website_seo_metadata] 对象
 */
public interface website_seo_metadata {

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_seo_optimized();

    public void setIs_seo_optimized(String is_seo_optimized);

    public String getWebsite_meta_description();

    public void setWebsite_meta_description(String website_meta_description);

    public String getWebsite_meta_keywords();

    public void setWebsite_meta_keywords(String website_meta_keywords);

    public String getWebsite_meta_og_img();

    public void setWebsite_meta_og_img(String website_meta_og_img);

    public String getWebsite_meta_title();

    public void setWebsite_meta_title(String website_meta_title);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
