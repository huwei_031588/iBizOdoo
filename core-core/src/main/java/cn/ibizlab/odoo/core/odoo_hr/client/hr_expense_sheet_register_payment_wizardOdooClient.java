package cn.ibizlab.odoo.core.odoo_hr.client;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.sql.Timestamp;
import java.security.Principal;
import java.net.URL;
import java.util.*;

import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_expense_sheet_register_payment_wizardClientModel;
import cn.ibizlab.odoo.util.filter.SearchContext;
import cn.ibizlab.odoo.util.helper.OdooSearchContextHelper;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;
import cn.ibizlab.odoo.util.config.OdooClientProperties;

import com.google.common.collect.Lists;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageImpl;

@Component
public class hr_expense_sheet_register_payment_wizardOdooClient{

	@Autowired
	OdooClientProperties odooClientProperties;

	public static String URL ;
	public static String DB ;
	public static Integer USERID  ;
	public static String PASS ;

	public static XmlRpcClient client ;

	public hr_expense_sheet_register_payment_wizardOdooClient(){
		
	}

	public void initClient () {
		if(client == null) {
			URL = odooClientProperties.getUrl();
			DB = odooClientProperties.getDb();
			USERID = odooClientProperties.getUserId();
			PASS = odooClientProperties.getPass();
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			config.setEnabledForExtensions(true);
			client = new XmlRpcClient();
			try{
				config.setServerURL(new URL(String.format("%s/xmlrpc/2/object", URL)));
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
			client.setConfig(config);
		}
	}

	public void updateBatch(hr_expense_sheet_register_payment_wizardClientModel hr_expense_sheet_register_payment_wizard){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void update(hr_expense_sheet_register_payment_wizardClientModel hr_expense_sheet_register_payment_wizard){
		initClient();
		try{
			Integer id =hr_expense_sheet_register_payment_wizard.getId();
			Map map = hr_expense_sheet_register_payment_wizard.toMap();
			client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "hr.expense.sheet.register.payment.wizard", "write",Arrays.asList(Arrays.asList(id),map)));
			List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "hr.expense.sheet.register.payment.wizard", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", id)
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				hr_expense_sheet_register_payment_wizard.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void removeBatch(hr_expense_sheet_register_payment_wizardClientModel hr_expense_sheet_register_payment_wizard){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void create(hr_expense_sheet_register_payment_wizardClientModel hr_expense_sheet_register_payment_wizard){
		initClient();
		try{
			Map map = hr_expense_sheet_register_payment_wizard.toMap();
			Integer id = (Integer) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "hr.expense.sheet.register.payment.wizard", "create",Arrays.asList(map)));
			List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "hr.expense.sheet.register.payment.wizard", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", id)
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				hr_expense_sheet_register_payment_wizard.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

    public Page<hr_expense_sheet_register_payment_wizardClientModel> search(SearchContext context) {
		initClient();
		List<hr_expense_sheet_register_payment_wizardClientModel> hr_expense_sheet_register_payment_wizardList = new ArrayList<>();
		long totalnum = 0;
		try{
			totalnum = (Integer)client.execute(
				"execute_kw", Arrays.asList(
   						DB, USERID, PASS, "hr.expense.sheet.register.payment.wizard", "search_count",
    					OdooSearchContextHelper.getConditions(context)
			));
			if(totalnum<1)
				return new PageImpl(hr_expense_sheet_register_payment_wizardList,context.getPageable(),totalnum);
			List<Object> maps = Arrays.asList((Object[]) client.execute(	
				"execute_kw", Arrays.asList(
						DB, USERID, PASS, "hr.expense.sheet.register.payment.wizard", "search_read",
						OdooSearchContextHelper.getConditions(context),
						new HashMap() {
							{				
								put("order",OdooSearchContextHelper.getSorts(context));	
								put("offset",(int)context.getPageable().getOffset());
								put("limit", context.getPageable().getPageSize());
							}
			})));
			hr_expense_sheet_register_payment_wizardList = mapsToObjects(maps);
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		return new PageImpl(hr_expense_sheet_register_payment_wizardList,context.getPageable(),totalnum);
	}
	public void remove(hr_expense_sheet_register_payment_wizardClientModel hr_expense_sheet_register_payment_wizard){
		initClient();
		try{
			Integer id = hr_expense_sheet_register_payment_wizard.getId();
			client.execute("execute_kw", Arrays.asList(
				DB, USERID, PASS,
				"hr.expense.sheet.register.payment.wizard", "unlink",
				Arrays.asList(Arrays.asList(id))));
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void createBatch(hr_expense_sheet_register_payment_wizardClientModel hr_expense_sheet_register_payment_wizard){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void get(hr_expense_sheet_register_payment_wizardClientModel hr_expense_sheet_register_payment_wizard){
		initClient();
		try{
		 	List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "hr.expense.sheet.register.payment.wizard", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", hr_expense_sheet_register_payment_wizard.getId())
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				hr_expense_sheet_register_payment_wizard.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

    public List<hr_expense_sheet_register_payment_wizardClientModel> select() {
			return null;
	}

	private List<hr_expense_sheet_register_payment_wizardClientModel> mapsToObjects(List<Object> maps) throws Exception {
		List<hr_expense_sheet_register_payment_wizardClientModel> list = Lists.newArrayList();
		if (maps != null && maps.size() > 0) {
			Map<String, Object> map = null;
			for (int i = 0,size = maps.size(); i < size; i++) {
				map = (Map<String, Object>)maps.get(i);
				hr_expense_sheet_register_payment_wizardClientModel hr_expense_sheet_register_payment_wizard = new hr_expense_sheet_register_payment_wizardClientModel();
				hr_expense_sheet_register_payment_wizard.fromMap(map);
				list.add(hr_expense_sheet_register_payment_wizard);
			}
		}
		return list;
	}

	public void preAction (Principal principal,OdooClientHelper.CurOdooUser curUser) {
		String curUserName = principal.getName();
		int curUserId = 0;
		String dyncPassWord = OdooClientHelper.getCurUserdynaPass(curUserName,PASS);
		try{
			final XmlRpcClientConfigImpl common_config = new XmlRpcClientConfigImpl();
			common_config.setServerURL(
				new URL(String.format("%s/xmlrpc/2/common", URL)));
			//获取用户id
			curUserId = (int)client.execute(
				common_config, "authenticate", Arrays.asList(
						DB, curUserName, dyncPassWord, new HashMap()));
		}catch(Exception e){
			System.out.println("用户校验失败，可能无用户或密码错误"+ e.getMessage());
		}

		if(curUserId == 0) {
			try {
				//修改动态密码
				client.execute("execute_kw", Arrays.asList(
						DB, USERID, PASS,
						"res.users", "write",
						Arrays.asList(Arrays.asList(6),
							new HashMap() {{
								put("name", curUserName);
								put("new_password", dyncPassWord);
							}}
						)
				));
			} catch (Exception e) {
				System.out.println("用户密码重置，可能无用户" + e.getMessage());
			}
		}
		curUser.setUserId(curUserId);
		curUser.setPass(dyncPassWord);
	}

}
