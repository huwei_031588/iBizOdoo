package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_routing_workcenter;

/**
 * 实体[mrp_routing_workcenter] 服务对象接口
 */
public interface mrp_routing_workcenterRepository{


    public mrp_routing_workcenter createPO() ;
        public void createBatch(mrp_routing_workcenter mrp_routing_workcenter);

        public void remove(String id);

        public List<mrp_routing_workcenter> search();

        public void get(String id);

        public void updateBatch(mrp_routing_workcenter mrp_routing_workcenter);

        public void removeBatch(String id);

        public void update(mrp_routing_workcenter mrp_routing_workcenter);

        public void create(mrp_routing_workcenter mrp_routing_workcenter);


}
