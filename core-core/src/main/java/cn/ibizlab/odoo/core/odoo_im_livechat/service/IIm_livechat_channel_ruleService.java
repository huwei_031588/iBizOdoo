package cn.ibizlab.odoo.core.odoo_im_livechat.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;


/**
 * 实体[Im_livechat_channel_rule] 服务对象接口
 */
public interface IIm_livechat_channel_ruleService{

    Im_livechat_channel_rule get(Integer key) ;
    boolean update(Im_livechat_channel_rule et) ;
    void updateBatch(List<Im_livechat_channel_rule> list) ;
    boolean create(Im_livechat_channel_rule et) ;
    void createBatch(List<Im_livechat_channel_rule> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Im_livechat_channel_rule> searchDefault(Im_livechat_channel_ruleSearchContext context) ;

}



