package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [邮件统计] 对象
 */
@Data
public class Mail_mail_statistics extends EntityClient implements Serializable {

    /**
     * 安排
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled")
    private Timestamp scheduled;

    /**
     * 点击率
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "clicked" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("clicked")
    private Timestamp clicked;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 收件人email地址
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 状态更新
     */
    @DEField(name = "state_update")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "state_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("state_update")
    private Timestamp stateUpdate;

    /**
     * 已回复
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "replied" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("replied")
    private Timestamp replied;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 文档模型
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 消息ID
     */
    @DEField(name = "message_id")
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private String messageId;

    /**
     * 异常
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "exception" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("exception")
    private Timestamp exception;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 被退回
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "bounced" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bounced")
    private Timestamp bounced;

    /**
     * 忽略
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "ignored" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("ignored")
    private Timestamp ignored;

    /**
     * 文档ID
     */
    @DEField(name = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 已汇
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sent" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sent")
    private Timestamp sent;

    /**
     * 邮件ID（技术）
     */
    @DEField(name = "mail_mail_id_int")
    @JSONField(name = "mail_mail_id_int")
    @JsonProperty("mail_mail_id_int")
    private Integer mailMailIdInt;

    /**
     * 点击链接
     */
    @JSONField(name = "links_click_ids")
    @JsonProperty("links_click_ids")
    private String linksClickIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 已开启
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "opened" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("opened")
    private Timestamp opened;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 群发邮件营销
     */
    @JSONField(name = "mass_mailing_campaign_id_text")
    @JsonProperty("mass_mailing_campaign_id_text")
    private String massMailingCampaignIdText;

    /**
     * 群发邮件
     */
    @JSONField(name = "mass_mailing_id_text")
    @JsonProperty("mass_mailing_id_text")
    private String massMailingIdText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 群发邮件营销
     */
    @DEField(name = "mass_mailing_campaign_id")
    @JSONField(name = "mass_mailing_campaign_id")
    @JsonProperty("mass_mailing_campaign_id")
    private Integer massMailingCampaignId;

    /**
     * 邮件
     */
    @DEField(name = "mail_mail_id")
    @JSONField(name = "mail_mail_id")
    @JsonProperty("mail_mail_id")
    private Integer mailMailId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 群发邮件
     */
    @DEField(name = "mass_mailing_id")
    @JSONField(name = "mass_mailing_id")
    @JsonProperty("mass_mailing_id")
    private Integer massMailingId;


    /**
     * 
     */
    @JSONField(name = "odoomailmail")
    @JsonProperty("odoomailmail")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail odooMailMail;

    /**
     * 
     */
    @JSONField(name = "odoomassmailingcampaign")
    @JsonProperty("odoomassmailingcampaign")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign odooMassMailingCampaign;

    /**
     * 
     */
    @JSONField(name = "odoomassmailing")
    @JsonProperty("odoomassmailing")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing odooMassMailing;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [安排]
     */
    public void setScheduled(Timestamp scheduled){
        this.scheduled = scheduled ;
        this.modify("scheduled",scheduled);
    }
    /**
     * 设置 [点击率]
     */
    public void setClicked(Timestamp clicked){
        this.clicked = clicked ;
        this.modify("clicked",clicked);
    }
    /**
     * 设置 [收件人email地址]
     */
    public void setEmail(String email){
        this.email = email ;
        this.modify("email",email);
    }
    /**
     * 设置 [状态更新]
     */
    public void setStateUpdate(Timestamp stateUpdate){
        this.stateUpdate = stateUpdate ;
        this.modify("state_update",stateUpdate);
    }
    /**
     * 设置 [已回复]
     */
    public void setReplied(Timestamp replied){
        this.replied = replied ;
        this.modify("replied",replied);
    }
    /**
     * 设置 [文档模型]
     */
    public void setModel(String model){
        this.model = model ;
        this.modify("model",model);
    }
    /**
     * 设置 [消息ID]
     */
    public void setMessageId(String messageId){
        this.messageId = messageId ;
        this.modify("message_id",messageId);
    }
    /**
     * 设置 [异常]
     */
    public void setException(Timestamp exception){
        this.exception = exception ;
        this.modify("exception",exception);
    }
    /**
     * 设置 [被退回]
     */
    public void setBounced(Timestamp bounced){
        this.bounced = bounced ;
        this.modify("bounced",bounced);
    }
    /**
     * 设置 [忽略]
     */
    public void setIgnored(Timestamp ignored){
        this.ignored = ignored ;
        this.modify("ignored",ignored);
    }
    /**
     * 设置 [文档ID]
     */
    public void setResId(Integer resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }
    /**
     * 设置 [已汇]
     */
    public void setSent(Timestamp sent){
        this.sent = sent ;
        this.modify("sent",sent);
    }
    /**
     * 设置 [邮件ID（技术）]
     */
    public void setMailMailIdInt(Integer mailMailIdInt){
        this.mailMailIdInt = mailMailIdInt ;
        this.modify("mail_mail_id_int",mailMailIdInt);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [已开启]
     */
    public void setOpened(Timestamp opened){
        this.opened = opened ;
        this.modify("opened",opened);
    }
    /**
     * 设置 [群发邮件营销]
     */
    public void setMassMailingCampaignId(Integer massMailingCampaignId){
        this.massMailingCampaignId = massMailingCampaignId ;
        this.modify("mass_mailing_campaign_id",massMailingCampaignId);
    }
    /**
     * 设置 [邮件]
     */
    public void setMailMailId(Integer mailMailId){
        this.mailMailId = mailMailId ;
        this.modify("mail_mail_id",mailMailId);
    }
    /**
     * 设置 [群发邮件]
     */
    public void setMassMailingId(Integer massMailingId){
        this.massMailingId = massMailingId ;
        this.modify("mass_mailing_id",massMailingId);
    }

}


