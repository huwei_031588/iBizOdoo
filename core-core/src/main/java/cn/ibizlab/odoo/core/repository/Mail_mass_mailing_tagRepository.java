package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_mass_mailing_tag;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_tagSearchContext;

/**
 * 实体 [群发邮件标签] 存储对象
 */
public interface Mail_mass_mailing_tagRepository extends Repository<Mail_mass_mailing_tag> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_mass_mailing_tag> searchDefault(Mail_mass_mailing_tagSearchContext context);

    Mail_mass_mailing_tag convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_tag domain , Mail_mass_mailing_tag po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_tag convert2Domain( Mail_mass_mailing_tag po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_tag domain) ;

}
