package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_reversal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_reversalSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_move_reversalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_move_reversalOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_move_reversalClientModel;

/**
 * 实体[会计凭证逆转] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_move_reversalServiceImpl implements IAccount_move_reversalService {

    @Autowired
    account_move_reversalOdooClient account_move_reversalOdooClient;


    @Override
    public Account_move_reversal get(Integer id) {
        account_move_reversalClientModel clientModel = new account_move_reversalClientModel();
        clientModel.setId(id);
		account_move_reversalOdooClient.get(clientModel);
        Account_move_reversal et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_move_reversal();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Account_move_reversal et) {
        account_move_reversalClientModel clientModel = convert2Model(et,null);
		account_move_reversalOdooClient.update(clientModel);
        Account_move_reversal rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_move_reversal> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_move_reversalClientModel clientModel = new account_move_reversalClientModel();
        clientModel.setId(id);
		account_move_reversalOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_move_reversal et) {
        account_move_reversalClientModel clientModel = convert2Model(et,null);
		account_move_reversalOdooClient.create(clientModel);
        Account_move_reversal rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_move_reversal> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_move_reversal> searchDefault(Account_move_reversalSearchContext context) {
        List<Account_move_reversal> list = new ArrayList<Account_move_reversal>();
        Page<account_move_reversalClientModel> clientModelList = account_move_reversalOdooClient.search(context);
        for(account_move_reversalClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_move_reversal>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_move_reversalClientModel convert2Model(Account_move_reversal domain , account_move_reversalClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_move_reversalClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_move_reversal convert2Domain( account_move_reversalClientModel model ,Account_move_reversal domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_move_reversal();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



