package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model_brand;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_model_brandSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_model_brandService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_model_brandOdooClient;
import cn.ibizlab.odoo.core.odoo_fleet.clientmodel.fleet_vehicle_model_brandClientModel;

/**
 * 实体[车辆品牌] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_model_brandServiceImpl implements IFleet_vehicle_model_brandService {

    @Autowired
    fleet_vehicle_model_brandOdooClient fleet_vehicle_model_brandOdooClient;


    @Override
    public boolean create(Fleet_vehicle_model_brand et) {
        fleet_vehicle_model_brandClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_model_brandOdooClient.create(clientModel);
        Fleet_vehicle_model_brand rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_model_brand> list){
    }

    @Override
    public boolean update(Fleet_vehicle_model_brand et) {
        fleet_vehicle_model_brandClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_model_brandOdooClient.update(clientModel);
        Fleet_vehicle_model_brand rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Fleet_vehicle_model_brand> list){
    }

    @Override
    public Fleet_vehicle_model_brand get(Integer id) {
        fleet_vehicle_model_brandClientModel clientModel = new fleet_vehicle_model_brandClientModel();
        clientModel.setId(id);
		fleet_vehicle_model_brandOdooClient.get(clientModel);
        Fleet_vehicle_model_brand et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Fleet_vehicle_model_brand();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        fleet_vehicle_model_brandClientModel clientModel = new fleet_vehicle_model_brandClientModel();
        clientModel.setId(id);
		fleet_vehicle_model_brandOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_model_brand> searchDefault(Fleet_vehicle_model_brandSearchContext context) {
        List<Fleet_vehicle_model_brand> list = new ArrayList<Fleet_vehicle_model_brand>();
        Page<fleet_vehicle_model_brandClientModel> clientModelList = fleet_vehicle_model_brandOdooClient.search(context);
        for(fleet_vehicle_model_brandClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Fleet_vehicle_model_brand>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public fleet_vehicle_model_brandClientModel convert2Model(Fleet_vehicle_model_brand domain , fleet_vehicle_model_brandClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new fleet_vehicle_model_brandClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Fleet_vehicle_model_brand convert2Domain( fleet_vehicle_model_brandClientModel model ,Fleet_vehicle_model_brand domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Fleet_vehicle_model_brand();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



