package cn.ibizlab.odoo.core.odoo_gamification.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[gamification_badge] 对象
 */
public class gamification_badgeClientModel implements Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 挑战的奖励
     */
    public String challenge_ids;

    @JsonIgnore
    public boolean challenge_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 奖励按照
     */
    public String goal_definition_ids;

    @JsonIgnore
    public boolean goal_definition_idsDirtyFlag;
    
    /**
     * 授予的员工人数
     */
    public Integer granted_employees_count;

    @JsonIgnore
    public boolean granted_employees_countDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 图像
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 论坛徽章等级
     */
    public String level;

    @JsonIgnore
    public boolean levelDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要激活
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 徽章
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 所有者
     */
    public String owner_ids;

    @JsonIgnore
    public boolean owner_idsDirtyFlag;
    
    /**
     * 其他的允许发送
     */
    public Integer remaining_sending;

    @JsonIgnore
    public boolean remaining_sendingDirtyFlag;
    
    /**
     * 允许授予
     */
    public String rule_auth;

    @JsonIgnore
    public boolean rule_authDirtyFlag;
    
    /**
     * 需要徽章
     */
    public String rule_auth_badge_ids;

    @JsonIgnore
    public boolean rule_auth_badge_idsDirtyFlag;
    
    /**
     * 授权用户
     */
    public String rule_auth_user_ids;

    @JsonIgnore
    public boolean rule_auth_user_idsDirtyFlag;
    
    /**
     * 月度限额发放
     */
    public String rule_max;

    @JsonIgnore
    public boolean rule_maxDirtyFlag;
    
    /**
     * 限制数量
     */
    public Integer rule_max_number;

    @JsonIgnore
    public boolean rule_max_numberDirtyFlag;
    
    /**
     * 总计
     */
    public Integer stat_count;

    @JsonIgnore
    public boolean stat_countDirtyFlag;
    
    /**
     * 用户数量
     */
    public Integer stat_count_distinct;

    @JsonIgnore
    public boolean stat_count_distinctDirtyFlag;
    
    /**
     * 我的总计
     */
    public Integer stat_my;

    @JsonIgnore
    public boolean stat_myDirtyFlag;
    
    /**
     * 月度发放总数
     */
    public Integer stat_my_monthly_sending;

    @JsonIgnore
    public boolean stat_my_monthly_sendingDirtyFlag;
    
    /**
     * 我的月份总计
     */
    public Integer stat_my_this_month;

    @JsonIgnore
    public boolean stat_my_this_monthDirtyFlag;
    
    /**
     * 每月总数
     */
    public Integer stat_this_month;

    @JsonIgnore
    public boolean stat_this_monthDirtyFlag;
    
    /**
     * 唯一的所有者
     */
    public String unique_owner_ids;

    @JsonIgnore
    public boolean unique_owner_idsDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [挑战的奖励]
     */
    @JsonProperty("challenge_ids")
    public String getChallenge_ids(){
        return this.challenge_ids ;
    }

    /**
     * 设置 [挑战的奖励]
     */
    @JsonProperty("challenge_ids")
    public void setChallenge_ids(String  challenge_ids){
        this.challenge_ids = challenge_ids ;
        this.challenge_idsDirtyFlag = true ;
    }

     /**
     * 获取 [挑战的奖励]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_idsDirtyFlag(){
        return this.challenge_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [奖励按照]
     */
    @JsonProperty("goal_definition_ids")
    public String getGoal_definition_ids(){
        return this.goal_definition_ids ;
    }

    /**
     * 设置 [奖励按照]
     */
    @JsonProperty("goal_definition_ids")
    public void setGoal_definition_ids(String  goal_definition_ids){
        this.goal_definition_ids = goal_definition_ids ;
        this.goal_definition_idsDirtyFlag = true ;
    }

     /**
     * 获取 [奖励按照]脏标记
     */
    @JsonIgnore
    public boolean getGoal_definition_idsDirtyFlag(){
        return this.goal_definition_idsDirtyFlag ;
    }   

    /**
     * 获取 [授予的员工人数]
     */
    @JsonProperty("granted_employees_count")
    public Integer getGranted_employees_count(){
        return this.granted_employees_count ;
    }

    /**
     * 设置 [授予的员工人数]
     */
    @JsonProperty("granted_employees_count")
    public void setGranted_employees_count(Integer  granted_employees_count){
        this.granted_employees_count = granted_employees_count ;
        this.granted_employees_countDirtyFlag = true ;
    }

     /**
     * 获取 [授予的员工人数]脏标记
     */
    @JsonIgnore
    public boolean getGranted_employees_countDirtyFlag(){
        return this.granted_employees_countDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [图像]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [论坛徽章等级]
     */
    @JsonProperty("level")
    public String getLevel(){
        return this.level ;
    }

    /**
     * 设置 [论坛徽章等级]
     */
    @JsonProperty("level")
    public void setLevel(String  level){
        this.level = level ;
        this.levelDirtyFlag = true ;
    }

     /**
     * 获取 [论坛徽章等级]脏标记
     */
    @JsonIgnore
    public boolean getLevelDirtyFlag(){
        return this.levelDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要激活]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要激活]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要激活]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [徽章]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [徽章]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [徽章]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_ids")
    public String getOwner_ids(){
        return this.owner_ids ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_ids")
    public void setOwner_ids(String  owner_ids){
        this.owner_ids = owner_ids ;
        this.owner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_idsDirtyFlag(){
        return this.owner_idsDirtyFlag ;
    }   

    /**
     * 获取 [其他的允许发送]
     */
    @JsonProperty("remaining_sending")
    public Integer getRemaining_sending(){
        return this.remaining_sending ;
    }

    /**
     * 设置 [其他的允许发送]
     */
    @JsonProperty("remaining_sending")
    public void setRemaining_sending(Integer  remaining_sending){
        this.remaining_sending = remaining_sending ;
        this.remaining_sendingDirtyFlag = true ;
    }

     /**
     * 获取 [其他的允许发送]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_sendingDirtyFlag(){
        return this.remaining_sendingDirtyFlag ;
    }   

    /**
     * 获取 [允许授予]
     */
    @JsonProperty("rule_auth")
    public String getRule_auth(){
        return this.rule_auth ;
    }

    /**
     * 设置 [允许授予]
     */
    @JsonProperty("rule_auth")
    public void setRule_auth(String  rule_auth){
        this.rule_auth = rule_auth ;
        this.rule_authDirtyFlag = true ;
    }

     /**
     * 获取 [允许授予]脏标记
     */
    @JsonIgnore
    public boolean getRule_authDirtyFlag(){
        return this.rule_authDirtyFlag ;
    }   

    /**
     * 获取 [需要徽章]
     */
    @JsonProperty("rule_auth_badge_ids")
    public String getRule_auth_badge_ids(){
        return this.rule_auth_badge_ids ;
    }

    /**
     * 设置 [需要徽章]
     */
    @JsonProperty("rule_auth_badge_ids")
    public void setRule_auth_badge_ids(String  rule_auth_badge_ids){
        this.rule_auth_badge_ids = rule_auth_badge_ids ;
        this.rule_auth_badge_idsDirtyFlag = true ;
    }

     /**
     * 获取 [需要徽章]脏标记
     */
    @JsonIgnore
    public boolean getRule_auth_badge_idsDirtyFlag(){
        return this.rule_auth_badge_idsDirtyFlag ;
    }   

    /**
     * 获取 [授权用户]
     */
    @JsonProperty("rule_auth_user_ids")
    public String getRule_auth_user_ids(){
        return this.rule_auth_user_ids ;
    }

    /**
     * 设置 [授权用户]
     */
    @JsonProperty("rule_auth_user_ids")
    public void setRule_auth_user_ids(String  rule_auth_user_ids){
        this.rule_auth_user_ids = rule_auth_user_ids ;
        this.rule_auth_user_idsDirtyFlag = true ;
    }

     /**
     * 获取 [授权用户]脏标记
     */
    @JsonIgnore
    public boolean getRule_auth_user_idsDirtyFlag(){
        return this.rule_auth_user_idsDirtyFlag ;
    }   

    /**
     * 获取 [月度限额发放]
     */
    @JsonProperty("rule_max")
    public String getRule_max(){
        return this.rule_max ;
    }

    /**
     * 设置 [月度限额发放]
     */
    @JsonProperty("rule_max")
    public void setRule_max(String  rule_max){
        this.rule_max = rule_max ;
        this.rule_maxDirtyFlag = true ;
    }

     /**
     * 获取 [月度限额发放]脏标记
     */
    @JsonIgnore
    public boolean getRule_maxDirtyFlag(){
        return this.rule_maxDirtyFlag ;
    }   

    /**
     * 获取 [限制数量]
     */
    @JsonProperty("rule_max_number")
    public Integer getRule_max_number(){
        return this.rule_max_number ;
    }

    /**
     * 设置 [限制数量]
     */
    @JsonProperty("rule_max_number")
    public void setRule_max_number(Integer  rule_max_number){
        this.rule_max_number = rule_max_number ;
        this.rule_max_numberDirtyFlag = true ;
    }

     /**
     * 获取 [限制数量]脏标记
     */
    @JsonIgnore
    public boolean getRule_max_numberDirtyFlag(){
        return this.rule_max_numberDirtyFlag ;
    }   

    /**
     * 获取 [总计]
     */
    @JsonProperty("stat_count")
    public Integer getStat_count(){
        return this.stat_count ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("stat_count")
    public void setStat_count(Integer  stat_count){
        this.stat_count = stat_count ;
        this.stat_countDirtyFlag = true ;
    }

     /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getStat_countDirtyFlag(){
        return this.stat_countDirtyFlag ;
    }   

    /**
     * 获取 [用户数量]
     */
    @JsonProperty("stat_count_distinct")
    public Integer getStat_count_distinct(){
        return this.stat_count_distinct ;
    }

    /**
     * 设置 [用户数量]
     */
    @JsonProperty("stat_count_distinct")
    public void setStat_count_distinct(Integer  stat_count_distinct){
        this.stat_count_distinct = stat_count_distinct ;
        this.stat_count_distinctDirtyFlag = true ;
    }

     /**
     * 获取 [用户数量]脏标记
     */
    @JsonIgnore
    public boolean getStat_count_distinctDirtyFlag(){
        return this.stat_count_distinctDirtyFlag ;
    }   

    /**
     * 获取 [我的总计]
     */
    @JsonProperty("stat_my")
    public Integer getStat_my(){
        return this.stat_my ;
    }

    /**
     * 设置 [我的总计]
     */
    @JsonProperty("stat_my")
    public void setStat_my(Integer  stat_my){
        this.stat_my = stat_my ;
        this.stat_myDirtyFlag = true ;
    }

     /**
     * 获取 [我的总计]脏标记
     */
    @JsonIgnore
    public boolean getStat_myDirtyFlag(){
        return this.stat_myDirtyFlag ;
    }   

    /**
     * 获取 [月度发放总数]
     */
    @JsonProperty("stat_my_monthly_sending")
    public Integer getStat_my_monthly_sending(){
        return this.stat_my_monthly_sending ;
    }

    /**
     * 设置 [月度发放总数]
     */
    @JsonProperty("stat_my_monthly_sending")
    public void setStat_my_monthly_sending(Integer  stat_my_monthly_sending){
        this.stat_my_monthly_sending = stat_my_monthly_sending ;
        this.stat_my_monthly_sendingDirtyFlag = true ;
    }

     /**
     * 获取 [月度发放总数]脏标记
     */
    @JsonIgnore
    public boolean getStat_my_monthly_sendingDirtyFlag(){
        return this.stat_my_monthly_sendingDirtyFlag ;
    }   

    /**
     * 获取 [我的月份总计]
     */
    @JsonProperty("stat_my_this_month")
    public Integer getStat_my_this_month(){
        return this.stat_my_this_month ;
    }

    /**
     * 设置 [我的月份总计]
     */
    @JsonProperty("stat_my_this_month")
    public void setStat_my_this_month(Integer  stat_my_this_month){
        this.stat_my_this_month = stat_my_this_month ;
        this.stat_my_this_monthDirtyFlag = true ;
    }

     /**
     * 获取 [我的月份总计]脏标记
     */
    @JsonIgnore
    public boolean getStat_my_this_monthDirtyFlag(){
        return this.stat_my_this_monthDirtyFlag ;
    }   

    /**
     * 获取 [每月总数]
     */
    @JsonProperty("stat_this_month")
    public Integer getStat_this_month(){
        return this.stat_this_month ;
    }

    /**
     * 设置 [每月总数]
     */
    @JsonProperty("stat_this_month")
    public void setStat_this_month(Integer  stat_this_month){
        this.stat_this_month = stat_this_month ;
        this.stat_this_monthDirtyFlag = true ;
    }

     /**
     * 获取 [每月总数]脏标记
     */
    @JsonIgnore
    public boolean getStat_this_monthDirtyFlag(){
        return this.stat_this_monthDirtyFlag ;
    }   

    /**
     * 获取 [唯一的所有者]
     */
    @JsonProperty("unique_owner_ids")
    public String getUnique_owner_ids(){
        return this.unique_owner_ids ;
    }

    /**
     * 设置 [唯一的所有者]
     */
    @JsonProperty("unique_owner_ids")
    public void setUnique_owner_ids(String  unique_owner_ids){
        this.unique_owner_ids = unique_owner_ids ;
        this.unique_owner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [唯一的所有者]脏标记
     */
    @JsonIgnore
    public boolean getUnique_owner_idsDirtyFlag(){
        return this.unique_owner_idsDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("challenge_ids") instanceof Boolean)&& map.get("challenge_ids")!=null){
			Object[] objs = (Object[])map.get("challenge_ids");
			if(objs.length > 0){
				Integer[] challenge_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChallenge_ids(Arrays.toString(challenge_ids).replace(" ",""));
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("goal_definition_ids") instanceof Boolean)&& map.get("goal_definition_ids")!=null){
			Object[] objs = (Object[])map.get("goal_definition_ids");
			if(objs.length > 0){
				Integer[] goal_definition_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setGoal_definition_ids(Arrays.toString(goal_definition_ids).replace(" ",""));
			}
		}
		if(!(map.get("granted_employees_count") instanceof Boolean)&& map.get("granted_employees_count")!=null){
			this.setGranted_employees_count((Integer)map.get("granted_employees_count"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("image") instanceof Boolean)&& map.get("image")!=null){
			//暂时忽略
			//this.setImage(((String)map.get("image")).getBytes("UTF-8"));
		}
		if(!(map.get("level") instanceof Boolean)&& map.get("level")!=null){
			this.setLevel((String)map.get("level"));
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids).replace(" ",""));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids).replace(" ",""));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids).replace(" ",""));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("owner_ids") instanceof Boolean)&& map.get("owner_ids")!=null){
			Object[] objs = (Object[])map.get("owner_ids");
			if(objs.length > 0){
				Integer[] owner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setOwner_ids(Arrays.toString(owner_ids).replace(" ",""));
			}
		}
		if(!(map.get("remaining_sending") instanceof Boolean)&& map.get("remaining_sending")!=null){
			this.setRemaining_sending((Integer)map.get("remaining_sending"));
		}
		if(!(map.get("rule_auth") instanceof Boolean)&& map.get("rule_auth")!=null){
			this.setRule_auth((String)map.get("rule_auth"));
		}
		if(!(map.get("rule_auth_badge_ids") instanceof Boolean)&& map.get("rule_auth_badge_ids")!=null){
			Object[] objs = (Object[])map.get("rule_auth_badge_ids");
			if(objs.length > 0){
				Integer[] rule_auth_badge_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRule_auth_badge_ids(Arrays.toString(rule_auth_badge_ids).replace(" ",""));
			}
		}
		if(!(map.get("rule_auth_user_ids") instanceof Boolean)&& map.get("rule_auth_user_ids")!=null){
			Object[] objs = (Object[])map.get("rule_auth_user_ids");
			if(objs.length > 0){
				Integer[] rule_auth_user_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRule_auth_user_ids(Arrays.toString(rule_auth_user_ids).replace(" ",""));
			}
		}
		if(map.get("rule_max") instanceof Boolean){
			this.setRule_max(((Boolean)map.get("rule_max"))? "true" : "false");
		}
		if(!(map.get("rule_max_number") instanceof Boolean)&& map.get("rule_max_number")!=null){
			this.setRule_max_number((Integer)map.get("rule_max_number"));
		}
		if(!(map.get("stat_count") instanceof Boolean)&& map.get("stat_count")!=null){
			this.setStat_count((Integer)map.get("stat_count"));
		}
		if(!(map.get("stat_count_distinct") instanceof Boolean)&& map.get("stat_count_distinct")!=null){
			this.setStat_count_distinct((Integer)map.get("stat_count_distinct"));
		}
		if(!(map.get("stat_my") instanceof Boolean)&& map.get("stat_my")!=null){
			this.setStat_my((Integer)map.get("stat_my"));
		}
		if(!(map.get("stat_my_monthly_sending") instanceof Boolean)&& map.get("stat_my_monthly_sending")!=null){
			this.setStat_my_monthly_sending((Integer)map.get("stat_my_monthly_sending"));
		}
		if(!(map.get("stat_my_this_month") instanceof Boolean)&& map.get("stat_my_this_month")!=null){
			this.setStat_my_this_month((Integer)map.get("stat_my_this_month"));
		}
		if(!(map.get("stat_this_month") instanceof Boolean)&& map.get("stat_this_month")!=null){
			this.setStat_this_month((Integer)map.get("stat_this_month"));
		}
		if(!(map.get("unique_owner_ids") instanceof Boolean)&& map.get("unique_owner_ids")!=null){
			Object[] objs = (Object[])map.get("unique_owner_ids");
			if(objs.length > 0){
				Integer[] unique_owner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setUnique_owner_ids(Arrays.toString(unique_owner_ids).replace(" ",""));
			}
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getChallenge_ids()!=null&&this.getChallenge_idsDirtyFlag()){
			map.put("challenge_ids",this.getChallenge_ids());
		}else if(this.getChallenge_idsDirtyFlag()){
			map.put("challenge_ids",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getGoal_definition_ids()!=null&&this.getGoal_definition_idsDirtyFlag()){
			map.put("goal_definition_ids",this.getGoal_definition_ids());
		}else if(this.getGoal_definition_idsDirtyFlag()){
			map.put("goal_definition_ids",false);
		}
		if(this.getGranted_employees_count()!=null&&this.getGranted_employees_countDirtyFlag()){
			map.put("granted_employees_count",this.getGranted_employees_count());
		}else if(this.getGranted_employees_countDirtyFlag()){
			map.put("granted_employees_count",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getImage()!=null&&this.getImageDirtyFlag()){
			//暂不支持binary类型image
		}else if(this.getImageDirtyFlag()){
			map.put("image",false);
		}
		if(this.getLevel()!=null&&this.getLevelDirtyFlag()){
			map.put("level",this.getLevel());
		}else if(this.getLevelDirtyFlag()){
			map.put("level",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getOwner_ids()!=null&&this.getOwner_idsDirtyFlag()){
			map.put("owner_ids",this.getOwner_ids());
		}else if(this.getOwner_idsDirtyFlag()){
			map.put("owner_ids",false);
		}
		if(this.getRemaining_sending()!=null&&this.getRemaining_sendingDirtyFlag()){
			map.put("remaining_sending",this.getRemaining_sending());
		}else if(this.getRemaining_sendingDirtyFlag()){
			map.put("remaining_sending",false);
		}
		if(this.getRule_auth()!=null&&this.getRule_authDirtyFlag()){
			map.put("rule_auth",this.getRule_auth());
		}else if(this.getRule_authDirtyFlag()){
			map.put("rule_auth",false);
		}
		if(this.getRule_auth_badge_ids()!=null&&this.getRule_auth_badge_idsDirtyFlag()){
			map.put("rule_auth_badge_ids",this.getRule_auth_badge_ids());
		}else if(this.getRule_auth_badge_idsDirtyFlag()){
			map.put("rule_auth_badge_ids",false);
		}
		if(this.getRule_auth_user_ids()!=null&&this.getRule_auth_user_idsDirtyFlag()){
			map.put("rule_auth_user_ids",this.getRule_auth_user_ids());
		}else if(this.getRule_auth_user_idsDirtyFlag()){
			map.put("rule_auth_user_ids",false);
		}
		if(this.getRule_max()!=null&&this.getRule_maxDirtyFlag()){
			map.put("rule_max",Boolean.parseBoolean(this.getRule_max()));		
		}		if(this.getRule_max_number()!=null&&this.getRule_max_numberDirtyFlag()){
			map.put("rule_max_number",this.getRule_max_number());
		}else if(this.getRule_max_numberDirtyFlag()){
			map.put("rule_max_number",false);
		}
		if(this.getStat_count()!=null&&this.getStat_countDirtyFlag()){
			map.put("stat_count",this.getStat_count());
		}else if(this.getStat_countDirtyFlag()){
			map.put("stat_count",false);
		}
		if(this.getStat_count_distinct()!=null&&this.getStat_count_distinctDirtyFlag()){
			map.put("stat_count_distinct",this.getStat_count_distinct());
		}else if(this.getStat_count_distinctDirtyFlag()){
			map.put("stat_count_distinct",false);
		}
		if(this.getStat_my()!=null&&this.getStat_myDirtyFlag()){
			map.put("stat_my",this.getStat_my());
		}else if(this.getStat_myDirtyFlag()){
			map.put("stat_my",false);
		}
		if(this.getStat_my_monthly_sending()!=null&&this.getStat_my_monthly_sendingDirtyFlag()){
			map.put("stat_my_monthly_sending",this.getStat_my_monthly_sending());
		}else if(this.getStat_my_monthly_sendingDirtyFlag()){
			map.put("stat_my_monthly_sending",false);
		}
		if(this.getStat_my_this_month()!=null&&this.getStat_my_this_monthDirtyFlag()){
			map.put("stat_my_this_month",this.getStat_my_this_month());
		}else if(this.getStat_my_this_monthDirtyFlag()){
			map.put("stat_my_this_month",false);
		}
		if(this.getStat_this_month()!=null&&this.getStat_this_monthDirtyFlag()){
			map.put("stat_this_month",this.getStat_this_month());
		}else if(this.getStat_this_monthDirtyFlag()){
			map.put("stat_this_month",false);
		}
		if(this.getUnique_owner_ids()!=null&&this.getUnique_owner_idsDirtyFlag()){
			map.put("unique_owner_ids",this.getUnique_owner_ids());
		}else if(this.getUnique_owner_idsDirtyFlag()){
			map.put("unique_owner_ids",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
