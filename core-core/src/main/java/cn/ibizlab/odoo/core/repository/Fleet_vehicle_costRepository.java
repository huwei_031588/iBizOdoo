package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_vehicle_cost;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;

/**
 * 实体 [车辆相关的费用] 存储对象
 */
public interface Fleet_vehicle_costRepository extends Repository<Fleet_vehicle_cost> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_vehicle_cost> searchDefault(Fleet_vehicle_costSearchContext context);

    Fleet_vehicle_cost convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_cost domain , Fleet_vehicle_cost po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_cost convert2Domain( Fleet_vehicle_cost po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_cost domain) ;

}
