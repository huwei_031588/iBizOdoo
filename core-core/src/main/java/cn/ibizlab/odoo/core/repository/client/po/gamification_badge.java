package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [gamification_badge] 对象
 */
public interface gamification_badge {

    public String getActive();

    public void setActive(String active);

    public String getChallenge_ids();

    public void setChallenge_ids(String challenge_ids);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getGoal_definition_ids();

    public void setGoal_definition_ids(String goal_definition_ids);

    public Integer getGranted_employees_count();

    public void setGranted_employees_count(Integer granted_employees_count);

    public Integer getId();

    public void setId(Integer id);

    public byte[] getImage();

    public void setImage(byte[] image);

    public String getLevel();

    public void setLevel(String level);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public String getOwner_ids();

    public void setOwner_ids(String owner_ids);

    public Integer getRemaining_sending();

    public void setRemaining_sending(Integer remaining_sending);

    public String getRule_auth();

    public void setRule_auth(String rule_auth);

    public String getRule_auth_badge_ids();

    public void setRule_auth_badge_ids(String rule_auth_badge_ids);

    public String getRule_auth_user_ids();

    public void setRule_auth_user_ids(String rule_auth_user_ids);

    public String getRule_max();

    public void setRule_max(String rule_max);

    public Integer getRule_max_number();

    public void setRule_max_number(Integer rule_max_number);

    public Integer getStat_count();

    public void setStat_count(Integer stat_count);

    public Integer getStat_count_distinct();

    public void setStat_count_distinct(Integer stat_count_distinct);

    public Integer getStat_my();

    public void setStat_my(Integer stat_my);

    public Integer getStat_my_monthly_sending();

    public void setStat_my_monthly_sending(Integer stat_my_monthly_sending);

    public Integer getStat_my_this_month();

    public void setStat_my_this_month(Integer stat_my_this_month);

    public Integer getStat_this_month();

    public void setStat_this_month(Integer stat_this_month);

    public String getUnique_owner_ids();

    public void setUnique_owner_ids(String unique_owner_ids);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
