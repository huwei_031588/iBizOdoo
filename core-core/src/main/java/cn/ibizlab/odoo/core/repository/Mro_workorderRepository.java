package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_workorder;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_workorderSearchContext;

/**
 * 实体 [工单] 存储对象
 */
public interface Mro_workorderRepository extends Repository<Mro_workorder> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_workorder> searchDefault(Mro_workorderSearchContext context);

    Mro_workorder convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_workorder domain , Mro_workorder po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_workorder convert2Domain( Mro_workorder po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_workorder domain) ;

}
