package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [寄出邮件] 对象
 */
@Data
public class Mail_mail extends EntityClient implements Serializable {

    /**
     * 通知
     */
    @JSONField(name = "notification")
    @JsonProperty("notification")
    private String notification;

    /**
     * 安排的发送日期
     */
    @DEField(name = "scheduled_date")
    @JSONField(name = "scheduled_date")
    @JsonProperty("scheduled_date")
    private String scheduledDate;

    /**
     * 收藏夹
     */
    @JSONField(name = "starred_partner_ids")
    @JsonProperty("starred_partner_ids")
    private String starredPartnerIds;

    /**
     * 自动删除
     */
    @DEField(name = "auto_delete")
    @JSONField(name = "auto_delete")
    @JsonProperty("auto_delete")
    private String autoDelete;

    /**
     * 富文本内容
     */
    @DEField(name = "body_html")
    @JSONField(name = "body_html")
    @JsonProperty("body_html")
    private String bodyHtml;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 相关评级
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 收件人
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;

    /**
     * 下级消息
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 至（合作伙伴）
     */
    @JSONField(name = "recipient_ids")
    @JsonProperty("recipient_ids")
    private String recipientIds;

    /**
     * 追踪值
     */
    @JSONField(name = "tracking_value_ids")
    @JsonProperty("tracking_value_ids")
    private String trackingValueIds;

    /**
     * 待处理的业务伙伴
     */
    @JSONField(name = "needaction_partner_ids")
    @JsonProperty("needaction_partner_ids")
    private String needactionPartnerIds;

    /**
     * 抄送
     */
    @DEField(name = "email_cc")
    @JSONField(name = "email_cc")
    @JsonProperty("email_cc")
    private String emailCc;

    /**
     * 附件
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;

    /**
     * 失败原因
     */
    @DEField(name = "failure_reason")
    @JSONField(name = "failure_reason")
    @JsonProperty("failure_reason")
    private String failureReason;

    /**
     * 统计信息
     */
    @JSONField(name = "statistics_ids")
    @JsonProperty("statistics_ids")
    private String statisticsIds;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 标题
     */
    @JSONField(name = "headers")
    @JsonProperty("headers")
    private String headers;

    /**
     * 至
     */
    @DEField(name = "email_to")
    @JSONField(name = "email_to")
    @JsonProperty("email_to")
    private String emailTo;

    /**
     * 渠道
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * 通知
     */
    @JSONField(name = "notification_ids")
    @JsonProperty("notification_ids")
    private String notificationIds;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 参考
     */
    @JSONField(name = "references")
    @JsonProperty("references")
    private String references;

    /**
     * 群发邮件
     */
    @JSONField(name = "mailing_id_text")
    @JsonProperty("mailing_id_text")
    private String mailingIdText;

    /**
     * 回复 至
     */
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;

    /**
     * 需审核
     */
    @JSONField(name = "need_moderation")
    @JsonProperty("need_moderation")
    private String needModeration;

    /**
     * 消息ID
     */
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private String messageId;

    /**
     * 无响应
     */
    @JSONField(name = "no_auto_thread")
    @JsonProperty("no_auto_thread")
    private String noAutoThread;

    /**
     * 作者
     */
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    private Integer authorId;

    /**
     * 从
     */
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    /**
     * 相关的文档模型
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 消息记录名称
     */
    @JSONField(name = "record_name")
    @JsonProperty("record_name")
    private String recordName;

    /**
     * 加星的邮件
     */
    @JSONField(name = "starred")
    @JsonProperty("starred")
    private String starred;

    /**
     * 添加签名
     */
    @JSONField(name = "add_sign")
    @JsonProperty("add_sign")
    private String addSign;

    /**
     * 布局
     */
    @JSONField(name = "layout")
    @JsonProperty("layout")
    private String layout;

    /**
     * 上级消息
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 评级值
     */
    @JSONField(name = "rating_value")
    @JsonProperty("rating_value")
    private Double ratingValue;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 已发布
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 内容
     */
    @JSONField(name = "body")
    @JsonProperty("body")
    private String body;

    /**
     * 邮件发送服务器
     */
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 相关文档编号
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 子类型
     */
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    private Integer subtypeId;

    /**
     * 收件服务器
     */
    @JSONField(name = "fetchmail_server_id_text")
    @JsonProperty("fetchmail_server_id_text")
    private String fetchmailServerIdText;

    /**
     * 有误差
     */
    @JSONField(name = "has_error")
    @JsonProperty("has_error")
    private String hasError;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 作者头像
     */
    @JSONField(name = "author_avatar")
    @JsonProperty("author_avatar")
    private byte[] authorAvatar;

    /**
     * 管理状态
     */
    @JSONField(name = "moderation_status")
    @JsonProperty("moderation_status")
    private String moderationStatus;

    /**
     * 邮件活动类型
     */
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    private Integer mailActivityTypeId;

    /**
     * 管理员
     */
    @JSONField(name = "moderator_id")
    @JsonProperty("moderator_id")
    private Integer moderatorId;

    /**
     * 类型
     */
    @JSONField(name = "message_type")
    @JsonProperty("message_type")
    private String messageType;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 主题
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 待处理
     */
    @JSONField(name = "needaction")
    @JsonProperty("needaction")
    private String needaction;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 群发邮件
     */
    @DEField(name = "mailing_id")
    @JSONField(name = "mailing_id")
    @JsonProperty("mailing_id")
    private Integer mailingId;

    /**
     * 收件服务器
     */
    @DEField(name = "fetchmail_server_id")
    @JSONField(name = "fetchmail_server_id")
    @JsonProperty("fetchmail_server_id")
    private Integer fetchmailServerId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 消息
     */
    @DEField(name = "mail_message_id")
    @JSONField(name = "mail_message_id")
    @JsonProperty("mail_message_id")
    private Integer mailMessageId;


    /**
     * 
     */
    @JSONField(name = "odoofetchmailserver")
    @JsonProperty("odoofetchmailserver")
    private cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server odooFetchmailServer;

    /**
     * 
     */
    @JSONField(name = "odoomailing")
    @JsonProperty("odoomailing")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing odooMailing;

    /**
     * 
     */
    @JSONField(name = "odoomailmessage")
    @JsonProperty("odoomailmessage")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message odooMailMessage;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [通知]
     */
    public void setNotification(String notification){
        this.notification = notification ;
        this.modify("notification",notification);
    }
    /**
     * 设置 [安排的发送日期]
     */
    public void setScheduledDate(String scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }
    /**
     * 设置 [自动删除]
     */
    public void setAutoDelete(String autoDelete){
        this.autoDelete = autoDelete ;
        this.modify("auto_delete",autoDelete);
    }
    /**
     * 设置 [富文本内容]
     */
    public void setBodyHtml(String bodyHtml){
        this.bodyHtml = bodyHtml ;
        this.modify("body_html",bodyHtml);
    }
    /**
     * 设置 [抄送]
     */
    public void setEmailCc(String emailCc){
        this.emailCc = emailCc ;
        this.modify("email_cc",emailCc);
    }
    /**
     * 设置 [失败原因]
     */
    public void setFailureReason(String failureReason){
        this.failureReason = failureReason ;
        this.modify("failure_reason",failureReason);
    }
    /**
     * 设置 [标题]
     */
    public void setHeaders(String headers){
        this.headers = headers ;
        this.modify("headers",headers);
    }
    /**
     * 设置 [至]
     */
    public void setEmailTo(String emailTo){
        this.emailTo = emailTo ;
        this.modify("email_to",emailTo);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [参考]
     */
    public void setReferences(String references){
        this.references = references ;
        this.modify("references",references);
    }
    /**
     * 设置 [群发邮件]
     */
    public void setMailingId(Integer mailingId){
        this.mailingId = mailingId ;
        this.modify("mailing_id",mailingId);
    }
    /**
     * 设置 [收件服务器]
     */
    public void setFetchmailServerId(Integer fetchmailServerId){
        this.fetchmailServerId = fetchmailServerId ;
        this.modify("fetchmail_server_id",fetchmailServerId);
    }
    /**
     * 设置 [消息]
     */
    public void setMailMessageId(Integer mailMessageId){
        this.mailMessageId = mailMessageId ;
        this.modify("mail_message_id",mailMessageId);
    }

}


