package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_templateSearchContext;

/**
 * 实体 [报价单模板] 存储模型
 */
public interface Sale_order_template{

    /**
     * 报价单模板
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [报价单模板]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 在线签名
     */
    String getRequire_signature();

    void setRequire_signature(String require_signature);

    /**
     * 获取 [在线签名]脏标记
     */
    boolean getRequire_signatureDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 在线支付
     */
    String getRequire_payment();

    void setRequire_payment(String require_payment);

    /**
     * 获取 [在线支付]脏标记
     */
    boolean getRequire_paymentDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 报价单时长
     */
    Integer getNumber_of_days();

    void setNumber_of_days(Integer number_of_days);

    /**
     * 获取 [报价单时长]脏标记
     */
    boolean getNumber_of_daysDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 条款和条件
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [条款和条件]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 可选产品
     */
    String getSale_order_template_option_ids();

    void setSale_order_template_option_ids(String sale_order_template_option_ids);

    /**
     * 获取 [可选产品]脏标记
     */
    boolean getSale_order_template_option_idsDirtyFlag();

    /**
     * 明细行
     */
    String getSale_order_template_line_ids();

    void setSale_order_template_line_ids(String sale_order_template_line_ids);

    /**
     * 获取 [明细行]脏标记
     */
    boolean getSale_order_template_line_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 确认邮件
     */
    String getMail_template_id_text();

    void setMail_template_id_text(String mail_template_id_text);

    /**
     * 获取 [确认邮件]脏标记
     */
    boolean getMail_template_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 确认邮件
     */
    Integer getMail_template_id();

    void setMail_template_id(Integer mail_template_id);

    /**
     * 获取 [确认邮件]脏标记
     */
    boolean getMail_template_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
