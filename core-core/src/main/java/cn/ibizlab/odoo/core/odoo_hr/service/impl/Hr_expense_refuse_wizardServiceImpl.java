package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_refuse_wizard;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_refuse_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expense_refuse_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_expense_refuse_wizardOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_expense_refuse_wizardClientModel;

/**
 * 实体[费用拒绝原因向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_expense_refuse_wizardServiceImpl implements IHr_expense_refuse_wizardService {

    @Autowired
    hr_expense_refuse_wizardOdooClient hr_expense_refuse_wizardOdooClient;


    @Override
    public boolean create(Hr_expense_refuse_wizard et) {
        hr_expense_refuse_wizardClientModel clientModel = convert2Model(et,null);
		hr_expense_refuse_wizardOdooClient.create(clientModel);
        Hr_expense_refuse_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_expense_refuse_wizard> list){
    }

    @Override
    public boolean update(Hr_expense_refuse_wizard et) {
        hr_expense_refuse_wizardClientModel clientModel = convert2Model(et,null);
		hr_expense_refuse_wizardOdooClient.update(clientModel);
        Hr_expense_refuse_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_expense_refuse_wizard> list){
    }

    @Override
    public Hr_expense_refuse_wizard get(Integer id) {
        hr_expense_refuse_wizardClientModel clientModel = new hr_expense_refuse_wizardClientModel();
        clientModel.setId(id);
		hr_expense_refuse_wizardOdooClient.get(clientModel);
        Hr_expense_refuse_wizard et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_expense_refuse_wizard();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        hr_expense_refuse_wizardClientModel clientModel = new hr_expense_refuse_wizardClientModel();
        clientModel.setId(id);
		hr_expense_refuse_wizardOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_expense_refuse_wizard> searchDefault(Hr_expense_refuse_wizardSearchContext context) {
        List<Hr_expense_refuse_wizard> list = new ArrayList<Hr_expense_refuse_wizard>();
        Page<hr_expense_refuse_wizardClientModel> clientModelList = hr_expense_refuse_wizardOdooClient.search(context);
        for(hr_expense_refuse_wizardClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_expense_refuse_wizard>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_expense_refuse_wizardClientModel convert2Model(Hr_expense_refuse_wizard domain , hr_expense_refuse_wizardClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_expense_refuse_wizardClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("hr_expense_idsdirtyflag"))
                model.setHr_expense_ids(domain.getHrExpenseIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("reasondirtyflag"))
                model.setReason(domain.getReason());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("hr_expense_sheet_id_textdirtyflag"))
                model.setHr_expense_sheet_id_text(domain.getHrExpenseSheetIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("hr_expense_sheet_iddirtyflag"))
                model.setHr_expense_sheet_id(domain.getHrExpenseSheetId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_expense_refuse_wizard convert2Domain( hr_expense_refuse_wizardClientModel model ,Hr_expense_refuse_wizard domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_expense_refuse_wizard();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getHr_expense_idsDirtyFlag())
            domain.setHrExpenseIds(model.getHr_expense_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getReasonDirtyFlag())
            domain.setReason(model.getReason());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getHr_expense_sheet_id_textDirtyFlag())
            domain.setHrExpenseSheetIdText(model.getHr_expense_sheet_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getHr_expense_sheet_idDirtyFlag())
            domain.setHrExpenseSheetId(model.getHr_expense_sheet_id());
        return domain ;
    }

}

    



