package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_abstract_paymentSearchContext;


/**
 * 实体[Account_abstract_payment] 服务对象接口
 */
public interface IAccount_abstract_paymentService{

    boolean update(Account_abstract_payment et) ;
    void updateBatch(List<Account_abstract_payment> list) ;
    Account_abstract_payment get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_abstract_payment et) ;
    void createBatch(List<Account_abstract_payment> list) ;
    Page<Account_abstract_payment> searchDefault(Account_abstract_paymentSearchContext context) ;

}



