package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_leave_report;

/**
 * 实体[hr_leave_report] 服务对象接口
 */
public interface hr_leave_reportRepository{


    public hr_leave_report createPO() ;
        public void remove(String id);

        public void update(hr_leave_report hr_leave_report);

        public void removeBatch(String id);

        public List<hr_leave_report> search();

        public void createBatch(hr_leave_report hr_leave_report);

        public void create(hr_leave_report hr_leave_report);

        public void get(String id);

        public void updateBatch(hr_leave_report hr_leave_report);


}
