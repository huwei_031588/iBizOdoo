package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Calendar_attendee;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_attendeeSearchContext;

/**
 * 实体 [日历出席者信息] 存储对象
 */
public interface Calendar_attendeeRepository extends Repository<Calendar_attendee> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Calendar_attendee> searchDefault(Calendar_attendeeSearchContext context);

    Calendar_attendee convert2PO(cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_attendee domain , Calendar_attendee po) ;

    cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_attendee convert2Domain( Calendar_attendee po ,cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_attendee domain) ;

}
