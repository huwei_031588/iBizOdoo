package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mail_mass_mailing_contact] 对象
 */
public interface mail_mass_mailing_contact {

    public String getCompany_name();

    public void setCompany_name(String company_name);

    public Integer getCountry_id();

    public void setCountry_id(Integer country_id);

    public String getCountry_id_text();

    public void setCountry_id_text(String country_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmail();

    public void setEmail(String email);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_blacklisted();

    public void setIs_blacklisted(String is_blacklisted);

    public String getIs_email_valid();

    public void setIs_email_valid(String is_email_valid);

    public String getList_ids();

    public void setList_ids(String list_ids);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public Integer getMessage_bounce();

    public void setMessage_bounce(Integer message_bounce);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public String getOpt_out();

    public void setOpt_out(String opt_out);

    public String getSubscription_list_ids();

    public void setSubscription_list_ids(String subscription_list_ids);

    public String getTag_ids();

    public void setTag_ids(String tag_ids);

    public Integer getTitle_id();

    public void setTitle_id(Integer title_id);

    public String getTitle_id_text();

    public void setTitle_id_text(String title_id_text);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
