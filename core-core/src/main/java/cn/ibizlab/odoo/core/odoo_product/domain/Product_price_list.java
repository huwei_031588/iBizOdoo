package cn.ibizlab.odoo.core.odoo_product.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [基于价格列表版本的单位产品价格] 对象
 */
@Data
public class Product_price_list extends EntityClient implements Serializable {

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 数量-5
     */
    @JSONField(name = "qty5")
    @JsonProperty("qty5")
    private Integer qty5;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 数量-3
     */
    @JSONField(name = "qty3")
    @JsonProperty("qty3")
    private Integer qty3;

    /**
     * 数量-4
     */
    @JSONField(name = "qty4")
    @JsonProperty("qty4")
    private Integer qty4;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 数量-1
     */
    @JSONField(name = "qty1")
    @JsonProperty("qty1")
    private Integer qty1;

    /**
     * 数量-2
     */
    @JSONField(name = "qty2")
    @JsonProperty("qty2")
    private Integer qty2;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 价格表
     */
    @JSONField(name = "price_list_text")
    @JsonProperty("price_list_text")
    private String priceListText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 价格表
     */
    @DEField(name = "price_list")
    @JSONField(name = "price_list")
    @JsonProperty("price_list")
    private Integer priceList;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odooprice")
    @JsonProperty("odooprice")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist odooPrice;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [数量-5]
     */
    public void setQty5(Integer qty5){
        this.qty5 = qty5 ;
        this.modify("qty5",qty5);
    }
    /**
     * 设置 [数量-3]
     */
    public void setQty3(Integer qty3){
        this.qty3 = qty3 ;
        this.modify("qty3",qty3);
    }
    /**
     * 设置 [数量-4]
     */
    public void setQty4(Integer qty4){
        this.qty4 = qty4 ;
        this.modify("qty4",qty4);
    }
    /**
     * 设置 [数量-1]
     */
    public void setQty1(Integer qty1){
        this.qty1 = qty1 ;
        this.modify("qty1",qty1);
    }
    /**
     * 设置 [数量-2]
     */
    public void setQty2(Integer qty2){
        this.qty2 = qty2 ;
        this.modify("qty2",qty2);
    }
    /**
     * 设置 [价格表]
     */
    public void setPriceList(Integer priceList){
        this.priceList = priceList ;
        this.modify("price_list",priceList);
    }

}


