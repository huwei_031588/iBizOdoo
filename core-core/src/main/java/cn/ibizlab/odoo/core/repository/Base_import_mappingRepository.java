package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_import_mapping;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_mappingSearchContext;

/**
 * 实体 [基础导入对照] 存储对象
 */
public interface Base_import_mappingRepository extends Repository<Base_import_mapping> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_import_mapping> searchDefault(Base_import_mappingSearchContext context);

    Base_import_mapping convert2PO(cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_mapping domain , Base_import_mapping po) ;

    cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_mapping convert2Domain( Base_import_mapping po ,cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_mapping domain) ;

}
