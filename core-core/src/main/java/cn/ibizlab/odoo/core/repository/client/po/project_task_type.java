package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [project_task_type] 对象
 */
public interface project_task_type {

    public String getAuto_validation_kanban_state();

    public void setAuto_validation_kanban_state(String auto_validation_kanban_state);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getFold();

    public void setFold(String fold);

    public Integer getId();

    public void setId(Integer id);

    public String getLegend_blocked();

    public void setLegend_blocked(String legend_blocked);

    public String getLegend_done();

    public void setLegend_done(String legend_done);

    public String getLegend_normal();

    public void setLegend_normal(String legend_normal);

    public String getLegend_priority();

    public void setLegend_priority(String legend_priority);

    public Integer getMail_template_id();

    public void setMail_template_id(Integer mail_template_id);

    public String getMail_template_id_text();

    public void setMail_template_id_text(String mail_template_id_text);

    public String getName();

    public void setName(String name);

    public String getProject_ids();

    public void setProject_ids(String project_ids);

    public Integer getRating_template_id();

    public void setRating_template_id(Integer rating_template_id);

    public String getRating_template_id_text();

    public void setRating_template_id_text(String rating_template_id_text);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
