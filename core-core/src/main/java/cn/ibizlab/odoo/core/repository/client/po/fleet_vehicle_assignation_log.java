package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [fleet_vehicle_assignation_log] 对象
 */
public interface fleet_vehicle_assignation_log {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_end();

    public void setDate_end(Timestamp date_end);

    public Timestamp getDate_start();

    public void setDate_start(Timestamp date_start);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getDriver_id();

    public void setDriver_id(Integer driver_id);

    public String getDriver_id_text();

    public void setDriver_id_text(String driver_id_text);

    public Integer getId();

    public void setId(Integer id);

    public Integer getVehicle_id();

    public void setVehicle_id(Integer vehicle_id);

    public String getVehicle_id_text();

    public void setVehicle_id_text(String vehicle_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
