package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_image;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_imageSearchContext;

/**
 * 实体 [产品图片] 存储对象
 */
public interface Product_imageRepository extends Repository<Product_image> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_image> searchDefault(Product_imageSearchContext context);

    Product_image convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_image domain , Product_image po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_image convert2Domain( Product_image po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_image domain) ;

}
