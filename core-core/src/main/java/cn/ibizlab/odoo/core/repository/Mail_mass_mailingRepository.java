package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_mass_mailing;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailingSearchContext;

/**
 * 实体 [群发邮件] 存储对象
 */
public interface Mail_mass_mailingRepository extends Repository<Mail_mass_mailing> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_mass_mailing> searchDefault(Mail_mass_mailingSearchContext context);

    Mail_mass_mailing convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing domain , Mail_mass_mailing po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing convert2Domain( Mail_mass_mailing po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing domain) ;

}
