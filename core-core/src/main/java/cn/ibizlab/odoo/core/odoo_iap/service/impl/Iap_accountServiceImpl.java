package cn.ibizlab.odoo.core.odoo_iap.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account;
import cn.ibizlab.odoo.core.odoo_iap.filter.Iap_accountSearchContext;
import cn.ibizlab.odoo.core.odoo_iap.service.IIap_accountService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_iap.client.iap_accountOdooClient;
import cn.ibizlab.odoo.core.odoo_iap.clientmodel.iap_accountClientModel;

/**
 * 实体[IAP 账户] 服务对象接口实现
 */
@Slf4j
@Service
public class Iap_accountServiceImpl implements IIap_accountService {

    @Autowired
    iap_accountOdooClient iap_accountOdooClient;


    @Override
    public boolean remove(Integer id) {
        iap_accountClientModel clientModel = new iap_accountClientModel();
        clientModel.setId(id);
		iap_accountOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Iap_account get(Integer id) {
        iap_accountClientModel clientModel = new iap_accountClientModel();
        clientModel.setId(id);
		iap_accountOdooClient.get(clientModel);
        Iap_account et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Iap_account();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Iap_account et) {
        iap_accountClientModel clientModel = convert2Model(et,null);
		iap_accountOdooClient.update(clientModel);
        Iap_account rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Iap_account> list){
    }

    @Override
    public boolean create(Iap_account et) {
        iap_accountClientModel clientModel = convert2Model(et,null);
		iap_accountOdooClient.create(clientModel);
        Iap_account rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Iap_account> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Iap_account> searchDefault(Iap_accountSearchContext context) {
        List<Iap_account> list = new ArrayList<Iap_account>();
        Page<iap_accountClientModel> clientModelList = iap_accountOdooClient.search(context);
        for(iap_accountClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Iap_account>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public iap_accountClientModel convert2Model(Iap_account domain , iap_accountClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new iap_accountClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("account_tokendirtyflag"))
                model.setAccount_token(domain.getAccountToken());
            if((Boolean) domain.getExtensionparams().get("service_namedirtyflag"))
                model.setService_name(domain.getServiceName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Iap_account convert2Domain( iap_accountClientModel model ,Iap_account domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Iap_account();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAccount_tokenDirtyFlag())
            domain.setAccountToken(model.getAccount_token());
        if(model.getService_nameDirtyFlag())
            domain.setServiceName(model.getService_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        return domain ;
    }

}

    



