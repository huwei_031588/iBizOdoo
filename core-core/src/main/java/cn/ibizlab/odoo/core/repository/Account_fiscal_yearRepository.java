package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_fiscal_year;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_yearSearchContext;

/**
 * 实体 [会计年度] 存储对象
 */
public interface Account_fiscal_yearRepository extends Repository<Account_fiscal_year> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_fiscal_year> searchDefault(Account_fiscal_yearSearchContext context);

    Account_fiscal_year convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_year domain , Account_fiscal_year po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_year convert2Domain( Account_fiscal_year po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_year domain) ;

}
