package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activitySearchContext;

/**
 * 实体 [活动] 存储模型
 */
public interface Mail_activity{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 邮件模板
     */
    String getMail_template_ids();

    void setMail_template_ids(String mail_template_ids);

    /**
     * 获取 [邮件模板]脏标记
     */
    boolean getMail_template_idsDirtyFlag();

    /**
     * 自动活动
     */
    String getAutomated();

    void setAutomated(String automated);

    /**
     * 获取 [自动活动]脏标记
     */
    boolean getAutomatedDirtyFlag();

    /**
     * 文档名称
     */
    String getRes_name();

    void setRes_name(String res_name);

    /**
     * 获取 [文档名称]脏标记
     */
    boolean getRes_nameDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 到期时间
     */
    Timestamp getDate_deadline();

    void setDate_deadline(Timestamp date_deadline);

    /**
     * 获取 [到期时间]脏标记
     */
    boolean getDate_deadlineDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 摘要
     */
    String getSummary();

    void setSummary(String summary);

    /**
     * 获取 [摘要]脏标记
     */
    boolean getSummaryDirtyFlag();

    /**
     * 下一活动可用
     */
    String getHas_recommended_activities();

    void setHas_recommended_activities(String has_recommended_activities);

    /**
     * 获取 [下一活动可用]脏标记
     */
    boolean getHas_recommended_activitiesDirtyFlag();

    /**
     * 相关文档编号
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [相关文档编号]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 备注
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [备注]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 反馈
     */
    String getFeedback();

    void setFeedback(String feedback);

    /**
     * 获取 [反馈]脏标记
     */
    boolean getFeedbackDirtyFlag();

    /**
     * 文档模型
     */
    Integer getRes_model_id();

    void setRes_model_id(Integer res_model_id);

    /**
     * 获取 [文档模型]脏标记
     */
    boolean getRes_model_idDirtyFlag();

    /**
     * 相关的文档模型
     */
    String getRes_model();

    void setRes_model(String res_model);

    /**
     * 获取 [相关的文档模型]脏标记
     */
    boolean getRes_modelDirtyFlag();

    /**
     * 分派给
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [分派给]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 类别
     */
    String getActivity_category();

    void setActivity_category(String activity_category);

    /**
     * 获取 [类别]脏标记
     */
    boolean getActivity_categoryDirtyFlag();

    /**
     * 活动
     */
    String getActivity_type_id_text();

    void setActivity_type_id_text(String activity_type_id_text);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_type_id_textDirtyFlag();

    /**
     * 前一活动类型
     */
    String getPrevious_activity_type_id_text();

    void setPrevious_activity_type_id_text(String previous_activity_type_id_text);

    /**
     * 获取 [前一活动类型]脏标记
     */
    boolean getPrevious_activity_type_id_textDirtyFlag();

    /**
     * 相关便签
     */
    String getNote_id_text();

    void setNote_id_text(String note_id_text);

    /**
     * 获取 [相关便签]脏标记
     */
    boolean getNote_id_textDirtyFlag();

    /**
     * 建立者
     */
    String getCreate_user_id_text();

    void setCreate_user_id_text(String create_user_id_text);

    /**
     * 获取 [建立者]脏标记
     */
    boolean getCreate_user_id_textDirtyFlag();

    /**
     * 图标
     */
    String getIcon();

    void setIcon(String icon);

    /**
     * 获取 [图标]脏标记
     */
    boolean getIconDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 排版类型
     */
    String getActivity_decoration();

    void setActivity_decoration(String activity_decoration);

    /**
     * 获取 [排版类型]脏标记
     */
    boolean getActivity_decorationDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 自动安排下一个活动
     */
    String getForce_next();

    void setForce_next(String force_next);

    /**
     * 获取 [自动安排下一个活动]脏标记
     */
    boolean getForce_nextDirtyFlag();

    /**
     * 推荐的活动类型
     */
    String getRecommended_activity_type_id_text();

    void setRecommended_activity_type_id_text(String recommended_activity_type_id_text);

    /**
     * 获取 [推荐的活动类型]脏标记
     */
    boolean getRecommended_activity_type_id_textDirtyFlag();

    /**
     * 日历会议
     */
    String getCalendar_event_id_text();

    void setCalendar_event_id_text(String calendar_event_id_text);

    /**
     * 获取 [日历会议]脏标记
     */
    boolean getCalendar_event_id_textDirtyFlag();

    /**
     * 推荐的活动类型
     */
    Integer getRecommended_activity_type_id();

    void setRecommended_activity_type_id(Integer recommended_activity_type_id);

    /**
     * 获取 [推荐的活动类型]脏标记
     */
    boolean getRecommended_activity_type_idDirtyFlag();

    /**
     * 活动
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 建立者
     */
    Integer getCreate_user_id();

    void setCreate_user_id(Integer create_user_id);

    /**
     * 获取 [建立者]脏标记
     */
    boolean getCreate_user_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 分派给
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [分派给]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 相关便签
     */
    Integer getNote_id();

    void setNote_id(Integer note_id);

    /**
     * 获取 [相关便签]脏标记
     */
    boolean getNote_idDirtyFlag();

    /**
     * 前一活动类型
     */
    Integer getPrevious_activity_type_id();

    void setPrevious_activity_type_id(Integer previous_activity_type_id);

    /**
     * 获取 [前一活动类型]脏标记
     */
    boolean getPrevious_activity_type_idDirtyFlag();

    /**
     * 日历会议
     */
    Integer getCalendar_event_id();

    void setCalendar_event_id(Integer calendar_event_id);

    /**
     * 获取 [日历会议]脏标记
     */
    boolean getCalendar_event_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
