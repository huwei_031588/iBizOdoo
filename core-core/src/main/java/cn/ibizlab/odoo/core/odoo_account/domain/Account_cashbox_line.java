package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [钱箱明细] 对象
 */
@Data
public class Account_cashbox_line extends EntityClient implements Serializable {

    /**
     * 在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行
     */
    @DEField(name = "default_pos_id")
    @JSONField(name = "default_pos_id")
    @JsonProperty("default_pos_id")
    private Integer defaultPosId;

    /**
     * 小计
     */
    @JSONField(name = "subtotal")
    @JsonProperty("subtotal")
    private Double subtotal;

    /**
     * 硬币／账单　价值
     */
    @DEField(name = "coin_value")
    @JSONField(name = "coin_value")
    @JsonProperty("coin_value")
    private Double coinValue;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 货币和账单编号
     */
    @JSONField(name = "number")
    @JsonProperty("number")
    private Integer number;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 钱箱
     */
    @DEField(name = "cashbox_id")
    @JSONField(name = "cashbox_id")
    @JsonProperty("cashbox_id")
    private Integer cashboxId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocashbox")
    @JsonProperty("odoocashbox")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox odooCashbox;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]
     */
    public void setDefaultPosId(Integer defaultPosId){
        this.defaultPosId = defaultPosId ;
        this.modify("default_pos_id",defaultPosId);
    }
    /**
     * 设置 [硬币／账单　价值]
     */
    public void setCoinValue(Double coinValue){
        this.coinValue = coinValue ;
        this.modify("coin_value",coinValue);
    }
    /**
     * 设置 [货币和账单编号]
     */
    public void setNumber(Integer number){
        this.number = number ;
        this.modify("number",number);
    }
    /**
     * 设置 [钱箱]
     */
    public void setCashboxId(Integer cashboxId){
        this.cashboxId = cashboxId ;
        this.modify("cashbox_id",cashboxId);
    }

}


