package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [fleet_vehicle] 对象
 */
public interface Ifleet_vehicle {

    /**
     * 获取 [注册日期]
     */
    public void setAcquisition_date(Timestamp acquisition_date);
    
    /**
     * 设置 [注册日期]
     */
    public Timestamp getAcquisition_date();

    /**
     * 获取 [注册日期]脏标记
     */
    public boolean getAcquisition_dateDirtyFlag();
    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [下一活动截止日期]
     */
    public void setActivity_date_deadline(Timestamp activity_date_deadline);
    
    /**
     * 设置 [下一活动截止日期]
     */
    public Timestamp getActivity_date_deadline();

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    public boolean getActivity_date_deadlineDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setActivity_ids(String activity_ids);
    
    /**
     * 设置 [活动]
     */
    public String getActivity_ids();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getActivity_idsDirtyFlag();
    /**
     * 获取 [活动状态]
     */
    public void setActivity_state(String activity_state);
    
    /**
     * 设置 [活动状态]
     */
    public String getActivity_state();

    /**
     * 获取 [活动状态]脏标记
     */
    public boolean getActivity_stateDirtyFlag();
    /**
     * 获取 [下一活动摘要]
     */
    public void setActivity_summary(String activity_summary);
    
    /**
     * 设置 [下一活动摘要]
     */
    public String getActivity_summary();

    /**
     * 获取 [下一活动摘要]脏标记
     */
    public boolean getActivity_summaryDirtyFlag();
    /**
     * 获取 [下一活动类型]
     */
    public void setActivity_type_id(Integer activity_type_id);
    
    /**
     * 设置 [下一活动类型]
     */
    public Integer getActivity_type_id();

    /**
     * 获取 [下一活动类型]脏标记
     */
    public boolean getActivity_type_idDirtyFlag();
    /**
     * 获取 [责任用户]
     */
    public void setActivity_user_id(Integer activity_user_id);
    
    /**
     * 设置 [责任用户]
     */
    public Integer getActivity_user_id();

    /**
     * 获取 [责任用户]脏标记
     */
    public boolean getActivity_user_idDirtyFlag();
    /**
     * 获取 [品牌]
     */
    public void setBrand_id(Integer brand_id);
    
    /**
     * 设置 [品牌]
     */
    public Integer getBrand_id();

    /**
     * 获取 [品牌]脏标记
     */
    public boolean getBrand_idDirtyFlag();
    /**
     * 获取 [品牌]
     */
    public void setBrand_id_text(String brand_id_text);
    
    /**
     * 设置 [品牌]
     */
    public String getBrand_id_text();

    /**
     * 获取 [品牌]脏标记
     */
    public boolean getBrand_id_textDirtyFlag();
    /**
     * 获取 [目录值（包括增值税）]
     */
    public void setCar_value(Double car_value);
    
    /**
     * 设置 [目录值（包括增值税）]
     */
    public Double getCar_value();

    /**
     * 获取 [目录值（包括增值税）]脏标记
     */
    public boolean getCar_valueDirtyFlag();
    /**
     * 获取 [二氧化碳排放量]
     */
    public void setCo2(Double co2);
    
    /**
     * 设置 [二氧化碳排放量]
     */
    public Double getCo2();

    /**
     * 获取 [二氧化碳排放量]脏标记
     */
    public boolean getCo2DirtyFlag();
    /**
     * 获取 [颜色]
     */
    public void setColor(String color);
    
    /**
     * 设置 [颜色]
     */
    public String getColor();

    /**
     * 获取 [颜色]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [合同统计]
     */
    public void setContract_count(Integer contract_count);
    
    /**
     * 设置 [合同统计]
     */
    public Integer getContract_count();

    /**
     * 获取 [合同统计]脏标记
     */
    public boolean getContract_countDirtyFlag();
    /**
     * 获取 [有合同待续签]
     */
    public void setContract_renewal_due_soon(String contract_renewal_due_soon);
    
    /**
     * 设置 [有合同待续签]
     */
    public String getContract_renewal_due_soon();

    /**
     * 获取 [有合同待续签]脏标记
     */
    public boolean getContract_renewal_due_soonDirtyFlag();
    /**
     * 获取 [需马上续签合同的名称]
     */
    public void setContract_renewal_name(String contract_renewal_name);
    
    /**
     * 设置 [需马上续签合同的名称]
     */
    public String getContract_renewal_name();

    /**
     * 获取 [需马上续签合同的名称]脏标记
     */
    public boolean getContract_renewal_nameDirtyFlag();
    /**
     * 获取 [有逾期合同]
     */
    public void setContract_renewal_overdue(String contract_renewal_overdue);
    
    /**
     * 设置 [有逾期合同]
     */
    public String getContract_renewal_overdue();

    /**
     * 获取 [有逾期合同]脏标记
     */
    public boolean getContract_renewal_overdueDirtyFlag();
    /**
     * 获取 [截止或者逾期减一的合同总计]
     */
    public void setContract_renewal_total(String contract_renewal_total);
    
    /**
     * 设置 [截止或者逾期减一的合同总计]
     */
    public String getContract_renewal_total();

    /**
     * 获取 [截止或者逾期减一的合同总计]脏标记
     */
    public boolean getContract_renewal_totalDirtyFlag();
    /**
     * 获取 [费用]
     */
    public void setCost_count(Integer cost_count);
    
    /**
     * 设置 [费用]
     */
    public Integer getCost_count();

    /**
     * 获取 [费用]脏标记
     */
    public boolean getCost_countDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [车门数量]
     */
    public void setDoors(Integer doors);
    
    /**
     * 设置 [车门数量]
     */
    public Integer getDoors();

    /**
     * 获取 [车门数量]脏标记
     */
    public boolean getDoorsDirtyFlag();
    /**
     * 获取 [驾驶员]
     */
    public void setDriver_id(Integer driver_id);
    
    /**
     * 设置 [驾驶员]
     */
    public Integer getDriver_id();

    /**
     * 获取 [驾驶员]脏标记
     */
    public boolean getDriver_idDirtyFlag();
    /**
     * 获取 [驾驶员]
     */
    public void setDriver_id_text(String driver_id_text);
    
    /**
     * 设置 [驾驶员]
     */
    public String getDriver_id_text();

    /**
     * 获取 [驾驶员]脏标记
     */
    public boolean getDriver_id_textDirtyFlag();
    /**
     * 获取 [首次合同日期]
     */
    public void setFirst_contract_date(Timestamp first_contract_date);
    
    /**
     * 设置 [首次合同日期]
     */
    public Timestamp getFirst_contract_date();

    /**
     * 获取 [首次合同日期]脏标记
     */
    public boolean getFirst_contract_dateDirtyFlag();
    /**
     * 获取 [加油记录统计]
     */
    public void setFuel_logs_count(Integer fuel_logs_count);
    
    /**
     * 设置 [加油记录统计]
     */
    public Integer getFuel_logs_count();

    /**
     * 获取 [加油记录统计]脏标记
     */
    public boolean getFuel_logs_countDirtyFlag();
    /**
     * 获取 [燃油类型]
     */
    public void setFuel_type(String fuel_type);
    
    /**
     * 设置 [燃油类型]
     */
    public String getFuel_type();

    /**
     * 获取 [燃油类型]脏标记
     */
    public boolean getFuel_typeDirtyFlag();
    /**
     * 获取 [马力]
     */
    public void setHorsepower(Integer horsepower);
    
    /**
     * 设置 [马力]
     */
    public Integer getHorsepower();

    /**
     * 获取 [马力]脏标记
     */
    public boolean getHorsepowerDirtyFlag();
    /**
     * 获取 [车船税]
     */
    public void setHorsepower_tax(Double horsepower_tax);
    
    /**
     * 设置 [车船税]
     */
    public Double getHorsepower_tax();

    /**
     * 获取 [车船税]脏标记
     */
    public boolean getHorsepower_taxDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [徽标]
     */
    public void setImage(byte[] image);
    
    /**
     * 设置 [徽标]
     */
    public byte[] getImage();

    /**
     * 获取 [徽标]脏标记
     */
    public boolean getImageDirtyFlag();
    /**
     * 获取 [车辆标志图片(普通大小)]
     */
    public void setImage_medium(byte[] image_medium);
    
    /**
     * 设置 [车辆标志图片(普通大小)]
     */
    public byte[] getImage_medium();

    /**
     * 获取 [车辆标志图片(普通大小)]脏标记
     */
    public boolean getImage_mediumDirtyFlag();
    /**
     * 获取 [车辆标志图片(小图片)]
     */
    public void setImage_small(byte[] image_small);
    
    /**
     * 设置 [车辆标志图片(小图片)]
     */
    public byte[] getImage_small();

    /**
     * 获取 [车辆标志图片(小图片)]脏标记
     */
    public boolean getImage_smallDirtyFlag();
    /**
     * 获取 [车辆牌照]
     */
    public void setLicense_plate(String license_plate);
    
    /**
     * 设置 [车辆牌照]
     */
    public String getLicense_plate();

    /**
     * 获取 [车辆牌照]脏标记
     */
    public boolean getLicense_plateDirtyFlag();
    /**
     * 获取 [地点]
     */
    public void setLocation(String location);
    
    /**
     * 设置 [地点]
     */
    public String getLocation();

    /**
     * 获取 [地点]脏标记
     */
    public boolean getLocationDirtyFlag();
    /**
     * 获取 [合同]
     */
    public void setLog_contracts(String log_contracts);
    
    /**
     * 设置 [合同]
     */
    public String getLog_contracts();

    /**
     * 获取 [合同]脏标记
     */
    public boolean getLog_contractsDirtyFlag();
    /**
     * 获取 [指派记录]
     */
    public void setLog_drivers(String log_drivers);
    
    /**
     * 设置 [指派记录]
     */
    public String getLog_drivers();

    /**
     * 获取 [指派记录]脏标记
     */
    public boolean getLog_driversDirtyFlag();
    /**
     * 获取 [燃油记录]
     */
    public void setLog_fuel(String log_fuel);
    
    /**
     * 设置 [燃油记录]
     */
    public String getLog_fuel();

    /**
     * 获取 [燃油记录]脏标记
     */
    public boolean getLog_fuelDirtyFlag();
    /**
     * 获取 [服务记录]
     */
    public void setLog_services(String log_services);
    
    /**
     * 设置 [服务记录]
     */
    public String getLog_services();

    /**
     * 获取 [服务记录]脏标记
     */
    public boolean getLog_servicesDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [需要激活]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [需要激活]
     */
    public String getMessage_needaction();

    /**
     * 获取 [需要激活]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [行动数量]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [行动数量]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [行动数量]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [型号]
     */
    public void setModel_id(Integer model_id);
    
    /**
     * 设置 [型号]
     */
    public Integer getModel_id();

    /**
     * 获取 [型号]脏标记
     */
    public boolean getModel_idDirtyFlag();
    /**
     * 获取 [型号]
     */
    public void setModel_id_text(String model_id_text);
    
    /**
     * 设置 [型号]
     */
    public String getModel_id_text();

    /**
     * 获取 [型号]脏标记
     */
    public boolean getModel_id_textDirtyFlag();
    /**
     * 获取 [型号年份]
     */
    public void setModel_year(String model_year);
    
    /**
     * 设置 [型号年份]
     */
    public String getModel_year();

    /**
     * 获取 [型号年份]脏标记
     */
    public boolean getModel_yearDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [最新里程表]
     */
    public void setOdometer(Double odometer);
    
    /**
     * 设置 [最新里程表]
     */
    public Double getOdometer();

    /**
     * 获取 [最新里程表]脏标记
     */
    public boolean getOdometerDirtyFlag();
    /**
     * 获取 [里程表]
     */
    public void setOdometer_count(Integer odometer_count);
    
    /**
     * 设置 [里程表]
     */
    public Integer getOdometer_count();

    /**
     * 获取 [里程表]脏标记
     */
    public boolean getOdometer_countDirtyFlag();
    /**
     * 获取 [里程表单位]
     */
    public void setOdometer_unit(String odometer_unit);
    
    /**
     * 设置 [里程表单位]
     */
    public String getOdometer_unit();

    /**
     * 获取 [里程表单位]脏标记
     */
    public boolean getOdometer_unitDirtyFlag();
    /**
     * 获取 [动力]
     */
    public void setPower(Integer power);
    
    /**
     * 设置 [动力]
     */
    public Integer getPower();

    /**
     * 获取 [动力]脏标记
     */
    public boolean getPowerDirtyFlag();
    /**
     * 获取 [残余价值]
     */
    public void setResidual_value(Double residual_value);
    
    /**
     * 设置 [残余价值]
     */
    public Double getResidual_value();

    /**
     * 获取 [残余价值]脏标记
     */
    public boolean getResidual_valueDirtyFlag();
    /**
     * 获取 [座位数]
     */
    public void setSeats(Integer seats);
    
    /**
     * 设置 [座位数]
     */
    public Integer getSeats();

    /**
     * 获取 [座位数]脏标记
     */
    public boolean getSeatsDirtyFlag();
    /**
     * 获取 [服务]
     */
    public void setService_count(Integer service_count);
    
    /**
     * 设置 [服务]
     */
    public Integer getService_count();

    /**
     * 获取 [服务]脏标记
     */
    public boolean getService_countDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState_id(Integer state_id);
    
    /**
     * 设置 [状态]
     */
    public Integer getState_id();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getState_idDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState_id_text(String state_id_text);
    
    /**
     * 设置 [状态]
     */
    public String getState_id_text();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getState_id_textDirtyFlag();
    /**
     * 获取 [标签]
     */
    public void setTag_ids(String tag_ids);
    
    /**
     * 设置 [标签]
     */
    public String getTag_ids();

    /**
     * 获取 [标签]脏标记
     */
    public boolean getTag_idsDirtyFlag();
    /**
     * 获取 [变速器]
     */
    public void setTransmission(String transmission);
    
    /**
     * 设置 [变速器]
     */
    public String getTransmission();

    /**
     * 获取 [变速器]脏标记
     */
    public boolean getTransmissionDirtyFlag();
    /**
     * 获取 [车架号]
     */
    public void setVin_sn(String vin_sn);
    
    /**
     * 设置 [车架号]
     */
    public String getVin_sn();

    /**
     * 获取 [车架号]脏标记
     */
    public boolean getVin_snDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
