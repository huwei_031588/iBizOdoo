package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_partner_merge_automatic_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.base_partner_merge_automatic_wizardOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.base_partner_merge_automatic_wizardClientModel;

/**
 * 实体[合并业务伙伴向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_partner_merge_automatic_wizardServiceImpl implements IBase_partner_merge_automatic_wizardService {

    @Autowired
    base_partner_merge_automatic_wizardOdooClient base_partner_merge_automatic_wizardOdooClient;


    @Override
    public boolean update(Base_partner_merge_automatic_wizard et) {
        base_partner_merge_automatic_wizardClientModel clientModel = convert2Model(et,null);
		base_partner_merge_automatic_wizardOdooClient.update(clientModel);
        Base_partner_merge_automatic_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Base_partner_merge_automatic_wizard> list){
    }

    @Override
    public boolean create(Base_partner_merge_automatic_wizard et) {
        base_partner_merge_automatic_wizardClientModel clientModel = convert2Model(et,null);
		base_partner_merge_automatic_wizardOdooClient.create(clientModel);
        Base_partner_merge_automatic_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_partner_merge_automatic_wizard> list){
    }

    @Override
    public Base_partner_merge_automatic_wizard get(Integer id) {
        base_partner_merge_automatic_wizardClientModel clientModel = new base_partner_merge_automatic_wizardClientModel();
        clientModel.setId(id);
		base_partner_merge_automatic_wizardOdooClient.get(clientModel);
        Base_partner_merge_automatic_wizard et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Base_partner_merge_automatic_wizard();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        base_partner_merge_automatic_wizardClientModel clientModel = new base_partner_merge_automatic_wizardClientModel();
        clientModel.setId(id);
		base_partner_merge_automatic_wizardOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_partner_merge_automatic_wizard> searchDefault(Base_partner_merge_automatic_wizardSearchContext context) {
        List<Base_partner_merge_automatic_wizard> list = new ArrayList<Base_partner_merge_automatic_wizard>();
        Page<base_partner_merge_automatic_wizardClientModel> clientModelList = base_partner_merge_automatic_wizardOdooClient.search(context);
        for(base_partner_merge_automatic_wizardClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Base_partner_merge_automatic_wizard>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public base_partner_merge_automatic_wizardClientModel convert2Model(Base_partner_merge_automatic_wizard domain , base_partner_merge_automatic_wizardClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new base_partner_merge_automatic_wizardClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("maximum_groupdirtyflag"))
                model.setMaximum_group(domain.getMaximumGroup());
            if((Boolean) domain.getExtensionparams().get("group_by_parent_iddirtyflag"))
                model.setGroup_by_parent_id(domain.getGroupByParentId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("number_groupdirtyflag"))
                model.setNumber_group(domain.getNumberGroup());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("line_idsdirtyflag"))
                model.setLine_ids(domain.getLineIds());
            if((Boolean) domain.getExtensionparams().get("group_by_vatdirtyflag"))
                model.setGroup_by_vat(domain.getGroupByVat());
            if((Boolean) domain.getExtensionparams().get("exclude_contactdirtyflag"))
                model.setExclude_contact(domain.getExcludeContact());
            if((Boolean) domain.getExtensionparams().get("partner_idsdirtyflag"))
                model.setPartner_ids(domain.getPartnerIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("group_by_emaildirtyflag"))
                model.setGroup_by_email(domain.getGroupByEmail());
            if((Boolean) domain.getExtensionparams().get("group_by_is_companydirtyflag"))
                model.setGroup_by_is_company(domain.getGroupByIsCompany());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("exclude_journal_itemdirtyflag"))
                model.setExclude_journal_item(domain.getExcludeJournalItem());
            if((Boolean) domain.getExtensionparams().get("group_by_namedirtyflag"))
                model.setGroup_by_name(domain.getGroupByName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("dst_partner_id_textdirtyflag"))
                model.setDst_partner_id_text(domain.getDstPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("dst_partner_iddirtyflag"))
                model.setDst_partner_id(domain.getDstPartnerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("current_line_iddirtyflag"))
                model.setCurrent_line_id(domain.getCurrentLineId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Base_partner_merge_automatic_wizard convert2Domain( base_partner_merge_automatic_wizardClientModel model ,Base_partner_merge_automatic_wizard domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Base_partner_merge_automatic_wizard();
        }

        if(model.getMaximum_groupDirtyFlag())
            domain.setMaximumGroup(model.getMaximum_group());
        if(model.getGroup_by_parent_idDirtyFlag())
            domain.setGroupByParentId(model.getGroup_by_parent_id());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNumber_groupDirtyFlag())
            domain.setNumberGroup(model.getNumber_group());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getLine_idsDirtyFlag())
            domain.setLineIds(model.getLine_ids());
        if(model.getGroup_by_vatDirtyFlag())
            domain.setGroupByVat(model.getGroup_by_vat());
        if(model.getExclude_contactDirtyFlag())
            domain.setExcludeContact(model.getExclude_contact());
        if(model.getPartner_idsDirtyFlag())
            domain.setPartnerIds(model.getPartner_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getGroup_by_emailDirtyFlag())
            domain.setGroupByEmail(model.getGroup_by_email());
        if(model.getGroup_by_is_companyDirtyFlag())
            domain.setGroupByIsCompany(model.getGroup_by_is_company());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getExclude_journal_itemDirtyFlag())
            domain.setExcludeJournalItem(model.getExclude_journal_item());
        if(model.getGroup_by_nameDirtyFlag())
            domain.setGroupByName(model.getGroup_by_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getDst_partner_id_textDirtyFlag())
            domain.setDstPartnerIdText(model.getDst_partner_id_text());
        if(model.getDst_partner_idDirtyFlag())
            domain.setDstPartnerId(model.getDst_partner_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCurrent_line_idDirtyFlag())
            domain.setCurrentLineId(model.getCurrent_line_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



