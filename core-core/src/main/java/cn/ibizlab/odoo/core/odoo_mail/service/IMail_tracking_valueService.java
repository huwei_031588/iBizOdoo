package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_tracking_value;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_tracking_valueSearchContext;


/**
 * 实体[Mail_tracking_value] 服务对象接口
 */
public interface IMail_tracking_valueService{

    boolean update(Mail_tracking_value et) ;
    void updateBatch(List<Mail_tracking_value> list) ;
    boolean create(Mail_tracking_value et) ;
    void createBatch(List<Mail_tracking_value> list) ;
    Mail_tracking_value get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mail_tracking_value> searchDefault(Mail_tracking_valueSearchContext context) ;

}



