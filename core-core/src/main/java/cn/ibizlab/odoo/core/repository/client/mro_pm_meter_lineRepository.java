package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mro_pm_meter_line;

/**
 * 实体[mro_pm_meter_line] 服务对象接口
 */
public interface mro_pm_meter_lineRepository{


    public mro_pm_meter_line createPO() ;
        public void create(mro_pm_meter_line mro_pm_meter_line);

        public void get(String id);

        public void removeBatch(String id);

        public void update(mro_pm_meter_line mro_pm_meter_line);

        public void updateBatch(mro_pm_meter_line mro_pm_meter_line);

        public void remove(String id);

        public void createBatch(mro_pm_meter_line mro_pm_meter_line);

        public List<mro_pm_meter_line> search();


}
