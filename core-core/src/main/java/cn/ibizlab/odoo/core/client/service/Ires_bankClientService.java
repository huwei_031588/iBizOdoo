package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_bank;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_bank] 服务对象接口
 */
public interface Ires_bankClientService{

    public Ires_bank createModel() ;

    public void remove(Ires_bank res_bank);

    public void createBatch(List<Ires_bank> res_banks);

    public Page<Ires_bank> search(SearchContext context);

    public void removeBatch(List<Ires_bank> res_banks);

    public void update(Ires_bank res_bank);

    public void updateBatch(List<Ires_bank> res_banks);

    public void get(Ires_bank res_bank);

    public void create(Ires_bank res_bank);

    public Page<Ires_bank> select(SearchContext context);

}
