package cn.ibizlab.odoo.core.odoo_portal.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizard_userSearchContext;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_wizard_userService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_portal.client.portal_wizard_userOdooClient;
import cn.ibizlab.odoo.core.odoo_portal.clientmodel.portal_wizard_userClientModel;

/**
 * 实体[门户用户配置] 服务对象接口实现
 */
@Slf4j
@Service
public class Portal_wizard_userServiceImpl implements IPortal_wizard_userService {

    @Autowired
    portal_wizard_userOdooClient portal_wizard_userOdooClient;


    @Override
    public boolean remove(Integer id) {
        portal_wizard_userClientModel clientModel = new portal_wizard_userClientModel();
        clientModel.setId(id);
		portal_wizard_userOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Portal_wizard_user et) {
        portal_wizard_userClientModel clientModel = convert2Model(et,null);
		portal_wizard_userOdooClient.create(clientModel);
        Portal_wizard_user rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Portal_wizard_user> list){
    }

    @Override
    public Portal_wizard_user get(Integer id) {
        portal_wizard_userClientModel clientModel = new portal_wizard_userClientModel();
        clientModel.setId(id);
		portal_wizard_userOdooClient.get(clientModel);
        Portal_wizard_user et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Portal_wizard_user();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Portal_wizard_user et) {
        portal_wizard_userClientModel clientModel = convert2Model(et,null);
		portal_wizard_userOdooClient.update(clientModel);
        Portal_wizard_user rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Portal_wizard_user> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Portal_wizard_user> searchDefault(Portal_wizard_userSearchContext context) {
        List<Portal_wizard_user> list = new ArrayList<Portal_wizard_user>();
        Page<portal_wizard_userClientModel> clientModelList = portal_wizard_userOdooClient.search(context);
        for(portal_wizard_userClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Portal_wizard_user>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public portal_wizard_userClientModel convert2Model(Portal_wizard_user domain , portal_wizard_userClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new portal_wizard_userClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("in_portaldirtyflag"))
                model.setIn_portal(domain.getInPortal());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("emaildirtyflag"))
                model.setEmail(domain.getEmail());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("wizard_iddirtyflag"))
                model.setWizard_id(domain.getWizardId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Portal_wizard_user convert2Domain( portal_wizard_userClientModel model ,Portal_wizard_user domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Portal_wizard_user();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIn_portalDirtyFlag())
            domain.setInPortal(model.getIn_portal());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getEmailDirtyFlag())
            domain.setEmail(model.getEmail());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getWizard_idDirtyFlag())
            domain.setWizardId(model.getWizard_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        return domain ;
    }

}

    



