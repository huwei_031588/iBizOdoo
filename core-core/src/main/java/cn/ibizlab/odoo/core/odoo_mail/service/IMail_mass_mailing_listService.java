package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;


/**
 * 实体[Mail_mass_mailing_list] 服务对象接口
 */
public interface IMail_mass_mailing_listService{

    boolean update(Mail_mass_mailing_list et) ;
    void updateBatch(List<Mail_mass_mailing_list> list) ;
    boolean create(Mail_mass_mailing_list et) ;
    void createBatch(List<Mail_mass_mailing_list> list) ;
    Mail_mass_mailing_list get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mail_mass_mailing_list> searchDefault(Mail_mass_mailing_listSearchContext context) ;

}



