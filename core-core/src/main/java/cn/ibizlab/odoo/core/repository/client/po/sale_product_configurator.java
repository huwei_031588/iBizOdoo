package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [sale_product_configurator] 对象
 */
public interface sale_product_configurator {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public Integer getPricelist_id();

    public void setPricelist_id(Integer pricelist_id);

    public String getPricelist_id_text();

    public void setPricelist_id_text(String pricelist_id_text);

    public Integer getProduct_template_id();

    public void setProduct_template_id(Integer product_template_id);

    public String getProduct_template_id_text();

    public void setProduct_template_id_text(String product_template_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
