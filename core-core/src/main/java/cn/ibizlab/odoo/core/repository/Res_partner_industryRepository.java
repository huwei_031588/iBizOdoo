package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_partner_industry;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_industrySearchContext;

/**
 * 实体 [工业] 存储对象
 */
public interface Res_partner_industryRepository extends Repository<Res_partner_industry> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_partner_industry> searchDefault(Res_partner_industrySearchContext context);

    Res_partner_industry convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_industry domain , Res_partner_industry po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_industry convert2Domain( Res_partner_industry po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_industry domain) ;

}
