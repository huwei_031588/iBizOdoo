package cn.ibizlab.odoo.core.odoo_im_livechat.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [实时聊天频道规则] 对象
 */
@Data
public class Im_livechat_channel_rule extends EntityClient implements Serializable {

    /**
     * 匹配的订单
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 自动弹出时间
     */
    @DEField(name = "auto_popup_timer")
    @JSONField(name = "auto_popup_timer")
    @JsonProperty("auto_popup_timer")
    private Integer autoPopupTimer;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * URL的正则表达式
     */
    @DEField(name = "regex_url")
    @JSONField(name = "regex_url")
    @JsonProperty("regex_url")
    private String regexUrl;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 动作
     */
    @JSONField(name = "action")
    @JsonProperty("action")
    private String action;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 国家
     */
    @JSONField(name = "country_ids")
    @JsonProperty("country_ids")
    private String countryIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 渠道
     */
    @JSONField(name = "channel_id_text")
    @JsonProperty("channel_id_text")
    private String channelIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 渠道
     */
    @DEField(name = "channel_id")
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    private Integer channelId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoochannel")
    @JsonProperty("odoochannel")
    private cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel odooChannel;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [匹配的订单]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [自动弹出时间]
     */
    public void setAutoPopupTimer(Integer autoPopupTimer){
        this.autoPopupTimer = autoPopupTimer ;
        this.modify("auto_popup_timer",autoPopupTimer);
    }
    /**
     * 设置 [URL的正则表达式]
     */
    public void setRegexUrl(String regexUrl){
        this.regexUrl = regexUrl ;
        this.modify("regex_url",regexUrl);
    }
    /**
     * 设置 [动作]
     */
    public void setAction(String action){
        this.action = action ;
        this.modify("action",action);
    }
    /**
     * 设置 [渠道]
     */
    public void setChannelId(Integer channelId){
        this.channelId = channelId ;
        this.modify("channel_id",channelId);
    }

}


