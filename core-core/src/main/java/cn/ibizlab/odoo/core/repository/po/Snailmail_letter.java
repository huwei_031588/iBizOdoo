package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_snailmail.filter.Snailmail_letterSearchContext;

/**
 * 实体 [Snailmail 信纸] 存储模型
 */
public interface Snailmail_letter{

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 双面
     */
    String getDuplex();

    void setDuplex(String duplex);

    /**
     * 获取 [双面]脏标记
     */
    boolean getDuplexDirtyFlag();

    /**
     * 颜色
     */
    String getColor();

    void setColor(String color);

    /**
     * 获取 [颜色]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 型号
     */
    String getModel();

    void setModel(String model);

    /**
     * 获取 [型号]脏标记
     */
    boolean getModelDirtyFlag();

    /**
     * 附件
     */
    Integer getAttachment_id();

    void setAttachment_id(Integer attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getAttachment_idDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 可选的打印和附加的报表
     */
    Integer getReport_template();

    void setReport_template(Integer report_template);

    /**
     * 获取 [可选的打印和附加的报表]脏标记
     */
    boolean getReport_templateDirtyFlag();

    /**
     * 信息
     */
    String getInfo_msg();

    void setInfo_msg(String info_msg);

    /**
     * 获取 [信息]脏标记
     */
    boolean getInfo_msgDirtyFlag();

    /**
     * 文档ID
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [文档ID]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 收件人
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [收件人]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 发信人用户
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [发信人用户]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 发信人用户
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [发信人用户]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 收件人
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [收件人]脏标记
     */
    boolean getPartner_idDirtyFlag();

}
