package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_resend_cancel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_cancelSearchContext;

/**
 * 实体 [重发请求被拒绝] 存储对象
 */
public interface Mail_resend_cancelRepository extends Repository<Mail_resend_cancel> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_resend_cancel> searchDefault(Mail_resend_cancelSearchContext context);

    Mail_resend_cancel convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel domain , Mail_resend_cancel po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel convert2Domain( Mail_resend_cancel po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel domain) ;

}
