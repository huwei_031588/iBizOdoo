package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Event_confirm;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_confirmSearchContext;

/**
 * 实体 [活动确认] 存储对象
 */
public interface Event_confirmRepository extends Repository<Event_confirm> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Event_confirm> searchDefault(Event_confirmSearchContext context);

    Event_confirm convert2PO(cn.ibizlab.odoo.core.odoo_event.domain.Event_confirm domain , Event_confirm po) ;

    cn.ibizlab.odoo.core.odoo_event.domain.Event_confirm convert2Domain( Event_confirm po ,cn.ibizlab.odoo.core.odoo_event.domain.Event_confirm domain) ;

}
