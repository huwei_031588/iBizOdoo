package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_website.filter.Website_pageSearchContext;

/**
 * 实体 [页] 存储模型
 */
public interface Website_page{

    /**
     * 视图类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [视图类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 视图结构
     */
    String getArch();

    void setArch(String arch);

    /**
     * 获取 [视图结构]脏标记
     */
    boolean getArchDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 网站元说明
     */
    String getWebsite_meta_description();

    void setWebsite_meta_description(String website_meta_description);

    /**
     * 获取 [网站元说明]脏标记
     */
    boolean getWebsite_meta_descriptionDirtyFlag();

    /**
     * 视图名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [视图名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 网站网址
     */
    String getWebsite_url();

    void setWebsite_url(String website_url);

    /**
     * 获取 [网站网址]脏标记
     */
    boolean getWebsite_urlDirtyFlag();

    /**
     * 序号
     */
    Integer getPriority();

    void setPriority(Integer priority);

    /**
     * 获取 [序号]脏标记
     */
    boolean getPriorityDirtyFlag();

    /**
     * 外部 ID
     */
    String getXml_id();

    void setXml_id(String xml_id);

    /**
     * 获取 [外部 ID]脏标记
     */
    boolean getXml_idDirtyFlag();

    /**
     * 页面已索引
     */
    String getWebsite_indexed();

    void setWebsite_indexed(String website_indexed);

    /**
     * 获取 [页面已索引]脏标记
     */
    boolean getWebsite_indexedDirtyFlag();

    /**
     * 视图
     */
    Integer getView_id();

    void setView_id(Integer view_id);

    /**
     * 获取 [视图]脏标记
     */
    boolean getView_idDirtyFlag();

    /**
     * 主题模板
     */
    Integer getTheme_template_id();

    void setTheme_template_id(Integer theme_template_id);

    /**
     * 获取 [主题模板]脏标记
     */
    boolean getTheme_template_idDirtyFlag();

    /**
     * 网站opengraph图像
     */
    String getWebsite_meta_og_img();

    void setWebsite_meta_og_img(String website_meta_og_img);

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    boolean getWebsite_meta_og_imgDirtyFlag();

    /**
     * 作为可选继承显示
     */
    String getCustomize_show();

    void setCustomize_show(String customize_show);

    /**
     * 获取 [作为可选继承显示]脏标记
     */
    boolean getCustomize_showDirtyFlag();

    /**
     * 模型
     */
    String getModel();

    void setModel(String model);

    /**
     * 获取 [模型]脏标记
     */
    boolean getModelDirtyFlag();

    /**
     * 下级字段
     */
    String getField_parent();

    void setField_parent(String field_parent);

    /**
     * 获取 [下级字段]脏标记
     */
    boolean getField_parentDirtyFlag();

    /**
     * Arch Blob
     */
    String getArch_db();

    void setArch_db(String arch_db);

    /**
     * 获取 [Arch Blob]脏标记
     */
    boolean getArch_dbDirtyFlag();

    /**
     * 主页
     */
    String getIs_homepage();

    void setIs_homepage(String is_homepage);

    /**
     * 获取 [主页]脏标记
     */
    boolean getIs_homepageDirtyFlag();

    /**
     * 在当前网站显示
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [在当前网站显示]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 网站meta标题
     */
    String getWebsite_meta_title();

    void setWebsite_meta_title(String website_meta_title);

    /**
     * 获取 [网站meta标题]脏标记
     */
    boolean getWebsite_meta_titleDirtyFlag();

    /**
     * 模型数据
     */
    Integer getModel_data_id();

    void setModel_data_id(Integer model_data_id);

    /**
     * 获取 [模型数据]脏标记
     */
    boolean getModel_data_idDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 视图继承模式
     */
    String getMode();

    void setMode(String mode);

    /**
     * 获取 [视图继承模式]脏标记
     */
    boolean getModeDirtyFlag();

    /**
     * 相关菜单
     */
    String getMenu_ids();

    void setMenu_ids(String menu_ids);

    /**
     * 获取 [相关菜单]脏标记
     */
    boolean getMenu_idsDirtyFlag();

    /**
     * 继承于此的视图
     */
    String getInherit_children_ids();

    void setInherit_children_ids(String inherit_children_ids);

    /**
     * 获取 [继承于此的视图]脏标记
     */
    boolean getInherit_children_idsDirtyFlag();

    /**
     * 基础视图结构
     */
    String getArch_base();

    void setArch_base(String arch_base);

    /**
     * 获取 [基础视图结构]脏标记
     */
    boolean getArch_baseDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 可见
     */
    String getIs_visible();

    void setIs_visible(String is_visible);

    /**
     * 获取 [可见]脏标记
     */
    boolean getIs_visibleDirtyFlag();

    /**
     * 网站页面
     */
    Integer getFirst_page_id();

    void setFirst_page_id(Integer first_page_id);

    /**
     * 获取 [网站页面]脏标记
     */
    boolean getFirst_page_idDirtyFlag();

    /**
     * SEO优化
     */
    String getIs_seo_optimized();

    void setIs_seo_optimized(String is_seo_optimized);

    /**
     * 获取 [SEO优化]脏标记
     */
    boolean getIs_seo_optimizedDirtyFlag();

    /**
     * 标题颜色
     */
    String getHeader_color();

    void setHeader_color(String header_color);

    /**
     * 获取 [标题颜色]脏标记
     */
    boolean getHeader_colorDirtyFlag();

    /**
     * 已发布
     */
    String getIs_published();

    void setIs_published(String is_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getIs_publishedDirtyFlag();

    /**
     * 页
     */
    String getPage_ids();

    void setPage_ids(String page_ids);

    /**
     * 获取 [页]脏标记
     */
    boolean getPage_idsDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 页面 URL
     */
    String getUrl();

    void setUrl(String url);

    /**
     * 获取 [页面 URL]脏标记
     */
    boolean getUrlDirtyFlag();

    /**
     * 标题覆盖层
     */
    String getHeader_overlay();

    void setHeader_overlay(String header_overlay);

    /**
     * 获取 [标题覆盖层]脏标记
     */
    boolean getHeader_overlayDirtyFlag();

    /**
     * 网站meta关键词
     */
    String getWebsite_meta_keywords();

    void setWebsite_meta_keywords(String website_meta_keywords);

    /**
     * 获取 [网站meta关键词]脏标记
     */
    boolean getWebsite_meta_keywordsDirtyFlag();

    /**
     * 发布日期
     */
    Timestamp getDate_publish();

    void setDate_publish(Timestamp date_publish);

    /**
     * 获取 [发布日期]脏标记
     */
    boolean getDate_publishDirtyFlag();

    /**
     * 群组
     */
    String getGroups_id();

    void setGroups_id(String groups_id);

    /**
     * 获取 [群组]脏标记
     */
    boolean getGroups_idDirtyFlag();

    /**
     * 键
     */
    String getKey();

    void setKey(String key);

    /**
     * 获取 [键]脏标记
     */
    boolean getKeyDirtyFlag();

    /**
     * 继承的视图
     */
    Integer getInherit_id();

    void setInherit_id(Integer inherit_id);

    /**
     * 获取 [继承的视图]脏标记
     */
    boolean getInherit_idDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 模型
     */
    String getModel_ids();

    void setModel_ids(String model_ids);

    /**
     * 获取 [模型]脏标记
     */
    boolean getModel_idsDirtyFlag();

    /**
     * Arch 文件名
     */
    String getArch_fs();

    void setArch_fs(String arch_fs);

    /**
     * 获取 [Arch 文件名]脏标记
     */
    boolean getArch_fsDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
