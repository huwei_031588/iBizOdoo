package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_accountSearchContext;

/**
 * 实体 [科目] 存储模型
 */
public interface Account_account{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 期初借方
     */
    Double getOpening_debit();

    void setOpening_debit(Double opening_debit);

    /**
     * 获取 [期初借方]脏标记
     */
    boolean getOpening_debitDirtyFlag();

    /**
     * 内部备注
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [内部备注]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 期初贷方
     */
    Double getOpening_credit();

    void setOpening_credit(Double opening_credit);

    /**
     * 获取 [期初贷方]脏标记
     */
    boolean getOpening_creditDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 代码
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [代码]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 默认税
     */
    String getTax_ids();

    void setTax_ids(String tax_ids);

    /**
     * 获取 [默认税]脏标记
     */
    boolean getTax_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 废弃
     */
    String getDeprecated();

    void setDeprecated(String deprecated);

    /**
     * 获取 [废弃]脏标记
     */
    boolean getDeprecatedDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最近的发票和付款匹配时间
     */
    Timestamp getLast_time_entries_checked();

    void setLast_time_entries_checked(Timestamp last_time_entries_checked);

    /**
     * 获取 [最近的发票和付款匹配时间]脏标记
     */
    boolean getLast_time_entries_checkedDirtyFlag();

    /**
     * 允许核销
     */
    String getReconcile();

    void setReconcile(String reconcile);

    /**
     * 获取 [允许核销]脏标记
     */
    boolean getReconcileDirtyFlag();

    /**
     * 标签
     */
    String getTag_ids();

    void setTag_ids(String tag_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getTag_idsDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 内部类型
     */
    String getInternal_type();

    void setInternal_type(String internal_type);

    /**
     * 获取 [内部类型]脏标记
     */
    boolean getInternal_typeDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 科目币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [科目币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 内部群组
     */
    String getInternal_group();

    void setInternal_group(String internal_group);

    /**
     * 获取 [内部群组]脏标记
     */
    boolean getInternal_groupDirtyFlag();

    /**
     * 组
     */
    String getGroup_id_text();

    void setGroup_id_text(String group_id_text);

    /**
     * 获取 [组]脏标记
     */
    boolean getGroup_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 类型
     */
    String getUser_type_id_text();

    void setUser_type_id_text(String user_type_id_text);

    /**
     * 获取 [类型]脏标记
     */
    boolean getUser_type_id_textDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 组
     */
    Integer getGroup_id();

    void setGroup_id(Integer group_id);

    /**
     * 获取 [组]脏标记
     */
    boolean getGroup_idDirtyFlag();

    /**
     * 类型
     */
    Integer getUser_type_id();

    void setUser_type_id(Integer user_type_id);

    /**
     * 获取 [类型]脏标记
     */
    boolean getUser_type_idDirtyFlag();

    /**
     * 科目币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [科目币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
