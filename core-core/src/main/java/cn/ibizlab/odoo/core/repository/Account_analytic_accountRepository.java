package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_analytic_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_accountSearchContext;

/**
 * 实体 [分析账户] 存储对象
 */
public interface Account_analytic_accountRepository extends Repository<Account_analytic_account> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_analytic_account> searchDefault(Account_analytic_accountSearchContext context);

    Account_analytic_account convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account domain , Account_analytic_account po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account convert2Domain( Account_analytic_account po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account domain) ;

}
