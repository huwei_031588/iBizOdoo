package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mail_mass_mailing_tag;

/**
 * 实体[mail_mass_mailing_tag] 服务对象接口
 */
public interface mail_mass_mailing_tagRepository{


    public mail_mass_mailing_tag createPO() ;
        public List<mail_mass_mailing_tag> search();

        public void removeBatch(String id);

        public void remove(String id);

        public void updateBatch(mail_mass_mailing_tag mail_mass_mailing_tag);

        public void create(mail_mass_mailing_tag mail_mass_mailing_tag);

        public void update(mail_mass_mailing_tag mail_mass_mailing_tag);

        public void createBatch(mail_mass_mailing_tag mail_mass_mailing_tag);

        public void get(String id);


}
