package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_package_destination;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_destinationSearchContext;

/**
 * 实体 [包裹目的地] 存储对象
 */
public interface Stock_package_destinationRepository extends Repository<Stock_package_destination> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_package_destination> searchDefault(Stock_package_destinationSearchContext context);

    Stock_package_destination convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_destination domain , Stock_package_destination po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_destination convert2Domain( Stock_package_destination po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_destination domain) ;

}
