package cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_challenge;

import cn.ibizlab.odoo.core.odoo_gamification.valuerule.validator.gamification_challenge.Gamification_challengeReward_idDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Gamification_challenge
 * 属性：Reward_id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Gamification_challengeReward_idDefaultValidator.class})
public @interface Gamification_challengeReward_idDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
