package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivity_lossService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_workcenter_productivity_lossOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_workcenter_productivity_lossClientModel;

/**
 * 实体[工作中心生产力损失] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_workcenter_productivity_lossServiceImpl implements IMrp_workcenter_productivity_lossService {

    @Autowired
    mrp_workcenter_productivity_lossOdooClient mrp_workcenter_productivity_lossOdooClient;


    @Override
    public boolean create(Mrp_workcenter_productivity_loss et) {
        mrp_workcenter_productivity_lossClientModel clientModel = convert2Model(et,null);
		mrp_workcenter_productivity_lossOdooClient.create(clientModel);
        Mrp_workcenter_productivity_loss rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_workcenter_productivity_loss> list){
    }

    @Override
    public boolean remove(Integer id) {
        mrp_workcenter_productivity_lossClientModel clientModel = new mrp_workcenter_productivity_lossClientModel();
        clientModel.setId(id);
		mrp_workcenter_productivity_lossOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mrp_workcenter_productivity_loss get(Integer id) {
        mrp_workcenter_productivity_lossClientModel clientModel = new mrp_workcenter_productivity_lossClientModel();
        clientModel.setId(id);
		mrp_workcenter_productivity_lossOdooClient.get(clientModel);
        Mrp_workcenter_productivity_loss et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_workcenter_productivity_loss();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mrp_workcenter_productivity_loss et) {
        mrp_workcenter_productivity_lossClientModel clientModel = convert2Model(et,null);
		mrp_workcenter_productivity_lossOdooClient.update(clientModel);
        Mrp_workcenter_productivity_loss rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_workcenter_productivity_loss> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_workcenter_productivity_loss> searchDefault(Mrp_workcenter_productivity_lossSearchContext context) {
        List<Mrp_workcenter_productivity_loss> list = new ArrayList<Mrp_workcenter_productivity_loss>();
        Page<mrp_workcenter_productivity_lossClientModel> clientModelList = mrp_workcenter_productivity_lossOdooClient.search(context);
        for(mrp_workcenter_productivity_lossClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_workcenter_productivity_loss>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_workcenter_productivity_lossClientModel convert2Model(Mrp_workcenter_productivity_loss domain , mrp_workcenter_productivity_lossClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_workcenter_productivity_lossClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("manualdirtyflag"))
                model.setManual(domain.getManual());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("loss_typedirtyflag"))
                model.setLoss_type(domain.getLossType());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("loss_iddirtyflag"))
                model.setLoss_id(domain.getLossId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_workcenter_productivity_loss convert2Domain( mrp_workcenter_productivity_lossClientModel model ,Mrp_workcenter_productivity_loss domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_workcenter_productivity_loss();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getManualDirtyFlag())
            domain.setManual(model.getManual());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getLoss_typeDirtyFlag())
            domain.setLossType(model.getLoss_type());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getLoss_idDirtyFlag())
            domain.setLossId(model.getLoss_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



