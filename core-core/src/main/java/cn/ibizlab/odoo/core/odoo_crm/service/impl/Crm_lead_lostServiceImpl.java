package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_lostSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead_lostService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_crm.client.crm_lead_lostOdooClient;
import cn.ibizlab.odoo.core.odoo_crm.clientmodel.crm_lead_lostClientModel;

/**
 * 实体[获取丢失原因] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_lead_lostServiceImpl implements ICrm_lead_lostService {

    @Autowired
    crm_lead_lostOdooClient crm_lead_lostOdooClient;


    @Override
    public boolean remove(Integer id) {
        crm_lead_lostClientModel clientModel = new crm_lead_lostClientModel();
        clientModel.setId(id);
		crm_lead_lostOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Crm_lead_lost et) {
        crm_lead_lostClientModel clientModel = convert2Model(et,null);
		crm_lead_lostOdooClient.update(clientModel);
        Crm_lead_lost rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Crm_lead_lost> list){
    }

    @Override
    public Crm_lead_lost get(Integer id) {
        crm_lead_lostClientModel clientModel = new crm_lead_lostClientModel();
        clientModel.setId(id);
		crm_lead_lostOdooClient.get(clientModel);
        Crm_lead_lost et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Crm_lead_lost();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Crm_lead_lost et) {
        crm_lead_lostClientModel clientModel = convert2Model(et,null);
		crm_lead_lostOdooClient.create(clientModel);
        Crm_lead_lost rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_lead_lost> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_lead_lost> searchDefault(Crm_lead_lostSearchContext context) {
        List<Crm_lead_lost> list = new ArrayList<Crm_lead_lost>();
        Page<crm_lead_lostClientModel> clientModelList = crm_lead_lostOdooClient.search(context);
        for(crm_lead_lostClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Crm_lead_lost>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public crm_lead_lostClientModel convert2Model(Crm_lead_lost domain , crm_lead_lostClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new crm_lead_lostClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("lost_reason_id_textdirtyflag"))
                model.setLost_reason_id_text(domain.getLostReasonIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("lost_reason_iddirtyflag"))
                model.setLost_reason_id(domain.getLostReasonId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Crm_lead_lost convert2Domain( crm_lead_lostClientModel model ,Crm_lead_lost domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Crm_lead_lost();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getLost_reason_id_textDirtyFlag())
            domain.setLostReasonIdText(model.getLost_reason_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getLost_reason_idDirtyFlag())
            domain.setLostReasonId(model.getLost_reason_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



