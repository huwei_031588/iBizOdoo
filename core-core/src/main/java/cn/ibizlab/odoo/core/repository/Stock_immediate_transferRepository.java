package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_immediate_transfer;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_immediate_transferSearchContext;

/**
 * 实体 [立即调拨] 存储对象
 */
public interface Stock_immediate_transferRepository extends Repository<Stock_immediate_transfer> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_immediate_transfer> searchDefault(Stock_immediate_transferSearchContext context);

    Stock_immediate_transfer convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_immediate_transfer domain , Stock_immediate_transfer po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_immediate_transfer convert2Domain( Stock_immediate_transfer po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_immediate_transfer domain) ;

}
