package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_vehicle_tag;

/**
 * 实体[fleet_vehicle_tag] 服务对象接口
 */
public interface fleet_vehicle_tagRepository{


    public fleet_vehicle_tag createPO() ;
        public void createBatch(fleet_vehicle_tag fleet_vehicle_tag);

        public void removeBatch(String id);

        public List<fleet_vehicle_tag> search();

        public void remove(String id);

        public void create(fleet_vehicle_tag fleet_vehicle_tag);

        public void updateBatch(fleet_vehicle_tag fleet_vehicle_tag);

        public void update(fleet_vehicle_tag fleet_vehicle_tag);

        public void get(String id);


}
