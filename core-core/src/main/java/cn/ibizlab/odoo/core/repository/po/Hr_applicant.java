package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicantSearchContext;

/**
 * 实体 [申请人] 存储模型
 */
public interface Hr_applicant{

    /**
     * 评价
     */
    String getPriority();

    void setPriority(String priority);

    /**
     * 获取 [评价]脏标记
     */
    boolean getPriorityDirtyFlag();

    /**
     * 期望的薪酬
     */
    Double getSalary_expected();

    void setSalary_expected(Double salary_expected);

    /**
     * 获取 [期望的薪酬]脏标记
     */
    boolean getSalary_expectedDirtyFlag();

    /**
     * 标签
     */
    String getCateg_ids();

    void setCateg_ids(String categ_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getCateg_idsDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 提议薪酬
     */
    Double getSalary_proposed();

    void setSalary_proposed(Double salary_proposed);

    /**
     * 获取 [提议薪酬]脏标记
     */
    boolean getSalary_proposedDirtyFlag();

    /**
     * 附件数量
     */
    Integer getAttachment_number();

    void setAttachment_number(Integer attachment_number);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getAttachment_numberDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 信息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [信息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 电话
     */
    String getPartner_phone();

    void setPartner_phone(String partner_phone);

    /**
     * 获取 [电话]脏标记
     */
    boolean getPartner_phoneDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 看板状态
     */
    String getKanban_state();

    void setKanban_state(String kanban_state);

    /**
     * 获取 [看板状态]脏标记
     */
    boolean getKanban_stateDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 已指派
     */
    Timestamp getDate_open();

    void setDate_open(Timestamp date_open);

    /**
     * 获取 [已指派]脏标记
     */
    boolean getDate_openDirtyFlag();

    /**
     * 开启天数
     */
    Double getDay_open();

    void setDay_open(Double day_open);

    /**
     * 获取 [开启天数]脏标记
     */
    boolean getDay_openDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 可用量
     */
    Timestamp getAvailability();

    void setAvailability(Timestamp availability);

    /**
     * 获取 [可用量]脏标记
     */
    boolean getAvailabilityDirtyFlag();

    /**
     * 期望薪资
     */
    String getSalary_expected_extra();

    void setSalary_expected_extra(String salary_expected_extra);

    /**
     * 获取 [期望薪资]脏标记
     */
    boolean getSalary_expected_extraDirtyFlag();

    /**
     * 已关闭
     */
    Timestamp getDate_closed();

    void setDate_closed(Timestamp date_closed);

    /**
     * 获取 [已关闭]脏标记
     */
    boolean getDate_closedDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 手机
     */
    String getPartner_mobile();

    void setPartner_mobile(String partner_mobile);

    /**
     * 获取 [手机]脏标记
     */
    boolean getPartner_mobileDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 薪酬标准
     */
    String getSalary_proposed_extra();

    void setSalary_proposed_extra(String salary_proposed_extra);

    /**
     * 获取 [薪酬标准]脏标记
     */
    boolean getSalary_proposed_extraDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * EMail
     */
    String getEmail_from();

    void setEmail_from(String email_from);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getEmail_fromDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 主题/应用 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [主题/应用 名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 最后阶段更新
     */
    Timestamp getDate_last_stage_update();

    void setDate_last_stage_update(Timestamp date_last_stage_update);

    /**
     * 获取 [最后阶段更新]脏标记
     */
    boolean getDate_last_stage_updateDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 引荐于
     */
    String getReference();

    void setReference(String reference);

    /**
     * 获取 [引荐于]脏标记
     */
    boolean getReferenceDirtyFlag();

    /**
     * 附件
     */
    String getAttachment_ids();

    void setAttachment_ids(String attachment_ids);

    /**
     * 获取 [附件]脏标记
     */
    boolean getAttachment_idsDirtyFlag();

    /**
     * 关注者的EMail
     */
    String getEmail_cc();

    void setEmail_cc(String email_cc);

    /**
     * 获取 [关注者的EMail]脏标记
     */
    boolean getEmail_ccDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建日期
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建日期]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 申请人姓名
     */
    String getPartner_name();

    void setPartner_name(String partner_name);

    /**
     * 获取 [申请人姓名]脏标记
     */
    boolean getPartner_nameDirtyFlag();

    /**
     * 关闭日期
     */
    Double getDay_close();

    void setDay_close(Double day_close);

    /**
     * 获取 [关闭日期]脏标记
     */
    boolean getDay_closeDirtyFlag();

    /**
     * 概率
     */
    Double getProbability();

    void setProbability(Double probability);

    /**
     * 获取 [概率]脏标记
     */
    boolean getProbabilityDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 延迟关闭
     */
    Double getDelay_close();

    void setDelay_close(Double delay_close);

    /**
     * 获取 [延迟关闭]脏标记
     */
    boolean getDelay_closeDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 来源
     */
    String getSource_id_text();

    void setSource_id_text(String source_id_text);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_id_textDirtyFlag();

    /**
     * 营销
     */
    String getCampaign_id_text();

    void setCampaign_id_text(String campaign_id_text);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_id_textDirtyFlag();

    /**
     * 部门
     */
    String getDepartment_id_text();

    void setDepartment_id_text(String department_id_text);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_id_textDirtyFlag();

    /**
     * 最终阶段
     */
    String getLast_stage_id_text();

    void setLast_stage_id_text(String last_stage_id_text);

    /**
     * 获取 [最终阶段]脏标记
     */
    boolean getLast_stage_id_textDirtyFlag();

    /**
     * 阶段
     */
    String getStage_id_text();

    void setStage_id_text(String stage_id_text);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_id_textDirtyFlag();

    /**
     * 媒体
     */
    String getMedium_id_text();

    void setMedium_id_text(String medium_id_text);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_id_textDirtyFlag();

    /**
     * 看板进展
     */
    String getLegend_normal();

    void setLegend_normal(String legend_normal);

    /**
     * 获取 [看板进展]脏标记
     */
    boolean getLegend_normalDirtyFlag();

    /**
     * 负责人
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 学历
     */
    String getType_id_text();

    void setType_id_text(String type_id_text);

    /**
     * 获取 [学历]脏标记
     */
    boolean getType_id_textDirtyFlag();

    /**
     * 申请的职位
     */
    String getJob_id_text();

    void setJob_id_text(String job_id_text);

    /**
     * 获取 [申请的职位]脏标记
     */
    boolean getJob_id_textDirtyFlag();

    /**
     * 看板有效
     */
    String getLegend_done();

    void setLegend_done(String legend_done);

    /**
     * 获取 [看板有效]脏标记
     */
    boolean getLegend_doneDirtyFlag();

    /**
     * 看板阻塞
     */
    String getLegend_blocked();

    void setLegend_blocked(String legend_blocked);

    /**
     * 获取 [看板阻塞]脏标记
     */
    boolean getLegend_blockedDirtyFlag();

    /**
     * 员工姓名
     */
    String getEmployee_name();

    void setEmployee_name(String employee_name);

    /**
     * 获取 [员工姓名]脏标记
     */
    boolean getEmployee_nameDirtyFlag();

    /**
     * 用户EMail
     */
    String getUser_email();

    void setUser_email(String user_email);

    /**
     * 获取 [用户EMail]脏标记
     */
    boolean getUser_emailDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 联系
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [联系]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 申请的职位
     */
    Integer getJob_id();

    void setJob_id(Integer job_id);

    /**
     * 获取 [申请的职位]脏标记
     */
    boolean getJob_idDirtyFlag();

    /**
     * 营销
     */
    Integer getCampaign_id();

    void setCampaign_id(Integer campaign_id);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_idDirtyFlag();

    /**
     * 最终阶段
     */
    Integer getLast_stage_id();

    void setLast_stage_id(Integer last_stage_id);

    /**
     * 获取 [最终阶段]脏标记
     */
    boolean getLast_stage_idDirtyFlag();

    /**
     * 部门
     */
    Integer getDepartment_id();

    void setDepartment_id(Integer department_id);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_idDirtyFlag();

    /**
     * 联系
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [联系]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 媒体
     */
    Integer getMedium_id();

    void setMedium_id(Integer medium_id);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_idDirtyFlag();

    /**
     * 员工
     */
    Integer getEmp_id();

    void setEmp_id(Integer emp_id);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmp_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 学历
     */
    Integer getType_id();

    void setType_id(Integer type_id);

    /**
     * 获取 [学历]脏标记
     */
    boolean getType_idDirtyFlag();

    /**
     * 来源
     */
    Integer getSource_id();

    void setSource_id(Integer source_id);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_idDirtyFlag();

    /**
     * 阶段
     */
    Integer getStage_id();

    void setStage_id(Integer stage_id);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 负责人
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_idDirtyFlag();

}
