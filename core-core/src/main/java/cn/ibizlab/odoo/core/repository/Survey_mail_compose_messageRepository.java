package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Survey_mail_compose_message;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_mail_compose_messageSearchContext;

/**
 * 实体 [调查的功能EMail撰写向导] 存储对象
 */
public interface Survey_mail_compose_messageRepository extends Repository<Survey_mail_compose_message> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Survey_mail_compose_message> searchDefault(Survey_mail_compose_messageSearchContext context);

    Survey_mail_compose_message convert2PO(cn.ibizlab.odoo.core.odoo_survey.domain.Survey_mail_compose_message domain , Survey_mail_compose_message po) ;

    cn.ibizlab.odoo.core.odoo_survey.domain.Survey_mail_compose_message convert2Domain( Survey_mail_compose_message po ,cn.ibizlab.odoo.core.odoo_survey.domain.Survey_mail_compose_message domain) ;

}
