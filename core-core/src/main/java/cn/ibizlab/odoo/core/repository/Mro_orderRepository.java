package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_order;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_orderSearchContext;

/**
 * 实体 [Maintenance Order] 存储对象
 */
public interface Mro_orderRepository extends Repository<Mro_order> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_order> searchDefault(Mro_orderSearchContext context);

    Mro_order convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order domain , Mro_order po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order convert2Domain( Mro_order po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order domain) ;

}
