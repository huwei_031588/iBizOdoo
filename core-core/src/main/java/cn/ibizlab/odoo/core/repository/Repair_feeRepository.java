package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Repair_fee;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_feeSearchContext;

/**
 * 实体 [修理费] 存储对象
 */
public interface Repair_feeRepository extends Repository<Repair_fee> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Repair_fee> searchDefault(Repair_feeSearchContext context);

    Repair_fee convert2PO(cn.ibizlab.odoo.core.odoo_repair.domain.Repair_fee domain , Repair_fee po) ;

    cn.ibizlab.odoo.core.odoo_repair.domain.Repair_fee convert2Domain( Repair_fee po ,cn.ibizlab.odoo.core.odoo_repair.domain.Repair_fee domain) ;

}
