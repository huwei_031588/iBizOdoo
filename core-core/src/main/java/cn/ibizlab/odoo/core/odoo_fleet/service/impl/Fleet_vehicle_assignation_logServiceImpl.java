package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_assignation_log;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_assignation_logSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_assignation_logService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_assignation_logOdooClient;
import cn.ibizlab.odoo.core.odoo_fleet.clientmodel.fleet_vehicle_assignation_logClientModel;

/**
 * 实体[交通工具驾驶历史] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_assignation_logServiceImpl implements IFleet_vehicle_assignation_logService {

    @Autowired
    fleet_vehicle_assignation_logOdooClient fleet_vehicle_assignation_logOdooClient;


    @Override
    public Fleet_vehicle_assignation_log get(Integer id) {
        fleet_vehicle_assignation_logClientModel clientModel = new fleet_vehicle_assignation_logClientModel();
        clientModel.setId(id);
		fleet_vehicle_assignation_logOdooClient.get(clientModel);
        Fleet_vehicle_assignation_log et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Fleet_vehicle_assignation_log();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Fleet_vehicle_assignation_log et) {
        fleet_vehicle_assignation_logClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_assignation_logOdooClient.create(clientModel);
        Fleet_vehicle_assignation_log rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_assignation_log> list){
    }

    @Override
    public boolean remove(Integer id) {
        fleet_vehicle_assignation_logClientModel clientModel = new fleet_vehicle_assignation_logClientModel();
        clientModel.setId(id);
		fleet_vehicle_assignation_logOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Fleet_vehicle_assignation_log et) {
        fleet_vehicle_assignation_logClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_assignation_logOdooClient.update(clientModel);
        Fleet_vehicle_assignation_log rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Fleet_vehicle_assignation_log> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_assignation_log> searchDefault(Fleet_vehicle_assignation_logSearchContext context) {
        List<Fleet_vehicle_assignation_log> list = new ArrayList<Fleet_vehicle_assignation_log>();
        Page<fleet_vehicle_assignation_logClientModel> clientModelList = fleet_vehicle_assignation_logOdooClient.search(context);
        for(fleet_vehicle_assignation_logClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Fleet_vehicle_assignation_log>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public fleet_vehicle_assignation_logClientModel convert2Model(Fleet_vehicle_assignation_log domain , fleet_vehicle_assignation_logClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new fleet_vehicle_assignation_logClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("date_startdirtyflag"))
                model.setDate_start(domain.getDateStart());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("date_enddirtyflag"))
                model.setDate_end(domain.getDateEnd());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("vehicle_id_textdirtyflag"))
                model.setVehicle_id_text(domain.getVehicleIdText());
            if((Boolean) domain.getExtensionparams().get("driver_id_textdirtyflag"))
                model.setDriver_id_text(domain.getDriverIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("vehicle_iddirtyflag"))
                model.setVehicle_id(domain.getVehicleId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("driver_iddirtyflag"))
                model.setDriver_id(domain.getDriverId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Fleet_vehicle_assignation_log convert2Domain( fleet_vehicle_assignation_logClientModel model ,Fleet_vehicle_assignation_log domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Fleet_vehicle_assignation_log();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDate_startDirtyFlag())
            domain.setDateStart(model.getDate_start());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDate_endDirtyFlag())
            domain.setDateEnd(model.getDate_end());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getVehicle_id_textDirtyFlag())
            domain.setVehicleIdText(model.getVehicle_id_text());
        if(model.getDriver_id_textDirtyFlag())
            domain.setDriverIdText(model.getDriver_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getVehicle_idDirtyFlag())
            domain.setVehicleId(model.getVehicle_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getDriver_idDirtyFlag())
            domain.setDriverId(model.getDriver_id());
        return domain ;
    }

}

    



