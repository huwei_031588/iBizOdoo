package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_job;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_jobSearchContext;

/**
 * 实体 [工作岗位] 存储对象
 */
public interface Hr_jobRepository extends Repository<Hr_job> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_job> searchDefault(Hr_jobSearchContext context);

    Hr_job convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job domain , Hr_job po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job convert2Domain( Hr_job po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job domain) ;

}
