package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Web_editor_converter_test;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;

/**
 * 实体 [Web编辑器转换器测试] 存储对象
 */
public interface Web_editor_converter_testRepository extends Repository<Web_editor_converter_test> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Web_editor_converter_test> searchDefault(Web_editor_converter_testSearchContext context);

    Web_editor_converter_test convert2PO(cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test domain , Web_editor_converter_test po) ;

    cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test convert2Domain( Web_editor_converter_test po ,cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test domain) ;

}
