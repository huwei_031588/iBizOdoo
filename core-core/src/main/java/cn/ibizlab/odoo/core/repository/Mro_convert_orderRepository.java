package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_convert_order;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_convert_orderSearchContext;

/**
 * 实体 [Convert Order to Task] 存储对象
 */
public interface Mro_convert_orderRepository extends Repository<Mro_convert_order> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_convert_order> searchDefault(Mro_convert_orderSearchContext context);

    Mro_convert_order convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_convert_order domain , Mro_convert_order po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_convert_order convert2Domain( Mro_convert_order po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_convert_order domain) ;

}
