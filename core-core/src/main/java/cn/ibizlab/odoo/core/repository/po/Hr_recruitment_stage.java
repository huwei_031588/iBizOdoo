package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;

/**
 * 实体 [招聘阶段] 存储模型
 */
public interface Hr_recruitment_stage{

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 要求
     */
    String getRequirements();

    void setRequirements(String requirements);

    /**
     * 获取 [要求]脏标记
     */
    boolean getRequirementsDirtyFlag();

    /**
     * 灰色看板标签
     */
    String getLegend_normal();

    void setLegend_normal(String legend_normal);

    /**
     * 获取 [灰色看板标签]脏标记
     */
    boolean getLegend_normalDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 阶段名
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [阶段名]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 在招聘管道收起
     */
    String getFold();

    void setFold(String fold);

    /**
     * 获取 [在招聘管道收起]脏标记
     */
    boolean getFoldDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 绿色看板标签
     */
    String getLegend_done();

    void setLegend_done(String legend_done);

    /**
     * 获取 [绿色看板标签]脏标记
     */
    boolean getLegend_doneDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 红色的看板标签
     */
    String getLegend_blocked();

    void setLegend_blocked(String legend_blocked);

    /**
     * 获取 [红色的看板标签]脏标记
     */
    boolean getLegend_blockedDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 自动发邮件
     */
    String getTemplate_id_text();

    void setTemplate_id_text(String template_id_text);

    /**
     * 获取 [自动发邮件]脏标记
     */
    boolean getTemplate_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 具体职位
     */
    String getJob_id_text();

    void setJob_id_text(String job_id_text);

    /**
     * 获取 [具体职位]脏标记
     */
    boolean getJob_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 具体职位
     */
    Integer getJob_id();

    void setJob_id(Integer job_id);

    /**
     * 获取 [具体职位]脏标记
     */
    boolean getJob_idDirtyFlag();

    /**
     * 自动发邮件
     */
    Integer getTemplate_id();

    void setTemplate_id(Integer template_id);

    /**
     * 获取 [自动发邮件]脏标记
     */
    boolean getTemplate_idDirtyFlag();

}
