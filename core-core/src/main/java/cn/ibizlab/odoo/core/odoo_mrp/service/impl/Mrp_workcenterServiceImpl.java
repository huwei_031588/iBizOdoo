package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenterSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenterService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_workcenterOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_workcenterClientModel;

/**
 * 实体[工作中心] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_workcenterServiceImpl implements IMrp_workcenterService {

    @Autowired
    mrp_workcenterOdooClient mrp_workcenterOdooClient;


    @Override
    public boolean create(Mrp_workcenter et) {
        mrp_workcenterClientModel clientModel = convert2Model(et,null);
		mrp_workcenterOdooClient.create(clientModel);
        Mrp_workcenter rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_workcenter> list){
    }

    @Override
    public Mrp_workcenter get(Integer id) {
        mrp_workcenterClientModel clientModel = new mrp_workcenterClientModel();
        clientModel.setId(id);
		mrp_workcenterOdooClient.get(clientModel);
        Mrp_workcenter et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_workcenter();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mrp_workcenter et) {
        mrp_workcenterClientModel clientModel = convert2Model(et,null);
		mrp_workcenterOdooClient.update(clientModel);
        Mrp_workcenter rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_workcenter> list){
    }

    @Override
    public boolean remove(Integer id) {
        mrp_workcenterClientModel clientModel = new mrp_workcenterClientModel();
        clientModel.setId(id);
		mrp_workcenterOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_workcenter> searchDefault(Mrp_workcenterSearchContext context) {
        List<Mrp_workcenter> list = new ArrayList<Mrp_workcenter>();
        Page<mrp_workcenterClientModel> clientModelList = mrp_workcenterOdooClient.search(context);
        for(mrp_workcenterClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_workcenter>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_workcenterClientModel convert2Model(Mrp_workcenter domain , mrp_workcenterClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_workcenterClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("costs_hourdirtyflag"))
                model.setCosts_hour(domain.getCostsHour());
            if((Boolean) domain.getExtensionparams().get("workcenter_loaddirtyflag"))
                model.setWorkcenter_load(domain.getWorkcenterLoad());
            if((Boolean) domain.getExtensionparams().get("working_statedirtyflag"))
                model.setWorking_state(domain.getWorkingState());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("workorder_late_countdirtyflag"))
                model.setWorkorder_late_count(domain.getWorkorderLateCount());
            if((Boolean) domain.getExtensionparams().get("performancedirtyflag"))
                model.setPerformance(domain.getPerformance());
            if((Boolean) domain.getExtensionparams().get("oee_targetdirtyflag"))
                model.setOee_target(domain.getOeeTarget());
            if((Boolean) domain.getExtensionparams().get("capacitydirtyflag"))
                model.setCapacity(domain.getCapacity());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("workorder_countdirtyflag"))
                model.setWorkorder_count(domain.getWorkorderCount());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("time_idsdirtyflag"))
                model.setTime_ids(domain.getTimeIds());
            if((Boolean) domain.getExtensionparams().get("productive_timedirtyflag"))
                model.setProductive_time(domain.getProductiveTime());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("routing_line_idsdirtyflag"))
                model.setRouting_line_ids(domain.getRoutingLineIds());
            if((Boolean) domain.getExtensionparams().get("oeedirtyflag"))
                model.setOee(domain.getOee());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("time_stopdirtyflag"))
                model.setTime_stop(domain.getTimeStop());
            if((Boolean) domain.getExtensionparams().get("time_startdirtyflag"))
                model.setTime_start(domain.getTimeStart());
            if((Boolean) domain.getExtensionparams().get("workorder_pending_countdirtyflag"))
                model.setWorkorder_pending_count(domain.getWorkorderPendingCount());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("blocked_timedirtyflag"))
                model.setBlocked_time(domain.getBlockedTime());
            if((Boolean) domain.getExtensionparams().get("workorder_ready_countdirtyflag"))
                model.setWorkorder_ready_count(domain.getWorkorderReadyCount());
            if((Boolean) domain.getExtensionparams().get("workorder_progress_countdirtyflag"))
                model.setWorkorder_progress_count(domain.getWorkorderProgressCount());
            if((Boolean) domain.getExtensionparams().get("order_idsdirtyflag"))
                model.setOrder_ids(domain.getOrderIds());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("tzdirtyflag"))
                model.setTz(domain.getTz());
            if((Boolean) domain.getExtensionparams().get("time_efficiencydirtyflag"))
                model.setTime_efficiency(domain.getTimeEfficiency());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_id_textdirtyflag"))
                model.setResource_calendar_id_text(domain.getResourceCalendarIdText());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_iddirtyflag"))
                model.setResource_calendar_id(domain.getResourceCalendarId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("resource_iddirtyflag"))
                model.setResource_id(domain.getResourceId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_workcenter convert2Domain( mrp_workcenterClientModel model ,Mrp_workcenter domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_workcenter();
        }

        if(model.getCosts_hourDirtyFlag())
            domain.setCostsHour(model.getCosts_hour());
        if(model.getWorkcenter_loadDirtyFlag())
            domain.setWorkcenterLoad(model.getWorkcenter_load());
        if(model.getWorking_stateDirtyFlag())
            domain.setWorkingState(model.getWorking_state());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWorkorder_late_countDirtyFlag())
            domain.setWorkorderLateCount(model.getWorkorder_late_count());
        if(model.getPerformanceDirtyFlag())
            domain.setPerformance(model.getPerformance());
        if(model.getOee_targetDirtyFlag())
            domain.setOeeTarget(model.getOee_target());
        if(model.getCapacityDirtyFlag())
            domain.setCapacity(model.getCapacity());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWorkorder_countDirtyFlag())
            domain.setWorkorderCount(model.getWorkorder_count());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getTime_idsDirtyFlag())
            domain.setTimeIds(model.getTime_ids());
        if(model.getProductive_timeDirtyFlag())
            domain.setProductiveTime(model.getProductive_time());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getRouting_line_idsDirtyFlag())
            domain.setRoutingLineIds(model.getRouting_line_ids());
        if(model.getOeeDirtyFlag())
            domain.setOee(model.getOee());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getTime_stopDirtyFlag())
            domain.setTimeStop(model.getTime_stop());
        if(model.getTime_startDirtyFlag())
            domain.setTimeStart(model.getTime_start());
        if(model.getWorkorder_pending_countDirtyFlag())
            domain.setWorkorderPendingCount(model.getWorkorder_pending_count());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getBlocked_timeDirtyFlag())
            domain.setBlockedTime(model.getBlocked_time());
        if(model.getWorkorder_ready_countDirtyFlag())
            domain.setWorkorderReadyCount(model.getWorkorder_ready_count());
        if(model.getWorkorder_progress_countDirtyFlag())
            domain.setWorkorderProgressCount(model.getWorkorder_progress_count());
        if(model.getOrder_idsDirtyFlag())
            domain.setOrderIds(model.getOrder_ids());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getTzDirtyFlag())
            domain.setTz(model.getTz());
        if(model.getTime_efficiencyDirtyFlag())
            domain.setTimeEfficiency(model.getTime_efficiency());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getResource_calendar_id_textDirtyFlag())
            domain.setResourceCalendarIdText(model.getResource_calendar_id_text());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getResource_calendar_idDirtyFlag())
            domain.setResourceCalendarId(model.getResource_calendar_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getResource_idDirtyFlag())
            domain.setResourceId(model.getResource_id());
        return domain ;
    }

}

    



