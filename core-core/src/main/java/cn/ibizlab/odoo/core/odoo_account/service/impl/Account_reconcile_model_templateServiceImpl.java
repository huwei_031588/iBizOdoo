package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconcile_model_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_reconcile_model_templateOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_reconcile_model_templateClientModel;

/**
 * 实体[核销模型模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_reconcile_model_templateServiceImpl implements IAccount_reconcile_model_templateService {

    @Autowired
    account_reconcile_model_templateOdooClient account_reconcile_model_templateOdooClient;


    @Override
    public Account_reconcile_model_template get(Integer id) {
        account_reconcile_model_templateClientModel clientModel = new account_reconcile_model_templateClientModel();
        clientModel.setId(id);
		account_reconcile_model_templateOdooClient.get(clientModel);
        Account_reconcile_model_template et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_reconcile_model_template();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Account_reconcile_model_template et) {
        account_reconcile_model_templateClientModel clientModel = convert2Model(et,null);
		account_reconcile_model_templateOdooClient.update(clientModel);
        Account_reconcile_model_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_reconcile_model_template> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_reconcile_model_templateClientModel clientModel = new account_reconcile_model_templateClientModel();
        clientModel.setId(id);
		account_reconcile_model_templateOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_reconcile_model_template et) {
        account_reconcile_model_templateClientModel clientModel = convert2Model(et,null);
		account_reconcile_model_templateOdooClient.create(clientModel);
        Account_reconcile_model_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_reconcile_model_template> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_reconcile_model_template> searchDefault(Account_reconcile_model_templateSearchContext context) {
        List<Account_reconcile_model_template> list = new ArrayList<Account_reconcile_model_template>();
        Page<account_reconcile_model_templateClientModel> clientModelList = account_reconcile_model_templateOdooClient.search(context);
        for(account_reconcile_model_templateClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_reconcile_model_template>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_reconcile_model_templateClientModel convert2Model(Account_reconcile_model_template domain , account_reconcile_model_templateClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_reconcile_model_templateClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("match_amount_mindirtyflag"))
                model.setMatch_amount_min(domain.getMatchAmountMin());
            if((Boolean) domain.getExtensionparams().get("force_tax_includeddirtyflag"))
                model.setForce_tax_included(domain.getForceTaxIncluded());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("match_partner_category_idsdirtyflag"))
                model.setMatch_partner_category_ids(domain.getMatchPartnerCategoryIds());
            if((Boolean) domain.getExtensionparams().get("second_amount_typedirtyflag"))
                model.setSecond_amount_type(domain.getSecondAmountType());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("match_partner_idsdirtyflag"))
                model.setMatch_partner_ids(domain.getMatchPartnerIds());
            if((Boolean) domain.getExtensionparams().get("match_journal_idsdirtyflag"))
                model.setMatch_journal_ids(domain.getMatchJournalIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("labeldirtyflag"))
                model.setLabel(domain.getLabel());
            if((Boolean) domain.getExtensionparams().get("match_total_amount_paramdirtyflag"))
                model.setMatch_total_amount_param(domain.getMatchTotalAmountParam());
            if((Boolean) domain.getExtensionparams().get("rule_typedirtyflag"))
                model.setRule_type(domain.getRuleType());
            if((Boolean) domain.getExtensionparams().get("match_amountdirtyflag"))
                model.setMatch_amount(domain.getMatchAmount());
            if((Boolean) domain.getExtensionparams().get("second_amountdirtyflag"))
                model.setSecond_amount(domain.getSecondAmount());
            if((Boolean) domain.getExtensionparams().get("amount_typedirtyflag"))
                model.setAmount_type(domain.getAmountType());
            if((Boolean) domain.getExtensionparams().get("match_same_currencydirtyflag"))
                model.setMatch_same_currency(domain.getMatchSameCurrency());
            if((Boolean) domain.getExtensionparams().get("match_amount_maxdirtyflag"))
                model.setMatch_amount_max(domain.getMatchAmountMax());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("auto_reconciledirtyflag"))
                model.setAuto_reconcile(domain.getAutoReconcile());
            if((Boolean) domain.getExtensionparams().get("match_total_amountdirtyflag"))
                model.setMatch_total_amount(domain.getMatchTotalAmount());
            if((Boolean) domain.getExtensionparams().get("has_second_linedirtyflag"))
                model.setHas_second_line(domain.getHasSecondLine());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("match_naturedirtyflag"))
                model.setMatch_nature(domain.getMatchNature());
            if((Boolean) domain.getExtensionparams().get("second_labeldirtyflag"))
                model.setSecond_label(domain.getSecondLabel());
            if((Boolean) domain.getExtensionparams().get("force_second_tax_includeddirtyflag"))
                model.setForce_second_tax_included(domain.getForceSecondTaxIncluded());
            if((Boolean) domain.getExtensionparams().get("match_label_paramdirtyflag"))
                model.setMatch_label_param(domain.getMatchLabelParam());
            if((Boolean) domain.getExtensionparams().get("match_labeldirtyflag"))
                model.setMatch_label(domain.getMatchLabel());
            if((Boolean) domain.getExtensionparams().get("match_partnerdirtyflag"))
                model.setMatch_partner(domain.getMatchPartner());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("tax_id_textdirtyflag"))
                model.setTax_id_text(domain.getTaxIdText());
            if((Boolean) domain.getExtensionparams().get("second_tax_id_textdirtyflag"))
                model.setSecond_tax_id_text(domain.getSecondTaxIdText());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("chart_template_id_textdirtyflag"))
                model.setChart_template_id_text(domain.getChartTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("second_account_id_textdirtyflag"))
                model.setSecond_account_id_text(domain.getSecondAccountIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("second_account_iddirtyflag"))
                model.setSecond_account_id(domain.getSecondAccountId());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("chart_template_iddirtyflag"))
                model.setChart_template_id(domain.getChartTemplateId());
            if((Boolean) domain.getExtensionparams().get("second_tax_iddirtyflag"))
                model.setSecond_tax_id(domain.getSecondTaxId());
            if((Boolean) domain.getExtensionparams().get("tax_iddirtyflag"))
                model.setTax_id(domain.getTaxId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_reconcile_model_template convert2Domain( account_reconcile_model_templateClientModel model ,Account_reconcile_model_template domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_reconcile_model_template();
        }

        if(model.getMatch_amount_minDirtyFlag())
            domain.setMatchAmountMin(model.getMatch_amount_min());
        if(model.getForce_tax_includedDirtyFlag())
            domain.setForceTaxIncluded(model.getForce_tax_included());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMatch_partner_category_idsDirtyFlag())
            domain.setMatchPartnerCategoryIds(model.getMatch_partner_category_ids());
        if(model.getSecond_amount_typeDirtyFlag())
            domain.setSecondAmountType(model.getSecond_amount_type());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getMatch_partner_idsDirtyFlag())
            domain.setMatchPartnerIds(model.getMatch_partner_ids());
        if(model.getMatch_journal_idsDirtyFlag())
            domain.setMatchJournalIds(model.getMatch_journal_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getLabelDirtyFlag())
            domain.setLabel(model.getLabel());
        if(model.getMatch_total_amount_paramDirtyFlag())
            domain.setMatchTotalAmountParam(model.getMatch_total_amount_param());
        if(model.getRule_typeDirtyFlag())
            domain.setRuleType(model.getRule_type());
        if(model.getMatch_amountDirtyFlag())
            domain.setMatchAmount(model.getMatch_amount());
        if(model.getSecond_amountDirtyFlag())
            domain.setSecondAmount(model.getSecond_amount());
        if(model.getAmount_typeDirtyFlag())
            domain.setAmountType(model.getAmount_type());
        if(model.getMatch_same_currencyDirtyFlag())
            domain.setMatchSameCurrency(model.getMatch_same_currency());
        if(model.getMatch_amount_maxDirtyFlag())
            domain.setMatchAmountMax(model.getMatch_amount_max());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getAuto_reconcileDirtyFlag())
            domain.setAutoReconcile(model.getAuto_reconcile());
        if(model.getMatch_total_amountDirtyFlag())
            domain.setMatchTotalAmount(model.getMatch_total_amount());
        if(model.getHas_second_lineDirtyFlag())
            domain.setHasSecondLine(model.getHas_second_line());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMatch_natureDirtyFlag())
            domain.setMatchNature(model.getMatch_nature());
        if(model.getSecond_labelDirtyFlag())
            domain.setSecondLabel(model.getSecond_label());
        if(model.getForce_second_tax_includedDirtyFlag())
            domain.setForceSecondTaxIncluded(model.getForce_second_tax_included());
        if(model.getMatch_label_paramDirtyFlag())
            domain.setMatchLabelParam(model.getMatch_label_param());
        if(model.getMatch_labelDirtyFlag())
            domain.setMatchLabel(model.getMatch_label());
        if(model.getMatch_partnerDirtyFlag())
            domain.setMatchPartner(model.getMatch_partner());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getTax_id_textDirtyFlag())
            domain.setTaxIdText(model.getTax_id_text());
        if(model.getSecond_tax_id_textDirtyFlag())
            domain.setSecondTaxIdText(model.getSecond_tax_id_text());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getChart_template_id_textDirtyFlag())
            domain.setChartTemplateIdText(model.getChart_template_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getSecond_account_id_textDirtyFlag())
            domain.setSecondAccountIdText(model.getSecond_account_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getSecond_account_idDirtyFlag())
            domain.setSecondAccountId(model.getSecond_account_id());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getChart_template_idDirtyFlag())
            domain.setChartTemplateId(model.getChart_template_id());
        if(model.getSecond_tax_idDirtyFlag())
            domain.setSecondTaxId(model.getSecond_tax_id());
        if(model.getTax_idDirtyFlag())
            domain.setTaxId(model.getTax_id());
        return domain ;
    }

}

    



