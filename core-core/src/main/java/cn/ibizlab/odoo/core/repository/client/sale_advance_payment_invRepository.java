package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.sale_advance_payment_inv;

/**
 * 实体[sale_advance_payment_inv] 服务对象接口
 */
public interface sale_advance_payment_invRepository{


    public sale_advance_payment_inv createPO() ;
        public void create(sale_advance_payment_inv sale_advance_payment_inv);

        public void removeBatch(String id);

        public void updateBatch(sale_advance_payment_inv sale_advance_payment_inv);

        public void get(String id);

        public void remove(String id);

        public void update(sale_advance_payment_inv sale_advance_payment_inv);

        public List<sale_advance_payment_inv> search();

        public void createBatch(sale_advance_payment_inv sale_advance_payment_inv);


}
