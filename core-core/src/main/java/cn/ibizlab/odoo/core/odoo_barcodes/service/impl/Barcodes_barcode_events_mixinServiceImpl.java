package cn.ibizlab.odoo.core.odoo_barcodes.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.odoo.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_barcodes.service.IBarcodes_barcode_events_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_barcodes.client.barcodes_barcode_events_mixinOdooClient;
import cn.ibizlab.odoo.core.odoo_barcodes.clientmodel.barcodes_barcode_events_mixinClientModel;

/**
 * 实体[条形码事件混合] 服务对象接口实现
 */
@Slf4j
@Service
public class Barcodes_barcode_events_mixinServiceImpl implements IBarcodes_barcode_events_mixinService {

    @Autowired
    barcodes_barcode_events_mixinOdooClient barcodes_barcode_events_mixinOdooClient;


    @Override
    public boolean create(Barcodes_barcode_events_mixin et) {
        barcodes_barcode_events_mixinClientModel clientModel = convert2Model(et,null);
		barcodes_barcode_events_mixinOdooClient.create(clientModel);
        Barcodes_barcode_events_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Barcodes_barcode_events_mixin> list){
    }

    @Override
    public boolean update(Barcodes_barcode_events_mixin et) {
        barcodes_barcode_events_mixinClientModel clientModel = convert2Model(et,null);
		barcodes_barcode_events_mixinOdooClient.update(clientModel);
        Barcodes_barcode_events_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Barcodes_barcode_events_mixin> list){
    }

    @Override
    public boolean remove(Integer id) {
        barcodes_barcode_events_mixinClientModel clientModel = new barcodes_barcode_events_mixinClientModel();
        clientModel.setId(id);
		barcodes_barcode_events_mixinOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Barcodes_barcode_events_mixin get(Integer id) {
        barcodes_barcode_events_mixinClientModel clientModel = new barcodes_barcode_events_mixinClientModel();
        clientModel.setId(id);
		barcodes_barcode_events_mixinOdooClient.get(clientModel);
        Barcodes_barcode_events_mixin et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Barcodes_barcode_events_mixin();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Barcodes_barcode_events_mixin> searchDefault(Barcodes_barcode_events_mixinSearchContext context) {
        List<Barcodes_barcode_events_mixin> list = new ArrayList<Barcodes_barcode_events_mixin>();
        Page<barcodes_barcode_events_mixinClientModel> clientModelList = barcodes_barcode_events_mixinOdooClient.search(context);
        for(barcodes_barcode_events_mixinClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Barcodes_barcode_events_mixin>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public barcodes_barcode_events_mixinClientModel convert2Model(Barcodes_barcode_events_mixin domain , barcodes_barcode_events_mixinClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new barcodes_barcode_events_mixinClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("_barcode_scanneddirtyflag"))
                model.set_barcode_scanned(domain.getBarcodeScanned());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Barcodes_barcode_events_mixin convert2Domain( barcodes_barcode_events_mixinClientModel model ,Barcodes_barcode_events_mixin domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Barcodes_barcode_events_mixin();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.get_barcode_scannedDirtyFlag())
            domain.setBarcodeScanned(model.get_barcode_scanned());
        return domain ;
    }

}

    



