package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_import_tests_models_float;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_floatSearchContext;

/**
 * 实体 [测试:基本导入模型浮动] 存储对象
 */
public interface Base_import_tests_models_floatRepository extends Repository<Base_import_tests_models_float> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_import_tests_models_float> searchDefault(Base_import_tests_models_floatSearchContext context);

    Base_import_tests_models_float convert2PO(cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_float domain , Base_import_tests_models_float po) ;

    cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_float convert2Domain( Base_import_tests_models_float po ,cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_float domain) ;

}
