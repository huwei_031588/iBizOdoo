package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_paymentSearchContext;


/**
 * 实体[Account_payment] 服务对象接口
 */
public interface IAccount_paymentService{

    boolean update(Account_payment et) ;
    void updateBatch(List<Account_payment> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_payment et) ;
    void createBatch(List<Account_payment> list) ;
    Account_payment get(Integer key) ;
    Page<Account_payment> searchDefault(Account_paymentSearchContext context) ;

}



