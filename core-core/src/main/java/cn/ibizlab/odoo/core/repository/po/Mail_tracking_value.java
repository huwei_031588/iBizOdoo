package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_tracking_valueSearchContext;

/**
 * 实体 [邮件跟踪值] 存储模型
 */
public interface Mail_tracking_value{

    /**
     * 字段说明
     */
    String getField_desc();

    void setField_desc(String field_desc);

    /**
     * 获取 [字段说明]脏标记
     */
    boolean getField_descDirtyFlag();

    /**
     * 旧字符值
     */
    String getOld_value_char();

    void setOld_value_char(String old_value_char);

    /**
     * 获取 [旧字符值]脏标记
     */
    boolean getOld_value_charDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 新日期时间值
     */
    Timestamp getNew_value_datetime();

    void setNew_value_datetime(Timestamp new_value_datetime);

    /**
     * 获取 [新日期时间值]脏标记
     */
    boolean getNew_value_datetimeDirtyFlag();

    /**
     * 旧货币值
     */
    Double getOld_value_monetary();

    void setOld_value_monetary(Double old_value_monetary);

    /**
     * 获取 [旧货币值]脏标记
     */
    boolean getOld_value_monetaryDirtyFlag();

    /**
     * 新字符值
     */
    String getNew_value_char();

    void setNew_value_char(String new_value_char);

    /**
     * 获取 [新字符值]脏标记
     */
    boolean getNew_value_charDirtyFlag();

    /**
     * 新文本值
     */
    String getNew_value_text();

    void setNew_value_text(String new_value_text);

    /**
     * 获取 [新文本值]脏标记
     */
    boolean getNew_value_textDirtyFlag();

    /**
     * 跟踪字段次序
     */
    Integer getTrack_sequence();

    void setTrack_sequence(Integer track_sequence);

    /**
     * 获取 [跟踪字段次序]脏标记
     */
    boolean getTrack_sequenceDirtyFlag();

    /**
     * 新货币值
     */
    Double getNew_value_monetary();

    void setNew_value_monetary(Double new_value_monetary);

    /**
     * 获取 [新货币值]脏标记
     */
    boolean getNew_value_monetaryDirtyFlag();

    /**
     * 旧日期时间值
     */
    Timestamp getOld_value_datetime();

    void setOld_value_datetime(Timestamp old_value_datetime);

    /**
     * 获取 [旧日期时间值]脏标记
     */
    boolean getOld_value_datetimeDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 旧整数值
     */
    Integer getOld_value_integer();

    void setOld_value_integer(Integer old_value_integer);

    /**
     * 获取 [旧整数值]脏标记
     */
    boolean getOld_value_integerDirtyFlag();

    /**
     * 旧文本值
     */
    String getOld_value_text();

    void setOld_value_text(String old_value_text);

    /**
     * 获取 [旧文本值]脏标记
     */
    boolean getOld_value_textDirtyFlag();

    /**
     * 字段类型
     */
    String getField_type();

    void setField_type(String field_type);

    /**
     * 获取 [字段类型]脏标记
     */
    boolean getField_typeDirtyFlag();

    /**
     * 新整数值
     */
    Integer getNew_value_integer();

    void setNew_value_integer(Integer new_value_integer);

    /**
     * 获取 [新整数值]脏标记
     */
    boolean getNew_value_integerDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 新浮点值
     */
    Double getNew_value_float();

    void setNew_value_float(Double new_value_float);

    /**
     * 获取 [新浮点值]脏标记
     */
    boolean getNew_value_floatDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 更改的字段
     */
    String getField();

    void setField(String field);

    /**
     * 获取 [更改的字段]脏标记
     */
    boolean getFieldDirtyFlag();

    /**
     * 旧浮点值
     */
    Double getOld_value_float();

    void setOld_value_float(Double old_value_float);

    /**
     * 获取 [旧浮点值]脏标记
     */
    boolean getOld_value_floatDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 邮件消息ID
     */
    Integer getMail_message_id();

    void setMail_message_id(Integer mail_message_id);

    /**
     * 获取 [邮件消息ID]脏标记
     */
    boolean getMail_message_idDirtyFlag();

}
