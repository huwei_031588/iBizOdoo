package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badgeSearchContext;

/**
 * 实体 [游戏化徽章] 存储模型
 */
public interface Gamification_badge{

    /**
     * 允许授予
     */
    String getRule_auth();

    void setRule_auth(String rule_auth);

    /**
     * 获取 [允许授予]脏标记
     */
    boolean getRule_authDirtyFlag();

    /**
     * 限制数量
     */
    Integer getRule_max_number();

    void setRule_max_number(Integer rule_max_number);

    /**
     * 获取 [限制数量]脏标记
     */
    boolean getRule_max_numberDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 月度限额发放
     */
    String getRule_max();

    void setRule_max(String rule_max);

    /**
     * 获取 [月度限额发放]脏标记
     */
    boolean getRule_maxDirtyFlag();

    /**
     * 授权用户
     */
    String getRule_auth_user_ids();

    void setRule_auth_user_ids(String rule_auth_user_ids);

    /**
     * 获取 [授权用户]脏标记
     */
    boolean getRule_auth_user_idsDirtyFlag();

    /**
     * 需要徽章
     */
    String getRule_auth_badge_ids();

    void setRule_auth_badge_ids(String rule_auth_badge_ids);

    /**
     * 获取 [需要徽章]脏标记
     */
    boolean getRule_auth_badge_idsDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 其他的允许发送
     */
    Integer getRemaining_sending();

    void setRemaining_sending(Integer remaining_sending);

    /**
     * 获取 [其他的允许发送]脏标记
     */
    boolean getRemaining_sendingDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 我的总计
     */
    Integer getStat_my();

    void setStat_my(Integer stat_my);

    /**
     * 获取 [我的总计]脏标记
     */
    boolean getStat_myDirtyFlag();

    /**
     * 图像
     */
    byte[] getImage();

    void setImage(byte[] image);

    /**
     * 获取 [图像]脏标记
     */
    boolean getImageDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 唯一的所有者
     */
    String getUnique_owner_ids();

    void setUnique_owner_ids(String unique_owner_ids);

    /**
     * 获取 [唯一的所有者]脏标记
     */
    boolean getUnique_owner_idsDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 论坛徽章等级
     */
    String getLevel();

    void setLevel(String level);

    /**
     * 获取 [论坛徽章等级]脏标记
     */
    boolean getLevelDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 所有者
     */
    String getOwner_ids();

    void setOwner_ids(String owner_ids);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_idsDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 奖励按照
     */
    String getGoal_definition_ids();

    void setGoal_definition_ids(String goal_definition_ids);

    /**
     * 获取 [奖励按照]脏标记
     */
    boolean getGoal_definition_idsDirtyFlag();

    /**
     * 月度发放总数
     */
    Integer getStat_my_monthly_sending();

    void setStat_my_monthly_sending(Integer stat_my_monthly_sending);

    /**
     * 获取 [月度发放总数]脏标记
     */
    boolean getStat_my_monthly_sendingDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 总计
     */
    Integer getStat_count();

    void setStat_count(Integer stat_count);

    /**
     * 获取 [总计]脏标记
     */
    boolean getStat_countDirtyFlag();

    /**
     * 徽章
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [徽章]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 每月总数
     */
    Integer getStat_this_month();

    void setStat_this_month(Integer stat_this_month);

    /**
     * 获取 [每月总数]脏标记
     */
    boolean getStat_this_monthDirtyFlag();

    /**
     * 需要激活
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要激活]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 用户数量
     */
    Integer getStat_count_distinct();

    void setStat_count_distinct(Integer stat_count_distinct);

    /**
     * 获取 [用户数量]脏标记
     */
    boolean getStat_count_distinctDirtyFlag();

    /**
     * 我的月份总计
     */
    Integer getStat_my_this_month();

    void setStat_my_this_month(Integer stat_my_this_month);

    /**
     * 获取 [我的月份总计]脏标记
     */
    boolean getStat_my_this_monthDirtyFlag();

    /**
     * 挑战的奖励
     */
    String getChallenge_ids();

    void setChallenge_ids(String challenge_ids);

    /**
     * 获取 [挑战的奖励]脏标记
     */
    boolean getChallenge_idsDirtyFlag();

    /**
     * 授予的员工人数
     */
    Integer getGranted_employees_count();

    void setGranted_employees_count(Integer granted_employees_count);

    /**
     * 获取 [授予的员工人数]脏标记
     */
    boolean getGranted_employees_countDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
