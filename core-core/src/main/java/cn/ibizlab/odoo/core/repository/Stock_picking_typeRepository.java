package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_picking_type;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_picking_typeSearchContext;

/**
 * 实体 [拣货类型] 存储对象
 */
public interface Stock_picking_typeRepository extends Repository<Stock_picking_type> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_picking_type> searchDefault(Stock_picking_typeSearchContext context);

    Stock_picking_type convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type domain , Stock_picking_type po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type convert2Domain( Stock_picking_type po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type domain) ;

}
