package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_exclusion;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_exclusionSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_template_attribute_exclusionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_template_attribute_exclusionOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_template_attribute_exclusionClientModel;

/**
 * 实体[产品模板属性排除] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_template_attribute_exclusionServiceImpl implements IProduct_template_attribute_exclusionService {

    @Autowired
    product_template_attribute_exclusionOdooClient product_template_attribute_exclusionOdooClient;


    @Override
    public boolean create(Product_template_attribute_exclusion et) {
        product_template_attribute_exclusionClientModel clientModel = convert2Model(et,null);
		product_template_attribute_exclusionOdooClient.create(clientModel);
        Product_template_attribute_exclusion rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_template_attribute_exclusion> list){
    }

    @Override
    public Product_template_attribute_exclusion get(Integer id) {
        product_template_attribute_exclusionClientModel clientModel = new product_template_attribute_exclusionClientModel();
        clientModel.setId(id);
		product_template_attribute_exclusionOdooClient.get(clientModel);
        Product_template_attribute_exclusion et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_template_attribute_exclusion();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Product_template_attribute_exclusion et) {
        product_template_attribute_exclusionClientModel clientModel = convert2Model(et,null);
		product_template_attribute_exclusionOdooClient.update(clientModel);
        Product_template_attribute_exclusion rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_template_attribute_exclusion> list){
    }

    @Override
    public boolean remove(Integer id) {
        product_template_attribute_exclusionClientModel clientModel = new product_template_attribute_exclusionClientModel();
        clientModel.setId(id);
		product_template_attribute_exclusionOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_template_attribute_exclusion> searchDefault(Product_template_attribute_exclusionSearchContext context) {
        List<Product_template_attribute_exclusion> list = new ArrayList<Product_template_attribute_exclusion>();
        Page<product_template_attribute_exclusionClientModel> clientModelList = product_template_attribute_exclusionOdooClient.search(context);
        for(product_template_attribute_exclusionClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_template_attribute_exclusion>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_template_attribute_exclusionClientModel convert2Model(Product_template_attribute_exclusion domain , product_template_attribute_exclusionClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_template_attribute_exclusionClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("value_idsdirtyflag"))
                model.setValue_ids(domain.getValueIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_id_textdirtyflag"))
                model.setProduct_tmpl_id_text(domain.getProductTmplIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_template_attribute_value_id_textdirtyflag"))
                model.setProduct_template_attribute_value_id_text(domain.getProductTemplateAttributeValueIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("product_template_attribute_value_iddirtyflag"))
                model.setProduct_template_attribute_value_id(domain.getProductTemplateAttributeValueId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_template_attribute_exclusion convert2Domain( product_template_attribute_exclusionClientModel model ,Product_template_attribute_exclusion domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_template_attribute_exclusion();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getValue_idsDirtyFlag())
            domain.setValueIds(model.getValue_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getProduct_tmpl_id_textDirtyFlag())
            domain.setProductTmplIdText(model.getProduct_tmpl_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_template_attribute_value_id_textDirtyFlag())
            domain.setProductTemplateAttributeValueIdText(model.getProduct_template_attribute_value_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getProduct_template_attribute_value_idDirtyFlag())
            domain.setProductTemplateAttributeValueId(model.getProduct_template_attribute_value_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



