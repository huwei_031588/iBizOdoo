package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_company;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_companySearchContext;

/**
 * 实体 [公司] 存储对象
 */
public interface Res_companyRepository extends Repository<Res_company> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_company> searchDefault(Res_companySearchContext context);

    Res_company convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_company domain , Res_company po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_company convert2Domain( Res_company po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_company domain) ;

}
