package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_parameter;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_parameterSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_parameterService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_parameterOdooClient;
import cn.ibizlab.odoo.core.odoo_mro.clientmodel.mro_pm_parameterClientModel;

/**
 * 实体[Asset Parameters] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_parameterServiceImpl implements IMro_pm_parameterService {

    @Autowired
    mro_pm_parameterOdooClient mro_pm_parameterOdooClient;


    @Override
    public boolean update(Mro_pm_parameter et) {
        mro_pm_parameterClientModel clientModel = convert2Model(et,null);
		mro_pm_parameterOdooClient.update(clientModel);
        Mro_pm_parameter rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mro_pm_parameter> list){
    }

    @Override
    public boolean remove(Integer id) {
        mro_pm_parameterClientModel clientModel = new mro_pm_parameterClientModel();
        clientModel.setId(id);
		mro_pm_parameterOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mro_pm_parameter et) {
        mro_pm_parameterClientModel clientModel = convert2Model(et,null);
		mro_pm_parameterOdooClient.create(clientModel);
        Mro_pm_parameter rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_parameter> list){
    }

    @Override
    public Mro_pm_parameter get(Integer id) {
        mro_pm_parameterClientModel clientModel = new mro_pm_parameterClientModel();
        clientModel.setId(id);
		mro_pm_parameterOdooClient.get(clientModel);
        Mro_pm_parameter et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mro_pm_parameter();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_parameter> searchDefault(Mro_pm_parameterSearchContext context) {
        List<Mro_pm_parameter> list = new ArrayList<Mro_pm_parameter>();
        Page<mro_pm_parameterClientModel> clientModelList = mro_pm_parameterOdooClient.search(context);
        for(mro_pm_parameterClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mro_pm_parameter>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mro_pm_parameterClientModel convert2Model(Mro_pm_parameter domain , mro_pm_parameterClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mro_pm_parameterClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("parameter_uom_textdirtyflag"))
                model.setParameter_uom_text(domain.getParameterUomText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("parameter_uomdirtyflag"))
                model.setParameter_uom(domain.getParameterUom());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mro_pm_parameter convert2Domain( mro_pm_parameterClientModel model ,Mro_pm_parameter domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mro_pm_parameter();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getParameter_uom_textDirtyFlag())
            domain.setParameterUomText(model.getParameter_uom_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getParameter_uomDirtyFlag())
            domain.setParameterUom(model.getParameter_uom());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



