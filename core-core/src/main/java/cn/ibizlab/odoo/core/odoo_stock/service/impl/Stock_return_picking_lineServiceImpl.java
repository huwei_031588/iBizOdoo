package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_picking_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_return_picking_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_return_picking_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_return_picking_lineClientModel;

/**
 * 实体[退料明细行] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_return_picking_lineServiceImpl implements IStock_return_picking_lineService {

    @Autowired
    stock_return_picking_lineOdooClient stock_return_picking_lineOdooClient;


    @Override
    public boolean update(Stock_return_picking_line et) {
        stock_return_picking_lineClientModel clientModel = convert2Model(et,null);
		stock_return_picking_lineOdooClient.update(clientModel);
        Stock_return_picking_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_return_picking_line> list){
    }

    @Override
    public boolean create(Stock_return_picking_line et) {
        stock_return_picking_lineClientModel clientModel = convert2Model(et,null);
		stock_return_picking_lineOdooClient.create(clientModel);
        Stock_return_picking_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_return_picking_line> list){
    }

    @Override
    public Stock_return_picking_line get(Integer id) {
        stock_return_picking_lineClientModel clientModel = new stock_return_picking_lineClientModel();
        clientModel.setId(id);
		stock_return_picking_lineOdooClient.get(clientModel);
        Stock_return_picking_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_return_picking_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        stock_return_picking_lineClientModel clientModel = new stock_return_picking_lineClientModel();
        clientModel.setId(id);
		stock_return_picking_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_return_picking_line> searchDefault(Stock_return_picking_lineSearchContext context) {
        List<Stock_return_picking_line> list = new ArrayList<Stock_return_picking_line>();
        Page<stock_return_picking_lineClientModel> clientModelList = stock_return_picking_lineOdooClient.search(context);
        for(stock_return_picking_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_return_picking_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_return_picking_lineClientModel convert2Model(Stock_return_picking_line domain , stock_return_picking_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_return_picking_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("quantitydirtyflag"))
                model.setQuantity(domain.getQuantity());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("to_refunddirtyflag"))
                model.setTo_refund(domain.getToRefund());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("move_id_textdirtyflag"))
                model.setMove_id_text(domain.getMoveIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("uom_iddirtyflag"))
                model.setUom_id(domain.getUomId());
            if((Boolean) domain.getExtensionparams().get("move_iddirtyflag"))
                model.setMove_id(domain.getMoveId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("wizard_iddirtyflag"))
                model.setWizard_id(domain.getWizardId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_return_picking_line convert2Domain( stock_return_picking_lineClientModel model ,Stock_return_picking_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_return_picking_line();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getQuantityDirtyFlag())
            domain.setQuantity(model.getQuantity());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getTo_refundDirtyFlag())
            domain.setToRefund(model.getTo_refund());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getMove_id_textDirtyFlag())
            domain.setMoveIdText(model.getMove_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getUom_idDirtyFlag())
            domain.setUomId(model.getUom_id());
        if(model.getMove_idDirtyFlag())
            domain.setMoveId(model.getMove_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getWizard_idDirtyFlag())
            domain.setWizardId(model.getWizard_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



