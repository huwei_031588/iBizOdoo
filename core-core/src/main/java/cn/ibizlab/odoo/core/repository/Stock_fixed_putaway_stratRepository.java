package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_fixed_putaway_strat;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;

/**
 * 实体 [位置固定上架策略] 存储对象
 */
public interface Stock_fixed_putaway_stratRepository extends Repository<Stock_fixed_putaway_strat> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_fixed_putaway_strat> searchDefault(Stock_fixed_putaway_stratSearchContext context);

    Stock_fixed_putaway_strat convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_fixed_putaway_strat domain , Stock_fixed_putaway_strat po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_fixed_putaway_strat convert2Domain( Stock_fixed_putaway_strat po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_fixed_putaway_strat domain) ;

}
