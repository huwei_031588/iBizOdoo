package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_invoice;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoiceSearchContext;

/**
 * 实体 [发票] 存储对象
 */
public interface Account_invoiceRepository extends Repository<Account_invoice> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_invoice> searchDefault(Account_invoiceSearchContext context);

    Account_invoice convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice domain , Account_invoice po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice convert2Domain( Account_invoice po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice domain) ;

}
