package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channelSearchContext;


/**
 * 实体[Mail_channel] 服务对象接口
 */
public interface IMail_channelService{

    boolean update(Mail_channel et) ;
    void updateBatch(List<Mail_channel> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mail_channel et) ;
    void createBatch(List<Mail_channel> list) ;
    Mail_channel get(Integer key) ;
    Page<Mail_channel> searchDefault(Mail_channelSearchContext context) ;

}



