package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Payment_icon;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_iconSearchContext;

/**
 * 实体 [支付图标] 存储对象
 */
public interface Payment_iconRepository extends Repository<Payment_icon> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Payment_icon> searchDefault(Payment_iconSearchContext context);

    Payment_icon convert2PO(cn.ibizlab.odoo.core.odoo_payment.domain.Payment_icon domain , Payment_icon po) ;

    cn.ibizlab.odoo.core.odoo_payment.domain.Payment_icon convert2Domain( Payment_icon po ,cn.ibizlab.odoo.core.odoo_payment.domain.Payment_icon domain) ;

}
