package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Board_board;
import cn.ibizlab.odoo.core.odoo_board.filter.Board_boardSearchContext;

/**
 * 实体 [仪表板] 存储对象
 */
public interface Board_boardRepository extends Repository<Board_board> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Board_board> searchDefault(Board_boardSearchContext context);

    Board_board convert2PO(cn.ibizlab.odoo.core.odoo_board.domain.Board_board domain , Board_board po) ;

    cn.ibizlab.odoo.core.odoo_board.domain.Board_board convert2Domain( Board_board po ,cn.ibizlab.odoo.core.odoo_board.domain.Board_board domain) ;

}
