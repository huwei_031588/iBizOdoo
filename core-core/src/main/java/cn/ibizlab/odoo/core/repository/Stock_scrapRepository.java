package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_scrap;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scrapSearchContext;

/**
 * 实体 [报废] 存储对象
 */
public interface Stock_scrapRepository extends Repository<Stock_scrap> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_scrap> searchDefault(Stock_scrapSearchContext context);

    Stock_scrap convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap domain , Stock_scrap po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap convert2Domain( Stock_scrap po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap domain) ;

}
