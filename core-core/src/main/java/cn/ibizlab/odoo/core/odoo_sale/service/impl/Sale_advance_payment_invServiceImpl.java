package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_advance_payment_inv;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_advance_payment_invService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_sale.client.sale_advance_payment_invOdooClient;
import cn.ibizlab.odoo.core.odoo_sale.clientmodel.sale_advance_payment_invClientModel;

/**
 * 实体[销售预付款发票] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_advance_payment_invServiceImpl implements ISale_advance_payment_invService {

    @Autowired
    sale_advance_payment_invOdooClient sale_advance_payment_invOdooClient;


    @Override
    public boolean remove(Integer id) {
        sale_advance_payment_invClientModel clientModel = new sale_advance_payment_invClientModel();
        clientModel.setId(id);
		sale_advance_payment_invOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Sale_advance_payment_inv et) {
        sale_advance_payment_invClientModel clientModel = convert2Model(et,null);
		sale_advance_payment_invOdooClient.update(clientModel);
        Sale_advance_payment_inv rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Sale_advance_payment_inv> list){
    }

    @Override
    public Sale_advance_payment_inv get(Integer id) {
        sale_advance_payment_invClientModel clientModel = new sale_advance_payment_invClientModel();
        clientModel.setId(id);
		sale_advance_payment_invOdooClient.get(clientModel);
        Sale_advance_payment_inv et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Sale_advance_payment_inv();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Sale_advance_payment_inv et) {
        sale_advance_payment_invClientModel clientModel = convert2Model(et,null);
		sale_advance_payment_invOdooClient.create(clientModel);
        Sale_advance_payment_inv rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_advance_payment_inv> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_advance_payment_inv> searchDefault(Sale_advance_payment_invSearchContext context) {
        List<Sale_advance_payment_inv> list = new ArrayList<Sale_advance_payment_inv>();
        Page<sale_advance_payment_invClientModel> clientModelList = sale_advance_payment_invOdooClient.search(context);
        for(sale_advance_payment_invClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Sale_advance_payment_inv>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public sale_advance_payment_invClientModel convert2Model(Sale_advance_payment_inv domain , sale_advance_payment_invClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new sale_advance_payment_invClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("deposit_taxes_iddirtyflag"))
                model.setDeposit_taxes_id(domain.getDepositTaxesId());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("countdirtyflag"))
                model.setCount(domain.getCount());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("advance_payment_methoddirtyflag"))
                model.setAdvance_payment_method(domain.getAdvancePaymentMethod());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("deposit_account_id_textdirtyflag"))
                model.setDeposit_account_id_text(domain.getDepositAccountIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("deposit_account_iddirtyflag"))
                model.setDeposit_account_id(domain.getDepositAccountId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Sale_advance_payment_inv convert2Domain( sale_advance_payment_invClientModel model ,Sale_advance_payment_inv domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Sale_advance_payment_inv();
        }

        if(model.getDeposit_taxes_idDirtyFlag())
            domain.setDepositTaxesId(model.getDeposit_taxes_id());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getCountDirtyFlag())
            domain.setCount(model.getCount());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAdvance_payment_methodDirtyFlag())
            domain.setAdvancePaymentMethod(model.getAdvance_payment_method());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getDeposit_account_id_textDirtyFlag())
            domain.setDepositAccountIdText(model.getDeposit_account_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getDeposit_account_idDirtyFlag())
            domain.setDepositAccountId(model.getDeposit_account_id());
        return domain ;
    }

}

    



