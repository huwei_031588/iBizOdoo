package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_production;

/**
 * 实体[mrp_production] 服务对象接口
 */
public interface mrp_productionRepository{


    public mrp_production createPO() ;
        public void removeBatch(String id);

        public void createBatch(mrp_production mrp_production);

        public void create(mrp_production mrp_production);

        public void updateBatch(mrp_production mrp_production);

        public void get(String id);

        public List<mrp_production> search();

        public void remove(String id);

        public void update(mrp_production mrp_production);


}
