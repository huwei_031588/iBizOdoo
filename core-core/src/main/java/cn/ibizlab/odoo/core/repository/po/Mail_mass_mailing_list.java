package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;

/**
 * 实体 [邮件列表] 存储模型
 */
public interface Mail_mass_mailing_list{

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 联系人人数
     */
    Integer getContact_nbr();

    void setContact_nbr(Integer contact_nbr);

    /**
     * 获取 [联系人人数]脏标记
     */
    boolean getContact_nbrDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 订阅信息
     */
    String getSubscription_contact_ids();

    void setSubscription_contact_ids(String subscription_contact_ids);

    /**
     * 获取 [订阅信息]脏标记
     */
    boolean getSubscription_contact_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 邮件列表
     */
    String getContact_ids();

    void setContact_ids(String contact_ids);

    /**
     * 获取 [邮件列表]脏标记
     */
    boolean getContact_idsDirtyFlag();

    /**
     * 网站弹出内容
     */
    String getPopup_content();

    void setPopup_content(String popup_content);

    /**
     * 获取 [网站弹出内容]脏标记
     */
    boolean getPopup_contentDirtyFlag();

    /**
     * 公开
     */
    String getIs_public();

    void setIs_public(String is_public);

    /**
     * 获取 [公开]脏标记
     */
    boolean getIs_publicDirtyFlag();

    /**
     * 邮件列表
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [邮件列表]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 网站弹出重新定向网址
     */
    String getPopup_redirect_url();

    void setPopup_redirect_url(String popup_redirect_url);

    /**
     * 获取 [网站弹出重新定向网址]脏标记
     */
    boolean getPopup_redirect_urlDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
