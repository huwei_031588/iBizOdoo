package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.survey_question;

/**
 * 实体[survey_question] 服务对象接口
 */
public interface survey_questionRepository{


    public survey_question createPO() ;
        public List<survey_question> search();

        public void remove(String id);

        public void update(survey_question survey_question);

        public void get(String id);

        public void create(survey_question survey_question);

        public void createBatch(survey_question survey_question);

        public void updateBatch(survey_question survey_question);

        public void removeBatch(String id);


}
