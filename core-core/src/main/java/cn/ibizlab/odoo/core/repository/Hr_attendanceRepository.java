package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_attendance;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_attendanceSearchContext;

/**
 * 实体 [出勤] 存储对象
 */
public interface Hr_attendanceRepository extends Repository<Hr_attendance> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_attendance> searchDefault(Hr_attendanceSearchContext context);

    Hr_attendance convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance domain , Hr_attendance po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance convert2Domain( Hr_attendance po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance domain) ;

}
