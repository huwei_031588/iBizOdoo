package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Barcodes_barcode_events_mixin;
import cn.ibizlab.odoo.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;

/**
 * 实体 [条形码事件混合] 存储对象
 */
public interface Barcodes_barcode_events_mixinRepository extends Repository<Barcodes_barcode_events_mixin> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Barcodes_barcode_events_mixin> searchDefault(Barcodes_barcode_events_mixinSearchContext context);

    Barcodes_barcode_events_mixin convert2PO(cn.ibizlab.odoo.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin domain , Barcodes_barcode_events_mixin po) ;

    cn.ibizlab.odoo.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin convert2Domain( Barcodes_barcode_events_mixin po ,cn.ibizlab.odoo.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin domain) ;

}
