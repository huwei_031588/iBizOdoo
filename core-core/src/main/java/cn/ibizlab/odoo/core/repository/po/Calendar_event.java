package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_eventSearchContext;

/**
 * 实体 [活动] 存储模型
 */
public interface Calendar_event{

    /**
     * 地点
     */
    String getLocation();

    void setLocation(String location);

    /**
     * 获取 [地点]脏标记
     */
    boolean getLocationDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 循环ID日期
     */
    Timestamp getRecurrent_id_date();

    void setRecurrent_id_date(Timestamp recurrent_id_date);

    /**
     * 获取 [循环ID日期]脏标记
     */
    boolean getRecurrent_id_dateDirtyFlag();

    /**
     * 周六
     */
    String getSa();

    void setSa(String sa);

    /**
     * 获取 [周六]脏标记
     */
    boolean getSaDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 循环
     */
    String getRecurrency();

    void setRecurrency(String recurrency);

    /**
     * 获取 [循环]脏标记
     */
    boolean getRecurrencyDirtyFlag();

    /**
     * 重复
     */
    Integer getCount();

    void setCount(Integer count);

    /**
     * 获取 [重复]脏标记
     */
    boolean getCountDirtyFlag();

    /**
     * 周五
     */
    String getFr();

    void setFr(String fr);

    /**
     * 获取 [周五]脏标记
     */
    boolean getFrDirtyFlag();

    /**
     * 循环规则
     */
    String getRrule();

    void setRrule(String rrule);

    /**
     * 获取 [循环规则]脏标记
     */
    boolean getRruleDirtyFlag();

    /**
     * 选项
     */
    String getMonth_by();

    void setMonth_by(String month_by);

    /**
     * 获取 [选项]脏标记
     */
    boolean getMonth_byDirtyFlag();

    /**
     * 工作日
     */
    String getWeek_list();

    void setWeek_list(String week_list);

    /**
     * 获取 [工作日]脏标记
     */
    boolean getWeek_listDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 停止
     */
    Timestamp getStop();

    void setStop(Timestamp stop);

    /**
     * 获取 [停止]脏标记
     */
    boolean getStopDirtyFlag();

    /**
     * 文档ID
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [文档ID]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 循环ID
     */
    Integer getRecurrent_id();

    void setRecurrent_id(Integer recurrent_id);

    /**
     * 获取 [循环ID]脏标记
     */
    boolean getRecurrent_idDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 重复终止
     */
    String getEnd_type();

    void setEnd_type(String end_type);

    /**
     * 获取 [重复终止]脏标记
     */
    boolean getEnd_typeDirtyFlag();

    /**
     * 参与者
     */
    String getAttendee_ids();

    void setAttendee_ids(String attendee_ids);

    /**
     * 获取 [参与者]脏标记
     */
    boolean getAttendee_idsDirtyFlag();

    /**
     * 出席者状态
     */
    String getAttendee_status();

    void setAttendee_status(String attendee_status);

    /**
     * 获取 [出席者状态]脏标记
     */
    boolean getAttendee_statusDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 重新提起
     */
    String getRrule_type();

    void setRrule_type(String rrule_type);

    /**
     * 获取 [重新提起]脏标记
     */
    boolean getRrule_typeDirtyFlag();

    /**
     * 重复
     */
    Integer getInterval();

    void setInterval(Integer interval);

    /**
     * 获取 [重复]脏标记
     */
    boolean getIntervalDirtyFlag();

    /**
     * 隐私
     */
    String getPrivacy();

    void setPrivacy(String privacy);

    /**
     * 获取 [隐私]脏标记
     */
    boolean getPrivacyDirtyFlag();

    /**
     * 持续时间
     */
    Double getDuration();

    void setDuration(Double duration);

    /**
     * 获取 [持续时间]脏标记
     */
    boolean getDurationDirtyFlag();

    /**
     * 开始时间
     */
    Timestamp getStart_datetime();

    void setStart_datetime(Timestamp start_datetime);

    /**
     * 获取 [开始时间]脏标记
     */
    boolean getStart_datetimeDirtyFlag();

    /**
     * 出席者
     */
    String getIs_attendee();

    void setIs_attendee(String is_attendee);

    /**
     * 获取 [出席者]脏标记
     */
    boolean getIs_attendeeDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getStart_date();

    void setStart_date(Timestamp start_date);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getStart_dateDirtyFlag();

    /**
     * 周一
     */
    String getMo();

    void setMo(String mo);

    /**
     * 获取 [周一]脏标记
     */
    boolean getMoDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 周三
     */
    String getWe();

    void setWe(String we);

    /**
     * 获取 [周三]脏标记
     */
    boolean getWeDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 活动时间
     */
    String getDisplay_time();

    void setDisplay_time(String display_time);

    /**
     * 获取 [活动时间]脏标记
     */
    boolean getDisplay_timeDirtyFlag();

    /**
     * 日期
     */
    String getDisplay_start();

    void setDisplay_start(String display_start);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDisplay_startDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getStop_date();

    void setStop_date(Timestamp stop_date);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getStop_dateDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 重复直到
     */
    Timestamp getFinal_date();

    void setFinal_date(Timestamp final_date);

    /**
     * 获取 [重复直到]脏标记
     */
    boolean getFinal_dateDirtyFlag();

    /**
     * 文档模型
     */
    Integer getRes_model_id();

    void setRes_model_id(Integer res_model_id);

    /**
     * 获取 [文档模型]脏标记
     */
    boolean getRes_model_idDirtyFlag();

    /**
     * 事件是否突出显示
     */
    String getIs_highlighted();

    void setIs_highlighted(String is_highlighted);

    /**
     * 获取 [事件是否突出显示]脏标记
     */
    boolean getIs_highlightedDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 结束日期时间
     */
    Timestamp getStop_datetime();

    void setStop_datetime(Timestamp stop_datetime);

    /**
     * 获取 [结束日期时间]脏标记
     */
    boolean getStop_datetimeDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 文档模型名称
     */
    String getRes_model();

    void setRes_model(String res_model);

    /**
     * 获取 [文档模型名称]脏标记
     */
    boolean getRes_modelDirtyFlag();

    /**
     * 周二
     */
    String getTu();

    void setTu(String tu);

    /**
     * 获取 [周二]脏标记
     */
    boolean getTuDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示时间为
     */
    String getShow_as();

    void setShow_as(String show_as);

    /**
     * 获取 [显示时间为]脏标记
     */
    boolean getShow_asDirtyFlag();

    /**
     * 标签
     */
    String getCateg_ids();

    void setCateg_ids(String categ_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getCateg_idsDirtyFlag();

    /**
     * 提醒
     */
    String getAlarm_ids();

    void setAlarm_ids(String alarm_ids);

    /**
     * 获取 [提醒]脏标记
     */
    boolean getAlarm_idsDirtyFlag();

    /**
     * 周四
     */
    String getTh();

    void setTh(String th);

    /**
     * 获取 [周四]脏标记
     */
    boolean getThDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 按 天
     */
    String getByday();

    void setByday(String byday);

    /**
     * 获取 [按 天]脏标记
     */
    boolean getBydayDirtyFlag();

    /**
     * 日期
     */
    Integer getDay();

    void setDay(Integer day);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDayDirtyFlag();

    /**
     * 开始
     */
    Timestamp getStart();

    void setStart(Timestamp start);

    /**
     * 获取 [开始]脏标记
     */
    boolean getStartDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 与会者
     */
    String getPartner_ids();

    void setPartner_ids(String partner_ids);

    /**
     * 获取 [与会者]脏标记
     */
    boolean getPartner_idsDirtyFlag();

    /**
     * 全天
     */
    String getAllday();

    void setAllday(String allday);

    /**
     * 获取 [全天]脏标记
     */
    boolean getAlldayDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 周日
     */
    String getSu();

    void setSu(String su);

    /**
     * 获取 [周日]脏标记
     */
    boolean getSuDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 会议主题
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [会议主题]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 商机
     */
    String getOpportunity_id_text();

    void setOpportunity_id_text(String opportunity_id_text);

    /**
     * 获取 [商机]脏标记
     */
    boolean getOpportunity_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 所有者
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 负责人
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 申请人
     */
    String getApplicant_id_text();

    void setApplicant_id_text(String applicant_id_text);

    /**
     * 获取 [申请人]脏标记
     */
    boolean getApplicant_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 申请人
     */
    Integer getApplicant_id();

    void setApplicant_id(Integer applicant_id);

    /**
     * 获取 [申请人]脏标记
     */
    boolean getApplicant_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 所有者
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 商机
     */
    Integer getOpportunity_id();

    void setOpportunity_id(Integer opportunity_id);

    /**
     * 获取 [商机]脏标记
     */
    boolean getOpportunity_idDirtyFlag();

}
