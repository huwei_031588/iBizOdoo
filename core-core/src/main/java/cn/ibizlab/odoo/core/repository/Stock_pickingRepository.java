package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_picking;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_pickingSearchContext;

/**
 * 实体 [调拨] 存储对象
 */
public interface Stock_pickingRepository extends Repository<Stock_picking> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_picking> searchDefault(Stock_pickingSearchContext context);

    Stock_picking convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking domain , Stock_picking po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking convert2Domain( Stock_picking po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking domain) ;

}
