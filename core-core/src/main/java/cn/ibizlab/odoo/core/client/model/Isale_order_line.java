package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [sale_order_line] 对象
 */
public interface Isale_order_line {

    /**
     * 获取 [分析明细行]
     */
    public void setAnalytic_line_ids(String analytic_line_ids);
    
    /**
     * 设置 [分析明细行]
     */
    public String getAnalytic_line_ids();

    /**
     * 获取 [分析明细行]脏标记
     */
    public boolean getAnalytic_line_idsDirtyFlag();
    /**
     * 获取 [分析标签]
     */
    public void setAnalytic_tag_ids(String analytic_tag_ids);
    
    /**
     * 设置 [分析标签]
     */
    public String getAnalytic_tag_ids();

    /**
     * 获取 [分析标签]脏标记
     */
    public boolean getAnalytic_tag_idsDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [交货提前时间]
     */
    public void setCustomer_lead(Double customer_lead);
    
    /**
     * 设置 [交货提前时间]
     */
    public Double getCustomer_lead();

    /**
     * 获取 [交货提前时间]脏标记
     */
    public boolean getCustomer_leadDirtyFlag();
    /**
     * 获取 [折扣(%)]
     */
    public void setDiscount(Double discount);
    
    /**
     * 设置 [折扣(%)]
     */
    public Double getDiscount();

    /**
     * 获取 [折扣(%)]脏标记
     */
    public boolean getDiscountDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [显示类型]
     */
    public void setDisplay_type(String display_type);
    
    /**
     * 设置 [显示类型]
     */
    public String getDisplay_type();

    /**
     * 获取 [显示类型]脏标记
     */
    public boolean getDisplay_typeDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setEvent_id(Integer event_id);
    
    /**
     * 设置 [活动]
     */
    public Integer getEvent_id();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getEvent_idDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setEvent_id_text(String event_id_text);
    
    /**
     * 设置 [活动]
     */
    public String getEvent_id_text();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getEvent_id_textDirtyFlag();
    /**
     * 获取 [是一张活动票吗？]
     */
    public void setEvent_ok(String event_ok);
    
    /**
     * 设置 [是一张活动票吗？]
     */
    public String getEvent_ok();

    /**
     * 获取 [是一张活动票吗？]脏标记
     */
    public boolean getEvent_okDirtyFlag();
    /**
     * 获取 [活动入场券]
     */
    public void setEvent_ticket_id(Integer event_ticket_id);
    
    /**
     * 设置 [活动入场券]
     */
    public Integer getEvent_ticket_id();

    /**
     * 获取 [活动入场券]脏标记
     */
    public boolean getEvent_ticket_idDirtyFlag();
    /**
     * 获取 [活动入场券]
     */
    public void setEvent_ticket_id_text(String event_ticket_id_text);
    
    /**
     * 设置 [活动入场券]
     */
    public String getEvent_ticket_id_text();

    /**
     * 获取 [活动入场券]脏标记
     */
    public boolean getEvent_ticket_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [发票行]
     */
    public void setInvoice_lines(String invoice_lines);
    
    /**
     * 设置 [发票行]
     */
    public String getInvoice_lines();

    /**
     * 获取 [发票行]脏标记
     */
    public boolean getInvoice_linesDirtyFlag();
    /**
     * 获取 [发票状态]
     */
    public void setInvoice_status(String invoice_status);
    
    /**
     * 设置 [发票状态]
     */
    public String getInvoice_status();

    /**
     * 获取 [发票状态]脏标记
     */
    public boolean getInvoice_statusDirtyFlag();
    /**
     * 获取 [是首付款吗？]
     */
    public void setIs_downpayment(String is_downpayment);
    
    /**
     * 设置 [是首付款吗？]
     */
    public String getIs_downpayment();

    /**
     * 获取 [是首付款吗？]脏标记
     */
    public boolean getIs_downpaymentDirtyFlag();
    /**
     * 获取 [可报销]
     */
    public void setIs_expense(String is_expense);
    
    /**
     * 设置 [可报销]
     */
    public String getIs_expense();

    /**
     * 获取 [可报销]脏标记
     */
    public boolean getIs_expenseDirtyFlag();
    /**
     * 获取 [链接的订单明细]
     */
    public void setLinked_line_id(Integer linked_line_id);
    
    /**
     * 设置 [链接的订单明细]
     */
    public Integer getLinked_line_id();

    /**
     * 获取 [链接的订单明细]脏标记
     */
    public boolean getLinked_line_idDirtyFlag();
    /**
     * 获取 [链接的订单明细]
     */
    public void setLinked_line_id_text(String linked_line_id_text);
    
    /**
     * 设置 [链接的订单明细]
     */
    public String getLinked_line_id_text();

    /**
     * 获取 [链接的订单明细]脏标记
     */
    public boolean getLinked_line_id_textDirtyFlag();
    /**
     * 获取 [库存移动]
     */
    public void setMove_ids(String move_ids);
    
    /**
     * 设置 [库存移动]
     */
    public String getMove_ids();

    /**
     * 获取 [库存移动]脏标记
     */
    public boolean getMove_idsDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setName(String name);
    
    /**
     * 设置 [说明]
     */
    public String getName();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [姓名简称]
     */
    public void setName_short(String name_short);
    
    /**
     * 设置 [姓名简称]
     */
    public String getName_short();

    /**
     * 获取 [姓名简称]脏标记
     */
    public boolean getName_shortDirtyFlag();
    /**
     * 获取 [链接选项]
     */
    public void setOption_line_ids(String option_line_ids);
    
    /**
     * 设置 [链接选项]
     */
    public String getOption_line_ids();

    /**
     * 获取 [链接选项]脏标记
     */
    public boolean getOption_line_idsDirtyFlag();
    /**
     * 获取 [订单关联]
     */
    public void setOrder_id(Integer order_id);
    
    /**
     * 设置 [订单关联]
     */
    public Integer getOrder_id();

    /**
     * 获取 [订单关联]脏标记
     */
    public boolean getOrder_idDirtyFlag();
    /**
     * 获取 [订单关联]
     */
    public void setOrder_id_text(String order_id_text);
    
    /**
     * 设置 [订单关联]
     */
    public String getOrder_id_text();

    /**
     * 获取 [订单关联]脏标记
     */
    public boolean getOrder_id_textDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setOrder_partner_id(Integer order_partner_id);
    
    /**
     * 设置 [客户]
     */
    public Integer getOrder_partner_id();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getOrder_partner_idDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setOrder_partner_id_text(String order_partner_id_text);
    
    /**
     * 设置 [客户]
     */
    public String getOrder_partner_id_text();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getOrder_partner_id_textDirtyFlag();
    /**
     * 获取 [降价]
     */
    public void setPrice_reduce(Double price_reduce);
    
    /**
     * 设置 [降价]
     */
    public Double getPrice_reduce();

    /**
     * 获取 [降价]脏标记
     */
    public boolean getPrice_reduceDirtyFlag();
    /**
     * 获取 [不含税价]
     */
    public void setPrice_reduce_taxexcl(Double price_reduce_taxexcl);
    
    /**
     * 设置 [不含税价]
     */
    public Double getPrice_reduce_taxexcl();

    /**
     * 获取 [不含税价]脏标记
     */
    public boolean getPrice_reduce_taxexclDirtyFlag();
    /**
     * 获取 [减税后价格]
     */
    public void setPrice_reduce_taxinc(Double price_reduce_taxinc);
    
    /**
     * 设置 [减税后价格]
     */
    public Double getPrice_reduce_taxinc();

    /**
     * 获取 [减税后价格]脏标记
     */
    public boolean getPrice_reduce_taxincDirtyFlag();
    /**
     * 获取 [小计]
     */
    public void setPrice_subtotal(Double price_subtotal);
    
    /**
     * 设置 [小计]
     */
    public Double getPrice_subtotal();

    /**
     * 获取 [小计]脏标记
     */
    public boolean getPrice_subtotalDirtyFlag();
    /**
     * 获取 [税额总计]
     */
    public void setPrice_tax(Double price_tax);
    
    /**
     * 设置 [税额总计]
     */
    public Double getPrice_tax();

    /**
     * 获取 [税额总计]脏标记
     */
    public boolean getPrice_taxDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setPrice_total(Double price_total);
    
    /**
     * 设置 [总计]
     */
    public Double getPrice_total();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getPrice_totalDirtyFlag();
    /**
     * 获取 [单价]
     */
    public void setPrice_unit(Double price_unit);
    
    /**
     * 设置 [单价]
     */
    public Double getPrice_unit();

    /**
     * 获取 [单价]脏标记
     */
    public boolean getPrice_unitDirtyFlag();
    /**
     * 获取 [用户输入自定义产品属性值]
     */
    public void setProduct_custom_attribute_value_ids(String product_custom_attribute_value_ids);
    
    /**
     * 设置 [用户输入自定义产品属性值]
     */
    public String getProduct_custom_attribute_value_ids();

    /**
     * 获取 [用户输入自定义产品属性值]脏标记
     */
    public boolean getProduct_custom_attribute_value_idsDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [产品]
     */
    public Integer getProduct_id();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [产品]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [产品]
     */
    public String getProduct_id_text();

    /**
     * 获取 [产品]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [产品图片]
     */
    public void setProduct_image(byte[] product_image);
    
    /**
     * 设置 [产品图片]
     */
    public byte[] getProduct_image();

    /**
     * 获取 [产品图片]脏标记
     */
    public boolean getProduct_imageDirtyFlag();
    /**
     * 获取 [没有创建变量的产品属性值]
     */
    public void setProduct_no_variant_attribute_value_ids(String product_no_variant_attribute_value_ids);
    
    /**
     * 设置 [没有创建变量的产品属性值]
     */
    public String getProduct_no_variant_attribute_value_ids();

    /**
     * 获取 [没有创建变量的产品属性值]脏标记
     */
    public boolean getProduct_no_variant_attribute_value_idsDirtyFlag();
    /**
     * 获取 [包裹]
     */
    public void setProduct_packaging(Integer product_packaging);
    
    /**
     * 设置 [包裹]
     */
    public Integer getProduct_packaging();

    /**
     * 获取 [包裹]脏标记
     */
    public boolean getProduct_packagingDirtyFlag();
    /**
     * 获取 [包裹]
     */
    public void setProduct_packaging_text(String product_packaging_text);
    
    /**
     * 设置 [包裹]
     */
    public String getProduct_packaging_text();

    /**
     * 获取 [包裹]脏标记
     */
    public boolean getProduct_packaging_textDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom(Integer product_uom);
    
    /**
     * 设置 [计量单位]
     */
    public Integer getProduct_uom();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uomDirtyFlag();
    /**
     * 获取 [订购数量]
     */
    public void setProduct_uom_qty(Double product_uom_qty);
    
    /**
     * 设置 [订购数量]
     */
    public Double getProduct_uom_qty();

    /**
     * 获取 [订购数量]脏标记
     */
    public boolean getProduct_uom_qtyDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_text(String product_uom_text);
    
    /**
     * 设置 [计量单位]
     */
    public String getProduct_uom_text();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_textDirtyFlag();
    /**
     * 获取 [允许编辑]
     */
    public void setProduct_updatable(String product_updatable);
    
    /**
     * 设置 [允许编辑]
     */
    public String getProduct_updatable();

    /**
     * 获取 [允许编辑]脏标记
     */
    public boolean getProduct_updatableDirtyFlag();
    /**
     * 获取 [生成采购订单号]
     */
    public void setPurchase_line_count(Integer purchase_line_count);
    
    /**
     * 设置 [生成采购订单号]
     */
    public Integer getPurchase_line_count();

    /**
     * 获取 [生成采购订单号]脏标记
     */
    public boolean getPurchase_line_countDirtyFlag();
    /**
     * 获取 [生成采购订单明细行]
     */
    public void setPurchase_line_ids(String purchase_line_ids);
    
    /**
     * 设置 [生成采购订单明细行]
     */
    public String getPurchase_line_ids();

    /**
     * 获取 [生成采购订单明细行]脏标记
     */
    public boolean getPurchase_line_idsDirtyFlag();
    /**
     * 获取 [已交货数量]
     */
    public void setQty_delivered(Double qty_delivered);
    
    /**
     * 设置 [已交货数量]
     */
    public Double getQty_delivered();

    /**
     * 获取 [已交货数量]脏标记
     */
    public boolean getQty_deliveredDirtyFlag();
    /**
     * 获取 [手动发货]
     */
    public void setQty_delivered_manual(Double qty_delivered_manual);
    
    /**
     * 设置 [手动发货]
     */
    public Double getQty_delivered_manual();

    /**
     * 获取 [手动发货]脏标记
     */
    public boolean getQty_delivered_manualDirtyFlag();
    /**
     * 获取 [更新数量的方法]
     */
    public void setQty_delivered_method(String qty_delivered_method);
    
    /**
     * 设置 [更新数量的方法]
     */
    public String getQty_delivered_method();

    /**
     * 获取 [更新数量的方法]脏标记
     */
    public boolean getQty_delivered_methodDirtyFlag();
    /**
     * 获取 [已开发票数量]
     */
    public void setQty_invoiced(Double qty_invoiced);
    
    /**
     * 设置 [已开发票数量]
     */
    public Double getQty_invoiced();

    /**
     * 获取 [已开发票数量]脏标记
     */
    public boolean getQty_invoicedDirtyFlag();
    /**
     * 获取 [发票数量]
     */
    public void setQty_to_invoice(Double qty_to_invoice);
    
    /**
     * 设置 [发票数量]
     */
    public Double getQty_to_invoice();

    /**
     * 获取 [发票数量]脏标记
     */
    public boolean getQty_to_invoiceDirtyFlag();
    /**
     * 获取 [路线]
     */
    public void setRoute_id(Integer route_id);
    
    /**
     * 设置 [路线]
     */
    public Integer getRoute_id();

    /**
     * 获取 [路线]脏标记
     */
    public boolean getRoute_idDirtyFlag();
    /**
     * 获取 [路线]
     */
    public void setRoute_id_text(String route_id_text);
    
    /**
     * 设置 [路线]
     */
    public String getRoute_id_text();

    /**
     * 获取 [路线]脏标记
     */
    public boolean getRoute_id_textDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setSalesman_id(Integer salesman_id);
    
    /**
     * 设置 [销售员]
     */
    public Integer getSalesman_id();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getSalesman_idDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setSalesman_id_text(String salesman_id_text);
    
    /**
     * 设置 [销售员]
     */
    public String getSalesman_id_text();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getSalesman_id_textDirtyFlag();
    /**
     * 获取 [可选产品行]
     */
    public void setSale_order_option_ids(String sale_order_option_ids);
    
    /**
     * 设置 [可选产品行]
     */
    public String getSale_order_option_ids();

    /**
     * 获取 [可选产品行]脏标记
     */
    public boolean getSale_order_option_idsDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [订单状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [订单状态]
     */
    public String getState();

    /**
     * 获取 [订单状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [税率]
     */
    public void setTax_id(String tax_id);
    
    /**
     * 设置 [税率]
     */
    public String getTax_id();

    /**
     * 获取 [税率]脏标记
     */
    public boolean getTax_idDirtyFlag();
    /**
     * 获取 [未含税的发票金额]
     */
    public void setUntaxed_amount_invoiced(Double untaxed_amount_invoiced);
    
    /**
     * 设置 [未含税的发票金额]
     */
    public Double getUntaxed_amount_invoiced();

    /**
     * 获取 [未含税的发票金额]脏标记
     */
    public boolean getUntaxed_amount_invoicedDirtyFlag();
    /**
     * 获取 [不含税待开票金额]
     */
    public void setUntaxed_amount_to_invoice(Double untaxed_amount_to_invoice);
    
    /**
     * 设置 [不含税待开票金额]
     */
    public Double getUntaxed_amount_to_invoice();

    /**
     * 获取 [不含税待开票金额]脏标记
     */
    public boolean getUntaxed_amount_to_invoiceDirtyFlag();
    /**
     * 获取 [警告]
     */
    public void setWarning_stock(String warning_stock);
    
    /**
     * 设置 [警告]
     */
    public String getWarning_stock();

    /**
     * 获取 [警告]脏标记
     */
    public boolean getWarning_stockDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
