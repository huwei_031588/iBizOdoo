package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_leadSearchContext;

/**
 * 实体 [线索/商机] 存储模型
 */
public interface Crm_lead{

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * #会议
     */
    Integer getMeeting_count();

    void setMeeting_count(Integer meeting_count);

    /**
     * 获取 [#会议]脏标记
     */
    boolean getMeeting_countDirtyFlag();

    /**
     * 工作岗位
     */
    String getIbizfunction();

    void setIbizfunction(String ibizfunction);

    /**
     * 获取 [工作岗位]脏标记
     */
    boolean getIbizfunctionDirtyFlag();

    /**
     * 退回
     */
    Integer getMessage_bounce();

    void setMessage_bounce(Integer message_bounce);

    /**
     * 获取 [退回]脏标记
     */
    boolean getMessage_bounceDirtyFlag();

    /**
     * 城市
     */
    String getCity();

    void setCity(String city);

    /**
     * 获取 [城市]脏标记
     */
    boolean getCityDirtyFlag();

    /**
     * 关闭日期
     */
    Double getDay_close();

    void setDay_close(Double day_close);

    /**
     * 获取 [关闭日期]脏标记
     */
    boolean getDay_closeDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 按比例分摊收入
     */
    Double getExpected_revenue();

    void setExpected_revenue(Double expected_revenue);

    /**
     * 获取 [按比例分摊收入]脏标记
     */
    boolean getExpected_revenueDirtyFlag();

    /**
     * 关闭日期
     */
    Timestamp getDate_closed();

    void setDate_closed(Timestamp date_closed);

    /**
     * 获取 [关闭日期]脏标记
     */
    boolean getDate_closedDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 全局抄送
     */
    String getEmail_cc();

    void setEmail_cc(String email_cc);

    /**
     * 获取 [全局抄送]脏标记
     */
    boolean getEmail_ccDirtyFlag();

    /**
     * 联系人姓名
     */
    String getContact_name();

    void setContact_name(String contact_name);

    /**
     * 获取 [联系人姓名]脏标记
     */
    boolean getContact_nameDirtyFlag();

    /**
     * 最后阶段更新
     */
    Timestamp getDate_last_stage_update();

    void setDate_last_stage_update(Timestamp date_last_stage_update);

    /**
     * 获取 [最后阶段更新]脏标记
     */
    boolean getDate_last_stage_updateDirtyFlag();

    /**
     * 预期收益
     */
    Double getPlanned_revenue();

    void setPlanned_revenue(Double planned_revenue);

    /**
     * 获取 [预期收益]脏标记
     */
    boolean getPlanned_revenueDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 预期结束
     */
    Timestamp getDate_deadline();

    void setDate_deadline(Timestamp date_deadline);

    /**
     * 获取 [预期结束]脏标记
     */
    boolean getDate_deadlineDirtyFlag();

    /**
     * 邮政编码
     */
    String getZip();

    void setZip(String zip);

    /**
     * 获取 [邮政编码]脏标记
     */
    boolean getZipDirtyFlag();

    /**
     * 商机
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [商机]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 便签
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [便签]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 手机
     */
    String getMobile();

    void setMobile(String mobile);

    /**
     * 获取 [手机]脏标记
     */
    boolean getMobileDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 网站
     */
    String getWebsite();

    void setWebsite(String website);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsiteDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * EMail
     */
    String getEmail_from();

    void setEmail_from(String email_from);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getEmail_fromDirtyFlag();

    /**
     * 转换日期
     */
    Timestamp getDate_conversion();

    void setDate_conversion(Timestamp date_conversion);

    /**
     * 获取 [转换日期]脏标记
     */
    boolean getDate_conversionDirtyFlag();

    /**
     * 客户名称
     */
    String getPartner_name();

    void setPartner_name(String partner_name);

    /**
     * 获取 [客户名称]脏标记
     */
    boolean getPartner_nameDirtyFlag();

    /**
     * 销售订单的总数
     */
    Double getSale_amount_total();

    void setSale_amount_total(Double sale_amount_total);

    /**
     * 获取 [销售订单的总数]脏标记
     */
    boolean getSale_amount_totalDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 需要激活
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要激活]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 看板状态
     */
    String getKanban_state();

    void setKanban_state(String kanban_state);

    /**
     * 获取 [看板状态]脏标记
     */
    boolean getKanban_stateDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 引荐于
     */
    String getReferred();

    void setReferred(String referred);

    /**
     * 获取 [引荐于]脏标记
     */
    boolean getReferredDirtyFlag();

    /**
     * 概率
     */
    Double getProbability();

    void setProbability(Double probability);

    /**
     * 获取 [概率]脏标记
     */
    boolean getProbabilityDirtyFlag();

    /**
     * 最近行动
     */
    Timestamp getDate_action_last();

    void setDate_action_last(Timestamp date_action_last);

    /**
     * 获取 [最近行动]脏标记
     */
    boolean getDate_action_lastDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 分配日期
     */
    Timestamp getDate_open();

    void setDate_open(Timestamp date_open);

    /**
     * 获取 [分配日期]脏标记
     */
    boolean getDate_openDirtyFlag();

    /**
     * 电话
     */
    String getPhone();

    void setPhone(String phone);

    /**
     * 获取 [电话]脏标记
     */
    boolean getPhoneDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 分配天数
     */
    Double getDay_open();

    void setDay_open(Double day_open);

    /**
     * 获取 [分配天数]脏标记
     */
    boolean getDay_openDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 报价单的数量
     */
    Integer getSale_number();

    void setSale_number(Integer sale_number);

    /**
     * 获取 [报价单的数量]脏标记
     */
    boolean getSale_numberDirtyFlag();

    /**
     * 街道 2
     */
    String getStreet2();

    void setStreet2(String street2);

    /**
     * 获取 [街道 2]脏标记
     */
    boolean getStreet2DirtyFlag();

    /**
     * 订单
     */
    String getOrder_ids();

    void setOrder_ids(String order_ids);

    /**
     * 获取 [订单]脏标记
     */
    boolean getOrder_idsDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 街道
     */
    String getStreet();

    void setStreet(String street);

    /**
     * 获取 [街道]脏标记
     */
    boolean getStreetDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 标签
     */
    String getTag_ids();

    void setTag_ids(String tag_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getTag_idsDirtyFlag();

    /**
     * 黑名单
     */
    String getIs_blacklisted();

    void setIs_blacklisted(String is_blacklisted);

    /**
     * 获取 [黑名单]脏标记
     */
    boolean getIs_blacklistedDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 优先级
     */
    String getPriority();

    void setPriority(String priority);

    /**
     * 获取 [优先级]脏标记
     */
    boolean getPriorityDirtyFlag();

    /**
     * 销售员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 来源
     */
    String getSource_id_text();

    void setSource_id_text(String source_id_text);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_id_textDirtyFlag();

    /**
     * 业务伙伴联系姓名
     */
    String getPartner_address_name();

    void setPartner_address_name(String partner_address_name);

    /**
     * 获取 [业务伙伴联系姓名]脏标记
     */
    boolean getPartner_address_nameDirtyFlag();

    /**
     * 媒介
     */
    String getMedium_id_text();

    void setMedium_id_text(String medium_id_text);

    /**
     * 获取 [媒介]脏标记
     */
    boolean getMedium_id_textDirtyFlag();

    /**
     * 币种
     */
    Integer getCompany_currency();

    void setCompany_currency(Integer company_currency);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCompany_currencyDirtyFlag();

    /**
     * 销售团队
     */
    String getTeam_id_text();

    void setTeam_id_text(String team_id_text);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_id_textDirtyFlag();

    /**
     * Partner Contact Mobile
     */
    String getPartner_address_mobile();

    void setPartner_address_mobile(String partner_address_mobile);

    /**
     * 获取 [Partner Contact Mobile]脏标记
     */
    boolean getPartner_address_mobileDirtyFlag();

    /**
     * 最后更新
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 用户EMail
     */
    String getUser_email();

    void setUser_email(String user_email);

    /**
     * 获取 [用户EMail]脏标记
     */
    boolean getUser_emailDirtyFlag();

    /**
     * 用户 登录
     */
    String getUser_login();

    void setUser_login(String user_login);

    /**
     * 获取 [用户 登录]脏标记
     */
    boolean getUser_loginDirtyFlag();

    /**
     * 合作伙伴联系电话
     */
    String getPartner_address_phone();

    void setPartner_address_phone(String partner_address_phone);

    /**
     * 获取 [合作伙伴联系电话]脏标记
     */
    boolean getPartner_address_phoneDirtyFlag();

    /**
     * 省份
     */
    String getState_id_text();

    void setState_id_text(String state_id_text);

    /**
     * 获取 [省份]脏标记
     */
    boolean getState_id_textDirtyFlag();

    /**
     * 营销
     */
    String getCampaign_id_text();

    void setCampaign_id_text(String campaign_id_text);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_id_textDirtyFlag();

    /**
     * 合作伙伴黑名单
     */
    String getPartner_is_blacklisted();

    void setPartner_is_blacklisted(String partner_is_blacklisted);

    /**
     * 获取 [合作伙伴黑名单]脏标记
     */
    boolean getPartner_is_blacklistedDirtyFlag();

    /**
     * 业务伙伴联系EMail
     */
    String getPartner_address_email();

    void setPartner_address_email(String partner_address_email);

    /**
     * 获取 [业务伙伴联系EMail]脏标记
     */
    boolean getPartner_address_emailDirtyFlag();

    /**
     * 阶段
     */
    String getStage_id_text();

    void setStage_id_text(String stage_id_text);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 国家
     */
    String getCountry_id_text();

    void setCountry_id_text(String country_id_text);

    /**
     * 获取 [国家]脏标记
     */
    boolean getCountry_id_textDirtyFlag();

    /**
     * 称谓
     */
    String getTitle_text();

    void setTitle_text(String title_text);

    /**
     * 获取 [称谓]脏标记
     */
    boolean getTitle_textDirtyFlag();

    /**
     * 失去原因
     */
    String getLost_reason_text();

    void setLost_reason_text(String lost_reason_text);

    /**
     * 获取 [失去原因]脏标记
     */
    boolean getLost_reason_textDirtyFlag();

    /**
     * 失去原因
     */
    Integer getLost_reason();

    void setLost_reason(Integer lost_reason);

    /**
     * 获取 [失去原因]脏标记
     */
    boolean getLost_reasonDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 销售员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 省份
     */
    Integer getState_id();

    void setState_id(Integer state_id);

    /**
     * 获取 [省份]脏标记
     */
    boolean getState_idDirtyFlag();

    /**
     * 媒介
     */
    Integer getMedium_id();

    void setMedium_id(Integer medium_id);

    /**
     * 获取 [媒介]脏标记
     */
    boolean getMedium_idDirtyFlag();

    /**
     * 阶段
     */
    Integer getStage_id();

    void setStage_id(Integer stage_id);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_idDirtyFlag();

    /**
     * 来源
     */
    Integer getSource_id();

    void setSource_id(Integer source_id);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_idDirtyFlag();

    /**
     * 国家
     */
    Integer getCountry_id();

    void setCountry_id(Integer country_id);

    /**
     * 获取 [国家]脏标记
     */
    boolean getCountry_idDirtyFlag();

    /**
     * 营销
     */
    Integer getCampaign_id();

    void setCampaign_id(Integer campaign_id);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_idDirtyFlag();

    /**
     * 客户
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 销售团队
     */
    Integer getTeam_id();

    void setTeam_id(Integer team_id);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_idDirtyFlag();

    /**
     * 称谓
     */
    Integer getTitle();

    void setTitle(Integer title);

    /**
     * 获取 [称谓]脏标记
     */
    boolean getTitleDirtyFlag();

}
