package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_cancelSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_resend_cancelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_resend_cancelOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_resend_cancelClientModel;

/**
 * 实体[重发请求被拒绝] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_resend_cancelServiceImpl implements IMail_resend_cancelService {

    @Autowired
    mail_resend_cancelOdooClient mail_resend_cancelOdooClient;


    @Override
    public boolean update(Mail_resend_cancel et) {
        mail_resend_cancelClientModel clientModel = convert2Model(et,null);
		mail_resend_cancelOdooClient.update(clientModel);
        Mail_resend_cancel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_resend_cancel> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_resend_cancelClientModel clientModel = new mail_resend_cancelClientModel();
        clientModel.setId(id);
		mail_resend_cancelOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mail_resend_cancel et) {
        mail_resend_cancelClientModel clientModel = convert2Model(et,null);
		mail_resend_cancelOdooClient.create(clientModel);
        Mail_resend_cancel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_resend_cancel> list){
    }

    @Override
    public Mail_resend_cancel get(Integer id) {
        mail_resend_cancelClientModel clientModel = new mail_resend_cancelClientModel();
        clientModel.setId(id);
		mail_resend_cancelOdooClient.get(clientModel);
        Mail_resend_cancel et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_resend_cancel();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_resend_cancel> searchDefault(Mail_resend_cancelSearchContext context) {
        List<Mail_resend_cancel> list = new ArrayList<Mail_resend_cancel>();
        Page<mail_resend_cancelClientModel> clientModelList = mail_resend_cancelOdooClient.search(context);
        for(mail_resend_cancelClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_resend_cancel>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_resend_cancelClientModel convert2Model(Mail_resend_cancel domain , mail_resend_cancelClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_resend_cancelClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("modeldirtyflag"))
                model.setModel(domain.getModel());
            if((Boolean) domain.getExtensionparams().get("help_messagedirtyflag"))
                model.setHelp_message(domain.getHelpMessage());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_resend_cancel convert2Domain( mail_resend_cancelClientModel model ,Mail_resend_cancel domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_resend_cancel();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getModelDirtyFlag())
            domain.setModel(model.getModel());
        if(model.getHelp_messageDirtyFlag())
            domain.setHelpMessage(model.getHelp_message());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



