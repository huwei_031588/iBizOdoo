package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_traceability_report;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_traceability_reportSearchContext;

/**
 * 实体 [追溯报告] 存储对象
 */
public interface Stock_traceability_reportRepository extends Repository<Stock_traceability_report> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_traceability_report> searchDefault(Stock_traceability_reportSearchContext context);

    Stock_traceability_report convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_traceability_report domain , Stock_traceability_report po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_traceability_report convert2Domain( Stock_traceability_report po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_traceability_report domain) ;

}
