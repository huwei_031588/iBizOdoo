package cn.ibizlab.odoo.core.odoo_payment.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_icon;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_iconSearchContext;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_iconService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_payment.client.payment_iconOdooClient;
import cn.ibizlab.odoo.core.odoo_payment.clientmodel.payment_iconClientModel;

/**
 * 实体[支付图标] 服务对象接口实现
 */
@Slf4j
@Service
public class Payment_iconServiceImpl implements IPayment_iconService {

    @Autowired
    payment_iconOdooClient payment_iconOdooClient;


    @Override
    public boolean update(Payment_icon et) {
        payment_iconClientModel clientModel = convert2Model(et,null);
		payment_iconOdooClient.update(clientModel);
        Payment_icon rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Payment_icon> list){
    }

    @Override
    public boolean remove(Integer id) {
        payment_iconClientModel clientModel = new payment_iconClientModel();
        clientModel.setId(id);
		payment_iconOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Payment_icon get(Integer id) {
        payment_iconClientModel clientModel = new payment_iconClientModel();
        clientModel.setId(id);
		payment_iconOdooClient.get(clientModel);
        Payment_icon et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Payment_icon();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Payment_icon et) {
        payment_iconClientModel clientModel = convert2Model(et,null);
		payment_iconOdooClient.create(clientModel);
        Payment_icon rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Payment_icon> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Payment_icon> searchDefault(Payment_iconSearchContext context) {
        List<Payment_icon> list = new ArrayList<Payment_icon>();
        Page<payment_iconClientModel> clientModelList = payment_iconOdooClient.search(context);
        for(payment_iconClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Payment_icon>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public payment_iconClientModel convert2Model(Payment_icon domain , payment_iconClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new payment_iconClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("acquirer_idsdirtyflag"))
                model.setAcquirer_ids(domain.getAcquirerIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("image_payment_formdirtyflag"))
                model.setImage_payment_form(domain.getImagePaymentForm());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Payment_icon convert2Domain( payment_iconClientModel model ,Payment_icon domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Payment_icon();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getAcquirer_idsDirtyFlag())
            domain.setAcquirerIds(model.getAcquirer_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getImage_payment_formDirtyFlag())
            domain.setImagePaymentForm(model.getImage_payment_form());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



