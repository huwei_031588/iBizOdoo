package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_style;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_styleSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_styleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_styleOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_styleClientModel;

/**
 * 实体[产品风格] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_styleServiceImpl implements IProduct_styleService {

    @Autowired
    product_styleOdooClient product_styleOdooClient;


    @Override
    public boolean remove(Integer id) {
        product_styleClientModel clientModel = new product_styleClientModel();
        clientModel.setId(id);
		product_styleOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Product_style get(Integer id) {
        product_styleClientModel clientModel = new product_styleClientModel();
        clientModel.setId(id);
		product_styleOdooClient.get(clientModel);
        Product_style et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_style();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Product_style et) {
        product_styleClientModel clientModel = convert2Model(et,null);
		product_styleOdooClient.create(clientModel);
        Product_style rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_style> list){
    }

    @Override
    public boolean update(Product_style et) {
        product_styleClientModel clientModel = convert2Model(et,null);
		product_styleOdooClient.update(clientModel);
        Product_style rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_style> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_style> searchDefault(Product_styleSearchContext context) {
        List<Product_style> list = new ArrayList<Product_style>();
        Page<product_styleClientModel> clientModelList = product_styleOdooClient.search(context);
        for(product_styleClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_style>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_styleClientModel convert2Model(Product_style domain , product_styleClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_styleClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("html_classdirtyflag"))
                model.setHtml_class(domain.getHtmlClass());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_style convert2Domain( product_styleClientModel model ,Product_style domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_style();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getHtml_classDirtyFlag())
            domain.setHtmlClass(model.getHtml_class());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



