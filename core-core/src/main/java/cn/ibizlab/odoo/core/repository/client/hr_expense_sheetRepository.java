package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_expense_sheet;

/**
 * 实体[hr_expense_sheet] 服务对象接口
 */
public interface hr_expense_sheetRepository{


    public hr_expense_sheet createPO() ;
        public void create(hr_expense_sheet hr_expense_sheet);

        public List<hr_expense_sheet> search();

        public void createBatch(hr_expense_sheet hr_expense_sheet);

        public void updateBatch(hr_expense_sheet hr_expense_sheet);

        public void update(hr_expense_sheet hr_expense_sheet);

        public void get(String id);

        public void remove(String id);

        public void removeBatch(String id);


}
