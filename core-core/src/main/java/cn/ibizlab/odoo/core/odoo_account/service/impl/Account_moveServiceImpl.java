package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_moveSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_moveService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_moveOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_moveClientModel;

/**
 * 实体[凭证录入] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_moveServiceImpl implements IAccount_moveService {

    @Autowired
    account_moveOdooClient account_moveOdooClient;


    @Override
    public boolean update(Account_move et) {
        account_moveClientModel clientModel = convert2Model(et,null);
		account_moveOdooClient.update(clientModel);
        Account_move rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_move> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_moveClientModel clientModel = new account_moveClientModel();
        clientModel.setId(id);
		account_moveOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Account_move get(Integer id) {
        account_moveClientModel clientModel = new account_moveClientModel();
        clientModel.setId(id);
		account_moveOdooClient.get(clientModel);
        Account_move et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_move();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_move et) {
        account_moveClientModel clientModel = convert2Model(et,null);
		account_moveOdooClient.create(clientModel);
        Account_move rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_move> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_move> searchDefault(Account_moveSearchContext context) {
        List<Account_move> list = new ArrayList<Account_move>();
        Page<account_moveClientModel> clientModelList = account_moveOdooClient.search(context);
        for(account_moveClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_move>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_moveClientModel convert2Model(Account_move domain , account_moveClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_moveClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("narrationdirtyflag"))
                model.setNarration(domain.getNarration());
            if((Boolean) domain.getExtensionparams().get("matched_percentagedirtyflag"))
                model.setMatched_percentage(domain.getMatchedPercentage());
            if((Boolean) domain.getExtensionparams().get("tax_type_domaindirtyflag"))
                model.setTax_type_domain(domain.getTaxTypeDomain());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("refdirtyflag"))
                model.setRef(domain.getRef());
            if((Boolean) domain.getExtensionparams().get("dummy_account_iddirtyflag"))
                model.setDummy_account_id(domain.getDummyAccountId());
            if((Boolean) domain.getExtensionparams().get("auto_reversedirtyflag"))
                model.setAuto_reverse(domain.getAutoReverse());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("reverse_datedirtyflag"))
                model.setReverse_date(domain.getReverseDate());
            if((Boolean) domain.getExtensionparams().get("line_idsdirtyflag"))
                model.setLine_ids(domain.getLineIds());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("reverse_entry_id_textdirtyflag"))
                model.setReverse_entry_id_text(domain.getReverseEntryIdText());
            if((Boolean) domain.getExtensionparams().get("stock_move_id_textdirtyflag"))
                model.setStock_move_id_text(domain.getStockMoveIdText());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("stock_move_iddirtyflag"))
                model.setStock_move_id(domain.getStockMoveId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("tax_cash_basis_rec_iddirtyflag"))
                model.setTax_cash_basis_rec_id(domain.getTaxCashBasisRecId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("reverse_entry_iddirtyflag"))
                model.setReverse_entry_id(domain.getReverseEntryId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_move convert2Domain( account_moveClientModel model ,Account_move domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_move();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNarrationDirtyFlag())
            domain.setNarration(model.getNarration());
        if(model.getMatched_percentageDirtyFlag())
            domain.setMatchedPercentage(model.getMatched_percentage());
        if(model.getTax_type_domainDirtyFlag())
            domain.setTaxTypeDomain(model.getTax_type_domain());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getRefDirtyFlag())
            domain.setRef(model.getRef());
        if(model.getDummy_account_idDirtyFlag())
            domain.setDummyAccountId(model.getDummy_account_id());
        if(model.getAuto_reverseDirtyFlag())
            domain.setAutoReverse(model.getAuto_reverse());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getReverse_dateDirtyFlag())
            domain.setReverseDate(model.getReverse_date());
        if(model.getLine_idsDirtyFlag())
            domain.setLineIds(model.getLine_ids());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getReverse_entry_id_textDirtyFlag())
            domain.setReverseEntryIdText(model.getReverse_entry_id_text());
        if(model.getStock_move_id_textDirtyFlag())
            domain.setStockMoveIdText(model.getStock_move_id_text());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getStock_move_idDirtyFlag())
            domain.setStockMoveId(model.getStock_move_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getTax_cash_basis_rec_idDirtyFlag())
            domain.setTaxCashBasisRecId(model.getTax_cash_basis_rec_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getReverse_entry_idDirtyFlag())
            domain.setReverseEntryId(model.getReverse_entry_id());
        return domain ;
    }

}

    



