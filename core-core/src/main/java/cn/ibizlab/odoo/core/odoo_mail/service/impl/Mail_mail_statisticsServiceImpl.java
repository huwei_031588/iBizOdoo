package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mail_statisticsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_mail_statisticsOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_mail_statisticsClientModel;

/**
 * 实体[邮件统计] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mail_statisticsServiceImpl implements IMail_mail_statisticsService {

    @Autowired
    mail_mail_statisticsOdooClient mail_mail_statisticsOdooClient;


    @Override
    public boolean update(Mail_mail_statistics et) {
        mail_mail_statisticsClientModel clientModel = convert2Model(et,null);
		mail_mail_statisticsOdooClient.update(clientModel);
        Mail_mail_statistics rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_mail_statistics> list){
    }

    @Override
    public Mail_mail_statistics get(Integer id) {
        mail_mail_statisticsClientModel clientModel = new mail_mail_statisticsClientModel();
        clientModel.setId(id);
		mail_mail_statisticsOdooClient.get(clientModel);
        Mail_mail_statistics et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_mail_statistics();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mail_mail_statisticsClientModel clientModel = new mail_mail_statisticsClientModel();
        clientModel.setId(id);
		mail_mail_statisticsOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mail_mail_statistics et) {
        mail_mail_statisticsClientModel clientModel = convert2Model(et,null);
		mail_mail_statisticsOdooClient.create(clientModel);
        Mail_mail_statistics rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mail_statistics> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mail_statistics> searchDefault(Mail_mail_statisticsSearchContext context) {
        List<Mail_mail_statistics> list = new ArrayList<Mail_mail_statistics>();
        Page<mail_mail_statisticsClientModel> clientModelList = mail_mail_statisticsOdooClient.search(context);
        for(mail_mail_statisticsClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_mail_statistics>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_mail_statisticsClientModel convert2Model(Mail_mail_statistics domain , mail_mail_statisticsClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_mail_statisticsClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("scheduleddirtyflag"))
                model.setScheduled(domain.getScheduled());
            if((Boolean) domain.getExtensionparams().get("clickeddirtyflag"))
                model.setClicked(domain.getClicked());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("emaildirtyflag"))
                model.setEmail(domain.getEmail());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("state_updatedirtyflag"))
                model.setState_update(domain.getStateUpdate());
            if((Boolean) domain.getExtensionparams().get("replieddirtyflag"))
                model.setReplied(domain.getReplied());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("modeldirtyflag"))
                model.setModel(domain.getModel());
            if((Boolean) domain.getExtensionparams().get("message_iddirtyflag"))
                model.setMessage_id(domain.getMessageId());
            if((Boolean) domain.getExtensionparams().get("exceptiondirtyflag"))
                model.setException(domain.getException());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("bounceddirtyflag"))
                model.setBounced(domain.getBounced());
            if((Boolean) domain.getExtensionparams().get("ignoreddirtyflag"))
                model.setIgnored(domain.getIgnored());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("sentdirtyflag"))
                model.setSent(domain.getSent());
            if((Boolean) domain.getExtensionparams().get("mail_mail_id_intdirtyflag"))
                model.setMail_mail_id_int(domain.getMailMailIdInt());
            if((Boolean) domain.getExtensionparams().get("links_click_idsdirtyflag"))
                model.setLinks_click_ids(domain.getLinksClickIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("openeddirtyflag"))
                model.setOpened(domain.getOpened());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_campaign_id_textdirtyflag"))
                model.setMass_mailing_campaign_id_text(domain.getMassMailingCampaignIdText());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_id_textdirtyflag"))
                model.setMass_mailing_id_text(domain.getMassMailingIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_campaign_iddirtyflag"))
                model.setMass_mailing_campaign_id(domain.getMassMailingCampaignId());
            if((Boolean) domain.getExtensionparams().get("mail_mail_iddirtyflag"))
                model.setMail_mail_id(domain.getMailMailId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_iddirtyflag"))
                model.setMass_mailing_id(domain.getMassMailingId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_mail_statistics convert2Domain( mail_mail_statisticsClientModel model ,Mail_mail_statistics domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_mail_statistics();
        }

        if(model.getScheduledDirtyFlag())
            domain.setScheduled(model.getScheduled());
        if(model.getClickedDirtyFlag())
            domain.setClicked(model.getClicked());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getEmailDirtyFlag())
            domain.setEmail(model.getEmail());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getState_updateDirtyFlag())
            domain.setStateUpdate(model.getState_update());
        if(model.getRepliedDirtyFlag())
            domain.setReplied(model.getReplied());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getModelDirtyFlag())
            domain.setModel(model.getModel());
        if(model.getMessage_idDirtyFlag())
            domain.setMessageId(model.getMessage_id());
        if(model.getExceptionDirtyFlag())
            domain.setException(model.getException());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getBouncedDirtyFlag())
            domain.setBounced(model.getBounced());
        if(model.getIgnoredDirtyFlag())
            domain.setIgnored(model.getIgnored());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getSentDirtyFlag())
            domain.setSent(model.getSent());
        if(model.getMail_mail_id_intDirtyFlag())
            domain.setMailMailIdInt(model.getMail_mail_id_int());
        if(model.getLinks_click_idsDirtyFlag())
            domain.setLinksClickIds(model.getLinks_click_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getOpenedDirtyFlag())
            domain.setOpened(model.getOpened());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMass_mailing_campaign_id_textDirtyFlag())
            domain.setMassMailingCampaignIdText(model.getMass_mailing_campaign_id_text());
        if(model.getMass_mailing_id_textDirtyFlag())
            domain.setMassMailingIdText(model.getMass_mailing_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getMass_mailing_campaign_idDirtyFlag())
            domain.setMassMailingCampaignId(model.getMass_mailing_campaign_id());
        if(model.getMail_mail_idDirtyFlag())
            domain.setMailMailId(model.getMail_mail_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getMass_mailing_idDirtyFlag())
            domain.setMassMailingId(model.getMass_mailing_id());
        return domain ;
    }

}

    



