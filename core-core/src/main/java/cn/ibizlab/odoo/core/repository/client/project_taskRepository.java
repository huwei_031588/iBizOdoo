package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.project_task;

/**
 * 实体[project_task] 服务对象接口
 */
public interface project_taskRepository{


    public project_task createPO() ;
        public void updateBatch(project_task project_task);

        public void createBatch(project_task project_task);

        public void create(project_task project_task);

        public void remove(String id);

        public void get(String id);

        public void update(project_task project_task);

        public void removeBatch(String id);

        public List<project_task> search();


}
