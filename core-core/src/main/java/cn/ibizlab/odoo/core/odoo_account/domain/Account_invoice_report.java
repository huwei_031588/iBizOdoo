package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [发票统计] 对象
 */
@Data
public class Account_invoice_report extends EntityClient implements Serializable {

    /**
     * 发票 #
     */
    @JSONField(name = "number")
    @JsonProperty("number")
    private String number;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 数量
     */
    @DEField(name = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 当前货币不含税总计
     */
    @JSONField(name = "user_currency_price_total")
    @JsonProperty("user_currency_price_total")
    private Double userCurrencyPriceTotal;

    /**
     * 到期金额
     */
    @JSONField(name = "residual")
    @JsonProperty("residual")
    private Double residual;

    /**
     * 发票状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 到期日期
     */
    @DEField(name = "date_due")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_due" , format="yyyy-MM-dd")
    @JsonProperty("date_due")
    private Timestamp dateDue;

    /**
     * 行数
     */
    @JSONField(name = "nbr")
    @JsonProperty("nbr")
    private Integer nbr;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 开票日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 总计
     */
    @DEField(name = "amount_total")
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private Double amountTotal;

    /**
     * 参考计量单位
     */
    @DEField(name = "uom_name")
    @JSONField(name = "uom_name")
    @JsonProperty("uom_name")
    private String uomName;

    /**
     * 平均价格
     */
    @DEField(name = "price_average")
    @JSONField(name = "price_average")
    @JsonProperty("price_average")
    private Double priceAverage;

    /**
     * 汇率
     */
    @DEField(name = "currency_rate")
    @JSONField(name = "currency_rate")
    @JsonProperty("currency_rate")
    private Double currencyRate;

    /**
     * 不含税总计
     */
    @DEField(name = "price_total")
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private Double priceTotal;

    /**
     * 本币平均价格
     */
    @JSONField(name = "user_currency_price_average")
    @JsonProperty("user_currency_price_average")
    private Double userCurrencyPriceAverage;

    /**
     * 余额总计
     */
    @JSONField(name = "user_currency_residual")
    @JsonProperty("user_currency_residual")
    private Double userCurrencyResidual;

    /**
     * 类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 销售团队
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;

    /**
     * 分析账户
     */
    @JSONField(name = "account_analytic_id_text")
    @JsonProperty("account_analytic_id_text")
    private String accountAnalyticIdText;

    /**
     * 产品类别
     */
    @JSONField(name = "categ_id_text")
    @JsonProperty("categ_id_text")
    private String categIdText;

    /**
     * 日记账
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 税科目调整
     */
    @JSONField(name = "fiscal_position_id_text")
    @JsonProperty("fiscal_position_id_text")
    private String fiscalPositionIdText;

    /**
     * 发票
     */
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    private String invoiceIdText;

    /**
     * 业务伙伴的国家
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 付款条款
     */
    @JSONField(name = "payment_term_id_text")
    @JsonProperty("payment_term_id_text")
    private String paymentTermIdText;

    /**
     * 业务伙伴
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 收入/费用科目
     */
    @JSONField(name = "account_line_id_text")
    @JsonProperty("account_line_id_text")
    private String accountLineIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 业务伙伴公司
     */
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    private String commercialPartnerIdText;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 销售员
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 收／付款账户
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 业务伙伴的国家
     */
    @DEField(name = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 银行账户
     */
    @DEField(name = "partner_bank_id")
    @JSONField(name = "partner_bank_id")
    @JsonProperty("partner_bank_id")
    private Integer partnerBankId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 产品类别
     */
    @DEField(name = "categ_id")
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    private Integer categId;

    /**
     * 业务伙伴公司
     */
    @DEField(name = "commercial_partner_id")
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    private Integer commercialPartnerId;

    /**
     * 收／付款账户
     */
    @DEField(name = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 付款条款
     */
    @DEField(name = "payment_term_id")
    @JSONField(name = "payment_term_id")
    @JsonProperty("payment_term_id")
    private Integer paymentTermId;

    /**
     * 分析账户
     */
    @DEField(name = "account_analytic_id")
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    private Integer accountAnalyticId;

    /**
     * 税科目调整
     */
    @DEField(name = "fiscal_position_id")
    @JSONField(name = "fiscal_position_id")
    @JsonProperty("fiscal_position_id")
    private Integer fiscalPositionId;

    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 收入/费用科目
     */
    @DEField(name = "account_line_id")
    @JSONField(name = "account_line_id")
    @JsonProperty("account_line_id")
    private Integer accountLineId;

    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Integer teamId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 发票
     */
    @DEField(name = "invoice_id")
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    private Integer invoiceId;


    /**
     * 
     */
    @JSONField(name = "odooaccount")
    @JsonProperty("odooaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JSONField(name = "odooaccountline")
    @JsonProperty("odooaccountline")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooAccountLine;

    /**
     * 
     */
    @JSONField(name = "odooaccountanalytic")
    @JsonProperty("odooaccountanalytic")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account odooAccountAnalytic;

    /**
     * 
     */
    @JSONField(name = "odoofiscalposition")
    @JsonProperty("odoofiscalposition")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition;

    /**
     * 
     */
    @JSONField(name = "odooinvoice")
    @JsonProperty("odooinvoice")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice odooInvoice;

    /**
     * 
     */
    @JSONField(name = "odoojournal")
    @JsonProperty("odoojournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JSONField(name = "odoopaymentterm")
    @JsonProperty("odoopaymentterm")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term odooPaymentTerm;

    /**
     * 
     */
    @JSONField(name = "odooteam")
    @JsonProperty("odooteam")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JSONField(name = "odoocateg")
    @JsonProperty("odoocateg")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_category odooCateg;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocountry")
    @JsonProperty("odoocountry")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoopartnerbank")
    @JsonProperty("odoopartnerbank")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank odooPartnerBank;

    /**
     * 
     */
    @JSONField(name = "odoocommercialpartner")
    @JsonProperty("odoocommercialpartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooCommercialPartner;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;




    /**
     * 设置 [发票 #]
     */
    public void setNumber(String number){
        this.number = number ;
        this.modify("number",number);
    }
    /**
     * 设置 [数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }
    /**
     * 设置 [到期金额]
     */
    public void setResidual(Double residual){
        this.residual = residual ;
        this.modify("residual",residual);
    }
    /**
     * 设置 [发票状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [到期日期]
     */
    public void setDateDue(Timestamp dateDue){
        this.dateDue = dateDue ;
        this.modify("date_due",dateDue);
    }
    /**
     * 设置 [行数]
     */
    public void setNbr(Integer nbr){
        this.nbr = nbr ;
        this.modify("nbr",nbr);
    }
    /**
     * 设置 [开票日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [总计]
     */
    public void setAmountTotal(Double amountTotal){
        this.amountTotal = amountTotal ;
        this.modify("amount_total",amountTotal);
    }
    /**
     * 设置 [参考计量单位]
     */
    public void setUomName(String uomName){
        this.uomName = uomName ;
        this.modify("uom_name",uomName);
    }
    /**
     * 设置 [平均价格]
     */
    public void setPriceAverage(Double priceAverage){
        this.priceAverage = priceAverage ;
        this.modify("price_average",priceAverage);
    }
    /**
     * 设置 [汇率]
     */
    public void setCurrencyRate(Double currencyRate){
        this.currencyRate = currencyRate ;
        this.modify("currency_rate",currencyRate);
    }
    /**
     * 设置 [不含税总计]
     */
    public void setPriceTotal(Double priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }
    /**
     * 设置 [类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [销售员]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [业务伙伴的国家]
     */
    public void setCountryId(Integer countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }
    /**
     * 设置 [银行账户]
     */
    public void setPartnerBankId(Integer partnerBankId){
        this.partnerBankId = partnerBankId ;
        this.modify("partner_bank_id",partnerBankId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [产品类别]
     */
    public void setCategId(Integer categId){
        this.categId = categId ;
        this.modify("categ_id",categId);
    }
    /**
     * 设置 [业务伙伴公司]
     */
    public void setCommercialPartnerId(Integer commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }
    /**
     * 设置 [收／付款账户]
     */
    public void setAccountId(Integer accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [付款条款]
     */
    public void setPaymentTermId(Integer paymentTermId){
        this.paymentTermId = paymentTermId ;
        this.modify("payment_term_id",paymentTermId);
    }
    /**
     * 设置 [分析账户]
     */
    public void setAccountAnalyticId(Integer accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }
    /**
     * 设置 [税科目调整]
     */
    public void setFiscalPositionId(Integer fiscalPositionId){
        this.fiscalPositionId = fiscalPositionId ;
        this.modify("fiscal_position_id",fiscalPositionId);
    }
    /**
     * 设置 [日记账]
     */
    public void setJournalId(Integer journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }
    /**
     * 设置 [收入/费用科目]
     */
    public void setAccountLineId(Integer accountLineId){
        this.accountLineId = accountLineId ;
        this.modify("account_line_id",accountLineId);
    }
    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Integer teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [发票]
     */
    public void setInvoiceId(Integer invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }

}


