package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_taxSearchContext;


/**
 * 实体[Account_fiscal_position_tax] 服务对象接口
 */
public interface IAccount_fiscal_position_taxService{

    boolean create(Account_fiscal_position_tax et) ;
    void createBatch(List<Account_fiscal_position_tax> list) ;
    boolean update(Account_fiscal_position_tax et) ;
    void updateBatch(List<Account_fiscal_position_tax> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_fiscal_position_tax get(Integer key) ;
    Page<Account_fiscal_position_tax> searchDefault(Account_fiscal_position_taxSearchContext context) ;

}



