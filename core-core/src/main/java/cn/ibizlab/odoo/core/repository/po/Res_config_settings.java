package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_settingsSearchContext;

/**
 * 实体 [配置设定] 存储模型
 */
public interface Res_config_settings{

    /**
     * 导入.qif 文件
     */
    String getModule_account_bank_statement_import_qif();

    void setModule_account_bank_statement_import_qif(String module_account_bank_statement_import_qif);

    /**
     * 获取 [导入.qif 文件]脏标记
     */
    boolean getModule_account_bank_statement_import_qifDirtyFlag();

    /**
     * 导入.ofx格式
     */
    String getModule_account_bank_statement_import_ofx();

    void setModule_account_bank_statement_import_ofx(String module_account_bank_statement_import_ofx);

    /**
     * 获取 [导入.ofx格式]脏标记
     */
    boolean getModule_account_bank_statement_import_ofxDirtyFlag();

    /**
     * Google 地图
     */
    String getHas_google_maps();

    void setHas_google_maps(String has_google_maps);

    /**
     * 获取 [Google 地图]脏标记
     */
    boolean getHas_google_mapsDirtyFlag();

    /**
     * 欧盟数字商品增值税
     */
    String getModule_l10n_eu_service();

    void setModule_l10n_eu_service(String module_l10n_eu_service);

    /**
     * 获取 [欧盟数字商品增值税]脏标记
     */
    boolean getModule_l10n_eu_serviceDirtyFlag();

    /**
     * 内容发布网络 (CDN)
     */
    String getCdn_activated();

    void setCdn_activated(String cdn_activated);

    /**
     * 获取 [内容发布网络 (CDN)]脏标记
     */
    boolean getCdn_activatedDirtyFlag();

    /**
     * 默认语言代码
     */
    String getWebsite_default_lang_code();

    void setWebsite_default_lang_code(String website_default_lang_code);

    /**
     * 获取 [默认语言代码]脏标记
     */
    boolean getWebsite_default_lang_codeDirtyFlag();

    /**
     * 允许在登录页开启密码重置功能
     */
    String getAuth_signup_reset_password();

    void setAuth_signup_reset_password(String auth_signup_reset_password);

    /**
     * 获取 [允许在登录页开启密码重置功能]脏标记
     */
    boolean getAuth_signup_reset_passwordDirtyFlag();

    /**
     * 国家/地区分组
     */
    String getWebsite_country_group_ids();

    void setWebsite_country_group_ids(String website_country_group_ids);

    /**
     * 获取 [国家/地区分组]脏标记
     */
    boolean getWebsite_country_group_idsDirtyFlag();

    /**
     * Instagram 账号
     */
    String getSocial_instagram();

    void setSocial_instagram(String social_instagram);

    /**
     * 获取 [Instagram 账号]脏标记
     */
    boolean getSocial_instagramDirtyFlag();

    /**
     * 采购招标
     */
    String getModule_purchase_requisition();

    void setModule_purchase_requisition(String module_purchase_requisition);

    /**
     * 获取 [采购招标]脏标记
     */
    boolean getModule_purchase_requisitionDirtyFlag();

    /**
     * Easypost 接口
     */
    String getModule_delivery_easypost();

    void setModule_delivery_easypost(String module_delivery_easypost);

    /**
     * 获取 [Easypost 接口]脏标记
     */
    boolean getModule_delivery_easypostDirtyFlag();

    /**
     * 折扣
     */
    String getGroup_discount_per_so_line();

    void setGroup_discount_per_so_line(String group_discount_per_so_line);

    /**
     * 获取 [折扣]脏标记
     */
    boolean getGroup_discount_per_so_lineDirtyFlag();

    /**
     * 自动开票
     */
    String getAutomatic_invoice();

    void setAutomatic_invoice(String automatic_invoice);

    /**
     * 获取 [自动开票]脏标记
     */
    boolean getAutomatic_invoiceDirtyFlag();

    /**
     * Plaid 接口
     */
    String getModule_account_plaid();

    void setModule_account_plaid(String module_account_plaid);

    /**
     * 获取 [Plaid 接口]脏标记
     */
    boolean getModule_account_plaidDirtyFlag();

    /**
     * Google 客户 ID
     */
    String getGoogle_management_client_id();

    void setGoogle_management_client_id(String google_management_client_id);

    /**
     * 获取 [Google 客户 ID]脏标记
     */
    boolean getGoogle_management_client_idDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 允许支票打印和存款
     */
    String getModule_account_check_printing();

    void setModule_account_check_printing(String module_account_check_printing);

    /**
     * 获取 [允许支票打印和存款]脏标记
     */
    boolean getModule_account_check_printingDirtyFlag();

    /**
     * 自动填充公司数据
     */
    String getModule_partner_autocomplete();

    void setModule_partner_autocomplete(String module_partner_autocomplete);

    /**
     * 获取 [自动填充公司数据]脏标记
     */
    boolean getModule_partner_autocompleteDirtyFlag();

    /**
     * 语言数量
     */
    Integer getLanguage_count();

    void setLanguage_count(Integer language_count);

    /**
     * 获取 [语言数量]脏标记
     */
    boolean getLanguage_countDirtyFlag();

    /**
     * 链接跟踪器
     */
    String getModule_website_links();

    void setModule_website_links(String module_website_links);

    /**
     * 获取 [链接跟踪器]脏标记
     */
    boolean getModule_website_linksDirtyFlag();

    /**
     * 联系表单上的技术数据
     */
    String getWebsite_form_enable_metadata();

    void setWebsite_form_enable_metadata(String website_form_enable_metadata);

    /**
     * 获取 [联系表单上的技术数据]脏标记
     */
    boolean getWebsite_form_enable_metadataDirtyFlag();

    /**
     * 设置社交平台
     */
    String getHas_social_network();

    void setHas_social_network(String has_social_network);

    /**
     * 获取 [设置社交平台]脏标记
     */
    boolean getHas_social_networkDirtyFlag();

    /**
     * 客户地址
     */
    String getGroup_sale_delivery_address();

    void setGroup_sale_delivery_address(String group_sale_delivery_address);

    /**
     * 获取 [客户地址]脏标记
     */
    boolean getGroup_sale_delivery_addressDirtyFlag();

    /**
     * 谷歌分析密钥
     */
    String getGoogle_analytics_key();

    void setGoogle_analytics_key(String google_analytics_key);

    /**
     * 获取 [谷歌分析密钥]脏标记
     */
    boolean getGoogle_analytics_keyDirtyFlag();

    /**
     * LDAP认证
     */
    String getModule_auth_ldap();

    void setModule_auth_ldap(String module_auth_ldap);

    /**
     * 获取 [LDAP认证]脏标记
     */
    boolean getModule_auth_ldapDirtyFlag();

    /**
     * 具体用户账号
     */
    String getSpecific_user_account();

    void setSpecific_user_account(String specific_user_account);

    /**
     * 获取 [具体用户账号]脏标记
     */
    boolean getSpecific_user_accountDirtyFlag();

    /**
     * 在线发布
     */
    String getModule_website_hr_recruitment();

    void setModule_website_hr_recruitment(String module_website_hr_recruitment);

    /**
     * 获取 [在线发布]脏标记
     */
    boolean getModule_website_hr_recruitmentDirtyFlag();

    /**
     * 预测
     */
    String getModule_project_forecast();

    void setModule_project_forecast(String module_project_forecast);

    /**
     * 获取 [预测]脏标记
     */
    boolean getModule_project_forecastDirtyFlag();

    /**
     * 寄售
     */
    String getGroup_stock_tracking_owner();

    void setGroup_stock_tracking_owner(String group_stock_tracking_owner);

    /**
     * 获取 [寄售]脏标记
     */
    boolean getGroup_stock_tracking_ownerDirtyFlag();

    /**
     * 允许用户同步Google日历
     */
    String getModule_google_calendar();

    void setModule_google_calendar(String module_google_calendar);

    /**
     * 获取 [允许用户同步Google日历]脏标记
     */
    boolean getModule_google_calendarDirtyFlag();

    /**
     * 开票
     */
    String getModule_account();

    void setModule_account(String module_account);

    /**
     * 获取 [开票]脏标记
     */
    boolean getModule_accountDirtyFlag();

    /**
     * 附加Google文档到记录
     */
    String getModule_google_drive();

    void setModule_google_drive(String module_google_drive);

    /**
     * 获取 [附加Google文档到记录]脏标记
     */
    boolean getModule_google_driveDirtyFlag();

    /**
     * POS价格表
     */
    String getPos_pricelist_setting();

    void setPos_pricelist_setting(String pos_pricelist_setting);

    /**
     * 获取 [POS价格表]脏标记
     */
    boolean getPos_pricelist_settingDirtyFlag();

    /**
     * 多公司间所有公司的公共用户
     */
    String getCompany_share_partner();

    void setCompany_share_partner(String company_share_partner);

    /**
     * 获取 [多公司间所有公司的公共用户]脏标记
     */
    boolean getCompany_share_partnerDirtyFlag();

    /**
     * 自动汇率
     */
    String getModule_currency_rate_live();

    void setModule_currency_rate_live(String module_currency_rate_live);

    /**
     * 获取 [自动汇率]脏标记
     */
    boolean getModule_currency_rate_liveDirtyFlag();

    /**
     * 形式发票
     */
    String getGroup_proforma_sales();

    void setGroup_proforma_sales(String group_proforma_sales);

    /**
     * 获取 [形式发票]脏标记
     */
    boolean getGroup_proforma_salesDirtyFlag();

    /**
     * FedEx 接口
     */
    String getModule_delivery_fedex();

    void setModule_delivery_fedex(String module_delivery_fedex);

    /**
     * 获取 [FedEx 接口]脏标记
     */
    boolean getModule_delivery_fedexDirtyFlag();

    /**
     * 特定的EMail
     */
    String getModule_product_email_template();

    void setModule_product_email_template(String module_product_email_template);

    /**
     * 获取 [特定的EMail]脏标记
     */
    boolean getModule_product_email_templateDirtyFlag();

    /**
     * 显示效果
     */
    String getShow_effect();

    void setShow_effect(String show_effect);

    /**
     * 获取 [显示效果]脏标记
     */
    boolean getShow_effectDirtyFlag();

    /**
     * 拣货策略
     */
    String getDefault_picking_policy();

    void setDefault_picking_policy(String default_picking_policy);

    /**
     * 获取 [拣货策略]脏标记
     */
    boolean getDefault_picking_policyDirtyFlag();

    /**
     * Youtube账号
     */
    String getSocial_youtube();

    void setSocial_youtube(String social_youtube);

    /**
     * 获取 [Youtube账号]脏标记
     */
    boolean getSocial_youtubeDirtyFlag();

    /**
     * 网站公司
     */
    Integer getWebsite_company_id();

    void setWebsite_company_id(Integer website_company_id);

    /**
     * 获取 [网站公司]脏标记
     */
    boolean getWebsite_company_idDirtyFlag();

    /**
     * 副产品
     */
    String getModule_mrp_byproduct();

    void setModule_mrp_byproduct(String module_mrp_byproduct);

    /**
     * 获取 [副产品]脏标记
     */
    boolean getModule_mrp_byproductDirtyFlag();

    /**
     * USPS 接口
     */
    String getModule_delivery_usps();

    void setModule_delivery_usps(String module_delivery_usps);

    /**
     * 获取 [USPS 接口]脏标记
     */
    boolean getModule_delivery_uspsDirtyFlag();

    /**
     * DHL 接口
     */
    String getModule_delivery_dhl();

    void setModule_delivery_dhl(String module_delivery_dhl);

    /**
     * 获取 [DHL 接口]脏标记
     */
    boolean getModule_delivery_dhlDirtyFlag();

    /**
     * 使用项目评级
     */
    String getGroup_project_rating();

    void setGroup_project_rating(String group_project_rating);

    /**
     * 获取 [使用项目评级]脏标记
     */
    boolean getGroup_project_ratingDirtyFlag();

    /**
     * Google 地图 API 密钥
     */
    String getGoogle_maps_api_key();

    void setGoogle_maps_api_key(String google_maps_api_key);

    /**
     * 获取 [Google 地图 API 密钥]脏标记
     */
    boolean getGoogle_maps_api_keyDirtyFlag();

    /**
     * 线索
     */
    String getGroup_use_lead();

    void setGroup_use_lead(String group_use_lead);

    /**
     * 获取 [线索]脏标记
     */
    boolean getGroup_use_leadDirtyFlag();

    /**
     * 交货包裹
     */
    String getGroup_stock_tracking_lot();

    void setGroup_stock_tracking_lot(String group_stock_tracking_lot);

    /**
     * 获取 [交货包裹]脏标记
     */
    boolean getGroup_stock_tracking_lotDirtyFlag();

    /**
     * 多步路由
     */
    String getGroup_stock_adv_location();

    void setGroup_stock_adv_location(String group_stock_adv_location);

    /**
     * 获取 [多步路由]脏标记
     */
    boolean getGroup_stock_adv_locationDirtyFlag();

    /**
     * 产品复价
     */
    String getPos_sales_price();

    void setPos_sales_price(String pos_sales_price);

    /**
     * 获取 [产品复价]脏标记
     */
    boolean getPos_sales_priceDirtyFlag();

    /**
     * 公司有科目表
     */
    String getHas_chart_of_accounts();

    void setHas_chart_of_accounts(String has_chart_of_accounts);

    /**
     * 获取 [公司有科目表]脏标记
     */
    boolean getHas_chart_of_accountsDirtyFlag();

    /**
     * 计价方法
     */
    String getMulti_sales_price_method();

    void setMulti_sales_price_method(String multi_sales_price_method);

    /**
     * 获取 [计价方法]脏标记
     */
    boolean getMulti_sales_price_methodDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 保留
     */
    String getModule_procurement_jit();

    void setModule_procurement_jit(String module_procurement_jit);

    /**
     * 获取 [保留]脏标记
     */
    boolean getModule_procurement_jitDirtyFlag();

    /**
     * 资产管理
     */
    String getModule_account_asset();

    void setModule_account_asset(String module_account_asset);

    /**
     * 获取 [资产管理]脏标记
     */
    boolean getModule_account_assetDirtyFlag();

    /**
     * 允许员工通过EMail记录费用
     */
    String getUse_mailgateway();

    void setUse_mailgateway(String use_mailgateway);

    /**
     * 获取 [允许员工通过EMail记录费用]脏标记
     */
    boolean getUse_mailgatewayDirtyFlag();

    /**
     * 为每个客户使用价格表来适配您的价格
     */
    String getGroup_sale_pricelist();

    void setGroup_sale_pricelist(String group_sale_pricelist);

    /**
     * 获取 [为每个客户使用价格表来适配您的价格]脏标记
     */
    boolean getGroup_sale_pricelistDirtyFlag();

    /**
     * 默认销售团队
     */
    Integer getCrm_default_team_id();

    void setCrm_default_team_id(Integer crm_default_team_id);

    /**
     * 获取 [默认销售团队]脏标记
     */
    boolean getCrm_default_team_idDirtyFlag();

    /**
     * 储存位置
     */
    String getGroup_stock_multi_locations();

    void setGroup_stock_multi_locations(String group_stock_multi_locations);

    /**
     * 获取 [储存位置]脏标记
     */
    boolean getGroup_stock_multi_locationsDirtyFlag();

    /**
     * 默认制造提前期
     */
    String getUse_manufacturing_lead();

    void setUse_manufacturing_lead(String use_manufacturing_lead);

    /**
     * 获取 [默认制造提前期]脏标记
     */
    boolean getUse_manufacturing_leadDirtyFlag();

    /**
     * Google 电子表格
     */
    String getModule_google_spreadsheet();

    void setModule_google_spreadsheet(String module_google_spreadsheet);

    /**
     * 获取 [Google 电子表格]脏标记
     */
    boolean getModule_google_spreadsheetDirtyFlag();

    /**
     * 税目汇总表
     */
    String getShow_line_subtotals_tax_selection();

    void setShow_line_subtotals_tax_selection(String show_line_subtotals_tax_selection);

    /**
     * 获取 [税目汇总表]脏标记
     */
    boolean getShow_line_subtotals_tax_selectionDirtyFlag();

    /**
     * 语言
     */
    String getLanguage_ids();

    void setLanguage_ids(String language_ids);

    /**
     * 获取 [语言]脏标记
     */
    boolean getLanguage_idsDirtyFlag();

    /**
     * 电商物流成本
     */
    String getModule_website_sale_delivery();

    void setModule_website_sale_delivery(String module_website_sale_delivery);

    /**
     * 获取 [电商物流成本]脏标记
     */
    boolean getModule_website_sale_deliveryDirtyFlag();

    /**
     * 主生产排程
     */
    String getModule_mrp_mps();

    void setModule_mrp_mps(String module_mrp_mps);

    /**
     * 获取 [主生产排程]脏标记
     */
    boolean getModule_mrp_mpsDirtyFlag();

    /**
     * 信用不足
     */
    String getPartner_autocomplete_insufficient_credit();

    void setPartner_autocomplete_insufficient_credit(String partner_autocomplete_insufficient_credit);

    /**
     * 获取 [信用不足]脏标记
     */
    boolean getPartner_autocomplete_insufficient_creditDirtyFlag();

    /**
     * 显示组织架构图
     */
    String getModule_hr_org_chart();

    void setModule_hr_org_chart(String module_hr_org_chart);

    /**
     * 获取 [显示组织架构图]脏标记
     */
    boolean getModule_hr_org_chartDirtyFlag();

    /**
     * 到期日
     */
    String getModule_product_expiry();

    void setModule_product_expiry(String module_product_expiry);

    /**
     * 获取 [到期日]脏标记
     */
    boolean getModule_product_expiryDirtyFlag();

    /**
     * bpost 接口
     */
    String getModule_delivery_bpost();

    void setModule_delivery_bpost(String module_delivery_bpost);

    /**
     * 获取 [bpost 接口]脏标记
     */
    boolean getModule_delivery_bpostDirtyFlag();

    /**
     * 条码扫描器
     */
    String getModule_stock_barcode();

    void setModule_stock_barcode(String module_stock_barcode);

    /**
     * 获取 [条码扫描器]脏标记
     */
    boolean getModule_stock_barcodeDirtyFlag();

    /**
     * GitHub账户
     */
    String getSocial_github();

    void setSocial_github(String social_github);

    /**
     * 获取 [GitHub账户]脏标记
     */
    boolean getSocial_githubDirtyFlag();

    /**
     * 国际贸易统计组织
     */
    String getModule_account_intrastat();

    void setModule_account_intrastat(String module_account_intrastat);

    /**
     * 获取 [国际贸易统计组织]脏标记
     */
    boolean getModule_account_intrastatDirtyFlag();

    /**
     * 锁定
     */
    String getAuto_done_setting();

    void setAuto_done_setting(String auto_done_setting);

    /**
     * 获取 [锁定]脏标记
     */
    boolean getAuto_done_settingDirtyFlag();

    /**
     * 顾客账号
     */
    String getAuth_signup_uninvited();

    void setAuth_signup_uninvited(String auth_signup_uninvited);

    /**
     * 获取 [顾客账号]脏标记
     */
    boolean getAuth_signup_uninvitedDirtyFlag();

    /**
     * 分享产品 给所有公司
     */
    String getCompany_share_product();

    void setCompany_share_product(String company_share_product);

    /**
     * 获取 [分享产品 给所有公司]脏标记
     */
    boolean getCompany_share_productDirtyFlag();

    /**
     * 报价单模板
     */
    String getGroup_sale_order_template();

    void setGroup_sale_order_template(String group_sale_order_template);

    /**
     * 获取 [报价单模板]脏标记
     */
    boolean getGroup_sale_order_templateDirtyFlag();

    /**
     * 使用SEPA直接计入借方
     */
    String getModule_account_sepa_direct_debit();

    void setModule_account_sepa_direct_debit(String module_account_sepa_direct_debit);

    /**
     * 获取 [使用SEPA直接计入借方]脏标记
     */
    boolean getModule_account_sepa_direct_debitDirtyFlag();

    /**
     * 默认报价有效期
     */
    String getUse_quotation_validity_days();

    void setUse_quotation_validity_days(String use_quotation_validity_days);

    /**
     * 获取 [默认报价有效期]脏标记
     */
    boolean getUse_quotation_validity_daysDirtyFlag();

    /**
     * 催款等级
     */
    String getModule_account_reports_followup();

    void setModule_account_reports_followup(String module_account_reports_followup);

    /**
     * 获取 [催款等级]脏标记
     */
    boolean getModule_account_reports_followupDirtyFlag();

    /**
     * 使用批量付款
     */
    String getModule_account_batch_payment();

    void setModule_account_batch_payment(String module_account_batch_payment);

    /**
     * 获取 [使用批量付款]脏标记
     */
    boolean getModule_account_batch_paymentDirtyFlag();

    /**
     * Twitter账号
     */
    String getSocial_twitter();

    void setSocial_twitter(String social_twitter);

    /**
     * 获取 [Twitter账号]脏标记
     */
    boolean getSocial_twitterDirtyFlag();

    /**
     * 预算管理
     */
    String getModule_account_budget();

    void setModule_account_budget(String module_account_budget);

    /**
     * 获取 [预算管理]脏标记
     */
    boolean getModule_account_budgetDirtyFlag();

    /**
     * MRP 工单
     */
    String getGroup_mrp_routings();

    void setGroup_mrp_routings(String group_mrp_routings);

    /**
     * 获取 [MRP 工单]脏标记
     */
    boolean getGroup_mrp_routingsDirtyFlag();

    /**
     * 现金舍入
     */
    String getGroup_cash_rounding();

    void setGroup_cash_rounding(String group_cash_rounding);

    /**
     * 获取 [现金舍入]脏标记
     */
    boolean getGroup_cash_roundingDirtyFlag();

    /**
     * 到岸成本
     */
    String getModule_stock_landed_costs();

    void setModule_stock_landed_costs(String module_stock_landed_costs);

    /**
     * 获取 [到岸成本]脏标记
     */
    boolean getModule_stock_landed_costsDirtyFlag();

    /**
     * 网站名称
     */
    String getWebsite_name();

    void setWebsite_name(String website_name);

    /**
     * 获取 [网站名称]脏标记
     */
    boolean getWebsite_nameDirtyFlag();

    /**
     * 库存
     */
    String getModule_website_sale_stock();

    void setModule_website_sale_stock(String module_website_sale_stock);

    /**
     * 获取 [库存]脏标记
     */
    boolean getModule_website_sale_stockDirtyFlag();

    /**
     * 网站弹出窗口
     */
    String getGroup_website_popup_on_exit();

    void setGroup_website_popup_on_exit(String group_website_popup_on_exit);

    /**
     * 获取 [网站弹出窗口]脏标记
     */
    boolean getGroup_website_popup_on_exitDirtyFlag();

    /**
     * 耿宗并计划
     */
    String getModule_website_event_track();

    void setModule_website_event_track(String module_website_event_track);

    /**
     * 获取 [耿宗并计划]脏标记
     */
    boolean getModule_website_event_trackDirtyFlag();

    /**
     * 供应商价格表
     */
    String getGroup_manage_vendor_price();

    void setGroup_manage_vendor_price(String group_manage_vendor_price);

    /**
     * 获取 [供应商价格表]脏标记
     */
    boolean getGroup_manage_vendor_priceDirtyFlag();

    /**
     * 运输成本
     */
    String getModule_delivery();

    void setModule_delivery(String module_delivery);

    /**
     * 获取 [运输成本]脏标记
     */
    boolean getModule_deliveryDirtyFlag();

    /**
     * 自动票据处理
     */
    String getModule_account_invoice_extract();

    void setModule_account_invoice_extract(String module_account_invoice_extract);

    /**
     * 获取 [自动票据处理]脏标记
     */
    boolean getModule_account_invoice_extractDirtyFlag();

    /**
     * 网站直播频道
     */
    Integer getChannel_id();

    void setChannel_id(Integer channel_id);

    /**
     * 获取 [网站直播频道]脏标记
     */
    boolean getChannel_idDirtyFlag();

    /**
     * 销售订单警告
     */
    String getGroup_warning_sale();

    void setGroup_warning_sale(String group_warning_sale);

    /**
     * 获取 [销售订单警告]脏标记
     */
    boolean getGroup_warning_saleDirtyFlag();

    /**
     * CDN基本网址
     */
    String getCdn_url();

    void setCdn_url(String cdn_url);

    /**
     * 获取 [CDN基本网址]脏标记
     */
    boolean getCdn_urlDirtyFlag();

    /**
     * 条码
     */
    String getModule_event_barcode();

    void setModule_event_barcode(String module_event_barcode);

    /**
     * 获取 [条码]脏标记
     */
    boolean getModule_event_barcodeDirtyFlag();

    /**
     * 别名域
     */
    String getAlias_domain();

    void setAlias_domain(String alias_domain);

    /**
     * 获取 [别名域]脏标记
     */
    boolean getAlias_domainDirtyFlag();

    /**
     * 领英账号
     */
    String getSocial_linkedin();

    void setSocial_linkedin(String social_linkedin);

    /**
     * 获取 [领英账号]脏标记
     */
    boolean getSocial_linkedinDirtyFlag();

    /**
     * 多仓库
     */
    String getGroup_stock_multi_warehouses();

    void setGroup_stock_multi_warehouses(String group_stock_multi_warehouses);

    /**
     * 获取 [多仓库]脏标记
     */
    boolean getGroup_stock_multi_warehousesDirtyFlag();

    /**
     * 销售员
     */
    Integer getSalesperson_id();

    void setSalesperson_id(Integer salesperson_id);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getSalesperson_idDirtyFlag();

    /**
     * 动态报告
     */
    String getModule_account_reports();

    void setModule_account_reports(String module_account_reports);

    /**
     * 获取 [动态报告]脏标记
     */
    boolean getModule_account_reportsDirtyFlag();

    /**
     * 显示产品的价目表
     */
    String getGroup_product_pricelist();

    void setGroup_product_pricelist(String group_product_pricelist);

    /**
     * 获取 [显示产品的价目表]脏标记
     */
    boolean getGroup_product_pricelistDirtyFlag();

    /**
     * 号码格式
     */
    String getModule_crm_phone_validation();

    void setModule_crm_phone_validation(String module_crm_phone_validation);

    /**
     * 获取 [号码格式]脏标记
     */
    boolean getModule_crm_phone_validationDirtyFlag();

    /**
     * A / B测试
     */
    String getModule_website_version();

    void setModule_website_version(String module_website_version);

    /**
     * 获取 [A / B测试]脏标记
     */
    boolean getModule_website_versionDirtyFlag();

    /**
     * 允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据
     */
    String getModule_base_import();

    void setModule_base_import(String module_base_import);

    /**
     * 获取 [允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据]脏标记
     */
    boolean getModule_base_importDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 以 CSV 格式导入
     */
    String getModule_account_bank_statement_import_csv();

    void setModule_account_bank_statement_import_csv(String module_account_bank_statement_import_csv);

    /**
     * 获取 [以 CSV 格式导入]脏标记
     */
    boolean getModule_account_bank_statement_import_csvDirtyFlag();

    /**
     * 科目税
     */
    String getModule_account_taxcloud();

    void setModule_account_taxcloud(String module_account_taxcloud);

    /**
     * 获取 [科目税]脏标记
     */
    boolean getModule_account_taxcloudDirtyFlag();

    /**
     * 默认条款和条件
     */
    String getUse_sale_note();

    void setUse_sale_note(String use_sale_note);

    /**
     * 获取 [默认条款和条件]脏标记
     */
    boolean getUse_sale_noteDirtyFlag();

    /**
     * 放弃时长
     */
    Double getCart_abandoned_delay();

    void setCart_abandoned_delay(Double cart_abandoned_delay);

    /**
     * 获取 [放弃时长]脏标记
     */
    boolean getCart_abandoned_delayDirtyFlag();

    /**
     * 网站域名
     */
    String getWebsite_domain();

    void setWebsite_domain(String website_domain);

    /**
     * 获取 [网站域名]脏标记
     */
    boolean getWebsite_domainDirtyFlag();

    /**
     * Accounting
     */
    String getModule_account_accountant();

    void setModule_account_accountant(String module_account_accountant);

    /**
     * 获取 [Accounting]脏标记
     */
    boolean getModule_account_accountantDirtyFlag();

    /**
     * 毛利
     */
    String getModule_sale_margin();

    void setModule_sale_margin(String module_sale_margin);

    /**
     * 获取 [毛利]脏标记
     */
    boolean getModule_sale_marginDirtyFlag();

    /**
     * 摘要邮件
     */
    String getDigest_emails();

    void setDigest_emails(String digest_emails);

    /**
     * 获取 [摘要邮件]脏标记
     */
    boolean getDigest_emailsDirtyFlag();

    /**
     * 协作pad
     */
    String getModule_pad();

    void setModule_pad(String module_pad);

    /**
     * 获取 [协作pad]脏标记
     */
    boolean getModule_padDirtyFlag();

    /**
     * 发票警告
     */
    String getGroup_warning_account();

    void setGroup_warning_account(String group_warning_account);

    /**
     * 获取 [发票警告]脏标记
     */
    boolean getGroup_warning_accountDirtyFlag();

    /**
     * 贸易条款
     */
    String getGroup_display_incoterm();

    void setGroup_display_incoterm(String group_display_incoterm);

    /**
     * 获取 [贸易条款]脏标记
     */
    boolean getGroup_display_incotermDirtyFlag();

    /**
     * 心愿单
     */
    String getModule_website_sale_wishlist();

    void setModule_website_sale_wishlist(String module_website_sale_wishlist);

    /**
     * 获取 [心愿单]脏标记
     */
    boolean getModule_website_sale_wishlistDirtyFlag();

    /**
     * 默认访问权限
     */
    String getUser_default_rights();

    void setUser_default_rights(String user_default_rights);

    /**
     * 获取 [默认访问权限]脏标记
     */
    boolean getUser_default_rightsDirtyFlag();

    /**
     * 账单控制
     */
    String getDefault_purchase_method();

    void setDefault_purchase_method(String default_purchase_method);

    /**
     * 获取 [账单控制]脏标记
     */
    boolean getDefault_purchase_methodDirtyFlag();

    /**
     * 送货地址
     */
    String getGroup_delivery_invoice_address();

    void setGroup_delivery_invoice_address(String group_delivery_invoice_address);

    /**
     * 获取 [送货地址]脏标记
     */
    boolean getGroup_delivery_invoice_addressDirtyFlag();

    /**
     * 显示批次 / 序列号
     */
    String getGroup_lot_on_delivery_slip();

    void setGroup_lot_on_delivery_slip(String group_lot_on_delivery_slip);

    /**
     * 获取 [显示批次 / 序列号]脏标记
     */
    boolean getGroup_lot_on_delivery_slipDirtyFlag();

    /**
     * 入场券
     */
    String getModule_event_sale();

    void setModule_event_sale(String module_event_sale);

    /**
     * 获取 [入场券]脏标记
     */
    boolean getModule_event_saleDirtyFlag();

    /**
     * 显示含税明细行在汇总表(B2B).
     */
    String getGroup_show_line_subtotals_tax_included();

    void setGroup_show_line_subtotals_tax_included(String group_show_line_subtotals_tax_included);

    /**
     * 获取 [显示含税明细行在汇总表(B2B).]脏标记
     */
    boolean getGroup_show_line_subtotals_tax_includedDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 变体和选项
     */
    String getGroup_product_variant();

    void setGroup_product_variant(String group_product_variant);

    /**
     * 获取 [变体和选项]脏标记
     */
    boolean getGroup_product_variantDirtyFlag();

    /**
     * SEPA贷记交易
     */
    String getModule_account_sepa();

    void setModule_account_sepa(String module_account_sepa);

    /**
     * 获取 [SEPA贷记交易]脏标记
     */
    boolean getModule_account_sepaDirtyFlag();

    /**
     * 多币种
     */
    String getGroup_multi_currency();

    void setGroup_multi_currency(String group_multi_currency);

    /**
     * 获取 [多币种]脏标记
     */
    boolean getGroup_multi_currencyDirtyFlag();

    /**
     * 使用供应商单据里的产品
     */
    String getGroup_products_in_bills();

    void setGroup_products_in_bills(String group_products_in_bills);

    /**
     * 获取 [使用供应商单据里的产品]脏标记
     */
    boolean getGroup_products_in_billsDirtyFlag();

    /**
     * 分析会计
     */
    String getGroup_analytic_accounting();

    void setGroup_analytic_accounting(String group_analytic_accounting);

    /**
     * 获取 [分析会计]脏标记
     */
    boolean getGroup_analytic_accountingDirtyFlag();

    /**
     * 产品包装
     */
    String getGroup_stock_packaging();

    void setGroup_stock_packaging(String group_stock_packaging);

    /**
     * 获取 [产品包装]脏标记
     */
    boolean getGroup_stock_packagingDirtyFlag();

    /**
     * 谷歌文档密钥
     */
    String getWebsite_slide_google_app_key();

    void setWebsite_slide_google_app_key(String website_slide_google_app_key);

    /**
     * 获取 [谷歌文档密钥]脏标记
     */
    boolean getWebsite_slide_google_app_keyDirtyFlag();

    /**
     * 采购订单批准
     */
    String getPo_order_approval();

    void setPo_order_approval(String po_order_approval);

    /**
     * 获取 [采购订单批准]脏标记
     */
    boolean getPo_order_approvalDirtyFlag();

    /**
     * 销售模块是否已安装
     */
    String getIs_installed_sale();

    void setIs_installed_sale(String is_installed_sale);

    /**
     * 获取 [销售模块是否已安装]脏标记
     */
    boolean getIs_installed_saleDirtyFlag();

    /**
     * 发票在线付款
     */
    String getModule_account_payment();

    void setModule_account_payment(String module_account_payment);

    /**
     * 获取 [发票在线付款]脏标记
     */
    boolean getModule_account_paymentDirtyFlag();

    /**
     * 分析标签
     */
    String getGroup_analytic_tags();

    void setGroup_analytic_tags(String group_analytic_tags);

    /**
     * 获取 [分析标签]脏标记
     */
    boolean getGroup_analytic_tagsDirtyFlag();

    /**
     * 交货日期
     */
    String getGroup_sale_order_dates();

    void setGroup_sale_order_dates(String group_sale_order_dates);

    /**
     * 获取 [交货日期]脏标记
     */
    boolean getGroup_sale_order_datesDirtyFlag();

    /**
     * Asterisk (开源VoIP平台)
     */
    String getModule_voip();

    void setModule_voip(String module_voip);

    /**
     * 获取 [Asterisk (开源VoIP平台)]脏标记
     */
    boolean getModule_voipDirtyFlag();

    /**
     * 购物车恢复EMail
     */
    Integer getCart_recovery_mail_template();

    void setCart_recovery_mail_template(Integer cart_recovery_mail_template);

    /**
     * 获取 [购物车恢复EMail]脏标记
     */
    boolean getCart_recovery_mail_templateDirtyFlag();

    /**
     * 多网站
     */
    String getGroup_multi_website();

    void setGroup_multi_website(String group_multi_website);

    /**
     * 获取 [多网站]脏标记
     */
    boolean getGroup_multi_websiteDirtyFlag();

    /**
     * 使用外部验证提供者 (OAuth)
     */
    String getModule_auth_oauth();

    void setModule_auth_oauth(String module_auth_oauth);

    /**
     * 获取 [使用外部验证提供者 (OAuth)]脏标记
     */
    boolean getModule_auth_oauthDirtyFlag();

    /**
     * 送货管理
     */
    String getSale_delivery_settings();

    void setSale_delivery_settings(String sale_delivery_settings);

    /**
     * 获取 [送货管理]脏标记
     */
    boolean getSale_delivery_settingsDirtyFlag();

    /**
     * 报价单生成器
     */
    String getModule_sale_quotation_builder();

    void setModule_sale_quotation_builder(String module_sale_quotation_builder);

    /**
     * 获取 [报价单生成器]脏标记
     */
    boolean getModule_sale_quotation_builderDirtyFlag();

    /**
     * 管理公司间交易
     */
    String getModule_inter_company_rules();

    void setModule_inter_company_rules(String module_inter_company_rules);

    /**
     * 获取 [管理公司间交易]脏标记
     */
    boolean getModule_inter_company_rulesDirtyFlag();

    /**
     * 销售的安全提前期
     */
    String getUse_security_lead();

    void setUse_security_lead(String use_security_lead);

    /**
     * 获取 [销售的安全提前期]脏标记
     */
    boolean getUse_security_leadDirtyFlag();

    /**
     * 开票策略
     */
    String getDefault_invoice_policy();

    void setDefault_invoice_policy(String default_invoice_policy);

    /**
     * 获取 [开票策略]脏标记
     */
    boolean getDefault_invoice_policyDirtyFlag();

    /**
     * 每个产品的多种销售价格
     */
    String getMulti_sales_price();

    void setMulti_sales_price(String multi_sales_price);

    /**
     * 获取 [每个产品的多种销售价格]脏标记
     */
    boolean getMulti_sales_priceDirtyFlag();

    /**
     * 锁定确认订单
     */
    String getLock_confirmed_po();

    void setLock_confirmed_po(String lock_confirmed_po);

    /**
     * 获取 [锁定确认订单]脏标记
     */
    boolean getLock_confirmed_poDirtyFlag();

    /**
     * 重量单位
     */
    String getProduct_weight_in_lbs();

    void setProduct_weight_in_lbs(String product_weight_in_lbs);

    /**
     * 获取 [重量单位]脏标记
     */
    boolean getProduct_weight_in_lbsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 谷歌分析仪表板
     */
    String getHas_google_analytics_dashboard();

    void setHas_google_analytics_dashboard(String has_google_analytics_dashboard);

    /**
     * 获取 [谷歌分析仪表板]脏标记
     */
    boolean getHas_google_analytics_dashboardDirtyFlag();

    /**
     * 批量拣货
     */
    String getModule_stock_picking_batch();

    void setModule_stock_picking_batch(String module_stock_picking_batch);

    /**
     * 获取 [批量拣货]脏标记
     */
    boolean getModule_stock_picking_batchDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 默认的费用别名
     */
    String getExpense_alias_prefix();

    void setExpense_alias_prefix(String expense_alias_prefix);

    /**
     * 获取 [默认的费用别名]脏标记
     */
    boolean getExpense_alias_prefixDirtyFlag();

    /**
     * 收入识别
     */
    String getModule_account_deferred_revenue();

    void setModule_account_deferred_revenue(String module_account_deferred_revenue);

    /**
     * 获取 [收入识别]脏标记
     */
    boolean getModule_account_deferred_revenueDirtyFlag();

    /**
     * 脸书账号
     */
    String getSocial_facebook();

    void setSocial_facebook(String social_facebook);

    /**
     * 获取 [脸书账号]脏标记
     */
    boolean getSocial_facebookDirtyFlag();

    /**
     * Unsplash图像库
     */
    String getModule_web_unsplash();

    void setModule_web_unsplash(String module_web_unsplash);

    /**
     * 获取 [Unsplash图像库]脏标记
     */
    boolean getModule_web_unsplashDirtyFlag();

    /**
     * 群发邮件营销
     */
    String getGroup_mass_mailing_campaign();

    void setGroup_mass_mailing_campaign(String group_mass_mailing_campaign);

    /**
     * 获取 [群发邮件营销]脏标记
     */
    boolean getGroup_mass_mailing_campaignDirtyFlag();

    /**
     * 用 CAMT.053 格式导入
     */
    String getModule_account_bank_statement_import_camt();

    void setModule_account_bank_statement_import_camt(String module_account_bank_statement_import_camt);

    /**
     * 获取 [用 CAMT.053 格式导入]脏标记
     */
    boolean getModule_account_bank_statement_import_camtDirtyFlag();

    /**
     * 允许产品毛利
     */
    String getModule_product_margin();

    void setModule_product_margin(String module_product_margin);

    /**
     * 获取 [允许产品毛利]脏标记
     */
    boolean getModule_product_marginDirtyFlag();

    /**
     * 子任务
     */
    String getGroup_subtask_project();

    void setGroup_subtask_project(String group_subtask_project);

    /**
     * 获取 [子任务]脏标记
     */
    boolean getGroup_subtask_projectDirtyFlag();

    /**
     * 三方匹配:采购，收货和发票
     */
    String getModule_account_3way_match();

    void setModule_account_3way_match(String module_account_3way_match);

    /**
     * 获取 [三方匹配:采购，收货和发票]脏标记
     */
    boolean getModule_account_3way_matchDirtyFlag();

    /**
     * 数字内容
     */
    String getModule_website_sale_digital();

    void setModule_website_sale_digital(String module_website_sale_digital);

    /**
     * 获取 [数字内容]脏标记
     */
    boolean getModule_website_sale_digitalDirtyFlag();

    /**
     * 优惠券和促销
     */
    String getModule_sale_coupon();

    void setModule_sale_coupon(String module_sale_coupon);

    /**
     * 获取 [优惠券和促销]脏标记
     */
    boolean getModule_sale_couponDirtyFlag();

    /**
     * 在取消订阅页面上显示黑名单按钮
     */
    String getShow_blacklist_buttons();

    void setShow_blacklist_buttons(String show_blacklist_buttons);

    /**
     * 获取 [在取消订阅页面上显示黑名单按钮]脏标记
     */
    boolean getShow_blacklist_buttonsDirtyFlag();

    /**
     * 库存警报
     */
    String getGroup_warning_stock();

    void setGroup_warning_stock(String group_warning_stock);

    /**
     * 获取 [库存警报]脏标记
     */
    boolean getGroup_warning_stockDirtyFlag();

    /**
     * 管理多公司
     */
    String getGroup_multi_company();

    void setGroup_multi_company(String group_multi_company);

    /**
     * 获取 [管理多公司]脏标记
     */
    boolean getGroup_multi_companyDirtyFlag();

    /**
     * 员工 PIN
     */
    String getGroup_attendance_use_pin();

    void setGroup_attendance_use_pin(String group_attendance_use_pin);

    /**
     * 获取 [员工 PIN]脏标记
     */
    boolean getGroup_attendance_use_pinDirtyFlag();

    /**
     * 销售团队
     */
    Integer getSalesteam_id();

    void setSalesteam_id(Integer salesteam_id);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getSalesteam_idDirtyFlag();

    /**
     * 图标
     */
    byte[] getFavicon();

    void setFavicon(byte[] favicon);

    /**
     * 获取 [图标]脏标记
     */
    boolean getFaviconDirtyFlag();

    /**
     * 产品比较工具
     */
    String getModule_website_sale_comparison();

    void setModule_website_sale_comparison(String module_website_sale_comparison);

    /**
     * 获取 [产品比较工具]脏标记
     */
    boolean getModule_website_sale_comparisonDirtyFlag();

    /**
     * 可用阈值
     */
    Double getAvailable_threshold();

    void setAvailable_threshold(Double available_threshold);

    /**
     * 获取 [可用阈值]脏标记
     */
    boolean getAvailable_thresholdDirtyFlag();

    /**
     * 安全交货时间
     */
    String getUse_po_lead();

    void setUse_po_lead(String use_po_lead);

    /**
     * 获取 [安全交货时间]脏标记
     */
    boolean getUse_po_leadDirtyFlag();

    /**
     * 财年
     */
    String getGroup_fiscal_year();

    void setGroup_fiscal_year(String group_fiscal_year);

    /**
     * 获取 [财年]脏标记
     */
    boolean getGroup_fiscal_yearDirtyFlag();

    /**
     * 任务日志
     */
    String getModule_hr_timesheet();

    void setModule_hr_timesheet(String module_hr_timesheet);

    /**
     * 获取 [任务日志]脏标记
     */
    boolean getModule_hr_timesheetDirtyFlag();

    /**
     * 产品生命周期管理 (PLM)
     */
    String getModule_mrp_plm();

    void setModule_mrp_plm(String module_mrp_plm);

    /**
     * 获取 [产品生命周期管理 (PLM)]脏标记
     */
    boolean getModule_mrp_plmDirtyFlag();

    /**
     * 银行接口－自动同步银行费用
     */
    String getModule_account_yodlee();

    void setModule_account_yodlee(String module_account_yodlee);

    /**
     * 获取 [银行接口－自动同步银行费用]脏标记
     */
    boolean getModule_account_yodleeDirtyFlag();

    /**
     * CDN筛选
     */
    String getCdn_filters();

    void setCdn_filters(String cdn_filters);

    /**
     * 获取 [CDN筛选]脏标记
     */
    boolean getCdn_filtersDirtyFlag();

    /**
     * 无重新排程传播
     */
    String getUse_propagation_minimum_delta();

    void setUse_propagation_minimum_delta(String use_propagation_minimum_delta);

    /**
     * 获取 [无重新排程传播]脏标记
     */
    boolean getUse_propagation_minimum_deltaDirtyFlag();

    /**
     * UPS 接口
     */
    String getModule_delivery_ups();

    void setModule_delivery_ups(String module_delivery_ups);

    /**
     * 获取 [UPS 接口]脏标记
     */
    boolean getModule_delivery_upsDirtyFlag();

    /**
     * 失败的邮件
     */
    Integer getFail_counter();

    void setFail_counter(Integer fail_counter);

    /**
     * 获取 [失败的邮件]脏标记
     */
    boolean getFail_counterDirtyFlag();

    /**
     * 批次和序列号
     */
    String getGroup_stock_production_lot();

    void setGroup_stock_production_lot(String group_stock_production_lot);

    /**
     * 获取 [批次和序列号]脏标记
     */
    boolean getGroup_stock_production_lotDirtyFlag();

    /**
     * 有会计分录
     */
    String getHas_accounting_entries();

    void setHas_accounting_entries(String has_accounting_entries);

    /**
     * 获取 [有会计分录]脏标记
     */
    boolean getHas_accounting_entriesDirtyFlag();

    /**
     * 计量单位
     */
    String getGroup_uom();

    void setGroup_uom(String group_uom);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getGroup_uomDirtyFlag();

    /**
     * 指定邮件服务器
     */
    String getMass_mailing_outgoing_mail_server();

    void setMass_mailing_outgoing_mail_server(String mass_mailing_outgoing_mail_server);

    /**
     * 获取 [指定邮件服务器]脏标记
     */
    boolean getMass_mailing_outgoing_mail_serverDirtyFlag();

    /**
     * 默认线索别名
     */
    String getCrm_alias_prefix();

    void setCrm_alias_prefix(String crm_alias_prefix);

    /**
     * 获取 [默认线索别名]脏标记
     */
    boolean getCrm_alias_prefixDirtyFlag();

    /**
     * Google+账户
     */
    String getSocial_googleplus();

    void setSocial_googleplus(String social_googleplus);

    /**
     * 获取 [Google+账户]脏标记
     */
    boolean getSocial_googleplusDirtyFlag();

    /**
     * 订单特定路线
     */
    String getGroup_route_so_lines();

    void setGroup_route_so_lines(String group_route_so_lines);

    /**
     * 获取 [订单特定路线]脏标记
     */
    boolean getGroup_route_so_linesDirtyFlag();

    /**
     * Google 客户端密钥
     */
    String getGoogle_management_client_secret();

    void setGoogle_management_client_secret(String google_management_client_secret);

    /**
     * 获取 [Google 客户端密钥]脏标记
     */
    boolean getGoogle_management_client_secretDirtyFlag();

    /**
     * 默认社交分享图片
     */
    byte[] getSocial_default_image();

    void setSocial_default_image(byte[] social_default_image);

    /**
     * 获取 [默认社交分享图片]脏标记
     */
    boolean getSocial_default_imageDirtyFlag();

    /**
     * Google 分析
     */
    String getHas_google_analytics();

    void setHas_google_analytics(String has_google_analytics);

    /**
     * 获取 [Google 分析]脏标记
     */
    boolean getHas_google_analyticsDirtyFlag();

    /**
     * 调查登记
     */
    String getModule_website_event_questions();

    void setModule_website_event_questions(String module_website_event_questions);

    /**
     * 获取 [调查登记]脏标记
     */
    boolean getModule_website_event_questionsDirtyFlag();

    /**
     * 默认语言
     */
    Integer getWebsite_default_lang_id();

    void setWebsite_default_lang_id(Integer website_default_lang_id);

    /**
     * 获取 [默认语言]脏标记
     */
    boolean getWebsite_default_lang_idDirtyFlag();

    /**
     * 库存可用性
     */
    String getInventory_availability();

    void setInventory_availability(String inventory_availability);

    /**
     * 获取 [库存可用性]脏标记
     */
    boolean getInventory_availabilityDirtyFlag();

    /**
     * 采购警告
     */
    String getGroup_warning_purchase();

    void setGroup_warning_purchase(String group_warning_purchase);

    /**
     * 获取 [采购警告]脏标记
     */
    boolean getGroup_warning_purchaseDirtyFlag();

    /**
     * 质量
     */
    String getModule_quality_control();

    void setModule_quality_control(String module_quality_control);

    /**
     * 获取 [质量]脏标记
     */
    boolean getModule_quality_controlDirtyFlag();

    /**
     * 手动分配EMail
     */
    String getGenerate_lead_from_alias();

    void setGenerate_lead_from_alias(String generate_lead_from_alias);

    /**
     * 获取 [手动分配EMail]脏标记
     */
    boolean getGenerate_lead_from_aliasDirtyFlag();

    /**
     * 价格表
     */
    String getSale_pricelist_setting();

    void setSale_pricelist_setting(String sale_pricelist_setting);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getSale_pricelist_settingDirtyFlag();

    /**
     * 给客户显示价目表
     */
    String getGroup_pricelist_item();

    void setGroup_pricelist_item(String group_pricelist_item);

    /**
     * 获取 [给客户显示价目表]脏标记
     */
    boolean getGroup_pricelist_itemDirtyFlag();

    /**
     * 外部邮件服务器
     */
    String getExternal_email_server_default();

    void setExternal_email_server_default(String external_email_server_default);

    /**
     * 获取 [外部邮件服务器]脏标记
     */
    boolean getExternal_email_server_defaultDirtyFlag();

    /**
     * 访问秘钥
     */
    String getUnsplash_access_key();

    void setUnsplash_access_key(String unsplash_access_key);

    /**
     * 获取 [访问秘钥]脏标记
     */
    boolean getUnsplash_access_keyDirtyFlag();

    /**
     * 用Gengo翻译您的网站
     */
    String getModule_base_gengo();

    void setModule_base_gengo(String module_base_gengo);

    /**
     * 获取 [用Gengo翻译您的网站]脏标记
     */
    boolean getModule_base_gengoDirtyFlag();

    /**
     * 在线票务
     */
    String getModule_website_event_sale();

    void setModule_website_event_sale(String module_website_event_sale);

    /**
     * 获取 [在线票务]脏标记
     */
    boolean getModule_website_event_saleDirtyFlag();

    /**
     * 默认销售人员
     */
    Integer getCrm_default_user_id();

    void setCrm_default_user_id(Integer crm_default_user_id);

    /**
     * 获取 [默认销售人员]脏标记
     */
    boolean getCrm_default_user_idDirtyFlag();

    /**
     * 代发货
     */
    String getModule_stock_dropshipping();

    void setModule_stock_dropshipping(String module_stock_dropshipping);

    /**
     * 获取 [代发货]脏标记
     */
    boolean getModule_stock_dropshippingDirtyFlag();

    /**
     * 从你的网站流量创建线索/商机
     */
    String getModule_crm_reveal();

    void setModule_crm_reveal(String module_crm_reveal);

    /**
     * 获取 [从你的网站流量创建线索/商机]脏标记
     */
    boolean getModule_crm_revealDirtyFlag();

    /**
     * 邮件服务器
     */
    Integer getMass_mailing_mail_server_id();

    void setMass_mailing_mail_server_id(Integer mass_mailing_mail_server_id);

    /**
     * 获取 [邮件服务器]脏标记
     */
    boolean getMass_mailing_mail_server_idDirtyFlag();

    /**
     * 明细行汇总含税(B2B).
     */
    String getGroup_show_line_subtotals_tax_excluded();

    void setGroup_show_line_subtotals_tax_excluded(String group_show_line_subtotals_tax_excluded);

    /**
     * 获取 [明细行汇总含税(B2B).]脏标记
     */
    boolean getGroup_show_line_subtotals_tax_excludedDirtyFlag();

    /**
     * 面试表单
     */
    String getModule_hr_recruitment_survey();

    void setModule_hr_recruitment_survey(String module_hr_recruitment_survey);

    /**
     * 获取 [面试表单]脏标记
     */
    boolean getModule_hr_recruitment_surveyDirtyFlag();

    /**
     * 工单
     */
    String getModule_mrp_workorder();

    void setModule_mrp_workorder(String module_mrp_workorder);

    /**
     * 获取 [工单]脏标记
     */
    boolean getModule_mrp_workorderDirtyFlag();

    /**
     * 集成卡支付
     */
    String getModule_pos_mercury();

    void setModule_pos_mercury(String module_pos_mercury);

    /**
     * 获取 [集成卡支付]脏标记
     */
    boolean getModule_pos_mercuryDirtyFlag();

    /**
     * 交流
     */
    String getInvoice_reference_type();

    void setInvoice_reference_type(String invoice_reference_type);

    /**
     * 获取 [交流]脏标记
     */
    boolean getInvoice_reference_typeDirtyFlag();

    /**
     * 公司货币
     */
    Integer getCompany_currency_id();

    void setCompany_currency_id(Integer company_currency_id);

    /**
     * 获取 [公司货币]脏标记
     */
    boolean getCompany_currency_idDirtyFlag();

    /**
     * 现金收付制
     */
    String getTax_exigibility();

    void setTax_exigibility(String tax_exigibility);

    /**
     * 获取 [现金收付制]脏标记
     */
    boolean getTax_exigibilityDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 银行核销阈值
     */
    Timestamp getAccount_bank_reconciliation_start();

    void setAccount_bank_reconciliation_start(Timestamp account_bank_reconciliation_start);

    /**
     * 获取 [银行核销阈值]脏标记
     */
    boolean getAccount_bank_reconciliation_startDirtyFlag();

    /**
     * 税率现金收付制日记账
     */
    Integer getTax_cash_basis_journal_id();

    void setTax_cash_basis_journal_id(Integer tax_cash_basis_journal_id);

    /**
     * 获取 [税率现金收付制日记账]脏标记
     */
    boolean getTax_cash_basis_journal_idDirtyFlag();

    /**
     * 采购提前时间
     */
    Double getPo_lead();

    void setPo_lead(Double po_lead);

    /**
     * 获取 [采购提前时间]脏标记
     */
    boolean getPo_leadDirtyFlag();

    /**
     * 彩色打印
     */
    String getSnailmail_color();

    void setSnailmail_color(String snailmail_color);

    /**
     * 获取 [彩色打印]脏标记
     */
    boolean getSnailmail_colorDirtyFlag();

    /**
     * 纸张格式
     */
    Integer getPaperformat_id();

    void setPaperformat_id(Integer paperformat_id);

    /**
     * 获取 [纸张格式]脏标记
     */
    boolean getPaperformat_idDirtyFlag();

    /**
     * 在线签名
     */
    String getPortal_confirmation_sign();

    void setPortal_confirmation_sign(String portal_confirmation_sign);

    /**
     * 获取 [在线签名]脏标记
     */
    boolean getPortal_confirmation_signDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 摘要邮件
     */
    String getDigest_id_text();

    void setDigest_id_text(String digest_id_text);

    /**
     * 获取 [摘要邮件]脏标记
     */
    boolean getDigest_id_textDirtyFlag();

    /**
     * 条款及条件
     */
    String getSale_note();

    void setSale_note(String sale_note);

    /**
     * 获取 [条款及条件]脏标记
     */
    boolean getSale_noteDirtyFlag();

    /**
     * 用作通过注册创建的新用户的模版
     */
    String getAuth_signup_template_user_id_text();

    void setAuth_signup_template_user_id_text(String auth_signup_template_user_id_text);

    /**
     * 获取 [用作通过注册创建的新用户的模版]脏标记
     */
    boolean getAuth_signup_template_user_id_textDirtyFlag();

    /**
     * 打印
     */
    String getInvoice_is_print();

    void setInvoice_is_print(String invoice_is_print);

    /**
     * 获取 [打印]脏标记
     */
    boolean getInvoice_is_printDirtyFlag();

    /**
     * 押金产品
     */
    String getDeposit_default_product_id_text();

    void setDeposit_default_product_id_text(String deposit_default_product_id_text);

    /**
     * 获取 [押金产品]脏标记
     */
    boolean getDeposit_default_product_id_textDirtyFlag();

    /**
     * 公司的上班时间
     */
    Integer getResource_calendar_id();

    void setResource_calendar_id(Integer resource_calendar_id);

    /**
     * 获取 [公司的上班时间]脏标记
     */
    boolean getResource_calendar_idDirtyFlag();

    /**
     * 采购订单修改 *
     */
    String getPo_lock();

    void setPo_lock(String po_lock);

    /**
     * 获取 [采购订单修改 *]脏标记
     */
    boolean getPo_lockDirtyFlag();

    /**
     * 透过邮递
     */
    String getInvoice_is_snailmail();

    void setInvoice_is_snailmail(String invoice_is_snailmail);

    /**
     * 获取 [透过邮递]脏标记
     */
    boolean getInvoice_is_snailmailDirtyFlag();

    /**
     * 制造提前期(日)
     */
    Double getManufacturing_lead();

    void setManufacturing_lead(Double manufacturing_lead);

    /**
     * 获取 [制造提前期(日)]脏标记
     */
    boolean getManufacturing_leadDirtyFlag();

    /**
     * 审批层级 *
     */
    String getPo_double_validation();

    void setPo_double_validation(String po_double_validation);

    /**
     * 获取 [审批层级 *]脏标记
     */
    boolean getPo_double_validationDirtyFlag();

    /**
     * 自定义报表页脚
     */
    String getReport_footer();

    void setReport_footer(String report_footer);

    /**
     * 获取 [自定义报表页脚]脏标记
     */
    boolean getReport_footerDirtyFlag();

    /**
     * EMail模板
     */
    String getTemplate_id_text();

    void setTemplate_id_text(String template_id_text);

    /**
     * 获取 [EMail模板]脏标记
     */
    boolean getTemplate_id_textDirtyFlag();

    /**
     * 发送EMail
     */
    String getInvoice_is_email();

    void setInvoice_is_email(String invoice_is_email);

    /**
     * 获取 [发送EMail]脏标记
     */
    boolean getInvoice_is_emailDirtyFlag();

    /**
     * 在线支付
     */
    String getPortal_confirmation_pay();

    void setPortal_confirmation_pay(String portal_confirmation_pay);

    /**
     * 获取 [在线支付]脏标记
     */
    boolean getPortal_confirmation_payDirtyFlag();

    /**
     * 显示SEPA QR码
     */
    String getQr_code();

    void setQr_code(String qr_code);

    /**
     * 获取 [显示SEPA QR码]脏标记
     */
    boolean getQr_codeDirtyFlag();

    /**
     * 双面打印
     */
    String getSnailmail_duplex();

    void setSnailmail_duplex(String snailmail_duplex);

    /**
     * 获取 [双面打印]脏标记
     */
    boolean getSnailmail_duplexDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 安全时间
     */
    Double getSecurity_lead();

    void setSecurity_lead(Double security_lead);

    /**
     * 获取 [安全时间]脏标记
     */
    boolean getSecurity_leadDirtyFlag();

    /**
     * 文档模板
     */
    Integer getExternal_report_layout_id();

    void setExternal_report_layout_id(Integer external_report_layout_id);

    /**
     * 获取 [文档模板]脏标记
     */
    boolean getExternal_report_layout_idDirtyFlag();

    /**
     * 默认进项税
     */
    Integer getPurchase_tax_id();

    void setPurchase_tax_id(Integer purchase_tax_id);

    /**
     * 获取 [默认进项税]脏标记
     */
    boolean getPurchase_tax_idDirtyFlag();

    /**
     * 传播的最小差值
     */
    Integer getPropagation_minimum_delta();

    void setPropagation_minimum_delta(Integer propagation_minimum_delta);

    /**
     * 获取 [传播的最小差值]脏标记
     */
    boolean getPropagation_minimum_deltaDirtyFlag();

    /**
     * 默认报价有效期（日）
     */
    Integer getQuotation_validity_days();

    void setQuotation_validity_days(Integer quotation_validity_days);

    /**
     * 获取 [默认报价有效期（日）]脏标记
     */
    boolean getQuotation_validity_daysDirtyFlag();

    /**
     * 税率计算的舍入方法
     */
    String getTax_calculation_rounding_method();

    void setTax_calculation_rounding_method(String tax_calculation_rounding_method);

    /**
     * 获取 [税率计算的舍入方法]脏标记
     */
    boolean getTax_calculation_rounding_methodDirtyFlag();

    /**
     * 汇兑损益
     */
    Integer getCurrency_exchange_journal_id();

    void setCurrency_exchange_journal_id(Integer currency_exchange_journal_id);

    /**
     * 获取 [汇兑损益]脏标记
     */
    boolean getCurrency_exchange_journal_idDirtyFlag();

    /**
     * 最小金额
     */
    Double getPo_double_validation_amount();

    void setPo_double_validation_amount(Double po_double_validation_amount);

    /**
     * 获取 [最小金额]脏标记
     */
    boolean getPo_double_validation_amountDirtyFlag();

    /**
     * 模板
     */
    String getChart_template_id_text();

    void setChart_template_id_text(String chart_template_id_text);

    /**
     * 获取 [模板]脏标记
     */
    boolean getChart_template_id_textDirtyFlag();

    /**
     * 默认销售税
     */
    Integer getSale_tax_id();

    void setSale_tax_id(Integer sale_tax_id);

    /**
     * 获取 [默认销售税]脏标记
     */
    boolean getSale_tax_idDirtyFlag();

    /**
     * 默认模板
     */
    String getDefault_sale_order_template_id_text();

    void setDefault_sale_order_template_id_text(String default_sale_order_template_id_text);

    /**
     * 获取 [默认模板]脏标记
     */
    boolean getDefault_sale_order_template_id_textDirtyFlag();

    /**
     * 默认模板
     */
    Integer getDefault_sale_order_template_id();

    void setDefault_sale_order_template_id(Integer default_sale_order_template_id);

    /**
     * 获取 [默认模板]脏标记
     */
    boolean getDefault_sale_order_template_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 摘要邮件
     */
    Integer getDigest_id();

    void setDigest_id(Integer digest_id);

    /**
     * 获取 [摘要邮件]脏标记
     */
    boolean getDigest_idDirtyFlag();

    /**
     * EMail模板
     */
    Integer getTemplate_id();

    void setTemplate_id(Integer template_id);

    /**
     * 获取 [EMail模板]脏标记
     */
    boolean getTemplate_idDirtyFlag();

    /**
     * 模板
     */
    Integer getChart_template_id();

    void setChart_template_id(Integer chart_template_id);

    /**
     * 获取 [模板]脏标记
     */
    boolean getChart_template_idDirtyFlag();

    /**
     * 押金产品
     */
    Integer getDeposit_default_product_id();

    void setDeposit_default_product_id(Integer deposit_default_product_id);

    /**
     * 获取 [押金产品]脏标记
     */
    boolean getDeposit_default_product_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 用作通过注册创建的新用户的模版
     */
    Integer getAuth_signup_template_user_id();

    void setAuth_signup_template_user_id(Integer auth_signup_template_user_id);

    /**
     * 获取 [用作通过注册创建的新用户的模版]脏标记
     */
    boolean getAuth_signup_template_user_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
