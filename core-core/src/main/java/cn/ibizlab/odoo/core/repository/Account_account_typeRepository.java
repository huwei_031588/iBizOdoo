package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_account_type;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_typeSearchContext;

/**
 * 实体 [科目类型] 存储对象
 */
public interface Account_account_typeRepository extends Repository<Account_account_type> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_account_type> searchDefault(Account_account_typeSearchContext context);

    Account_account_type convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type domain , Account_account_type po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type convert2Domain( Account_account_type po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type domain) ;

}
