package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_journalSearchContext;

/**
 * 实体 [日记账] 存储模型
 */
public interface Account_journal{

    /**
     * 信用票：下一号码
     */
    Integer getRefund_sequence_number_next();

    void setRefund_sequence_number_next(Integer refund_sequence_number_next);

    /**
     * 获取 [信用票：下一号码]脏标记
     */
    boolean getRefund_sequence_number_nextDirtyFlag();

    /**
     * 允许的科目类型
     */
    String getType_control_ids();

    void setType_control_ids(String type_control_ids);

    /**
     * 获取 [允许的科目类型]脏标记
     */
    boolean getType_control_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 至少一个转出
     */
    String getAt_least_one_outbound();

    void setAt_least_one_outbound(String at_least_one_outbound);

    /**
     * 获取 [至少一个转出]脏标记
     */
    boolean getAt_least_one_outboundDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 分录序列
     */
    Integer getSequence_id();

    void setSequence_id(Integer sequence_id);

    /**
     * 获取 [分录序列]脏标记
     */
    boolean getSequence_idDirtyFlag();

    /**
     * 在仪表板显示日记账
     */
    String getShow_on_dashboard();

    void setShow_on_dashboard(String show_on_dashboard);

    /**
     * 获取 [在仪表板显示日记账]脏标记
     */
    boolean getShow_on_dashboardDirtyFlag();

    /**
     * 在销售点中使用
     */
    String getJournal_user();

    void setJournal_user(String journal_user);

    /**
     * 获取 [在销售点中使用]脏标记
     */
    boolean getJournal_userDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 银行费用
     */
    String getBank_statements_source();

    void setBank_statements_source(String bank_statements_source);

    /**
     * 获取 [银行费用]脏标记
     */
    boolean getBank_statements_sourceDirtyFlag();

    /**
     * 至少一个转入
     */
    String getAt_least_one_inbound();

    void setAt_least_one_inbound(String at_least_one_inbound);

    /**
     * 获取 [至少一个转入]脏标记
     */
    boolean getAt_least_one_inboundDirtyFlag();

    /**
     * 银行核销时过账
     */
    String getPost_at_bank_rec();

    void setPost_at_bank_rec(String post_at_bank_rec);

    /**
     * 获取 [银行核销时过账]脏标记
     */
    boolean getPost_at_bank_recDirtyFlag();

    /**
     * 日记账名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [日记账名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 下一号码
     */
    Integer getSequence_number_next();

    void setSequence_number_next(Integer sequence_number_next);

    /**
     * 获取 [下一号码]脏标记
     */
    boolean getSequence_number_nextDirtyFlag();

    /**
     * 属于用户的当前公司
     */
    String getBelongs_to_company();

    void setBelongs_to_company(String belongs_to_company);

    /**
     * 获取 [属于用户的当前公司]脏标记
     */
    boolean getBelongs_to_companyDirtyFlag();

    /**
     * 简码
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [简码]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 为收款
     */
    String getInbound_payment_method_ids();

    void setInbound_payment_method_ids(String inbound_payment_method_ids);

    /**
     * 获取 [为收款]脏标记
     */
    boolean getInbound_payment_method_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 分组发票明细行
     */
    String getGroup_invoice_lines();

    void setGroup_invoice_lines(String group_invoice_lines);

    /**
     * 获取 [分组发票明细行]脏标记
     */
    boolean getGroup_invoice_linesDirtyFlag();

    /**
     * 信用票分录序列
     */
    Integer getRefund_sequence_id();

    void setRefund_sequence_id(Integer refund_sequence_id);

    /**
     * 获取 [信用票分录序列]脏标记
     */
    boolean getRefund_sequence_idDirtyFlag();

    /**
     * 别名域
     */
    String getAlias_domain();

    void setAlias_domain(String alias_domain);

    /**
     * 获取 [别名域]脏标记
     */
    boolean getAlias_domainDirtyFlag();

    /**
     * 允许的科目
     */
    String getAccount_control_ids();

    void setAccount_control_ids(String account_control_ids);

    /**
     * 获取 [允许的科目]脏标记
     */
    boolean getAccount_control_idsDirtyFlag();

    /**
     * 允许取消分录
     */
    String getUpdate_posted();

    void setUpdate_posted(String update_posted);

    /**
     * 获取 [允许取消分录]脏标记
     */
    boolean getUpdate_postedDirtyFlag();

    /**
     * 授权差异的总金额
     */
    Double getAmount_authorized_diff();

    void setAmount_authorized_diff(Double amount_authorized_diff);

    /**
     * 获取 [授权差异的总金额]脏标记
     */
    boolean getAmount_authorized_diffDirtyFlag();

    /**
     * 看板仪表板图表
     */
    String getKanban_dashboard_graph();

    void setKanban_dashboard_graph(String kanban_dashboard_graph);

    /**
     * 获取 [看板仪表板图表]脏标记
     */
    boolean getKanban_dashboard_graphDirtyFlag();

    /**
     * 为付款
     */
    String getOutbound_payment_method_ids();

    void setOutbound_payment_method_ids(String outbound_payment_method_ids);

    /**
     * 获取 [为付款]脏标记
     */
    boolean getOutbound_payment_method_idsDirtyFlag();

    /**
     * 类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 看板仪表板
     */
    String getKanban_dashboard();

    void setKanban_dashboard(String kanban_dashboard);

    /**
     * 获取 [看板仪表板]脏标记
     */
    boolean getKanban_dashboardDirtyFlag();

    /**
     * 专用的信用票序列
     */
    String getRefund_sequence();

    void setRefund_sequence(String refund_sequence);

    /**
     * 获取 [专用的信用票序列]脏标记
     */
    boolean getRefund_sequenceDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 利润科目
     */
    String getProfit_account_id_text();

    void setProfit_account_id_text(String profit_account_id_text);

    /**
     * 获取 [利润科目]脏标记
     */
    boolean getProfit_account_id_textDirtyFlag();

    /**
     * 供应商账单的别名
     */
    String getAlias_name();

    void setAlias_name(String alias_name);

    /**
     * 获取 [供应商账单的别名]脏标记
     */
    boolean getAlias_nameDirtyFlag();

    /**
     * 账户号码
     */
    String getBank_acc_number();

    void setBank_acc_number(String bank_acc_number);

    /**
     * 获取 [账户号码]脏标记
     */
    boolean getBank_acc_numberDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 银行
     */
    Integer getBank_id();

    void setBank_id(Integer bank_id);

    /**
     * 获取 [银行]脏标记
     */
    boolean getBank_idDirtyFlag();

    /**
     * 损失科目
     */
    String getLoss_account_id_text();

    void setLoss_account_id_text(String loss_account_id_text);

    /**
     * 获取 [损失科目]脏标记
     */
    boolean getLoss_account_id_textDirtyFlag();

    /**
     * 默认贷方科目
     */
    String getDefault_credit_account_id_text();

    void setDefault_credit_account_id_text(String default_credit_account_id_text);

    /**
     * 获取 [默认贷方科目]脏标记
     */
    boolean getDefault_credit_account_id_textDirtyFlag();

    /**
     * 账户持有人
     */
    Integer getCompany_partner_id();

    void setCompany_partner_id(Integer company_partner_id);

    /**
     * 获取 [账户持有人]脏标记
     */
    boolean getCompany_partner_idDirtyFlag();

    /**
     * 默认借方科目
     */
    String getDefault_debit_account_id_text();

    void setDefault_debit_account_id_text(String default_debit_account_id_text);

    /**
     * 获取 [默认借方科目]脏标记
     */
    boolean getDefault_debit_account_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 银行账户
     */
    Integer getBank_account_id();

    void setBank_account_id(Integer bank_account_id);

    /**
     * 获取 [银行账户]脏标记
     */
    boolean getBank_account_idDirtyFlag();

    /**
     * 损失科目
     */
    Integer getLoss_account_id();

    void setLoss_account_id(Integer loss_account_id);

    /**
     * 获取 [损失科目]脏标记
     */
    boolean getLoss_account_idDirtyFlag();

    /**
     * 别名
     */
    Integer getAlias_id();

    void setAlias_id(Integer alias_id);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 默认贷方科目
     */
    Integer getDefault_credit_account_id();

    void setDefault_credit_account_id(Integer default_credit_account_id);

    /**
     * 获取 [默认贷方科目]脏标记
     */
    boolean getDefault_credit_account_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 默认借方科目
     */
    Integer getDefault_debit_account_id();

    void setDefault_debit_account_id(Integer default_debit_account_id);

    /**
     * 获取 [默认借方科目]脏标记
     */
    boolean getDefault_debit_account_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 利润科目
     */
    Integer getProfit_account_id();

    void setProfit_account_id(Integer profit_account_id);

    /**
     * 获取 [利润科目]脏标记
     */
    boolean getProfit_account_idDirtyFlag();

}
