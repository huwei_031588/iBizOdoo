package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Resource_calendar_leaves;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;

/**
 * 实体 [休假详情] 存储对象
 */
public interface Resource_calendar_leavesRepository extends Repository<Resource_calendar_leaves> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Resource_calendar_leaves> searchDefault(Resource_calendar_leavesSearchContext context);

    Resource_calendar_leaves convert2PO(cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_leaves domain , Resource_calendar_leaves po) ;

    cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_leaves convert2Domain( Resource_calendar_leaves po ,cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_leaves domain) ;

}
