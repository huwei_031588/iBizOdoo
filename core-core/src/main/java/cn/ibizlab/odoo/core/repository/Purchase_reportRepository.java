package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Purchase_report;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_reportSearchContext;

/**
 * 实体 [采购报表] 存储对象
 */
public interface Purchase_reportRepository extends Repository<Purchase_report> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Purchase_report> searchDefault(Purchase_reportSearchContext context);

    Purchase_report convert2PO(cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report domain , Purchase_report po) ;

    cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report convert2Domain( Purchase_report po ,cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report domain) ;

}
