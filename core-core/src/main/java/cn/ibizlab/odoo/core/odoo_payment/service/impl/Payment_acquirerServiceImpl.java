package cn.ibizlab.odoo.core.odoo_payment.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirerSearchContext;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_acquirerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_payment.client.payment_acquirerOdooClient;
import cn.ibizlab.odoo.core.odoo_payment.clientmodel.payment_acquirerClientModel;

/**
 * 实体[付款收单方] 服务对象接口实现
 */
@Slf4j
@Service
public class Payment_acquirerServiceImpl implements IPayment_acquirerService {

    @Autowired
    payment_acquirerOdooClient payment_acquirerOdooClient;


    @Override
    public boolean remove(Integer id) {
        payment_acquirerClientModel clientModel = new payment_acquirerClientModel();
        clientModel.setId(id);
		payment_acquirerOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Payment_acquirer et) {
        payment_acquirerClientModel clientModel = convert2Model(et,null);
		payment_acquirerOdooClient.update(clientModel);
        Payment_acquirer rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Payment_acquirer> list){
    }

    @Override
    public Payment_acquirer get(Integer id) {
        payment_acquirerClientModel clientModel = new payment_acquirerClientModel();
        clientModel.setId(id);
		payment_acquirerOdooClient.get(clientModel);
        Payment_acquirer et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Payment_acquirer();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Payment_acquirer et) {
        payment_acquirerClientModel clientModel = convert2Model(et,null);
		payment_acquirerOdooClient.create(clientModel);
        Payment_acquirer rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Payment_acquirer> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Payment_acquirer> searchDefault(Payment_acquirerSearchContext context) {
        List<Payment_acquirer> list = new ArrayList<Payment_acquirer>();
        Page<payment_acquirerClientModel> clientModelList = payment_acquirerOdooClient.search(context);
        for(payment_acquirerClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Payment_acquirer>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public payment_acquirerClientModel convert2Model(Payment_acquirer domain , payment_acquirerClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new payment_acquirerClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("fees_implementeddirtyflag"))
                model.setFees_implemented(domain.getFeesImplemented());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("done_msgdirtyflag"))
                model.setDone_msg(domain.getDoneMsg());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("payment_icon_idsdirtyflag"))
                model.setPayment_icon_ids(domain.getPaymentIconIds());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("fees_int_vardirtyflag"))
                model.setFees_int_var(domain.getFeesIntVar());
            if((Boolean) domain.getExtensionparams().get("fees_activedirtyflag"))
                model.setFees_active(domain.getFeesActive());
            if((Boolean) domain.getExtensionparams().get("registration_view_template_iddirtyflag"))
                model.setRegistration_view_template_id(domain.getRegistrationViewTemplateId());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("post_msgdirtyflag"))
                model.setPost_msg(domain.getPostMsg());
            if((Boolean) domain.getExtensionparams().get("environmentdirtyflag"))
                model.setEnvironment(domain.getEnvironment());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("token_implementeddirtyflag"))
                model.setToken_implemented(domain.getTokenImplemented());
            if((Boolean) domain.getExtensionparams().get("fees_dom_fixeddirtyflag"))
                model.setFees_dom_fixed(domain.getFeesDomFixed());
            if((Boolean) domain.getExtensionparams().get("fees_int_fixeddirtyflag"))
                model.setFees_int_fixed(domain.getFeesIntFixed());
            if((Boolean) domain.getExtensionparams().get("module_iddirtyflag"))
                model.setModule_id(domain.getModuleId());
            if((Boolean) domain.getExtensionparams().get("authorize_implementeddirtyflag"))
                model.setAuthorize_implemented(domain.getAuthorizeImplemented());
            if((Boolean) domain.getExtensionparams().get("payment_flowdirtyflag"))
                model.setPayment_flow(domain.getPaymentFlow());
            if((Boolean) domain.getExtensionparams().get("specific_countriesdirtyflag"))
                model.setSpecific_countries(domain.getSpecificCountries());
            if((Boolean) domain.getExtensionparams().get("fees_dom_vardirtyflag"))
                model.setFees_dom_var(domain.getFeesDomVar());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("pre_msgdirtyflag"))
                model.setPre_msg(domain.getPreMsg());
            if((Boolean) domain.getExtensionparams().get("country_idsdirtyflag"))
                model.setCountry_ids(domain.getCountryIds());
            if((Boolean) domain.getExtensionparams().get("qr_codedirtyflag"))
                model.setQr_code(domain.getQrCode());
            if((Boolean) domain.getExtensionparams().get("pending_msgdirtyflag"))
                model.setPending_msg(domain.getPendingMsg());
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("view_template_iddirtyflag"))
                model.setView_template_id(domain.getViewTemplateId());
            if((Boolean) domain.getExtensionparams().get("inbound_payment_method_idsdirtyflag"))
                model.setInbound_payment_method_ids(domain.getInboundPaymentMethodIds());
            if((Boolean) domain.getExtensionparams().get("so_reference_typedirtyflag"))
                model.setSo_reference_type(domain.getSoReferenceType());
            if((Boolean) domain.getExtensionparams().get("module_statedirtyflag"))
                model.setModule_state(domain.getModuleState());
            if((Boolean) domain.getExtensionparams().get("cancel_msgdirtyflag"))
                model.setCancel_msg(domain.getCancelMsg());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("providerdirtyflag"))
                model.setProvider(domain.getProvider());
            if((Boolean) domain.getExtensionparams().get("save_tokendirtyflag"))
                model.setSave_token(domain.getSaveToken());
            if((Boolean) domain.getExtensionparams().get("capture_manuallydirtyflag"))
                model.setCapture_manually(domain.getCaptureManually());
            if((Boolean) domain.getExtensionparams().get("error_msgdirtyflag"))
                model.setError_msg(domain.getErrorMsg());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Payment_acquirer convert2Domain( payment_acquirerClientModel model ,Payment_acquirer domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Payment_acquirer();
        }

        if(model.getFees_implementedDirtyFlag())
            domain.setFeesImplemented(model.getFees_implemented());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getDone_msgDirtyFlag())
            domain.setDoneMsg(model.getDone_msg());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPayment_icon_idsDirtyFlag())
            domain.setPaymentIconIds(model.getPayment_icon_ids());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getFees_int_varDirtyFlag())
            domain.setFeesIntVar(model.getFees_int_var());
        if(model.getFees_activeDirtyFlag())
            domain.setFeesActive(model.getFees_active());
        if(model.getRegistration_view_template_idDirtyFlag())
            domain.setRegistrationViewTemplateId(model.getRegistration_view_template_id());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getPost_msgDirtyFlag())
            domain.setPostMsg(model.getPost_msg());
        if(model.getEnvironmentDirtyFlag())
            domain.setEnvironment(model.getEnvironment());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getToken_implementedDirtyFlag())
            domain.setTokenImplemented(model.getToken_implemented());
        if(model.getFees_dom_fixedDirtyFlag())
            domain.setFeesDomFixed(model.getFees_dom_fixed());
        if(model.getFees_int_fixedDirtyFlag())
            domain.setFeesIntFixed(model.getFees_int_fixed());
        if(model.getModule_idDirtyFlag())
            domain.setModuleId(model.getModule_id());
        if(model.getAuthorize_implementedDirtyFlag())
            domain.setAuthorizeImplemented(model.getAuthorize_implemented());
        if(model.getPayment_flowDirtyFlag())
            domain.setPaymentFlow(model.getPayment_flow());
        if(model.getSpecific_countriesDirtyFlag())
            domain.setSpecificCountries(model.getSpecific_countries());
        if(model.getFees_dom_varDirtyFlag())
            domain.setFeesDomVar(model.getFees_dom_var());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getPre_msgDirtyFlag())
            domain.setPreMsg(model.getPre_msg());
        if(model.getCountry_idsDirtyFlag())
            domain.setCountryIds(model.getCountry_ids());
        if(model.getQr_codeDirtyFlag())
            domain.setQrCode(model.getQr_code());
        if(model.getPending_msgDirtyFlag())
            domain.setPendingMsg(model.getPending_msg());
        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.getView_template_idDirtyFlag())
            domain.setViewTemplateId(model.getView_template_id());
        if(model.getInbound_payment_method_idsDirtyFlag())
            domain.setInboundPaymentMethodIds(model.getInbound_payment_method_ids());
        if(model.getSo_reference_typeDirtyFlag())
            domain.setSoReferenceType(model.getSo_reference_type());
        if(model.getModule_stateDirtyFlag())
            domain.setModuleState(model.getModule_state());
        if(model.getCancel_msgDirtyFlag())
            domain.setCancelMsg(model.getCancel_msg());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getProviderDirtyFlag())
            domain.setProvider(model.getProvider());
        if(model.getSave_tokenDirtyFlag())
            domain.setSaveToken(model.getSave_token());
        if(model.getCapture_manuallyDirtyFlag())
            domain.setCaptureManually(model.getCapture_manually());
        if(model.getError_msgDirtyFlag())
            domain.setErrorMsg(model.getError_msg());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



