package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_refund;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_refundSearchContext;


/**
 * 实体[Account_invoice_refund] 服务对象接口
 */
public interface IAccount_invoice_refundService{

    Account_invoice_refund get(Integer key) ;
    boolean create(Account_invoice_refund et) ;
    void createBatch(List<Account_invoice_refund> list) ;
    boolean update(Account_invoice_refund et) ;
    void updateBatch(List<Account_invoice_refund> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Account_invoice_refund> searchDefault(Account_invoice_refundSearchContext context) ;

}



