package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_quant;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_quant.Stock_quantReserved_quantityDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_quant
 * 属性：Reserved_quantity
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_quantReserved_quantityDefaultValidator.class})
public @interface Stock_quantReserved_quantityDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
