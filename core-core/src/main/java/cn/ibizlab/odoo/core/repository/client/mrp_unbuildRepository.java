package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_unbuild;

/**
 * 实体[mrp_unbuild] 服务对象接口
 */
public interface mrp_unbuildRepository{


    public mrp_unbuild createPO() ;
        public void createBatch(mrp_unbuild mrp_unbuild);

        public void updateBatch(mrp_unbuild mrp_unbuild);

        public void create(mrp_unbuild mrp_unbuild);

        public List<mrp_unbuild> search();

        public void removeBatch(String id);

        public void update(mrp_unbuild mrp_unbuild);

        public void remove(String id);

        public void get(String id);


}
