package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_cashbox_lineSearchContext;

/**
 * 实体 [钱箱明细] 存储模型
 */
public interface Account_cashbox_line{

    /**
     * 在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行
     */
    Integer getDefault_pos_id();

    void setDefault_pos_id(Integer default_pos_id);

    /**
     * 获取 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]脏标记
     */
    boolean getDefault_pos_idDirtyFlag();

    /**
     * 小计
     */
    Double getSubtotal();

    void setSubtotal(Double subtotal);

    /**
     * 获取 [小计]脏标记
     */
    boolean getSubtotalDirtyFlag();

    /**
     * 硬币／账单　价值
     */
    Double getCoin_value();

    void setCoin_value(Double coin_value);

    /**
     * 获取 [硬币／账单　价值]脏标记
     */
    boolean getCoin_valueDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 货币和账单编号
     */
    Integer getNumber();

    void setNumber(Integer number);

    /**
     * 获取 [货币和账单编号]脏标记
     */
    boolean getNumberDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 钱箱
     */
    Integer getCashbox_id();

    void setCashbox_id(Integer cashbox_id);

    /**
     * 获取 [钱箱]脏标记
     */
    boolean getCashbox_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
