package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_vehicle_log_services;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_servicesSearchContext;

/**
 * 实体 [车辆服务] 存储对象
 */
public interface Fleet_vehicle_log_servicesRepository extends Repository<Fleet_vehicle_log_services> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_vehicle_log_services> searchDefault(Fleet_vehicle_log_servicesSearchContext context);

    Fleet_vehicle_log_services convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_services domain , Fleet_vehicle_log_services po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_services convert2Domain( Fleet_vehicle_log_services po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_services domain) ;

}
