package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_unbuildSearchContext;

/**
 * 实体 [拆解数量短缺警告] 存储对象
 */
public interface Stock_warn_insufficient_qty_unbuildRepository extends Repository<Stock_warn_insufficient_qty_unbuild> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_warn_insufficient_qty_unbuild> searchDefault(Stock_warn_insufficient_qty_unbuildSearchContext context);

    Stock_warn_insufficient_qty_unbuild convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild domain , Stock_warn_insufficient_qty_unbuild po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild convert2Domain( Stock_warn_insufficient_qty_unbuild po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild domain) ;

}
