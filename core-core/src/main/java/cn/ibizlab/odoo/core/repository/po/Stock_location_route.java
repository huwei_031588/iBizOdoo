package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_location_routeSearchContext;

/**
 * 实体 [库存路线] 存储模型
 */
public interface Stock_location_route{

    /**
     * 在销售订单行上可选
     */
    String getSale_selectable();

    void setSale_selectable(String sale_selectable);

    /**
     * 获取 [在销售订单行上可选]脏标记
     */
    boolean getSale_selectableDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 产品
     */
    String getProduct_ids();

    void setProduct_ids(String product_ids);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 路线
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [路线]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 产品类别
     */
    String getCateg_ids();

    void setCateg_ids(String categ_ids);

    /**
     * 获取 [产品类别]脏标记
     */
    boolean getCateg_idsDirtyFlag();

    /**
     * 可应用于仓库
     */
    String getWarehouse_selectable();

    void setWarehouse_selectable(String warehouse_selectable);

    /**
     * 获取 [可应用于仓库]脏标记
     */
    boolean getWarehouse_selectableDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 规则
     */
    String getRule_ids();

    void setRule_ids(String rule_ids);

    /**
     * 获取 [规则]脏标记
     */
    boolean getRule_idsDirtyFlag();

    /**
     * 可应用于产品
     */
    String getProduct_selectable();

    void setProduct_selectable(String product_selectable);

    /**
     * 获取 [可应用于产品]脏标记
     */
    boolean getProduct_selectableDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 可应用于产品类别
     */
    String getProduct_categ_selectable();

    void setProduct_categ_selectable(String product_categ_selectable);

    /**
     * 获取 [可应用于产品类别]脏标记
     */
    boolean getProduct_categ_selectableDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 仓库
     */
    String getWarehouse_ids();

    void setWarehouse_ids(String warehouse_ids);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_idsDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 供应的仓库
     */
    String getSupplied_wh_id_text();

    void setSupplied_wh_id_text(String supplied_wh_id_text);

    /**
     * 获取 [供应的仓库]脏标记
     */
    boolean getSupplied_wh_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 供应仓库
     */
    String getSupplier_wh_id_text();

    void setSupplier_wh_id_text(String supplier_wh_id_text);

    /**
     * 获取 [供应仓库]脏标记
     */
    boolean getSupplier_wh_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 供应仓库
     */
    Integer getSupplier_wh_id();

    void setSupplier_wh_id(Integer supplier_wh_id);

    /**
     * 获取 [供应仓库]脏标记
     */
    boolean getSupplier_wh_idDirtyFlag();

    /**
     * 供应的仓库
     */
    Integer getSupplied_wh_id();

    void setSupplied_wh_id(Integer supplied_wh_id);

    /**
     * 获取 [供应的仓库]脏标记
     */
    boolean getSupplied_wh_idDirtyFlag();

}
