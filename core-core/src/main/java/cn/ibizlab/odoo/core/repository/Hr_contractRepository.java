package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_contract;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contractSearchContext;

/**
 * 实体 [Contract] 存储对象
 */
public interface Hr_contractRepository extends Repository<Hr_contract> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_contract> searchDefault(Hr_contractSearchContext context);

    Hr_contract convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract domain , Hr_contract po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract convert2Domain( Hr_contract po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract domain) ;

}
