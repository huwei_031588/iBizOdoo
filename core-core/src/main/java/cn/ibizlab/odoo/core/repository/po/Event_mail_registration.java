package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_event.filter.Event_mail_registrationSearchContext;

/**
 * 实体 [登记邮件调度] 存储模型
 */
public interface Event_mail_registration{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * EMail发送
     */
    String getMail_sent();

    void setMail_sent(String mail_sent);

    /**
     * 获取 [EMail发送]脏标记
     */
    boolean getMail_sentDirtyFlag();

    /**
     * 定期时间
     */
    Timestamp getScheduled_date();

    void setScheduled_date(Timestamp scheduled_date);

    /**
     * 获取 [定期时间]脏标记
     */
    boolean getScheduled_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 出席者
     */
    String getRegistration_id_text();

    void setRegistration_id_text(String registration_id_text);

    /**
     * 获取 [出席者]脏标记
     */
    boolean getRegistration_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 出席者
     */
    Integer getRegistration_id();

    void setRegistration_id(Integer registration_id);

    /**
     * 获取 [出席者]脏标记
     */
    boolean getRegistration_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 邮件调度
     */
    Integer getScheduler_id();

    void setScheduler_id(Integer scheduler_id);

    /**
     * 获取 [邮件调度]脏标记
     */
    boolean getScheduler_idDirtyFlag();

}
