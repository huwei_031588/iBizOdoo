package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Base_automationSearchContext;

/**
 * 实体 [自动动作] 存储模型
 */
public interface Base_automation{

    /**
     * 绑定模型
     */
    Integer getBinding_model_id();

    void setBinding_model_id(Integer binding_model_id);

    /**
     * 获取 [绑定模型]脏标记
     */
    boolean getBinding_model_idDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 更新前域表达式
     */
    String getFilter_pre_domain();

    void setFilter_pre_domain(String filter_pre_domain);

    /**
     * 获取 [更新前域表达式]脏标记
     */
    boolean getFilter_pre_domainDirtyFlag();

    /**
     * 变化字段的触发器
     */
    String getOn_change_fields();

    void setOn_change_fields(String on_change_fields);

    /**
     * 获取 [变化字段的触发器]脏标记
     */
    boolean getOn_change_fieldsDirtyFlag();

    /**
     * 摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 用户字段名字
     */
    String getActivity_user_field_name();

    void setActivity_user_field_name(String activity_user_field_name);

    /**
     * 获取 [用户字段名字]脏标记
     */
    boolean getActivity_user_field_nameDirtyFlag();

    /**
     * 可用于网站
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [可用于网站]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 用途
     */
    String getUsage();

    void setUsage(String usage);

    /**
     * 获取 [用途]脏标记
     */
    boolean getUsageDirtyFlag();

    /**
     * 目标模型
     */
    String getCrud_model_name();

    void setCrud_model_name(String crud_model_name);

    /**
     * 获取 [目标模型]脏标记
     */
    boolean getCrud_model_nameDirtyFlag();

    /**
     * 延迟类型
     */
    String getTrg_date_range_type();

    void setTrg_date_range_type(String trg_date_range_type);

    /**
     * 获取 [延迟类型]脏标记
     */
    boolean getTrg_date_range_typeDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 映射的值
     */
    String getFields_lines();

    void setFields_lines(String fields_lines);

    /**
     * 获取 [映射的值]脏标记
     */
    boolean getFields_linesDirtyFlag();

    /**
     * 备注
     */
    String getActivity_note();

    void setActivity_note(String activity_note);

    /**
     * 获取 [备注]脏标记
     */
    boolean getActivity_noteDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 活动
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * Python 代码
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [Python 代码]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 活动用户类型
     */
    String getActivity_user_type();

    void setActivity_user_type(String activity_user_type);

    /**
     * 获取 [活动用户类型]脏标记
     */
    boolean getActivity_user_typeDirtyFlag();

    /**
     * 网站路径
     */
    String getWebsite_path();

    void setWebsite_path(String website_path);

    /**
     * 获取 [网站路径]脏标记
     */
    boolean getWebsite_pathDirtyFlag();

    /**
     * 添加关注者
     */
    String getPartner_ids();

    void setPartner_ids(String partner_ids);

    /**
     * 获取 [添加关注者]脏标记
     */
    boolean getPartner_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 触发日期
     */
    Integer getTrg_date_id();

    void setTrg_date_id(Integer trg_date_id);

    /**
     * 获取 [触发日期]脏标记
     */
    boolean getTrg_date_idDirtyFlag();

    /**
     * 外部 ID
     */
    String getXml_id();

    void setXml_id(String xml_id);

    /**
     * 获取 [外部 ID]脏标记
     */
    boolean getXml_idDirtyFlag();

    /**
     * 最后运行
     */
    Timestamp getLast_run();

    void setLast_run(Timestamp last_run);

    /**
     * 获取 [最后运行]脏标记
     */
    boolean getLast_runDirtyFlag();

    /**
     * EMail模板
     */
    Integer getTemplate_id();

    void setTemplate_id(Integer template_id);

    /**
     * 获取 [EMail模板]脏标记
     */
    boolean getTemplate_idDirtyFlag();

    /**
     * 绑定类型
     */
    String getBinding_type();

    void setBinding_type(String binding_type);

    /**
     * 获取 [绑定类型]脏标记
     */
    boolean getBinding_typeDirtyFlag();

    /**
     * 动作名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [动作名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 模型名称
     */
    String getModel_name();

    void setModel_name(String model_name);

    /**
     * 获取 [模型名称]脏标记
     */
    boolean getModel_nameDirtyFlag();

    /**
     * 待办的行动
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [待办的行动]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 添加频道
     */
    String getChannel_ids();

    void setChannel_ids(String channel_ids);

    /**
     * 获取 [添加频道]脏标记
     */
    boolean getChannel_idsDirtyFlag();

    /**
     * 模型
     */
    Integer getModel_id();

    void setModel_id(Integer model_id);

    /**
     * 获取 [模型]脏标记
     */
    boolean getModel_idDirtyFlag();

    /**
     * 动作说明
     */
    String getHelp();

    void setHelp(String help);

    /**
     * 获取 [动作说明]脏标记
     */
    boolean getHelpDirtyFlag();

    /**
     * 网站网址
     */
    String getWebsite_url();

    void setWebsite_url(String website_url);

    /**
     * 获取 [网站网址]脏标记
     */
    boolean getWebsite_urlDirtyFlag();

    /**
     * 负责人
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 截止日期至
     */
    Integer getActivity_date_deadline_range();

    void setActivity_date_deadline_range(Integer activity_date_deadline_range);

    /**
     * 获取 [截止日期至]脏标记
     */
    boolean getActivity_date_deadline_rangeDirtyFlag();

    /**
     * 触发日期后的延迟
     */
    Integer getTrg_date_range();

    void setTrg_date_range(Integer trg_date_range);

    /**
     * 获取 [触发日期后的延迟]脏标记
     */
    boolean getTrg_date_rangeDirtyFlag();

    /**
     * 动作类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [动作类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 应用于
     */
    String getFilter_domain();

    void setFilter_domain(String filter_domain);

    /**
     * 获取 [应用于]脏标记
     */
    boolean getFilter_domainDirtyFlag();

    /**
     * 到期类型
     */
    String getActivity_date_deadline_range_type();

    void setActivity_date_deadline_range_type(String activity_date_deadline_range_type);

    /**
     * 获取 [到期类型]脏标记
     */
    boolean getActivity_date_deadline_range_typeDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 下级动作
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [下级动作]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 链接使用字段
     */
    Integer getLink_field_id();

    void setLink_field_id(Integer link_field_id);

    /**
     * 获取 [链接使用字段]脏标记
     */
    boolean getLink_field_idDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 触发条件
     */
    String getTrigger();

    void setTrigger(String trigger);

    /**
     * 获取 [触发条件]脏标记
     */
    boolean getTriggerDirtyFlag();

    /**
     * 服务器动作
     */
    Integer getAction_server_id();

    void setAction_server_id(Integer action_server_id);

    /**
     * 获取 [服务器动作]脏标记
     */
    boolean getAction_server_idDirtyFlag();

    /**
     * 创建/写目标模型
     */
    Integer getCrud_model_id();

    void setCrud_model_id(Integer crud_model_id);

    /**
     * 获取 [创建/写目标模型]脏标记
     */
    boolean getCrud_model_idDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 使用日历
     */
    String getTrg_date_calendar_id_text();

    void setTrg_date_calendar_id_text(String trg_date_calendar_id_text);

    /**
     * 获取 [使用日历]脏标记
     */
    boolean getTrg_date_calendar_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 使用日历
     */
    Integer getTrg_date_calendar_id();

    void setTrg_date_calendar_id(Integer trg_date_calendar_id);

    /**
     * 获取 [使用日历]脏标记
     */
    boolean getTrg_date_calendar_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
