package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_productionSearchContext;

/**
 * 实体 [Production Order] 存储模型
 */
public interface Mrp_production{

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * # 完工工单
     */
    Integer getWorkorder_done_count();

    void setWorkorder_done_count(Integer workorder_done_count);

    /**
     * 获取 [# 完工工单]脏标记
     */
    boolean getWorkorder_done_countDirtyFlag();

    /**
     * 完工产品
     */
    String getFinished_move_line_ids();

    void setFinished_move_line_ids(String finished_move_line_ids);

    /**
     * 获取 [完工产品]脏标记
     */
    boolean getFinished_move_line_idsDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 已生产数量
     */
    Double getQty_produced();

    void setQty_produced(Double qty_produced);

    /**
     * 获取 [已生产数量]脏标记
     */
    boolean getQty_producedDirtyFlag();

    /**
     * 优先级
     */
    String getPriority();

    void setPriority(String priority);

    /**
     * 获取 [优先级]脏标记
     */
    boolean getPriorityDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 允许取消预留库存
     */
    String getUnreserve_visible();

    void setUnreserve_visible(String unreserve_visible);

    /**
     * 获取 [允许取消预留库存]脏标记
     */
    boolean getUnreserve_visibleDirtyFlag();

    /**
     * 有移动
     */
    String getHas_moves();

    void setHas_moves(String has_moves);

    /**
     * 获取 [有移动]脏标记
     */
    boolean getHas_movesDirtyFlag();

    /**
     * 是锁定
     */
    String getIs_locked();

    void setIs_locked(String is_locked);

    /**
     * 获取 [是锁定]脏标记
     */
    boolean getIs_lockedDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_start();

    void setDate_start(Timestamp date_start);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_startDirtyFlag();

    /**
     * 产成品
     */
    String getMove_finished_ids();

    void setMove_finished_ids(String move_finished_ids);

    /**
     * 获取 [产成品]脏标记
     */
    boolean getMove_finished_idsDirtyFlag();

    /**
     * 报废转移
     */
    Integer getScrap_count();

    void setScrap_count(Integer scrap_count);

    /**
     * 获取 [报废转移]脏标记
     */
    boolean getScrap_countDirtyFlag();

    /**
     * 报废
     */
    String getScrap_ids();

    void setScrap_ids(String scrap_ids);

    /**
     * 获取 [报废]脏标记
     */
    boolean getScrap_idsDirtyFlag();

    /**
     * 显示最后一批
     */
    String getShow_final_lots();

    void setShow_final_lots(String show_final_lots);

    /**
     * 获取 [显示最后一批]脏标记
     */
    boolean getShow_final_lotsDirtyFlag();

    /**
     * 参考
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [参考]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 生产货物的库存移动
     */
    String getMove_dest_ids();

    void setMove_dest_ids(String move_dest_ids);

    /**
     * 获取 [生产货物的库存移动]脏标记
     */
    boolean getMove_dest_idsDirtyFlag();

    /**
     * 补货组
     */
    Integer getProcurement_group_id();

    void setProcurement_group_id(Integer procurement_group_id);

    /**
     * 获取 [补货组]脏标记
     */
    boolean getProcurement_group_idDirtyFlag();

    /**
     * 出库单
     */
    Integer getDelivery_count();

    void setDelivery_count(Integer delivery_count);

    /**
     * 获取 [出库单]脏标记
     */
    boolean getDelivery_countDirtyFlag();

    /**
     * 待生产数量
     */
    Double getProduct_qty();

    void setProduct_qty(Double product_qty);

    /**
     * 获取 [待生产数量]脏标记
     */
    boolean getProduct_qtyDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * # 工单
     */
    Integer getWorkorder_count();

    void setWorkorder_count(Integer workorder_count);

    /**
     * 获取 [# 工单]脏标记
     */
    boolean getWorkorder_countDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 截止日期结束
     */
    Timestamp getDate_planned_finished();

    void setDate_planned_finished(Timestamp date_planned_finished);

    /**
     * 获取 [截止日期结束]脏标记
     */
    boolean getDate_planned_finishedDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 来源
     */
    String getOrigin();

    void setOrigin(String origin);

    /**
     * 获取 [来源]脏标记
     */
    boolean getOriginDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 截止日期开始
     */
    Timestamp getDate_planned_start();

    void setDate_planned_start(Timestamp date_planned_start);

    /**
     * 获取 [截止日期开始]脏标记
     */
    boolean getDate_planned_startDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 检查生产的数量
     */
    String getCheck_to_done();

    void setCheck_to_done(String check_to_done);

    /**
     * 获取 [检查生产的数量]脏标记
     */
    boolean getCheck_to_doneDirtyFlag();

    /**
     * 允许发布库存
     */
    String getPost_visible();

    void setPost_visible(String post_visible);

    /**
     * 获取 [允许发布库存]脏标记
     */
    boolean getPost_visibleDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 与此制造订单相关的拣货
     */
    String getPicking_ids();

    void setPicking_ids(String picking_ids);

    /**
     * 获取 [与此制造订单相关的拣货]脏标记
     */
    boolean getPicking_idsDirtyFlag();

    /**
     * 数量总计
     */
    Double getProduct_uom_qty();

    void setProduct_uom_qty(Double product_uom_qty);

    /**
     * 获取 [数量总计]脏标记
     */
    boolean getProduct_uom_qtyDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 消费量少于计划数量
     */
    String getConsumed_less_than_planned();

    void setConsumed_less_than_planned(String consumed_less_than_planned);

    /**
     * 获取 [消费量少于计划数量]脏标记
     */
    boolean getConsumed_less_than_plannedDirtyFlag();

    /**
     * 原材料
     */
    String getMove_raw_ids();

    void setMove_raw_ids(String move_raw_ids);

    /**
     * 获取 [原材料]脏标记
     */
    boolean getMove_raw_idsDirtyFlag();

    /**
     * 材料可用性
     */
    String getAvailability();

    void setAvailability(String availability);

    /**
     * 获取 [材料可用性]脏标记
     */
    boolean getAvailabilityDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 工单
     */
    String getWorkorder_ids();

    void setWorkorder_ids(String workorder_ids);

    /**
     * 获取 [工单]脏标记
     */
    boolean getWorkorder_idsDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getDate_finished();

    void setDate_finished(Timestamp date_finished);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getDate_finishedDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 传播取消以及拆分
     */
    String getPropagate();

    void setPropagate(String propagate);

    /**
     * 获取 [传播取消以及拆分]脏标记
     */
    boolean getPropagateDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 成品位置
     */
    String getLocation_dest_id_text();

    void setLocation_dest_id_text(String location_dest_id_text);

    /**
     * 获取 [成品位置]脏标记
     */
    boolean getLocation_dest_id_textDirtyFlag();

    /**
     * 原料位置
     */
    String getLocation_src_id_text();

    void setLocation_src_id_text(String location_src_id_text);

    /**
     * 获取 [原料位置]脏标记
     */
    boolean getLocation_src_id_textDirtyFlag();

    /**
     * 生产位置
     */
    Integer getProduction_location_id();

    void setProduction_location_id(Integer production_location_id);

    /**
     * 获取 [生产位置]脏标记
     */
    boolean getProduction_location_idDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 作业类型
     */
    String getPicking_type_id_text();

    void setPicking_type_id_text(String picking_type_id_text);

    /**
     * 获取 [作业类型]脏标记
     */
    boolean getPicking_type_id_textDirtyFlag();

    /**
     * 负责人
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 产品模板
     */
    Integer getProduct_tmpl_id();

    void setProduct_tmpl_id(Integer product_tmpl_id);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_idDirtyFlag();

    /**
     * 计量单位
     */
    String getProduct_uom_id_text();

    void setProduct_uom_id_text(String product_uom_id_text);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 工艺
     */
    String getRouting_id_text();

    void setRouting_id_text(String routing_id_text);

    /**
     * 获取 [工艺]脏标记
     */
    boolean getRouting_id_textDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 工艺
     */
    Integer getRouting_id();

    void setRouting_id(Integer routing_id);

    /**
     * 获取 [工艺]脏标记
     */
    boolean getRouting_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 负责人
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 物料清单
     */
    Integer getBom_id();

    void setBom_id(Integer bom_id);

    /**
     * 获取 [物料清单]脏标记
     */
    boolean getBom_idDirtyFlag();

    /**
     * 原料位置
     */
    Integer getLocation_src_id();

    void setLocation_src_id(Integer location_src_id);

    /**
     * 获取 [原料位置]脏标记
     */
    boolean getLocation_src_idDirtyFlag();

    /**
     * 作业类型
     */
    Integer getPicking_type_id();

    void setPicking_type_id(Integer picking_type_id);

    /**
     * 获取 [作业类型]脏标记
     */
    boolean getPicking_type_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 成品位置
     */
    Integer getLocation_dest_id();

    void setLocation_dest_id(Integer location_dest_id);

    /**
     * 获取 [成品位置]脏标记
     */
    boolean getLocation_dest_idDirtyFlag();

}
