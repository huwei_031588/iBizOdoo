package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivityService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_workcenter_productivityOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_workcenter_productivityClientModel;

/**
 * 实体[工作中心生产力日志] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_workcenter_productivityServiceImpl implements IMrp_workcenter_productivityService {

    @Autowired
    mrp_workcenter_productivityOdooClient mrp_workcenter_productivityOdooClient;


    @Override
    public boolean create(Mrp_workcenter_productivity et) {
        mrp_workcenter_productivityClientModel clientModel = convert2Model(et,null);
		mrp_workcenter_productivityOdooClient.create(clientModel);
        Mrp_workcenter_productivity rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_workcenter_productivity> list){
    }

    @Override
    public boolean update(Mrp_workcenter_productivity et) {
        mrp_workcenter_productivityClientModel clientModel = convert2Model(et,null);
		mrp_workcenter_productivityOdooClient.update(clientModel);
        Mrp_workcenter_productivity rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_workcenter_productivity> list){
    }

    @Override
    public Mrp_workcenter_productivity get(Integer id) {
        mrp_workcenter_productivityClientModel clientModel = new mrp_workcenter_productivityClientModel();
        clientModel.setId(id);
		mrp_workcenter_productivityOdooClient.get(clientModel);
        Mrp_workcenter_productivity et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_workcenter_productivity();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mrp_workcenter_productivityClientModel clientModel = new mrp_workcenter_productivityClientModel();
        clientModel.setId(id);
		mrp_workcenter_productivityOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_workcenter_productivity> searchDefault(Mrp_workcenter_productivitySearchContext context) {
        List<Mrp_workcenter_productivity> list = new ArrayList<Mrp_workcenter_productivity>();
        Page<mrp_workcenter_productivityClientModel> clientModelList = mrp_workcenter_productivityOdooClient.search(context);
        for(mrp_workcenter_productivityClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_workcenter_productivity>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_workcenter_productivityClientModel convert2Model(Mrp_workcenter_productivity domain , mrp_workcenter_productivityClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_workcenter_productivityClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("durationdirtyflag"))
                model.setDuration(domain.getDuration());
            if((Boolean) domain.getExtensionparams().get("date_enddirtyflag"))
                model.setDate_end(domain.getDateEnd());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("date_startdirtyflag"))
                model.setDate_start(domain.getDateStart());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("workcenter_id_textdirtyflag"))
                model.setWorkcenter_id_text(domain.getWorkcenterIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("loss_typedirtyflag"))
                model.setLoss_type(domain.getLossType());
            if((Boolean) domain.getExtensionparams().get("workorder_id_textdirtyflag"))
                model.setWorkorder_id_text(domain.getWorkorderIdText());
            if((Boolean) domain.getExtensionparams().get("loss_id_textdirtyflag"))
                model.setLoss_id_text(domain.getLossIdText());
            if((Boolean) domain.getExtensionparams().get("production_iddirtyflag"))
                model.setProduction_id(domain.getProductionId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("workorder_iddirtyflag"))
                model.setWorkorder_id(domain.getWorkorderId());
            if((Boolean) domain.getExtensionparams().get("workcenter_iddirtyflag"))
                model.setWorkcenter_id(domain.getWorkcenterId());
            if((Boolean) domain.getExtensionparams().get("loss_iddirtyflag"))
                model.setLoss_id(domain.getLossId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_workcenter_productivity convert2Domain( mrp_workcenter_productivityClientModel model ,Mrp_workcenter_productivity domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_workcenter_productivity();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getDurationDirtyFlag())
            domain.setDuration(model.getDuration());
        if(model.getDate_endDirtyFlag())
            domain.setDateEnd(model.getDate_end());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDate_startDirtyFlag())
            domain.setDateStart(model.getDate_start());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWorkcenter_id_textDirtyFlag())
            domain.setWorkcenterIdText(model.getWorkcenter_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getLoss_typeDirtyFlag())
            domain.setLossType(model.getLoss_type());
        if(model.getWorkorder_id_textDirtyFlag())
            domain.setWorkorderIdText(model.getWorkorder_id_text());
        if(model.getLoss_id_textDirtyFlag())
            domain.setLossIdText(model.getLoss_id_text());
        if(model.getProduction_idDirtyFlag())
            domain.setProductionId(model.getProduction_id());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getWorkorder_idDirtyFlag())
            domain.setWorkorderId(model.getWorkorder_id());
        if(model.getWorkcenter_idDirtyFlag())
            domain.setWorkcenterId(model.getWorkcenter_id());
        if(model.getLoss_idDirtyFlag())
            domain.setLossId(model.getLoss_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



