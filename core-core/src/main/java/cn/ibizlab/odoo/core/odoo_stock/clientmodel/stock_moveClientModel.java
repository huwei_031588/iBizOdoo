package cn.ibizlab.odoo.core.odoo_stock.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[stock_move] 对象
 */
public class stock_moveClientModel implements Serializable{

    /**
     * 会计凭证
     */
    public String account_move_ids;

    @JsonIgnore
    public boolean account_move_idsDirtyFlag;
    
    /**
     * 批次
     */
    public String active_move_line_ids;

    @JsonIgnore
    public boolean active_move_line_idsDirtyFlag;
    
    /**
     * 在分拣确认后是否添加了移动
     */
    public String additional;

    @JsonIgnore
    public boolean additionalDirtyFlag;
    
    /**
     * 预测数量
     */
    public Double availability;

    @JsonIgnore
    public boolean availabilityDirtyFlag;
    
    /**
     * 欠单
     */
    public Integer backorder_id;

    @JsonIgnore
    public boolean backorder_idDirtyFlag;
    
    /**
     * BOM行
     */
    public Integer bom_line_id;

    @JsonIgnore
    public boolean bom_line_idDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 拆卸单
     */
    public Integer consume_unbuild_id;

    @JsonIgnore
    public boolean consume_unbuild_idDirtyFlag;
    
    /**
     * 拆卸单
     */
    public String consume_unbuild_id_text;

    @JsonIgnore
    public boolean consume_unbuild_id_textDirtyFlag;
    
    /**
     * 创建生产订单
     */
    public Integer created_production_id;

    @JsonIgnore
    public boolean created_production_idDirtyFlag;
    
    /**
     * 创建生产订单
     */
    public String created_production_id_text;

    @JsonIgnore
    public boolean created_production_id_textDirtyFlag;
    
    /**
     * 创建采购订单行
     */
    public Integer created_purchase_line_id;

    @JsonIgnore
    public boolean created_purchase_line_idDirtyFlag;
    
    /**
     * 创建采购订单行
     */
    public String created_purchase_line_id_text;

    @JsonIgnore
    public boolean created_purchase_line_id_textDirtyFlag;
    
    /**
     * 创建日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 预计日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_expected;

    @JsonIgnore
    public boolean date_expectedDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 完工批次已存在
     */
    public String finished_lots_exist;

    @JsonIgnore
    public boolean finished_lots_existDirtyFlag;
    
    /**
     * 补货组
     */
    public Integer group_id;

    @JsonIgnore
    public boolean group_idDirtyFlag;
    
    /**
     * 有移动行
     */
    public String has_move_lines;

    @JsonIgnore
    public boolean has_move_linesDirtyFlag;
    
    /**
     * 使用追踪的产品
     */
    public String has_tracking;

    @JsonIgnore
    public boolean has_trackingDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 库存
     */
    public Integer inventory_id;

    @JsonIgnore
    public boolean inventory_idDirtyFlag;
    
    /**
     * 库存
     */
    public String inventory_id_text;

    @JsonIgnore
    public boolean inventory_id_textDirtyFlag;
    
    /**
     * 完成
     */
    public String is_done;

    @JsonIgnore
    public boolean is_doneDirtyFlag;
    
    /**
     * 初始需求是否可以编辑
     */
    public String is_initial_demand_editable;

    @JsonIgnore
    public boolean is_initial_demand_editableDirtyFlag;
    
    /**
     * 是锁定
     */
    public String is_locked;

    @JsonIgnore
    public boolean is_lockedDirtyFlag;
    
    /**
     * 完成数量是否可以编辑
     */
    public String is_quantity_done_editable;

    @JsonIgnore
    public boolean is_quantity_done_editableDirtyFlag;
    
    /**
     * 目的位置
     */
    public Integer location_dest_id;

    @JsonIgnore
    public boolean location_dest_idDirtyFlag;
    
    /**
     * 目的位置
     */
    public String location_dest_id_text;

    @JsonIgnore
    public boolean location_dest_id_textDirtyFlag;
    
    /**
     * 源位置
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 源位置
     */
    public String location_id_text;

    @JsonIgnore
    public boolean location_id_textDirtyFlag;
    
    /**
     * 目的地移动
     */
    public String move_dest_ids;

    @JsonIgnore
    public boolean move_dest_idsDirtyFlag;
    
    /**
     * 凭证明细
     */
    public String move_line_ids;

    @JsonIgnore
    public boolean move_line_idsDirtyFlag;
    
    /**
     * 移动行无建议
     */
    public String move_line_nosuggest_ids;

    @JsonIgnore
    public boolean move_line_nosuggest_idsDirtyFlag;
    
    /**
     * 原始移动
     */
    public String move_orig_ids;

    @JsonIgnore
    public boolean move_orig_idsDirtyFlag;
    
    /**
     * 说明
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 追踪
     */
    public String needs_lots;

    @JsonIgnore
    public boolean needs_lotsDirtyFlag;
    
    /**
     * 备注
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 待加工的作业
     */
    public Integer operation_id;

    @JsonIgnore
    public boolean operation_idDirtyFlag;
    
    /**
     * 待加工的作业
     */
    public String operation_id_text;

    @JsonIgnore
    public boolean operation_id_textDirtyFlag;
    
    /**
     * 订单完成批次
     */
    public String order_finished_lot_ids;

    @JsonIgnore
    public boolean order_finished_lot_idsDirtyFlag;
    
    /**
     * 源文档
     */
    public String origin;

    @JsonIgnore
    public boolean originDirtyFlag;
    
    /**
     * 原始退回移动
     */
    public Integer origin_returned_move_id;

    @JsonIgnore
    public boolean origin_returned_move_idDirtyFlag;
    
    /**
     * 原始退回移动
     */
    public String origin_returned_move_id_text;

    @JsonIgnore
    public boolean origin_returned_move_id_textDirtyFlag;
    
    /**
     * 包裹层级
     */
    public Integer package_level_id;

    @JsonIgnore
    public boolean package_level_idDirtyFlag;
    
    /**
     * 目的地地址
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 目的地地址
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 作业的类型
     */
    public String picking_code;

    @JsonIgnore
    public boolean picking_codeDirtyFlag;
    
    /**
     * 调拨参照
     */
    public Integer picking_id;

    @JsonIgnore
    public boolean picking_idDirtyFlag;
    
    /**
     * 调拨参照
     */
    public String picking_id_text;

    @JsonIgnore
    public boolean picking_id_textDirtyFlag;
    
    /**
     * 调拨目的地地址
     */
    public Integer picking_partner_id;

    @JsonIgnore
    public boolean picking_partner_idDirtyFlag;
    
    /**
     * 移动整个包裹
     */
    public String picking_type_entire_packs;

    @JsonIgnore
    public boolean picking_type_entire_packsDirtyFlag;
    
    /**
     * 作业类型
     */
    public Integer picking_type_id;

    @JsonIgnore
    public boolean picking_type_idDirtyFlag;
    
    /**
     * 作业类型
     */
    public String picking_type_id_text;

    @JsonIgnore
    public boolean picking_type_id_textDirtyFlag;
    
    /**
     * 单价
     */
    public Double price_unit;

    @JsonIgnore
    public boolean price_unitDirtyFlag;
    
    /**
     * 优先级
     */
    public String priority;

    @JsonIgnore
    public boolean priorityDirtyFlag;
    
    /**
     * 供应方法
     */
    public String procure_method;

    @JsonIgnore
    public boolean procure_methodDirtyFlag;
    
    /**
     * 成品的生产订单
     */
    public Integer production_id;

    @JsonIgnore
    public boolean production_idDirtyFlag;
    
    /**
     * 成品的生产订单
     */
    public String production_id_text;

    @JsonIgnore
    public boolean production_id_textDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 首选包装
     */
    public Integer product_packaging;

    @JsonIgnore
    public boolean product_packagingDirtyFlag;
    
    /**
     * 首选包装
     */
    public String product_packaging_text;

    @JsonIgnore
    public boolean product_packaging_textDirtyFlag;
    
    /**
     * 实际数量
     */
    public Double product_qty;

    @JsonIgnore
    public boolean product_qtyDirtyFlag;
    
    /**
     * 产品模板
     */
    public Integer product_tmpl_id;

    @JsonIgnore
    public boolean product_tmpl_idDirtyFlag;
    
    /**
     * 产品类型
     */
    public String product_type;

    @JsonIgnore
    public boolean product_typeDirtyFlag;
    
    /**
     * 单位
     */
    public Integer product_uom;

    @JsonIgnore
    public boolean product_uomDirtyFlag;
    
    /**
     * 初始需求
     */
    public Double product_uom_qty;

    @JsonIgnore
    public boolean product_uom_qtyDirtyFlag;
    
    /**
     * 单位
     */
    public String product_uom_text;

    @JsonIgnore
    public boolean product_uom_textDirtyFlag;
    
    /**
     * 传播取消以及拆分
     */
    public String propagate;

    @JsonIgnore
    public boolean propagateDirtyFlag;
    
    /**
     * 采购订单行
     */
    public Integer purchase_line_id;

    @JsonIgnore
    public boolean purchase_line_idDirtyFlag;
    
    /**
     * 采购订单行
     */
    public String purchase_line_id_text;

    @JsonIgnore
    public boolean purchase_line_id_textDirtyFlag;
    
    /**
     * 完成数量
     */
    public Double quantity_done;

    @JsonIgnore
    public boolean quantity_doneDirtyFlag;
    
    /**
     * 原材料的生产订单
     */
    public Integer raw_material_production_id;

    @JsonIgnore
    public boolean raw_material_production_idDirtyFlag;
    
    /**
     * 原材料的生产订单
     */
    public String raw_material_production_id_text;

    @JsonIgnore
    public boolean raw_material_production_id_textDirtyFlag;
    
    /**
     * 编号
     */
    public String reference;

    @JsonIgnore
    public boolean referenceDirtyFlag;
    
    /**
     * 剩余数量
     */
    public Double remaining_qty;

    @JsonIgnore
    public boolean remaining_qtyDirtyFlag;
    
    /**
     * 剩余价值
     */
    public Double remaining_value;

    @JsonIgnore
    public boolean remaining_valueDirtyFlag;
    
    /**
     * 维修
     */
    public Integer repair_id;

    @JsonIgnore
    public boolean repair_idDirtyFlag;
    
    /**
     * 维修
     */
    public String repair_id_text;

    @JsonIgnore
    public boolean repair_id_textDirtyFlag;
    
    /**
     * 已预留数量
     */
    public Double reserved_availability;

    @JsonIgnore
    public boolean reserved_availabilityDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer restrict_partner_id;

    @JsonIgnore
    public boolean restrict_partner_idDirtyFlag;
    
    /**
     * 所有者
     */
    public String restrict_partner_id_text;

    @JsonIgnore
    public boolean restrict_partner_id_textDirtyFlag;
    
    /**
     * 全部退回移动
     */
    public String returned_move_ids;

    @JsonIgnore
    public boolean returned_move_idsDirtyFlag;
    
    /**
     * 目的路线
     */
    public String route_ids;

    @JsonIgnore
    public boolean route_idsDirtyFlag;
    
    /**
     * 库存规则
     */
    public Integer rule_id;

    @JsonIgnore
    public boolean rule_idDirtyFlag;
    
    /**
     * 库存规则
     */
    public String rule_id_text;

    @JsonIgnore
    public boolean rule_id_textDirtyFlag;
    
    /**
     * 销售明细行
     */
    public Integer sale_line_id;

    @JsonIgnore
    public boolean sale_line_idDirtyFlag;
    
    /**
     * 销售明细行
     */
    public String sale_line_id_text;

    @JsonIgnore
    public boolean sale_line_id_textDirtyFlag;
    
    /**
     * 已报废
     */
    public String scrapped;

    @JsonIgnore
    public boolean scrappedDirtyFlag;
    
    /**
     * 报废
     */
    public String scrap_ids;

    @JsonIgnore
    public boolean scrap_idsDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 详情可见
     */
    public String show_details_visible;

    @JsonIgnore
    public boolean show_details_visibleDirtyFlag;
    
    /**
     * 显示详细作业
     */
    public String show_operations;

    @JsonIgnore
    public boolean show_operationsDirtyFlag;
    
    /**
     * 从供应商
     */
    public String show_reserved_availability;

    @JsonIgnore
    public boolean show_reserved_availabilityDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 可用量
     */
    public String string_availability_info;

    @JsonIgnore
    public boolean string_availability_infoDirtyFlag;
    
    /**
     * 退款 (更新 SO/PO)
     */
    public String to_refund;

    @JsonIgnore
    public boolean to_refundDirtyFlag;
    
    /**
     * 拆卸顺序
     */
    public Integer unbuild_id;

    @JsonIgnore
    public boolean unbuild_idDirtyFlag;
    
    /**
     * 拆卸顺序
     */
    public String unbuild_id_text;

    @JsonIgnore
    public boolean unbuild_id_textDirtyFlag;
    
    /**
     * 单位因子
     */
    public Double unit_factor;

    @JsonIgnore
    public boolean unit_factorDirtyFlag;
    
    /**
     * 值
     */
    public Double value;

    @JsonIgnore
    public boolean valueDirtyFlag;
    
    /**
     * 仓库
     */
    public Integer warehouse_id;

    @JsonIgnore
    public boolean warehouse_idDirtyFlag;
    
    /**
     * 仓库
     */
    public String warehouse_id_text;

    @JsonIgnore
    public boolean warehouse_id_textDirtyFlag;
    
    /**
     * 待消耗的工单
     */
    public Integer workorder_id;

    @JsonIgnore
    public boolean workorder_idDirtyFlag;
    
    /**
     * 待消耗的工单
     */
    public String workorder_id_text;

    @JsonIgnore
    public boolean workorder_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [会计凭证]
     */
    @JsonProperty("account_move_ids")
    public String getAccount_move_ids(){
        return this.account_move_ids ;
    }

    /**
     * 设置 [会计凭证]
     */
    @JsonProperty("account_move_ids")
    public void setAccount_move_ids(String  account_move_ids){
        this.account_move_ids = account_move_ids ;
        this.account_move_idsDirtyFlag = true ;
    }

     /**
     * 获取 [会计凭证]脏标记
     */
    @JsonIgnore
    public boolean getAccount_move_idsDirtyFlag(){
        return this.account_move_idsDirtyFlag ;
    }   

    /**
     * 获取 [批次]
     */
    @JsonProperty("active_move_line_ids")
    public String getActive_move_line_ids(){
        return this.active_move_line_ids ;
    }

    /**
     * 设置 [批次]
     */
    @JsonProperty("active_move_line_ids")
    public void setActive_move_line_ids(String  active_move_line_ids){
        this.active_move_line_ids = active_move_line_ids ;
        this.active_move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [批次]脏标记
     */
    @JsonIgnore
    public boolean getActive_move_line_idsDirtyFlag(){
        return this.active_move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [在分拣确认后是否添加了移动]
     */
    @JsonProperty("additional")
    public String getAdditional(){
        return this.additional ;
    }

    /**
     * 设置 [在分拣确认后是否添加了移动]
     */
    @JsonProperty("additional")
    public void setAdditional(String  additional){
        this.additional = additional ;
        this.additionalDirtyFlag = true ;
    }

     /**
     * 获取 [在分拣确认后是否添加了移动]脏标记
     */
    @JsonIgnore
    public boolean getAdditionalDirtyFlag(){
        return this.additionalDirtyFlag ;
    }   

    /**
     * 获取 [预测数量]
     */
    @JsonProperty("availability")
    public Double getAvailability(){
        return this.availability ;
    }

    /**
     * 设置 [预测数量]
     */
    @JsonProperty("availability")
    public void setAvailability(Double  availability){
        this.availability = availability ;
        this.availabilityDirtyFlag = true ;
    }

     /**
     * 获取 [预测数量]脏标记
     */
    @JsonIgnore
    public boolean getAvailabilityDirtyFlag(){
        return this.availabilityDirtyFlag ;
    }   

    /**
     * 获取 [欠单]
     */
    @JsonProperty("backorder_id")
    public Integer getBackorder_id(){
        return this.backorder_id ;
    }

    /**
     * 设置 [欠单]
     */
    @JsonProperty("backorder_id")
    public void setBackorder_id(Integer  backorder_id){
        this.backorder_id = backorder_id ;
        this.backorder_idDirtyFlag = true ;
    }

     /**
     * 获取 [欠单]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_idDirtyFlag(){
        return this.backorder_idDirtyFlag ;
    }   

    /**
     * 获取 [BOM行]
     */
    @JsonProperty("bom_line_id")
    public Integer getBom_line_id(){
        return this.bom_line_id ;
    }

    /**
     * 设置 [BOM行]
     */
    @JsonProperty("bom_line_id")
    public void setBom_line_id(Integer  bom_line_id){
        this.bom_line_id = bom_line_id ;
        this.bom_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [BOM行]脏标记
     */
    @JsonIgnore
    public boolean getBom_line_idDirtyFlag(){
        return this.bom_line_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [拆卸单]
     */
    @JsonProperty("consume_unbuild_id")
    public Integer getConsume_unbuild_id(){
        return this.consume_unbuild_id ;
    }

    /**
     * 设置 [拆卸单]
     */
    @JsonProperty("consume_unbuild_id")
    public void setConsume_unbuild_id(Integer  consume_unbuild_id){
        this.consume_unbuild_id = consume_unbuild_id ;
        this.consume_unbuild_idDirtyFlag = true ;
    }

     /**
     * 获取 [拆卸单]脏标记
     */
    @JsonIgnore
    public boolean getConsume_unbuild_idDirtyFlag(){
        return this.consume_unbuild_idDirtyFlag ;
    }   

    /**
     * 获取 [拆卸单]
     */
    @JsonProperty("consume_unbuild_id_text")
    public String getConsume_unbuild_id_text(){
        return this.consume_unbuild_id_text ;
    }

    /**
     * 设置 [拆卸单]
     */
    @JsonProperty("consume_unbuild_id_text")
    public void setConsume_unbuild_id_text(String  consume_unbuild_id_text){
        this.consume_unbuild_id_text = consume_unbuild_id_text ;
        this.consume_unbuild_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [拆卸单]脏标记
     */
    @JsonIgnore
    public boolean getConsume_unbuild_id_textDirtyFlag(){
        return this.consume_unbuild_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建生产订单]
     */
    @JsonProperty("created_production_id")
    public Integer getCreated_production_id(){
        return this.created_production_id ;
    }

    /**
     * 设置 [创建生产订单]
     */
    @JsonProperty("created_production_id")
    public void setCreated_production_id(Integer  created_production_id){
        this.created_production_id = created_production_id ;
        this.created_production_idDirtyFlag = true ;
    }

     /**
     * 获取 [创建生产订单]脏标记
     */
    @JsonIgnore
    public boolean getCreated_production_idDirtyFlag(){
        return this.created_production_idDirtyFlag ;
    }   

    /**
     * 获取 [创建生产订单]
     */
    @JsonProperty("created_production_id_text")
    public String getCreated_production_id_text(){
        return this.created_production_id_text ;
    }

    /**
     * 设置 [创建生产订单]
     */
    @JsonProperty("created_production_id_text")
    public void setCreated_production_id_text(String  created_production_id_text){
        this.created_production_id_text = created_production_id_text ;
        this.created_production_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建生产订单]脏标记
     */
    @JsonIgnore
    public boolean getCreated_production_id_textDirtyFlag(){
        return this.created_production_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建采购订单行]
     */
    @JsonProperty("created_purchase_line_id")
    public Integer getCreated_purchase_line_id(){
        return this.created_purchase_line_id ;
    }

    /**
     * 设置 [创建采购订单行]
     */
    @JsonProperty("created_purchase_line_id")
    public void setCreated_purchase_line_id(Integer  created_purchase_line_id){
        this.created_purchase_line_id = created_purchase_line_id ;
        this.created_purchase_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [创建采购订单行]脏标记
     */
    @JsonIgnore
    public boolean getCreated_purchase_line_idDirtyFlag(){
        return this.created_purchase_line_idDirtyFlag ;
    }   

    /**
     * 获取 [创建采购订单行]
     */
    @JsonProperty("created_purchase_line_id_text")
    public String getCreated_purchase_line_id_text(){
        return this.created_purchase_line_id_text ;
    }

    /**
     * 设置 [创建采购订单行]
     */
    @JsonProperty("created_purchase_line_id_text")
    public void setCreated_purchase_line_id_text(String  created_purchase_line_id_text){
        this.created_purchase_line_id_text = created_purchase_line_id_text ;
        this.created_purchase_line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建采购订单行]脏标记
     */
    @JsonIgnore
    public boolean getCreated_purchase_line_id_textDirtyFlag(){
        return this.created_purchase_line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建日期]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建日期]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建日期]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [预计日期]
     */
    @JsonProperty("date_expected")
    public Timestamp getDate_expected(){
        return this.date_expected ;
    }

    /**
     * 设置 [预计日期]
     */
    @JsonProperty("date_expected")
    public void setDate_expected(Timestamp  date_expected){
        this.date_expected = date_expected ;
        this.date_expectedDirtyFlag = true ;
    }

     /**
     * 获取 [预计日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_expectedDirtyFlag(){
        return this.date_expectedDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [完工批次已存在]
     */
    @JsonProperty("finished_lots_exist")
    public String getFinished_lots_exist(){
        return this.finished_lots_exist ;
    }

    /**
     * 设置 [完工批次已存在]
     */
    @JsonProperty("finished_lots_exist")
    public void setFinished_lots_exist(String  finished_lots_exist){
        this.finished_lots_exist = finished_lots_exist ;
        this.finished_lots_existDirtyFlag = true ;
    }

     /**
     * 获取 [完工批次已存在]脏标记
     */
    @JsonIgnore
    public boolean getFinished_lots_existDirtyFlag(){
        return this.finished_lots_existDirtyFlag ;
    }   

    /**
     * 获取 [补货组]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return this.group_id ;
    }

    /**
     * 设置 [补货组]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

     /**
     * 获取 [补货组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return this.group_idDirtyFlag ;
    }   

    /**
     * 获取 [有移动行]
     */
    @JsonProperty("has_move_lines")
    public String getHas_move_lines(){
        return this.has_move_lines ;
    }

    /**
     * 设置 [有移动行]
     */
    @JsonProperty("has_move_lines")
    public void setHas_move_lines(String  has_move_lines){
        this.has_move_lines = has_move_lines ;
        this.has_move_linesDirtyFlag = true ;
    }

     /**
     * 获取 [有移动行]脏标记
     */
    @JsonIgnore
    public boolean getHas_move_linesDirtyFlag(){
        return this.has_move_linesDirtyFlag ;
    }   

    /**
     * 获取 [使用追踪的产品]
     */
    @JsonProperty("has_tracking")
    public String getHas_tracking(){
        return this.has_tracking ;
    }

    /**
     * 设置 [使用追踪的产品]
     */
    @JsonProperty("has_tracking")
    public void setHas_tracking(String  has_tracking){
        this.has_tracking = has_tracking ;
        this.has_trackingDirtyFlag = true ;
    }

     /**
     * 获取 [使用追踪的产品]脏标记
     */
    @JsonIgnore
    public boolean getHas_trackingDirtyFlag(){
        return this.has_trackingDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [库存]
     */
    @JsonProperty("inventory_id")
    public Integer getInventory_id(){
        return this.inventory_id ;
    }

    /**
     * 设置 [库存]
     */
    @JsonProperty("inventory_id")
    public void setInventory_id(Integer  inventory_id){
        this.inventory_id = inventory_id ;
        this.inventory_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存]脏标记
     */
    @JsonIgnore
    public boolean getInventory_idDirtyFlag(){
        return this.inventory_idDirtyFlag ;
    }   

    /**
     * 获取 [库存]
     */
    @JsonProperty("inventory_id_text")
    public String getInventory_id_text(){
        return this.inventory_id_text ;
    }

    /**
     * 设置 [库存]
     */
    @JsonProperty("inventory_id_text")
    public void setInventory_id_text(String  inventory_id_text){
        this.inventory_id_text = inventory_id_text ;
        this.inventory_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存]脏标记
     */
    @JsonIgnore
    public boolean getInventory_id_textDirtyFlag(){
        return this.inventory_id_textDirtyFlag ;
    }   

    /**
     * 获取 [完成]
     */
    @JsonProperty("is_done")
    public String getIs_done(){
        return this.is_done ;
    }

    /**
     * 设置 [完成]
     */
    @JsonProperty("is_done")
    public void setIs_done(String  is_done){
        this.is_done = is_done ;
        this.is_doneDirtyFlag = true ;
    }

     /**
     * 获取 [完成]脏标记
     */
    @JsonIgnore
    public boolean getIs_doneDirtyFlag(){
        return this.is_doneDirtyFlag ;
    }   

    /**
     * 获取 [初始需求是否可以编辑]
     */
    @JsonProperty("is_initial_demand_editable")
    public String getIs_initial_demand_editable(){
        return this.is_initial_demand_editable ;
    }

    /**
     * 设置 [初始需求是否可以编辑]
     */
    @JsonProperty("is_initial_demand_editable")
    public void setIs_initial_demand_editable(String  is_initial_demand_editable){
        this.is_initial_demand_editable = is_initial_demand_editable ;
        this.is_initial_demand_editableDirtyFlag = true ;
    }

     /**
     * 获取 [初始需求是否可以编辑]脏标记
     */
    @JsonIgnore
    public boolean getIs_initial_demand_editableDirtyFlag(){
        return this.is_initial_demand_editableDirtyFlag ;
    }   

    /**
     * 获取 [是锁定]
     */
    @JsonProperty("is_locked")
    public String getIs_locked(){
        return this.is_locked ;
    }

    /**
     * 设置 [是锁定]
     */
    @JsonProperty("is_locked")
    public void setIs_locked(String  is_locked){
        this.is_locked = is_locked ;
        this.is_lockedDirtyFlag = true ;
    }

     /**
     * 获取 [是锁定]脏标记
     */
    @JsonIgnore
    public boolean getIs_lockedDirtyFlag(){
        return this.is_lockedDirtyFlag ;
    }   

    /**
     * 获取 [完成数量是否可以编辑]
     */
    @JsonProperty("is_quantity_done_editable")
    public String getIs_quantity_done_editable(){
        return this.is_quantity_done_editable ;
    }

    /**
     * 设置 [完成数量是否可以编辑]
     */
    @JsonProperty("is_quantity_done_editable")
    public void setIs_quantity_done_editable(String  is_quantity_done_editable){
        this.is_quantity_done_editable = is_quantity_done_editable ;
        this.is_quantity_done_editableDirtyFlag = true ;
    }

     /**
     * 获取 [完成数量是否可以编辑]脏标记
     */
    @JsonIgnore
    public boolean getIs_quantity_done_editableDirtyFlag(){
        return this.is_quantity_done_editableDirtyFlag ;
    }   

    /**
     * 获取 [目的位置]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return this.location_dest_id ;
    }

    /**
     * 设置 [目的位置]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

     /**
     * 获取 [目的位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return this.location_dest_idDirtyFlag ;
    }   

    /**
     * 获取 [目的位置]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return this.location_dest_id_text ;
    }

    /**
     * 设置 [目的位置]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目的位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return this.location_dest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [源位置]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [源位置]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [源位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [源位置]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return this.location_id_text ;
    }

    /**
     * 设置 [源位置]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [源位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return this.location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [目的地移动]
     */
    @JsonProperty("move_dest_ids")
    public String getMove_dest_ids(){
        return this.move_dest_ids ;
    }

    /**
     * 设置 [目的地移动]
     */
    @JsonProperty("move_dest_ids")
    public void setMove_dest_ids(String  move_dest_ids){
        this.move_dest_ids = move_dest_ids ;
        this.move_dest_idsDirtyFlag = true ;
    }

     /**
     * 获取 [目的地移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_dest_idsDirtyFlag(){
        return this.move_dest_idsDirtyFlag ;
    }   

    /**
     * 获取 [凭证明细]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return this.move_line_ids ;
    }

    /**
     * 设置 [凭证明细]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [凭证明细]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return this.move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [移动行无建议]
     */
    @JsonProperty("move_line_nosuggest_ids")
    public String getMove_line_nosuggest_ids(){
        return this.move_line_nosuggest_ids ;
    }

    /**
     * 设置 [移动行无建议]
     */
    @JsonProperty("move_line_nosuggest_ids")
    public void setMove_line_nosuggest_ids(String  move_line_nosuggest_ids){
        this.move_line_nosuggest_ids = move_line_nosuggest_ids ;
        this.move_line_nosuggest_idsDirtyFlag = true ;
    }

     /**
     * 获取 [移动行无建议]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_nosuggest_idsDirtyFlag(){
        return this.move_line_nosuggest_idsDirtyFlag ;
    }   

    /**
     * 获取 [原始移动]
     */
    @JsonProperty("move_orig_ids")
    public String getMove_orig_ids(){
        return this.move_orig_ids ;
    }

    /**
     * 设置 [原始移动]
     */
    @JsonProperty("move_orig_ids")
    public void setMove_orig_ids(String  move_orig_ids){
        this.move_orig_ids = move_orig_ids ;
        this.move_orig_idsDirtyFlag = true ;
    }

     /**
     * 获取 [原始移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_orig_idsDirtyFlag(){
        return this.move_orig_idsDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [追踪]
     */
    @JsonProperty("needs_lots")
    public String getNeeds_lots(){
        return this.needs_lots ;
    }

    /**
     * 设置 [追踪]
     */
    @JsonProperty("needs_lots")
    public void setNeeds_lots(String  needs_lots){
        this.needs_lots = needs_lots ;
        this.needs_lotsDirtyFlag = true ;
    }

     /**
     * 获取 [追踪]脏标记
     */
    @JsonIgnore
    public boolean getNeeds_lotsDirtyFlag(){
        return this.needs_lotsDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [待加工的作业]
     */
    @JsonProperty("operation_id")
    public Integer getOperation_id(){
        return this.operation_id ;
    }

    /**
     * 设置 [待加工的作业]
     */
    @JsonProperty("operation_id")
    public void setOperation_id(Integer  operation_id){
        this.operation_id = operation_id ;
        this.operation_idDirtyFlag = true ;
    }

     /**
     * 获取 [待加工的作业]脏标记
     */
    @JsonIgnore
    public boolean getOperation_idDirtyFlag(){
        return this.operation_idDirtyFlag ;
    }   

    /**
     * 获取 [待加工的作业]
     */
    @JsonProperty("operation_id_text")
    public String getOperation_id_text(){
        return this.operation_id_text ;
    }

    /**
     * 设置 [待加工的作业]
     */
    @JsonProperty("operation_id_text")
    public void setOperation_id_text(String  operation_id_text){
        this.operation_id_text = operation_id_text ;
        this.operation_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [待加工的作业]脏标记
     */
    @JsonIgnore
    public boolean getOperation_id_textDirtyFlag(){
        return this.operation_id_textDirtyFlag ;
    }   

    /**
     * 获取 [订单完成批次]
     */
    @JsonProperty("order_finished_lot_ids")
    public String getOrder_finished_lot_ids(){
        return this.order_finished_lot_ids ;
    }

    /**
     * 设置 [订单完成批次]
     */
    @JsonProperty("order_finished_lot_ids")
    public void setOrder_finished_lot_ids(String  order_finished_lot_ids){
        this.order_finished_lot_ids = order_finished_lot_ids ;
        this.order_finished_lot_idsDirtyFlag = true ;
    }

     /**
     * 获取 [订单完成批次]脏标记
     */
    @JsonIgnore
    public boolean getOrder_finished_lot_idsDirtyFlag(){
        return this.order_finished_lot_idsDirtyFlag ;
    }   

    /**
     * 获取 [源文档]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return this.origin ;
    }

    /**
     * 设置 [源文档]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

     /**
     * 获取 [源文档]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return this.originDirtyFlag ;
    }   

    /**
     * 获取 [原始退回移动]
     */
    @JsonProperty("origin_returned_move_id")
    public Integer getOrigin_returned_move_id(){
        return this.origin_returned_move_id ;
    }

    /**
     * 设置 [原始退回移动]
     */
    @JsonProperty("origin_returned_move_id")
    public void setOrigin_returned_move_id(Integer  origin_returned_move_id){
        this.origin_returned_move_id = origin_returned_move_id ;
        this.origin_returned_move_idDirtyFlag = true ;
    }

     /**
     * 获取 [原始退回移动]脏标记
     */
    @JsonIgnore
    public boolean getOrigin_returned_move_idDirtyFlag(){
        return this.origin_returned_move_idDirtyFlag ;
    }   

    /**
     * 获取 [原始退回移动]
     */
    @JsonProperty("origin_returned_move_id_text")
    public String getOrigin_returned_move_id_text(){
        return this.origin_returned_move_id_text ;
    }

    /**
     * 设置 [原始退回移动]
     */
    @JsonProperty("origin_returned_move_id_text")
    public void setOrigin_returned_move_id_text(String  origin_returned_move_id_text){
        this.origin_returned_move_id_text = origin_returned_move_id_text ;
        this.origin_returned_move_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [原始退回移动]脏标记
     */
    @JsonIgnore
    public boolean getOrigin_returned_move_id_textDirtyFlag(){
        return this.origin_returned_move_id_textDirtyFlag ;
    }   

    /**
     * 获取 [包裹层级]
     */
    @JsonProperty("package_level_id")
    public Integer getPackage_level_id(){
        return this.package_level_id ;
    }

    /**
     * 设置 [包裹层级]
     */
    @JsonProperty("package_level_id")
    public void setPackage_level_id(Integer  package_level_id){
        this.package_level_id = package_level_id ;
        this.package_level_idDirtyFlag = true ;
    }

     /**
     * 获取 [包裹层级]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_idDirtyFlag(){
        return this.package_level_idDirtyFlag ;
    }   

    /**
     * 获取 [目的地地址]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [目的地地址]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [目的地地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [目的地地址]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [目的地地址]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目的地地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [作业的类型]
     */
    @JsonProperty("picking_code")
    public String getPicking_code(){
        return this.picking_code ;
    }

    /**
     * 设置 [作业的类型]
     */
    @JsonProperty("picking_code")
    public void setPicking_code(String  picking_code){
        this.picking_code = picking_code ;
        this.picking_codeDirtyFlag = true ;
    }

     /**
     * 获取 [作业的类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_codeDirtyFlag(){
        return this.picking_codeDirtyFlag ;
    }   

    /**
     * 获取 [调拨参照]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return this.picking_id ;
    }

    /**
     * 设置 [调拨参照]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

     /**
     * 获取 [调拨参照]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return this.picking_idDirtyFlag ;
    }   

    /**
     * 获取 [调拨参照]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return this.picking_id_text ;
    }

    /**
     * 设置 [调拨参照]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [调拨参照]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return this.picking_id_textDirtyFlag ;
    }   

    /**
     * 获取 [调拨目的地地址]
     */
    @JsonProperty("picking_partner_id")
    public Integer getPicking_partner_id(){
        return this.picking_partner_id ;
    }

    /**
     * 设置 [调拨目的地地址]
     */
    @JsonProperty("picking_partner_id")
    public void setPicking_partner_id(Integer  picking_partner_id){
        this.picking_partner_id = picking_partner_id ;
        this.picking_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [调拨目的地地址]脏标记
     */
    @JsonIgnore
    public boolean getPicking_partner_idDirtyFlag(){
        return this.picking_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [移动整个包裹]
     */
    @JsonProperty("picking_type_entire_packs")
    public String getPicking_type_entire_packs(){
        return this.picking_type_entire_packs ;
    }

    /**
     * 设置 [移动整个包裹]
     */
    @JsonProperty("picking_type_entire_packs")
    public void setPicking_type_entire_packs(String  picking_type_entire_packs){
        this.picking_type_entire_packs = picking_type_entire_packs ;
        this.picking_type_entire_packsDirtyFlag = true ;
    }

     /**
     * 获取 [移动整个包裹]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_entire_packsDirtyFlag(){
        return this.picking_type_entire_packsDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return this.picking_type_id ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return this.picking_type_idDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return this.picking_type_id_text ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return this.picking_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [单价]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return this.price_unit ;
    }

    /**
     * 设置 [单价]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

     /**
     * 获取 [单价]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return this.price_unitDirtyFlag ;
    }   

    /**
     * 获取 [优先级]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [优先级]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

     /**
     * 获取 [优先级]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }   

    /**
     * 获取 [供应方法]
     */
    @JsonProperty("procure_method")
    public String getProcure_method(){
        return this.procure_method ;
    }

    /**
     * 设置 [供应方法]
     */
    @JsonProperty("procure_method")
    public void setProcure_method(String  procure_method){
        this.procure_method = procure_method ;
        this.procure_methodDirtyFlag = true ;
    }

     /**
     * 获取 [供应方法]脏标记
     */
    @JsonIgnore
    public boolean getProcure_methodDirtyFlag(){
        return this.procure_methodDirtyFlag ;
    }   

    /**
     * 获取 [成品的生产订单]
     */
    @JsonProperty("production_id")
    public Integer getProduction_id(){
        return this.production_id ;
    }

    /**
     * 设置 [成品的生产订单]
     */
    @JsonProperty("production_id")
    public void setProduction_id(Integer  production_id){
        this.production_id = production_id ;
        this.production_idDirtyFlag = true ;
    }

     /**
     * 获取 [成品的生产订单]脏标记
     */
    @JsonIgnore
    public boolean getProduction_idDirtyFlag(){
        return this.production_idDirtyFlag ;
    }   

    /**
     * 获取 [成品的生产订单]
     */
    @JsonProperty("production_id_text")
    public String getProduction_id_text(){
        return this.production_id_text ;
    }

    /**
     * 设置 [成品的生产订单]
     */
    @JsonProperty("production_id_text")
    public void setProduction_id_text(String  production_id_text){
        this.production_id_text = production_id_text ;
        this.production_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [成品的生产订单]脏标记
     */
    @JsonIgnore
    public boolean getProduction_id_textDirtyFlag(){
        return this.production_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [首选包装]
     */
    @JsonProperty("product_packaging")
    public Integer getProduct_packaging(){
        return this.product_packaging ;
    }

    /**
     * 设置 [首选包装]
     */
    @JsonProperty("product_packaging")
    public void setProduct_packaging(Integer  product_packaging){
        this.product_packaging = product_packaging ;
        this.product_packagingDirtyFlag = true ;
    }

     /**
     * 获取 [首选包装]脏标记
     */
    @JsonIgnore
    public boolean getProduct_packagingDirtyFlag(){
        return this.product_packagingDirtyFlag ;
    }   

    /**
     * 获取 [首选包装]
     */
    @JsonProperty("product_packaging_text")
    public String getProduct_packaging_text(){
        return this.product_packaging_text ;
    }

    /**
     * 设置 [首选包装]
     */
    @JsonProperty("product_packaging_text")
    public void setProduct_packaging_text(String  product_packaging_text){
        this.product_packaging_text = product_packaging_text ;
        this.product_packaging_textDirtyFlag = true ;
    }

     /**
     * 获取 [首选包装]脏标记
     */
    @JsonIgnore
    public boolean getProduct_packaging_textDirtyFlag(){
        return this.product_packaging_textDirtyFlag ;
    }   

    /**
     * 获取 [实际数量]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return this.product_qty ;
    }

    /**
     * 设置 [实际数量]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [实际数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return this.product_qtyDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [产品类型]
     */
    @JsonProperty("product_type")
    public String getProduct_type(){
        return this.product_type ;
    }

    /**
     * 设置 [产品类型]
     */
    @JsonProperty("product_type")
    public void setProduct_type(String  product_type){
        this.product_type = product_type ;
        this.product_typeDirtyFlag = true ;
    }

     /**
     * 获取 [产品类型]脏标记
     */
    @JsonIgnore
    public boolean getProduct_typeDirtyFlag(){
        return this.product_typeDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return this.product_uom ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return this.product_uomDirtyFlag ;
    }   

    /**
     * 获取 [初始需求]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return this.product_uom_qty ;
    }

    /**
     * 设置 [初始需求]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [初始需求]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return this.product_uom_qtyDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return this.product_uom_text ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return this.product_uom_textDirtyFlag ;
    }   

    /**
     * 获取 [传播取消以及拆分]
     */
    @JsonProperty("propagate")
    public String getPropagate(){
        return this.propagate ;
    }

    /**
     * 设置 [传播取消以及拆分]
     */
    @JsonProperty("propagate")
    public void setPropagate(String  propagate){
        this.propagate = propagate ;
        this.propagateDirtyFlag = true ;
    }

     /**
     * 获取 [传播取消以及拆分]脏标记
     */
    @JsonIgnore
    public boolean getPropagateDirtyFlag(){
        return this.propagateDirtyFlag ;
    }   

    /**
     * 获取 [采购订单行]
     */
    @JsonProperty("purchase_line_id")
    public Integer getPurchase_line_id(){
        return this.purchase_line_id ;
    }

    /**
     * 设置 [采购订单行]
     */
    @JsonProperty("purchase_line_id")
    public void setPurchase_line_id(Integer  purchase_line_id){
        this.purchase_line_id = purchase_line_id ;
        this.purchase_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单行]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_idDirtyFlag(){
        return this.purchase_line_idDirtyFlag ;
    }   

    /**
     * 获取 [采购订单行]
     */
    @JsonProperty("purchase_line_id_text")
    public String getPurchase_line_id_text(){
        return this.purchase_line_id_text ;
    }

    /**
     * 设置 [采购订单行]
     */
    @JsonProperty("purchase_line_id_text")
    public void setPurchase_line_id_text(String  purchase_line_id_text){
        this.purchase_line_id_text = purchase_line_id_text ;
        this.purchase_line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单行]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_id_textDirtyFlag(){
        return this.purchase_line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [完成数量]
     */
    @JsonProperty("quantity_done")
    public Double getQuantity_done(){
        return this.quantity_done ;
    }

    /**
     * 设置 [完成数量]
     */
    @JsonProperty("quantity_done")
    public void setQuantity_done(Double  quantity_done){
        this.quantity_done = quantity_done ;
        this.quantity_doneDirtyFlag = true ;
    }

     /**
     * 获取 [完成数量]脏标记
     */
    @JsonIgnore
    public boolean getQuantity_doneDirtyFlag(){
        return this.quantity_doneDirtyFlag ;
    }   

    /**
     * 获取 [原材料的生产订单]
     */
    @JsonProperty("raw_material_production_id")
    public Integer getRaw_material_production_id(){
        return this.raw_material_production_id ;
    }

    /**
     * 设置 [原材料的生产订单]
     */
    @JsonProperty("raw_material_production_id")
    public void setRaw_material_production_id(Integer  raw_material_production_id){
        this.raw_material_production_id = raw_material_production_id ;
        this.raw_material_production_idDirtyFlag = true ;
    }

     /**
     * 获取 [原材料的生产订单]脏标记
     */
    @JsonIgnore
    public boolean getRaw_material_production_idDirtyFlag(){
        return this.raw_material_production_idDirtyFlag ;
    }   

    /**
     * 获取 [原材料的生产订单]
     */
    @JsonProperty("raw_material_production_id_text")
    public String getRaw_material_production_id_text(){
        return this.raw_material_production_id_text ;
    }

    /**
     * 设置 [原材料的生产订单]
     */
    @JsonProperty("raw_material_production_id_text")
    public void setRaw_material_production_id_text(String  raw_material_production_id_text){
        this.raw_material_production_id_text = raw_material_production_id_text ;
        this.raw_material_production_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [原材料的生产订单]脏标记
     */
    @JsonIgnore
    public boolean getRaw_material_production_id_textDirtyFlag(){
        return this.raw_material_production_id_textDirtyFlag ;
    }   

    /**
     * 获取 [编号]
     */
    @JsonProperty("reference")
    public String getReference(){
        return this.reference ;
    }

    /**
     * 设置 [编号]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

     /**
     * 获取 [编号]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return this.referenceDirtyFlag ;
    }   

    /**
     * 获取 [剩余数量]
     */
    @JsonProperty("remaining_qty")
    public Double getRemaining_qty(){
        return this.remaining_qty ;
    }

    /**
     * 设置 [剩余数量]
     */
    @JsonProperty("remaining_qty")
    public void setRemaining_qty(Double  remaining_qty){
        this.remaining_qty = remaining_qty ;
        this.remaining_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [剩余数量]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_qtyDirtyFlag(){
        return this.remaining_qtyDirtyFlag ;
    }   

    /**
     * 获取 [剩余价值]
     */
    @JsonProperty("remaining_value")
    public Double getRemaining_value(){
        return this.remaining_value ;
    }

    /**
     * 设置 [剩余价值]
     */
    @JsonProperty("remaining_value")
    public void setRemaining_value(Double  remaining_value){
        this.remaining_value = remaining_value ;
        this.remaining_valueDirtyFlag = true ;
    }

     /**
     * 获取 [剩余价值]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_valueDirtyFlag(){
        return this.remaining_valueDirtyFlag ;
    }   

    /**
     * 获取 [维修]
     */
    @JsonProperty("repair_id")
    public Integer getRepair_id(){
        return this.repair_id ;
    }

    /**
     * 设置 [维修]
     */
    @JsonProperty("repair_id")
    public void setRepair_id(Integer  repair_id){
        this.repair_id = repair_id ;
        this.repair_idDirtyFlag = true ;
    }

     /**
     * 获取 [维修]脏标记
     */
    @JsonIgnore
    public boolean getRepair_idDirtyFlag(){
        return this.repair_idDirtyFlag ;
    }   

    /**
     * 获取 [维修]
     */
    @JsonProperty("repair_id_text")
    public String getRepair_id_text(){
        return this.repair_id_text ;
    }

    /**
     * 设置 [维修]
     */
    @JsonProperty("repair_id_text")
    public void setRepair_id_text(String  repair_id_text){
        this.repair_id_text = repair_id_text ;
        this.repair_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [维修]脏标记
     */
    @JsonIgnore
    public boolean getRepair_id_textDirtyFlag(){
        return this.repair_id_textDirtyFlag ;
    }   

    /**
     * 获取 [已预留数量]
     */
    @JsonProperty("reserved_availability")
    public Double getReserved_availability(){
        return this.reserved_availability ;
    }

    /**
     * 设置 [已预留数量]
     */
    @JsonProperty("reserved_availability")
    public void setReserved_availability(Double  reserved_availability){
        this.reserved_availability = reserved_availability ;
        this.reserved_availabilityDirtyFlag = true ;
    }

     /**
     * 获取 [已预留数量]脏标记
     */
    @JsonIgnore
    public boolean getReserved_availabilityDirtyFlag(){
        return this.reserved_availabilityDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("restrict_partner_id")
    public Integer getRestrict_partner_id(){
        return this.restrict_partner_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("restrict_partner_id")
    public void setRestrict_partner_id(Integer  restrict_partner_id){
        this.restrict_partner_id = restrict_partner_id ;
        this.restrict_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getRestrict_partner_idDirtyFlag(){
        return this.restrict_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("restrict_partner_id_text")
    public String getRestrict_partner_id_text(){
        return this.restrict_partner_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("restrict_partner_id_text")
    public void setRestrict_partner_id_text(String  restrict_partner_id_text){
        this.restrict_partner_id_text = restrict_partner_id_text ;
        this.restrict_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getRestrict_partner_id_textDirtyFlag(){
        return this.restrict_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [全部退回移动]
     */
    @JsonProperty("returned_move_ids")
    public String getReturned_move_ids(){
        return this.returned_move_ids ;
    }

    /**
     * 设置 [全部退回移动]
     */
    @JsonProperty("returned_move_ids")
    public void setReturned_move_ids(String  returned_move_ids){
        this.returned_move_ids = returned_move_ids ;
        this.returned_move_idsDirtyFlag = true ;
    }

     /**
     * 获取 [全部退回移动]脏标记
     */
    @JsonIgnore
    public boolean getReturned_move_idsDirtyFlag(){
        return this.returned_move_idsDirtyFlag ;
    }   

    /**
     * 获取 [目的路线]
     */
    @JsonProperty("route_ids")
    public String getRoute_ids(){
        return this.route_ids ;
    }

    /**
     * 设置 [目的路线]
     */
    @JsonProperty("route_ids")
    public void setRoute_ids(String  route_ids){
        this.route_ids = route_ids ;
        this.route_idsDirtyFlag = true ;
    }

     /**
     * 获取 [目的路线]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idsDirtyFlag(){
        return this.route_idsDirtyFlag ;
    }   

    /**
     * 获取 [库存规则]
     */
    @JsonProperty("rule_id")
    public Integer getRule_id(){
        return this.rule_id ;
    }

    /**
     * 设置 [库存规则]
     */
    @JsonProperty("rule_id")
    public void setRule_id(Integer  rule_id){
        this.rule_id = rule_id ;
        this.rule_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存规则]脏标记
     */
    @JsonIgnore
    public boolean getRule_idDirtyFlag(){
        return this.rule_idDirtyFlag ;
    }   

    /**
     * 获取 [库存规则]
     */
    @JsonProperty("rule_id_text")
    public String getRule_id_text(){
        return this.rule_id_text ;
    }

    /**
     * 设置 [库存规则]
     */
    @JsonProperty("rule_id_text")
    public void setRule_id_text(String  rule_id_text){
        this.rule_id_text = rule_id_text ;
        this.rule_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存规则]脏标记
     */
    @JsonIgnore
    public boolean getRule_id_textDirtyFlag(){
        return this.rule_id_textDirtyFlag ;
    }   

    /**
     * 获取 [销售明细行]
     */
    @JsonProperty("sale_line_id")
    public Integer getSale_line_id(){
        return this.sale_line_id ;
    }

    /**
     * 设置 [销售明细行]
     */
    @JsonProperty("sale_line_id")
    public void setSale_line_id(Integer  sale_line_id){
        this.sale_line_id = sale_line_id ;
        this.sale_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售明细行]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_idDirtyFlag(){
        return this.sale_line_idDirtyFlag ;
    }   

    /**
     * 获取 [销售明细行]
     */
    @JsonProperty("sale_line_id_text")
    public String getSale_line_id_text(){
        return this.sale_line_id_text ;
    }

    /**
     * 设置 [销售明细行]
     */
    @JsonProperty("sale_line_id_text")
    public void setSale_line_id_text(String  sale_line_id_text){
        this.sale_line_id_text = sale_line_id_text ;
        this.sale_line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售明细行]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_id_textDirtyFlag(){
        return this.sale_line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [已报废]
     */
    @JsonProperty("scrapped")
    public String getScrapped(){
        return this.scrapped ;
    }

    /**
     * 设置 [已报废]
     */
    @JsonProperty("scrapped")
    public void setScrapped(String  scrapped){
        this.scrapped = scrapped ;
        this.scrappedDirtyFlag = true ;
    }

     /**
     * 获取 [已报废]脏标记
     */
    @JsonIgnore
    public boolean getScrappedDirtyFlag(){
        return this.scrappedDirtyFlag ;
    }   

    /**
     * 获取 [报废]
     */
    @JsonProperty("scrap_ids")
    public String getScrap_ids(){
        return this.scrap_ids ;
    }

    /**
     * 设置 [报废]
     */
    @JsonProperty("scrap_ids")
    public void setScrap_ids(String  scrap_ids){
        this.scrap_ids = scrap_ids ;
        this.scrap_idsDirtyFlag = true ;
    }

     /**
     * 获取 [报废]脏标记
     */
    @JsonIgnore
    public boolean getScrap_idsDirtyFlag(){
        return this.scrap_idsDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [详情可见]
     */
    @JsonProperty("show_details_visible")
    public String getShow_details_visible(){
        return this.show_details_visible ;
    }

    /**
     * 设置 [详情可见]
     */
    @JsonProperty("show_details_visible")
    public void setShow_details_visible(String  show_details_visible){
        this.show_details_visible = show_details_visible ;
        this.show_details_visibleDirtyFlag = true ;
    }

     /**
     * 获取 [详情可见]脏标记
     */
    @JsonIgnore
    public boolean getShow_details_visibleDirtyFlag(){
        return this.show_details_visibleDirtyFlag ;
    }   

    /**
     * 获取 [显示详细作业]
     */
    @JsonProperty("show_operations")
    public String getShow_operations(){
        return this.show_operations ;
    }

    /**
     * 设置 [显示详细作业]
     */
    @JsonProperty("show_operations")
    public void setShow_operations(String  show_operations){
        this.show_operations = show_operations ;
        this.show_operationsDirtyFlag = true ;
    }

     /**
     * 获取 [显示详细作业]脏标记
     */
    @JsonIgnore
    public boolean getShow_operationsDirtyFlag(){
        return this.show_operationsDirtyFlag ;
    }   

    /**
     * 获取 [从供应商]
     */
    @JsonProperty("show_reserved_availability")
    public String getShow_reserved_availability(){
        return this.show_reserved_availability ;
    }

    /**
     * 设置 [从供应商]
     */
    @JsonProperty("show_reserved_availability")
    public void setShow_reserved_availability(String  show_reserved_availability){
        this.show_reserved_availability = show_reserved_availability ;
        this.show_reserved_availabilityDirtyFlag = true ;
    }

     /**
     * 获取 [从供应商]脏标记
     */
    @JsonIgnore
    public boolean getShow_reserved_availabilityDirtyFlag(){
        return this.show_reserved_availabilityDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [可用量]
     */
    @JsonProperty("string_availability_info")
    public String getString_availability_info(){
        return this.string_availability_info ;
    }

    /**
     * 设置 [可用量]
     */
    @JsonProperty("string_availability_info")
    public void setString_availability_info(String  string_availability_info){
        this.string_availability_info = string_availability_info ;
        this.string_availability_infoDirtyFlag = true ;
    }

     /**
     * 获取 [可用量]脏标记
     */
    @JsonIgnore
    public boolean getString_availability_infoDirtyFlag(){
        return this.string_availability_infoDirtyFlag ;
    }   

    /**
     * 获取 [退款 (更新 SO/PO)]
     */
    @JsonProperty("to_refund")
    public String getTo_refund(){
        return this.to_refund ;
    }

    /**
     * 设置 [退款 (更新 SO/PO)]
     */
    @JsonProperty("to_refund")
    public void setTo_refund(String  to_refund){
        this.to_refund = to_refund ;
        this.to_refundDirtyFlag = true ;
    }

     /**
     * 获取 [退款 (更新 SO/PO)]脏标记
     */
    @JsonIgnore
    public boolean getTo_refundDirtyFlag(){
        return this.to_refundDirtyFlag ;
    }   

    /**
     * 获取 [拆卸顺序]
     */
    @JsonProperty("unbuild_id")
    public Integer getUnbuild_id(){
        return this.unbuild_id ;
    }

    /**
     * 设置 [拆卸顺序]
     */
    @JsonProperty("unbuild_id")
    public void setUnbuild_id(Integer  unbuild_id){
        this.unbuild_id = unbuild_id ;
        this.unbuild_idDirtyFlag = true ;
    }

     /**
     * 获取 [拆卸顺序]脏标记
     */
    @JsonIgnore
    public boolean getUnbuild_idDirtyFlag(){
        return this.unbuild_idDirtyFlag ;
    }   

    /**
     * 获取 [拆卸顺序]
     */
    @JsonProperty("unbuild_id_text")
    public String getUnbuild_id_text(){
        return this.unbuild_id_text ;
    }

    /**
     * 设置 [拆卸顺序]
     */
    @JsonProperty("unbuild_id_text")
    public void setUnbuild_id_text(String  unbuild_id_text){
        this.unbuild_id_text = unbuild_id_text ;
        this.unbuild_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [拆卸顺序]脏标记
     */
    @JsonIgnore
    public boolean getUnbuild_id_textDirtyFlag(){
        return this.unbuild_id_textDirtyFlag ;
    }   

    /**
     * 获取 [单位因子]
     */
    @JsonProperty("unit_factor")
    public Double getUnit_factor(){
        return this.unit_factor ;
    }

    /**
     * 设置 [单位因子]
     */
    @JsonProperty("unit_factor")
    public void setUnit_factor(Double  unit_factor){
        this.unit_factor = unit_factor ;
        this.unit_factorDirtyFlag = true ;
    }

     /**
     * 获取 [单位因子]脏标记
     */
    @JsonIgnore
    public boolean getUnit_factorDirtyFlag(){
        return this.unit_factorDirtyFlag ;
    }   

    /**
     * 获取 [值]
     */
    @JsonProperty("value")
    public Double getValue(){
        return this.value ;
    }

    /**
     * 设置 [值]
     */
    @JsonProperty("value")
    public void setValue(Double  value){
        this.value = value ;
        this.valueDirtyFlag = true ;
    }

     /**
     * 获取 [值]脏标记
     */
    @JsonIgnore
    public boolean getValueDirtyFlag(){
        return this.valueDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return this.warehouse_id ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return this.warehouse_idDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return this.warehouse_id_text ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return this.warehouse_id_textDirtyFlag ;
    }   

    /**
     * 获取 [待消耗的工单]
     */
    @JsonProperty("workorder_id")
    public Integer getWorkorder_id(){
        return this.workorder_id ;
    }

    /**
     * 设置 [待消耗的工单]
     */
    @JsonProperty("workorder_id")
    public void setWorkorder_id(Integer  workorder_id){
        this.workorder_id = workorder_id ;
        this.workorder_idDirtyFlag = true ;
    }

     /**
     * 获取 [待消耗的工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idDirtyFlag(){
        return this.workorder_idDirtyFlag ;
    }   

    /**
     * 获取 [待消耗的工单]
     */
    @JsonProperty("workorder_id_text")
    public String getWorkorder_id_text(){
        return this.workorder_id_text ;
    }

    /**
     * 设置 [待消耗的工单]
     */
    @JsonProperty("workorder_id_text")
    public void setWorkorder_id_text(String  workorder_id_text){
        this.workorder_id_text = workorder_id_text ;
        this.workorder_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [待消耗的工单]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_id_textDirtyFlag(){
        return this.workorder_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("account_move_ids") instanceof Boolean)&& map.get("account_move_ids")!=null){
			Object[] objs = (Object[])map.get("account_move_ids");
			if(objs.length > 0){
				Integer[] account_move_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAccount_move_ids(Arrays.toString(account_move_ids).replace(" ",""));
			}
		}
		if(!(map.get("active_move_line_ids") instanceof Boolean)&& map.get("active_move_line_ids")!=null){
			Object[] objs = (Object[])map.get("active_move_line_ids");
			if(objs.length > 0){
				Integer[] active_move_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setActive_move_line_ids(Arrays.toString(active_move_line_ids).replace(" ",""));
			}
		}
		if(map.get("additional") instanceof Boolean){
			this.setAdditional(((Boolean)map.get("additional"))? "true" : "false");
		}
		if(!(map.get("availability") instanceof Boolean)&& map.get("availability")!=null){
			this.setAvailability((Double)map.get("availability"));
		}
		if(!(map.get("backorder_id") instanceof Boolean)&& map.get("backorder_id")!=null){
			Object[] objs = (Object[])map.get("backorder_id");
			if(objs.length > 0){
				this.setBackorder_id((Integer)objs[0]);
			}
		}
		if(!(map.get("bom_line_id") instanceof Boolean)&& map.get("bom_line_id")!=null){
			Object[] objs = (Object[])map.get("bom_line_id");
			if(objs.length > 0){
				this.setBom_line_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("consume_unbuild_id") instanceof Boolean)&& map.get("consume_unbuild_id")!=null){
			Object[] objs = (Object[])map.get("consume_unbuild_id");
			if(objs.length > 0){
				this.setConsume_unbuild_id((Integer)objs[0]);
			}
		}
		if(!(map.get("consume_unbuild_id") instanceof Boolean)&& map.get("consume_unbuild_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("consume_unbuild_id");
			if(objs.length > 1){
				this.setConsume_unbuild_id_text((String)objs[1]);
			}
		}
		if(!(map.get("created_production_id") instanceof Boolean)&& map.get("created_production_id")!=null){
			Object[] objs = (Object[])map.get("created_production_id");
			if(objs.length > 0){
				this.setCreated_production_id((Integer)objs[0]);
			}
		}
		if(!(map.get("created_production_id") instanceof Boolean)&& map.get("created_production_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("created_production_id");
			if(objs.length > 1){
				this.setCreated_production_id_text((String)objs[1]);
			}
		}
		if(!(map.get("created_purchase_line_id") instanceof Boolean)&& map.get("created_purchase_line_id")!=null){
			Object[] objs = (Object[])map.get("created_purchase_line_id");
			if(objs.length > 0){
				this.setCreated_purchase_line_id((Integer)objs[0]);
			}
		}
		if(!(map.get("created_purchase_line_id") instanceof Boolean)&& map.get("created_purchase_line_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("created_purchase_line_id");
			if(objs.length > 1){
				this.setCreated_purchase_line_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_expected") instanceof Boolean)&& map.get("date_expected")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_expected"));
   			this.setDate_expected(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("finished_lots_exist") instanceof Boolean){
			this.setFinished_lots_exist(((Boolean)map.get("finished_lots_exist"))? "true" : "false");
		}
		if(!(map.get("group_id") instanceof Boolean)&& map.get("group_id")!=null){
			Object[] objs = (Object[])map.get("group_id");
			if(objs.length > 0){
				this.setGroup_id((Integer)objs[0]);
			}
		}
		if(map.get("has_move_lines") instanceof Boolean){
			this.setHas_move_lines(((Boolean)map.get("has_move_lines"))? "true" : "false");
		}
		if(!(map.get("has_tracking") instanceof Boolean)&& map.get("has_tracking")!=null){
			this.setHas_tracking((String)map.get("has_tracking"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("inventory_id") instanceof Boolean)&& map.get("inventory_id")!=null){
			Object[] objs = (Object[])map.get("inventory_id");
			if(objs.length > 0){
				this.setInventory_id((Integer)objs[0]);
			}
		}
		if(!(map.get("inventory_id") instanceof Boolean)&& map.get("inventory_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("inventory_id");
			if(objs.length > 1){
				this.setInventory_id_text((String)objs[1]);
			}
		}
		if(map.get("is_done") instanceof Boolean){
			this.setIs_done(((Boolean)map.get("is_done"))? "true" : "false");
		}
		if(map.get("is_initial_demand_editable") instanceof Boolean){
			this.setIs_initial_demand_editable(((Boolean)map.get("is_initial_demand_editable"))? "true" : "false");
		}
		if(map.get("is_locked") instanceof Boolean){
			this.setIs_locked(((Boolean)map.get("is_locked"))? "true" : "false");
		}
		if(map.get("is_quantity_done_editable") instanceof Boolean){
			this.setIs_quantity_done_editable(((Boolean)map.get("is_quantity_done_editable"))? "true" : "false");
		}
		if(!(map.get("location_dest_id") instanceof Boolean)&& map.get("location_dest_id")!=null){
			Object[] objs = (Object[])map.get("location_dest_id");
			if(objs.length > 0){
				this.setLocation_dest_id((Integer)objs[0]);
			}
		}
		if(!(map.get("location_dest_id") instanceof Boolean)&& map.get("location_dest_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("location_dest_id");
			if(objs.length > 1){
				this.setLocation_dest_id_text((String)objs[1]);
			}
		}
		if(!(map.get("location_id") instanceof Boolean)&& map.get("location_id")!=null){
			Object[] objs = (Object[])map.get("location_id");
			if(objs.length > 0){
				this.setLocation_id((Integer)objs[0]);
			}
		}
		if(!(map.get("location_id") instanceof Boolean)&& map.get("location_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("location_id");
			if(objs.length > 1){
				this.setLocation_id_text((String)objs[1]);
			}
		}
		if(!(map.get("move_dest_ids") instanceof Boolean)&& map.get("move_dest_ids")!=null){
			Object[] objs = (Object[])map.get("move_dest_ids");
			if(objs.length > 0){
				Integer[] move_dest_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_dest_ids(Arrays.toString(move_dest_ids).replace(" ",""));
			}
		}
		if(!(map.get("move_line_ids") instanceof Boolean)&& map.get("move_line_ids")!=null){
			Object[] objs = (Object[])map.get("move_line_ids");
			if(objs.length > 0){
				Integer[] move_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_line_ids(Arrays.toString(move_line_ids).replace(" ",""));
			}
		}
		if(!(map.get("move_line_nosuggest_ids") instanceof Boolean)&& map.get("move_line_nosuggest_ids")!=null){
			Object[] objs = (Object[])map.get("move_line_nosuggest_ids");
			if(objs.length > 0){
				Integer[] move_line_nosuggest_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_line_nosuggest_ids(Arrays.toString(move_line_nosuggest_ids).replace(" ",""));
			}
		}
		if(!(map.get("move_orig_ids") instanceof Boolean)&& map.get("move_orig_ids")!=null){
			Object[] objs = (Object[])map.get("move_orig_ids");
			if(objs.length > 0){
				Integer[] move_orig_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_orig_ids(Arrays.toString(move_orig_ids).replace(" ",""));
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(map.get("needs_lots") instanceof Boolean){
			this.setNeeds_lots(((Boolean)map.get("needs_lots"))? "true" : "false");
		}
		if(!(map.get("note") instanceof Boolean)&& map.get("note")!=null){
			this.setNote((String)map.get("note"));
		}
		if(!(map.get("operation_id") instanceof Boolean)&& map.get("operation_id")!=null){
			Object[] objs = (Object[])map.get("operation_id");
			if(objs.length > 0){
				this.setOperation_id((Integer)objs[0]);
			}
		}
		if(!(map.get("operation_id") instanceof Boolean)&& map.get("operation_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("operation_id");
			if(objs.length > 1){
				this.setOperation_id_text((String)objs[1]);
			}
		}
		if(!(map.get("order_finished_lot_ids") instanceof Boolean)&& map.get("order_finished_lot_ids")!=null){
			Object[] objs = (Object[])map.get("order_finished_lot_ids");
			if(objs.length > 0){
				Integer[] order_finished_lot_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setOrder_finished_lot_ids(Arrays.toString(order_finished_lot_ids).replace(" ",""));
			}
		}
		if(!(map.get("origin") instanceof Boolean)&& map.get("origin")!=null){
			this.setOrigin((String)map.get("origin"));
		}
		if(!(map.get("origin_returned_move_id") instanceof Boolean)&& map.get("origin_returned_move_id")!=null){
			Object[] objs = (Object[])map.get("origin_returned_move_id");
			if(objs.length > 0){
				this.setOrigin_returned_move_id((Integer)objs[0]);
			}
		}
		if(!(map.get("origin_returned_move_id") instanceof Boolean)&& map.get("origin_returned_move_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("origin_returned_move_id");
			if(objs.length > 1){
				this.setOrigin_returned_move_id_text((String)objs[1]);
			}
		}
		if(!(map.get("package_level_id") instanceof Boolean)&& map.get("package_level_id")!=null){
			Object[] objs = (Object[])map.get("package_level_id");
			if(objs.length > 0){
				this.setPackage_level_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("picking_code") instanceof Boolean)&& map.get("picking_code")!=null){
			this.setPicking_code((String)map.get("picking_code"));
		}
		if(!(map.get("picking_id") instanceof Boolean)&& map.get("picking_id")!=null){
			Object[] objs = (Object[])map.get("picking_id");
			if(objs.length > 0){
				this.setPicking_id((Integer)objs[0]);
			}
		}
		if(!(map.get("picking_id") instanceof Boolean)&& map.get("picking_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("picking_id");
			if(objs.length > 1){
				this.setPicking_id_text((String)objs[1]);
			}
		}
		if(!(map.get("picking_partner_id") instanceof Boolean)&& map.get("picking_partner_id")!=null){
			Object[] objs = (Object[])map.get("picking_partner_id");
			if(objs.length > 0){
				this.setPicking_partner_id((Integer)objs[0]);
			}
		}
		if(map.get("picking_type_entire_packs") instanceof Boolean){
			this.setPicking_type_entire_packs(((Boolean)map.get("picking_type_entire_packs"))? "true" : "false");
		}
		if(!(map.get("picking_type_id") instanceof Boolean)&& map.get("picking_type_id")!=null){
			Object[] objs = (Object[])map.get("picking_type_id");
			if(objs.length > 0){
				this.setPicking_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("picking_type_id") instanceof Boolean)&& map.get("picking_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("picking_type_id");
			if(objs.length > 1){
				this.setPicking_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("price_unit") instanceof Boolean)&& map.get("price_unit")!=null){
			this.setPrice_unit((Double)map.get("price_unit"));
		}
		if(!(map.get("priority") instanceof Boolean)&& map.get("priority")!=null){
			this.setPriority((String)map.get("priority"));
		}
		if(!(map.get("procure_method") instanceof Boolean)&& map.get("procure_method")!=null){
			this.setProcure_method((String)map.get("procure_method"));
		}
		if(!(map.get("production_id") instanceof Boolean)&& map.get("production_id")!=null){
			Object[] objs = (Object[])map.get("production_id");
			if(objs.length > 0){
				this.setProduction_id((Integer)objs[0]);
			}
		}
		if(!(map.get("production_id") instanceof Boolean)&& map.get("production_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("production_id");
			if(objs.length > 1){
				this.setProduction_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_packaging") instanceof Boolean)&& map.get("product_packaging")!=null){
			Object[] objs = (Object[])map.get("product_packaging");
			if(objs.length > 0){
				this.setProduct_packaging((Integer)objs[0]);
			}
		}
		if(!(map.get("product_packaging") instanceof Boolean)&& map.get("product_packaging")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_packaging");
			if(objs.length > 1){
				this.setProduct_packaging_text((String)objs[1]);
			}
		}
		if(!(map.get("product_qty") instanceof Boolean)&& map.get("product_qty")!=null){
			this.setProduct_qty((Double)map.get("product_qty"));
		}
		if(!(map.get("product_tmpl_id") instanceof Boolean)&& map.get("product_tmpl_id")!=null){
			Object[] objs = (Object[])map.get("product_tmpl_id");
			if(objs.length > 0){
				this.setProduct_tmpl_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_type") instanceof Boolean)&& map.get("product_type")!=null){
			this.setProduct_type((String)map.get("product_type"));
		}
		if(!(map.get("product_uom") instanceof Boolean)&& map.get("product_uom")!=null){
			Object[] objs = (Object[])map.get("product_uom");
			if(objs.length > 0){
				this.setProduct_uom((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_qty") instanceof Boolean)&& map.get("product_uom_qty")!=null){
			this.setProduct_uom_qty((Double)map.get("product_uom_qty"));
		}
		if(!(map.get("product_uom") instanceof Boolean)&& map.get("product_uom")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_uom");
			if(objs.length > 1){
				this.setProduct_uom_text((String)objs[1]);
			}
		}
		if(map.get("propagate") instanceof Boolean){
			this.setPropagate(((Boolean)map.get("propagate"))? "true" : "false");
		}
		if(!(map.get("purchase_line_id") instanceof Boolean)&& map.get("purchase_line_id")!=null){
			Object[] objs = (Object[])map.get("purchase_line_id");
			if(objs.length > 0){
				this.setPurchase_line_id((Integer)objs[0]);
			}
		}
		if(!(map.get("purchase_line_id") instanceof Boolean)&& map.get("purchase_line_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("purchase_line_id");
			if(objs.length > 1){
				this.setPurchase_line_id_text((String)objs[1]);
			}
		}
		if(!(map.get("quantity_done") instanceof Boolean)&& map.get("quantity_done")!=null){
			this.setQuantity_done((Double)map.get("quantity_done"));
		}
		if(!(map.get("raw_material_production_id") instanceof Boolean)&& map.get("raw_material_production_id")!=null){
			Object[] objs = (Object[])map.get("raw_material_production_id");
			if(objs.length > 0){
				this.setRaw_material_production_id((Integer)objs[0]);
			}
		}
		if(!(map.get("raw_material_production_id") instanceof Boolean)&& map.get("raw_material_production_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("raw_material_production_id");
			if(objs.length > 1){
				this.setRaw_material_production_id_text((String)objs[1]);
			}
		}
		if(!(map.get("reference") instanceof Boolean)&& map.get("reference")!=null){
			this.setReference((String)map.get("reference"));
		}
		if(!(map.get("remaining_qty") instanceof Boolean)&& map.get("remaining_qty")!=null){
			this.setRemaining_qty((Double)map.get("remaining_qty"));
		}
		if(!(map.get("remaining_value") instanceof Boolean)&& map.get("remaining_value")!=null){
			this.setRemaining_value((Double)map.get("remaining_value"));
		}
		if(!(map.get("repair_id") instanceof Boolean)&& map.get("repair_id")!=null){
			Object[] objs = (Object[])map.get("repair_id");
			if(objs.length > 0){
				this.setRepair_id((Integer)objs[0]);
			}
		}
		if(!(map.get("repair_id") instanceof Boolean)&& map.get("repair_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("repair_id");
			if(objs.length > 1){
				this.setRepair_id_text((String)objs[1]);
			}
		}
		if(!(map.get("reserved_availability") instanceof Boolean)&& map.get("reserved_availability")!=null){
			this.setReserved_availability((Double)map.get("reserved_availability"));
		}
		if(!(map.get("restrict_partner_id") instanceof Boolean)&& map.get("restrict_partner_id")!=null){
			Object[] objs = (Object[])map.get("restrict_partner_id");
			if(objs.length > 0){
				this.setRestrict_partner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("restrict_partner_id") instanceof Boolean)&& map.get("restrict_partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("restrict_partner_id");
			if(objs.length > 1){
				this.setRestrict_partner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("returned_move_ids") instanceof Boolean)&& map.get("returned_move_ids")!=null){
			Object[] objs = (Object[])map.get("returned_move_ids");
			if(objs.length > 0){
				Integer[] returned_move_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setReturned_move_ids(Arrays.toString(returned_move_ids).replace(" ",""));
			}
		}
		if(!(map.get("route_ids") instanceof Boolean)&& map.get("route_ids")!=null){
			Object[] objs = (Object[])map.get("route_ids");
			if(objs.length > 0){
				Integer[] route_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRoute_ids(Arrays.toString(route_ids).replace(" ",""));
			}
		}
		if(!(map.get("rule_id") instanceof Boolean)&& map.get("rule_id")!=null){
			Object[] objs = (Object[])map.get("rule_id");
			if(objs.length > 0){
				this.setRule_id((Integer)objs[0]);
			}
		}
		if(!(map.get("rule_id") instanceof Boolean)&& map.get("rule_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("rule_id");
			if(objs.length > 1){
				this.setRule_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sale_line_id") instanceof Boolean)&& map.get("sale_line_id")!=null){
			Object[] objs = (Object[])map.get("sale_line_id");
			if(objs.length > 0){
				this.setSale_line_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sale_line_id") instanceof Boolean)&& map.get("sale_line_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("sale_line_id");
			if(objs.length > 1){
				this.setSale_line_id_text((String)objs[1]);
			}
		}
		if(map.get("scrapped") instanceof Boolean){
			this.setScrapped(((Boolean)map.get("scrapped"))? "true" : "false");
		}
		if(!(map.get("scrap_ids") instanceof Boolean)&& map.get("scrap_ids")!=null){
			Object[] objs = (Object[])map.get("scrap_ids");
			if(objs.length > 0){
				Integer[] scrap_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setScrap_ids(Arrays.toString(scrap_ids).replace(" ",""));
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(map.get("show_details_visible") instanceof Boolean){
			this.setShow_details_visible(((Boolean)map.get("show_details_visible"))? "true" : "false");
		}
		if(map.get("show_operations") instanceof Boolean){
			this.setShow_operations(((Boolean)map.get("show_operations"))? "true" : "false");
		}
		if(map.get("show_reserved_availability") instanceof Boolean){
			this.setShow_reserved_availability(((Boolean)map.get("show_reserved_availability"))? "true" : "false");
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("string_availability_info") instanceof Boolean)&& map.get("string_availability_info")!=null){
			this.setString_availability_info((String)map.get("string_availability_info"));
		}
		if(map.get("to_refund") instanceof Boolean){
			this.setTo_refund(((Boolean)map.get("to_refund"))? "true" : "false");
		}
		if(!(map.get("unbuild_id") instanceof Boolean)&& map.get("unbuild_id")!=null){
			Object[] objs = (Object[])map.get("unbuild_id");
			if(objs.length > 0){
				this.setUnbuild_id((Integer)objs[0]);
			}
		}
		if(!(map.get("unbuild_id") instanceof Boolean)&& map.get("unbuild_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("unbuild_id");
			if(objs.length > 1){
				this.setUnbuild_id_text((String)objs[1]);
			}
		}
		if(!(map.get("unit_factor") instanceof Boolean)&& map.get("unit_factor")!=null){
			this.setUnit_factor((Double)map.get("unit_factor"));
		}
		if(!(map.get("value") instanceof Boolean)&& map.get("value")!=null){
			this.setValue((Double)map.get("value"));
		}
		if(!(map.get("warehouse_id") instanceof Boolean)&& map.get("warehouse_id")!=null){
			Object[] objs = (Object[])map.get("warehouse_id");
			if(objs.length > 0){
				this.setWarehouse_id((Integer)objs[0]);
			}
		}
		if(!(map.get("warehouse_id") instanceof Boolean)&& map.get("warehouse_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("warehouse_id");
			if(objs.length > 1){
				this.setWarehouse_id_text((String)objs[1]);
			}
		}
		if(!(map.get("workorder_id") instanceof Boolean)&& map.get("workorder_id")!=null){
			Object[] objs = (Object[])map.get("workorder_id");
			if(objs.length > 0){
				this.setWorkorder_id((Integer)objs[0]);
			}
		}
		if(!(map.get("workorder_id") instanceof Boolean)&& map.get("workorder_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("workorder_id");
			if(objs.length > 1){
				this.setWorkorder_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccount_move_ids()!=null&&this.getAccount_move_idsDirtyFlag()){
			map.put("account_move_ids",this.getAccount_move_ids());
		}else if(this.getAccount_move_idsDirtyFlag()){
			map.put("account_move_ids",false);
		}
		if(this.getActive_move_line_ids()!=null&&this.getActive_move_line_idsDirtyFlag()){
			map.put("active_move_line_ids",this.getActive_move_line_ids());
		}else if(this.getActive_move_line_idsDirtyFlag()){
			map.put("active_move_line_ids",false);
		}
		if(this.getAdditional()!=null&&this.getAdditionalDirtyFlag()){
			map.put("additional",Boolean.parseBoolean(this.getAdditional()));		
		}		if(this.getAvailability()!=null&&this.getAvailabilityDirtyFlag()){
			map.put("availability",this.getAvailability());
		}else if(this.getAvailabilityDirtyFlag()){
			map.put("availability",false);
		}
		if(this.getBackorder_id()!=null&&this.getBackorder_idDirtyFlag()){
			map.put("backorder_id",this.getBackorder_id());
		}else if(this.getBackorder_idDirtyFlag()){
			map.put("backorder_id",false);
		}
		if(this.getBom_line_id()!=null&&this.getBom_line_idDirtyFlag()){
			map.put("bom_line_id",this.getBom_line_id());
		}else if(this.getBom_line_idDirtyFlag()){
			map.put("bom_line_id",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getConsume_unbuild_id()!=null&&this.getConsume_unbuild_idDirtyFlag()){
			map.put("consume_unbuild_id",this.getConsume_unbuild_id());
		}else if(this.getConsume_unbuild_idDirtyFlag()){
			map.put("consume_unbuild_id",false);
		}
		if(this.getConsume_unbuild_id_text()!=null&&this.getConsume_unbuild_id_textDirtyFlag()){
			//忽略文本外键consume_unbuild_id_text
		}else if(this.getConsume_unbuild_id_textDirtyFlag()){
			map.put("consume_unbuild_id",false);
		}
		if(this.getCreated_production_id()!=null&&this.getCreated_production_idDirtyFlag()){
			map.put("created_production_id",this.getCreated_production_id());
		}else if(this.getCreated_production_idDirtyFlag()){
			map.put("created_production_id",false);
		}
		if(this.getCreated_production_id_text()!=null&&this.getCreated_production_id_textDirtyFlag()){
			//忽略文本外键created_production_id_text
		}else if(this.getCreated_production_id_textDirtyFlag()){
			map.put("created_production_id",false);
		}
		if(this.getCreated_purchase_line_id()!=null&&this.getCreated_purchase_line_idDirtyFlag()){
			map.put("created_purchase_line_id",this.getCreated_purchase_line_id());
		}else if(this.getCreated_purchase_line_idDirtyFlag()){
			map.put("created_purchase_line_id",false);
		}
		if(this.getCreated_purchase_line_id_text()!=null&&this.getCreated_purchase_line_id_textDirtyFlag()){
			//忽略文本外键created_purchase_line_id_text
		}else if(this.getCreated_purchase_line_id_textDirtyFlag()){
			map.put("created_purchase_line_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDate_expected()!=null&&this.getDate_expectedDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_expected());
			map.put("date_expected",datetimeStr);
		}else if(this.getDate_expectedDirtyFlag()){
			map.put("date_expected",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFinished_lots_exist()!=null&&this.getFinished_lots_existDirtyFlag()){
			map.put("finished_lots_exist",Boolean.parseBoolean(this.getFinished_lots_exist()));		
		}		if(this.getGroup_id()!=null&&this.getGroup_idDirtyFlag()){
			map.put("group_id",this.getGroup_id());
		}else if(this.getGroup_idDirtyFlag()){
			map.put("group_id",false);
		}
		if(this.getHas_move_lines()!=null&&this.getHas_move_linesDirtyFlag()){
			map.put("has_move_lines",Boolean.parseBoolean(this.getHas_move_lines()));		
		}		if(this.getHas_tracking()!=null&&this.getHas_trackingDirtyFlag()){
			map.put("has_tracking",this.getHas_tracking());
		}else if(this.getHas_trackingDirtyFlag()){
			map.put("has_tracking",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInventory_id()!=null&&this.getInventory_idDirtyFlag()){
			map.put("inventory_id",this.getInventory_id());
		}else if(this.getInventory_idDirtyFlag()){
			map.put("inventory_id",false);
		}
		if(this.getInventory_id_text()!=null&&this.getInventory_id_textDirtyFlag()){
			//忽略文本外键inventory_id_text
		}else if(this.getInventory_id_textDirtyFlag()){
			map.put("inventory_id",false);
		}
		if(this.getIs_done()!=null&&this.getIs_doneDirtyFlag()){
			map.put("is_done",Boolean.parseBoolean(this.getIs_done()));		
		}		if(this.getIs_initial_demand_editable()!=null&&this.getIs_initial_demand_editableDirtyFlag()){
			map.put("is_initial_demand_editable",Boolean.parseBoolean(this.getIs_initial_demand_editable()));		
		}		if(this.getIs_locked()!=null&&this.getIs_lockedDirtyFlag()){
			map.put("is_locked",Boolean.parseBoolean(this.getIs_locked()));		
		}		if(this.getIs_quantity_done_editable()!=null&&this.getIs_quantity_done_editableDirtyFlag()){
			map.put("is_quantity_done_editable",Boolean.parseBoolean(this.getIs_quantity_done_editable()));		
		}		if(this.getLocation_dest_id()!=null&&this.getLocation_dest_idDirtyFlag()){
			map.put("location_dest_id",this.getLocation_dest_id());
		}else if(this.getLocation_dest_idDirtyFlag()){
			map.put("location_dest_id",false);
		}
		if(this.getLocation_dest_id_text()!=null&&this.getLocation_dest_id_textDirtyFlag()){
			//忽略文本外键location_dest_id_text
		}else if(this.getLocation_dest_id_textDirtyFlag()){
			map.put("location_dest_id",false);
		}
		if(this.getLocation_id()!=null&&this.getLocation_idDirtyFlag()){
			map.put("location_id",this.getLocation_id());
		}else if(this.getLocation_idDirtyFlag()){
			map.put("location_id",false);
		}
		if(this.getLocation_id_text()!=null&&this.getLocation_id_textDirtyFlag()){
			//忽略文本外键location_id_text
		}else if(this.getLocation_id_textDirtyFlag()){
			map.put("location_id",false);
		}
		if(this.getMove_dest_ids()!=null&&this.getMove_dest_idsDirtyFlag()){
			map.put("move_dest_ids",this.getMove_dest_ids());
		}else if(this.getMove_dest_idsDirtyFlag()){
			map.put("move_dest_ids",false);
		}
		if(this.getMove_line_ids()!=null&&this.getMove_line_idsDirtyFlag()){
			map.put("move_line_ids",this.getMove_line_ids());
		}else if(this.getMove_line_idsDirtyFlag()){
			map.put("move_line_ids",false);
		}
		if(this.getMove_line_nosuggest_ids()!=null&&this.getMove_line_nosuggest_idsDirtyFlag()){
			map.put("move_line_nosuggest_ids",this.getMove_line_nosuggest_ids());
		}else if(this.getMove_line_nosuggest_idsDirtyFlag()){
			map.put("move_line_nosuggest_ids",false);
		}
		if(this.getMove_orig_ids()!=null&&this.getMove_orig_idsDirtyFlag()){
			map.put("move_orig_ids",this.getMove_orig_ids());
		}else if(this.getMove_orig_idsDirtyFlag()){
			map.put("move_orig_ids",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNeeds_lots()!=null&&this.getNeeds_lotsDirtyFlag()){
			map.put("needs_lots",Boolean.parseBoolean(this.getNeeds_lots()));		
		}		if(this.getNote()!=null&&this.getNoteDirtyFlag()){
			map.put("note",this.getNote());
		}else if(this.getNoteDirtyFlag()){
			map.put("note",false);
		}
		if(this.getOperation_id()!=null&&this.getOperation_idDirtyFlag()){
			map.put("operation_id",this.getOperation_id());
		}else if(this.getOperation_idDirtyFlag()){
			map.put("operation_id",false);
		}
		if(this.getOperation_id_text()!=null&&this.getOperation_id_textDirtyFlag()){
			//忽略文本外键operation_id_text
		}else if(this.getOperation_id_textDirtyFlag()){
			map.put("operation_id",false);
		}
		if(this.getOrder_finished_lot_ids()!=null&&this.getOrder_finished_lot_idsDirtyFlag()){
			map.put("order_finished_lot_ids",this.getOrder_finished_lot_ids());
		}else if(this.getOrder_finished_lot_idsDirtyFlag()){
			map.put("order_finished_lot_ids",false);
		}
		if(this.getOrigin()!=null&&this.getOriginDirtyFlag()){
			map.put("origin",this.getOrigin());
		}else if(this.getOriginDirtyFlag()){
			map.put("origin",false);
		}
		if(this.getOrigin_returned_move_id()!=null&&this.getOrigin_returned_move_idDirtyFlag()){
			map.put("origin_returned_move_id",this.getOrigin_returned_move_id());
		}else if(this.getOrigin_returned_move_idDirtyFlag()){
			map.put("origin_returned_move_id",false);
		}
		if(this.getOrigin_returned_move_id_text()!=null&&this.getOrigin_returned_move_id_textDirtyFlag()){
			//忽略文本外键origin_returned_move_id_text
		}else if(this.getOrigin_returned_move_id_textDirtyFlag()){
			map.put("origin_returned_move_id",false);
		}
		if(this.getPackage_level_id()!=null&&this.getPackage_level_idDirtyFlag()){
			map.put("package_level_id",this.getPackage_level_id());
		}else if(this.getPackage_level_idDirtyFlag()){
			map.put("package_level_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPicking_code()!=null&&this.getPicking_codeDirtyFlag()){
			map.put("picking_code",this.getPicking_code());
		}else if(this.getPicking_codeDirtyFlag()){
			map.put("picking_code",false);
		}
		if(this.getPicking_id()!=null&&this.getPicking_idDirtyFlag()){
			map.put("picking_id",this.getPicking_id());
		}else if(this.getPicking_idDirtyFlag()){
			map.put("picking_id",false);
		}
		if(this.getPicking_id_text()!=null&&this.getPicking_id_textDirtyFlag()){
			//忽略文本外键picking_id_text
		}else if(this.getPicking_id_textDirtyFlag()){
			map.put("picking_id",false);
		}
		if(this.getPicking_partner_id()!=null&&this.getPicking_partner_idDirtyFlag()){
			map.put("picking_partner_id",this.getPicking_partner_id());
		}else if(this.getPicking_partner_idDirtyFlag()){
			map.put("picking_partner_id",false);
		}
		if(this.getPicking_type_entire_packs()!=null&&this.getPicking_type_entire_packsDirtyFlag()){
			map.put("picking_type_entire_packs",Boolean.parseBoolean(this.getPicking_type_entire_packs()));		
		}		if(this.getPicking_type_id()!=null&&this.getPicking_type_idDirtyFlag()){
			map.put("picking_type_id",this.getPicking_type_id());
		}else if(this.getPicking_type_idDirtyFlag()){
			map.put("picking_type_id",false);
		}
		if(this.getPicking_type_id_text()!=null&&this.getPicking_type_id_textDirtyFlag()){
			//忽略文本外键picking_type_id_text
		}else if(this.getPicking_type_id_textDirtyFlag()){
			map.put("picking_type_id",false);
		}
		if(this.getPrice_unit()!=null&&this.getPrice_unitDirtyFlag()){
			map.put("price_unit",this.getPrice_unit());
		}else if(this.getPrice_unitDirtyFlag()){
			map.put("price_unit",false);
		}
		if(this.getPriority()!=null&&this.getPriorityDirtyFlag()){
			map.put("priority",this.getPriority());
		}else if(this.getPriorityDirtyFlag()){
			map.put("priority",false);
		}
		if(this.getProcure_method()!=null&&this.getProcure_methodDirtyFlag()){
			map.put("procure_method",this.getProcure_method());
		}else if(this.getProcure_methodDirtyFlag()){
			map.put("procure_method",false);
		}
		if(this.getProduction_id()!=null&&this.getProduction_idDirtyFlag()){
			map.put("production_id",this.getProduction_id());
		}else if(this.getProduction_idDirtyFlag()){
			map.put("production_id",false);
		}
		if(this.getProduction_id_text()!=null&&this.getProduction_id_textDirtyFlag()){
			//忽略文本外键production_id_text
		}else if(this.getProduction_id_textDirtyFlag()){
			map.put("production_id",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_packaging()!=null&&this.getProduct_packagingDirtyFlag()){
			map.put("product_packaging",this.getProduct_packaging());
		}else if(this.getProduct_packagingDirtyFlag()){
			map.put("product_packaging",false);
		}
		if(this.getProduct_packaging_text()!=null&&this.getProduct_packaging_textDirtyFlag()){
			//忽略文本外键product_packaging_text
		}else if(this.getProduct_packaging_textDirtyFlag()){
			map.put("product_packaging",false);
		}
		if(this.getProduct_qty()!=null&&this.getProduct_qtyDirtyFlag()){
			map.put("product_qty",this.getProduct_qty());
		}else if(this.getProduct_qtyDirtyFlag()){
			map.put("product_qty",false);
		}
		if(this.getProduct_tmpl_id()!=null&&this.getProduct_tmpl_idDirtyFlag()){
			map.put("product_tmpl_id",this.getProduct_tmpl_id());
		}else if(this.getProduct_tmpl_idDirtyFlag()){
			map.put("product_tmpl_id",false);
		}
		if(this.getProduct_type()!=null&&this.getProduct_typeDirtyFlag()){
			map.put("product_type",this.getProduct_type());
		}else if(this.getProduct_typeDirtyFlag()){
			map.put("product_type",false);
		}
		if(this.getProduct_uom()!=null&&this.getProduct_uomDirtyFlag()){
			map.put("product_uom",this.getProduct_uom());
		}else if(this.getProduct_uomDirtyFlag()){
			map.put("product_uom",false);
		}
		if(this.getProduct_uom_qty()!=null&&this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",this.getProduct_uom_qty());
		}else if(this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",false);
		}
		if(this.getProduct_uom_text()!=null&&this.getProduct_uom_textDirtyFlag()){
			//忽略文本外键product_uom_text
		}else if(this.getProduct_uom_textDirtyFlag()){
			map.put("product_uom",false);
		}
		if(this.getPropagate()!=null&&this.getPropagateDirtyFlag()){
			map.put("propagate",Boolean.parseBoolean(this.getPropagate()));		
		}		if(this.getPurchase_line_id()!=null&&this.getPurchase_line_idDirtyFlag()){
			map.put("purchase_line_id",this.getPurchase_line_id());
		}else if(this.getPurchase_line_idDirtyFlag()){
			map.put("purchase_line_id",false);
		}
		if(this.getPurchase_line_id_text()!=null&&this.getPurchase_line_id_textDirtyFlag()){
			//忽略文本外键purchase_line_id_text
		}else if(this.getPurchase_line_id_textDirtyFlag()){
			map.put("purchase_line_id",false);
		}
		if(this.getQuantity_done()!=null&&this.getQuantity_doneDirtyFlag()){
			map.put("quantity_done",this.getQuantity_done());
		}else if(this.getQuantity_doneDirtyFlag()){
			map.put("quantity_done",false);
		}
		if(this.getRaw_material_production_id()!=null&&this.getRaw_material_production_idDirtyFlag()){
			map.put("raw_material_production_id",this.getRaw_material_production_id());
		}else if(this.getRaw_material_production_idDirtyFlag()){
			map.put("raw_material_production_id",false);
		}
		if(this.getRaw_material_production_id_text()!=null&&this.getRaw_material_production_id_textDirtyFlag()){
			//忽略文本外键raw_material_production_id_text
		}else if(this.getRaw_material_production_id_textDirtyFlag()){
			map.put("raw_material_production_id",false);
		}
		if(this.getReference()!=null&&this.getReferenceDirtyFlag()){
			map.put("reference",this.getReference());
		}else if(this.getReferenceDirtyFlag()){
			map.put("reference",false);
		}
		if(this.getRemaining_qty()!=null&&this.getRemaining_qtyDirtyFlag()){
			map.put("remaining_qty",this.getRemaining_qty());
		}else if(this.getRemaining_qtyDirtyFlag()){
			map.put("remaining_qty",false);
		}
		if(this.getRemaining_value()!=null&&this.getRemaining_valueDirtyFlag()){
			map.put("remaining_value",this.getRemaining_value());
		}else if(this.getRemaining_valueDirtyFlag()){
			map.put("remaining_value",false);
		}
		if(this.getRepair_id()!=null&&this.getRepair_idDirtyFlag()){
			map.put("repair_id",this.getRepair_id());
		}else if(this.getRepair_idDirtyFlag()){
			map.put("repair_id",false);
		}
		if(this.getRepair_id_text()!=null&&this.getRepair_id_textDirtyFlag()){
			//忽略文本外键repair_id_text
		}else if(this.getRepair_id_textDirtyFlag()){
			map.put("repair_id",false);
		}
		if(this.getReserved_availability()!=null&&this.getReserved_availabilityDirtyFlag()){
			map.put("reserved_availability",this.getReserved_availability());
		}else if(this.getReserved_availabilityDirtyFlag()){
			map.put("reserved_availability",false);
		}
		if(this.getRestrict_partner_id()!=null&&this.getRestrict_partner_idDirtyFlag()){
			map.put("restrict_partner_id",this.getRestrict_partner_id());
		}else if(this.getRestrict_partner_idDirtyFlag()){
			map.put("restrict_partner_id",false);
		}
		if(this.getRestrict_partner_id_text()!=null&&this.getRestrict_partner_id_textDirtyFlag()){
			//忽略文本外键restrict_partner_id_text
		}else if(this.getRestrict_partner_id_textDirtyFlag()){
			map.put("restrict_partner_id",false);
		}
		if(this.getReturned_move_ids()!=null&&this.getReturned_move_idsDirtyFlag()){
			map.put("returned_move_ids",this.getReturned_move_ids());
		}else if(this.getReturned_move_idsDirtyFlag()){
			map.put("returned_move_ids",false);
		}
		if(this.getRoute_ids()!=null&&this.getRoute_idsDirtyFlag()){
			map.put("route_ids",this.getRoute_ids());
		}else if(this.getRoute_idsDirtyFlag()){
			map.put("route_ids",false);
		}
		if(this.getRule_id()!=null&&this.getRule_idDirtyFlag()){
			map.put("rule_id",this.getRule_id());
		}else if(this.getRule_idDirtyFlag()){
			map.put("rule_id",false);
		}
		if(this.getRule_id_text()!=null&&this.getRule_id_textDirtyFlag()){
			//忽略文本外键rule_id_text
		}else if(this.getRule_id_textDirtyFlag()){
			map.put("rule_id",false);
		}
		if(this.getSale_line_id()!=null&&this.getSale_line_idDirtyFlag()){
			map.put("sale_line_id",this.getSale_line_id());
		}else if(this.getSale_line_idDirtyFlag()){
			map.put("sale_line_id",false);
		}
		if(this.getSale_line_id_text()!=null&&this.getSale_line_id_textDirtyFlag()){
			//忽略文本外键sale_line_id_text
		}else if(this.getSale_line_id_textDirtyFlag()){
			map.put("sale_line_id",false);
		}
		if(this.getScrapped()!=null&&this.getScrappedDirtyFlag()){
			map.put("scrapped",Boolean.parseBoolean(this.getScrapped()));		
		}		if(this.getScrap_ids()!=null&&this.getScrap_idsDirtyFlag()){
			map.put("scrap_ids",this.getScrap_ids());
		}else if(this.getScrap_idsDirtyFlag()){
			map.put("scrap_ids",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getShow_details_visible()!=null&&this.getShow_details_visibleDirtyFlag()){
			map.put("show_details_visible",Boolean.parseBoolean(this.getShow_details_visible()));		
		}		if(this.getShow_operations()!=null&&this.getShow_operationsDirtyFlag()){
			map.put("show_operations",Boolean.parseBoolean(this.getShow_operations()));		
		}		if(this.getShow_reserved_availability()!=null&&this.getShow_reserved_availabilityDirtyFlag()){
			map.put("show_reserved_availability",Boolean.parseBoolean(this.getShow_reserved_availability()));		
		}		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getString_availability_info()!=null&&this.getString_availability_infoDirtyFlag()){
			map.put("string_availability_info",this.getString_availability_info());
		}else if(this.getString_availability_infoDirtyFlag()){
			map.put("string_availability_info",false);
		}
		if(this.getTo_refund()!=null&&this.getTo_refundDirtyFlag()){
			map.put("to_refund",Boolean.parseBoolean(this.getTo_refund()));		
		}		if(this.getUnbuild_id()!=null&&this.getUnbuild_idDirtyFlag()){
			map.put("unbuild_id",this.getUnbuild_id());
		}else if(this.getUnbuild_idDirtyFlag()){
			map.put("unbuild_id",false);
		}
		if(this.getUnbuild_id_text()!=null&&this.getUnbuild_id_textDirtyFlag()){
			//忽略文本外键unbuild_id_text
		}else if(this.getUnbuild_id_textDirtyFlag()){
			map.put("unbuild_id",false);
		}
		if(this.getUnit_factor()!=null&&this.getUnit_factorDirtyFlag()){
			map.put("unit_factor",this.getUnit_factor());
		}else if(this.getUnit_factorDirtyFlag()){
			map.put("unit_factor",false);
		}
		if(this.getValue()!=null&&this.getValueDirtyFlag()){
			map.put("value",this.getValue());
		}else if(this.getValueDirtyFlag()){
			map.put("value",false);
		}
		if(this.getWarehouse_id()!=null&&this.getWarehouse_idDirtyFlag()){
			map.put("warehouse_id",this.getWarehouse_id());
		}else if(this.getWarehouse_idDirtyFlag()){
			map.put("warehouse_id",false);
		}
		if(this.getWarehouse_id_text()!=null&&this.getWarehouse_id_textDirtyFlag()){
			//忽略文本外键warehouse_id_text
		}else if(this.getWarehouse_id_textDirtyFlag()){
			map.put("warehouse_id",false);
		}
		if(this.getWorkorder_id()!=null&&this.getWorkorder_idDirtyFlag()){
			map.put("workorder_id",this.getWorkorder_id());
		}else if(this.getWorkorder_idDirtyFlag()){
			map.put("workorder_id",false);
		}
		if(this.getWorkorder_id_text()!=null&&this.getWorkorder_id_textDirtyFlag()){
			//忽略文本外键workorder_id_text
		}else if(this.getWorkorder_id_textDirtyFlag()){
			map.put("workorder_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
