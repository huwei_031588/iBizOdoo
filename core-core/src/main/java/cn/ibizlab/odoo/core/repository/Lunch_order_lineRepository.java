package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Lunch_order_line;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_lineSearchContext;

/**
 * 实体 [工作餐订单行] 存储对象
 */
public interface Lunch_order_lineRepository extends Repository<Lunch_order_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Lunch_order_line> searchDefault(Lunch_order_lineSearchContext context);

    Lunch_order_line convert2PO(cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line domain , Lunch_order_line po) ;

    cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line convert2Domain( Lunch_order_line po ,cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line domain) ;

}
