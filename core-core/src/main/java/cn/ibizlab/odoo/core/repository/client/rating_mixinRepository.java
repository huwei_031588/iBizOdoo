package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.rating_mixin;

/**
 * 实体[rating_mixin] 服务对象接口
 */
public interface rating_mixinRepository{


    public rating_mixin createPO() ;
        public void updateBatch(rating_mixin rating_mixin);

        public void create(rating_mixin rating_mixin);

        public void get(String id);

        public void removeBatch(String id);

        public List<rating_mixin> search();

        public void update(rating_mixin rating_mixin);

        public void createBatch(rating_mixin rating_mixin);

        public void remove(String id);


}
