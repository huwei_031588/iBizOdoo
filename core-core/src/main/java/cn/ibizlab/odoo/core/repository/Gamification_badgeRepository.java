package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Gamification_badge;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badgeSearchContext;

/**
 * 实体 [游戏化徽章] 存储对象
 */
public interface Gamification_badgeRepository extends Repository<Gamification_badge> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Gamification_badge> searchDefault(Gamification_badgeSearchContext context);

    Gamification_badge convert2PO(cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge domain , Gamification_badge po) ;

    cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge convert2Domain( Gamification_badge po ,cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge domain) ;

}
