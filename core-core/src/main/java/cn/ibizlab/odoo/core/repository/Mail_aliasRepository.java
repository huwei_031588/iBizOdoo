package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_alias;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_aliasSearchContext;

/**
 * 实体 [EMail别名] 存储对象
 */
public interface Mail_aliasRepository extends Repository<Mail_alias> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_alias> searchDefault(Mail_aliasSearchContext context);

    Mail_alias convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias domain , Mail_alias po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias convert2Domain( Mail_alias po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias domain) ;

}
