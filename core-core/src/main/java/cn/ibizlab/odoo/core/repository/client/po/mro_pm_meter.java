package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mro_pm_meter] 对象
 */
public interface mro_pm_meter {

    public Integer getAsset_id();

    public void setAsset_id(Integer asset_id);

    public String getAsset_id_text();

    public void setAsset_id_text(String asset_id_text);

    public Double getAv_time();

    public void setAv_time(Double av_time);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate();

    public void setDate(Timestamp date);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getMeter_line_ids();

    public void setMeter_line_ids(String meter_line_ids);

    public Integer getMeter_uom();

    public void setMeter_uom(Integer meter_uom);

    public String getMeter_uom_text();

    public void setMeter_uom_text(String meter_uom_text);

    public Double getMin_utilization();

    public void setMin_utilization(Double min_utilization);

    public Integer getName();

    public void setName(Integer name);

    public String getName_text();

    public void setName_text(String name_text);

    public Double getNew_value();

    public void setNew_value(Double new_value);

    public Integer getParent_meter_id();

    public void setParent_meter_id(Integer parent_meter_id);

    public String getParent_meter_id_text();

    public void setParent_meter_id_text(String parent_meter_id_text);

    public Integer getParent_ratio_id();

    public void setParent_ratio_id(Integer parent_ratio_id);

    public String getParent_ratio_id_text();

    public void setParent_ratio_id_text(String parent_ratio_id_text);

    public String getReading_type();

    public void setReading_type(String reading_type);

    public String getState();

    public void setState(String state);

    public Double getTotal_value();

    public void setTotal_value(Double total_value);

    public Double getUtilization();

    public void setUtilization(Double utilization);

    public Double getValue();

    public void setValue(Double value);

    public String getView_line_ids();

    public void setView_line_ids(String view_line_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
