package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_attribute_custom_value;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attribute_custom_valueSearchContext;

/**
 * 实体 [产品属性自定义值] 存储对象
 */
public interface Product_attribute_custom_valueRepository extends Repository<Product_attribute_custom_value> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_attribute_custom_value> searchDefault(Product_attribute_custom_valueSearchContext context);

    Product_attribute_custom_value convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_custom_value domain , Product_attribute_custom_value po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_custom_value convert2Domain( Product_attribute_custom_value po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_custom_value domain) ;

}
