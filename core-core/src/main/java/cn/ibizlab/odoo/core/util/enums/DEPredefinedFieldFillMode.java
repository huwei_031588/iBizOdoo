package cn.ibizlab.odoo.core.util.enums;

/**
 * 实体预置属性填充模式
 */
public enum DEPredefinedFieldFillMode {
    /**
     * 默认不处理
     */
    DEFAULT,
    /**
     * 插入填充字段
     */
    INSERT,
    /**
     * 更新填充字段
     */
    UPDATE,
    /**
     * 插入和更新填充字段
     */
    INSERT_UPDATE
}
