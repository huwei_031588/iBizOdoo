package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_tracking_value;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_tracking_valueSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_tracking_valueService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_tracking_valueOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_tracking_valueClientModel;

/**
 * 实体[邮件跟踪值] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_tracking_valueServiceImpl implements IMail_tracking_valueService {

    @Autowired
    mail_tracking_valueOdooClient mail_tracking_valueOdooClient;


    @Override
    public boolean update(Mail_tracking_value et) {
        mail_tracking_valueClientModel clientModel = convert2Model(et,null);
		mail_tracking_valueOdooClient.update(clientModel);
        Mail_tracking_value rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_tracking_value> list){
    }

    @Override
    public boolean create(Mail_tracking_value et) {
        mail_tracking_valueClientModel clientModel = convert2Model(et,null);
		mail_tracking_valueOdooClient.create(clientModel);
        Mail_tracking_value rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_tracking_value> list){
    }

    @Override
    public Mail_tracking_value get(Integer id) {
        mail_tracking_valueClientModel clientModel = new mail_tracking_valueClientModel();
        clientModel.setId(id);
		mail_tracking_valueOdooClient.get(clientModel);
        Mail_tracking_value et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_tracking_value();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mail_tracking_valueClientModel clientModel = new mail_tracking_valueClientModel();
        clientModel.setId(id);
		mail_tracking_valueOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_tracking_value> searchDefault(Mail_tracking_valueSearchContext context) {
        List<Mail_tracking_value> list = new ArrayList<Mail_tracking_value>();
        Page<mail_tracking_valueClientModel> clientModelList = mail_tracking_valueOdooClient.search(context);
        for(mail_tracking_valueClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_tracking_value>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_tracking_valueClientModel convert2Model(Mail_tracking_value domain , mail_tracking_valueClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_tracking_valueClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("field_descdirtyflag"))
                model.setField_desc(domain.getFieldDesc());
            if((Boolean) domain.getExtensionparams().get("old_value_chardirtyflag"))
                model.setOld_value_char(domain.getOldValueChar());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("new_value_datetimedirtyflag"))
                model.setNew_value_datetime(domain.getNewValueDatetime());
            if((Boolean) domain.getExtensionparams().get("old_value_monetarydirtyflag"))
                model.setOld_value_monetary(domain.getOldValueMonetary());
            if((Boolean) domain.getExtensionparams().get("new_value_chardirtyflag"))
                model.setNew_value_char(domain.getNewValueChar());
            if((Boolean) domain.getExtensionparams().get("new_value_textdirtyflag"))
                model.setNew_value_text(domain.getNewValueText());
            if((Boolean) domain.getExtensionparams().get("track_sequencedirtyflag"))
                model.setTrack_sequence(domain.getTrackSequence());
            if((Boolean) domain.getExtensionparams().get("new_value_monetarydirtyflag"))
                model.setNew_value_monetary(domain.getNewValueMonetary());
            if((Boolean) domain.getExtensionparams().get("old_value_datetimedirtyflag"))
                model.setOld_value_datetime(domain.getOldValueDatetime());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("old_value_integerdirtyflag"))
                model.setOld_value_integer(domain.getOldValueInteger());
            if((Boolean) domain.getExtensionparams().get("old_value_textdirtyflag"))
                model.setOld_value_text(domain.getOldValueText());
            if((Boolean) domain.getExtensionparams().get("field_typedirtyflag"))
                model.setField_type(domain.getFieldType());
            if((Boolean) domain.getExtensionparams().get("new_value_integerdirtyflag"))
                model.setNew_value_integer(domain.getNewValueInteger());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("new_value_floatdirtyflag"))
                model.setNew_value_float(domain.getNewValueFloat());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("fielddirtyflag"))
                model.setField(domain.getField());
            if((Boolean) domain.getExtensionparams().get("old_value_floatdirtyflag"))
                model.setOld_value_float(domain.getOldValueFloat());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("mail_message_iddirtyflag"))
                model.setMail_message_id(domain.getMailMessageId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_tracking_value convert2Domain( mail_tracking_valueClientModel model ,Mail_tracking_value domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_tracking_value();
        }

        if(model.getField_descDirtyFlag())
            domain.setFieldDesc(model.getField_desc());
        if(model.getOld_value_charDirtyFlag())
            domain.setOldValueChar(model.getOld_value_char());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNew_value_datetimeDirtyFlag())
            domain.setNewValueDatetime(model.getNew_value_datetime());
        if(model.getOld_value_monetaryDirtyFlag())
            domain.setOldValueMonetary(model.getOld_value_monetary());
        if(model.getNew_value_charDirtyFlag())
            domain.setNewValueChar(model.getNew_value_char());
        if(model.getNew_value_textDirtyFlag())
            domain.setNewValueText(model.getNew_value_text());
        if(model.getTrack_sequenceDirtyFlag())
            domain.setTrackSequence(model.getTrack_sequence());
        if(model.getNew_value_monetaryDirtyFlag())
            domain.setNewValueMonetary(model.getNew_value_monetary());
        if(model.getOld_value_datetimeDirtyFlag())
            domain.setOldValueDatetime(model.getOld_value_datetime());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getOld_value_integerDirtyFlag())
            domain.setOldValueInteger(model.getOld_value_integer());
        if(model.getOld_value_textDirtyFlag())
            domain.setOldValueText(model.getOld_value_text());
        if(model.getField_typeDirtyFlag())
            domain.setFieldType(model.getField_type());
        if(model.getNew_value_integerDirtyFlag())
            domain.setNewValueInteger(model.getNew_value_integer());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNew_value_floatDirtyFlag())
            domain.setNewValueFloat(model.getNew_value_float());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getFieldDirtyFlag())
            domain.setField(model.getField());
        if(model.getOld_value_floatDirtyFlag())
            domain.setOldValueFloat(model.getOld_value_float());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getMail_message_idDirtyFlag())
            domain.setMailMessageId(model.getMail_message_id());
        return domain ;
    }

}

    



