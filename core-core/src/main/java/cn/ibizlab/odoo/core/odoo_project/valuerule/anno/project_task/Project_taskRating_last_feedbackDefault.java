package cn.ibizlab.odoo.core.odoo_project.valuerule.anno.project_task;

import cn.ibizlab.odoo.core.odoo_project.valuerule.validator.project_task.Project_taskRating_last_feedbackDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Project_task
 * 属性：Rating_last_feedback
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Project_taskRating_last_feedbackDefaultValidator.class})
public @interface Project_taskRating_last_feedbackDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[1048576]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
