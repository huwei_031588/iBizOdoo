package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_pm_meter_interval;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_intervalSearchContext;

/**
 * 实体 [Meter interval] 存储对象
 */
public interface Mro_pm_meter_intervalRepository extends Repository<Mro_pm_meter_interval> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_pm_meter_interval> searchDefault(Mro_pm_meter_intervalSearchContext context);

    Mro_pm_meter_interval convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_interval domain , Mro_pm_meter_interval po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_interval convert2Domain( Mro_pm_meter_interval po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_interval domain) ;

}
