package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_messageSearchContext;

/**
 * 实体 [消息] 存储对象
 */
public interface Mail_messageRepository extends Repository<Mail_message> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_message> searchDefault(Mail_messageSearchContext context);

    Mail_message convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message domain , Mail_message po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message convert2Domain( Mail_message po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message domain) ;

}
