package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leaveSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leaveService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_leaveOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_leaveClientModel;

/**
 * 实体[休假] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_leaveServiceImpl implements IHr_leaveService {

    @Autowired
    hr_leaveOdooClient hr_leaveOdooClient;


    @Override
    public boolean update(Hr_leave et) {
        hr_leaveClientModel clientModel = convert2Model(et,null);
		hr_leaveOdooClient.update(clientModel);
        Hr_leave rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_leave> list){
    }

    @Override
    public boolean create(Hr_leave et) {
        hr_leaveClientModel clientModel = convert2Model(et,null);
		hr_leaveOdooClient.create(clientModel);
        Hr_leave rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_leave> list){
    }

    @Override
    public Hr_leave get(Integer id) {
        hr_leaveClientModel clientModel = new hr_leaveClientModel();
        clientModel.setId(id);
		hr_leaveOdooClient.get(clientModel);
        Hr_leave et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_leave();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        hr_leaveClientModel clientModel = new hr_leaveClientModel();
        clientModel.setId(id);
		hr_leaveOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_leave> searchDefault(Hr_leaveSearchContext context) {
        List<Hr_leave> list = new ArrayList<Hr_leave>();
        Page<hr_leaveClientModel> clientModelList = hr_leaveOdooClient.search(context);
        for(hr_leaveClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_leave>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_leaveClientModel convert2Model(Hr_leave domain , hr_leaveClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_leaveClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("holiday_typedirtyflag"))
                model.setHoliday_type(domain.getHolidayType());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("request_date_fromdirtyflag"))
                model.setRequest_date_from(domain.getRequestDateFrom());
            if((Boolean) domain.getExtensionparams().get("notesdirtyflag"))
                model.setNotes(domain.getNotes());
            if((Boolean) domain.getExtensionparams().get("number_of_daysdirtyflag"))
                model.setNumber_of_days(domain.getNumberOfDays());
            if((Boolean) domain.getExtensionparams().get("request_date_todirtyflag"))
                model.setRequest_date_to(domain.getRequestDateTo());
            if((Boolean) domain.getExtensionparams().get("request_unit_halfdirtyflag"))
                model.setRequest_unit_half(domain.getRequestUnitHalf());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("number_of_days_displaydirtyflag"))
                model.setNumber_of_days_display(domain.getNumberOfDaysDisplay());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("request_hour_fromdirtyflag"))
                model.setRequest_hour_from(domain.getRequestHourFrom());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("payslip_statusdirtyflag"))
                model.setPayslip_status(domain.getPayslipStatus());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("request_unit_hoursdirtyflag"))
                model.setRequest_unit_hours(domain.getRequestUnitHours());
            if((Boolean) domain.getExtensionparams().get("date_todirtyflag"))
                model.setDate_to(domain.getDateTo());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("can_resetdirtyflag"))
                model.setCan_reset(domain.getCanReset());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("number_of_hours_displaydirtyflag"))
                model.setNumber_of_hours_display(domain.getNumberOfHoursDisplay());
            if((Boolean) domain.getExtensionparams().get("request_unit_customdirtyflag"))
                model.setRequest_unit_custom(domain.getRequestUnitCustom());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("report_notedirtyflag"))
                model.setReport_note(domain.getReportNote());
            if((Boolean) domain.getExtensionparams().get("request_hour_todirtyflag"))
                model.setRequest_hour_to(domain.getRequestHourTo());
            if((Boolean) domain.getExtensionparams().get("date_fromdirtyflag"))
                model.setDate_from(domain.getDateFrom());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("linked_request_idsdirtyflag"))
                model.setLinked_request_ids(domain.getLinkedRequestIds());
            if((Boolean) domain.getExtensionparams().get("can_approvedirtyflag"))
                model.setCan_approve(domain.getCanApprove());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("request_date_from_perioddirtyflag"))
                model.setRequest_date_from_period(domain.getRequestDateFromPeriod());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("duration_displaydirtyflag"))
                model.setDuration_display(domain.getDurationDisplay());
            if((Boolean) domain.getExtensionparams().get("mode_company_id_textdirtyflag"))
                model.setMode_company_id_text(domain.getModeCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("leave_type_request_unitdirtyflag"))
                model.setLeave_type_request_unit(domain.getLeaveTypeRequestUnit());
            if((Boolean) domain.getExtensionparams().get("employee_id_textdirtyflag"))
                model.setEmployee_id_text(domain.getEmployeeIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("meeting_id_textdirtyflag"))
                model.setMeeting_id_text(domain.getMeetingIdText());
            if((Boolean) domain.getExtensionparams().get("second_approver_id_textdirtyflag"))
                model.setSecond_approver_id_text(domain.getSecondApproverIdText());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("first_approver_id_textdirtyflag"))
                model.setFirst_approver_id_text(domain.getFirstApproverIdText());
            if((Boolean) domain.getExtensionparams().get("holiday_status_id_textdirtyflag"))
                model.setHoliday_status_id_text(domain.getHolidayStatusIdText());
            if((Boolean) domain.getExtensionparams().get("manager_id_textdirtyflag"))
                model.setManager_id_text(domain.getManagerIdText());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("validation_typedirtyflag"))
                model.setValidation_type(domain.getValidationType());
            if((Boolean) domain.getExtensionparams().get("department_id_textdirtyflag"))
                model.setDepartment_id_text(domain.getDepartmentIdText());
            if((Boolean) domain.getExtensionparams().get("manager_iddirtyflag"))
                model.setManager_id(domain.getManagerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("mode_company_iddirtyflag"))
                model.setMode_company_id(domain.getModeCompanyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("second_approver_iddirtyflag"))
                model.setSecond_approver_id(domain.getSecondApproverId());
            if((Boolean) domain.getExtensionparams().get("meeting_iddirtyflag"))
                model.setMeeting_id(domain.getMeetingId());
            if((Boolean) domain.getExtensionparams().get("holiday_status_iddirtyflag"))
                model.setHoliday_status_id(domain.getHolidayStatusId());
            if((Boolean) domain.getExtensionparams().get("department_iddirtyflag"))
                model.setDepartment_id(domain.getDepartmentId());
            if((Boolean) domain.getExtensionparams().get("employee_iddirtyflag"))
                model.setEmployee_id(domain.getEmployeeId());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("first_approver_iddirtyflag"))
                model.setFirst_approver_id(domain.getFirstApproverId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_leave convert2Domain( hr_leaveClientModel model ,Hr_leave domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_leave();
        }

        if(model.getHoliday_typeDirtyFlag())
            domain.setHolidayType(model.getHoliday_type());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getRequest_date_fromDirtyFlag())
            domain.setRequestDateFrom(model.getRequest_date_from());
        if(model.getNotesDirtyFlag())
            domain.setNotes(model.getNotes());
        if(model.getNumber_of_daysDirtyFlag())
            domain.setNumberOfDays(model.getNumber_of_days());
        if(model.getRequest_date_toDirtyFlag())
            domain.setRequestDateTo(model.getRequest_date_to());
        if(model.getRequest_unit_halfDirtyFlag())
            domain.setRequestUnitHalf(model.getRequest_unit_half());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getNumber_of_days_displayDirtyFlag())
            domain.setNumberOfDaysDisplay(model.getNumber_of_days_display());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getRequest_hour_fromDirtyFlag())
            domain.setRequestHourFrom(model.getRequest_hour_from());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getPayslip_statusDirtyFlag())
            domain.setPayslipStatus(model.getPayslip_status());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getRequest_unit_hoursDirtyFlag())
            domain.setRequestUnitHours(model.getRequest_unit_hours());
        if(model.getDate_toDirtyFlag())
            domain.setDateTo(model.getDate_to());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getCan_resetDirtyFlag())
            domain.setCanReset(model.getCan_reset());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getNumber_of_hours_displayDirtyFlag())
            domain.setNumberOfHoursDisplay(model.getNumber_of_hours_display());
        if(model.getRequest_unit_customDirtyFlag())
            domain.setRequestUnitCustom(model.getRequest_unit_custom());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getReport_noteDirtyFlag())
            domain.setReportNote(model.getReport_note());
        if(model.getRequest_hour_toDirtyFlag())
            domain.setRequestHourTo(model.getRequest_hour_to());
        if(model.getDate_fromDirtyFlag())
            domain.setDateFrom(model.getDate_from());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getLinked_request_idsDirtyFlag())
            domain.setLinkedRequestIds(model.getLinked_request_ids());
        if(model.getCan_approveDirtyFlag())
            domain.setCanApprove(model.getCan_approve());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getRequest_date_from_periodDirtyFlag())
            domain.setRequestDateFromPeriod(model.getRequest_date_from_period());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getDuration_displayDirtyFlag())
            domain.setDurationDisplay(model.getDuration_display());
        if(model.getMode_company_id_textDirtyFlag())
            domain.setModeCompanyIdText(model.getMode_company_id_text());
        if(model.getLeave_type_request_unitDirtyFlag())
            domain.setLeaveTypeRequestUnit(model.getLeave_type_request_unit());
        if(model.getEmployee_id_textDirtyFlag())
            domain.setEmployeeIdText(model.getEmployee_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMeeting_id_textDirtyFlag())
            domain.setMeetingIdText(model.getMeeting_id_text());
        if(model.getSecond_approver_id_textDirtyFlag())
            domain.setSecondApproverIdText(model.getSecond_approver_id_text());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getFirst_approver_id_textDirtyFlag())
            domain.setFirstApproverIdText(model.getFirst_approver_id_text());
        if(model.getHoliday_status_id_textDirtyFlag())
            domain.setHolidayStatusIdText(model.getHoliday_status_id_text());
        if(model.getManager_id_textDirtyFlag())
            domain.setManagerIdText(model.getManager_id_text());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getValidation_typeDirtyFlag())
            domain.setValidationType(model.getValidation_type());
        if(model.getDepartment_id_textDirtyFlag())
            domain.setDepartmentIdText(model.getDepartment_id_text());
        if(model.getManager_idDirtyFlag())
            domain.setManagerId(model.getManager_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getMode_company_idDirtyFlag())
            domain.setModeCompanyId(model.getMode_company_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getSecond_approver_idDirtyFlag())
            domain.setSecondApproverId(model.getSecond_approver_id());
        if(model.getMeeting_idDirtyFlag())
            domain.setMeetingId(model.getMeeting_id());
        if(model.getHoliday_status_idDirtyFlag())
            domain.setHolidayStatusId(model.getHoliday_status_id());
        if(model.getDepartment_idDirtyFlag())
            domain.setDepartmentId(model.getDepartment_id());
        if(model.getEmployee_idDirtyFlag())
            domain.setEmployeeId(model.getEmployee_id());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getFirst_approver_idDirtyFlag())
            domain.setFirstApproverId(model.getFirst_approver_id());
        return domain ;
    }

}

    



