package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_pm_replan;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_replanSearchContext;

/**
 * 实体 [Replan PM] 存储对象
 */
public interface Mro_pm_replanRepository extends Repository<Mro_pm_replan> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_pm_replan> searchDefault(Mro_pm_replanSearchContext context);

    Mro_pm_replan convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_replan domain , Mro_pm_replan po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_replan convert2Domain( Mro_pm_replan po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_replan domain) ;

}
