package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [仓库] 对象
 */
@Data
public class Stock_warehouse extends EntityClient implements Serializable {

    /**
     * 补充路线
     */
    @JSONField(name = "resupply_route_ids")
    @JsonProperty("resupply_route_ids")
    private String resupplyRouteIds;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 补给 自
     */
    @JSONField(name = "resupply_wh_ids")
    @JsonProperty("resupply_wh_ids")
    private String resupplyWhIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 仓库
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 出向运输
     */
    @DEField(name = "delivery_steps")
    @JSONField(name = "delivery_steps")
    @JsonProperty("delivery_steps")
    private String deliverySteps;

    /**
     * 仓库个数
     */
    @JSONField(name = "warehouse_count")
    @JsonProperty("warehouse_count")
    private Integer warehouseCount;

    /**
     * 制造补给
     */
    @DEField(name = "manufacture_to_resupply")
    @JSONField(name = "manufacture_to_resupply")
    @JsonProperty("manufacture_to_resupply")
    private String manufactureToResupply;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 路线
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 入库
     */
    @DEField(name = "reception_steps")
    @JSONField(name = "reception_steps")
    @JsonProperty("reception_steps")
    private String receptionSteps;

    /**
     * 购买补给
     */
    @DEField(name = "buy_to_resupply")
    @JSONField(name = "buy_to_resupply")
    @JsonProperty("buy_to_resupply")
    private String buyToResupply;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 缩写
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 制造
     */
    @DEField(name = "manufacture_steps")
    @JSONField(name = "manufacture_steps")
    @JsonProperty("manufacture_steps")
    private String manufactureSteps;

    /**
     * 显示补给
     */
    @JSONField(name = "show_resupply")
    @JsonProperty("show_resupply")
    private String showResupply;

    /**
     * 视图位置
     */
    @JSONField(name = "view_location_id_text")
    @JsonProperty("view_location_id_text")
    private String viewLocationIdText;

    /**
     * 进货位置
     */
    @JSONField(name = "wh_input_stock_loc_id_text")
    @JsonProperty("wh_input_stock_loc_id_text")
    private String whInputStockLocIdText;

    /**
     * 制造地点后的库存
     */
    @JSONField(name = "sam_loc_id_text")
    @JsonProperty("sam_loc_id_text")
    private String samLocIdText;

    /**
     * 越库路线
     */
    @JSONField(name = "crossdock_route_id_text")
    @JsonProperty("crossdock_route_id_text")
    private String crossdockRouteIdText;

    /**
     * 在制造路线前拣货
     */
    @JSONField(name = "pbm_route_id_text")
    @JsonProperty("pbm_route_id_text")
    private String pbmRouteIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 在制造作业类型前拣货
     */
    @JSONField(name = "pbm_type_id_text")
    @JsonProperty("pbm_type_id_text")
    private String pbmTypeIdText;

    /**
     * 分拣类型
     */
    @JSONField(name = "pick_type_id_text")
    @JsonProperty("pick_type_id_text")
    private String pickTypeIdText;

    /**
     * 内部类型
     */
    @JSONField(name = "int_type_id_text")
    @JsonProperty("int_type_id_text")
    private String intTypeIdText;

    /**
     * 生产操作类型
     */
    @JSONField(name = "manu_type_id_text")
    @JsonProperty("manu_type_id_text")
    private String manuTypeIdText;

    /**
     * 购买规则
     */
    @JSONField(name = "buy_pull_id_text")
    @JsonProperty("buy_pull_id_text")
    private String buyPullIdText;

    /**
     * 打包位置
     */
    @JSONField(name = "wh_pack_stock_loc_id_text")
    @JsonProperty("wh_pack_stock_loc_id_text")
    private String whPackStockLocIdText;

    /**
     * 制造规则
     */
    @JSONField(name = "manufacture_pull_id_text")
    @JsonProperty("manufacture_pull_id_text")
    private String manufacturePullIdText;

    /**
     * 收货路线
     */
    @JSONField(name = "reception_route_id_text")
    @JsonProperty("reception_route_id_text")
    private String receptionRouteIdText;

    /**
     * 交货路线
     */
    @JSONField(name = "delivery_route_id_text")
    @JsonProperty("delivery_route_id_text")
    private String deliveryRouteIdText;

    /**
     * 制造运营类型后的库存
     */
    @JSONField(name = "sam_type_id_text")
    @JsonProperty("sam_type_id_text")
    private String samTypeIdText;

    /**
     * 地址
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 出库类型
     */
    @JSONField(name = "out_type_id_text")
    @JsonProperty("out_type_id_text")
    private String outTypeIdText;

    /**
     * MTO规则
     */
    @JSONField(name = "mto_pull_id_text")
    @JsonProperty("mto_pull_id_text")
    private String mtoPullIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 库存位置
     */
    @JSONField(name = "lot_stock_id_text")
    @JsonProperty("lot_stock_id_text")
    private String lotStockIdText;

    /**
     * 在制造（按订单补货）MTO规则之前拣货
     */
    @JSONField(name = "pbm_mto_pull_id_text")
    @JsonProperty("pbm_mto_pull_id_text")
    private String pbmMtoPullIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 在制造位置前拣货
     */
    @JSONField(name = "pbm_loc_id_text")
    @JsonProperty("pbm_loc_id_text")
    private String pbmLocIdText;

    /**
     * 包裹类型
     */
    @JSONField(name = "pack_type_id_text")
    @JsonProperty("pack_type_id_text")
    private String packTypeIdText;

    /**
     * 制造规则后的库存
     */
    @JSONField(name = "sam_rule_id_text")
    @JsonProperty("sam_rule_id_text")
    private String samRuleIdText;

    /**
     * 入库类型
     */
    @JSONField(name = "in_type_id_text")
    @JsonProperty("in_type_id_text")
    private String inTypeIdText;

    /**
     * 出货位置
     */
    @JSONField(name = "wh_output_stock_loc_id_text")
    @JsonProperty("wh_output_stock_loc_id_text")
    private String whOutputStockLocIdText;

    /**
     * 质量管理位置
     */
    @JSONField(name = "wh_qc_stock_loc_id_text")
    @JsonProperty("wh_qc_stock_loc_id_text")
    private String whQcStockLocIdText;

    /**
     * 出货位置
     */
    @DEField(name = "wh_output_stock_loc_id")
    @JSONField(name = "wh_output_stock_loc_id")
    @JsonProperty("wh_output_stock_loc_id")
    private Integer whOutputStockLocId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 在制造位置前拣货
     */
    @DEField(name = "pbm_loc_id")
    @JSONField(name = "pbm_loc_id")
    @JsonProperty("pbm_loc_id")
    private Integer pbmLocId;

    /**
     * 视图位置
     */
    @DEField(name = "view_location_id")
    @JSONField(name = "view_location_id")
    @JsonProperty("view_location_id")
    private Integer viewLocationId;

    /**
     * 出库类型
     */
    @DEField(name = "out_type_id")
    @JSONField(name = "out_type_id")
    @JsonProperty("out_type_id")
    private Integer outTypeId;

    /**
     * 生产操作类型
     */
    @DEField(name = "manu_type_id")
    @JSONField(name = "manu_type_id")
    @JsonProperty("manu_type_id")
    private Integer manuTypeId;

    /**
     * 在制造（按订单补货）MTO规则之前拣货
     */
    @DEField(name = "pbm_mto_pull_id")
    @JSONField(name = "pbm_mto_pull_id")
    @JsonProperty("pbm_mto_pull_id")
    private Integer pbmMtoPullId;

    /**
     * MTO规则
     */
    @DEField(name = "mto_pull_id")
    @JSONField(name = "mto_pull_id")
    @JsonProperty("mto_pull_id")
    private Integer mtoPullId;

    /**
     * 制造运营类型后的库存
     */
    @DEField(name = "sam_type_id")
    @JSONField(name = "sam_type_id")
    @JsonProperty("sam_type_id")
    private Integer samTypeId;

    /**
     * 制造规则
     */
    @DEField(name = "manufacture_pull_id")
    @JSONField(name = "manufacture_pull_id")
    @JsonProperty("manufacture_pull_id")
    private Integer manufacturePullId;

    /**
     * 制造地点后的库存
     */
    @DEField(name = "sam_loc_id")
    @JSONField(name = "sam_loc_id")
    @JsonProperty("sam_loc_id")
    private Integer samLocId;

    /**
     * 购买规则
     */
    @DEField(name = "buy_pull_id")
    @JSONField(name = "buy_pull_id")
    @JsonProperty("buy_pull_id")
    private Integer buyPullId;

    /**
     * 内部类型
     */
    @DEField(name = "int_type_id")
    @JSONField(name = "int_type_id")
    @JsonProperty("int_type_id")
    private Integer intTypeId;

    /**
     * 质量管理位置
     */
    @DEField(name = "wh_qc_stock_loc_id")
    @JSONField(name = "wh_qc_stock_loc_id")
    @JsonProperty("wh_qc_stock_loc_id")
    private Integer whQcStockLocId;

    /**
     * 入库类型
     */
    @DEField(name = "in_type_id")
    @JSONField(name = "in_type_id")
    @JsonProperty("in_type_id")
    private Integer inTypeId;

    /**
     * 在制造路线前拣货
     */
    @DEField(name = "pbm_route_id")
    @JSONField(name = "pbm_route_id")
    @JsonProperty("pbm_route_id")
    private Integer pbmRouteId;

    /**
     * 分拣类型
     */
    @DEField(name = "pick_type_id")
    @JSONField(name = "pick_type_id")
    @JsonProperty("pick_type_id")
    private Integer pickTypeId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 地址
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 收货路线
     */
    @DEField(name = "reception_route_id")
    @JSONField(name = "reception_route_id")
    @JsonProperty("reception_route_id")
    private Integer receptionRouteId;

    /**
     * 包裹类型
     */
    @DEField(name = "pack_type_id")
    @JSONField(name = "pack_type_id")
    @JsonProperty("pack_type_id")
    private Integer packTypeId;

    /**
     * 在制造作业类型前拣货
     */
    @DEField(name = "pbm_type_id")
    @JSONField(name = "pbm_type_id")
    @JsonProperty("pbm_type_id")
    private Integer pbmTypeId;

    /**
     * 交货路线
     */
    @DEField(name = "delivery_route_id")
    @JSONField(name = "delivery_route_id")
    @JsonProperty("delivery_route_id")
    private Integer deliveryRouteId;

    /**
     * 进货位置
     */
    @DEField(name = "wh_input_stock_loc_id")
    @JSONField(name = "wh_input_stock_loc_id")
    @JsonProperty("wh_input_stock_loc_id")
    private Integer whInputStockLocId;

    /**
     * 库存位置
     */
    @DEField(name = "lot_stock_id")
    @JSONField(name = "lot_stock_id")
    @JsonProperty("lot_stock_id")
    private Integer lotStockId;

    /**
     * 制造规则后的库存
     */
    @DEField(name = "sam_rule_id")
    @JSONField(name = "sam_rule_id")
    @JsonProperty("sam_rule_id")
    private Integer samRuleId;

    /**
     * 越库路线
     */
    @DEField(name = "crossdock_route_id")
    @JSONField(name = "crossdock_route_id")
    @JsonProperty("crossdock_route_id")
    private Integer crossdockRouteId;

    /**
     * 打包位置
     */
    @DEField(name = "wh_pack_stock_loc_id")
    @JSONField(name = "wh_pack_stock_loc_id")
    @JsonProperty("wh_pack_stock_loc_id")
    private Integer whPackStockLocId;


    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoocrossdockroute")
    @JsonProperty("odoocrossdockroute")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route odooCrossdockRoute;

    /**
     * 
     */
    @JSONField(name = "odoodeliveryroute")
    @JsonProperty("odoodeliveryroute")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route odooDeliveryRoute;

    /**
     * 
     */
    @JSONField(name = "odoopbmroute")
    @JsonProperty("odoopbmroute")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route odooPbmRoute;

    /**
     * 
     */
    @JSONField(name = "odooreceptionroute")
    @JsonProperty("odooreceptionroute")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route odooReceptionRoute;

    /**
     * 
     */
    @JSONField(name = "odoolotstock")
    @JsonProperty("odoolotstock")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLotStock;

    /**
     * 
     */
    @JSONField(name = "odoopbmloc")
    @JsonProperty("odoopbmloc")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooPbmLoc;

    /**
     * 
     */
    @JSONField(name = "odoosamloc")
    @JsonProperty("odoosamloc")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooSamLoc;

    /**
     * 
     */
    @JSONField(name = "odooviewlocation")
    @JsonProperty("odooviewlocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooViewLocation;

    /**
     * 
     */
    @JSONField(name = "odoowhinputstockloc")
    @JsonProperty("odoowhinputstockloc")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooWhInputStockLoc;

    /**
     * 
     */
    @JSONField(name = "odoowhoutputstockloc")
    @JsonProperty("odoowhoutputstockloc")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooWhOutputStockLoc;

    /**
     * 
     */
    @JSONField(name = "odoowhpackstockloc")
    @JsonProperty("odoowhpackstockloc")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooWhPackStockLoc;

    /**
     * 
     */
    @JSONField(name = "odoowhqcstockloc")
    @JsonProperty("odoowhqcstockloc")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooWhQcStockLoc;

    /**
     * 
     */
    @JSONField(name = "odoointtype")
    @JsonProperty("odoointtype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooIntType;

    /**
     * 
     */
    @JSONField(name = "odoointype")
    @JsonProperty("odoointype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooInType;

    /**
     * 
     */
    @JSONField(name = "odoomanutype")
    @JsonProperty("odoomanutype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooManuType;

    /**
     * 
     */
    @JSONField(name = "odooouttype")
    @JsonProperty("odooouttype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooOutType;

    /**
     * 
     */
    @JSONField(name = "odoopacktype")
    @JsonProperty("odoopacktype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooPackType;

    /**
     * 
     */
    @JSONField(name = "odoopbmtype")
    @JsonProperty("odoopbmtype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooPbmType;

    /**
     * 
     */
    @JSONField(name = "odoopicktype")
    @JsonProperty("odoopicktype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooPickType;

    /**
     * 
     */
    @JSONField(name = "odoosamtype")
    @JsonProperty("odoosamtype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooSamType;

    /**
     * 
     */
    @JSONField(name = "odoobuypull")
    @JsonProperty("odoobuypull")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule odooBuyPull;

    /**
     * 
     */
    @JSONField(name = "odoomanufacturepull")
    @JsonProperty("odoomanufacturepull")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule odooManufacturePull;

    /**
     * 
     */
    @JSONField(name = "odoomtopull")
    @JsonProperty("odoomtopull")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule odooMtoPull;

    /**
     * 
     */
    @JSONField(name = "odoopbmmtopull")
    @JsonProperty("odoopbmmtopull")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule odooPbmMtoPull;

    /**
     * 
     */
    @JSONField(name = "odoosamrule")
    @JsonProperty("odoosamrule")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule odooSamRule;




    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [仓库]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [出向运输]
     */
    public void setDeliverySteps(String deliverySteps){
        this.deliverySteps = deliverySteps ;
        this.modify("delivery_steps",deliverySteps);
    }
    /**
     * 设置 [制造补给]
     */
    public void setManufactureToResupply(String manufactureToResupply){
        this.manufactureToResupply = manufactureToResupply ;
        this.modify("manufacture_to_resupply",manufactureToResupply);
    }
    /**
     * 设置 [入库]
     */
    public void setReceptionSteps(String receptionSteps){
        this.receptionSteps = receptionSteps ;
        this.modify("reception_steps",receptionSteps);
    }
    /**
     * 设置 [购买补给]
     */
    public void setBuyToResupply(String buyToResupply){
        this.buyToResupply = buyToResupply ;
        this.modify("buy_to_resupply",buyToResupply);
    }
    /**
     * 设置 [缩写]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }
    /**
     * 设置 [制造]
     */
    public void setManufactureSteps(String manufactureSteps){
        this.manufactureSteps = manufactureSteps ;
        this.modify("manufacture_steps",manufactureSteps);
    }
    /**
     * 设置 [出货位置]
     */
    public void setWhOutputStockLocId(Integer whOutputStockLocId){
        this.whOutputStockLocId = whOutputStockLocId ;
        this.modify("wh_output_stock_loc_id",whOutputStockLocId);
    }
    /**
     * 设置 [在制造位置前拣货]
     */
    public void setPbmLocId(Integer pbmLocId){
        this.pbmLocId = pbmLocId ;
        this.modify("pbm_loc_id",pbmLocId);
    }
    /**
     * 设置 [视图位置]
     */
    public void setViewLocationId(Integer viewLocationId){
        this.viewLocationId = viewLocationId ;
        this.modify("view_location_id",viewLocationId);
    }
    /**
     * 设置 [出库类型]
     */
    public void setOutTypeId(Integer outTypeId){
        this.outTypeId = outTypeId ;
        this.modify("out_type_id",outTypeId);
    }
    /**
     * 设置 [生产操作类型]
     */
    public void setManuTypeId(Integer manuTypeId){
        this.manuTypeId = manuTypeId ;
        this.modify("manu_type_id",manuTypeId);
    }
    /**
     * 设置 [在制造（按订单补货）MTO规则之前拣货]
     */
    public void setPbmMtoPullId(Integer pbmMtoPullId){
        this.pbmMtoPullId = pbmMtoPullId ;
        this.modify("pbm_mto_pull_id",pbmMtoPullId);
    }
    /**
     * 设置 [MTO规则]
     */
    public void setMtoPullId(Integer mtoPullId){
        this.mtoPullId = mtoPullId ;
        this.modify("mto_pull_id",mtoPullId);
    }
    /**
     * 设置 [制造运营类型后的库存]
     */
    public void setSamTypeId(Integer samTypeId){
        this.samTypeId = samTypeId ;
        this.modify("sam_type_id",samTypeId);
    }
    /**
     * 设置 [制造规则]
     */
    public void setManufacturePullId(Integer manufacturePullId){
        this.manufacturePullId = manufacturePullId ;
        this.modify("manufacture_pull_id",manufacturePullId);
    }
    /**
     * 设置 [制造地点后的库存]
     */
    public void setSamLocId(Integer samLocId){
        this.samLocId = samLocId ;
        this.modify("sam_loc_id",samLocId);
    }
    /**
     * 设置 [购买规则]
     */
    public void setBuyPullId(Integer buyPullId){
        this.buyPullId = buyPullId ;
        this.modify("buy_pull_id",buyPullId);
    }
    /**
     * 设置 [内部类型]
     */
    public void setIntTypeId(Integer intTypeId){
        this.intTypeId = intTypeId ;
        this.modify("int_type_id",intTypeId);
    }
    /**
     * 设置 [质量管理位置]
     */
    public void setWhQcStockLocId(Integer whQcStockLocId){
        this.whQcStockLocId = whQcStockLocId ;
        this.modify("wh_qc_stock_loc_id",whQcStockLocId);
    }
    /**
     * 设置 [入库类型]
     */
    public void setInTypeId(Integer inTypeId){
        this.inTypeId = inTypeId ;
        this.modify("in_type_id",inTypeId);
    }
    /**
     * 设置 [在制造路线前拣货]
     */
    public void setPbmRouteId(Integer pbmRouteId){
        this.pbmRouteId = pbmRouteId ;
        this.modify("pbm_route_id",pbmRouteId);
    }
    /**
     * 设置 [分拣类型]
     */
    public void setPickTypeId(Integer pickTypeId){
        this.pickTypeId = pickTypeId ;
        this.modify("pick_type_id",pickTypeId);
    }
    /**
     * 设置 [地址]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [收货路线]
     */
    public void setReceptionRouteId(Integer receptionRouteId){
        this.receptionRouteId = receptionRouteId ;
        this.modify("reception_route_id",receptionRouteId);
    }
    /**
     * 设置 [包裹类型]
     */
    public void setPackTypeId(Integer packTypeId){
        this.packTypeId = packTypeId ;
        this.modify("pack_type_id",packTypeId);
    }
    /**
     * 设置 [在制造作业类型前拣货]
     */
    public void setPbmTypeId(Integer pbmTypeId){
        this.pbmTypeId = pbmTypeId ;
        this.modify("pbm_type_id",pbmTypeId);
    }
    /**
     * 设置 [交货路线]
     */
    public void setDeliveryRouteId(Integer deliveryRouteId){
        this.deliveryRouteId = deliveryRouteId ;
        this.modify("delivery_route_id",deliveryRouteId);
    }
    /**
     * 设置 [进货位置]
     */
    public void setWhInputStockLocId(Integer whInputStockLocId){
        this.whInputStockLocId = whInputStockLocId ;
        this.modify("wh_input_stock_loc_id",whInputStockLocId);
    }
    /**
     * 设置 [库存位置]
     */
    public void setLotStockId(Integer lotStockId){
        this.lotStockId = lotStockId ;
        this.modify("lot_stock_id",lotStockId);
    }
    /**
     * 设置 [制造规则后的库存]
     */
    public void setSamRuleId(Integer samRuleId){
        this.samRuleId = samRuleId ;
        this.modify("sam_rule_id",samRuleId);
    }
    /**
     * 设置 [越库路线]
     */
    public void setCrossdockRouteId(Integer crossdockRouteId){
        this.crossdockRouteId = crossdockRouteId ;
        this.modify("crossdock_route_id",crossdockRouteId);
    }
    /**
     * 设置 [打包位置]
     */
    public void setWhPackStockLocId(Integer whPackStockLocId){
        this.whPackStockLocId = whPackStockLocId ;
        this.modify("wh_pack_stock_loc_id",whPackStockLocId);
    }

}


