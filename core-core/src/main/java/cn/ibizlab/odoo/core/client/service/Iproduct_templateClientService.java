package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_template] 服务对象接口
 */
public interface Iproduct_templateClientService{

    public Iproduct_template createModel() ;

    public void remove(Iproduct_template product_template);

    public void create(Iproduct_template product_template);

    public void update(Iproduct_template product_template);

    public void removeBatch(List<Iproduct_template> product_templates);

    public void updateBatch(List<Iproduct_template> product_templates);

    public void get(Iproduct_template product_template);

    public Page<Iproduct_template> search(SearchContext context);

    public void createBatch(List<Iproduct_template> product_templates);

    public Page<Iproduct_template> select(SearchContext context);

}
