package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_partner_autocomplete_sync;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_autocomplete_syncSearchContext;

/**
 * 实体 [合作伙伴自动完成同步] 存储对象
 */
public interface Res_partner_autocomplete_syncRepository extends Repository<Res_partner_autocomplete_sync> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_partner_autocomplete_sync> searchDefault(Res_partner_autocomplete_syncSearchContext context);

    Res_partner_autocomplete_sync convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_autocomplete_sync domain , Res_partner_autocomplete_sync po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_autocomplete_sync convert2Domain( Res_partner_autocomplete_sync po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_autocomplete_sync domain) ;

}
