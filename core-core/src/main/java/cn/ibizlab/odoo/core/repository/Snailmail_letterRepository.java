package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Snailmail_letter;
import cn.ibizlab.odoo.core.odoo_snailmail.filter.Snailmail_letterSearchContext;

/**
 * 实体 [Snailmail 信纸] 存储对象
 */
public interface Snailmail_letterRepository extends Repository<Snailmail_letter> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Snailmail_letter> searchDefault(Snailmail_letterSearchContext context);

    Snailmail_letter convert2PO(cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter domain , Snailmail_letter po) ;

    cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter convert2Domain( Snailmail_letter po ,cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter domain) ;

}
