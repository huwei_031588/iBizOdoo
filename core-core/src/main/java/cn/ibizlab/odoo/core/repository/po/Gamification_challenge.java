package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challengeSearchContext;

/**
 * 实体 [游戏化挑战] 存储模型
 */
public interface Gamification_challenge{

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 挑战名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [挑战名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 需要激活
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要激活]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getStart_date();

    void setStart_date(Timestamp start_date);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getStart_dateDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 最新报告日期
     */
    Timestamp getLast_report_date();

    void setLast_report_date(Timestamp last_report_date);

    /**
     * 获取 [最新报告日期]脏标记
     */
    boolean getLast_report_dateDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getEnd_date();

    void setEnd_date(Timestamp end_date);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getEnd_dateDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 用户领域
     */
    String getUser_domain();

    void setUser_domain(String user_domain);

    /**
     * 获取 [用户领域]脏标记
     */
    boolean getUser_domainDirtyFlag();

    /**
     * 出现在
     */
    String getCategory();

    void setCategory(String category);

    /**
     * 获取 [出现在]脏标记
     */
    boolean getCategoryDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 用户
     */
    String getUser_ids();

    void setUser_ids(String user_ids);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_idsDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 明细行
     */
    String getLine_ids();

    void setLine_ids(String line_ids);

    /**
     * 获取 [明细行]脏标记
     */
    boolean getLine_idsDirtyFlag();

    /**
     * 报告的频率
     */
    String getReport_message_frequency();

    void setReport_message_frequency(String report_message_frequency);

    /**
     * 获取 [报告的频率]脏标记
     */
    boolean getReport_message_frequencyDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 每完成一个目标就马上奖励
     */
    String getReward_realtime();

    void setReward_realtime(String reward_realtime);

    /**
     * 获取 [每完成一个目标就马上奖励]脏标记
     */
    boolean getReward_realtimeDirtyFlag();

    /**
     * 奖励未达成目标的最优者?
     */
    String getReward_failure();

    void setReward_failure(String reward_failure);

    /**
     * 获取 [奖励未达成目标的最优者?]脏标记
     */
    boolean getReward_failureDirtyFlag();

    /**
     * 周期
     */
    String getPeriod();

    void setPeriod(String period);

    /**
     * 获取 [周期]脏标记
     */
    boolean getPeriodDirtyFlag();

    /**
     * 显示模式
     */
    String getVisibility_mode();

    void setVisibility_mode(String visibility_mode);

    /**
     * 获取 [显示模式]脏标记
     */
    boolean getVisibility_modeDirtyFlag();

    /**
     * 未更新的手动目标稍后将被提醒
     */
    Integer getRemind_update_delay();

    void setRemind_update_delay(Integer remind_update_delay);

    /**
     * 获取 [未更新的手动目标稍后将被提醒]脏标记
     */
    boolean getRemind_update_delayDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 建议用户
     */
    String getInvited_user_ids();

    void setInvited_user_ids(String invited_user_ids);

    /**
     * 获取 [建议用户]脏标记
     */
    boolean getInvited_user_idsDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 下次报告日期
     */
    Timestamp getNext_report_date();

    void setNext_report_date(Timestamp next_report_date);

    /**
     * 获取 [下次报告日期]脏标记
     */
    boolean getNext_report_dateDirtyFlag();

    /**
     * 负责人
     */
    String getManager_id_text();

    void setManager_id_text(String manager_id_text);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getManager_id_textDirtyFlag();

    /**
     * 抄送
     */
    String getReport_message_group_id_text();

    void setReport_message_group_id_text(String report_message_group_id_text);

    /**
     * 获取 [抄送]脏标记
     */
    boolean getReport_message_group_id_textDirtyFlag();

    /**
     * 报告模板
     */
    String getReport_template_id_text();

    void setReport_template_id_text(String report_template_id_text);

    /**
     * 获取 [报告模板]脏标记
     */
    boolean getReport_template_id_textDirtyFlag();

    /**
     * 每位获得成功的用户
     */
    String getReward_id_text();

    void setReward_id_text(String reward_id_text);

    /**
     * 获取 [每位获得成功的用户]脏标记
     */
    boolean getReward_id_textDirtyFlag();

    /**
     * 第一位用户
     */
    String getReward_first_id_text();

    void setReward_first_id_text(String reward_first_id_text);

    /**
     * 获取 [第一位用户]脏标记
     */
    boolean getReward_first_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 第二位用户
     */
    String getReward_second_id_text();

    void setReward_second_id_text(String reward_second_id_text);

    /**
     * 获取 [第二位用户]脏标记
     */
    boolean getReward_second_id_textDirtyFlag();

    /**
     * 第三位用户
     */
    String getReward_third_id_text();

    void setReward_third_id_text(String reward_third_id_text);

    /**
     * 获取 [第三位用户]脏标记
     */
    boolean getReward_third_id_textDirtyFlag();

    /**
     * 第一位用户
     */
    Integer getReward_first_id();

    void setReward_first_id(Integer reward_first_id);

    /**
     * 获取 [第一位用户]脏标记
     */
    boolean getReward_first_idDirtyFlag();

    /**
     * 报告模板
     */
    Integer getReport_template_id();

    void setReport_template_id(Integer report_template_id);

    /**
     * 获取 [报告模板]脏标记
     */
    boolean getReport_template_idDirtyFlag();

    /**
     * 第二位用户
     */
    Integer getReward_second_id();

    void setReward_second_id(Integer reward_second_id);

    /**
     * 获取 [第二位用户]脏标记
     */
    boolean getReward_second_idDirtyFlag();

    /**
     * 第三位用户
     */
    Integer getReward_third_id();

    void setReward_third_id(Integer reward_third_id);

    /**
     * 获取 [第三位用户]脏标记
     */
    boolean getReward_third_idDirtyFlag();

    /**
     * 抄送
     */
    Integer getReport_message_group_id();

    void setReport_message_group_id(Integer report_message_group_id);

    /**
     * 获取 [抄送]脏标记
     */
    boolean getReport_message_group_idDirtyFlag();

    /**
     * 每位获得成功的用户
     */
    Integer getReward_id();

    void setReward_id(Integer reward_id);

    /**
     * 获取 [每位获得成功的用户]脏标记
     */
    boolean getReward_idDirtyFlag();

    /**
     * 负责人
     */
    Integer getManager_id();

    void setManager_id(Integer manager_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getManager_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
