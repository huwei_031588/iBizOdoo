package cn.ibizlab.odoo.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [员工] 对象
 */
@Data
public class Hr_employee extends EntityClient implements Serializable {

    /**
     * 办公手机
     */
    @DEField(name = "mobile_phone")
    @JSONField(name = "mobile_phone")
    @JsonProperty("mobile_phone")
    private String mobilePhone;

    /**
     * 错误数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 行动数量
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 起始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "leave_date_from" , format="yyyy-MM-dd")
    @JsonProperty("leave_date_from")
    private Timestamp leaveDateFrom;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 子女数目
     */
    @JSONField(name = "children")
    @JsonProperty("children")
    private Integer children;

    /**
     * 小尺寸照片
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * PIN
     */
    @JSONField(name = "pin")
    @JsonProperty("pin")
    private String pin;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 毕业院校
     */
    @DEField(name = "study_school")
    @JSONField(name = "study_school")
    @JsonProperty("study_school")
    private String studySchool;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 婚姻状况
     */
    @JSONField(name = "marital")
    @JsonProperty("marital")
    private String marital;

    /**
     * 直接徽章
     */
    @JSONField(name = "direct_badge_ids")
    @JsonProperty("direct_badge_ids")
    private String directBadgeIds;

    /**
     * 中等尺寸照片
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 紧急电话
     */
    @DEField(name = "emergency_phone")
    @JSONField(name = "emergency_phone")
    @JsonProperty("emergency_phone")
    private String emergencyPhone;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 上班距离
     */
    @DEField(name = "km_home_work")
    @JSONField(name = "km_home_work")
    @JsonProperty("km_home_work")
    private Integer kmHomeWork;

    /**
     * 签证号
     */
    @DEField(name = "visa_no")
    @JSONField(name = "visa_no")
    @JsonProperty("visa_no")
    private String visaNo;

    /**
     * 出生地
     */
    @DEField(name = "place_of_birth")
    @JSONField(name = "place_of_birth")
    @JsonProperty("place_of_birth")
    private String placeOfBirth;

    /**
     * 配偶全名
     */
    @DEField(name = "spouse_complete_name")
    @JSONField(name = "spouse_complete_name")
    @JsonProperty("spouse_complete_name")
    private String spouseCompleteName;

    /**
     * 社会保险号SIN
     */
    @JSONField(name = "sinid")
    @JsonProperty("sinid")
    private String sinid;

    /**
     * 员工徽章
     */
    @JSONField(name = "badge_ids")
    @JsonProperty("badge_ids")
    private String badgeIds;

    /**
     * 工作EMail
     */
    @DEField(name = "work_email")
    @JSONField(name = "work_email")
    @JsonProperty("work_email")
    private String workEmail;

    /**
     * 拥有徽章
     */
    @JSONField(name = "has_badges")
    @JsonProperty("has_badges")
    private String hasBadges;

    /**
     * 当前休假类型
     */
    @JSONField(name = "current_leave_id")
    @JsonProperty("current_leave_id")
    private Integer currentLeaveId;

    /**
     * 员工合同
     */
    @JSONField(name = "contract_ids")
    @JsonProperty("contract_ids")
    private String contractIds;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 工牌 ID
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 出生日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "birthday" , format="yyyy-MM-dd")
    @JsonProperty("birthday")
    private Timestamp birthday;

    /**
     * 证书等级
     */
    @JSONField(name = "certificate")
    @JsonProperty("certificate")
    private String certificate;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 研究领域
     */
    @DEField(name = "study_field")
    @JSONField(name = "study_field")
    @JsonProperty("study_field")
    private String studyField;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 下一活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 出勤状态
     */
    @JSONField(name = "attendance_state")
    @JsonProperty("attendance_state")
    private String attendanceState;

    /**
     * 合同统计
     */
    @JSONField(name = "contracts_count")
    @JsonProperty("contracts_count")
    private Integer contractsCount;

    /**
     * 性别
     */
    @JSONField(name = "gender")
    @JsonProperty("gender")
    private String gender;

    /**
     * 照片
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 配偶生日
     */
    @DEField(name = "spouse_birthdate")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "spouse_birthdate" , format="yyyy-MM-dd")
    @JsonProperty("spouse_birthdate")
    private Timestamp spouseBirthdate;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 休假天数
     */
    @JSONField(name = "leaves_count")
    @JsonProperty("leaves_count")
    private Double leavesCount;

    /**
     * 备注
     */
    @JSONField(name = "notes")
    @JsonProperty("notes")
    private String notes;

    /**
     * 附加说明
     */
    @DEField(name = "additional_note")
    @JSONField(name = "additional_note")
    @JsonProperty("additional_note")
    private String additionalNote;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 体检日期
     */
    @DEField(name = "medic_exam")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "medic_exam" , format="yyyy-MM-dd")
    @JsonProperty("medic_exam")
    private Timestamp medicExam;

    /**
     * 签证到期日期
     */
    @DEField(name = "visa_expire")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "visa_expire" , format="yyyy-MM-dd")
    @JsonProperty("visa_expire")
    private Timestamp visaExpire;

    /**
     * 办公电话
     */
    @DEField(name = "work_phone")
    @JSONField(name = "work_phone")
    @JsonProperty("work_phone")
    private String workPhone;

    /**
     * 紧急联系人
     */
    @DEField(name = "emergency_contact")
    @JSONField(name = "emergency_contact")
    @JsonProperty("emergency_contact")
    private String emergencyContact;

    /**
     * 新近雇用的员工
     */
    @JSONField(name = "newly_hired_employee")
    @JsonProperty("newly_hired_employee")
    private String newlyHiredEmployee;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 已与公司地址相关联的员工住址
     */
    @JSONField(name = "is_address_home_a_company")
    @JsonProperty("is_address_home_a_company")
    private String isAddressHomeACompany;

    /**
     * 出勤
     */
    @JSONField(name = "attendance_ids")
    @JsonProperty("attendance_ids")
    private String attendanceIds;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 当前休假状态
     */
    @JSONField(name = "current_leave_state")
    @JsonProperty("current_leave_state")
    private String currentLeaveState;

    /**
     * 下属
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 信息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 手动出勤
     */
    @JSONField(name = "manual_attendance")
    @JsonProperty("manual_attendance")
    private String manualAttendance;

    /**
     * 剩余的法定休假
     */
    @JSONField(name = "remaining_leaves")
    @JsonProperty("remaining_leaves")
    private Double remainingLeaves;

    /**
     * 社会保障号SSN
     */
    @JSONField(name = "ssnid")
    @JsonProperty("ssnid")
    private String ssnid;

    /**
     * 工作头衔
     */
    @DEField(name = "job_title")
    @JSONField(name = "job_title")
    @JsonProperty("job_title")
    private String jobTitle;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 今日缺勤
     */
    @JSONField(name = "is_absent_totay")
    @JsonProperty("is_absent_totay")
    private String isAbsentTotay;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 护照号
     */
    @DEField(name = "passport_id")
    @JSONField(name = "passport_id")
    @JsonProperty("passport_id")
    private String passportId;

    /**
     * 员工人力资源目标
     */
    @JSONField(name = "goal_ids")
    @JsonProperty("goal_ids")
    private String goalIds;

    /**
     * 至日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "leave_date_to" , format="yyyy-MM-dd")
    @JsonProperty("leave_date_to")
    private Timestamp leaveDateTo;

    /**
     * 是管理者
     */
    @JSONField(name = "manager")
    @JsonProperty("manager")
    private String manager;

    /**
     * 工作地点
     */
    @DEField(name = "work_location")
    @JSONField(name = "work_location")
    @JsonProperty("work_location")
    private String workLocation;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 当前合同
     */
    @JSONField(name = "contract_id")
    @JsonProperty("contract_id")
    private Integer contractId;

    /**
     * 员工文档
     */
    @DEField(name = "google_drive_link")
    @JSONField(name = "google_drive_link")
    @JsonProperty("google_drive_link")
    private String googleDriveLink;

    /**
     * 工作许可编号
     */
    @DEField(name = "permit_no")
    @JSONField(name = "permit_no")
    @JsonProperty("permit_no")
    private String permitNo;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 身份证号
     */
    @DEField(name = "identification_id")
    @JSONField(name = "identification_id")
    @JsonProperty("identification_id")
    private String identificationId;

    /**
     * 公司汽车
     */
    @JSONField(name = "vehicle")
    @JsonProperty("vehicle")
    private String vehicle;

    /**
     * 标签
     */
    @JSONField(name = "category_ids")
    @JsonProperty("category_ids")
    private String categoryIds;

    /**
     * 能查看剩余的休假
     */
    @JSONField(name = "show_leaves")
    @JsonProperty("show_leaves")
    private String showLeaves;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 经理
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 家庭住址
     */
    @JSONField(name = "address_home_id_text")
    @JsonProperty("address_home_id_text")
    private String addressHomeIdText;

    /**
     * 时区
     */
    @JSONField(name = "tz")
    @JsonProperty("tz")
    private String tz;

    /**
     * 工作时间
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;

    /**
     * 用户
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 部门
     */
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;

    /**
     * 国籍
     */
    @JSONField(name = "country_of_birth_text")
    @JsonProperty("country_of_birth_text")
    private String countryOfBirthText;

    /**
     * 负责人
     */
    @JSONField(name = "expense_manager_id_text")
    @JsonProperty("expense_manager_id_text")
    private String expenseManagerIdText;

    /**
     * 工作岗位
     */
    @JSONField(name = "job_id_text")
    @JsonProperty("job_id_text")
    private String jobIdText;

    /**
     * 工作地址
     */
    @JSONField(name = "address_id_text")
    @JsonProperty("address_id_text")
    private String addressIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 教练
     */
    @JSONField(name = "coach_id_text")
    @JsonProperty("coach_id_text")
    private String coachIdText;

    /**
     * 国籍(国家)
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;

    /**
     * 家庭住址
     */
    @DEField(name = "address_home_id")
    @JSONField(name = "address_home_id")
    @JsonProperty("address_home_id")
    private Integer addressHomeId;

    /**
     * 负责人
     */
    @DEField(name = "expense_manager_id")
    @JSONField(name = "expense_manager_id")
    @JsonProperty("expense_manager_id")
    private Integer expenseManagerId;

    /**
     * 银行账户号码
     */
    @DEField(name = "bank_account_id")
    @JSONField(name = "bank_account_id")
    @JsonProperty("bank_account_id")
    private Integer bankAccountId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 国籍(国家)
     */
    @DEField(name = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 工作岗位
     */
    @DEField(name = "job_id")
    @JSONField(name = "job_id")
    @JsonProperty("job_id")
    private Integer jobId;

    /**
     * 资源
     */
    @DEField(name = "resource_id")
    @JSONField(name = "resource_id")
    @JsonProperty("resource_id")
    private Integer resourceId;

    /**
     * 用户
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 部门
     */
    @DEField(name = "department_id")
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Integer departmentId;

    /**
     * 经理
     */
    @DEField(name = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 上次出勤
     */
    @DEField(name = "last_attendance_id")
    @JSONField(name = "last_attendance_id")
    @JsonProperty("last_attendance_id")
    private Integer lastAttendanceId;

    /**
     * 教练
     */
    @DEField(name = "coach_id")
    @JSONField(name = "coach_id")
    @JsonProperty("coach_id")
    private Integer coachId;

    /**
     * 工作地址
     */
    @DEField(name = "address_id")
    @JSONField(name = "address_id")
    @JsonProperty("address_id")
    private Integer addressId;

    /**
     * 国籍
     */
    @DEField(name = "country_of_birth")
    @JSONField(name = "country_of_birth")
    @JsonProperty("country_of_birth")
    private Integer countryOfBirth;

    /**
     * 工作时间
     */
    @DEField(name = "resource_calendar_id")
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;


    /**
     * 
     */
    @JSONField(name = "odoolastattendance")
    @JsonProperty("odoolastattendance")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance odooLastAttendance;

    /**
     * 
     */
    @JSONField(name = "odoodepartment")
    @JsonProperty("odoodepartment")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department odooDepartment;

    /**
     * 
     */
    @JSONField(name = "odoocoach")
    @JsonProperty("odoocoach")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee odooCoach;

    /**
     * 
     */
    @JSONField(name = "odooparent")
    @JsonProperty("odooparent")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee odooParent;

    /**
     * 
     */
    @JSONField(name = "odoojob")
    @JsonProperty("odoojob")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job odooJob;

    /**
     * 
     */
    @JSONField(name = "odooresourcecalendar")
    @JsonProperty("odooresourcecalendar")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar odooResourceCalendar;

    /**
     * 
     */
    @JSONField(name = "odooresource")
    @JsonProperty("odooresource")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource odooResource;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocountry")
    @JsonProperty("odoocountry")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JSONField(name = "odoocountryof")
    @JsonProperty("odoocountryof")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country odooCountryOf;

    /**
     * 
     */
    @JSONField(name = "odoobankaccount")
    @JsonProperty("odoobankaccount")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank odooBankAccount;

    /**
     * 
     */
    @JSONField(name = "odooaddresshome")
    @JsonProperty("odooaddresshome")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooAddressHome;

    /**
     * 
     */
    @JSONField(name = "odooaddress")
    @JsonProperty("odooaddress")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooAddress;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odooexpensemanager")
    @JsonProperty("odooexpensemanager")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooExpenseManager;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [办公手机]
     */
    public void setMobilePhone(String mobilePhone){
        this.mobilePhone = mobilePhone ;
        this.modify("mobile_phone",mobilePhone);
    }
    /**
     * 设置 [子女数目]
     */
    public void setChildren(Integer children){
        this.children = children ;
        this.modify("children",children);
    }
    /**
     * 设置 [PIN]
     */
    public void setPin(String pin){
        this.pin = pin ;
        this.modify("pin",pin);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [毕业院校]
     */
    public void setStudySchool(String studySchool){
        this.studySchool = studySchool ;
        this.modify("study_school",studySchool);
    }
    /**
     * 设置 [婚姻状况]
     */
    public void setMarital(String marital){
        this.marital = marital ;
        this.modify("marital",marital);
    }
    /**
     * 设置 [紧急电话]
     */
    public void setEmergencyPhone(String emergencyPhone){
        this.emergencyPhone = emergencyPhone ;
        this.modify("emergency_phone",emergencyPhone);
    }
    /**
     * 设置 [上班距离]
     */
    public void setKmHomeWork(Integer kmHomeWork){
        this.kmHomeWork = kmHomeWork ;
        this.modify("km_home_work",kmHomeWork);
    }
    /**
     * 设置 [签证号]
     */
    public void setVisaNo(String visaNo){
        this.visaNo = visaNo ;
        this.modify("visa_no",visaNo);
    }
    /**
     * 设置 [出生地]
     */
    public void setPlaceOfBirth(String placeOfBirth){
        this.placeOfBirth = placeOfBirth ;
        this.modify("place_of_birth",placeOfBirth);
    }
    /**
     * 设置 [配偶全名]
     */
    public void setSpouseCompleteName(String spouseCompleteName){
        this.spouseCompleteName = spouseCompleteName ;
        this.modify("spouse_complete_name",spouseCompleteName);
    }
    /**
     * 设置 [社会保险号SIN]
     */
    public void setSinid(String sinid){
        this.sinid = sinid ;
        this.modify("sinid",sinid);
    }
    /**
     * 设置 [工作EMail]
     */
    public void setWorkEmail(String workEmail){
        this.workEmail = workEmail ;
        this.modify("work_email",workEmail);
    }
    /**
     * 设置 [工牌 ID]
     */
    public void setBarcode(String barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }
    /**
     * 设置 [出生日期]
     */
    public void setBirthday(Timestamp birthday){
        this.birthday = birthday ;
        this.modify("birthday",birthday);
    }
    /**
     * 设置 [证书等级]
     */
    public void setCertificate(String certificate){
        this.certificate = certificate ;
        this.modify("certificate",certificate);
    }
    /**
     * 设置 [研究领域]
     */
    public void setStudyField(String studyField){
        this.studyField = studyField ;
        this.modify("study_field",studyField);
    }
    /**
     * 设置 [性别]
     */
    public void setGender(String gender){
        this.gender = gender ;
        this.modify("gender",gender);
    }
    /**
     * 设置 [配偶生日]
     */
    public void setSpouseBirthdate(Timestamp spouseBirthdate){
        this.spouseBirthdate = spouseBirthdate ;
        this.modify("spouse_birthdate",spouseBirthdate);
    }
    /**
     * 设置 [备注]
     */
    public void setNotes(String notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }
    /**
     * 设置 [附加说明]
     */
    public void setAdditionalNote(String additionalNote){
        this.additionalNote = additionalNote ;
        this.modify("additional_note",additionalNote);
    }
    /**
     * 设置 [体检日期]
     */
    public void setMedicExam(Timestamp medicExam){
        this.medicExam = medicExam ;
        this.modify("medic_exam",medicExam);
    }
    /**
     * 设置 [签证到期日期]
     */
    public void setVisaExpire(Timestamp visaExpire){
        this.visaExpire = visaExpire ;
        this.modify("visa_expire",visaExpire);
    }
    /**
     * 设置 [办公电话]
     */
    public void setWorkPhone(String workPhone){
        this.workPhone = workPhone ;
        this.modify("work_phone",workPhone);
    }
    /**
     * 设置 [紧急联系人]
     */
    public void setEmergencyContact(String emergencyContact){
        this.emergencyContact = emergencyContact ;
        this.modify("emergency_contact",emergencyContact);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [社会保障号SSN]
     */
    public void setSsnid(String ssnid){
        this.ssnid = ssnid ;
        this.modify("ssnid",ssnid);
    }
    /**
     * 设置 [工作头衔]
     */
    public void setJobTitle(String jobTitle){
        this.jobTitle = jobTitle ;
        this.modify("job_title",jobTitle);
    }
    /**
     * 设置 [护照号]
     */
    public void setPassportId(String passportId){
        this.passportId = passportId ;
        this.modify("passport_id",passportId);
    }
    /**
     * 设置 [是管理者]
     */
    public void setManager(String manager){
        this.manager = manager ;
        this.modify("manager",manager);
    }
    /**
     * 设置 [工作地点]
     */
    public void setWorkLocation(String workLocation){
        this.workLocation = workLocation ;
        this.modify("work_location",workLocation);
    }
    /**
     * 设置 [员工文档]
     */
    public void setGoogleDriveLink(String googleDriveLink){
        this.googleDriveLink = googleDriveLink ;
        this.modify("google_drive_link",googleDriveLink);
    }
    /**
     * 设置 [工作许可编号]
     */
    public void setPermitNo(String permitNo){
        this.permitNo = permitNo ;
        this.modify("permit_no",permitNo);
    }
    /**
     * 设置 [身份证号]
     */
    public void setIdentificationId(String identificationId){
        this.identificationId = identificationId ;
        this.modify("identification_id",identificationId);
    }
    /**
     * 设置 [公司汽车]
     */
    public void setVehicle(String vehicle){
        this.vehicle = vehicle ;
        this.modify("vehicle",vehicle);
    }
    /**
     * 设置 [家庭住址]
     */
    public void setAddressHomeId(Integer addressHomeId){
        this.addressHomeId = addressHomeId ;
        this.modify("address_home_id",addressHomeId);
    }
    /**
     * 设置 [负责人]
     */
    public void setExpenseManagerId(Integer expenseManagerId){
        this.expenseManagerId = expenseManagerId ;
        this.modify("expense_manager_id",expenseManagerId);
    }
    /**
     * 设置 [银行账户号码]
     */
    public void setBankAccountId(Integer bankAccountId){
        this.bankAccountId = bankAccountId ;
        this.modify("bank_account_id",bankAccountId);
    }
    /**
     * 设置 [国籍(国家)]
     */
    public void setCountryId(Integer countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [工作岗位]
     */
    public void setJobId(Integer jobId){
        this.jobId = jobId ;
        this.modify("job_id",jobId);
    }
    /**
     * 设置 [资源]
     */
    public void setResourceId(Integer resourceId){
        this.resourceId = resourceId ;
        this.modify("resource_id",resourceId);
    }
    /**
     * 设置 [用户]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [部门]
     */
    public void setDepartmentId(Integer departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }
    /**
     * 设置 [经理]
     */
    public void setParentId(Integer parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }
    /**
     * 设置 [上次出勤]
     */
    public void setLastAttendanceId(Integer lastAttendanceId){
        this.lastAttendanceId = lastAttendanceId ;
        this.modify("last_attendance_id",lastAttendanceId);
    }
    /**
     * 设置 [教练]
     */
    public void setCoachId(Integer coachId){
        this.coachId = coachId ;
        this.modify("coach_id",coachId);
    }
    /**
     * 设置 [工作地址]
     */
    public void setAddressId(Integer addressId){
        this.addressId = addressId ;
        this.modify("address_id",addressId);
    }
    /**
     * 设置 [国籍]
     */
    public void setCountryOfBirth(Integer countryOfBirth){
        this.countryOfBirth = countryOfBirth ;
        this.modify("country_of_birth",countryOfBirth);
    }
    /**
     * 设置 [工作时间]
     */
    public void setResourceCalendarId(Integer resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }

}


