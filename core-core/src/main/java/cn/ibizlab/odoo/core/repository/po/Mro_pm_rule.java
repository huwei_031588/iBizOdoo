package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_ruleSearchContext;

/**
 * 实体 [Preventive Maintenance Rule] 存储模型
 */
public interface Mro_pm_rule{

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * Planning horizon (months)
     */
    Double getHorizon();

    void setHorizon(Double horizon);

    /**
     * 获取 [Planning horizon (months)]脏标记
     */
    boolean getHorizonDirtyFlag();

    /**
     * 任务
     */
    String getPm_rules_line_ids();

    void setPm_rules_line_ids(String pm_rules_line_ids);

    /**
     * 获取 [任务]脏标记
     */
    boolean getPm_rules_line_idsDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * Parameter
     */
    String getParameter_id_text();

    void setParameter_id_text(String parameter_id_text);

    /**
     * 获取 [Parameter]脏标记
     */
    boolean getParameter_id_textDirtyFlag();

    /**
     * Asset Category
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [Asset Category]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 单位
     */
    Integer getParameter_uom();

    void setParameter_uom(Integer parameter_uom);

    /**
     * 获取 [单位]脏标记
     */
    boolean getParameter_uomDirtyFlag();

    /**
     * Parameter
     */
    Integer getParameter_id();

    void setParameter_id(Integer parameter_id);

    /**
     * 获取 [Parameter]脏标记
     */
    boolean getParameter_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * Asset Category
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [Asset Category]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
