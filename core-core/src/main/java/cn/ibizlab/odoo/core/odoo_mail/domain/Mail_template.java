package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [EMail模板] 对象
 */
@Data
public class Mail_template extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 默认收件人
     */
    @DEField(name = "use_default_to")
    @JSONField(name = "use_default_to")
    @JsonProperty("use_default_to")
    private String useDefaultTo;

    /**
     * 主题
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 附件
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 回复 至
     */
    @DEField(name = "reply_to")
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;

    /**
     * 子模型
     */
    @DEField(name = "sub_object")
    @JSONField(name = "sub_object")
    @JsonProperty("sub_object")
    private Integer subObject;

    /**
     * 计划日期
     */
    @DEField(name = "scheduled_date")
    @JSONField(name = "scheduled_date")
    @JsonProperty("scheduled_date")
    private String scheduledDate;

    /**
     * 应用于
     */
    @DEField(name = "model_id")
    @JSONField(name = "model_id")
    @JsonProperty("model_id")
    private Integer modelId;

    /**
     * 邮件发送服务器
     */
    @DEField(name = "mail_server_id")
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;

    /**
     * 可选的打印和附加报表
     */
    @DEField(name = "report_template")
    @JSONField(name = "report_template")
    @JsonProperty("report_template")
    private Integer reportTemplate;

    /**
     * 正文
     */
    @DEField(name = "body_html")
    @JSONField(name = "body_html")
    @JsonProperty("body_html")
    private String bodyHtml;

    /**
     * 边栏操作
     */
    @DEField(name = "ref_ir_act_window")
    @JSONField(name = "ref_ir_act_window")
    @JsonProperty("ref_ir_act_window")
    private Integer refIrActWindow;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 至（合作伙伴）
     */
    @DEField(name = "partner_to")
    @JSONField(name = "partner_to")
    @JsonProperty("partner_to")
    private String partnerTo;

    /**
     * 相关的文档模型
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 从
     */
    @DEField(name = "email_from")
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    /**
     * 自动删除
     */
    @DEField(name = "auto_delete")
    @JSONField(name = "auto_delete")
    @JsonProperty("auto_delete")
    private String autoDelete;

    /**
     * 抄送
     */
    @DEField(name = "email_cc")
    @JSONField(name = "email_cc")
    @JsonProperty("email_cc")
    private String emailCc;

    /**
     * 报告文件名
     */
    @DEField(name = "report_name")
    @JSONField(name = "report_name")
    @JsonProperty("report_name")
    private String reportName;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 字段
     */
    @DEField(name = "model_object_field")
    @JSONField(name = "model_object_field")
    @JsonProperty("model_object_field")
    private Integer modelObjectField;

    /**
     * 至（EMail）
     */
    @DEField(name = "email_to")
    @JSONField(name = "email_to")
    @JsonProperty("email_to")
    private String emailTo;

    /**
     * 默认值
     */
    @DEField(name = "null_value")
    @JSONField(name = "null_value")
    @JsonProperty("null_value")
    private String nullValue;

    /**
     * 占位符表达式
     */
    @JSONField(name = "copyvalue")
    @JsonProperty("copyvalue")
    private String copyvalue;

    /**
     * 添加签名
     */
    @DEField(name = "user_signature")
    @JSONField(name = "user_signature")
    @JsonProperty("user_signature")
    private String userSignature;

    /**
     * 子字段
     */
    @DEField(name = "sub_model_object_field")
    @JSONField(name = "sub_model_object_field")
    @JsonProperty("sub_model_object_field")
    private Integer subModelObjectField;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 语言
     */
    @JSONField(name = "lang")
    @JsonProperty("lang")
    private String lang;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [默认收件人]
     */
    public void setUseDefaultTo(String useDefaultTo){
        this.useDefaultTo = useDefaultTo ;
        this.modify("use_default_to",useDefaultTo);
    }
    /**
     * 设置 [主题]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }
    /**
     * 设置 [回复 至]
     */
    public void setReplyTo(String replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }
    /**
     * 设置 [子模型]
     */
    public void setSubObject(Integer subObject){
        this.subObject = subObject ;
        this.modify("sub_object",subObject);
    }
    /**
     * 设置 [计划日期]
     */
    public void setScheduledDate(String scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }
    /**
     * 设置 [应用于]
     */
    public void setModelId(Integer modelId){
        this.modelId = modelId ;
        this.modify("model_id",modelId);
    }
    /**
     * 设置 [邮件发送服务器]
     */
    public void setMailServerId(Integer mailServerId){
        this.mailServerId = mailServerId ;
        this.modify("mail_server_id",mailServerId);
    }
    /**
     * 设置 [可选的打印和附加报表]
     */
    public void setReportTemplate(Integer reportTemplate){
        this.reportTemplate = reportTemplate ;
        this.modify("report_template",reportTemplate);
    }
    /**
     * 设置 [正文]
     */
    public void setBodyHtml(String bodyHtml){
        this.bodyHtml = bodyHtml ;
        this.modify("body_html",bodyHtml);
    }
    /**
     * 设置 [边栏操作]
     */
    public void setRefIrActWindow(Integer refIrActWindow){
        this.refIrActWindow = refIrActWindow ;
        this.modify("ref_ir_act_window",refIrActWindow);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [至（合作伙伴）]
     */
    public void setPartnerTo(String partnerTo){
        this.partnerTo = partnerTo ;
        this.modify("partner_to",partnerTo);
    }
    /**
     * 设置 [相关的文档模型]
     */
    public void setModel(String model){
        this.model = model ;
        this.modify("model",model);
    }
    /**
     * 设置 [从]
     */
    public void setEmailFrom(String emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }
    /**
     * 设置 [自动删除]
     */
    public void setAutoDelete(String autoDelete){
        this.autoDelete = autoDelete ;
        this.modify("auto_delete",autoDelete);
    }
    /**
     * 设置 [抄送]
     */
    public void setEmailCc(String emailCc){
        this.emailCc = emailCc ;
        this.modify("email_cc",emailCc);
    }
    /**
     * 设置 [报告文件名]
     */
    public void setReportName(String reportName){
        this.reportName = reportName ;
        this.modify("report_name",reportName);
    }
    /**
     * 设置 [字段]
     */
    public void setModelObjectField(Integer modelObjectField){
        this.modelObjectField = modelObjectField ;
        this.modify("model_object_field",modelObjectField);
    }
    /**
     * 设置 [至（EMail）]
     */
    public void setEmailTo(String emailTo){
        this.emailTo = emailTo ;
        this.modify("email_to",emailTo);
    }
    /**
     * 设置 [默认值]
     */
    public void setNullValue(String nullValue){
        this.nullValue = nullValue ;
        this.modify("null_value",nullValue);
    }
    /**
     * 设置 [占位符表达式]
     */
    public void setCopyvalue(String copyvalue){
        this.copyvalue = copyvalue ;
        this.modify("copyvalue",copyvalue);
    }
    /**
     * 设置 [添加签名]
     */
    public void setUserSignature(String userSignature){
        this.userSignature = userSignature ;
        this.modify("user_signature",userSignature);
    }
    /**
     * 设置 [子字段]
     */
    public void setSubModelObjectField(Integer subModelObjectField){
        this.subModelObjectField = subModelObjectField ;
        this.modify("sub_model_object_field",subModelObjectField);
    }
    /**
     * 设置 [语言]
     */
    public void setLang(String lang){
        this.lang = lang ;
        this.modify("lang",lang);
    }

}


