package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_ruleSearchContext;

/**
 * 实体 [库存规则] 存储模型
 */
public interface Stock_rule{

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 移动供应方法
     */
    String getProcure_method();

    void setProcure_method(String procure_method);

    /**
     * 获取 [移动供应方法]脏标记
     */
    boolean getProcure_methodDirtyFlag();

    /**
     * 传播取消以及拆分
     */
    String getPropagate();

    void setPropagate(String propagate);

    /**
     * 获取 [传播取消以及拆分]脏标记
     */
    boolean getPropagateDirtyFlag();

    /**
     * 固定的补货组
     */
    Integer getGroup_id();

    void setGroup_id(Integer group_id);

    /**
     * 获取 [固定的补货组]脏标记
     */
    boolean getGroup_idDirtyFlag();

    /**
     * 规则消息
     */
    String getRule_message();

    void setRule_message(String rule_message);

    /**
     * 获取 [规则消息]脏标记
     */
    boolean getRule_messageDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 自动移动
     */
    String getAuto();

    void setAuto(String auto);

    /**
     * 获取 [自动移动]脏标记
     */
    boolean getAutoDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 延迟
     */
    Integer getDelay();

    void setDelay(Integer delay);

    /**
     * 获取 [延迟]脏标记
     */
    boolean getDelayDirtyFlag();

    /**
     * 补货组的传播
     */
    String getGroup_propagation_option();

    void setGroup_propagation_option(String group_propagation_option);

    /**
     * 获取 [补货组的传播]脏标记
     */
    boolean getGroup_propagation_optionDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 动作
     */
    String getAction();

    void setAction(String action);

    /**
     * 获取 [动作]脏标记
     */
    boolean getActionDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 业务伙伴地址
     */
    String getPartner_address_id_text();

    void setPartner_address_id_text(String partner_address_id_text);

    /**
     * 获取 [业务伙伴地址]脏标记
     */
    boolean getPartner_address_id_textDirtyFlag();

    /**
     * 目的位置
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [目的位置]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 路线
     */
    String getRoute_id_text();

    void setRoute_id_text(String route_id_text);

    /**
     * 获取 [路线]脏标记
     */
    boolean getRoute_id_textDirtyFlag();

    /**
     * 路线序列
     */
    Integer getRoute_sequence();

    void setRoute_sequence(Integer route_sequence);

    /**
     * 获取 [路线序列]脏标记
     */
    boolean getRoute_sequenceDirtyFlag();

    /**
     * 作业类型
     */
    String getPicking_type_id_text();

    void setPicking_type_id_text(String picking_type_id_text);

    /**
     * 获取 [作业类型]脏标记
     */
    boolean getPicking_type_id_textDirtyFlag();

    /**
     * 传播的仓库
     */
    String getPropagate_warehouse_id_text();

    void setPropagate_warehouse_id_text(String propagate_warehouse_id_text);

    /**
     * 获取 [传播的仓库]脏标记
     */
    boolean getPropagate_warehouse_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 源位置
     */
    String getLocation_src_id_text();

    void setLocation_src_id_text(String location_src_id_text);

    /**
     * 获取 [源位置]脏标记
     */
    boolean getLocation_src_id_textDirtyFlag();

    /**
     * 仓库
     */
    String getWarehouse_id_text();

    void setWarehouse_id_text(String warehouse_id_text);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_id_textDirtyFlag();

    /**
     * 目的位置
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [目的位置]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 业务伙伴地址
     */
    Integer getPartner_address_id();

    void setPartner_address_id(Integer partner_address_id);

    /**
     * 获取 [业务伙伴地址]脏标记
     */
    boolean getPartner_address_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 仓库
     */
    Integer getWarehouse_id();

    void setWarehouse_id(Integer warehouse_id);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 作业类型
     */
    Integer getPicking_type_id();

    void setPicking_type_id(Integer picking_type_id);

    /**
     * 获取 [作业类型]脏标记
     */
    boolean getPicking_type_idDirtyFlag();

    /**
     * 路线
     */
    Integer getRoute_id();

    void setRoute_id(Integer route_id);

    /**
     * 获取 [路线]脏标记
     */
    boolean getRoute_idDirtyFlag();

    /**
     * 源位置
     */
    Integer getLocation_src_id();

    void setLocation_src_id(Integer location_src_id);

    /**
     * 获取 [源位置]脏标记
     */
    boolean getLocation_src_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 传播的仓库
     */
    Integer getPropagate_warehouse_id();

    void setPropagate_warehouse_id(Integer propagate_warehouse_id);

    /**
     * 获取 [传播的仓库]脏标记
     */
    boolean getPropagate_warehouse_idDirtyFlag();

}
