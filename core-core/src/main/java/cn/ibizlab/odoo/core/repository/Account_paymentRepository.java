package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_paymentSearchContext;

/**
 * 实体 [付款] 存储对象
 */
public interface Account_paymentRepository extends Repository<Account_payment> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_payment> searchDefault(Account_paymentSearchContext context);

    Account_payment convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_payment domain , Account_payment po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_payment convert2Domain( Account_payment po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_payment domain) ;

}
