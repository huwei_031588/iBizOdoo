package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_repairSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qty_repairService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_warn_insufficient_qty_repairOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_warn_insufficient_qty_repairClientModel;

/**
 * 实体[警告维修数量不足] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warn_insufficient_qty_repairServiceImpl implements IStock_warn_insufficient_qty_repairService {

    @Autowired
    stock_warn_insufficient_qty_repairOdooClient stock_warn_insufficient_qty_repairOdooClient;


    @Override
    public boolean remove(Integer id) {
        stock_warn_insufficient_qty_repairClientModel clientModel = new stock_warn_insufficient_qty_repairClientModel();
        clientModel.setId(id);
		stock_warn_insufficient_qty_repairOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Stock_warn_insufficient_qty_repair get(Integer id) {
        stock_warn_insufficient_qty_repairClientModel clientModel = new stock_warn_insufficient_qty_repairClientModel();
        clientModel.setId(id);
		stock_warn_insufficient_qty_repairOdooClient.get(clientModel);
        Stock_warn_insufficient_qty_repair et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_warn_insufficient_qty_repair();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Stock_warn_insufficient_qty_repair et) {
        stock_warn_insufficient_qty_repairClientModel clientModel = convert2Model(et,null);
		stock_warn_insufficient_qty_repairOdooClient.create(clientModel);
        Stock_warn_insufficient_qty_repair rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warn_insufficient_qty_repair> list){
    }

    @Override
    public boolean update(Stock_warn_insufficient_qty_repair et) {
        stock_warn_insufficient_qty_repairClientModel clientModel = convert2Model(et,null);
		stock_warn_insufficient_qty_repairOdooClient.update(clientModel);
        Stock_warn_insufficient_qty_repair rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_warn_insufficient_qty_repair> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warn_insufficient_qty_repair> searchDefault(Stock_warn_insufficient_qty_repairSearchContext context) {
        List<Stock_warn_insufficient_qty_repair> list = new ArrayList<Stock_warn_insufficient_qty_repair>();
        Page<stock_warn_insufficient_qty_repairClientModel> clientModelList = stock_warn_insufficient_qty_repairOdooClient.search(context);
        for(stock_warn_insufficient_qty_repairClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_warn_insufficient_qty_repair>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_warn_insufficient_qty_repairClientModel convert2Model(Stock_warn_insufficient_qty_repair domain , stock_warn_insufficient_qty_repairClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_warn_insufficient_qty_repairClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("quant_idsdirtyflag"))
                model.setQuant_ids(domain.getQuantIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("repair_id_textdirtyflag"))
                model.setRepair_id_text(domain.getRepairIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("repair_iddirtyflag"))
                model.setRepair_id(domain.getRepairId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_warn_insufficient_qty_repair convert2Domain( stock_warn_insufficient_qty_repairClientModel model ,Stock_warn_insufficient_qty_repair domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_warn_insufficient_qty_repair();
        }

        if(model.getQuant_idsDirtyFlag())
            domain.setQuantIds(model.getQuant_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getRepair_id_textDirtyFlag())
            domain.setRepairIdText(model.getRepair_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getRepair_idDirtyFlag())
            domain.setRepairId(model.getRepair_id());
        return domain ;
    }

}

    



