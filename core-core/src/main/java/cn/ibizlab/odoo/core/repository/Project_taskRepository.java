package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Project_task;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_taskSearchContext;

/**
 * 实体 [任务] 存储对象
 */
public interface Project_taskRepository extends Repository<Project_task> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Project_task> searchDefault(Project_taskSearchContext context);

    Project_task convert2PO(cn.ibizlab.odoo.core.odoo_project.domain.Project_task domain , Project_task po) ;

    cn.ibizlab.odoo.core.odoo_project.domain.Project_task convert2Domain( Project_task po ,cn.ibizlab.odoo.core.odoo_project.domain.Project_task domain) ;

}
