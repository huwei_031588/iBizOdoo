package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.maintenance_equipment_category;

/**
 * 实体[maintenance_equipment_category] 服务对象接口
 */
public interface maintenance_equipment_categoryRepository{


    public maintenance_equipment_category createPO() ;
        public void createBatch(maintenance_equipment_category maintenance_equipment_category);

        public void create(maintenance_equipment_category maintenance_equipment_category);

        public List<maintenance_equipment_category> search();

        public void update(maintenance_equipment_category maintenance_equipment_category);

        public void get(String id);

        public void updateBatch(maintenance_equipment_category maintenance_equipment_category);

        public void removeBatch(String id);

        public void remove(String id);


}
