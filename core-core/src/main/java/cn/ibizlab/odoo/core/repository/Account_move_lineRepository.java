package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_move_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_lineSearchContext;

/**
 * 实体 [日记账项目] 存储对象
 */
public interface Account_move_lineRepository extends Repository<Account_move_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_move_line> searchDefault(Account_move_lineSearchContext context);

    Account_move_line convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line domain , Account_move_line po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line convert2Domain( Account_move_line po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line domain) ;

}
