package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_attendance] 对象
 */
public interface hr_attendance {

    public Timestamp getCheck_in();

    public void setCheck_in(Timestamp check_in);

    public Timestamp getCheck_out();

    public void setCheck_out(Timestamp check_out);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getDepartment_id();

    public void setDepartment_id(Integer department_id);

    public String getDepartment_id_text();

    public void setDepartment_id_text(String department_id_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getEmployee_id();

    public void setEmployee_id(Integer employee_id);

    public String getEmployee_id_text();

    public void setEmployee_id_text(String employee_id_text);

    public Integer getId();

    public void setId(Integer id);

    public Double getWorked_hours();

    public void setWorked_hours(Double worked_hours);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
