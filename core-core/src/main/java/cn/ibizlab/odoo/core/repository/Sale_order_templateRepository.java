package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Sale_order_template;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_templateSearchContext;

/**
 * 实体 [报价单模板] 存储对象
 */
public interface Sale_order_templateRepository extends Repository<Sale_order_template> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Sale_order_template> searchDefault(Sale_order_templateSearchContext context);

    Sale_order_template convert2PO(cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template domain , Sale_order_template po) ;

    cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template convert2Domain( Sale_order_template po ,cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template domain) ;

}
