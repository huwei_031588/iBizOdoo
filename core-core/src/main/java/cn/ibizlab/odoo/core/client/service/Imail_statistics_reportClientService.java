package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_statistics_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_statistics_report] 服务对象接口
 */
public interface Imail_statistics_reportClientService{

    public Imail_statistics_report createModel() ;

    public void get(Imail_statistics_report mail_statistics_report);

    public void remove(Imail_statistics_report mail_statistics_report);

    public Page<Imail_statistics_report> search(SearchContext context);

    public void updateBatch(List<Imail_statistics_report> mail_statistics_reports);

    public void removeBatch(List<Imail_statistics_report> mail_statistics_reports);

    public void create(Imail_statistics_report mail_statistics_report);

    public void createBatch(List<Imail_statistics_report> mail_statistics_reports);

    public void update(Imail_statistics_report mail_statistics_report);

    public Page<Imail_statistics_report> select(SearchContext context);

}
