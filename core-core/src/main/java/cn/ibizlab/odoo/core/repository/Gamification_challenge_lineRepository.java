package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Gamification_challenge_line;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challenge_lineSearchContext;

/**
 * 实体 [游戏化挑战的一般目标] 存储对象
 */
public interface Gamification_challenge_lineRepository extends Repository<Gamification_challenge_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Gamification_challenge_line> searchDefault(Gamification_challenge_lineSearchContext context);

    Gamification_challenge_line convert2PO(cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line domain , Gamification_challenge_line po) ;

    cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line convert2Domain( Gamification_challenge_line po ,cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line domain) ;

}
