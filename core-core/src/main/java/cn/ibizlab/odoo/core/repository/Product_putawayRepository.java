package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_putaway;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_putawaySearchContext;

/**
 * 实体 [上架策略] 存储对象
 */
public interface Product_putawayRepository extends Repository<Product_putaway> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_putaway> searchDefault(Product_putawaySearchContext context);

    Product_putaway convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway domain , Product_putaway po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway convert2Domain( Product_putaway po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway domain) ;

}
