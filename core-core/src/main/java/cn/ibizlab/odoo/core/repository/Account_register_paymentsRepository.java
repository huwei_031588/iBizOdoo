package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_register_payments;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_register_paymentsSearchContext;

/**
 * 实体 [登记付款] 存储对象
 */
public interface Account_register_paymentsRepository extends Repository<Account_register_payments> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_register_payments> searchDefault(Account_register_paymentsSearchContext context);

    Account_register_payments convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments domain , Account_register_payments po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments convert2Domain( Account_register_payments po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments domain) ;

}
