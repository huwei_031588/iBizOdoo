package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_attribute;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attributeSearchContext;

/**
 * 实体 [产品属性] 存储对象
 */
public interface Product_attributeRepository extends Repository<Product_attribute> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_attribute> searchDefault(Product_attributeSearchContext context);

    Product_attribute convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute domain , Product_attribute po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute convert2Domain( Product_attribute po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute domain) ;

}
