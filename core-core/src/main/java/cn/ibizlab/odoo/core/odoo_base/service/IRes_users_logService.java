package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_users_log;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_users_logSearchContext;


/**
 * 实体[Res_users_log] 服务对象接口
 */
public interface IRes_users_logService{

    boolean update(Res_users_log et) ;
    void updateBatch(List<Res_users_log> list) ;
    Res_users_log get(Integer key) ;
    boolean create(Res_users_log et) ;
    void createBatch(List<Res_users_log> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Res_users_log> searchDefault(Res_users_logSearchContext context) ;

}



