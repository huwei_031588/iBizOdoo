package cn.ibizlab.odoo.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_cancelSearchContext;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_cancelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_repair.client.repair_cancelOdooClient;
import cn.ibizlab.odoo.core.odoo_repair.clientmodel.repair_cancelClientModel;

/**
 * 实体[取消维修] 服务对象接口实现
 */
@Slf4j
@Service
public class Repair_cancelServiceImpl implements IRepair_cancelService {

    @Autowired
    repair_cancelOdooClient repair_cancelOdooClient;


    @Override
    public boolean update(Repair_cancel et) {
        repair_cancelClientModel clientModel = convert2Model(et,null);
		repair_cancelOdooClient.update(clientModel);
        Repair_cancel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Repair_cancel> list){
    }

    @Override
    public boolean create(Repair_cancel et) {
        repair_cancelClientModel clientModel = convert2Model(et,null);
		repair_cancelOdooClient.create(clientModel);
        Repair_cancel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Repair_cancel> list){
    }

    @Override
    public Repair_cancel get(Integer id) {
        repair_cancelClientModel clientModel = new repair_cancelClientModel();
        clientModel.setId(id);
		repair_cancelOdooClient.get(clientModel);
        Repair_cancel et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Repair_cancel();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        repair_cancelClientModel clientModel = new repair_cancelClientModel();
        clientModel.setId(id);
		repair_cancelOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Repair_cancel> searchDefault(Repair_cancelSearchContext context) {
        List<Repair_cancel> list = new ArrayList<Repair_cancel>();
        Page<repair_cancelClientModel> clientModelList = repair_cancelOdooClient.search(context);
        for(repair_cancelClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Repair_cancel>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public repair_cancelClientModel convert2Model(Repair_cancel domain , repair_cancelClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new repair_cancelClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Repair_cancel convert2Domain( repair_cancelClientModel model ,Repair_cancel domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Repair_cancel();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



