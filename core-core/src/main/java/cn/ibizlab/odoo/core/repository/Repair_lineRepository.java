package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Repair_line;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_lineSearchContext;

/**
 * 实体 [修理明细行(零件)] 存储对象
 */
public interface Repair_lineRepository extends Repository<Repair_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Repair_line> searchDefault(Repair_lineSearchContext context);

    Repair_line convert2PO(cn.ibizlab.odoo.core.odoo_repair.domain.Repair_line domain , Repair_line po) ;

    cn.ibizlab.odoo.core.odoo_repair.domain.Repair_line convert2Domain( Repair_line po ,cn.ibizlab.odoo.core.odoo_repair.domain.Repair_line domain) ;

}
