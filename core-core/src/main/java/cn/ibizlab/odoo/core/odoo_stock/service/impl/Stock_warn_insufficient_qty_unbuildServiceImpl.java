package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_unbuildSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qty_unbuildService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_warn_insufficient_qty_unbuildOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_warn_insufficient_qty_unbuildClientModel;

/**
 * 实体[拆解数量短缺警告] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warn_insufficient_qty_unbuildServiceImpl implements IStock_warn_insufficient_qty_unbuildService {

    @Autowired
    stock_warn_insufficient_qty_unbuildOdooClient stock_warn_insufficient_qty_unbuildOdooClient;


    @Override
    public boolean update(Stock_warn_insufficient_qty_unbuild et) {
        stock_warn_insufficient_qty_unbuildClientModel clientModel = convert2Model(et,null);
		stock_warn_insufficient_qty_unbuildOdooClient.update(clientModel);
        Stock_warn_insufficient_qty_unbuild rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_warn_insufficient_qty_unbuild> list){
    }

    @Override
    public boolean create(Stock_warn_insufficient_qty_unbuild et) {
        stock_warn_insufficient_qty_unbuildClientModel clientModel = convert2Model(et,null);
		stock_warn_insufficient_qty_unbuildOdooClient.create(clientModel);
        Stock_warn_insufficient_qty_unbuild rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warn_insufficient_qty_unbuild> list){
    }

    @Override
    public Stock_warn_insufficient_qty_unbuild get(Integer id) {
        stock_warn_insufficient_qty_unbuildClientModel clientModel = new stock_warn_insufficient_qty_unbuildClientModel();
        clientModel.setId(id);
		stock_warn_insufficient_qty_unbuildOdooClient.get(clientModel);
        Stock_warn_insufficient_qty_unbuild et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_warn_insufficient_qty_unbuild();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        stock_warn_insufficient_qty_unbuildClientModel clientModel = new stock_warn_insufficient_qty_unbuildClientModel();
        clientModel.setId(id);
		stock_warn_insufficient_qty_unbuildOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warn_insufficient_qty_unbuild> searchDefault(Stock_warn_insufficient_qty_unbuildSearchContext context) {
        List<Stock_warn_insufficient_qty_unbuild> list = new ArrayList<Stock_warn_insufficient_qty_unbuild>();
        Page<stock_warn_insufficient_qty_unbuildClientModel> clientModelList = stock_warn_insufficient_qty_unbuildOdooClient.search(context);
        for(stock_warn_insufficient_qty_unbuildClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_warn_insufficient_qty_unbuild>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_warn_insufficient_qty_unbuildClientModel convert2Model(Stock_warn_insufficient_qty_unbuild domain , stock_warn_insufficient_qty_unbuildClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_warn_insufficient_qty_unbuildClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("quant_idsdirtyflag"))
                model.setQuant_ids(domain.getQuantIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("unbuild_id_textdirtyflag"))
                model.setUnbuild_id_text(domain.getUnbuildIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("unbuild_iddirtyflag"))
                model.setUnbuild_id(domain.getUnbuildId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_warn_insufficient_qty_unbuild convert2Domain( stock_warn_insufficient_qty_unbuildClientModel model ,Stock_warn_insufficient_qty_unbuild domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_warn_insufficient_qty_unbuild();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getQuant_idsDirtyFlag())
            domain.setQuantIds(model.getQuant_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getUnbuild_id_textDirtyFlag())
            domain.setUnbuildIdText(model.getUnbuild_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getUnbuild_idDirtyFlag())
            domain.setUnbuildId(model.getUnbuild_id());
        return domain ;
    }

}

    



