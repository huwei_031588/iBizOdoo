package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_partial_reconcile] 对象
 */
public class account_partial_reconcileClientModel implements Serializable{

    /**
     * 金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 外币金额
     */
    public Double amount_currency;

    @JsonIgnore
    public boolean amount_currencyDirtyFlag;
    
    /**
     * 公司货币
     */
    public Integer company_currency_id;

    @JsonIgnore
    public boolean company_currency_idDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 贷方凭证
     */
    public Integer credit_move_id;

    @JsonIgnore
    public boolean credit_move_idDirtyFlag;
    
    /**
     * 贷方凭证
     */
    public String credit_move_id_text;

    @JsonIgnore
    public boolean credit_move_id_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 借方移动
     */
    public Integer debit_move_id;

    @JsonIgnore
    public boolean debit_move_idDirtyFlag;
    
    /**
     * 借方移动
     */
    public String debit_move_id_text;

    @JsonIgnore
    public boolean debit_move_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 完全核销
     */
    public Integer full_reconcile_id;

    @JsonIgnore
    public boolean full_reconcile_idDirtyFlag;
    
    /**
     * 完全核销
     */
    public String full_reconcile_id_text;

    @JsonIgnore
    public boolean full_reconcile_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 匹配明细的最大时间
     */
    public Timestamp max_date;

    @JsonIgnore
    public boolean max_dateDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [外币金额]
     */
    @JsonProperty("amount_currency")
    public Double getAmount_currency(){
        return this.amount_currency ;
    }

    /**
     * 设置 [外币金额]
     */
    @JsonProperty("amount_currency")
    public void setAmount_currency(Double  amount_currency){
        this.amount_currency = amount_currency ;
        this.amount_currencyDirtyFlag = true ;
    }

     /**
     * 获取 [外币金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_currencyDirtyFlag(){
        return this.amount_currencyDirtyFlag ;
    }   

    /**
     * 获取 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return this.company_currency_id ;
    }

    /**
     * 设置 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司货币]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return this.company_currency_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [贷方凭证]
     */
    @JsonProperty("credit_move_id")
    public Integer getCredit_move_id(){
        return this.credit_move_id ;
    }

    /**
     * 设置 [贷方凭证]
     */
    @JsonProperty("credit_move_id")
    public void setCredit_move_id(Integer  credit_move_id){
        this.credit_move_id = credit_move_id ;
        this.credit_move_idDirtyFlag = true ;
    }

     /**
     * 获取 [贷方凭证]脏标记
     */
    @JsonIgnore
    public boolean getCredit_move_idDirtyFlag(){
        return this.credit_move_idDirtyFlag ;
    }   

    /**
     * 获取 [贷方凭证]
     */
    @JsonProperty("credit_move_id_text")
    public String getCredit_move_id_text(){
        return this.credit_move_id_text ;
    }

    /**
     * 设置 [贷方凭证]
     */
    @JsonProperty("credit_move_id_text")
    public void setCredit_move_id_text(String  credit_move_id_text){
        this.credit_move_id_text = credit_move_id_text ;
        this.credit_move_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [贷方凭证]脏标记
     */
    @JsonIgnore
    public boolean getCredit_move_id_textDirtyFlag(){
        return this.credit_move_id_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [借方移动]
     */
    @JsonProperty("debit_move_id")
    public Integer getDebit_move_id(){
        return this.debit_move_id ;
    }

    /**
     * 设置 [借方移动]
     */
    @JsonProperty("debit_move_id")
    public void setDebit_move_id(Integer  debit_move_id){
        this.debit_move_id = debit_move_id ;
        this.debit_move_idDirtyFlag = true ;
    }

     /**
     * 获取 [借方移动]脏标记
     */
    @JsonIgnore
    public boolean getDebit_move_idDirtyFlag(){
        return this.debit_move_idDirtyFlag ;
    }   

    /**
     * 获取 [借方移动]
     */
    @JsonProperty("debit_move_id_text")
    public String getDebit_move_id_text(){
        return this.debit_move_id_text ;
    }

    /**
     * 设置 [借方移动]
     */
    @JsonProperty("debit_move_id_text")
    public void setDebit_move_id_text(String  debit_move_id_text){
        this.debit_move_id_text = debit_move_id_text ;
        this.debit_move_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [借方移动]脏标记
     */
    @JsonIgnore
    public boolean getDebit_move_id_textDirtyFlag(){
        return this.debit_move_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [完全核销]
     */
    @JsonProperty("full_reconcile_id")
    public Integer getFull_reconcile_id(){
        return this.full_reconcile_id ;
    }

    /**
     * 设置 [完全核销]
     */
    @JsonProperty("full_reconcile_id")
    public void setFull_reconcile_id(Integer  full_reconcile_id){
        this.full_reconcile_id = full_reconcile_id ;
        this.full_reconcile_idDirtyFlag = true ;
    }

     /**
     * 获取 [完全核销]脏标记
     */
    @JsonIgnore
    public boolean getFull_reconcile_idDirtyFlag(){
        return this.full_reconcile_idDirtyFlag ;
    }   

    /**
     * 获取 [完全核销]
     */
    @JsonProperty("full_reconcile_id_text")
    public String getFull_reconcile_id_text(){
        return this.full_reconcile_id_text ;
    }

    /**
     * 设置 [完全核销]
     */
    @JsonProperty("full_reconcile_id_text")
    public void setFull_reconcile_id_text(String  full_reconcile_id_text){
        this.full_reconcile_id_text = full_reconcile_id_text ;
        this.full_reconcile_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [完全核销]脏标记
     */
    @JsonIgnore
    public boolean getFull_reconcile_id_textDirtyFlag(){
        return this.full_reconcile_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [匹配明细的最大时间]
     */
    @JsonProperty("max_date")
    public Timestamp getMax_date(){
        return this.max_date ;
    }

    /**
     * 设置 [匹配明细的最大时间]
     */
    @JsonProperty("max_date")
    public void setMax_date(Timestamp  max_date){
        this.max_date = max_date ;
        this.max_dateDirtyFlag = true ;
    }

     /**
     * 获取 [匹配明细的最大时间]脏标记
     */
    @JsonIgnore
    public boolean getMax_dateDirtyFlag(){
        return this.max_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("amount") instanceof Boolean)&& map.get("amount")!=null){
			this.setAmount((Double)map.get("amount"));
		}
		if(!(map.get("amount_currency") instanceof Boolean)&& map.get("amount_currency")!=null){
			this.setAmount_currency((Double)map.get("amount_currency"));
		}
		if(!(map.get("company_currency_id") instanceof Boolean)&& map.get("company_currency_id")!=null){
			Object[] objs = (Object[])map.get("company_currency_id");
			if(objs.length > 0){
				this.setCompany_currency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("credit_move_id") instanceof Boolean)&& map.get("credit_move_id")!=null){
			Object[] objs = (Object[])map.get("credit_move_id");
			if(objs.length > 0){
				this.setCredit_move_id((Integer)objs[0]);
			}
		}
		if(!(map.get("credit_move_id") instanceof Boolean)&& map.get("credit_move_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("credit_move_id");
			if(objs.length > 1){
				this.setCredit_move_id_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 1){
				this.setCurrency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("debit_move_id") instanceof Boolean)&& map.get("debit_move_id")!=null){
			Object[] objs = (Object[])map.get("debit_move_id");
			if(objs.length > 0){
				this.setDebit_move_id((Integer)objs[0]);
			}
		}
		if(!(map.get("debit_move_id") instanceof Boolean)&& map.get("debit_move_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("debit_move_id");
			if(objs.length > 1){
				this.setDebit_move_id_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("full_reconcile_id") instanceof Boolean)&& map.get("full_reconcile_id")!=null){
			Object[] objs = (Object[])map.get("full_reconcile_id");
			if(objs.length > 0){
				this.setFull_reconcile_id((Integer)objs[0]);
			}
		}
		if(!(map.get("full_reconcile_id") instanceof Boolean)&& map.get("full_reconcile_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("full_reconcile_id");
			if(objs.length > 1){
				this.setFull_reconcile_id_text((String)objs[1]);
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("max_date") instanceof Boolean)&& map.get("max_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("max_date"));
   			this.setMax_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAmount()!=null&&this.getAmountDirtyFlag()){
			map.put("amount",this.getAmount());
		}else if(this.getAmountDirtyFlag()){
			map.put("amount",false);
		}
		if(this.getAmount_currency()!=null&&this.getAmount_currencyDirtyFlag()){
			map.put("amount_currency",this.getAmount_currency());
		}else if(this.getAmount_currencyDirtyFlag()){
			map.put("amount_currency",false);
		}
		if(this.getCompany_currency_id()!=null&&this.getCompany_currency_idDirtyFlag()){
			map.put("company_currency_id",this.getCompany_currency_id());
		}else if(this.getCompany_currency_idDirtyFlag()){
			map.put("company_currency_id",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCredit_move_id()!=null&&this.getCredit_move_idDirtyFlag()){
			map.put("credit_move_id",this.getCredit_move_id());
		}else if(this.getCredit_move_idDirtyFlag()){
			map.put("credit_move_id",false);
		}
		if(this.getCredit_move_id_text()!=null&&this.getCredit_move_id_textDirtyFlag()){
			//忽略文本外键credit_move_id_text
		}else if(this.getCredit_move_id_textDirtyFlag()){
			map.put("credit_move_id",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_id_text()!=null&&this.getCurrency_id_textDirtyFlag()){
			//忽略文本外键currency_id_text
		}else if(this.getCurrency_id_textDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDebit_move_id()!=null&&this.getDebit_move_idDirtyFlag()){
			map.put("debit_move_id",this.getDebit_move_id());
		}else if(this.getDebit_move_idDirtyFlag()){
			map.put("debit_move_id",false);
		}
		if(this.getDebit_move_id_text()!=null&&this.getDebit_move_id_textDirtyFlag()){
			//忽略文本外键debit_move_id_text
		}else if(this.getDebit_move_id_textDirtyFlag()){
			map.put("debit_move_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFull_reconcile_id()!=null&&this.getFull_reconcile_idDirtyFlag()){
			map.put("full_reconcile_id",this.getFull_reconcile_id());
		}else if(this.getFull_reconcile_idDirtyFlag()){
			map.put("full_reconcile_id",false);
		}
		if(this.getFull_reconcile_id_text()!=null&&this.getFull_reconcile_id_textDirtyFlag()){
			//忽略文本外键full_reconcile_id_text
		}else if(this.getFull_reconcile_id_textDirtyFlag()){
			map.put("full_reconcile_id",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getMax_date()!=null&&this.getMax_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getMax_date());
			map.put("max_date",datetimeStr);
		}else if(this.getMax_dateDirtyFlag()){
			map.put("max_date",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
