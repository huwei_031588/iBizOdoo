package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_messageSearchContext;

/**
 * 实体 [消息] 存储模型
 */
public interface Mail_message{

    /**
     * 待处理
     */
    String getNeedaction();

    void setNeedaction(String needaction);

    /**
     * 获取 [待处理]脏标记
     */
    boolean getNeedactionDirtyFlag();

    /**
     * 布局
     */
    String getLayout();

    void setLayout(String layout);

    /**
     * 获取 [布局]脏标记
     */
    boolean getLayoutDirtyFlag();

    /**
     * 相关的文档模型
     */
    String getModel();

    void setModel(String model);

    /**
     * 获取 [相关的文档模型]脏标记
     */
    boolean getModelDirtyFlag();

    /**
     * 无响应
     */
    String getNo_auto_thread();

    void setNo_auto_thread(String no_auto_thread);

    /**
     * 获取 [无响应]脏标记
     */
    boolean getNo_auto_threadDirtyFlag();

    /**
     * 收件人
     */
    String getPartner_ids();

    void setPartner_ids(String partner_ids);

    /**
     * 获取 [收件人]脏标记
     */
    boolean getPartner_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 消息记录名称
     */
    String getRecord_name();

    void setRecord_name(String record_name);

    /**
     * 获取 [消息记录名称]脏标记
     */
    boolean getRecord_nameDirtyFlag();

    /**
     * 消息ID
     */
    String getMessage_id();

    void setMessage_id(String message_id);

    /**
     * 获取 [消息ID]脏标记
     */
    boolean getMessage_idDirtyFlag();

    /**
     * 追踪值
     */
    String getTracking_value_ids();

    void setTracking_value_ids(String tracking_value_ids);

    /**
     * 获取 [追踪值]脏标记
     */
    boolean getTracking_value_idsDirtyFlag();

    /**
     * 管理状态
     */
    String getModeration_status();

    void setModeration_status(String moderation_status);

    /**
     * 获取 [管理状态]脏标记
     */
    boolean getModeration_statusDirtyFlag();

    /**
     * 通知
     */
    String getNotification_ids();

    void setNotification_ids(String notification_ids);

    /**
     * 获取 [通知]脏标记
     */
    boolean getNotification_idsDirtyFlag();

    /**
     * 从
     */
    String getEmail_from();

    void setEmail_from(String email_from);

    /**
     * 获取 [从]脏标记
     */
    boolean getEmail_fromDirtyFlag();

    /**
     * 渠道
     */
    String getChannel_ids();

    void setChannel_ids(String channel_ids);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getChannel_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 加星的邮件
     */
    String getStarred();

    void setStarred(String starred);

    /**
     * 获取 [加星的邮件]脏标记
     */
    boolean getStarredDirtyFlag();

    /**
     * 相关评级
     */
    String getRating_ids();

    void setRating_ids(String rating_ids);

    /**
     * 获取 [相关评级]脏标记
     */
    boolean getRating_idsDirtyFlag();

    /**
     * 待处理的业务伙伴
     */
    String getNeedaction_partner_ids();

    void setNeedaction_partner_ids(String needaction_partner_ids);

    /**
     * 获取 [待处理的业务伙伴]脏标记
     */
    boolean getNeedaction_partner_idsDirtyFlag();

    /**
     * 回复 至
     */
    String getReply_to();

    void setReply_to(String reply_to);

    /**
     * 获取 [回复 至]脏标记
     */
    boolean getReply_toDirtyFlag();

    /**
     * 邮件发送服务器
     */
    Integer getMail_server_id();

    void setMail_server_id(Integer mail_server_id);

    /**
     * 获取 [邮件发送服务器]脏标记
     */
    boolean getMail_server_idDirtyFlag();

    /**
     * 相关文档编号
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [相关文档编号]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 需审核
     */
    String getNeed_moderation();

    void setNeed_moderation(String need_moderation);

    /**
     * 获取 [需审核]脏标记
     */
    boolean getNeed_moderationDirtyFlag();

    /**
     * 主题
     */
    String getSubject();

    void setSubject(String subject);

    /**
     * 获取 [主题]脏标记
     */
    boolean getSubjectDirtyFlag();

    /**
     * 内容
     */
    String getBody();

    void setBody(String body);

    /**
     * 获取 [内容]脏标记
     */
    boolean getBodyDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 已发布
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 附件
     */
    String getAttachment_ids();

    void setAttachment_ids(String attachment_ids);

    /**
     * 获取 [附件]脏标记
     */
    boolean getAttachment_idsDirtyFlag();

    /**
     * 下级消息
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [下级消息]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 评级值
     */
    Double getRating_value();

    void setRating_value(Double rating_value);

    /**
     * 获取 [评级值]脏标记
     */
    boolean getRating_valueDirtyFlag();

    /**
     * 添加签名
     */
    String getAdd_sign();

    void setAdd_sign(String add_sign);

    /**
     * 获取 [添加签名]脏标记
     */
    boolean getAdd_signDirtyFlag();

    /**
     * 收藏夹
     */
    String getStarred_partner_ids();

    void setStarred_partner_ids(String starred_partner_ids);

    /**
     * 获取 [收藏夹]脏标记
     */
    boolean getStarred_partner_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 有误差
     */
    String getHas_error();

    void setHas_error(String has_error);

    /**
     * 获取 [有误差]脏标记
     */
    boolean getHas_errorDirtyFlag();

    /**
     * 类型
     */
    String getMessage_type();

    void setMessage_type(String message_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getMessage_typeDirtyFlag();

    /**
     * 作者
     */
    String getAuthor_id_text();

    void setAuthor_id_text(String author_id_text);

    /**
     * 获取 [作者]脏标记
     */
    boolean getAuthor_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 邮件活动类型
     */
    String getMail_activity_type_id_text();

    void setMail_activity_type_id_text(String mail_activity_type_id_text);

    /**
     * 获取 [邮件活动类型]脏标记
     */
    boolean getMail_activity_type_id_textDirtyFlag();

    /**
     * 管理员
     */
    String getModerator_id_text();

    void setModerator_id_text(String moderator_id_text);

    /**
     * 获取 [管理员]脏标记
     */
    boolean getModerator_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 子类型
     */
    String getSubtype_id_text();

    void setSubtype_id_text(String subtype_id_text);

    /**
     * 获取 [子类型]脏标记
     */
    boolean getSubtype_id_textDirtyFlag();

    /**
     * 作者头像
     */
    byte[] getAuthor_avatar();

    void setAuthor_avatar(byte[] author_avatar);

    /**
     * 获取 [作者头像]脏标记
     */
    boolean getAuthor_avatarDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 邮件活动类型
     */
    Integer getMail_activity_type_id();

    void setMail_activity_type_id(Integer mail_activity_type_id);

    /**
     * 获取 [邮件活动类型]脏标记
     */
    boolean getMail_activity_type_idDirtyFlag();

    /**
     * 管理员
     */
    Integer getModerator_id();

    void setModerator_id(Integer moderator_id);

    /**
     * 获取 [管理员]脏标记
     */
    boolean getModerator_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 作者
     */
    Integer getAuthor_id();

    void setAuthor_id(Integer author_id);

    /**
     * 获取 [作者]脏标记
     */
    boolean getAuthor_idDirtyFlag();

    /**
     * 上级消息
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级消息]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 子类型
     */
    Integer getSubtype_id();

    void setSubtype_id(Integer subtype_id);

    /**
     * 获取 [子类型]脏标记
     */
    boolean getSubtype_idDirtyFlag();

}
