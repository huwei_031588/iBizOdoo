package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_request;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_requestSearchContext;

/**
 * 实体 [保养请求] 存储对象
 */
public interface Mro_requestRepository extends Repository<Mro_request> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_request> searchDefault(Mro_requestSearchContext context);

    Mro_request convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request domain , Mro_request po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request convert2Domain( Mro_request po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request domain) ;

}
