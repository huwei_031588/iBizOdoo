package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [lunch_order_line] 对象
 */
public interface lunch_order_line {

    public String getCashmove();

    public void setCashmove(String cashmove);

    public Integer getCategory_id();

    public void setCategory_id(Integer category_id);

    public String getCategory_id_text();

    public void setCategory_id_text(String category_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getCurrency_id();

    public void setCurrency_id(Integer currency_id);

    public String getCurrency_id_text();

    public void setCurrency_id_text(String currency_id_text);

    public Timestamp getDate();

    public void setDate(Timestamp date);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getName();

    public void setName(String name);

    public String getNote();

    public void setNote(String note);

    public Integer getOrder_id();

    public void setOrder_id(Integer order_id);

    public Double getPrice();

    public void setPrice(Double price);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public String getState();

    public void setState(String state);

    public Integer getSupplier();

    public void setSupplier(Integer supplier);

    public String getSupplier_text();

    public void setSupplier_text(String supplier_text);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
