package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_attribute;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_attribute] 服务对象接口
 */
public interface Iproduct_attributeClientService{

    public Iproduct_attribute createModel() ;

    public void get(Iproduct_attribute product_attribute);

    public void updateBatch(List<Iproduct_attribute> product_attributes);

    public void remove(Iproduct_attribute product_attribute);

    public void update(Iproduct_attribute product_attribute);

    public Page<Iproduct_attribute> search(SearchContext context);

    public void createBatch(List<Iproduct_attribute> product_attributes);

    public void create(Iproduct_attribute product_attribute);

    public void removeBatch(List<Iproduct_attribute> product_attributes);

    public Page<Iproduct_attribute> select(SearchContext context);

}
