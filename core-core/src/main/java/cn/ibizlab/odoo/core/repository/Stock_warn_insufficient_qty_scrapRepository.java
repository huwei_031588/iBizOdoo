package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_warn_insufficient_qty_scrap;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_scrapSearchContext;

/**
 * 实体 [报废数量不足发出警告] 存储对象
 */
public interface Stock_warn_insufficient_qty_scrapRepository extends Repository<Stock_warn_insufficient_qty_scrap> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_warn_insufficient_qty_scrap> searchDefault(Stock_warn_insufficient_qty_scrapSearchContext context);

    Stock_warn_insufficient_qty_scrap convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_scrap domain , Stock_warn_insufficient_qty_scrap po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_scrap convert2Domain( Stock_warn_insufficient_qty_scrap po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_scrap domain) ;

}
