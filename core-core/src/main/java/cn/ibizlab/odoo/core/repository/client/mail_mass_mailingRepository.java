package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mail_mass_mailing;

/**
 * 实体[mail_mass_mailing] 服务对象接口
 */
public interface mail_mass_mailingRepository{


    public mail_mass_mailing createPO() ;
        public List<mail_mass_mailing> search();

        public void removeBatch(String id);

        public void create(mail_mass_mailing mail_mass_mailing);

        public void remove(String id);

        public void updateBatch(mail_mass_mailing mail_mass_mailing);

        public void update(mail_mass_mailing mail_mass_mailing);

        public void createBatch(mail_mass_mailing mail_mass_mailing);

        public void get(String id);


}
