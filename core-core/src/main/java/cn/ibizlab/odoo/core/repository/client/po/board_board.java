package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [board_board] 对象
 */
public interface board_board {

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
