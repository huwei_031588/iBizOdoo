package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expenseSearchContext;

/**
 * 实体 [费用] 存储模型
 */
public interface Hr_expense{

    /**
     * 附件数量
     */
    Integer getAttachment_number();

    void setAttachment_number(Integer attachment_number);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getAttachment_numberDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 总计
     */
    Double getTotal_amount();

    void setTotal_amount(Double total_amount);

    /**
     * 获取 [总计]脏标记
     */
    boolean getTotal_amountDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 税率设置
     */
    String getTax_ids();

    void setTax_ids(String tax_ids);

    /**
     * 获取 [税率设置]脏标记
     */
    boolean getTax_idsDirtyFlag();

    /**
     * 单价
     */
    Double getUnit_amount();

    void setUnit_amount(Double unit_amount);

    /**
     * 获取 [单价]脏标记
     */
    boolean getUnit_amountDirtyFlag();

    /**
     * 支付
     */
    String getPayment_mode();

    void setPayment_mode(String payment_mode);

    /**
     * 获取 [支付]脏标记
     */
    boolean getPayment_modeDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 合计 (公司货币)
     */
    Double getTotal_amount_company();

    void setTotal_amount_company(Double total_amount_company);

    /**
     * 获取 [合计 (公司货币)]脏标记
     */
    boolean getTotal_amount_companyDirtyFlag();

    /**
     * 由经理或会计人员明确地拒绝
     */
    String getIs_refused();

    void setIs_refused(String is_refused);

    /**
     * 获取 [由经理或会计人员明确地拒绝]脏标记
     */
    boolean getIs_refusedDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 信息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [信息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 数量
     */
    Double getQuantity();

    void setQuantity(Double quantity);

    /**
     * 获取 [数量]脏标记
     */
    boolean getQuantityDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 小计
     */
    Double getUntaxed_amount();

    void setUntaxed_amount(Double untaxed_amount);

    /**
     * 获取 [小计]脏标记
     */
    boolean getUntaxed_amountDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 分析标签
     */
    String getAnalytic_tag_ids();

    void setAnalytic_tag_ids(String analytic_tag_ids);

    /**
     * 获取 [分析标签]脏标记
     */
    boolean getAnalytic_tag_idsDirtyFlag();

    /**
     * 备注...
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [备注...]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 账单参照
     */
    String getReference();

    void setReference(String reference);

    /**
     * 获取 [账单参照]脏标记
     */
    boolean getReferenceDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 费用报表
     */
    String getSheet_id_text();

    void setSheet_id_text(String sheet_id_text);

    /**
     * 获取 [费用报表]脏标记
     */
    boolean getSheet_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司货币报告
     */
    String getCompany_currency_id_text();

    void setCompany_currency_id_text(String company_currency_id_text);

    /**
     * 获取 [公司货币报告]脏标记
     */
    boolean getCompany_currency_id_textDirtyFlag();

    /**
     * 员工
     */
    String getEmployee_id_text();

    void setEmployee_id_text(String employee_id_text);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_id_textDirtyFlag();

    /**
     * 单位
     */
    String getProduct_uom_id_text();

    void setProduct_uom_id_text(String product_uom_id_text);

    /**
     * 获取 [单位]脏标记
     */
    boolean getProduct_uom_id_textDirtyFlag();

    /**
     * 销售订单
     */
    String getSale_order_id_text();

    void setSale_order_id_text(String sale_order_id_text);

    /**
     * 获取 [销售订单]脏标记
     */
    boolean getSale_order_id_textDirtyFlag();

    /**
     * 分析账户
     */
    String getAnalytic_account_id_text();

    void setAnalytic_account_id_text(String analytic_account_id_text);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAnalytic_account_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 科目
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [科目]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 员工
     */
    Integer getEmployee_id();

    void setEmployee_id(Integer employee_id);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_idDirtyFlag();

    /**
     * 销售订单
     */
    Integer getSale_order_id();

    void setSale_order_id(Integer sale_order_id);

    /**
     * 获取 [销售订单]脏标记
     */
    boolean getSale_order_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 费用报表
     */
    Integer getSheet_id();

    void setSheet_id(Integer sheet_id);

    /**
     * 获取 [费用报表]脏标记
     */
    boolean getSheet_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 科目
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [科目]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 公司货币报告
     */
    Integer getCompany_currency_id();

    void setCompany_currency_id(Integer company_currency_id);

    /**
     * 获取 [公司货币报告]脏标记
     */
    boolean getCompany_currency_idDirtyFlag();

    /**
     * 分析账户
     */
    Integer getAnalytic_account_id();

    void setAnalytic_account_id(Integer analytic_account_id);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAnalytic_account_idDirtyFlag();

}
