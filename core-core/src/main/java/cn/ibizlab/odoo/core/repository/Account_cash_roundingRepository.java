package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_cash_rounding;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cash_roundingSearchContext;

/**
 * 实体 [帐户现金舍入] 存储对象
 */
public interface Account_cash_roundingRepository extends Repository<Account_cash_rounding> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_cash_rounding> searchDefault(Account_cash_roundingSearchContext context);

    Account_cash_rounding convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding domain , Account_cash_rounding po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding convert2Domain( Account_cash_rounding po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding domain) ;

}
