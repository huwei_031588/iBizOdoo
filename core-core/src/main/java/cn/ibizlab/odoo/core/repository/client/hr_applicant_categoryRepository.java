package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_applicant_category;

/**
 * 实体[hr_applicant_category] 服务对象接口
 */
public interface hr_applicant_categoryRepository{


    public hr_applicant_category createPO() ;
        public void updateBatch(hr_applicant_category hr_applicant_category);

        public List<hr_applicant_category> search();

        public void get(String id);

        public void create(hr_applicant_category hr_applicant_category);

        public void removeBatch(String id);

        public void remove(String id);

        public void update(hr_applicant_category hr_applicant_category);

        public void createBatch(hr_applicant_category hr_applicant_category);


}
