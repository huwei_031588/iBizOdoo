package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_config;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_configSearchContext;

/**
 * 实体 [配置] 存储对象
 */
public interface Res_configRepository extends Repository<Res_config> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_config> searchDefault(Res_configSearchContext context);

    Res_config convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_config domain , Res_config po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_config convert2Domain( Res_config po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_config domain) ;

}
