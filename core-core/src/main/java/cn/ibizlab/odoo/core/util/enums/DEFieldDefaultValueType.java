package cn.ibizlab.odoo.core.util.enums;

/**
 * 实体属性默认值类型
 */
public enum DEFieldDefaultValueType {
    /**
     * 用户全局对象
     */
    SESSION,
    /**
     * 系统全局对象
     */
    SYSTEM,
    /**
     * 唯一编码
     */
    UUID,
    /**
     * 网页请求
     */
    REQUEST,
    /**
     * 数据对象属性
     */
    DEFIELD,
    /**
     * 当前操作用户（编号）
     */
    OPPERSONID,
    /**
     * 当前操作用户（名称）
     */
    OPPERSONNAME,
    /**
     * 默认值
     */
    NONE
}
