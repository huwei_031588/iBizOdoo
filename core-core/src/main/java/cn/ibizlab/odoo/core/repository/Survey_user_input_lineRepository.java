package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Survey_user_input_line;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_input_lineSearchContext;

/**
 * 实体 [调查用户输入明细] 存储对象
 */
public interface Survey_user_input_lineRepository extends Repository<Survey_user_input_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Survey_user_input_line> searchDefault(Survey_user_input_lineSearchContext context);

    Survey_user_input_line convert2PO(cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line domain , Survey_user_input_line po) ;

    cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line convert2Domain( Survey_user_input_line po ,cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line domain) ;

}
