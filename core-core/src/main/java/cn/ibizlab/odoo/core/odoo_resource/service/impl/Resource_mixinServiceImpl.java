package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_resource.client.resource_mixinOdooClient;
import cn.ibizlab.odoo.core.odoo_resource.clientmodel.resource_mixinClientModel;

/**
 * 实体[资源装饰] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_mixinServiceImpl implements IResource_mixinService {

    @Autowired
    resource_mixinOdooClient resource_mixinOdooClient;


    @Override
    public boolean update(Resource_mixin et) {
        resource_mixinClientModel clientModel = convert2Model(et,null);
		resource_mixinOdooClient.update(clientModel);
        Resource_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Resource_mixin> list){
    }

    @Override
    public boolean remove(Integer id) {
        resource_mixinClientModel clientModel = new resource_mixinClientModel();
        clientModel.setId(id);
		resource_mixinOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Resource_mixin get(Integer id) {
        resource_mixinClientModel clientModel = new resource_mixinClientModel();
        clientModel.setId(id);
		resource_mixinOdooClient.get(clientModel);
        Resource_mixin et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Resource_mixin();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Resource_mixin et) {
        resource_mixinClientModel clientModel = convert2Model(et,null);
		resource_mixinOdooClient.create(clientModel);
        Resource_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_mixin> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_mixin> searchDefault(Resource_mixinSearchContext context) {
        List<Resource_mixin> list = new ArrayList<Resource_mixin>();
        Page<resource_mixinClientModel> clientModelList = resource_mixinOdooClient.search(context);
        for(resource_mixinClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Resource_mixin>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public resource_mixinClientModel convert2Model(Resource_mixin domain , resource_mixinClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new resource_mixinClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("tzdirtyflag"))
                model.setTz(domain.getTz());
            if((Boolean) domain.getExtensionparams().get("resource_id_textdirtyflag"))
                model.setResource_id_text(domain.getResourceIdText());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_id_textdirtyflag"))
                model.setResource_calendar_id_text(domain.getResourceCalendarIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_iddirtyflag"))
                model.setResource_calendar_id(domain.getResourceCalendarId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("resource_iddirtyflag"))
                model.setResource_id(domain.getResourceId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Resource_mixin convert2Domain( resource_mixinClientModel model ,Resource_mixin domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Resource_mixin();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getTzDirtyFlag())
            domain.setTz(model.getTz());
        if(model.getResource_id_textDirtyFlag())
            domain.setResourceIdText(model.getResource_id_text());
        if(model.getResource_calendar_id_textDirtyFlag())
            domain.setResourceCalendarIdText(model.getResource_calendar_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getResource_calendar_idDirtyFlag())
            domain.setResourceCalendarId(model.getResource_calendar_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getResource_idDirtyFlag())
            domain.setResourceId(model.getResource_id());
        return domain ;
    }

}

    



