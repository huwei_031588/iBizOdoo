package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activitySearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activityService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_activityOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_activityClientModel;

/**
 * 实体[活动] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_activityServiceImpl implements IMail_activityService {

    @Autowired
    mail_activityOdooClient mail_activityOdooClient;


    @Override
    public boolean update(Mail_activity et) {
        mail_activityClientModel clientModel = convert2Model(et,null);
		mail_activityOdooClient.update(clientModel);
        Mail_activity rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_activity> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_activityClientModel clientModel = new mail_activityClientModel();
        clientModel.setId(id);
		mail_activityOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mail_activity get(Integer id) {
        mail_activityClientModel clientModel = new mail_activityClientModel();
        clientModel.setId(id);
		mail_activityOdooClient.get(clientModel);
        Mail_activity et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_activity();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mail_activity et) {
        mail_activityClientModel clientModel = convert2Model(et,null);
		mail_activityOdooClient.create(clientModel);
        Mail_activity rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_activity> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_activity> searchDefault(Mail_activitySearchContext context) {
        List<Mail_activity> list = new ArrayList<Mail_activity>();
        Page<mail_activityClientModel> clientModelList = mail_activityOdooClient.search(context);
        for(mail_activityClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_activity>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_activityClientModel convert2Model(Mail_activity domain , mail_activityClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_activityClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("mail_template_idsdirtyflag"))
                model.setMail_template_ids(domain.getMailTemplateIds());
            if((Boolean) domain.getExtensionparams().get("automateddirtyflag"))
                model.setAutomated(domain.getAutomated());
            if((Boolean) domain.getExtensionparams().get("res_namedirtyflag"))
                model.setRes_name(domain.getResName());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("date_deadlinedirtyflag"))
                model.setDate_deadline(domain.getDateDeadline());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("summarydirtyflag"))
                model.setSummary(domain.getSummary());
            if((Boolean) domain.getExtensionparams().get("has_recommended_activitiesdirtyflag"))
                model.setHas_recommended_activities(domain.getHasRecommendedActivities());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("feedbackdirtyflag"))
                model.setFeedback(domain.getFeedback());
            if((Boolean) domain.getExtensionparams().get("res_model_iddirtyflag"))
                model.setRes_model_id(domain.getResModelId());
            if((Boolean) domain.getExtensionparams().get("res_modeldirtyflag"))
                model.setRes_model(domain.getResModel());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("activity_categorydirtyflag"))
                model.setActivity_category(domain.getActivityCategory());
            if((Boolean) domain.getExtensionparams().get("activity_type_id_textdirtyflag"))
                model.setActivity_type_id_text(domain.getActivityTypeIdText());
            if((Boolean) domain.getExtensionparams().get("previous_activity_type_id_textdirtyflag"))
                model.setPrevious_activity_type_id_text(domain.getPreviousActivityTypeIdText());
            if((Boolean) domain.getExtensionparams().get("note_id_textdirtyflag"))
                model.setNote_id_text(domain.getNoteIdText());
            if((Boolean) domain.getExtensionparams().get("create_user_id_textdirtyflag"))
                model.setCreate_user_id_text(domain.getCreateUserIdText());
            if((Boolean) domain.getExtensionparams().get("icondirtyflag"))
                model.setIcon(domain.getIcon());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("activity_decorationdirtyflag"))
                model.setActivity_decoration(domain.getActivityDecoration());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("force_nextdirtyflag"))
                model.setForce_next(domain.getForceNext());
            if((Boolean) domain.getExtensionparams().get("recommended_activity_type_id_textdirtyflag"))
                model.setRecommended_activity_type_id_text(domain.getRecommendedActivityTypeIdText());
            if((Boolean) domain.getExtensionparams().get("calendar_event_id_textdirtyflag"))
                model.setCalendar_event_id_text(domain.getCalendarEventIdText());
            if((Boolean) domain.getExtensionparams().get("recommended_activity_type_iddirtyflag"))
                model.setRecommended_activity_type_id(domain.getRecommendedActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("create_user_iddirtyflag"))
                model.setCreate_user_id(domain.getCreateUserId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("note_iddirtyflag"))
                model.setNote_id(domain.getNoteId());
            if((Boolean) domain.getExtensionparams().get("previous_activity_type_iddirtyflag"))
                model.setPrevious_activity_type_id(domain.getPreviousActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("calendar_event_iddirtyflag"))
                model.setCalendar_event_id(domain.getCalendarEventId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_activity convert2Domain( mail_activityClientModel model ,Mail_activity domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_activity();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMail_template_idsDirtyFlag())
            domain.setMailTemplateIds(model.getMail_template_ids());
        if(model.getAutomatedDirtyFlag())
            domain.setAutomated(model.getAutomated());
        if(model.getRes_nameDirtyFlag())
            domain.setResName(model.getRes_name());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDate_deadlineDirtyFlag())
            domain.setDateDeadline(model.getDate_deadline());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getSummaryDirtyFlag())
            domain.setSummary(model.getSummary());
        if(model.getHas_recommended_activitiesDirtyFlag())
            domain.setHasRecommendedActivities(model.getHas_recommended_activities());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getFeedbackDirtyFlag())
            domain.setFeedback(model.getFeedback());
        if(model.getRes_model_idDirtyFlag())
            domain.setResModelId(model.getRes_model_id());
        if(model.getRes_modelDirtyFlag())
            domain.setResModel(model.getRes_model());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getActivity_categoryDirtyFlag())
            domain.setActivityCategory(model.getActivity_category());
        if(model.getActivity_type_id_textDirtyFlag())
            domain.setActivityTypeIdText(model.getActivity_type_id_text());
        if(model.getPrevious_activity_type_id_textDirtyFlag())
            domain.setPreviousActivityTypeIdText(model.getPrevious_activity_type_id_text());
        if(model.getNote_id_textDirtyFlag())
            domain.setNoteIdText(model.getNote_id_text());
        if(model.getCreate_user_id_textDirtyFlag())
            domain.setCreateUserIdText(model.getCreate_user_id_text());
        if(model.getIconDirtyFlag())
            domain.setIcon(model.getIcon());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getActivity_decorationDirtyFlag())
            domain.setActivityDecoration(model.getActivity_decoration());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getForce_nextDirtyFlag())
            domain.setForceNext(model.getForce_next());
        if(model.getRecommended_activity_type_id_textDirtyFlag())
            domain.setRecommendedActivityTypeIdText(model.getRecommended_activity_type_id_text());
        if(model.getCalendar_event_id_textDirtyFlag())
            domain.setCalendarEventIdText(model.getCalendar_event_id_text());
        if(model.getRecommended_activity_type_idDirtyFlag())
            domain.setRecommendedActivityTypeId(model.getRecommended_activity_type_id());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getCreate_user_idDirtyFlag())
            domain.setCreateUserId(model.getCreate_user_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getNote_idDirtyFlag())
            domain.setNoteId(model.getNote_id());
        if(model.getPrevious_activity_type_idDirtyFlag())
            domain.setPreviousActivityTypeId(model.getPrevious_activity_type_id());
        if(model.getCalendar_event_idDirtyFlag())
            domain.setCalendarEventId(model.getCalendar_event_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



