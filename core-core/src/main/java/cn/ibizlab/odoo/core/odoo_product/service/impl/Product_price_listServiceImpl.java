package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_listSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_price_listService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_price_listOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_price_listClientModel;

/**
 * 实体[基于价格列表版本的单位产品价格] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_price_listServiceImpl implements IProduct_price_listService {

    @Autowired
    product_price_listOdooClient product_price_listOdooClient;


    @Override
    public boolean remove(Integer id) {
        product_price_listClientModel clientModel = new product_price_listClientModel();
        clientModel.setId(id);
		product_price_listOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Product_price_list et) {
        product_price_listClientModel clientModel = convert2Model(et,null);
		product_price_listOdooClient.update(clientModel);
        Product_price_list rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_price_list> list){
    }

    @Override
    public boolean create(Product_price_list et) {
        product_price_listClientModel clientModel = convert2Model(et,null);
		product_price_listOdooClient.create(clientModel);
        Product_price_list rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_price_list> list){
    }

    @Override
    public Product_price_list get(Integer id) {
        product_price_listClientModel clientModel = new product_price_listClientModel();
        clientModel.setId(id);
		product_price_listOdooClient.get(clientModel);
        Product_price_list et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_price_list();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_price_list> searchDefault(Product_price_listSearchContext context) {
        List<Product_price_list> list = new ArrayList<Product_price_list>();
        Page<product_price_listClientModel> clientModelList = product_price_listOdooClient.search(context);
        for(product_price_listClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_price_list>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_price_listClientModel convert2Model(Product_price_list domain , product_price_listClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_price_listClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("qty5dirtyflag"))
                model.setQty5(domain.getQty5());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("qty3dirtyflag"))
                model.setQty3(domain.getQty3());
            if((Boolean) domain.getExtensionparams().get("qty4dirtyflag"))
                model.setQty4(domain.getQty4());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("qty1dirtyflag"))
                model.setQty1(domain.getQty1());
            if((Boolean) domain.getExtensionparams().get("qty2dirtyflag"))
                model.setQty2(domain.getQty2());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("price_list_textdirtyflag"))
                model.setPrice_list_text(domain.getPriceListText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("price_listdirtyflag"))
                model.setPrice_list(domain.getPriceList());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_price_list convert2Domain( product_price_listClientModel model ,Product_price_list domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_price_list();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getQty5DirtyFlag())
            domain.setQty5(model.getQty5());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getQty3DirtyFlag())
            domain.setQty3(model.getQty3());
        if(model.getQty4DirtyFlag())
            domain.setQty4(model.getQty4());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getQty1DirtyFlag())
            domain.setQty1(model.getQty1());
        if(model.getQty2DirtyFlag())
            domain.setQty2(model.getQty2());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPrice_list_textDirtyFlag())
            domain.setPriceListText(model.getPrice_list_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPrice_listDirtyFlag())
            domain.setPriceList(model.getPrice_list());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



