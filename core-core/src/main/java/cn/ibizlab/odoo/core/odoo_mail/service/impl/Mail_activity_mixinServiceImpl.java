package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activity_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_activity_mixinOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_activity_mixinClientModel;

/**
 * 实体[活动Mixin] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_activity_mixinServiceImpl implements IMail_activity_mixinService {

    @Autowired
    mail_activity_mixinOdooClient mail_activity_mixinOdooClient;


    @Override
    public Mail_activity_mixin get(Integer id) {
        mail_activity_mixinClientModel clientModel = new mail_activity_mixinClientModel();
        clientModel.setId(id);
		mail_activity_mixinOdooClient.get(clientModel);
        Mail_activity_mixin et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_activity_mixin();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mail_activity_mixin et) {
        mail_activity_mixinClientModel clientModel = convert2Model(et,null);
		mail_activity_mixinOdooClient.update(clientModel);
        Mail_activity_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_activity_mixin> list){
    }

    @Override
    public boolean create(Mail_activity_mixin et) {
        mail_activity_mixinClientModel clientModel = convert2Model(et,null);
		mail_activity_mixinOdooClient.create(clientModel);
        Mail_activity_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_activity_mixin> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_activity_mixinClientModel clientModel = new mail_activity_mixinClientModel();
        clientModel.setId(id);
		mail_activity_mixinOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_activity_mixin> searchDefault(Mail_activity_mixinSearchContext context) {
        List<Mail_activity_mixin> list = new ArrayList<Mail_activity_mixin>();
        Page<mail_activity_mixinClientModel> clientModelList = mail_activity_mixinOdooClient.search(context);
        for(mail_activity_mixinClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_activity_mixin>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_activity_mixinClientModel convert2Model(Mail_activity_mixin domain , mail_activity_mixinClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_activity_mixinClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_activity_mixin convert2Domain( mail_activity_mixinClientModel model ,Mail_activity_mixin domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_activity_mixin();
        }

        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        return domain ;
    }

}

    



