package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_country_group;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_country_groupSearchContext;

/**
 * 实体 [国家/地区群组] 存储对象
 */
public interface Res_country_groupRepository extends Repository<Res_country_group> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_country_group> searchDefault(Res_country_groupSearchContext context);

    Res_country_group convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_country_group domain , Res_country_group po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_country_group convert2Domain( Res_country_group po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_country_group domain) ;

}
