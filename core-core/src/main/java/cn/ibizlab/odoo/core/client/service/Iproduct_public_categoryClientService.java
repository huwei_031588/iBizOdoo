package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_public_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_public_category] 服务对象接口
 */
public interface Iproduct_public_categoryClientService{

    public Iproduct_public_category createModel() ;

    public void get(Iproduct_public_category product_public_category);

    public void create(Iproduct_public_category product_public_category);

    public void removeBatch(List<Iproduct_public_category> product_public_categories);

    public void createBatch(List<Iproduct_public_category> product_public_categories);

    public void update(Iproduct_public_category product_public_category);

    public void updateBatch(List<Iproduct_public_category> product_public_categories);

    public Page<Iproduct_public_category> search(SearchContext context);

    public void remove(Iproduct_public_category product_public_category);

    public Page<Iproduct_public_category> select(SearchContext context);

}
