package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_lineSearchContext;

/**
 * 实体 [分析行] 存储模型
 */
public interface Account_analytic_line{

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 代码
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [代码]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 数量
     */
    Double getUnit_amount();

    void setUnit_amount(Double unit_amount);

    /**
     * 获取 [数量]脏标记
     */
    boolean getUnit_amountDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 标签
     */
    String getTag_ids();

    void setTag_ids(String tag_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getTag_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 参考.
     */
    String getRef();

    void setRef(String ref);

    /**
     * 获取 [参考.]脏标记
     */
    boolean getRefDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 分析账户
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 日记账项目
     */
    String getMove_id_text();

    void setMove_id_text(String move_id_text);

    /**
     * 获取 [日记账项目]脏标记
     */
    boolean getMove_id_textDirtyFlag();

    /**
     * 销售订单项目
     */
    String getSo_line_text();

    void setSo_line_text(String so_line_text);

    /**
     * 获取 [销售订单项目]脏标记
     */
    boolean getSo_line_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 财务会计
     */
    String getGeneral_account_id_text();

    void setGeneral_account_id_text(String general_account_id_text);

    /**
     * 获取 [财务会计]脏标记
     */
    boolean getGeneral_account_id_textDirtyFlag();

    /**
     * 组
     */
    String getGroup_id_text();

    void setGroup_id_text(String group_id_text);

    /**
     * 获取 [组]脏标记
     */
    boolean getGroup_id_textDirtyFlag();

    /**
     * 用户
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 计量单位
     */
    String getProduct_uom_id_text();

    void setProduct_uom_id_text(String product_uom_id_text);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_id_textDirtyFlag();

    /**
     * 销售订单项目
     */
    Integer getSo_line();

    void setSo_line(Integer so_line);

    /**
     * 获取 [销售订单项目]脏标记
     */
    boolean getSo_lineDirtyFlag();

    /**
     * 财务会计
     */
    Integer getGeneral_account_id();

    void setGeneral_account_id(Integer general_account_id);

    /**
     * 获取 [财务会计]脏标记
     */
    boolean getGeneral_account_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 分析账户
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 组
     */
    Integer getGroup_id();

    void setGroup_id(Integer group_id);

    /**
     * 获取 [组]脏标记
     */
    boolean getGroup_idDirtyFlag();

    /**
     * 用户
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 日记账项目
     */
    Integer getMove_id();

    void setMove_id(Integer move_id);

    /**
     * 获取 [日记账项目]脏标记
     */
    boolean getMove_idDirtyFlag();

}
