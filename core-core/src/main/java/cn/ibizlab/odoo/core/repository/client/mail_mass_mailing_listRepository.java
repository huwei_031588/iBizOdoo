package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mail_mass_mailing_list;

/**
 * 实体[mail_mass_mailing_list] 服务对象接口
 */
public interface mail_mass_mailing_listRepository{


    public mail_mass_mailing_list createPO() ;
        public void update(mail_mass_mailing_list mail_mass_mailing_list);

        public List<mail_mass_mailing_list> search();

        public void create(mail_mass_mailing_list mail_mass_mailing_list);

        public void createBatch(mail_mass_mailing_list mail_mass_mailing_list);

        public void remove(String id);

        public void updateBatch(mail_mass_mailing_list mail_mass_mailing_list);

        public void removeBatch(String id);

        public void get(String id);


}
