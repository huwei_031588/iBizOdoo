package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Note_note;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_noteSearchContext;

/**
 * 实体 [便签] 存储对象
 */
public interface Note_noteRepository extends Repository<Note_note> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Note_note> searchDefault(Note_noteSearchContext context);

    Note_note convert2PO(cn.ibizlab.odoo.core.odoo_note.domain.Note_note domain , Note_note po) ;

    cn.ibizlab.odoo.core.odoo_note.domain.Note_note convert2Domain( Note_note po ,cn.ibizlab.odoo.core.odoo_note.domain.Note_note domain) ;

}
