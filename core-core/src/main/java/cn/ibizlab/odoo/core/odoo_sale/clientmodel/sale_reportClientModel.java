package cn.ibizlab.odoo.core.odoo_sale.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[sale_report] 对象
 */
public class sale_reportClientModel implements Serializable{

    /**
     * 分析账户
     */
    public Integer analytic_account_id;

    @JsonIgnore
    public boolean analytic_account_idDirtyFlag;
    
    /**
     * 分析账户
     */
    public String analytic_account_id_text;

    @JsonIgnore
    public boolean analytic_account_id_textDirtyFlag;
    
    /**
     * 营销
     */
    public Integer campaign_id;

    @JsonIgnore
    public boolean campaign_idDirtyFlag;
    
    /**
     * 营销
     */
    public String campaign_id_text;

    @JsonIgnore
    public boolean campaign_id_textDirtyFlag;
    
    /**
     * 产品种类
     */
    public Integer categ_id;

    @JsonIgnore
    public boolean categ_idDirtyFlag;
    
    /**
     * 产品种类
     */
    public String categ_id_text;

    @JsonIgnore
    public boolean categ_id_textDirtyFlag;
    
    /**
     * 客户实体
     */
    public Integer commercial_partner_id;

    @JsonIgnore
    public boolean commercial_partner_idDirtyFlag;
    
    /**
     * 客户实体
     */
    public String commercial_partner_id_text;

    @JsonIgnore
    public boolean commercial_partner_id_textDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 确认日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp confirmation_date;

    @JsonIgnore
    public boolean confirmation_dateDirtyFlag;
    
    /**
     * 客户国家
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 客户国家
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 单据日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 折扣 %
     */
    public Double discount;

    @JsonIgnore
    public boolean discountDirtyFlag;
    
    /**
     * 折扣金额
     */
    public Double discount_amount;

    @JsonIgnore
    public boolean discount_amountDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 媒体
     */
    public Integer medium_id;

    @JsonIgnore
    public boolean medium_idDirtyFlag;
    
    /**
     * 媒体
     */
    public String medium_id_text;

    @JsonIgnore
    public boolean medium_id_textDirtyFlag;
    
    /**
     * 订单关联
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * # 明细行
     */
    public Integer nbr;

    @JsonIgnore
    public boolean nbrDirtyFlag;
    
    /**
     * 订单 #
     */
    public Integer order_id;

    @JsonIgnore
    public boolean order_idDirtyFlag;
    
    /**
     * 订单 #
     */
    public String order_id_text;

    @JsonIgnore
    public boolean order_id_textDirtyFlag;
    
    /**
     * 客户
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 客户
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 价格表
     */
    public Integer pricelist_id;

    @JsonIgnore
    public boolean pricelist_idDirtyFlag;
    
    /**
     * 价格表
     */
    public String pricelist_id_text;

    @JsonIgnore
    public boolean pricelist_id_textDirtyFlag;
    
    /**
     * 不含税总计
     */
    public Double price_subtotal;

    @JsonIgnore
    public boolean price_subtotalDirtyFlag;
    
    /**
     * 总计
     */
    public Double price_total;

    @JsonIgnore
    public boolean price_totalDirtyFlag;
    
    /**
     * 产品变体
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品变体
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_tmpl_id;

    @JsonIgnore
    public boolean product_tmpl_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_tmpl_id_text;

    @JsonIgnore
    public boolean product_tmpl_id_textDirtyFlag;
    
    /**
     * 计量单位
     */
    public Integer product_uom;

    @JsonIgnore
    public boolean product_uomDirtyFlag;
    
    /**
     * 订购数量
     */
    public Double product_uom_qty;

    @JsonIgnore
    public boolean product_uom_qtyDirtyFlag;
    
    /**
     * 计量单位
     */
    public String product_uom_text;

    @JsonIgnore
    public boolean product_uom_textDirtyFlag;
    
    /**
     * 已送货数量
     */
    public Double qty_delivered;

    @JsonIgnore
    public boolean qty_deliveredDirtyFlag;
    
    /**
     * 已开票数量
     */
    public Double qty_invoiced;

    @JsonIgnore
    public boolean qty_invoicedDirtyFlag;
    
    /**
     * 待开票数量
     */
    public Double qty_to_invoice;

    @JsonIgnore
    public boolean qty_to_invoiceDirtyFlag;
    
    /**
     * 来源
     */
    public Integer source_id;

    @JsonIgnore
    public boolean source_idDirtyFlag;
    
    /**
     * 来源
     */
    public String source_id_text;

    @JsonIgnore
    public boolean source_id_textDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 销售团队
     */
    public Integer team_id;

    @JsonIgnore
    public boolean team_idDirtyFlag;
    
    /**
     * 销售团队
     */
    public String team_id_text;

    @JsonIgnore
    public boolean team_id_textDirtyFlag;
    
    /**
     * 不含税已开票金额
     */
    public Double untaxed_amount_invoiced;

    @JsonIgnore
    public boolean untaxed_amount_invoicedDirtyFlag;
    
    /**
     * 不含税待开票金额
     */
    public Double untaxed_amount_to_invoice;

    @JsonIgnore
    public boolean untaxed_amount_to_invoiceDirtyFlag;
    
    /**
     * 销售员
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 销售员
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 体积
     */
    public Double volume;

    @JsonIgnore
    public boolean volumeDirtyFlag;
    
    /**
     * 仓库
     */
    public Integer warehouse_id;

    @JsonIgnore
    public boolean warehouse_idDirtyFlag;
    
    /**
     * 仓库
     */
    public String warehouse_id_text;

    @JsonIgnore
    public boolean warehouse_id_textDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 毛重
     */
    public Double weight;

    @JsonIgnore
    public boolean weightDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [分析账户]
     */
    @JsonProperty("analytic_account_id")
    public Integer getAnalytic_account_id(){
        return this.analytic_account_id ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("analytic_account_id")
    public void setAnalytic_account_id(Integer  analytic_account_id){
        this.analytic_account_id = analytic_account_id ;
        this.analytic_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_idDirtyFlag(){
        return this.analytic_account_idDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("analytic_account_id_text")
    public String getAnalytic_account_id_text(){
        return this.analytic_account_id_text ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("analytic_account_id_text")
    public void setAnalytic_account_id_text(String  analytic_account_id_text){
        this.analytic_account_id_text = analytic_account_id_text ;
        this.analytic_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_id_textDirtyFlag(){
        return this.analytic_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return this.campaign_id ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return this.campaign_idDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return this.campaign_id_text ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return this.campaign_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品种类]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return this.categ_id ;
    }

    /**
     * 设置 [产品种类]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品种类]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return this.categ_idDirtyFlag ;
    }   

    /**
     * 获取 [产品种类]
     */
    @JsonProperty("categ_id_text")
    public String getCateg_id_text(){
        return this.categ_id_text ;
    }

    /**
     * 设置 [产品种类]
     */
    @JsonProperty("categ_id_text")
    public void setCateg_id_text(String  categ_id_text){
        this.categ_id_text = categ_id_text ;
        this.categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品种类]脏标记
     */
    @JsonIgnore
    public boolean getCateg_id_textDirtyFlag(){
        return this.categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [客户实体]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return this.commercial_partner_id ;
    }

    /**
     * 设置 [客户实体]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return this.commercial_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [客户实体]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return this.commercial_partner_id_text ;
    }

    /**
     * 设置 [客户实体]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return this.commercial_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [确认日期]
     */
    @JsonProperty("confirmation_date")
    public Timestamp getConfirmation_date(){
        return this.confirmation_date ;
    }

    /**
     * 设置 [确认日期]
     */
    @JsonProperty("confirmation_date")
    public void setConfirmation_date(Timestamp  confirmation_date){
        this.confirmation_date = confirmation_date ;
        this.confirmation_dateDirtyFlag = true ;
    }

     /**
     * 获取 [确认日期]脏标记
     */
    @JsonIgnore
    public boolean getConfirmation_dateDirtyFlag(){
        return this.confirmation_dateDirtyFlag ;
    }   

    /**
     * 获取 [客户国家]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [客户国家]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [客户国家]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [客户国家]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [单据日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [单据日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [单据日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [折扣 %]
     */
    @JsonProperty("discount")
    public Double getDiscount(){
        return this.discount ;
    }

    /**
     * 设置 [折扣 %]
     */
    @JsonProperty("discount")
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.discountDirtyFlag = true ;
    }

     /**
     * 获取 [折扣 %]脏标记
     */
    @JsonIgnore
    public boolean getDiscountDirtyFlag(){
        return this.discountDirtyFlag ;
    }   

    /**
     * 获取 [折扣金额]
     */
    @JsonProperty("discount_amount")
    public Double getDiscount_amount(){
        return this.discount_amount ;
    }

    /**
     * 设置 [折扣金额]
     */
    @JsonProperty("discount_amount")
    public void setDiscount_amount(Double  discount_amount){
        this.discount_amount = discount_amount ;
        this.discount_amountDirtyFlag = true ;
    }

     /**
     * 获取 [折扣金额]脏标记
     */
    @JsonIgnore
    public boolean getDiscount_amountDirtyFlag(){
        return this.discount_amountDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return this.medium_id ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return this.medium_idDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return this.medium_id_text ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return this.medium_id_textDirtyFlag ;
    }   

    /**
     * 获取 [订单关联]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [订单关联]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [订单关联]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [# 明细行]
     */
    @JsonProperty("nbr")
    public Integer getNbr(){
        return this.nbr ;
    }

    /**
     * 设置 [# 明细行]
     */
    @JsonProperty("nbr")
    public void setNbr(Integer  nbr){
        this.nbr = nbr ;
        this.nbrDirtyFlag = true ;
    }

     /**
     * 获取 [# 明细行]脏标记
     */
    @JsonIgnore
    public boolean getNbrDirtyFlag(){
        return this.nbrDirtyFlag ;
    }   

    /**
     * 获取 [订单 #]
     */
    @JsonProperty("order_id")
    public Integer getOrder_id(){
        return this.order_id ;
    }

    /**
     * 设置 [订单 #]
     */
    @JsonProperty("order_id")
    public void setOrder_id(Integer  order_id){
        this.order_id = order_id ;
        this.order_idDirtyFlag = true ;
    }

     /**
     * 获取 [订单 #]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idDirtyFlag(){
        return this.order_idDirtyFlag ;
    }   

    /**
     * 获取 [订单 #]
     */
    @JsonProperty("order_id_text")
    public String getOrder_id_text(){
        return this.order_id_text ;
    }

    /**
     * 设置 [订单 #]
     */
    @JsonProperty("order_id_text")
    public void setOrder_id_text(String  order_id_text){
        this.order_id_text = order_id_text ;
        this.order_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [订单 #]脏标记
     */
    @JsonIgnore
    public boolean getOrder_id_textDirtyFlag(){
        return this.order_id_textDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return this.pricelist_id ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return this.pricelist_idDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public String getPricelist_id_text(){
        return this.pricelist_id_text ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public void setPricelist_id_text(String  pricelist_id_text){
        this.pricelist_id_text = pricelist_id_text ;
        this.pricelist_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_id_textDirtyFlag(){
        return this.pricelist_id_textDirtyFlag ;
    }   

    /**
     * 获取 [不含税总计]
     */
    @JsonProperty("price_subtotal")
    public Double getPrice_subtotal(){
        return this.price_subtotal ;
    }

    /**
     * 设置 [不含税总计]
     */
    @JsonProperty("price_subtotal")
    public void setPrice_subtotal(Double  price_subtotal){
        this.price_subtotal = price_subtotal ;
        this.price_subtotalDirtyFlag = true ;
    }

     /**
     * 获取 [不含税总计]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotalDirtyFlag(){
        return this.price_subtotalDirtyFlag ;
    }   

    /**
     * 获取 [总计]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return this.price_total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

     /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return this.price_totalDirtyFlag ;
    }   

    /**
     * 获取 [产品变体]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品变体]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品变体]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品变体]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品变体]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品变体]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return this.product_tmpl_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return this.product_tmpl_id_textDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return this.product_uom ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return this.product_uomDirtyFlag ;
    }   

    /**
     * 获取 [订购数量]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return this.product_uom_qty ;
    }

    /**
     * 设置 [订购数量]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [订购数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return this.product_uom_qtyDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return this.product_uom_text ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return this.product_uom_textDirtyFlag ;
    }   

    /**
     * 获取 [已送货数量]
     */
    @JsonProperty("qty_delivered")
    public Double getQty_delivered(){
        return this.qty_delivered ;
    }

    /**
     * 设置 [已送货数量]
     */
    @JsonProperty("qty_delivered")
    public void setQty_delivered(Double  qty_delivered){
        this.qty_delivered = qty_delivered ;
        this.qty_deliveredDirtyFlag = true ;
    }

     /**
     * 获取 [已送货数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_deliveredDirtyFlag(){
        return this.qty_deliveredDirtyFlag ;
    }   

    /**
     * 获取 [已开票数量]
     */
    @JsonProperty("qty_invoiced")
    public Double getQty_invoiced(){
        return this.qty_invoiced ;
    }

    /**
     * 设置 [已开票数量]
     */
    @JsonProperty("qty_invoiced")
    public void setQty_invoiced(Double  qty_invoiced){
        this.qty_invoiced = qty_invoiced ;
        this.qty_invoicedDirtyFlag = true ;
    }

     /**
     * 获取 [已开票数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_invoicedDirtyFlag(){
        return this.qty_invoicedDirtyFlag ;
    }   

    /**
     * 获取 [待开票数量]
     */
    @JsonProperty("qty_to_invoice")
    public Double getQty_to_invoice(){
        return this.qty_to_invoice ;
    }

    /**
     * 设置 [待开票数量]
     */
    @JsonProperty("qty_to_invoice")
    public void setQty_to_invoice(Double  qty_to_invoice){
        this.qty_to_invoice = qty_to_invoice ;
        this.qty_to_invoiceDirtyFlag = true ;
    }

     /**
     * 获取 [待开票数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_to_invoiceDirtyFlag(){
        return this.qty_to_invoiceDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return this.source_id ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return this.source_idDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return this.source_id_text ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return this.source_id_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }   

    /**
     * 获取 [不含税已开票金额]
     */
    @JsonProperty("untaxed_amount_invoiced")
    public Double getUntaxed_amount_invoiced(){
        return this.untaxed_amount_invoiced ;
    }

    /**
     * 设置 [不含税已开票金额]
     */
    @JsonProperty("untaxed_amount_invoiced")
    public void setUntaxed_amount_invoiced(Double  untaxed_amount_invoiced){
        this.untaxed_amount_invoiced = untaxed_amount_invoiced ;
        this.untaxed_amount_invoicedDirtyFlag = true ;
    }

     /**
     * 获取 [不含税已开票金额]脏标记
     */
    @JsonIgnore
    public boolean getUntaxed_amount_invoicedDirtyFlag(){
        return this.untaxed_amount_invoicedDirtyFlag ;
    }   

    /**
     * 获取 [不含税待开票金额]
     */
    @JsonProperty("untaxed_amount_to_invoice")
    public Double getUntaxed_amount_to_invoice(){
        return this.untaxed_amount_to_invoice ;
    }

    /**
     * 设置 [不含税待开票金额]
     */
    @JsonProperty("untaxed_amount_to_invoice")
    public void setUntaxed_amount_to_invoice(Double  untaxed_amount_to_invoice){
        this.untaxed_amount_to_invoice = untaxed_amount_to_invoice ;
        this.untaxed_amount_to_invoiceDirtyFlag = true ;
    }

     /**
     * 获取 [不含税待开票金额]脏标记
     */
    @JsonIgnore
    public boolean getUntaxed_amount_to_invoiceDirtyFlag(){
        return this.untaxed_amount_to_invoiceDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [体积]
     */
    @JsonProperty("volume")
    public Double getVolume(){
        return this.volume ;
    }

    /**
     * 设置 [体积]
     */
    @JsonProperty("volume")
    public void setVolume(Double  volume){
        this.volume = volume ;
        this.volumeDirtyFlag = true ;
    }

     /**
     * 获取 [体积]脏标记
     */
    @JsonIgnore
    public boolean getVolumeDirtyFlag(){
        return this.volumeDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return this.warehouse_id ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return this.warehouse_idDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return this.warehouse_id_text ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return this.warehouse_id_textDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [毛重]
     */
    @JsonProperty("weight")
    public Double getWeight(){
        return this.weight ;
    }

    /**
     * 设置 [毛重]
     */
    @JsonProperty("weight")
    public void setWeight(Double  weight){
        this.weight = weight ;
        this.weightDirtyFlag = true ;
    }

     /**
     * 获取 [毛重]脏标记
     */
    @JsonIgnore
    public boolean getWeightDirtyFlag(){
        return this.weightDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("analytic_account_id") instanceof Boolean)&& map.get("analytic_account_id")!=null){
			Object[] objs = (Object[])map.get("analytic_account_id");
			if(objs.length > 0){
				this.setAnalytic_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("analytic_account_id") instanceof Boolean)&& map.get("analytic_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("analytic_account_id");
			if(objs.length > 1){
				this.setAnalytic_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 0){
				this.setCampaign_id((Integer)objs[0]);
			}
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 1){
				this.setCampaign_id_text((String)objs[1]);
			}
		}
		if(!(map.get("categ_id") instanceof Boolean)&& map.get("categ_id")!=null){
			Object[] objs = (Object[])map.get("categ_id");
			if(objs.length > 0){
				this.setCateg_id((Integer)objs[0]);
			}
		}
		if(!(map.get("categ_id") instanceof Boolean)&& map.get("categ_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("categ_id");
			if(objs.length > 1){
				this.setCateg_id_text((String)objs[1]);
			}
		}
		if(!(map.get("commercial_partner_id") instanceof Boolean)&& map.get("commercial_partner_id")!=null){
			Object[] objs = (Object[])map.get("commercial_partner_id");
			if(objs.length > 0){
				this.setCommercial_partner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("commercial_partner_id") instanceof Boolean)&& map.get("commercial_partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("commercial_partner_id");
			if(objs.length > 1){
				this.setCommercial_partner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("confirmation_date") instanceof Boolean)&& map.get("confirmation_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("confirmation_date"));
   			this.setConfirmation_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 0){
				this.setCountry_id((Integer)objs[0]);
			}
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 1){
				this.setCountry_id_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("discount") instanceof Boolean)&& map.get("discount")!=null){
			this.setDiscount((Double)map.get("discount"));
		}
		if(!(map.get("discount_amount") instanceof Boolean)&& map.get("discount_amount")!=null){
			this.setDiscount_amount((Double)map.get("discount_amount"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 0){
				this.setMedium_id((Integer)objs[0]);
			}
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 1){
				this.setMedium_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("nbr") instanceof Boolean)&& map.get("nbr")!=null){
			this.setNbr((Integer)map.get("nbr"));
		}
		if(!(map.get("order_id") instanceof Boolean)&& map.get("order_id")!=null){
			Object[] objs = (Object[])map.get("order_id");
			if(objs.length > 0){
				this.setOrder_id((Integer)objs[0]);
			}
		}
		if(!(map.get("order_id") instanceof Boolean)&& map.get("order_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("order_id");
			if(objs.length > 1){
				this.setOrder_id_text((String)objs[1]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("pricelist_id") instanceof Boolean)&& map.get("pricelist_id")!=null){
			Object[] objs = (Object[])map.get("pricelist_id");
			if(objs.length > 0){
				this.setPricelist_id((Integer)objs[0]);
			}
		}
		if(!(map.get("pricelist_id") instanceof Boolean)&& map.get("pricelist_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("pricelist_id");
			if(objs.length > 1){
				this.setPricelist_id_text((String)objs[1]);
			}
		}
		if(!(map.get("price_subtotal") instanceof Boolean)&& map.get("price_subtotal")!=null){
			this.setPrice_subtotal((Double)map.get("price_subtotal"));
		}
		if(!(map.get("price_total") instanceof Boolean)&& map.get("price_total")!=null){
			this.setPrice_total((Double)map.get("price_total"));
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_tmpl_id") instanceof Boolean)&& map.get("product_tmpl_id")!=null){
			Object[] objs = (Object[])map.get("product_tmpl_id");
			if(objs.length > 0){
				this.setProduct_tmpl_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_tmpl_id") instanceof Boolean)&& map.get("product_tmpl_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_tmpl_id");
			if(objs.length > 1){
				this.setProduct_tmpl_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_uom") instanceof Boolean)&& map.get("product_uom")!=null){
			Object[] objs = (Object[])map.get("product_uom");
			if(objs.length > 0){
				this.setProduct_uom((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_qty") instanceof Boolean)&& map.get("product_uom_qty")!=null){
			this.setProduct_uom_qty((Double)map.get("product_uom_qty"));
		}
		if(!(map.get("product_uom") instanceof Boolean)&& map.get("product_uom")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_uom");
			if(objs.length > 1){
				this.setProduct_uom_text((String)objs[1]);
			}
		}
		if(!(map.get("qty_delivered") instanceof Boolean)&& map.get("qty_delivered")!=null){
			this.setQty_delivered((Double)map.get("qty_delivered"));
		}
		if(!(map.get("qty_invoiced") instanceof Boolean)&& map.get("qty_invoiced")!=null){
			this.setQty_invoiced((Double)map.get("qty_invoiced"));
		}
		if(!(map.get("qty_to_invoice") instanceof Boolean)&& map.get("qty_to_invoice")!=null){
			this.setQty_to_invoice((Double)map.get("qty_to_invoice"));
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 0){
				this.setSource_id((Integer)objs[0]);
			}
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 1){
				this.setSource_id_text((String)objs[1]);
			}
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 0){
				this.setTeam_id((Integer)objs[0]);
			}
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 1){
				this.setTeam_id_text((String)objs[1]);
			}
		}
		if(!(map.get("untaxed_amount_invoiced") instanceof Boolean)&& map.get("untaxed_amount_invoiced")!=null){
			this.setUntaxed_amount_invoiced((Double)map.get("untaxed_amount_invoiced"));
		}
		if(!(map.get("untaxed_amount_to_invoice") instanceof Boolean)&& map.get("untaxed_amount_to_invoice")!=null){
			this.setUntaxed_amount_to_invoice((Double)map.get("untaxed_amount_to_invoice"));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("volume") instanceof Boolean)&& map.get("volume")!=null){
			this.setVolume((Double)map.get("volume"));
		}
		if(!(map.get("warehouse_id") instanceof Boolean)&& map.get("warehouse_id")!=null){
			Object[] objs = (Object[])map.get("warehouse_id");
			if(objs.length > 0){
				this.setWarehouse_id((Integer)objs[0]);
			}
		}
		if(!(map.get("warehouse_id") instanceof Boolean)&& map.get("warehouse_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("warehouse_id");
			if(objs.length > 1){
				this.setWarehouse_id_text((String)objs[1]);
			}
		}
		if(!(map.get("website_id") instanceof Boolean)&& map.get("website_id")!=null){
			Object[] objs = (Object[])map.get("website_id");
			if(objs.length > 0){
				this.setWebsite_id((Integer)objs[0]);
			}
		}
		if(!(map.get("weight") instanceof Boolean)&& map.get("weight")!=null){
			this.setWeight((Double)map.get("weight"));
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAnalytic_account_id()!=null&&this.getAnalytic_account_idDirtyFlag()){
			map.put("analytic_account_id",this.getAnalytic_account_id());
		}else if(this.getAnalytic_account_idDirtyFlag()){
			map.put("analytic_account_id",false);
		}
		if(this.getAnalytic_account_id_text()!=null&&this.getAnalytic_account_id_textDirtyFlag()){
			//忽略文本外键analytic_account_id_text
		}else if(this.getAnalytic_account_id_textDirtyFlag()){
			map.put("analytic_account_id",false);
		}
		if(this.getCampaign_id()!=null&&this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",this.getCampaign_id());
		}else if(this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getCampaign_id_text()!=null&&this.getCampaign_id_textDirtyFlag()){
			//忽略文本外键campaign_id_text
		}else if(this.getCampaign_id_textDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getCateg_id()!=null&&this.getCateg_idDirtyFlag()){
			map.put("categ_id",this.getCateg_id());
		}else if(this.getCateg_idDirtyFlag()){
			map.put("categ_id",false);
		}
		if(this.getCateg_id_text()!=null&&this.getCateg_id_textDirtyFlag()){
			//忽略文本外键categ_id_text
		}else if(this.getCateg_id_textDirtyFlag()){
			map.put("categ_id",false);
		}
		if(this.getCommercial_partner_id()!=null&&this.getCommercial_partner_idDirtyFlag()){
			map.put("commercial_partner_id",this.getCommercial_partner_id());
		}else if(this.getCommercial_partner_idDirtyFlag()){
			map.put("commercial_partner_id",false);
		}
		if(this.getCommercial_partner_id_text()!=null&&this.getCommercial_partner_id_textDirtyFlag()){
			//忽略文本外键commercial_partner_id_text
		}else if(this.getCommercial_partner_id_textDirtyFlag()){
			map.put("commercial_partner_id",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getConfirmation_date()!=null&&this.getConfirmation_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getConfirmation_date());
			map.put("confirmation_date",datetimeStr);
		}else if(this.getConfirmation_dateDirtyFlag()){
			map.put("confirmation_date",false);
		}
		if(this.getCountry_id()!=null&&this.getCountry_idDirtyFlag()){
			map.put("country_id",this.getCountry_id());
		}else if(this.getCountry_idDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getCountry_id_text()!=null&&this.getCountry_id_textDirtyFlag()){
			//忽略文本外键country_id_text
		}else if(this.getCountry_id_textDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDiscount()!=null&&this.getDiscountDirtyFlag()){
			map.put("discount",this.getDiscount());
		}else if(this.getDiscountDirtyFlag()){
			map.put("discount",false);
		}
		if(this.getDiscount_amount()!=null&&this.getDiscount_amountDirtyFlag()){
			map.put("discount_amount",this.getDiscount_amount());
		}else if(this.getDiscount_amountDirtyFlag()){
			map.put("discount_amount",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getMedium_id()!=null&&this.getMedium_idDirtyFlag()){
			map.put("medium_id",this.getMedium_id());
		}else if(this.getMedium_idDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getMedium_id_text()!=null&&this.getMedium_id_textDirtyFlag()){
			//忽略文本外键medium_id_text
		}else if(this.getMedium_id_textDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNbr()!=null&&this.getNbrDirtyFlag()){
			map.put("nbr",this.getNbr());
		}else if(this.getNbrDirtyFlag()){
			map.put("nbr",false);
		}
		if(this.getOrder_id()!=null&&this.getOrder_idDirtyFlag()){
			map.put("order_id",this.getOrder_id());
		}else if(this.getOrder_idDirtyFlag()){
			map.put("order_id",false);
		}
		if(this.getOrder_id_text()!=null&&this.getOrder_id_textDirtyFlag()){
			//忽略文本外键order_id_text
		}else if(this.getOrder_id_textDirtyFlag()){
			map.put("order_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPricelist_id()!=null&&this.getPricelist_idDirtyFlag()){
			map.put("pricelist_id",this.getPricelist_id());
		}else if(this.getPricelist_idDirtyFlag()){
			map.put("pricelist_id",false);
		}
		if(this.getPricelist_id_text()!=null&&this.getPricelist_id_textDirtyFlag()){
			//忽略文本外键pricelist_id_text
		}else if(this.getPricelist_id_textDirtyFlag()){
			map.put("pricelist_id",false);
		}
		if(this.getPrice_subtotal()!=null&&this.getPrice_subtotalDirtyFlag()){
			map.put("price_subtotal",this.getPrice_subtotal());
		}else if(this.getPrice_subtotalDirtyFlag()){
			map.put("price_subtotal",false);
		}
		if(this.getPrice_total()!=null&&this.getPrice_totalDirtyFlag()){
			map.put("price_total",this.getPrice_total());
		}else if(this.getPrice_totalDirtyFlag()){
			map.put("price_total",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_tmpl_id()!=null&&this.getProduct_tmpl_idDirtyFlag()){
			map.put("product_tmpl_id",this.getProduct_tmpl_id());
		}else if(this.getProduct_tmpl_idDirtyFlag()){
			map.put("product_tmpl_id",false);
		}
		if(this.getProduct_tmpl_id_text()!=null&&this.getProduct_tmpl_id_textDirtyFlag()){
			//忽略文本外键product_tmpl_id_text
		}else if(this.getProduct_tmpl_id_textDirtyFlag()){
			map.put("product_tmpl_id",false);
		}
		if(this.getProduct_uom()!=null&&this.getProduct_uomDirtyFlag()){
			map.put("product_uom",this.getProduct_uom());
		}else if(this.getProduct_uomDirtyFlag()){
			map.put("product_uom",false);
		}
		if(this.getProduct_uom_qty()!=null&&this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",this.getProduct_uom_qty());
		}else if(this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",false);
		}
		if(this.getProduct_uom_text()!=null&&this.getProduct_uom_textDirtyFlag()){
			//忽略文本外键product_uom_text
		}else if(this.getProduct_uom_textDirtyFlag()){
			map.put("product_uom",false);
		}
		if(this.getQty_delivered()!=null&&this.getQty_deliveredDirtyFlag()){
			map.put("qty_delivered",this.getQty_delivered());
		}else if(this.getQty_deliveredDirtyFlag()){
			map.put("qty_delivered",false);
		}
		if(this.getQty_invoiced()!=null&&this.getQty_invoicedDirtyFlag()){
			map.put("qty_invoiced",this.getQty_invoiced());
		}else if(this.getQty_invoicedDirtyFlag()){
			map.put("qty_invoiced",false);
		}
		if(this.getQty_to_invoice()!=null&&this.getQty_to_invoiceDirtyFlag()){
			map.put("qty_to_invoice",this.getQty_to_invoice());
		}else if(this.getQty_to_invoiceDirtyFlag()){
			map.put("qty_to_invoice",false);
		}
		if(this.getSource_id()!=null&&this.getSource_idDirtyFlag()){
			map.put("source_id",this.getSource_id());
		}else if(this.getSource_idDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getSource_id_text()!=null&&this.getSource_id_textDirtyFlag()){
			//忽略文本外键source_id_text
		}else if(this.getSource_id_textDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getTeam_id()!=null&&this.getTeam_idDirtyFlag()){
			map.put("team_id",this.getTeam_id());
		}else if(this.getTeam_idDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getTeam_id_text()!=null&&this.getTeam_id_textDirtyFlag()){
			//忽略文本外键team_id_text
		}else if(this.getTeam_id_textDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getUntaxed_amount_invoiced()!=null&&this.getUntaxed_amount_invoicedDirtyFlag()){
			map.put("untaxed_amount_invoiced",this.getUntaxed_amount_invoiced());
		}else if(this.getUntaxed_amount_invoicedDirtyFlag()){
			map.put("untaxed_amount_invoiced",false);
		}
		if(this.getUntaxed_amount_to_invoice()!=null&&this.getUntaxed_amount_to_invoiceDirtyFlag()){
			map.put("untaxed_amount_to_invoice",this.getUntaxed_amount_to_invoice());
		}else if(this.getUntaxed_amount_to_invoiceDirtyFlag()){
			map.put("untaxed_amount_to_invoice",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getVolume()!=null&&this.getVolumeDirtyFlag()){
			map.put("volume",this.getVolume());
		}else if(this.getVolumeDirtyFlag()){
			map.put("volume",false);
		}
		if(this.getWarehouse_id()!=null&&this.getWarehouse_idDirtyFlag()){
			map.put("warehouse_id",this.getWarehouse_id());
		}else if(this.getWarehouse_idDirtyFlag()){
			map.put("warehouse_id",false);
		}
		if(this.getWarehouse_id_text()!=null&&this.getWarehouse_id_textDirtyFlag()){
			//忽略文本外键warehouse_id_text
		}else if(this.getWarehouse_id_textDirtyFlag()){
			map.put("warehouse_id",false);
		}
		if(this.getWebsite_id()!=null&&this.getWebsite_idDirtyFlag()){
			map.put("website_id",this.getWebsite_id());
		}else if(this.getWebsite_idDirtyFlag()){
			map.put("website_id",false);
		}
		if(this.getWeight()!=null&&this.getWeightDirtyFlag()){
			map.put("weight",this.getWeight());
		}else if(this.getWeightDirtyFlag()){
			map.put("weight",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
