package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_presenceSearchContext;

/**
 * 实体 [用户上线] 存储模型
 */
public interface Bus_presence{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后登录
     */
    Timestamp getLast_presence();

    void setLast_presence(Timestamp last_presence);

    /**
     * 获取 [最后登录]脏标记
     */
    boolean getLast_presenceDirtyFlag();

    /**
     * 最后在线
     */
    Timestamp getLast_poll();

    void setLast_poll(Timestamp last_poll);

    /**
     * 获取 [最后在线]脏标记
     */
    boolean getLast_pollDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * IM的状态
     */
    String getStatus();

    void setStatus(String status);

    /**
     * 获取 [IM的状态]脏标记
     */
    boolean getStatusDirtyFlag();

    /**
     * 最后修改时间
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改时间]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 用户
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 用户
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_idDirtyFlag();

}
