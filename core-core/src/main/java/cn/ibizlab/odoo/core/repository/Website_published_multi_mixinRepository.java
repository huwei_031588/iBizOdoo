package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Website_published_multi_mixin;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_published_multi_mixinSearchContext;

/**
 * 实体 [多网站发布的Mixin] 存储对象
 */
public interface Website_published_multi_mixinRepository extends Repository<Website_published_multi_mixin> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Website_published_multi_mixin> searchDefault(Website_published_multi_mixinSearchContext context);

    Website_published_multi_mixin convert2PO(cn.ibizlab.odoo.core.odoo_website.domain.Website_published_multi_mixin domain , Website_published_multi_mixin po) ;

    cn.ibizlab.odoo.core.odoo_website.domain.Website_published_multi_mixin convert2Domain( Website_published_multi_mixin po ,cn.ibizlab.odoo.core.odoo_website.domain.Website_published_multi_mixin domain) ;

}
