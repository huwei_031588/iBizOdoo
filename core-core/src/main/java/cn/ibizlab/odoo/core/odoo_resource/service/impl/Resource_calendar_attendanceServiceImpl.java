package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendar_attendanceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_resource.client.resource_calendar_attendanceOdooClient;
import cn.ibizlab.odoo.core.odoo_resource.clientmodel.resource_calendar_attendanceClientModel;

/**
 * 实体[工作细节] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_calendar_attendanceServiceImpl implements IResource_calendar_attendanceService {

    @Autowired
    resource_calendar_attendanceOdooClient resource_calendar_attendanceOdooClient;


    @Override
    public boolean remove(Integer id) {
        resource_calendar_attendanceClientModel clientModel = new resource_calendar_attendanceClientModel();
        clientModel.setId(id);
		resource_calendar_attendanceOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Resource_calendar_attendance get(Integer id) {
        resource_calendar_attendanceClientModel clientModel = new resource_calendar_attendanceClientModel();
        clientModel.setId(id);
		resource_calendar_attendanceOdooClient.get(clientModel);
        Resource_calendar_attendance et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Resource_calendar_attendance();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Resource_calendar_attendance et) {
        resource_calendar_attendanceClientModel clientModel = convert2Model(et,null);
		resource_calendar_attendanceOdooClient.update(clientModel);
        Resource_calendar_attendance rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Resource_calendar_attendance> list){
    }

    @Override
    public boolean create(Resource_calendar_attendance et) {
        resource_calendar_attendanceClientModel clientModel = convert2Model(et,null);
		resource_calendar_attendanceOdooClient.create(clientModel);
        Resource_calendar_attendance rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_calendar_attendance> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_calendar_attendance> searchDefault(Resource_calendar_attendanceSearchContext context) {
        List<Resource_calendar_attendance> list = new ArrayList<Resource_calendar_attendance>();
        Page<resource_calendar_attendanceClientModel> clientModelList = resource_calendar_attendanceOdooClient.search(context);
        for(resource_calendar_attendanceClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Resource_calendar_attendance>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public resource_calendar_attendanceClientModel convert2Model(Resource_calendar_attendance domain , resource_calendar_attendanceClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new resource_calendar_attendanceClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("dayofweekdirtyflag"))
                model.setDayofweek(domain.getDayofweek());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("hour_todirtyflag"))
                model.setHour_to(domain.getHourTo());
            if((Boolean) domain.getExtensionparams().get("day_perioddirtyflag"))
                model.setDay_period(domain.getDayPeriod());
            if((Boolean) domain.getExtensionparams().get("hour_fromdirtyflag"))
                model.setHour_from(domain.getHourFrom());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("date_todirtyflag"))
                model.setDate_to(domain.getDateTo());
            if((Boolean) domain.getExtensionparams().get("date_fromdirtyflag"))
                model.setDate_from(domain.getDateFrom());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("calendar_id_textdirtyflag"))
                model.setCalendar_id_text(domain.getCalendarIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("calendar_iddirtyflag"))
                model.setCalendar_id(domain.getCalendarId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Resource_calendar_attendance convert2Domain( resource_calendar_attendanceClientModel model ,Resource_calendar_attendance domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Resource_calendar_attendance();
        }

        if(model.getDayofweekDirtyFlag())
            domain.setDayofweek(model.getDayofweek());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getHour_toDirtyFlag())
            domain.setHourTo(model.getHour_to());
        if(model.getDay_periodDirtyFlag())
            domain.setDayPeriod(model.getDay_period());
        if(model.getHour_fromDirtyFlag())
            domain.setHourFrom(model.getHour_from());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDate_toDirtyFlag())
            domain.setDateTo(model.getDate_to());
        if(model.getDate_fromDirtyFlag())
            domain.setDateFrom(model.getDate_from());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCalendar_id_textDirtyFlag())
            domain.setCalendarIdText(model.getCalendar_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCalendar_idDirtyFlag())
            domain.setCalendarId(model.getCalendar_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



