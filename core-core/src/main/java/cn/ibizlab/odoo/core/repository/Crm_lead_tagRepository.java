package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_lead_tag;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_tagSearchContext;

/**
 * 实体 [线索标签] 存储对象
 */
public interface Crm_lead_tagRepository extends Repository<Crm_lead_tag> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_lead_tag> searchDefault(Crm_lead_tagSearchContext context);

    Crm_lead_tag convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_tag domain , Crm_lead_tag po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_tag convert2Domain( Crm_lead_tag po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_tag domain) ;

}
