package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Project_tags;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_tagsSearchContext;

/**
 * 实体 [项目标签] 存储对象
 */
public interface Project_tagsRepository extends Repository<Project_tags> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Project_tags> searchDefault(Project_tagsSearchContext context);

    Project_tags convert2PO(cn.ibizlab.odoo.core.odoo_project.domain.Project_tags domain , Project_tags po) ;

    cn.ibizlab.odoo.core.odoo_project.domain.Project_tags convert2Domain( Project_tags po ,cn.ibizlab.odoo.core.odoo_project.domain.Project_tags domain) ;

}
