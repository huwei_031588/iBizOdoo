package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.product_style;

/**
 * 实体[product_style] 服务对象接口
 */
public interface product_styleRepository{


    public product_style createPO() ;
        public List<product_style> search();

        public void update(product_style product_style);

        public void createBatch(product_style product_style);

        public void create(product_style product_style);

        public void remove(String id);

        public void removeBatch(String id);

        public void get(String id);

        public void updateBatch(product_style product_style);


}
