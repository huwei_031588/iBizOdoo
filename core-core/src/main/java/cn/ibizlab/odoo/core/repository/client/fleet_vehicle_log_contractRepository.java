package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_vehicle_log_contract;

/**
 * 实体[fleet_vehicle_log_contract] 服务对象接口
 */
public interface fleet_vehicle_log_contractRepository{


    public fleet_vehicle_log_contract createPO() ;
        public void remove(String id);

        public List<fleet_vehicle_log_contract> search();

        public void update(fleet_vehicle_log_contract fleet_vehicle_log_contract);

        public void get(String id);

        public void updateBatch(fleet_vehicle_log_contract fleet_vehicle_log_contract);

        public void createBatch(fleet_vehicle_log_contract fleet_vehicle_log_contract);

        public void create(fleet_vehicle_log_contract fleet_vehicle_log_contract);

        public void removeBatch(String id);


}
