package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_lineSearchContext;

/**
 * 实体 [银行对账单明细] 存储模型
 */
public interface Account_bank_statement_line{

    /**
     * 合作伙伴名称
     */
    String getPartner_name();

    void setPartner_name(String partner_name);

    /**
     * 获取 [合作伙伴名称]脏标记
     */
    boolean getPartner_nameDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 银行账户号码
     */
    String getAccount_number();

    void setAccount_number(String account_number);

    /**
     * 获取 [银行账户号码]脏标记
     */
    boolean getAccount_numberDirtyFlag();

    /**
     * 参考
     */
    String getRef();

    void setRef(String ref);

    /**
     * 获取 [参考]脏标记
     */
    boolean getRefDirtyFlag();

    /**
     * 备注
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [备注]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 日记账项目
     */
    String getJournal_entry_ids();

    void setJournal_entry_ids(String journal_entry_ids);

    /**
     * 获取 [日记账项目]脏标记
     */
    boolean getJournal_entry_idsDirtyFlag();

    /**
     * 标签
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [标签]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 日记账分录名称
     */
    String getMove_name();

    void setMove_name(String move_name);

    /**
     * 获取 [日记账分录名称]脏标记
     */
    boolean getMove_nameDirtyFlag();

    /**
     * 导入ID
     */
    String getUnique_import_id();

    void setUnique_import_id(String unique_import_id);

    /**
     * 获取 [导入ID]脏标记
     */
    boolean getUnique_import_idDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * POS报表
     */
    Integer getPos_statement_id();

    void setPos_statement_id(Integer pos_statement_id);

    /**
     * 获取 [POS报表]脏标记
     */
    boolean getPos_statement_idDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 货币金额
     */
    Double getAmount_currency();

    void setAmount_currency(Double amount_currency);

    /**
     * 获取 [货币金额]脏标记
     */
    boolean getAmount_currencyDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 日记账
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 现金分类账
     */
    Integer getJournal_currency_id();

    void setJournal_currency_id(Integer journal_currency_id);

    /**
     * 获取 [现金分类账]脏标记
     */
    boolean getJournal_currency_idDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 报告
     */
    String getStatement_id_text();

    void setStatement_id_text(String statement_id_text);

    /**
     * 获取 [报告]脏标记
     */
    boolean getStatement_id_textDirtyFlag();

    /**
     * 对方科目
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [对方科目]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 对方科目
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [对方科目]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 银行账户
     */
    Integer getBank_account_id();

    void setBank_account_id(Integer bank_account_id);

    /**
     * 获取 [银行账户]脏标记
     */
    boolean getBank_account_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 日记账
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * 报告
     */
    Integer getStatement_id();

    void setStatement_id(Integer statement_id);

    /**
     * 获取 [报告]脏标记
     */
    boolean getStatement_idDirtyFlag();

}
