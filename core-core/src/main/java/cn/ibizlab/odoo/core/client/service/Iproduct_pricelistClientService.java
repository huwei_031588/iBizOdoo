package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_pricelist] 服务对象接口
 */
public interface Iproduct_pricelistClientService{

    public Iproduct_pricelist createModel() ;

    public void remove(Iproduct_pricelist product_pricelist);

    public void get(Iproduct_pricelist product_pricelist);

    public Page<Iproduct_pricelist> search(SearchContext context);

    public void createBatch(List<Iproduct_pricelist> product_pricelists);

    public void updateBatch(List<Iproduct_pricelist> product_pricelists);

    public void removeBatch(List<Iproduct_pricelist> product_pricelists);

    public void create(Iproduct_pricelist product_pricelist);

    public void update(Iproduct_pricelist product_pricelist);

    public Page<Iproduct_pricelist> select(SearchContext context);

}
