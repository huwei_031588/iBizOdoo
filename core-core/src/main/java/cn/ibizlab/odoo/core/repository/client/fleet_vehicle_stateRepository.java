package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_vehicle_state;

/**
 * 实体[fleet_vehicle_state] 服务对象接口
 */
public interface fleet_vehicle_stateRepository{


    public fleet_vehicle_state createPO() ;
        public void createBatch(fleet_vehicle_state fleet_vehicle_state);

        public void create(fleet_vehicle_state fleet_vehicle_state);

        public void updateBatch(fleet_vehicle_state fleet_vehicle_state);

        public void removeBatch(String id);

        public List<fleet_vehicle_state> search();

        public void remove(String id);

        public void update(fleet_vehicle_state fleet_vehicle_state);

        public void get(String id);


}
