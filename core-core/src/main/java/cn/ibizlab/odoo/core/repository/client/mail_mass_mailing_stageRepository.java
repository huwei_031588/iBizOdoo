package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mail_mass_mailing_stage;

/**
 * 实体[mail_mass_mailing_stage] 服务对象接口
 */
public interface mail_mass_mailing_stageRepository{


    public mail_mass_mailing_stage createPO() ;
        public void create(mail_mass_mailing_stage mail_mass_mailing_stage);

        public void updateBatch(mail_mass_mailing_stage mail_mass_mailing_stage);

        public void createBatch(mail_mass_mailing_stage mail_mass_mailing_stage);

        public void removeBatch(String id);

        public void update(mail_mass_mailing_stage mail_mass_mailing_stage);

        public void remove(String id);

        public void get(String id);

        public List<mail_mass_mailing_stage> search();


}
