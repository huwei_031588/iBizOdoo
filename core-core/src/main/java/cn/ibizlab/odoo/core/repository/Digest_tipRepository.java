package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Digest_tip;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_tipSearchContext;

/**
 * 实体 [摘要提示] 存储对象
 */
public interface Digest_tipRepository extends Repository<Digest_tip> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Digest_tip> searchDefault(Digest_tipSearchContext context);

    Digest_tip convert2PO(cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip domain , Digest_tip po) ;

    cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip convert2Domain( Digest_tip po ,cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip domain) ;

}
