package cn.ibizlab.odoo.core.odoo_mail.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_mass_mailing_list] 对象
 */
public class mail_mass_mailing_listClientModel implements Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 邮件列表
     */
    public String contact_ids;

    @JsonIgnore
    public boolean contact_idsDirtyFlag;
    
    /**
     * 联系人人数
     */
    public Integer contact_nbr;

    @JsonIgnore
    public boolean contact_nbrDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 公开
     */
    public String is_public;

    @JsonIgnore
    public boolean is_publicDirtyFlag;
    
    /**
     * 邮件列表
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 网站弹出内容
     */
    public String popup_content;

    @JsonIgnore
    public boolean popup_contentDirtyFlag;
    
    /**
     * 网站弹出重新定向网址
     */
    public String popup_redirect_url;

    @JsonIgnore
    public boolean popup_redirect_urlDirtyFlag;
    
    /**
     * 订阅信息
     */
    public String subscription_contact_ids;

    @JsonIgnore
    public boolean subscription_contact_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [邮件列表]
     */
    @JsonProperty("contact_ids")
    public String getContact_ids(){
        return this.contact_ids ;
    }

    /**
     * 设置 [邮件列表]
     */
    @JsonProperty("contact_ids")
    public void setContact_ids(String  contact_ids){
        this.contact_ids = contact_ids ;
        this.contact_idsDirtyFlag = true ;
    }

     /**
     * 获取 [邮件列表]脏标记
     */
    @JsonIgnore
    public boolean getContact_idsDirtyFlag(){
        return this.contact_idsDirtyFlag ;
    }   

    /**
     * 获取 [联系人人数]
     */
    @JsonProperty("contact_nbr")
    public Integer getContact_nbr(){
        return this.contact_nbr ;
    }

    /**
     * 设置 [联系人人数]
     */
    @JsonProperty("contact_nbr")
    public void setContact_nbr(Integer  contact_nbr){
        this.contact_nbr = contact_nbr ;
        this.contact_nbrDirtyFlag = true ;
    }

     /**
     * 获取 [联系人人数]脏标记
     */
    @JsonIgnore
    public boolean getContact_nbrDirtyFlag(){
        return this.contact_nbrDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [公开]
     */
    @JsonProperty("is_public")
    public String getIs_public(){
        return this.is_public ;
    }

    /**
     * 设置 [公开]
     */
    @JsonProperty("is_public")
    public void setIs_public(String  is_public){
        this.is_public = is_public ;
        this.is_publicDirtyFlag = true ;
    }

     /**
     * 获取 [公开]脏标记
     */
    @JsonIgnore
    public boolean getIs_publicDirtyFlag(){
        return this.is_publicDirtyFlag ;
    }   

    /**
     * 获取 [邮件列表]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [邮件列表]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [邮件列表]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [网站弹出内容]
     */
    @JsonProperty("popup_content")
    public String getPopup_content(){
        return this.popup_content ;
    }

    /**
     * 设置 [网站弹出内容]
     */
    @JsonProperty("popup_content")
    public void setPopup_content(String  popup_content){
        this.popup_content = popup_content ;
        this.popup_contentDirtyFlag = true ;
    }

     /**
     * 获取 [网站弹出内容]脏标记
     */
    @JsonIgnore
    public boolean getPopup_contentDirtyFlag(){
        return this.popup_contentDirtyFlag ;
    }   

    /**
     * 获取 [网站弹出重新定向网址]
     */
    @JsonProperty("popup_redirect_url")
    public String getPopup_redirect_url(){
        return this.popup_redirect_url ;
    }

    /**
     * 设置 [网站弹出重新定向网址]
     */
    @JsonProperty("popup_redirect_url")
    public void setPopup_redirect_url(String  popup_redirect_url){
        this.popup_redirect_url = popup_redirect_url ;
        this.popup_redirect_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站弹出重新定向网址]脏标记
     */
    @JsonIgnore
    public boolean getPopup_redirect_urlDirtyFlag(){
        return this.popup_redirect_urlDirtyFlag ;
    }   

    /**
     * 获取 [订阅信息]
     */
    @JsonProperty("subscription_contact_ids")
    public String getSubscription_contact_ids(){
        return this.subscription_contact_ids ;
    }

    /**
     * 设置 [订阅信息]
     */
    @JsonProperty("subscription_contact_ids")
    public void setSubscription_contact_ids(String  subscription_contact_ids){
        this.subscription_contact_ids = subscription_contact_ids ;
        this.subscription_contact_idsDirtyFlag = true ;
    }

     /**
     * 获取 [订阅信息]脏标记
     */
    @JsonIgnore
    public boolean getSubscription_contact_idsDirtyFlag(){
        return this.subscription_contact_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("contact_ids") instanceof Boolean)&& map.get("contact_ids")!=null){
			Object[] objs = (Object[])map.get("contact_ids");
			if(objs.length > 0){
				Integer[] contact_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setContact_ids(Arrays.toString(contact_ids).replace(" ",""));
			}
		}
		if(!(map.get("contact_nbr") instanceof Boolean)&& map.get("contact_nbr")!=null){
			this.setContact_nbr((Integer)map.get("contact_nbr"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_public") instanceof Boolean){
			this.setIs_public(((Boolean)map.get("is_public"))? "true" : "false");
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("popup_content") instanceof Boolean)&& map.get("popup_content")!=null){
			this.setPopup_content((String)map.get("popup_content"));
		}
		if(!(map.get("popup_redirect_url") instanceof Boolean)&& map.get("popup_redirect_url")!=null){
			this.setPopup_redirect_url((String)map.get("popup_redirect_url"));
		}
		if(!(map.get("subscription_contact_ids") instanceof Boolean)&& map.get("subscription_contact_ids")!=null){
			Object[] objs = (Object[])map.get("subscription_contact_ids");
			if(objs.length > 0){
				Integer[] subscription_contact_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setSubscription_contact_ids(Arrays.toString(subscription_contact_ids).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getContact_ids()!=null&&this.getContact_idsDirtyFlag()){
			map.put("contact_ids",this.getContact_ids());
		}else if(this.getContact_idsDirtyFlag()){
			map.put("contact_ids",false);
		}
		if(this.getContact_nbr()!=null&&this.getContact_nbrDirtyFlag()){
			map.put("contact_nbr",this.getContact_nbr());
		}else if(this.getContact_nbrDirtyFlag()){
			map.put("contact_nbr",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_public()!=null&&this.getIs_publicDirtyFlag()){
			map.put("is_public",Boolean.parseBoolean(this.getIs_public()));		
		}		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPopup_content()!=null&&this.getPopup_contentDirtyFlag()){
			map.put("popup_content",this.getPopup_content());
		}else if(this.getPopup_contentDirtyFlag()){
			map.put("popup_content",false);
		}
		if(this.getPopup_redirect_url()!=null&&this.getPopup_redirect_urlDirtyFlag()){
			map.put("popup_redirect_url",this.getPopup_redirect_url());
		}else if(this.getPopup_redirect_urlDirtyFlag()){
			map.put("popup_redirect_url",false);
		}
		if(this.getSubscription_contact_ids()!=null&&this.getSubscription_contact_idsDirtyFlag()){
			map.put("subscription_contact_ids",this.getSubscription_contact_ids());
		}else if(this.getSubscription_contact_idsDirtyFlag()){
			map.put("subscription_contact_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
