package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_fuelSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_log_fuelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_log_fuelOdooClient;
import cn.ibizlab.odoo.core.odoo_fleet.clientmodel.fleet_vehicle_log_fuelClientModel;

/**
 * 实体[车辆燃油记录] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_log_fuelServiceImpl implements IFleet_vehicle_log_fuelService {

    @Autowired
    fleet_vehicle_log_fuelOdooClient fleet_vehicle_log_fuelOdooClient;


    @Override
    public boolean remove(Integer id) {
        fleet_vehicle_log_fuelClientModel clientModel = new fleet_vehicle_log_fuelClientModel();
        clientModel.setId(id);
		fleet_vehicle_log_fuelOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Fleet_vehicle_log_fuel et) {
        fleet_vehicle_log_fuelClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_log_fuelOdooClient.create(clientModel);
        Fleet_vehicle_log_fuel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_log_fuel> list){
    }

    @Override
    public boolean update(Fleet_vehicle_log_fuel et) {
        fleet_vehicle_log_fuelClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_log_fuelOdooClient.update(clientModel);
        Fleet_vehicle_log_fuel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Fleet_vehicle_log_fuel> list){
    }

    @Override
    public Fleet_vehicle_log_fuel get(Integer id) {
        fleet_vehicle_log_fuelClientModel clientModel = new fleet_vehicle_log_fuelClientModel();
        clientModel.setId(id);
		fleet_vehicle_log_fuelOdooClient.get(clientModel);
        Fleet_vehicle_log_fuel et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Fleet_vehicle_log_fuel();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_log_fuel> searchDefault(Fleet_vehicle_log_fuelSearchContext context) {
        List<Fleet_vehicle_log_fuel> list = new ArrayList<Fleet_vehicle_log_fuel>();
        Page<fleet_vehicle_log_fuelClientModel> clientModelList = fleet_vehicle_log_fuelOdooClient.search(context);
        for(fleet_vehicle_log_fuelClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Fleet_vehicle_log_fuel>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public fleet_vehicle_log_fuelClientModel convert2Model(Fleet_vehicle_log_fuel domain , fleet_vehicle_log_fuelClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new fleet_vehicle_log_fuelClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("price_per_literdirtyflag"))
                model.setPrice_per_liter(domain.getPricePerLiter());
            if((Boolean) domain.getExtensionparams().get("cost_idsdirtyflag"))
                model.setCost_ids(domain.getCostIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("literdirtyflag"))
                model.setLiter(domain.getLiter());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("notesdirtyflag"))
                model.setNotes(domain.getNotes());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("inv_refdirtyflag"))
                model.setInv_ref(domain.getInvRef());
            if((Boolean) domain.getExtensionparams().get("cost_subtype_iddirtyflag"))
                model.setCost_subtype_id(domain.getCostSubtypeId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("odometer_iddirtyflag"))
                model.setOdometer_id(domain.getOdometerId());
            if((Boolean) domain.getExtensionparams().get("cost_typedirtyflag"))
                model.setCost_type(domain.getCostType());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("purchaser_id_textdirtyflag"))
                model.setPurchaser_id_text(domain.getPurchaserIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("cost_amountdirtyflag"))
                model.setCost_amount(domain.getCostAmount());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("vehicle_iddirtyflag"))
                model.setVehicle_id(domain.getVehicleId());
            if((Boolean) domain.getExtensionparams().get("auto_generateddirtyflag"))
                model.setAuto_generated(domain.getAutoGenerated());
            if((Boolean) domain.getExtensionparams().get("contract_iddirtyflag"))
                model.setContract_id(domain.getContractId());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("vendor_id_textdirtyflag"))
                model.setVendor_id_text(domain.getVendorIdText());
            if((Boolean) domain.getExtensionparams().get("odometerdirtyflag"))
                model.setOdometer(domain.getOdometer());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("odometer_unitdirtyflag"))
                model.setOdometer_unit(domain.getOdometerUnit());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("cost_iddirtyflag"))
                model.setCost_id(domain.getCostId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("vendor_iddirtyflag"))
                model.setVendor_id(domain.getVendorId());
            if((Boolean) domain.getExtensionparams().get("purchaser_iddirtyflag"))
                model.setPurchaser_id(domain.getPurchaserId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Fleet_vehicle_log_fuel convert2Domain( fleet_vehicle_log_fuelClientModel model ,Fleet_vehicle_log_fuel domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Fleet_vehicle_log_fuel();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getPrice_per_literDirtyFlag())
            domain.setPricePerLiter(model.getPrice_per_liter());
        if(model.getCost_idsDirtyFlag())
            domain.setCostIds(model.getCost_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getLiterDirtyFlag())
            domain.setLiter(model.getLiter());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNotesDirtyFlag())
            domain.setNotes(model.getNotes());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getInv_refDirtyFlag())
            domain.setInvRef(model.getInv_ref());
        if(model.getCost_subtype_idDirtyFlag())
            domain.setCostSubtypeId(model.getCost_subtype_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getOdometer_idDirtyFlag())
            domain.setOdometerId(model.getOdometer_id());
        if(model.getCost_typeDirtyFlag())
            domain.setCostType(model.getCost_type());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getPurchaser_id_textDirtyFlag())
            domain.setPurchaserIdText(model.getPurchaser_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCost_amountDirtyFlag())
            domain.setCostAmount(model.getCost_amount());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getVehicle_idDirtyFlag())
            domain.setVehicleId(model.getVehicle_id());
        if(model.getAuto_generatedDirtyFlag())
            domain.setAutoGenerated(model.getAuto_generated());
        if(model.getContract_idDirtyFlag())
            domain.setContractId(model.getContract_id());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getVendor_id_textDirtyFlag())
            domain.setVendorIdText(model.getVendor_id_text());
        if(model.getOdometerDirtyFlag())
            domain.setOdometer(model.getOdometer());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getOdometer_unitDirtyFlag())
            domain.setOdometerUnit(model.getOdometer_unit());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCost_idDirtyFlag())
            domain.setCostId(model.getCost_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getVendor_idDirtyFlag())
            domain.setVendorId(model.getVendor_id());
        if(model.getPurchaser_idDirtyFlag())
            domain.setPurchaserId(model.getPurchaser_id());
        return domain ;
    }

}

    



