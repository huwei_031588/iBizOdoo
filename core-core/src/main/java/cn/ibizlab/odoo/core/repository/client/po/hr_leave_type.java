package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_leave_type] 对象
 */
public interface hr_leave_type {

    public String getActive();

    public void setActive(String active);

    public String getAllocation_type();

    public void setAllocation_type(String allocation_type);

    public Integer getCateg_id();

    public void setCateg_id(Integer categ_id);

    public String getCateg_id_text();

    public void setCateg_id_text(String categ_id_text);

    public String getColor_name();

    public void setColor_name(String color_name);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getDouble_validation();

    public void setDouble_validation(String double_validation);

    public Double getGroup_days_allocation();

    public void setGroup_days_allocation(Double group_days_allocation);

    public Double getGroup_days_leave();

    public void setGroup_days_leave(Double group_days_leave);

    public Integer getId();

    public void setId(Integer id);

    public Double getLeaves_taken();

    public void setLeaves_taken(Double leaves_taken);

    public Double getMax_leaves();

    public void setMax_leaves(Double max_leaves);

    public String getName();

    public void setName(String name);

    public Double getRemaining_leaves();

    public void setRemaining_leaves(Double remaining_leaves);

    public String getRequest_unit();

    public void setRequest_unit(String request_unit);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public String getTime_type();

    public void setTime_type(String time_type);

    public String getUnpaid();

    public void setUnpaid(String unpaid);

    public String getValid();

    public void setValid(String valid);

    public String getValidation_type();

    public void setValidation_type(String validation_type);

    public Timestamp getValidity_start();

    public void setValidity_start(Timestamp validity_start);

    public Timestamp getValidity_stop();

    public void setValidity_stop(Timestamp validity_stop);

    public Double getVirtual_remaining_leaves();

    public void setVirtual_remaining_leaves(Double virtual_remaining_leaves);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
