package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_survey.client.survey_stageOdooClient;
import cn.ibizlab.odoo.core.odoo_survey.clientmodel.survey_stageClientModel;

/**
 * 实体[调查阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_stageServiceImpl implements ISurvey_stageService {

    @Autowired
    survey_stageOdooClient survey_stageOdooClient;


    @Override
    public boolean update(Survey_stage et) {
        survey_stageClientModel clientModel = convert2Model(et,null);
		survey_stageOdooClient.update(clientModel);
        Survey_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Survey_stage> list){
    }

    @Override
    public Survey_stage get(Integer id) {
        survey_stageClientModel clientModel = new survey_stageClientModel();
        clientModel.setId(id);
		survey_stageOdooClient.get(clientModel);
        Survey_stage et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Survey_stage();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Survey_stage et) {
        survey_stageClientModel clientModel = convert2Model(et,null);
		survey_stageOdooClient.create(clientModel);
        Survey_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_stage> list){
    }

    @Override
    public boolean remove(Integer id) {
        survey_stageClientModel clientModel = new survey_stageClientModel();
        clientModel.setId(id);
		survey_stageOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_stage> searchDefault(Survey_stageSearchContext context) {
        List<Survey_stage> list = new ArrayList<Survey_stage>();
        Page<survey_stageClientModel> clientModelList = survey_stageOdooClient.search(context);
        for(survey_stageClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Survey_stage>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public survey_stageClientModel convert2Model(Survey_stage domain , survey_stageClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new survey_stageClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("closeddirtyflag"))
                model.setClosed(domain.getClosed());
            if((Boolean) domain.getExtensionparams().get("folddirtyflag"))
                model.setFold(domain.getFold());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Survey_stage convert2Domain( survey_stageClientModel model ,Survey_stage domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Survey_stage();
        }

        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getClosedDirtyFlag())
            domain.setClosed(model.getClosed());
        if(model.getFoldDirtyFlag())
            domain.setFold(model.getFold());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



