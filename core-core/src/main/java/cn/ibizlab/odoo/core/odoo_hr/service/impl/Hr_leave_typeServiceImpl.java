package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_leave_typeOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_leave_typeClientModel;

/**
 * 实体[休假类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_leave_typeServiceImpl implements IHr_leave_typeService {

    @Autowired
    hr_leave_typeOdooClient hr_leave_typeOdooClient;


    @Override
    public boolean create(Hr_leave_type et) {
        hr_leave_typeClientModel clientModel = convert2Model(et,null);
		hr_leave_typeOdooClient.create(clientModel);
        Hr_leave_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_leave_type> list){
    }

    @Override
    public boolean update(Hr_leave_type et) {
        hr_leave_typeClientModel clientModel = convert2Model(et,null);
		hr_leave_typeOdooClient.update(clientModel);
        Hr_leave_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_leave_type> list){
    }

    @Override
    public Hr_leave_type get(Integer id) {
        hr_leave_typeClientModel clientModel = new hr_leave_typeClientModel();
        clientModel.setId(id);
		hr_leave_typeOdooClient.get(clientModel);
        Hr_leave_type et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_leave_type();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        hr_leave_typeClientModel clientModel = new hr_leave_typeClientModel();
        clientModel.setId(id);
		hr_leave_typeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_leave_type> searchDefault(Hr_leave_typeSearchContext context) {
        List<Hr_leave_type> list = new ArrayList<Hr_leave_type>();
        Page<hr_leave_typeClientModel> clientModelList = hr_leave_typeOdooClient.search(context);
        for(hr_leave_typeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_leave_type>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_leave_typeClientModel convert2Model(Hr_leave_type domain , hr_leave_typeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_leave_typeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("validdirtyflag"))
                model.setValid(domain.getValid());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("virtual_remaining_leavesdirtyflag"))
                model.setVirtual_remaining_leaves(domain.getVirtualRemainingLeaves());
            if((Boolean) domain.getExtensionparams().get("leaves_takendirtyflag"))
                model.setLeaves_taken(domain.getLeavesTaken());
            if((Boolean) domain.getExtensionparams().get("group_days_allocationdirtyflag"))
                model.setGroup_days_allocation(domain.getGroupDaysAllocation());
            if((Boolean) domain.getExtensionparams().get("double_validationdirtyflag"))
                model.setDouble_validation(domain.getDoubleValidation());
            if((Boolean) domain.getExtensionparams().get("allocation_typedirtyflag"))
                model.setAllocation_type(domain.getAllocationType());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("unpaiddirtyflag"))
                model.setUnpaid(domain.getUnpaid());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("max_leavesdirtyflag"))
                model.setMax_leaves(domain.getMaxLeaves());
            if((Boolean) domain.getExtensionparams().get("validity_stopdirtyflag"))
                model.setValidity_stop(domain.getValidityStop());
            if((Boolean) domain.getExtensionparams().get("validation_typedirtyflag"))
                model.setValidation_type(domain.getValidationType());
            if((Boolean) domain.getExtensionparams().get("time_typedirtyflag"))
                model.setTime_type(domain.getTimeType());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("request_unitdirtyflag"))
                model.setRequest_unit(domain.getRequestUnit());
            if((Boolean) domain.getExtensionparams().get("group_days_leavedirtyflag"))
                model.setGroup_days_leave(domain.getGroupDaysLeave());
            if((Boolean) domain.getExtensionparams().get("color_namedirtyflag"))
                model.setColor_name(domain.getColorName());
            if((Boolean) domain.getExtensionparams().get("remaining_leavesdirtyflag"))
                model.setRemaining_leaves(domain.getRemainingLeaves());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("validity_startdirtyflag"))
                model.setValidity_start(domain.getValidityStart());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("categ_id_textdirtyflag"))
                model.setCateg_id_text(domain.getCategIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("categ_iddirtyflag"))
                model.setCateg_id(domain.getCategId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_leave_type convert2Domain( hr_leave_typeClientModel model ,Hr_leave_type domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_leave_type();
        }

        if(model.getValidDirtyFlag())
            domain.setValid(model.getValid());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getVirtual_remaining_leavesDirtyFlag())
            domain.setVirtualRemainingLeaves(model.getVirtual_remaining_leaves());
        if(model.getLeaves_takenDirtyFlag())
            domain.setLeavesTaken(model.getLeaves_taken());
        if(model.getGroup_days_allocationDirtyFlag())
            domain.setGroupDaysAllocation(model.getGroup_days_allocation());
        if(model.getDouble_validationDirtyFlag())
            domain.setDoubleValidation(model.getDouble_validation());
        if(model.getAllocation_typeDirtyFlag())
            domain.setAllocationType(model.getAllocation_type());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getUnpaidDirtyFlag())
            domain.setUnpaid(model.getUnpaid());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMax_leavesDirtyFlag())
            domain.setMaxLeaves(model.getMax_leaves());
        if(model.getValidity_stopDirtyFlag())
            domain.setValidityStop(model.getValidity_stop());
        if(model.getValidation_typeDirtyFlag())
            domain.setValidationType(model.getValidation_type());
        if(model.getTime_typeDirtyFlag())
            domain.setTimeType(model.getTime_type());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getRequest_unitDirtyFlag())
            domain.setRequestUnit(model.getRequest_unit());
        if(model.getGroup_days_leaveDirtyFlag())
            domain.setGroupDaysLeave(model.getGroup_days_leave());
        if(model.getColor_nameDirtyFlag())
            domain.setColorName(model.getColor_name());
        if(model.getRemaining_leavesDirtyFlag())
            domain.setRemainingLeaves(model.getRemaining_leaves());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getValidity_startDirtyFlag())
            domain.setValidityStart(model.getValidity_start());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getCateg_id_textDirtyFlag())
            domain.setCategIdText(model.getCateg_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCateg_idDirtyFlag())
            domain.setCategId(model.getCateg_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



