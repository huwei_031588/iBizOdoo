package cn.ibizlab.odoo.core.odoo_base.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[base_partner_merge_automatic_wizard] 对象
 */
public class base_partner_merge_automatic_wizardClientModel implements Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 当前行
     */
    public Integer current_line_id;

    @JsonIgnore
    public boolean current_line_idDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 目的地之联系人
     */
    public Integer dst_partner_id;

    @JsonIgnore
    public boolean dst_partner_idDirtyFlag;
    
    /**
     * 目的地之联系人
     */
    public String dst_partner_id_text;

    @JsonIgnore
    public boolean dst_partner_id_textDirtyFlag;
    
    /**
     * 与系统用户相关的联系人
     */
    public String exclude_contact;

    @JsonIgnore
    public boolean exclude_contactDirtyFlag;
    
    /**
     * 与日记账相关的联系人
     */
    public String exclude_journal_item;

    @JsonIgnore
    public boolean exclude_journal_itemDirtyFlag;
    
    /**
     * EMail
     */
    public String group_by_email;

    @JsonIgnore
    public boolean group_by_emailDirtyFlag;
    
    /**
     * 是公司
     */
    public String group_by_is_company;

    @JsonIgnore
    public boolean group_by_is_companyDirtyFlag;
    
    /**
     * 名称
     */
    public String group_by_name;

    @JsonIgnore
    public boolean group_by_nameDirtyFlag;
    
    /**
     * 上级公司
     */
    public String group_by_parent_id;

    @JsonIgnore
    public boolean group_by_parent_idDirtyFlag;
    
    /**
     * 增值税
     */
    public String group_by_vat;

    @JsonIgnore
    public boolean group_by_vatDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 明细行
     */
    public String line_ids;

    @JsonIgnore
    public boolean line_idsDirtyFlag;
    
    /**
     * 联系人组的最多联系人数量
     */
    public Integer maximum_group;

    @JsonIgnore
    public boolean maximum_groupDirtyFlag;
    
    /**
     * 联系人组
     */
    public Integer number_group;

    @JsonIgnore
    public boolean number_groupDirtyFlag;
    
    /**
     * 联系人
     */
    public String partner_ids;

    @JsonIgnore
    public boolean partner_idsDirtyFlag;
    
    /**
     * 省/ 州
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [当前行]
     */
    @JsonProperty("current_line_id")
    public Integer getCurrent_line_id(){
        return this.current_line_id ;
    }

    /**
     * 设置 [当前行]
     */
    @JsonProperty("current_line_id")
    public void setCurrent_line_id(Integer  current_line_id){
        this.current_line_id = current_line_id ;
        this.current_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [当前行]脏标记
     */
    @JsonIgnore
    public boolean getCurrent_line_idDirtyFlag(){
        return this.current_line_idDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [目的地之联系人]
     */
    @JsonProperty("dst_partner_id")
    public Integer getDst_partner_id(){
        return this.dst_partner_id ;
    }

    /**
     * 设置 [目的地之联系人]
     */
    @JsonProperty("dst_partner_id")
    public void setDst_partner_id(Integer  dst_partner_id){
        this.dst_partner_id = dst_partner_id ;
        this.dst_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [目的地之联系人]脏标记
     */
    @JsonIgnore
    public boolean getDst_partner_idDirtyFlag(){
        return this.dst_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [目的地之联系人]
     */
    @JsonProperty("dst_partner_id_text")
    public String getDst_partner_id_text(){
        return this.dst_partner_id_text ;
    }

    /**
     * 设置 [目的地之联系人]
     */
    @JsonProperty("dst_partner_id_text")
    public void setDst_partner_id_text(String  dst_partner_id_text){
        this.dst_partner_id_text = dst_partner_id_text ;
        this.dst_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目的地之联系人]脏标记
     */
    @JsonIgnore
    public boolean getDst_partner_id_textDirtyFlag(){
        return this.dst_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [与系统用户相关的联系人]
     */
    @JsonProperty("exclude_contact")
    public String getExclude_contact(){
        return this.exclude_contact ;
    }

    /**
     * 设置 [与系统用户相关的联系人]
     */
    @JsonProperty("exclude_contact")
    public void setExclude_contact(String  exclude_contact){
        this.exclude_contact = exclude_contact ;
        this.exclude_contactDirtyFlag = true ;
    }

     /**
     * 获取 [与系统用户相关的联系人]脏标记
     */
    @JsonIgnore
    public boolean getExclude_contactDirtyFlag(){
        return this.exclude_contactDirtyFlag ;
    }   

    /**
     * 获取 [与日记账相关的联系人]
     */
    @JsonProperty("exclude_journal_item")
    public String getExclude_journal_item(){
        return this.exclude_journal_item ;
    }

    /**
     * 设置 [与日记账相关的联系人]
     */
    @JsonProperty("exclude_journal_item")
    public void setExclude_journal_item(String  exclude_journal_item){
        this.exclude_journal_item = exclude_journal_item ;
        this.exclude_journal_itemDirtyFlag = true ;
    }

     /**
     * 获取 [与日记账相关的联系人]脏标记
     */
    @JsonIgnore
    public boolean getExclude_journal_itemDirtyFlag(){
        return this.exclude_journal_itemDirtyFlag ;
    }   

    /**
     * 获取 [EMail]
     */
    @JsonProperty("group_by_email")
    public String getGroup_by_email(){
        return this.group_by_email ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("group_by_email")
    public void setGroup_by_email(String  group_by_email){
        this.group_by_email = group_by_email ;
        this.group_by_emailDirtyFlag = true ;
    }

     /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_emailDirtyFlag(){
        return this.group_by_emailDirtyFlag ;
    }   

    /**
     * 获取 [是公司]
     */
    @JsonProperty("group_by_is_company")
    public String getGroup_by_is_company(){
        return this.group_by_is_company ;
    }

    /**
     * 设置 [是公司]
     */
    @JsonProperty("group_by_is_company")
    public void setGroup_by_is_company(String  group_by_is_company){
        this.group_by_is_company = group_by_is_company ;
        this.group_by_is_companyDirtyFlag = true ;
    }

     /**
     * 获取 [是公司]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_is_companyDirtyFlag(){
        return this.group_by_is_companyDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("group_by_name")
    public String getGroup_by_name(){
        return this.group_by_name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("group_by_name")
    public void setGroup_by_name(String  group_by_name){
        this.group_by_name = group_by_name ;
        this.group_by_nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_nameDirtyFlag(){
        return this.group_by_nameDirtyFlag ;
    }   

    /**
     * 获取 [上级公司]
     */
    @JsonProperty("group_by_parent_id")
    public String getGroup_by_parent_id(){
        return this.group_by_parent_id ;
    }

    /**
     * 设置 [上级公司]
     */
    @JsonProperty("group_by_parent_id")
    public void setGroup_by_parent_id(String  group_by_parent_id){
        this.group_by_parent_id = group_by_parent_id ;
        this.group_by_parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级公司]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_parent_idDirtyFlag(){
        return this.group_by_parent_idDirtyFlag ;
    }   

    /**
     * 获取 [增值税]
     */
    @JsonProperty("group_by_vat")
    public String getGroup_by_vat(){
        return this.group_by_vat ;
    }

    /**
     * 设置 [增值税]
     */
    @JsonProperty("group_by_vat")
    public void setGroup_by_vat(String  group_by_vat){
        this.group_by_vat = group_by_vat ;
        this.group_by_vatDirtyFlag = true ;
    }

     /**
     * 获取 [增值税]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_vatDirtyFlag(){
        return this.group_by_vatDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [明细行]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return this.line_ids ;
    }

    /**
     * 设置 [明细行]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [明细行]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return this.line_idsDirtyFlag ;
    }   

    /**
     * 获取 [联系人组的最多联系人数量]
     */
    @JsonProperty("maximum_group")
    public Integer getMaximum_group(){
        return this.maximum_group ;
    }

    /**
     * 设置 [联系人组的最多联系人数量]
     */
    @JsonProperty("maximum_group")
    public void setMaximum_group(Integer  maximum_group){
        this.maximum_group = maximum_group ;
        this.maximum_groupDirtyFlag = true ;
    }

     /**
     * 获取 [联系人组的最多联系人数量]脏标记
     */
    @JsonIgnore
    public boolean getMaximum_groupDirtyFlag(){
        return this.maximum_groupDirtyFlag ;
    }   

    /**
     * 获取 [联系人组]
     */
    @JsonProperty("number_group")
    public Integer getNumber_group(){
        return this.number_group ;
    }

    /**
     * 设置 [联系人组]
     */
    @JsonProperty("number_group")
    public void setNumber_group(Integer  number_group){
        this.number_group = number_group ;
        this.number_groupDirtyFlag = true ;
    }

     /**
     * 获取 [联系人组]脏标记
     */
    @JsonIgnore
    public boolean getNumber_groupDirtyFlag(){
        return this.number_groupDirtyFlag ;
    }   

    /**
     * 获取 [联系人]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return this.partner_ids ;
    }

    /**
     * 设置 [联系人]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [联系人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return this.partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [省/ 州]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [省/ 州]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [省/ 州]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("current_line_id") instanceof Boolean)&& map.get("current_line_id")!=null){
			Object[] objs = (Object[])map.get("current_line_id");
			if(objs.length > 0){
				this.setCurrent_line_id((Integer)objs[0]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("dst_partner_id") instanceof Boolean)&& map.get("dst_partner_id")!=null){
			Object[] objs = (Object[])map.get("dst_partner_id");
			if(objs.length > 0){
				this.setDst_partner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("dst_partner_id") instanceof Boolean)&& map.get("dst_partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("dst_partner_id");
			if(objs.length > 1){
				this.setDst_partner_id_text((String)objs[1]);
			}
		}
		if(map.get("exclude_contact") instanceof Boolean){
			this.setExclude_contact(((Boolean)map.get("exclude_contact"))? "true" : "false");
		}
		if(map.get("exclude_journal_item") instanceof Boolean){
			this.setExclude_journal_item(((Boolean)map.get("exclude_journal_item"))? "true" : "false");
		}
		if(map.get("group_by_email") instanceof Boolean){
			this.setGroup_by_email(((Boolean)map.get("group_by_email"))? "true" : "false");
		}
		if(map.get("group_by_is_company") instanceof Boolean){
			this.setGroup_by_is_company(((Boolean)map.get("group_by_is_company"))? "true" : "false");
		}
		if(map.get("group_by_name") instanceof Boolean){
			this.setGroup_by_name(((Boolean)map.get("group_by_name"))? "true" : "false");
		}
		if(map.get("group_by_parent_id") instanceof Boolean){
			this.setGroup_by_parent_id(((Boolean)map.get("group_by_parent_id"))? "true" : "false");
		}
		if(map.get("group_by_vat") instanceof Boolean){
			this.setGroup_by_vat(((Boolean)map.get("group_by_vat"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("line_ids") instanceof Boolean)&& map.get("line_ids")!=null){
			Object[] objs = (Object[])map.get("line_ids");
			if(objs.length > 0){
				Integer[] line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setLine_ids(Arrays.toString(line_ids).replace(" ",""));
			}
		}
		if(!(map.get("maximum_group") instanceof Boolean)&& map.get("maximum_group")!=null){
			this.setMaximum_group((Integer)map.get("maximum_group"));
		}
		if(!(map.get("number_group") instanceof Boolean)&& map.get("number_group")!=null){
			this.setNumber_group((Integer)map.get("number_group"));
		}
		if(!(map.get("partner_ids") instanceof Boolean)&& map.get("partner_ids")!=null){
			Object[] objs = (Object[])map.get("partner_ids");
			if(objs.length > 0){
				Integer[] partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPartner_ids(Arrays.toString(partner_ids).replace(" ",""));
			}
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrent_line_id()!=null&&this.getCurrent_line_idDirtyFlag()){
			map.put("current_line_id",this.getCurrent_line_id());
		}else if(this.getCurrent_line_idDirtyFlag()){
			map.put("current_line_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getDst_partner_id()!=null&&this.getDst_partner_idDirtyFlag()){
			map.put("dst_partner_id",this.getDst_partner_id());
		}else if(this.getDst_partner_idDirtyFlag()){
			map.put("dst_partner_id",false);
		}
		if(this.getDst_partner_id_text()!=null&&this.getDst_partner_id_textDirtyFlag()){
			//忽略文本外键dst_partner_id_text
		}else if(this.getDst_partner_id_textDirtyFlag()){
			map.put("dst_partner_id",false);
		}
		if(this.getExclude_contact()!=null&&this.getExclude_contactDirtyFlag()){
			map.put("exclude_contact",Boolean.parseBoolean(this.getExclude_contact()));		
		}		if(this.getExclude_journal_item()!=null&&this.getExclude_journal_itemDirtyFlag()){
			map.put("exclude_journal_item",Boolean.parseBoolean(this.getExclude_journal_item()));		
		}		if(this.getGroup_by_email()!=null&&this.getGroup_by_emailDirtyFlag()){
			map.put("group_by_email",Boolean.parseBoolean(this.getGroup_by_email()));		
		}		if(this.getGroup_by_is_company()!=null&&this.getGroup_by_is_companyDirtyFlag()){
			map.put("group_by_is_company",Boolean.parseBoolean(this.getGroup_by_is_company()));		
		}		if(this.getGroup_by_name()!=null&&this.getGroup_by_nameDirtyFlag()){
			map.put("group_by_name",Boolean.parseBoolean(this.getGroup_by_name()));		
		}		if(this.getGroup_by_parent_id()!=null&&this.getGroup_by_parent_idDirtyFlag()){
			map.put("group_by_parent_id",Boolean.parseBoolean(this.getGroup_by_parent_id()));		
		}		if(this.getGroup_by_vat()!=null&&this.getGroup_by_vatDirtyFlag()){
			map.put("group_by_vat",Boolean.parseBoolean(this.getGroup_by_vat()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLine_ids()!=null&&this.getLine_idsDirtyFlag()){
			map.put("line_ids",this.getLine_ids());
		}else if(this.getLine_idsDirtyFlag()){
			map.put("line_ids",false);
		}
		if(this.getMaximum_group()!=null&&this.getMaximum_groupDirtyFlag()){
			map.put("maximum_group",this.getMaximum_group());
		}else if(this.getMaximum_groupDirtyFlag()){
			map.put("maximum_group",false);
		}
		if(this.getNumber_group()!=null&&this.getNumber_groupDirtyFlag()){
			map.put("number_group",this.getNumber_group());
		}else if(this.getNumber_groupDirtyFlag()){
			map.put("number_group",false);
		}
		if(this.getPartner_ids()!=null&&this.getPartner_idsDirtyFlag()){
			map.put("partner_ids",this.getPartner_ids());
		}else if(this.getPartner_idsDirtyFlag()){
			map.put("partner_ids",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
