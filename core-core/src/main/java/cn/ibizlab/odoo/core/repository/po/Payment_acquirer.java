package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirerSearchContext;

/**
 * 实体 [付款收单方] 存储模型
 */
public interface Payment_acquirer{

    /**
     * 支持费用计算
     */
    String getFees_implemented();

    void setFees_implemented(String fees_implemented);

    /**
     * 获取 [支持费用计算]脏标记
     */
    boolean getFees_implementedDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 图像
     */
    byte[] getImage();

    void setImage(byte[] image);

    /**
     * 获取 [图像]脏标记
     */
    boolean getImageDirtyFlag();

    /**
     * 完成的信息
     */
    String getDone_msg();

    void setDone_msg(String done_msg);

    /**
     * 获取 [完成的信息]脏标记
     */
    boolean getDone_msgDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 支持的支付图标
     */
    String getPayment_icon_ids();

    void setPayment_icon_ids(String payment_icon_ids);

    /**
     * 获取 [支持的支付图标]脏标记
     */
    boolean getPayment_icon_idsDirtyFlag();

    /**
     * 在门户/网站可见
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [在门户/网站可见]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 可变的交易费用（百分比）
     */
    Double getFees_int_var();

    void setFees_int_var(Double fees_int_var);

    /**
     * 获取 [可变的交易费用（百分比）]脏标记
     */
    boolean getFees_int_varDirtyFlag();

    /**
     * 添加额外的费用
     */
    String getFees_active();

    void setFees_active(String fees_active);

    /**
     * 获取 [添加额外的费用]脏标记
     */
    boolean getFees_activeDirtyFlag();

    /**
     * S2S表单模板
     */
    Integer getRegistration_view_template_id();

    void setRegistration_view_template_id(Integer registration_view_template_id);

    /**
     * 获取 [S2S表单模板]脏标记
     */
    boolean getRegistration_view_template_idDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 感谢留言
     */
    String getPost_msg();

    void setPost_msg(String post_msg);

    /**
     * 获取 [感谢留言]脏标记
     */
    boolean getPost_msgDirtyFlag();

    /**
     * 环境
     */
    String getEnvironment();

    void setEnvironment(String environment);

    /**
     * 获取 [环境]脏标记
     */
    boolean getEnvironmentDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 小尺寸图像
     */
    byte[] getImage_small();

    void setImage_small(byte[] image_small);

    /**
     * 获取 [小尺寸图像]脏标记
     */
    boolean getImage_smallDirtyFlag();

    /**
     * 支持保存卡片资料
     */
    String getToken_implemented();

    void setToken_implemented(String token_implemented);

    /**
     * 获取 [支持保存卡片资料]脏标记
     */
    boolean getToken_implementedDirtyFlag();

    /**
     * 国内固定费用
     */
    Double getFees_dom_fixed();

    void setFees_dom_fixed(Double fees_dom_fixed);

    /**
     * 获取 [国内固定费用]脏标记
     */
    boolean getFees_dom_fixedDirtyFlag();

    /**
     * 固定的手续费
     */
    Double getFees_int_fixed();

    void setFees_int_fixed(Double fees_int_fixed);

    /**
     * 获取 [固定的手续费]脏标记
     */
    boolean getFees_int_fixedDirtyFlag();

    /**
     * 对应模块
     */
    Integer getModule_id();

    void setModule_id(Integer module_id);

    /**
     * 获取 [对应模块]脏标记
     */
    boolean getModule_idDirtyFlag();

    /**
     * 批准支持的机制
     */
    String getAuthorize_implemented();

    void setAuthorize_implemented(String authorize_implemented);

    /**
     * 获取 [批准支持的机制]脏标记
     */
    boolean getAuthorize_implementedDirtyFlag();

    /**
     * 立即支付
     */
    String getPayment_flow();

    void setPayment_flow(String payment_flow);

    /**
     * 获取 [立即支付]脏标记
     */
    boolean getPayment_flowDirtyFlag();

    /**
     * 特定国家/地区
     */
    String getSpecific_countries();

    void setSpecific_countries(String specific_countries);

    /**
     * 获取 [特定国家/地区]脏标记
     */
    boolean getSpecific_countriesDirtyFlag();

    /**
     * 动态内部费用(百分比)
     */
    Double getFees_dom_var();

    void setFees_dom_var(Double fees_dom_var);

    /**
     * 获取 [动态内部费用(百分比)]脏标记
     */
    boolean getFees_dom_varDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 帮助信息
     */
    String getPre_msg();

    void setPre_msg(String pre_msg);

    /**
     * 获取 [帮助信息]脏标记
     */
    boolean getPre_msgDirtyFlag();

    /**
     * 国家
     */
    String getCountry_ids();

    void setCountry_ids(String country_ids);

    /**
     * 获取 [国家]脏标记
     */
    boolean getCountry_idsDirtyFlag();

    /**
     * 使用SEPA QR 二维码
     */
    String getQr_code();

    void setQr_code(String qr_code);

    /**
     * 获取 [使用SEPA QR 二维码]脏标记
     */
    boolean getQr_codeDirtyFlag();

    /**
     * 待定消息
     */
    String getPending_msg();

    void setPending_msg(String pending_msg);

    /**
     * 获取 [待定消息]脏标记
     */
    boolean getPending_msgDirtyFlag();

    /**
     * 中等尺寸图像
     */
    byte[] getImage_medium();

    void setImage_medium(byte[] image_medium);

    /**
     * 获取 [中等尺寸图像]脏标记
     */
    boolean getImage_mediumDirtyFlag();

    /**
     * 窗体按钮模板
     */
    Integer getView_template_id();

    void setView_template_id(Integer view_template_id);

    /**
     * 获取 [窗体按钮模板]脏标记
     */
    boolean getView_template_idDirtyFlag();

    /**
     * 未收款
     */
    String getInbound_payment_method_ids();

    void setInbound_payment_method_ids(String inbound_payment_method_ids);

    /**
     * 获取 [未收款]脏标记
     */
    boolean getInbound_payment_method_idsDirtyFlag();

    /**
     * 交流
     */
    String getSo_reference_type();

    void setSo_reference_type(String so_reference_type);

    /**
     * 获取 [交流]脏标记
     */
    boolean getSo_reference_typeDirtyFlag();

    /**
     * 安装状态
     */
    String getModule_state();

    void setModule_state(String module_state);

    /**
     * 获取 [安装状态]脏标记
     */
    boolean getModule_stateDirtyFlag();

    /**
     * 取消消息
     */
    String getCancel_msg();

    void setCancel_msg(String cancel_msg);

    /**
     * 获取 [取消消息]脏标记
     */
    boolean getCancel_msgDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 服务商
     */
    String getProvider();

    void setProvider(String provider);

    /**
     * 获取 [服务商]脏标记
     */
    boolean getProviderDirtyFlag();

    /**
     * 保存卡
     */
    String getSave_token();

    void setSave_token(String save_token);

    /**
     * 获取 [保存卡]脏标记
     */
    boolean getSave_tokenDirtyFlag();

    /**
     * 手动获取金额
     */
    String getCapture_manually();

    void setCapture_manually(String capture_manually);

    /**
     * 获取 [手动获取金额]脏标记
     */
    boolean getCapture_manuallyDirtyFlag();

    /**
     * 错误消息
     */
    String getError_msg();

    void setError_msg(String error_msg);

    /**
     * 获取 [错误消息]脏标记
     */
    boolean getError_msgDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 付款日记账
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [付款日记账]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 付款日记账
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [付款日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
