package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_locationSearchContext;

/**
 * 实体 [库存位置] 存储模型
 */
public interface Stock_location{

    /**
     * 通道(X)
     */
    Integer getPosx();

    void setPosx(Integer posx);

    /**
     * 获取 [通道(X)]脏标记
     */
    boolean getPosxDirtyFlag();

    /**
     * 货架(Y)
     */
    Integer getPosy();

    void setPosy(Integer posy);

    /**
     * 获取 [货架(Y)]脏标记
     */
    boolean getPosyDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 条码
     */
    String getBarcode();

    void setBarcode(String barcode);

    /**
     * 获取 [条码]脏标记
     */
    boolean getBarcodeDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 高度(Z)
     */
    Integer getPosz();

    void setPosz(Integer posz);

    /**
     * 获取 [高度(Z)]脏标记
     */
    boolean getPoszDirtyFlag();

    /**
     * 完整的位置名称
     */
    String getComplete_name();

    void setComplete_name(String complete_name);

    /**
     * 获取 [完整的位置名称]脏标记
     */
    boolean getComplete_nameDirtyFlag();

    /**
     * 是一个报废位置？
     */
    String getScrap_location();

    void setScrap_location(String scrap_location);

    /**
     * 获取 [是一个报废位置？]脏标记
     */
    boolean getScrap_locationDirtyFlag();

    /**
     * 是一个退回位置？
     */
    String getReturn_location();

    void setReturn_location(String return_location);

    /**
     * 获取 [是一个退回位置？]脏标记
     */
    boolean getReturn_locationDirtyFlag();

    /**
     * 位置名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [位置名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 父级路径
     */
    String getParent_path();

    void setParent_path(String parent_path);

    /**
     * 获取 [父级路径]脏标记
     */
    boolean getParent_pathDirtyFlag();

    /**
     * 即时库存
     */
    String getQuant_ids();

    void setQuant_ids(String quant_ids);

    /**
     * 获取 [即时库存]脏标记
     */
    boolean getQuant_idsDirtyFlag();

    /**
     * 位置类型
     */
    String getUsage();

    void setUsage(String usage);

    /**
     * 获取 [位置类型]脏标记
     */
    boolean getUsageDirtyFlag();

    /**
     * 额外的信息
     */
    String getComment();

    void setComment(String comment);

    /**
     * 获取 [额外的信息]脏标记
     */
    boolean getCommentDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 包含
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [包含]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 上级位置
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [上级位置]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 库存计价科目（出向）
     */
    String getValuation_out_account_id_text();

    void setValuation_out_account_id_text(String valuation_out_account_id_text);

    /**
     * 获取 [库存计价科目（出向）]脏标记
     */
    boolean getValuation_out_account_id_textDirtyFlag();

    /**
     * 所有者
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 下架策略
     */
    String getRemoval_strategy_id_text();

    void setRemoval_strategy_id_text(String removal_strategy_id_text);

    /**
     * 获取 [下架策略]脏标记
     */
    boolean getRemoval_strategy_id_textDirtyFlag();

    /**
     * 上架策略
     */
    String getPutaway_strategy_id_text();

    void setPutaway_strategy_id_text(String putaway_strategy_id_text);

    /**
     * 获取 [上架策略]脏标记
     */
    boolean getPutaway_strategy_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 库存计价科目（入向）
     */
    String getValuation_in_account_id_text();

    void setValuation_in_account_id_text(String valuation_in_account_id_text);

    /**
     * 获取 [库存计价科目（入向）]脏标记
     */
    boolean getValuation_in_account_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 库存计价科目（入向）
     */
    Integer getValuation_in_account_id();

    void setValuation_in_account_id(Integer valuation_in_account_id);

    /**
     * 获取 [库存计价科目（入向）]脏标记
     */
    boolean getValuation_in_account_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 上级位置
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [上级位置]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 库存计价科目（出向）
     */
    Integer getValuation_out_account_id();

    void setValuation_out_account_id(Integer valuation_out_account_id);

    /**
     * 获取 [库存计价科目（出向）]脏标记
     */
    boolean getValuation_out_account_idDirtyFlag();

    /**
     * 所有者
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 上架策略
     */
    Integer getPutaway_strategy_id();

    void setPutaway_strategy_id(Integer putaway_strategy_id);

    /**
     * 获取 [上架策略]脏标记
     */
    boolean getPutaway_strategy_idDirtyFlag();

    /**
     * 下架策略
     */
    Integer getRemoval_strategy_id();

    void setRemoval_strategy_id(Integer removal_strategy_id);

    /**
     * 获取 [下架策略]脏标记
     */
    boolean getRemoval_strategy_idDirtyFlag();

}
