package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Event_type;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_typeSearchContext;

/**
 * 实体 [活动类别] 存储对象
 */
public interface Event_typeRepository extends Repository<Event_type> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Event_type> searchDefault(Event_typeSearchContext context);

    Event_type convert2PO(cn.ibizlab.odoo.core.odoo_event.domain.Event_type domain , Event_type po) ;

    cn.ibizlab.odoo.core.odoo_event.domain.Event_type convert2Domain( Event_type po ,cn.ibizlab.odoo.core.odoo_event.domain.Event_type domain) ;

}
