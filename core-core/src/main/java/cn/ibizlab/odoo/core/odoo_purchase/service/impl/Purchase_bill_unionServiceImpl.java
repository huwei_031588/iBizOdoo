package cn.ibizlab.odoo.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_bill_unionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_purchase.client.purchase_bill_unionOdooClient;
import cn.ibizlab.odoo.core.odoo_purchase.clientmodel.purchase_bill_unionClientModel;

/**
 * 实体[采购 & 账单] 服务对象接口实现
 */
@Slf4j
@Service
public class Purchase_bill_unionServiceImpl implements IPurchase_bill_unionService {

    @Autowired
    purchase_bill_unionOdooClient purchase_bill_unionOdooClient;


    @Override
    public boolean create(Purchase_bill_union et) {
        purchase_bill_unionClientModel clientModel = convert2Model(et,null);
		purchase_bill_unionOdooClient.create(clientModel);
        Purchase_bill_union rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Purchase_bill_union> list){
    }

    @Override
    public Purchase_bill_union get(Integer id) {
        purchase_bill_unionClientModel clientModel = new purchase_bill_unionClientModel();
        clientModel.setId(id);
		purchase_bill_unionOdooClient.get(clientModel);
        Purchase_bill_union et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Purchase_bill_union();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Purchase_bill_union et) {
        purchase_bill_unionClientModel clientModel = convert2Model(et,null);
		purchase_bill_unionOdooClient.update(clientModel);
        Purchase_bill_union rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Purchase_bill_union> list){
    }

    @Override
    public boolean remove(Integer id) {
        purchase_bill_unionClientModel clientModel = new purchase_bill_unionClientModel();
        clientModel.setId(id);
		purchase_bill_unionOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Purchase_bill_union> searchDefault(Purchase_bill_unionSearchContext context) {
        List<Purchase_bill_union> list = new ArrayList<Purchase_bill_union>();
        Page<purchase_bill_unionClientModel> clientModelList = purchase_bill_unionOdooClient.search(context);
        for(purchase_bill_unionClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Purchase_bill_union>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public purchase_bill_unionClientModel convert2Model(Purchase_bill_union domain , purchase_bill_unionClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new purchase_bill_unionClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("referencedirtyflag"))
                model.setReference(domain.getReference());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("purchase_order_id_textdirtyflag"))
                model.setPurchase_order_id_text(domain.getPurchaseOrderIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("vendor_bill_id_textdirtyflag"))
                model.setVendor_bill_id_text(domain.getVendorBillIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("vendor_bill_iddirtyflag"))
                model.setVendor_bill_id(domain.getVendorBillId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("purchase_order_iddirtyflag"))
                model.setPurchase_order_id(domain.getPurchaseOrderId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Purchase_bill_union convert2Domain( purchase_bill_unionClientModel model ,Purchase_bill_union domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Purchase_bill_union();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getReferenceDirtyFlag())
            domain.setReference(model.getReference());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getPurchase_order_id_textDirtyFlag())
            domain.setPurchaseOrderIdText(model.getPurchase_order_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getVendor_bill_id_textDirtyFlag())
            domain.setVendorBillIdText(model.getVendor_bill_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getVendor_bill_idDirtyFlag())
            domain.setVendorBillId(model.getVendor_bill_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getPurchase_order_idDirtyFlag())
            domain.setPurchaseOrderId(model.getPurchase_order_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        return domain ;
    }

}

    



