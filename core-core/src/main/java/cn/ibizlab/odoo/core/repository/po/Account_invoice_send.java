package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_sendSearchContext;

/**
 * 实体 [发送会计发票] 存储模型
 */
public interface Account_invoice_send{

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 相关评级
     */
    String getRating_ids();

    void setRating_ids(String rating_ids);

    /**
     * 获取 [相关评级]脏标记
     */
    boolean getRating_idsDirtyFlag();

    /**
     * 追踪值
     */
    String getTracking_value_ids();

    void setTracking_value_ids(String tracking_value_ids);

    /**
     * 获取 [追踪值]脏标记
     */
    boolean getTracking_value_idsDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 邮件列表
     */
    String getMailing_list_ids();

    void setMailing_list_ids(String mailing_list_ids);

    /**
     * 获取 [邮件列表]脏标记
     */
    boolean getMailing_list_idsDirtyFlag();

    /**
     * 已打印
     */
    String getPrinted();

    void setPrinted(String printed);

    /**
     * 获取 [已打印]脏标记
     */
    boolean getPrintedDirtyFlag();

    /**
     * 附件
     */
    String getAttachment_ids();

    void setAttachment_ids(String attachment_ids);

    /**
     * 获取 [附件]脏标记
     */
    boolean getAttachment_idsDirtyFlag();

    /**
     * 不发送的发票
     */
    String getInvoice_without_email();

    void setInvoice_without_email(String invoice_without_email);

    /**
     * 获取 [不发送的发票]脏标记
     */
    boolean getInvoice_without_emailDirtyFlag();

    /**
     * 待处理的业务伙伴
     */
    String getNeedaction_partner_ids();

    void setNeedaction_partner_ids(String needaction_partner_ids);

    /**
     * 获取 [待处理的业务伙伴]脏标记
     */
    boolean getNeedaction_partner_idsDirtyFlag();

    /**
     * 透过邮递
     */
    String getSnailmail_is_letter();

    void setSnailmail_is_letter(String snailmail_is_letter);

    /**
     * 获取 [透过邮递]脏标记
     */
    boolean getSnailmail_is_letterDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 打印
     */
    String getIs_print();

    void setIs_print(String is_print);

    /**
     * 获取 [打印]脏标记
     */
    boolean getIs_printDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 邮戳(s)
     */
    Double getSnailmail_cost();

    void setSnailmail_cost(Double snailmail_cost);

    /**
     * 获取 [邮戳(s)]脏标记
     */
    boolean getSnailmail_costDirtyFlag();

    /**
     * 频道
     */
    String getChannel_ids();

    void setChannel_ids(String channel_ids);

    /**
     * 获取 [频道]脏标记
     */
    boolean getChannel_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 收藏夹
     */
    String getStarred_partner_ids();

    void setStarred_partner_ids(String starred_partner_ids);

    /**
     * 获取 [收藏夹]脏标记
     */
    boolean getStarred_partner_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 额外的联系人
     */
    String getPartner_ids();

    void setPartner_ids(String partner_ids);

    /**
     * 获取 [额外的联系人]脏标记
     */
    boolean getPartner_idsDirtyFlag();

    /**
     * 下级信息
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [下级信息]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 信
     */
    String getLetter_ids();

    void setLetter_ids(String letter_ids);

    /**
     * 获取 [信]脏标记
     */
    boolean getLetter_idsDirtyFlag();

    /**
     * 通知
     */
    String getNotification_ids();

    void setNotification_ids(String notification_ids);

    /**
     * 获取 [通知]脏标记
     */
    boolean getNotification_idsDirtyFlag();

    /**
     * 发票
     */
    String getInvoice_ids();

    void setInvoice_ids(String invoice_ids);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_idsDirtyFlag();

    /**
     * EMail
     */
    String getIs_email();

    void setIs_email(String is_email);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getIs_emailDirtyFlag();

    /**
     * 上级消息
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级消息]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 使用有效域
     */
    String getUse_active_domain();

    void setUse_active_domain(String use_active_domain);

    /**
     * 获取 [使用有效域]脏标记
     */
    boolean getUse_active_domainDirtyFlag();

    /**
     * 审核状态
     */
    String getModeration_status();

    void setModeration_status(String moderation_status);

    /**
     * 获取 [审核状态]脏标记
     */
    boolean getModeration_statusDirtyFlag();

    /**
     * 审核人
     */
    Integer getModerator_id();

    void setModerator_id(Integer moderator_id);

    /**
     * 获取 [审核人]脏标记
     */
    boolean getModerator_idDirtyFlag();

    /**
     * 有误差
     */
    String getHas_error();

    void setHas_error(String has_error);

    /**
     * 获取 [有误差]脏标记
     */
    boolean getHas_errorDirtyFlag();

    /**
     * 有效域
     */
    String getActive_domain();

    void setActive_domain(String active_domain);

    /**
     * 获取 [有效域]脏标记
     */
    boolean getActive_domainDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 线程无应答
     */
    String getNo_auto_thread();

    void setNo_auto_thread(String no_auto_thread);

    /**
     * 获取 [线程无应答]脏标记
     */
    boolean getNo_auto_threadDirtyFlag();

    /**
     * 写作模式
     */
    String getComposition_mode();

    void setComposition_mode(String composition_mode);

    /**
     * 获取 [写作模式]脏标记
     */
    boolean getComposition_modeDirtyFlag();

    /**
     * 添加签名
     */
    String getAdd_sign();

    void setAdd_sign(String add_sign);

    /**
     * 获取 [添加签名]脏标记
     */
    boolean getAdd_signDirtyFlag();

    /**
     * 来自
     */
    String getEmail_from();

    void setEmail_from(String email_from);

    /**
     * 获取 [来自]脏标记
     */
    boolean getEmail_fromDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 邮件活动类型
     */
    Integer getMail_activity_type_id();

    void setMail_activity_type_id(Integer mail_activity_type_id);

    /**
     * 获取 [邮件活动类型]脏标记
     */
    boolean getMail_activity_type_idDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 待处理
     */
    String getNeedaction();

    void setNeedaction(String needaction);

    /**
     * 获取 [待处理]脏标记
     */
    boolean getNeedactionDirtyFlag();

    /**
     * 主题
     */
    String getSubject();

    void setSubject(String subject);

    /**
     * 获取 [主题]脏标记
     */
    boolean getSubjectDirtyFlag();

    /**
     * 消息记录名称
     */
    String getRecord_name();

    void setRecord_name(String record_name);

    /**
     * 获取 [消息记录名称]脏标记
     */
    boolean getRecord_nameDirtyFlag();

    /**
     * 作者头像
     */
    byte[] getAuthor_avatar();

    void setAuthor_avatar(byte[] author_avatar);

    /**
     * 获取 [作者头像]脏标记
     */
    boolean getAuthor_avatarDirtyFlag();

    /**
     * 评级值
     */
    Double getRating_value();

    void setRating_value(Double rating_value);

    /**
     * 获取 [评级值]脏标记
     */
    boolean getRating_valueDirtyFlag();

    /**
     * 删除邮件
     */
    String getAuto_delete();

    void setAuto_delete(String auto_delete);

    /**
     * 获取 [删除邮件]脏标记
     */
    boolean getAuto_deleteDirtyFlag();

    /**
     * 删除消息副本
     */
    String getAuto_delete_message();

    void setAuto_delete_message(String auto_delete_message);

    /**
     * 获取 [删除消息副本]脏标记
     */
    boolean getAuto_delete_messageDirtyFlag();

    /**
     * 内容
     */
    String getBody();

    void setBody(String body);

    /**
     * 获取 [内容]脏标记
     */
    boolean getBodyDirtyFlag();

    /**
     * 邮件发送服务器
     */
    Integer getMail_server_id();

    void setMail_server_id(Integer mail_server_id);

    /**
     * 获取 [邮件发送服务器]脏标记
     */
    boolean getMail_server_idDirtyFlag();

    /**
     * Message-Id
     */
    String getMessage_id();

    void setMessage_id(String message_id);

    /**
     * 获取 [Message-Id]脏标记
     */
    boolean getMessage_idDirtyFlag();

    /**
     * 相关的文档模型
     */
    String getModel();

    void setModel(String model);

    /**
     * 获取 [相关的文档模型]脏标记
     */
    boolean getModelDirtyFlag();

    /**
     * 回复 至
     */
    String getReply_to();

    void setReply_to(String reply_to);

    /**
     * 获取 [回复 至]脏标记
     */
    boolean getReply_toDirtyFlag();

    /**
     * 类型
     */
    String getMessage_type();

    void setMessage_type(String message_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getMessage_typeDirtyFlag();

    /**
     * 标星号邮件
     */
    String getStarred();

    void setStarred(String starred);

    /**
     * 获取 [标星号邮件]脏标记
     */
    boolean getStarredDirtyFlag();

    /**
     * 记录内部备注
     */
    String getIs_log();

    void setIs_log(String is_log);

    /**
     * 获取 [记录内部备注]脏标记
     */
    boolean getIs_logDirtyFlag();

    /**
     * 通知关注者
     */
    String getNotify();

    void setNotify(String notify);

    /**
     * 获取 [通知关注者]脏标记
     */
    boolean getNotifyDirtyFlag();

    /**
     * 群发邮件营销
     */
    Integer getMass_mailing_campaign_id();

    void setMass_mailing_campaign_id(Integer mass_mailing_campaign_id);

    /**
     * 获取 [群发邮件营销]脏标记
     */
    boolean getMass_mailing_campaign_idDirtyFlag();

    /**
     * 需要审核
     */
    String getNeed_moderation();

    void setNeed_moderation(String need_moderation);

    /**
     * 获取 [需要审核]脏标记
     */
    boolean getNeed_moderationDirtyFlag();

    /**
     * 子类型
     */
    Integer getSubtype_id();

    void setSubtype_id(Integer subtype_id);

    /**
     * 获取 [子类型]脏标记
     */
    boolean getSubtype_idDirtyFlag();

    /**
     * 作者
     */
    Integer getAuthor_id();

    void setAuthor_id(Integer author_id);

    /**
     * 获取 [作者]脏标记
     */
    boolean getAuthor_idDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 已发布
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 群发邮件
     */
    Integer getMass_mailing_id();

    void setMass_mailing_id(Integer mass_mailing_id);

    /**
     * 获取 [群发邮件]脏标记
     */
    boolean getMass_mailing_idDirtyFlag();

    /**
     * 相关文档编号
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [相关文档编号]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 使用模版
     */
    String getTemplate_id_text();

    void setTemplate_id_text(String template_id_text);

    /**
     * 获取 [使用模版]脏标记
     */
    boolean getTemplate_id_textDirtyFlag();

    /**
     * 群发邮件标题
     */
    String getMass_mailing_name();

    void setMass_mailing_name(String mass_mailing_name);

    /**
     * 获取 [群发邮件标题]脏标记
     */
    boolean getMass_mailing_nameDirtyFlag();

    /**
     * 布局
     */
    String getLayout();

    void setLayout(String layout);

    /**
     * 获取 [布局]脏标记
     */
    boolean getLayoutDirtyFlag();

    /**
     * 邮件撰写者
     */
    Integer getComposer_id();

    void setComposer_id(Integer composer_id);

    /**
     * 获取 [邮件撰写者]脏标记
     */
    boolean getComposer_idDirtyFlag();

    /**
     * 使用模版
     */
    Integer getTemplate_id();

    void setTemplate_id(Integer template_id);

    /**
     * 获取 [使用模版]脏标记
     */
    boolean getTemplate_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
