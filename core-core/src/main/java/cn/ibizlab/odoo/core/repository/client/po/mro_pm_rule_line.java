package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mro_pm_rule_line] 对象
 */
public interface mro_pm_rule_line {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public Integer getMeter_interval_id();

    public void setMeter_interval_id(Integer meter_interval_id);

    public String getMeter_interval_id_text();

    public void setMeter_interval_id_text(String meter_interval_id_text);

    public Integer getPm_rule_id();

    public void setPm_rule_id(Integer pm_rule_id);

    public String getPm_rule_id_text();

    public void setPm_rule_id_text(String pm_rule_id_text);

    public Integer getTask_id();

    public void setTask_id(Integer task_id);

    public String getTask_id_text();

    public void setTask_id_text(String task_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
