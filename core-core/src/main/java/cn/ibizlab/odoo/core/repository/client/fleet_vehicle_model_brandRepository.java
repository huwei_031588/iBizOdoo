package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_vehicle_model_brand;

/**
 * 实体[fleet_vehicle_model_brand] 服务对象接口
 */
public interface fleet_vehicle_model_brandRepository{


    public fleet_vehicle_model_brand createPO() ;
        public void updateBatch(fleet_vehicle_model_brand fleet_vehicle_model_brand);

        public void createBatch(fleet_vehicle_model_brand fleet_vehicle_model_brand);

        public void removeBatch(String id);

        public void get(String id);

        public List<fleet_vehicle_model_brand> search();

        public void remove(String id);

        public void update(fleet_vehicle_model_brand fleet_vehicle_model_brand);

        public void create(fleet_vehicle_model_brand fleet_vehicle_model_brand);


}
