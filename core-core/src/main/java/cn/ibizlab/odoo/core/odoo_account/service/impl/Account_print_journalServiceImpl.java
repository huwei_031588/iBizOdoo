package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_print_journal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_print_journalSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_print_journalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_print_journalOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_print_journalClientModel;

/**
 * 实体[会计打印日记账] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_print_journalServiceImpl implements IAccount_print_journalService {

    @Autowired
    account_print_journalOdooClient account_print_journalOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_print_journalClientModel clientModel = new account_print_journalClientModel();
        clientModel.setId(id);
		account_print_journalOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_print_journal et) {
        account_print_journalClientModel clientModel = convert2Model(et,null);
		account_print_journalOdooClient.update(clientModel);
        Account_print_journal rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_print_journal> list){
    }

    @Override
    public Account_print_journal get(Integer id) {
        account_print_journalClientModel clientModel = new account_print_journalClientModel();
        clientModel.setId(id);
		account_print_journalOdooClient.get(clientModel);
        Account_print_journal et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_print_journal();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_print_journal et) {
        account_print_journalClientModel clientModel = convert2Model(et,null);
		account_print_journalOdooClient.create(clientModel);
        Account_print_journal rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_print_journal> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_print_journal> searchDefault(Account_print_journalSearchContext context) {
        List<Account_print_journal> list = new ArrayList<Account_print_journal>();
        Page<account_print_journalClientModel> clientModelList = account_print_journalOdooClient.search(context);
        for(account_print_journalClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_print_journal>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_print_journalClientModel convert2Model(Account_print_journal domain , account_print_journalClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_print_journalClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("date_todirtyflag"))
                model.setDate_to(domain.getDateTo());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("amount_currencydirtyflag"))
                model.setAmount_currency(domain.getAmountCurrency());
            if((Boolean) domain.getExtensionparams().get("sort_selectiondirtyflag"))
                model.setSort_selection(domain.getSortSelection());
            if((Boolean) domain.getExtensionparams().get("date_fromdirtyflag"))
                model.setDate_from(domain.getDateFrom());
            if((Boolean) domain.getExtensionparams().get("journal_idsdirtyflag"))
                model.setJournal_ids(domain.getJournalIds());
            if((Boolean) domain.getExtensionparams().get("target_movedirtyflag"))
                model.setTarget_move(domain.getTargetMove());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_print_journal convert2Domain( account_print_journalClientModel model ,Account_print_journal domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_print_journal();
        }

        if(model.getDate_toDirtyFlag())
            domain.setDateTo(model.getDate_to());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getAmount_currencyDirtyFlag())
            domain.setAmountCurrency(model.getAmount_currency());
        if(model.getSort_selectionDirtyFlag())
            domain.setSortSelection(model.getSort_selection());
        if(model.getDate_fromDirtyFlag())
            domain.setDateFrom(model.getDate_from());
        if(model.getJournal_idsDirtyFlag())
            domain.setJournalIds(model.getJournal_ids());
        if(model.getTarget_moveDirtyFlag())
            domain.setTargetMove(model.getTarget_move());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



