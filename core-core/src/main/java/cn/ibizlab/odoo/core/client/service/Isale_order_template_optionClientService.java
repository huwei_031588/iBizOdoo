package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isale_order_template_option;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_order_template_option] 服务对象接口
 */
public interface Isale_order_template_optionClientService{

    public Isale_order_template_option createModel() ;

    public void removeBatch(List<Isale_order_template_option> sale_order_template_options);

    public void updateBatch(List<Isale_order_template_option> sale_order_template_options);

    public void createBatch(List<Isale_order_template_option> sale_order_template_options);

    public void remove(Isale_order_template_option sale_order_template_option);

    public void update(Isale_order_template_option sale_order_template_option);

    public Page<Isale_order_template_option> search(SearchContext context);

    public void create(Isale_order_template_option sale_order_template_option);

    public void get(Isale_order_template_option sale_order_template_option);

    public Page<Isale_order_template_option> select(SearchContext context);

}
