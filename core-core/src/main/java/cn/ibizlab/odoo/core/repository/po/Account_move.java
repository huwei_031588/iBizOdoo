package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_moveSearchContext;

/**
 * 实体 [凭证录入] 存储模型
 */
public interface Account_move{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 内部备注
     */
    String getNarration();

    void setNarration(String narration);

    /**
     * 获取 [内部备注]脏标记
     */
    boolean getNarrationDirtyFlag();

    /**
     * 匹配百分比
     */
    Double getMatched_percentage();

    void setMatched_percentage(Double matched_percentage);

    /**
     * 获取 [匹配百分比]脏标记
     */
    boolean getMatched_percentageDirtyFlag();

    /**
     * 税类别域
     */
    String getTax_type_domain();

    void setTax_type_domain(String tax_type_domain);

    /**
     * 获取 [税类别域]脏标记
     */
    boolean getTax_type_domainDirtyFlag();

    /**
     * 号码
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [号码]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 参考
     */
    String getRef();

    void setRef(String ref);

    /**
     * 获取 [参考]脏标记
     */
    boolean getRefDirtyFlag();

    /**
     * 科目
     */
    Integer getDummy_account_id();

    void setDummy_account_id(Integer dummy_account_id);

    /**
     * 获取 [科目]脏标记
     */
    boolean getDummy_account_idDirtyFlag();

    /**
     * 自动撤销
     */
    String getAuto_reverse();

    void setAuto_reverse(String auto_reverse);

    /**
     * 获取 [自动撤销]脏标记
     */
    boolean getAuto_reverseDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 撤销日期
     */
    Timestamp getReverse_date();

    void setReverse_date(Timestamp reverse_date);

    /**
     * 获取 [撤销日期]脏标记
     */
    boolean getReverse_dateDirtyFlag();

    /**
     * 日记账项目
     */
    String getLine_ids();

    void setLine_ids(String line_ids);

    /**
     * 获取 [日记账项目]脏标记
     */
    boolean getLine_idsDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 撤销分录
     */
    String getReverse_entry_id_text();

    void setReverse_entry_id_text(String reverse_entry_id_text);

    /**
     * 获取 [撤销分录]脏标记
     */
    boolean getReverse_entry_id_textDirtyFlag();

    /**
     * 库存移动
     */
    String getStock_move_id_text();

    void setStock_move_id_text(String stock_move_id_text);

    /**
     * 获取 [库存移动]脏标记
     */
    boolean getStock_move_id_textDirtyFlag();

    /**
     * 日记账
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 库存移动
     */
    Integer getStock_move_id();

    void setStock_move_id(Integer stock_move_id);

    /**
     * 获取 [库存移动]脏标记
     */
    boolean getStock_move_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 日记账
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * 税率现金收付制分录
     */
    Integer getTax_cash_basis_rec_id();

    void setTax_cash_basis_rec_id(Integer tax_cash_basis_rec_id);

    /**
     * 获取 [税率现金收付制分录]脏标记
     */
    boolean getTax_cash_basis_rec_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 撤销分录
     */
    Integer getReverse_entry_id();

    void setReverse_entry_id(Integer reverse_entry_id);

    /**
     * 获取 [撤销分录]脏标记
     */
    boolean getReverse_entry_idDirtyFlag();

}
