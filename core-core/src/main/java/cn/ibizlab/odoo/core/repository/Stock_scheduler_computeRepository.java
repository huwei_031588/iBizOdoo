package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_scheduler_compute;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scheduler_computeSearchContext;

/**
 * 实体 [手动运行调度器] 存储对象
 */
public interface Stock_scheduler_computeRepository extends Repository<Stock_scheduler_compute> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_scheduler_compute> searchDefault(Stock_scheduler_computeSearchContext context);

    Stock_scheduler_compute convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scheduler_compute domain , Stock_scheduler_compute po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scheduler_compute convert2Domain( Stock_scheduler_compute po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scheduler_compute domain) ;

}
