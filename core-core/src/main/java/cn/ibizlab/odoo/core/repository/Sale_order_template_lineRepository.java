package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Sale_order_template_line;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_template_lineSearchContext;

/**
 * 实体 [报价单模板行] 存储对象
 */
public interface Sale_order_template_lineRepository extends Repository<Sale_order_template_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Sale_order_template_line> searchDefault(Sale_order_template_lineSearchContext context);

    Sale_order_template_line convert2PO(cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_line domain , Sale_order_template_line po) ;

    cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_line convert2Domain( Sale_order_template_line po ,cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_line domain) ;

}
