package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_messageSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_messageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_messageOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_messageClientModel;

/**
 * 实体[消息] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_messageServiceImpl implements IMail_messageService {

    @Autowired
    mail_messageOdooClient mail_messageOdooClient;


    @Override
    public boolean create(Mail_message et) {
        mail_messageClientModel clientModel = convert2Model(et,null);
		mail_messageOdooClient.create(clientModel);
        Mail_message rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_message> list){
    }

    @Override
    public Mail_message get(Integer id) {
        mail_messageClientModel clientModel = new mail_messageClientModel();
        clientModel.setId(id);
		mail_messageOdooClient.get(clientModel);
        Mail_message et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_message();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mail_messageClientModel clientModel = new mail_messageClientModel();
        clientModel.setId(id);
		mail_messageOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Mail_message et) {
        mail_messageClientModel clientModel = convert2Model(et,null);
		mail_messageOdooClient.update(clientModel);
        Mail_message rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_message> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_message> searchDefault(Mail_messageSearchContext context) {
        List<Mail_message> list = new ArrayList<Mail_message>();
        Page<mail_messageClientModel> clientModelList = mail_messageOdooClient.search(context);
        for(mail_messageClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_message>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_messageClientModel convert2Model(Mail_message domain , mail_messageClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_messageClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("needactiondirtyflag"))
                model.setNeedaction(domain.getNeedaction());
            if((Boolean) domain.getExtensionparams().get("layoutdirtyflag"))
                model.setLayout(domain.getLayout());
            if((Boolean) domain.getExtensionparams().get("modeldirtyflag"))
                model.setModel(domain.getModel());
            if((Boolean) domain.getExtensionparams().get("no_auto_threaddirtyflag"))
                model.setNo_auto_thread(domain.getNoAutoThread());
            if((Boolean) domain.getExtensionparams().get("partner_idsdirtyflag"))
                model.setPartner_ids(domain.getPartnerIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("record_namedirtyflag"))
                model.setRecord_name(domain.getRecordName());
            if((Boolean) domain.getExtensionparams().get("message_iddirtyflag"))
                model.setMessage_id(domain.getMessageId());
            if((Boolean) domain.getExtensionparams().get("tracking_value_idsdirtyflag"))
                model.setTracking_value_ids(domain.getTrackingValueIds());
            if((Boolean) domain.getExtensionparams().get("moderation_statusdirtyflag"))
                model.setModeration_status(domain.getModerationStatus());
            if((Boolean) domain.getExtensionparams().get("notification_idsdirtyflag"))
                model.setNotification_ids(domain.getNotificationIds());
            if((Boolean) domain.getExtensionparams().get("email_fromdirtyflag"))
                model.setEmail_from(domain.getEmailFrom());
            if((Boolean) domain.getExtensionparams().get("channel_idsdirtyflag"))
                model.setChannel_ids(domain.getChannelIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("starreddirtyflag"))
                model.setStarred(domain.getStarred());
            if((Boolean) domain.getExtensionparams().get("rating_idsdirtyflag"))
                model.setRating_ids(domain.getRatingIds());
            if((Boolean) domain.getExtensionparams().get("needaction_partner_idsdirtyflag"))
                model.setNeedaction_partner_ids(domain.getNeedactionPartnerIds());
            if((Boolean) domain.getExtensionparams().get("reply_todirtyflag"))
                model.setReply_to(domain.getReplyTo());
            if((Boolean) domain.getExtensionparams().get("mail_server_iddirtyflag"))
                model.setMail_server_id(domain.getMailServerId());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("need_moderationdirtyflag"))
                model.setNeed_moderation(domain.getNeedModeration());
            if((Boolean) domain.getExtensionparams().get("subjectdirtyflag"))
                model.setSubject(domain.getSubject());
            if((Boolean) domain.getExtensionparams().get("bodydirtyflag"))
                model.setBody(domain.getBody());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("attachment_idsdirtyflag"))
                model.setAttachment_ids(domain.getAttachmentIds());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("rating_valuedirtyflag"))
                model.setRating_value(domain.getRatingValue());
            if((Boolean) domain.getExtensionparams().get("add_signdirtyflag"))
                model.setAdd_sign(domain.getAddSign());
            if((Boolean) domain.getExtensionparams().get("starred_partner_idsdirtyflag"))
                model.setStarred_partner_ids(domain.getStarredPartnerIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("has_errordirtyflag"))
                model.setHas_error(domain.getHasError());
            if((Boolean) domain.getExtensionparams().get("message_typedirtyflag"))
                model.setMessage_type(domain.getMessageType());
            if((Boolean) domain.getExtensionparams().get("author_id_textdirtyflag"))
                model.setAuthor_id_text(domain.getAuthorIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("mail_activity_type_id_textdirtyflag"))
                model.setMail_activity_type_id_text(domain.getMailActivityTypeIdText());
            if((Boolean) domain.getExtensionparams().get("moderator_id_textdirtyflag"))
                model.setModerator_id_text(domain.getModeratorIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("subtype_id_textdirtyflag"))
                model.setSubtype_id_text(domain.getSubtypeIdText());
            if((Boolean) domain.getExtensionparams().get("author_avatardirtyflag"))
                model.setAuthor_avatar(domain.getAuthorAvatar());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("mail_activity_type_iddirtyflag"))
                model.setMail_activity_type_id(domain.getMailActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("moderator_iddirtyflag"))
                model.setModerator_id(domain.getModeratorId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("author_iddirtyflag"))
                model.setAuthor_id(domain.getAuthorId());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("subtype_iddirtyflag"))
                model.setSubtype_id(domain.getSubtypeId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_message convert2Domain( mail_messageClientModel model ,Mail_message domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_message();
        }

        if(model.getNeedactionDirtyFlag())
            domain.setNeedaction(model.getNeedaction());
        if(model.getLayoutDirtyFlag())
            domain.setLayout(model.getLayout());
        if(model.getModelDirtyFlag())
            domain.setModel(model.getModel());
        if(model.getNo_auto_threadDirtyFlag())
            domain.setNoAutoThread(model.getNo_auto_thread());
        if(model.getPartner_idsDirtyFlag())
            domain.setPartnerIds(model.getPartner_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getRecord_nameDirtyFlag())
            domain.setRecordName(model.getRecord_name());
        if(model.getMessage_idDirtyFlag())
            domain.setMessageId(model.getMessage_id());
        if(model.getTracking_value_idsDirtyFlag())
            domain.setTrackingValueIds(model.getTracking_value_ids());
        if(model.getModeration_statusDirtyFlag())
            domain.setModerationStatus(model.getModeration_status());
        if(model.getNotification_idsDirtyFlag())
            domain.setNotificationIds(model.getNotification_ids());
        if(model.getEmail_fromDirtyFlag())
            domain.setEmailFrom(model.getEmail_from());
        if(model.getChannel_idsDirtyFlag())
            domain.setChannelIds(model.getChannel_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getStarredDirtyFlag())
            domain.setStarred(model.getStarred());
        if(model.getRating_idsDirtyFlag())
            domain.setRatingIds(model.getRating_ids());
        if(model.getNeedaction_partner_idsDirtyFlag())
            domain.setNeedactionPartnerIds(model.getNeedaction_partner_ids());
        if(model.getReply_toDirtyFlag())
            domain.setReplyTo(model.getReply_to());
        if(model.getMail_server_idDirtyFlag())
            domain.setMailServerId(model.getMail_server_id());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getNeed_moderationDirtyFlag())
            domain.setNeedModeration(model.getNeed_moderation());
        if(model.getSubjectDirtyFlag())
            domain.setSubject(model.getSubject());
        if(model.getBodyDirtyFlag())
            domain.setBody(model.getBody());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getAttachment_idsDirtyFlag())
            domain.setAttachmentIds(model.getAttachment_ids());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getRating_valueDirtyFlag())
            domain.setRatingValue(model.getRating_value());
        if(model.getAdd_signDirtyFlag())
            domain.setAddSign(model.getAdd_sign());
        if(model.getStarred_partner_idsDirtyFlag())
            domain.setStarredPartnerIds(model.getStarred_partner_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getHas_errorDirtyFlag())
            domain.setHasError(model.getHas_error());
        if(model.getMessage_typeDirtyFlag())
            domain.setMessageType(model.getMessage_type());
        if(model.getAuthor_id_textDirtyFlag())
            domain.setAuthorIdText(model.getAuthor_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getMail_activity_type_id_textDirtyFlag())
            domain.setMailActivityTypeIdText(model.getMail_activity_type_id_text());
        if(model.getModerator_id_textDirtyFlag())
            domain.setModeratorIdText(model.getModerator_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getSubtype_id_textDirtyFlag())
            domain.setSubtypeIdText(model.getSubtype_id_text());
        if(model.getAuthor_avatarDirtyFlag())
            domain.setAuthorAvatar(model.getAuthor_avatar());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getMail_activity_type_idDirtyFlag())
            domain.setMailActivityTypeId(model.getMail_activity_type_id());
        if(model.getModerator_idDirtyFlag())
            domain.setModeratorId(model.getModerator_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getAuthor_idDirtyFlag())
            domain.setAuthorId(model.getAuthor_id());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getSubtype_idDirtyFlag())
            domain.setSubtypeId(model.getSubtype_id());
        return domain ;
    }

}

    



