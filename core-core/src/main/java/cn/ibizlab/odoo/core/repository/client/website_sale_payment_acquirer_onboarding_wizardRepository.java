package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.website_sale_payment_acquirer_onboarding_wizard;

/**
 * 实体[website_sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface website_sale_payment_acquirer_onboarding_wizardRepository{


    public website_sale_payment_acquirer_onboarding_wizard createPO() ;
        public void create(website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

        public void updateBatch(website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

        public void removeBatch(String id);

        public void get(String id);

        public void update(website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

        public void remove(String id);

        public List<website_sale_payment_acquirer_onboarding_wizard> search();

        public void createBatch(website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);


}
