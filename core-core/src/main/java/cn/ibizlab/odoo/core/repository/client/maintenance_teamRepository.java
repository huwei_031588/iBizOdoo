package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.maintenance_team;

/**
 * 实体[maintenance_team] 服务对象接口
 */
public interface maintenance_teamRepository{


    public maintenance_team createPO() ;
        public void get(String id);

        public void remove(String id);

        public void update(maintenance_team maintenance_team);

        public void createBatch(maintenance_team maintenance_team);

        public List<maintenance_team> search();

        public void updateBatch(maintenance_team maintenance_team);

        public void create(maintenance_team maintenance_team);

        public void removeBatch(String id);


}
