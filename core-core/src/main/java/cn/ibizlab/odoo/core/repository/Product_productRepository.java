package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_product;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_productSearchContext;

/**
 * 实体 [产品] 存储对象
 */
public interface Product_productRepository extends Repository<Product_product> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_product> searchDefault(Product_productSearchContext context);

    Product_product convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_product domain , Product_product po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_product convert2Domain( Product_product po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_product domain) ;

}
