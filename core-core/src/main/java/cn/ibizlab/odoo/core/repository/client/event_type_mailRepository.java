package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.event_type_mail;

/**
 * 实体[event_type_mail] 服务对象接口
 */
public interface event_type_mailRepository{


    public event_type_mail createPO() ;
        public void createBatch(event_type_mail event_type_mail);

        public void update(event_type_mail event_type_mail);

        public List<event_type_mail> search();

        public void remove(String id);

        public void updateBatch(event_type_mail event_type_mail);

        public void removeBatch(String id);

        public void get(String id);

        public void create(event_type_mail event_type_mail);


}
