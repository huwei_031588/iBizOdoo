package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [event_mail_registration] 对象
 */
public interface event_mail_registration {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getMail_sent();

    public void setMail_sent(String mail_sent);

    public Integer getRegistration_id();

    public void setRegistration_id(Integer registration_id);

    public String getRegistration_id_text();

    public void setRegistration_id_text(String registration_id_text);

    public Timestamp getScheduled_date();

    public void setScheduled_date(Timestamp scheduled_date);

    public Integer getScheduler_id();

    public void setScheduler_id(Integer scheduler_id);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
