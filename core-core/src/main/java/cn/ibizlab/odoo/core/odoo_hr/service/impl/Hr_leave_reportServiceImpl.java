package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_report;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_leave_reportOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_leave_reportClientModel;

/**
 * 实体[请假摘要/报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_leave_reportServiceImpl implements IHr_leave_reportService {

    @Autowired
    hr_leave_reportOdooClient hr_leave_reportOdooClient;


    @Override
    public Hr_leave_report get(Integer id) {
        hr_leave_reportClientModel clientModel = new hr_leave_reportClientModel();
        clientModel.setId(id);
		hr_leave_reportOdooClient.get(clientModel);
        Hr_leave_report et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_leave_report();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Hr_leave_report et) {
        hr_leave_reportClientModel clientModel = convert2Model(et,null);
		hr_leave_reportOdooClient.create(clientModel);
        Hr_leave_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_leave_report> list){
    }

    @Override
    public boolean update(Hr_leave_report et) {
        hr_leave_reportClientModel clientModel = convert2Model(et,null);
		hr_leave_reportOdooClient.update(clientModel);
        Hr_leave_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_leave_report> list){
    }

    @Override
    public boolean remove(Integer id) {
        hr_leave_reportClientModel clientModel = new hr_leave_reportClientModel();
        clientModel.setId(id);
		hr_leave_reportOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_leave_report> searchDefault(Hr_leave_reportSearchContext context) {
        List<Hr_leave_report> list = new ArrayList<Hr_leave_report>();
        Page<hr_leave_reportClientModel> clientModelList = hr_leave_reportOdooClient.search(context);
        for(hr_leave_reportClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_leave_report>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_leave_reportClientModel convert2Model(Hr_leave_report domain , hr_leave_reportClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_leave_reportClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("date_todirtyflag"))
                model.setDate_to(domain.getDateTo());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("number_of_daysdirtyflag"))
                model.setNumber_of_days(domain.getNumberOfDays());
            if((Boolean) domain.getExtensionparams().get("payslip_statusdirtyflag"))
                model.setPayslip_status(domain.getPayslipStatus());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("date_fromdirtyflag"))
                model.setDate_from(domain.getDateFrom());
            if((Boolean) domain.getExtensionparams().get("holiday_typedirtyflag"))
                model.setHoliday_type(domain.getHolidayType());
            if((Boolean) domain.getExtensionparams().get("holiday_status_id_textdirtyflag"))
                model.setHoliday_status_id_text(domain.getHolidayStatusIdText());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("employee_id_textdirtyflag"))
                model.setEmployee_id_text(domain.getEmployeeIdText());
            if((Boolean) domain.getExtensionparams().get("department_id_textdirtyflag"))
                model.setDepartment_id_text(domain.getDepartmentIdText());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("employee_iddirtyflag"))
                model.setEmployee_id(domain.getEmployeeId());
            if((Boolean) domain.getExtensionparams().get("holiday_status_iddirtyflag"))
                model.setHoliday_status_id(domain.getHolidayStatusId());
            if((Boolean) domain.getExtensionparams().get("department_iddirtyflag"))
                model.setDepartment_id(domain.getDepartmentId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_leave_report convert2Domain( hr_leave_reportClientModel model ,Hr_leave_report domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_leave_report();
        }

        if(model.getDate_toDirtyFlag())
            domain.setDateTo(model.getDate_to());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNumber_of_daysDirtyFlag())
            domain.setNumberOfDays(model.getNumber_of_days());
        if(model.getPayslip_statusDirtyFlag())
            domain.setPayslipStatus(model.getPayslip_status());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getDate_fromDirtyFlag())
            domain.setDateFrom(model.getDate_from());
        if(model.getHoliday_typeDirtyFlag())
            domain.setHolidayType(model.getHoliday_type());
        if(model.getHoliday_status_id_textDirtyFlag())
            domain.setHolidayStatusIdText(model.getHoliday_status_id_text());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getEmployee_id_textDirtyFlag())
            domain.setEmployeeIdText(model.getEmployee_id_text());
        if(model.getDepartment_id_textDirtyFlag())
            domain.setDepartmentIdText(model.getDepartment_id_text());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getEmployee_idDirtyFlag())
            domain.setEmployeeId(model.getEmployee_id());
        if(model.getHoliday_status_idDirtyFlag())
            domain.setHolidayStatusId(model.getHoliday_status_id());
        if(model.getDepartment_idDirtyFlag())
            domain.setDepartmentId(model.getDepartment_id());
        return domain ;
    }

}

    



