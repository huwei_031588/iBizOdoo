package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_jobSearchContext;

/**
 * 实体 [工作岗位] 存储模型
 */
public interface Hr_job{

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 当前员工数量
     */
    Integer getNo_of_employee();

    void setNo_of_employee(Integer no_of_employee);

    /**
     * 获取 [当前员工数量]脏标记
     */
    boolean getNo_of_employeeDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 已雇用员工
     */
    Integer getNo_of_hired_employee();

    void setNo_of_hired_employee(Integer no_of_hired_employee);

    /**
     * 获取 [已雇用员工]脏标记
     */
    boolean getNo_of_hired_employeeDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 网站元说明
     */
    String getWebsite_meta_description();

    void setWebsite_meta_description(String website_meta_description);

    /**
     * 获取 [网站元说明]脏标记
     */
    boolean getWebsite_meta_descriptionDirtyFlag();

    /**
     * 期望的新员工
     */
    Integer getNo_of_recruitment();

    void setNo_of_recruitment(Integer no_of_recruitment);

    /**
     * 获取 [期望的新员工]脏标记
     */
    boolean getNo_of_recruitmentDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * SEO优化
     */
    String getIs_seo_optimized();

    void setIs_seo_optimized(String is_seo_optimized);

    /**
     * 获取 [SEO优化]脏标记
     */
    boolean getIs_seo_optimizedDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 工作岗位
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [工作岗位]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 工作说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [工作说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 网站网址
     */
    String getWebsite_url();

    void setWebsite_url(String website_url);

    /**
     * 获取 [网站网址]脏标记
     */
    boolean getWebsite_urlDirtyFlag();

    /**
     * 要求
     */
    String getRequirements();

    void setRequirements(String requirements);

    /**
     * 获取 [要求]脏标记
     */
    boolean getRequirementsDirtyFlag();

    /**
     * 网站meta标题
     */
    String getWebsite_meta_title();

    void setWebsite_meta_title(String website_meta_title);

    /**
     * 获取 [网站meta标题]脏标记
     */
    boolean getWebsite_meta_titleDirtyFlag();

    /**
     * 应用数量
     */
    Integer getApplication_count();

    void setApplication_count(Integer application_count);

    /**
     * 获取 [应用数量]脏标记
     */
    boolean getApplication_countDirtyFlag();

    /**
     * 求职申请
     */
    String getApplication_ids();

    void setApplication_ids(String application_ids);

    /**
     * 获取 [求职申请]脏标记
     */
    boolean getApplication_idsDirtyFlag();

    /**
     * 已发布
     */
    String getIs_published();

    void setIs_published(String is_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getIs_publishedDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 网站opengraph图像
     */
    String getWebsite_meta_og_img();

    void setWebsite_meta_og_img(String website_meta_og_img);

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    boolean getWebsite_meta_og_imgDirtyFlag();

    /**
     * 预计员工数合计
     */
    Integer getExpected_employees();

    void setExpected_employees(Integer expected_employees);

    /**
     * 获取 [预计员工数合计]脏标记
     */
    boolean getExpected_employeesDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 在当前网站显示
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [在当前网站显示]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 信息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [信息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 网站说明
     */
    String getWebsite_description();

    void setWebsite_description(String website_description);

    /**
     * 获取 [网站说明]脏标记
     */
    boolean getWebsite_descriptionDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 文档
     */
    String getDocument_ids();

    void setDocument_ids(String document_ids);

    /**
     * 获取 [文档]脏标记
     */
    boolean getDocument_idsDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 员工
     */
    String getEmployee_ids();

    void setEmployee_ids(String employee_ids);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_idsDirtyFlag();

    /**
     * 文档数
     */
    Integer getDocuments_count();

    void setDocuments_count(Integer documents_count);

    /**
     * 获取 [文档数]脏标记
     */
    boolean getDocuments_countDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 网站meta关键词
     */
    String getWebsite_meta_keywords();

    void setWebsite_meta_keywords(String website_meta_keywords);

    /**
     * 获取 [网站meta关键词]脏标记
     */
    boolean getWebsite_meta_keywordsDirtyFlag();

    /**
     * 默认值
     */
    String getAlias_defaults();

    void setAlias_defaults(String alias_defaults);

    /**
     * 获取 [默认值]脏标记
     */
    boolean getAlias_defaultsDirtyFlag();

    /**
     * 别名
     */
    String getAlias_name();

    void setAlias_name(String alias_name);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_nameDirtyFlag();

    /**
     * 记录线索ID
     */
    Integer getAlias_force_thread_id();

    void setAlias_force_thread_id(Integer alias_force_thread_id);

    /**
     * 获取 [记录线索ID]脏标记
     */
    boolean getAlias_force_thread_idDirtyFlag();

    /**
     * 网域别名
     */
    String getAlias_domain();

    void setAlias_domain(String alias_domain);

    /**
     * 获取 [网域别名]脏标记
     */
    boolean getAlias_domainDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 上级模型
     */
    Integer getAlias_parent_model_id();

    void setAlias_parent_model_id(Integer alias_parent_model_id);

    /**
     * 获取 [上级模型]脏标记
     */
    boolean getAlias_parent_model_idDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 上级记录ID
     */
    Integer getAlias_parent_thread_id();

    void setAlias_parent_thread_id(Integer alias_parent_thread_id);

    /**
     * 获取 [上级记录ID]脏标记
     */
    boolean getAlias_parent_thread_idDirtyFlag();

    /**
     * 模型别名
     */
    Integer getAlias_model_id();

    void setAlias_model_id(Integer alias_model_id);

    /**
     * 获取 [模型别名]脏标记
     */
    boolean getAlias_model_idDirtyFlag();

    /**
     * 工作地点
     */
    String getAddress_id_text();

    void setAddress_id_text(String address_id_text);

    /**
     * 获取 [工作地点]脏标记
     */
    boolean getAddress_id_textDirtyFlag();

    /**
     * 所有者
     */
    Integer getAlias_user_id();

    void setAlias_user_id(Integer alias_user_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getAlias_user_idDirtyFlag();

    /**
     * 部门
     */
    String getDepartment_id_text();

    void setDepartment_id_text(String department_id_text);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 招聘负责人
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [招聘负责人]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 部门经理
     */
    String getManager_id_text();

    void setManager_id_text(String manager_id_text);

    /**
     * 获取 [部门经理]脏标记
     */
    boolean getManager_id_textDirtyFlag();

    /**
     * 别名联系人安全
     */
    String getAlias_contact();

    void setAlias_contact(String alias_contact);

    /**
     * 获取 [别名联系人安全]脏标记
     */
    boolean getAlias_contactDirtyFlag();

    /**
     * 人力资源主管
     */
    String getHr_responsible_id_text();

    void setHr_responsible_id_text(String hr_responsible_id_text);

    /**
     * 获取 [人力资源主管]脏标记
     */
    boolean getHr_responsible_id_textDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 部门经理
     */
    Integer getManager_id();

    void setManager_id(Integer manager_id);

    /**
     * 获取 [部门经理]脏标记
     */
    boolean getManager_idDirtyFlag();

    /**
     * 工作地点
     */
    Integer getAddress_id();

    void setAddress_id(Integer address_id);

    /**
     * 获取 [工作地点]脏标记
     */
    boolean getAddress_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 人力资源主管
     */
    Integer getHr_responsible_id();

    void setHr_responsible_id(Integer hr_responsible_id);

    /**
     * 获取 [人力资源主管]脏标记
     */
    boolean getHr_responsible_idDirtyFlag();

    /**
     * 部门
     */
    Integer getDepartment_id();

    void setDepartment_id(Integer department_id);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_idDirtyFlag();

    /**
     * 别名
     */
    Integer getAlias_id();

    void setAlias_id(Integer alias_id);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_idDirtyFlag();

    /**
     * 招聘负责人
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [招聘负责人]脏标记
     */
    boolean getUser_idDirtyFlag();

}
