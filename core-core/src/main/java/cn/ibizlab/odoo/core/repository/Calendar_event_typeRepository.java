package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Calendar_event_type;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_event_typeSearchContext;

/**
 * 实体 [事件会议类型] 存储对象
 */
public interface Calendar_event_typeRepository extends Repository<Calendar_event_type> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Calendar_event_type> searchDefault(Calendar_event_typeSearchContext context);

    Calendar_event_type convert2PO(cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type domain , Calendar_event_type po) ;

    cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type convert2Domain( Calendar_event_type po ,cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type domain) ;

}
