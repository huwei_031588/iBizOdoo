package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Purchase_bill_union;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;

/**
 * 实体 [采购 & 账单] 存储对象
 */
public interface Purchase_bill_unionRepository extends Repository<Purchase_bill_union> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Purchase_bill_union> searchDefault(Purchase_bill_unionSearchContext context);

    Purchase_bill_union convert2PO(cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union domain , Purchase_bill_union po) ;

    cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union convert2Domain( Purchase_bill_union po ,cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union domain) ;

}
