package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_registrationSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_registrationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_event.client.event_registrationOdooClient;
import cn.ibizlab.odoo.core.odoo_event.clientmodel.event_registrationClientModel;

/**
 * 实体[事件记录] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_registrationServiceImpl implements IEvent_registrationService {

    @Autowired
    event_registrationOdooClient event_registrationOdooClient;


    @Override
    public boolean create(Event_registration et) {
        event_registrationClientModel clientModel = convert2Model(et,null);
		event_registrationOdooClient.create(clientModel);
        Event_registration rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_registration> list){
    }

    @Override
    public boolean remove(Integer id) {
        event_registrationClientModel clientModel = new event_registrationClientModel();
        clientModel.setId(id);
		event_registrationOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Event_registration et) {
        event_registrationClientModel clientModel = convert2Model(et,null);
		event_registrationOdooClient.update(clientModel);
        Event_registration rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Event_registration> list){
    }

    @Override
    public Event_registration get(Integer id) {
        event_registrationClientModel clientModel = new event_registrationClientModel();
        clientModel.setId(id);
		event_registrationOdooClient.get(clientModel);
        Event_registration et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Event_registration();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_registration> searchDefault(Event_registrationSearchContext context) {
        List<Event_registration> list = new ArrayList<Event_registration>();
        Page<event_registrationClientModel> clientModelList = event_registrationOdooClient.search(context);
        for(event_registrationClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Event_registration>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public event_registrationClientModel convert2Model(Event_registration domain , event_registrationClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new event_registrationClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("phonedirtyflag"))
                model.setPhone(domain.getPhone());
            if((Boolean) domain.getExtensionparams().get("date_opendirtyflag"))
                model.setDate_open(domain.getDateOpen());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("origindirtyflag"))
                model.setOrigin(domain.getOrigin());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("emaildirtyflag"))
                model.setEmail(domain.getEmail());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("date_closeddirtyflag"))
                model.setDate_closed(domain.getDateClosed());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("event_begin_datedirtyflag"))
                model.setEvent_begin_date(domain.getEventBeginDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("sale_order_id_textdirtyflag"))
                model.setSale_order_id_text(domain.getSaleOrderIdText());
            if((Boolean) domain.getExtensionparams().get("event_end_datedirtyflag"))
                model.setEvent_end_date(domain.getEventEndDate());
            if((Boolean) domain.getExtensionparams().get("sale_order_line_id_textdirtyflag"))
                model.setSale_order_line_id_text(domain.getSaleOrderLineIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("event_ticket_id_textdirtyflag"))
                model.setEvent_ticket_id_text(domain.getEventTicketIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("event_id_textdirtyflag"))
                model.setEvent_id_text(domain.getEventIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("event_iddirtyflag"))
                model.setEvent_id(domain.getEventId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("sale_order_line_iddirtyflag"))
                model.setSale_order_line_id(domain.getSaleOrderLineId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("event_ticket_iddirtyflag"))
                model.setEvent_ticket_id(domain.getEventTicketId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("sale_order_iddirtyflag"))
                model.setSale_order_id(domain.getSaleOrderId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Event_registration convert2Domain( event_registrationClientModel model ,Event_registration domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Event_registration();
        }

        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getPhoneDirtyFlag())
            domain.setPhone(model.getPhone());
        if(model.getDate_openDirtyFlag())
            domain.setDateOpen(model.getDate_open());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getOriginDirtyFlag())
            domain.setOrigin(model.getOrigin());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getEmailDirtyFlag())
            domain.setEmail(model.getEmail());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDate_closedDirtyFlag())
            domain.setDateClosed(model.getDate_closed());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getEvent_begin_dateDirtyFlag())
            domain.setEventBeginDate(model.getEvent_begin_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getSale_order_id_textDirtyFlag())
            domain.setSaleOrderIdText(model.getSale_order_id_text());
        if(model.getEvent_end_dateDirtyFlag())
            domain.setEventEndDate(model.getEvent_end_date());
        if(model.getSale_order_line_id_textDirtyFlag())
            domain.setSaleOrderLineIdText(model.getSale_order_line_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getEvent_ticket_id_textDirtyFlag())
            domain.setEventTicketIdText(model.getEvent_ticket_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getEvent_id_textDirtyFlag())
            domain.setEventIdText(model.getEvent_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getEvent_idDirtyFlag())
            domain.setEventId(model.getEvent_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getSale_order_line_idDirtyFlag())
            domain.setSaleOrderLineId(model.getSale_order_line_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getEvent_ticket_idDirtyFlag())
            domain.setEventTicketId(model.getEvent_ticket_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getSale_order_idDirtyFlag())
            domain.setSaleOrderId(model.getSale_order_id());
        return domain ;
    }

}

    



