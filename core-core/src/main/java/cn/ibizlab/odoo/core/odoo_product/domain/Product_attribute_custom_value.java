package cn.ibizlab.odoo.core.odoo_product.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [产品属性自定义值] 对象
 */
@Data
public class Product_attribute_custom_value extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 自定义值
     */
    @DEField(name = "custom_value")
    @JSONField(name = "custom_value")
    @JsonProperty("custom_value")
    private String customValue;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 销售订单行
     */
    @JSONField(name = "sale_order_line_id_text")
    @JsonProperty("sale_order_line_id_text")
    private String saleOrderLineIdText;

    /**
     * 属性
     */
    @JSONField(name = "attribute_value_id_text")
    @JsonProperty("attribute_value_id_text")
    private String attributeValueIdText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 销售订单行
     */
    @DEField(name = "sale_order_line_id")
    @JSONField(name = "sale_order_line_id")
    @JsonProperty("sale_order_line_id")
    private Integer saleOrderLineId;

    /**
     * 属性
     */
    @DEField(name = "attribute_value_id")
    @JSONField(name = "attribute_value_id")
    @JsonProperty("attribute_value_id")
    private Integer attributeValueId;


    /**
     * 
     */
    @JSONField(name = "odooattributevalue")
    @JsonProperty("odooattributevalue")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_value odooAttributeValue;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoosaleorderline")
    @JsonProperty("odoosaleorderline")
    private cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line odooSaleOrderLine;




    /**
     * 设置 [自定义值]
     */
    public void setCustomValue(String customValue){
        this.customValue = customValue ;
        this.modify("custom_value",customValue);
    }
    /**
     * 设置 [销售订单行]
     */
    public void setSaleOrderLineId(Integer saleOrderLineId){
        this.saleOrderLineId = saleOrderLineId ;
        this.modify("sale_order_line_id",saleOrderLineId);
    }
    /**
     * 设置 [属性]
     */
    public void setAttributeValueId(Integer attributeValueId){
        this.attributeValueId = attributeValueId ;
        this.modify("attribute_value_id",attributeValueId);
    }

}


