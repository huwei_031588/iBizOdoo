package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_activity_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_mixinSearchContext;

/**
 * 实体 [活动Mixin] 存储对象
 */
public interface Mail_activity_mixinRepository extends Repository<Mail_activity_mixin> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_activity_mixin> searchDefault(Mail_activity_mixinSearchContext context);

    Mail_activity_mixin convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_mixin domain , Mail_activity_mixin po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_mixin convert2Domain( Mail_activity_mixin po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_mixin domain) ;

}
