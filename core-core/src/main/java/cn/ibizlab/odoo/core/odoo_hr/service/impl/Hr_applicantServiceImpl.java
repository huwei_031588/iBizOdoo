package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicantSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_applicantService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_applicantOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_applicantClientModel;

/**
 * 实体[申请人] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_applicantServiceImpl implements IHr_applicantService {

    @Autowired
    hr_applicantOdooClient hr_applicantOdooClient;


    @Override
    public boolean create(Hr_applicant et) {
        hr_applicantClientModel clientModel = convert2Model(et,null);
		hr_applicantOdooClient.create(clientModel);
        Hr_applicant rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_applicant> list){
    }

    @Override
    public boolean remove(Integer id) {
        hr_applicantClientModel clientModel = new hr_applicantClientModel();
        clientModel.setId(id);
		hr_applicantOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Hr_applicant et) {
        hr_applicantClientModel clientModel = convert2Model(et,null);
		hr_applicantOdooClient.update(clientModel);
        Hr_applicant rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_applicant> list){
    }

    @Override
    public Hr_applicant get(Integer id) {
        hr_applicantClientModel clientModel = new hr_applicantClientModel();
        clientModel.setId(id);
		hr_applicantOdooClient.get(clientModel);
        Hr_applicant et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_applicant();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_applicant> searchDefault(Hr_applicantSearchContext context) {
        List<Hr_applicant> list = new ArrayList<Hr_applicant>();
        Page<hr_applicantClientModel> clientModelList = hr_applicantOdooClient.search(context);
        for(hr_applicantClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_applicant>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_applicantClientModel convert2Model(Hr_applicant domain , hr_applicantClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_applicantClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("prioritydirtyflag"))
                model.setPriority(domain.getPriority());
            if((Boolean) domain.getExtensionparams().get("salary_expecteddirtyflag"))
                model.setSalary_expected(domain.getSalaryExpected());
            if((Boolean) domain.getExtensionparams().get("categ_idsdirtyflag"))
                model.setCateg_ids(domain.getCategIds());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("salary_proposeddirtyflag"))
                model.setSalary_proposed(domain.getSalaryProposed());
            if((Boolean) domain.getExtensionparams().get("attachment_numberdirtyflag"))
                model.setAttachment_number(domain.getAttachmentNumber());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("partner_phonedirtyflag"))
                model.setPartner_phone(domain.getPartnerPhone());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("kanban_statedirtyflag"))
                model.setKanban_state(domain.getKanbanState());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("date_opendirtyflag"))
                model.setDate_open(domain.getDateOpen());
            if((Boolean) domain.getExtensionparams().get("day_opendirtyflag"))
                model.setDay_open(domain.getDayOpen());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("availabilitydirtyflag"))
                model.setAvailability(domain.getAvailability());
            if((Boolean) domain.getExtensionparams().get("salary_expected_extradirtyflag"))
                model.setSalary_expected_extra(domain.getSalaryExpectedExtra());
            if((Boolean) domain.getExtensionparams().get("date_closeddirtyflag"))
                model.setDate_closed(domain.getDateClosed());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("partner_mobiledirtyflag"))
                model.setPartner_mobile(domain.getPartnerMobile());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("salary_proposed_extradirtyflag"))
                model.setSalary_proposed_extra(domain.getSalaryProposedExtra());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("email_fromdirtyflag"))
                model.setEmail_from(domain.getEmailFrom());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("date_last_stage_updatedirtyflag"))
                model.setDate_last_stage_update(domain.getDateLastStageUpdate());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("referencedirtyflag"))
                model.setReference(domain.getReference());
            if((Boolean) domain.getExtensionparams().get("attachment_idsdirtyflag"))
                model.setAttachment_ids(domain.getAttachmentIds());
            if((Boolean) domain.getExtensionparams().get("email_ccdirtyflag"))
                model.setEmail_cc(domain.getEmailCc());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("partner_namedirtyflag"))
                model.setPartner_name(domain.getPartnerName());
            if((Boolean) domain.getExtensionparams().get("day_closedirtyflag"))
                model.setDay_close(domain.getDayClose());
            if((Boolean) domain.getExtensionparams().get("probabilitydirtyflag"))
                model.setProbability(domain.getProbability());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("delay_closedirtyflag"))
                model.setDelay_close(domain.getDelayClose());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("source_id_textdirtyflag"))
                model.setSource_id_text(domain.getSourceIdText());
            if((Boolean) domain.getExtensionparams().get("campaign_id_textdirtyflag"))
                model.setCampaign_id_text(domain.getCampaignIdText());
            if((Boolean) domain.getExtensionparams().get("department_id_textdirtyflag"))
                model.setDepartment_id_text(domain.getDepartmentIdText());
            if((Boolean) domain.getExtensionparams().get("last_stage_id_textdirtyflag"))
                model.setLast_stage_id_text(domain.getLastStageIdText());
            if((Boolean) domain.getExtensionparams().get("stage_id_textdirtyflag"))
                model.setStage_id_text(domain.getStageIdText());
            if((Boolean) domain.getExtensionparams().get("medium_id_textdirtyflag"))
                model.setMedium_id_text(domain.getMediumIdText());
            if((Boolean) domain.getExtensionparams().get("legend_normaldirtyflag"))
                model.setLegend_normal(domain.getLegendNormal());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("type_id_textdirtyflag"))
                model.setType_id_text(domain.getTypeIdText());
            if((Boolean) domain.getExtensionparams().get("job_id_textdirtyflag"))
                model.setJob_id_text(domain.getJobIdText());
            if((Boolean) domain.getExtensionparams().get("legend_donedirtyflag"))
                model.setLegend_done(domain.getLegendDone());
            if((Boolean) domain.getExtensionparams().get("legend_blockeddirtyflag"))
                model.setLegend_blocked(domain.getLegendBlocked());
            if((Boolean) domain.getExtensionparams().get("employee_namedirtyflag"))
                model.setEmployee_name(domain.getEmployeeName());
            if((Boolean) domain.getExtensionparams().get("user_emaildirtyflag"))
                model.setUser_email(domain.getUserEmail());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("job_iddirtyflag"))
                model.setJob_id(domain.getJobId());
            if((Boolean) domain.getExtensionparams().get("campaign_iddirtyflag"))
                model.setCampaign_id(domain.getCampaignId());
            if((Boolean) domain.getExtensionparams().get("last_stage_iddirtyflag"))
                model.setLast_stage_id(domain.getLastStageId());
            if((Boolean) domain.getExtensionparams().get("department_iddirtyflag"))
                model.setDepartment_id(domain.getDepartmentId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("medium_iddirtyflag"))
                model.setMedium_id(domain.getMediumId());
            if((Boolean) domain.getExtensionparams().get("emp_iddirtyflag"))
                model.setEmp_id(domain.getEmpId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("type_iddirtyflag"))
                model.setType_id(domain.getTypeId());
            if((Boolean) domain.getExtensionparams().get("source_iddirtyflag"))
                model.setSource_id(domain.getSourceId());
            if((Boolean) domain.getExtensionparams().get("stage_iddirtyflag"))
                model.setStage_id(domain.getStageId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_applicant convert2Domain( hr_applicantClientModel model ,Hr_applicant domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_applicant();
        }

        if(model.getPriorityDirtyFlag())
            domain.setPriority(model.getPriority());
        if(model.getSalary_expectedDirtyFlag())
            domain.setSalaryExpected(model.getSalary_expected());
        if(model.getCateg_idsDirtyFlag())
            domain.setCategIds(model.getCateg_ids());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getSalary_proposedDirtyFlag())
            domain.setSalaryProposed(model.getSalary_proposed());
        if(model.getAttachment_numberDirtyFlag())
            domain.setAttachmentNumber(model.getAttachment_number());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getPartner_phoneDirtyFlag())
            domain.setPartnerPhone(model.getPartner_phone());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getKanban_stateDirtyFlag())
            domain.setKanbanState(model.getKanban_state());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getDate_openDirtyFlag())
            domain.setDateOpen(model.getDate_open());
        if(model.getDay_openDirtyFlag())
            domain.setDayOpen(model.getDay_open());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getAvailabilityDirtyFlag())
            domain.setAvailability(model.getAvailability());
        if(model.getSalary_expected_extraDirtyFlag())
            domain.setSalaryExpectedExtra(model.getSalary_expected_extra());
        if(model.getDate_closedDirtyFlag())
            domain.setDateClosed(model.getDate_closed());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getPartner_mobileDirtyFlag())
            domain.setPartnerMobile(model.getPartner_mobile());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getSalary_proposed_extraDirtyFlag())
            domain.setSalaryProposedExtra(model.getSalary_proposed_extra());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getEmail_fromDirtyFlag())
            domain.setEmailFrom(model.getEmail_from());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getDate_last_stage_updateDirtyFlag())
            domain.setDateLastStageUpdate(model.getDate_last_stage_update());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getReferenceDirtyFlag())
            domain.setReference(model.getReference());
        if(model.getAttachment_idsDirtyFlag())
            domain.setAttachmentIds(model.getAttachment_ids());
        if(model.getEmail_ccDirtyFlag())
            domain.setEmailCc(model.getEmail_cc());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getPartner_nameDirtyFlag())
            domain.setPartnerName(model.getPartner_name());
        if(model.getDay_closeDirtyFlag())
            domain.setDayClose(model.getDay_close());
        if(model.getProbabilityDirtyFlag())
            domain.setProbability(model.getProbability());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getDelay_closeDirtyFlag())
            domain.setDelayClose(model.getDelay_close());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getSource_id_textDirtyFlag())
            domain.setSourceIdText(model.getSource_id_text());
        if(model.getCampaign_id_textDirtyFlag())
            domain.setCampaignIdText(model.getCampaign_id_text());
        if(model.getDepartment_id_textDirtyFlag())
            domain.setDepartmentIdText(model.getDepartment_id_text());
        if(model.getLast_stage_id_textDirtyFlag())
            domain.setLastStageIdText(model.getLast_stage_id_text());
        if(model.getStage_id_textDirtyFlag())
            domain.setStageIdText(model.getStage_id_text());
        if(model.getMedium_id_textDirtyFlag())
            domain.setMediumIdText(model.getMedium_id_text());
        if(model.getLegend_normalDirtyFlag())
            domain.setLegendNormal(model.getLegend_normal());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getType_id_textDirtyFlag())
            domain.setTypeIdText(model.getType_id_text());
        if(model.getJob_id_textDirtyFlag())
            domain.setJobIdText(model.getJob_id_text());
        if(model.getLegend_doneDirtyFlag())
            domain.setLegendDone(model.getLegend_done());
        if(model.getLegend_blockedDirtyFlag())
            domain.setLegendBlocked(model.getLegend_blocked());
        if(model.getEmployee_nameDirtyFlag())
            domain.setEmployeeName(model.getEmployee_name());
        if(model.getUser_emailDirtyFlag())
            domain.setUserEmail(model.getUser_email());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getJob_idDirtyFlag())
            domain.setJobId(model.getJob_id());
        if(model.getCampaign_idDirtyFlag())
            domain.setCampaignId(model.getCampaign_id());
        if(model.getLast_stage_idDirtyFlag())
            domain.setLastStageId(model.getLast_stage_id());
        if(model.getDepartment_idDirtyFlag())
            domain.setDepartmentId(model.getDepartment_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getMedium_idDirtyFlag())
            domain.setMediumId(model.getMedium_id());
        if(model.getEmp_idDirtyFlag())
            domain.setEmpId(model.getEmp_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getType_idDirtyFlag())
            domain.setTypeId(model.getType_id());
        if(model.getSource_idDirtyFlag())
            domain.setSourceId(model.getSource_id());
        if(model.getStage_idDirtyFlag())
            domain.setStageId(model.getStage_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        return domain ;
    }

}

    



