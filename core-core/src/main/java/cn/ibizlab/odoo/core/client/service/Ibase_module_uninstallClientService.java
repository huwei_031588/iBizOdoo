package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_module_uninstall;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_module_uninstall] 服务对象接口
 */
public interface Ibase_module_uninstallClientService{

    public Ibase_module_uninstall createModel() ;

    public void update(Ibase_module_uninstall base_module_uninstall);

    public void updateBatch(List<Ibase_module_uninstall> base_module_uninstalls);

    public void createBatch(List<Ibase_module_uninstall> base_module_uninstalls);

    public void get(Ibase_module_uninstall base_module_uninstall);

    public void removeBatch(List<Ibase_module_uninstall> base_module_uninstalls);

    public Page<Ibase_module_uninstall> search(SearchContext context);

    public void remove(Ibase_module_uninstall base_module_uninstall);

    public void create(Ibase_module_uninstall base_module_uninstall);

    public Page<Ibase_module_uninstall> select(SearchContext context);

}
