package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_alertSearchContext;

/**
 * 实体 [午餐提醒] 存储模型
 */
public interface Lunch_alert{

    /**
     * 日
     */
    Timestamp getSpecific_day();

    void setSpecific_day(Timestamp specific_day);

    /**
     * 获取 [日]脏标记
     */
    boolean getSpecific_dayDirtyFlag();

    /**
     * 显示
     */
    String getDisplay();

    void setDisplay(String display);

    /**
     * 获取 [显示]脏标记
     */
    boolean getDisplayDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 周四
     */
    String getThursday();

    void setThursday(String thursday);

    /**
     * 获取 [周四]脏标记
     */
    boolean getThursdayDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 周三
     */
    String getWednesday();

    void setWednesday(String wednesday);

    /**
     * 获取 [周三]脏标记
     */
    boolean getWednesdayDirtyFlag();

    /**
     * 周二
     */
    String getTuesday();

    void setTuesday(String tuesday);

    /**
     * 获取 [周二]脏标记
     */
    boolean getTuesdayDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 消息
     */
    String getMessage();

    void setMessage(String message);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessageDirtyFlag();

    /**
     * 重新提起
     */
    String getAlert_type();

    void setAlert_type(String alert_type);

    /**
     * 获取 [重新提起]脏标记
     */
    boolean getAlert_typeDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 周六
     */
    String getSaturday();

    void setSaturday(String saturday);

    /**
     * 获取 [周六]脏标记
     */
    boolean getSaturdayDirtyFlag();

    /**
     * 介于
     */
    Double getStart_hour();

    void setStart_hour(Double start_hour);

    /**
     * 获取 [介于]脏标记
     */
    boolean getStart_hourDirtyFlag();

    /**
     * 周一
     */
    String getMonday();

    void setMonday(String monday);

    /**
     * 获取 [周一]脏标记
     */
    boolean getMondayDirtyFlag();

    /**
     * 周五
     */
    String getFriday();

    void setFriday(String friday);

    /**
     * 获取 [周五]脏标记
     */
    boolean getFridayDirtyFlag();

    /**
     * 和
     */
    Double getEnd_hour();

    void setEnd_hour(Double end_hour);

    /**
     * 获取 [和]脏标记
     */
    boolean getEnd_hourDirtyFlag();

    /**
     * 周日
     */
    String getSunday();

    void setSunday(String sunday);

    /**
     * 获取 [周日]脏标记
     */
    boolean getSundayDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 供应商
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 供应商
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
