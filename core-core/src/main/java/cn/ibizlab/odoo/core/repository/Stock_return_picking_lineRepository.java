package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_return_picking_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_picking_lineSearchContext;

/**
 * 实体 [退料明细行] 存储对象
 */
public interface Stock_return_picking_lineRepository extends Repository<Stock_return_picking_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_return_picking_line> searchDefault(Stock_return_picking_lineSearchContext context);

    Stock_return_picking_line convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking_line domain , Stock_return_picking_line po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking_line convert2Domain( Stock_return_picking_line po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking_line domain) ;

}
