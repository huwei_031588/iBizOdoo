package cn.ibizlab.odoo.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_convert_order;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_convert_orderSearchContext;


/**
 * 实体[Mro_convert_order] 服务对象接口
 */
public interface IMro_convert_orderService{

    boolean create(Mro_convert_order et) ;
    void createBatch(List<Mro_convert_order> list) ;
    Mro_convert_order get(Integer key) ;
    boolean update(Mro_convert_order et) ;
    void updateBatch(List<Mro_convert_order> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mro_convert_order> searchDefault(Mro_convert_orderSearchContext context) ;

}



