package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.lunch_alert;

/**
 * 实体[lunch_alert] 服务对象接口
 */
public interface lunch_alertRepository{


    public lunch_alert createPO() ;
        public void removeBatch(String id);

        public void createBatch(lunch_alert lunch_alert);

        public List<lunch_alert> search();

        public void remove(String id);

        public void get(String id);

        public void updateBatch(lunch_alert lunch_alert);

        public void update(lunch_alert lunch_alert);

        public void create(lunch_alert lunch_alert);


}
