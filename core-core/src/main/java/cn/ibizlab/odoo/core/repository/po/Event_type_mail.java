package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_event.filter.Event_type_mailSearchContext;

/**
 * 实体 [基于活动分类的邮件调度] 存储模型
 */
public interface Event_type_mail{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 触发器
     */
    String getInterval_type();

    void setInterval_type(String interval_type);

    /**
     * 获取 [触发器]脏标记
     */
    boolean getInterval_typeDirtyFlag();

    /**
     * 单位
     */
    String getInterval_unit();

    void setInterval_unit(String interval_unit);

    /**
     * 获取 [单位]脏标记
     */
    boolean getInterval_unitDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 间隔
     */
    Integer getInterval_nbr();

    void setInterval_nbr(Integer interval_nbr);

    /**
     * 获取 [间隔]脏标记
     */
    boolean getInterval_nbrDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 活动类型
     */
    String getEvent_type_id_text();

    void setEvent_type_id_text(String event_type_id_text);

    /**
     * 获取 [活动类型]脏标记
     */
    boolean getEvent_type_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * EMail模板
     */
    String getTemplate_id_text();

    void setTemplate_id_text(String template_id_text);

    /**
     * 获取 [EMail模板]脏标记
     */
    boolean getTemplate_id_textDirtyFlag();

    /**
     * 活动类型
     */
    Integer getEvent_type_id();

    void setEvent_type_id(Integer event_type_id);

    /**
     * 获取 [活动类型]脏标记
     */
    boolean getEvent_type_idDirtyFlag();

    /**
     * EMail模板
     */
    Integer getTemplate_id();

    void setTemplate_id(Integer template_id);

    /**
     * 获取 [EMail模板]脏标记
     */
    boolean getTemplate_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
