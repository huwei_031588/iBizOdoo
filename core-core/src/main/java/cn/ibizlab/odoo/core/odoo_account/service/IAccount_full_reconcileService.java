package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_full_reconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_full_reconcileSearchContext;


/**
 * 实体[Account_full_reconcile] 服务对象接口
 */
public interface IAccount_full_reconcileService{

    boolean create(Account_full_reconcile et) ;
    void createBatch(List<Account_full_reconcile> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Account_full_reconcile get(Integer key) ;
    boolean update(Account_full_reconcile et) ;
    void updateBatch(List<Account_full_reconcile> list) ;
    Page<Account_full_reconcile> searchDefault(Account_full_reconcileSearchContext context) ;

}



