package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_register_paymentsSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_register_paymentsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_register_paymentsOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_register_paymentsClientModel;

/**
 * 实体[登记付款] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_register_paymentsServiceImpl implements IAccount_register_paymentsService {

    @Autowired
    account_register_paymentsOdooClient account_register_paymentsOdooClient;


    @Override
    public Account_register_payments get(Integer id) {
        account_register_paymentsClientModel clientModel = new account_register_paymentsClientModel();
        clientModel.setId(id);
		account_register_paymentsOdooClient.get(clientModel);
        Account_register_payments et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_register_payments();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_register_payments et) {
        account_register_paymentsClientModel clientModel = convert2Model(et,null);
		account_register_paymentsOdooClient.create(clientModel);
        Account_register_payments rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_register_payments> list){
    }

    @Override
    public boolean update(Account_register_payments et) {
        account_register_paymentsClientModel clientModel = convert2Model(et,null);
		account_register_paymentsOdooClient.update(clientModel);
        Account_register_payments rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_register_payments> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_register_paymentsClientModel clientModel = new account_register_paymentsClientModel();
        clientModel.setId(id);
		account_register_paymentsOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_register_payments> searchDefault(Account_register_paymentsSearchContext context) {
        List<Account_register_payments> list = new ArrayList<Account_register_payments>();
        Page<account_register_paymentsClientModel> clientModelList = account_register_paymentsOdooClient.search(context);
        for(account_register_paymentsClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_register_payments>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_register_paymentsClientModel convert2Model(Account_register_payments domain , account_register_paymentsClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_register_paymentsClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("show_communication_fielddirtyflag"))
                model.setShow_communication_field(domain.getShowCommunicationField());
            if((Boolean) domain.getExtensionparams().get("payment_typedirtyflag"))
                model.setPayment_type(domain.getPaymentType());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("partner_typedirtyflag"))
                model.setPartner_type(domain.getPartnerType());
            if((Boolean) domain.getExtensionparams().get("communicationdirtyflag"))
                model.setCommunication(domain.getCommunication());
            if((Boolean) domain.getExtensionparams().get("payment_differencedirtyflag"))
                model.setPayment_difference(domain.getPaymentDifference());
            if((Boolean) domain.getExtensionparams().get("show_partner_bank_accountdirtyflag"))
                model.setShow_partner_bank_account(domain.getShowPartnerBankAccount());
            if((Boolean) domain.getExtensionparams().get("group_invoicesdirtyflag"))
                model.setGroup_invoices(domain.getGroupInvoices());
            if((Boolean) domain.getExtensionparams().get("hide_payment_methoddirtyflag"))
                model.setHide_payment_method(domain.getHidePaymentMethod());
            if((Boolean) domain.getExtensionparams().get("invoice_idsdirtyflag"))
                model.setInvoice_ids(domain.getInvoiceIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("payment_datedirtyflag"))
                model.setPayment_date(domain.getPaymentDate());
            if((Boolean) domain.getExtensionparams().get("writeoff_labeldirtyflag"))
                model.setWriteoff_label(domain.getWriteoffLabel());
            if((Boolean) domain.getExtensionparams().get("multidirtyflag"))
                model.setMulti(domain.getMulti());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("payment_difference_handlingdirtyflag"))
                model.setPayment_difference_handling(domain.getPaymentDifferenceHandling());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("payment_method_codedirtyflag"))
                model.setPayment_method_code(domain.getPaymentMethodCode());
            if((Boolean) domain.getExtensionparams().get("payment_method_id_textdirtyflag"))
                model.setPayment_method_id_text(domain.getPaymentMethodIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("writeoff_account_id_textdirtyflag"))
                model.setWriteoff_account_id_text(domain.getWriteoffAccountIdText());
            if((Boolean) domain.getExtensionparams().get("writeoff_account_iddirtyflag"))
                model.setWriteoff_account_id(domain.getWriteoffAccountId());
            if((Boolean) domain.getExtensionparams().get("partner_bank_account_iddirtyflag"))
                model.setPartner_bank_account_id(domain.getPartnerBankAccountId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("payment_method_iddirtyflag"))
                model.setPayment_method_id(domain.getPaymentMethodId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_register_payments convert2Domain( account_register_paymentsClientModel model ,Account_register_payments domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_register_payments();
        }

        if(model.getShow_communication_fieldDirtyFlag())
            domain.setShowCommunicationField(model.getShow_communication_field());
        if(model.getPayment_typeDirtyFlag())
            domain.setPaymentType(model.getPayment_type());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPartner_typeDirtyFlag())
            domain.setPartnerType(model.getPartner_type());
        if(model.getCommunicationDirtyFlag())
            domain.setCommunication(model.getCommunication());
        if(model.getPayment_differenceDirtyFlag())
            domain.setPaymentDifference(model.getPayment_difference());
        if(model.getShow_partner_bank_accountDirtyFlag())
            domain.setShowPartnerBankAccount(model.getShow_partner_bank_account());
        if(model.getGroup_invoicesDirtyFlag())
            domain.setGroupInvoices(model.getGroup_invoices());
        if(model.getHide_payment_methodDirtyFlag())
            domain.setHidePaymentMethod(model.getHide_payment_method());
        if(model.getInvoice_idsDirtyFlag())
            domain.setInvoiceIds(model.getInvoice_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getPayment_dateDirtyFlag())
            domain.setPaymentDate(model.getPayment_date());
        if(model.getWriteoff_labelDirtyFlag())
            domain.setWriteoffLabel(model.getWriteoff_label());
        if(model.getMultiDirtyFlag())
            domain.setMulti(model.getMulti());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getPayment_difference_handlingDirtyFlag())
            domain.setPaymentDifferenceHandling(model.getPayment_difference_handling());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPayment_method_codeDirtyFlag())
            domain.setPaymentMethodCode(model.getPayment_method_code());
        if(model.getPayment_method_id_textDirtyFlag())
            domain.setPaymentMethodIdText(model.getPayment_method_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getWriteoff_account_id_textDirtyFlag())
            domain.setWriteoffAccountIdText(model.getWriteoff_account_id_text());
        if(model.getWriteoff_account_idDirtyFlag())
            domain.setWriteoffAccountId(model.getWriteoff_account_id());
        if(model.getPartner_bank_account_idDirtyFlag())
            domain.setPartnerBankAccountId(model.getPartner_bank_account_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getPayment_method_idDirtyFlag())
            domain.setPaymentMethodId(model.getPayment_method_id());
        return domain ;
    }

}

    



