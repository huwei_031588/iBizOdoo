package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_vehicle_log_fuel;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_fuelSearchContext;

/**
 * 实体 [车辆燃油记录] 存储对象
 */
public interface Fleet_vehicle_log_fuelRepository extends Repository<Fleet_vehicle_log_fuel> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_vehicle_log_fuel> searchDefault(Fleet_vehicle_log_fuelSearchContext context);

    Fleet_vehicle_log_fuel convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel domain , Fleet_vehicle_log_fuel po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel convert2Domain( Fleet_vehicle_log_fuel po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel domain) ;

}
