package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.survey_survey;

/**
 * 实体[survey_survey] 服务对象接口
 */
public interface survey_surveyRepository{


    public survey_survey createPO() ;
        public void create(survey_survey survey_survey);

        public List<survey_survey> search();

        public void remove(String id);

        public void createBatch(survey_survey survey_survey);

        public void update(survey_survey survey_survey);

        public void updateBatch(survey_survey survey_survey);

        public void removeBatch(String id);

        public void get(String id);


}
