package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_loss_typeSearchContext;

/**
 * 实体 [MRP工单生产力损失] 存储对象
 */
public interface Mrp_workcenter_productivity_loss_typeRepository extends Repository<Mrp_workcenter_productivity_loss_type> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_workcenter_productivity_loss_type> searchDefault(Mrp_workcenter_productivity_loss_typeSearchContext context);

    Mrp_workcenter_productivity_loss_type convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type domain , Mrp_workcenter_productivity_loss_type po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type convert2Domain( Mrp_workcenter_productivity_loss_type po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type domain) ;

}
