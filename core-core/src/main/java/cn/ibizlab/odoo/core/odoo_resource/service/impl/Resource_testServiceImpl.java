package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_test;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_testSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_testService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_resource.client.resource_testOdooClient;
import cn.ibizlab.odoo.core.odoo_resource.clientmodel.resource_testClientModel;

/**
 * 实体[测试资源模型] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_testServiceImpl implements IResource_testService {

    @Autowired
    resource_testOdooClient resource_testOdooClient;


    @Override
    public boolean remove(Integer id) {
        resource_testClientModel clientModel = new resource_testClientModel();
        clientModel.setId(id);
		resource_testOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Resource_test et) {
        resource_testClientModel clientModel = convert2Model(et,null);
		resource_testOdooClient.update(clientModel);
        Resource_test rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Resource_test> list){
    }

    @Override
    public boolean create(Resource_test et) {
        resource_testClientModel clientModel = convert2Model(et,null);
		resource_testOdooClient.create(clientModel);
        Resource_test rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_test> list){
    }

    @Override
    public Resource_test get(Integer id) {
        resource_testClientModel clientModel = new resource_testClientModel();
        clientModel.setId(id);
		resource_testOdooClient.get(clientModel);
        Resource_test et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Resource_test();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_test> searchDefault(Resource_testSearchContext context) {
        List<Resource_test> list = new ArrayList<Resource_test>();
        Page<resource_testClientModel> clientModelList = resource_testOdooClient.search(context);
        for(resource_testClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Resource_test>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public resource_testClientModel convert2Model(Resource_test domain , resource_testClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new resource_testClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_id_textdirtyflag"))
                model.setResource_calendar_id_text(domain.getResourceCalendarIdText());
            if((Boolean) domain.getExtensionparams().get("resource_id_textdirtyflag"))
                model.setResource_id_text(domain.getResourceIdText());
            if((Boolean) domain.getExtensionparams().get("tzdirtyflag"))
                model.setTz(domain.getTz());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("resource_iddirtyflag"))
                model.setResource_id(domain.getResourceId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_iddirtyflag"))
                model.setResource_calendar_id(domain.getResourceCalendarId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Resource_test convert2Domain( resource_testClientModel model ,Resource_test domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Resource_test();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getResource_calendar_id_textDirtyFlag())
            domain.setResourceCalendarIdText(model.getResource_calendar_id_text());
        if(model.getResource_id_textDirtyFlag())
            domain.setResourceIdText(model.getResource_id_text());
        if(model.getTzDirtyFlag())
            domain.setTz(model.getTz());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getResource_idDirtyFlag())
            domain.setResourceId(model.getResource_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getResource_calendar_idDirtyFlag())
            domain.setResourceCalendarId(model.getResource_calendar_id());
        return domain ;
    }

}

    



