package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [gamification_badge_user] 对象
 */
public interface gamification_badge_user {

    public Integer getBadge_id();

    public void setBadge_id(Integer badge_id);

    public String getBadge_id_text();

    public void setBadge_id_text(String badge_id_text);

    public String getBadge_name();

    public void setBadge_name(String badge_name);

    public Integer getChallenge_id();

    public void setChallenge_id(Integer challenge_id);

    public String getChallenge_id_text();

    public void setChallenge_id_text(String challenge_id_text);

    public String getComment();

    public void setComment(String comment);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getEmployee_id();

    public void setEmployee_id(Integer employee_id);

    public String getEmployee_id_text();

    public void setEmployee_id_text(String employee_id_text);

    public Integer getId();

    public void setId(Integer id);

    public String getLevel();

    public void setLevel(String level);

    public Integer getSender_id();

    public void setSender_id(Integer sender_id);

    public String getSender_id_text();

    public void setSender_id_text(String sender_id_text);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
