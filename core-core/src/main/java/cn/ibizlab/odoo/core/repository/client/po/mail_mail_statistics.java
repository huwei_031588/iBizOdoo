package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mail_mail_statistics] 对象
 */
public interface mail_mail_statistics {

    public Timestamp getBounced();

    public void setBounced(Timestamp bounced);

    public Timestamp getClicked();

    public void setClicked(Timestamp clicked);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmail();

    public void setEmail(String email);

    public Timestamp getException();

    public void setException(Timestamp exception);

    public Integer getId();

    public void setId(Integer id);

    public Timestamp getIgnored();

    public void setIgnored(Timestamp ignored);

    public String getLinks_click_ids();

    public void setLinks_click_ids(String links_click_ids);

    public Integer getMail_mail_id();

    public void setMail_mail_id(Integer mail_mail_id);

    public Integer getMail_mail_id_int();

    public void setMail_mail_id_int(Integer mail_mail_id_int);

    public Integer getMass_mailing_campaign_id();

    public void setMass_mailing_campaign_id(Integer mass_mailing_campaign_id);

    public String getMass_mailing_campaign_id_text();

    public void setMass_mailing_campaign_id_text(String mass_mailing_campaign_id_text);

    public Integer getMass_mailing_id();

    public void setMass_mailing_id(Integer mass_mailing_id);

    public String getMass_mailing_id_text();

    public void setMass_mailing_id_text(String mass_mailing_id_text);

    public String getMessage_id();

    public void setMessage_id(String message_id);

    public String getModel();

    public void setModel(String model);

    public Timestamp getOpened();

    public void setOpened(Timestamp opened);

    public Timestamp getReplied();

    public void setReplied(Timestamp replied);

    public Integer getRes_id();

    public void setRes_id(Integer res_id);

    public Timestamp getScheduled();

    public void setScheduled(Timestamp scheduled);

    public Timestamp getSent();

    public void setSent(Timestamp sent);

    public String getState();

    public void setState(String state);

    public Timestamp getState_update();

    public void setState_update(Timestamp state_update);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
