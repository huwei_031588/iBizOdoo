package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [project_task_type] 对象
 */
public interface Iproject_task_type {

    /**
     * 获取 [自动看板状态]
     */
    public void setAuto_validation_kanban_state(String auto_validation_kanban_state);
    
    /**
     * 设置 [自动看板状态]
     */
    public String getAuto_validation_kanban_state();

    /**
     * 获取 [自动看板状态]脏标记
     */
    public boolean getAuto_validation_kanban_stateDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [集中到看板中]
     */
    public void setFold(String fold);
    
    /**
     * 设置 [集中到看板中]
     */
    public String getFold();

    /**
     * 获取 [集中到看板中]脏标记
     */
    public boolean getFoldDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [红色的看板标签]
     */
    public void setLegend_blocked(String legend_blocked);
    
    /**
     * 设置 [红色的看板标签]
     */
    public String getLegend_blocked();

    /**
     * 获取 [红色的看板标签]脏标记
     */
    public boolean getLegend_blockedDirtyFlag();
    /**
     * 获取 [绿色看板标签]
     */
    public void setLegend_done(String legend_done);
    
    /**
     * 设置 [绿色看板标签]
     */
    public String getLegend_done();

    /**
     * 获取 [绿色看板标签]脏标记
     */
    public boolean getLegend_doneDirtyFlag();
    /**
     * 获取 [灰色看板标签]
     */
    public void setLegend_normal(String legend_normal);
    
    /**
     * 设置 [灰色看板标签]
     */
    public String getLegend_normal();

    /**
     * 获取 [灰色看板标签]脏标记
     */
    public boolean getLegend_normalDirtyFlag();
    /**
     * 获取 [星标解释]
     */
    public void setLegend_priority(String legend_priority);
    
    /**
     * 设置 [星标解释]
     */
    public String getLegend_priority();

    /**
     * 获取 [星标解释]脏标记
     */
    public boolean getLegend_priorityDirtyFlag();
    /**
     * 获取 [EMail模板]
     */
    public void setMail_template_id(Integer mail_template_id);
    
    /**
     * 设置 [EMail模板]
     */
    public Integer getMail_template_id();

    /**
     * 获取 [EMail模板]脏标记
     */
    public boolean getMail_template_idDirtyFlag();
    /**
     * 获取 [EMail模板]
     */
    public void setMail_template_id_text(String mail_template_id_text);
    
    /**
     * 设置 [EMail模板]
     */
    public String getMail_template_id_text();

    /**
     * 获取 [EMail模板]脏标记
     */
    public boolean getMail_template_id_textDirtyFlag();
    /**
     * 获取 [阶段名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [阶段名称]
     */
    public String getName();

    /**
     * 获取 [阶段名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [项目]
     */
    public void setProject_ids(String project_ids);
    
    /**
     * 设置 [项目]
     */
    public String getProject_ids();

    /**
     * 获取 [项目]脏标记
     */
    public boolean getProject_idsDirtyFlag();
    /**
     * 获取 [点评邮件模板]
     */
    public void setRating_template_id(Integer rating_template_id);
    
    /**
     * 设置 [点评邮件模板]
     */
    public Integer getRating_template_id();

    /**
     * 获取 [点评邮件模板]脏标记
     */
    public boolean getRating_template_idDirtyFlag();
    /**
     * 获取 [点评邮件模板]
     */
    public void setRating_template_id_text(String rating_template_id_text);
    
    /**
     * 设置 [点评邮件模板]
     */
    public String getRating_template_id_text();

    /**
     * 获取 [点评邮件模板]脏标记
     */
    public boolean getRating_template_id_textDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
