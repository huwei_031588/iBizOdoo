package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_reconcile_model_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;

/**
 * 实体 [核销模型模板] 存储对象
 */
public interface Account_reconcile_model_templateRepository extends Repository<Account_reconcile_model_template> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_reconcile_model_template> searchDefault(Account_reconcile_model_templateSearchContext context);

    Account_reconcile_model_template convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model_template domain , Account_reconcile_model_template po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model_template convert2Domain( Account_reconcile_model_template po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model_template domain) ;

}
