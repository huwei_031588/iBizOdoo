package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Lunch_alert;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_alertSearchContext;

/**
 * 实体 [午餐提醒] 存储对象
 */
public interface Lunch_alertRepository extends Repository<Lunch_alert> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Lunch_alert> searchDefault(Lunch_alertSearchContext context);

    Lunch_alert convert2PO(cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_alert domain , Lunch_alert po) ;

    cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_alert convert2Domain( Lunch_alert po ,cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_alert domain) ;

}
