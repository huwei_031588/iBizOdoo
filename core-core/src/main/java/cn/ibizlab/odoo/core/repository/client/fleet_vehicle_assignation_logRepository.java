package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_vehicle_assignation_log;

/**
 * 实体[fleet_vehicle_assignation_log] 服务对象接口
 */
public interface fleet_vehicle_assignation_logRepository{


    public fleet_vehicle_assignation_log createPO() ;
        public void removeBatch(String id);

        public List<fleet_vehicle_assignation_log> search();

        public void updateBatch(fleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public void update(fleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public void createBatch(fleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public void get(String id);

        public void create(fleet_vehicle_assignation_log fleet_vehicle_assignation_log);

        public void remove(String id);


}
