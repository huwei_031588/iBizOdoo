package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.survey_stage;

/**
 * 实体[survey_stage] 服务对象接口
 */
public interface survey_stageRepository{


    public survey_stage createPO() ;
        public List<survey_stage> search();

        public void updateBatch(survey_stage survey_stage);

        public void removeBatch(String id);

        public void create(survey_stage survey_stage);

        public void remove(String id);

        public void createBatch(survey_stage survey_stage);

        public void update(survey_stage survey_stage);

        public void get(String id);


}
