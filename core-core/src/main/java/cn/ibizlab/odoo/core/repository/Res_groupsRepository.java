package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_groups;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_groupsSearchContext;

/**
 * 实体 [访问群] 存储对象
 */
public interface Res_groupsRepository extends Repository<Res_groups> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_groups> searchDefault(Res_groupsSearchContext context);

    Res_groups convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_groups domain , Res_groups po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_groups convert2Domain( Res_groups po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_groups domain) ;

}
