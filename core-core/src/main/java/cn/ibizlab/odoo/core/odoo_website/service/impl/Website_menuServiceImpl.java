package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_menu;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_menuSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_menuService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_website.client.website_menuOdooClient;
import cn.ibizlab.odoo.core.odoo_website.clientmodel.website_menuClientModel;

/**
 * 实体[网站菜单] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_menuServiceImpl implements IWebsite_menuService {

    @Autowired
    website_menuOdooClient website_menuOdooClient;


    @Override
    public boolean update(Website_menu et) {
        website_menuClientModel clientModel = convert2Model(et,null);
		website_menuOdooClient.update(clientModel);
        Website_menu rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Website_menu> list){
    }

    @Override
    public Website_menu get(Integer id) {
        website_menuClientModel clientModel = new website_menuClientModel();
        clientModel.setId(id);
		website_menuOdooClient.get(clientModel);
        Website_menu et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Website_menu();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        website_menuClientModel clientModel = new website_menuClientModel();
        clientModel.setId(id);
		website_menuOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Website_menu et) {
        website_menuClientModel clientModel = convert2Model(et,null);
		website_menuOdooClient.create(clientModel);
        Website_menu rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_menu> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_menu> searchDefault(Website_menuSearchContext context) {
        List<Website_menu> list = new ArrayList<Website_menu>();
        Page<website_menuClientModel> clientModelList = website_menuOdooClient.search(context);
        for(website_menuClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Website_menu>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public website_menuClientModel convert2Model(Website_menu domain , website_menuClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new website_menuClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("theme_template_iddirtyflag"))
                model.setTheme_template_id(domain.getThemeTemplateId());
            if((Boolean) domain.getExtensionparams().get("is_visibledirtyflag"))
                model.setIs_visible(domain.getIsVisible());
            if((Boolean) domain.getExtensionparams().get("urldirtyflag"))
                model.setUrl(domain.getUrl());
            if((Boolean) domain.getExtensionparams().get("new_windowdirtyflag"))
                model.setNew_window(domain.getNewWindow());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("parent_pathdirtyflag"))
                model.setParent_path(domain.getParentPath());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("child_iddirtyflag"))
                model.setChild_id(domain.getChildId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("page_id_textdirtyflag"))
                model.setPage_id_text(domain.getPageIdText());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("page_iddirtyflag"))
                model.setPage_id(domain.getPageId());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Website_menu convert2Domain( website_menuClientModel model ,Website_menu domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Website_menu();
        }

        if(model.getTheme_template_idDirtyFlag())
            domain.setThemeTemplateId(model.getTheme_template_id());
        if(model.getIs_visibleDirtyFlag())
            domain.setIsVisible(model.getIs_visible());
        if(model.getUrlDirtyFlag())
            domain.setUrl(model.getUrl());
        if(model.getNew_windowDirtyFlag())
            domain.setNewWindow(model.getNew_window());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getParent_pathDirtyFlag())
            domain.setParentPath(model.getParent_path());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getChild_idDirtyFlag())
            domain.setChildId(model.getChild_id());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPage_id_textDirtyFlag())
            domain.setPageIdText(model.getPage_id_text());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPage_idDirtyFlag())
            domain.setPageId(model.getPage_id());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



