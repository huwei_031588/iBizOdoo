package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_year;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_yearSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_yearService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_fiscal_yearOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_fiscal_yearClientModel;

/**
 * 实体[会计年度] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_fiscal_yearServiceImpl implements IAccount_fiscal_yearService {

    @Autowired
    account_fiscal_yearOdooClient account_fiscal_yearOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_fiscal_yearClientModel clientModel = new account_fiscal_yearClientModel();
        clientModel.setId(id);
		account_fiscal_yearOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_fiscal_year et) {
        account_fiscal_yearClientModel clientModel = convert2Model(et,null);
		account_fiscal_yearOdooClient.update(clientModel);
        Account_fiscal_year rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_fiscal_year> list){
    }

    @Override
    public Account_fiscal_year get(Integer id) {
        account_fiscal_yearClientModel clientModel = new account_fiscal_yearClientModel();
        clientModel.setId(id);
		account_fiscal_yearOdooClient.get(clientModel);
        Account_fiscal_year et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_fiscal_year();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_fiscal_year et) {
        account_fiscal_yearClientModel clientModel = convert2Model(et,null);
		account_fiscal_yearOdooClient.create(clientModel);
        Account_fiscal_year rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_fiscal_year> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_fiscal_year> searchDefault(Account_fiscal_yearSearchContext context) {
        List<Account_fiscal_year> list = new ArrayList<Account_fiscal_year>();
        Page<account_fiscal_yearClientModel> clientModelList = account_fiscal_yearOdooClient.search(context);
        for(account_fiscal_yearClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_fiscal_year>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_fiscal_yearClientModel convert2Model(Account_fiscal_year domain , account_fiscal_yearClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_fiscal_yearClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("date_fromdirtyflag"))
                model.setDate_from(domain.getDateFrom());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("date_todirtyflag"))
                model.setDate_to(domain.getDateTo());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_fiscal_year convert2Domain( account_fiscal_yearClientModel model ,Account_fiscal_year domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_fiscal_year();
        }

        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDate_fromDirtyFlag())
            domain.setDateFrom(model.getDate_from());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDate_toDirtyFlag())
            domain.setDateTo(model.getDate_to());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



