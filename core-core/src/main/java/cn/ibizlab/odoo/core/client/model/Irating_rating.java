package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [rating_rating] 对象
 */
public interface Irating_rating {

    /**
     * 获取 [安全令牌]
     */
    public void setAccess_token(String access_token);
    
    /**
     * 设置 [安全令牌]
     */
    public String getAccess_token();

    /**
     * 获取 [安全令牌]脏标记
     */
    public boolean getAccess_tokenDirtyFlag();
    /**
     * 获取 [已填写的评级]
     */
    public void setConsumed(String consumed);
    
    /**
     * 设置 [已填写的评级]
     */
    public String getConsumed();

    /**
     * 获取 [已填写的评级]脏标记
     */
    public boolean getConsumedDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setFeedback(String feedback);
    
    /**
     * 设置 [备注]
     */
    public String getFeedback();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getFeedbackDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [链接信息]
     */
    public void setMessage_id(Integer message_id);
    
    /**
     * 设置 [链接信息]
     */
    public Integer getMessage_id();

    /**
     * 获取 [链接信息]脏标记
     */
    public boolean getMessage_idDirtyFlag();
    /**
     * 获取 [父级文档]
     */
    public void setParent_res_id(Integer parent_res_id);
    
    /**
     * 设置 [父级文档]
     */
    public Integer getParent_res_id();

    /**
     * 获取 [父级文档]脏标记
     */
    public boolean getParent_res_idDirtyFlag();
    /**
     * 获取 [父级文档模型]
     */
    public void setParent_res_model(String parent_res_model);
    
    /**
     * 设置 [父级文档模型]
     */
    public String getParent_res_model();

    /**
     * 获取 [父级文档模型]脏标记
     */
    public boolean getParent_res_modelDirtyFlag();
    /**
     * 获取 [父级相关文档模型]
     */
    public void setParent_res_model_id(Integer parent_res_model_id);
    
    /**
     * 设置 [父级相关文档模型]
     */
    public Integer getParent_res_model_id();

    /**
     * 获取 [父级相关文档模型]脏标记
     */
    public boolean getParent_res_model_idDirtyFlag();
    /**
     * 获取 [父级文档名称]
     */
    public void setParent_res_name(String parent_res_name);
    
    /**
     * 设置 [父级文档名称]
     */
    public String getParent_res_name();

    /**
     * 获取 [父级文档名称]脏标记
     */
    public boolean getParent_res_nameDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [客户]
     */
    public Integer getPartner_id();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [客户]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [客户]
     */
    public String getPartner_id_text();

    /**
     * 获取 [客户]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [评级人员]
     */
    public void setRated_partner_id(Integer rated_partner_id);
    
    /**
     * 设置 [评级人员]
     */
    public Integer getRated_partner_id();

    /**
     * 获取 [评级人员]脏标记
     */
    public boolean getRated_partner_idDirtyFlag();
    /**
     * 获取 [评级人员]
     */
    public void setRated_partner_id_text(String rated_partner_id_text);
    
    /**
     * 设置 [评级人员]
     */
    public String getRated_partner_id_text();

    /**
     * 获取 [评级人员]脏标记
     */
    public boolean getRated_partner_id_textDirtyFlag();
    /**
     * 获取 [评级数值]
     */
    public void setRating(Double rating);
    
    /**
     * 设置 [评级数值]
     */
    public Double getRating();

    /**
     * 获取 [评级数值]脏标记
     */
    public boolean getRatingDirtyFlag();
    /**
     * 获取 [图像]
     */
    public void setRating_image(byte[] rating_image);
    
    /**
     * 设置 [图像]
     */
    public byte[] getRating_image();

    /**
     * 获取 [图像]脏标记
     */
    public boolean getRating_imageDirtyFlag();
    /**
     * 获取 [评级]
     */
    public void setRating_text(String rating_text);
    
    /**
     * 设置 [评级]
     */
    public String getRating_text();

    /**
     * 获取 [评级]脏标记
     */
    public boolean getRating_textDirtyFlag();
    /**
     * 获取 [文档]
     */
    public void setRes_id(Integer res_id);
    
    /**
     * 设置 [文档]
     */
    public Integer getRes_id();

    /**
     * 获取 [文档]脏标记
     */
    public boolean getRes_idDirtyFlag();
    /**
     * 获取 [文档模型]
     */
    public void setRes_model(String res_model);
    
    /**
     * 设置 [文档模型]
     */
    public String getRes_model();

    /**
     * 获取 [文档模型]脏标记
     */
    public boolean getRes_modelDirtyFlag();
    /**
     * 获取 [相关的文档模型]
     */
    public void setRes_model_id(Integer res_model_id);
    
    /**
     * 设置 [相关的文档模型]
     */
    public Integer getRes_model_id();

    /**
     * 获取 [相关的文档模型]脏标记
     */
    public boolean getRes_model_idDirtyFlag();
    /**
     * 获取 [资源名称]
     */
    public void setRes_name(String res_name);
    
    /**
     * 设置 [资源名称]
     */
    public String getRes_name();

    /**
     * 获取 [资源名称]脏标记
     */
    public boolean getRes_nameDirtyFlag();
    /**
     * 获取 [已发布]
     */
    public void setWebsite_published(String website_published);
    
    /**
     * 设置 [已发布]
     */
    public String getWebsite_published();

    /**
     * 获取 [已发布]脏标记
     */
    public boolean getWebsite_publishedDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
