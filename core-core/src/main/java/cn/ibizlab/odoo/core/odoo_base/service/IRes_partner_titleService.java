package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_title;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_titleSearchContext;


/**
 * 实体[Res_partner_title] 服务对象接口
 */
public interface IRes_partner_titleService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Res_partner_title get(Integer key) ;
    boolean update(Res_partner_title et) ;
    void updateBatch(List<Res_partner_title> list) ;
    boolean create(Res_partner_title et) ;
    void createBatch(List<Res_partner_title> list) ;
    Page<Res_partner_title> searchDefault(Res_partner_titleSearchContext context) ;

}



