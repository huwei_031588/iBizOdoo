package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_statistics_report;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_statistics_reportSearchContext;

/**
 * 实体 [群发邮件阶段] 存储对象
 */
public interface Mail_statistics_reportRepository extends Repository<Mail_statistics_report> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_statistics_report> searchDefault(Mail_statistics_reportSearchContext context);

    Mail_statistics_report convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_statistics_report domain , Mail_statistics_report po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_statistics_report convert2Domain( Mail_statistics_report po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_statistics_report domain) ;

}
