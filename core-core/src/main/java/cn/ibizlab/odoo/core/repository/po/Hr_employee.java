package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employeeSearchContext;

/**
 * 实体 [员工] 存储模型
 */
public interface Hr_employee{

    /**
     * 办公手机
     */
    String getMobile_phone();

    void setMobile_phone(String mobile_phone);

    /**
     * 获取 [办公手机]脏标记
     */
    boolean getMobile_phoneDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 起始日期
     */
    Timestamp getLeave_date_from();

    void setLeave_date_from(Timestamp leave_date_from);

    /**
     * 获取 [起始日期]脏标记
     */
    boolean getLeave_date_fromDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 子女数目
     */
    Integer getChildren();

    void setChildren(Integer children);

    /**
     * 获取 [子女数目]脏标记
     */
    boolean getChildrenDirtyFlag();

    /**
     * 小尺寸照片
     */
    byte[] getImage_small();

    void setImage_small(byte[] image_small);

    /**
     * 获取 [小尺寸照片]脏标记
     */
    boolean getImage_smallDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * PIN
     */
    String getPin();

    void setPin(String pin);

    /**
     * 获取 [PIN]脏标记
     */
    boolean getPinDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 毕业院校
     */
    String getStudy_school();

    void setStudy_school(String study_school);

    /**
     * 获取 [毕业院校]脏标记
     */
    boolean getStudy_schoolDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 婚姻状况
     */
    String getMarital();

    void setMarital(String marital);

    /**
     * 获取 [婚姻状况]脏标记
     */
    boolean getMaritalDirtyFlag();

    /**
     * 直接徽章
     */
    String getDirect_badge_ids();

    void setDirect_badge_ids(String direct_badge_ids);

    /**
     * 获取 [直接徽章]脏标记
     */
    boolean getDirect_badge_idsDirtyFlag();

    /**
     * 中等尺寸照片
     */
    byte[] getImage_medium();

    void setImage_medium(byte[] image_medium);

    /**
     * 获取 [中等尺寸照片]脏标记
     */
    boolean getImage_mediumDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 紧急电话
     */
    String getEmergency_phone();

    void setEmergency_phone(String emergency_phone);

    /**
     * 获取 [紧急电话]脏标记
     */
    boolean getEmergency_phoneDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 上班距离
     */
    Integer getKm_home_work();

    void setKm_home_work(Integer km_home_work);

    /**
     * 获取 [上班距离]脏标记
     */
    boolean getKm_home_workDirtyFlag();

    /**
     * 签证号
     */
    String getVisa_no();

    void setVisa_no(String visa_no);

    /**
     * 获取 [签证号]脏标记
     */
    boolean getVisa_noDirtyFlag();

    /**
     * 出生地
     */
    String getPlace_of_birth();

    void setPlace_of_birth(String place_of_birth);

    /**
     * 获取 [出生地]脏标记
     */
    boolean getPlace_of_birthDirtyFlag();

    /**
     * 配偶全名
     */
    String getSpouse_complete_name();

    void setSpouse_complete_name(String spouse_complete_name);

    /**
     * 获取 [配偶全名]脏标记
     */
    boolean getSpouse_complete_nameDirtyFlag();

    /**
     * 社会保险号SIN
     */
    String getSinid();

    void setSinid(String sinid);

    /**
     * 获取 [社会保险号SIN]脏标记
     */
    boolean getSinidDirtyFlag();

    /**
     * 员工徽章
     */
    String getBadge_ids();

    void setBadge_ids(String badge_ids);

    /**
     * 获取 [员工徽章]脏标记
     */
    boolean getBadge_idsDirtyFlag();

    /**
     * 工作EMail
     */
    String getWork_email();

    void setWork_email(String work_email);

    /**
     * 获取 [工作EMail]脏标记
     */
    boolean getWork_emailDirtyFlag();

    /**
     * 拥有徽章
     */
    String getHas_badges();

    void setHas_badges(String has_badges);

    /**
     * 获取 [拥有徽章]脏标记
     */
    boolean getHas_badgesDirtyFlag();

    /**
     * 当前休假类型
     */
    Integer getCurrent_leave_id();

    void setCurrent_leave_id(Integer current_leave_id);

    /**
     * 获取 [当前休假类型]脏标记
     */
    boolean getCurrent_leave_idDirtyFlag();

    /**
     * 员工合同
     */
    String getContract_ids();

    void setContract_ids(String contract_ids);

    /**
     * 获取 [员工合同]脏标记
     */
    boolean getContract_idsDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 工牌 ID
     */
    String getBarcode();

    void setBarcode(String barcode);

    /**
     * 获取 [工牌 ID]脏标记
     */
    boolean getBarcodeDirtyFlag();

    /**
     * 出生日期
     */
    Timestamp getBirthday();

    void setBirthday(Timestamp birthday);

    /**
     * 获取 [出生日期]脏标记
     */
    boolean getBirthdayDirtyFlag();

    /**
     * 证书等级
     */
    String getCertificate();

    void setCertificate(String certificate);

    /**
     * 获取 [证书等级]脏标记
     */
    boolean getCertificateDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 研究领域
     */
    String getStudy_field();

    void setStudy_field(String study_field);

    /**
     * 获取 [研究领域]脏标记
     */
    boolean getStudy_fieldDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 出勤状态
     */
    String getAttendance_state();

    void setAttendance_state(String attendance_state);

    /**
     * 获取 [出勤状态]脏标记
     */
    boolean getAttendance_stateDirtyFlag();

    /**
     * 合同统计
     */
    Integer getContracts_count();

    void setContracts_count(Integer contracts_count);

    /**
     * 获取 [合同统计]脏标记
     */
    boolean getContracts_countDirtyFlag();

    /**
     * 性别
     */
    String getGender();

    void setGender(String gender);

    /**
     * 获取 [性别]脏标记
     */
    boolean getGenderDirtyFlag();

    /**
     * 照片
     */
    byte[] getImage();

    void setImage(byte[] image);

    /**
     * 获取 [照片]脏标记
     */
    boolean getImageDirtyFlag();

    /**
     * 配偶生日
     */
    Timestamp getSpouse_birthdate();

    void setSpouse_birthdate(Timestamp spouse_birthdate);

    /**
     * 获取 [配偶生日]脏标记
     */
    boolean getSpouse_birthdateDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 休假天数
     */
    Double getLeaves_count();

    void setLeaves_count(Double leaves_count);

    /**
     * 获取 [休假天数]脏标记
     */
    boolean getLeaves_countDirtyFlag();

    /**
     * 备注
     */
    String getNotes();

    void setNotes(String notes);

    /**
     * 获取 [备注]脏标记
     */
    boolean getNotesDirtyFlag();

    /**
     * 附加说明
     */
    String getAdditional_note();

    void setAdditional_note(String additional_note);

    /**
     * 获取 [附加说明]脏标记
     */
    boolean getAdditional_noteDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 体检日期
     */
    Timestamp getMedic_exam();

    void setMedic_exam(Timestamp medic_exam);

    /**
     * 获取 [体检日期]脏标记
     */
    boolean getMedic_examDirtyFlag();

    /**
     * 签证到期日期
     */
    Timestamp getVisa_expire();

    void setVisa_expire(Timestamp visa_expire);

    /**
     * 获取 [签证到期日期]脏标记
     */
    boolean getVisa_expireDirtyFlag();

    /**
     * 办公电话
     */
    String getWork_phone();

    void setWork_phone(String work_phone);

    /**
     * 获取 [办公电话]脏标记
     */
    boolean getWork_phoneDirtyFlag();

    /**
     * 紧急联系人
     */
    String getEmergency_contact();

    void setEmergency_contact(String emergency_contact);

    /**
     * 获取 [紧急联系人]脏标记
     */
    boolean getEmergency_contactDirtyFlag();

    /**
     * 新近雇用的员工
     */
    String getNewly_hired_employee();

    void setNewly_hired_employee(String newly_hired_employee);

    /**
     * 获取 [新近雇用的员工]脏标记
     */
    boolean getNewly_hired_employeeDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 已与公司地址相关联的员工住址
     */
    String getIs_address_home_a_company();

    void setIs_address_home_a_company(String is_address_home_a_company);

    /**
     * 获取 [已与公司地址相关联的员工住址]脏标记
     */
    boolean getIs_address_home_a_companyDirtyFlag();

    /**
     * 出勤
     */
    String getAttendance_ids();

    void setAttendance_ids(String attendance_ids);

    /**
     * 获取 [出勤]脏标记
     */
    boolean getAttendance_idsDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 当前休假状态
     */
    String getCurrent_leave_state();

    void setCurrent_leave_state(String current_leave_state);

    /**
     * 获取 [当前休假状态]脏标记
     */
    boolean getCurrent_leave_stateDirtyFlag();

    /**
     * 下属
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [下属]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 信息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [信息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 手动出勤
     */
    String getManual_attendance();

    void setManual_attendance(String manual_attendance);

    /**
     * 获取 [手动出勤]脏标记
     */
    boolean getManual_attendanceDirtyFlag();

    /**
     * 剩余的法定休假
     */
    Double getRemaining_leaves();

    void setRemaining_leaves(Double remaining_leaves);

    /**
     * 获取 [剩余的法定休假]脏标记
     */
    boolean getRemaining_leavesDirtyFlag();

    /**
     * 社会保障号SSN
     */
    String getSsnid();

    void setSsnid(String ssnid);

    /**
     * 获取 [社会保障号SSN]脏标记
     */
    boolean getSsnidDirtyFlag();

    /**
     * 工作头衔
     */
    String getJob_title();

    void setJob_title(String job_title);

    /**
     * 获取 [工作头衔]脏标记
     */
    boolean getJob_titleDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 今日缺勤
     */
    String getIs_absent_totay();

    void setIs_absent_totay(String is_absent_totay);

    /**
     * 获取 [今日缺勤]脏标记
     */
    boolean getIs_absent_totayDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 护照号
     */
    String getPassport_id();

    void setPassport_id(String passport_id);

    /**
     * 获取 [护照号]脏标记
     */
    boolean getPassport_idDirtyFlag();

    /**
     * 员工人力资源目标
     */
    String getGoal_ids();

    void setGoal_ids(String goal_ids);

    /**
     * 获取 [员工人力资源目标]脏标记
     */
    boolean getGoal_idsDirtyFlag();

    /**
     * 至日期
     */
    Timestamp getLeave_date_to();

    void setLeave_date_to(Timestamp leave_date_to);

    /**
     * 获取 [至日期]脏标记
     */
    boolean getLeave_date_toDirtyFlag();

    /**
     * 是管理者
     */
    String getManager();

    void setManager(String manager);

    /**
     * 获取 [是管理者]脏标记
     */
    boolean getManagerDirtyFlag();

    /**
     * 工作地点
     */
    String getWork_location();

    void setWork_location(String work_location);

    /**
     * 获取 [工作地点]脏标记
     */
    boolean getWork_locationDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 当前合同
     */
    Integer getContract_id();

    void setContract_id(Integer contract_id);

    /**
     * 获取 [当前合同]脏标记
     */
    boolean getContract_idDirtyFlag();

    /**
     * 员工文档
     */
    String getGoogle_drive_link();

    void setGoogle_drive_link(String google_drive_link);

    /**
     * 获取 [员工文档]脏标记
     */
    boolean getGoogle_drive_linkDirtyFlag();

    /**
     * 工作许可编号
     */
    String getPermit_no();

    void setPermit_no(String permit_no);

    /**
     * 获取 [工作许可编号]脏标记
     */
    boolean getPermit_noDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 身份证号
     */
    String getIdentification_id();

    void setIdentification_id(String identification_id);

    /**
     * 获取 [身份证号]脏标记
     */
    boolean getIdentification_idDirtyFlag();

    /**
     * 公司汽车
     */
    String getVehicle();

    void setVehicle(String vehicle);

    /**
     * 获取 [公司汽车]脏标记
     */
    boolean getVehicleDirtyFlag();

    /**
     * 标签
     */
    String getCategory_ids();

    void setCategory_ids(String category_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getCategory_idsDirtyFlag();

    /**
     * 能查看剩余的休假
     */
    String getShow_leaves();

    void setShow_leaves(String show_leaves);

    /**
     * 获取 [能查看剩余的休假]脏标记
     */
    boolean getShow_leavesDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 经理
     */
    String getParent_id_text();

    void setParent_id_text(String parent_id_text);

    /**
     * 获取 [经理]脏标记
     */
    boolean getParent_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 家庭住址
     */
    String getAddress_home_id_text();

    void setAddress_home_id_text(String address_home_id_text);

    /**
     * 获取 [家庭住址]脏标记
     */
    boolean getAddress_home_id_textDirtyFlag();

    /**
     * 时区
     */
    String getTz();

    void setTz(String tz);

    /**
     * 获取 [时区]脏标记
     */
    boolean getTzDirtyFlag();

    /**
     * 工作时间
     */
    String getResource_calendar_id_text();

    void setResource_calendar_id_text(String resource_calendar_id_text);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getResource_calendar_id_textDirtyFlag();

    /**
     * 用户
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 部门
     */
    String getDepartment_id_text();

    void setDepartment_id_text(String department_id_text);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_id_textDirtyFlag();

    /**
     * 国籍
     */
    String getCountry_of_birth_text();

    void setCountry_of_birth_text(String country_of_birth_text);

    /**
     * 获取 [国籍]脏标记
     */
    boolean getCountry_of_birth_textDirtyFlag();

    /**
     * 负责人
     */
    String getExpense_manager_id_text();

    void setExpense_manager_id_text(String expense_manager_id_text);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getExpense_manager_id_textDirtyFlag();

    /**
     * 工作岗位
     */
    String getJob_id_text();

    void setJob_id_text(String job_id_text);

    /**
     * 获取 [工作岗位]脏标记
     */
    boolean getJob_id_textDirtyFlag();

    /**
     * 工作地址
     */
    String getAddress_id_text();

    void setAddress_id_text(String address_id_text);

    /**
     * 获取 [工作地址]脏标记
     */
    boolean getAddress_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 教练
     */
    String getCoach_id_text();

    void setCoach_id_text(String coach_id_text);

    /**
     * 获取 [教练]脏标记
     */
    boolean getCoach_id_textDirtyFlag();

    /**
     * 国籍(国家)
     */
    String getCountry_id_text();

    void setCountry_id_text(String country_id_text);

    /**
     * 获取 [国籍(国家)]脏标记
     */
    boolean getCountry_id_textDirtyFlag();

    /**
     * 家庭住址
     */
    Integer getAddress_home_id();

    void setAddress_home_id(Integer address_home_id);

    /**
     * 获取 [家庭住址]脏标记
     */
    boolean getAddress_home_idDirtyFlag();

    /**
     * 负责人
     */
    Integer getExpense_manager_id();

    void setExpense_manager_id(Integer expense_manager_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getExpense_manager_idDirtyFlag();

    /**
     * 银行账户号码
     */
    Integer getBank_account_id();

    void setBank_account_id(Integer bank_account_id);

    /**
     * 获取 [银行账户号码]脏标记
     */
    boolean getBank_account_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 国籍(国家)
     */
    Integer getCountry_id();

    void setCountry_id(Integer country_id);

    /**
     * 获取 [国籍(国家)]脏标记
     */
    boolean getCountry_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 工作岗位
     */
    Integer getJob_id();

    void setJob_id(Integer job_id);

    /**
     * 获取 [工作岗位]脏标记
     */
    boolean getJob_idDirtyFlag();

    /**
     * 资源
     */
    Integer getResource_id();

    void setResource_id(Integer resource_id);

    /**
     * 获取 [资源]脏标记
     */
    boolean getResource_idDirtyFlag();

    /**
     * 用户
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 部门
     */
    Integer getDepartment_id();

    void setDepartment_id(Integer department_id);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_idDirtyFlag();

    /**
     * 经理
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [经理]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 上次出勤
     */
    Integer getLast_attendance_id();

    void setLast_attendance_id(Integer last_attendance_id);

    /**
     * 获取 [上次出勤]脏标记
     */
    boolean getLast_attendance_idDirtyFlag();

    /**
     * 教练
     */
    Integer getCoach_id();

    void setCoach_id(Integer coach_id);

    /**
     * 获取 [教练]脏标记
     */
    boolean getCoach_idDirtyFlag();

    /**
     * 工作地址
     */
    Integer getAddress_id();

    void setAddress_id(Integer address_id);

    /**
     * 获取 [工作地址]脏标记
     */
    boolean getAddress_idDirtyFlag();

    /**
     * 国籍
     */
    Integer getCountry_of_birth();

    void setCountry_of_birth(Integer country_of_birth);

    /**
     * 获取 [国籍]脏标记
     */
    boolean getCountry_of_birthDirtyFlag();

    /**
     * 工作时间
     */
    Integer getResource_calendar_id();

    void setResource_calendar_id(Integer resource_calendar_id);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getResource_calendar_idDirtyFlag();

}
