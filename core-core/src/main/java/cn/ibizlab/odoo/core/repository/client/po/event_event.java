package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [event_event] 对象
 */
public interface event_event {

    public String getActive();

    public void setActive(String active);

    public Integer getAddress_id();

    public void setAddress_id(Integer address_id);

    public String getAddress_id_text();

    public void setAddress_id_text(String address_id_text);

    public String getAuto_confirm();

    public void setAuto_confirm(String auto_confirm);

    public String getBadge_back();

    public void setBadge_back(String badge_back);

    public String getBadge_front();

    public void setBadge_front(String badge_front);

    public String getBadge_innerleft();

    public void setBadge_innerleft(String badge_innerleft);

    public String getBadge_innerright();

    public void setBadge_innerright(String badge_innerright);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Integer getCountry_id();

    public void setCountry_id(Integer country_id);

    public String getCountry_id_text();

    public void setCountry_id_text(String country_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_begin();

    public void setDate_begin(Timestamp date_begin);

    public String getDate_begin_located();

    public void setDate_begin_located(String date_begin_located);

    public Timestamp getDate_end();

    public void setDate_end(Timestamp date_end);

    public String getDate_end_located();

    public void setDate_end_located(String date_end_located);

    public String getDate_tz();

    public void setDate_tz(String date_tz);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEvent_logo();

    public void setEvent_logo(String event_logo);

    public String getEvent_mail_ids();

    public void setEvent_mail_ids(String event_mail_ids);

    public String getEvent_ticket_ids();

    public void setEvent_ticket_ids(String event_ticket_ids);

    public Integer getEvent_type_id();

    public void setEvent_type_id(Integer event_type_id);

    public String getEvent_type_id_text();

    public void setEvent_type_id_text(String event_type_id_text);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_online();

    public void setIs_online(String is_online);

    public String getIs_participating();

    public void setIs_participating(String is_participating);

    public String getIs_published();

    public void setIs_published(String is_published);

    public String getIs_seo_optimized();

    public void setIs_seo_optimized(String is_seo_optimized);

    public Integer getMenu_id();

    public void setMenu_id(Integer menu_id);

    public String getMenu_id_text();

    public void setMenu_id_text(String menu_id_text);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public Integer getOrganizer_id();

    public void setOrganizer_id(Integer organizer_id);

    public String getOrganizer_id_text();

    public void setOrganizer_id_text(String organizer_id_text);

    public String getRegistration_ids();

    public void setRegistration_ids(String registration_ids);

    public String getSeats_availability();

    public void setSeats_availability(String seats_availability);

    public Integer getSeats_available();

    public void setSeats_available(Integer seats_available);

    public Integer getSeats_expected();

    public void setSeats_expected(Integer seats_expected);

    public Integer getSeats_max();

    public void setSeats_max(Integer seats_max);

    public Integer getSeats_min();

    public void setSeats_min(Integer seats_min);

    public Integer getSeats_reserved();

    public void setSeats_reserved(Integer seats_reserved);

    public Integer getSeats_unconfirmed();

    public void setSeats_unconfirmed(Integer seats_unconfirmed);

    public Integer getSeats_used();

    public void setSeats_used(Integer seats_used);

    public String getState();

    public void setState(String state);

    public String getTwitter_hashtag();

    public void setTwitter_hashtag(String twitter_hashtag);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public String getWebsite_menu();

    public void setWebsite_menu(String website_menu);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public String getWebsite_meta_description();

    public void setWebsite_meta_description(String website_meta_description);

    public String getWebsite_meta_keywords();

    public void setWebsite_meta_keywords(String website_meta_keywords);

    public String getWebsite_meta_og_img();

    public void setWebsite_meta_og_img(String website_meta_og_img);

    public String getWebsite_meta_title();

    public void setWebsite_meta_title(String website_meta_title);

    public String getWebsite_published();

    public void setWebsite_published(String website_published);

    public String getWebsite_url();

    public void setWebsite_url(String website_url);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
