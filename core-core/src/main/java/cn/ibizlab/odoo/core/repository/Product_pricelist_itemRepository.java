package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_pricelist_item;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelist_itemSearchContext;

/**
 * 实体 [价格表明细] 存储对象
 */
public interface Product_pricelist_itemRepository extends Repository<Product_pricelist_item> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_pricelist_item> searchDefault(Product_pricelist_itemSearchContext context);

    Product_pricelist_item convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist_item domain , Product_pricelist_item po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist_item convert2Domain( Product_pricelist_item po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist_item domain) ;

}
