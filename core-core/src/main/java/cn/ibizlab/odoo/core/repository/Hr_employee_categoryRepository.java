package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_employee_category;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employee_categorySearchContext;

/**
 * 实体 [员工类别] 存储对象
 */
public interface Hr_employee_categoryRepository extends Repository<Hr_employee_category> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_employee_category> searchDefault(Hr_employee_categorySearchContext context);

    Hr_employee_category convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee_category domain , Hr_employee_category po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee_category convert2Domain( Hr_employee_category po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee_category domain) ;

}
