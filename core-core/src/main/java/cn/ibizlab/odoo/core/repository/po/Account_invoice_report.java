package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_reportSearchContext;

/**
 * 实体 [发票统计] 存储模型
 */
public interface Account_invoice_report{

    /**
     * 发票 #
     */
    String getNumber();

    void setNumber(String number);

    /**
     * 获取 [发票 #]脏标记
     */
    boolean getNumberDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 数量
     */
    Double getProduct_qty();

    void setProduct_qty(Double product_qty);

    /**
     * 获取 [数量]脏标记
     */
    boolean getProduct_qtyDirtyFlag();

    /**
     * 当前货币不含税总计
     */
    Double getUser_currency_price_total();

    void setUser_currency_price_total(Double user_currency_price_total);

    /**
     * 获取 [当前货币不含税总计]脏标记
     */
    boolean getUser_currency_price_totalDirtyFlag();

    /**
     * 到期金额
     */
    Double getResidual();

    void setResidual(Double residual);

    /**
     * 获取 [到期金额]脏标记
     */
    boolean getResidualDirtyFlag();

    /**
     * 发票状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [发票状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 到期日期
     */
    Timestamp getDate_due();

    void setDate_due(Timestamp date_due);

    /**
     * 获取 [到期日期]脏标记
     */
    boolean getDate_dueDirtyFlag();

    /**
     * 行数
     */
    Integer getNbr();

    void setNbr(Integer nbr);

    /**
     * 获取 [行数]脏标记
     */
    boolean getNbrDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 开票日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [开票日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 总计
     */
    Double getAmount_total();

    void setAmount_total(Double amount_total);

    /**
     * 获取 [总计]脏标记
     */
    boolean getAmount_totalDirtyFlag();

    /**
     * 参考计量单位
     */
    String getUom_name();

    void setUom_name(String uom_name);

    /**
     * 获取 [参考计量单位]脏标记
     */
    boolean getUom_nameDirtyFlag();

    /**
     * 平均价格
     */
    Double getPrice_average();

    void setPrice_average(Double price_average);

    /**
     * 获取 [平均价格]脏标记
     */
    boolean getPrice_averageDirtyFlag();

    /**
     * 汇率
     */
    Double getCurrency_rate();

    void setCurrency_rate(Double currency_rate);

    /**
     * 获取 [汇率]脏标记
     */
    boolean getCurrency_rateDirtyFlag();

    /**
     * 不含税总计
     */
    Double getPrice_total();

    void setPrice_total(Double price_total);

    /**
     * 获取 [不含税总计]脏标记
     */
    boolean getPrice_totalDirtyFlag();

    /**
     * 本币平均价格
     */
    Double getUser_currency_price_average();

    void setUser_currency_price_average(Double user_currency_price_average);

    /**
     * 获取 [本币平均价格]脏标记
     */
    boolean getUser_currency_price_averageDirtyFlag();

    /**
     * 余额总计
     */
    Double getUser_currency_residual();

    void setUser_currency_residual(Double user_currency_residual);

    /**
     * 获取 [余额总计]脏标记
     */
    boolean getUser_currency_residualDirtyFlag();

    /**
     * 类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 销售团队
     */
    String getTeam_id_text();

    void setTeam_id_text(String team_id_text);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_id_textDirtyFlag();

    /**
     * 分析账户
     */
    String getAccount_analytic_id_text();

    void setAccount_analytic_id_text(String account_analytic_id_text);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_analytic_id_textDirtyFlag();

    /**
     * 产品类别
     */
    String getCateg_id_text();

    void setCateg_id_text(String categ_id_text);

    /**
     * 获取 [产品类别]脏标记
     */
    boolean getCateg_id_textDirtyFlag();

    /**
     * 日记账
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 税科目调整
     */
    String getFiscal_position_id_text();

    void setFiscal_position_id_text(String fiscal_position_id_text);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getFiscal_position_id_textDirtyFlag();

    /**
     * 发票
     */
    String getInvoice_id_text();

    void setInvoice_id_text(String invoice_id_text);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_id_textDirtyFlag();

    /**
     * 业务伙伴的国家
     */
    String getCountry_id_text();

    void setCountry_id_text(String country_id_text);

    /**
     * 获取 [业务伙伴的国家]脏标记
     */
    boolean getCountry_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 付款条款
     */
    String getPayment_term_id_text();

    void setPayment_term_id_text(String payment_term_id_text);

    /**
     * 获取 [付款条款]脏标记
     */
    boolean getPayment_term_id_textDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 收入/费用科目
     */
    String getAccount_line_id_text();

    void setAccount_line_id_text(String account_line_id_text);

    /**
     * 获取 [收入/费用科目]脏标记
     */
    boolean getAccount_line_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 业务伙伴公司
     */
    String getCommercial_partner_id_text();

    void setCommercial_partner_id_text(String commercial_partner_id_text);

    /**
     * 获取 [业务伙伴公司]脏标记
     */
    boolean getCommercial_partner_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 销售员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 收／付款账户
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [收／付款账户]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 销售员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 业务伙伴的国家
     */
    Integer getCountry_id();

    void setCountry_id(Integer country_id);

    /**
     * 获取 [业务伙伴的国家]脏标记
     */
    boolean getCountry_idDirtyFlag();

    /**
     * 银行账户
     */
    Integer getPartner_bank_id();

    void setPartner_bank_id(Integer partner_bank_id);

    /**
     * 获取 [银行账户]脏标记
     */
    boolean getPartner_bank_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 产品类别
     */
    Integer getCateg_id();

    void setCateg_id(Integer categ_id);

    /**
     * 获取 [产品类别]脏标记
     */
    boolean getCateg_idDirtyFlag();

    /**
     * 业务伙伴公司
     */
    Integer getCommercial_partner_id();

    void setCommercial_partner_id(Integer commercial_partner_id);

    /**
     * 获取 [业务伙伴公司]脏标记
     */
    boolean getCommercial_partner_idDirtyFlag();

    /**
     * 收／付款账户
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [收／付款账户]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 付款条款
     */
    Integer getPayment_term_id();

    void setPayment_term_id(Integer payment_term_id);

    /**
     * 获取 [付款条款]脏标记
     */
    boolean getPayment_term_idDirtyFlag();

    /**
     * 分析账户
     */
    Integer getAccount_analytic_id();

    void setAccount_analytic_id(Integer account_analytic_id);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_analytic_idDirtyFlag();

    /**
     * 税科目调整
     */
    Integer getFiscal_position_id();

    void setFiscal_position_id(Integer fiscal_position_id);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getFiscal_position_idDirtyFlag();

    /**
     * 日记账
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * 收入/费用科目
     */
    Integer getAccount_line_id();

    void setAccount_line_id(Integer account_line_id);

    /**
     * 获取 [收入/费用科目]脏标记
     */
    boolean getAccount_line_idDirtyFlag();

    /**
     * 销售团队
     */
    Integer getTeam_id();

    void setTeam_id(Integer team_id);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 发票
     */
    Integer getInvoice_id();

    void setInvoice_id(Integer invoice_id);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_idDirtyFlag();

}
