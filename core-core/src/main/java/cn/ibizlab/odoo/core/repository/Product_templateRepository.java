package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_template;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_templateSearchContext;

/**
 * 实体 [产品模板] 存储对象
 */
public interface Product_templateRepository extends Repository<Product_template> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_template> searchDefault(Product_templateSearchContext context);

    Product_template convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_template domain , Product_template po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_template convert2Domain( Product_template po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_template domain) ;

}
