package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_report_operatorSearchContext;

/**
 * 实体 [实时聊天支持操作员报告] 存储模型
 */
public interface Im_livechat_report_operator{

    /**
     * 该回答了
     */
    Double getTime_to_answer();

    void setTime_to_answer(Double time_to_answer);

    /**
     * 获取 [该回答了]脏标记
     */
    boolean getTime_to_answerDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 会话的开始日期
     */
    Timestamp getStart_date();

    void setStart_date(Timestamp start_date);

    /**
     * 获取 [会话的开始日期]脏标记
     */
    boolean getStart_dateDirtyFlag();

    /**
     * # 会话
     */
    Integer getNbr_channel();

    void setNbr_channel(Integer nbr_channel);

    /**
     * 获取 [# 会话]脏标记
     */
    boolean getNbr_channelDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 平均时间
     */
    Double getDuration();

    void setDuration(Double duration);

    /**
     * 获取 [平均时间]脏标记
     */
    boolean getDurationDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 渠道
     */
    String getLivechat_channel_id_text();

    void setLivechat_channel_id_text(String livechat_channel_id_text);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getLivechat_channel_id_textDirtyFlag();

    /**
     * 对话
     */
    String getChannel_id_text();

    void setChannel_id_text(String channel_id_text);

    /**
     * 获取 [对话]脏标记
     */
    boolean getChannel_id_textDirtyFlag();

    /**
     * 运算符
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [运算符]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 对话
     */
    Integer getChannel_id();

    void setChannel_id(Integer channel_id);

    /**
     * 获取 [对话]脏标记
     */
    boolean getChannel_idDirtyFlag();

    /**
     * 运算符
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [运算符]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 渠道
     */
    Integer getLivechat_channel_id();

    void setLivechat_channel_id(Integer livechat_channel_id);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getLivechat_channel_idDirtyFlag();

}
