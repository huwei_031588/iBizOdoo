package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mail_statistics_report] 对象
 */
public interface mail_statistics_report {

    public Integer getBounced();

    public void setBounced(Integer bounced);

    public String getCampaign();

    public void setCampaign(String campaign);

    public Integer getClicked();

    public void setClicked(Integer clicked);

    public Integer getDelivered();

    public void setDelivered(Integer delivered);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmail_from();

    public void setEmail_from(String email_from);

    public Integer getId();

    public void setId(Integer id);

    public String getName();

    public void setName(String name);

    public Integer getOpened();

    public void setOpened(Integer opened);

    public Integer getReplied();

    public void setReplied(Integer replied);

    public Timestamp getScheduled_date();

    public void setScheduled_date(Timestamp scheduled_date);

    public Integer getSent();

    public void setSent(Integer sent);

    public String getState();

    public void setState(String state);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
