package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_pm_meter_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_lineSearchContext;

/**
 * 实体 [History of Asset Meter Reading] 存储对象
 */
public interface Mro_pm_meter_lineRepository extends Repository<Mro_pm_meter_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_pm_meter_line> searchDefault(Mro_pm_meter_lineSearchContext context);

    Mro_pm_meter_line convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_line domain , Mro_pm_meter_line po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_line convert2Domain( Mro_pm_meter_line po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_line domain) ;

}
