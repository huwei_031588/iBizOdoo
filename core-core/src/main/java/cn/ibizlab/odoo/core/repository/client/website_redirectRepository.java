package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.website_redirect;

/**
 * 实体[website_redirect] 服务对象接口
 */
public interface website_redirectRepository{


    public website_redirect createPO() ;
        public void remove(String id);

        public void update(website_redirect website_redirect);

        public List<website_redirect> search();

        public void get(String id);

        public void createBatch(website_redirect website_redirect);

        public void updateBatch(website_redirect website_redirect);

        public void create(website_redirect website_redirect);

        public void removeBatch(String id);


}
