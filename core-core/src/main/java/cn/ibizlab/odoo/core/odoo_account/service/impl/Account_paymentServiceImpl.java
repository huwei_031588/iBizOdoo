package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_paymentSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_paymentService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_paymentOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_paymentClientModel;

/**
 * 实体[付款] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_paymentServiceImpl implements IAccount_paymentService {

    @Autowired
    account_paymentOdooClient account_paymentOdooClient;


    @Override
    public boolean update(Account_payment et) {
        account_paymentClientModel clientModel = convert2Model(et,null);
		account_paymentOdooClient.update(clientModel);
        Account_payment rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_payment> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_paymentClientModel clientModel = new account_paymentClientModel();
        clientModel.setId(id);
		account_paymentOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_payment et) {
        account_paymentClientModel clientModel = convert2Model(et,null);
		account_paymentOdooClient.create(clientModel);
        Account_payment rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_payment> list){
    }

    @Override
    public Account_payment get(Integer id) {
        account_paymentClientModel clientModel = new account_paymentClientModel();
        clientModel.setId(id);
		account_paymentOdooClient.get(clientModel);
        Account_payment et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_payment();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_payment> searchDefault(Account_paymentSearchContext context) {
        List<Account_payment> list = new ArrayList<Account_payment>();
        Page<account_paymentClientModel> clientModelList = account_paymentOdooClient.search(context);
        for(account_paymentClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_payment>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_paymentClientModel convert2Model(Account_payment domain , account_paymentClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_paymentClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("invoice_idsdirtyflag"))
                model.setInvoice_ids(domain.getInvoiceIds());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("payment_typedirtyflag"))
                model.setPayment_type(domain.getPaymentType());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("has_invoicesdirtyflag"))
                model.setHas_invoices(domain.getHasInvoices());
            if((Boolean) domain.getExtensionparams().get("payment_datedirtyflag"))
                model.setPayment_date(domain.getPaymentDate());
            if((Boolean) domain.getExtensionparams().get("multidirtyflag"))
                model.setMulti(domain.getMulti());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("move_reconcileddirtyflag"))
                model.setMove_reconciled(domain.getMoveReconciled());
            if((Boolean) domain.getExtensionparams().get("destination_account_iddirtyflag"))
                model.setDestination_account_id(domain.getDestinationAccountId());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("payment_differencedirtyflag"))
                model.setPayment_difference(domain.getPaymentDifference());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("payment_difference_handlingdirtyflag"))
                model.setPayment_difference_handling(domain.getPaymentDifferenceHandling());
            if((Boolean) domain.getExtensionparams().get("reconciled_invoice_idsdirtyflag"))
                model.setReconciled_invoice_ids(domain.getReconciledInvoiceIds());
            if((Boolean) domain.getExtensionparams().get("payment_referencedirtyflag"))
                model.setPayment_reference(domain.getPaymentReference());
            if((Boolean) domain.getExtensionparams().get("writeoff_labeldirtyflag"))
                model.setWriteoff_label(domain.getWriteoffLabel());
            if((Boolean) domain.getExtensionparams().get("move_line_idsdirtyflag"))
                model.setMove_line_ids(domain.getMoveLineIds());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("show_partner_bank_accountdirtyflag"))
                model.setShow_partner_bank_account(domain.getShowPartnerBankAccount());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("partner_typedirtyflag"))
                model.setPartner_type(domain.getPartnerType());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("communicationdirtyflag"))
                model.setCommunication(domain.getCommunication());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("hide_payment_methoddirtyflag"))
                model.setHide_payment_method(domain.getHidePaymentMethod());
            if((Boolean) domain.getExtensionparams().get("move_namedirtyflag"))
                model.setMove_name(domain.getMoveName());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("destination_journal_id_textdirtyflag"))
                model.setDestination_journal_id_text(domain.getDestinationJournalIdText());
            if((Boolean) domain.getExtensionparams().get("payment_method_codedirtyflag"))
                model.setPayment_method_code(domain.getPaymentMethodCode());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("writeoff_account_id_textdirtyflag"))
                model.setWriteoff_account_id_text(domain.getWriteoffAccountIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("payment_method_id_textdirtyflag"))
                model.setPayment_method_id_text(domain.getPaymentMethodIdText());
            if((Boolean) domain.getExtensionparams().get("payment_token_id_textdirtyflag"))
                model.setPayment_token_id_text(domain.getPaymentTokenIdText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("payment_token_iddirtyflag"))
                model.setPayment_token_id(domain.getPaymentTokenId());
            if((Boolean) domain.getExtensionparams().get("payment_transaction_iddirtyflag"))
                model.setPayment_transaction_id(domain.getPaymentTransactionId());
            if((Boolean) domain.getExtensionparams().get("writeoff_account_iddirtyflag"))
                model.setWriteoff_account_id(domain.getWriteoffAccountId());
            if((Boolean) domain.getExtensionparams().get("partner_bank_account_iddirtyflag"))
                model.setPartner_bank_account_id(domain.getPartnerBankAccountId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("payment_method_iddirtyflag"))
                model.setPayment_method_id(domain.getPaymentMethodId());
            if((Boolean) domain.getExtensionparams().get("destination_journal_iddirtyflag"))
                model.setDestination_journal_id(domain.getDestinationJournalId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_payment convert2Domain( account_paymentClientModel model ,Account_payment domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_payment();
        }

        if(model.getInvoice_idsDirtyFlag())
            domain.setInvoiceIds(model.getInvoice_ids());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getPayment_typeDirtyFlag())
            domain.setPaymentType(model.getPayment_type());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getHas_invoicesDirtyFlag())
            domain.setHasInvoices(model.getHas_invoices());
        if(model.getPayment_dateDirtyFlag())
            domain.setPaymentDate(model.getPayment_date());
        if(model.getMultiDirtyFlag())
            domain.setMulti(model.getMulti());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getMove_reconciledDirtyFlag())
            domain.setMoveReconciled(model.getMove_reconciled());
        if(model.getDestination_account_idDirtyFlag())
            domain.setDestinationAccountId(model.getDestination_account_id());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getPayment_differenceDirtyFlag())
            domain.setPaymentDifference(model.getPayment_difference());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getPayment_difference_handlingDirtyFlag())
            domain.setPaymentDifferenceHandling(model.getPayment_difference_handling());
        if(model.getReconciled_invoice_idsDirtyFlag())
            domain.setReconciledInvoiceIds(model.getReconciled_invoice_ids());
        if(model.getPayment_referenceDirtyFlag())
            domain.setPaymentReference(model.getPayment_reference());
        if(model.getWriteoff_labelDirtyFlag())
            domain.setWriteoffLabel(model.getWriteoff_label());
        if(model.getMove_line_idsDirtyFlag())
            domain.setMoveLineIds(model.getMove_line_ids());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getShow_partner_bank_accountDirtyFlag())
            domain.setShowPartnerBankAccount(model.getShow_partner_bank_account());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getPartner_typeDirtyFlag())
            domain.setPartnerType(model.getPartner_type());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCommunicationDirtyFlag())
            domain.setCommunication(model.getCommunication());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getHide_payment_methodDirtyFlag())
            domain.setHidePaymentMethod(model.getHide_payment_method());
        if(model.getMove_nameDirtyFlag())
            domain.setMoveName(model.getMove_name());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getDestination_journal_id_textDirtyFlag())
            domain.setDestinationJournalIdText(model.getDestination_journal_id_text());
        if(model.getPayment_method_codeDirtyFlag())
            domain.setPaymentMethodCode(model.getPayment_method_code());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWriteoff_account_id_textDirtyFlag())
            domain.setWriteoffAccountIdText(model.getWriteoff_account_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getPayment_method_id_textDirtyFlag())
            domain.setPaymentMethodIdText(model.getPayment_method_id_text());
        if(model.getPayment_token_id_textDirtyFlag())
            domain.setPaymentTokenIdText(model.getPayment_token_id_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPayment_token_idDirtyFlag())
            domain.setPaymentTokenId(model.getPayment_token_id());
        if(model.getPayment_transaction_idDirtyFlag())
            domain.setPaymentTransactionId(model.getPayment_transaction_id());
        if(model.getWriteoff_account_idDirtyFlag())
            domain.setWriteoffAccountId(model.getWriteoff_account_id());
        if(model.getPartner_bank_account_idDirtyFlag())
            domain.setPartnerBankAccountId(model.getPartner_bank_account_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPayment_method_idDirtyFlag())
            domain.setPaymentMethodId(model.getPayment_method_id());
        if(model.getDestination_journal_idDirtyFlag())
            domain.setDestinationJournalId(model.getDestination_journal_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        return domain ;
    }

}

    



