package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isms_send_sms;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sms_send_sms] 服务对象接口
 */
public interface Isms_send_smsClientService{

    public Isms_send_sms createModel() ;

    public void remove(Isms_send_sms sms_send_sms);

    public void updateBatch(List<Isms_send_sms> sms_send_sms);

    public void get(Isms_send_sms sms_send_sms);

    public void create(Isms_send_sms sms_send_sms);

    public Page<Isms_send_sms> search(SearchContext context);

    public void removeBatch(List<Isms_send_sms> sms_send_sms);

    public void update(Isms_send_sms sms_send_sms);

    public void createBatch(List<Isms_send_sms> sms_send_sms);

    public Page<Isms_send_sms> select(SearchContext context);

}
