package cn.ibizlab.odoo.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_teamSearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_teamService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_maintenance.client.maintenance_teamOdooClient;
import cn.ibizlab.odoo.core.odoo_maintenance.clientmodel.maintenance_teamClientModel;

/**
 * 实体[保养团队] 服务对象接口实现
 */
@Slf4j
@Service
public class Maintenance_teamServiceImpl implements IMaintenance_teamService {

    @Autowired
    maintenance_teamOdooClient maintenance_teamOdooClient;


    @Override
    public boolean remove(Integer id) {
        maintenance_teamClientModel clientModel = new maintenance_teamClientModel();
        clientModel.setId(id);
		maintenance_teamOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Maintenance_team et) {
        maintenance_teamClientModel clientModel = convert2Model(et,null);
		maintenance_teamOdooClient.update(clientModel);
        Maintenance_team rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Maintenance_team> list){
    }

    @Override
    public Maintenance_team get(Integer id) {
        maintenance_teamClientModel clientModel = new maintenance_teamClientModel();
        clientModel.setId(id);
		maintenance_teamOdooClient.get(clientModel);
        Maintenance_team et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Maintenance_team();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Maintenance_team et) {
        maintenance_teamClientModel clientModel = convert2Model(et,null);
		maintenance_teamOdooClient.create(clientModel);
        Maintenance_team rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Maintenance_team> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Maintenance_team> searchDefault(Maintenance_teamSearchContext context) {
        List<Maintenance_team> list = new ArrayList<Maintenance_team>();
        Page<maintenance_teamClientModel> clientModelList = maintenance_teamOdooClient.search(context);
        for(maintenance_teamClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Maintenance_team>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public maintenance_teamClientModel convert2Model(Maintenance_team domain , maintenance_teamClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new maintenance_teamClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("todo_request_count_blockdirtyflag"))
                model.setTodo_request_count_block(domain.getTodoRequestCountBlock());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("equipment_idsdirtyflag"))
                model.setEquipment_ids(domain.getEquipmentIds());
            if((Boolean) domain.getExtensionparams().get("member_idsdirtyflag"))
                model.setMember_ids(domain.getMemberIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("todo_request_count_high_prioritydirtyflag"))
                model.setTodo_request_count_high_priority(domain.getTodoRequestCountHighPriority());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("request_idsdirtyflag"))
                model.setRequest_ids(domain.getRequestIds());
            if((Boolean) domain.getExtensionparams().get("todo_request_countdirtyflag"))
                model.setTodo_request_count(domain.getTodoRequestCount());
            if((Boolean) domain.getExtensionparams().get("todo_request_idsdirtyflag"))
                model.setTodo_request_ids(domain.getTodoRequestIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("todo_request_count_unscheduleddirtyflag"))
                model.setTodo_request_count_unscheduled(domain.getTodoRequestCountUnscheduled());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("todo_request_count_datedirtyflag"))
                model.setTodo_request_count_date(domain.getTodoRequestCountDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Maintenance_team convert2Domain( maintenance_teamClientModel model ,Maintenance_team domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Maintenance_team();
        }

        if(model.getTodo_request_count_blockDirtyFlag())
            domain.setTodoRequestCountBlock(model.getTodo_request_count_block());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getEquipment_idsDirtyFlag())
            domain.setEquipmentIds(model.getEquipment_ids());
        if(model.getMember_idsDirtyFlag())
            domain.setMemberIds(model.getMember_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getTodo_request_count_high_priorityDirtyFlag())
            domain.setTodoRequestCountHighPriority(model.getTodo_request_count_high_priority());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getRequest_idsDirtyFlag())
            domain.setRequestIds(model.getRequest_ids());
        if(model.getTodo_request_countDirtyFlag())
            domain.setTodoRequestCount(model.getTodo_request_count());
        if(model.getTodo_request_idsDirtyFlag())
            domain.setTodoRequestIds(model.getTodo_request_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getTodo_request_count_unscheduledDirtyFlag())
            domain.setTodoRequestCountUnscheduled(model.getTodo_request_count_unscheduled());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getTodo_request_count_dateDirtyFlag())
            domain.setTodoRequestCountDate(model.getTodo_request_count_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



