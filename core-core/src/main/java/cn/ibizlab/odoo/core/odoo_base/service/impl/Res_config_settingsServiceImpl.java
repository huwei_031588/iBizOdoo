package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_settingsSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_config_settingsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_config_settingsOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_config_settingsClientModel;

/**
 * 实体[配置设定] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_config_settingsServiceImpl implements IRes_config_settingsService {

    @Autowired
    res_config_settingsOdooClient res_config_settingsOdooClient;


    @Override
    public boolean remove(Integer id) {
        res_config_settingsClientModel clientModel = new res_config_settingsClientModel();
        clientModel.setId(id);
		res_config_settingsOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Res_config_settings get(Integer id) {
        res_config_settingsClientModel clientModel = new res_config_settingsClientModel();
        clientModel.setId(id);
		res_config_settingsOdooClient.get(clientModel);
        Res_config_settings et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_config_settings();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Res_config_settings et) {
        res_config_settingsClientModel clientModel = convert2Model(et,null);
		res_config_settingsOdooClient.update(clientModel);
        Res_config_settings rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_config_settings> list){
    }

    @Override
    public boolean create(Res_config_settings et) {
        res_config_settingsClientModel clientModel = convert2Model(et,null);
		res_config_settingsOdooClient.create(clientModel);
        Res_config_settings rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_config_settings> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_config_settings> searchDefault(Res_config_settingsSearchContext context) {
        List<Res_config_settings> list = new ArrayList<Res_config_settings>();
        Page<res_config_settingsClientModel> clientModelList = res_config_settingsOdooClient.search(context);
        for(res_config_settingsClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_config_settings>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_config_settingsClientModel convert2Model(Res_config_settings domain , res_config_settingsClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_config_settingsClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("module_account_bank_statement_import_qifdirtyflag"))
                model.setModule_account_bank_statement_import_qif(domain.getModuleAccountBankStatementImportQif());
            if((Boolean) domain.getExtensionparams().get("module_account_bank_statement_import_ofxdirtyflag"))
                model.setModule_account_bank_statement_import_ofx(domain.getModuleAccountBankStatementImportOfx());
            if((Boolean) domain.getExtensionparams().get("has_google_mapsdirtyflag"))
                model.setHas_google_maps(domain.getHasGoogleMaps());
            if((Boolean) domain.getExtensionparams().get("module_l10n_eu_servicedirtyflag"))
                model.setModule_l10n_eu_service(domain.getModuleL10nEuService());
            if((Boolean) domain.getExtensionparams().get("cdn_activateddirtyflag"))
                model.setCdn_activated(domain.getCdnActivated());
            if((Boolean) domain.getExtensionparams().get("website_default_lang_codedirtyflag"))
                model.setWebsite_default_lang_code(domain.getWebsiteDefaultLangCode());
            if((Boolean) domain.getExtensionparams().get("auth_signup_reset_passworddirtyflag"))
                model.setAuth_signup_reset_password(domain.getAuthSignupResetPassword());
            if((Boolean) domain.getExtensionparams().get("website_country_group_idsdirtyflag"))
                model.setWebsite_country_group_ids(domain.getWebsiteCountryGroupIds());
            if((Boolean) domain.getExtensionparams().get("social_instagramdirtyflag"))
                model.setSocial_instagram(domain.getSocialInstagram());
            if((Boolean) domain.getExtensionparams().get("module_purchase_requisitiondirtyflag"))
                model.setModule_purchase_requisition(domain.getModulePurchaseRequisition());
            if((Boolean) domain.getExtensionparams().get("module_delivery_easypostdirtyflag"))
                model.setModule_delivery_easypost(domain.getModuleDeliveryEasypost());
            if((Boolean) domain.getExtensionparams().get("group_discount_per_so_linedirtyflag"))
                model.setGroup_discount_per_so_line(domain.getGroupDiscountPerSoLine());
            if((Boolean) domain.getExtensionparams().get("automatic_invoicedirtyflag"))
                model.setAutomatic_invoice(domain.getAutomaticInvoice());
            if((Boolean) domain.getExtensionparams().get("module_account_plaiddirtyflag"))
                model.setModule_account_plaid(domain.getModuleAccountPlaid());
            if((Boolean) domain.getExtensionparams().get("google_management_client_iddirtyflag"))
                model.setGoogle_management_client_id(domain.getGoogleManagementClientId());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("module_account_check_printingdirtyflag"))
                model.setModule_account_check_printing(domain.getModuleAccountCheckPrinting());
            if((Boolean) domain.getExtensionparams().get("module_partner_autocompletedirtyflag"))
                model.setModule_partner_autocomplete(domain.getModulePartnerAutocomplete());
            if((Boolean) domain.getExtensionparams().get("language_countdirtyflag"))
                model.setLanguage_count(domain.getLanguageCount());
            if((Boolean) domain.getExtensionparams().get("module_website_linksdirtyflag"))
                model.setModule_website_links(domain.getModuleWebsiteLinks());
            if((Boolean) domain.getExtensionparams().get("website_form_enable_metadatadirtyflag"))
                model.setWebsite_form_enable_metadata(domain.getWebsiteFormEnableMetadata());
            if((Boolean) domain.getExtensionparams().get("has_social_networkdirtyflag"))
                model.setHas_social_network(domain.getHasSocialNetwork());
            if((Boolean) domain.getExtensionparams().get("group_sale_delivery_addressdirtyflag"))
                model.setGroup_sale_delivery_address(domain.getGroupSaleDeliveryAddress());
            if((Boolean) domain.getExtensionparams().get("google_analytics_keydirtyflag"))
                model.setGoogle_analytics_key(domain.getGoogleAnalyticsKey());
            if((Boolean) domain.getExtensionparams().get("module_auth_ldapdirtyflag"))
                model.setModule_auth_ldap(domain.getModuleAuthLdap());
            if((Boolean) domain.getExtensionparams().get("specific_user_accountdirtyflag"))
                model.setSpecific_user_account(domain.getSpecificUserAccount());
            if((Boolean) domain.getExtensionparams().get("module_website_hr_recruitmentdirtyflag"))
                model.setModule_website_hr_recruitment(domain.getModuleWebsiteHrRecruitment());
            if((Boolean) domain.getExtensionparams().get("module_project_forecastdirtyflag"))
                model.setModule_project_forecast(domain.getModuleProjectForecast());
            if((Boolean) domain.getExtensionparams().get("group_stock_tracking_ownerdirtyflag"))
                model.setGroup_stock_tracking_owner(domain.getGroupStockTrackingOwner());
            if((Boolean) domain.getExtensionparams().get("module_google_calendardirtyflag"))
                model.setModule_google_calendar(domain.getModuleGoogleCalendar());
            if((Boolean) domain.getExtensionparams().get("module_accountdirtyflag"))
                model.setModule_account(domain.getModuleAccount());
            if((Boolean) domain.getExtensionparams().get("module_google_drivedirtyflag"))
                model.setModule_google_drive(domain.getModuleGoogleDrive());
            if((Boolean) domain.getExtensionparams().get("pos_pricelist_settingdirtyflag"))
                model.setPos_pricelist_setting(domain.getPosPricelistSetting());
            if((Boolean) domain.getExtensionparams().get("company_share_partnerdirtyflag"))
                model.setCompany_share_partner(domain.getCompanySharePartner());
            if((Boolean) domain.getExtensionparams().get("module_currency_rate_livedirtyflag"))
                model.setModule_currency_rate_live(domain.getModuleCurrencyRateLive());
            if((Boolean) domain.getExtensionparams().get("group_proforma_salesdirtyflag"))
                model.setGroup_proforma_sales(domain.getGroupProformaSales());
            if((Boolean) domain.getExtensionparams().get("module_delivery_fedexdirtyflag"))
                model.setModule_delivery_fedex(domain.getModuleDeliveryFedex());
            if((Boolean) domain.getExtensionparams().get("module_product_email_templatedirtyflag"))
                model.setModule_product_email_template(domain.getModuleProductEmailTemplate());
            if((Boolean) domain.getExtensionparams().get("show_effectdirtyflag"))
                model.setShow_effect(domain.getShowEffect());
            if((Boolean) domain.getExtensionparams().get("default_picking_policydirtyflag"))
                model.setDefault_picking_policy(domain.getDefaultPickingPolicy());
            if((Boolean) domain.getExtensionparams().get("social_youtubedirtyflag"))
                model.setSocial_youtube(domain.getSocialYoutube());
            if((Boolean) domain.getExtensionparams().get("website_company_iddirtyflag"))
                model.setWebsite_company_id(domain.getWebsiteCompanyId());
            if((Boolean) domain.getExtensionparams().get("module_mrp_byproductdirtyflag"))
                model.setModule_mrp_byproduct(domain.getModuleMrpByproduct());
            if((Boolean) domain.getExtensionparams().get("module_delivery_uspsdirtyflag"))
                model.setModule_delivery_usps(domain.getModuleDeliveryUsps());
            if((Boolean) domain.getExtensionparams().get("module_delivery_dhldirtyflag"))
                model.setModule_delivery_dhl(domain.getModuleDeliveryDhl());
            if((Boolean) domain.getExtensionparams().get("group_project_ratingdirtyflag"))
                model.setGroup_project_rating(domain.getGroupProjectRating());
            if((Boolean) domain.getExtensionparams().get("google_maps_api_keydirtyflag"))
                model.setGoogle_maps_api_key(domain.getGoogleMapsApiKey());
            if((Boolean) domain.getExtensionparams().get("group_use_leaddirtyflag"))
                model.setGroup_use_lead(domain.getGroupUseLead());
            if((Boolean) domain.getExtensionparams().get("group_stock_tracking_lotdirtyflag"))
                model.setGroup_stock_tracking_lot(domain.getGroupStockTrackingLot());
            if((Boolean) domain.getExtensionparams().get("group_stock_adv_locationdirtyflag"))
                model.setGroup_stock_adv_location(domain.getGroupStockAdvLocation());
            if((Boolean) domain.getExtensionparams().get("pos_sales_pricedirtyflag"))
                model.setPos_sales_price(domain.getPosSalesPrice());
            if((Boolean) domain.getExtensionparams().get("has_chart_of_accountsdirtyflag"))
                model.setHas_chart_of_accounts(domain.getHasChartOfAccounts());
            if((Boolean) domain.getExtensionparams().get("multi_sales_price_methoddirtyflag"))
                model.setMulti_sales_price_method(domain.getMultiSalesPriceMethod());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("module_procurement_jitdirtyflag"))
                model.setModule_procurement_jit(domain.getModuleProcurementJit());
            if((Boolean) domain.getExtensionparams().get("module_account_assetdirtyflag"))
                model.setModule_account_asset(domain.getModuleAccountAsset());
            if((Boolean) domain.getExtensionparams().get("use_mailgatewaydirtyflag"))
                model.setUse_mailgateway(domain.getUseMailgateway());
            if((Boolean) domain.getExtensionparams().get("group_sale_pricelistdirtyflag"))
                model.setGroup_sale_pricelist(domain.getGroupSalePricelist());
            if((Boolean) domain.getExtensionparams().get("crm_default_team_iddirtyflag"))
                model.setCrm_default_team_id(domain.getCrmDefaultTeamId());
            if((Boolean) domain.getExtensionparams().get("group_stock_multi_locationsdirtyflag"))
                model.setGroup_stock_multi_locations(domain.getGroupStockMultiLocations());
            if((Boolean) domain.getExtensionparams().get("use_manufacturing_leaddirtyflag"))
                model.setUse_manufacturing_lead(domain.getUseManufacturingLead());
            if((Boolean) domain.getExtensionparams().get("module_google_spreadsheetdirtyflag"))
                model.setModule_google_spreadsheet(domain.getModuleGoogleSpreadsheet());
            if((Boolean) domain.getExtensionparams().get("show_line_subtotals_tax_selectiondirtyflag"))
                model.setShow_line_subtotals_tax_selection(domain.getShowLineSubtotalsTaxSelection());
            if((Boolean) domain.getExtensionparams().get("language_idsdirtyflag"))
                model.setLanguage_ids(domain.getLanguageIds());
            if((Boolean) domain.getExtensionparams().get("module_website_sale_deliverydirtyflag"))
                model.setModule_website_sale_delivery(domain.getModuleWebsiteSaleDelivery());
            if((Boolean) domain.getExtensionparams().get("module_mrp_mpsdirtyflag"))
                model.setModule_mrp_mps(domain.getModuleMrpMps());
            if((Boolean) domain.getExtensionparams().get("partner_autocomplete_insufficient_creditdirtyflag"))
                model.setPartner_autocomplete_insufficient_credit(domain.getPartnerAutocompleteInsufficientCredit());
            if((Boolean) domain.getExtensionparams().get("module_hr_org_chartdirtyflag"))
                model.setModule_hr_org_chart(domain.getModuleHrOrgChart());
            if((Boolean) domain.getExtensionparams().get("module_product_expirydirtyflag"))
                model.setModule_product_expiry(domain.getModuleProductExpiry());
            if((Boolean) domain.getExtensionparams().get("module_delivery_bpostdirtyflag"))
                model.setModule_delivery_bpost(domain.getModuleDeliveryBpost());
            if((Boolean) domain.getExtensionparams().get("module_stock_barcodedirtyflag"))
                model.setModule_stock_barcode(domain.getModuleStockBarcode());
            if((Boolean) domain.getExtensionparams().get("social_githubdirtyflag"))
                model.setSocial_github(domain.getSocialGithub());
            if((Boolean) domain.getExtensionparams().get("module_account_intrastatdirtyflag"))
                model.setModule_account_intrastat(domain.getModuleAccountIntrastat());
            if((Boolean) domain.getExtensionparams().get("auto_done_settingdirtyflag"))
                model.setAuto_done_setting(domain.getAutoDoneSetting());
            if((Boolean) domain.getExtensionparams().get("auth_signup_uninviteddirtyflag"))
                model.setAuth_signup_uninvited(domain.getAuthSignupUninvited());
            if((Boolean) domain.getExtensionparams().get("company_share_productdirtyflag"))
                model.setCompany_share_product(domain.getCompanyShareProduct());
            if((Boolean) domain.getExtensionparams().get("group_sale_order_templatedirtyflag"))
                model.setGroup_sale_order_template(domain.getGroupSaleOrderTemplate());
            if((Boolean) domain.getExtensionparams().get("module_account_sepa_direct_debitdirtyflag"))
                model.setModule_account_sepa_direct_debit(domain.getModuleAccountSepaDirectDebit());
            if((Boolean) domain.getExtensionparams().get("use_quotation_validity_daysdirtyflag"))
                model.setUse_quotation_validity_days(domain.getUseQuotationValidityDays());
            if((Boolean) domain.getExtensionparams().get("module_account_reports_followupdirtyflag"))
                model.setModule_account_reports_followup(domain.getModuleAccountReportsFollowup());
            if((Boolean) domain.getExtensionparams().get("module_account_batch_paymentdirtyflag"))
                model.setModule_account_batch_payment(domain.getModuleAccountBatchPayment());
            if((Boolean) domain.getExtensionparams().get("social_twitterdirtyflag"))
                model.setSocial_twitter(domain.getSocialTwitter());
            if((Boolean) domain.getExtensionparams().get("module_account_budgetdirtyflag"))
                model.setModule_account_budget(domain.getModuleAccountBudget());
            if((Boolean) domain.getExtensionparams().get("group_mrp_routingsdirtyflag"))
                model.setGroup_mrp_routings(domain.getGroupMrpRoutings());
            if((Boolean) domain.getExtensionparams().get("group_cash_roundingdirtyflag"))
                model.setGroup_cash_rounding(domain.getGroupCashRounding());
            if((Boolean) domain.getExtensionparams().get("module_stock_landed_costsdirtyflag"))
                model.setModule_stock_landed_costs(domain.getModuleStockLandedCosts());
            if((Boolean) domain.getExtensionparams().get("website_namedirtyflag"))
                model.setWebsite_name(domain.getWebsiteName());
            if((Boolean) domain.getExtensionparams().get("module_website_sale_stockdirtyflag"))
                model.setModule_website_sale_stock(domain.getModuleWebsiteSaleStock());
            if((Boolean) domain.getExtensionparams().get("group_website_popup_on_exitdirtyflag"))
                model.setGroup_website_popup_on_exit(domain.getGroupWebsitePopupOnExit());
            if((Boolean) domain.getExtensionparams().get("module_website_event_trackdirtyflag"))
                model.setModule_website_event_track(domain.getModuleWebsiteEventTrack());
            if((Boolean) domain.getExtensionparams().get("group_manage_vendor_pricedirtyflag"))
                model.setGroup_manage_vendor_price(domain.getGroupManageVendorPrice());
            if((Boolean) domain.getExtensionparams().get("module_deliverydirtyflag"))
                model.setModule_delivery(domain.getModuleDelivery());
            if((Boolean) domain.getExtensionparams().get("module_account_invoice_extractdirtyflag"))
                model.setModule_account_invoice_extract(domain.getModuleAccountInvoiceExtract());
            if((Boolean) domain.getExtensionparams().get("channel_iddirtyflag"))
                model.setChannel_id(domain.getChannelId());
            if((Boolean) domain.getExtensionparams().get("group_warning_saledirtyflag"))
                model.setGroup_warning_sale(domain.getGroupWarningSale());
            if((Boolean) domain.getExtensionparams().get("cdn_urldirtyflag"))
                model.setCdn_url(domain.getCdnUrl());
            if((Boolean) domain.getExtensionparams().get("module_event_barcodedirtyflag"))
                model.setModule_event_barcode(domain.getModuleEventBarcode());
            if((Boolean) domain.getExtensionparams().get("alias_domaindirtyflag"))
                model.setAlias_domain(domain.getAliasDomain());
            if((Boolean) domain.getExtensionparams().get("social_linkedindirtyflag"))
                model.setSocial_linkedin(domain.getSocialLinkedin());
            if((Boolean) domain.getExtensionparams().get("group_stock_multi_warehousesdirtyflag"))
                model.setGroup_stock_multi_warehouses(domain.getGroupStockMultiWarehouses());
            if((Boolean) domain.getExtensionparams().get("salesperson_iddirtyflag"))
                model.setSalesperson_id(domain.getSalespersonId());
            if((Boolean) domain.getExtensionparams().get("module_account_reportsdirtyflag"))
                model.setModule_account_reports(domain.getModuleAccountReports());
            if((Boolean) domain.getExtensionparams().get("group_product_pricelistdirtyflag"))
                model.setGroup_product_pricelist(domain.getGroupProductPricelist());
            if((Boolean) domain.getExtensionparams().get("module_crm_phone_validationdirtyflag"))
                model.setModule_crm_phone_validation(domain.getModuleCrmPhoneValidation());
            if((Boolean) domain.getExtensionparams().get("module_website_versiondirtyflag"))
                model.setModule_website_version(domain.getModuleWebsiteVersion());
            if((Boolean) domain.getExtensionparams().get("module_base_importdirtyflag"))
                model.setModule_base_import(domain.getModuleBaseImport());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("module_account_bank_statement_import_csvdirtyflag"))
                model.setModule_account_bank_statement_import_csv(domain.getModuleAccountBankStatementImportCsv());
            if((Boolean) domain.getExtensionparams().get("module_account_taxclouddirtyflag"))
                model.setModule_account_taxcloud(domain.getModuleAccountTaxcloud());
            if((Boolean) domain.getExtensionparams().get("use_sale_notedirtyflag"))
                model.setUse_sale_note(domain.getUseSaleNote());
            if((Boolean) domain.getExtensionparams().get("cart_abandoned_delaydirtyflag"))
                model.setCart_abandoned_delay(domain.getCartAbandonedDelay());
            if((Boolean) domain.getExtensionparams().get("website_domaindirtyflag"))
                model.setWebsite_domain(domain.getWebsiteDomain());
            if((Boolean) domain.getExtensionparams().get("module_account_accountantdirtyflag"))
                model.setModule_account_accountant(domain.getModuleAccountAccountant());
            if((Boolean) domain.getExtensionparams().get("module_sale_margindirtyflag"))
                model.setModule_sale_margin(domain.getModuleSaleMargin());
            if((Boolean) domain.getExtensionparams().get("digest_emailsdirtyflag"))
                model.setDigest_emails(domain.getDigestEmails());
            if((Boolean) domain.getExtensionparams().get("module_paddirtyflag"))
                model.setModule_pad(domain.getModulePad());
            if((Boolean) domain.getExtensionparams().get("group_warning_accountdirtyflag"))
                model.setGroup_warning_account(domain.getGroupWarningAccount());
            if((Boolean) domain.getExtensionparams().get("group_display_incotermdirtyflag"))
                model.setGroup_display_incoterm(domain.getGroupDisplayIncoterm());
            if((Boolean) domain.getExtensionparams().get("module_website_sale_wishlistdirtyflag"))
                model.setModule_website_sale_wishlist(domain.getModuleWebsiteSaleWishlist());
            if((Boolean) domain.getExtensionparams().get("user_default_rightsdirtyflag"))
                model.setUser_default_rights(domain.getUserDefaultRights());
            if((Boolean) domain.getExtensionparams().get("default_purchase_methoddirtyflag"))
                model.setDefault_purchase_method(domain.getDefaultPurchaseMethod());
            if((Boolean) domain.getExtensionparams().get("group_delivery_invoice_addressdirtyflag"))
                model.setGroup_delivery_invoice_address(domain.getGroupDeliveryInvoiceAddress());
            if((Boolean) domain.getExtensionparams().get("group_lot_on_delivery_slipdirtyflag"))
                model.setGroup_lot_on_delivery_slip(domain.getGroupLotOnDeliverySlip());
            if((Boolean) domain.getExtensionparams().get("module_event_saledirtyflag"))
                model.setModule_event_sale(domain.getModuleEventSale());
            if((Boolean) domain.getExtensionparams().get("group_show_line_subtotals_tax_includeddirtyflag"))
                model.setGroup_show_line_subtotals_tax_included(domain.getGroupShowLineSubtotalsTaxIncluded());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("group_product_variantdirtyflag"))
                model.setGroup_product_variant(domain.getGroupProductVariant());
            if((Boolean) domain.getExtensionparams().get("module_account_sepadirtyflag"))
                model.setModule_account_sepa(domain.getModuleAccountSepa());
            if((Boolean) domain.getExtensionparams().get("group_multi_currencydirtyflag"))
                model.setGroup_multi_currency(domain.getGroupMultiCurrency());
            if((Boolean) domain.getExtensionparams().get("group_products_in_billsdirtyflag"))
                model.setGroup_products_in_bills(domain.getGroupProductsInBills());
            if((Boolean) domain.getExtensionparams().get("group_analytic_accountingdirtyflag"))
                model.setGroup_analytic_accounting(domain.getGroupAnalyticAccounting());
            if((Boolean) domain.getExtensionparams().get("group_stock_packagingdirtyflag"))
                model.setGroup_stock_packaging(domain.getGroupStockPackaging());
            if((Boolean) domain.getExtensionparams().get("website_slide_google_app_keydirtyflag"))
                model.setWebsite_slide_google_app_key(domain.getWebsiteSlideGoogleAppKey());
            if((Boolean) domain.getExtensionparams().get("po_order_approvaldirtyflag"))
                model.setPo_order_approval(domain.getPoOrderApproval());
            if((Boolean) domain.getExtensionparams().get("is_installed_saledirtyflag"))
                model.setIs_installed_sale(domain.getIsInstalledSale());
            if((Boolean) domain.getExtensionparams().get("module_account_paymentdirtyflag"))
                model.setModule_account_payment(domain.getModuleAccountPayment());
            if((Boolean) domain.getExtensionparams().get("group_analytic_tagsdirtyflag"))
                model.setGroup_analytic_tags(domain.getGroupAnalyticTags());
            if((Boolean) domain.getExtensionparams().get("group_sale_order_datesdirtyflag"))
                model.setGroup_sale_order_dates(domain.getGroupSaleOrderDates());
            if((Boolean) domain.getExtensionparams().get("module_voipdirtyflag"))
                model.setModule_voip(domain.getModuleVoip());
            if((Boolean) domain.getExtensionparams().get("cart_recovery_mail_templatedirtyflag"))
                model.setCart_recovery_mail_template(domain.getCartRecoveryMailTemplate());
            if((Boolean) domain.getExtensionparams().get("group_multi_websitedirtyflag"))
                model.setGroup_multi_website(domain.getGroupMultiWebsite());
            if((Boolean) domain.getExtensionparams().get("module_auth_oauthdirtyflag"))
                model.setModule_auth_oauth(domain.getModuleAuthOauth());
            if((Boolean) domain.getExtensionparams().get("sale_delivery_settingsdirtyflag"))
                model.setSale_delivery_settings(domain.getSaleDeliverySettings());
            if((Boolean) domain.getExtensionparams().get("module_sale_quotation_builderdirtyflag"))
                model.setModule_sale_quotation_builder(domain.getModuleSaleQuotationBuilder());
            if((Boolean) domain.getExtensionparams().get("module_inter_company_rulesdirtyflag"))
                model.setModule_inter_company_rules(domain.getModuleInterCompanyRules());
            if((Boolean) domain.getExtensionparams().get("use_security_leaddirtyflag"))
                model.setUse_security_lead(domain.getUseSecurityLead());
            if((Boolean) domain.getExtensionparams().get("default_invoice_policydirtyflag"))
                model.setDefault_invoice_policy(domain.getDefaultInvoicePolicy());
            if((Boolean) domain.getExtensionparams().get("multi_sales_pricedirtyflag"))
                model.setMulti_sales_price(domain.getMultiSalesPrice());
            if((Boolean) domain.getExtensionparams().get("lock_confirmed_podirtyflag"))
                model.setLock_confirmed_po(domain.getLockConfirmedPo());
            if((Boolean) domain.getExtensionparams().get("product_weight_in_lbsdirtyflag"))
                model.setProduct_weight_in_lbs(domain.getProductWeightInLbs());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("has_google_analytics_dashboarddirtyflag"))
                model.setHas_google_analytics_dashboard(domain.getHasGoogleAnalyticsDashboard());
            if((Boolean) domain.getExtensionparams().get("module_stock_picking_batchdirtyflag"))
                model.setModule_stock_picking_batch(domain.getModuleStockPickingBatch());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("expense_alias_prefixdirtyflag"))
                model.setExpense_alias_prefix(domain.getExpenseAliasPrefix());
            if((Boolean) domain.getExtensionparams().get("module_account_deferred_revenuedirtyflag"))
                model.setModule_account_deferred_revenue(domain.getModuleAccountDeferredRevenue());
            if((Boolean) domain.getExtensionparams().get("social_facebookdirtyflag"))
                model.setSocial_facebook(domain.getSocialFacebook());
            if((Boolean) domain.getExtensionparams().get("module_web_unsplashdirtyflag"))
                model.setModule_web_unsplash(domain.getModuleWebUnsplash());
            if((Boolean) domain.getExtensionparams().get("group_mass_mailing_campaigndirtyflag"))
                model.setGroup_mass_mailing_campaign(domain.getGroupMassMailingCampaign());
            if((Boolean) domain.getExtensionparams().get("module_account_bank_statement_import_camtdirtyflag"))
                model.setModule_account_bank_statement_import_camt(domain.getModuleAccountBankStatementImportCamt());
            if((Boolean) domain.getExtensionparams().get("module_product_margindirtyflag"))
                model.setModule_product_margin(domain.getModuleProductMargin());
            if((Boolean) domain.getExtensionparams().get("group_subtask_projectdirtyflag"))
                model.setGroup_subtask_project(domain.getGroupSubtaskProject());
            if((Boolean) domain.getExtensionparams().get("module_account_3way_matchdirtyflag"))
                model.setModule_account_3way_match(domain.getModuleAccount3wayMatch());
            if((Boolean) domain.getExtensionparams().get("module_website_sale_digitaldirtyflag"))
                model.setModule_website_sale_digital(domain.getModuleWebsiteSaleDigital());
            if((Boolean) domain.getExtensionparams().get("module_sale_coupondirtyflag"))
                model.setModule_sale_coupon(domain.getModuleSaleCoupon());
            if((Boolean) domain.getExtensionparams().get("show_blacklist_buttonsdirtyflag"))
                model.setShow_blacklist_buttons(domain.getShowBlacklistButtons());
            if((Boolean) domain.getExtensionparams().get("group_warning_stockdirtyflag"))
                model.setGroup_warning_stock(domain.getGroupWarningStock());
            if((Boolean) domain.getExtensionparams().get("group_multi_companydirtyflag"))
                model.setGroup_multi_company(domain.getGroupMultiCompany());
            if((Boolean) domain.getExtensionparams().get("group_attendance_use_pindirtyflag"))
                model.setGroup_attendance_use_pin(domain.getGroupAttendanceUsePin());
            if((Boolean) domain.getExtensionparams().get("salesteam_iddirtyflag"))
                model.setSalesteam_id(domain.getSalesteamId());
            if((Boolean) domain.getExtensionparams().get("favicondirtyflag"))
                model.setFavicon(domain.getFavicon());
            if((Boolean) domain.getExtensionparams().get("module_website_sale_comparisondirtyflag"))
                model.setModule_website_sale_comparison(domain.getModuleWebsiteSaleComparison());
            if((Boolean) domain.getExtensionparams().get("available_thresholddirtyflag"))
                model.setAvailable_threshold(domain.getAvailableThreshold());
            if((Boolean) domain.getExtensionparams().get("use_po_leaddirtyflag"))
                model.setUse_po_lead(domain.getUsePoLead());
            if((Boolean) domain.getExtensionparams().get("group_fiscal_yeardirtyflag"))
                model.setGroup_fiscal_year(domain.getGroupFiscalYear());
            if((Boolean) domain.getExtensionparams().get("module_hr_timesheetdirtyflag"))
                model.setModule_hr_timesheet(domain.getModuleHrTimesheet());
            if((Boolean) domain.getExtensionparams().get("module_mrp_plmdirtyflag"))
                model.setModule_mrp_plm(domain.getModuleMrpPlm());
            if((Boolean) domain.getExtensionparams().get("module_account_yodleedirtyflag"))
                model.setModule_account_yodlee(domain.getModuleAccountYodlee());
            if((Boolean) domain.getExtensionparams().get("cdn_filtersdirtyflag"))
                model.setCdn_filters(domain.getCdnFilters());
            if((Boolean) domain.getExtensionparams().get("use_propagation_minimum_deltadirtyflag"))
                model.setUse_propagation_minimum_delta(domain.getUsePropagationMinimumDelta());
            if((Boolean) domain.getExtensionparams().get("module_delivery_upsdirtyflag"))
                model.setModule_delivery_ups(domain.getModuleDeliveryUps());
            if((Boolean) domain.getExtensionparams().get("fail_counterdirtyflag"))
                model.setFail_counter(domain.getFailCounter());
            if((Boolean) domain.getExtensionparams().get("group_stock_production_lotdirtyflag"))
                model.setGroup_stock_production_lot(domain.getGroupStockProductionLot());
            if((Boolean) domain.getExtensionparams().get("has_accounting_entriesdirtyflag"))
                model.setHas_accounting_entries(domain.getHasAccountingEntries());
            if((Boolean) domain.getExtensionparams().get("group_uomdirtyflag"))
                model.setGroup_uom(domain.getGroupUom());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_outgoing_mail_serverdirtyflag"))
                model.setMass_mailing_outgoing_mail_server(domain.getMassMailingOutgoingMailServer());
            if((Boolean) domain.getExtensionparams().get("crm_alias_prefixdirtyflag"))
                model.setCrm_alias_prefix(domain.getCrmAliasPrefix());
            if((Boolean) domain.getExtensionparams().get("social_googleplusdirtyflag"))
                model.setSocial_googleplus(domain.getSocialGoogleplus());
            if((Boolean) domain.getExtensionparams().get("group_route_so_linesdirtyflag"))
                model.setGroup_route_so_lines(domain.getGroupRouteSoLines());
            if((Boolean) domain.getExtensionparams().get("google_management_client_secretdirtyflag"))
                model.setGoogle_management_client_secret(domain.getGoogleManagementClientSecret());
            if((Boolean) domain.getExtensionparams().get("social_default_imagedirtyflag"))
                model.setSocial_default_image(domain.getSocialDefaultImage());
            if((Boolean) domain.getExtensionparams().get("has_google_analyticsdirtyflag"))
                model.setHas_google_analytics(domain.getHasGoogleAnalytics());
            if((Boolean) domain.getExtensionparams().get("module_website_event_questionsdirtyflag"))
                model.setModule_website_event_questions(domain.getModuleWebsiteEventQuestions());
            if((Boolean) domain.getExtensionparams().get("website_default_lang_iddirtyflag"))
                model.setWebsite_default_lang_id(domain.getWebsiteDefaultLangId());
            if((Boolean) domain.getExtensionparams().get("inventory_availabilitydirtyflag"))
                model.setInventory_availability(domain.getInventoryAvailability());
            if((Boolean) domain.getExtensionparams().get("group_warning_purchasedirtyflag"))
                model.setGroup_warning_purchase(domain.getGroupWarningPurchase());
            if((Boolean) domain.getExtensionparams().get("module_quality_controldirtyflag"))
                model.setModule_quality_control(domain.getModuleQualityControl());
            if((Boolean) domain.getExtensionparams().get("generate_lead_from_aliasdirtyflag"))
                model.setGenerate_lead_from_alias(domain.getGenerateLeadFromAlias());
            if((Boolean) domain.getExtensionparams().get("sale_pricelist_settingdirtyflag"))
                model.setSale_pricelist_setting(domain.getSalePricelistSetting());
            if((Boolean) domain.getExtensionparams().get("group_pricelist_itemdirtyflag"))
                model.setGroup_pricelist_item(domain.getGroupPricelistItem());
            if((Boolean) domain.getExtensionparams().get("external_email_server_defaultdirtyflag"))
                model.setExternal_email_server_default(domain.getExternalEmailServerDefault());
            if((Boolean) domain.getExtensionparams().get("unsplash_access_keydirtyflag"))
                model.setUnsplash_access_key(domain.getUnsplashAccessKey());
            if((Boolean) domain.getExtensionparams().get("module_base_gengodirtyflag"))
                model.setModule_base_gengo(domain.getModuleBaseGengo());
            if((Boolean) domain.getExtensionparams().get("module_website_event_saledirtyflag"))
                model.setModule_website_event_sale(domain.getModuleWebsiteEventSale());
            if((Boolean) domain.getExtensionparams().get("crm_default_user_iddirtyflag"))
                model.setCrm_default_user_id(domain.getCrmDefaultUserId());
            if((Boolean) domain.getExtensionparams().get("module_stock_dropshippingdirtyflag"))
                model.setModule_stock_dropshipping(domain.getModuleStockDropshipping());
            if((Boolean) domain.getExtensionparams().get("module_crm_revealdirtyflag"))
                model.setModule_crm_reveal(domain.getModuleCrmReveal());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_mail_server_iddirtyflag"))
                model.setMass_mailing_mail_server_id(domain.getMassMailingMailServerId());
            if((Boolean) domain.getExtensionparams().get("group_show_line_subtotals_tax_excludeddirtyflag"))
                model.setGroup_show_line_subtotals_tax_excluded(domain.getGroupShowLineSubtotalsTaxExcluded());
            if((Boolean) domain.getExtensionparams().get("module_hr_recruitment_surveydirtyflag"))
                model.setModule_hr_recruitment_survey(domain.getModuleHrRecruitmentSurvey());
            if((Boolean) domain.getExtensionparams().get("module_mrp_workorderdirtyflag"))
                model.setModule_mrp_workorder(domain.getModuleMrpWorkorder());
            if((Boolean) domain.getExtensionparams().get("module_pos_mercurydirtyflag"))
                model.setModule_pos_mercury(domain.getModulePosMercury());
            if((Boolean) domain.getExtensionparams().get("invoice_reference_typedirtyflag"))
                model.setInvoice_reference_type(domain.getInvoiceReferenceType());
            if((Boolean) domain.getExtensionparams().get("company_currency_iddirtyflag"))
                model.setCompany_currency_id(domain.getCompanyCurrencyId());
            if((Boolean) domain.getExtensionparams().get("tax_exigibilitydirtyflag"))
                model.setTax_exigibility(domain.getTaxExigibility());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("account_bank_reconciliation_startdirtyflag"))
                model.setAccount_bank_reconciliation_start(domain.getAccountBankReconciliationStart());
            if((Boolean) domain.getExtensionparams().get("tax_cash_basis_journal_iddirtyflag"))
                model.setTax_cash_basis_journal_id(domain.getTaxCashBasisJournalId());
            if((Boolean) domain.getExtensionparams().get("po_leaddirtyflag"))
                model.setPo_lead(domain.getPoLead());
            if((Boolean) domain.getExtensionparams().get("snailmail_colordirtyflag"))
                model.setSnailmail_color(domain.getSnailmailColor());
            if((Boolean) domain.getExtensionparams().get("paperformat_iddirtyflag"))
                model.setPaperformat_id(domain.getPaperformatId());
            if((Boolean) domain.getExtensionparams().get("portal_confirmation_signdirtyflag"))
                model.setPortal_confirmation_sign(domain.getPortalConfirmationSign());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("digest_id_textdirtyflag"))
                model.setDigest_id_text(domain.getDigestIdText());
            if((Boolean) domain.getExtensionparams().get("sale_notedirtyflag"))
                model.setSale_note(domain.getSaleNote());
            if((Boolean) domain.getExtensionparams().get("auth_signup_template_user_id_textdirtyflag"))
                model.setAuth_signup_template_user_id_text(domain.getAuthSignupTemplateUserIdText());
            if((Boolean) domain.getExtensionparams().get("invoice_is_printdirtyflag"))
                model.setInvoice_is_print(domain.getInvoiceIsPrint());
            if((Boolean) domain.getExtensionparams().get("deposit_default_product_id_textdirtyflag"))
                model.setDeposit_default_product_id_text(domain.getDepositDefaultProductIdText());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_iddirtyflag"))
                model.setResource_calendar_id(domain.getResourceCalendarId());
            if((Boolean) domain.getExtensionparams().get("po_lockdirtyflag"))
                model.setPo_lock(domain.getPoLock());
            if((Boolean) domain.getExtensionparams().get("invoice_is_snailmaildirtyflag"))
                model.setInvoice_is_snailmail(domain.getInvoiceIsSnailmail());
            if((Boolean) domain.getExtensionparams().get("manufacturing_leaddirtyflag"))
                model.setManufacturing_lead(domain.getManufacturingLead());
            if((Boolean) domain.getExtensionparams().get("po_double_validationdirtyflag"))
                model.setPo_double_validation(domain.getPoDoubleValidation());
            if((Boolean) domain.getExtensionparams().get("report_footerdirtyflag"))
                model.setReport_footer(domain.getReportFooter());
            if((Boolean) domain.getExtensionparams().get("template_id_textdirtyflag"))
                model.setTemplate_id_text(domain.getTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("invoice_is_emaildirtyflag"))
                model.setInvoice_is_email(domain.getInvoiceIsEmail());
            if((Boolean) domain.getExtensionparams().get("portal_confirmation_paydirtyflag"))
                model.setPortal_confirmation_pay(domain.getPortalConfirmationPay());
            if((Boolean) domain.getExtensionparams().get("qr_codedirtyflag"))
                model.setQr_code(domain.getQrCode());
            if((Boolean) domain.getExtensionparams().get("snailmail_duplexdirtyflag"))
                model.setSnailmail_duplex(domain.getSnailmailDuplex());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("security_leaddirtyflag"))
                model.setSecurity_lead(domain.getSecurityLead());
            if((Boolean) domain.getExtensionparams().get("external_report_layout_iddirtyflag"))
                model.setExternal_report_layout_id(domain.getExternalReportLayoutId());
            if((Boolean) domain.getExtensionparams().get("purchase_tax_iddirtyflag"))
                model.setPurchase_tax_id(domain.getPurchaseTaxId());
            if((Boolean) domain.getExtensionparams().get("propagation_minimum_deltadirtyflag"))
                model.setPropagation_minimum_delta(domain.getPropagationMinimumDelta());
            if((Boolean) domain.getExtensionparams().get("quotation_validity_daysdirtyflag"))
                model.setQuotation_validity_days(domain.getQuotationValidityDays());
            if((Boolean) domain.getExtensionparams().get("tax_calculation_rounding_methoddirtyflag"))
                model.setTax_calculation_rounding_method(domain.getTaxCalculationRoundingMethod());
            if((Boolean) domain.getExtensionparams().get("currency_exchange_journal_iddirtyflag"))
                model.setCurrency_exchange_journal_id(domain.getCurrencyExchangeJournalId());
            if((Boolean) domain.getExtensionparams().get("po_double_validation_amountdirtyflag"))
                model.setPo_double_validation_amount(domain.getPoDoubleValidationAmount());
            if((Boolean) domain.getExtensionparams().get("chart_template_id_textdirtyflag"))
                model.setChart_template_id_text(domain.getChartTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("sale_tax_iddirtyflag"))
                model.setSale_tax_id(domain.getSaleTaxId());
            if((Boolean) domain.getExtensionparams().get("default_sale_order_template_id_textdirtyflag"))
                model.setDefault_sale_order_template_id_text(domain.getDefaultSaleOrderTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("default_sale_order_template_iddirtyflag"))
                model.setDefault_sale_order_template_id(domain.getDefaultSaleOrderTemplateId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("digest_iddirtyflag"))
                model.setDigest_id(domain.getDigestId());
            if((Boolean) domain.getExtensionparams().get("template_iddirtyflag"))
                model.setTemplate_id(domain.getTemplateId());
            if((Boolean) domain.getExtensionparams().get("chart_template_iddirtyflag"))
                model.setChart_template_id(domain.getChartTemplateId());
            if((Boolean) domain.getExtensionparams().get("deposit_default_product_iddirtyflag"))
                model.setDeposit_default_product_id(domain.getDepositDefaultProductId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("auth_signup_template_user_iddirtyflag"))
                model.setAuth_signup_template_user_id(domain.getAuthSignupTemplateUserId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_config_settings convert2Domain( res_config_settingsClientModel model ,Res_config_settings domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_config_settings();
        }

        if(model.getModule_account_bank_statement_import_qifDirtyFlag())
            domain.setModuleAccountBankStatementImportQif(model.getModule_account_bank_statement_import_qif());
        if(model.getModule_account_bank_statement_import_ofxDirtyFlag())
            domain.setModuleAccountBankStatementImportOfx(model.getModule_account_bank_statement_import_ofx());
        if(model.getHas_google_mapsDirtyFlag())
            domain.setHasGoogleMaps(model.getHas_google_maps());
        if(model.getModule_l10n_eu_serviceDirtyFlag())
            domain.setModuleL10nEuService(model.getModule_l10n_eu_service());
        if(model.getCdn_activatedDirtyFlag())
            domain.setCdnActivated(model.getCdn_activated());
        if(model.getWebsite_default_lang_codeDirtyFlag())
            domain.setWebsiteDefaultLangCode(model.getWebsite_default_lang_code());
        if(model.getAuth_signup_reset_passwordDirtyFlag())
            domain.setAuthSignupResetPassword(model.getAuth_signup_reset_password());
        if(model.getWebsite_country_group_idsDirtyFlag())
            domain.setWebsiteCountryGroupIds(model.getWebsite_country_group_ids());
        if(model.getSocial_instagramDirtyFlag())
            domain.setSocialInstagram(model.getSocial_instagram());
        if(model.getModule_purchase_requisitionDirtyFlag())
            domain.setModulePurchaseRequisition(model.getModule_purchase_requisition());
        if(model.getModule_delivery_easypostDirtyFlag())
            domain.setModuleDeliveryEasypost(model.getModule_delivery_easypost());
        if(model.getGroup_discount_per_so_lineDirtyFlag())
            domain.setGroupDiscountPerSoLine(model.getGroup_discount_per_so_line());
        if(model.getAutomatic_invoiceDirtyFlag())
            domain.setAutomaticInvoice(model.getAutomatic_invoice());
        if(model.getModule_account_plaidDirtyFlag())
            domain.setModuleAccountPlaid(model.getModule_account_plaid());
        if(model.getGoogle_management_client_idDirtyFlag())
            domain.setGoogleManagementClientId(model.getGoogle_management_client_id());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getModule_account_check_printingDirtyFlag())
            domain.setModuleAccountCheckPrinting(model.getModule_account_check_printing());
        if(model.getModule_partner_autocompleteDirtyFlag())
            domain.setModulePartnerAutocomplete(model.getModule_partner_autocomplete());
        if(model.getLanguage_countDirtyFlag())
            domain.setLanguageCount(model.getLanguage_count());
        if(model.getModule_website_linksDirtyFlag())
            domain.setModuleWebsiteLinks(model.getModule_website_links());
        if(model.getWebsite_form_enable_metadataDirtyFlag())
            domain.setWebsiteFormEnableMetadata(model.getWebsite_form_enable_metadata());
        if(model.getHas_social_networkDirtyFlag())
            domain.setHasSocialNetwork(model.getHas_social_network());
        if(model.getGroup_sale_delivery_addressDirtyFlag())
            domain.setGroupSaleDeliveryAddress(model.getGroup_sale_delivery_address());
        if(model.getGoogle_analytics_keyDirtyFlag())
            domain.setGoogleAnalyticsKey(model.getGoogle_analytics_key());
        if(model.getModule_auth_ldapDirtyFlag())
            domain.setModuleAuthLdap(model.getModule_auth_ldap());
        if(model.getSpecific_user_accountDirtyFlag())
            domain.setSpecificUserAccount(model.getSpecific_user_account());
        if(model.getModule_website_hr_recruitmentDirtyFlag())
            domain.setModuleWebsiteHrRecruitment(model.getModule_website_hr_recruitment());
        if(model.getModule_project_forecastDirtyFlag())
            domain.setModuleProjectForecast(model.getModule_project_forecast());
        if(model.getGroup_stock_tracking_ownerDirtyFlag())
            domain.setGroupStockTrackingOwner(model.getGroup_stock_tracking_owner());
        if(model.getModule_google_calendarDirtyFlag())
            domain.setModuleGoogleCalendar(model.getModule_google_calendar());
        if(model.getModule_accountDirtyFlag())
            domain.setModuleAccount(model.getModule_account());
        if(model.getModule_google_driveDirtyFlag())
            domain.setModuleGoogleDrive(model.getModule_google_drive());
        if(model.getPos_pricelist_settingDirtyFlag())
            domain.setPosPricelistSetting(model.getPos_pricelist_setting());
        if(model.getCompany_share_partnerDirtyFlag())
            domain.setCompanySharePartner(model.getCompany_share_partner());
        if(model.getModule_currency_rate_liveDirtyFlag())
            domain.setModuleCurrencyRateLive(model.getModule_currency_rate_live());
        if(model.getGroup_proforma_salesDirtyFlag())
            domain.setGroupProformaSales(model.getGroup_proforma_sales());
        if(model.getModule_delivery_fedexDirtyFlag())
            domain.setModuleDeliveryFedex(model.getModule_delivery_fedex());
        if(model.getModule_product_email_templateDirtyFlag())
            domain.setModuleProductEmailTemplate(model.getModule_product_email_template());
        if(model.getShow_effectDirtyFlag())
            domain.setShowEffect(model.getShow_effect());
        if(model.getDefault_picking_policyDirtyFlag())
            domain.setDefaultPickingPolicy(model.getDefault_picking_policy());
        if(model.getSocial_youtubeDirtyFlag())
            domain.setSocialYoutube(model.getSocial_youtube());
        if(model.getWebsite_company_idDirtyFlag())
            domain.setWebsiteCompanyId(model.getWebsite_company_id());
        if(model.getModule_mrp_byproductDirtyFlag())
            domain.setModuleMrpByproduct(model.getModule_mrp_byproduct());
        if(model.getModule_delivery_uspsDirtyFlag())
            domain.setModuleDeliveryUsps(model.getModule_delivery_usps());
        if(model.getModule_delivery_dhlDirtyFlag())
            domain.setModuleDeliveryDhl(model.getModule_delivery_dhl());
        if(model.getGroup_project_ratingDirtyFlag())
            domain.setGroupProjectRating(model.getGroup_project_rating());
        if(model.getGoogle_maps_api_keyDirtyFlag())
            domain.setGoogleMapsApiKey(model.getGoogle_maps_api_key());
        if(model.getGroup_use_leadDirtyFlag())
            domain.setGroupUseLead(model.getGroup_use_lead());
        if(model.getGroup_stock_tracking_lotDirtyFlag())
            domain.setGroupStockTrackingLot(model.getGroup_stock_tracking_lot());
        if(model.getGroup_stock_adv_locationDirtyFlag())
            domain.setGroupStockAdvLocation(model.getGroup_stock_adv_location());
        if(model.getPos_sales_priceDirtyFlag())
            domain.setPosSalesPrice(model.getPos_sales_price());
        if(model.getHas_chart_of_accountsDirtyFlag())
            domain.setHasChartOfAccounts(model.getHas_chart_of_accounts());
        if(model.getMulti_sales_price_methodDirtyFlag())
            domain.setMultiSalesPriceMethod(model.getMulti_sales_price_method());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getModule_procurement_jitDirtyFlag())
            domain.setModuleProcurementJit(model.getModule_procurement_jit());
        if(model.getModule_account_assetDirtyFlag())
            domain.setModuleAccountAsset(model.getModule_account_asset());
        if(model.getUse_mailgatewayDirtyFlag())
            domain.setUseMailgateway(model.getUse_mailgateway());
        if(model.getGroup_sale_pricelistDirtyFlag())
            domain.setGroupSalePricelist(model.getGroup_sale_pricelist());
        if(model.getCrm_default_team_idDirtyFlag())
            domain.setCrmDefaultTeamId(model.getCrm_default_team_id());
        if(model.getGroup_stock_multi_locationsDirtyFlag())
            domain.setGroupStockMultiLocations(model.getGroup_stock_multi_locations());
        if(model.getUse_manufacturing_leadDirtyFlag())
            domain.setUseManufacturingLead(model.getUse_manufacturing_lead());
        if(model.getModule_google_spreadsheetDirtyFlag())
            domain.setModuleGoogleSpreadsheet(model.getModule_google_spreadsheet());
        if(model.getShow_line_subtotals_tax_selectionDirtyFlag())
            domain.setShowLineSubtotalsTaxSelection(model.getShow_line_subtotals_tax_selection());
        if(model.getLanguage_idsDirtyFlag())
            domain.setLanguageIds(model.getLanguage_ids());
        if(model.getModule_website_sale_deliveryDirtyFlag())
            domain.setModuleWebsiteSaleDelivery(model.getModule_website_sale_delivery());
        if(model.getModule_mrp_mpsDirtyFlag())
            domain.setModuleMrpMps(model.getModule_mrp_mps());
        if(model.getPartner_autocomplete_insufficient_creditDirtyFlag())
            domain.setPartnerAutocompleteInsufficientCredit(model.getPartner_autocomplete_insufficient_credit());
        if(model.getModule_hr_org_chartDirtyFlag())
            domain.setModuleHrOrgChart(model.getModule_hr_org_chart());
        if(model.getModule_product_expiryDirtyFlag())
            domain.setModuleProductExpiry(model.getModule_product_expiry());
        if(model.getModule_delivery_bpostDirtyFlag())
            domain.setModuleDeliveryBpost(model.getModule_delivery_bpost());
        if(model.getModule_stock_barcodeDirtyFlag())
            domain.setModuleStockBarcode(model.getModule_stock_barcode());
        if(model.getSocial_githubDirtyFlag())
            domain.setSocialGithub(model.getSocial_github());
        if(model.getModule_account_intrastatDirtyFlag())
            domain.setModuleAccountIntrastat(model.getModule_account_intrastat());
        if(model.getAuto_done_settingDirtyFlag())
            domain.setAutoDoneSetting(model.getAuto_done_setting());
        if(model.getAuth_signup_uninvitedDirtyFlag())
            domain.setAuthSignupUninvited(model.getAuth_signup_uninvited());
        if(model.getCompany_share_productDirtyFlag())
            domain.setCompanyShareProduct(model.getCompany_share_product());
        if(model.getGroup_sale_order_templateDirtyFlag())
            domain.setGroupSaleOrderTemplate(model.getGroup_sale_order_template());
        if(model.getModule_account_sepa_direct_debitDirtyFlag())
            domain.setModuleAccountSepaDirectDebit(model.getModule_account_sepa_direct_debit());
        if(model.getUse_quotation_validity_daysDirtyFlag())
            domain.setUseQuotationValidityDays(model.getUse_quotation_validity_days());
        if(model.getModule_account_reports_followupDirtyFlag())
            domain.setModuleAccountReportsFollowup(model.getModule_account_reports_followup());
        if(model.getModule_account_batch_paymentDirtyFlag())
            domain.setModuleAccountBatchPayment(model.getModule_account_batch_payment());
        if(model.getSocial_twitterDirtyFlag())
            domain.setSocialTwitter(model.getSocial_twitter());
        if(model.getModule_account_budgetDirtyFlag())
            domain.setModuleAccountBudget(model.getModule_account_budget());
        if(model.getGroup_mrp_routingsDirtyFlag())
            domain.setGroupMrpRoutings(model.getGroup_mrp_routings());
        if(model.getGroup_cash_roundingDirtyFlag())
            domain.setGroupCashRounding(model.getGroup_cash_rounding());
        if(model.getModule_stock_landed_costsDirtyFlag())
            domain.setModuleStockLandedCosts(model.getModule_stock_landed_costs());
        if(model.getWebsite_nameDirtyFlag())
            domain.setWebsiteName(model.getWebsite_name());
        if(model.getModule_website_sale_stockDirtyFlag())
            domain.setModuleWebsiteSaleStock(model.getModule_website_sale_stock());
        if(model.getGroup_website_popup_on_exitDirtyFlag())
            domain.setGroupWebsitePopupOnExit(model.getGroup_website_popup_on_exit());
        if(model.getModule_website_event_trackDirtyFlag())
            domain.setModuleWebsiteEventTrack(model.getModule_website_event_track());
        if(model.getGroup_manage_vendor_priceDirtyFlag())
            domain.setGroupManageVendorPrice(model.getGroup_manage_vendor_price());
        if(model.getModule_deliveryDirtyFlag())
            domain.setModuleDelivery(model.getModule_delivery());
        if(model.getModule_account_invoice_extractDirtyFlag())
            domain.setModuleAccountInvoiceExtract(model.getModule_account_invoice_extract());
        if(model.getChannel_idDirtyFlag())
            domain.setChannelId(model.getChannel_id());
        if(model.getGroup_warning_saleDirtyFlag())
            domain.setGroupWarningSale(model.getGroup_warning_sale());
        if(model.getCdn_urlDirtyFlag())
            domain.setCdnUrl(model.getCdn_url());
        if(model.getModule_event_barcodeDirtyFlag())
            domain.setModuleEventBarcode(model.getModule_event_barcode());
        if(model.getAlias_domainDirtyFlag())
            domain.setAliasDomain(model.getAlias_domain());
        if(model.getSocial_linkedinDirtyFlag())
            domain.setSocialLinkedin(model.getSocial_linkedin());
        if(model.getGroup_stock_multi_warehousesDirtyFlag())
            domain.setGroupStockMultiWarehouses(model.getGroup_stock_multi_warehouses());
        if(model.getSalesperson_idDirtyFlag())
            domain.setSalespersonId(model.getSalesperson_id());
        if(model.getModule_account_reportsDirtyFlag())
            domain.setModuleAccountReports(model.getModule_account_reports());
        if(model.getGroup_product_pricelistDirtyFlag())
            domain.setGroupProductPricelist(model.getGroup_product_pricelist());
        if(model.getModule_crm_phone_validationDirtyFlag())
            domain.setModuleCrmPhoneValidation(model.getModule_crm_phone_validation());
        if(model.getModule_website_versionDirtyFlag())
            domain.setModuleWebsiteVersion(model.getModule_website_version());
        if(model.getModule_base_importDirtyFlag())
            domain.setModuleBaseImport(model.getModule_base_import());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getModule_account_bank_statement_import_csvDirtyFlag())
            domain.setModuleAccountBankStatementImportCsv(model.getModule_account_bank_statement_import_csv());
        if(model.getModule_account_taxcloudDirtyFlag())
            domain.setModuleAccountTaxcloud(model.getModule_account_taxcloud());
        if(model.getUse_sale_noteDirtyFlag())
            domain.setUseSaleNote(model.getUse_sale_note());
        if(model.getCart_abandoned_delayDirtyFlag())
            domain.setCartAbandonedDelay(model.getCart_abandoned_delay());
        if(model.getWebsite_domainDirtyFlag())
            domain.setWebsiteDomain(model.getWebsite_domain());
        if(model.getModule_account_accountantDirtyFlag())
            domain.setModuleAccountAccountant(model.getModule_account_accountant());
        if(model.getModule_sale_marginDirtyFlag())
            domain.setModuleSaleMargin(model.getModule_sale_margin());
        if(model.getDigest_emailsDirtyFlag())
            domain.setDigestEmails(model.getDigest_emails());
        if(model.getModule_padDirtyFlag())
            domain.setModulePad(model.getModule_pad());
        if(model.getGroup_warning_accountDirtyFlag())
            domain.setGroupWarningAccount(model.getGroup_warning_account());
        if(model.getGroup_display_incotermDirtyFlag())
            domain.setGroupDisplayIncoterm(model.getGroup_display_incoterm());
        if(model.getModule_website_sale_wishlistDirtyFlag())
            domain.setModuleWebsiteSaleWishlist(model.getModule_website_sale_wishlist());
        if(model.getUser_default_rightsDirtyFlag())
            domain.setUserDefaultRights(model.getUser_default_rights());
        if(model.getDefault_purchase_methodDirtyFlag())
            domain.setDefaultPurchaseMethod(model.getDefault_purchase_method());
        if(model.getGroup_delivery_invoice_addressDirtyFlag())
            domain.setGroupDeliveryInvoiceAddress(model.getGroup_delivery_invoice_address());
        if(model.getGroup_lot_on_delivery_slipDirtyFlag())
            domain.setGroupLotOnDeliverySlip(model.getGroup_lot_on_delivery_slip());
        if(model.getModule_event_saleDirtyFlag())
            domain.setModuleEventSale(model.getModule_event_sale());
        if(model.getGroup_show_line_subtotals_tax_includedDirtyFlag())
            domain.setGroupShowLineSubtotalsTaxIncluded(model.getGroup_show_line_subtotals_tax_included());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getGroup_product_variantDirtyFlag())
            domain.setGroupProductVariant(model.getGroup_product_variant());
        if(model.getModule_account_sepaDirtyFlag())
            domain.setModuleAccountSepa(model.getModule_account_sepa());
        if(model.getGroup_multi_currencyDirtyFlag())
            domain.setGroupMultiCurrency(model.getGroup_multi_currency());
        if(model.getGroup_products_in_billsDirtyFlag())
            domain.setGroupProductsInBills(model.getGroup_products_in_bills());
        if(model.getGroup_analytic_accountingDirtyFlag())
            domain.setGroupAnalyticAccounting(model.getGroup_analytic_accounting());
        if(model.getGroup_stock_packagingDirtyFlag())
            domain.setGroupStockPackaging(model.getGroup_stock_packaging());
        if(model.getWebsite_slide_google_app_keyDirtyFlag())
            domain.setWebsiteSlideGoogleAppKey(model.getWebsite_slide_google_app_key());
        if(model.getPo_order_approvalDirtyFlag())
            domain.setPoOrderApproval(model.getPo_order_approval());
        if(model.getIs_installed_saleDirtyFlag())
            domain.setIsInstalledSale(model.getIs_installed_sale());
        if(model.getModule_account_paymentDirtyFlag())
            domain.setModuleAccountPayment(model.getModule_account_payment());
        if(model.getGroup_analytic_tagsDirtyFlag())
            domain.setGroupAnalyticTags(model.getGroup_analytic_tags());
        if(model.getGroup_sale_order_datesDirtyFlag())
            domain.setGroupSaleOrderDates(model.getGroup_sale_order_dates());
        if(model.getModule_voipDirtyFlag())
            domain.setModuleVoip(model.getModule_voip());
        if(model.getCart_recovery_mail_templateDirtyFlag())
            domain.setCartRecoveryMailTemplate(model.getCart_recovery_mail_template());
        if(model.getGroup_multi_websiteDirtyFlag())
            domain.setGroupMultiWebsite(model.getGroup_multi_website());
        if(model.getModule_auth_oauthDirtyFlag())
            domain.setModuleAuthOauth(model.getModule_auth_oauth());
        if(model.getSale_delivery_settingsDirtyFlag())
            domain.setSaleDeliverySettings(model.getSale_delivery_settings());
        if(model.getModule_sale_quotation_builderDirtyFlag())
            domain.setModuleSaleQuotationBuilder(model.getModule_sale_quotation_builder());
        if(model.getModule_inter_company_rulesDirtyFlag())
            domain.setModuleInterCompanyRules(model.getModule_inter_company_rules());
        if(model.getUse_security_leadDirtyFlag())
            domain.setUseSecurityLead(model.getUse_security_lead());
        if(model.getDefault_invoice_policyDirtyFlag())
            domain.setDefaultInvoicePolicy(model.getDefault_invoice_policy());
        if(model.getMulti_sales_priceDirtyFlag())
            domain.setMultiSalesPrice(model.getMulti_sales_price());
        if(model.getLock_confirmed_poDirtyFlag())
            domain.setLockConfirmedPo(model.getLock_confirmed_po());
        if(model.getProduct_weight_in_lbsDirtyFlag())
            domain.setProductWeightInLbs(model.getProduct_weight_in_lbs());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getHas_google_analytics_dashboardDirtyFlag())
            domain.setHasGoogleAnalyticsDashboard(model.getHas_google_analytics_dashboard());
        if(model.getModule_stock_picking_batchDirtyFlag())
            domain.setModuleStockPickingBatch(model.getModule_stock_picking_batch());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getExpense_alias_prefixDirtyFlag())
            domain.setExpenseAliasPrefix(model.getExpense_alias_prefix());
        if(model.getModule_account_deferred_revenueDirtyFlag())
            domain.setModuleAccountDeferredRevenue(model.getModule_account_deferred_revenue());
        if(model.getSocial_facebookDirtyFlag())
            domain.setSocialFacebook(model.getSocial_facebook());
        if(model.getModule_web_unsplashDirtyFlag())
            domain.setModuleWebUnsplash(model.getModule_web_unsplash());
        if(model.getGroup_mass_mailing_campaignDirtyFlag())
            domain.setGroupMassMailingCampaign(model.getGroup_mass_mailing_campaign());
        if(model.getModule_account_bank_statement_import_camtDirtyFlag())
            domain.setModuleAccountBankStatementImportCamt(model.getModule_account_bank_statement_import_camt());
        if(model.getModule_product_marginDirtyFlag())
            domain.setModuleProductMargin(model.getModule_product_margin());
        if(model.getGroup_subtask_projectDirtyFlag())
            domain.setGroupSubtaskProject(model.getGroup_subtask_project());
        if(model.getModule_account_3way_matchDirtyFlag())
            domain.setModuleAccount3wayMatch(model.getModule_account_3way_match());
        if(model.getModule_website_sale_digitalDirtyFlag())
            domain.setModuleWebsiteSaleDigital(model.getModule_website_sale_digital());
        if(model.getModule_sale_couponDirtyFlag())
            domain.setModuleSaleCoupon(model.getModule_sale_coupon());
        if(model.getShow_blacklist_buttonsDirtyFlag())
            domain.setShowBlacklistButtons(model.getShow_blacklist_buttons());
        if(model.getGroup_warning_stockDirtyFlag())
            domain.setGroupWarningStock(model.getGroup_warning_stock());
        if(model.getGroup_multi_companyDirtyFlag())
            domain.setGroupMultiCompany(model.getGroup_multi_company());
        if(model.getGroup_attendance_use_pinDirtyFlag())
            domain.setGroupAttendanceUsePin(model.getGroup_attendance_use_pin());
        if(model.getSalesteam_idDirtyFlag())
            domain.setSalesteamId(model.getSalesteam_id());
        if(model.getFaviconDirtyFlag())
            domain.setFavicon(model.getFavicon());
        if(model.getModule_website_sale_comparisonDirtyFlag())
            domain.setModuleWebsiteSaleComparison(model.getModule_website_sale_comparison());
        if(model.getAvailable_thresholdDirtyFlag())
            domain.setAvailableThreshold(model.getAvailable_threshold());
        if(model.getUse_po_leadDirtyFlag())
            domain.setUsePoLead(model.getUse_po_lead());
        if(model.getGroup_fiscal_yearDirtyFlag())
            domain.setGroupFiscalYear(model.getGroup_fiscal_year());
        if(model.getModule_hr_timesheetDirtyFlag())
            domain.setModuleHrTimesheet(model.getModule_hr_timesheet());
        if(model.getModule_mrp_plmDirtyFlag())
            domain.setModuleMrpPlm(model.getModule_mrp_plm());
        if(model.getModule_account_yodleeDirtyFlag())
            domain.setModuleAccountYodlee(model.getModule_account_yodlee());
        if(model.getCdn_filtersDirtyFlag())
            domain.setCdnFilters(model.getCdn_filters());
        if(model.getUse_propagation_minimum_deltaDirtyFlag())
            domain.setUsePropagationMinimumDelta(model.getUse_propagation_minimum_delta());
        if(model.getModule_delivery_upsDirtyFlag())
            domain.setModuleDeliveryUps(model.getModule_delivery_ups());
        if(model.getFail_counterDirtyFlag())
            domain.setFailCounter(model.getFail_counter());
        if(model.getGroup_stock_production_lotDirtyFlag())
            domain.setGroupStockProductionLot(model.getGroup_stock_production_lot());
        if(model.getHas_accounting_entriesDirtyFlag())
            domain.setHasAccountingEntries(model.getHas_accounting_entries());
        if(model.getGroup_uomDirtyFlag())
            domain.setGroupUom(model.getGroup_uom());
        if(model.getMass_mailing_outgoing_mail_serverDirtyFlag())
            domain.setMassMailingOutgoingMailServer(model.getMass_mailing_outgoing_mail_server());
        if(model.getCrm_alias_prefixDirtyFlag())
            domain.setCrmAliasPrefix(model.getCrm_alias_prefix());
        if(model.getSocial_googleplusDirtyFlag())
            domain.setSocialGoogleplus(model.getSocial_googleplus());
        if(model.getGroup_route_so_linesDirtyFlag())
            domain.setGroupRouteSoLines(model.getGroup_route_so_lines());
        if(model.getGoogle_management_client_secretDirtyFlag())
            domain.setGoogleManagementClientSecret(model.getGoogle_management_client_secret());
        if(model.getSocial_default_imageDirtyFlag())
            domain.setSocialDefaultImage(model.getSocial_default_image());
        if(model.getHas_google_analyticsDirtyFlag())
            domain.setHasGoogleAnalytics(model.getHas_google_analytics());
        if(model.getModule_website_event_questionsDirtyFlag())
            domain.setModuleWebsiteEventQuestions(model.getModule_website_event_questions());
        if(model.getWebsite_default_lang_idDirtyFlag())
            domain.setWebsiteDefaultLangId(model.getWebsite_default_lang_id());
        if(model.getInventory_availabilityDirtyFlag())
            domain.setInventoryAvailability(model.getInventory_availability());
        if(model.getGroup_warning_purchaseDirtyFlag())
            domain.setGroupWarningPurchase(model.getGroup_warning_purchase());
        if(model.getModule_quality_controlDirtyFlag())
            domain.setModuleQualityControl(model.getModule_quality_control());
        if(model.getGenerate_lead_from_aliasDirtyFlag())
            domain.setGenerateLeadFromAlias(model.getGenerate_lead_from_alias());
        if(model.getSale_pricelist_settingDirtyFlag())
            domain.setSalePricelistSetting(model.getSale_pricelist_setting());
        if(model.getGroup_pricelist_itemDirtyFlag())
            domain.setGroupPricelistItem(model.getGroup_pricelist_item());
        if(model.getExternal_email_server_defaultDirtyFlag())
            domain.setExternalEmailServerDefault(model.getExternal_email_server_default());
        if(model.getUnsplash_access_keyDirtyFlag())
            domain.setUnsplashAccessKey(model.getUnsplash_access_key());
        if(model.getModule_base_gengoDirtyFlag())
            domain.setModuleBaseGengo(model.getModule_base_gengo());
        if(model.getModule_website_event_saleDirtyFlag())
            domain.setModuleWebsiteEventSale(model.getModule_website_event_sale());
        if(model.getCrm_default_user_idDirtyFlag())
            domain.setCrmDefaultUserId(model.getCrm_default_user_id());
        if(model.getModule_stock_dropshippingDirtyFlag())
            domain.setModuleStockDropshipping(model.getModule_stock_dropshipping());
        if(model.getModule_crm_revealDirtyFlag())
            domain.setModuleCrmReveal(model.getModule_crm_reveal());
        if(model.getMass_mailing_mail_server_idDirtyFlag())
            domain.setMassMailingMailServerId(model.getMass_mailing_mail_server_id());
        if(model.getGroup_show_line_subtotals_tax_excludedDirtyFlag())
            domain.setGroupShowLineSubtotalsTaxExcluded(model.getGroup_show_line_subtotals_tax_excluded());
        if(model.getModule_hr_recruitment_surveyDirtyFlag())
            domain.setModuleHrRecruitmentSurvey(model.getModule_hr_recruitment_survey());
        if(model.getModule_mrp_workorderDirtyFlag())
            domain.setModuleMrpWorkorder(model.getModule_mrp_workorder());
        if(model.getModule_pos_mercuryDirtyFlag())
            domain.setModulePosMercury(model.getModule_pos_mercury());
        if(model.getInvoice_reference_typeDirtyFlag())
            domain.setInvoiceReferenceType(model.getInvoice_reference_type());
        if(model.getCompany_currency_idDirtyFlag())
            domain.setCompanyCurrencyId(model.getCompany_currency_id());
        if(model.getTax_exigibilityDirtyFlag())
            domain.setTaxExigibility(model.getTax_exigibility());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getAccount_bank_reconciliation_startDirtyFlag())
            domain.setAccountBankReconciliationStart(model.getAccount_bank_reconciliation_start());
        if(model.getTax_cash_basis_journal_idDirtyFlag())
            domain.setTaxCashBasisJournalId(model.getTax_cash_basis_journal_id());
        if(model.getPo_leadDirtyFlag())
            domain.setPoLead(model.getPo_lead());
        if(model.getSnailmail_colorDirtyFlag())
            domain.setSnailmailColor(model.getSnailmail_color());
        if(model.getPaperformat_idDirtyFlag())
            domain.setPaperformatId(model.getPaperformat_id());
        if(model.getPortal_confirmation_signDirtyFlag())
            domain.setPortalConfirmationSign(model.getPortal_confirmation_sign());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getDigest_id_textDirtyFlag())
            domain.setDigestIdText(model.getDigest_id_text());
        if(model.getSale_noteDirtyFlag())
            domain.setSaleNote(model.getSale_note());
        if(model.getAuth_signup_template_user_id_textDirtyFlag())
            domain.setAuthSignupTemplateUserIdText(model.getAuth_signup_template_user_id_text());
        if(model.getInvoice_is_printDirtyFlag())
            domain.setInvoiceIsPrint(model.getInvoice_is_print());
        if(model.getDeposit_default_product_id_textDirtyFlag())
            domain.setDepositDefaultProductIdText(model.getDeposit_default_product_id_text());
        if(model.getResource_calendar_idDirtyFlag())
            domain.setResourceCalendarId(model.getResource_calendar_id());
        if(model.getPo_lockDirtyFlag())
            domain.setPoLock(model.getPo_lock());
        if(model.getInvoice_is_snailmailDirtyFlag())
            domain.setInvoiceIsSnailmail(model.getInvoice_is_snailmail());
        if(model.getManufacturing_leadDirtyFlag())
            domain.setManufacturingLead(model.getManufacturing_lead());
        if(model.getPo_double_validationDirtyFlag())
            domain.setPoDoubleValidation(model.getPo_double_validation());
        if(model.getReport_footerDirtyFlag())
            domain.setReportFooter(model.getReport_footer());
        if(model.getTemplate_id_textDirtyFlag())
            domain.setTemplateIdText(model.getTemplate_id_text());
        if(model.getInvoice_is_emailDirtyFlag())
            domain.setInvoiceIsEmail(model.getInvoice_is_email());
        if(model.getPortal_confirmation_payDirtyFlag())
            domain.setPortalConfirmationPay(model.getPortal_confirmation_pay());
        if(model.getQr_codeDirtyFlag())
            domain.setQrCode(model.getQr_code());
        if(model.getSnailmail_duplexDirtyFlag())
            domain.setSnailmailDuplex(model.getSnailmail_duplex());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getSecurity_leadDirtyFlag())
            domain.setSecurityLead(model.getSecurity_lead());
        if(model.getExternal_report_layout_idDirtyFlag())
            domain.setExternalReportLayoutId(model.getExternal_report_layout_id());
        if(model.getPurchase_tax_idDirtyFlag())
            domain.setPurchaseTaxId(model.getPurchase_tax_id());
        if(model.getPropagation_minimum_deltaDirtyFlag())
            domain.setPropagationMinimumDelta(model.getPropagation_minimum_delta());
        if(model.getQuotation_validity_daysDirtyFlag())
            domain.setQuotationValidityDays(model.getQuotation_validity_days());
        if(model.getTax_calculation_rounding_methodDirtyFlag())
            domain.setTaxCalculationRoundingMethod(model.getTax_calculation_rounding_method());
        if(model.getCurrency_exchange_journal_idDirtyFlag())
            domain.setCurrencyExchangeJournalId(model.getCurrency_exchange_journal_id());
        if(model.getPo_double_validation_amountDirtyFlag())
            domain.setPoDoubleValidationAmount(model.getPo_double_validation_amount());
        if(model.getChart_template_id_textDirtyFlag())
            domain.setChartTemplateIdText(model.getChart_template_id_text());
        if(model.getSale_tax_idDirtyFlag())
            domain.setSaleTaxId(model.getSale_tax_id());
        if(model.getDefault_sale_order_template_id_textDirtyFlag())
            domain.setDefaultSaleOrderTemplateIdText(model.getDefault_sale_order_template_id_text());
        if(model.getDefault_sale_order_template_idDirtyFlag())
            domain.setDefaultSaleOrderTemplateId(model.getDefault_sale_order_template_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getDigest_idDirtyFlag())
            domain.setDigestId(model.getDigest_id());
        if(model.getTemplate_idDirtyFlag())
            domain.setTemplateId(model.getTemplate_id());
        if(model.getChart_template_idDirtyFlag())
            domain.setChartTemplateId(model.getChart_template_id());
        if(model.getDeposit_default_product_idDirtyFlag())
            domain.setDepositDefaultProductId(model.getDeposit_default_product_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getAuth_signup_template_user_idDirtyFlag())
            domain.setAuthSignupTemplateUserId(model.getAuth_signup_template_user_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



