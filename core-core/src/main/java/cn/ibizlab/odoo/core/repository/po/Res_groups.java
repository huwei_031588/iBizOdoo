package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Res_groupsSearchContext;

/**
 * 实体 [访问群] 存储模型
 */
public interface Res_groups{

    /**
     * 用户
     */
    String getUsers();

    void setUsers(String users);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUsersDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 继承
     */
    String getImplied_ids();

    void setImplied_ids(String implied_ids);

    /**
     * 获取 [继承]脏标记
     */
    boolean getImplied_idsDirtyFlag();

    /**
     * 及物继承
     */
    String getTrans_implied_ids();

    void setTrans_implied_ids(String trans_implied_ids);

    /**
     * 获取 [及物继承]脏标记
     */
    boolean getTrans_implied_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 视图
     */
    String getView_access();

    void setView_access(String view_access);

    /**
     * 获取 [视图]脏标记
     */
    boolean getView_accessDirtyFlag();

    /**
     * 规则
     */
    String getRule_groups();

    void setRule_groups(String rule_groups);

    /**
     * 获取 [规则]脏标记
     */
    boolean getRule_groupsDirtyFlag();

    /**
     * 访问控制
     */
    String getModel_access();

    void setModel_access(String model_access);

    /**
     * 获取 [访问控制]脏标记
     */
    boolean getModel_accessDirtyFlag();

    /**
     * 应用
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [应用]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 访问菜单
     */
    String getMenu_access();

    void setMenu_access(String menu_access);

    /**
     * 获取 [访问菜单]脏标记
     */
    boolean getMenu_accessDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 备注
     */
    String getComment();

    void setComment(String comment);

    /**
     * 获取 [备注]脏标记
     */
    boolean getCommentDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 共享用户组
     */
    String getShare();

    void setShare(String share);

    /**
     * 获取 [共享用户组]脏标记
     */
    boolean getShareDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 群组名称
     */
    String getFull_name();

    void setFull_name(String full_name);

    /**
     * 获取 [群组名称]脏标记
     */
    boolean getFull_nameDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
