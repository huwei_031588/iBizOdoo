package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_page;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_pageSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_pageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_website.client.website_pageOdooClient;
import cn.ibizlab.odoo.core.odoo_website.clientmodel.website_pageClientModel;

/**
 * 实体[页] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_pageServiceImpl implements IWebsite_pageService {

    @Autowired
    website_pageOdooClient website_pageOdooClient;


    @Override
    public boolean update(Website_page et) {
        website_pageClientModel clientModel = convert2Model(et,null);
		website_pageOdooClient.update(clientModel);
        Website_page rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Website_page> list){
    }

    @Override
    public Website_page get(Integer id) {
        website_pageClientModel clientModel = new website_pageClientModel();
        clientModel.setId(id);
		website_pageOdooClient.get(clientModel);
        Website_page et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Website_page();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        website_pageClientModel clientModel = new website_pageClientModel();
        clientModel.setId(id);
		website_pageOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Website_page et) {
        website_pageClientModel clientModel = convert2Model(et,null);
		website_pageOdooClient.create(clientModel);
        Website_page rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_page> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_page> searchDefault(Website_pageSearchContext context) {
        List<Website_page> list = new ArrayList<Website_page>();
        Page<website_pageClientModel> clientModelList = website_pageOdooClient.search(context);
        for(website_pageClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Website_page>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public website_pageClientModel convert2Model(Website_page domain , website_pageClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new website_pageClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("archdirtyflag"))
                model.setArch(domain.getArch());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("website_meta_descriptiondirtyflag"))
                model.setWebsite_meta_description(domain.getWebsiteMetaDescription());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("website_urldirtyflag"))
                model.setWebsite_url(domain.getWebsiteUrl());
            if((Boolean) domain.getExtensionparams().get("prioritydirtyflag"))
                model.setPriority(domain.getPriority());
            if((Boolean) domain.getExtensionparams().get("xml_iddirtyflag"))
                model.setXml_id(domain.getXmlId());
            if((Boolean) domain.getExtensionparams().get("website_indexeddirtyflag"))
                model.setWebsite_indexed(domain.getWebsiteIndexed());
            if((Boolean) domain.getExtensionparams().get("view_iddirtyflag"))
                model.setView_id(domain.getViewId());
            if((Boolean) domain.getExtensionparams().get("theme_template_iddirtyflag"))
                model.setTheme_template_id(domain.getThemeTemplateId());
            if((Boolean) domain.getExtensionparams().get("website_meta_og_imgdirtyflag"))
                model.setWebsite_meta_og_img(domain.getWebsiteMetaOgImg());
            if((Boolean) domain.getExtensionparams().get("customize_showdirtyflag"))
                model.setCustomize_show(domain.getCustomizeShow());
            if((Boolean) domain.getExtensionparams().get("modeldirtyflag"))
                model.setModel(domain.getModel());
            if((Boolean) domain.getExtensionparams().get("field_parentdirtyflag"))
                model.setField_parent(domain.getFieldParent());
            if((Boolean) domain.getExtensionparams().get("arch_dbdirtyflag"))
                model.setArch_db(domain.getArchDb());
            if((Boolean) domain.getExtensionparams().get("is_homepagedirtyflag"))
                model.setIs_homepage(domain.getIsHomepage());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("website_meta_titledirtyflag"))
                model.setWebsite_meta_title(domain.getWebsiteMetaTitle());
            if((Boolean) domain.getExtensionparams().get("model_data_iddirtyflag"))
                model.setModel_data_id(domain.getModelDataId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("modedirtyflag"))
                model.setMode(domain.getMode());
            if((Boolean) domain.getExtensionparams().get("menu_idsdirtyflag"))
                model.setMenu_ids(domain.getMenuIds());
            if((Boolean) domain.getExtensionparams().get("inherit_children_idsdirtyflag"))
                model.setInherit_children_ids(domain.getInheritChildrenIds());
            if((Boolean) domain.getExtensionparams().get("arch_basedirtyflag"))
                model.setArch_base(domain.getArchBase());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("is_visibledirtyflag"))
                model.setIs_visible(domain.getIsVisible());
            if((Boolean) domain.getExtensionparams().get("first_page_iddirtyflag"))
                model.setFirst_page_id(domain.getFirstPageId());
            if((Boolean) domain.getExtensionparams().get("is_seo_optimizeddirtyflag"))
                model.setIs_seo_optimized(domain.getIsSeoOptimized());
            if((Boolean) domain.getExtensionparams().get("header_colordirtyflag"))
                model.setHeader_color(domain.getHeaderColor());
            if((Boolean) domain.getExtensionparams().get("is_publisheddirtyflag"))
                model.setIs_published(domain.getIsPublished());
            if((Boolean) domain.getExtensionparams().get("page_idsdirtyflag"))
                model.setPage_ids(domain.getPageIds());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("urldirtyflag"))
                model.setUrl(domain.getUrl());
            if((Boolean) domain.getExtensionparams().get("header_overlaydirtyflag"))
                model.setHeader_overlay(domain.getHeaderOverlay());
            if((Boolean) domain.getExtensionparams().get("website_meta_keywordsdirtyflag"))
                model.setWebsite_meta_keywords(domain.getWebsiteMetaKeywords());
            if((Boolean) domain.getExtensionparams().get("date_publishdirtyflag"))
                model.setDate_publish(domain.getDatePublish());
            if((Boolean) domain.getExtensionparams().get("groups_iddirtyflag"))
                model.setGroups_id(domain.getGroupsId());
            if((Boolean) domain.getExtensionparams().get("keydirtyflag"))
                model.setKey(domain.getKey());
            if((Boolean) domain.getExtensionparams().get("inherit_iddirtyflag"))
                model.setInherit_id(domain.getInheritId());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("model_idsdirtyflag"))
                model.setModel_ids(domain.getModelIds());
            if((Boolean) domain.getExtensionparams().get("arch_fsdirtyflag"))
                model.setArch_fs(domain.getArchFs());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Website_page convert2Domain( website_pageClientModel model ,Website_page domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Website_page();
        }

        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getArchDirtyFlag())
            domain.setArch(model.getArch());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWebsite_meta_descriptionDirtyFlag())
            domain.setWebsiteMetaDescription(model.getWebsite_meta_description());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWebsite_urlDirtyFlag())
            domain.setWebsiteUrl(model.getWebsite_url());
        if(model.getPriorityDirtyFlag())
            domain.setPriority(model.getPriority());
        if(model.getXml_idDirtyFlag())
            domain.setXmlId(model.getXml_id());
        if(model.getWebsite_indexedDirtyFlag())
            domain.setWebsiteIndexed(model.getWebsite_indexed());
        if(model.getView_idDirtyFlag())
            domain.setViewId(model.getView_id());
        if(model.getTheme_template_idDirtyFlag())
            domain.setThemeTemplateId(model.getTheme_template_id());
        if(model.getWebsite_meta_og_imgDirtyFlag())
            domain.setWebsiteMetaOgImg(model.getWebsite_meta_og_img());
        if(model.getCustomize_showDirtyFlag())
            domain.setCustomizeShow(model.getCustomize_show());
        if(model.getModelDirtyFlag())
            domain.setModel(model.getModel());
        if(model.getField_parentDirtyFlag())
            domain.setFieldParent(model.getField_parent());
        if(model.getArch_dbDirtyFlag())
            domain.setArchDb(model.getArch_db());
        if(model.getIs_homepageDirtyFlag())
            domain.setIsHomepage(model.getIs_homepage());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getWebsite_meta_titleDirtyFlag())
            domain.setWebsiteMetaTitle(model.getWebsite_meta_title());
        if(model.getModel_data_idDirtyFlag())
            domain.setModelDataId(model.getModel_data_id());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getModeDirtyFlag())
            domain.setMode(model.getMode());
        if(model.getMenu_idsDirtyFlag())
            domain.setMenuIds(model.getMenu_ids());
        if(model.getInherit_children_idsDirtyFlag())
            domain.setInheritChildrenIds(model.getInherit_children_ids());
        if(model.getArch_baseDirtyFlag())
            domain.setArchBase(model.getArch_base());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIs_visibleDirtyFlag())
            domain.setIsVisible(model.getIs_visible());
        if(model.getFirst_page_idDirtyFlag())
            domain.setFirstPageId(model.getFirst_page_id());
        if(model.getIs_seo_optimizedDirtyFlag())
            domain.setIsSeoOptimized(model.getIs_seo_optimized());
        if(model.getHeader_colorDirtyFlag())
            domain.setHeaderColor(model.getHeader_color());
        if(model.getIs_publishedDirtyFlag())
            domain.setIsPublished(model.getIs_published());
        if(model.getPage_idsDirtyFlag())
            domain.setPageIds(model.getPage_ids());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getUrlDirtyFlag())
            domain.setUrl(model.getUrl());
        if(model.getHeader_overlayDirtyFlag())
            domain.setHeaderOverlay(model.getHeader_overlay());
        if(model.getWebsite_meta_keywordsDirtyFlag())
            domain.setWebsiteMetaKeywords(model.getWebsite_meta_keywords());
        if(model.getDate_publishDirtyFlag())
            domain.setDatePublish(model.getDate_publish());
        if(model.getGroups_idDirtyFlag())
            domain.setGroupsId(model.getGroups_id());
        if(model.getKeyDirtyFlag())
            domain.setKey(model.getKey());
        if(model.getInherit_idDirtyFlag())
            domain.setInheritId(model.getInherit_id());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getModel_idsDirtyFlag())
            domain.setModelIds(model.getModel_ids());
        if(model.getArch_fsDirtyFlag())
            domain.setArchFs(model.getArch_fs());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



