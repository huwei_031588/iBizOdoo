package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_recruitment_stage] 对象
 */
public interface hr_recruitment_stage {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getFold();

    public void setFold(String fold);

    public Integer getId();

    public void setId(Integer id);

    public Integer getJob_id();

    public void setJob_id(Integer job_id);

    public String getJob_id_text();

    public void setJob_id_text(String job_id_text);

    public String getLegend_blocked();

    public void setLegend_blocked(String legend_blocked);

    public String getLegend_done();

    public void setLegend_done(String legend_done);

    public String getLegend_normal();

    public void setLegend_normal(String legend_normal);

    public String getName();

    public void setName(String name);

    public String getRequirements();

    public void setRequirements(String requirements);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public Integer getTemplate_id();

    public void setTemplate_id(Integer template_id);

    public String getTemplate_id_text();

    public void setTemplate_id_text(String template_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
