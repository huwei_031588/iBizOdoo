package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_event.filter.Event_event_ticketSearchContext;

/**
 * 实体 [活动入场券] 存储模型
 */
public interface Event_event_ticket{

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 可用席位
     */
    Integer getSeats_available();

    void setSeats_available(Integer seats_available);

    /**
     * 获取 [可用席位]脏标记
     */
    boolean getSeats_availableDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 减税后价格
     */
    Double getPrice_reduce_taxinc();

    void setPrice_reduce_taxinc(Double price_reduce_taxinc);

    /**
     * 获取 [减税后价格]脏标记
     */
    boolean getPrice_reduce_taxincDirtyFlag();

    /**
     * 降价
     */
    Double getPrice_reduce();

    void setPrice_reduce(Double price_reduce);

    /**
     * 获取 [降价]脏标记
     */
    boolean getPrice_reduceDirtyFlag();

    /**
     * 登记
     */
    String getRegistration_ids();

    void setRegistration_ids(String registration_ids);

    /**
     * 获取 [登记]脏标记
     */
    boolean getRegistration_idsDirtyFlag();

    /**
     * 未确认的席位预订
     */
    Integer getSeats_unconfirmed();

    void setSeats_unconfirmed(Integer seats_unconfirmed);

    /**
     * 获取 [未确认的席位预订]脏标记
     */
    boolean getSeats_unconfirmedDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 预订席位
     */
    Integer getSeats_reserved();

    void setSeats_reserved(Integer seats_reserved);

    /**
     * 获取 [预订席位]脏标记
     */
    boolean getSeats_reservedDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 价格
     */
    Double getPrice();

    void setPrice(Double price);

    /**
     * 获取 [价格]脏标记
     */
    boolean getPriceDirtyFlag();

    /**
     * 座椅
     */
    Integer getSeats_used();

    void setSeats_used(Integer seats_used);

    /**
     * 获取 [座椅]脏标记
     */
    boolean getSeats_usedDirtyFlag();

    /**
     * 销售结束
     */
    Timestamp getDeadline();

    void setDeadline(Timestamp deadline);

    /**
     * 获取 [销售结束]脏标记
     */
    boolean getDeadlineDirtyFlag();

    /**
     * 可用席位
     */
    String getSeats_availability();

    void setSeats_availability(String seats_availability);

    /**
     * 获取 [可用席位]脏标记
     */
    boolean getSeats_availabilityDirtyFlag();

    /**
     * 已结束
     */
    String getIs_expired();

    void setIs_expired(String is_expired);

    /**
     * 获取 [已结束]脏标记
     */
    boolean getIs_expiredDirtyFlag();

    /**
     * 最多可用席位
     */
    Integer getSeats_max();

    void setSeats_max(Integer seats_max);

    /**
     * 获取 [最多可用席位]脏标记
     */
    boolean getSeats_maxDirtyFlag();

    /**
     * 活动类别
     */
    String getEvent_type_id_text();

    void setEvent_type_id_text(String event_type_id_text);

    /**
     * 获取 [活动类别]脏标记
     */
    boolean getEvent_type_id_textDirtyFlag();

    /**
     * 活动
     */
    String getEvent_id_text();

    void setEvent_id_text(String event_id_text);

    /**
     * 获取 [活动]脏标记
     */
    boolean getEvent_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 活动
     */
    Integer getEvent_id();

    void setEvent_id(Integer event_id);

    /**
     * 获取 [活动]脏标记
     */
    boolean getEvent_idDirtyFlag();

    /**
     * 活动类别
     */
    Integer getEvent_type_id();

    void setEvent_type_id(Integer event_type_id);

    /**
     * 获取 [活动类别]脏标记
     */
    boolean getEvent_type_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
