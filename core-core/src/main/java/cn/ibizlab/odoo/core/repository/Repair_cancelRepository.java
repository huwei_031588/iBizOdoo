package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Repair_cancel;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_cancelSearchContext;

/**
 * 实体 [取消维修] 存储对象
 */
public interface Repair_cancelRepository extends Repository<Repair_cancel> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Repair_cancel> searchDefault(Repair_cancelSearchContext context);

    Repair_cancel convert2PO(cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel domain , Repair_cancel po) ;

    cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel convert2Domain( Repair_cancel po ,cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel domain) ;

}
