package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_leave_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_typeSearchContext;

/**
 * 实体 [休假类型] 存储对象
 */
public interface Hr_leave_typeRepository extends Repository<Hr_leave_type> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_leave_type> searchDefault(Hr_leave_typeSearchContext context);

    Hr_leave_type convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type domain , Hr_leave_type po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type convert2Domain( Hr_leave_type po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type domain) ;

}
