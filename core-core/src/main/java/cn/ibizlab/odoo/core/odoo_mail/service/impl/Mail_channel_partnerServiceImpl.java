package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channel_partnerSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_channel_partnerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_channel_partnerOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_channel_partnerClientModel;

/**
 * 实体[渠道指南] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_channel_partnerServiceImpl implements IMail_channel_partnerService {

    @Autowired
    mail_channel_partnerOdooClient mail_channel_partnerOdooClient;


    @Override
    public boolean create(Mail_channel_partner et) {
        mail_channel_partnerClientModel clientModel = convert2Model(et,null);
		mail_channel_partnerOdooClient.create(clientModel);
        Mail_channel_partner rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_channel_partner> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_channel_partnerClientModel clientModel = new mail_channel_partnerClientModel();
        clientModel.setId(id);
		mail_channel_partnerOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mail_channel_partner get(Integer id) {
        mail_channel_partnerClientModel clientModel = new mail_channel_partnerClientModel();
        clientModel.setId(id);
		mail_channel_partnerOdooClient.get(clientModel);
        Mail_channel_partner et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_channel_partner();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mail_channel_partner et) {
        mail_channel_partnerClientModel clientModel = convert2Model(et,null);
		mail_channel_partnerOdooClient.update(clientModel);
        Mail_channel_partner rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_channel_partner> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_channel_partner> searchDefault(Mail_channel_partnerSearchContext context) {
        List<Mail_channel_partner> list = new ArrayList<Mail_channel_partner>();
        Page<mail_channel_partnerClientModel> clientModelList = mail_channel_partnerOdooClient.search(context);
        for(mail_channel_partnerClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_channel_partner>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_channel_partnerClientModel convert2Model(Mail_channel_partner domain , mail_channel_partnerClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_channel_partnerClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("is_blacklisteddirtyflag"))
                model.setIs_blacklisted(domain.getIsBlacklisted());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("is_pinneddirtyflag"))
                model.setIs_pinned(domain.getIsPinned());
            if((Boolean) domain.getExtensionparams().get("fold_statedirtyflag"))
                model.setFold_state(domain.getFoldState());
            if((Boolean) domain.getExtensionparams().get("is_minimizeddirtyflag"))
                model.setIs_minimized(domain.getIsMinimized());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("channel_id_textdirtyflag"))
                model.setChannel_id_text(domain.getChannelIdText());
            if((Boolean) domain.getExtensionparams().get("partner_emaildirtyflag"))
                model.setPartner_email(domain.getPartnerEmail());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("channel_iddirtyflag"))
                model.setChannel_id(domain.getChannelId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("seen_message_iddirtyflag"))
                model.setSeen_message_id(domain.getSeenMessageId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_channel_partner convert2Domain( mail_channel_partnerClientModel model ,Mail_channel_partner domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_channel_partner();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIs_blacklistedDirtyFlag())
            domain.setIsBlacklisted(model.getIs_blacklisted());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIs_pinnedDirtyFlag())
            domain.setIsPinned(model.getIs_pinned());
        if(model.getFold_stateDirtyFlag())
            domain.setFoldState(model.getFold_state());
        if(model.getIs_minimizedDirtyFlag())
            domain.setIsMinimized(model.getIs_minimized());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getChannel_id_textDirtyFlag())
            domain.setChannelIdText(model.getChannel_id_text());
        if(model.getPartner_emailDirtyFlag())
            domain.setPartnerEmail(model.getPartner_email());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getChannel_idDirtyFlag())
            domain.setChannelId(model.getChannel_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getSeen_message_idDirtyFlag())
            domain.setSeenMessageId(model.getSeen_message_id());
        return domain ;
    }

}

    



