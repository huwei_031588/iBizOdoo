package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.event_confirm;

/**
 * 实体[event_confirm] 服务对象接口
 */
public interface event_confirmRepository{


    public event_confirm createPO() ;
        public List<event_confirm> search();

        public void create(event_confirm event_confirm);

        public void update(event_confirm event_confirm);

        public void removeBatch(String id);

        public void updateBatch(event_confirm event_confirm);

        public void get(String id);

        public void remove(String id);

        public void createBatch(event_confirm event_confirm);


}
