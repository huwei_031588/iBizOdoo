package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.lunch_product;

/**
 * 实体[lunch_product] 服务对象接口
 */
public interface lunch_productRepository{


    public lunch_product createPO() ;
        public void create(lunch_product lunch_product);

        public void get(String id);

        public void createBatch(lunch_product lunch_product);

        public void updateBatch(lunch_product lunch_product);

        public List<lunch_product> search();

        public void removeBatch(String id);

        public void update(lunch_product lunch_product);

        public void remove(String id);


}
