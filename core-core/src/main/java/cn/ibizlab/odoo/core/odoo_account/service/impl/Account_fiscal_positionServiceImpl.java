package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_positionSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_positionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_fiscal_positionOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_fiscal_positionClientModel;

/**
 * 实体[税科目调整] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_fiscal_positionServiceImpl implements IAccount_fiscal_positionService {

    @Autowired
    account_fiscal_positionOdooClient account_fiscal_positionOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_fiscal_positionClientModel clientModel = new account_fiscal_positionClientModel();
        clientModel.setId(id);
		account_fiscal_positionOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_fiscal_position et) {
        account_fiscal_positionClientModel clientModel = convert2Model(et,null);
		account_fiscal_positionOdooClient.create(clientModel);
        Account_fiscal_position rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_fiscal_position> list){
    }

    @Override
    public boolean update(Account_fiscal_position et) {
        account_fiscal_positionClientModel clientModel = convert2Model(et,null);
		account_fiscal_positionOdooClient.update(clientModel);
        Account_fiscal_position rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_fiscal_position> list){
    }

    @Override
    public Account_fiscal_position get(Integer id) {
        account_fiscal_positionClientModel clientModel = new account_fiscal_positionClientModel();
        clientModel.setId(id);
		account_fiscal_positionOdooClient.get(clientModel);
        Account_fiscal_position et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_fiscal_position();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_fiscal_position> searchDefault(Account_fiscal_positionSearchContext context) {
        List<Account_fiscal_position> list = new ArrayList<Account_fiscal_position>();
        Page<account_fiscal_positionClientModel> clientModelList = account_fiscal_positionOdooClient.search(context);
        for(account_fiscal_positionClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_fiscal_position>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_fiscal_positionClientModel convert2Model(Account_fiscal_position domain , account_fiscal_positionClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_fiscal_positionClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("vat_requireddirtyflag"))
                model.setVat_required(domain.getVatRequired());
            if((Boolean) domain.getExtensionparams().get("account_idsdirtyflag"))
                model.setAccount_ids(domain.getAccountIds());
            if((Boolean) domain.getExtensionparams().get("states_countdirtyflag"))
                model.setStates_count(domain.getStatesCount());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("state_idsdirtyflag"))
                model.setState_ids(domain.getStateIds());
            if((Boolean) domain.getExtensionparams().get("auto_applydirtyflag"))
                model.setAuto_apply(domain.getAutoApply());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("zip_todirtyflag"))
                model.setZip_to(domain.getZipTo());
            if((Boolean) domain.getExtensionparams().get("tax_idsdirtyflag"))
                model.setTax_ids(domain.getTaxIds());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("zip_fromdirtyflag"))
                model.setZip_from(domain.getZipFrom());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("country_group_id_textdirtyflag"))
                model.setCountry_group_id_text(domain.getCountryGroupIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("country_id_textdirtyflag"))
                model.setCountry_id_text(domain.getCountryIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("country_group_iddirtyflag"))
                model.setCountry_group_id(domain.getCountryGroupId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_fiscal_position convert2Domain( account_fiscal_positionClientModel model ,Account_fiscal_position domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_fiscal_position();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getVat_requiredDirtyFlag())
            domain.setVatRequired(model.getVat_required());
        if(model.getAccount_idsDirtyFlag())
            domain.setAccountIds(model.getAccount_ids());
        if(model.getStates_countDirtyFlag())
            domain.setStatesCount(model.getStates_count());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getState_idsDirtyFlag())
            domain.setStateIds(model.getState_ids());
        if(model.getAuto_applyDirtyFlag())
            domain.setAutoApply(model.getAuto_apply());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getZip_toDirtyFlag())
            domain.setZipTo(model.getZip_to());
        if(model.getTax_idsDirtyFlag())
            domain.setTaxIds(model.getTax_ids());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getZip_fromDirtyFlag())
            domain.setZipFrom(model.getZip_from());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCountry_group_id_textDirtyFlag())
            domain.setCountryGroupIdText(model.getCountry_group_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCountry_id_textDirtyFlag())
            domain.setCountryIdText(model.getCountry_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCountry_group_idDirtyFlag())
            domain.setCountryGroupId(model.getCountry_group_id());
        return domain ;
    }

}

    



