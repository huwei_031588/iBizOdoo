package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_pickingSearchContext;

/**
 * 实体 [退回拣货] 存储模型
 */
public interface Stock_return_picking{

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 移动
     */
    String getProduct_return_moves();

    void setProduct_return_moves(String product_return_moves);

    /**
     * 获取 [移动]脏标记
     */
    boolean getProduct_return_movesDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 链接的移动已存在
     */
    String getMove_dest_exists();

    void setMove_dest_exists(String move_dest_exists);

    /**
     * 获取 [链接的移动已存在]脏标记
     */
    boolean getMove_dest_existsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 原始位置
     */
    String getOriginal_location_id_text();

    void setOriginal_location_id_text(String original_location_id_text);

    /**
     * 获取 [原始位置]脏标记
     */
    boolean getOriginal_location_id_textDirtyFlag();

    /**
     * 退回位置
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [退回位置]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 上级位置
     */
    String getParent_location_id_text();

    void setParent_location_id_text(String parent_location_id_text);

    /**
     * 获取 [上级位置]脏标记
     */
    boolean getParent_location_id_textDirtyFlag();

    /**
     * 分拣
     */
    String getPicking_id_text();

    void setPicking_id_text(String picking_id_text);

    /**
     * 获取 [分拣]脏标记
     */
    boolean getPicking_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 上级位置
     */
    Integer getParent_location_id();

    void setParent_location_id(Integer parent_location_id);

    /**
     * 获取 [上级位置]脏标记
     */
    boolean getParent_location_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 原始位置
     */
    Integer getOriginal_location_id();

    void setOriginal_location_id(Integer original_location_id);

    /**
     * 获取 [原始位置]脏标记
     */
    boolean getOriginal_location_idDirtyFlag();

    /**
     * 退回位置
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [退回位置]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 分拣
     */
    Integer getPicking_id();

    void setPicking_id(Integer picking_id);

    /**
     * 获取 [分拣]脏标记
     */
    boolean getPicking_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
