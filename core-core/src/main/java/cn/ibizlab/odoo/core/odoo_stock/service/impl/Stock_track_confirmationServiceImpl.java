package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_confirmationSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_track_confirmationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_track_confirmationOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_track_confirmationClientModel;

/**
 * 实体[库存追溯确认] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_track_confirmationServiceImpl implements IStock_track_confirmationService {

    @Autowired
    stock_track_confirmationOdooClient stock_track_confirmationOdooClient;


    @Override
    public boolean create(Stock_track_confirmation et) {
        stock_track_confirmationClientModel clientModel = convert2Model(et,null);
		stock_track_confirmationOdooClient.create(clientModel);
        Stock_track_confirmation rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_track_confirmation> list){
    }

    @Override
    public Stock_track_confirmation get(Integer id) {
        stock_track_confirmationClientModel clientModel = new stock_track_confirmationClientModel();
        clientModel.setId(id);
		stock_track_confirmationOdooClient.get(clientModel);
        Stock_track_confirmation et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_track_confirmation();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        stock_track_confirmationClientModel clientModel = new stock_track_confirmationClientModel();
        clientModel.setId(id);
		stock_track_confirmationOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Stock_track_confirmation et) {
        stock_track_confirmationClientModel clientModel = convert2Model(et,null);
		stock_track_confirmationOdooClient.update(clientModel);
        Stock_track_confirmation rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_track_confirmation> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_track_confirmation> searchDefault(Stock_track_confirmationSearchContext context) {
        List<Stock_track_confirmation> list = new ArrayList<Stock_track_confirmation>();
        Page<stock_track_confirmationClientModel> clientModelList = stock_track_confirmationOdooClient.search(context);
        for(stock_track_confirmationClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_track_confirmation>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_track_confirmationClientModel convert2Model(Stock_track_confirmation domain , stock_track_confirmationClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_track_confirmationClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("tracking_line_idsdirtyflag"))
                model.setTracking_line_ids(domain.getTrackingLineIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("inventory_id_textdirtyflag"))
                model.setInventory_id_text(domain.getInventoryIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("inventory_iddirtyflag"))
                model.setInventory_id(domain.getInventoryId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_track_confirmation convert2Domain( stock_track_confirmationClientModel model ,Stock_track_confirmation domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_track_confirmation();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getTracking_line_idsDirtyFlag())
            domain.setTrackingLineIds(model.getTracking_line_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getInventory_id_textDirtyFlag())
            domain.setInventoryIdText(model.getInventory_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getInventory_idDirtyFlag())
            domain.setInventoryId(model.getInventory_id());
        return domain ;
    }

}

    



