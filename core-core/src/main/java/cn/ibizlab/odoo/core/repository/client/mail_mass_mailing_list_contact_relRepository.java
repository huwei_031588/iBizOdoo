package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mail_mass_mailing_list_contact_rel;

/**
 * 实体[mail_mass_mailing_list_contact_rel] 服务对象接口
 */
public interface mail_mass_mailing_list_contact_relRepository{


    public mail_mass_mailing_list_contact_rel createPO() ;
        public List<mail_mass_mailing_list_contact_rel> search();

        public void create(mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

        public void remove(String id);

        public void removeBatch(String id);

        public void createBatch(mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

        public void update(mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

        public void updateBatch(mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel);

        public void get(String id);


}
