package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendarSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendarService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_resource.client.resource_calendarOdooClient;
import cn.ibizlab.odoo.core.odoo_resource.clientmodel.resource_calendarClientModel;

/**
 * 实体[资源工作时间] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_calendarServiceImpl implements IResource_calendarService {

    @Autowired
    resource_calendarOdooClient resource_calendarOdooClient;


    @Override
    public boolean create(Resource_calendar et) {
        resource_calendarClientModel clientModel = convert2Model(et,null);
		resource_calendarOdooClient.create(clientModel);
        Resource_calendar rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_calendar> list){
    }

    @Override
    public Resource_calendar get(Integer id) {
        resource_calendarClientModel clientModel = new resource_calendarClientModel();
        clientModel.setId(id);
		resource_calendarOdooClient.get(clientModel);
        Resource_calendar et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Resource_calendar();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        resource_calendarClientModel clientModel = new resource_calendarClientModel();
        clientModel.setId(id);
		resource_calendarOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Resource_calendar et) {
        resource_calendarClientModel clientModel = convert2Model(et,null);
		resource_calendarOdooClient.update(clientModel);
        Resource_calendar rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Resource_calendar> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_calendar> searchDefault(Resource_calendarSearchContext context) {
        List<Resource_calendar> list = new ArrayList<Resource_calendar>();
        Page<resource_calendarClientModel> clientModelList = resource_calendarOdooClient.search(context);
        for(resource_calendarClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Resource_calendar>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public resource_calendarClientModel convert2Model(Resource_calendar domain , resource_calendarClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new resource_calendarClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("tzdirtyflag"))
                model.setTz(domain.getTz());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("global_leave_idsdirtyflag"))
                model.setGlobal_leave_ids(domain.getGlobalLeaveIds());
            if((Boolean) domain.getExtensionparams().get("attendance_idsdirtyflag"))
                model.setAttendance_ids(domain.getAttendanceIds());
            if((Boolean) domain.getExtensionparams().get("hours_per_daydirtyflag"))
                model.setHours_per_day(domain.getHoursPerDay());
            if((Boolean) domain.getExtensionparams().get("leave_idsdirtyflag"))
                model.setLeave_ids(domain.getLeaveIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Resource_calendar convert2Domain( resource_calendarClientModel model ,Resource_calendar domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Resource_calendar();
        }

        if(model.getTzDirtyFlag())
            domain.setTz(model.getTz());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getGlobal_leave_idsDirtyFlag())
            domain.setGlobalLeaveIds(model.getGlobal_leave_ids());
        if(model.getAttendance_idsDirtyFlag())
            domain.setAttendanceIds(model.getAttendance_ids());
        if(model.getHours_per_dayDirtyFlag())
            domain.setHoursPerDay(model.getHours_per_day());
        if(model.getLeave_idsDirtyFlag())
            domain.setLeaveIds(model.getLeave_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



