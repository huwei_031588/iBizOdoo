package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channel_partnerSearchContext;

/**
 * 实体 [渠道指南] 存储模型
 */
public interface Mail_channel_partner{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 黑名单
     */
    String getIs_blacklisted();

    void setIs_blacklisted(String is_blacklisted);

    /**
     * 获取 [黑名单]脏标记
     */
    boolean getIs_blacklistedDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 是否置顶
     */
    String getIs_pinned();

    void setIs_pinned(String is_pinned);

    /**
     * 获取 [是否置顶]脏标记
     */
    boolean getIs_pinnedDirtyFlag();

    /**
     * 对话收拢状态
     */
    String getFold_state();

    void setFold_state(String fold_state);

    /**
     * 获取 [对话收拢状态]脏标记
     */
    boolean getFold_stateDirtyFlag();

    /**
     * 对话已最小化
     */
    String getIs_minimized();

    void setIs_minimized(String is_minimized);

    /**
     * 获取 [对话已最小化]脏标记
     */
    boolean getIs_minimizedDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 收件人
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [收件人]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 渠道
     */
    String getChannel_id_text();

    void setChannel_id_text(String channel_id_text);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getChannel_id_textDirtyFlag();

    /**
     * EMail
     */
    String getPartner_email();

    void setPartner_email(String partner_email);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getPartner_emailDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 渠道
     */
    Integer getChannel_id();

    void setChannel_id(Integer channel_id);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getChannel_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 收件人
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [收件人]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 最近一次查阅
     */
    Integer getSeen_message_id();

    void setSeen_message_id(Integer seen_message_id);

    /**
     * 获取 [最近一次查阅]脏标记
     */
    boolean getSeen_message_idDirtyFlag();

}
