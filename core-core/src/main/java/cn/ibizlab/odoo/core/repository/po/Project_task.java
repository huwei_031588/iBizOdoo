package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_project.filter.Project_taskSearchContext;

/**
 * 实体 [任务] 存储模型
 */
public interface Project_task{

    /**
     * 起始日期
     */
    Timestamp getDate_start();

    void setDate_start(Timestamp date_start);

    /**
     * 获取 [起始日期]脏标记
     */
    boolean getDate_startDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 点评数
     */
    Integer getRating_count();

    void setRating_count(Integer rating_count);

    /**
     * 获取 [点评数]脏标记
     */
    boolean getRating_countDirtyFlag();

    /**
     * EMail
     */
    String getEmail_from();

    void setEmail_from(String email_from);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getEmail_fromDirtyFlag();

    /**
     * 主要附件
     */
    String getAttachment_ids();

    void setAttachment_ids(String attachment_ids);

    /**
     * 获取 [主要附件]脏标记
     */
    boolean getAttachment_idsDirtyFlag();

    /**
     * 截止日期
     */
    Timestamp getDate_deadline();

    void setDate_deadline(Timestamp date_deadline);

    /**
     * 获取 [截止日期]脏标记
     */
    boolean getDate_deadlineDirtyFlag();

    /**
     * 看板状态标签
     */
    String getKanban_state_label();

    void setKanban_state_label(String kanban_state_label);

    /**
     * 获取 [看板状态标签]脏标记
     */
    boolean getKanban_state_labelDirtyFlag();

    /**
     * 访问警告
     */
    String getAccess_warning();

    void setAccess_warning(String access_warning);

    /**
     * 获取 [访问警告]脏标记
     */
    boolean getAccess_warningDirtyFlag();

    /**
     * 点评
     */
    String getRating_ids();

    void setRating_ids(String rating_ids);

    /**
     * 获取 [点评]脏标记
     */
    boolean getRating_idsDirtyFlag();

    /**
     * 最新值评级
     */
    Double getRating_last_value();

    void setRating_last_value(Double rating_last_value);

    /**
     * 获取 [最新值评级]脏标记
     */
    boolean getRating_last_valueDirtyFlag();

    /**
     * 分派日期
     */
    Timestamp getDate_assign();

    void setDate_assign(Timestamp date_assign);

    /**
     * 获取 [分派日期]脏标记
     */
    boolean getDate_assignDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 最新反馈评级
     */
    String getRating_last_feedback();

    void setRating_last_feedback(String rating_last_feedback);

    /**
     * 获取 [最新反馈评级]脏标记
     */
    boolean getRating_last_feedbackDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 安全令牌
     */
    String getAccess_token();

    void setAccess_token(String access_token);

    /**
     * 获取 [安全令牌]脏标记
     */
    boolean getAccess_tokenDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 子任务数
     */
    Integer getSubtask_count();

    void setSubtask_count(Integer subtask_count);

    /**
     * 获取 [子任务数]脏标记
     */
    boolean getSubtask_countDirtyFlag();

    /**
     * 分配的工作时间
     */
    Double getWorking_hours_open();

    void setWorking_hours_open(Double working_hours_open);

    /**
     * 获取 [分配的工作时间]脏标记
     */
    boolean getWorking_hours_openDirtyFlag();

    /**
     * 工作时间结束
     */
    Double getWorking_hours_close();

    void setWorking_hours_close(Double working_hours_close);

    /**
     * 获取 [工作时间结束]脏标记
     */
    boolean getWorking_hours_closeDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 最新图像评级
     */
    byte[] getRating_last_image();

    void setRating_last_image(byte[] rating_last_image);

    /**
     * 获取 [最新图像评级]脏标记
     */
    boolean getRating_last_imageDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 封面图像
     */
    Integer getDisplayed_image_id();

    void setDisplayed_image_id(Integer displayed_image_id);

    /**
     * 获取 [封面图像]脏标记
     */
    boolean getDisplayed_image_idDirtyFlag();

    /**
     * 标签
     */
    String getTag_ids();

    void setTag_ids(String tag_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getTag_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 看板状态
     */
    String getKanban_state();

    void setKanban_state(String kanban_state);

    /**
     * 获取 [看板状态]脏标记
     */
    boolean getKanban_stateDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 便签
     */
    String getNotes();

    void setNotes(String notes);

    /**
     * 获取 [便签]脏标记
     */
    boolean getNotesDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 最后阶段更新
     */
    Timestamp getDate_last_stage_update();

    void setDate_last_stage_update(Timestamp date_last_stage_update);

    /**
     * 获取 [最后阶段更新]脏标记
     */
    boolean getDate_last_stage_updateDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 工作日分配
     */
    Double getWorking_days_open();

    void setWorking_days_open(Double working_days_open);

    /**
     * 获取 [工作日分配]脏标记
     */
    boolean getWorking_days_openDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 工作日结束
     */
    Double getWorking_days_close();

    void setWorking_days_close(Double working_days_close);

    /**
     * 获取 [工作日结束]脏标记
     */
    boolean getWorking_days_closeDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 关注者的EMail
     */
    String getEmail_cc();

    void setEmail_cc(String email_cc);

    /**
     * 获取 [关注者的EMail]脏标记
     */
    boolean getEmail_ccDirtyFlag();

    /**
     * 期末日期
     */
    Timestamp getDate_end();

    void setDate_end(Timestamp date_end);

    /**
     * 获取 [期末日期]脏标记
     */
    boolean getDate_endDirtyFlag();

    /**
     * 子任务
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [子任务]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 已计划的时数
     */
    Double getPlanned_hours();

    void setPlanned_hours(Double planned_hours);

    /**
     * 获取 [已计划的时数]脏标记
     */
    boolean getPlanned_hoursDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 优先级
     */
    String getPriority();

    void setPriority(String priority);

    /**
     * 获取 [优先级]脏标记
     */
    boolean getPriorityDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 创建于
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建于]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 门户访问网址
     */
    String getAccess_url();

    void setAccess_url(String access_url);

    /**
     * 获取 [门户访问网址]脏标记
     */
    boolean getAccess_urlDirtyFlag();

    /**
     * 称谓
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [称谓]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 子任务
     */
    Double getSubtask_planned_hours();

    void setSubtask_planned_hours(Double subtask_planned_hours);

    /**
     * 获取 [子任务]脏标记
     */
    boolean getSubtask_planned_hoursDirtyFlag();

    /**
     * 看板有效解释
     */
    String getLegend_done();

    void setLegend_done(String legend_done);

    /**
     * 获取 [看板有效解释]脏标记
     */
    boolean getLegend_doneDirtyFlag();

    /**
     * 看板阻塞说明
     */
    String getLegend_blocked();

    void setLegend_blocked(String legend_blocked);

    /**
     * 获取 [看板阻塞说明]脏标记
     */
    boolean getLegend_blockedDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 客户
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 项目管理员
     */
    Integer getManager_id();

    void setManager_id(Integer manager_id);

    /**
     * 获取 [项目管理员]脏标记
     */
    boolean getManager_idDirtyFlag();

    /**
     * 项目
     */
    String getProject_id_text();

    void setProject_id_text(String project_id_text);

    /**
     * 获取 [项目]脏标记
     */
    boolean getProject_id_textDirtyFlag();

    /**
     * 上级任务
     */
    String getParent_id_text();

    void setParent_id_text(String parent_id_text);

    /**
     * 获取 [上级任务]脏标记
     */
    boolean getParent_id_textDirtyFlag();

    /**
     * 分派给
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [分派给]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 用户EMail
     */
    String getUser_email();

    void setUser_email(String user_email);

    /**
     * 获取 [用户EMail]脏标记
     */
    boolean getUser_emailDirtyFlag();

    /**
     * 子任务项目
     */
    Integer getSubtask_project_id();

    void setSubtask_project_id(Integer subtask_project_id);

    /**
     * 获取 [子任务项目]脏标记
     */
    boolean getSubtask_project_idDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 阶段
     */
    String getStage_id_text();

    void setStage_id_text(String stage_id_text);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_id_textDirtyFlag();

    /**
     * 看板进展中说明
     */
    String getLegend_normal();

    void setLegend_normal(String legend_normal);

    /**
     * 获取 [看板进展中说明]脏标记
     */
    boolean getLegend_normalDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 分派给
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [分派给]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 项目
     */
    Integer getProject_id();

    void setProject_id(Integer project_id);

    /**
     * 获取 [项目]脏标记
     */
    boolean getProject_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 客户
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 上级任务
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级任务]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 阶段
     */
    Integer getStage_id();

    void setStage_id(Integer stage_id);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_idDirtyFlag();

}
