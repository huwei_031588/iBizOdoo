package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Sale_report;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_reportSearchContext;

/**
 * 实体 [销售分析报告] 存储对象
 */
public interface Sale_reportRepository extends Repository<Sale_report> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Sale_report> searchDefault(Sale_reportSearchContext context);

    Sale_report convert2PO(cn.ibizlab.odoo.core.odoo_sale.domain.Sale_report domain , Sale_report po) ;

    cn.ibizlab.odoo.core.odoo_sale.domain.Sale_report convert2Domain( Sale_report po ,cn.ibizlab.odoo.core.odoo_sale.domain.Sale_report domain) ;

}
