package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_currency_rate;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currency_rateSearchContext;

/**
 * 实体 [汇率] 存储对象
 */
public interface Res_currency_rateRepository extends Repository<Res_currency_rate> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_currency_rate> searchDefault(Res_currency_rateSearchContext context);

    Res_currency_rate convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_currency_rate domain , Res_currency_rate po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_currency_rate convert2Domain( Res_currency_rate po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_currency_rate domain) ;

}
