package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mail_mass_mailing_list_contact_rel] 对象
 */
public interface mail_mass_mailing_list_contact_rel {

    public Integer getContact_count();

    public void setContact_count(Integer contact_count);

    public Integer getContact_id();

    public void setContact_id(Integer contact_id);

    public String getContact_id_text();

    public void setContact_id_text(String contact_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_blacklisted();

    public void setIs_blacklisted(String is_blacklisted);

    public Integer getList_id();

    public void setList_id(Integer list_id);

    public String getList_id_text();

    public void setList_id_text(String list_id_text);

    public Integer getMessage_bounce();

    public void setMessage_bounce(Integer message_bounce);

    public String getOpt_out();

    public void setOpt_out(String opt_out);

    public Timestamp getUnsubscription_date();

    public void setUnsubscription_date(Timestamp unsubscription_date);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
