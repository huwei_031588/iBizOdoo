package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_sale.client.sale_reportOdooClient;
import cn.ibizlab.odoo.core.odoo_sale.clientmodel.sale_reportClientModel;

/**
 * 实体[销售分析报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_reportServiceImpl implements ISale_reportService {

    @Autowired
    sale_reportOdooClient sale_reportOdooClient;


    @Override
    public Sale_report get(Integer id) {
        sale_reportClientModel clientModel = new sale_reportClientModel();
        clientModel.setId(id);
		sale_reportOdooClient.get(clientModel);
        Sale_report et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Sale_report();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Sale_report et) {
        sale_reportClientModel clientModel = convert2Model(et,null);
		sale_reportOdooClient.create(clientModel);
        Sale_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_report> list){
    }

    @Override
    public boolean update(Sale_report et) {
        sale_reportClientModel clientModel = convert2Model(et,null);
		sale_reportOdooClient.update(clientModel);
        Sale_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Sale_report> list){
    }

    @Override
    public boolean remove(Integer id) {
        sale_reportClientModel clientModel = new sale_reportClientModel();
        clientModel.setId(id);
		sale_reportOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_report> searchDefault(Sale_reportSearchContext context) {
        List<Sale_report> list = new ArrayList<Sale_report>();
        Page<sale_reportClientModel> clientModelList = sale_reportOdooClient.search(context);
        for(sale_reportClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Sale_report>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public sale_reportClientModel convert2Model(Sale_report domain , sale_reportClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new sale_reportClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("price_subtotaldirtyflag"))
                model.setPrice_subtotal(domain.getPriceSubtotal());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("discountdirtyflag"))
                model.setDiscount(domain.getDiscount());
            if((Boolean) domain.getExtensionparams().get("discount_amountdirtyflag"))
                model.setDiscount_amount(domain.getDiscountAmount());
            if((Boolean) domain.getExtensionparams().get("volumedirtyflag"))
                model.setVolume(domain.getVolume());
            if((Boolean) domain.getExtensionparams().get("price_totaldirtyflag"))
                model.setPrice_total(domain.getPriceTotal());
            if((Boolean) domain.getExtensionparams().get("confirmation_datedirtyflag"))
                model.setConfirmation_date(domain.getConfirmationDate());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("qty_to_invoicedirtyflag"))
                model.setQty_to_invoice(domain.getQtyToInvoice());
            if((Boolean) domain.getExtensionparams().get("nbrdirtyflag"))
                model.setNbr(domain.getNbr());
            if((Boolean) domain.getExtensionparams().get("weightdirtyflag"))
                model.setWeight(domain.getWeight());
            if((Boolean) domain.getExtensionparams().get("product_uom_qtydirtyflag"))
                model.setProduct_uom_qty(domain.getProductUomQty());
            if((Boolean) domain.getExtensionparams().get("untaxed_amount_to_invoicedirtyflag"))
                model.setUntaxed_amount_to_invoice(domain.getUntaxedAmountToInvoice());
            if((Boolean) domain.getExtensionparams().get("untaxed_amount_invoiceddirtyflag"))
                model.setUntaxed_amount_invoiced(domain.getUntaxedAmountInvoiced());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("qty_delivereddirtyflag"))
                model.setQty_delivered(domain.getQtyDelivered());
            if((Boolean) domain.getExtensionparams().get("qty_invoiceddirtyflag"))
                model.setQty_invoiced(domain.getQtyInvoiced());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_id_textdirtyflag"))
                model.setProduct_tmpl_id_text(domain.getProductTmplIdText());
            if((Boolean) domain.getExtensionparams().get("warehouse_id_textdirtyflag"))
                model.setWarehouse_id_text(domain.getWarehouseIdText());
            if((Boolean) domain.getExtensionparams().get("analytic_account_id_textdirtyflag"))
                model.setAnalytic_account_id_text(domain.getAnalyticAccountIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("country_id_textdirtyflag"))
                model.setCountry_id_text(domain.getCountryIdText());
            if((Boolean) domain.getExtensionparams().get("categ_id_textdirtyflag"))
                model.setCateg_id_text(domain.getCategIdText());
            if((Boolean) domain.getExtensionparams().get("source_id_textdirtyflag"))
                model.setSource_id_text(domain.getSourceIdText());
            if((Boolean) domain.getExtensionparams().get("campaign_id_textdirtyflag"))
                model.setCampaign_id_text(domain.getCampaignIdText());
            if((Boolean) domain.getExtensionparams().get("order_id_textdirtyflag"))
                model.setOrder_id_text(domain.getOrderIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("commercial_partner_id_textdirtyflag"))
                model.setCommercial_partner_id_text(domain.getCommercialPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("medium_id_textdirtyflag"))
                model.setMedium_id_text(domain.getMediumIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_textdirtyflag"))
                model.setProduct_uom_text(domain.getProductUomText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("team_id_textdirtyflag"))
                model.setTeam_id_text(domain.getTeamIdText());
            if((Boolean) domain.getExtensionparams().get("pricelist_id_textdirtyflag"))
                model.setPricelist_id_text(domain.getPricelistIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("team_iddirtyflag"))
                model.setTeam_id(domain.getTeamId());
            if((Boolean) domain.getExtensionparams().get("commercial_partner_iddirtyflag"))
                model.setCommercial_partner_id(domain.getCommercialPartnerId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("analytic_account_iddirtyflag"))
                model.setAnalytic_account_id(domain.getAnalyticAccountId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("medium_iddirtyflag"))
                model.setMedium_id(domain.getMediumId());
            if((Boolean) domain.getExtensionparams().get("order_iddirtyflag"))
                model.setOrder_id(domain.getOrderId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("source_iddirtyflag"))
                model.setSource_id(domain.getSourceId());
            if((Boolean) domain.getExtensionparams().get("campaign_iddirtyflag"))
                model.setCampaign_id(domain.getCampaignId());
            if((Boolean) domain.getExtensionparams().get("categ_iddirtyflag"))
                model.setCateg_id(domain.getCategId());
            if((Boolean) domain.getExtensionparams().get("warehouse_iddirtyflag"))
                model.setWarehouse_id(domain.getWarehouseId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("pricelist_iddirtyflag"))
                model.setPricelist_id(domain.getPricelistId());
            if((Boolean) domain.getExtensionparams().get("product_uomdirtyflag"))
                model.setProduct_uom(domain.getProductUom());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Sale_report convert2Domain( sale_reportClientModel model ,Sale_report domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Sale_report();
        }

        if(model.getPrice_subtotalDirtyFlag())
            domain.setPriceSubtotal(model.getPrice_subtotal());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDiscountDirtyFlag())
            domain.setDiscount(model.getDiscount());
        if(model.getDiscount_amountDirtyFlag())
            domain.setDiscountAmount(model.getDiscount_amount());
        if(model.getVolumeDirtyFlag())
            domain.setVolume(model.getVolume());
        if(model.getPrice_totalDirtyFlag())
            domain.setPriceTotal(model.getPrice_total());
        if(model.getConfirmation_dateDirtyFlag())
            domain.setConfirmationDate(model.getConfirmation_date());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getQty_to_invoiceDirtyFlag())
            domain.setQtyToInvoice(model.getQty_to_invoice());
        if(model.getNbrDirtyFlag())
            domain.setNbr(model.getNbr());
        if(model.getWeightDirtyFlag())
            domain.setWeight(model.getWeight());
        if(model.getProduct_uom_qtyDirtyFlag())
            domain.setProductUomQty(model.getProduct_uom_qty());
        if(model.getUntaxed_amount_to_invoiceDirtyFlag())
            domain.setUntaxedAmountToInvoice(model.getUntaxed_amount_to_invoice());
        if(model.getUntaxed_amount_invoicedDirtyFlag())
            domain.setUntaxedAmountInvoiced(model.getUntaxed_amount_invoiced());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getQty_deliveredDirtyFlag())
            domain.setQtyDelivered(model.getQty_delivered());
        if(model.getQty_invoicedDirtyFlag())
            domain.setQtyInvoiced(model.getQty_invoiced());
        if(model.getProduct_tmpl_id_textDirtyFlag())
            domain.setProductTmplIdText(model.getProduct_tmpl_id_text());
        if(model.getWarehouse_id_textDirtyFlag())
            domain.setWarehouseIdText(model.getWarehouse_id_text());
        if(model.getAnalytic_account_id_textDirtyFlag())
            domain.setAnalyticAccountIdText(model.getAnalytic_account_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getCountry_id_textDirtyFlag())
            domain.setCountryIdText(model.getCountry_id_text());
        if(model.getCateg_id_textDirtyFlag())
            domain.setCategIdText(model.getCateg_id_text());
        if(model.getSource_id_textDirtyFlag())
            domain.setSourceIdText(model.getSource_id_text());
        if(model.getCampaign_id_textDirtyFlag())
            domain.setCampaignIdText(model.getCampaign_id_text());
        if(model.getOrder_id_textDirtyFlag())
            domain.setOrderIdText(model.getOrder_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCommercial_partner_id_textDirtyFlag())
            domain.setCommercialPartnerIdText(model.getCommercial_partner_id_text());
        if(model.getMedium_id_textDirtyFlag())
            domain.setMediumIdText(model.getMedium_id_text());
        if(model.getProduct_uom_textDirtyFlag())
            domain.setProductUomText(model.getProduct_uom_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getTeam_id_textDirtyFlag())
            domain.setTeamIdText(model.getTeam_id_text());
        if(model.getPricelist_id_textDirtyFlag())
            domain.setPricelistIdText(model.getPricelist_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getTeam_idDirtyFlag())
            domain.setTeamId(model.getTeam_id());
        if(model.getCommercial_partner_idDirtyFlag())
            domain.setCommercialPartnerId(model.getCommercial_partner_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getAnalytic_account_idDirtyFlag())
            domain.setAnalyticAccountId(model.getAnalytic_account_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getMedium_idDirtyFlag())
            domain.setMediumId(model.getMedium_id());
        if(model.getOrder_idDirtyFlag())
            domain.setOrderId(model.getOrder_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getSource_idDirtyFlag())
            domain.setSourceId(model.getSource_id());
        if(model.getCampaign_idDirtyFlag())
            domain.setCampaignId(model.getCampaign_id());
        if(model.getCateg_idDirtyFlag())
            domain.setCategId(model.getCateg_id());
        if(model.getWarehouse_idDirtyFlag())
            domain.setWarehouseId(model.getWarehouse_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getPricelist_idDirtyFlag())
            domain.setPricelistId(model.getPricelist_id());
        if(model.getProduct_uomDirtyFlag())
            domain.setProductUom(model.getProduct_uom());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        return domain ;
    }

}

    



