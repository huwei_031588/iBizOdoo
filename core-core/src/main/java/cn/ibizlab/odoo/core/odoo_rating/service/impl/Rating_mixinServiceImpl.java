package cn.ibizlab.odoo.core.odoo_rating.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_rating.service.IRating_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_rating.client.rating_mixinOdooClient;
import cn.ibizlab.odoo.core.odoo_rating.clientmodel.rating_mixinClientModel;

/**
 * 实体[混合评级] 服务对象接口实现
 */
@Slf4j
@Service
public class Rating_mixinServiceImpl implements IRating_mixinService {

    @Autowired
    rating_mixinOdooClient rating_mixinOdooClient;


    @Override
    public Rating_mixin get(Integer id) {
        rating_mixinClientModel clientModel = new rating_mixinClientModel();
        clientModel.setId(id);
		rating_mixinOdooClient.get(clientModel);
        Rating_mixin et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Rating_mixin();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Rating_mixin et) {
        rating_mixinClientModel clientModel = convert2Model(et,null);
		rating_mixinOdooClient.create(clientModel);
        Rating_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Rating_mixin> list){
    }

    @Override
    public boolean update(Rating_mixin et) {
        rating_mixinClientModel clientModel = convert2Model(et,null);
		rating_mixinOdooClient.update(clientModel);
        Rating_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Rating_mixin> list){
    }

    @Override
    public boolean remove(Integer id) {
        rating_mixinClientModel clientModel = new rating_mixinClientModel();
        clientModel.setId(id);
		rating_mixinOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Rating_mixin> searchDefault(Rating_mixinSearchContext context) {
        List<Rating_mixin> list = new ArrayList<Rating_mixin>();
        Page<rating_mixinClientModel> clientModelList = rating_mixinOdooClient.search(context);
        for(rating_mixinClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Rating_mixin>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public rating_mixinClientModel convert2Model(Rating_mixin domain , rating_mixinClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new rating_mixinClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("rating_last_feedbackdirtyflag"))
                model.setRating_last_feedback(domain.getRatingLastFeedback());
            if((Boolean) domain.getExtensionparams().get("rating_countdirtyflag"))
                model.setRating_count(domain.getRatingCount());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("rating_idsdirtyflag"))
                model.setRating_ids(domain.getRatingIds());
            if((Boolean) domain.getExtensionparams().get("rating_last_imagedirtyflag"))
                model.setRating_last_image(domain.getRatingLastImage());
            if((Boolean) domain.getExtensionparams().get("rating_last_valuedirtyflag"))
                model.setRating_last_value(domain.getRatingLastValue());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Rating_mixin convert2Domain( rating_mixinClientModel model ,Rating_mixin domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Rating_mixin();
        }

        if(model.getRating_last_feedbackDirtyFlag())
            domain.setRatingLastFeedback(model.getRating_last_feedback());
        if(model.getRating_countDirtyFlag())
            domain.setRatingCount(model.getRating_count());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getRating_idsDirtyFlag())
            domain.setRatingIds(model.getRating_ids());
        if(model.getRating_last_imageDirtyFlag())
            domain.setRatingLastImage(model.getRating_last_image());
        if(model.getRating_last_valueDirtyFlag())
            domain.setRatingLastValue(model.getRating_last_value());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        return domain ;
    }

}

    



