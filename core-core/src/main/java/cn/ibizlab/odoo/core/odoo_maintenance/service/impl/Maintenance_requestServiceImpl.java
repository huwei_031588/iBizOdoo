package cn.ibizlab.odoo.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_requestSearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_requestService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_maintenance.client.maintenance_requestOdooClient;
import cn.ibizlab.odoo.core.odoo_maintenance.clientmodel.maintenance_requestClientModel;

/**
 * 实体[保养请求] 服务对象接口实现
 */
@Slf4j
@Service
public class Maintenance_requestServiceImpl implements IMaintenance_requestService {

    @Autowired
    maintenance_requestOdooClient maintenance_requestOdooClient;


    @Override
    public boolean remove(Integer id) {
        maintenance_requestClientModel clientModel = new maintenance_requestClientModel();
        clientModel.setId(id);
		maintenance_requestOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Maintenance_request et) {
        maintenance_requestClientModel clientModel = convert2Model(et,null);
		maintenance_requestOdooClient.create(clientModel);
        Maintenance_request rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Maintenance_request> list){
    }

    @Override
    public boolean update(Maintenance_request et) {
        maintenance_requestClientModel clientModel = convert2Model(et,null);
		maintenance_requestOdooClient.update(clientModel);
        Maintenance_request rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Maintenance_request> list){
    }

    @Override
    public Maintenance_request get(Integer id) {
        maintenance_requestClientModel clientModel = new maintenance_requestClientModel();
        clientModel.setId(id);
		maintenance_requestOdooClient.get(clientModel);
        Maintenance_request et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Maintenance_request();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Maintenance_request> searchDefault(Maintenance_requestSearchContext context) {
        List<Maintenance_request> list = new ArrayList<Maintenance_request>();
        Page<maintenance_requestClientModel> clientModelList = maintenance_requestOdooClient.search(context);
        for(maintenance_requestClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Maintenance_request>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public maintenance_requestClientModel convert2Model(Maintenance_request domain , maintenance_requestClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new maintenance_requestClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("durationdirtyflag"))
                model.setDuration(domain.getDuration());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("schedule_datedirtyflag"))
                model.setSchedule_date(domain.getScheduleDate());
            if((Boolean) domain.getExtensionparams().get("kanban_statedirtyflag"))
                model.setKanban_state(domain.getKanbanState());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("maintenance_typedirtyflag"))
                model.setMaintenance_type(domain.getMaintenanceType());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("close_datedirtyflag"))
                model.setClose_date(domain.getCloseDate());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("request_datedirtyflag"))
                model.setRequest_date(domain.getRequestDate());
            if((Boolean) domain.getExtensionparams().get("archivedirtyflag"))
                model.setArchive(domain.getArchive());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("prioritydirtyflag"))
                model.setPriority(domain.getPriority());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("owner_user_id_textdirtyflag"))
                model.setOwner_user_id_text(domain.getOwnerUserIdText());
            if((Boolean) domain.getExtensionparams().get("employee_id_textdirtyflag"))
                model.setEmployee_id_text(domain.getEmployeeIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("maintenance_team_id_textdirtyflag"))
                model.setMaintenance_team_id_text(domain.getMaintenanceTeamIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("equipment_id_textdirtyflag"))
                model.setEquipment_id_text(domain.getEquipmentIdText());
            if((Boolean) domain.getExtensionparams().get("stage_id_textdirtyflag"))
                model.setStage_id_text(domain.getStageIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("department_id_textdirtyflag"))
                model.setDepartment_id_text(domain.getDepartmentIdText());
            if((Boolean) domain.getExtensionparams().get("employee_iddirtyflag"))
                model.setEmployee_id(domain.getEmployeeId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("equipment_iddirtyflag"))
                model.setEquipment_id(domain.getEquipmentId());
            if((Boolean) domain.getExtensionparams().get("stage_iddirtyflag"))
                model.setStage_id(domain.getStageId());
            if((Boolean) domain.getExtensionparams().get("owner_user_iddirtyflag"))
                model.setOwner_user_id(domain.getOwnerUserId());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("department_iddirtyflag"))
                model.setDepartment_id(domain.getDepartmentId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("maintenance_team_iddirtyflag"))
                model.setMaintenance_team_id(domain.getMaintenanceTeamId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Maintenance_request convert2Domain( maintenance_requestClientModel model ,Maintenance_request domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Maintenance_request();
        }

        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getDurationDirtyFlag())
            domain.setDuration(model.getDuration());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getSchedule_dateDirtyFlag())
            domain.setScheduleDate(model.getSchedule_date());
        if(model.getKanban_stateDirtyFlag())
            domain.setKanbanState(model.getKanban_state());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getMaintenance_typeDirtyFlag())
            domain.setMaintenanceType(model.getMaintenance_type());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getClose_dateDirtyFlag())
            domain.setCloseDate(model.getClose_date());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getRequest_dateDirtyFlag())
            domain.setRequestDate(model.getRequest_date());
        if(model.getArchiveDirtyFlag())
            domain.setArchive(model.getArchive());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getPriorityDirtyFlag())
            domain.setPriority(model.getPriority());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getOwner_user_id_textDirtyFlag())
            domain.setOwnerUserIdText(model.getOwner_user_id_text());
        if(model.getEmployee_id_textDirtyFlag())
            domain.setEmployeeIdText(model.getEmployee_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMaintenance_team_id_textDirtyFlag())
            domain.setMaintenanceTeamIdText(model.getMaintenance_team_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getEquipment_id_textDirtyFlag())
            domain.setEquipmentIdText(model.getEquipment_id_text());
        if(model.getStage_id_textDirtyFlag())
            domain.setStageIdText(model.getStage_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getDepartment_id_textDirtyFlag())
            domain.setDepartmentIdText(model.getDepartment_id_text());
        if(model.getEmployee_idDirtyFlag())
            domain.setEmployeeId(model.getEmployee_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getEquipment_idDirtyFlag())
            domain.setEquipmentId(model.getEquipment_id());
        if(model.getStage_idDirtyFlag())
            domain.setStageId(model.getStage_id());
        if(model.getOwner_user_idDirtyFlag())
            domain.setOwnerUserId(model.getOwner_user_id());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getDepartment_idDirtyFlag())
            domain.setDepartmentId(model.getDepartment_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getMaintenance_team_idDirtyFlag())
            domain.setMaintenanceTeamId(model.getMaintenance_team_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



