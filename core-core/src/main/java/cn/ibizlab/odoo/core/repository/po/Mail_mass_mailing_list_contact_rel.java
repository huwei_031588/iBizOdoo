package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;

/**
 * 实体 [群发邮件订阅信息] 存储模型
 */
public interface Mail_mass_mailing_list_contact_rel{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 取消订阅日期
     */
    Timestamp getUnsubscription_date();

    void setUnsubscription_date(Timestamp unsubscription_date);

    /**
     * 获取 [取消订阅日期]脏标记
     */
    boolean getUnsubscription_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 退出
     */
    String getOpt_out();

    void setOpt_out(String opt_out);

    /**
     * 获取 [退出]脏标记
     */
    boolean getOpt_outDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 被退回
     */
    Integer getMessage_bounce();

    void setMessage_bounce(Integer message_bounce);

    /**
     * 获取 [被退回]脏标记
     */
    boolean getMessage_bounceDirtyFlag();

    /**
     * 黑名单
     */
    String getIs_blacklisted();

    void setIs_blacklisted(String is_blacklisted);

    /**
     * 获取 [黑名单]脏标记
     */
    boolean getIs_blacklistedDirtyFlag();

    /**
     * 邮件列表
     */
    String getList_id_text();

    void setList_id_text(String list_id_text);

    /**
     * 获取 [邮件列表]脏标记
     */
    boolean getList_id_textDirtyFlag();

    /**
     * 联系
     */
    String getContact_id_text();

    void setContact_id_text(String contact_id_text);

    /**
     * 获取 [联系]脏标记
     */
    boolean getContact_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 联系人人数
     */
    Integer getContact_count();

    void setContact_count(Integer contact_count);

    /**
     * 获取 [联系人人数]脏标记
     */
    boolean getContact_countDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 联系
     */
    Integer getContact_id();

    void setContact_id(Integer contact_id);

    /**
     * 获取 [联系]脏标记
     */
    boolean getContact_idDirtyFlag();

    /**
     * 邮件列表
     */
    Integer getList_id();

    void setList_id(Integer list_id);

    /**
     * 获取 [邮件列表]脏标记
     */
    boolean getList_idDirtyFlag();

}
