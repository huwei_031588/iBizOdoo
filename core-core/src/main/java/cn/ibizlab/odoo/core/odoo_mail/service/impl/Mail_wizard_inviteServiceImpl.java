package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_wizard_invite;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_wizard_inviteSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_wizard_inviteService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_wizard_inviteOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_wizard_inviteClientModel;

/**
 * 实体[邀请向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_wizard_inviteServiceImpl implements IMail_wizard_inviteService {

    @Autowired
    mail_wizard_inviteOdooClient mail_wizard_inviteOdooClient;


    @Override
    public Mail_wizard_invite get(Integer id) {
        mail_wizard_inviteClientModel clientModel = new mail_wizard_inviteClientModel();
        clientModel.setId(id);
		mail_wizard_inviteOdooClient.get(clientModel);
        Mail_wizard_invite et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_wizard_invite();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mail_wizard_invite et) {
        mail_wizard_inviteClientModel clientModel = convert2Model(et,null);
		mail_wizard_inviteOdooClient.create(clientModel);
        Mail_wizard_invite rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_wizard_invite> list){
    }

    @Override
    public boolean update(Mail_wizard_invite et) {
        mail_wizard_inviteClientModel clientModel = convert2Model(et,null);
		mail_wizard_inviteOdooClient.update(clientModel);
        Mail_wizard_invite rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_wizard_invite> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_wizard_inviteClientModel clientModel = new mail_wizard_inviteClientModel();
        clientModel.setId(id);
		mail_wizard_inviteOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_wizard_invite> searchDefault(Mail_wizard_inviteSearchContext context) {
        List<Mail_wizard_invite> list = new ArrayList<Mail_wizard_invite>();
        Page<mail_wizard_inviteClientModel> clientModelList = mail_wizard_inviteOdooClient.search(context);
        for(mail_wizard_inviteClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_wizard_invite>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_wizard_inviteClientModel convert2Model(Mail_wizard_invite domain , mail_wizard_inviteClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_wizard_inviteClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("channel_idsdirtyflag"))
                model.setChannel_ids(domain.getChannelIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("messagedirtyflag"))
                model.setMessage(domain.getMessage());
            if((Boolean) domain.getExtensionparams().get("partner_idsdirtyflag"))
                model.setPartner_ids(domain.getPartnerIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("res_modeldirtyflag"))
                model.setRes_model(domain.getResModel());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("send_maildirtyflag"))
                model.setSend_mail(domain.getSendMail());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_wizard_invite convert2Domain( mail_wizard_inviteClientModel model ,Mail_wizard_invite domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_wizard_invite();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getChannel_idsDirtyFlag())
            domain.setChannelIds(model.getChannel_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMessageDirtyFlag())
            domain.setMessage(model.getMessage());
        if(model.getPartner_idsDirtyFlag())
            domain.setPartnerIds(model.getPartner_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getRes_modelDirtyFlag())
            domain.setResModel(model.getRes_model());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getSend_mailDirtyFlag())
            domain.setSendMail(model.getSend_mail());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



