package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.survey_page;

/**
 * 实体[survey_page] 服务对象接口
 */
public interface survey_pageRepository{


    public survey_page createPO() ;
        public void get(String id);

        public void updateBatch(survey_page survey_page);

        public void update(survey_page survey_page);

        public void createBatch(survey_page survey_page);

        public void removeBatch(String id);

        public List<survey_page> search();

        public void remove(String id);

        public void create(survey_page survey_page);


}
