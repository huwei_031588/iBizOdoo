package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_contacts;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_contactsSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_contactsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_contactsOdooClient;
import cn.ibizlab.odoo.core.odoo_calendar.clientmodel.calendar_contactsClientModel;

/**
 * 实体[日历联系人] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_contactsServiceImpl implements ICalendar_contactsService {

    @Autowired
    calendar_contactsOdooClient calendar_contactsOdooClient;


    @Override
    public Calendar_contacts get(Integer id) {
        calendar_contactsClientModel clientModel = new calendar_contactsClientModel();
        clientModel.setId(id);
		calendar_contactsOdooClient.get(clientModel);
        Calendar_contacts et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Calendar_contacts();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        calendar_contactsClientModel clientModel = new calendar_contactsClientModel();
        clientModel.setId(id);
		calendar_contactsOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Calendar_contacts et) {
        calendar_contactsClientModel clientModel = convert2Model(et,null);
		calendar_contactsOdooClient.update(clientModel);
        Calendar_contacts rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Calendar_contacts> list){
    }

    @Override
    public boolean create(Calendar_contacts et) {
        calendar_contactsClientModel clientModel = convert2Model(et,null);
		calendar_contactsOdooClient.create(clientModel);
        Calendar_contacts rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_contacts> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_contacts> searchDefault(Calendar_contactsSearchContext context) {
        List<Calendar_contacts> list = new ArrayList<Calendar_contacts>();
        Page<calendar_contactsClientModel> clientModelList = calendar_contactsOdooClient.search(context);
        for(calendar_contactsClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Calendar_contacts>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public calendar_contactsClientModel convert2Model(Calendar_contacts domain , calendar_contactsClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new calendar_contactsClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Calendar_contacts convert2Domain( calendar_contactsClientModel model ,Calendar_contacts domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Calendar_contacts();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        return domain ;
    }

}

    



