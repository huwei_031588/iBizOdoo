package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.stock_warn_insufficient_qty_unbuild;

/**
 * 实体[stock_warn_insufficient_qty_unbuild] 服务对象接口
 */
public interface stock_warn_insufficient_qty_unbuildRepository{


    public stock_warn_insufficient_qty_unbuild createPO() ;
        public void remove(String id);

        public void updateBatch(stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild);

        public void get(String id);

        public List<stock_warn_insufficient_qty_unbuild> search();

        public void removeBatch(String id);

        public void create(stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild);

        public void createBatch(stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild);

        public void update(stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild);


}
