package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_lang;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_langSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_langService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_langOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_langClientModel;

/**
 * 实体[语言] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_langServiceImpl implements IRes_langService {

    @Autowired
    res_langOdooClient res_langOdooClient;


    @Override
    public boolean update(Res_lang et) {
        res_langClientModel clientModel = convert2Model(et,null);
		res_langOdooClient.update(clientModel);
        Res_lang rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_lang> list){
    }

    @Override
    public boolean create(Res_lang et) {
        res_langClientModel clientModel = convert2Model(et,null);
		res_langOdooClient.create(clientModel);
        Res_lang rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_lang> list){
    }

    @Override
    public boolean remove(Integer id) {
        res_langClientModel clientModel = new res_langClientModel();
        clientModel.setId(id);
		res_langOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Res_lang get(Integer id) {
        res_langClientModel clientModel = new res_langClientModel();
        clientModel.setId(id);
		res_langOdooClient.get(clientModel);
        Res_lang et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_lang();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_lang> searchDefault(Res_langSearchContext context) {
        List<Res_lang> list = new ArrayList<Res_lang>();
        Page<res_langClientModel> clientModelList = res_langOdooClient.search(context);
        for(res_langClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_lang>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_langClientModel convert2Model(Res_lang domain , res_langClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_langClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("translatabledirtyflag"))
                model.setTranslatable(domain.getTranslatable());
            if((Boolean) domain.getExtensionparams().get("thousands_sepdirtyflag"))
                model.setThousands_sep(domain.getThousandsSep());
            if((Boolean) domain.getExtensionparams().get("iso_codedirtyflag"))
                model.setIso_code(domain.getIsoCode());
            if((Boolean) domain.getExtensionparams().get("decimal_pointdirtyflag"))
                model.setDecimal_point(domain.getDecimalPoint());
            if((Boolean) domain.getExtensionparams().get("groupingdirtyflag"))
                model.setGrouping(domain.getGrouping());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("directiondirtyflag"))
                model.setDirection(domain.getDirection());
            if((Boolean) domain.getExtensionparams().get("date_formatdirtyflag"))
                model.setDate_format(domain.getDateFormat());
            if((Boolean) domain.getExtensionparams().get("time_formatdirtyflag"))
                model.setTime_format(domain.getTimeFormat());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("week_startdirtyflag"))
                model.setWeek_start(domain.getWeekStart());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_lang convert2Domain( res_langClientModel model ,Res_lang domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_lang();
        }

        if(model.getTranslatableDirtyFlag())
            domain.setTranslatable(model.getTranslatable());
        if(model.getThousands_sepDirtyFlag())
            domain.setThousandsSep(model.getThousands_sep());
        if(model.getIso_codeDirtyFlag())
            domain.setIsoCode(model.getIso_code());
        if(model.getDecimal_pointDirtyFlag())
            domain.setDecimalPoint(model.getDecimal_point());
        if(model.getGroupingDirtyFlag())
            domain.setGrouping(model.getGrouping());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDirectionDirtyFlag())
            domain.setDirection(model.getDirection());
        if(model.getDate_formatDirtyFlag())
            domain.setDateFormat(model.getDate_format());
        if(model.getTime_formatDirtyFlag())
            domain.setTimeFormat(model.getTime_format());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWeek_startDirtyFlag())
            domain.setWeekStart(model.getWeek_start());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



