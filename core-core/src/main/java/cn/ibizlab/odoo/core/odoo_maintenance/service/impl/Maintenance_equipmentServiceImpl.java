package cn.ibizlab.odoo.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_equipmentService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_maintenance.client.maintenance_equipmentOdooClient;
import cn.ibizlab.odoo.core.odoo_maintenance.clientmodel.maintenance_equipmentClientModel;

/**
 * 实体[保养设备] 服务对象接口实现
 */
@Slf4j
@Service
public class Maintenance_equipmentServiceImpl implements IMaintenance_equipmentService {

    @Autowired
    maintenance_equipmentOdooClient maintenance_equipmentOdooClient;


    @Override
    public boolean remove(Integer id) {
        maintenance_equipmentClientModel clientModel = new maintenance_equipmentClientModel();
        clientModel.setId(id);
		maintenance_equipmentOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Maintenance_equipment get(Integer id) {
        maintenance_equipmentClientModel clientModel = new maintenance_equipmentClientModel();
        clientModel.setId(id);
		maintenance_equipmentOdooClient.get(clientModel);
        Maintenance_equipment et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Maintenance_equipment();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Maintenance_equipment et) {
        maintenance_equipmentClientModel clientModel = convert2Model(et,null);
		maintenance_equipmentOdooClient.create(clientModel);
        Maintenance_equipment rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Maintenance_equipment> list){
    }

    @Override
    public boolean update(Maintenance_equipment et) {
        maintenance_equipmentClientModel clientModel = convert2Model(et,null);
		maintenance_equipmentOdooClient.update(clientModel);
        Maintenance_equipment rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Maintenance_equipment> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Maintenance_equipment> searchDefault(Maintenance_equipmentSearchContext context) {
        List<Maintenance_equipment> list = new ArrayList<Maintenance_equipment>();
        Page<maintenance_equipmentClientModel> clientModelList = maintenance_equipmentOdooClient.search(context);
        for(maintenance_equipmentClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Maintenance_equipment>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public maintenance_equipmentClientModel convert2Model(Maintenance_equipment domain , maintenance_equipmentClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new maintenance_equipmentClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("equipment_assign_todirtyflag"))
                model.setEquipment_assign_to(domain.getEquipmentAssignTo());
            if((Boolean) domain.getExtensionparams().get("locationdirtyflag"))
                model.setLocation(domain.getLocation());
            if((Boolean) domain.getExtensionparams().get("warranty_datedirtyflag"))
                model.setWarranty_date(domain.getWarrantyDate());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("maintenance_durationdirtyflag"))
                model.setMaintenance_duration(domain.getMaintenanceDuration());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("modeldirtyflag"))
                model.setModel(domain.getModel());
            if((Boolean) domain.getExtensionparams().get("maintenance_open_countdirtyflag"))
                model.setMaintenance_open_count(domain.getMaintenanceOpenCount());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("maintenance_countdirtyflag"))
                model.setMaintenance_count(domain.getMaintenanceCount());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("partner_refdirtyflag"))
                model.setPartner_ref(domain.getPartnerRef());
            if((Boolean) domain.getExtensionparams().get("maintenance_idsdirtyflag"))
                model.setMaintenance_ids(domain.getMaintenanceIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("serial_nodirtyflag"))
                model.setSerial_no(domain.getSerialNo());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("costdirtyflag"))
                model.setCost(domain.getCost());
            if((Boolean) domain.getExtensionparams().get("next_action_datedirtyflag"))
                model.setNext_action_date(domain.getNextActionDate());
            if((Boolean) domain.getExtensionparams().get("assign_datedirtyflag"))
                model.setAssign_date(domain.getAssignDate());
            if((Boolean) domain.getExtensionparams().get("effective_datedirtyflag"))
                model.setEffective_date(domain.getEffectiveDate());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("scrap_datedirtyflag"))
                model.setScrap_date(domain.getScrapDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("perioddirtyflag"))
                model.setPeriod(domain.getPeriod());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("department_id_textdirtyflag"))
                model.setDepartment_id_text(domain.getDepartmentIdText());
            if((Boolean) domain.getExtensionparams().get("employee_id_textdirtyflag"))
                model.setEmployee_id_text(domain.getEmployeeIdText());
            if((Boolean) domain.getExtensionparams().get("technician_user_id_textdirtyflag"))
                model.setTechnician_user_id_text(domain.getTechnicianUserIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("owner_user_id_textdirtyflag"))
                model.setOwner_user_id_text(domain.getOwnerUserIdText());
            if((Boolean) domain.getExtensionparams().get("maintenance_team_id_textdirtyflag"))
                model.setMaintenance_team_id_text(domain.getMaintenanceTeamIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("technician_user_iddirtyflag"))
                model.setTechnician_user_id(domain.getTechnicianUserId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("employee_iddirtyflag"))
                model.setEmployee_id(domain.getEmployeeId());
            if((Boolean) domain.getExtensionparams().get("maintenance_team_iddirtyflag"))
                model.setMaintenance_team_id(domain.getMaintenanceTeamId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("owner_user_iddirtyflag"))
                model.setOwner_user_id(domain.getOwnerUserId());
            if((Boolean) domain.getExtensionparams().get("department_iddirtyflag"))
                model.setDepartment_id(domain.getDepartmentId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Maintenance_equipment convert2Domain( maintenance_equipmentClientModel model ,Maintenance_equipment domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Maintenance_equipment();
        }

        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getEquipment_assign_toDirtyFlag())
            domain.setEquipmentAssignTo(model.getEquipment_assign_to());
        if(model.getLocationDirtyFlag())
            domain.setLocation(model.getLocation());
        if(model.getWarranty_dateDirtyFlag())
            domain.setWarrantyDate(model.getWarranty_date());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getMaintenance_durationDirtyFlag())
            domain.setMaintenanceDuration(model.getMaintenance_duration());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getModelDirtyFlag())
            domain.setModel(model.getModel());
        if(model.getMaintenance_open_countDirtyFlag())
            domain.setMaintenanceOpenCount(model.getMaintenance_open_count());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getMaintenance_countDirtyFlag())
            domain.setMaintenanceCount(model.getMaintenance_count());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getPartner_refDirtyFlag())
            domain.setPartnerRef(model.getPartner_ref());
        if(model.getMaintenance_idsDirtyFlag())
            domain.setMaintenanceIds(model.getMaintenance_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getSerial_noDirtyFlag())
            domain.setSerialNo(model.getSerial_no());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getCostDirtyFlag())
            domain.setCost(model.getCost());
        if(model.getNext_action_dateDirtyFlag())
            domain.setNextActionDate(model.getNext_action_date());
        if(model.getAssign_dateDirtyFlag())
            domain.setAssignDate(model.getAssign_date());
        if(model.getEffective_dateDirtyFlag())
            domain.setEffectiveDate(model.getEffective_date());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getScrap_dateDirtyFlag())
            domain.setScrapDate(model.getScrap_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getPeriodDirtyFlag())
            domain.setPeriod(model.getPeriod());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getDepartment_id_textDirtyFlag())
            domain.setDepartmentIdText(model.getDepartment_id_text());
        if(model.getEmployee_id_textDirtyFlag())
            domain.setEmployeeIdText(model.getEmployee_id_text());
        if(model.getTechnician_user_id_textDirtyFlag())
            domain.setTechnicianUserIdText(model.getTechnician_user_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getOwner_user_id_textDirtyFlag())
            domain.setOwnerUserIdText(model.getOwner_user_id_text());
        if(model.getMaintenance_team_id_textDirtyFlag())
            domain.setMaintenanceTeamIdText(model.getMaintenance_team_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getTechnician_user_idDirtyFlag())
            domain.setTechnicianUserId(model.getTechnician_user_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getEmployee_idDirtyFlag())
            domain.setEmployeeId(model.getEmployee_id());
        if(model.getMaintenance_team_idDirtyFlag())
            domain.setMaintenanceTeamId(model.getMaintenance_team_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getOwner_user_idDirtyFlag())
            domain.setOwnerUserId(model.getOwner_user_id());
        if(model.getDepartment_idDirtyFlag())
            domain.setDepartmentId(model.getDepartment_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



