package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_replenish;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_replenishSearchContext;

/**
 * 实体 [补料] 存储对象
 */
public interface Product_replenishRepository extends Repository<Product_replenish> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_replenish> searchDefault(Product_replenishSearchContext context);

    Product_replenish convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish domain , Product_replenish po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish convert2Domain( Product_replenish po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish domain) ;

}
