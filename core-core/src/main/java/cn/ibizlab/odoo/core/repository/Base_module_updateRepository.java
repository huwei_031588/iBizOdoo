package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_module_update;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_updateSearchContext;

/**
 * 实体 [更新模块] 存储对象
 */
public interface Base_module_updateRepository extends Repository<Base_module_update> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_module_update> searchDefault(Base_module_updateSearchContext context);

    Base_module_update convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Base_module_update domain , Base_module_update po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Base_module_update convert2Domain( Base_module_update po ,cn.ibizlab.odoo.core.odoo_base.domain.Base_module_update domain) ;

}
