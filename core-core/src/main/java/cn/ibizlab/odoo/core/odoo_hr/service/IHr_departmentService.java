package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_departmentSearchContext;


/**
 * 实体[Hr_department] 服务对象接口
 */
public interface IHr_departmentService{

    boolean update(Hr_department et) ;
    void updateBatch(List<Hr_department> list) ;
    boolean create(Hr_department et) ;
    void createBatch(List<Hr_department> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Hr_department get(Integer key) ;
    Page<Hr_department> searchDefault(Hr_departmentSearchContext context) ;

}



