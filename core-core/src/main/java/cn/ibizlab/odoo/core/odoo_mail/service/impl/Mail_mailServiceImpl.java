package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mailSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mailService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_mailOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_mailClientModel;

/**
 * 实体[寄出邮件] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mailServiceImpl implements IMail_mailService {

    @Autowired
    mail_mailOdooClient mail_mailOdooClient;


    @Override
    public boolean update(Mail_mail et) {
        mail_mailClientModel clientModel = convert2Model(et,null);
		mail_mailOdooClient.update(clientModel);
        Mail_mail rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_mail> list){
    }

    @Override
    public Mail_mail get(Integer id) {
        mail_mailClientModel clientModel = new mail_mailClientModel();
        clientModel.setId(id);
		mail_mailOdooClient.get(clientModel);
        Mail_mail et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_mail();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mail_mailClientModel clientModel = new mail_mailClientModel();
        clientModel.setId(id);
		mail_mailOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mail_mail et) {
        mail_mailClientModel clientModel = convert2Model(et,null);
		mail_mailOdooClient.create(clientModel);
        Mail_mail rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mail> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mail> searchDefault(Mail_mailSearchContext context) {
        List<Mail_mail> list = new ArrayList<Mail_mail>();
        Page<mail_mailClientModel> clientModelList = mail_mailOdooClient.search(context);
        for(mail_mailClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_mail>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_mailClientModel convert2Model(Mail_mail domain , mail_mailClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_mailClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("notificationdirtyflag"))
                model.setNotification(domain.getNotification());
            if((Boolean) domain.getExtensionparams().get("scheduled_datedirtyflag"))
                model.setScheduled_date(domain.getScheduledDate());
            if((Boolean) domain.getExtensionparams().get("starred_partner_idsdirtyflag"))
                model.setStarred_partner_ids(domain.getStarredPartnerIds());
            if((Boolean) domain.getExtensionparams().get("auto_deletedirtyflag"))
                model.setAuto_delete(domain.getAutoDelete());
            if((Boolean) domain.getExtensionparams().get("body_htmldirtyflag"))
                model.setBody_html(domain.getBodyHtml());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("rating_idsdirtyflag"))
                model.setRating_ids(domain.getRatingIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("partner_idsdirtyflag"))
                model.setPartner_ids(domain.getPartnerIds());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("recipient_idsdirtyflag"))
                model.setRecipient_ids(domain.getRecipientIds());
            if((Boolean) domain.getExtensionparams().get("tracking_value_idsdirtyflag"))
                model.setTracking_value_ids(domain.getTrackingValueIds());
            if((Boolean) domain.getExtensionparams().get("needaction_partner_idsdirtyflag"))
                model.setNeedaction_partner_ids(domain.getNeedactionPartnerIds());
            if((Boolean) domain.getExtensionparams().get("email_ccdirtyflag"))
                model.setEmail_cc(domain.getEmailCc());
            if((Boolean) domain.getExtensionparams().get("attachment_idsdirtyflag"))
                model.setAttachment_ids(domain.getAttachmentIds());
            if((Boolean) domain.getExtensionparams().get("failure_reasondirtyflag"))
                model.setFailure_reason(domain.getFailureReason());
            if((Boolean) domain.getExtensionparams().get("statistics_idsdirtyflag"))
                model.setStatistics_ids(domain.getStatisticsIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("headersdirtyflag"))
                model.setHeaders(domain.getHeaders());
            if((Boolean) domain.getExtensionparams().get("email_todirtyflag"))
                model.setEmail_to(domain.getEmailTo());
            if((Boolean) domain.getExtensionparams().get("channel_idsdirtyflag"))
                model.setChannel_ids(domain.getChannelIds());
            if((Boolean) domain.getExtensionparams().get("notification_idsdirtyflag"))
                model.setNotification_ids(domain.getNotificationIds());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("referencesdirtyflag"))
                model.setReferences(domain.getReferences());
            if((Boolean) domain.getExtensionparams().get("mailing_id_textdirtyflag"))
                model.setMailing_id_text(domain.getMailingIdText());
            if((Boolean) domain.getExtensionparams().get("reply_todirtyflag"))
                model.setReply_to(domain.getReplyTo());
            if((Boolean) domain.getExtensionparams().get("need_moderationdirtyflag"))
                model.setNeed_moderation(domain.getNeedModeration());
            if((Boolean) domain.getExtensionparams().get("message_iddirtyflag"))
                model.setMessage_id(domain.getMessageId());
            if((Boolean) domain.getExtensionparams().get("no_auto_threaddirtyflag"))
                model.setNo_auto_thread(domain.getNoAutoThread());
            if((Boolean) domain.getExtensionparams().get("author_iddirtyflag"))
                model.setAuthor_id(domain.getAuthorId());
            if((Boolean) domain.getExtensionparams().get("email_fromdirtyflag"))
                model.setEmail_from(domain.getEmailFrom());
            if((Boolean) domain.getExtensionparams().get("modeldirtyflag"))
                model.setModel(domain.getModel());
            if((Boolean) domain.getExtensionparams().get("record_namedirtyflag"))
                model.setRecord_name(domain.getRecordName());
            if((Boolean) domain.getExtensionparams().get("starreddirtyflag"))
                model.setStarred(domain.getStarred());
            if((Boolean) domain.getExtensionparams().get("add_signdirtyflag"))
                model.setAdd_sign(domain.getAddSign());
            if((Boolean) domain.getExtensionparams().get("layoutdirtyflag"))
                model.setLayout(domain.getLayout());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("rating_valuedirtyflag"))
                model.setRating_value(domain.getRatingValue());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("bodydirtyflag"))
                model.setBody(domain.getBody());
            if((Boolean) domain.getExtensionparams().get("mail_server_iddirtyflag"))
                model.setMail_server_id(domain.getMailServerId());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("subtype_iddirtyflag"))
                model.setSubtype_id(domain.getSubtypeId());
            if((Boolean) domain.getExtensionparams().get("fetchmail_server_id_textdirtyflag"))
                model.setFetchmail_server_id_text(domain.getFetchmailServerIdText());
            if((Boolean) domain.getExtensionparams().get("has_errordirtyflag"))
                model.setHas_error(domain.getHasError());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("author_avatardirtyflag"))
                model.setAuthor_avatar(domain.getAuthorAvatar());
            if((Boolean) domain.getExtensionparams().get("moderation_statusdirtyflag"))
                model.setModeration_status(domain.getModerationStatus());
            if((Boolean) domain.getExtensionparams().get("mail_activity_type_iddirtyflag"))
                model.setMail_activity_type_id(domain.getMailActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("moderator_iddirtyflag"))
                model.setModerator_id(domain.getModeratorId());
            if((Boolean) domain.getExtensionparams().get("message_typedirtyflag"))
                model.setMessage_type(domain.getMessageType());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("subjectdirtyflag"))
                model.setSubject(domain.getSubject());
            if((Boolean) domain.getExtensionparams().get("needactiondirtyflag"))
                model.setNeedaction(domain.getNeedaction());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("mailing_iddirtyflag"))
                model.setMailing_id(domain.getMailingId());
            if((Boolean) domain.getExtensionparams().get("fetchmail_server_iddirtyflag"))
                model.setFetchmail_server_id(domain.getFetchmailServerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("mail_message_iddirtyflag"))
                model.setMail_message_id(domain.getMailMessageId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_mail convert2Domain( mail_mailClientModel model ,Mail_mail domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_mail();
        }

        if(model.getNotificationDirtyFlag())
            domain.setNotification(model.getNotification());
        if(model.getScheduled_dateDirtyFlag())
            domain.setScheduledDate(model.getScheduled_date());
        if(model.getStarred_partner_idsDirtyFlag())
            domain.setStarredPartnerIds(model.getStarred_partner_ids());
        if(model.getAuto_deleteDirtyFlag())
            domain.setAutoDelete(model.getAuto_delete());
        if(model.getBody_htmlDirtyFlag())
            domain.setBodyHtml(model.getBody_html());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getRating_idsDirtyFlag())
            domain.setRatingIds(model.getRating_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPartner_idsDirtyFlag())
            domain.setPartnerIds(model.getPartner_ids());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getRecipient_idsDirtyFlag())
            domain.setRecipientIds(model.getRecipient_ids());
        if(model.getTracking_value_idsDirtyFlag())
            domain.setTrackingValueIds(model.getTracking_value_ids());
        if(model.getNeedaction_partner_idsDirtyFlag())
            domain.setNeedactionPartnerIds(model.getNeedaction_partner_ids());
        if(model.getEmail_ccDirtyFlag())
            domain.setEmailCc(model.getEmail_cc());
        if(model.getAttachment_idsDirtyFlag())
            domain.setAttachmentIds(model.getAttachment_ids());
        if(model.getFailure_reasonDirtyFlag())
            domain.setFailureReason(model.getFailure_reason());
        if(model.getStatistics_idsDirtyFlag())
            domain.setStatisticsIds(model.getStatistics_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getHeadersDirtyFlag())
            domain.setHeaders(model.getHeaders());
        if(model.getEmail_toDirtyFlag())
            domain.setEmailTo(model.getEmail_to());
        if(model.getChannel_idsDirtyFlag())
            domain.setChannelIds(model.getChannel_ids());
        if(model.getNotification_idsDirtyFlag())
            domain.setNotificationIds(model.getNotification_ids());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getReferencesDirtyFlag())
            domain.setReferences(model.getReferences());
        if(model.getMailing_id_textDirtyFlag())
            domain.setMailingIdText(model.getMailing_id_text());
        if(model.getReply_toDirtyFlag())
            domain.setReplyTo(model.getReply_to());
        if(model.getNeed_moderationDirtyFlag())
            domain.setNeedModeration(model.getNeed_moderation());
        if(model.getMessage_idDirtyFlag())
            domain.setMessageId(model.getMessage_id());
        if(model.getNo_auto_threadDirtyFlag())
            domain.setNoAutoThread(model.getNo_auto_thread());
        if(model.getAuthor_idDirtyFlag())
            domain.setAuthorId(model.getAuthor_id());
        if(model.getEmail_fromDirtyFlag())
            domain.setEmailFrom(model.getEmail_from());
        if(model.getModelDirtyFlag())
            domain.setModel(model.getModel());
        if(model.getRecord_nameDirtyFlag())
            domain.setRecordName(model.getRecord_name());
        if(model.getStarredDirtyFlag())
            domain.setStarred(model.getStarred());
        if(model.getAdd_signDirtyFlag())
            domain.setAddSign(model.getAdd_sign());
        if(model.getLayoutDirtyFlag())
            domain.setLayout(model.getLayout());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getRating_valueDirtyFlag())
            domain.setRatingValue(model.getRating_value());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getBodyDirtyFlag())
            domain.setBody(model.getBody());
        if(model.getMail_server_idDirtyFlag())
            domain.setMailServerId(model.getMail_server_id());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getSubtype_idDirtyFlag())
            domain.setSubtypeId(model.getSubtype_id());
        if(model.getFetchmail_server_id_textDirtyFlag())
            domain.setFetchmailServerIdText(model.getFetchmail_server_id_text());
        if(model.getHas_errorDirtyFlag())
            domain.setHasError(model.getHas_error());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getAuthor_avatarDirtyFlag())
            domain.setAuthorAvatar(model.getAuthor_avatar());
        if(model.getModeration_statusDirtyFlag())
            domain.setModerationStatus(model.getModeration_status());
        if(model.getMail_activity_type_idDirtyFlag())
            domain.setMailActivityTypeId(model.getMail_activity_type_id());
        if(model.getModerator_idDirtyFlag())
            domain.setModeratorId(model.getModerator_id());
        if(model.getMessage_typeDirtyFlag())
            domain.setMessageType(model.getMessage_type());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getSubjectDirtyFlag())
            domain.setSubject(model.getSubject());
        if(model.getNeedactionDirtyFlag())
            domain.setNeedaction(model.getNeedaction());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getMailing_idDirtyFlag())
            domain.setMailingId(model.getMailing_id());
        if(model.getFetchmail_server_idDirtyFlag())
            domain.setFetchmailServerId(model.getFetchmail_server_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getMail_message_idDirtyFlag())
            domain.setMailMessageId(model.getMail_message_id());
        return domain ;
    }

}

    



