package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;

/**
 * 实体 [实时聊天频道规则] 存储模型
 */
public interface Im_livechat_channel_rule{

    /**
     * 匹配的订单
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [匹配的订单]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 自动弹出时间
     */
    Integer getAuto_popup_timer();

    void setAuto_popup_timer(Integer auto_popup_timer);

    /**
     * 获取 [自动弹出时间]脏标记
     */
    boolean getAuto_popup_timerDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * URL的正则表达式
     */
    String getRegex_url();

    void setRegex_url(String regex_url);

    /**
     * 获取 [URL的正则表达式]脏标记
     */
    boolean getRegex_urlDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 动作
     */
    String getAction();

    void setAction(String action);

    /**
     * 获取 [动作]脏标记
     */
    boolean getActionDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 国家
     */
    String getCountry_ids();

    void setCountry_ids(String country_ids);

    /**
     * 获取 [国家]脏标记
     */
    boolean getCountry_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 渠道
     */
    String getChannel_id_text();

    void setChannel_id_text(String channel_id_text);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getChannel_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 渠道
     */
    Integer getChannel_id();

    void setChannel_id(Integer channel_id);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getChannel_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
