package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [survey_mail_compose_message] 对象
 */
public interface survey_mail_compose_message {

    public String getActive_domain();

    public void setActive_domain(String active_domain);

    public String getAdd_sign();

    public void setAdd_sign(String add_sign);

    public String getAttachment_ids();

    public void setAttachment_ids(String attachment_ids);

    public byte[] getAuthor_avatar();

    public void setAuthor_avatar(byte[] author_avatar);

    public Integer getAuthor_id();

    public void setAuthor_id(Integer author_id);

    public String getAuthor_id_text();

    public void setAuthor_id_text(String author_id_text);

    public String getAuto_delete();

    public void setAuto_delete(String auto_delete);

    public String getAuto_delete_message();

    public void setAuto_delete_message(String auto_delete_message);

    public String getBody();

    public void setBody(String body);

    public String getChannel_ids();

    public void setChannel_ids(String channel_ids);

    public String getChild_ids();

    public void setChild_ids(String child_ids);

    public String getComposition_mode();

    public void setComposition_mode(String composition_mode);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate();

    public void setDate(Timestamp date);

    public Timestamp getDate_deadline();

    public void setDate_deadline(Timestamp date_deadline);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmail_from();

    public void setEmail_from(String email_from);

    public String getHas_error();

    public void setHas_error(String has_error);

    public String getIbizpublic();

    public void setIbizpublic(String ibizpublic);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_log();

    public void setIs_log(String is_log);

    public String getLayout();

    public void setLayout(String layout);

    public String getMailing_list_ids();

    public void setMailing_list_ids(String mailing_list_ids);

    public Integer getMail_activity_type_id();

    public void setMail_activity_type_id(Integer mail_activity_type_id);

    public String getMail_activity_type_id_text();

    public void setMail_activity_type_id_text(String mail_activity_type_id_text);

    public Integer getMass_mailing_campaign_id();

    public void setMass_mailing_campaign_id(Integer mass_mailing_campaign_id);

    public String getMass_mailing_campaign_id_text();

    public void setMass_mailing_campaign_id_text(String mass_mailing_campaign_id_text);

    public Integer getMass_mailing_id();

    public void setMass_mailing_id(Integer mass_mailing_id);

    public String getMass_mailing_id_text();

    public void setMass_mailing_id_text(String mass_mailing_id_text);

    public String getMass_mailing_name();

    public void setMass_mailing_name(String mass_mailing_name);

    public String getMessage_id();

    public void setMessage_id(String message_id);

    public String getMessage_type();

    public void setMessage_type(String message_type);

    public String getModel();

    public void setModel(String model);

    public String getModeration_status();

    public void setModeration_status(String moderation_status);

    public Integer getModerator_id();

    public void setModerator_id(Integer moderator_id);

    public String getModerator_id_text();

    public void setModerator_id_text(String moderator_id_text);

    public String getMulti_email();

    public void setMulti_email(String multi_email);

    public String getNeedaction();

    public void setNeedaction(String needaction);

    public String getNeedaction_partner_ids();

    public void setNeedaction_partner_ids(String needaction_partner_ids);

    public String getNeed_moderation();

    public void setNeed_moderation(String need_moderation);

    public String getNotification_ids();

    public void setNotification_ids(String notification_ids);

    public String getNotify();

    public void setNotify(String notify);

    public String getNo_auto_thread();

    public void setNo_auto_thread(String no_auto_thread);

    public Integer getParent_id();

    public void setParent_id(Integer parent_id);

    public String getPartner_ids();

    public void setPartner_ids(String partner_ids);

    public String getPublic_url();

    public void setPublic_url(String public_url);

    public String getPublic_url_html();

    public void setPublic_url_html(String public_url_html);

    public String getRating_ids();

    public void setRating_ids(String rating_ids);

    public Double getRating_value();

    public void setRating_value(Double rating_value);

    public String getRecord_name();

    public void setRecord_name(String record_name);

    public String getReply_to();

    public void setReply_to(String reply_to);

    public Integer getRes_id();

    public void setRes_id(Integer res_id);

    public String getStarred();

    public void setStarred(String starred);

    public String getStarred_partner_ids();

    public void setStarred_partner_ids(String starred_partner_ids);

    public String getSubject();

    public void setSubject(String subject);

    public Integer getSubtype_id();

    public void setSubtype_id(Integer subtype_id);

    public String getSubtype_id_text();

    public void setSubtype_id_text(String subtype_id_text);

    public Integer getSurvey_id();

    public void setSurvey_id(Integer survey_id);

    public Integer getTemplate_id();

    public void setTemplate_id(Integer template_id);

    public String getTemplate_id_text();

    public void setTemplate_id_text(String template_id_text);

    public String getTracking_value_ids();

    public void setTracking_value_ids(String tracking_value_ids);

    public String getUse_active_domain();

    public void setUse_active_domain(String use_active_domain);

    public String getWebsite_published();

    public void setWebsite_published(String website_published);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
