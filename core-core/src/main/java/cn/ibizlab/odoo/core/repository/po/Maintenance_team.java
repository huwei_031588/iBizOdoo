package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_teamSearchContext;

/**
 * 实体 [保养团队] 存储模型
 */
public interface Maintenance_team{

    /**
     * 已阻止请求的数量
     */
    Integer getTodo_request_count_block();

    void setTodo_request_count_block(Integer todo_request_count_block);

    /**
     * 获取 [已阻止请求的数量]脏标记
     */
    boolean getTodo_request_count_blockDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 设备
     */
    String getEquipment_ids();

    void setEquipment_ids(String equipment_ids);

    /**
     * 获取 [设备]脏标记
     */
    boolean getEquipment_idsDirtyFlag();

    /**
     * 团队成员
     */
    String getMember_ids();

    void setMember_ids(String member_ids);

    /**
     * 获取 [团队成员]脏标记
     */
    boolean getMember_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 高优先级的请求数量
     */
    Integer getTodo_request_count_high_priority();

    void setTodo_request_count_high_priority(Integer todo_request_count_high_priority);

    /**
     * 获取 [高优先级的请求数量]脏标记
     */
    boolean getTodo_request_count_high_priorityDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 请求
     */
    String getRequest_ids();

    void setRequest_ids(String request_ids);

    /**
     * 获取 [请求]脏标记
     */
    boolean getRequest_idsDirtyFlag();

    /**
     * 请求数量
     */
    Integer getTodo_request_count();

    void setTodo_request_count(Integer todo_request_count);

    /**
     * 获取 [请求数量]脏标记
     */
    boolean getTodo_request_countDirtyFlag();

    /**
     * 请求
     */
    String getTodo_request_ids();

    void setTodo_request_ids(String todo_request_ids);

    /**
     * 获取 [请求]脏标记
     */
    boolean getTodo_request_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 未计划请求的数量
     */
    Integer getTodo_request_count_unscheduled();

    void setTodo_request_count_unscheduled(Integer todo_request_count_unscheduled);

    /**
     * 获取 [未计划请求的数量]脏标记
     */
    boolean getTodo_request_count_unscheduledDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 已计划请求的数量
     */
    Integer getTodo_request_count_date();

    void setTodo_request_count_date(Integer todo_request_count_date);

    /**
     * 获取 [已计划请求的数量]脏标记
     */
    boolean getTodo_request_count_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
