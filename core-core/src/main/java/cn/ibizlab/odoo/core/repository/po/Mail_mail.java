package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mailSearchContext;

/**
 * 实体 [寄出邮件] 存储模型
 */
public interface Mail_mail{

    /**
     * 通知
     */
    String getNotification();

    void setNotification(String notification);

    /**
     * 获取 [通知]脏标记
     */
    boolean getNotificationDirtyFlag();

    /**
     * 安排的发送日期
     */
    String getScheduled_date();

    void setScheduled_date(String scheduled_date);

    /**
     * 获取 [安排的发送日期]脏标记
     */
    boolean getScheduled_dateDirtyFlag();

    /**
     * 收藏夹
     */
    String getStarred_partner_ids();

    void setStarred_partner_ids(String starred_partner_ids);

    /**
     * 获取 [收藏夹]脏标记
     */
    boolean getStarred_partner_idsDirtyFlag();

    /**
     * 自动删除
     */
    String getAuto_delete();

    void setAuto_delete(String auto_delete);

    /**
     * 获取 [自动删除]脏标记
     */
    boolean getAuto_deleteDirtyFlag();

    /**
     * 富文本内容
     */
    String getBody_html();

    void setBody_html(String body_html);

    /**
     * 获取 [富文本内容]脏标记
     */
    boolean getBody_htmlDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 相关评级
     */
    String getRating_ids();

    void setRating_ids(String rating_ids);

    /**
     * 获取 [相关评级]脏标记
     */
    boolean getRating_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 收件人
     */
    String getPartner_ids();

    void setPartner_ids(String partner_ids);

    /**
     * 获取 [收件人]脏标记
     */
    boolean getPartner_idsDirtyFlag();

    /**
     * 下级消息
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [下级消息]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 至（合作伙伴）
     */
    String getRecipient_ids();

    void setRecipient_ids(String recipient_ids);

    /**
     * 获取 [至（合作伙伴）]脏标记
     */
    boolean getRecipient_idsDirtyFlag();

    /**
     * 追踪值
     */
    String getTracking_value_ids();

    void setTracking_value_ids(String tracking_value_ids);

    /**
     * 获取 [追踪值]脏标记
     */
    boolean getTracking_value_idsDirtyFlag();

    /**
     * 待处理的业务伙伴
     */
    String getNeedaction_partner_ids();

    void setNeedaction_partner_ids(String needaction_partner_ids);

    /**
     * 获取 [待处理的业务伙伴]脏标记
     */
    boolean getNeedaction_partner_idsDirtyFlag();

    /**
     * 抄送
     */
    String getEmail_cc();

    void setEmail_cc(String email_cc);

    /**
     * 获取 [抄送]脏标记
     */
    boolean getEmail_ccDirtyFlag();

    /**
     * 附件
     */
    String getAttachment_ids();

    void setAttachment_ids(String attachment_ids);

    /**
     * 获取 [附件]脏标记
     */
    boolean getAttachment_idsDirtyFlag();

    /**
     * 失败原因
     */
    String getFailure_reason();

    void setFailure_reason(String failure_reason);

    /**
     * 获取 [失败原因]脏标记
     */
    boolean getFailure_reasonDirtyFlag();

    /**
     * 统计信息
     */
    String getStatistics_ids();

    void setStatistics_ids(String statistics_ids);

    /**
     * 获取 [统计信息]脏标记
     */
    boolean getStatistics_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 标题
     */
    String getHeaders();

    void setHeaders(String headers);

    /**
     * 获取 [标题]脏标记
     */
    boolean getHeadersDirtyFlag();

    /**
     * 至
     */
    String getEmail_to();

    void setEmail_to(String email_to);

    /**
     * 获取 [至]脏标记
     */
    boolean getEmail_toDirtyFlag();

    /**
     * 渠道
     */
    String getChannel_ids();

    void setChannel_ids(String channel_ids);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getChannel_idsDirtyFlag();

    /**
     * 通知
     */
    String getNotification_ids();

    void setNotification_ids(String notification_ids);

    /**
     * 获取 [通知]脏标记
     */
    boolean getNotification_idsDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 参考
     */
    String getReferences();

    void setReferences(String references);

    /**
     * 获取 [参考]脏标记
     */
    boolean getReferencesDirtyFlag();

    /**
     * 群发邮件
     */
    String getMailing_id_text();

    void setMailing_id_text(String mailing_id_text);

    /**
     * 获取 [群发邮件]脏标记
     */
    boolean getMailing_id_textDirtyFlag();

    /**
     * 回复 至
     */
    String getReply_to();

    void setReply_to(String reply_to);

    /**
     * 获取 [回复 至]脏标记
     */
    boolean getReply_toDirtyFlag();

    /**
     * 需审核
     */
    String getNeed_moderation();

    void setNeed_moderation(String need_moderation);

    /**
     * 获取 [需审核]脏标记
     */
    boolean getNeed_moderationDirtyFlag();

    /**
     * 消息ID
     */
    String getMessage_id();

    void setMessage_id(String message_id);

    /**
     * 获取 [消息ID]脏标记
     */
    boolean getMessage_idDirtyFlag();

    /**
     * 无响应
     */
    String getNo_auto_thread();

    void setNo_auto_thread(String no_auto_thread);

    /**
     * 获取 [无响应]脏标记
     */
    boolean getNo_auto_threadDirtyFlag();

    /**
     * 作者
     */
    Integer getAuthor_id();

    void setAuthor_id(Integer author_id);

    /**
     * 获取 [作者]脏标记
     */
    boolean getAuthor_idDirtyFlag();

    /**
     * 从
     */
    String getEmail_from();

    void setEmail_from(String email_from);

    /**
     * 获取 [从]脏标记
     */
    boolean getEmail_fromDirtyFlag();

    /**
     * 相关的文档模型
     */
    String getModel();

    void setModel(String model);

    /**
     * 获取 [相关的文档模型]脏标记
     */
    boolean getModelDirtyFlag();

    /**
     * 消息记录名称
     */
    String getRecord_name();

    void setRecord_name(String record_name);

    /**
     * 获取 [消息记录名称]脏标记
     */
    boolean getRecord_nameDirtyFlag();

    /**
     * 加星的邮件
     */
    String getStarred();

    void setStarred(String starred);

    /**
     * 获取 [加星的邮件]脏标记
     */
    boolean getStarredDirtyFlag();

    /**
     * 添加签名
     */
    String getAdd_sign();

    void setAdd_sign(String add_sign);

    /**
     * 获取 [添加签名]脏标记
     */
    boolean getAdd_signDirtyFlag();

    /**
     * 布局
     */
    String getLayout();

    void setLayout(String layout);

    /**
     * 获取 [布局]脏标记
     */
    boolean getLayoutDirtyFlag();

    /**
     * 上级消息
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级消息]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 评级值
     */
    Double getRating_value();

    void setRating_value(Double rating_value);

    /**
     * 获取 [评级值]脏标记
     */
    boolean getRating_valueDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 已发布
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 内容
     */
    String getBody();

    void setBody(String body);

    /**
     * 获取 [内容]脏标记
     */
    boolean getBodyDirtyFlag();

    /**
     * 邮件发送服务器
     */
    Integer getMail_server_id();

    void setMail_server_id(Integer mail_server_id);

    /**
     * 获取 [邮件发送服务器]脏标记
     */
    boolean getMail_server_idDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 相关文档编号
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [相关文档编号]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 子类型
     */
    Integer getSubtype_id();

    void setSubtype_id(Integer subtype_id);

    /**
     * 获取 [子类型]脏标记
     */
    boolean getSubtype_idDirtyFlag();

    /**
     * 收件服务器
     */
    String getFetchmail_server_id_text();

    void setFetchmail_server_id_text(String fetchmail_server_id_text);

    /**
     * 获取 [收件服务器]脏标记
     */
    boolean getFetchmail_server_id_textDirtyFlag();

    /**
     * 有误差
     */
    String getHas_error();

    void setHas_error(String has_error);

    /**
     * 获取 [有误差]脏标记
     */
    boolean getHas_errorDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 作者头像
     */
    byte[] getAuthor_avatar();

    void setAuthor_avatar(byte[] author_avatar);

    /**
     * 获取 [作者头像]脏标记
     */
    boolean getAuthor_avatarDirtyFlag();

    /**
     * 管理状态
     */
    String getModeration_status();

    void setModeration_status(String moderation_status);

    /**
     * 获取 [管理状态]脏标记
     */
    boolean getModeration_statusDirtyFlag();

    /**
     * 邮件活动类型
     */
    Integer getMail_activity_type_id();

    void setMail_activity_type_id(Integer mail_activity_type_id);

    /**
     * 获取 [邮件活动类型]脏标记
     */
    boolean getMail_activity_type_idDirtyFlag();

    /**
     * 管理员
     */
    Integer getModerator_id();

    void setModerator_id(Integer moderator_id);

    /**
     * 获取 [管理员]脏标记
     */
    boolean getModerator_idDirtyFlag();

    /**
     * 类型
     */
    String getMessage_type();

    void setMessage_type(String message_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getMessage_typeDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 主题
     */
    String getSubject();

    void setSubject(String subject);

    /**
     * 获取 [主题]脏标记
     */
    boolean getSubjectDirtyFlag();

    /**
     * 待处理
     */
    String getNeedaction();

    void setNeedaction(String needaction);

    /**
     * 获取 [待处理]脏标记
     */
    boolean getNeedactionDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 群发邮件
     */
    Integer getMailing_id();

    void setMailing_id(Integer mailing_id);

    /**
     * 获取 [群发邮件]脏标记
     */
    boolean getMailing_idDirtyFlag();

    /**
     * 收件服务器
     */
    Integer getFetchmail_server_id();

    void setFetchmail_server_id(Integer fetchmail_server_id);

    /**
     * 获取 [收件服务器]脏标记
     */
    boolean getFetchmail_server_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 消息
     */
    Integer getMail_message_id();

    void setMail_message_id(Integer mail_message_id);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMail_message_idDirtyFlag();

}
