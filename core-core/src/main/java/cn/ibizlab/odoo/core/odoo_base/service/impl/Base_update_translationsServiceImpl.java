package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_update_translations;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_update_translationsSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_update_translationsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.base_update_translationsOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.base_update_translationsClientModel;

/**
 * 实体[更新翻译] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_update_translationsServiceImpl implements IBase_update_translationsService {

    @Autowired
    base_update_translationsOdooClient base_update_translationsOdooClient;


    @Override
    public boolean update(Base_update_translations et) {
        base_update_translationsClientModel clientModel = convert2Model(et,null);
		base_update_translationsOdooClient.update(clientModel);
        Base_update_translations rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Base_update_translations> list){
    }

    @Override
    public Base_update_translations get(Integer id) {
        base_update_translationsClientModel clientModel = new base_update_translationsClientModel();
        clientModel.setId(id);
		base_update_translationsOdooClient.get(clientModel);
        Base_update_translations et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Base_update_translations();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Base_update_translations et) {
        base_update_translationsClientModel clientModel = convert2Model(et,null);
		base_update_translationsOdooClient.create(clientModel);
        Base_update_translations rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_update_translations> list){
    }

    @Override
    public boolean remove(Integer id) {
        base_update_translationsClientModel clientModel = new base_update_translationsClientModel();
        clientModel.setId(id);
		base_update_translationsOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_update_translations> searchDefault(Base_update_translationsSearchContext context) {
        List<Base_update_translations> list = new ArrayList<Base_update_translations>();
        Page<base_update_translationsClientModel> clientModelList = base_update_translationsOdooClient.search(context);
        for(base_update_translationsClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Base_update_translations>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public base_update_translationsClientModel convert2Model(Base_update_translations domain , base_update_translationsClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new base_update_translationsClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("langdirtyflag"))
                model.setLang(domain.getLang());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Base_update_translations convert2Domain( base_update_translationsClientModel model ,Base_update_translations domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Base_update_translations();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getLangDirtyFlag())
            domain.setLang(model.getLang());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



