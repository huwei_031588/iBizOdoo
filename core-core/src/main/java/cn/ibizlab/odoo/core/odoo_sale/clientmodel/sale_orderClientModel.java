package cn.ibizlab.odoo.core.odoo_sale.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[sale_order] 对象
 */
public class sale_orderClientModel implements Serializable{

    /**
     * 安全令牌
     */
    public String access_token;

    @JsonIgnore
    public boolean access_tokenDirtyFlag;
    
    /**
     * 门户访问网址
     */
    public String access_url;

    @JsonIgnore
    public boolean access_urlDirtyFlag;
    
    /**
     * 访问警告
     */
    public String access_warning;

    @JsonIgnore
    public boolean access_warningDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 按组分配税额
     */
    public byte[] amount_by_group;

    @JsonIgnore
    public boolean amount_by_groupDirtyFlag;
    
    /**
     * 税率
     */
    public Double amount_tax;

    @JsonIgnore
    public boolean amount_taxDirtyFlag;
    
    /**
     * 总计
     */
    public Double amount_total;

    @JsonIgnore
    public boolean amount_totalDirtyFlag;
    
    /**
     * 折扣前金额
     */
    public Double amount_undiscounted;

    @JsonIgnore
    public boolean amount_undiscountedDirtyFlag;
    
    /**
     * 未税金额
     */
    public Double amount_untaxed;

    @JsonIgnore
    public boolean amount_untaxedDirtyFlag;
    
    /**
     * 分析账户
     */
    public Integer analytic_account_id;

    @JsonIgnore
    public boolean analytic_account_idDirtyFlag;
    
    /**
     * 分析账户
     */
    public String analytic_account_id_text;

    @JsonIgnore
    public boolean analytic_account_id_textDirtyFlag;
    
    /**
     * 已授权的交易
     */
    public String authorized_transaction_ids;

    @JsonIgnore
    public boolean authorized_transaction_idsDirtyFlag;
    
    /**
     * 营销
     */
    public Integer campaign_id;

    @JsonIgnore
    public boolean campaign_idDirtyFlag;
    
    /**
     * 营销
     */
    public String campaign_id_text;

    @JsonIgnore
    public boolean campaign_id_textDirtyFlag;
    
    /**
     * 购物车数量
     */
    public Integer cart_quantity;

    @JsonIgnore
    public boolean cart_quantityDirtyFlag;
    
    /**
     * 购物车恢复EMail已发送
     */
    public String cart_recovery_email_sent;

    @JsonIgnore
    public boolean cart_recovery_email_sentDirtyFlag;
    
    /**
     * 客户订单号
     */
    public String client_order_ref;

    @JsonIgnore
    public boolean client_order_refDirtyFlag;
    
    /**
     * 承诺日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp commitment_date;

    @JsonIgnore
    public boolean commitment_dateDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 确认日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp confirmation_date;

    @JsonIgnore
    public boolean confirmation_dateDirtyFlag;
    
    /**
     * 创建日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 汇率
     */
    public Double currency_rate;

    @JsonIgnore
    public boolean currency_rateDirtyFlag;
    
    /**
     * 单据日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_order;

    @JsonIgnore
    public boolean date_orderDirtyFlag;
    
    /**
     * 出库单
     */
    public Integer delivery_count;

    @JsonIgnore
    public boolean delivery_countDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 实际日期
     */
    public Timestamp effective_date;

    @JsonIgnore
    public boolean effective_dateDirtyFlag;
    
    /**
     * 预计日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp expected_date;

    @JsonIgnore
    public boolean expected_dateDirtyFlag;
    
    /**
     * # 费用
     */
    public Integer expense_count;

    @JsonIgnore
    public boolean expense_countDirtyFlag;
    
    /**
     * 费用
     */
    public String expense_ids;

    @JsonIgnore
    public boolean expense_idsDirtyFlag;
    
    /**
     * 税科目调整
     */
    public Integer fiscal_position_id;

    @JsonIgnore
    public boolean fiscal_position_idDirtyFlag;
    
    /**
     * 税科目调整
     */
    public String fiscal_position_id_text;

    @JsonIgnore
    public boolean fiscal_position_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 贸易条款
     */
    public Integer incoterm;

    @JsonIgnore
    public boolean incotermDirtyFlag;
    
    /**
     * 贸易条款
     */
    public String incoterm_text;

    @JsonIgnore
    public boolean incoterm_textDirtyFlag;
    
    /**
     * 发票数
     */
    public Integer invoice_count;

    @JsonIgnore
    public boolean invoice_countDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_ids;

    @JsonIgnore
    public boolean invoice_idsDirtyFlag;
    
    /**
     * 发票状态
     */
    public String invoice_status;

    @JsonIgnore
    public boolean invoice_statusDirtyFlag;
    
    /**
     * 遗弃的购物车
     */
    public String is_abandoned_cart;

    @JsonIgnore
    public boolean is_abandoned_cartDirtyFlag;
    
    /**
     * 过期了
     */
    public String is_expired;

    @JsonIgnore
    public boolean is_expiredDirtyFlag;
    
    /**
     * 媒体
     */
    public Integer medium_id;

    @JsonIgnore
    public boolean medium_idDirtyFlag;
    
    /**
     * 媒体
     */
    public String medium_id_text;

    @JsonIgnore
    public boolean medium_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 订单关联
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 条款和条件
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 只是服务
     */
    public String only_services;

    @JsonIgnore
    public boolean only_servicesDirtyFlag;
    
    /**
     * 商机
     */
    public Integer opportunity_id;

    @JsonIgnore
    public boolean opportunity_idDirtyFlag;
    
    /**
     * 商机
     */
    public String opportunity_id_text;

    @JsonIgnore
    public boolean opportunity_id_textDirtyFlag;
    
    /**
     * 订单行
     */
    public String order_line;

    @JsonIgnore
    public boolean order_lineDirtyFlag;
    
    /**
     * 源文档
     */
    public String origin;

    @JsonIgnore
    public boolean originDirtyFlag;
    
    /**
     * 客户
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 客户
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 发票地址
     */
    public Integer partner_invoice_id;

    @JsonIgnore
    public boolean partner_invoice_idDirtyFlag;
    
    /**
     * 发票地址
     */
    public String partner_invoice_id_text;

    @JsonIgnore
    public boolean partner_invoice_id_textDirtyFlag;
    
    /**
     * 送货地址
     */
    public Integer partner_shipping_id;

    @JsonIgnore
    public boolean partner_shipping_idDirtyFlag;
    
    /**
     * 送货地址
     */
    public String partner_shipping_id_text;

    @JsonIgnore
    public boolean partner_shipping_id_textDirtyFlag;
    
    /**
     * 付款条款
     */
    public Integer payment_term_id;

    @JsonIgnore
    public boolean payment_term_idDirtyFlag;
    
    /**
     * 付款条款
     */
    public String payment_term_id_text;

    @JsonIgnore
    public boolean payment_term_id_textDirtyFlag;
    
    /**
     * 拣货
     */
    public String picking_ids;

    @JsonIgnore
    public boolean picking_idsDirtyFlag;
    
    /**
     * 送货策略
     */
    public String picking_policy;

    @JsonIgnore
    public boolean picking_policyDirtyFlag;
    
    /**
     * 价格表
     */
    public Integer pricelist_id;

    @JsonIgnore
    public boolean pricelist_idDirtyFlag;
    
    /**
     * 价格表
     */
    public String pricelist_id_text;

    @JsonIgnore
    public boolean pricelist_id_textDirtyFlag;
    
    /**
     * 补货组
     */
    public Integer procurement_group_id;

    @JsonIgnore
    public boolean procurement_group_idDirtyFlag;
    
    /**
     * 采购订单号
     */
    public Integer purchase_order_count;

    @JsonIgnore
    public boolean purchase_order_countDirtyFlag;
    
    /**
     * 付款参考:
     */
    public String reference;

    @JsonIgnore
    public boolean referenceDirtyFlag;
    
    /**
     * 剩余确认天数
     */
    public Integer remaining_validity_days;

    @JsonIgnore
    public boolean remaining_validity_daysDirtyFlag;
    
    /**
     * 在线支付
     */
    public String require_payment;

    @JsonIgnore
    public boolean require_paymentDirtyFlag;
    
    /**
     * 在线签名
     */
    public String require_signature;

    @JsonIgnore
    public boolean require_signatureDirtyFlag;
    
    /**
     * 可选产品行
     */
    public String sale_order_option_ids;

    @JsonIgnore
    public boolean sale_order_option_idsDirtyFlag;
    
    /**
     * 报价单模板
     */
    public Integer sale_order_template_id;

    @JsonIgnore
    public boolean sale_order_template_idDirtyFlag;
    
    /**
     * 报价单模板
     */
    public String sale_order_template_id_text;

    @JsonIgnore
    public boolean sale_order_template_id_textDirtyFlag;
    
    /**
     * 签名
     */
    public byte[] signature;

    @JsonIgnore
    public boolean signatureDirtyFlag;
    
    /**
     * 已签核
     */
    public String signed_by;

    @JsonIgnore
    public boolean signed_byDirtyFlag;
    
    /**
     * 来源
     */
    public Integer source_id;

    @JsonIgnore
    public boolean source_idDirtyFlag;
    
    /**
     * 来源
     */
    public String source_id_text;

    @JsonIgnore
    public boolean source_id_textDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 标签
     */
    public String tag_ids;

    @JsonIgnore
    public boolean tag_idsDirtyFlag;
    
    /**
     * 销售团队
     */
    public Integer team_id;

    @JsonIgnore
    public boolean team_idDirtyFlag;
    
    /**
     * 销售团队
     */
    public String team_id_text;

    @JsonIgnore
    public boolean team_id_textDirtyFlag;
    
    /**
     * 交易
     */
    public String transaction_ids;

    @JsonIgnore
    public boolean transaction_idsDirtyFlag;
    
    /**
     * 类型名称
     */
    public String type_name;

    @JsonIgnore
    public boolean type_nameDirtyFlag;
    
    /**
     * 销售员
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 销售员
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 验证
     */
    public Timestamp validity_date;

    @JsonIgnore
    public boolean validity_dateDirtyFlag;
    
    /**
     * 仓库
     */
    public Integer warehouse_id;

    @JsonIgnore
    public boolean warehouse_idDirtyFlag;
    
    /**
     * 仓库
     */
    public String warehouse_id_text;

    @JsonIgnore
    public boolean warehouse_id_textDirtyFlag;
    
    /**
     * 警告
     */
    public String warning_stock;

    @JsonIgnore
    public boolean warning_stockDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 网页显示订单明细
     */
    public String website_order_line;

    @JsonIgnore
    public boolean website_order_lineDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [安全令牌]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return this.access_token ;
    }

    /**
     * 设置 [安全令牌]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

     /**
     * 获取 [安全令牌]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return this.access_tokenDirtyFlag ;
    }   

    /**
     * 获取 [门户访问网址]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return this.access_url ;
    }

    /**
     * 设置 [门户访问网址]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

     /**
     * 获取 [门户访问网址]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return this.access_urlDirtyFlag ;
    }   

    /**
     * 获取 [访问警告]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return this.access_warning ;
    }

    /**
     * 设置 [访问警告]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

     /**
     * 获取 [访问警告]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return this.access_warningDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [按组分配税额]
     */
    @JsonProperty("amount_by_group")
    public byte[] getAmount_by_group(){
        return this.amount_by_group ;
    }

    /**
     * 设置 [按组分配税额]
     */
    @JsonProperty("amount_by_group")
    public void setAmount_by_group(byte[]  amount_by_group){
        this.amount_by_group = amount_by_group ;
        this.amount_by_groupDirtyFlag = true ;
    }

     /**
     * 获取 [按组分配税额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_by_groupDirtyFlag(){
        return this.amount_by_groupDirtyFlag ;
    }   

    /**
     * 获取 [税率]
     */
    @JsonProperty("amount_tax")
    public Double getAmount_tax(){
        return this.amount_tax ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("amount_tax")
    public void setAmount_tax(Double  amount_tax){
        this.amount_tax = amount_tax ;
        this.amount_taxDirtyFlag = true ;
    }

     /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getAmount_taxDirtyFlag(){
        return this.amount_taxDirtyFlag ;
    }   

    /**
     * 获取 [总计]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return this.amount_total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

     /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return this.amount_totalDirtyFlag ;
    }   

    /**
     * 获取 [折扣前金额]
     */
    @JsonProperty("amount_undiscounted")
    public Double getAmount_undiscounted(){
        return this.amount_undiscounted ;
    }

    /**
     * 设置 [折扣前金额]
     */
    @JsonProperty("amount_undiscounted")
    public void setAmount_undiscounted(Double  amount_undiscounted){
        this.amount_undiscounted = amount_undiscounted ;
        this.amount_undiscountedDirtyFlag = true ;
    }

     /**
     * 获取 [折扣前金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_undiscountedDirtyFlag(){
        return this.amount_undiscountedDirtyFlag ;
    }   

    /**
     * 获取 [未税金额]
     */
    @JsonProperty("amount_untaxed")
    public Double getAmount_untaxed(){
        return this.amount_untaxed ;
    }

    /**
     * 设置 [未税金额]
     */
    @JsonProperty("amount_untaxed")
    public void setAmount_untaxed(Double  amount_untaxed){
        this.amount_untaxed = amount_untaxed ;
        this.amount_untaxedDirtyFlag = true ;
    }

     /**
     * 获取 [未税金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxedDirtyFlag(){
        return this.amount_untaxedDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("analytic_account_id")
    public Integer getAnalytic_account_id(){
        return this.analytic_account_id ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("analytic_account_id")
    public void setAnalytic_account_id(Integer  analytic_account_id){
        this.analytic_account_id = analytic_account_id ;
        this.analytic_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_idDirtyFlag(){
        return this.analytic_account_idDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("analytic_account_id_text")
    public String getAnalytic_account_id_text(){
        return this.analytic_account_id_text ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("analytic_account_id_text")
    public void setAnalytic_account_id_text(String  analytic_account_id_text){
        this.analytic_account_id_text = analytic_account_id_text ;
        this.analytic_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_id_textDirtyFlag(){
        return this.analytic_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [已授权的交易]
     */
    @JsonProperty("authorized_transaction_ids")
    public String getAuthorized_transaction_ids(){
        return this.authorized_transaction_ids ;
    }

    /**
     * 设置 [已授权的交易]
     */
    @JsonProperty("authorized_transaction_ids")
    public void setAuthorized_transaction_ids(String  authorized_transaction_ids){
        this.authorized_transaction_ids = authorized_transaction_ids ;
        this.authorized_transaction_idsDirtyFlag = true ;
    }

     /**
     * 获取 [已授权的交易]脏标记
     */
    @JsonIgnore
    public boolean getAuthorized_transaction_idsDirtyFlag(){
        return this.authorized_transaction_idsDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return this.campaign_id ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return this.campaign_idDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return this.campaign_id_text ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return this.campaign_id_textDirtyFlag ;
    }   

    /**
     * 获取 [购物车数量]
     */
    @JsonProperty("cart_quantity")
    public Integer getCart_quantity(){
        return this.cart_quantity ;
    }

    /**
     * 设置 [购物车数量]
     */
    @JsonProperty("cart_quantity")
    public void setCart_quantity(Integer  cart_quantity){
        this.cart_quantity = cart_quantity ;
        this.cart_quantityDirtyFlag = true ;
    }

     /**
     * 获取 [购物车数量]脏标记
     */
    @JsonIgnore
    public boolean getCart_quantityDirtyFlag(){
        return this.cart_quantityDirtyFlag ;
    }   

    /**
     * 获取 [购物车恢复EMail已发送]
     */
    @JsonProperty("cart_recovery_email_sent")
    public String getCart_recovery_email_sent(){
        return this.cart_recovery_email_sent ;
    }

    /**
     * 设置 [购物车恢复EMail已发送]
     */
    @JsonProperty("cart_recovery_email_sent")
    public void setCart_recovery_email_sent(String  cart_recovery_email_sent){
        this.cart_recovery_email_sent = cart_recovery_email_sent ;
        this.cart_recovery_email_sentDirtyFlag = true ;
    }

     /**
     * 获取 [购物车恢复EMail已发送]脏标记
     */
    @JsonIgnore
    public boolean getCart_recovery_email_sentDirtyFlag(){
        return this.cart_recovery_email_sentDirtyFlag ;
    }   

    /**
     * 获取 [客户订单号]
     */
    @JsonProperty("client_order_ref")
    public String getClient_order_ref(){
        return this.client_order_ref ;
    }

    /**
     * 设置 [客户订单号]
     */
    @JsonProperty("client_order_ref")
    public void setClient_order_ref(String  client_order_ref){
        this.client_order_ref = client_order_ref ;
        this.client_order_refDirtyFlag = true ;
    }

     /**
     * 获取 [客户订单号]脏标记
     */
    @JsonIgnore
    public boolean getClient_order_refDirtyFlag(){
        return this.client_order_refDirtyFlag ;
    }   

    /**
     * 获取 [承诺日期]
     */
    @JsonProperty("commitment_date")
    public Timestamp getCommitment_date(){
        return this.commitment_date ;
    }

    /**
     * 设置 [承诺日期]
     */
    @JsonProperty("commitment_date")
    public void setCommitment_date(Timestamp  commitment_date){
        this.commitment_date = commitment_date ;
        this.commitment_dateDirtyFlag = true ;
    }

     /**
     * 获取 [承诺日期]脏标记
     */
    @JsonIgnore
    public boolean getCommitment_dateDirtyFlag(){
        return this.commitment_dateDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [确认日期]
     */
    @JsonProperty("confirmation_date")
    public Timestamp getConfirmation_date(){
        return this.confirmation_date ;
    }

    /**
     * 设置 [确认日期]
     */
    @JsonProperty("confirmation_date")
    public void setConfirmation_date(Timestamp  confirmation_date){
        this.confirmation_date = confirmation_date ;
        this.confirmation_dateDirtyFlag = true ;
    }

     /**
     * 获取 [确认日期]脏标记
     */
    @JsonIgnore
    public boolean getConfirmation_dateDirtyFlag(){
        return this.confirmation_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建日期]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建日期]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建日期]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [汇率]
     */
    @JsonProperty("currency_rate")
    public Double getCurrency_rate(){
        return this.currency_rate ;
    }

    /**
     * 设置 [汇率]
     */
    @JsonProperty("currency_rate")
    public void setCurrency_rate(Double  currency_rate){
        this.currency_rate = currency_rate ;
        this.currency_rateDirtyFlag = true ;
    }

     /**
     * 获取 [汇率]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_rateDirtyFlag(){
        return this.currency_rateDirtyFlag ;
    }   

    /**
     * 获取 [单据日期]
     */
    @JsonProperty("date_order")
    public Timestamp getDate_order(){
        return this.date_order ;
    }

    /**
     * 设置 [单据日期]
     */
    @JsonProperty("date_order")
    public void setDate_order(Timestamp  date_order){
        this.date_order = date_order ;
        this.date_orderDirtyFlag = true ;
    }

     /**
     * 获取 [单据日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_orderDirtyFlag(){
        return this.date_orderDirtyFlag ;
    }   

    /**
     * 获取 [出库单]
     */
    @JsonProperty("delivery_count")
    public Integer getDelivery_count(){
        return this.delivery_count ;
    }

    /**
     * 设置 [出库单]
     */
    @JsonProperty("delivery_count")
    public void setDelivery_count(Integer  delivery_count){
        this.delivery_count = delivery_count ;
        this.delivery_countDirtyFlag = true ;
    }

     /**
     * 获取 [出库单]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_countDirtyFlag(){
        return this.delivery_countDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [实际日期]
     */
    @JsonProperty("effective_date")
    public Timestamp getEffective_date(){
        return this.effective_date ;
    }

    /**
     * 设置 [实际日期]
     */
    @JsonProperty("effective_date")
    public void setEffective_date(Timestamp  effective_date){
        this.effective_date = effective_date ;
        this.effective_dateDirtyFlag = true ;
    }

     /**
     * 获取 [实际日期]脏标记
     */
    @JsonIgnore
    public boolean getEffective_dateDirtyFlag(){
        return this.effective_dateDirtyFlag ;
    }   

    /**
     * 获取 [预计日期]
     */
    @JsonProperty("expected_date")
    public Timestamp getExpected_date(){
        return this.expected_date ;
    }

    /**
     * 设置 [预计日期]
     */
    @JsonProperty("expected_date")
    public void setExpected_date(Timestamp  expected_date){
        this.expected_date = expected_date ;
        this.expected_dateDirtyFlag = true ;
    }

     /**
     * 获取 [预计日期]脏标记
     */
    @JsonIgnore
    public boolean getExpected_dateDirtyFlag(){
        return this.expected_dateDirtyFlag ;
    }   

    /**
     * 获取 [# 费用]
     */
    @JsonProperty("expense_count")
    public Integer getExpense_count(){
        return this.expense_count ;
    }

    /**
     * 设置 [# 费用]
     */
    @JsonProperty("expense_count")
    public void setExpense_count(Integer  expense_count){
        this.expense_count = expense_count ;
        this.expense_countDirtyFlag = true ;
    }

     /**
     * 获取 [# 费用]脏标记
     */
    @JsonIgnore
    public boolean getExpense_countDirtyFlag(){
        return this.expense_countDirtyFlag ;
    }   

    /**
     * 获取 [费用]
     */
    @JsonProperty("expense_ids")
    public String getExpense_ids(){
        return this.expense_ids ;
    }

    /**
     * 设置 [费用]
     */
    @JsonProperty("expense_ids")
    public void setExpense_ids(String  expense_ids){
        this.expense_ids = expense_ids ;
        this.expense_idsDirtyFlag = true ;
    }

     /**
     * 获取 [费用]脏标记
     */
    @JsonIgnore
    public boolean getExpense_idsDirtyFlag(){
        return this.expense_idsDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public Integer getFiscal_position_id(){
        return this.fiscal_position_id ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public void setFiscal_position_id(Integer  fiscal_position_id){
        this.fiscal_position_id = fiscal_position_id ;
        this.fiscal_position_idDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_idDirtyFlag(){
        return this.fiscal_position_idDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public String getFiscal_position_id_text(){
        return this.fiscal_position_id_text ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public void setFiscal_position_id_text(String  fiscal_position_id_text){
        this.fiscal_position_id_text = fiscal_position_id_text ;
        this.fiscal_position_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_id_textDirtyFlag(){
        return this.fiscal_position_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [贸易条款]
     */
    @JsonProperty("incoterm")
    public Integer getIncoterm(){
        return this.incoterm ;
    }

    /**
     * 设置 [贸易条款]
     */
    @JsonProperty("incoterm")
    public void setIncoterm(Integer  incoterm){
        this.incoterm = incoterm ;
        this.incotermDirtyFlag = true ;
    }

     /**
     * 获取 [贸易条款]脏标记
     */
    @JsonIgnore
    public boolean getIncotermDirtyFlag(){
        return this.incotermDirtyFlag ;
    }   

    /**
     * 获取 [贸易条款]
     */
    @JsonProperty("incoterm_text")
    public String getIncoterm_text(){
        return this.incoterm_text ;
    }

    /**
     * 设置 [贸易条款]
     */
    @JsonProperty("incoterm_text")
    public void setIncoterm_text(String  incoterm_text){
        this.incoterm_text = incoterm_text ;
        this.incoterm_textDirtyFlag = true ;
    }

     /**
     * 获取 [贸易条款]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_textDirtyFlag(){
        return this.incoterm_textDirtyFlag ;
    }   

    /**
     * 获取 [发票数]
     */
    @JsonProperty("invoice_count")
    public Integer getInvoice_count(){
        return this.invoice_count ;
    }

    /**
     * 设置 [发票数]
     */
    @JsonProperty("invoice_count")
    public void setInvoice_count(Integer  invoice_count){
        this.invoice_count = invoice_count ;
        this.invoice_countDirtyFlag = true ;
    }

     /**
     * 获取 [发票数]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_countDirtyFlag(){
        return this.invoice_countDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return this.invoice_ids ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return this.invoice_idsDirtyFlag ;
    }   

    /**
     * 获取 [发票状态]
     */
    @JsonProperty("invoice_status")
    public String getInvoice_status(){
        return this.invoice_status ;
    }

    /**
     * 设置 [发票状态]
     */
    @JsonProperty("invoice_status")
    public void setInvoice_status(String  invoice_status){
        this.invoice_status = invoice_status ;
        this.invoice_statusDirtyFlag = true ;
    }

     /**
     * 获取 [发票状态]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_statusDirtyFlag(){
        return this.invoice_statusDirtyFlag ;
    }   

    /**
     * 获取 [遗弃的购物车]
     */
    @JsonProperty("is_abandoned_cart")
    public String getIs_abandoned_cart(){
        return this.is_abandoned_cart ;
    }

    /**
     * 设置 [遗弃的购物车]
     */
    @JsonProperty("is_abandoned_cart")
    public void setIs_abandoned_cart(String  is_abandoned_cart){
        this.is_abandoned_cart = is_abandoned_cart ;
        this.is_abandoned_cartDirtyFlag = true ;
    }

     /**
     * 获取 [遗弃的购物车]脏标记
     */
    @JsonIgnore
    public boolean getIs_abandoned_cartDirtyFlag(){
        return this.is_abandoned_cartDirtyFlag ;
    }   

    /**
     * 获取 [过期了]
     */
    @JsonProperty("is_expired")
    public String getIs_expired(){
        return this.is_expired ;
    }

    /**
     * 设置 [过期了]
     */
    @JsonProperty("is_expired")
    public void setIs_expired(String  is_expired){
        this.is_expired = is_expired ;
        this.is_expiredDirtyFlag = true ;
    }

     /**
     * 获取 [过期了]脏标记
     */
    @JsonIgnore
    public boolean getIs_expiredDirtyFlag(){
        return this.is_expiredDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return this.medium_id ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return this.medium_idDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return this.medium_id_text ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return this.medium_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [订单关联]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [订单关联]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [订单关联]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [条款和条件]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [条款和条件]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [条款和条件]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [只是服务]
     */
    @JsonProperty("only_services")
    public String getOnly_services(){
        return this.only_services ;
    }

    /**
     * 设置 [只是服务]
     */
    @JsonProperty("only_services")
    public void setOnly_services(String  only_services){
        this.only_services = only_services ;
        this.only_servicesDirtyFlag = true ;
    }

     /**
     * 获取 [只是服务]脏标记
     */
    @JsonIgnore
    public boolean getOnly_servicesDirtyFlag(){
        return this.only_servicesDirtyFlag ;
    }   

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_id")
    public Integer getOpportunity_id(){
        return this.opportunity_id ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_id")
    public void setOpportunity_id(Integer  opportunity_id){
        this.opportunity_id = opportunity_id ;
        this.opportunity_idDirtyFlag = true ;
    }

     /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_idDirtyFlag(){
        return this.opportunity_idDirtyFlag ;
    }   

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_id_text")
    public String getOpportunity_id_text(){
        return this.opportunity_id_text ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_id_text")
    public void setOpportunity_id_text(String  opportunity_id_text){
        this.opportunity_id_text = opportunity_id_text ;
        this.opportunity_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_id_textDirtyFlag(){
        return this.opportunity_id_textDirtyFlag ;
    }   

    /**
     * 获取 [订单行]
     */
    @JsonProperty("order_line")
    public String getOrder_line(){
        return this.order_line ;
    }

    /**
     * 设置 [订单行]
     */
    @JsonProperty("order_line")
    public void setOrder_line(String  order_line){
        this.order_line = order_line ;
        this.order_lineDirtyFlag = true ;
    }

     /**
     * 获取 [订单行]脏标记
     */
    @JsonIgnore
    public boolean getOrder_lineDirtyFlag(){
        return this.order_lineDirtyFlag ;
    }   

    /**
     * 获取 [源文档]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return this.origin ;
    }

    /**
     * 设置 [源文档]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

     /**
     * 获取 [源文档]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return this.originDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [发票地址]
     */
    @JsonProperty("partner_invoice_id")
    public Integer getPartner_invoice_id(){
        return this.partner_invoice_id ;
    }

    /**
     * 设置 [发票地址]
     */
    @JsonProperty("partner_invoice_id")
    public void setPartner_invoice_id(Integer  partner_invoice_id){
        this.partner_invoice_id = partner_invoice_id ;
        this.partner_invoice_idDirtyFlag = true ;
    }

     /**
     * 获取 [发票地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_invoice_idDirtyFlag(){
        return this.partner_invoice_idDirtyFlag ;
    }   

    /**
     * 获取 [发票地址]
     */
    @JsonProperty("partner_invoice_id_text")
    public String getPartner_invoice_id_text(){
        return this.partner_invoice_id_text ;
    }

    /**
     * 设置 [发票地址]
     */
    @JsonProperty("partner_invoice_id_text")
    public void setPartner_invoice_id_text(String  partner_invoice_id_text){
        this.partner_invoice_id_text = partner_invoice_id_text ;
        this.partner_invoice_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [发票地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_invoice_id_textDirtyFlag(){
        return this.partner_invoice_id_textDirtyFlag ;
    }   

    /**
     * 获取 [送货地址]
     */
    @JsonProperty("partner_shipping_id")
    public Integer getPartner_shipping_id(){
        return this.partner_shipping_id ;
    }

    /**
     * 设置 [送货地址]
     */
    @JsonProperty("partner_shipping_id")
    public void setPartner_shipping_id(Integer  partner_shipping_id){
        this.partner_shipping_id = partner_shipping_id ;
        this.partner_shipping_idDirtyFlag = true ;
    }

     /**
     * 获取 [送货地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_idDirtyFlag(){
        return this.partner_shipping_idDirtyFlag ;
    }   

    /**
     * 获取 [送货地址]
     */
    @JsonProperty("partner_shipping_id_text")
    public String getPartner_shipping_id_text(){
        return this.partner_shipping_id_text ;
    }

    /**
     * 设置 [送货地址]
     */
    @JsonProperty("partner_shipping_id_text")
    public void setPartner_shipping_id_text(String  partner_shipping_id_text){
        this.partner_shipping_id_text = partner_shipping_id_text ;
        this.partner_shipping_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [送货地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_id_textDirtyFlag(){
        return this.partner_shipping_id_textDirtyFlag ;
    }   

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_term_id")
    public Integer getPayment_term_id(){
        return this.payment_term_id ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_term_id")
    public void setPayment_term_id(Integer  payment_term_id){
        this.payment_term_id = payment_term_id ;
        this.payment_term_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_idDirtyFlag(){
        return this.payment_term_idDirtyFlag ;
    }   

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_term_id_text")
    public String getPayment_term_id_text(){
        return this.payment_term_id_text ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_term_id_text")
    public void setPayment_term_id_text(String  payment_term_id_text){
        this.payment_term_id_text = payment_term_id_text ;
        this.payment_term_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_id_textDirtyFlag(){
        return this.payment_term_id_textDirtyFlag ;
    }   

    /**
     * 获取 [拣货]
     */
    @JsonProperty("picking_ids")
    public String getPicking_ids(){
        return this.picking_ids ;
    }

    /**
     * 设置 [拣货]
     */
    @JsonProperty("picking_ids")
    public void setPicking_ids(String  picking_ids){
        this.picking_ids = picking_ids ;
        this.picking_idsDirtyFlag = true ;
    }

     /**
     * 获取 [拣货]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idsDirtyFlag(){
        return this.picking_idsDirtyFlag ;
    }   

    /**
     * 获取 [送货策略]
     */
    @JsonProperty("picking_policy")
    public String getPicking_policy(){
        return this.picking_policy ;
    }

    /**
     * 设置 [送货策略]
     */
    @JsonProperty("picking_policy")
    public void setPicking_policy(String  picking_policy){
        this.picking_policy = picking_policy ;
        this.picking_policyDirtyFlag = true ;
    }

     /**
     * 获取 [送货策略]脏标记
     */
    @JsonIgnore
    public boolean getPicking_policyDirtyFlag(){
        return this.picking_policyDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return this.pricelist_id ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return this.pricelist_idDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public String getPricelist_id_text(){
        return this.pricelist_id_text ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public void setPricelist_id_text(String  pricelist_id_text){
        this.pricelist_id_text = pricelist_id_text ;
        this.pricelist_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_id_textDirtyFlag(){
        return this.pricelist_id_textDirtyFlag ;
    }   

    /**
     * 获取 [补货组]
     */
    @JsonProperty("procurement_group_id")
    public Integer getProcurement_group_id(){
        return this.procurement_group_id ;
    }

    /**
     * 设置 [补货组]
     */
    @JsonProperty("procurement_group_id")
    public void setProcurement_group_id(Integer  procurement_group_id){
        this.procurement_group_id = procurement_group_id ;
        this.procurement_group_idDirtyFlag = true ;
    }

     /**
     * 获取 [补货组]脏标记
     */
    @JsonIgnore
    public boolean getProcurement_group_idDirtyFlag(){
        return this.procurement_group_idDirtyFlag ;
    }   

    /**
     * 获取 [采购订单号]
     */
    @JsonProperty("purchase_order_count")
    public Integer getPurchase_order_count(){
        return this.purchase_order_count ;
    }

    /**
     * 设置 [采购订单号]
     */
    @JsonProperty("purchase_order_count")
    public void setPurchase_order_count(Integer  purchase_order_count){
        this.purchase_order_count = purchase_order_count ;
        this.purchase_order_countDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单号]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_countDirtyFlag(){
        return this.purchase_order_countDirtyFlag ;
    }   

    /**
     * 获取 [付款参考:]
     */
    @JsonProperty("reference")
    public String getReference(){
        return this.reference ;
    }

    /**
     * 设置 [付款参考:]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

     /**
     * 获取 [付款参考:]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return this.referenceDirtyFlag ;
    }   

    /**
     * 获取 [剩余确认天数]
     */
    @JsonProperty("remaining_validity_days")
    public Integer getRemaining_validity_days(){
        return this.remaining_validity_days ;
    }

    /**
     * 设置 [剩余确认天数]
     */
    @JsonProperty("remaining_validity_days")
    public void setRemaining_validity_days(Integer  remaining_validity_days){
        this.remaining_validity_days = remaining_validity_days ;
        this.remaining_validity_daysDirtyFlag = true ;
    }

     /**
     * 获取 [剩余确认天数]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_validity_daysDirtyFlag(){
        return this.remaining_validity_daysDirtyFlag ;
    }   

    /**
     * 获取 [在线支付]
     */
    @JsonProperty("require_payment")
    public String getRequire_payment(){
        return this.require_payment ;
    }

    /**
     * 设置 [在线支付]
     */
    @JsonProperty("require_payment")
    public void setRequire_payment(String  require_payment){
        this.require_payment = require_payment ;
        this.require_paymentDirtyFlag = true ;
    }

     /**
     * 获取 [在线支付]脏标记
     */
    @JsonIgnore
    public boolean getRequire_paymentDirtyFlag(){
        return this.require_paymentDirtyFlag ;
    }   

    /**
     * 获取 [在线签名]
     */
    @JsonProperty("require_signature")
    public String getRequire_signature(){
        return this.require_signature ;
    }

    /**
     * 设置 [在线签名]
     */
    @JsonProperty("require_signature")
    public void setRequire_signature(String  require_signature){
        this.require_signature = require_signature ;
        this.require_signatureDirtyFlag = true ;
    }

     /**
     * 获取 [在线签名]脏标记
     */
    @JsonIgnore
    public boolean getRequire_signatureDirtyFlag(){
        return this.require_signatureDirtyFlag ;
    }   

    /**
     * 获取 [可选产品行]
     */
    @JsonProperty("sale_order_option_ids")
    public String getSale_order_option_ids(){
        return this.sale_order_option_ids ;
    }

    /**
     * 设置 [可选产品行]
     */
    @JsonProperty("sale_order_option_ids")
    public void setSale_order_option_ids(String  sale_order_option_ids){
        this.sale_order_option_ids = sale_order_option_ids ;
        this.sale_order_option_idsDirtyFlag = true ;
    }

     /**
     * 获取 [可选产品行]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_option_idsDirtyFlag(){
        return this.sale_order_option_idsDirtyFlag ;
    }   

    /**
     * 获取 [报价单模板]
     */
    @JsonProperty("sale_order_template_id")
    public Integer getSale_order_template_id(){
        return this.sale_order_template_id ;
    }

    /**
     * 设置 [报价单模板]
     */
    @JsonProperty("sale_order_template_id")
    public void setSale_order_template_id(Integer  sale_order_template_id){
        this.sale_order_template_id = sale_order_template_id ;
        this.sale_order_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [报价单模板]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_idDirtyFlag(){
        return this.sale_order_template_idDirtyFlag ;
    }   

    /**
     * 获取 [报价单模板]
     */
    @JsonProperty("sale_order_template_id_text")
    public String getSale_order_template_id_text(){
        return this.sale_order_template_id_text ;
    }

    /**
     * 设置 [报价单模板]
     */
    @JsonProperty("sale_order_template_id_text")
    public void setSale_order_template_id_text(String  sale_order_template_id_text){
        this.sale_order_template_id_text = sale_order_template_id_text ;
        this.sale_order_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [报价单模板]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_id_textDirtyFlag(){
        return this.sale_order_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [签名]
     */
    @JsonProperty("signature")
    public byte[] getSignature(){
        return this.signature ;
    }

    /**
     * 设置 [签名]
     */
    @JsonProperty("signature")
    public void setSignature(byte[]  signature){
        this.signature = signature ;
        this.signatureDirtyFlag = true ;
    }

     /**
     * 获取 [签名]脏标记
     */
    @JsonIgnore
    public boolean getSignatureDirtyFlag(){
        return this.signatureDirtyFlag ;
    }   

    /**
     * 获取 [已签核]
     */
    @JsonProperty("signed_by")
    public String getSigned_by(){
        return this.signed_by ;
    }

    /**
     * 设置 [已签核]
     */
    @JsonProperty("signed_by")
    public void setSigned_by(String  signed_by){
        this.signed_by = signed_by ;
        this.signed_byDirtyFlag = true ;
    }

     /**
     * 获取 [已签核]脏标记
     */
    @JsonIgnore
    public boolean getSigned_byDirtyFlag(){
        return this.signed_byDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return this.source_id ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return this.source_idDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return this.source_id_text ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return this.source_id_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return this.tag_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return this.tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }   

    /**
     * 获取 [交易]
     */
    @JsonProperty("transaction_ids")
    public String getTransaction_ids(){
        return this.transaction_ids ;
    }

    /**
     * 设置 [交易]
     */
    @JsonProperty("transaction_ids")
    public void setTransaction_ids(String  transaction_ids){
        this.transaction_ids = transaction_ids ;
        this.transaction_idsDirtyFlag = true ;
    }

     /**
     * 获取 [交易]脏标记
     */
    @JsonIgnore
    public boolean getTransaction_idsDirtyFlag(){
        return this.transaction_idsDirtyFlag ;
    }   

    /**
     * 获取 [类型名称]
     */
    @JsonProperty("type_name")
    public String getType_name(){
        return this.type_name ;
    }

    /**
     * 设置 [类型名称]
     */
    @JsonProperty("type_name")
    public void setType_name(String  type_name){
        this.type_name = type_name ;
        this.type_nameDirtyFlag = true ;
    }

     /**
     * 获取 [类型名称]脏标记
     */
    @JsonIgnore
    public boolean getType_nameDirtyFlag(){
        return this.type_nameDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [验证]
     */
    @JsonProperty("validity_date")
    public Timestamp getValidity_date(){
        return this.validity_date ;
    }

    /**
     * 设置 [验证]
     */
    @JsonProperty("validity_date")
    public void setValidity_date(Timestamp  validity_date){
        this.validity_date = validity_date ;
        this.validity_dateDirtyFlag = true ;
    }

     /**
     * 获取 [验证]脏标记
     */
    @JsonIgnore
    public boolean getValidity_dateDirtyFlag(){
        return this.validity_dateDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return this.warehouse_id ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return this.warehouse_idDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return this.warehouse_id_text ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return this.warehouse_id_textDirtyFlag ;
    }   

    /**
     * 获取 [警告]
     */
    @JsonProperty("warning_stock")
    public String getWarning_stock(){
        return this.warning_stock ;
    }

    /**
     * 设置 [警告]
     */
    @JsonProperty("warning_stock")
    public void setWarning_stock(String  warning_stock){
        this.warning_stock = warning_stock ;
        this.warning_stockDirtyFlag = true ;
    }

     /**
     * 获取 [警告]脏标记
     */
    @JsonIgnore
    public boolean getWarning_stockDirtyFlag(){
        return this.warning_stockDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [网页显示订单明细]
     */
    @JsonProperty("website_order_line")
    public String getWebsite_order_line(){
        return this.website_order_line ;
    }

    /**
     * 设置 [网页显示订单明细]
     */
    @JsonProperty("website_order_line")
    public void setWebsite_order_line(String  website_order_line){
        this.website_order_line = website_order_line ;
        this.website_order_lineDirtyFlag = true ;
    }

     /**
     * 获取 [网页显示订单明细]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_order_lineDirtyFlag(){
        return this.website_order_lineDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("access_token") instanceof Boolean)&& map.get("access_token")!=null){
			this.setAccess_token((String)map.get("access_token"));
		}
		if(!(map.get("access_url") instanceof Boolean)&& map.get("access_url")!=null){
			this.setAccess_url((String)map.get("access_url"));
		}
		if(!(map.get("access_warning") instanceof Boolean)&& map.get("access_warning")!=null){
			this.setAccess_warning((String)map.get("access_warning"));
		}
		if(!(map.get("activity_date_deadline") instanceof Boolean)&& map.get("activity_date_deadline")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("activity_date_deadline"));
   			this.setActivity_date_deadline(new Timestamp(parse.getTime()));
		}
		if(!(map.get("activity_ids") instanceof Boolean)&& map.get("activity_ids")!=null){
			Object[] objs = (Object[])map.get("activity_ids");
			if(objs.length > 0){
				Integer[] activity_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setActivity_ids(Arrays.toString(activity_ids).replace(" ",""));
			}
		}
		if(!(map.get("activity_state") instanceof Boolean)&& map.get("activity_state")!=null){
			this.setActivity_state((String)map.get("activity_state"));
		}
		if(!(map.get("activity_summary") instanceof Boolean)&& map.get("activity_summary")!=null){
			this.setActivity_summary((String)map.get("activity_summary"));
		}
		if(!(map.get("activity_type_id") instanceof Boolean)&& map.get("activity_type_id")!=null){
			Object[] objs = (Object[])map.get("activity_type_id");
			if(objs.length > 0){
				this.setActivity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("activity_user_id") instanceof Boolean)&& map.get("activity_user_id")!=null){
			Object[] objs = (Object[])map.get("activity_user_id");
			if(objs.length > 0){
				this.setActivity_user_id((Integer)objs[0]);
			}
		}
		if(!(map.get("amount_by_group") instanceof Boolean)&& map.get("amount_by_group")!=null){
			//暂时忽略
			//this.setAmount_by_group(((String)map.get("amount_by_group")).getBytes("UTF-8"));
		}
		if(!(map.get("amount_tax") instanceof Boolean)&& map.get("amount_tax")!=null){
			this.setAmount_tax((Double)map.get("amount_tax"));
		}
		if(!(map.get("amount_total") instanceof Boolean)&& map.get("amount_total")!=null){
			this.setAmount_total((Double)map.get("amount_total"));
		}
		if(!(map.get("amount_undiscounted") instanceof Boolean)&& map.get("amount_undiscounted")!=null){
			this.setAmount_undiscounted((Double)map.get("amount_undiscounted"));
		}
		if(!(map.get("amount_untaxed") instanceof Boolean)&& map.get("amount_untaxed")!=null){
			this.setAmount_untaxed((Double)map.get("amount_untaxed"));
		}
		if(!(map.get("analytic_account_id") instanceof Boolean)&& map.get("analytic_account_id")!=null){
			Object[] objs = (Object[])map.get("analytic_account_id");
			if(objs.length > 0){
				this.setAnalytic_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("analytic_account_id") instanceof Boolean)&& map.get("analytic_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("analytic_account_id");
			if(objs.length > 1){
				this.setAnalytic_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("authorized_transaction_ids") instanceof Boolean)&& map.get("authorized_transaction_ids")!=null){
			Object[] objs = (Object[])map.get("authorized_transaction_ids");
			if(objs.length > 0){
				Integer[] authorized_transaction_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAuthorized_transaction_ids(Arrays.toString(authorized_transaction_ids).replace(" ",""));
			}
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 0){
				this.setCampaign_id((Integer)objs[0]);
			}
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 1){
				this.setCampaign_id_text((String)objs[1]);
			}
		}
		if(!(map.get("cart_quantity") instanceof Boolean)&& map.get("cart_quantity")!=null){
			this.setCart_quantity((Integer)map.get("cart_quantity"));
		}
		if(map.get("cart_recovery_email_sent") instanceof Boolean){
			this.setCart_recovery_email_sent(((Boolean)map.get("cart_recovery_email_sent"))? "true" : "false");
		}
		if(!(map.get("client_order_ref") instanceof Boolean)&& map.get("client_order_ref")!=null){
			this.setClient_order_ref((String)map.get("client_order_ref"));
		}
		if(!(map.get("commitment_date") instanceof Boolean)&& map.get("commitment_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("commitment_date"));
   			this.setCommitment_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("confirmation_date") instanceof Boolean)&& map.get("confirmation_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("confirmation_date"));
   			this.setConfirmation_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_rate") instanceof Boolean)&& map.get("currency_rate")!=null){
			this.setCurrency_rate((Double)map.get("currency_rate"));
		}
		if(!(map.get("date_order") instanceof Boolean)&& map.get("date_order")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_order"));
   			this.setDate_order(new Timestamp(parse.getTime()));
		}
		if(!(map.get("delivery_count") instanceof Boolean)&& map.get("delivery_count")!=null){
			this.setDelivery_count((Integer)map.get("delivery_count"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("effective_date") instanceof Boolean)&& map.get("effective_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("effective_date"));
   			this.setEffective_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("expected_date") instanceof Boolean)&& map.get("expected_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("expected_date"));
   			this.setExpected_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("expense_count") instanceof Boolean)&& map.get("expense_count")!=null){
			this.setExpense_count((Integer)map.get("expense_count"));
		}
		if(!(map.get("expense_ids") instanceof Boolean)&& map.get("expense_ids")!=null){
			Object[] objs = (Object[])map.get("expense_ids");
			if(objs.length > 0){
				Integer[] expense_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setExpense_ids(Arrays.toString(expense_ids).replace(" ",""));
			}
		}
		if(!(map.get("fiscal_position_id") instanceof Boolean)&& map.get("fiscal_position_id")!=null){
			Object[] objs = (Object[])map.get("fiscal_position_id");
			if(objs.length > 0){
				this.setFiscal_position_id((Integer)objs[0]);
			}
		}
		if(!(map.get("fiscal_position_id") instanceof Boolean)&& map.get("fiscal_position_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("fiscal_position_id");
			if(objs.length > 1){
				this.setFiscal_position_id_text((String)objs[1]);
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("incoterm") instanceof Boolean)&& map.get("incoterm")!=null){
			Object[] objs = (Object[])map.get("incoterm");
			if(objs.length > 0){
				this.setIncoterm((Integer)objs[0]);
			}
		}
		if(!(map.get("incoterm") instanceof Boolean)&& map.get("incoterm")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("incoterm");
			if(objs.length > 1){
				this.setIncoterm_text((String)objs[1]);
			}
		}
		if(!(map.get("invoice_count") instanceof Boolean)&& map.get("invoice_count")!=null){
			this.setInvoice_count((Integer)map.get("invoice_count"));
		}
		if(!(map.get("invoice_ids") instanceof Boolean)&& map.get("invoice_ids")!=null){
			Object[] objs = (Object[])map.get("invoice_ids");
			if(objs.length > 0){
				Integer[] invoice_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setInvoice_ids(Arrays.toString(invoice_ids).replace(" ",""));
			}
		}
		if(!(map.get("invoice_status") instanceof Boolean)&& map.get("invoice_status")!=null){
			this.setInvoice_status((String)map.get("invoice_status"));
		}
		if(map.get("is_abandoned_cart") instanceof Boolean){
			this.setIs_abandoned_cart(((Boolean)map.get("is_abandoned_cart"))? "true" : "false");
		}
		if(map.get("is_expired") instanceof Boolean){
			this.setIs_expired(((Boolean)map.get("is_expired"))? "true" : "false");
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 0){
				this.setMedium_id((Integer)objs[0]);
			}
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 1){
				this.setMedium_id_text((String)objs[1]);
			}
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids).replace(" ",""));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids).replace(" ",""));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids).replace(" ",""));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("note") instanceof Boolean)&& map.get("note")!=null){
			this.setNote((String)map.get("note"));
		}
		if(map.get("only_services") instanceof Boolean){
			this.setOnly_services(((Boolean)map.get("only_services"))? "true" : "false");
		}
		if(!(map.get("opportunity_id") instanceof Boolean)&& map.get("opportunity_id")!=null){
			Object[] objs = (Object[])map.get("opportunity_id");
			if(objs.length > 0){
				this.setOpportunity_id((Integer)objs[0]);
			}
		}
		if(!(map.get("opportunity_id") instanceof Boolean)&& map.get("opportunity_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("opportunity_id");
			if(objs.length > 1){
				this.setOpportunity_id_text((String)objs[1]);
			}
		}
		if(!(map.get("order_line") instanceof Boolean)&& map.get("order_line")!=null){
			Object[] objs = (Object[])map.get("order_line");
			if(objs.length > 0){
				Integer[] order_line = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setOrder_line(Arrays.toString(order_line).replace(" ",""));
			}
		}
		if(!(map.get("origin") instanceof Boolean)&& map.get("origin")!=null){
			this.setOrigin((String)map.get("origin"));
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("partner_invoice_id") instanceof Boolean)&& map.get("partner_invoice_id")!=null){
			Object[] objs = (Object[])map.get("partner_invoice_id");
			if(objs.length > 0){
				this.setPartner_invoice_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_invoice_id") instanceof Boolean)&& map.get("partner_invoice_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_invoice_id");
			if(objs.length > 1){
				this.setPartner_invoice_id_text((String)objs[1]);
			}
		}
		if(!(map.get("partner_shipping_id") instanceof Boolean)&& map.get("partner_shipping_id")!=null){
			Object[] objs = (Object[])map.get("partner_shipping_id");
			if(objs.length > 0){
				this.setPartner_shipping_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_shipping_id") instanceof Boolean)&& map.get("partner_shipping_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_shipping_id");
			if(objs.length > 1){
				this.setPartner_shipping_id_text((String)objs[1]);
			}
		}
		if(!(map.get("payment_term_id") instanceof Boolean)&& map.get("payment_term_id")!=null){
			Object[] objs = (Object[])map.get("payment_term_id");
			if(objs.length > 0){
				this.setPayment_term_id((Integer)objs[0]);
			}
		}
		if(!(map.get("payment_term_id") instanceof Boolean)&& map.get("payment_term_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("payment_term_id");
			if(objs.length > 1){
				this.setPayment_term_id_text((String)objs[1]);
			}
		}
		if(!(map.get("picking_ids") instanceof Boolean)&& map.get("picking_ids")!=null){
			Object[] objs = (Object[])map.get("picking_ids");
			if(objs.length > 0){
				Integer[] picking_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPicking_ids(Arrays.toString(picking_ids).replace(" ",""));
			}
		}
		if(!(map.get("picking_policy") instanceof Boolean)&& map.get("picking_policy")!=null){
			this.setPicking_policy((String)map.get("picking_policy"));
		}
		if(!(map.get("pricelist_id") instanceof Boolean)&& map.get("pricelist_id")!=null){
			Object[] objs = (Object[])map.get("pricelist_id");
			if(objs.length > 0){
				this.setPricelist_id((Integer)objs[0]);
			}
		}
		if(!(map.get("pricelist_id") instanceof Boolean)&& map.get("pricelist_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("pricelist_id");
			if(objs.length > 1){
				this.setPricelist_id_text((String)objs[1]);
			}
		}
		if(!(map.get("procurement_group_id") instanceof Boolean)&& map.get("procurement_group_id")!=null){
			Object[] objs = (Object[])map.get("procurement_group_id");
			if(objs.length > 0){
				this.setProcurement_group_id((Integer)objs[0]);
			}
		}
		if(!(map.get("purchase_order_count") instanceof Boolean)&& map.get("purchase_order_count")!=null){
			this.setPurchase_order_count((Integer)map.get("purchase_order_count"));
		}
		if(!(map.get("reference") instanceof Boolean)&& map.get("reference")!=null){
			this.setReference((String)map.get("reference"));
		}
		if(!(map.get("remaining_validity_days") instanceof Boolean)&& map.get("remaining_validity_days")!=null){
			this.setRemaining_validity_days((Integer)map.get("remaining_validity_days"));
		}
		if(map.get("require_payment") instanceof Boolean){
			this.setRequire_payment(((Boolean)map.get("require_payment"))? "true" : "false");
		}
		if(map.get("require_signature") instanceof Boolean){
			this.setRequire_signature(((Boolean)map.get("require_signature"))? "true" : "false");
		}
		if(!(map.get("sale_order_option_ids") instanceof Boolean)&& map.get("sale_order_option_ids")!=null){
			Object[] objs = (Object[])map.get("sale_order_option_ids");
			if(objs.length > 0){
				Integer[] sale_order_option_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setSale_order_option_ids(Arrays.toString(sale_order_option_ids).replace(" ",""));
			}
		}
		if(!(map.get("sale_order_template_id") instanceof Boolean)&& map.get("sale_order_template_id")!=null){
			Object[] objs = (Object[])map.get("sale_order_template_id");
			if(objs.length > 0){
				this.setSale_order_template_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sale_order_template_id") instanceof Boolean)&& map.get("sale_order_template_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("sale_order_template_id");
			if(objs.length > 1){
				this.setSale_order_template_id_text((String)objs[1]);
			}
		}
		if(!(map.get("signature") instanceof Boolean)&& map.get("signature")!=null){
			//暂时忽略
			//this.setSignature(((String)map.get("signature")).getBytes("UTF-8"));
		}
		if(!(map.get("signed_by") instanceof Boolean)&& map.get("signed_by")!=null){
			this.setSigned_by((String)map.get("signed_by"));
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 0){
				this.setSource_id((Integer)objs[0]);
			}
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 1){
				this.setSource_id_text((String)objs[1]);
			}
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("tag_ids") instanceof Boolean)&& map.get("tag_ids")!=null){
			Object[] objs = (Object[])map.get("tag_ids");
			if(objs.length > 0){
				Integer[] tag_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTag_ids(Arrays.toString(tag_ids).replace(" ",""));
			}
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 0){
				this.setTeam_id((Integer)objs[0]);
			}
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 1){
				this.setTeam_id_text((String)objs[1]);
			}
		}
		if(!(map.get("transaction_ids") instanceof Boolean)&& map.get("transaction_ids")!=null){
			Object[] objs = (Object[])map.get("transaction_ids");
			if(objs.length > 0){
				Integer[] transaction_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTransaction_ids(Arrays.toString(transaction_ids).replace(" ",""));
			}
		}
		if(!(map.get("type_name") instanceof Boolean)&& map.get("type_name")!=null){
			this.setType_name((String)map.get("type_name"));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("validity_date") instanceof Boolean)&& map.get("validity_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("validity_date"));
   			this.setValidity_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("warehouse_id") instanceof Boolean)&& map.get("warehouse_id")!=null){
			Object[] objs = (Object[])map.get("warehouse_id");
			if(objs.length > 0){
				this.setWarehouse_id((Integer)objs[0]);
			}
		}
		if(!(map.get("warehouse_id") instanceof Boolean)&& map.get("warehouse_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("warehouse_id");
			if(objs.length > 1){
				this.setWarehouse_id_text((String)objs[1]);
			}
		}
		if(!(map.get("warning_stock") instanceof Boolean)&& map.get("warning_stock")!=null){
			this.setWarning_stock((String)map.get("warning_stock"));
		}
		if(!(map.get("website_id") instanceof Boolean)&& map.get("website_id")!=null){
			Object[] objs = (Object[])map.get("website_id");
			if(objs.length > 0){
				this.setWebsite_id((Integer)objs[0]);
			}
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids).replace(" ",""));
			}
		}
		if(!(map.get("website_order_line") instanceof Boolean)&& map.get("website_order_line")!=null){
			Object[] objs = (Object[])map.get("website_order_line");
			if(objs.length > 0){
				Integer[] website_order_line = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_order_line(Arrays.toString(website_order_line).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccess_token()!=null&&this.getAccess_tokenDirtyFlag()){
			map.put("access_token",this.getAccess_token());
		}else if(this.getAccess_tokenDirtyFlag()){
			map.put("access_token",false);
		}
		if(this.getAccess_url()!=null&&this.getAccess_urlDirtyFlag()){
			map.put("access_url",this.getAccess_url());
		}else if(this.getAccess_urlDirtyFlag()){
			map.put("access_url",false);
		}
		if(this.getAccess_warning()!=null&&this.getAccess_warningDirtyFlag()){
			map.put("access_warning",this.getAccess_warning());
		}else if(this.getAccess_warningDirtyFlag()){
			map.put("access_warning",false);
		}
		if(this.getActivity_date_deadline()!=null&&this.getActivity_date_deadlineDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getActivity_date_deadline());
			map.put("activity_date_deadline",datetimeStr);
		}else if(this.getActivity_date_deadlineDirtyFlag()){
			map.put("activity_date_deadline",false);
		}
		if(this.getActivity_ids()!=null&&this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",this.getActivity_ids());
		}else if(this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",false);
		}
		if(this.getActivity_state()!=null&&this.getActivity_stateDirtyFlag()){
			map.put("activity_state",this.getActivity_state());
		}else if(this.getActivity_stateDirtyFlag()){
			map.put("activity_state",false);
		}
		if(this.getActivity_summary()!=null&&this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",this.getActivity_summary());
		}else if(this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",false);
		}
		if(this.getActivity_type_id()!=null&&this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",this.getActivity_type_id());
		}else if(this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",false);
		}
		if(this.getActivity_user_id()!=null&&this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",this.getActivity_user_id());
		}else if(this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",false);
		}
		if(this.getAmount_by_group()!=null&&this.getAmount_by_groupDirtyFlag()){
			//暂不支持binary类型amount_by_group
		}else if(this.getAmount_by_groupDirtyFlag()){
			map.put("amount_by_group",false);
		}
		if(this.getAmount_tax()!=null&&this.getAmount_taxDirtyFlag()){
			map.put("amount_tax",this.getAmount_tax());
		}else if(this.getAmount_taxDirtyFlag()){
			map.put("amount_tax",false);
		}
		if(this.getAmount_total()!=null&&this.getAmount_totalDirtyFlag()){
			map.put("amount_total",this.getAmount_total());
		}else if(this.getAmount_totalDirtyFlag()){
			map.put("amount_total",false);
		}
		if(this.getAmount_undiscounted()!=null&&this.getAmount_undiscountedDirtyFlag()){
			map.put("amount_undiscounted",this.getAmount_undiscounted());
		}else if(this.getAmount_undiscountedDirtyFlag()){
			map.put("amount_undiscounted",false);
		}
		if(this.getAmount_untaxed()!=null&&this.getAmount_untaxedDirtyFlag()){
			map.put("amount_untaxed",this.getAmount_untaxed());
		}else if(this.getAmount_untaxedDirtyFlag()){
			map.put("amount_untaxed",false);
		}
		if(this.getAnalytic_account_id()!=null&&this.getAnalytic_account_idDirtyFlag()){
			map.put("analytic_account_id",this.getAnalytic_account_id());
		}else if(this.getAnalytic_account_idDirtyFlag()){
			map.put("analytic_account_id",false);
		}
		if(this.getAnalytic_account_id_text()!=null&&this.getAnalytic_account_id_textDirtyFlag()){
			//忽略文本外键analytic_account_id_text
		}else if(this.getAnalytic_account_id_textDirtyFlag()){
			map.put("analytic_account_id",false);
		}
		if(this.getAuthorized_transaction_ids()!=null&&this.getAuthorized_transaction_idsDirtyFlag()){
			map.put("authorized_transaction_ids",this.getAuthorized_transaction_ids());
		}else if(this.getAuthorized_transaction_idsDirtyFlag()){
			map.put("authorized_transaction_ids",false);
		}
		if(this.getCampaign_id()!=null&&this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",this.getCampaign_id());
		}else if(this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getCampaign_id_text()!=null&&this.getCampaign_id_textDirtyFlag()){
			//忽略文本外键campaign_id_text
		}else if(this.getCampaign_id_textDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getCart_quantity()!=null&&this.getCart_quantityDirtyFlag()){
			map.put("cart_quantity",this.getCart_quantity());
		}else if(this.getCart_quantityDirtyFlag()){
			map.put("cart_quantity",false);
		}
		if(this.getCart_recovery_email_sent()!=null&&this.getCart_recovery_email_sentDirtyFlag()){
			map.put("cart_recovery_email_sent",Boolean.parseBoolean(this.getCart_recovery_email_sent()));		
		}		if(this.getClient_order_ref()!=null&&this.getClient_order_refDirtyFlag()){
			map.put("client_order_ref",this.getClient_order_ref());
		}else if(this.getClient_order_refDirtyFlag()){
			map.put("client_order_ref",false);
		}
		if(this.getCommitment_date()!=null&&this.getCommitment_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCommitment_date());
			map.put("commitment_date",datetimeStr);
		}else if(this.getCommitment_dateDirtyFlag()){
			map.put("commitment_date",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getConfirmation_date()!=null&&this.getConfirmation_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getConfirmation_date());
			map.put("confirmation_date",datetimeStr);
		}else if(this.getConfirmation_dateDirtyFlag()){
			map.put("confirmation_date",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_rate()!=null&&this.getCurrency_rateDirtyFlag()){
			map.put("currency_rate",this.getCurrency_rate());
		}else if(this.getCurrency_rateDirtyFlag()){
			map.put("currency_rate",false);
		}
		if(this.getDate_order()!=null&&this.getDate_orderDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_order());
			map.put("date_order",datetimeStr);
		}else if(this.getDate_orderDirtyFlag()){
			map.put("date_order",false);
		}
		if(this.getDelivery_count()!=null&&this.getDelivery_countDirtyFlag()){
			map.put("delivery_count",this.getDelivery_count());
		}else if(this.getDelivery_countDirtyFlag()){
			map.put("delivery_count",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEffective_date()!=null&&this.getEffective_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getEffective_date());
			map.put("effective_date",datetimeStr);
		}else if(this.getEffective_dateDirtyFlag()){
			map.put("effective_date",false);
		}
		if(this.getExpected_date()!=null&&this.getExpected_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getExpected_date());
			map.put("expected_date",datetimeStr);
		}else if(this.getExpected_dateDirtyFlag()){
			map.put("expected_date",false);
		}
		if(this.getExpense_count()!=null&&this.getExpense_countDirtyFlag()){
			map.put("expense_count",this.getExpense_count());
		}else if(this.getExpense_countDirtyFlag()){
			map.put("expense_count",false);
		}
		if(this.getExpense_ids()!=null&&this.getExpense_idsDirtyFlag()){
			map.put("expense_ids",this.getExpense_ids());
		}else if(this.getExpense_idsDirtyFlag()){
			map.put("expense_ids",false);
		}
		if(this.getFiscal_position_id()!=null&&this.getFiscal_position_idDirtyFlag()){
			map.put("fiscal_position_id",this.getFiscal_position_id());
		}else if(this.getFiscal_position_idDirtyFlag()){
			map.put("fiscal_position_id",false);
		}
		if(this.getFiscal_position_id_text()!=null&&this.getFiscal_position_id_textDirtyFlag()){
			//忽略文本外键fiscal_position_id_text
		}else if(this.getFiscal_position_id_textDirtyFlag()){
			map.put("fiscal_position_id",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIncoterm()!=null&&this.getIncotermDirtyFlag()){
			map.put("incoterm",this.getIncoterm());
		}else if(this.getIncotermDirtyFlag()){
			map.put("incoterm",false);
		}
		if(this.getIncoterm_text()!=null&&this.getIncoterm_textDirtyFlag()){
			//忽略文本外键incoterm_text
		}else if(this.getIncoterm_textDirtyFlag()){
			map.put("incoterm",false);
		}
		if(this.getInvoice_count()!=null&&this.getInvoice_countDirtyFlag()){
			map.put("invoice_count",this.getInvoice_count());
		}else if(this.getInvoice_countDirtyFlag()){
			map.put("invoice_count",false);
		}
		if(this.getInvoice_ids()!=null&&this.getInvoice_idsDirtyFlag()){
			map.put("invoice_ids",this.getInvoice_ids());
		}else if(this.getInvoice_idsDirtyFlag()){
			map.put("invoice_ids",false);
		}
		if(this.getInvoice_status()!=null&&this.getInvoice_statusDirtyFlag()){
			map.put("invoice_status",this.getInvoice_status());
		}else if(this.getInvoice_statusDirtyFlag()){
			map.put("invoice_status",false);
		}
		if(this.getIs_abandoned_cart()!=null&&this.getIs_abandoned_cartDirtyFlag()){
			map.put("is_abandoned_cart",Boolean.parseBoolean(this.getIs_abandoned_cart()));		
		}		if(this.getIs_expired()!=null&&this.getIs_expiredDirtyFlag()){
			map.put("is_expired",Boolean.parseBoolean(this.getIs_expired()));		
		}		if(this.getMedium_id()!=null&&this.getMedium_idDirtyFlag()){
			map.put("medium_id",this.getMedium_id());
		}else if(this.getMedium_idDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getMedium_id_text()!=null&&this.getMedium_id_textDirtyFlag()){
			//忽略文本外键medium_id_text
		}else if(this.getMedium_id_textDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNote()!=null&&this.getNoteDirtyFlag()){
			map.put("note",this.getNote());
		}else if(this.getNoteDirtyFlag()){
			map.put("note",false);
		}
		if(this.getOnly_services()!=null&&this.getOnly_servicesDirtyFlag()){
			map.put("only_services",Boolean.parseBoolean(this.getOnly_services()));		
		}		if(this.getOpportunity_id()!=null&&this.getOpportunity_idDirtyFlag()){
			map.put("opportunity_id",this.getOpportunity_id());
		}else if(this.getOpportunity_idDirtyFlag()){
			map.put("opportunity_id",false);
		}
		if(this.getOpportunity_id_text()!=null&&this.getOpportunity_id_textDirtyFlag()){
			//忽略文本外键opportunity_id_text
		}else if(this.getOpportunity_id_textDirtyFlag()){
			map.put("opportunity_id",false);
		}
		if(this.getOrder_line()!=null&&this.getOrder_lineDirtyFlag()){
			map.put("order_line",this.getOrder_line());
		}else if(this.getOrder_lineDirtyFlag()){
			map.put("order_line",false);
		}
		if(this.getOrigin()!=null&&this.getOriginDirtyFlag()){
			map.put("origin",this.getOrigin());
		}else if(this.getOriginDirtyFlag()){
			map.put("origin",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_invoice_id()!=null&&this.getPartner_invoice_idDirtyFlag()){
			map.put("partner_invoice_id",this.getPartner_invoice_id());
		}else if(this.getPartner_invoice_idDirtyFlag()){
			map.put("partner_invoice_id",false);
		}
		if(this.getPartner_invoice_id_text()!=null&&this.getPartner_invoice_id_textDirtyFlag()){
			//忽略文本外键partner_invoice_id_text
		}else if(this.getPartner_invoice_id_textDirtyFlag()){
			map.put("partner_invoice_id",false);
		}
		if(this.getPartner_shipping_id()!=null&&this.getPartner_shipping_idDirtyFlag()){
			map.put("partner_shipping_id",this.getPartner_shipping_id());
		}else if(this.getPartner_shipping_idDirtyFlag()){
			map.put("partner_shipping_id",false);
		}
		if(this.getPartner_shipping_id_text()!=null&&this.getPartner_shipping_id_textDirtyFlag()){
			//忽略文本外键partner_shipping_id_text
		}else if(this.getPartner_shipping_id_textDirtyFlag()){
			map.put("partner_shipping_id",false);
		}
		if(this.getPayment_term_id()!=null&&this.getPayment_term_idDirtyFlag()){
			map.put("payment_term_id",this.getPayment_term_id());
		}else if(this.getPayment_term_idDirtyFlag()){
			map.put("payment_term_id",false);
		}
		if(this.getPayment_term_id_text()!=null&&this.getPayment_term_id_textDirtyFlag()){
			//忽略文本外键payment_term_id_text
		}else if(this.getPayment_term_id_textDirtyFlag()){
			map.put("payment_term_id",false);
		}
		if(this.getPicking_ids()!=null&&this.getPicking_idsDirtyFlag()){
			map.put("picking_ids",this.getPicking_ids());
		}else if(this.getPicking_idsDirtyFlag()){
			map.put("picking_ids",false);
		}
		if(this.getPicking_policy()!=null&&this.getPicking_policyDirtyFlag()){
			map.put("picking_policy",this.getPicking_policy());
		}else if(this.getPicking_policyDirtyFlag()){
			map.put("picking_policy",false);
		}
		if(this.getPricelist_id()!=null&&this.getPricelist_idDirtyFlag()){
			map.put("pricelist_id",this.getPricelist_id());
		}else if(this.getPricelist_idDirtyFlag()){
			map.put("pricelist_id",false);
		}
		if(this.getPricelist_id_text()!=null&&this.getPricelist_id_textDirtyFlag()){
			//忽略文本外键pricelist_id_text
		}else if(this.getPricelist_id_textDirtyFlag()){
			map.put("pricelist_id",false);
		}
		if(this.getProcurement_group_id()!=null&&this.getProcurement_group_idDirtyFlag()){
			map.put("procurement_group_id",this.getProcurement_group_id());
		}else if(this.getProcurement_group_idDirtyFlag()){
			map.put("procurement_group_id",false);
		}
		if(this.getPurchase_order_count()!=null&&this.getPurchase_order_countDirtyFlag()){
			map.put("purchase_order_count",this.getPurchase_order_count());
		}else if(this.getPurchase_order_countDirtyFlag()){
			map.put("purchase_order_count",false);
		}
		if(this.getReference()!=null&&this.getReferenceDirtyFlag()){
			map.put("reference",this.getReference());
		}else if(this.getReferenceDirtyFlag()){
			map.put("reference",false);
		}
		if(this.getRemaining_validity_days()!=null&&this.getRemaining_validity_daysDirtyFlag()){
			map.put("remaining_validity_days",this.getRemaining_validity_days());
		}else if(this.getRemaining_validity_daysDirtyFlag()){
			map.put("remaining_validity_days",false);
		}
		if(this.getRequire_payment()!=null&&this.getRequire_paymentDirtyFlag()){
			map.put("require_payment",Boolean.parseBoolean(this.getRequire_payment()));		
		}		if(this.getRequire_signature()!=null&&this.getRequire_signatureDirtyFlag()){
			map.put("require_signature",Boolean.parseBoolean(this.getRequire_signature()));		
		}		if(this.getSale_order_option_ids()!=null&&this.getSale_order_option_idsDirtyFlag()){
			map.put("sale_order_option_ids",this.getSale_order_option_ids());
		}else if(this.getSale_order_option_idsDirtyFlag()){
			map.put("sale_order_option_ids",false);
		}
		if(this.getSale_order_template_id()!=null&&this.getSale_order_template_idDirtyFlag()){
			map.put("sale_order_template_id",this.getSale_order_template_id());
		}else if(this.getSale_order_template_idDirtyFlag()){
			map.put("sale_order_template_id",false);
		}
		if(this.getSale_order_template_id_text()!=null&&this.getSale_order_template_id_textDirtyFlag()){
			//忽略文本外键sale_order_template_id_text
		}else if(this.getSale_order_template_id_textDirtyFlag()){
			map.put("sale_order_template_id",false);
		}
		if(this.getSignature()!=null&&this.getSignatureDirtyFlag()){
			//暂不支持binary类型signature
		}else if(this.getSignatureDirtyFlag()){
			map.put("signature",false);
		}
		if(this.getSigned_by()!=null&&this.getSigned_byDirtyFlag()){
			map.put("signed_by",this.getSigned_by());
		}else if(this.getSigned_byDirtyFlag()){
			map.put("signed_by",false);
		}
		if(this.getSource_id()!=null&&this.getSource_idDirtyFlag()){
			map.put("source_id",this.getSource_id());
		}else if(this.getSource_idDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getSource_id_text()!=null&&this.getSource_id_textDirtyFlag()){
			//忽略文本外键source_id_text
		}else if(this.getSource_id_textDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getTag_ids()!=null&&this.getTag_idsDirtyFlag()){
			map.put("tag_ids",this.getTag_ids());
		}else if(this.getTag_idsDirtyFlag()){
			map.put("tag_ids",false);
		}
		if(this.getTeam_id()!=null&&this.getTeam_idDirtyFlag()){
			map.put("team_id",this.getTeam_id());
		}else if(this.getTeam_idDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getTeam_id_text()!=null&&this.getTeam_id_textDirtyFlag()){
			//忽略文本外键team_id_text
		}else if(this.getTeam_id_textDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getTransaction_ids()!=null&&this.getTransaction_idsDirtyFlag()){
			map.put("transaction_ids",this.getTransaction_ids());
		}else if(this.getTransaction_idsDirtyFlag()){
			map.put("transaction_ids",false);
		}
		if(this.getType_name()!=null&&this.getType_nameDirtyFlag()){
			map.put("type_name",this.getType_name());
		}else if(this.getType_nameDirtyFlag()){
			map.put("type_name",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getValidity_date()!=null&&this.getValidity_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getValidity_date());
			map.put("validity_date",datetimeStr);
		}else if(this.getValidity_dateDirtyFlag()){
			map.put("validity_date",false);
		}
		if(this.getWarehouse_id()!=null&&this.getWarehouse_idDirtyFlag()){
			map.put("warehouse_id",this.getWarehouse_id());
		}else if(this.getWarehouse_idDirtyFlag()){
			map.put("warehouse_id",false);
		}
		if(this.getWarehouse_id_text()!=null&&this.getWarehouse_id_textDirtyFlag()){
			//忽略文本外键warehouse_id_text
		}else if(this.getWarehouse_id_textDirtyFlag()){
			map.put("warehouse_id",false);
		}
		if(this.getWarning_stock()!=null&&this.getWarning_stockDirtyFlag()){
			map.put("warning_stock",this.getWarning_stock());
		}else if(this.getWarning_stockDirtyFlag()){
			map.put("warning_stock",false);
		}
		if(this.getWebsite_id()!=null&&this.getWebsite_idDirtyFlag()){
			map.put("website_id",this.getWebsite_id());
		}else if(this.getWebsite_idDirtyFlag()){
			map.put("website_id",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWebsite_order_line()!=null&&this.getWebsite_order_lineDirtyFlag()){
			map.put("website_order_line",this.getWebsite_order_line());
		}else if(this.getWebsite_order_lineDirtyFlag()){
			map.put("website_order_line",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
