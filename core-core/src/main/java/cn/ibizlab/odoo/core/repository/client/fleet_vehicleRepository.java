package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_vehicle;

/**
 * 实体[fleet_vehicle] 服务对象接口
 */
public interface fleet_vehicleRepository{


    public fleet_vehicle createPO() ;
        public void removeBatch(String id);

        public void remove(String id);

        public void createBatch(fleet_vehicle fleet_vehicle);

        public void updateBatch(fleet_vehicle fleet_vehicle);

        public void update(fleet_vehicle fleet_vehicle);

        public void get(String id);

        public void create(fleet_vehicle fleet_vehicle);

        public List<fleet_vehicle> search();


}
