package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_activity_report;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_activity_reportSearchContext;

/**
 * 实体 [CRM活动分析] 存储对象
 */
public interface Crm_activity_reportRepository extends Repository<Crm_activity_report> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_activity_report> searchDefault(Crm_activity_reportSearchContext context);

    Crm_activity_report convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report domain , Crm_activity_report po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report convert2Domain( Crm_activity_report po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report domain) ;

}
