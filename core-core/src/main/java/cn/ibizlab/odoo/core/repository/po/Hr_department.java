package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_departmentSearchContext;

/**
 * 实体 [HR 部门] 存储模型
 */
public interface Hr_department{

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 今日缺勤
     */
    Integer getAbsence_of_today();

    void setAbsence_of_today(Integer absence_of_today);

    /**
     * 获取 [今日缺勤]脏标记
     */
    boolean getAbsence_of_todayDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 待批准休假
     */
    Integer getLeave_to_approve_count();

    void setLeave_to_approve_count(Integer leave_to_approve_count);

    /**
     * 获取 [待批准休假]脏标记
     */
    boolean getLeave_to_approve_countDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 部门名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [部门名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 待批准的费用报告
     */
    Integer getExpense_sheets_to_approve_count();

    void setExpense_sheets_to_approve_count(Integer expense_sheets_to_approve_count);

    /**
     * 获取 [待批准的费用报告]脏标记
     */
    boolean getExpense_sheets_to_approve_countDirtyFlag();

    /**
     * 待批准的分配
     */
    Integer getAllocation_to_approve_count();

    void setAllocation_to_approve_count(Integer allocation_to_approve_count);

    /**
     * 获取 [待批准的分配]脏标记
     */
    boolean getAllocation_to_approve_countDirtyFlag();

    /**
     * 新雇用的员工
     */
    Integer getNew_hired_employee();

    void setNew_hired_employee(Integer new_hired_employee);

    /**
     * 获取 [新雇用的员工]脏标记
     */
    boolean getNew_hired_employeeDirtyFlag();

    /**
     * 笔记
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [笔记]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 子部门
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [子部门]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 预期的员工
     */
    Integer getExpected_employee();

    void setExpected_employee(Integer expected_employee);

    /**
     * 获取 [预期的员工]脏标记
     */
    boolean getExpected_employeeDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 员工总数
     */
    Integer getTotal_employee();

    void setTotal_employee(Integer total_employee);

    /**
     * 获取 [员工总数]脏标记
     */
    boolean getTotal_employeeDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 工作
     */
    String getJobs_ids();

    void setJobs_ids(String jobs_ids);

    /**
     * 获取 [工作]脏标记
     */
    boolean getJobs_idsDirtyFlag();

    /**
     * 信息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [信息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 新申请
     */
    Integer getNew_applicant_count();

    void setNew_applicant_count(Integer new_applicant_count);

    /**
     * 获取 [新申请]脏标记
     */
    boolean getNew_applicant_countDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 完整名称
     */
    String getComplete_name();

    void setComplete_name(String complete_name);

    /**
     * 获取 [完整名称]脏标记
     */
    boolean getComplete_nameDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 会员
     */
    String getMember_ids();

    void setMember_ids(String member_ids);

    /**
     * 获取 [会员]脏标记
     */
    boolean getMember_idsDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 经理
     */
    String getManager_id_text();

    void setManager_id_text(String manager_id_text);

    /**
     * 获取 [经理]脏标记
     */
    boolean getManager_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 上级部门
     */
    String getParent_id_text();

    void setParent_id_text(String parent_id_text);

    /**
     * 获取 [上级部门]脏标记
     */
    boolean getParent_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 经理
     */
    Integer getManager_id();

    void setManager_id(Integer manager_id);

    /**
     * 获取 [经理]脏标记
     */
    boolean getManager_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 上级部门
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级部门]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

}
