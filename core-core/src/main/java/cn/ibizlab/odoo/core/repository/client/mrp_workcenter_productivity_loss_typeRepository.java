package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_workcenter_productivity_loss_type;

/**
 * 实体[mrp_workcenter_productivity_loss_type] 服务对象接口
 */
public interface mrp_workcenter_productivity_loss_typeRepository{


    public mrp_workcenter_productivity_loss_type createPO() ;
        public void createBatch(mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public void create(mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public void removeBatch(String id);

        public void updateBatch(mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public void get(String id);

        public void update(mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type);

        public void remove(String id);

        public List<mrp_workcenter_productivity_loss_type> search();


}
