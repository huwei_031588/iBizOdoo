package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mail_mass_mailing_campaign] 对象
 */
public interface mail_mass_mailing_campaign {

    public Integer getBounced();

    public void setBounced(Integer bounced);

    public Integer getBounced_ratio();

    public void setBounced_ratio(Integer bounced_ratio);

    public Integer getCampaign_id();

    public void setCampaign_id(Integer campaign_id);

    public String getCampaign_id_text();

    public void setCampaign_id_text(String campaign_id_text);

    public Integer getClicks_ratio();

    public void setClicks_ratio(Integer clicks_ratio);

    public Integer getColor();

    public void setColor(Integer color);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getDelivered();

    public void setDelivered(Integer delivered);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getFailed();

    public void setFailed(Integer failed);

    public Integer getId();

    public void setId(Integer id);

    public Integer getIgnored();

    public void setIgnored(Integer ignored);

    public String getMass_mailing_ids();

    public void setMass_mailing_ids(String mass_mailing_ids);

    public Integer getMedium_id();

    public void setMedium_id(Integer medium_id);

    public String getMedium_id_text();

    public void setMedium_id_text(String medium_id_text);

    public String getName();

    public void setName(String name);

    public Integer getOpened();

    public void setOpened(Integer opened);

    public Integer getOpened_ratio();

    public void setOpened_ratio(Integer opened_ratio);

    public Integer getReceived_ratio();

    public void setReceived_ratio(Integer received_ratio);

    public Integer getReplied();

    public void setReplied(Integer replied);

    public Integer getReplied_ratio();

    public void setReplied_ratio(Integer replied_ratio);

    public Integer getScheduled();

    public void setScheduled(Integer scheduled);

    public Integer getSent();

    public void setSent(Integer sent);

    public Integer getSource_id();

    public void setSource_id(Integer source_id);

    public String getSource_id_text();

    public void setSource_id_text(String source_id_text);

    public Integer getStage_id();

    public void setStage_id(Integer stage_id);

    public String getStage_id_text();

    public void setStage_id_text(String stage_id_text);

    public String getTag_ids();

    public void setTag_ids(String tag_ids);

    public Integer getTotal();

    public void setTotal(Integer total);

    public Integer getTotal_mailings();

    public void setTotal_mailings(Integer total_mailings);

    public String getUnique_ab_testing();

    public void setUnique_ab_testing(String unique_ab_testing);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
