package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_bom;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bomSearchContext;

/**
 * 实体 [物料清单] 存储对象
 */
public interface Mrp_bomRepository extends Repository<Mrp_bom> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_bom> searchDefault(Mrp_bomSearchContext context);

    Mrp_bom convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom domain , Mrp_bom po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom convert2Domain( Mrp_bom po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom domain) ;

}
