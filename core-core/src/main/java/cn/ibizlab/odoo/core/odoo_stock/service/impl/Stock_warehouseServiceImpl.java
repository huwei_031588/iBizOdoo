package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouseSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warehouseService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_warehouseOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_warehouseClientModel;

/**
 * 实体[仓库] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warehouseServiceImpl implements IStock_warehouseService {

    @Autowired
    stock_warehouseOdooClient stock_warehouseOdooClient;


    @Override
    public Stock_warehouse get(Integer id) {
        stock_warehouseClientModel clientModel = new stock_warehouseClientModel();
        clientModel.setId(id);
		stock_warehouseOdooClient.get(clientModel);
        Stock_warehouse et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_warehouse();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Stock_warehouse et) {
        stock_warehouseClientModel clientModel = convert2Model(et,null);
		stock_warehouseOdooClient.create(clientModel);
        Stock_warehouse rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warehouse> list){
    }

    @Override
    public boolean update(Stock_warehouse et) {
        stock_warehouseClientModel clientModel = convert2Model(et,null);
		stock_warehouseOdooClient.update(clientModel);
        Stock_warehouse rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_warehouse> list){
    }

    @Override
    public boolean remove(Integer id) {
        stock_warehouseClientModel clientModel = new stock_warehouseClientModel();
        clientModel.setId(id);
		stock_warehouseOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warehouse> searchDefault(Stock_warehouseSearchContext context) {
        List<Stock_warehouse> list = new ArrayList<Stock_warehouse>();
        Page<stock_warehouseClientModel> clientModelList = stock_warehouseOdooClient.search(context);
        for(stock_warehouseClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_warehouse>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_warehouseClientModel convert2Model(Stock_warehouse domain , stock_warehouseClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_warehouseClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("resupply_route_idsdirtyflag"))
                model.setResupply_route_ids(domain.getResupplyRouteIds());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("resupply_wh_idsdirtyflag"))
                model.setResupply_wh_ids(domain.getResupplyWhIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("delivery_stepsdirtyflag"))
                model.setDelivery_steps(domain.getDeliverySteps());
            if((Boolean) domain.getExtensionparams().get("warehouse_countdirtyflag"))
                model.setWarehouse_count(domain.getWarehouseCount());
            if((Boolean) domain.getExtensionparams().get("manufacture_to_resupplydirtyflag"))
                model.setManufacture_to_resupply(domain.getManufactureToResupply());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("route_idsdirtyflag"))
                model.setRoute_ids(domain.getRouteIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("reception_stepsdirtyflag"))
                model.setReception_steps(domain.getReceptionSteps());
            if((Boolean) domain.getExtensionparams().get("buy_to_resupplydirtyflag"))
                model.setBuy_to_resupply(domain.getBuyToResupply());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("manufacture_stepsdirtyflag"))
                model.setManufacture_steps(domain.getManufactureSteps());
            if((Boolean) domain.getExtensionparams().get("show_resupplydirtyflag"))
                model.setShow_resupply(domain.getShowResupply());
            if((Boolean) domain.getExtensionparams().get("view_location_id_textdirtyflag"))
                model.setView_location_id_text(domain.getViewLocationIdText());
            if((Boolean) domain.getExtensionparams().get("wh_input_stock_loc_id_textdirtyflag"))
                model.setWh_input_stock_loc_id_text(domain.getWhInputStockLocIdText());
            if((Boolean) domain.getExtensionparams().get("sam_loc_id_textdirtyflag"))
                model.setSam_loc_id_text(domain.getSamLocIdText());
            if((Boolean) domain.getExtensionparams().get("crossdock_route_id_textdirtyflag"))
                model.setCrossdock_route_id_text(domain.getCrossdockRouteIdText());
            if((Boolean) domain.getExtensionparams().get("pbm_route_id_textdirtyflag"))
                model.setPbm_route_id_text(domain.getPbmRouteIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("pbm_type_id_textdirtyflag"))
                model.setPbm_type_id_text(domain.getPbmTypeIdText());
            if((Boolean) domain.getExtensionparams().get("pick_type_id_textdirtyflag"))
                model.setPick_type_id_text(domain.getPickTypeIdText());
            if((Boolean) domain.getExtensionparams().get("int_type_id_textdirtyflag"))
                model.setInt_type_id_text(domain.getIntTypeIdText());
            if((Boolean) domain.getExtensionparams().get("manu_type_id_textdirtyflag"))
                model.setManu_type_id_text(domain.getManuTypeIdText());
            if((Boolean) domain.getExtensionparams().get("buy_pull_id_textdirtyflag"))
                model.setBuy_pull_id_text(domain.getBuyPullIdText());
            if((Boolean) domain.getExtensionparams().get("wh_pack_stock_loc_id_textdirtyflag"))
                model.setWh_pack_stock_loc_id_text(domain.getWhPackStockLocIdText());
            if((Boolean) domain.getExtensionparams().get("manufacture_pull_id_textdirtyflag"))
                model.setManufacture_pull_id_text(domain.getManufacturePullIdText());
            if((Boolean) domain.getExtensionparams().get("reception_route_id_textdirtyflag"))
                model.setReception_route_id_text(domain.getReceptionRouteIdText());
            if((Boolean) domain.getExtensionparams().get("delivery_route_id_textdirtyflag"))
                model.setDelivery_route_id_text(domain.getDeliveryRouteIdText());
            if((Boolean) domain.getExtensionparams().get("sam_type_id_textdirtyflag"))
                model.setSam_type_id_text(domain.getSamTypeIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("out_type_id_textdirtyflag"))
                model.setOut_type_id_text(domain.getOutTypeIdText());
            if((Boolean) domain.getExtensionparams().get("mto_pull_id_textdirtyflag"))
                model.setMto_pull_id_text(domain.getMtoPullIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("lot_stock_id_textdirtyflag"))
                model.setLot_stock_id_text(domain.getLotStockIdText());
            if((Boolean) domain.getExtensionparams().get("pbm_mto_pull_id_textdirtyflag"))
                model.setPbm_mto_pull_id_text(domain.getPbmMtoPullIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("pbm_loc_id_textdirtyflag"))
                model.setPbm_loc_id_text(domain.getPbmLocIdText());
            if((Boolean) domain.getExtensionparams().get("pack_type_id_textdirtyflag"))
                model.setPack_type_id_text(domain.getPackTypeIdText());
            if((Boolean) domain.getExtensionparams().get("sam_rule_id_textdirtyflag"))
                model.setSam_rule_id_text(domain.getSamRuleIdText());
            if((Boolean) domain.getExtensionparams().get("in_type_id_textdirtyflag"))
                model.setIn_type_id_text(domain.getInTypeIdText());
            if((Boolean) domain.getExtensionparams().get("wh_output_stock_loc_id_textdirtyflag"))
                model.setWh_output_stock_loc_id_text(domain.getWhOutputStockLocIdText());
            if((Boolean) domain.getExtensionparams().get("wh_qc_stock_loc_id_textdirtyflag"))
                model.setWh_qc_stock_loc_id_text(domain.getWhQcStockLocIdText());
            if((Boolean) domain.getExtensionparams().get("wh_output_stock_loc_iddirtyflag"))
                model.setWh_output_stock_loc_id(domain.getWhOutputStockLocId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("pbm_loc_iddirtyflag"))
                model.setPbm_loc_id(domain.getPbmLocId());
            if((Boolean) domain.getExtensionparams().get("view_location_iddirtyflag"))
                model.setView_location_id(domain.getViewLocationId());
            if((Boolean) domain.getExtensionparams().get("out_type_iddirtyflag"))
                model.setOut_type_id(domain.getOutTypeId());
            if((Boolean) domain.getExtensionparams().get("manu_type_iddirtyflag"))
                model.setManu_type_id(domain.getManuTypeId());
            if((Boolean) domain.getExtensionparams().get("pbm_mto_pull_iddirtyflag"))
                model.setPbm_mto_pull_id(domain.getPbmMtoPullId());
            if((Boolean) domain.getExtensionparams().get("mto_pull_iddirtyflag"))
                model.setMto_pull_id(domain.getMtoPullId());
            if((Boolean) domain.getExtensionparams().get("sam_type_iddirtyflag"))
                model.setSam_type_id(domain.getSamTypeId());
            if((Boolean) domain.getExtensionparams().get("manufacture_pull_iddirtyflag"))
                model.setManufacture_pull_id(domain.getManufacturePullId());
            if((Boolean) domain.getExtensionparams().get("sam_loc_iddirtyflag"))
                model.setSam_loc_id(domain.getSamLocId());
            if((Boolean) domain.getExtensionparams().get("buy_pull_iddirtyflag"))
                model.setBuy_pull_id(domain.getBuyPullId());
            if((Boolean) domain.getExtensionparams().get("int_type_iddirtyflag"))
                model.setInt_type_id(domain.getIntTypeId());
            if((Boolean) domain.getExtensionparams().get("wh_qc_stock_loc_iddirtyflag"))
                model.setWh_qc_stock_loc_id(domain.getWhQcStockLocId());
            if((Boolean) domain.getExtensionparams().get("in_type_iddirtyflag"))
                model.setIn_type_id(domain.getInTypeId());
            if((Boolean) domain.getExtensionparams().get("pbm_route_iddirtyflag"))
                model.setPbm_route_id(domain.getPbmRouteId());
            if((Boolean) domain.getExtensionparams().get("pick_type_iddirtyflag"))
                model.setPick_type_id(domain.getPickTypeId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("reception_route_iddirtyflag"))
                model.setReception_route_id(domain.getReceptionRouteId());
            if((Boolean) domain.getExtensionparams().get("pack_type_iddirtyflag"))
                model.setPack_type_id(domain.getPackTypeId());
            if((Boolean) domain.getExtensionparams().get("pbm_type_iddirtyflag"))
                model.setPbm_type_id(domain.getPbmTypeId());
            if((Boolean) domain.getExtensionparams().get("delivery_route_iddirtyflag"))
                model.setDelivery_route_id(domain.getDeliveryRouteId());
            if((Boolean) domain.getExtensionparams().get("wh_input_stock_loc_iddirtyflag"))
                model.setWh_input_stock_loc_id(domain.getWhInputStockLocId());
            if((Boolean) domain.getExtensionparams().get("lot_stock_iddirtyflag"))
                model.setLot_stock_id(domain.getLotStockId());
            if((Boolean) domain.getExtensionparams().get("sam_rule_iddirtyflag"))
                model.setSam_rule_id(domain.getSamRuleId());
            if((Boolean) domain.getExtensionparams().get("crossdock_route_iddirtyflag"))
                model.setCrossdock_route_id(domain.getCrossdockRouteId());
            if((Boolean) domain.getExtensionparams().get("wh_pack_stock_loc_iddirtyflag"))
                model.setWh_pack_stock_loc_id(domain.getWhPackStockLocId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_warehouse convert2Domain( stock_warehouseClientModel model ,Stock_warehouse domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_warehouse();
        }

        if(model.getResupply_route_idsDirtyFlag())
            domain.setResupplyRouteIds(model.getResupply_route_ids());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getResupply_wh_idsDirtyFlag())
            domain.setResupplyWhIds(model.getResupply_wh_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDelivery_stepsDirtyFlag())
            domain.setDeliverySteps(model.getDelivery_steps());
        if(model.getWarehouse_countDirtyFlag())
            domain.setWarehouseCount(model.getWarehouse_count());
        if(model.getManufacture_to_resupplyDirtyFlag())
            domain.setManufactureToResupply(model.getManufacture_to_resupply());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getRoute_idsDirtyFlag())
            domain.setRouteIds(model.getRoute_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getReception_stepsDirtyFlag())
            domain.setReceptionSteps(model.getReception_steps());
        if(model.getBuy_to_resupplyDirtyFlag())
            domain.setBuyToResupply(model.getBuy_to_resupply());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getManufacture_stepsDirtyFlag())
            domain.setManufactureSteps(model.getManufacture_steps());
        if(model.getShow_resupplyDirtyFlag())
            domain.setShowResupply(model.getShow_resupply());
        if(model.getView_location_id_textDirtyFlag())
            domain.setViewLocationIdText(model.getView_location_id_text());
        if(model.getWh_input_stock_loc_id_textDirtyFlag())
            domain.setWhInputStockLocIdText(model.getWh_input_stock_loc_id_text());
        if(model.getSam_loc_id_textDirtyFlag())
            domain.setSamLocIdText(model.getSam_loc_id_text());
        if(model.getCrossdock_route_id_textDirtyFlag())
            domain.setCrossdockRouteIdText(model.getCrossdock_route_id_text());
        if(model.getPbm_route_id_textDirtyFlag())
            domain.setPbmRouteIdText(model.getPbm_route_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPbm_type_id_textDirtyFlag())
            domain.setPbmTypeIdText(model.getPbm_type_id_text());
        if(model.getPick_type_id_textDirtyFlag())
            domain.setPickTypeIdText(model.getPick_type_id_text());
        if(model.getInt_type_id_textDirtyFlag())
            domain.setIntTypeIdText(model.getInt_type_id_text());
        if(model.getManu_type_id_textDirtyFlag())
            domain.setManuTypeIdText(model.getManu_type_id_text());
        if(model.getBuy_pull_id_textDirtyFlag())
            domain.setBuyPullIdText(model.getBuy_pull_id_text());
        if(model.getWh_pack_stock_loc_id_textDirtyFlag())
            domain.setWhPackStockLocIdText(model.getWh_pack_stock_loc_id_text());
        if(model.getManufacture_pull_id_textDirtyFlag())
            domain.setManufacturePullIdText(model.getManufacture_pull_id_text());
        if(model.getReception_route_id_textDirtyFlag())
            domain.setReceptionRouteIdText(model.getReception_route_id_text());
        if(model.getDelivery_route_id_textDirtyFlag())
            domain.setDeliveryRouteIdText(model.getDelivery_route_id_text());
        if(model.getSam_type_id_textDirtyFlag())
            domain.setSamTypeIdText(model.getSam_type_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getOut_type_id_textDirtyFlag())
            domain.setOutTypeIdText(model.getOut_type_id_text());
        if(model.getMto_pull_id_textDirtyFlag())
            domain.setMtoPullIdText(model.getMto_pull_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getLot_stock_id_textDirtyFlag())
            domain.setLotStockIdText(model.getLot_stock_id_text());
        if(model.getPbm_mto_pull_id_textDirtyFlag())
            domain.setPbmMtoPullIdText(model.getPbm_mto_pull_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getPbm_loc_id_textDirtyFlag())
            domain.setPbmLocIdText(model.getPbm_loc_id_text());
        if(model.getPack_type_id_textDirtyFlag())
            domain.setPackTypeIdText(model.getPack_type_id_text());
        if(model.getSam_rule_id_textDirtyFlag())
            domain.setSamRuleIdText(model.getSam_rule_id_text());
        if(model.getIn_type_id_textDirtyFlag())
            domain.setInTypeIdText(model.getIn_type_id_text());
        if(model.getWh_output_stock_loc_id_textDirtyFlag())
            domain.setWhOutputStockLocIdText(model.getWh_output_stock_loc_id_text());
        if(model.getWh_qc_stock_loc_id_textDirtyFlag())
            domain.setWhQcStockLocIdText(model.getWh_qc_stock_loc_id_text());
        if(model.getWh_output_stock_loc_idDirtyFlag())
            domain.setWhOutputStockLocId(model.getWh_output_stock_loc_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPbm_loc_idDirtyFlag())
            domain.setPbmLocId(model.getPbm_loc_id());
        if(model.getView_location_idDirtyFlag())
            domain.setViewLocationId(model.getView_location_id());
        if(model.getOut_type_idDirtyFlag())
            domain.setOutTypeId(model.getOut_type_id());
        if(model.getManu_type_idDirtyFlag())
            domain.setManuTypeId(model.getManu_type_id());
        if(model.getPbm_mto_pull_idDirtyFlag())
            domain.setPbmMtoPullId(model.getPbm_mto_pull_id());
        if(model.getMto_pull_idDirtyFlag())
            domain.setMtoPullId(model.getMto_pull_id());
        if(model.getSam_type_idDirtyFlag())
            domain.setSamTypeId(model.getSam_type_id());
        if(model.getManufacture_pull_idDirtyFlag())
            domain.setManufacturePullId(model.getManufacture_pull_id());
        if(model.getSam_loc_idDirtyFlag())
            domain.setSamLocId(model.getSam_loc_id());
        if(model.getBuy_pull_idDirtyFlag())
            domain.setBuyPullId(model.getBuy_pull_id());
        if(model.getInt_type_idDirtyFlag())
            domain.setIntTypeId(model.getInt_type_id());
        if(model.getWh_qc_stock_loc_idDirtyFlag())
            domain.setWhQcStockLocId(model.getWh_qc_stock_loc_id());
        if(model.getIn_type_idDirtyFlag())
            domain.setInTypeId(model.getIn_type_id());
        if(model.getPbm_route_idDirtyFlag())
            domain.setPbmRouteId(model.getPbm_route_id());
        if(model.getPick_type_idDirtyFlag())
            domain.setPickTypeId(model.getPick_type_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getReception_route_idDirtyFlag())
            domain.setReceptionRouteId(model.getReception_route_id());
        if(model.getPack_type_idDirtyFlag())
            domain.setPackTypeId(model.getPack_type_id());
        if(model.getPbm_type_idDirtyFlag())
            domain.setPbmTypeId(model.getPbm_type_id());
        if(model.getDelivery_route_idDirtyFlag())
            domain.setDeliveryRouteId(model.getDelivery_route_id());
        if(model.getWh_input_stock_loc_idDirtyFlag())
            domain.setWhInputStockLocId(model.getWh_input_stock_loc_id());
        if(model.getLot_stock_idDirtyFlag())
            domain.setLotStockId(model.getLot_stock_id());
        if(model.getSam_rule_idDirtyFlag())
            domain.setSamRuleId(model.getSam_rule_id());
        if(model.getCrossdock_route_idDirtyFlag())
            domain.setCrossdockRouteId(model.getCrossdock_route_id());
        if(model.getWh_pack_stock_loc_idDirtyFlag())
            domain.setWhPackStockLocId(model.getWh_pack_stock_loc_id());
        return domain ;
    }

}

    



