package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.sale_product_configurator;

/**
 * 实体[sale_product_configurator] 服务对象接口
 */
public interface sale_product_configuratorRepository{


    public sale_product_configurator createPO() ;
        public void create(sale_product_configurator sale_product_configurator);

        public void createBatch(sale_product_configurator sale_product_configurator);

        public void removeBatch(String id);

        public List<sale_product_configurator> search();

        public void get(String id);

        public void update(sale_product_configurator sale_product_configurator);

        public void updateBatch(sale_product_configurator sale_product_configurator);

        public void remove(String id);


}
