package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Rating_rating;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_ratingSearchContext;

/**
 * 实体 [评级] 存储对象
 */
public interface Rating_ratingRepository extends Repository<Rating_rating> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Rating_rating> searchDefault(Rating_ratingSearchContext context);

    Rating_rating convert2PO(cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating domain , Rating_rating po) ;

    cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating convert2Domain( Rating_rating po ,cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating domain) ;

}
