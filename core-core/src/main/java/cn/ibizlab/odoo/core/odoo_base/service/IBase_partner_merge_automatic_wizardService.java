package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;


/**
 * 实体[Base_partner_merge_automatic_wizard] 服务对象接口
 */
public interface IBase_partner_merge_automatic_wizardService{

    boolean update(Base_partner_merge_automatic_wizard et) ;
    void updateBatch(List<Base_partner_merge_automatic_wizard> list) ;
    boolean create(Base_partner_merge_automatic_wizard et) ;
    void createBatch(List<Base_partner_merge_automatic_wizard> list) ;
    Base_partner_merge_automatic_wizard get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Base_partner_merge_automatic_wizard> searchDefault(Base_partner_merge_automatic_wizardSearchContext context) ;

}



