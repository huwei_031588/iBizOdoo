package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_leave;

/**
 * 实体[hr_leave] 服务对象接口
 */
public interface hr_leaveRepository{


    public hr_leave createPO() ;
        public void createBatch(hr_leave hr_leave);

        public void remove(String id);

        public void removeBatch(String id);

        public void updateBatch(hr_leave hr_leave);

        public void create(hr_leave hr_leave);

        public List<hr_leave> search();

        public void get(String id);

        public void update(hr_leave hr_leave);


}
