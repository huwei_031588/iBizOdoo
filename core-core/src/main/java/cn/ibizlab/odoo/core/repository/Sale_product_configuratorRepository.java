package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Sale_product_configurator;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_product_configuratorSearchContext;

/**
 * 实体 [销售产品配置器] 存储对象
 */
public interface Sale_product_configuratorRepository extends Repository<Sale_product_configurator> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Sale_product_configurator> searchDefault(Sale_product_configuratorSearchContext context);

    Sale_product_configurator convert2PO(cn.ibizlab.odoo.core.odoo_sale.domain.Sale_product_configurator domain , Sale_product_configurator po) ;

    cn.ibizlab.odoo.core.odoo_sale.domain.Sale_product_configurator convert2Domain( Sale_product_configurator po ,cn.ibizlab.odoo.core.odoo_sale.domain.Sale_product_configurator domain) ;

}
