package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_pickingSearchContext;


/**
 * 实体[Stock_return_picking] 服务对象接口
 */
public interface IStock_return_pickingService{

    boolean update(Stock_return_picking et) ;
    void updateBatch(List<Stock_return_picking> list) ;
    boolean create(Stock_return_picking et) ;
    void createBatch(List<Stock_return_picking> list) ;
    Stock_return_picking get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Stock_return_picking> searchDefault(Stock_return_pickingSearchContext context) ;

}



