package cn.ibizlab.odoo.core.odoo_project.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_taskSearchContext;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_taskService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_project.client.project_taskOdooClient;
import cn.ibizlab.odoo.core.odoo_project.clientmodel.project_taskClientModel;

/**
 * 实体[任务] 服务对象接口实现
 */
@Slf4j
@Service
public class Project_taskServiceImpl implements IProject_taskService {

    @Autowired
    project_taskOdooClient project_taskOdooClient;


    @Override
    public boolean update(Project_task et) {
        project_taskClientModel clientModel = convert2Model(et,null);
		project_taskOdooClient.update(clientModel);
        Project_task rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Project_task> list){
    }

    @Override
    public Project_task get(Integer id) {
        project_taskClientModel clientModel = new project_taskClientModel();
        clientModel.setId(id);
		project_taskOdooClient.get(clientModel);
        Project_task et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Project_task();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        project_taskClientModel clientModel = new project_taskClientModel();
        clientModel.setId(id);
		project_taskOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Project_task et) {
        project_taskClientModel clientModel = convert2Model(et,null);
		project_taskOdooClient.create(clientModel);
        Project_task rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Project_task> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Project_task> searchDefault(Project_taskSearchContext context) {
        List<Project_task> list = new ArrayList<Project_task>();
        Page<project_taskClientModel> clientModelList = project_taskOdooClient.search(context);
        for(project_taskClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Project_task>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public project_taskClientModel convert2Model(Project_task domain , project_taskClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new project_taskClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("date_startdirtyflag"))
                model.setDate_start(domain.getDateStart());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("rating_countdirtyflag"))
                model.setRating_count(domain.getRatingCount());
            if((Boolean) domain.getExtensionparams().get("email_fromdirtyflag"))
                model.setEmail_from(domain.getEmailFrom());
            if((Boolean) domain.getExtensionparams().get("attachment_idsdirtyflag"))
                model.setAttachment_ids(domain.getAttachmentIds());
            if((Boolean) domain.getExtensionparams().get("date_deadlinedirtyflag"))
                model.setDate_deadline(domain.getDateDeadline());
            if((Boolean) domain.getExtensionparams().get("kanban_state_labeldirtyflag"))
                model.setKanban_state_label(domain.getKanbanStateLabel());
            if((Boolean) domain.getExtensionparams().get("access_warningdirtyflag"))
                model.setAccess_warning(domain.getAccessWarning());
            if((Boolean) domain.getExtensionparams().get("rating_idsdirtyflag"))
                model.setRating_ids(domain.getRatingIds());
            if((Boolean) domain.getExtensionparams().get("rating_last_valuedirtyflag"))
                model.setRating_last_value(domain.getRatingLastValue());
            if((Boolean) domain.getExtensionparams().get("date_assigndirtyflag"))
                model.setDate_assign(domain.getDateAssign());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("rating_last_feedbackdirtyflag"))
                model.setRating_last_feedback(domain.getRatingLastFeedback());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("access_tokendirtyflag"))
                model.setAccess_token(domain.getAccessToken());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("subtask_countdirtyflag"))
                model.setSubtask_count(domain.getSubtaskCount());
            if((Boolean) domain.getExtensionparams().get("working_hours_opendirtyflag"))
                model.setWorking_hours_open(domain.getWorkingHoursOpen());
            if((Boolean) domain.getExtensionparams().get("working_hours_closedirtyflag"))
                model.setWorking_hours_close(domain.getWorkingHoursClose());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("rating_last_imagedirtyflag"))
                model.setRating_last_image(domain.getRatingLastImage());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("displayed_image_iddirtyflag"))
                model.setDisplayed_image_id(domain.getDisplayedImageId());
            if((Boolean) domain.getExtensionparams().get("tag_idsdirtyflag"))
                model.setTag_ids(domain.getTagIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("kanban_statedirtyflag"))
                model.setKanban_state(domain.getKanbanState());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("notesdirtyflag"))
                model.setNotes(domain.getNotes());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("date_last_stage_updatedirtyflag"))
                model.setDate_last_stage_update(domain.getDateLastStageUpdate());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("working_days_opendirtyflag"))
                model.setWorking_days_open(domain.getWorkingDaysOpen());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("working_days_closedirtyflag"))
                model.setWorking_days_close(domain.getWorkingDaysClose());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("email_ccdirtyflag"))
                model.setEmail_cc(domain.getEmailCc());
            if((Boolean) domain.getExtensionparams().get("date_enddirtyflag"))
                model.setDate_end(domain.getDateEnd());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("planned_hoursdirtyflag"))
                model.setPlanned_hours(domain.getPlannedHours());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("prioritydirtyflag"))
                model.setPriority(domain.getPriority());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("access_urldirtyflag"))
                model.setAccess_url(domain.getAccessUrl());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("subtask_planned_hoursdirtyflag"))
                model.setSubtask_planned_hours(domain.getSubtaskPlannedHours());
            if((Boolean) domain.getExtensionparams().get("legend_donedirtyflag"))
                model.setLegend_done(domain.getLegendDone());
            if((Boolean) domain.getExtensionparams().get("legend_blockeddirtyflag"))
                model.setLegend_blocked(domain.getLegendBlocked());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("manager_iddirtyflag"))
                model.setManager_id(domain.getManagerId());
            if((Boolean) domain.getExtensionparams().get("project_id_textdirtyflag"))
                model.setProject_id_text(domain.getProjectIdText());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("user_emaildirtyflag"))
                model.setUser_email(domain.getUserEmail());
            if((Boolean) domain.getExtensionparams().get("subtask_project_iddirtyflag"))
                model.setSubtask_project_id(domain.getSubtaskProjectId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("stage_id_textdirtyflag"))
                model.setStage_id_text(domain.getStageIdText());
            if((Boolean) domain.getExtensionparams().get("legend_normaldirtyflag"))
                model.setLegend_normal(domain.getLegendNormal());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("project_iddirtyflag"))
                model.setProject_id(domain.getProjectId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("stage_iddirtyflag"))
                model.setStage_id(domain.getStageId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Project_task convert2Domain( project_taskClientModel model ,Project_task domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Project_task();
        }

        if(model.getDate_startDirtyFlag())
            domain.setDateStart(model.getDate_start());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getRating_countDirtyFlag())
            domain.setRatingCount(model.getRating_count());
        if(model.getEmail_fromDirtyFlag())
            domain.setEmailFrom(model.getEmail_from());
        if(model.getAttachment_idsDirtyFlag())
            domain.setAttachmentIds(model.getAttachment_ids());
        if(model.getDate_deadlineDirtyFlag())
            domain.setDateDeadline(model.getDate_deadline());
        if(model.getKanban_state_labelDirtyFlag())
            domain.setKanbanStateLabel(model.getKanban_state_label());
        if(model.getAccess_warningDirtyFlag())
            domain.setAccessWarning(model.getAccess_warning());
        if(model.getRating_idsDirtyFlag())
            domain.setRatingIds(model.getRating_ids());
        if(model.getRating_last_valueDirtyFlag())
            domain.setRatingLastValue(model.getRating_last_value());
        if(model.getDate_assignDirtyFlag())
            domain.setDateAssign(model.getDate_assign());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getRating_last_feedbackDirtyFlag())
            domain.setRatingLastFeedback(model.getRating_last_feedback());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getAccess_tokenDirtyFlag())
            domain.setAccessToken(model.getAccess_token());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getSubtask_countDirtyFlag())
            domain.setSubtaskCount(model.getSubtask_count());
        if(model.getWorking_hours_openDirtyFlag())
            domain.setWorkingHoursOpen(model.getWorking_hours_open());
        if(model.getWorking_hours_closeDirtyFlag())
            domain.setWorkingHoursClose(model.getWorking_hours_close());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getRating_last_imageDirtyFlag())
            domain.setRatingLastImage(model.getRating_last_image());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplayed_image_idDirtyFlag())
            domain.setDisplayedImageId(model.getDisplayed_image_id());
        if(model.getTag_idsDirtyFlag())
            domain.setTagIds(model.getTag_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getKanban_stateDirtyFlag())
            domain.setKanbanState(model.getKanban_state());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getNotesDirtyFlag())
            domain.setNotes(model.getNotes());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getDate_last_stage_updateDirtyFlag())
            domain.setDateLastStageUpdate(model.getDate_last_stage_update());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getWorking_days_openDirtyFlag())
            domain.setWorkingDaysOpen(model.getWorking_days_open());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getWorking_days_closeDirtyFlag())
            domain.setWorkingDaysClose(model.getWorking_days_close());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getEmail_ccDirtyFlag())
            domain.setEmailCc(model.getEmail_cc());
        if(model.getDate_endDirtyFlag())
            domain.setDateEnd(model.getDate_end());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getPlanned_hoursDirtyFlag())
            domain.setPlannedHours(model.getPlanned_hours());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getPriorityDirtyFlag())
            domain.setPriority(model.getPriority());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getAccess_urlDirtyFlag())
            domain.setAccessUrl(model.getAccess_url());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getSubtask_planned_hoursDirtyFlag())
            domain.setSubtaskPlannedHours(model.getSubtask_planned_hours());
        if(model.getLegend_doneDirtyFlag())
            domain.setLegendDone(model.getLegend_done());
        if(model.getLegend_blockedDirtyFlag())
            domain.setLegendBlocked(model.getLegend_blocked());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getManager_idDirtyFlag())
            domain.setManagerId(model.getManager_id());
        if(model.getProject_id_textDirtyFlag())
            domain.setProjectIdText(model.getProject_id_text());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getUser_emailDirtyFlag())
            domain.setUserEmail(model.getUser_email());
        if(model.getSubtask_project_idDirtyFlag())
            domain.setSubtaskProjectId(model.getSubtask_project_id());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getStage_id_textDirtyFlag())
            domain.setStageIdText(model.getStage_id_text());
        if(model.getLegend_normalDirtyFlag())
            domain.setLegendNormal(model.getLegend_normal());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getProject_idDirtyFlag())
            domain.setProjectId(model.getProject_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getStage_idDirtyFlag())
            domain.setStageId(model.getStage_id());
        return domain ;
    }

}

    



