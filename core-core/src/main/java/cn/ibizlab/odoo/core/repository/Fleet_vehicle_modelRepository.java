package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_vehicle_model;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_modelSearchContext;

/**
 * 实体 [车辆型号] 存储对象
 */
public interface Fleet_vehicle_modelRepository extends Repository<Fleet_vehicle_model> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_vehicle_model> searchDefault(Fleet_vehicle_modelSearchContext context);

    Fleet_vehicle_model convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model domain , Fleet_vehicle_model po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model convert2Domain( Fleet_vehicle_model po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model domain) ;

}
