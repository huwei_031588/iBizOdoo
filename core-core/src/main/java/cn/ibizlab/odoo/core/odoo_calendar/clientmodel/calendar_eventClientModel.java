package cn.ibizlab.odoo.core.odoo_calendar.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[calendar_event] 对象
 */
public class calendar_eventClientModel implements Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 提醒
     */
    public String alarm_ids;

    @JsonIgnore
    public boolean alarm_idsDirtyFlag;
    
    /**
     * 全天
     */
    public String allday;

    @JsonIgnore
    public boolean alldayDirtyFlag;
    
    /**
     * 申请人
     */
    public Integer applicant_id;

    @JsonIgnore
    public boolean applicant_idDirtyFlag;
    
    /**
     * 申请人
     */
    public String applicant_id_text;

    @JsonIgnore
    public boolean applicant_id_textDirtyFlag;
    
    /**
     * 参与者
     */
    public String attendee_ids;

    @JsonIgnore
    public boolean attendee_idsDirtyFlag;
    
    /**
     * 出席者状态
     */
    public String attendee_status;

    @JsonIgnore
    public boolean attendee_statusDirtyFlag;
    
    /**
     * 按 天
     */
    public String byday;

    @JsonIgnore
    public boolean bydayDirtyFlag;
    
    /**
     * 标签
     */
    public String categ_ids;

    @JsonIgnore
    public boolean categ_idsDirtyFlag;
    
    /**
     * 重复
     */
    public Integer count;

    @JsonIgnore
    public boolean countDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    public Integer day;

    @JsonIgnore
    public boolean dayDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 日期
     */
    public String display_start;

    @JsonIgnore
    public boolean display_startDirtyFlag;
    
    /**
     * 活动时间
     */
    public String display_time;

    @JsonIgnore
    public boolean display_timeDirtyFlag;
    
    /**
     * 持续时间
     */
    public Double duration;

    @JsonIgnore
    public boolean durationDirtyFlag;
    
    /**
     * 重复终止
     */
    public String end_type;

    @JsonIgnore
    public boolean end_typeDirtyFlag;
    
    /**
     * 重复直到
     */
    public Timestamp final_date;

    @JsonIgnore
    public boolean final_dateDirtyFlag;
    
    /**
     * 周五
     */
    public String fr;

    @JsonIgnore
    public boolean frDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 重复
     */
    public Integer interval;

    @JsonIgnore
    public boolean intervalDirtyFlag;
    
    /**
     * 出席者
     */
    public String is_attendee;

    @JsonIgnore
    public boolean is_attendeeDirtyFlag;
    
    /**
     * 事件是否突出显示
     */
    public String is_highlighted;

    @JsonIgnore
    public boolean is_highlightedDirtyFlag;
    
    /**
     * 地点
     */
    public String location;

    @JsonIgnore
    public boolean locationDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 周一
     */
    public String mo;

    @JsonIgnore
    public boolean moDirtyFlag;
    
    /**
     * 选项
     */
    public String month_by;

    @JsonIgnore
    public boolean month_byDirtyFlag;
    
    /**
     * 会议主题
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 商机
     */
    public Integer opportunity_id;

    @JsonIgnore
    public boolean opportunity_idDirtyFlag;
    
    /**
     * 商机
     */
    public String opportunity_id_text;

    @JsonIgnore
    public boolean opportunity_id_textDirtyFlag;
    
    /**
     * 负责人
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 与会者
     */
    public String partner_ids;

    @JsonIgnore
    public boolean partner_idsDirtyFlag;
    
    /**
     * 隐私
     */
    public String privacy;

    @JsonIgnore
    public boolean privacyDirtyFlag;
    
    /**
     * 循环
     */
    public String recurrency;

    @JsonIgnore
    public boolean recurrencyDirtyFlag;
    
    /**
     * 循环ID
     */
    public Integer recurrent_id;

    @JsonIgnore
    public boolean recurrent_idDirtyFlag;
    
    /**
     * 循环ID日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp recurrent_id_date;

    @JsonIgnore
    public boolean recurrent_id_dateDirtyFlag;
    
    /**
     * 文档ID
     */
    public Integer res_id;

    @JsonIgnore
    public boolean res_idDirtyFlag;
    
    /**
     * 文档模型名称
     */
    public String res_model;

    @JsonIgnore
    public boolean res_modelDirtyFlag;
    
    /**
     * 文档模型
     */
    public Integer res_model_id;

    @JsonIgnore
    public boolean res_model_idDirtyFlag;
    
    /**
     * 循环规则
     */
    public String rrule;

    @JsonIgnore
    public boolean rruleDirtyFlag;
    
    /**
     * 重新提起
     */
    public String rrule_type;

    @JsonIgnore
    public boolean rrule_typeDirtyFlag;
    
    /**
     * 周六
     */
    public String sa;

    @JsonIgnore
    public boolean saDirtyFlag;
    
    /**
     * 显示时间为
     */
    public String show_as;

    @JsonIgnore
    public boolean show_asDirtyFlag;
    
    /**
     * 开始
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp start;

    @JsonIgnore
    public boolean startDirtyFlag;
    
    /**
     * 开始日期
     */
    public Timestamp start_date;

    @JsonIgnore
    public boolean start_dateDirtyFlag;
    
    /**
     * 开始时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp start_datetime;

    @JsonIgnore
    public boolean start_datetimeDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 停止
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp stop;

    @JsonIgnore
    public boolean stopDirtyFlag;
    
    /**
     * 结束日期
     */
    public Timestamp stop_date;

    @JsonIgnore
    public boolean stop_dateDirtyFlag;
    
    /**
     * 结束日期时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp stop_datetime;

    @JsonIgnore
    public boolean stop_datetimeDirtyFlag;
    
    /**
     * 周日
     */
    public String su;

    @JsonIgnore
    public boolean suDirtyFlag;
    
    /**
     * 周四
     */
    public String th;

    @JsonIgnore
    public boolean thDirtyFlag;
    
    /**
     * 周二
     */
    public String tu;

    @JsonIgnore
    public boolean tuDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 所有者
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 周三
     */
    public String we;

    @JsonIgnore
    public boolean weDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 工作日
     */
    public String week_list;

    @JsonIgnore
    public boolean week_listDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [提醒]
     */
    @JsonProperty("alarm_ids")
    public String getAlarm_ids(){
        return this.alarm_ids ;
    }

    /**
     * 设置 [提醒]
     */
    @JsonProperty("alarm_ids")
    public void setAlarm_ids(String  alarm_ids){
        this.alarm_ids = alarm_ids ;
        this.alarm_idsDirtyFlag = true ;
    }

     /**
     * 获取 [提醒]脏标记
     */
    @JsonIgnore
    public boolean getAlarm_idsDirtyFlag(){
        return this.alarm_idsDirtyFlag ;
    }   

    /**
     * 获取 [全天]
     */
    @JsonProperty("allday")
    public String getAllday(){
        return this.allday ;
    }

    /**
     * 设置 [全天]
     */
    @JsonProperty("allday")
    public void setAllday(String  allday){
        this.allday = allday ;
        this.alldayDirtyFlag = true ;
    }

     /**
     * 获取 [全天]脏标记
     */
    @JsonIgnore
    public boolean getAlldayDirtyFlag(){
        return this.alldayDirtyFlag ;
    }   

    /**
     * 获取 [申请人]
     */
    @JsonProperty("applicant_id")
    public Integer getApplicant_id(){
        return this.applicant_id ;
    }

    /**
     * 设置 [申请人]
     */
    @JsonProperty("applicant_id")
    public void setApplicant_id(Integer  applicant_id){
        this.applicant_id = applicant_id ;
        this.applicant_idDirtyFlag = true ;
    }

     /**
     * 获取 [申请人]脏标记
     */
    @JsonIgnore
    public boolean getApplicant_idDirtyFlag(){
        return this.applicant_idDirtyFlag ;
    }   

    /**
     * 获取 [申请人]
     */
    @JsonProperty("applicant_id_text")
    public String getApplicant_id_text(){
        return this.applicant_id_text ;
    }

    /**
     * 设置 [申请人]
     */
    @JsonProperty("applicant_id_text")
    public void setApplicant_id_text(String  applicant_id_text){
        this.applicant_id_text = applicant_id_text ;
        this.applicant_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [申请人]脏标记
     */
    @JsonIgnore
    public boolean getApplicant_id_textDirtyFlag(){
        return this.applicant_id_textDirtyFlag ;
    }   

    /**
     * 获取 [参与者]
     */
    @JsonProperty("attendee_ids")
    public String getAttendee_ids(){
        return this.attendee_ids ;
    }

    /**
     * 设置 [参与者]
     */
    @JsonProperty("attendee_ids")
    public void setAttendee_ids(String  attendee_ids){
        this.attendee_ids = attendee_ids ;
        this.attendee_idsDirtyFlag = true ;
    }

     /**
     * 获取 [参与者]脏标记
     */
    @JsonIgnore
    public boolean getAttendee_idsDirtyFlag(){
        return this.attendee_idsDirtyFlag ;
    }   

    /**
     * 获取 [出席者状态]
     */
    @JsonProperty("attendee_status")
    public String getAttendee_status(){
        return this.attendee_status ;
    }

    /**
     * 设置 [出席者状态]
     */
    @JsonProperty("attendee_status")
    public void setAttendee_status(String  attendee_status){
        this.attendee_status = attendee_status ;
        this.attendee_statusDirtyFlag = true ;
    }

     /**
     * 获取 [出席者状态]脏标记
     */
    @JsonIgnore
    public boolean getAttendee_statusDirtyFlag(){
        return this.attendee_statusDirtyFlag ;
    }   

    /**
     * 获取 [按 天]
     */
    @JsonProperty("byday")
    public String getByday(){
        return this.byday ;
    }

    /**
     * 设置 [按 天]
     */
    @JsonProperty("byday")
    public void setByday(String  byday){
        this.byday = byday ;
        this.bydayDirtyFlag = true ;
    }

     /**
     * 获取 [按 天]脏标记
     */
    @JsonIgnore
    public boolean getBydayDirtyFlag(){
        return this.bydayDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("categ_ids")
    public String getCateg_ids(){
        return this.categ_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("categ_ids")
    public void setCateg_ids(String  categ_ids){
        this.categ_ids = categ_ids ;
        this.categ_idsDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idsDirtyFlag(){
        return this.categ_idsDirtyFlag ;
    }   

    /**
     * 获取 [重复]
     */
    @JsonProperty("count")
    public Integer getCount(){
        return this.count ;
    }

    /**
     * 设置 [重复]
     */
    @JsonProperty("count")
    public void setCount(Integer  count){
        this.count = count ;
        this.countDirtyFlag = true ;
    }

     /**
     * 获取 [重复]脏标记
     */
    @JsonIgnore
    public boolean getCountDirtyFlag(){
        return this.countDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("day")
    public Integer getDay(){
        return this.day ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("day")
    public void setDay(Integer  day){
        this.day = day ;
        this.dayDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDayDirtyFlag(){
        return this.dayDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("display_start")
    public String getDisplay_start(){
        return this.display_start ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("display_start")
    public void setDisplay_start(String  display_start){
        this.display_start = display_start ;
        this.display_startDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_startDirtyFlag(){
        return this.display_startDirtyFlag ;
    }   

    /**
     * 获取 [活动时间]
     */
    @JsonProperty("display_time")
    public String getDisplay_time(){
        return this.display_time ;
    }

    /**
     * 设置 [活动时间]
     */
    @JsonProperty("display_time")
    public void setDisplay_time(String  display_time){
        this.display_time = display_time ;
        this.display_timeDirtyFlag = true ;
    }

     /**
     * 获取 [活动时间]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_timeDirtyFlag(){
        return this.display_timeDirtyFlag ;
    }   

    /**
     * 获取 [持续时间]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return this.duration ;
    }

    /**
     * 设置 [持续时间]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

     /**
     * 获取 [持续时间]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return this.durationDirtyFlag ;
    }   

    /**
     * 获取 [重复终止]
     */
    @JsonProperty("end_type")
    public String getEnd_type(){
        return this.end_type ;
    }

    /**
     * 设置 [重复终止]
     */
    @JsonProperty("end_type")
    public void setEnd_type(String  end_type){
        this.end_type = end_type ;
        this.end_typeDirtyFlag = true ;
    }

     /**
     * 获取 [重复终止]脏标记
     */
    @JsonIgnore
    public boolean getEnd_typeDirtyFlag(){
        return this.end_typeDirtyFlag ;
    }   

    /**
     * 获取 [重复直到]
     */
    @JsonProperty("final_date")
    public Timestamp getFinal_date(){
        return this.final_date ;
    }

    /**
     * 设置 [重复直到]
     */
    @JsonProperty("final_date")
    public void setFinal_date(Timestamp  final_date){
        this.final_date = final_date ;
        this.final_dateDirtyFlag = true ;
    }

     /**
     * 获取 [重复直到]脏标记
     */
    @JsonIgnore
    public boolean getFinal_dateDirtyFlag(){
        return this.final_dateDirtyFlag ;
    }   

    /**
     * 获取 [周五]
     */
    @JsonProperty("fr")
    public String getFr(){
        return this.fr ;
    }

    /**
     * 设置 [周五]
     */
    @JsonProperty("fr")
    public void setFr(String  fr){
        this.fr = fr ;
        this.frDirtyFlag = true ;
    }

     /**
     * 获取 [周五]脏标记
     */
    @JsonIgnore
    public boolean getFrDirtyFlag(){
        return this.frDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [重复]
     */
    @JsonProperty("interval")
    public Integer getInterval(){
        return this.interval ;
    }

    /**
     * 设置 [重复]
     */
    @JsonProperty("interval")
    public void setInterval(Integer  interval){
        this.interval = interval ;
        this.intervalDirtyFlag = true ;
    }

     /**
     * 获取 [重复]脏标记
     */
    @JsonIgnore
    public boolean getIntervalDirtyFlag(){
        return this.intervalDirtyFlag ;
    }   

    /**
     * 获取 [出席者]
     */
    @JsonProperty("is_attendee")
    public String getIs_attendee(){
        return this.is_attendee ;
    }

    /**
     * 设置 [出席者]
     */
    @JsonProperty("is_attendee")
    public void setIs_attendee(String  is_attendee){
        this.is_attendee = is_attendee ;
        this.is_attendeeDirtyFlag = true ;
    }

     /**
     * 获取 [出席者]脏标记
     */
    @JsonIgnore
    public boolean getIs_attendeeDirtyFlag(){
        return this.is_attendeeDirtyFlag ;
    }   

    /**
     * 获取 [事件是否突出显示]
     */
    @JsonProperty("is_highlighted")
    public String getIs_highlighted(){
        return this.is_highlighted ;
    }

    /**
     * 设置 [事件是否突出显示]
     */
    @JsonProperty("is_highlighted")
    public void setIs_highlighted(String  is_highlighted){
        this.is_highlighted = is_highlighted ;
        this.is_highlightedDirtyFlag = true ;
    }

     /**
     * 获取 [事件是否突出显示]脏标记
     */
    @JsonIgnore
    public boolean getIs_highlightedDirtyFlag(){
        return this.is_highlightedDirtyFlag ;
    }   

    /**
     * 获取 [地点]
     */
    @JsonProperty("location")
    public String getLocation(){
        return this.location ;
    }

    /**
     * 设置 [地点]
     */
    @JsonProperty("location")
    public void setLocation(String  location){
        this.location = location ;
        this.locationDirtyFlag = true ;
    }

     /**
     * 获取 [地点]脏标记
     */
    @JsonIgnore
    public boolean getLocationDirtyFlag(){
        return this.locationDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [周一]
     */
    @JsonProperty("mo")
    public String getMo(){
        return this.mo ;
    }

    /**
     * 设置 [周一]
     */
    @JsonProperty("mo")
    public void setMo(String  mo){
        this.mo = mo ;
        this.moDirtyFlag = true ;
    }

     /**
     * 获取 [周一]脏标记
     */
    @JsonIgnore
    public boolean getMoDirtyFlag(){
        return this.moDirtyFlag ;
    }   

    /**
     * 获取 [选项]
     */
    @JsonProperty("month_by")
    public String getMonth_by(){
        return this.month_by ;
    }

    /**
     * 设置 [选项]
     */
    @JsonProperty("month_by")
    public void setMonth_by(String  month_by){
        this.month_by = month_by ;
        this.month_byDirtyFlag = true ;
    }

     /**
     * 获取 [选项]脏标记
     */
    @JsonIgnore
    public boolean getMonth_byDirtyFlag(){
        return this.month_byDirtyFlag ;
    }   

    /**
     * 获取 [会议主题]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [会议主题]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [会议主题]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_id")
    public Integer getOpportunity_id(){
        return this.opportunity_id ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_id")
    public void setOpportunity_id(Integer  opportunity_id){
        this.opportunity_id = opportunity_id ;
        this.opportunity_idDirtyFlag = true ;
    }

     /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_idDirtyFlag(){
        return this.opportunity_idDirtyFlag ;
    }   

    /**
     * 获取 [商机]
     */
    @JsonProperty("opportunity_id_text")
    public String getOpportunity_id_text(){
        return this.opportunity_id_text ;
    }

    /**
     * 设置 [商机]
     */
    @JsonProperty("opportunity_id_text")
    public void setOpportunity_id_text(String  opportunity_id_text){
        this.opportunity_id_text = opportunity_id_text ;
        this.opportunity_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [商机]脏标记
     */
    @JsonIgnore
    public boolean getOpportunity_id_textDirtyFlag(){
        return this.opportunity_id_textDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [与会者]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return this.partner_ids ;
    }

    /**
     * 设置 [与会者]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [与会者]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return this.partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [隐私]
     */
    @JsonProperty("privacy")
    public String getPrivacy(){
        return this.privacy ;
    }

    /**
     * 设置 [隐私]
     */
    @JsonProperty("privacy")
    public void setPrivacy(String  privacy){
        this.privacy = privacy ;
        this.privacyDirtyFlag = true ;
    }

     /**
     * 获取 [隐私]脏标记
     */
    @JsonIgnore
    public boolean getPrivacyDirtyFlag(){
        return this.privacyDirtyFlag ;
    }   

    /**
     * 获取 [循环]
     */
    @JsonProperty("recurrency")
    public String getRecurrency(){
        return this.recurrency ;
    }

    /**
     * 设置 [循环]
     */
    @JsonProperty("recurrency")
    public void setRecurrency(String  recurrency){
        this.recurrency = recurrency ;
        this.recurrencyDirtyFlag = true ;
    }

     /**
     * 获取 [循环]脏标记
     */
    @JsonIgnore
    public boolean getRecurrencyDirtyFlag(){
        return this.recurrencyDirtyFlag ;
    }   

    /**
     * 获取 [循环ID]
     */
    @JsonProperty("recurrent_id")
    public Integer getRecurrent_id(){
        return this.recurrent_id ;
    }

    /**
     * 设置 [循环ID]
     */
    @JsonProperty("recurrent_id")
    public void setRecurrent_id(Integer  recurrent_id){
        this.recurrent_id = recurrent_id ;
        this.recurrent_idDirtyFlag = true ;
    }

     /**
     * 获取 [循环ID]脏标记
     */
    @JsonIgnore
    public boolean getRecurrent_idDirtyFlag(){
        return this.recurrent_idDirtyFlag ;
    }   

    /**
     * 获取 [循环ID日期]
     */
    @JsonProperty("recurrent_id_date")
    public Timestamp getRecurrent_id_date(){
        return this.recurrent_id_date ;
    }

    /**
     * 设置 [循环ID日期]
     */
    @JsonProperty("recurrent_id_date")
    public void setRecurrent_id_date(Timestamp  recurrent_id_date){
        this.recurrent_id_date = recurrent_id_date ;
        this.recurrent_id_dateDirtyFlag = true ;
    }

     /**
     * 获取 [循环ID日期]脏标记
     */
    @JsonIgnore
    public boolean getRecurrent_id_dateDirtyFlag(){
        return this.recurrent_id_dateDirtyFlag ;
    }   

    /**
     * 获取 [文档ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [文档ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

     /**
     * 获取 [文档ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }   

    /**
     * 获取 [文档模型名称]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return this.res_model ;
    }

    /**
     * 设置 [文档模型名称]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

     /**
     * 获取 [文档模型名称]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return this.res_modelDirtyFlag ;
    }   

    /**
     * 获取 [文档模型]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return this.res_model_id ;
    }

    /**
     * 设置 [文档模型]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [文档模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return this.res_model_idDirtyFlag ;
    }   

    /**
     * 获取 [循环规则]
     */
    @JsonProperty("rrule")
    public String getRrule(){
        return this.rrule ;
    }

    /**
     * 设置 [循环规则]
     */
    @JsonProperty("rrule")
    public void setRrule(String  rrule){
        this.rrule = rrule ;
        this.rruleDirtyFlag = true ;
    }

     /**
     * 获取 [循环规则]脏标记
     */
    @JsonIgnore
    public boolean getRruleDirtyFlag(){
        return this.rruleDirtyFlag ;
    }   

    /**
     * 获取 [重新提起]
     */
    @JsonProperty("rrule_type")
    public String getRrule_type(){
        return this.rrule_type ;
    }

    /**
     * 设置 [重新提起]
     */
    @JsonProperty("rrule_type")
    public void setRrule_type(String  rrule_type){
        this.rrule_type = rrule_type ;
        this.rrule_typeDirtyFlag = true ;
    }

     /**
     * 获取 [重新提起]脏标记
     */
    @JsonIgnore
    public boolean getRrule_typeDirtyFlag(){
        return this.rrule_typeDirtyFlag ;
    }   

    /**
     * 获取 [周六]
     */
    @JsonProperty("sa")
    public String getSa(){
        return this.sa ;
    }

    /**
     * 设置 [周六]
     */
    @JsonProperty("sa")
    public void setSa(String  sa){
        this.sa = sa ;
        this.saDirtyFlag = true ;
    }

     /**
     * 获取 [周六]脏标记
     */
    @JsonIgnore
    public boolean getSaDirtyFlag(){
        return this.saDirtyFlag ;
    }   

    /**
     * 获取 [显示时间为]
     */
    @JsonProperty("show_as")
    public String getShow_as(){
        return this.show_as ;
    }

    /**
     * 设置 [显示时间为]
     */
    @JsonProperty("show_as")
    public void setShow_as(String  show_as){
        this.show_as = show_as ;
        this.show_asDirtyFlag = true ;
    }

     /**
     * 获取 [显示时间为]脏标记
     */
    @JsonIgnore
    public boolean getShow_asDirtyFlag(){
        return this.show_asDirtyFlag ;
    }   

    /**
     * 获取 [开始]
     */
    @JsonProperty("start")
    public Timestamp getStart(){
        return this.start ;
    }

    /**
     * 设置 [开始]
     */
    @JsonProperty("start")
    public void setStart(Timestamp  start){
        this.start = start ;
        this.startDirtyFlag = true ;
    }

     /**
     * 获取 [开始]脏标记
     */
    @JsonIgnore
    public boolean getStartDirtyFlag(){
        return this.startDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return this.start_date ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return this.start_dateDirtyFlag ;
    }   

    /**
     * 获取 [开始时间]
     */
    @JsonProperty("start_datetime")
    public Timestamp getStart_datetime(){
        return this.start_datetime ;
    }

    /**
     * 设置 [开始时间]
     */
    @JsonProperty("start_datetime")
    public void setStart_datetime(Timestamp  start_datetime){
        this.start_datetime = start_datetime ;
        this.start_datetimeDirtyFlag = true ;
    }

     /**
     * 获取 [开始时间]脏标记
     */
    @JsonIgnore
    public boolean getStart_datetimeDirtyFlag(){
        return this.start_datetimeDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [停止]
     */
    @JsonProperty("stop")
    public Timestamp getStop(){
        return this.stop ;
    }

    /**
     * 设置 [停止]
     */
    @JsonProperty("stop")
    public void setStop(Timestamp  stop){
        this.stop = stop ;
        this.stopDirtyFlag = true ;
    }

     /**
     * 获取 [停止]脏标记
     */
    @JsonIgnore
    public boolean getStopDirtyFlag(){
        return this.stopDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("stop_date")
    public Timestamp getStop_date(){
        return this.stop_date ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("stop_date")
    public void setStop_date(Timestamp  stop_date){
        this.stop_date = stop_date ;
        this.stop_dateDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getStop_dateDirtyFlag(){
        return this.stop_dateDirtyFlag ;
    }   

    /**
     * 获取 [结束日期时间]
     */
    @JsonProperty("stop_datetime")
    public Timestamp getStop_datetime(){
        return this.stop_datetime ;
    }

    /**
     * 设置 [结束日期时间]
     */
    @JsonProperty("stop_datetime")
    public void setStop_datetime(Timestamp  stop_datetime){
        this.stop_datetime = stop_datetime ;
        this.stop_datetimeDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期时间]脏标记
     */
    @JsonIgnore
    public boolean getStop_datetimeDirtyFlag(){
        return this.stop_datetimeDirtyFlag ;
    }   

    /**
     * 获取 [周日]
     */
    @JsonProperty("su")
    public String getSu(){
        return this.su ;
    }

    /**
     * 设置 [周日]
     */
    @JsonProperty("su")
    public void setSu(String  su){
        this.su = su ;
        this.suDirtyFlag = true ;
    }

     /**
     * 获取 [周日]脏标记
     */
    @JsonIgnore
    public boolean getSuDirtyFlag(){
        return this.suDirtyFlag ;
    }   

    /**
     * 获取 [周四]
     */
    @JsonProperty("th")
    public String getTh(){
        return this.th ;
    }

    /**
     * 设置 [周四]
     */
    @JsonProperty("th")
    public void setTh(String  th){
        this.th = th ;
        this.thDirtyFlag = true ;
    }

     /**
     * 获取 [周四]脏标记
     */
    @JsonIgnore
    public boolean getThDirtyFlag(){
        return this.thDirtyFlag ;
    }   

    /**
     * 获取 [周二]
     */
    @JsonProperty("tu")
    public String getTu(){
        return this.tu ;
    }

    /**
     * 设置 [周二]
     */
    @JsonProperty("tu")
    public void setTu(String  tu){
        this.tu = tu ;
        this.tuDirtyFlag = true ;
    }

     /**
     * 获取 [周二]脏标记
     */
    @JsonIgnore
    public boolean getTuDirtyFlag(){
        return this.tuDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [周三]
     */
    @JsonProperty("we")
    public String getWe(){
        return this.we ;
    }

    /**
     * 设置 [周三]
     */
    @JsonProperty("we")
    public void setWe(String  we){
        this.we = we ;
        this.weDirtyFlag = true ;
    }

     /**
     * 获取 [周三]脏标记
     */
    @JsonIgnore
    public boolean getWeDirtyFlag(){
        return this.weDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [工作日]
     */
    @JsonProperty("week_list")
    public String getWeek_list(){
        return this.week_list ;
    }

    /**
     * 设置 [工作日]
     */
    @JsonProperty("week_list")
    public void setWeek_list(String  week_list){
        this.week_list = week_list ;
        this.week_listDirtyFlag = true ;
    }

     /**
     * 获取 [工作日]脏标记
     */
    @JsonIgnore
    public boolean getWeek_listDirtyFlag(){
        return this.week_listDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("activity_ids") instanceof Boolean)&& map.get("activity_ids")!=null){
			Object[] objs = (Object[])map.get("activity_ids");
			if(objs.length > 0){
				Integer[] activity_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setActivity_ids(Arrays.toString(activity_ids).replace(" ",""));
			}
		}
		if(!(map.get("alarm_ids") instanceof Boolean)&& map.get("alarm_ids")!=null){
			Object[] objs = (Object[])map.get("alarm_ids");
			if(objs.length > 0){
				Integer[] alarm_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAlarm_ids(Arrays.toString(alarm_ids).replace(" ",""));
			}
		}
		if(map.get("allday") instanceof Boolean){
			this.setAllday(((Boolean)map.get("allday"))? "true" : "false");
		}
		if(!(map.get("applicant_id") instanceof Boolean)&& map.get("applicant_id")!=null){
			Object[] objs = (Object[])map.get("applicant_id");
			if(objs.length > 0){
				this.setApplicant_id((Integer)objs[0]);
			}
		}
		if(!(map.get("applicant_id") instanceof Boolean)&& map.get("applicant_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("applicant_id");
			if(objs.length > 1){
				this.setApplicant_id_text((String)objs[1]);
			}
		}
		if(!(map.get("attendee_ids") instanceof Boolean)&& map.get("attendee_ids")!=null){
			Object[] objs = (Object[])map.get("attendee_ids");
			if(objs.length > 0){
				Integer[] attendee_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAttendee_ids(Arrays.toString(attendee_ids).replace(" ",""));
			}
		}
		if(!(map.get("attendee_status") instanceof Boolean)&& map.get("attendee_status")!=null){
			this.setAttendee_status((String)map.get("attendee_status"));
		}
		if(!(map.get("byday") instanceof Boolean)&& map.get("byday")!=null){
			this.setByday((String)map.get("byday"));
		}
		if(!(map.get("categ_ids") instanceof Boolean)&& map.get("categ_ids")!=null){
			Object[] objs = (Object[])map.get("categ_ids");
			if(objs.length > 0){
				Integer[] categ_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setCateg_ids(Arrays.toString(categ_ids).replace(" ",""));
			}
		}
		if(!(map.get("count") instanceof Boolean)&& map.get("count")!=null){
			this.setCount((Integer)map.get("count"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("day") instanceof Boolean)&& map.get("day")!=null){
			this.setDay((Integer)map.get("day"));
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("display_start") instanceof Boolean)&& map.get("display_start")!=null){
			this.setDisplay_start((String)map.get("display_start"));
		}
		if(!(map.get("display_time") instanceof Boolean)&& map.get("display_time")!=null){
			this.setDisplay_time((String)map.get("display_time"));
		}
		if(!(map.get("duration") instanceof Boolean)&& map.get("duration")!=null){
			this.setDuration((Double)map.get("duration"));
		}
		if(!(map.get("end_type") instanceof Boolean)&& map.get("end_type")!=null){
			this.setEnd_type((String)map.get("end_type"));
		}
		if(!(map.get("final_date") instanceof Boolean)&& map.get("final_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("final_date"));
   			this.setFinal_date(new Timestamp(parse.getTime()));
		}
		if(map.get("fr") instanceof Boolean){
			this.setFr(((Boolean)map.get("fr"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("interval") instanceof Boolean)&& map.get("interval")!=null){
			this.setInterval((Integer)map.get("interval"));
		}
		if(map.get("is_attendee") instanceof Boolean){
			this.setIs_attendee(((Boolean)map.get("is_attendee"))? "true" : "false");
		}
		if(map.get("is_highlighted") instanceof Boolean){
			this.setIs_highlighted(((Boolean)map.get("is_highlighted"))? "true" : "false");
		}
		if(!(map.get("location") instanceof Boolean)&& map.get("location")!=null){
			this.setLocation((String)map.get("location"));
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids).replace(" ",""));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids).replace(" ",""));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids).replace(" ",""));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(map.get("mo") instanceof Boolean){
			this.setMo(((Boolean)map.get("mo"))? "true" : "false");
		}
		if(!(map.get("month_by") instanceof Boolean)&& map.get("month_by")!=null){
			this.setMonth_by((String)map.get("month_by"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("opportunity_id") instanceof Boolean)&& map.get("opportunity_id")!=null){
			Object[] objs = (Object[])map.get("opportunity_id");
			if(objs.length > 0){
				this.setOpportunity_id((Integer)objs[0]);
			}
		}
		if(!(map.get("opportunity_id") instanceof Boolean)&& map.get("opportunity_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("opportunity_id");
			if(objs.length > 1){
				this.setOpportunity_id_text((String)objs[1]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_ids") instanceof Boolean)&& map.get("partner_ids")!=null){
			Object[] objs = (Object[])map.get("partner_ids");
			if(objs.length > 0){
				Integer[] partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPartner_ids(Arrays.toString(partner_ids).replace(" ",""));
			}
		}
		if(!(map.get("privacy") instanceof Boolean)&& map.get("privacy")!=null){
			this.setPrivacy((String)map.get("privacy"));
		}
		if(map.get("recurrency") instanceof Boolean){
			this.setRecurrency(((Boolean)map.get("recurrency"))? "true" : "false");
		}
		if(!(map.get("recurrent_id") instanceof Boolean)&& map.get("recurrent_id")!=null){
			this.setRecurrent_id((Integer)map.get("recurrent_id"));
		}
		if(!(map.get("recurrent_id_date") instanceof Boolean)&& map.get("recurrent_id_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("recurrent_id_date"));
   			this.setRecurrent_id_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("res_id") instanceof Boolean)&& map.get("res_id")!=null){
			this.setRes_id((Integer)map.get("res_id"));
		}
		if(!(map.get("res_model") instanceof Boolean)&& map.get("res_model")!=null){
			this.setRes_model((String)map.get("res_model"));
		}
		if(!(map.get("res_model_id") instanceof Boolean)&& map.get("res_model_id")!=null){
			Object[] objs = (Object[])map.get("res_model_id");
			if(objs.length > 0){
				this.setRes_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("rrule") instanceof Boolean)&& map.get("rrule")!=null){
			this.setRrule((String)map.get("rrule"));
		}
		if(!(map.get("rrule_type") instanceof Boolean)&& map.get("rrule_type")!=null){
			this.setRrule_type((String)map.get("rrule_type"));
		}
		if(map.get("sa") instanceof Boolean){
			this.setSa(((Boolean)map.get("sa"))? "true" : "false");
		}
		if(!(map.get("show_as") instanceof Boolean)&& map.get("show_as")!=null){
			this.setShow_as((String)map.get("show_as"));
		}
		if(!(map.get("start") instanceof Boolean)&& map.get("start")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("start"));
   			this.setStart(new Timestamp(parse.getTime()));
		}
		if(!(map.get("start_date") instanceof Boolean)&& map.get("start_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("start_date"));
   			this.setStart_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("start_datetime") instanceof Boolean)&& map.get("start_datetime")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("start_datetime"));
   			this.setStart_datetime(new Timestamp(parse.getTime()));
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("stop") instanceof Boolean)&& map.get("stop")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("stop"));
   			this.setStop(new Timestamp(parse.getTime()));
		}
		if(!(map.get("stop_date") instanceof Boolean)&& map.get("stop_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("stop_date"));
   			this.setStop_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("stop_datetime") instanceof Boolean)&& map.get("stop_datetime")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("stop_datetime"));
   			this.setStop_datetime(new Timestamp(parse.getTime()));
		}
		if(map.get("su") instanceof Boolean){
			this.setSu(((Boolean)map.get("su"))? "true" : "false");
		}
		if(map.get("th") instanceof Boolean){
			this.setTh(((Boolean)map.get("th"))? "true" : "false");
		}
		if(map.get("tu") instanceof Boolean){
			this.setTu(((Boolean)map.get("tu"))? "true" : "false");
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(map.get("we") instanceof Boolean){
			this.setWe(((Boolean)map.get("we"))? "true" : "false");
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids).replace(" ",""));
			}
		}
		if(!(map.get("week_list") instanceof Boolean)&& map.get("week_list")!=null){
			this.setWeek_list((String)map.get("week_list"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getActivity_ids()!=null&&this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",this.getActivity_ids());
		}else if(this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",false);
		}
		if(this.getAlarm_ids()!=null&&this.getAlarm_idsDirtyFlag()){
			map.put("alarm_ids",this.getAlarm_ids());
		}else if(this.getAlarm_idsDirtyFlag()){
			map.put("alarm_ids",false);
		}
		if(this.getAllday()!=null&&this.getAlldayDirtyFlag()){
			map.put("allday",Boolean.parseBoolean(this.getAllday()));		
		}		if(this.getApplicant_id()!=null&&this.getApplicant_idDirtyFlag()){
			map.put("applicant_id",this.getApplicant_id());
		}else if(this.getApplicant_idDirtyFlag()){
			map.put("applicant_id",false);
		}
		if(this.getApplicant_id_text()!=null&&this.getApplicant_id_textDirtyFlag()){
			//忽略文本外键applicant_id_text
		}else if(this.getApplicant_id_textDirtyFlag()){
			map.put("applicant_id",false);
		}
		if(this.getAttendee_ids()!=null&&this.getAttendee_idsDirtyFlag()){
			map.put("attendee_ids",this.getAttendee_ids());
		}else if(this.getAttendee_idsDirtyFlag()){
			map.put("attendee_ids",false);
		}
		if(this.getAttendee_status()!=null&&this.getAttendee_statusDirtyFlag()){
			map.put("attendee_status",this.getAttendee_status());
		}else if(this.getAttendee_statusDirtyFlag()){
			map.put("attendee_status",false);
		}
		if(this.getByday()!=null&&this.getBydayDirtyFlag()){
			map.put("byday",this.getByday());
		}else if(this.getBydayDirtyFlag()){
			map.put("byday",false);
		}
		if(this.getCateg_ids()!=null&&this.getCateg_idsDirtyFlag()){
			map.put("categ_ids",this.getCateg_ids());
		}else if(this.getCateg_idsDirtyFlag()){
			map.put("categ_ids",false);
		}
		if(this.getCount()!=null&&this.getCountDirtyFlag()){
			map.put("count",this.getCount());
		}else if(this.getCountDirtyFlag()){
			map.put("count",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDay()!=null&&this.getDayDirtyFlag()){
			map.put("day",this.getDay());
		}else if(this.getDayDirtyFlag()){
			map.put("day",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getDisplay_start()!=null&&this.getDisplay_startDirtyFlag()){
			map.put("display_start",this.getDisplay_start());
		}else if(this.getDisplay_startDirtyFlag()){
			map.put("display_start",false);
		}
		if(this.getDisplay_time()!=null&&this.getDisplay_timeDirtyFlag()){
			map.put("display_time",this.getDisplay_time());
		}else if(this.getDisplay_timeDirtyFlag()){
			map.put("display_time",false);
		}
		if(this.getDuration()!=null&&this.getDurationDirtyFlag()){
			map.put("duration",this.getDuration());
		}else if(this.getDurationDirtyFlag()){
			map.put("duration",false);
		}
		if(this.getEnd_type()!=null&&this.getEnd_typeDirtyFlag()){
			map.put("end_type",this.getEnd_type());
		}else if(this.getEnd_typeDirtyFlag()){
			map.put("end_type",false);
		}
		if(this.getFinal_date()!=null&&this.getFinal_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getFinal_date());
			map.put("final_date",datetimeStr);
		}else if(this.getFinal_dateDirtyFlag()){
			map.put("final_date",false);
		}
		if(this.getFr()!=null&&this.getFrDirtyFlag()){
			map.put("fr",Boolean.parseBoolean(this.getFr()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInterval()!=null&&this.getIntervalDirtyFlag()){
			map.put("interval",this.getInterval());
		}else if(this.getIntervalDirtyFlag()){
			map.put("interval",false);
		}
		if(this.getIs_attendee()!=null&&this.getIs_attendeeDirtyFlag()){
			map.put("is_attendee",Boolean.parseBoolean(this.getIs_attendee()));		
		}		if(this.getIs_highlighted()!=null&&this.getIs_highlightedDirtyFlag()){
			map.put("is_highlighted",Boolean.parseBoolean(this.getIs_highlighted()));		
		}		if(this.getLocation()!=null&&this.getLocationDirtyFlag()){
			map.put("location",this.getLocation());
		}else if(this.getLocationDirtyFlag()){
			map.put("location",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getMo()!=null&&this.getMoDirtyFlag()){
			map.put("mo",Boolean.parseBoolean(this.getMo()));		
		}		if(this.getMonth_by()!=null&&this.getMonth_byDirtyFlag()){
			map.put("month_by",this.getMonth_by());
		}else if(this.getMonth_byDirtyFlag()){
			map.put("month_by",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getOpportunity_id()!=null&&this.getOpportunity_idDirtyFlag()){
			map.put("opportunity_id",this.getOpportunity_id());
		}else if(this.getOpportunity_idDirtyFlag()){
			map.put("opportunity_id",false);
		}
		if(this.getOpportunity_id_text()!=null&&this.getOpportunity_id_textDirtyFlag()){
			//忽略文本外键opportunity_id_text
		}else if(this.getOpportunity_id_textDirtyFlag()){
			map.put("opportunity_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_ids()!=null&&this.getPartner_idsDirtyFlag()){
			map.put("partner_ids",this.getPartner_ids());
		}else if(this.getPartner_idsDirtyFlag()){
			map.put("partner_ids",false);
		}
		if(this.getPrivacy()!=null&&this.getPrivacyDirtyFlag()){
			map.put("privacy",this.getPrivacy());
		}else if(this.getPrivacyDirtyFlag()){
			map.put("privacy",false);
		}
		if(this.getRecurrency()!=null&&this.getRecurrencyDirtyFlag()){
			map.put("recurrency",Boolean.parseBoolean(this.getRecurrency()));		
		}		if(this.getRecurrent_id()!=null&&this.getRecurrent_idDirtyFlag()){
			map.put("recurrent_id",this.getRecurrent_id());
		}else if(this.getRecurrent_idDirtyFlag()){
			map.put("recurrent_id",false);
		}
		if(this.getRecurrent_id_date()!=null&&this.getRecurrent_id_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getRecurrent_id_date());
			map.put("recurrent_id_date",datetimeStr);
		}else if(this.getRecurrent_id_dateDirtyFlag()){
			map.put("recurrent_id_date",false);
		}
		if(this.getRes_id()!=null&&this.getRes_idDirtyFlag()){
			map.put("res_id",this.getRes_id());
		}else if(this.getRes_idDirtyFlag()){
			map.put("res_id",false);
		}
		if(this.getRes_model()!=null&&this.getRes_modelDirtyFlag()){
			map.put("res_model",this.getRes_model());
		}else if(this.getRes_modelDirtyFlag()){
			map.put("res_model",false);
		}
		if(this.getRes_model_id()!=null&&this.getRes_model_idDirtyFlag()){
			map.put("res_model_id",this.getRes_model_id());
		}else if(this.getRes_model_idDirtyFlag()){
			map.put("res_model_id",false);
		}
		if(this.getRrule()!=null&&this.getRruleDirtyFlag()){
			map.put("rrule",this.getRrule());
		}else if(this.getRruleDirtyFlag()){
			map.put("rrule",false);
		}
		if(this.getRrule_type()!=null&&this.getRrule_typeDirtyFlag()){
			map.put("rrule_type",this.getRrule_type());
		}else if(this.getRrule_typeDirtyFlag()){
			map.put("rrule_type",false);
		}
		if(this.getSa()!=null&&this.getSaDirtyFlag()){
			map.put("sa",Boolean.parseBoolean(this.getSa()));		
		}		if(this.getShow_as()!=null&&this.getShow_asDirtyFlag()){
			map.put("show_as",this.getShow_as());
		}else if(this.getShow_asDirtyFlag()){
			map.put("show_as",false);
		}
		if(this.getStart()!=null&&this.getStartDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getStart());
			map.put("start",datetimeStr);
		}else if(this.getStartDirtyFlag()){
			map.put("start",false);
		}
		if(this.getStart_date()!=null&&this.getStart_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getStart_date());
			map.put("start_date",datetimeStr);
		}else if(this.getStart_dateDirtyFlag()){
			map.put("start_date",false);
		}
		if(this.getStart_datetime()!=null&&this.getStart_datetimeDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getStart_datetime());
			map.put("start_datetime",datetimeStr);
		}else if(this.getStart_datetimeDirtyFlag()){
			map.put("start_datetime",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getStop()!=null&&this.getStopDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getStop());
			map.put("stop",datetimeStr);
		}else if(this.getStopDirtyFlag()){
			map.put("stop",false);
		}
		if(this.getStop_date()!=null&&this.getStop_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getStop_date());
			map.put("stop_date",datetimeStr);
		}else if(this.getStop_dateDirtyFlag()){
			map.put("stop_date",false);
		}
		if(this.getStop_datetime()!=null&&this.getStop_datetimeDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getStop_datetime());
			map.put("stop_datetime",datetimeStr);
		}else if(this.getStop_datetimeDirtyFlag()){
			map.put("stop_datetime",false);
		}
		if(this.getSu()!=null&&this.getSuDirtyFlag()){
			map.put("su",Boolean.parseBoolean(this.getSu()));		
		}		if(this.getTh()!=null&&this.getThDirtyFlag()){
			map.put("th",Boolean.parseBoolean(this.getTh()));		
		}		if(this.getTu()!=null&&this.getTuDirtyFlag()){
			map.put("tu",Boolean.parseBoolean(this.getTu()));		
		}		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getWe()!=null&&this.getWeDirtyFlag()){
			map.put("we",Boolean.parseBoolean(this.getWe()));		
		}		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWeek_list()!=null&&this.getWeek_listDirtyFlag()){
			map.put("week_list",this.getWeek_list());
		}else if(this.getWeek_listDirtyFlag()){
			map.put("week_list",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
