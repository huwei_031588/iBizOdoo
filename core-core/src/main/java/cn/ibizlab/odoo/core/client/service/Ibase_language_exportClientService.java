package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_language_export;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_language_export] 服务对象接口
 */
public interface Ibase_language_exportClientService{

    public Ibase_language_export createModel() ;

    public void create(Ibase_language_export base_language_export);

    public void removeBatch(List<Ibase_language_export> base_language_exports);

    public void createBatch(List<Ibase_language_export> base_language_exports);

    public void get(Ibase_language_export base_language_export);

    public void update(Ibase_language_export base_language_export);

    public void remove(Ibase_language_export base_language_export);

    public Page<Ibase_language_export> search(SearchContext context);

    public void updateBatch(List<Ibase_language_export> base_language_exports);

    public Page<Ibase_language_export> select(SearchContext context);

}
