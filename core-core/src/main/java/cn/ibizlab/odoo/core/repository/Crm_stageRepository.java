package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_stage;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_stageSearchContext;

/**
 * 实体 [CRM 阶段] 存储对象
 */
public interface Crm_stageRepository extends Repository<Crm_stage> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_stage> searchDefault(Crm_stageSearchContext context);

    Crm_stage convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage domain , Crm_stage po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage convert2Domain( Crm_stage po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage domain) ;

}
