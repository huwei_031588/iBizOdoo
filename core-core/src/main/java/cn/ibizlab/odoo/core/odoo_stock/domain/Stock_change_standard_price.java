package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [更改标准价] 对象
 */
@Data
public class Stock_change_standard_price extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 对方科目必填
     */
    @DEField(name = "counterpart_account_id_required")
    @JSONField(name = "counterpart_account_id_required")
    @JsonProperty("counterpart_account_id_required")
    private String counterpartAccountIdRequired;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 价格
     */
    @DEField(name = "new_price")
    @JSONField(name = "new_price")
    @JsonProperty("new_price")
    private Double newPrice;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 对方科目
     */
    @JSONField(name = "counterpart_account_id_text")
    @JsonProperty("counterpart_account_id_text")
    private String counterpartAccountIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 对方科目
     */
    @DEField(name = "counterpart_account_id")
    @JSONField(name = "counterpart_account_id")
    @JsonProperty("counterpart_account_id")
    private Integer counterpartAccountId;


    /**
     * 
     */
    @JSONField(name = "odoocounterpartaccount")
    @JsonProperty("odoocounterpartaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooCounterpartAccount;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [对方科目必填]
     */
    public void setCounterpartAccountIdRequired(String counterpartAccountIdRequired){
        this.counterpartAccountIdRequired = counterpartAccountIdRequired ;
        this.modify("counterpart_account_id_required",counterpartAccountIdRequired);
    }
    /**
     * 设置 [价格]
     */
    public void setNewPrice(Double newPrice){
        this.newPrice = newPrice ;
        this.modify("new_price",newPrice);
    }
    /**
     * 设置 [对方科目]
     */
    public void setCounterpartAccountId(Integer counterpartAccountId){
        this.counterpartAccountId = counterpartAccountId ;
        this.modify("counterpart_account_id",counterpartAccountId);
    }

}


