package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_product.filter.Product_attribute_custom_valueSearchContext;

/**
 * 实体 [产品属性自定义值] 存储模型
 */
public interface Product_attribute_custom_value{

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 自定义值
     */
    String getCustom_value();

    void setCustom_value(String custom_value);

    /**
     * 获取 [自定义值]脏标记
     */
    boolean getCustom_valueDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 销售订单行
     */
    String getSale_order_line_id_text();

    void setSale_order_line_id_text(String sale_order_line_id_text);

    /**
     * 获取 [销售订单行]脏标记
     */
    boolean getSale_order_line_id_textDirtyFlag();

    /**
     * 属性
     */
    String getAttribute_value_id_text();

    void setAttribute_value_id_text(String attribute_value_id_text);

    /**
     * 获取 [属性]脏标记
     */
    boolean getAttribute_value_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 销售订单行
     */
    Integer getSale_order_line_id();

    void setSale_order_line_id(Integer sale_order_line_id);

    /**
     * 获取 [销售订单行]脏标记
     */
    boolean getSale_order_line_idDirtyFlag();

    /**
     * 属性
     */
    Integer getAttribute_value_id();

    void setAttribute_value_id(Integer attribute_value_id);

    /**
     * 获取 [属性]脏标记
     */
    boolean getAttribute_value_idDirtyFlag();

}
