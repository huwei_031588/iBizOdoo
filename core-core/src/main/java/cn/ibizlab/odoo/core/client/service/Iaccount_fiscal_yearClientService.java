package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_year;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_fiscal_year] 服务对象接口
 */
public interface Iaccount_fiscal_yearClientService{

    public Iaccount_fiscal_year createModel() ;

    public void removeBatch(List<Iaccount_fiscal_year> account_fiscal_years);

    public void updateBatch(List<Iaccount_fiscal_year> account_fiscal_years);

    public void get(Iaccount_fiscal_year account_fiscal_year);

    public void remove(Iaccount_fiscal_year account_fiscal_year);

    public void createBatch(List<Iaccount_fiscal_year> account_fiscal_years);

    public void update(Iaccount_fiscal_year account_fiscal_year);

    public Page<Iaccount_fiscal_year> search(SearchContext context);

    public void create(Iaccount_fiscal_year account_fiscal_year);

    public Page<Iaccount_fiscal_year> select(SearchContext context);

}
