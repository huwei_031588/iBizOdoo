package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_confirm;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_confirmSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_confirmService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_confirmOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_invoice_confirmClientModel;

/**
 * 实体[确认选定的发票] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_confirmServiceImpl implements IAccount_invoice_confirmService {

    @Autowired
    account_invoice_confirmOdooClient account_invoice_confirmOdooClient;


    @Override
    public boolean create(Account_invoice_confirm et) {
        account_invoice_confirmClientModel clientModel = convert2Model(et,null);
		account_invoice_confirmOdooClient.create(clientModel);
        Account_invoice_confirm rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_confirm> list){
    }

    @Override
    public Account_invoice_confirm get(Integer id) {
        account_invoice_confirmClientModel clientModel = new account_invoice_confirmClientModel();
        clientModel.setId(id);
		account_invoice_confirmOdooClient.get(clientModel);
        Account_invoice_confirm et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_invoice_confirm();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Account_invoice_confirm et) {
        account_invoice_confirmClientModel clientModel = convert2Model(et,null);
		account_invoice_confirmOdooClient.update(clientModel);
        Account_invoice_confirm rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_invoice_confirm> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_invoice_confirmClientModel clientModel = new account_invoice_confirmClientModel();
        clientModel.setId(id);
		account_invoice_confirmOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_confirm> searchDefault(Account_invoice_confirmSearchContext context) {
        List<Account_invoice_confirm> list = new ArrayList<Account_invoice_confirm>();
        Page<account_invoice_confirmClientModel> clientModelList = account_invoice_confirmOdooClient.search(context);
        for(account_invoice_confirmClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_invoice_confirm>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_invoice_confirmClientModel convert2Model(Account_invoice_confirm domain , account_invoice_confirmClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_invoice_confirmClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_invoice_confirm convert2Domain( account_invoice_confirmClientModel model ,Account_invoice_confirm domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_invoice_confirm();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



