package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Res_countrySearchContext;

/**
 * 实体 [国家/地区] 存储模型
 */
public interface Res_country{

    /**
     * 输入视图
     */
    Integer getAddress_view_id();

    void setAddress_view_id(Integer address_view_id);

    /**
     * 获取 [输入视图]脏标记
     */
    boolean getAddress_view_idDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 图像
     */
    byte[] getImage();

    void setImage(byte[] image);

    /**
     * 获取 [图像]脏标记
     */
    boolean getImageDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 国家/地区分组
     */
    String getCountry_group_ids();

    void setCountry_group_ids(String country_group_ids);

    /**
     * 获取 [国家/地区分组]脏标记
     */
    boolean getCountry_group_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 省份
     */
    String getState_ids();

    void setState_ids(String state_ids);

    /**
     * 获取 [省份]脏标记
     */
    boolean getState_idsDirtyFlag();

    /**
     * 客户姓名位置
     */
    String getName_position();

    void setName_position(String name_position);

    /**
     * 获取 [客户姓名位置]脏标记
     */
    boolean getName_positionDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 国家/地区长途区号
     */
    Integer getPhone_code();

    void setPhone_code(Integer phone_code);

    /**
     * 获取 [国家/地区长途区号]脏标记
     */
    boolean getPhone_codeDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 国家/地区代码
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [国家/地区代码]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 增值税标签
     */
    String getVat_label();

    void setVat_label(String vat_label);

    /**
     * 获取 [增值税标签]脏标记
     */
    boolean getVat_labelDirtyFlag();

    /**
     * 国家/地区名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [国家/地区名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 报表布局
     */
    String getAddress_format();

    void setAddress_format(String address_format);

    /**
     * 获取 [报表布局]脏标记
     */
    boolean getAddress_formatDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

}
