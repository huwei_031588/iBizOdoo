package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.maintenance_equipment;

/**
 * 实体[maintenance_equipment] 服务对象接口
 */
public interface maintenance_equipmentRepository{


    public maintenance_equipment createPO() ;
        public void remove(String id);

        public void removeBatch(String id);

        public void create(maintenance_equipment maintenance_equipment);

        public void updateBatch(maintenance_equipment maintenance_equipment);

        public void update(maintenance_equipment maintenance_equipment);

        public void get(String id);

        public List<maintenance_equipment> search();

        public void createBatch(maintenance_equipment maintenance_equipment);


}
