package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_invoice_refund;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_refundSearchContext;

/**
 * 实体 [信用票] 存储对象
 */
public interface Account_invoice_refundRepository extends Repository<Account_invoice_refund> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_invoice_refund> searchDefault(Account_invoice_refundSearchContext context);

    Account_invoice_refund convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_refund domain , Account_invoice_refund po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_refund convert2Domain( Account_invoice_refund po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_refund domain) ;

}
