package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_financial_year_op;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_financial_year_opSearchContext;

/**
 * 实体 [上个财政年的期初] 存储对象
 */
public interface Account_financial_year_opRepository extends Repository<Account_financial_year_op> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_financial_year_op> searchDefault(Account_financial_year_opSearchContext context);

    Account_financial_year_op convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op domain , Account_financial_year_op po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op convert2Domain( Account_financial_year_op po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op domain) ;

}
