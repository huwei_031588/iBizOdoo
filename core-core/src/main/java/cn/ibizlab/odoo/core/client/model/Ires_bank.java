package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [res_bank] 对象
 */
public interface Ires_bank {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [银行识别代码]
     */
    public void setBic(String bic);
    
    /**
     * 设置 [银行识别代码]
     */
    public String getBic();

    /**
     * 获取 [银行识别代码]脏标记
     */
    public boolean getBicDirtyFlag();
    /**
     * 获取 [城市]
     */
    public void setCity(String city);
    
    /**
     * 设置 [城市]
     */
    public String getCity();

    /**
     * 获取 [城市]脏标记
     */
    public boolean getCityDirtyFlag();
    /**
     * 获取 [国家/地区]
     */
    public void setCountry(Integer country);
    
    /**
     * 设置 [国家/地区]
     */
    public Integer getCountry();

    /**
     * 获取 [国家/地区]脏标记
     */
    public boolean getCountryDirtyFlag();
    /**
     * 获取 [国家/地区]
     */
    public void setCountry_text(String country_text);
    
    /**
     * 设置 [国家/地区]
     */
    public String getCountry_text();

    /**
     * 获取 [国家/地区]脏标记
     */
    public boolean getCountry_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [EMail]
     */
    public void setEmail(String email);
    
    /**
     * 设置 [EMail]
     */
    public String getEmail();

    /**
     * 获取 [EMail]脏标记
     */
    public boolean getEmailDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [电话]
     */
    public void setPhone(String phone);
    
    /**
     * 设置 [电话]
     */
    public String getPhone();

    /**
     * 获取 [电话]脏标记
     */
    public boolean getPhoneDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(Integer state);
    
    /**
     * 设置 [状态]
     */
    public Integer getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState_text(String state_text);
    
    /**
     * 设置 [状态]
     */
    public String getState_text();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getState_textDirtyFlag();
    /**
     * 获取 [街道]
     */
    public void setStreet(String street);
    
    /**
     * 设置 [街道]
     */
    public String getStreet();

    /**
     * 获取 [街道]脏标记
     */
    public boolean getStreetDirtyFlag();
    /**
     * 获取 [街道 2]
     */
    public void setStreet2(String street2);
    
    /**
     * 设置 [街道 2]
     */
    public String getStreet2();

    /**
     * 获取 [街道 2]脏标记
     */
    public boolean getStreet2DirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [邮政编码]
     */
    public void setZip(String zip);
    
    /**
     * 设置 [邮政编码]
     */
    public String getZip();

    /**
     * 获取 [邮政编码]脏标记
     */
    public boolean getZipDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
