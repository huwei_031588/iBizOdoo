package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_modelSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconcile_modelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_reconcile_modelOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_reconcile_modelClientModel;

/**
 * 实体[请在发票和付款匹配期间创建日记账分录] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_reconcile_modelServiceImpl implements IAccount_reconcile_modelService {

    @Autowired
    account_reconcile_modelOdooClient account_reconcile_modelOdooClient;


    @Override
    public Account_reconcile_model get(Integer id) {
        account_reconcile_modelClientModel clientModel = new account_reconcile_modelClientModel();
        clientModel.setId(id);
		account_reconcile_modelOdooClient.get(clientModel);
        Account_reconcile_model et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_reconcile_model();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_reconcile_modelClientModel clientModel = new account_reconcile_modelClientModel();
        clientModel.setId(id);
		account_reconcile_modelOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_reconcile_model et) {
        account_reconcile_modelClientModel clientModel = convert2Model(et,null);
		account_reconcile_modelOdooClient.create(clientModel);
        Account_reconcile_model rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_reconcile_model> list){
    }

    @Override
    public boolean update(Account_reconcile_model et) {
        account_reconcile_modelClientModel clientModel = convert2Model(et,null);
		account_reconcile_modelOdooClient.update(clientModel);
        Account_reconcile_model rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_reconcile_model> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_reconcile_model> searchDefault(Account_reconcile_modelSearchContext context) {
        List<Account_reconcile_model> list = new ArrayList<Account_reconcile_model>();
        Page<account_reconcile_modelClientModel> clientModelList = account_reconcile_modelOdooClient.search(context);
        for(account_reconcile_modelClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_reconcile_model>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_reconcile_modelClientModel convert2Model(Account_reconcile_model domain , account_reconcile_modelClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_reconcile_modelClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("labeldirtyflag"))
                model.setLabel(domain.getLabel());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("force_tax_includeddirtyflag"))
                model.setForce_tax_included(domain.getForceTaxIncluded());
            if((Boolean) domain.getExtensionparams().get("amount_typedirtyflag"))
                model.setAmount_type(domain.getAmountType());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("force_second_tax_includeddirtyflag"))
                model.setForce_second_tax_included(domain.getForceSecondTaxIncluded());
            if((Boolean) domain.getExtensionparams().get("auto_reconciledirtyflag"))
                model.setAuto_reconcile(domain.getAutoReconcile());
            if((Boolean) domain.getExtensionparams().get("match_total_amountdirtyflag"))
                model.setMatch_total_amount(domain.getMatchTotalAmount());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("match_journal_idsdirtyflag"))
                model.setMatch_journal_ids(domain.getMatchJournalIds());
            if((Boolean) domain.getExtensionparams().get("has_second_linedirtyflag"))
                model.setHas_second_line(domain.getHasSecondLine());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("match_naturedirtyflag"))
                model.setMatch_nature(domain.getMatchNature());
            if((Boolean) domain.getExtensionparams().get("second_amount_typedirtyflag"))
                model.setSecond_amount_type(domain.getSecondAmountType());
            if((Boolean) domain.getExtensionparams().get("match_labeldirtyflag"))
                model.setMatch_label(domain.getMatchLabel());
            if((Boolean) domain.getExtensionparams().get("match_amount_maxdirtyflag"))
                model.setMatch_amount_max(domain.getMatchAmountMax());
            if((Boolean) domain.getExtensionparams().get("match_total_amount_paramdirtyflag"))
                model.setMatch_total_amount_param(domain.getMatchTotalAmountParam());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("second_amountdirtyflag"))
                model.setSecond_amount(domain.getSecondAmount());
            if((Boolean) domain.getExtensionparams().get("match_amountdirtyflag"))
                model.setMatch_amount(domain.getMatchAmount());
            if((Boolean) domain.getExtensionparams().get("analytic_tag_idsdirtyflag"))
                model.setAnalytic_tag_ids(domain.getAnalyticTagIds());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("rule_typedirtyflag"))
                model.setRule_type(domain.getRuleType());
            if((Boolean) domain.getExtensionparams().get("match_partner_idsdirtyflag"))
                model.setMatch_partner_ids(domain.getMatchPartnerIds());
            if((Boolean) domain.getExtensionparams().get("match_partner_category_idsdirtyflag"))
                model.setMatch_partner_category_ids(domain.getMatchPartnerCategoryIds());
            if((Boolean) domain.getExtensionparams().get("second_labeldirtyflag"))
                model.setSecond_label(domain.getSecondLabel());
            if((Boolean) domain.getExtensionparams().get("match_partnerdirtyflag"))
                model.setMatch_partner(domain.getMatchPartner());
            if((Boolean) domain.getExtensionparams().get("match_amount_mindirtyflag"))
                model.setMatch_amount_min(domain.getMatchAmountMin());
            if((Boolean) domain.getExtensionparams().get("match_same_currencydirtyflag"))
                model.setMatch_same_currency(domain.getMatchSameCurrency());
            if((Boolean) domain.getExtensionparams().get("second_analytic_tag_idsdirtyflag"))
                model.setSecond_analytic_tag_ids(domain.getSecondAnalyticTagIds());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("match_label_paramdirtyflag"))
                model.setMatch_label_param(domain.getMatchLabelParam());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("second_analytic_account_id_textdirtyflag"))
                model.setSecond_analytic_account_id_text(domain.getSecondAnalyticAccountIdText());
            if((Boolean) domain.getExtensionparams().get("second_tax_id_textdirtyflag"))
                model.setSecond_tax_id_text(domain.getSecondTaxIdText());
            if((Boolean) domain.getExtensionparams().get("second_tax_amount_typedirtyflag"))
                model.setSecond_tax_amount_type(domain.getSecondTaxAmountType());
            if((Boolean) domain.getExtensionparams().get("tax_amount_typedirtyflag"))
                model.setTax_amount_type(domain.getTaxAmountType());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("second_journal_id_textdirtyflag"))
                model.setSecond_journal_id_text(domain.getSecondJournalIdText());
            if((Boolean) domain.getExtensionparams().get("is_tax_price_includeddirtyflag"))
                model.setIs_tax_price_included(domain.getIsTaxPriceIncluded());
            if((Boolean) domain.getExtensionparams().get("second_account_id_textdirtyflag"))
                model.setSecond_account_id_text(domain.getSecondAccountIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("analytic_account_id_textdirtyflag"))
                model.setAnalytic_account_id_text(domain.getAnalyticAccountIdText());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("tax_id_textdirtyflag"))
                model.setTax_id_text(domain.getTaxIdText());
            if((Boolean) domain.getExtensionparams().get("is_second_tax_price_includeddirtyflag"))
                model.setIs_second_tax_price_included(domain.getIsSecondTaxPriceIncluded());
            if((Boolean) domain.getExtensionparams().get("second_analytic_account_iddirtyflag"))
                model.setSecond_analytic_account_id(domain.getSecondAnalyticAccountId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("tax_iddirtyflag"))
                model.setTax_id(domain.getTaxId());
            if((Boolean) domain.getExtensionparams().get("second_account_iddirtyflag"))
                model.setSecond_account_id(domain.getSecondAccountId());
            if((Boolean) domain.getExtensionparams().get("second_journal_iddirtyflag"))
                model.setSecond_journal_id(domain.getSecondJournalId());
            if((Boolean) domain.getExtensionparams().get("analytic_account_iddirtyflag"))
                model.setAnalytic_account_id(domain.getAnalyticAccountId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("second_tax_iddirtyflag"))
                model.setSecond_tax_id(domain.getSecondTaxId());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_reconcile_model convert2Domain( account_reconcile_modelClientModel model ,Account_reconcile_model domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_reconcile_model();
        }

        if(model.getLabelDirtyFlag())
            domain.setLabel(model.getLabel());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getForce_tax_includedDirtyFlag())
            domain.setForceTaxIncluded(model.getForce_tax_included());
        if(model.getAmount_typeDirtyFlag())
            domain.setAmountType(model.getAmount_type());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getForce_second_tax_includedDirtyFlag())
            domain.setForceSecondTaxIncluded(model.getForce_second_tax_included());
        if(model.getAuto_reconcileDirtyFlag())
            domain.setAutoReconcile(model.getAuto_reconcile());
        if(model.getMatch_total_amountDirtyFlag())
            domain.setMatchTotalAmount(model.getMatch_total_amount());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getMatch_journal_idsDirtyFlag())
            domain.setMatchJournalIds(model.getMatch_journal_ids());
        if(model.getHas_second_lineDirtyFlag())
            domain.setHasSecondLine(model.getHas_second_line());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMatch_natureDirtyFlag())
            domain.setMatchNature(model.getMatch_nature());
        if(model.getSecond_amount_typeDirtyFlag())
            domain.setSecondAmountType(model.getSecond_amount_type());
        if(model.getMatch_labelDirtyFlag())
            domain.setMatchLabel(model.getMatch_label());
        if(model.getMatch_amount_maxDirtyFlag())
            domain.setMatchAmountMax(model.getMatch_amount_max());
        if(model.getMatch_total_amount_paramDirtyFlag())
            domain.setMatchTotalAmountParam(model.getMatch_total_amount_param());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getSecond_amountDirtyFlag())
            domain.setSecondAmount(model.getSecond_amount());
        if(model.getMatch_amountDirtyFlag())
            domain.setMatchAmount(model.getMatch_amount());
        if(model.getAnalytic_tag_idsDirtyFlag())
            domain.setAnalyticTagIds(model.getAnalytic_tag_ids());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getRule_typeDirtyFlag())
            domain.setRuleType(model.getRule_type());
        if(model.getMatch_partner_idsDirtyFlag())
            domain.setMatchPartnerIds(model.getMatch_partner_ids());
        if(model.getMatch_partner_category_idsDirtyFlag())
            domain.setMatchPartnerCategoryIds(model.getMatch_partner_category_ids());
        if(model.getSecond_labelDirtyFlag())
            domain.setSecondLabel(model.getSecond_label());
        if(model.getMatch_partnerDirtyFlag())
            domain.setMatchPartner(model.getMatch_partner());
        if(model.getMatch_amount_minDirtyFlag())
            domain.setMatchAmountMin(model.getMatch_amount_min());
        if(model.getMatch_same_currencyDirtyFlag())
            domain.setMatchSameCurrency(model.getMatch_same_currency());
        if(model.getSecond_analytic_tag_idsDirtyFlag())
            domain.setSecondAnalyticTagIds(model.getSecond_analytic_tag_ids());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getMatch_label_paramDirtyFlag())
            domain.setMatchLabelParam(model.getMatch_label_param());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getSecond_analytic_account_id_textDirtyFlag())
            domain.setSecondAnalyticAccountIdText(model.getSecond_analytic_account_id_text());
        if(model.getSecond_tax_id_textDirtyFlag())
            domain.setSecondTaxIdText(model.getSecond_tax_id_text());
        if(model.getSecond_tax_amount_typeDirtyFlag())
            domain.setSecondTaxAmountType(model.getSecond_tax_amount_type());
        if(model.getTax_amount_typeDirtyFlag())
            domain.setTaxAmountType(model.getTax_amount_type());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getSecond_journal_id_textDirtyFlag())
            domain.setSecondJournalIdText(model.getSecond_journal_id_text());
        if(model.getIs_tax_price_includedDirtyFlag())
            domain.setIsTaxPriceIncluded(model.getIs_tax_price_included());
        if(model.getSecond_account_id_textDirtyFlag())
            domain.setSecondAccountIdText(model.getSecond_account_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getAnalytic_account_id_textDirtyFlag())
            domain.setAnalyticAccountIdText(model.getAnalytic_account_id_text());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getTax_id_textDirtyFlag())
            domain.setTaxIdText(model.getTax_id_text());
        if(model.getIs_second_tax_price_includedDirtyFlag())
            domain.setIsSecondTaxPriceIncluded(model.getIs_second_tax_price_included());
        if(model.getSecond_analytic_account_idDirtyFlag())
            domain.setSecondAnalyticAccountId(model.getSecond_analytic_account_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getTax_idDirtyFlag())
            domain.setTaxId(model.getTax_id());
        if(model.getSecond_account_idDirtyFlag())
            domain.setSecondAccountId(model.getSecond_account_id());
        if(model.getSecond_journal_idDirtyFlag())
            domain.setSecondJournalId(model.getSecond_journal_id());
        if(model.getAnalytic_account_idDirtyFlag())
            domain.setAnalyticAccountId(model.getAnalytic_account_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getSecond_tax_idDirtyFlag())
            domain.setSecondTaxId(model.getSecond_tax_id());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



