package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_send;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_sendSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_sendService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_sendOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_invoice_sendClientModel;

/**
 * 实体[发送会计发票] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_sendServiceImpl implements IAccount_invoice_sendService {

    @Autowired
    account_invoice_sendOdooClient account_invoice_sendOdooClient;


    @Override
    public Account_invoice_send get(Integer id) {
        account_invoice_sendClientModel clientModel = new account_invoice_sendClientModel();
        clientModel.setId(id);
		account_invoice_sendOdooClient.get(clientModel);
        Account_invoice_send et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_invoice_send();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_invoice_sendClientModel clientModel = new account_invoice_sendClientModel();
        clientModel.setId(id);
		account_invoice_sendOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_invoice_send et) {
        account_invoice_sendClientModel clientModel = convert2Model(et,null);
		account_invoice_sendOdooClient.update(clientModel);
        Account_invoice_send rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_invoice_send> list){
    }

    @Override
    public boolean create(Account_invoice_send et) {
        account_invoice_sendClientModel clientModel = convert2Model(et,null);
		account_invoice_sendOdooClient.create(clientModel);
        Account_invoice_send rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_send> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_send> searchDefault(Account_invoice_sendSearchContext context) {
        List<Account_invoice_send> list = new ArrayList<Account_invoice_send>();
        Page<account_invoice_sendClientModel> clientModelList = account_invoice_sendOdooClient.search(context);
        for(account_invoice_sendClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_invoice_send>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_invoice_sendClientModel convert2Model(Account_invoice_send domain , account_invoice_sendClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_invoice_sendClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("rating_idsdirtyflag"))
                model.setRating_ids(domain.getRatingIds());
            if((Boolean) domain.getExtensionparams().get("tracking_value_idsdirtyflag"))
                model.setTracking_value_ids(domain.getTrackingValueIds());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("mailing_list_idsdirtyflag"))
                model.setMailing_list_ids(domain.getMailingListIds());
            if((Boolean) domain.getExtensionparams().get("printeddirtyflag"))
                model.setPrinted(domain.getPrinted());
            if((Boolean) domain.getExtensionparams().get("attachment_idsdirtyflag"))
                model.setAttachment_ids(domain.getAttachmentIds());
            if((Boolean) domain.getExtensionparams().get("invoice_without_emaildirtyflag"))
                model.setInvoice_without_email(domain.getInvoiceWithoutEmail());
            if((Boolean) domain.getExtensionparams().get("needaction_partner_idsdirtyflag"))
                model.setNeedaction_partner_ids(domain.getNeedactionPartnerIds());
            if((Boolean) domain.getExtensionparams().get("snailmail_is_letterdirtyflag"))
                model.setSnailmail_is_letter(domain.getSnailmailIsLetter());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("is_printdirtyflag"))
                model.setIs_print(domain.getIsPrint());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("snailmail_costdirtyflag"))
                model.setSnailmail_cost(domain.getSnailmailCost());
            if((Boolean) domain.getExtensionparams().get("channel_idsdirtyflag"))
                model.setChannel_ids(domain.getChannelIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("starred_partner_idsdirtyflag"))
                model.setStarred_partner_ids(domain.getStarredPartnerIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("partner_idsdirtyflag"))
                model.setPartner_ids(domain.getPartnerIds());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("letter_idsdirtyflag"))
                model.setLetter_ids(domain.getLetterIds());
            if((Boolean) domain.getExtensionparams().get("notification_idsdirtyflag"))
                model.setNotification_ids(domain.getNotificationIds());
            if((Boolean) domain.getExtensionparams().get("invoice_idsdirtyflag"))
                model.setInvoice_ids(domain.getInvoiceIds());
            if((Boolean) domain.getExtensionparams().get("is_emaildirtyflag"))
                model.setIs_email(domain.getIsEmail());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("use_active_domaindirtyflag"))
                model.setUse_active_domain(domain.getUseActiveDomain());
            if((Boolean) domain.getExtensionparams().get("moderation_statusdirtyflag"))
                model.setModeration_status(domain.getModerationStatus());
            if((Boolean) domain.getExtensionparams().get("moderator_iddirtyflag"))
                model.setModerator_id(domain.getModeratorId());
            if((Boolean) domain.getExtensionparams().get("has_errordirtyflag"))
                model.setHas_error(domain.getHasError());
            if((Boolean) domain.getExtensionparams().get("active_domaindirtyflag"))
                model.setActive_domain(domain.getActiveDomain());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("no_auto_threaddirtyflag"))
                model.setNo_auto_thread(domain.getNoAutoThread());
            if((Boolean) domain.getExtensionparams().get("composition_modedirtyflag"))
                model.setComposition_mode(domain.getCompositionMode());
            if((Boolean) domain.getExtensionparams().get("add_signdirtyflag"))
                model.setAdd_sign(domain.getAddSign());
            if((Boolean) domain.getExtensionparams().get("email_fromdirtyflag"))
                model.setEmail_from(domain.getEmailFrom());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("mail_activity_type_iddirtyflag"))
                model.setMail_activity_type_id(domain.getMailActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("needactiondirtyflag"))
                model.setNeedaction(domain.getNeedaction());
            if((Boolean) domain.getExtensionparams().get("subjectdirtyflag"))
                model.setSubject(domain.getSubject());
            if((Boolean) domain.getExtensionparams().get("record_namedirtyflag"))
                model.setRecord_name(domain.getRecordName());
            if((Boolean) domain.getExtensionparams().get("author_avatardirtyflag"))
                model.setAuthor_avatar(domain.getAuthorAvatar());
            if((Boolean) domain.getExtensionparams().get("rating_valuedirtyflag"))
                model.setRating_value(domain.getRatingValue());
            if((Boolean) domain.getExtensionparams().get("auto_deletedirtyflag"))
                model.setAuto_delete(domain.getAutoDelete());
            if((Boolean) domain.getExtensionparams().get("auto_delete_messagedirtyflag"))
                model.setAuto_delete_message(domain.getAutoDeleteMessage());
            if((Boolean) domain.getExtensionparams().get("bodydirtyflag"))
                model.setBody(domain.getBody());
            if((Boolean) domain.getExtensionparams().get("mail_server_iddirtyflag"))
                model.setMail_server_id(domain.getMailServerId());
            if((Boolean) domain.getExtensionparams().get("message_iddirtyflag"))
                model.setMessage_id(domain.getMessageId());
            if((Boolean) domain.getExtensionparams().get("modeldirtyflag"))
                model.setModel(domain.getModel());
            if((Boolean) domain.getExtensionparams().get("reply_todirtyflag"))
                model.setReply_to(domain.getReplyTo());
            if((Boolean) domain.getExtensionparams().get("message_typedirtyflag"))
                model.setMessage_type(domain.getMessageType());
            if((Boolean) domain.getExtensionparams().get("starreddirtyflag"))
                model.setStarred(domain.getStarred());
            if((Boolean) domain.getExtensionparams().get("is_logdirtyflag"))
                model.setIs_log(domain.getIsLog());
            if((Boolean) domain.getExtensionparams().get("notifydirtyflag"))
                model.setNotify(domain.getNotify());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_campaign_iddirtyflag"))
                model.setMass_mailing_campaign_id(domain.getMassMailingCampaignId());
            if((Boolean) domain.getExtensionparams().get("need_moderationdirtyflag"))
                model.setNeed_moderation(domain.getNeedModeration());
            if((Boolean) domain.getExtensionparams().get("subtype_iddirtyflag"))
                model.setSubtype_id(domain.getSubtypeId());
            if((Boolean) domain.getExtensionparams().get("author_iddirtyflag"))
                model.setAuthor_id(domain.getAuthorId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_iddirtyflag"))
                model.setMass_mailing_id(domain.getMassMailingId());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("template_id_textdirtyflag"))
                model.setTemplate_id_text(domain.getTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_namedirtyflag"))
                model.setMass_mailing_name(domain.getMassMailingName());
            if((Boolean) domain.getExtensionparams().get("layoutdirtyflag"))
                model.setLayout(domain.getLayout());
            if((Boolean) domain.getExtensionparams().get("composer_iddirtyflag"))
                model.setComposer_id(domain.getComposerId());
            if((Boolean) domain.getExtensionparams().get("template_iddirtyflag"))
                model.setTemplate_id(domain.getTemplateId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_invoice_send convert2Domain( account_invoice_sendClientModel model ,Account_invoice_send domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_invoice_send();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getRating_idsDirtyFlag())
            domain.setRatingIds(model.getRating_ids());
        if(model.getTracking_value_idsDirtyFlag())
            domain.setTrackingValueIds(model.getTracking_value_ids());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getMailing_list_idsDirtyFlag())
            domain.setMailingListIds(model.getMailing_list_ids());
        if(model.getPrintedDirtyFlag())
            domain.setPrinted(model.getPrinted());
        if(model.getAttachment_idsDirtyFlag())
            domain.setAttachmentIds(model.getAttachment_ids());
        if(model.getInvoice_without_emailDirtyFlag())
            domain.setInvoiceWithoutEmail(model.getInvoice_without_email());
        if(model.getNeedaction_partner_idsDirtyFlag())
            domain.setNeedactionPartnerIds(model.getNeedaction_partner_ids());
        if(model.getSnailmail_is_letterDirtyFlag())
            domain.setSnailmailIsLetter(model.getSnailmail_is_letter());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getIs_printDirtyFlag())
            domain.setIsPrint(model.getIs_print());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getSnailmail_costDirtyFlag())
            domain.setSnailmailCost(model.getSnailmail_cost());
        if(model.getChannel_idsDirtyFlag())
            domain.setChannelIds(model.getChannel_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getStarred_partner_idsDirtyFlag())
            domain.setStarredPartnerIds(model.getStarred_partner_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getPartner_idsDirtyFlag())
            domain.setPartnerIds(model.getPartner_ids());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getLetter_idsDirtyFlag())
            domain.setLetterIds(model.getLetter_ids());
        if(model.getNotification_idsDirtyFlag())
            domain.setNotificationIds(model.getNotification_ids());
        if(model.getInvoice_idsDirtyFlag())
            domain.setInvoiceIds(model.getInvoice_ids());
        if(model.getIs_emailDirtyFlag())
            domain.setIsEmail(model.getIs_email());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getUse_active_domainDirtyFlag())
            domain.setUseActiveDomain(model.getUse_active_domain());
        if(model.getModeration_statusDirtyFlag())
            domain.setModerationStatus(model.getModeration_status());
        if(model.getModerator_idDirtyFlag())
            domain.setModeratorId(model.getModerator_id());
        if(model.getHas_errorDirtyFlag())
            domain.setHasError(model.getHas_error());
        if(model.getActive_domainDirtyFlag())
            domain.setActiveDomain(model.getActive_domain());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getNo_auto_threadDirtyFlag())
            domain.setNoAutoThread(model.getNo_auto_thread());
        if(model.getComposition_modeDirtyFlag())
            domain.setCompositionMode(model.getComposition_mode());
        if(model.getAdd_signDirtyFlag())
            domain.setAddSign(model.getAdd_sign());
        if(model.getEmail_fromDirtyFlag())
            domain.setEmailFrom(model.getEmail_from());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getMail_activity_type_idDirtyFlag())
            domain.setMailActivityTypeId(model.getMail_activity_type_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getNeedactionDirtyFlag())
            domain.setNeedaction(model.getNeedaction());
        if(model.getSubjectDirtyFlag())
            domain.setSubject(model.getSubject());
        if(model.getRecord_nameDirtyFlag())
            domain.setRecordName(model.getRecord_name());
        if(model.getAuthor_avatarDirtyFlag())
            domain.setAuthorAvatar(model.getAuthor_avatar());
        if(model.getRating_valueDirtyFlag())
            domain.setRatingValue(model.getRating_value());
        if(model.getAuto_deleteDirtyFlag())
            domain.setAutoDelete(model.getAuto_delete());
        if(model.getAuto_delete_messageDirtyFlag())
            domain.setAutoDeleteMessage(model.getAuto_delete_message());
        if(model.getBodyDirtyFlag())
            domain.setBody(model.getBody());
        if(model.getMail_server_idDirtyFlag())
            domain.setMailServerId(model.getMail_server_id());
        if(model.getMessage_idDirtyFlag())
            domain.setMessageId(model.getMessage_id());
        if(model.getModelDirtyFlag())
            domain.setModel(model.getModel());
        if(model.getReply_toDirtyFlag())
            domain.setReplyTo(model.getReply_to());
        if(model.getMessage_typeDirtyFlag())
            domain.setMessageType(model.getMessage_type());
        if(model.getStarredDirtyFlag())
            domain.setStarred(model.getStarred());
        if(model.getIs_logDirtyFlag())
            domain.setIsLog(model.getIs_log());
        if(model.getNotifyDirtyFlag())
            domain.setNotify(model.getNotify());
        if(model.getMass_mailing_campaign_idDirtyFlag())
            domain.setMassMailingCampaignId(model.getMass_mailing_campaign_id());
        if(model.getNeed_moderationDirtyFlag())
            domain.setNeedModeration(model.getNeed_moderation());
        if(model.getSubtype_idDirtyFlag())
            domain.setSubtypeId(model.getSubtype_id());
        if(model.getAuthor_idDirtyFlag())
            domain.setAuthorId(model.getAuthor_id());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getMass_mailing_idDirtyFlag())
            domain.setMassMailingId(model.getMass_mailing_id());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getTemplate_id_textDirtyFlag())
            domain.setTemplateIdText(model.getTemplate_id_text());
        if(model.getMass_mailing_nameDirtyFlag())
            domain.setMassMailingName(model.getMass_mailing_name());
        if(model.getLayoutDirtyFlag())
            domain.setLayout(model.getLayout());
        if(model.getComposer_idDirtyFlag())
            domain.setComposerId(model.getComposer_id());
        if(model.getTemplate_idDirtyFlag())
            domain.setTemplateId(model.getTemplate_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



