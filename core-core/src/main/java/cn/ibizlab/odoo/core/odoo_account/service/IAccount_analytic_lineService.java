package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_lineSearchContext;


/**
 * 实体[Account_analytic_line] 服务对象接口
 */
public interface IAccount_analytic_lineService{

    boolean create(Account_analytic_line et) ;
    void createBatch(List<Account_analytic_line> list) ;
    Account_analytic_line get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_analytic_line et) ;
    void updateBatch(List<Account_analytic_line> list) ;
    Page<Account_analytic_line> searchDefault(Account_analytic_lineSearchContext context) ;

}



