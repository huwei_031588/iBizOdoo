package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_applicant_category;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicant_categorySearchContext;

/**
 * 实体 [申请人类别] 存储对象
 */
public interface Hr_applicant_categoryRepository extends Repository<Hr_applicant_category> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_applicant_category> searchDefault(Hr_applicant_categorySearchContext context);

    Hr_applicant_category convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant_category domain , Hr_applicant_category po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant_category convert2Domain( Hr_applicant_category po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant_category domain) ;

}
