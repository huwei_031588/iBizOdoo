package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_job;

/**
 * 实体[hr_job] 服务对象接口
 */
public interface hr_jobRepository{


    public hr_job createPO() ;
        public void update(hr_job hr_job);

        public void createBatch(hr_job hr_job);

        public void remove(String id);

        public void removeBatch(String id);

        public void create(hr_job hr_job);

        public List<hr_job> search();

        public void updateBatch(hr_job hr_job);

        public void get(String id);


}
