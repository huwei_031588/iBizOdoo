package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Event_mail_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mail_registrationSearchContext;

/**
 * 实体 [登记邮件调度] 存储对象
 */
public interface Event_mail_registrationRepository extends Repository<Event_mail_registration> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Event_mail_registration> searchDefault(Event_mail_registrationSearchContext context);

    Event_mail_registration convert2PO(cn.ibizlab.odoo.core.odoo_event.domain.Event_mail_registration domain , Event_mail_registration po) ;

    cn.ibizlab.odoo.core.odoo_event.domain.Event_mail_registration convert2Domain( Event_mail_registration po ,cn.ibizlab.odoo.core.odoo_event.domain.Event_mail_registration domain) ;

}
