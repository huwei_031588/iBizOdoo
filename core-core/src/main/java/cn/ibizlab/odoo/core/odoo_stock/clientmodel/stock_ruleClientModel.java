package cn.ibizlab.odoo.core.odoo_stock.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[stock_rule] 对象
 */
public class stock_ruleClientModel implements Serializable{

    /**
     * 动作
     */
    public String action;

    @JsonIgnore
    public boolean actionDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 自动移动
     */
    public String auto;

    @JsonIgnore
    public boolean autoDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 延迟
     */
    public Integer delay;

    @JsonIgnore
    public boolean delayDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 固定的补货组
     */
    public Integer group_id;

    @JsonIgnore
    public boolean group_idDirtyFlag;
    
    /**
     * 补货组的传播
     */
    public String group_propagation_option;

    @JsonIgnore
    public boolean group_propagation_optionDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 目的位置
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 目的位置
     */
    public String location_id_text;

    @JsonIgnore
    public boolean location_id_textDirtyFlag;
    
    /**
     * 源位置
     */
    public Integer location_src_id;

    @JsonIgnore
    public boolean location_src_idDirtyFlag;
    
    /**
     * 源位置
     */
    public String location_src_id_text;

    @JsonIgnore
    public boolean location_src_id_textDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 业务伙伴地址
     */
    public Integer partner_address_id;

    @JsonIgnore
    public boolean partner_address_idDirtyFlag;
    
    /**
     * 业务伙伴地址
     */
    public String partner_address_id_text;

    @JsonIgnore
    public boolean partner_address_id_textDirtyFlag;
    
    /**
     * 作业类型
     */
    public Integer picking_type_id;

    @JsonIgnore
    public boolean picking_type_idDirtyFlag;
    
    /**
     * 作业类型
     */
    public String picking_type_id_text;

    @JsonIgnore
    public boolean picking_type_id_textDirtyFlag;
    
    /**
     * 移动供应方法
     */
    public String procure_method;

    @JsonIgnore
    public boolean procure_methodDirtyFlag;
    
    /**
     * 传播取消以及拆分
     */
    public String propagate;

    @JsonIgnore
    public boolean propagateDirtyFlag;
    
    /**
     * 传播的仓库
     */
    public Integer propagate_warehouse_id;

    @JsonIgnore
    public boolean propagate_warehouse_idDirtyFlag;
    
    /**
     * 传播的仓库
     */
    public String propagate_warehouse_id_text;

    @JsonIgnore
    public boolean propagate_warehouse_id_textDirtyFlag;
    
    /**
     * 路线
     */
    public Integer route_id;

    @JsonIgnore
    public boolean route_idDirtyFlag;
    
    /**
     * 路线
     */
    public String route_id_text;

    @JsonIgnore
    public boolean route_id_textDirtyFlag;
    
    /**
     * 路线序列
     */
    public Integer route_sequence;

    @JsonIgnore
    public boolean route_sequenceDirtyFlag;
    
    /**
     * 规则消息
     */
    public String rule_message;

    @JsonIgnore
    public boolean rule_messageDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 仓库
     */
    public Integer warehouse_id;

    @JsonIgnore
    public boolean warehouse_idDirtyFlag;
    
    /**
     * 仓库
     */
    public String warehouse_id_text;

    @JsonIgnore
    public boolean warehouse_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [动作]
     */
    @JsonProperty("action")
    public String getAction(){
        return this.action ;
    }

    /**
     * 设置 [动作]
     */
    @JsonProperty("action")
    public void setAction(String  action){
        this.action = action ;
        this.actionDirtyFlag = true ;
    }

     /**
     * 获取 [动作]脏标记
     */
    @JsonIgnore
    public boolean getActionDirtyFlag(){
        return this.actionDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [自动移动]
     */
    @JsonProperty("auto")
    public String getAuto(){
        return this.auto ;
    }

    /**
     * 设置 [自动移动]
     */
    @JsonProperty("auto")
    public void setAuto(String  auto){
        this.auto = auto ;
        this.autoDirtyFlag = true ;
    }

     /**
     * 获取 [自动移动]脏标记
     */
    @JsonIgnore
    public boolean getAutoDirtyFlag(){
        return this.autoDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [延迟]
     */
    @JsonProperty("delay")
    public Integer getDelay(){
        return this.delay ;
    }

    /**
     * 设置 [延迟]
     */
    @JsonProperty("delay")
    public void setDelay(Integer  delay){
        this.delay = delay ;
        this.delayDirtyFlag = true ;
    }

     /**
     * 获取 [延迟]脏标记
     */
    @JsonIgnore
    public boolean getDelayDirtyFlag(){
        return this.delayDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [固定的补货组]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return this.group_id ;
    }

    /**
     * 设置 [固定的补货组]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

     /**
     * 获取 [固定的补货组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return this.group_idDirtyFlag ;
    }   

    /**
     * 获取 [补货组的传播]
     */
    @JsonProperty("group_propagation_option")
    public String getGroup_propagation_option(){
        return this.group_propagation_option ;
    }

    /**
     * 设置 [补货组的传播]
     */
    @JsonProperty("group_propagation_option")
    public void setGroup_propagation_option(String  group_propagation_option){
        this.group_propagation_option = group_propagation_option ;
        this.group_propagation_optionDirtyFlag = true ;
    }

     /**
     * 获取 [补货组的传播]脏标记
     */
    @JsonIgnore
    public boolean getGroup_propagation_optionDirtyFlag(){
        return this.group_propagation_optionDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [目的位置]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [目的位置]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [目的位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [目的位置]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return this.location_id_text ;
    }

    /**
     * 设置 [目的位置]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目的位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return this.location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [源位置]
     */
    @JsonProperty("location_src_id")
    public Integer getLocation_src_id(){
        return this.location_src_id ;
    }

    /**
     * 设置 [源位置]
     */
    @JsonProperty("location_src_id")
    public void setLocation_src_id(Integer  location_src_id){
        this.location_src_id = location_src_id ;
        this.location_src_idDirtyFlag = true ;
    }

     /**
     * 获取 [源位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_src_idDirtyFlag(){
        return this.location_src_idDirtyFlag ;
    }   

    /**
     * 获取 [源位置]
     */
    @JsonProperty("location_src_id_text")
    public String getLocation_src_id_text(){
        return this.location_src_id_text ;
    }

    /**
     * 设置 [源位置]
     */
    @JsonProperty("location_src_id_text")
    public void setLocation_src_id_text(String  location_src_id_text){
        this.location_src_id_text = location_src_id_text ;
        this.location_src_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [源位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_src_id_textDirtyFlag(){
        return this.location_src_id_textDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴地址]
     */
    @JsonProperty("partner_address_id")
    public Integer getPartner_address_id(){
        return this.partner_address_id ;
    }

    /**
     * 设置 [业务伙伴地址]
     */
    @JsonProperty("partner_address_id")
    public void setPartner_address_id(Integer  partner_address_id){
        this.partner_address_id = partner_address_id ;
        this.partner_address_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_idDirtyFlag(){
        return this.partner_address_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴地址]
     */
    @JsonProperty("partner_address_id_text")
    public String getPartner_address_id_text(){
        return this.partner_address_id_text ;
    }

    /**
     * 设置 [业务伙伴地址]
     */
    @JsonProperty("partner_address_id_text")
    public void setPartner_address_id_text(String  partner_address_id_text){
        this.partner_address_id_text = partner_address_id_text ;
        this.partner_address_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_id_textDirtyFlag(){
        return this.partner_address_id_textDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return this.picking_type_id ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return this.picking_type_idDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return this.picking_type_id_text ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return this.picking_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [移动供应方法]
     */
    @JsonProperty("procure_method")
    public String getProcure_method(){
        return this.procure_method ;
    }

    /**
     * 设置 [移动供应方法]
     */
    @JsonProperty("procure_method")
    public void setProcure_method(String  procure_method){
        this.procure_method = procure_method ;
        this.procure_methodDirtyFlag = true ;
    }

     /**
     * 获取 [移动供应方法]脏标记
     */
    @JsonIgnore
    public boolean getProcure_methodDirtyFlag(){
        return this.procure_methodDirtyFlag ;
    }   

    /**
     * 获取 [传播取消以及拆分]
     */
    @JsonProperty("propagate")
    public String getPropagate(){
        return this.propagate ;
    }

    /**
     * 设置 [传播取消以及拆分]
     */
    @JsonProperty("propagate")
    public void setPropagate(String  propagate){
        this.propagate = propagate ;
        this.propagateDirtyFlag = true ;
    }

     /**
     * 获取 [传播取消以及拆分]脏标记
     */
    @JsonIgnore
    public boolean getPropagateDirtyFlag(){
        return this.propagateDirtyFlag ;
    }   

    /**
     * 获取 [传播的仓库]
     */
    @JsonProperty("propagate_warehouse_id")
    public Integer getPropagate_warehouse_id(){
        return this.propagate_warehouse_id ;
    }

    /**
     * 设置 [传播的仓库]
     */
    @JsonProperty("propagate_warehouse_id")
    public void setPropagate_warehouse_id(Integer  propagate_warehouse_id){
        this.propagate_warehouse_id = propagate_warehouse_id ;
        this.propagate_warehouse_idDirtyFlag = true ;
    }

     /**
     * 获取 [传播的仓库]脏标记
     */
    @JsonIgnore
    public boolean getPropagate_warehouse_idDirtyFlag(){
        return this.propagate_warehouse_idDirtyFlag ;
    }   

    /**
     * 获取 [传播的仓库]
     */
    @JsonProperty("propagate_warehouse_id_text")
    public String getPropagate_warehouse_id_text(){
        return this.propagate_warehouse_id_text ;
    }

    /**
     * 设置 [传播的仓库]
     */
    @JsonProperty("propagate_warehouse_id_text")
    public void setPropagate_warehouse_id_text(String  propagate_warehouse_id_text){
        this.propagate_warehouse_id_text = propagate_warehouse_id_text ;
        this.propagate_warehouse_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [传播的仓库]脏标记
     */
    @JsonIgnore
    public boolean getPropagate_warehouse_id_textDirtyFlag(){
        return this.propagate_warehouse_id_textDirtyFlag ;
    }   

    /**
     * 获取 [路线]
     */
    @JsonProperty("route_id")
    public Integer getRoute_id(){
        return this.route_id ;
    }

    /**
     * 设置 [路线]
     */
    @JsonProperty("route_id")
    public void setRoute_id(Integer  route_id){
        this.route_id = route_id ;
        this.route_idDirtyFlag = true ;
    }

     /**
     * 获取 [路线]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idDirtyFlag(){
        return this.route_idDirtyFlag ;
    }   

    /**
     * 获取 [路线]
     */
    @JsonProperty("route_id_text")
    public String getRoute_id_text(){
        return this.route_id_text ;
    }

    /**
     * 设置 [路线]
     */
    @JsonProperty("route_id_text")
    public void setRoute_id_text(String  route_id_text){
        this.route_id_text = route_id_text ;
        this.route_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [路线]脏标记
     */
    @JsonIgnore
    public boolean getRoute_id_textDirtyFlag(){
        return this.route_id_textDirtyFlag ;
    }   

    /**
     * 获取 [路线序列]
     */
    @JsonProperty("route_sequence")
    public Integer getRoute_sequence(){
        return this.route_sequence ;
    }

    /**
     * 设置 [路线序列]
     */
    @JsonProperty("route_sequence")
    public void setRoute_sequence(Integer  route_sequence){
        this.route_sequence = route_sequence ;
        this.route_sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [路线序列]脏标记
     */
    @JsonIgnore
    public boolean getRoute_sequenceDirtyFlag(){
        return this.route_sequenceDirtyFlag ;
    }   

    /**
     * 获取 [规则消息]
     */
    @JsonProperty("rule_message")
    public String getRule_message(){
        return this.rule_message ;
    }

    /**
     * 设置 [规则消息]
     */
    @JsonProperty("rule_message")
    public void setRule_message(String  rule_message){
        this.rule_message = rule_message ;
        this.rule_messageDirtyFlag = true ;
    }

     /**
     * 获取 [规则消息]脏标记
     */
    @JsonIgnore
    public boolean getRule_messageDirtyFlag(){
        return this.rule_messageDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return this.warehouse_id ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return this.warehouse_idDirtyFlag ;
    }   

    /**
     * 获取 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return this.warehouse_id_text ;
    }

    /**
     * 设置 [仓库]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [仓库]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return this.warehouse_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("action") instanceof Boolean)&& map.get("action")!=null){
			this.setAction((String)map.get("action"));
		}
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("auto") instanceof Boolean)&& map.get("auto")!=null){
			this.setAuto((String)map.get("auto"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("delay") instanceof Boolean)&& map.get("delay")!=null){
			this.setDelay((Integer)map.get("delay"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("group_id") instanceof Boolean)&& map.get("group_id")!=null){
			Object[] objs = (Object[])map.get("group_id");
			if(objs.length > 0){
				this.setGroup_id((Integer)objs[0]);
			}
		}
		if(!(map.get("group_propagation_option") instanceof Boolean)&& map.get("group_propagation_option")!=null){
			this.setGroup_propagation_option((String)map.get("group_propagation_option"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("location_id") instanceof Boolean)&& map.get("location_id")!=null){
			Object[] objs = (Object[])map.get("location_id");
			if(objs.length > 0){
				this.setLocation_id((Integer)objs[0]);
			}
		}
		if(!(map.get("location_id") instanceof Boolean)&& map.get("location_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("location_id");
			if(objs.length > 1){
				this.setLocation_id_text((String)objs[1]);
			}
		}
		if(!(map.get("location_src_id") instanceof Boolean)&& map.get("location_src_id")!=null){
			Object[] objs = (Object[])map.get("location_src_id");
			if(objs.length > 0){
				this.setLocation_src_id((Integer)objs[0]);
			}
		}
		if(!(map.get("location_src_id") instanceof Boolean)&& map.get("location_src_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("location_src_id");
			if(objs.length > 1){
				this.setLocation_src_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("partner_address_id") instanceof Boolean)&& map.get("partner_address_id")!=null){
			Object[] objs = (Object[])map.get("partner_address_id");
			if(objs.length > 0){
				this.setPartner_address_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_address_id") instanceof Boolean)&& map.get("partner_address_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_address_id");
			if(objs.length > 1){
				this.setPartner_address_id_text((String)objs[1]);
			}
		}
		if(!(map.get("picking_type_id") instanceof Boolean)&& map.get("picking_type_id")!=null){
			Object[] objs = (Object[])map.get("picking_type_id");
			if(objs.length > 0){
				this.setPicking_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("picking_type_id") instanceof Boolean)&& map.get("picking_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("picking_type_id");
			if(objs.length > 1){
				this.setPicking_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("procure_method") instanceof Boolean)&& map.get("procure_method")!=null){
			this.setProcure_method((String)map.get("procure_method"));
		}
		if(map.get("propagate") instanceof Boolean){
			this.setPropagate(((Boolean)map.get("propagate"))? "true" : "false");
		}
		if(!(map.get("propagate_warehouse_id") instanceof Boolean)&& map.get("propagate_warehouse_id")!=null){
			Object[] objs = (Object[])map.get("propagate_warehouse_id");
			if(objs.length > 0){
				this.setPropagate_warehouse_id((Integer)objs[0]);
			}
		}
		if(!(map.get("propagate_warehouse_id") instanceof Boolean)&& map.get("propagate_warehouse_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("propagate_warehouse_id");
			if(objs.length > 1){
				this.setPropagate_warehouse_id_text((String)objs[1]);
			}
		}
		if(!(map.get("route_id") instanceof Boolean)&& map.get("route_id")!=null){
			Object[] objs = (Object[])map.get("route_id");
			if(objs.length > 0){
				this.setRoute_id((Integer)objs[0]);
			}
		}
		if(!(map.get("route_id") instanceof Boolean)&& map.get("route_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("route_id");
			if(objs.length > 1){
				this.setRoute_id_text((String)objs[1]);
			}
		}
		if(!(map.get("route_sequence") instanceof Boolean)&& map.get("route_sequence")!=null){
			this.setRoute_sequence((Integer)map.get("route_sequence"));
		}
		if(!(map.get("rule_message") instanceof Boolean)&& map.get("rule_message")!=null){
			this.setRule_message((String)map.get("rule_message"));
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("warehouse_id") instanceof Boolean)&& map.get("warehouse_id")!=null){
			Object[] objs = (Object[])map.get("warehouse_id");
			if(objs.length > 0){
				this.setWarehouse_id((Integer)objs[0]);
			}
		}
		if(!(map.get("warehouse_id") instanceof Boolean)&& map.get("warehouse_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("warehouse_id");
			if(objs.length > 1){
				this.setWarehouse_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAction()!=null&&this.getActionDirtyFlag()){
			map.put("action",this.getAction());
		}else if(this.getActionDirtyFlag()){
			map.put("action",false);
		}
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getAuto()!=null&&this.getAutoDirtyFlag()){
			map.put("auto",this.getAuto());
		}else if(this.getAutoDirtyFlag()){
			map.put("auto",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDelay()!=null&&this.getDelayDirtyFlag()){
			map.put("delay",this.getDelay());
		}else if(this.getDelayDirtyFlag()){
			map.put("delay",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getGroup_id()!=null&&this.getGroup_idDirtyFlag()){
			map.put("group_id",this.getGroup_id());
		}else if(this.getGroup_idDirtyFlag()){
			map.put("group_id",false);
		}
		if(this.getGroup_propagation_option()!=null&&this.getGroup_propagation_optionDirtyFlag()){
			map.put("group_propagation_option",this.getGroup_propagation_option());
		}else if(this.getGroup_propagation_optionDirtyFlag()){
			map.put("group_propagation_option",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLocation_id()!=null&&this.getLocation_idDirtyFlag()){
			map.put("location_id",this.getLocation_id());
		}else if(this.getLocation_idDirtyFlag()){
			map.put("location_id",false);
		}
		if(this.getLocation_id_text()!=null&&this.getLocation_id_textDirtyFlag()){
			//忽略文本外键location_id_text
		}else if(this.getLocation_id_textDirtyFlag()){
			map.put("location_id",false);
		}
		if(this.getLocation_src_id()!=null&&this.getLocation_src_idDirtyFlag()){
			map.put("location_src_id",this.getLocation_src_id());
		}else if(this.getLocation_src_idDirtyFlag()){
			map.put("location_src_id",false);
		}
		if(this.getLocation_src_id_text()!=null&&this.getLocation_src_id_textDirtyFlag()){
			//忽略文本外键location_src_id_text
		}else if(this.getLocation_src_id_textDirtyFlag()){
			map.put("location_src_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPartner_address_id()!=null&&this.getPartner_address_idDirtyFlag()){
			map.put("partner_address_id",this.getPartner_address_id());
		}else if(this.getPartner_address_idDirtyFlag()){
			map.put("partner_address_id",false);
		}
		if(this.getPartner_address_id_text()!=null&&this.getPartner_address_id_textDirtyFlag()){
			//忽略文本外键partner_address_id_text
		}else if(this.getPartner_address_id_textDirtyFlag()){
			map.put("partner_address_id",false);
		}
		if(this.getPicking_type_id()!=null&&this.getPicking_type_idDirtyFlag()){
			map.put("picking_type_id",this.getPicking_type_id());
		}else if(this.getPicking_type_idDirtyFlag()){
			map.put("picking_type_id",false);
		}
		if(this.getPicking_type_id_text()!=null&&this.getPicking_type_id_textDirtyFlag()){
			//忽略文本外键picking_type_id_text
		}else if(this.getPicking_type_id_textDirtyFlag()){
			map.put("picking_type_id",false);
		}
		if(this.getProcure_method()!=null&&this.getProcure_methodDirtyFlag()){
			map.put("procure_method",this.getProcure_method());
		}else if(this.getProcure_methodDirtyFlag()){
			map.put("procure_method",false);
		}
		if(this.getPropagate()!=null&&this.getPropagateDirtyFlag()){
			map.put("propagate",Boolean.parseBoolean(this.getPropagate()));		
		}		if(this.getPropagate_warehouse_id()!=null&&this.getPropagate_warehouse_idDirtyFlag()){
			map.put("propagate_warehouse_id",this.getPropagate_warehouse_id());
		}else if(this.getPropagate_warehouse_idDirtyFlag()){
			map.put("propagate_warehouse_id",false);
		}
		if(this.getPropagate_warehouse_id_text()!=null&&this.getPropagate_warehouse_id_textDirtyFlag()){
			//忽略文本外键propagate_warehouse_id_text
		}else if(this.getPropagate_warehouse_id_textDirtyFlag()){
			map.put("propagate_warehouse_id",false);
		}
		if(this.getRoute_id()!=null&&this.getRoute_idDirtyFlag()){
			map.put("route_id",this.getRoute_id());
		}else if(this.getRoute_idDirtyFlag()){
			map.put("route_id",false);
		}
		if(this.getRoute_id_text()!=null&&this.getRoute_id_textDirtyFlag()){
			//忽略文本外键route_id_text
		}else if(this.getRoute_id_textDirtyFlag()){
			map.put("route_id",false);
		}
		if(this.getRoute_sequence()!=null&&this.getRoute_sequenceDirtyFlag()){
			map.put("route_sequence",this.getRoute_sequence());
		}else if(this.getRoute_sequenceDirtyFlag()){
			map.put("route_sequence",false);
		}
		if(this.getRule_message()!=null&&this.getRule_messageDirtyFlag()){
			map.put("rule_message",this.getRule_message());
		}else if(this.getRule_messageDirtyFlag()){
			map.put("rule_message",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getWarehouse_id()!=null&&this.getWarehouse_idDirtyFlag()){
			map.put("warehouse_id",this.getWarehouse_id());
		}else if(this.getWarehouse_idDirtyFlag()){
			map.put("warehouse_id",false);
		}
		if(this.getWarehouse_id_text()!=null&&this.getWarehouse_id_textDirtyFlag()){
			//忽略文本外键warehouse_id_text
		}else if(this.getWarehouse_id_textDirtyFlag()){
			map.put("warehouse_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
