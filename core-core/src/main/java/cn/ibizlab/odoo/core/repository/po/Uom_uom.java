package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_uomSearchContext;

/**
 * 实体 [产品计量单位] 存储模型
 */
public interface Uom_uom{

    /**
     * 单位
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [单位]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 更大比率
     */
    Double getFactor_inv();

    void setFactor_inv(Double factor_inv);

    /**
     * 获取 [更大比率]脏标记
     */
    boolean getFactor_invDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 舍入精度
     */
    Double getRounding();

    void setRounding(Double rounding);

    /**
     * 获取 [舍入精度]脏标记
     */
    boolean getRoundingDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 比例
     */
    Double getFactor();

    void setFactor(Double factor);

    /**
     * 获取 [比例]脏标记
     */
    boolean getFactorDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 类型
     */
    String getUom_type();

    void setUom_type(String uom_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getUom_typeDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 类别
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [类别]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 分组销售点中的产品
     */
    String getIs_pos_groupable();

    void setIs_pos_groupable(String is_pos_groupable);

    /**
     * 获取 [分组销售点中的产品]脏标记
     */
    boolean getIs_pos_groupableDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 计量单位的类别
     */
    String getMeasure_type();

    void setMeasure_type(String measure_type);

    /**
     * 获取 [计量单位的类别]脏标记
     */
    boolean getMeasure_typeDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 类别
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [类别]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
