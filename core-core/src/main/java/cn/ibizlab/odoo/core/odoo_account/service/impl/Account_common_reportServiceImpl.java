package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_common_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_common_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_common_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_common_reportOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_common_reportClientModel;

/**
 * 实体[账户通用报表] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_common_reportServiceImpl implements IAccount_common_reportService {

    @Autowired
    account_common_reportOdooClient account_common_reportOdooClient;


    @Override
    public Account_common_report get(Integer id) {
        account_common_reportClientModel clientModel = new account_common_reportClientModel();
        clientModel.setId(id);
		account_common_reportOdooClient.get(clientModel);
        Account_common_report et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_common_report();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_common_reportClientModel clientModel = new account_common_reportClientModel();
        clientModel.setId(id);
		account_common_reportOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_common_report et) {
        account_common_reportClientModel clientModel = convert2Model(et,null);
		account_common_reportOdooClient.create(clientModel);
        Account_common_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_common_report> list){
    }

    @Override
    public boolean update(Account_common_report et) {
        account_common_reportClientModel clientModel = convert2Model(et,null);
		account_common_reportOdooClient.update(clientModel);
        Account_common_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_common_report> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_common_report> searchDefault(Account_common_reportSearchContext context) {
        List<Account_common_report> list = new ArrayList<Account_common_report>();
        Page<account_common_reportClientModel> clientModelList = account_common_reportOdooClient.search(context);
        for(account_common_reportClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_common_report>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_common_reportClientModel convert2Model(Account_common_report domain , account_common_reportClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_common_reportClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("date_todirtyflag"))
                model.setDate_to(domain.getDateTo());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("date_fromdirtyflag"))
                model.setDate_from(domain.getDateFrom());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("target_movedirtyflag"))
                model.setTarget_move(domain.getTargetMove());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("journal_idsdirtyflag"))
                model.setJournal_ids(domain.getJournalIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_common_report convert2Domain( account_common_reportClientModel model ,Account_common_report domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_common_report();
        }

        if(model.getDate_toDirtyFlag())
            domain.setDateTo(model.getDate_to());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDate_fromDirtyFlag())
            domain.setDateFrom(model.getDate_from());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getTarget_moveDirtyFlag())
            domain.setTargetMove(model.getTarget_move());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getJournal_idsDirtyFlag())
            domain.setJournalIds(model.getJournal_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



