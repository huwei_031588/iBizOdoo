package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_tracking_value] 对象
 */
public interface Imail_tracking_value {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [更改的字段]
     */
    public void setField(String field);
    
    /**
     * 设置 [更改的字段]
     */
    public String getField();

    /**
     * 获取 [更改的字段]脏标记
     */
    public boolean getFieldDirtyFlag();
    /**
     * 获取 [字段说明]
     */
    public void setField_desc(String field_desc);
    
    /**
     * 设置 [字段说明]
     */
    public String getField_desc();

    /**
     * 获取 [字段说明]脏标记
     */
    public boolean getField_descDirtyFlag();
    /**
     * 获取 [字段类型]
     */
    public void setField_type(String field_type);
    
    /**
     * 设置 [字段类型]
     */
    public String getField_type();

    /**
     * 获取 [字段类型]脏标记
     */
    public boolean getField_typeDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [邮件消息ID]
     */
    public void setMail_message_id(Integer mail_message_id);
    
    /**
     * 设置 [邮件消息ID]
     */
    public Integer getMail_message_id();

    /**
     * 获取 [邮件消息ID]脏标记
     */
    public boolean getMail_message_idDirtyFlag();
    /**
     * 获取 [新字符值]
     */
    public void setNew_value_char(String new_value_char);
    
    /**
     * 设置 [新字符值]
     */
    public String getNew_value_char();

    /**
     * 获取 [新字符值]脏标记
     */
    public boolean getNew_value_charDirtyFlag();
    /**
     * 获取 [新日期时间值]
     */
    public void setNew_value_datetime(Timestamp new_value_datetime);
    
    /**
     * 设置 [新日期时间值]
     */
    public Timestamp getNew_value_datetime();

    /**
     * 获取 [新日期时间值]脏标记
     */
    public boolean getNew_value_datetimeDirtyFlag();
    /**
     * 获取 [新浮点值]
     */
    public void setNew_value_float(Double new_value_float);
    
    /**
     * 设置 [新浮点值]
     */
    public Double getNew_value_float();

    /**
     * 获取 [新浮点值]脏标记
     */
    public boolean getNew_value_floatDirtyFlag();
    /**
     * 获取 [新整数值]
     */
    public void setNew_value_integer(Integer new_value_integer);
    
    /**
     * 设置 [新整数值]
     */
    public Integer getNew_value_integer();

    /**
     * 获取 [新整数值]脏标记
     */
    public boolean getNew_value_integerDirtyFlag();
    /**
     * 获取 [新货币值]
     */
    public void setNew_value_monetary(Double new_value_monetary);
    
    /**
     * 设置 [新货币值]
     */
    public Double getNew_value_monetary();

    /**
     * 获取 [新货币值]脏标记
     */
    public boolean getNew_value_monetaryDirtyFlag();
    /**
     * 获取 [新文本值]
     */
    public void setNew_value_text(String new_value_text);
    
    /**
     * 设置 [新文本值]
     */
    public String getNew_value_text();

    /**
     * 获取 [新文本值]脏标记
     */
    public boolean getNew_value_textDirtyFlag();
    /**
     * 获取 [旧字符值]
     */
    public void setOld_value_char(String old_value_char);
    
    /**
     * 设置 [旧字符值]
     */
    public String getOld_value_char();

    /**
     * 获取 [旧字符值]脏标记
     */
    public boolean getOld_value_charDirtyFlag();
    /**
     * 获取 [旧日期时间值]
     */
    public void setOld_value_datetime(Timestamp old_value_datetime);
    
    /**
     * 设置 [旧日期时间值]
     */
    public Timestamp getOld_value_datetime();

    /**
     * 获取 [旧日期时间值]脏标记
     */
    public boolean getOld_value_datetimeDirtyFlag();
    /**
     * 获取 [旧浮点值]
     */
    public void setOld_value_float(Double old_value_float);
    
    /**
     * 设置 [旧浮点值]
     */
    public Double getOld_value_float();

    /**
     * 获取 [旧浮点值]脏标记
     */
    public boolean getOld_value_floatDirtyFlag();
    /**
     * 获取 [旧整数值]
     */
    public void setOld_value_integer(Integer old_value_integer);
    
    /**
     * 设置 [旧整数值]
     */
    public Integer getOld_value_integer();

    /**
     * 获取 [旧整数值]脏标记
     */
    public boolean getOld_value_integerDirtyFlag();
    /**
     * 获取 [旧货币值]
     */
    public void setOld_value_monetary(Double old_value_monetary);
    
    /**
     * 设置 [旧货币值]
     */
    public Double getOld_value_monetary();

    /**
     * 获取 [旧货币值]脏标记
     */
    public boolean getOld_value_monetaryDirtyFlag();
    /**
     * 获取 [旧文本值]
     */
    public void setOld_value_text(String old_value_text);
    
    /**
     * 设置 [旧文本值]
     */
    public String getOld_value_text();

    /**
     * 获取 [旧文本值]脏标记
     */
    public boolean getOld_value_textDirtyFlag();
    /**
     * 获取 [跟踪字段次序]
     */
    public void setTrack_sequence(Integer track_sequence);
    
    /**
     * 设置 [跟踪字段次序]
     */
    public Integer getTrack_sequence();

    /**
     * 获取 [跟踪字段次序]脏标记
     */
    public boolean getTrack_sequenceDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
