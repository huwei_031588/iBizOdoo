package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.lunch_cashmove;

/**
 * 实体[lunch_cashmove] 服务对象接口
 */
public interface lunch_cashmoveRepository{


    public lunch_cashmove createPO() ;
        public void createBatch(lunch_cashmove lunch_cashmove);

        public void updateBatch(lunch_cashmove lunch_cashmove);

        public void removeBatch(String id);

        public void remove(String id);

        public void create(lunch_cashmove lunch_cashmove);

        public void update(lunch_cashmove lunch_cashmove);

        public void get(String id);

        public List<lunch_cashmove> search();


}
