package cn.ibizlab.odoo.core.odoo_digest.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_tipSearchContext;
import cn.ibizlab.odoo.core.odoo_digest.service.IDigest_tipService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_digest.client.digest_tipOdooClient;
import cn.ibizlab.odoo.core.odoo_digest.clientmodel.digest_tipClientModel;

/**
 * 实体[摘要提示] 服务对象接口实现
 */
@Slf4j
@Service
public class Digest_tipServiceImpl implements IDigest_tipService {

    @Autowired
    digest_tipOdooClient digest_tipOdooClient;


    @Override
    public boolean create(Digest_tip et) {
        digest_tipClientModel clientModel = convert2Model(et,null);
		digest_tipOdooClient.create(clientModel);
        Digest_tip rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Digest_tip> list){
    }

    @Override
    public boolean remove(Integer id) {
        digest_tipClientModel clientModel = new digest_tipClientModel();
        clientModel.setId(id);
		digest_tipOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Digest_tip et) {
        digest_tipClientModel clientModel = convert2Model(et,null);
		digest_tipOdooClient.update(clientModel);
        Digest_tip rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Digest_tip> list){
    }

    @Override
    public Digest_tip get(Integer id) {
        digest_tipClientModel clientModel = new digest_tipClientModel();
        clientModel.setId(id);
		digest_tipOdooClient.get(clientModel);
        Digest_tip et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Digest_tip();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Digest_tip> searchDefault(Digest_tipSearchContext context) {
        List<Digest_tip> list = new ArrayList<Digest_tip>();
        Page<digest_tipClientModel> clientModelList = digest_tipOdooClient.search(context);
        for(digest_tipClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Digest_tip>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public digest_tipClientModel convert2Model(Digest_tip domain , digest_tipClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new digest_tipClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("tip_descriptiondirtyflag"))
                model.setTip_description(domain.getTipDescription());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("user_idsdirtyflag"))
                model.setUser_ids(domain.getUserIds());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("group_id_textdirtyflag"))
                model.setGroup_id_text(domain.getGroupIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("group_iddirtyflag"))
                model.setGroup_id(domain.getGroupId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Digest_tip convert2Domain( digest_tipClientModel model ,Digest_tip domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Digest_tip();
        }

        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getTip_descriptionDirtyFlag())
            domain.setTipDescription(model.getTip_description());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getUser_idsDirtyFlag())
            domain.setUserIds(model.getUser_ids());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getGroup_id_textDirtyFlag())
            domain.setGroupIdText(model.getGroup_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getGroup_idDirtyFlag())
            domain.setGroupId(model.getGroup_id());
        return domain ;
    }

}

    



