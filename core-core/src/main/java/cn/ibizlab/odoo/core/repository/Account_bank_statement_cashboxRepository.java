package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_bank_statement_cashbox;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_cashboxSearchContext;

/**
 * 实体 [银行资金调节表] 存储对象
 */
public interface Account_bank_statement_cashboxRepository extends Repository<Account_bank_statement_cashbox> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_bank_statement_cashbox> searchDefault(Account_bank_statement_cashboxSearchContext context);

    Account_bank_statement_cashbox convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox domain , Account_bank_statement_cashbox po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox convert2Domain( Account_bank_statement_cashbox po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox domain) ;

}
