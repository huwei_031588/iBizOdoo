package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [lunch_order] 对象
 */
public interface lunch_order {

    public String getAlerts();

    public void setAlerts(String alerts);

    public String getBalance_visible();

    public void setBalance_visible(String balance_visible);

    public Double getCash_move_balance();

    public void setCash_move_balance(Double cash_move_balance);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getCurrency_id();

    public void setCurrency_id(Integer currency_id);

    public String getCurrency_id_text();

    public void setCurrency_id_text(String currency_id_text);

    public Timestamp getDate();

    public void setDate(Timestamp date);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getOrder_line_ids();

    public void setOrder_line_ids(String order_line_ids);

    public String getPrevious_order_ids();

    public void setPrevious_order_ids(String previous_order_ids);

    public String getPrevious_order_widget();

    public void setPrevious_order_widget(String previous_order_widget);

    public String getState();

    public void setState(String state);

    public Double getTotal();

    public void setTotal(Double total);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
