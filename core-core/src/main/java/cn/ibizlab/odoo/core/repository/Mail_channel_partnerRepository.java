package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_channel_partner;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channel_partnerSearchContext;

/**
 * 实体 [渠道指南] 存储对象
 */
public interface Mail_channel_partnerRepository extends Repository<Mail_channel_partner> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_channel_partner> searchDefault(Mail_channel_partnerSearchContext context);

    Mail_channel_partner convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner domain , Mail_channel_partner po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner convert2Domain( Mail_channel_partner po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner domain) ;

}
