package cn.ibizlab.odoo.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_workorder;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_workorderSearchContext;


/**
 * 实体[Mro_workorder] 服务对象接口
 */
public interface IMro_workorderService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Mro_workorder et) ;
    void updateBatch(List<Mro_workorder> list) ;
    boolean create(Mro_workorder et) ;
    void createBatch(List<Mro_workorder> list) ;
    Mro_workorder get(Integer key) ;
    Page<Mro_workorder> searchDefault(Mro_workorderSearchContext context) ;

}



