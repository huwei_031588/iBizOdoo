package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goalSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_goalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_goalOdooClient;
import cn.ibizlab.odoo.core.odoo_gamification.clientmodel.gamification_goalClientModel;

/**
 * 实体[游戏化目标] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_goalServiceImpl implements IGamification_goalService {

    @Autowired
    gamification_goalOdooClient gamification_goalOdooClient;


    @Override
    public boolean create(Gamification_goal et) {
        gamification_goalClientModel clientModel = convert2Model(et,null);
		gamification_goalOdooClient.create(clientModel);
        Gamification_goal rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_goal> list){
    }

    @Override
    public boolean remove(Integer id) {
        gamification_goalClientModel clientModel = new gamification_goalClientModel();
        clientModel.setId(id);
		gamification_goalOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Gamification_goal et) {
        gamification_goalClientModel clientModel = convert2Model(et,null);
		gamification_goalOdooClient.update(clientModel);
        Gamification_goal rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Gamification_goal> list){
    }

    @Override
    public Gamification_goal get(Integer id) {
        gamification_goalClientModel clientModel = new gamification_goalClientModel();
        clientModel.setId(id);
		gamification_goalOdooClient.get(clientModel);
        Gamification_goal et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Gamification_goal();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_goal> searchDefault(Gamification_goalSearchContext context) {
        List<Gamification_goal> list = new ArrayList<Gamification_goal>();
        Page<gamification_goalClientModel> clientModelList = gamification_goalOdooClient.search(context);
        for(gamification_goalClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Gamification_goal>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public gamification_goalClientModel convert2Model(Gamification_goal domain , gamification_goalClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new gamification_goalClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("start_datedirtyflag"))
                model.setStart_date(domain.getStartDate());
            if((Boolean) domain.getExtensionparams().get("to_updatedirtyflag"))
                model.setTo_update(domain.getToUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("remind_update_delaydirtyflag"))
                model.setRemind_update_delay(domain.getRemindUpdateDelay());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("completenessdirtyflag"))
                model.setCompleteness(domain.getCompleteness());
            if((Boolean) domain.getExtensionparams().get("target_goaldirtyflag"))
                model.setTarget_goal(domain.getTargetGoal());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("closeddirtyflag"))
                model.setClosed(domain.getClosed());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("last_updatedirtyflag"))
                model.setLast_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("end_datedirtyflag"))
                model.setEnd_date(domain.getEndDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("currentdirtyflag"))
                model.setCurrent(domain.getCurrent());
            if((Boolean) domain.getExtensionparams().get("definition_descriptiondirtyflag"))
                model.setDefinition_description(domain.getDefinitionDescription());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("definition_suffixdirtyflag"))
                model.setDefinition_suffix(domain.getDefinitionSuffix());
            if((Boolean) domain.getExtensionparams().get("challenge_id_textdirtyflag"))
                model.setChallenge_id_text(domain.getChallengeIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("definition_id_textdirtyflag"))
                model.setDefinition_id_text(domain.getDefinitionIdText());
            if((Boolean) domain.getExtensionparams().get("definition_displaydirtyflag"))
                model.setDefinition_display(domain.getDefinitionDisplay());
            if((Boolean) domain.getExtensionparams().get("line_id_textdirtyflag"))
                model.setLine_id_text(domain.getLineIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("definition_conditiondirtyflag"))
                model.setDefinition_condition(domain.getDefinitionCondition());
            if((Boolean) domain.getExtensionparams().get("computation_modedirtyflag"))
                model.setComputation_mode(domain.getComputationMode());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("line_iddirtyflag"))
                model.setLine_id(domain.getLineId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("definition_iddirtyflag"))
                model.setDefinition_id(domain.getDefinitionId());
            if((Boolean) domain.getExtensionparams().get("challenge_iddirtyflag"))
                model.setChallenge_id(domain.getChallengeId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Gamification_goal convert2Domain( gamification_goalClientModel model ,Gamification_goal domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Gamification_goal();
        }

        if(model.getStart_dateDirtyFlag())
            domain.setStartDate(model.getStart_date());
        if(model.getTo_updateDirtyFlag())
            domain.setToUpdate(model.getTo_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getRemind_update_delayDirtyFlag())
            domain.setRemindUpdateDelay(model.getRemind_update_delay());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getCompletenessDirtyFlag())
            domain.setCompleteness(model.getCompleteness());
        if(model.getTarget_goalDirtyFlag())
            domain.setTargetGoal(model.getTarget_goal());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getClosedDirtyFlag())
            domain.setClosed(model.getClosed());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getLast_updateDirtyFlag())
            domain.setLastUpdate(model.getLast_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getEnd_dateDirtyFlag())
            domain.setEndDate(model.getEnd_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCurrentDirtyFlag())
            domain.setCurrent(model.getCurrent());
        if(model.getDefinition_descriptionDirtyFlag())
            domain.setDefinitionDescription(model.getDefinition_description());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getDefinition_suffixDirtyFlag())
            domain.setDefinitionSuffix(model.getDefinition_suffix());
        if(model.getChallenge_id_textDirtyFlag())
            domain.setChallengeIdText(model.getChallenge_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getDefinition_id_textDirtyFlag())
            domain.setDefinitionIdText(model.getDefinition_id_text());
        if(model.getDefinition_displayDirtyFlag())
            domain.setDefinitionDisplay(model.getDefinition_display());
        if(model.getLine_id_textDirtyFlag())
            domain.setLineIdText(model.getLine_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getDefinition_conditionDirtyFlag())
            domain.setDefinitionCondition(model.getDefinition_condition());
        if(model.getComputation_modeDirtyFlag())
            domain.setComputationMode(model.getComputation_mode());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getLine_idDirtyFlag())
            domain.setLineId(model.getLine_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getDefinition_idDirtyFlag())
            domain.setDefinitionId(model.getDefinition_id());
        if(model.getChallenge_idDirtyFlag())
            domain.setChallengeId(model.getChallenge_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        return domain ;
    }

}

    



