package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_sale_payment_acquirer_onboarding_wizardSearchContext;

/**
 * 实体 [website.sale.payment.acquirer.onboarding.wizard] 存储对象
 */
public interface Website_sale_payment_acquirer_onboarding_wizardRepository extends Repository<Website_sale_payment_acquirer_onboarding_wizard> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Website_sale_payment_acquirer_onboarding_wizard> searchDefault(Website_sale_payment_acquirer_onboarding_wizardSearchContext context);

    Website_sale_payment_acquirer_onboarding_wizard convert2PO(cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard domain , Website_sale_payment_acquirer_onboarding_wizard po) ;

    cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard convert2Domain( Website_sale_payment_acquirer_onboarding_wizard po ,cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard domain) ;

}
