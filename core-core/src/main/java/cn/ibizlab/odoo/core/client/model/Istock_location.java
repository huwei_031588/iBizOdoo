package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [stock_location] 对象
 */
public interface Istock_location {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [条码]
     */
    public void setBarcode(String barcode);
    
    /**
     * 设置 [条码]
     */
    public String getBarcode();

    /**
     * 获取 [条码]脏标记
     */
    public boolean getBarcodeDirtyFlag();
    /**
     * 获取 [包含]
     */
    public void setChild_ids(String child_ids);
    
    /**
     * 设置 [包含]
     */
    public String getChild_ids();

    /**
     * 获取 [包含]脏标记
     */
    public boolean getChild_idsDirtyFlag();
    /**
     * 获取 [额外的信息]
     */
    public void setComment(String comment);
    
    /**
     * 设置 [额外的信息]
     */
    public String getComment();

    /**
     * 获取 [额外的信息]脏标记
     */
    public boolean getCommentDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [完整的位置名称]
     */
    public void setComplete_name(String complete_name);
    
    /**
     * 设置 [完整的位置名称]
     */
    public String getComplete_name();

    /**
     * 获取 [完整的位置名称]脏标记
     */
    public boolean getComplete_nameDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [上级位置]
     */
    public void setLocation_id(Integer location_id);
    
    /**
     * 设置 [上级位置]
     */
    public Integer getLocation_id();

    /**
     * 获取 [上级位置]脏标记
     */
    public boolean getLocation_idDirtyFlag();
    /**
     * 获取 [上级位置]
     */
    public void setLocation_id_text(String location_id_text);
    
    /**
     * 设置 [上级位置]
     */
    public String getLocation_id_text();

    /**
     * 获取 [上级位置]脏标记
     */
    public boolean getLocation_id_textDirtyFlag();
    /**
     * 获取 [位置名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [位置名称]
     */
    public String getName();

    /**
     * 获取 [位置名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [父级路径]
     */
    public void setParent_path(String parent_path);
    
    /**
     * 设置 [父级路径]
     */
    public String getParent_path();

    /**
     * 获取 [父级路径]脏标记
     */
    public boolean getParent_pathDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setPartner_id(Integer partner_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getPartner_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getPartner_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setPartner_id_text(String partner_id_text);
    
    /**
     * 设置 [所有者]
     */
    public String getPartner_id_text();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getPartner_id_textDirtyFlag();
    /**
     * 获取 [通道(X)]
     */
    public void setPosx(Integer posx);
    
    /**
     * 设置 [通道(X)]
     */
    public Integer getPosx();

    /**
     * 获取 [通道(X)]脏标记
     */
    public boolean getPosxDirtyFlag();
    /**
     * 获取 [货架(Y)]
     */
    public void setPosy(Integer posy);
    
    /**
     * 设置 [货架(Y)]
     */
    public Integer getPosy();

    /**
     * 获取 [货架(Y)]脏标记
     */
    public boolean getPosyDirtyFlag();
    /**
     * 获取 [高度(Z)]
     */
    public void setPosz(Integer posz);
    
    /**
     * 设置 [高度(Z)]
     */
    public Integer getPosz();

    /**
     * 获取 [高度(Z)]脏标记
     */
    public boolean getPoszDirtyFlag();
    /**
     * 获取 [上架策略]
     */
    public void setPutaway_strategy_id(Integer putaway_strategy_id);
    
    /**
     * 设置 [上架策略]
     */
    public Integer getPutaway_strategy_id();

    /**
     * 获取 [上架策略]脏标记
     */
    public boolean getPutaway_strategy_idDirtyFlag();
    /**
     * 获取 [上架策略]
     */
    public void setPutaway_strategy_id_text(String putaway_strategy_id_text);
    
    /**
     * 设置 [上架策略]
     */
    public String getPutaway_strategy_id_text();

    /**
     * 获取 [上架策略]脏标记
     */
    public boolean getPutaway_strategy_id_textDirtyFlag();
    /**
     * 获取 [即时库存]
     */
    public void setQuant_ids(String quant_ids);
    
    /**
     * 设置 [即时库存]
     */
    public String getQuant_ids();

    /**
     * 获取 [即时库存]脏标记
     */
    public boolean getQuant_idsDirtyFlag();
    /**
     * 获取 [下架策略]
     */
    public void setRemoval_strategy_id(Integer removal_strategy_id);
    
    /**
     * 设置 [下架策略]
     */
    public Integer getRemoval_strategy_id();

    /**
     * 获取 [下架策略]脏标记
     */
    public boolean getRemoval_strategy_idDirtyFlag();
    /**
     * 获取 [下架策略]
     */
    public void setRemoval_strategy_id_text(String removal_strategy_id_text);
    
    /**
     * 设置 [下架策略]
     */
    public String getRemoval_strategy_id_text();

    /**
     * 获取 [下架策略]脏标记
     */
    public boolean getRemoval_strategy_id_textDirtyFlag();
    /**
     * 获取 [是一个退回位置？]
     */
    public void setReturn_location(String return_location);
    
    /**
     * 设置 [是一个退回位置？]
     */
    public String getReturn_location();

    /**
     * 获取 [是一个退回位置？]脏标记
     */
    public boolean getReturn_locationDirtyFlag();
    /**
     * 获取 [是一个报废位置？]
     */
    public void setScrap_location(String scrap_location);
    
    /**
     * 设置 [是一个报废位置？]
     */
    public String getScrap_location();

    /**
     * 获取 [是一个报废位置？]脏标记
     */
    public boolean getScrap_locationDirtyFlag();
    /**
     * 获取 [位置类型]
     */
    public void setUsage(String usage);
    
    /**
     * 设置 [位置类型]
     */
    public String getUsage();

    /**
     * 获取 [位置类型]脏标记
     */
    public boolean getUsageDirtyFlag();
    /**
     * 获取 [库存计价科目（入向）]
     */
    public void setValuation_in_account_id(Integer valuation_in_account_id);
    
    /**
     * 设置 [库存计价科目（入向）]
     */
    public Integer getValuation_in_account_id();

    /**
     * 获取 [库存计价科目（入向）]脏标记
     */
    public boolean getValuation_in_account_idDirtyFlag();
    /**
     * 获取 [库存计价科目（入向）]
     */
    public void setValuation_in_account_id_text(String valuation_in_account_id_text);
    
    /**
     * 设置 [库存计价科目（入向）]
     */
    public String getValuation_in_account_id_text();

    /**
     * 获取 [库存计价科目（入向）]脏标记
     */
    public boolean getValuation_in_account_id_textDirtyFlag();
    /**
     * 获取 [库存计价科目（出向）]
     */
    public void setValuation_out_account_id(Integer valuation_out_account_id);
    
    /**
     * 设置 [库存计价科目（出向）]
     */
    public Integer getValuation_out_account_id();

    /**
     * 获取 [库存计价科目（出向）]脏标记
     */
    public boolean getValuation_out_account_idDirtyFlag();
    /**
     * 获取 [库存计价科目（出向）]
     */
    public void setValuation_out_account_id_text(String valuation_out_account_id_text);
    
    /**
     * 设置 [库存计价科目（出向）]
     */
    public String getValuation_out_account_id_text();

    /**
     * 获取 [库存计价科目（出向）]脏标记
     */
    public boolean getValuation_out_account_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
