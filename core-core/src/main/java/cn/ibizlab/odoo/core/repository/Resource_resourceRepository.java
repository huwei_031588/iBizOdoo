package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Resource_resource;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_resourceSearchContext;

/**
 * 实体 [资源] 存储对象
 */
public interface Resource_resourceRepository extends Repository<Resource_resource> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Resource_resource> searchDefault(Resource_resourceSearchContext context);

    Resource_resource convert2PO(cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource domain , Resource_resource po) ;

    cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource convert2Domain( Resource_resource po ,cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource domain) ;

}
