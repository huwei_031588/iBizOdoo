package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Lunch_cashmove;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;

/**
 * 实体 [午餐费的现金划转] 存储对象
 */
public interface Lunch_cashmoveRepository extends Repository<Lunch_cashmove> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Lunch_cashmove> searchDefault(Lunch_cashmoveSearchContext context);

    Lunch_cashmove convert2PO(cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_cashmove domain , Lunch_cashmove po) ;

    cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_cashmove convert2Domain( Lunch_cashmove po ,cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_cashmove domain) ;

}
