package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mail_mass_mailing_contact;

/**
 * 实体[mail_mass_mailing_contact] 服务对象接口
 */
public interface mail_mass_mailing_contactRepository{


    public mail_mass_mailing_contact createPO() ;
        public void removeBatch(String id);

        public void updateBatch(mail_mass_mailing_contact mail_mass_mailing_contact);

        public List<mail_mass_mailing_contact> search();

        public void create(mail_mass_mailing_contact mail_mass_mailing_contact);

        public void createBatch(mail_mass_mailing_contact mail_mass_mailing_contact);

        public void get(String id);

        public void update(mail_mass_mailing_contact mail_mass_mailing_contact);

        public void remove(String id);


}
