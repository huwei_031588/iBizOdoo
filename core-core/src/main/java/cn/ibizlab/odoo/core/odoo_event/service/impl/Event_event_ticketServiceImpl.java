package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_event_ticketSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_event_ticketService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_event.client.event_event_ticketOdooClient;
import cn.ibizlab.odoo.core.odoo_event.clientmodel.event_event_ticketClientModel;

/**
 * 实体[活动入场券] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_event_ticketServiceImpl implements IEvent_event_ticketService {

    @Autowired
    event_event_ticketOdooClient event_event_ticketOdooClient;


    @Override
    public boolean update(Event_event_ticket et) {
        event_event_ticketClientModel clientModel = convert2Model(et,null);
		event_event_ticketOdooClient.update(clientModel);
        Event_event_ticket rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Event_event_ticket> list){
    }

    @Override
    public Event_event_ticket get(Integer id) {
        event_event_ticketClientModel clientModel = new event_event_ticketClientModel();
        clientModel.setId(id);
		event_event_ticketOdooClient.get(clientModel);
        Event_event_ticket et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Event_event_ticket();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        event_event_ticketClientModel clientModel = new event_event_ticketClientModel();
        clientModel.setId(id);
		event_event_ticketOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Event_event_ticket et) {
        event_event_ticketClientModel clientModel = convert2Model(et,null);
		event_event_ticketOdooClient.create(clientModel);
        Event_event_ticket rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_event_ticket> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_event_ticket> searchDefault(Event_event_ticketSearchContext context) {
        List<Event_event_ticket> list = new ArrayList<Event_event_ticket>();
        Page<event_event_ticketClientModel> clientModelList = event_event_ticketOdooClient.search(context);
        for(event_event_ticketClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Event_event_ticket>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public event_event_ticketClientModel convert2Model(Event_event_ticket domain , event_event_ticketClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new event_event_ticketClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("seats_availabledirtyflag"))
                model.setSeats_available(domain.getSeatsAvailable());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("price_reduce_taxincdirtyflag"))
                model.setPrice_reduce_taxinc(domain.getPriceReduceTaxinc());
            if((Boolean) domain.getExtensionparams().get("price_reducedirtyflag"))
                model.setPrice_reduce(domain.getPriceReduce());
            if((Boolean) domain.getExtensionparams().get("registration_idsdirtyflag"))
                model.setRegistration_ids(domain.getRegistrationIds());
            if((Boolean) domain.getExtensionparams().get("seats_unconfirmeddirtyflag"))
                model.setSeats_unconfirmed(domain.getSeatsUnconfirmed());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("seats_reserveddirtyflag"))
                model.setSeats_reserved(domain.getSeatsReserved());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("pricedirtyflag"))
                model.setPrice(domain.getPrice());
            if((Boolean) domain.getExtensionparams().get("seats_useddirtyflag"))
                model.setSeats_used(domain.getSeatsUsed());
            if((Boolean) domain.getExtensionparams().get("deadlinedirtyflag"))
                model.setDeadline(domain.getDeadline());
            if((Boolean) domain.getExtensionparams().get("seats_availabilitydirtyflag"))
                model.setSeats_availability(domain.getSeatsAvailability());
            if((Boolean) domain.getExtensionparams().get("is_expireddirtyflag"))
                model.setIs_expired(domain.getIsExpired());
            if((Boolean) domain.getExtensionparams().get("seats_maxdirtyflag"))
                model.setSeats_max(domain.getSeatsMax());
            if((Boolean) domain.getExtensionparams().get("event_type_id_textdirtyflag"))
                model.setEvent_type_id_text(domain.getEventTypeIdText());
            if((Boolean) domain.getExtensionparams().get("event_id_textdirtyflag"))
                model.setEvent_id_text(domain.getEventIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("event_iddirtyflag"))
                model.setEvent_id(domain.getEventId());
            if((Boolean) domain.getExtensionparams().get("event_type_iddirtyflag"))
                model.setEvent_type_id(domain.getEventTypeId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Event_event_ticket convert2Domain( event_event_ticketClientModel model ,Event_event_ticket domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Event_event_ticket();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getSeats_availableDirtyFlag())
            domain.setSeatsAvailable(model.getSeats_available());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getPrice_reduce_taxincDirtyFlag())
            domain.setPriceReduceTaxinc(model.getPrice_reduce_taxinc());
        if(model.getPrice_reduceDirtyFlag())
            domain.setPriceReduce(model.getPrice_reduce());
        if(model.getRegistration_idsDirtyFlag())
            domain.setRegistrationIds(model.getRegistration_ids());
        if(model.getSeats_unconfirmedDirtyFlag())
            domain.setSeatsUnconfirmed(model.getSeats_unconfirmed());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getSeats_reservedDirtyFlag())
            domain.setSeatsReserved(model.getSeats_reserved());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getPriceDirtyFlag())
            domain.setPrice(model.getPrice());
        if(model.getSeats_usedDirtyFlag())
            domain.setSeatsUsed(model.getSeats_used());
        if(model.getDeadlineDirtyFlag())
            domain.setDeadline(model.getDeadline());
        if(model.getSeats_availabilityDirtyFlag())
            domain.setSeatsAvailability(model.getSeats_availability());
        if(model.getIs_expiredDirtyFlag())
            domain.setIsExpired(model.getIs_expired());
        if(model.getSeats_maxDirtyFlag())
            domain.setSeatsMax(model.getSeats_max());
        if(model.getEvent_type_id_textDirtyFlag())
            domain.setEventTypeIdText(model.getEvent_type_id_text());
        if(model.getEvent_id_textDirtyFlag())
            domain.setEventIdText(model.getEvent_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getEvent_idDirtyFlag())
            domain.setEventId(model.getEvent_id());
        if(model.getEvent_type_idDirtyFlag())
            domain.setEventTypeId(model.getEvent_type_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



