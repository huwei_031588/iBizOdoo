package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task_parts_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_task_parts_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_task_parts_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mro.client.mro_task_parts_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_mro.clientmodel.mro_task_parts_lineClientModel;

/**
 * 实体[Maintenance Planned Parts] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_task_parts_lineServiceImpl implements IMro_task_parts_lineService {

    @Autowired
    mro_task_parts_lineOdooClient mro_task_parts_lineOdooClient;


    @Override
    public boolean update(Mro_task_parts_line et) {
        mro_task_parts_lineClientModel clientModel = convert2Model(et,null);
		mro_task_parts_lineOdooClient.update(clientModel);
        Mro_task_parts_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mro_task_parts_line> list){
    }

    @Override
    public boolean remove(Integer id) {
        mro_task_parts_lineClientModel clientModel = new mro_task_parts_lineClientModel();
        clientModel.setId(id);
		mro_task_parts_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mro_task_parts_line et) {
        mro_task_parts_lineClientModel clientModel = convert2Model(et,null);
		mro_task_parts_lineOdooClient.create(clientModel);
        Mro_task_parts_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_task_parts_line> list){
    }

    @Override
    public Mro_task_parts_line get(Integer id) {
        mro_task_parts_lineClientModel clientModel = new mro_task_parts_lineClientModel();
        clientModel.setId(id);
		mro_task_parts_lineOdooClient.get(clientModel);
        Mro_task_parts_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mro_task_parts_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_task_parts_line> searchDefault(Mro_task_parts_lineSearchContext context) {
        List<Mro_task_parts_line> list = new ArrayList<Mro_task_parts_line>();
        Page<mro_task_parts_lineClientModel> clientModelList = mro_task_parts_lineOdooClient.search(context);
        for(mro_task_parts_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mro_task_parts_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mro_task_parts_lineClientModel convert2Model(Mro_task_parts_line domain , mro_task_parts_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mro_task_parts_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("parts_qtydirtyflag"))
                model.setParts_qty(domain.getPartsQty());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("task_id_textdirtyflag"))
                model.setTask_id_text(domain.getTaskIdText());
            if((Boolean) domain.getExtensionparams().get("parts_id_textdirtyflag"))
                model.setParts_id_text(domain.getPartsIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("parts_uom_textdirtyflag"))
                model.setParts_uom_text(domain.getPartsUomText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("parts_uomdirtyflag"))
                model.setParts_uom(domain.getPartsUom());
            if((Boolean) domain.getExtensionparams().get("parts_iddirtyflag"))
                model.setParts_id(domain.getPartsId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("task_iddirtyflag"))
                model.setTask_id(domain.getTaskId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mro_task_parts_line convert2Domain( mro_task_parts_lineClientModel model ,Mro_task_parts_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mro_task_parts_line();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getParts_qtyDirtyFlag())
            domain.setPartsQty(model.getParts_qty());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getTask_id_textDirtyFlag())
            domain.setTaskIdText(model.getTask_id_text());
        if(model.getParts_id_textDirtyFlag())
            domain.setPartsIdText(model.getParts_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getParts_uom_textDirtyFlag())
            domain.setPartsUomText(model.getParts_uom_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getParts_uomDirtyFlag())
            domain.setPartsUom(model.getParts_uom());
        if(model.getParts_idDirtyFlag())
            domain.setPartsId(model.getParts_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getTask_idDirtyFlag())
            domain.setTaskId(model.getTask_id());
        return domain ;
    }

}

    



