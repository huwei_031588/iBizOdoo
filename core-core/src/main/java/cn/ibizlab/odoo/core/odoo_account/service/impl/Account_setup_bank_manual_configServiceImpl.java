package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_setup_bank_manual_configSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_setup_bank_manual_configService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_setup_bank_manual_configOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_setup_bank_manual_configClientModel;

/**
 * 实体[银行设置通用配置] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_setup_bank_manual_configServiceImpl implements IAccount_setup_bank_manual_configService {

    @Autowired
    account_setup_bank_manual_configOdooClient account_setup_bank_manual_configOdooClient;


    @Override
    public boolean create(Account_setup_bank_manual_config et) {
        account_setup_bank_manual_configClientModel clientModel = convert2Model(et,null);
		account_setup_bank_manual_configOdooClient.create(clientModel);
        Account_setup_bank_manual_config rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_setup_bank_manual_config> list){
    }

    @Override
    public boolean update(Account_setup_bank_manual_config et) {
        account_setup_bank_manual_configClientModel clientModel = convert2Model(et,null);
		account_setup_bank_manual_configOdooClient.update(clientModel);
        Account_setup_bank_manual_config rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_setup_bank_manual_config> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_setup_bank_manual_configClientModel clientModel = new account_setup_bank_manual_configClientModel();
        clientModel.setId(id);
		account_setup_bank_manual_configOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Account_setup_bank_manual_config get(Integer id) {
        account_setup_bank_manual_configClientModel clientModel = new account_setup_bank_manual_configClientModel();
        clientModel.setId(id);
		account_setup_bank_manual_configOdooClient.get(clientModel);
        Account_setup_bank_manual_config et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_setup_bank_manual_config();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_setup_bank_manual_config> searchDefault(Account_setup_bank_manual_configSearchContext context) {
        List<Account_setup_bank_manual_config> list = new ArrayList<Account_setup_bank_manual_config>();
        Page<account_setup_bank_manual_configClientModel> clientModelList = account_setup_bank_manual_configOdooClient.search(context);
        for(account_setup_bank_manual_configClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_setup_bank_manual_config>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_setup_bank_manual_configClientModel convert2Model(Account_setup_bank_manual_config domain , account_setup_bank_manual_configClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_setup_bank_manual_configClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("linked_journal_iddirtyflag"))
                model.setLinked_journal_id(domain.getLinkedJournalId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("new_journal_namedirtyflag"))
                model.setNew_journal_name(domain.getNewJournalName());
            if((Boolean) domain.getExtensionparams().get("new_journal_codedirtyflag"))
                model.setNew_journal_code(domain.getNewJournalCode());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_or_link_optiondirtyflag"))
                model.setCreate_or_link_option(domain.getCreateOrLinkOption());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("related_acc_typedirtyflag"))
                model.setRelated_acc_type(domain.getRelatedAccType());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("bank_namedirtyflag"))
                model.setBank_name(domain.getBankName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("bank_iddirtyflag"))
                model.setBank_id(domain.getBankId());
            if((Boolean) domain.getExtensionparams().get("qr_code_validdirtyflag"))
                model.setQr_code_valid(domain.getQrCodeValid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("sanitized_acc_numberdirtyflag"))
                model.setSanitized_acc_number(domain.getSanitizedAccNumber());
            if((Boolean) domain.getExtensionparams().get("acc_typedirtyflag"))
                model.setAcc_type(domain.getAccType());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("acc_numberdirtyflag"))
                model.setAcc_number(domain.getAccNumber());
            if((Boolean) domain.getExtensionparams().get("bank_bicdirtyflag"))
                model.setBank_bic(domain.getBankBic());
            if((Boolean) domain.getExtensionparams().get("acc_holder_namedirtyflag"))
                model.setAcc_holder_name(domain.getAccHolderName());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("res_partner_bank_iddirtyflag"))
                model.setRes_partner_bank_id(domain.getResPartnerBankId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_setup_bank_manual_config convert2Domain( account_setup_bank_manual_configClientModel model ,Account_setup_bank_manual_config domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_setup_bank_manual_config();
        }

        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getLinked_journal_idDirtyFlag())
            domain.setLinkedJournalId(model.getLinked_journal_id());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNew_journal_nameDirtyFlag())
            domain.setNewJournalName(model.getNew_journal_name());
        if(model.getNew_journal_codeDirtyFlag())
            domain.setNewJournalCode(model.getNew_journal_code());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_or_link_optionDirtyFlag())
            domain.setCreateOrLinkOption(model.getCreate_or_link_option());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getRelated_acc_typeDirtyFlag())
            domain.setRelatedAccType(model.getRelated_acc_type());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getBank_nameDirtyFlag())
            domain.setBankName(model.getBank_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getBank_idDirtyFlag())
            domain.setBankId(model.getBank_id());
        if(model.getQr_code_validDirtyFlag())
            domain.setQrCodeValid(model.getQr_code_valid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getSanitized_acc_numberDirtyFlag())
            domain.setSanitizedAccNumber(model.getSanitized_acc_number());
        if(model.getAcc_typeDirtyFlag())
            domain.setAccType(model.getAcc_type());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getAcc_numberDirtyFlag())
            domain.setAccNumber(model.getAcc_number());
        if(model.getBank_bicDirtyFlag())
            domain.setBankBic(model.getBank_bic());
        if(model.getAcc_holder_nameDirtyFlag())
            domain.setAccHolderName(model.getAcc_holder_name());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getRes_partner_bank_idDirtyFlag())
            domain.setResPartnerBankId(model.getRes_partner_bank_id());
        return domain ;
    }

}

    



