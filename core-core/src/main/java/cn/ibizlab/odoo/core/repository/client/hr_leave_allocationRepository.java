package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_leave_allocation;

/**
 * 实体[hr_leave_allocation] 服务对象接口
 */
public interface hr_leave_allocationRepository{


    public hr_leave_allocation createPO() ;
        public void removeBatch(String id);

        public void remove(String id);

        public void get(String id);

        public void updateBatch(hr_leave_allocation hr_leave_allocation);

        public void update(hr_leave_allocation hr_leave_allocation);

        public void create(hr_leave_allocation hr_leave_allocation);

        public void createBatch(hr_leave_allocation hr_leave_allocation);

        public List<hr_leave_allocation> search();


}
