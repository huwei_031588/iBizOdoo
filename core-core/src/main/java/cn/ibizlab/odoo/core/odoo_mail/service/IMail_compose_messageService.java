package cn.ibizlab.odoo.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_compose_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_compose_messageSearchContext;


/**
 * 实体[Mail_compose_message] 服务对象接口
 */
public interface IMail_compose_messageService{

    Mail_compose_message get(Integer key) ;
    boolean update(Mail_compose_message et) ;
    void updateBatch(List<Mail_compose_message> list) ;
    boolean create(Mail_compose_message et) ;
    void createBatch(List<Mail_compose_message> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Mail_compose_message> searchDefault(Mail_compose_messageSearchContext context) ;

}



