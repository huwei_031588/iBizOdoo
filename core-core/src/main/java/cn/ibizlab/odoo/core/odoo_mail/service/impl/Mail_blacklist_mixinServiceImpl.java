package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklist_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_blacklist_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_blacklist_mixinOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_blacklist_mixinClientModel;

/**
 * 实体[mixin邮件黑名单] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_blacklist_mixinServiceImpl implements IMail_blacklist_mixinService {

    @Autowired
    mail_blacklist_mixinOdooClient mail_blacklist_mixinOdooClient;


    @Override
    public boolean remove(Integer id) {
        mail_blacklist_mixinClientModel clientModel = new mail_blacklist_mixinClientModel();
        clientModel.setId(id);
		mail_blacklist_mixinOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Mail_blacklist_mixin et) {
        mail_blacklist_mixinClientModel clientModel = convert2Model(et,null);
		mail_blacklist_mixinOdooClient.update(clientModel);
        Mail_blacklist_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_blacklist_mixin> list){
    }

    @Override
    public Mail_blacklist_mixin get(Integer id) {
        mail_blacklist_mixinClientModel clientModel = new mail_blacklist_mixinClientModel();
        clientModel.setId(id);
		mail_blacklist_mixinOdooClient.get(clientModel);
        Mail_blacklist_mixin et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_blacklist_mixin();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mail_blacklist_mixin et) {
        mail_blacklist_mixinClientModel clientModel = convert2Model(et,null);
		mail_blacklist_mixinOdooClient.create(clientModel);
        Mail_blacklist_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_blacklist_mixin> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_blacklist_mixin> searchDefault(Mail_blacklist_mixinSearchContext context) {
        List<Mail_blacklist_mixin> list = new ArrayList<Mail_blacklist_mixin>();
        Page<mail_blacklist_mixinClientModel> clientModelList = mail_blacklist_mixinOdooClient.search(context);
        for(mail_blacklist_mixinClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_blacklist_mixin>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_blacklist_mixinClientModel convert2Model(Mail_blacklist_mixin domain , mail_blacklist_mixinClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_blacklist_mixinClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("is_blacklisteddirtyflag"))
                model.setIs_blacklisted(domain.getIsBlacklisted());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_blacklist_mixin convert2Domain( mail_blacklist_mixinClientModel model ,Mail_blacklist_mixin domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_blacklist_mixin();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getIs_blacklistedDirtyFlag())
            domain.setIsBlacklisted(model.getIs_blacklisted());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        return domain ;
    }

}

    



