package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badgeSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_badgeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_badgeOdooClient;
import cn.ibizlab.odoo.core.odoo_gamification.clientmodel.gamification_badgeClientModel;

/**
 * 实体[游戏化徽章] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_badgeServiceImpl implements IGamification_badgeService {

    @Autowired
    gamification_badgeOdooClient gamification_badgeOdooClient;


    @Override
    public boolean remove(Integer id) {
        gamification_badgeClientModel clientModel = new gamification_badgeClientModel();
        clientModel.setId(id);
		gamification_badgeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Gamification_badge et) {
        gamification_badgeClientModel clientModel = convert2Model(et,null);
		gamification_badgeOdooClient.update(clientModel);
        Gamification_badge rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Gamification_badge> list){
    }

    @Override
    public boolean create(Gamification_badge et) {
        gamification_badgeClientModel clientModel = convert2Model(et,null);
		gamification_badgeOdooClient.create(clientModel);
        Gamification_badge rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_badge> list){
    }

    @Override
    public Gamification_badge get(Integer id) {
        gamification_badgeClientModel clientModel = new gamification_badgeClientModel();
        clientModel.setId(id);
		gamification_badgeOdooClient.get(clientModel);
        Gamification_badge et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Gamification_badge();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_badge> searchDefault(Gamification_badgeSearchContext context) {
        List<Gamification_badge> list = new ArrayList<Gamification_badge>();
        Page<gamification_badgeClientModel> clientModelList = gamification_badgeOdooClient.search(context);
        for(gamification_badgeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Gamification_badge>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public gamification_badgeClientModel convert2Model(Gamification_badge domain , gamification_badgeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new gamification_badgeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("rule_authdirtyflag"))
                model.setRule_auth(domain.getRuleAuth());
            if((Boolean) domain.getExtensionparams().get("rule_max_numberdirtyflag"))
                model.setRule_max_number(domain.getRuleMaxNumber());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("rule_maxdirtyflag"))
                model.setRule_max(domain.getRuleMax());
            if((Boolean) domain.getExtensionparams().get("rule_auth_user_idsdirtyflag"))
                model.setRule_auth_user_ids(domain.getRuleAuthUserIds());
            if((Boolean) domain.getExtensionparams().get("rule_auth_badge_idsdirtyflag"))
                model.setRule_auth_badge_ids(domain.getRuleAuthBadgeIds());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("remaining_sendingdirtyflag"))
                model.setRemaining_sending(domain.getRemainingSending());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("stat_mydirtyflag"))
                model.setStat_my(domain.getStatMy());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("unique_owner_idsdirtyflag"))
                model.setUnique_owner_ids(domain.getUniqueOwnerIds());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("leveldirtyflag"))
                model.setLevel(domain.getLevel());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("owner_idsdirtyflag"))
                model.setOwner_ids(domain.getOwnerIds());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("goal_definition_idsdirtyflag"))
                model.setGoal_definition_ids(domain.getGoalDefinitionIds());
            if((Boolean) domain.getExtensionparams().get("stat_my_monthly_sendingdirtyflag"))
                model.setStat_my_monthly_sending(domain.getStatMyMonthlySending());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("stat_countdirtyflag"))
                model.setStat_count(domain.getStatCount());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("stat_this_monthdirtyflag"))
                model.setStat_this_month(domain.getStatThisMonth());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("stat_count_distinctdirtyflag"))
                model.setStat_count_distinct(domain.getStatCountDistinct());
            if((Boolean) domain.getExtensionparams().get("stat_my_this_monthdirtyflag"))
                model.setStat_my_this_month(domain.getStatMyThisMonth());
            if((Boolean) domain.getExtensionparams().get("challenge_idsdirtyflag"))
                model.setChallenge_ids(domain.getChallengeIds());
            if((Boolean) domain.getExtensionparams().get("granted_employees_countdirtyflag"))
                model.setGranted_employees_count(domain.getGrantedEmployeesCount());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Gamification_badge convert2Domain( gamification_badgeClientModel model ,Gamification_badge domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Gamification_badge();
        }

        if(model.getRule_authDirtyFlag())
            domain.setRuleAuth(model.getRule_auth());
        if(model.getRule_max_numberDirtyFlag())
            domain.setRuleMaxNumber(model.getRule_max_number());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getRule_maxDirtyFlag())
            domain.setRuleMax(model.getRule_max());
        if(model.getRule_auth_user_idsDirtyFlag())
            domain.setRuleAuthUserIds(model.getRule_auth_user_ids());
        if(model.getRule_auth_badge_idsDirtyFlag())
            domain.setRuleAuthBadgeIds(model.getRule_auth_badge_ids());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getRemaining_sendingDirtyFlag())
            domain.setRemainingSending(model.getRemaining_sending());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getStat_myDirtyFlag())
            domain.setStatMy(model.getStat_my());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getUnique_owner_idsDirtyFlag())
            domain.setUniqueOwnerIds(model.getUnique_owner_ids());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getLevelDirtyFlag())
            domain.setLevel(model.getLevel());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getOwner_idsDirtyFlag())
            domain.setOwnerIds(model.getOwner_ids());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getGoal_definition_idsDirtyFlag())
            domain.setGoalDefinitionIds(model.getGoal_definition_ids());
        if(model.getStat_my_monthly_sendingDirtyFlag())
            domain.setStatMyMonthlySending(model.getStat_my_monthly_sending());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getStat_countDirtyFlag())
            domain.setStatCount(model.getStat_count());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getStat_this_monthDirtyFlag())
            domain.setStatThisMonth(model.getStat_this_month());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getStat_count_distinctDirtyFlag())
            domain.setStatCountDistinct(model.getStat_count_distinct());
        if(model.getStat_my_this_monthDirtyFlag())
            domain.setStatMyThisMonth(model.getStat_my_this_month());
        if(model.getChallenge_idsDirtyFlag())
            domain.setChallengeIds(model.getChallenge_ids());
        if(model.getGranted_employees_countDirtyFlag())
            domain.setGrantedEmployeesCount(model.getGranted_employees_count());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



