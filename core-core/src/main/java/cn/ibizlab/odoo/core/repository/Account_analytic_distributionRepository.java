package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_analytic_distribution;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_distributionSearchContext;

/**
 * 实体 [分析账户分配] 存储对象
 */
public interface Account_analytic_distributionRepository extends Repository<Account_analytic_distribution> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_analytic_distribution> searchDefault(Account_analytic_distributionSearchContext context);

    Account_analytic_distribution convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_distribution domain , Account_analytic_distribution po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_distribution convert2Domain( Account_analytic_distribution po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_distribution domain) ;

}
