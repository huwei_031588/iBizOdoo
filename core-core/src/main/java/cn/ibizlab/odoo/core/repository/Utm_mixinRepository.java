package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Utm_mixin;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mixinSearchContext;

/**
 * 实体 [UTM Mixin] 存储对象
 */
public interface Utm_mixinRepository extends Repository<Utm_mixin> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Utm_mixin> searchDefault(Utm_mixinSearchContext context);

    Utm_mixin convert2PO(cn.ibizlab.odoo.core.odoo_utm.domain.Utm_mixin domain , Utm_mixin po) ;

    cn.ibizlab.odoo.core.odoo_utm.domain.Utm_mixin convert2Domain( Utm_mixin po ,cn.ibizlab.odoo.core.odoo_utm.domain.Utm_mixin domain) ;

}
