package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.website_published_mixin;

/**
 * 实体[website_published_mixin] 服务对象接口
 */
public interface website_published_mixinRepository{


    public website_published_mixin createPO() ;
        public List<website_published_mixin> search();

        public void remove(String id);

        public void updateBatch(website_published_mixin website_published_mixin);

        public void createBatch(website_published_mixin website_published_mixin);

        public void create(website_published_mixin website_published_mixin);

        public void update(website_published_mixin website_published_mixin);

        public void get(String id);

        public void removeBatch(String id);


}
