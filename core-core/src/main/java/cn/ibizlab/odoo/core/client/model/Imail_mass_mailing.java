package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_mass_mailing] 对象
 */
public interface Imail_mass_mailing {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setAttachment_ids(String attachment_ids);
    
    /**
     * 设置 [附件]
     */
    public String getAttachment_ids();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getAttachment_idsDirtyFlag();
    /**
     * 获取 [正文]
     */
    public void setBody_html(String body_html);
    
    /**
     * 设置 [正文]
     */
    public String getBody_html();

    /**
     * 获取 [正文]脏标记
     */
    public boolean getBody_htmlDirtyFlag();
    /**
     * 获取 [被退回]
     */
    public void setBounced(Integer bounced);
    
    /**
     * 设置 [被退回]
     */
    public Integer getBounced();

    /**
     * 获取 [被退回]脏标记
     */
    public boolean getBouncedDirtyFlag();
    /**
     * 获取 [被退回的比率]
     */
    public void setBounced_ratio(Integer bounced_ratio);
    
    /**
     * 设置 [被退回的比率]
     */
    public Integer getBounced_ratio();

    /**
     * 获取 [被退回的比率]脏标记
     */
    public boolean getBounced_ratioDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id(Integer campaign_id);
    
    /**
     * 设置 [营销]
     */
    public Integer getCampaign_id();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_idDirtyFlag();
    /**
     * 获取 [营销]
     */
    public void setCampaign_id_text(String campaign_id_text);
    
    /**
     * 设置 [营销]
     */
    public String getCampaign_id_text();

    /**
     * 获取 [营销]脏标记
     */
    public boolean getCampaign_id_textDirtyFlag();
    /**
     * 获取 [点击率]
     */
    public void setClicked(Integer clicked);
    
    /**
     * 设置 [点击率]
     */
    public Integer getClicked();

    /**
     * 获取 [点击率]脏标记
     */
    public boolean getClickedDirtyFlag();
    /**
     * 获取 [点击数]
     */
    public void setClicks_ratio(Integer clicks_ratio);
    
    /**
     * 设置 [点击数]
     */
    public Integer getClicks_ratio();

    /**
     * 获取 [点击数]脏标记
     */
    public boolean getClicks_ratioDirtyFlag();
    /**
     * 获取 [颜色索引]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色索引]
     */
    public Integer getColor();

    /**
     * 获取 [颜色索引]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [A/B 测试百分比]
     */
    public void setContact_ab_pc(Integer contact_ab_pc);
    
    /**
     * 设置 [A/B 测试百分比]
     */
    public Integer getContact_ab_pc();

    /**
     * 获取 [A/B 测试百分比]脏标记
     */
    public boolean getContact_ab_pcDirtyFlag();
    /**
     * 获取 [邮件列表]
     */
    public void setContact_list_ids(String contact_list_ids);
    
    /**
     * 设置 [邮件列表]
     */
    public String getContact_list_ids();

    /**
     * 获取 [邮件列表]脏标记
     */
    public boolean getContact_list_idsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [使用线索]
     */
    public void setCrm_lead_activated(String crm_lead_activated);
    
    /**
     * 设置 [使用线索]
     */
    public String getCrm_lead_activated();

    /**
     * 获取 [使用线索]脏标记
     */
    public boolean getCrm_lead_activatedDirtyFlag();
    /**
     * 获取 [线索总数]
     */
    public void setCrm_lead_count(Integer crm_lead_count);
    
    /**
     * 设置 [线索总数]
     */
    public Integer getCrm_lead_count();

    /**
     * 获取 [线索总数]脏标记
     */
    public boolean getCrm_lead_countDirtyFlag();
    /**
     * 获取 [商机个数]
     */
    public void setCrm_opportunities_count(Integer crm_opportunities_count);
    
    /**
     * 设置 [商机个数]
     */
    public Integer getCrm_opportunities_count();

    /**
     * 获取 [商机个数]脏标记
     */
    public boolean getCrm_opportunities_countDirtyFlag();
    /**
     * 获取 [已送货]
     */
    public void setDelivered(Integer delivered);
    
    /**
     * 设置 [已送货]
     */
    public Integer getDelivered();

    /**
     * 获取 [已送货]脏标记
     */
    public boolean getDeliveredDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [从]
     */
    public void setEmail_from(String email_from);
    
    /**
     * 设置 [从]
     */
    public String getEmail_from();

    /**
     * 获取 [从]脏标记
     */
    public boolean getEmail_fromDirtyFlag();
    /**
     * 获取 [预期]
     */
    public void setExpected(Integer expected);
    
    /**
     * 设置 [预期]
     */
    public Integer getExpected();

    /**
     * 获取 [预期]脏标记
     */
    public boolean getExpectedDirtyFlag();
    /**
     * 获取 [失败的]
     */
    public void setFailed(Integer failed);
    
    /**
     * 设置 [失败的]
     */
    public Integer getFailed();

    /**
     * 获取 [失败的]脏标记
     */
    public boolean getFailedDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [忽略]
     */
    public void setIgnored(Integer ignored);
    
    /**
     * 设置 [忽略]
     */
    public Integer getIgnored();

    /**
     * 获取 [忽略]脏标记
     */
    public boolean getIgnoredDirtyFlag();
    /**
     * 获取 [归档]
     */
    public void setKeep_archives(String keep_archives);
    
    /**
     * 设置 [归档]
     */
    public String getKeep_archives();

    /**
     * 获取 [归档]脏标记
     */
    public boolean getKeep_archivesDirtyFlag();
    /**
     * 获取 [域]
     */
    public void setMailing_domain(String mailing_domain);
    
    /**
     * 设置 [域]
     */
    public String getMailing_domain();

    /**
     * 获取 [域]脏标记
     */
    public boolean getMailing_domainDirtyFlag();
    /**
     * 获取 [收件人模型]
     */
    public void setMailing_model_id(Integer mailing_model_id);
    
    /**
     * 设置 [收件人模型]
     */
    public Integer getMailing_model_id();

    /**
     * 获取 [收件人模型]脏标记
     */
    public boolean getMailing_model_idDirtyFlag();
    /**
     * 获取 [收件人模型]
     */
    public void setMailing_model_name(String mailing_model_name);
    
    /**
     * 设置 [收件人模型]
     */
    public String getMailing_model_name();

    /**
     * 获取 [收件人模型]脏标记
     */
    public boolean getMailing_model_nameDirtyFlag();
    /**
     * 获取 [收件人实物模型]
     */
    public void setMailing_model_real(String mailing_model_real);
    
    /**
     * 设置 [收件人实物模型]
     */
    public String getMailing_model_real();

    /**
     * 获取 [收件人实物模型]脏标记
     */
    public boolean getMailing_model_realDirtyFlag();
    /**
     * 获取 [邮件服务器]
     */
    public void setMail_server_id(Integer mail_server_id);
    
    /**
     * 设置 [邮件服务器]
     */
    public Integer getMail_server_id();

    /**
     * 获取 [邮件服务器]脏标记
     */
    public boolean getMail_server_idDirtyFlag();
    /**
     * 获取 [群发邮件营销]
     */
    public void setMass_mailing_campaign_id(Integer mass_mailing_campaign_id);
    
    /**
     * 设置 [群发邮件营销]
     */
    public Integer getMass_mailing_campaign_id();

    /**
     * 获取 [群发邮件营销]脏标记
     */
    public boolean getMass_mailing_campaign_idDirtyFlag();
    /**
     * 获取 [群发邮件营销]
     */
    public void setMass_mailing_campaign_id_text(String mass_mailing_campaign_id_text);
    
    /**
     * 设置 [群发邮件营销]
     */
    public String getMass_mailing_campaign_id_text();

    /**
     * 获取 [群发邮件营销]脏标记
     */
    public boolean getMass_mailing_campaign_id_textDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id(Integer medium_id);
    
    /**
     * 设置 [媒体]
     */
    public Integer getMedium_id();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_idDirtyFlag();
    /**
     * 获取 [媒体]
     */
    public void setMedium_id_text(String medium_id_text);
    
    /**
     * 设置 [媒体]
     */
    public String getMedium_id_text();

    /**
     * 获取 [媒体]脏标记
     */
    public boolean getMedium_id_textDirtyFlag();
    /**
     * 获取 [来源名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [来源名称]
     */
    public String getName();

    /**
     * 获取 [来源名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [安排的日期]
     */
    public void setNext_departure(Timestamp next_departure);
    
    /**
     * 设置 [安排的日期]
     */
    public Timestamp getNext_departure();

    /**
     * 获取 [安排的日期]脏标记
     */
    public boolean getNext_departureDirtyFlag();
    /**
     * 获取 [已开启]
     */
    public void setOpened(Integer opened);
    
    /**
     * 设置 [已开启]
     */
    public Integer getOpened();

    /**
     * 获取 [已开启]脏标记
     */
    public boolean getOpenedDirtyFlag();
    /**
     * 获取 [打开比例]
     */
    public void setOpened_ratio(Integer opened_ratio);
    
    /**
     * 设置 [打开比例]
     */
    public Integer getOpened_ratio();

    /**
     * 获取 [打开比例]脏标记
     */
    public boolean getOpened_ratioDirtyFlag();
    /**
     * 获取 [已接收比例]
     */
    public void setReceived_ratio(Integer received_ratio);
    
    /**
     * 设置 [已接收比例]
     */
    public Integer getReceived_ratio();

    /**
     * 获取 [已接收比例]脏标记
     */
    public boolean getReceived_ratioDirtyFlag();
    /**
     * 获取 [已回复]
     */
    public void setReplied(Integer replied);
    
    /**
     * 设置 [已回复]
     */
    public Integer getReplied();

    /**
     * 获取 [已回复]脏标记
     */
    public boolean getRepliedDirtyFlag();
    /**
     * 获取 [回复比例]
     */
    public void setReplied_ratio(Integer replied_ratio);
    
    /**
     * 设置 [回复比例]
     */
    public Integer getReplied_ratio();

    /**
     * 获取 [回复比例]脏标记
     */
    public boolean getReplied_ratioDirtyFlag();
    /**
     * 获取 [回复]
     */
    public void setReply_to(String reply_to);
    
    /**
     * 设置 [回复]
     */
    public String getReply_to();

    /**
     * 获取 [回复]脏标记
     */
    public boolean getReply_toDirtyFlag();
    /**
     * 获取 [回复模式]
     */
    public void setReply_to_mode(String reply_to_mode);
    
    /**
     * 设置 [回复模式]
     */
    public String getReply_to_mode();

    /**
     * 获取 [回复模式]脏标记
     */
    public boolean getReply_to_modeDirtyFlag();
    /**
     * 获取 [开票金额]
     */
    public void setSale_invoiced_amount(Integer sale_invoiced_amount);
    
    /**
     * 设置 [开票金额]
     */
    public Integer getSale_invoiced_amount();

    /**
     * 获取 [开票金额]脏标记
     */
    public boolean getSale_invoiced_amountDirtyFlag();
    /**
     * 获取 [报价个数]
     */
    public void setSale_quotation_count(Integer sale_quotation_count);
    
    /**
     * 设置 [报价个数]
     */
    public Integer getSale_quotation_count();

    /**
     * 获取 [报价个数]脏标记
     */
    public boolean getSale_quotation_countDirtyFlag();
    /**
     * 获取 [安排]
     */
    public void setScheduled(Integer scheduled);
    
    /**
     * 设置 [安排]
     */
    public Integer getScheduled();

    /**
     * 获取 [安排]脏标记
     */
    public boolean getScheduledDirtyFlag();
    /**
     * 获取 [在将来计划]
     */
    public void setSchedule_date(Timestamp schedule_date);
    
    /**
     * 设置 [在将来计划]
     */
    public Timestamp getSchedule_date();

    /**
     * 获取 [在将来计划]脏标记
     */
    public boolean getSchedule_dateDirtyFlag();
    /**
     * 获取 [已汇]
     */
    public void setSent(Integer sent);
    
    /**
     * 设置 [已汇]
     */
    public Integer getSent();

    /**
     * 获取 [已汇]脏标记
     */
    public boolean getSentDirtyFlag();
    /**
     * 获取 [发送日期]
     */
    public void setSent_date(Timestamp sent_date);
    
    /**
     * 设置 [发送日期]
     */
    public Timestamp getSent_date();

    /**
     * 获取 [发送日期]脏标记
     */
    public boolean getSent_dateDirtyFlag();
    /**
     * 获取 [主题]
     */
    public void setSource_id(Integer source_id);
    
    /**
     * 设置 [主题]
     */
    public Integer getSource_id();

    /**
     * 获取 [主题]脏标记
     */
    public boolean getSource_idDirtyFlag();
    /**
     * 获取 [状态]
     */
    public void setState(String state);
    
    /**
     * 设置 [状态]
     */
    public String getState();

    /**
     * 获取 [状态]脏标记
     */
    public boolean getStateDirtyFlag();
    /**
     * 获取 [邮件统计]
     */
    public void setStatistics_ids(String statistics_ids);
    
    /**
     * 设置 [邮件统计]
     */
    public String getStatistics_ids();

    /**
     * 获取 [邮件统计]脏标记
     */
    public boolean getStatistics_idsDirtyFlag();
    /**
     * 获取 [总计]
     */
    public void setTotal(Integer total);
    
    /**
     * 设置 [总计]
     */
    public Integer getTotal();

    /**
     * 获取 [总计]脏标记
     */
    public boolean getTotalDirtyFlag();
    /**
     * 获取 [邮件管理器]
     */
    public void setUser_id(Integer user_id);
    
    /**
     * 设置 [邮件管理器]
     */
    public Integer getUser_id();

    /**
     * 获取 [邮件管理器]脏标记
     */
    public boolean getUser_idDirtyFlag();
    /**
     * 获取 [邮件管理器]
     */
    public void setUser_id_text(String user_id_text);
    
    /**
     * 设置 [邮件管理器]
     */
    public String getUser_id_text();

    /**
     * 获取 [邮件管理器]脏标记
     */
    public boolean getUser_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
