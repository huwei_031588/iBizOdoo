package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_refundSearchContext;

/**
 * 实体 [信用票] 存储模型
 */
public interface Account_invoice_refund{

    /**
     * 原因
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [原因]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 贷方方法
     */
    String getFilter_refund();

    void setFilter_refund(String filter_refund);

    /**
     * 获取 [贷方方法]脏标记
     */
    boolean getFilter_refundDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 会计日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [会计日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 信用票日期
     */
    Timestamp getDate_invoice();

    void setDate_invoice(Timestamp date_invoice);

    /**
     * 获取 [信用票日期]脏标记
     */
    boolean getDate_invoiceDirtyFlag();

    /**
     * 当发票被部分支付时，字段filter_refund将被隐藏
     */
    String getRefund_only();

    void setRefund_only(String refund_only);

    /**
     * 获取 [当发票被部分支付时，字段filter_refund将被隐藏]脏标记
     */
    boolean getRefund_onlyDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
