package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_orderSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_orderService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_sale.client.sale_orderOdooClient;
import cn.ibizlab.odoo.core.odoo_sale.clientmodel.sale_orderClientModel;

/**
 * 实体[销售订单] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_orderServiceImpl implements ISale_orderService {

    @Autowired
    sale_orderOdooClient sale_orderOdooClient;


    @Override
    public boolean update(Sale_order et) {
        sale_orderClientModel clientModel = convert2Model(et,null);
		sale_orderOdooClient.update(clientModel);
        Sale_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Sale_order> list){
    }

    @Override
    public Sale_order get(Integer id) {
        sale_orderClientModel clientModel = new sale_orderClientModel();
        clientModel.setId(id);
		sale_orderOdooClient.get(clientModel);
        Sale_order et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Sale_order();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        sale_orderClientModel clientModel = new sale_orderClientModel();
        clientModel.setId(id);
		sale_orderOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Sale_order et) {
        sale_orderClientModel clientModel = convert2Model(et,null);
		sale_orderOdooClient.create(clientModel);
        Sale_order rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_order> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_order> searchDefault(Sale_orderSearchContext context) {
        List<Sale_order> list = new ArrayList<Sale_order>();
        Page<sale_orderClientModel> clientModelList = sale_orderOdooClient.search(context);
        for(sale_orderClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Sale_order>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public sale_orderClientModel convert2Model(Sale_order domain , sale_orderClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new sale_orderClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("date_orderdirtyflag"))
                model.setDate_order(domain.getDateOrder());
            if((Boolean) domain.getExtensionparams().get("invoice_idsdirtyflag"))
                model.setInvoice_ids(domain.getInvoiceIds());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("amount_untaxeddirtyflag"))
                model.setAmount_untaxed(domain.getAmountUntaxed());
            if((Boolean) domain.getExtensionparams().get("validity_datedirtyflag"))
                model.setValidity_date(domain.getValidityDate());
            if((Boolean) domain.getExtensionparams().get("amount_undiscounteddirtyflag"))
                model.setAmount_undiscounted(domain.getAmountUndiscounted());
            if((Boolean) domain.getExtensionparams().get("invoice_countdirtyflag"))
                model.setInvoice_count(domain.getInvoiceCount());
            if((Boolean) domain.getExtensionparams().get("access_warningdirtyflag"))
                model.setAccess_warning(domain.getAccessWarning());
            if((Boolean) domain.getExtensionparams().get("warning_stockdirtyflag"))
                model.setWarning_stock(domain.getWarningStock());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("effective_datedirtyflag"))
                model.setEffective_date(domain.getEffectiveDate());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("require_signaturedirtyflag"))
                model.setRequire_signature(domain.getRequireSignature());
            if((Boolean) domain.getExtensionparams().get("currency_ratedirtyflag"))
                model.setCurrency_rate(domain.getCurrencyRate());
            if((Boolean) domain.getExtensionparams().get("picking_policydirtyflag"))
                model.setPicking_policy(domain.getPickingPolicy());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("signaturedirtyflag"))
                model.setSignature(domain.getSignature());
            if((Boolean) domain.getExtensionparams().get("procurement_group_iddirtyflag"))
                model.setProcurement_group_id(domain.getProcurementGroupId());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("cart_quantitydirtyflag"))
                model.setCart_quantity(domain.getCartQuantity());
            if((Boolean) domain.getExtensionparams().get("type_namedirtyflag"))
                model.setType_name(domain.getTypeName());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("delivery_countdirtyflag"))
                model.setDelivery_count(domain.getDeliveryCount());
            if((Boolean) domain.getExtensionparams().get("signed_bydirtyflag"))
                model.setSigned_by(domain.getSignedBy());
            if((Boolean) domain.getExtensionparams().get("order_linedirtyflag"))
                model.setOrder_line(domain.getOrderLine());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("require_paymentdirtyflag"))
                model.setRequire_payment(domain.getRequirePayment());
            if((Boolean) domain.getExtensionparams().get("only_servicesdirtyflag"))
                model.setOnly_services(domain.getOnlyServices());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("website_order_linedirtyflag"))
                model.setWebsite_order_line(domain.getWebsiteOrderLine());
            if((Boolean) domain.getExtensionparams().get("referencedirtyflag"))
                model.setReference(domain.getReference());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("invoice_statusdirtyflag"))
                model.setInvoice_status(domain.getInvoiceStatus());
            if((Boolean) domain.getExtensionparams().get("tag_idsdirtyflag"))
                model.setTag_ids(domain.getTagIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("transaction_idsdirtyflag"))
                model.setTransaction_ids(domain.getTransactionIds());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("is_abandoned_cartdirtyflag"))
                model.setIs_abandoned_cart(domain.getIsAbandonedCart());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("expense_countdirtyflag"))
                model.setExpense_count(domain.getExpenseCount());
            if((Boolean) domain.getExtensionparams().get("authorized_transaction_idsdirtyflag"))
                model.setAuthorized_transaction_ids(domain.getAuthorizedTransactionIds());
            if((Boolean) domain.getExtensionparams().get("commitment_datedirtyflag"))
                model.setCommitment_date(domain.getCommitmentDate());
            if((Boolean) domain.getExtensionparams().get("is_expireddirtyflag"))
                model.setIs_expired(domain.getIsExpired());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("remaining_validity_daysdirtyflag"))
                model.setRemaining_validity_days(domain.getRemainingValidityDays());
            if((Boolean) domain.getExtensionparams().get("expected_datedirtyflag"))
                model.setExpected_date(domain.getExpectedDate());
            if((Boolean) domain.getExtensionparams().get("purchase_order_countdirtyflag"))
                model.setPurchase_order_count(domain.getPurchaseOrderCount());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("access_urldirtyflag"))
                model.setAccess_url(domain.getAccessUrl());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("sale_order_option_idsdirtyflag"))
                model.setSale_order_option_ids(domain.getSaleOrderOptionIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("origindirtyflag"))
                model.setOrigin(domain.getOrigin());
            if((Boolean) domain.getExtensionparams().get("confirmation_datedirtyflag"))
                model.setConfirmation_date(domain.getConfirmationDate());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("amount_by_groupdirtyflag"))
                model.setAmount_by_group(domain.getAmountByGroup());
            if((Boolean) domain.getExtensionparams().get("picking_idsdirtyflag"))
                model.setPicking_ids(domain.getPickingIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("access_tokendirtyflag"))
                model.setAccess_token(domain.getAccessToken());
            if((Boolean) domain.getExtensionparams().get("amount_totaldirtyflag"))
                model.setAmount_total(domain.getAmountTotal());
            if((Boolean) domain.getExtensionparams().get("client_order_refdirtyflag"))
                model.setClient_order_ref(domain.getClientOrderRef());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("cart_recovery_email_sentdirtyflag"))
                model.setCart_recovery_email_sent(domain.getCartRecoveryEmailSent());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("amount_taxdirtyflag"))
                model.setAmount_tax(domain.getAmountTax());
            if((Boolean) domain.getExtensionparams().get("expense_idsdirtyflag"))
                model.setExpense_ids(domain.getExpenseIds());
            if((Boolean) domain.getExtensionparams().get("partner_invoice_id_textdirtyflag"))
                model.setPartner_invoice_id_text(domain.getPartnerInvoiceIdText());
            if((Boolean) domain.getExtensionparams().get("incoterm_textdirtyflag"))
                model.setIncoterm_text(domain.getIncotermText());
            if((Boolean) domain.getExtensionparams().get("team_id_textdirtyflag"))
                model.setTeam_id_text(domain.getTeamIdText());
            if((Boolean) domain.getExtensionparams().get("analytic_account_id_textdirtyflag"))
                model.setAnalytic_account_id_text(domain.getAnalyticAccountIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("campaign_id_textdirtyflag"))
                model.setCampaign_id_text(domain.getCampaignIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("partner_shipping_id_textdirtyflag"))
                model.setPartner_shipping_id_text(domain.getPartnerShippingIdText());
            if((Boolean) domain.getExtensionparams().get("warehouse_id_textdirtyflag"))
                model.setWarehouse_id_text(domain.getWarehouseIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("opportunity_id_textdirtyflag"))
                model.setOpportunity_id_text(domain.getOpportunityIdText());
            if((Boolean) domain.getExtensionparams().get("source_id_textdirtyflag"))
                model.setSource_id_text(domain.getSourceIdText());
            if((Boolean) domain.getExtensionparams().get("medium_id_textdirtyflag"))
                model.setMedium_id_text(domain.getMediumIdText());
            if((Boolean) domain.getExtensionparams().get("fiscal_position_id_textdirtyflag"))
                model.setFiscal_position_id_text(domain.getFiscalPositionIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("pricelist_id_textdirtyflag"))
                model.setPricelist_id_text(domain.getPricelistIdText());
            if((Boolean) domain.getExtensionparams().get("payment_term_id_textdirtyflag"))
                model.setPayment_term_id_text(domain.getPaymentTermIdText());
            if((Boolean) domain.getExtensionparams().get("sale_order_template_id_textdirtyflag"))
                model.setSale_order_template_id_text(domain.getSaleOrderTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("team_iddirtyflag"))
                model.setTeam_id(domain.getTeamId());
            if((Boolean) domain.getExtensionparams().get("partner_invoice_iddirtyflag"))
                model.setPartner_invoice_id(domain.getPartnerInvoiceId());
            if((Boolean) domain.getExtensionparams().get("fiscal_position_iddirtyflag"))
                model.setFiscal_position_id(domain.getFiscalPositionId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("campaign_iddirtyflag"))
                model.setCampaign_id(domain.getCampaignId());
            if((Boolean) domain.getExtensionparams().get("source_iddirtyflag"))
                model.setSource_id(domain.getSourceId());
            if((Boolean) domain.getExtensionparams().get("sale_order_template_iddirtyflag"))
                model.setSale_order_template_id(domain.getSaleOrderTemplateId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("medium_iddirtyflag"))
                model.setMedium_id(domain.getMediumId());
            if((Boolean) domain.getExtensionparams().get("analytic_account_iddirtyflag"))
                model.setAnalytic_account_id(domain.getAnalyticAccountId());
            if((Boolean) domain.getExtensionparams().get("payment_term_iddirtyflag"))
                model.setPayment_term_id(domain.getPaymentTermId());
            if((Boolean) domain.getExtensionparams().get("partner_shipping_iddirtyflag"))
                model.setPartner_shipping_id(domain.getPartnerShippingId());
            if((Boolean) domain.getExtensionparams().get("opportunity_iddirtyflag"))
                model.setOpportunity_id(domain.getOpportunityId());
            if((Boolean) domain.getExtensionparams().get("incotermdirtyflag"))
                model.setIncoterm(domain.getIncoterm());
            if((Boolean) domain.getExtensionparams().get("pricelist_iddirtyflag"))
                model.setPricelist_id(domain.getPricelistId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("warehouse_iddirtyflag"))
                model.setWarehouse_id(domain.getWarehouseId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Sale_order convert2Domain( sale_orderClientModel model ,Sale_order domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Sale_order();
        }

        if(model.getDate_orderDirtyFlag())
            domain.setDateOrder(model.getDate_order());
        if(model.getInvoice_idsDirtyFlag())
            domain.setInvoiceIds(model.getInvoice_ids());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getAmount_untaxedDirtyFlag())
            domain.setAmountUntaxed(model.getAmount_untaxed());
        if(model.getValidity_dateDirtyFlag())
            domain.setValidityDate(model.getValidity_date());
        if(model.getAmount_undiscountedDirtyFlag())
            domain.setAmountUndiscounted(model.getAmount_undiscounted());
        if(model.getInvoice_countDirtyFlag())
            domain.setInvoiceCount(model.getInvoice_count());
        if(model.getAccess_warningDirtyFlag())
            domain.setAccessWarning(model.getAccess_warning());
        if(model.getWarning_stockDirtyFlag())
            domain.setWarningStock(model.getWarning_stock());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getEffective_dateDirtyFlag())
            domain.setEffectiveDate(model.getEffective_date());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getRequire_signatureDirtyFlag())
            domain.setRequireSignature(model.getRequire_signature());
        if(model.getCurrency_rateDirtyFlag())
            domain.setCurrencyRate(model.getCurrency_rate());
        if(model.getPicking_policyDirtyFlag())
            domain.setPickingPolicy(model.getPicking_policy());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getSignatureDirtyFlag())
            domain.setSignature(model.getSignature());
        if(model.getProcurement_group_idDirtyFlag())
            domain.setProcurementGroupId(model.getProcurement_group_id());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getCart_quantityDirtyFlag())
            domain.setCartQuantity(model.getCart_quantity());
        if(model.getType_nameDirtyFlag())
            domain.setTypeName(model.getType_name());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getDelivery_countDirtyFlag())
            domain.setDeliveryCount(model.getDelivery_count());
        if(model.getSigned_byDirtyFlag())
            domain.setSignedBy(model.getSigned_by());
        if(model.getOrder_lineDirtyFlag())
            domain.setOrderLine(model.getOrder_line());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getRequire_paymentDirtyFlag())
            domain.setRequirePayment(model.getRequire_payment());
        if(model.getOnly_servicesDirtyFlag())
            domain.setOnlyServices(model.getOnly_services());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getWebsite_order_lineDirtyFlag())
            domain.setWebsiteOrderLine(model.getWebsite_order_line());
        if(model.getReferenceDirtyFlag())
            domain.setReference(model.getReference());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getInvoice_statusDirtyFlag())
            domain.setInvoiceStatus(model.getInvoice_status());
        if(model.getTag_idsDirtyFlag())
            domain.setTagIds(model.getTag_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getTransaction_idsDirtyFlag())
            domain.setTransactionIds(model.getTransaction_ids());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getIs_abandoned_cartDirtyFlag())
            domain.setIsAbandonedCart(model.getIs_abandoned_cart());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getExpense_countDirtyFlag())
            domain.setExpenseCount(model.getExpense_count());
        if(model.getAuthorized_transaction_idsDirtyFlag())
            domain.setAuthorizedTransactionIds(model.getAuthorized_transaction_ids());
        if(model.getCommitment_dateDirtyFlag())
            domain.setCommitmentDate(model.getCommitment_date());
        if(model.getIs_expiredDirtyFlag())
            domain.setIsExpired(model.getIs_expired());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getRemaining_validity_daysDirtyFlag())
            domain.setRemainingValidityDays(model.getRemaining_validity_days());
        if(model.getExpected_dateDirtyFlag())
            domain.setExpectedDate(model.getExpected_date());
        if(model.getPurchase_order_countDirtyFlag())
            domain.setPurchaseOrderCount(model.getPurchase_order_count());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getAccess_urlDirtyFlag())
            domain.setAccessUrl(model.getAccess_url());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getSale_order_option_idsDirtyFlag())
            domain.setSaleOrderOptionIds(model.getSale_order_option_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getOriginDirtyFlag())
            domain.setOrigin(model.getOrigin());
        if(model.getConfirmation_dateDirtyFlag())
            domain.setConfirmationDate(model.getConfirmation_date());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getAmount_by_groupDirtyFlag())
            domain.setAmountByGroup(model.getAmount_by_group());
        if(model.getPicking_idsDirtyFlag())
            domain.setPickingIds(model.getPicking_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getAccess_tokenDirtyFlag())
            domain.setAccessToken(model.getAccess_token());
        if(model.getAmount_totalDirtyFlag())
            domain.setAmountTotal(model.getAmount_total());
        if(model.getClient_order_refDirtyFlag())
            domain.setClientOrderRef(model.getClient_order_ref());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getCart_recovery_email_sentDirtyFlag())
            domain.setCartRecoveryEmailSent(model.getCart_recovery_email_sent());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getAmount_taxDirtyFlag())
            domain.setAmountTax(model.getAmount_tax());
        if(model.getExpense_idsDirtyFlag())
            domain.setExpenseIds(model.getExpense_ids());
        if(model.getPartner_invoice_id_textDirtyFlag())
            domain.setPartnerInvoiceIdText(model.getPartner_invoice_id_text());
        if(model.getIncoterm_textDirtyFlag())
            domain.setIncotermText(model.getIncoterm_text());
        if(model.getTeam_id_textDirtyFlag())
            domain.setTeamIdText(model.getTeam_id_text());
        if(model.getAnalytic_account_id_textDirtyFlag())
            domain.setAnalyticAccountIdText(model.getAnalytic_account_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCampaign_id_textDirtyFlag())
            domain.setCampaignIdText(model.getCampaign_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getPartner_shipping_id_textDirtyFlag())
            domain.setPartnerShippingIdText(model.getPartner_shipping_id_text());
        if(model.getWarehouse_id_textDirtyFlag())
            domain.setWarehouseIdText(model.getWarehouse_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getOpportunity_id_textDirtyFlag())
            domain.setOpportunityIdText(model.getOpportunity_id_text());
        if(model.getSource_id_textDirtyFlag())
            domain.setSourceIdText(model.getSource_id_text());
        if(model.getMedium_id_textDirtyFlag())
            domain.setMediumIdText(model.getMedium_id_text());
        if(model.getFiscal_position_id_textDirtyFlag())
            domain.setFiscalPositionIdText(model.getFiscal_position_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPricelist_id_textDirtyFlag())
            domain.setPricelistIdText(model.getPricelist_id_text());
        if(model.getPayment_term_id_textDirtyFlag())
            domain.setPaymentTermIdText(model.getPayment_term_id_text());
        if(model.getSale_order_template_id_textDirtyFlag())
            domain.setSaleOrderTemplateIdText(model.getSale_order_template_id_text());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getTeam_idDirtyFlag())
            domain.setTeamId(model.getTeam_id());
        if(model.getPartner_invoice_idDirtyFlag())
            domain.setPartnerInvoiceId(model.getPartner_invoice_id());
        if(model.getFiscal_position_idDirtyFlag())
            domain.setFiscalPositionId(model.getFiscal_position_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getCampaign_idDirtyFlag())
            domain.setCampaignId(model.getCampaign_id());
        if(model.getSource_idDirtyFlag())
            domain.setSourceId(model.getSource_id());
        if(model.getSale_order_template_idDirtyFlag())
            domain.setSaleOrderTemplateId(model.getSale_order_template_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getMedium_idDirtyFlag())
            domain.setMediumId(model.getMedium_id());
        if(model.getAnalytic_account_idDirtyFlag())
            domain.setAnalyticAccountId(model.getAnalytic_account_id());
        if(model.getPayment_term_idDirtyFlag())
            domain.setPaymentTermId(model.getPayment_term_id());
        if(model.getPartner_shipping_idDirtyFlag())
            domain.setPartnerShippingId(model.getPartner_shipping_id());
        if(model.getOpportunity_idDirtyFlag())
            domain.setOpportunityId(model.getOpportunity_id());
        if(model.getIncotermDirtyFlag())
            domain.setIncoterm(model.getIncoterm());
        if(model.getPricelist_idDirtyFlag())
            domain.setPricelistId(model.getPricelist_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWarehouse_idDirtyFlag())
            domain.setWarehouseId(model.getWarehouse_id());
        return domain ;
    }

}

    



