package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_product_produce;

/**
 * 实体[mrp_product_produce] 服务对象接口
 */
public interface mrp_product_produceRepository{


    public mrp_product_produce createPO() ;
        public void createBatch(mrp_product_produce mrp_product_produce);

        public List<mrp_product_produce> search();

        public void removeBatch(String id);

        public void updateBatch(mrp_product_produce mrp_product_produce);

        public void get(String id);

        public void remove(String id);

        public void create(mrp_product_produce mrp_product_produce);

        public void update(mrp_product_produce mrp_product_produce);


}
