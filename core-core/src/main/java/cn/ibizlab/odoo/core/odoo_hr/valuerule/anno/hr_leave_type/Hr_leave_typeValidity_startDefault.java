package cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_leave_type;

import cn.ibizlab.odoo.core.odoo_hr.valuerule.validator.hr_leave_type.Hr_leave_typeValidity_startDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Hr_leave_type
 * 属性：Validity_start
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Hr_leave_typeValidity_startDefaultValidator.class})
public @interface Hr_leave_typeValidity_startDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
