package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_account_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_templateSearchContext;

/**
 * 实体 [科目模板] 存储对象
 */
public interface Account_account_templateRepository extends Repository<Account_account_template> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_account_template> searchDefault(Account_account_templateSearchContext context);

    Account_account_template convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template domain , Account_account_template po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template convert2Domain( Account_account_template po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template domain) ;

}
