package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_position_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_fiscal_position_templateOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_fiscal_position_templateClientModel;

/**
 * 实体[税科目调整模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_fiscal_position_templateServiceImpl implements IAccount_fiscal_position_templateService {

    @Autowired
    account_fiscal_position_templateOdooClient account_fiscal_position_templateOdooClient;


    @Override
    public boolean create(Account_fiscal_position_template et) {
        account_fiscal_position_templateClientModel clientModel = convert2Model(et,null);
		account_fiscal_position_templateOdooClient.create(clientModel);
        Account_fiscal_position_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_fiscal_position_template> list){
    }

    @Override
    public boolean update(Account_fiscal_position_template et) {
        account_fiscal_position_templateClientModel clientModel = convert2Model(et,null);
		account_fiscal_position_templateOdooClient.update(clientModel);
        Account_fiscal_position_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_fiscal_position_template> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_fiscal_position_templateClientModel clientModel = new account_fiscal_position_templateClientModel();
        clientModel.setId(id);
		account_fiscal_position_templateOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Account_fiscal_position_template get(Integer id) {
        account_fiscal_position_templateClientModel clientModel = new account_fiscal_position_templateClientModel();
        clientModel.setId(id);
		account_fiscal_position_templateOdooClient.get(clientModel);
        Account_fiscal_position_template et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_fiscal_position_template();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_fiscal_position_template> searchDefault(Account_fiscal_position_templateSearchContext context) {
        List<Account_fiscal_position_template> list = new ArrayList<Account_fiscal_position_template>();
        Page<account_fiscal_position_templateClientModel> clientModelList = account_fiscal_position_templateOdooClient.search(context);
        for(account_fiscal_position_templateClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_fiscal_position_template>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_fiscal_position_templateClientModel convert2Model(Account_fiscal_position_template domain , account_fiscal_position_templateClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_fiscal_position_templateClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("state_idsdirtyflag"))
                model.setState_ids(domain.getStateIds());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("auto_applydirtyflag"))
                model.setAuto_apply(domain.getAutoApply());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("account_idsdirtyflag"))
                model.setAccount_ids(domain.getAccountIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("zip_todirtyflag"))
                model.setZip_to(domain.getZipTo());
            if((Boolean) domain.getExtensionparams().get("zip_fromdirtyflag"))
                model.setZip_from(domain.getZipFrom());
            if((Boolean) domain.getExtensionparams().get("tax_idsdirtyflag"))
                model.setTax_ids(domain.getTaxIds());
            if((Boolean) domain.getExtensionparams().get("vat_requireddirtyflag"))
                model.setVat_required(domain.getVatRequired());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("chart_template_id_textdirtyflag"))
                model.setChart_template_id_text(domain.getChartTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("country_id_textdirtyflag"))
                model.setCountry_id_text(domain.getCountryIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("country_group_id_textdirtyflag"))
                model.setCountry_group_id_text(domain.getCountryGroupIdText());
            if((Boolean) domain.getExtensionparams().get("country_group_iddirtyflag"))
                model.setCountry_group_id(domain.getCountryGroupId());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("chart_template_iddirtyflag"))
                model.setChart_template_id(domain.getChartTemplateId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_fiscal_position_template convert2Domain( account_fiscal_position_templateClientModel model ,Account_fiscal_position_template domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_fiscal_position_template();
        }

        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getState_idsDirtyFlag())
            domain.setStateIds(model.getState_ids());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getAuto_applyDirtyFlag())
            domain.setAutoApply(model.getAuto_apply());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAccount_idsDirtyFlag())
            domain.setAccountIds(model.getAccount_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getZip_toDirtyFlag())
            domain.setZipTo(model.getZip_to());
        if(model.getZip_fromDirtyFlag())
            domain.setZipFrom(model.getZip_from());
        if(model.getTax_idsDirtyFlag())
            domain.setTaxIds(model.getTax_ids());
        if(model.getVat_requiredDirtyFlag())
            domain.setVatRequired(model.getVat_required());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getChart_template_id_textDirtyFlag())
            domain.setChartTemplateIdText(model.getChart_template_id_text());
        if(model.getCountry_id_textDirtyFlag())
            domain.setCountryIdText(model.getCountry_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCountry_group_id_textDirtyFlag())
            domain.setCountryGroupIdText(model.getCountry_group_id_text());
        if(model.getCountry_group_idDirtyFlag())
            domain.setCountryGroupId(model.getCountry_group_id());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getChart_template_idDirtyFlag())
            domain.setChartTemplateId(model.getChart_template_id());
        return domain ;
    }

}

    



