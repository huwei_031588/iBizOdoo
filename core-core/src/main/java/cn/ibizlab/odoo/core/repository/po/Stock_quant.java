package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantSearchContext;

/**
 * 实体 [即时库存] 存储模型
 */
public interface Stock_quant{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 预留数量
     */
    Double getReserved_quantity();

    void setReserved_quantity(Double reserved_quantity);

    /**
     * 获取 [预留数量]脏标记
     */
    boolean getReserved_quantityDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 数量
     */
    Double getQuantity();

    void setQuantity(Double quantity);

    /**
     * 获取 [数量]脏标记
     */
    boolean getQuantityDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 送货日期
     */
    Timestamp getIn_date();

    void setIn_date(Timestamp in_date);

    /**
     * 获取 [送货日期]脏标记
     */
    boolean getIn_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 位置
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [位置]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 所有者
     */
    String getOwner_id_text();

    void setOwner_id_text(String owner_id_text);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_id_textDirtyFlag();

    /**
     * 批次/序列号码
     */
    String getLot_id_text();

    void setLot_id_text(String lot_id_text);

    /**
     * 获取 [批次/序列号码]脏标记
     */
    boolean getLot_id_textDirtyFlag();

    /**
     * 产品模板
     */
    Integer getProduct_tmpl_id();

    void setProduct_tmpl_id(Integer product_tmpl_id);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_idDirtyFlag();

    /**
     * 包裹
     */
    String getPackage_id_text();

    void setPackage_id_text(String package_id_text);

    /**
     * 获取 [包裹]脏标记
     */
    boolean getPackage_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 所有者
     */
    Integer getOwner_id();

    void setOwner_id(Integer owner_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 位置
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [位置]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 包裹
     */
    Integer getPackage_id();

    void setPackage_id(Integer package_id);

    /**
     * 获取 [包裹]脏标记
     */
    boolean getPackage_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 批次/序列号码
     */
    Integer getLot_id();

    void setLot_id(Integer lot_id);

    /**
     * 获取 [批次/序列号码]脏标记
     */
    boolean getLot_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
