package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_ruleSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_ruleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_ruleOdooClient;
import cn.ibizlab.odoo.core.odoo_mro.clientmodel.mro_pm_ruleClientModel;

/**
 * 实体[Preventive Maintenance Rule] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_ruleServiceImpl implements IMro_pm_ruleService {

    @Autowired
    mro_pm_ruleOdooClient mro_pm_ruleOdooClient;


    @Override
    public boolean remove(Integer id) {
        mro_pm_ruleClientModel clientModel = new mro_pm_ruleClientModel();
        clientModel.setId(id);
		mro_pm_ruleOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mro_pm_rule et) {
        mro_pm_ruleClientModel clientModel = convert2Model(et,null);
		mro_pm_ruleOdooClient.create(clientModel);
        Mro_pm_rule rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_rule> list){
    }

    @Override
    public Mro_pm_rule get(Integer id) {
        mro_pm_ruleClientModel clientModel = new mro_pm_ruleClientModel();
        clientModel.setId(id);
		mro_pm_ruleOdooClient.get(clientModel);
        Mro_pm_rule et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mro_pm_rule();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mro_pm_rule et) {
        mro_pm_ruleClientModel clientModel = convert2Model(et,null);
		mro_pm_ruleOdooClient.update(clientModel);
        Mro_pm_rule rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mro_pm_rule> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_rule> searchDefault(Mro_pm_ruleSearchContext context) {
        List<Mro_pm_rule> list = new ArrayList<Mro_pm_rule>();
        Page<mro_pm_ruleClientModel> clientModelList = mro_pm_ruleOdooClient.search(context);
        for(mro_pm_ruleClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mro_pm_rule>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mro_pm_ruleClientModel convert2Model(Mro_pm_rule domain , mro_pm_ruleClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mro_pm_ruleClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("horizondirtyflag"))
                model.setHorizon(domain.getHorizon());
            if((Boolean) domain.getExtensionparams().get("pm_rules_line_idsdirtyflag"))
                model.setPm_rules_line_ids(domain.getPmRulesLineIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("parameter_id_textdirtyflag"))
                model.setParameter_id_text(domain.getParameterIdText());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("parameter_uomdirtyflag"))
                model.setParameter_uom(domain.getParameterUom());
            if((Boolean) domain.getExtensionparams().get("parameter_iddirtyflag"))
                model.setParameter_id(domain.getParameterId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mro_pm_rule convert2Domain( mro_pm_ruleClientModel model ,Mro_pm_rule domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mro_pm_rule();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getHorizonDirtyFlag())
            domain.setHorizon(model.getHorizon());
        if(model.getPm_rules_line_idsDirtyFlag())
            domain.setPmRulesLineIds(model.getPm_rules_line_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getParameter_id_textDirtyFlag())
            domain.setParameterIdText(model.getParameter_id_text());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getParameter_uomDirtyFlag())
            domain.setParameterUom(model.getParameter_uom());
        if(model.getParameter_idDirtyFlag())
            domain.setParameterId(model.getParameter_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



