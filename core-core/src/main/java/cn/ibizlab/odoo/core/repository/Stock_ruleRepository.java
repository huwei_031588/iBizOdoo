package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_rule;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_ruleSearchContext;

/**
 * 实体 [库存规则] 存储对象
 */
public interface Stock_ruleRepository extends Repository<Stock_rule> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_rule> searchDefault(Stock_ruleSearchContext context);

    Stock_rule convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule domain , Stock_rule po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule convert2Domain( Stock_rule po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule domain) ;

}
