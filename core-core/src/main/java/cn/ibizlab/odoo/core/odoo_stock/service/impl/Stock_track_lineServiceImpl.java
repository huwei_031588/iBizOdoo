package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_track_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_track_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_track_lineClientModel;

/**
 * 实体[库存追溯行] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_track_lineServiceImpl implements IStock_track_lineService {

    @Autowired
    stock_track_lineOdooClient stock_track_lineOdooClient;


    @Override
    public Stock_track_line get(Integer id) {
        stock_track_lineClientModel clientModel = new stock_track_lineClientModel();
        clientModel.setId(id);
		stock_track_lineOdooClient.get(clientModel);
        Stock_track_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_track_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Stock_track_line et) {
        stock_track_lineClientModel clientModel = convert2Model(et,null);
		stock_track_lineOdooClient.create(clientModel);
        Stock_track_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_track_line> list){
    }

    @Override
    public boolean remove(Integer id) {
        stock_track_lineClientModel clientModel = new stock_track_lineClientModel();
        clientModel.setId(id);
		stock_track_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Stock_track_line et) {
        stock_track_lineClientModel clientModel = convert2Model(et,null);
		stock_track_lineOdooClient.update(clientModel);
        Stock_track_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_track_line> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_track_line> searchDefault(Stock_track_lineSearchContext context) {
        List<Stock_track_line> list = new ArrayList<Stock_track_line>();
        Page<stock_track_lineClientModel> clientModelList = stock_track_lineOdooClient.search(context);
        for(stock_track_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_track_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_track_lineClientModel convert2Model(Stock_track_line domain , stock_track_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_track_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("trackingdirtyflag"))
                model.setTracking(domain.getTracking());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("wizard_iddirtyflag"))
                model.setWizard_id(domain.getWizardId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_track_line convert2Domain( stock_track_lineClientModel model ,Stock_track_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_track_line();
        }

        if(model.getTrackingDirtyFlag())
            domain.setTracking(model.getTracking());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getWizard_idDirtyFlag())
            domain.setWizardId(model.getWizard_id());
        return domain ;
    }

}

    



