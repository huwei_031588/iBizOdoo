package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_recruitment_degree;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_degreeSearchContext;

/**
 * 实体 [申请人学历] 存储对象
 */
public interface Hr_recruitment_degreeRepository extends Repository<Hr_recruitment_degree> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_recruitment_degree> searchDefault(Hr_recruitment_degreeSearchContext context);

    Hr_recruitment_degree convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_degree domain , Hr_recruitment_degree po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_degree convert2Domain( Hr_recruitment_degree po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_degree domain) ;

}
