package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_channel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channelSearchContext;

/**
 * 实体 [讨论频道] 存储对象
 */
public interface Mail_channelRepository extends Repository<Mail_channel> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_channel> searchDefault(Mail_channelSearchContext context);

    Mail_channel convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel domain , Mail_channel po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel convert2Domain( Mail_channel po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel domain) ;

}
