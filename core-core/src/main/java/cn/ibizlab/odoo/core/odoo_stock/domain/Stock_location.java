package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [库存位置] 对象
 */
@Data
public class Stock_location extends EntityClient implements Serializable {

    /**
     * 通道(X)
     */
    @JSONField(name = "posx")
    @JsonProperty("posx")
    private Integer posx;

    /**
     * 货架(Y)
     */
    @JSONField(name = "posy")
    @JsonProperty("posy")
    private Integer posy;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 条码
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 高度(Z)
     */
    @JSONField(name = "posz")
    @JsonProperty("posz")
    private Integer posz;

    /**
     * 完整的位置名称
     */
    @DEField(name = "complete_name")
    @JSONField(name = "complete_name")
    @JsonProperty("complete_name")
    private String completeName;

    /**
     * 是一个报废位置？
     */
    @DEField(name = "scrap_location")
    @JSONField(name = "scrap_location")
    @JsonProperty("scrap_location")
    private String scrapLocation;

    /**
     * 是一个退回位置？
     */
    @DEField(name = "return_location")
    @JSONField(name = "return_location")
    @JsonProperty("return_location")
    private String returnLocation;

    /**
     * 位置名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 父级路径
     */
    @DEField(name = "parent_path")
    @JSONField(name = "parent_path")
    @JsonProperty("parent_path")
    private String parentPath;

    /**
     * 即时库存
     */
    @JSONField(name = "quant_ids")
    @JsonProperty("quant_ids")
    private String quantIds;

    /**
     * 位置类型
     */
    @JSONField(name = "usage")
    @JsonProperty("usage")
    private String usage;

    /**
     * 额外的信息
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 包含
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 上级位置
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 库存计价科目（出向）
     */
    @JSONField(name = "valuation_out_account_id_text")
    @JsonProperty("valuation_out_account_id_text")
    private String valuationOutAccountIdText;

    /**
     * 所有者
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 下架策略
     */
    @JSONField(name = "removal_strategy_id_text")
    @JsonProperty("removal_strategy_id_text")
    private String removalStrategyIdText;

    /**
     * 上架策略
     */
    @JSONField(name = "putaway_strategy_id_text")
    @JsonProperty("putaway_strategy_id_text")
    private String putawayStrategyIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 库存计价科目（入向）
     */
    @JSONField(name = "valuation_in_account_id_text")
    @JsonProperty("valuation_in_account_id_text")
    private String valuationInAccountIdText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 库存计价科目（入向）
     */
    @DEField(name = "valuation_in_account_id")
    @JSONField(name = "valuation_in_account_id")
    @JsonProperty("valuation_in_account_id")
    private Integer valuationInAccountId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 上级位置
     */
    @DEField(name = "location_id")
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 库存计价科目（出向）
     */
    @DEField(name = "valuation_out_account_id")
    @JSONField(name = "valuation_out_account_id")
    @JsonProperty("valuation_out_account_id")
    private Integer valuationOutAccountId;

    /**
     * 所有者
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 上架策略
     */
    @DEField(name = "putaway_strategy_id")
    @JSONField(name = "putaway_strategy_id")
    @JsonProperty("putaway_strategy_id")
    private Integer putawayStrategyId;

    /**
     * 下架策略
     */
    @DEField(name = "removal_strategy_id")
    @JSONField(name = "removal_strategy_id")
    @JsonProperty("removal_strategy_id")
    private Integer removalStrategyId;


    /**
     * 
     */
    @JSONField(name = "odoovaluationinaccount")
    @JsonProperty("odoovaluationinaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooValuationInAccount;

    /**
     * 
     */
    @JSONField(name = "odoovaluationoutaccount")
    @JsonProperty("odoovaluationoutaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooValuationOutAccount;

    /**
     * 
     */
    @JSONField(name = "odooputawaystrategy")
    @JsonProperty("odooputawaystrategy")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway odooPutawayStrategy;

    /**
     * 
     */
    @JSONField(name = "odooremovalstrategy")
    @JsonProperty("odooremovalstrategy")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_removal odooRemovalStrategy;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoolocation")
    @JsonProperty("odoolocation")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooLocation;




    /**
     * 设置 [通道(X)]
     */
    public void setPosx(Integer posx){
        this.posx = posx ;
        this.modify("posx",posx);
    }
    /**
     * 设置 [货架(Y)]
     */
    public void setPosy(Integer posy){
        this.posy = posy ;
        this.modify("posy",posy);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [条码]
     */
    public void setBarcode(String barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }
    /**
     * 设置 [高度(Z)]
     */
    public void setPosz(Integer posz){
        this.posz = posz ;
        this.modify("posz",posz);
    }
    /**
     * 设置 [完整的位置名称]
     */
    public void setCompleteName(String completeName){
        this.completeName = completeName ;
        this.modify("complete_name",completeName);
    }
    /**
     * 设置 [是一个报废位置？]
     */
    public void setScrapLocation(String scrapLocation){
        this.scrapLocation = scrapLocation ;
        this.modify("scrap_location",scrapLocation);
    }
    /**
     * 设置 [是一个退回位置？]
     */
    public void setReturnLocation(String returnLocation){
        this.returnLocation = returnLocation ;
        this.modify("return_location",returnLocation);
    }
    /**
     * 设置 [位置名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [父级路径]
     */
    public void setParentPath(String parentPath){
        this.parentPath = parentPath ;
        this.modify("parent_path",parentPath);
    }
    /**
     * 设置 [位置类型]
     */
    public void setUsage(String usage){
        this.usage = usage ;
        this.modify("usage",usage);
    }
    /**
     * 设置 [额外的信息]
     */
    public void setComment(String comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }
    /**
     * 设置 [库存计价科目（入向）]
     */
    public void setValuationInAccountId(Integer valuationInAccountId){
        this.valuationInAccountId = valuationInAccountId ;
        this.modify("valuation_in_account_id",valuationInAccountId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [上级位置]
     */
    public void setLocationId(Integer locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }
    /**
     * 设置 [库存计价科目（出向）]
     */
    public void setValuationOutAccountId(Integer valuationOutAccountId){
        this.valuationOutAccountId = valuationOutAccountId ;
        this.modify("valuation_out_account_id",valuationOutAccountId);
    }
    /**
     * 设置 [所有者]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [上架策略]
     */
    public void setPutawayStrategyId(Integer putawayStrategyId){
        this.putawayStrategyId = putawayStrategyId ;
        this.modify("putaway_strategy_id",putawayStrategyId);
    }
    /**
     * 设置 [下架策略]
     */
    public void setRemovalStrategyId(Integer removalStrategyId){
        this.removalStrategyId = removalStrategyId ;
        this.modify("removal_strategy_id",removalStrategyId);
    }

}


