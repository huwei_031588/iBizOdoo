package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_ratingSearchContext;

/**
 * 实体 [评级] 存储模型
 */
public interface Rating_rating{

    /**
     * 备注
     */
    String getFeedback();

    void setFeedback(String feedback);

    /**
     * 获取 [备注]脏标记
     */
    boolean getFeedbackDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 评级数值
     */
    Double getRating();

    void setRating(Double rating);

    /**
     * 获取 [评级数值]脏标记
     */
    boolean getRatingDirtyFlag();

    /**
     * 评级
     */
    String getRating_text();

    void setRating_text(String rating_text);

    /**
     * 获取 [评级]脏标记
     */
    boolean getRating_textDirtyFlag();

    /**
     * 父级文档名称
     */
    String getParent_res_name();

    void setParent_res_name(String parent_res_name);

    /**
     * 获取 [父级文档名称]脏标记
     */
    boolean getParent_res_nameDirtyFlag();

    /**
     * 父级文档
     */
    Integer getParent_res_id();

    void setParent_res_id(Integer parent_res_id);

    /**
     * 获取 [父级文档]脏标记
     */
    boolean getParent_res_idDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 资源名称
     */
    String getRes_name();

    void setRes_name(String res_name);

    /**
     * 获取 [资源名称]脏标记
     */
    boolean getRes_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 父级相关文档模型
     */
    Integer getParent_res_model_id();

    void setParent_res_model_id(Integer parent_res_model_id);

    /**
     * 获取 [父级相关文档模型]脏标记
     */
    boolean getParent_res_model_idDirtyFlag();

    /**
     * 文档
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [文档]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 已填写的评级
     */
    String getConsumed();

    void setConsumed(String consumed);

    /**
     * 获取 [已填写的评级]脏标记
     */
    boolean getConsumedDirtyFlag();

    /**
     * 图像
     */
    byte[] getRating_image();

    void setRating_image(byte[] rating_image);

    /**
     * 获取 [图像]脏标记
     */
    boolean getRating_imageDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 相关的文档模型
     */
    Integer getRes_model_id();

    void setRes_model_id(Integer res_model_id);

    /**
     * 获取 [相关的文档模型]脏标记
     */
    boolean getRes_model_idDirtyFlag();

    /**
     * 文档模型
     */
    String getRes_model();

    void setRes_model(String res_model);

    /**
     * 获取 [文档模型]脏标记
     */
    boolean getRes_modelDirtyFlag();

    /**
     * 安全令牌
     */
    String getAccess_token();

    void setAccess_token(String access_token);

    /**
     * 获取 [安全令牌]脏标记
     */
    boolean getAccess_tokenDirtyFlag();

    /**
     * 父级文档模型
     */
    String getParent_res_model();

    void setParent_res_model(String parent_res_model);

    /**
     * 获取 [父级文档模型]脏标记
     */
    boolean getParent_res_modelDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 客户
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 评级人员
     */
    String getRated_partner_id_text();

    void setRated_partner_id_text(String rated_partner_id_text);

    /**
     * 获取 [评级人员]脏标记
     */
    boolean getRated_partner_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 已发布
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 链接信息
     */
    Integer getMessage_id();

    void setMessage_id(Integer message_id);

    /**
     * 获取 [链接信息]脏标记
     */
    boolean getMessage_idDirtyFlag();

    /**
     * 客户
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 评级人员
     */
    Integer getRated_partner_id();

    void setRated_partner_id(Integer rated_partner_id);

    /**
     * 获取 [评级人员]脏标记
     */
    boolean getRated_partner_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
