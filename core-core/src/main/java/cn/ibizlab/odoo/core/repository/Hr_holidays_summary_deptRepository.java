package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_holidays_summary_dept;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_deptSearchContext;

/**
 * 实体 [按部门的休假摘要报表] 存储对象
 */
public interface Hr_holidays_summary_deptRepository extends Repository<Hr_holidays_summary_dept> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_holidays_summary_dept> searchDefault(Hr_holidays_summary_deptSearchContext context);

    Hr_holidays_summary_dept convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_dept domain , Hr_holidays_summary_dept po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_dept convert2Domain( Hr_holidays_summary_dept po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_dept domain) ;

}
