package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_expense;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expenseSearchContext;

/**
 * 实体 [费用] 存储对象
 */
public interface Hr_expenseRepository extends Repository<Hr_expense> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_expense> searchDefault(Hr_expenseSearchContext context);

    Hr_expense convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense domain , Hr_expense po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense convert2Domain( Hr_expense po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense domain) ;

}
