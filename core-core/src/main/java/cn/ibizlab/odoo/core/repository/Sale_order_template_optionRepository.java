package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Sale_order_template_option;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_template_optionSearchContext;

/**
 * 实体 [报价模板选项] 存储对象
 */
public interface Sale_order_template_optionRepository extends Repository<Sale_order_template_option> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Sale_order_template_option> searchDefault(Sale_order_template_optionSearchContext context);

    Sale_order_template_option convert2PO(cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_option domain , Sale_order_template_option po) ;

    cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_option convert2Domain( Sale_order_template_option po ,cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_option domain) ;

}
