package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_assignation_logSearchContext;

/**
 * 实体 [交通工具驾驶历史] 存储模型
 */
public interface Fleet_vehicle_assignation_log{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_start();

    void setDate_start(Timestamp date_start);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_startDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 终止日期
     */
    Timestamp getDate_end();

    void setDate_end(Timestamp date_end);

    /**
     * 获取 [终止日期]脏标记
     */
    boolean getDate_endDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 车辆
     */
    String getVehicle_id_text();

    void setVehicle_id_text(String vehicle_id_text);

    /**
     * 获取 [车辆]脏标记
     */
    boolean getVehicle_id_textDirtyFlag();

    /**
     * 驾驶员
     */
    String getDriver_id_text();

    void setDriver_id_text(String driver_id_text);

    /**
     * 获取 [驾驶员]脏标记
     */
    boolean getDriver_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 车辆
     */
    Integer getVehicle_id();

    void setVehicle_id(Integer vehicle_id);

    /**
     * 获取 [车辆]脏标记
     */
    boolean getVehicle_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 驾驶员
     */
    Integer getDriver_id();

    void setDriver_id(Integer driver_id);

    /**
     * 获取 [驾驶员]脏标记
     */
    boolean getDriver_idDirtyFlag();

}
