package cn.ibizlab.odoo.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [记录生产明细] 对象
 */
@Data
public class Mrp_product_produce_line extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 待消耗
     */
    @DEField(name = "qty_to_consume")
    @JSONField(name = "qty_to_consume")
    @JsonProperty("qty_to_consume")
    private Double qtyToConsume;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 消耗的
     */
    @DEField(name = "qty_done")
    @JSONField(name = "qty_done")
    @JsonProperty("qty_done")
    private Double qtyDone;

    /**
     * 已预留
     */
    @DEField(name = "qty_reserved")
    @JSONField(name = "qty_reserved")
    @JsonProperty("qty_reserved")
    private Double qtyReserved;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 批次/序列号码
     */
    @JSONField(name = "lot_id_text")
    @JsonProperty("lot_id_text")
    private String lotIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 计量单位
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 追踪
     */
    @JSONField(name = "product_tracking")
    @JsonProperty("product_tracking")
    private String productTracking;

    /**
     * 分录
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;

    /**
     * 分录
     */
    @DEField(name = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Integer moveId;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 批次/序列号码
     */
    @DEField(name = "lot_id")
    @JSONField(name = "lot_id")
    @JsonProperty("lot_id")
    private Integer lotId;

    /**
     * 产品生产
     */
    @DEField(name = "product_produce_id")
    @JSONField(name = "product_produce_id")
    @JsonProperty("product_produce_id")
    private Integer productProduceId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 计量单位
     */
    @DEField(name = "product_uom_id")
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odooproductproduce")
    @JsonProperty("odooproductproduce")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce odooProductProduce;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoomove")
    @JsonProperty("odoomove")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move odooMove;

    /**
     * 
     */
    @JSONField(name = "odoolot")
    @JsonProperty("odoolot")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot odooLot;

    /**
     * 
     */
    @JSONField(name = "odooproductuom")
    @JsonProperty("odooproductuom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooProductUom;




    /**
     * 设置 [待消耗]
     */
    public void setQtyToConsume(Double qtyToConsume){
        this.qtyToConsume = qtyToConsume ;
        this.modify("qty_to_consume",qtyToConsume);
    }
    /**
     * 设置 [消耗的]
     */
    public void setQtyDone(Double qtyDone){
        this.qtyDone = qtyDone ;
        this.modify("qty_done",qtyDone);
    }
    /**
     * 设置 [已预留]
     */
    public void setQtyReserved(Double qtyReserved){
        this.qtyReserved = qtyReserved ;
        this.modify("qty_reserved",qtyReserved);
    }
    /**
     * 设置 [分录]
     */
    public void setMoveId(Integer moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [批次/序列号码]
     */
    public void setLotId(Integer lotId){
        this.lotId = lotId ;
        this.modify("lot_id",lotId);
    }
    /**
     * 设置 [产品生产]
     */
    public void setProductProduceId(Integer productProduceId){
        this.productProduceId = productProduceId ;
        this.modify("product_produce_id",productProduceId);
    }
    /**
     * 设置 [计量单位]
     */
    public void setProductUomId(Integer productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

}


