package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.gamification_goal_wizard;

/**
 * 实体[gamification_goal_wizard] 服务对象接口
 */
public interface gamification_goal_wizardRepository{


    public gamification_goal_wizard createPO() ;
        public void removeBatch(String id);

        public void update(gamification_goal_wizard gamification_goal_wizard);

        public void create(gamification_goal_wizard gamification_goal_wizard);

        public void get(String id);

        public void createBatch(gamification_goal_wizard gamification_goal_wizard);

        public void updateBatch(gamification_goal_wizard gamification_goal_wizard);

        public List<gamification_goal_wizard> search();

        public void remove(String id);


}
