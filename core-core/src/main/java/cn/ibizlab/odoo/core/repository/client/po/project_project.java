package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [project_project] 对象
 */
public interface project_project {

    public String getAccess_token();

    public void setAccess_token(String access_token);

    public String getAccess_url();

    public void setAccess_url(String access_url);

    public String getAccess_warning();

    public void setAccess_warning(String access_warning);

    public String getActive();

    public void setActive(String active);

    public String getAlias_contact();

    public void setAlias_contact(String alias_contact);

    public String getAlias_defaults();

    public void setAlias_defaults(String alias_defaults);

    public String getAlias_domain();

    public void setAlias_domain(String alias_domain);

    public Integer getAlias_force_thread_id();

    public void setAlias_force_thread_id(Integer alias_force_thread_id);

    public Integer getAlias_id();

    public void setAlias_id(Integer alias_id);

    public String getAlias_name();

    public void setAlias_name(String alias_name);

    public Integer getAlias_parent_thread_id();

    public void setAlias_parent_thread_id(Integer alias_parent_thread_id);

    public Integer getAlias_user_id();

    public void setAlias_user_id(Integer alias_user_id);

    public String getAlias_user_id_text();

    public void setAlias_user_id_text(String alias_user_id_text);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getCurrency_id();

    public void setCurrency_id(Integer currency_id);

    public String getCurrency_id_text();

    public void setCurrency_id_text(String currency_id_text);

    public Timestamp getDate();

    public void setDate(Timestamp date);

    public Timestamp getDate_start();

    public void setDate_start(Timestamp date_start);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getDoc_count();

    public void setDoc_count(Integer doc_count);

    public String getFavorite_user_ids();

    public void setFavorite_user_ids(String favorite_user_ids);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_favorite();

    public void setIs_favorite(String is_favorite);

    public String getLabel_tasks();

    public void setLabel_tasks(String label_tasks);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public Integer getPercentage_satisfaction_project();

    public void setPercentage_satisfaction_project(Integer percentage_satisfaction_project);

    public Integer getPercentage_satisfaction_task();

    public void setPercentage_satisfaction_task(Integer percentage_satisfaction_task);

    public String getPortal_show_rating();

    public void setPortal_show_rating(String portal_show_rating);

    public String getPrivacy_visibility();

    public void setPrivacy_visibility(String privacy_visibility);

    public Timestamp getRating_request_deadline();

    public void setRating_request_deadline(Timestamp rating_request_deadline);

    public String getRating_status();

    public void setRating_status(String rating_status);

    public String getRating_status_period();

    public void setRating_status_period(String rating_status_period);

    public Integer getResource_calendar_id();

    public void setResource_calendar_id(Integer resource_calendar_id);

    public String getResource_calendar_id_text();

    public void setResource_calendar_id_text(String resource_calendar_id_text);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public Integer getSubtask_project_id();

    public void setSubtask_project_id(Integer subtask_project_id);

    public String getSubtask_project_id_text();

    public void setSubtask_project_id_text(String subtask_project_id_text);

    public String getTasks();

    public void setTasks(String tasks);

    public Integer getTask_count();

    public void setTask_count(Integer task_count);

    public String getTask_ids();

    public void setTask_ids(String task_ids);

    public String getType_ids();

    public void setType_ids(String type_ids);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
