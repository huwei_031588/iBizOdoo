package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_department] 对象
 */
public interface hr_department {

    public Integer getAbsence_of_today();

    public void setAbsence_of_today(Integer absence_of_today);

    public String getActive();

    public void setActive(String active);

    public Integer getAllocation_to_approve_count();

    public void setAllocation_to_approve_count(Integer allocation_to_approve_count);

    public String getChild_ids();

    public void setChild_ids(String child_ids);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public String getComplete_name();

    public void setComplete_name(String complete_name);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getExpected_employee();

    public void setExpected_employee(Integer expected_employee);

    public Integer getExpense_sheets_to_approve_count();

    public void setExpense_sheets_to_approve_count(Integer expense_sheets_to_approve_count);

    public Integer getId();

    public void setId(Integer id);

    public String getJobs_ids();

    public void setJobs_ids(String jobs_ids);

    public Integer getLeave_to_approve_count();

    public void setLeave_to_approve_count(Integer leave_to_approve_count);

    public Integer getManager_id();

    public void setManager_id(Integer manager_id);

    public String getManager_id_text();

    public void setManager_id_text(String manager_id_text);

    public String getMember_ids();

    public void setMember_ids(String member_ids);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public Integer getNew_applicant_count();

    public void setNew_applicant_count(Integer new_applicant_count);

    public Integer getNew_hired_employee();

    public void setNew_hired_employee(Integer new_hired_employee);

    public String getNote();

    public void setNote(String note);

    public Integer getParent_id();

    public void setParent_id(Integer parent_id);

    public String getParent_id_text();

    public void setParent_id_text(String parent_id_text);

    public Integer getTotal_employee();

    public void setTotal_employee(Integer total_employee);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
