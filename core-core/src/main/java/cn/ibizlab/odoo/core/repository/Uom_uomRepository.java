package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Uom_uom;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_uomSearchContext;

/**
 * 实体 [产品计量单位] 存储对象
 */
public interface Uom_uomRepository extends Repository<Uom_uom> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Uom_uom> searchDefault(Uom_uomSearchContext context);

    Uom_uom convert2PO(cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom domain , Uom_uom po) ;

    cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom convert2Domain( Uom_uom po ,cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom domain) ;

}
