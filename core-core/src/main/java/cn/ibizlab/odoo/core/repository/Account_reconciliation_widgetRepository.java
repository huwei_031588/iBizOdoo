package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_reconciliation_widget;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;

/**
 * 实体 [会计核销挂账] 存储对象
 */
public interface Account_reconciliation_widgetRepository extends Repository<Account_reconciliation_widget> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_reconciliation_widget> searchDefault(Account_reconciliation_widgetSearchContext context);

    Account_reconciliation_widget convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget domain , Account_reconciliation_widget po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget convert2Domain( Account_reconciliation_widget po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget domain) ;

}
