package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_departmentSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_departmentService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_departmentOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_departmentClientModel;

/**
 * 实体[HR 部门] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_departmentServiceImpl implements IHr_departmentService {

    @Autowired
    hr_departmentOdooClient hr_departmentOdooClient;


    @Override
    public boolean update(Hr_department et) {
        hr_departmentClientModel clientModel = convert2Model(et,null);
		hr_departmentOdooClient.update(clientModel);
        Hr_department rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_department> list){
    }

    @Override
    public boolean create(Hr_department et) {
        hr_departmentClientModel clientModel = convert2Model(et,null);
		hr_departmentOdooClient.create(clientModel);
        Hr_department rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_department> list){
    }

    @Override
    public boolean remove(Integer id) {
        hr_departmentClientModel clientModel = new hr_departmentClientModel();
        clientModel.setId(id);
		hr_departmentOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Hr_department get(Integer id) {
        hr_departmentClientModel clientModel = new hr_departmentClientModel();
        clientModel.setId(id);
		hr_departmentOdooClient.get(clientModel);
        Hr_department et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_department();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_department> searchDefault(Hr_departmentSearchContext context) {
        List<Hr_department> list = new ArrayList<Hr_department>();
        Page<hr_departmentClientModel> clientModelList = hr_departmentOdooClient.search(context);
        for(hr_departmentClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_department>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_departmentClientModel convert2Model(Hr_department domain , hr_departmentClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_departmentClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("absence_of_todaydirtyflag"))
                model.setAbsence_of_today(domain.getAbsenceOfToday());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("leave_to_approve_countdirtyflag"))
                model.setLeave_to_approve_count(domain.getLeaveToApproveCount());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("expense_sheets_to_approve_countdirtyflag"))
                model.setExpense_sheets_to_approve_count(domain.getExpenseSheetsToApproveCount());
            if((Boolean) domain.getExtensionparams().get("allocation_to_approve_countdirtyflag"))
                model.setAllocation_to_approve_count(domain.getAllocationToApproveCount());
            if((Boolean) domain.getExtensionparams().get("new_hired_employeedirtyflag"))
                model.setNew_hired_employee(domain.getNewHiredEmployee());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("expected_employeedirtyflag"))
                model.setExpected_employee(domain.getExpectedEmployee());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("total_employeedirtyflag"))
                model.setTotal_employee(domain.getTotalEmployee());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("jobs_idsdirtyflag"))
                model.setJobs_ids(domain.getJobsIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("new_applicant_countdirtyflag"))
                model.setNew_applicant_count(domain.getNewApplicantCount());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("complete_namedirtyflag"))
                model.setComplete_name(domain.getCompleteName());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("member_idsdirtyflag"))
                model.setMember_ids(domain.getMemberIds());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("manager_id_textdirtyflag"))
                model.setManager_id_text(domain.getManagerIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("manager_iddirtyflag"))
                model.setManager_id(domain.getManagerId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_department convert2Domain( hr_departmentClientModel model ,Hr_department domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_department();
        }

        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getAbsence_of_todayDirtyFlag())
            domain.setAbsenceOfToday(model.getAbsence_of_today());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getLeave_to_approve_countDirtyFlag())
            domain.setLeaveToApproveCount(model.getLeave_to_approve_count());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getExpense_sheets_to_approve_countDirtyFlag())
            domain.setExpenseSheetsToApproveCount(model.getExpense_sheets_to_approve_count());
        if(model.getAllocation_to_approve_countDirtyFlag())
            domain.setAllocationToApproveCount(model.getAllocation_to_approve_count());
        if(model.getNew_hired_employeeDirtyFlag())
            domain.setNewHiredEmployee(model.getNew_hired_employee());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getExpected_employeeDirtyFlag())
            domain.setExpectedEmployee(model.getExpected_employee());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getTotal_employeeDirtyFlag())
            domain.setTotalEmployee(model.getTotal_employee());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getJobs_idsDirtyFlag())
            domain.setJobsIds(model.getJobs_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getNew_applicant_countDirtyFlag())
            domain.setNewApplicantCount(model.getNew_applicant_count());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getComplete_nameDirtyFlag())
            domain.setCompleteName(model.getComplete_name());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMember_idsDirtyFlag())
            domain.setMemberIds(model.getMember_ids());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getManager_id_textDirtyFlag())
            domain.setManagerIdText(model.getManager_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getManager_idDirtyFlag())
            domain.setManagerId(model.getManager_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        return domain ;
    }

}

    



