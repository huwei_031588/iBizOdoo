package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_contract_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contract_typeSearchContext;

/**
 * 实体 [合同类型] 存储对象
 */
public interface Hr_contract_typeRepository extends Repository<Hr_contract_type> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_contract_type> searchDefault(Hr_contract_typeSearchContext context);

    Hr_contract_type convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract_type domain , Hr_contract_type po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract_type convert2Domain( Hr_contract_type po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract_type domain) ;

}
