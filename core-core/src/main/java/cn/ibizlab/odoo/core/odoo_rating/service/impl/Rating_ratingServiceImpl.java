package cn.ibizlab.odoo.core.odoo_rating.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_ratingSearchContext;
import cn.ibizlab.odoo.core.odoo_rating.service.IRating_ratingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_rating.client.rating_ratingOdooClient;
import cn.ibizlab.odoo.core.odoo_rating.clientmodel.rating_ratingClientModel;

/**
 * 实体[评级] 服务对象接口实现
 */
@Slf4j
@Service
public class Rating_ratingServiceImpl implements IRating_ratingService {

    @Autowired
    rating_ratingOdooClient rating_ratingOdooClient;


    @Override
    public boolean update(Rating_rating et) {
        rating_ratingClientModel clientModel = convert2Model(et,null);
		rating_ratingOdooClient.update(clientModel);
        Rating_rating rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Rating_rating> list){
    }

    @Override
    public boolean create(Rating_rating et) {
        rating_ratingClientModel clientModel = convert2Model(et,null);
		rating_ratingOdooClient.create(clientModel);
        Rating_rating rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Rating_rating> list){
    }

    @Override
    public Rating_rating get(Integer id) {
        rating_ratingClientModel clientModel = new rating_ratingClientModel();
        clientModel.setId(id);
		rating_ratingOdooClient.get(clientModel);
        Rating_rating et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Rating_rating();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        rating_ratingClientModel clientModel = new rating_ratingClientModel();
        clientModel.setId(id);
		rating_ratingOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Rating_rating> searchDefault(Rating_ratingSearchContext context) {
        List<Rating_rating> list = new ArrayList<Rating_rating>();
        Page<rating_ratingClientModel> clientModelList = rating_ratingOdooClient.search(context);
        for(rating_ratingClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Rating_rating>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public rating_ratingClientModel convert2Model(Rating_rating domain , rating_ratingClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new rating_ratingClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("feedbackdirtyflag"))
                model.setFeedback(domain.getFeedback());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("ratingdirtyflag"))
                model.setRating(domain.getRating());
            if((Boolean) domain.getExtensionparams().get("rating_textdirtyflag"))
                model.setRating_text(domain.getRatingText());
            if((Boolean) domain.getExtensionparams().get("parent_res_namedirtyflag"))
                model.setParent_res_name(domain.getParentResName());
            if((Boolean) domain.getExtensionparams().get("parent_res_iddirtyflag"))
                model.setParent_res_id(domain.getParentResId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("res_namedirtyflag"))
                model.setRes_name(domain.getResName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("parent_res_model_iddirtyflag"))
                model.setParent_res_model_id(domain.getParentResModelId());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("consumeddirtyflag"))
                model.setConsumed(domain.getConsumed());
            if((Boolean) domain.getExtensionparams().get("rating_imagedirtyflag"))
                model.setRating_image(domain.getRatingImage());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("res_model_iddirtyflag"))
                model.setRes_model_id(domain.getResModelId());
            if((Boolean) domain.getExtensionparams().get("res_modeldirtyflag"))
                model.setRes_model(domain.getResModel());
            if((Boolean) domain.getExtensionparams().get("access_tokendirtyflag"))
                model.setAccess_token(domain.getAccessToken());
            if((Boolean) domain.getExtensionparams().get("parent_res_modeldirtyflag"))
                model.setParent_res_model(domain.getParentResModel());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("rated_partner_id_textdirtyflag"))
                model.setRated_partner_id_text(domain.getRatedPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("message_iddirtyflag"))
                model.setMessage_id(domain.getMessageId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("rated_partner_iddirtyflag"))
                model.setRated_partner_id(domain.getRatedPartnerId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Rating_rating convert2Domain( rating_ratingClientModel model ,Rating_rating domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Rating_rating();
        }

        if(model.getFeedbackDirtyFlag())
            domain.setFeedback(model.getFeedback());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getRatingDirtyFlag())
            domain.setRating(model.getRating());
        if(model.getRating_textDirtyFlag())
            domain.setRatingText(model.getRating_text());
        if(model.getParent_res_nameDirtyFlag())
            domain.setParentResName(model.getParent_res_name());
        if(model.getParent_res_idDirtyFlag())
            domain.setParentResId(model.getParent_res_id());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getRes_nameDirtyFlag())
            domain.setResName(model.getRes_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getParent_res_model_idDirtyFlag())
            domain.setParentResModelId(model.getParent_res_model_id());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getConsumedDirtyFlag())
            domain.setConsumed(model.getConsumed());
        if(model.getRating_imageDirtyFlag())
            domain.setRatingImage(model.getRating_image());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getRes_model_idDirtyFlag())
            domain.setResModelId(model.getRes_model_id());
        if(model.getRes_modelDirtyFlag())
            domain.setResModel(model.getRes_model());
        if(model.getAccess_tokenDirtyFlag())
            domain.setAccessToken(model.getAccess_token());
        if(model.getParent_res_modelDirtyFlag())
            domain.setParentResModel(model.getParent_res_model());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getRated_partner_id_textDirtyFlag())
            domain.setRatedPartnerIdText(model.getRated_partner_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMessage_idDirtyFlag())
            domain.setMessageId(model.getMessage_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getRated_partner_idDirtyFlag())
            domain.setRatedPartnerId(model.getRated_partner_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



