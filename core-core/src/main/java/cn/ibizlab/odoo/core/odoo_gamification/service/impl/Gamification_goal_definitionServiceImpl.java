package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_definition;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_goal_definitionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_goal_definitionOdooClient;
import cn.ibizlab.odoo.core.odoo_gamification.clientmodel.gamification_goal_definitionClientModel;

/**
 * 实体[游戏化目标定义] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_goal_definitionServiceImpl implements IGamification_goal_definitionService {

    @Autowired
    gamification_goal_definitionOdooClient gamification_goal_definitionOdooClient;


    @Override
    public Gamification_goal_definition get(Integer id) {
        gamification_goal_definitionClientModel clientModel = new gamification_goal_definitionClientModel();
        clientModel.setId(id);
		gamification_goal_definitionOdooClient.get(clientModel);
        Gamification_goal_definition et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Gamification_goal_definition();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Gamification_goal_definition et) {
        gamification_goal_definitionClientModel clientModel = convert2Model(et,null);
		gamification_goal_definitionOdooClient.update(clientModel);
        Gamification_goal_definition rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Gamification_goal_definition> list){
    }

    @Override
    public boolean create(Gamification_goal_definition et) {
        gamification_goal_definitionClientModel clientModel = convert2Model(et,null);
		gamification_goal_definitionOdooClient.create(clientModel);
        Gamification_goal_definition rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_goal_definition> list){
    }

    @Override
    public boolean remove(Integer id) {
        gamification_goal_definitionClientModel clientModel = new gamification_goal_definitionClientModel();
        clientModel.setId(id);
		gamification_goal_definitionOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_goal_definition> searchDefault(Gamification_goal_definitionSearchContext context) {
        List<Gamification_goal_definition> list = new ArrayList<Gamification_goal_definition>();
        Page<gamification_goal_definitionClientModel> clientModelList = gamification_goal_definitionOdooClient.search(context);
        for(gamification_goal_definitionClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Gamification_goal_definition>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public gamification_goal_definitionClientModel convert2Model(Gamification_goal_definition domain , gamification_goal_definitionClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new gamification_goal_definitionClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("suffixdirtyflag"))
                model.setSuffix(domain.getSuffix());
            if((Boolean) domain.getExtensionparams().get("batch_distinctive_fielddirtyflag"))
                model.setBatch_distinctive_field(domain.getBatchDistinctiveField());
            if((Boolean) domain.getExtensionparams().get("monetarydirtyflag"))
                model.setMonetary(domain.getMonetary());
            if((Boolean) domain.getExtensionparams().get("conditiondirtyflag"))
                model.setCondition(domain.getCondition());
            if((Boolean) domain.getExtensionparams().get("compute_codedirtyflag"))
                model.setCompute_code(domain.getComputeCode());
            if((Boolean) domain.getExtensionparams().get("computation_modedirtyflag"))
                model.setComputation_mode(domain.getComputationMode());
            if((Boolean) domain.getExtensionparams().get("action_iddirtyflag"))
                model.setAction_id(domain.getActionId());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("domaindirtyflag"))
                model.setDomain(domain.getDomain());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("field_iddirtyflag"))
                model.setField_id(domain.getFieldId());
            if((Boolean) domain.getExtensionparams().get("res_id_fielddirtyflag"))
                model.setRes_id_field(domain.getResIdField());
            if((Boolean) domain.getExtensionparams().get("batch_modedirtyflag"))
                model.setBatch_mode(domain.getBatchMode());
            if((Boolean) domain.getExtensionparams().get("model_iddirtyflag"))
                model.setModel_id(domain.getModelId());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("field_date_iddirtyflag"))
                model.setField_date_id(domain.getFieldDateId());
            if((Boolean) domain.getExtensionparams().get("display_modedirtyflag"))
                model.setDisplay_mode(domain.getDisplayMode());
            if((Boolean) domain.getExtensionparams().get("full_suffixdirtyflag"))
                model.setFull_suffix(domain.getFullSuffix());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("batch_user_expressiondirtyflag"))
                model.setBatch_user_expression(domain.getBatchUserExpression());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Gamification_goal_definition convert2Domain( gamification_goal_definitionClientModel model ,Gamification_goal_definition domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Gamification_goal_definition();
        }

        if(model.getSuffixDirtyFlag())
            domain.setSuffix(model.getSuffix());
        if(model.getBatch_distinctive_fieldDirtyFlag())
            domain.setBatchDistinctiveField(model.getBatch_distinctive_field());
        if(model.getMonetaryDirtyFlag())
            domain.setMonetary(model.getMonetary());
        if(model.getConditionDirtyFlag())
            domain.setCondition(model.getCondition());
        if(model.getCompute_codeDirtyFlag())
            domain.setComputeCode(model.getCompute_code());
        if(model.getComputation_modeDirtyFlag())
            domain.setComputationMode(model.getComputation_mode());
        if(model.getAction_idDirtyFlag())
            domain.setActionId(model.getAction_id());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDomainDirtyFlag())
            domain.setDomain(model.getDomain());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getField_idDirtyFlag())
            domain.setFieldId(model.getField_id());
        if(model.getRes_id_fieldDirtyFlag())
            domain.setResIdField(model.getRes_id_field());
        if(model.getBatch_modeDirtyFlag())
            domain.setBatchMode(model.getBatch_mode());
        if(model.getModel_idDirtyFlag())
            domain.setModelId(model.getModel_id());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getField_date_idDirtyFlag())
            domain.setFieldDateId(model.getField_date_id());
        if(model.getDisplay_modeDirtyFlag())
            domain.setDisplayMode(model.getDisplay_mode());
        if(model.getFull_suffixDirtyFlag())
            domain.setFullSuffix(model.getFull_suffix());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getBatch_user_expressionDirtyFlag())
            domain.setBatchUserExpression(model.getBatch_user_expression());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



