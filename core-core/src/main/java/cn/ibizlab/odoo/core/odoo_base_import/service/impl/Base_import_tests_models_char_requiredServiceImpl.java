package cn.ibizlab.odoo.core.odoo_base_import.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_required;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_requiredSearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_char_requiredService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base_import.client.base_import_tests_models_char_requiredOdooClient;
import cn.ibizlab.odoo.core.odoo_base_import.clientmodel.base_import_tests_models_char_requiredClientModel;

/**
 * 实体[测试:基本导入模型，字符必选] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_import_tests_models_char_requiredServiceImpl implements IBase_import_tests_models_char_requiredService {

    @Autowired
    base_import_tests_models_char_requiredOdooClient base_import_tests_models_char_requiredOdooClient;


    @Override
    public boolean create(Base_import_tests_models_char_required et) {
        base_import_tests_models_char_requiredClientModel clientModel = convert2Model(et,null);
		base_import_tests_models_char_requiredOdooClient.create(clientModel);
        Base_import_tests_models_char_required rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_import_tests_models_char_required> list){
    }

    @Override
    public boolean remove(Integer id) {
        base_import_tests_models_char_requiredClientModel clientModel = new base_import_tests_models_char_requiredClientModel();
        clientModel.setId(id);
		base_import_tests_models_char_requiredOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Base_import_tests_models_char_required get(Integer id) {
        base_import_tests_models_char_requiredClientModel clientModel = new base_import_tests_models_char_requiredClientModel();
        clientModel.setId(id);
		base_import_tests_models_char_requiredOdooClient.get(clientModel);
        Base_import_tests_models_char_required et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Base_import_tests_models_char_required();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Base_import_tests_models_char_required et) {
        base_import_tests_models_char_requiredClientModel clientModel = convert2Model(et,null);
		base_import_tests_models_char_requiredOdooClient.update(clientModel);
        Base_import_tests_models_char_required rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Base_import_tests_models_char_required> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_import_tests_models_char_required> searchDefault(Base_import_tests_models_char_requiredSearchContext context) {
        List<Base_import_tests_models_char_required> list = new ArrayList<Base_import_tests_models_char_required>();
        Page<base_import_tests_models_char_requiredClientModel> clientModelList = base_import_tests_models_char_requiredOdooClient.search(context);
        for(base_import_tests_models_char_requiredClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Base_import_tests_models_char_required>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public base_import_tests_models_char_requiredClientModel convert2Model(Base_import_tests_models_char_required domain , base_import_tests_models_char_requiredClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new base_import_tests_models_char_requiredClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("valuedirtyflag"))
                model.setValue(domain.getValue());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Base_import_tests_models_char_required convert2Domain( base_import_tests_models_char_requiredClientModel model ,Base_import_tests_models_char_required domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Base_import_tests_models_char_required();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getValueDirtyFlag())
            domain.setValue(model.getValue());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



