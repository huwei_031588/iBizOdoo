package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_partner_bank;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_bankSearchContext;

/**
 * 实体 [银行账户] 存储对象
 */
public interface Res_partner_bankRepository extends Repository<Res_partner_bank> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_partner_bank> searchDefault(Res_partner_bankSearchContext context);

    Res_partner_bank convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank domain , Res_partner_bank po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank convert2Domain( Res_partner_bank po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank domain) ;

}
