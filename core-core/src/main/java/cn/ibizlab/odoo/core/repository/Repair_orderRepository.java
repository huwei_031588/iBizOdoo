package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Repair_order;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_orderSearchContext;

/**
 * 实体 [维修单] 存储对象
 */
public interface Repair_orderRepository extends Repository<Repair_order> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Repair_order> searchDefault(Repair_orderSearchContext context);

    Repair_order convert2PO(cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order domain , Repair_order po) ;

    cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order convert2Domain( Repair_order po ,cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order domain) ;

}
