package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouse_orderpointSearchContext;


/**
 * 实体[Stock_warehouse_orderpoint] 服务对象接口
 */
public interface IStock_warehouse_orderpointService{

    boolean update(Stock_warehouse_orderpoint et) ;
    void updateBatch(List<Stock_warehouse_orderpoint> list) ;
    Stock_warehouse_orderpoint get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Stock_warehouse_orderpoint et) ;
    void createBatch(List<Stock_warehouse_orderpoint> list) ;
    Page<Stock_warehouse_orderpoint> searchDefault(Stock_warehouse_orderpointSearchContext context) ;

}



