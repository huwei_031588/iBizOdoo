package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_activity_reportSearchContext;

/**
 * 实体 [CRM活动分析] 存储模型
 */
public interface Crm_activity_report{

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 概率
     */
    Double getProbability();

    void setProbability(Double probability);

    /**
     * 获取 [概率]脏标记
     */
    boolean getProbabilityDirtyFlag();

    /**
     * 类型
     */
    String getLead_type();

    void setLead_type(String lead_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getLead_typeDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 摘要
     */
    String getSubject();

    void setSubject(String subject);

    /**
     * 获取 [摘要]脏标记
     */
    boolean getSubjectDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 销售团队
     */
    String getTeam_id_text();

    void setTeam_id_text(String team_id_text);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_id_textDirtyFlag();

    /**
     * 活动类型
     */
    String getMail_activity_type_id_text();

    void setMail_activity_type_id_text(String mail_activity_type_id_text);

    /**
     * 获取 [活动类型]脏标记
     */
    boolean getMail_activity_type_id_textDirtyFlag();

    /**
     * 线索
     */
    String getLead_id_text();

    void setLead_id_text(String lead_id_text);

    /**
     * 获取 [线索]脏标记
     */
    boolean getLead_id_textDirtyFlag();

    /**
     * 销售员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 子类型
     */
    String getSubtype_id_text();

    void setSubtype_id_text(String subtype_id_text);

    /**
     * 获取 [子类型]脏标记
     */
    boolean getSubtype_id_textDirtyFlag();

    /**
     * 国家
     */
    String getCountry_id_text();

    void setCountry_id_text(String country_id_text);

    /**
     * 获取 [国家]脏标记
     */
    boolean getCountry_id_textDirtyFlag();

    /**
     * 业务合作伙伴/客户
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务合作伙伴/客户]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 阶段
     */
    String getStage_id_text();

    void setStage_id_text(String stage_id_text);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getAuthor_id_text();

    void setAuthor_id_text(String author_id_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getAuthor_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getAuthor_id();

    void setAuthor_id(Integer author_id);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getAuthor_idDirtyFlag();

    /**
     * 国家
     */
    Integer getCountry_id();

    void setCountry_id(Integer country_id);

    /**
     * 获取 [国家]脏标记
     */
    boolean getCountry_idDirtyFlag();

    /**
     * 阶段
     */
    Integer getStage_id();

    void setStage_id(Integer stage_id);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_idDirtyFlag();

    /**
     * 子类型
     */
    Integer getSubtype_id();

    void setSubtype_id(Integer subtype_id);

    /**
     * 获取 [子类型]脏标记
     */
    boolean getSubtype_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 线索
     */
    Integer getLead_id();

    void setLead_id(Integer lead_id);

    /**
     * 获取 [线索]脏标记
     */
    boolean getLead_idDirtyFlag();

    /**
     * 活动类型
     */
    Integer getMail_activity_type_id();

    void setMail_activity_type_id(Integer mail_activity_type_id);

    /**
     * 获取 [活动类型]脏标记
     */
    boolean getMail_activity_type_idDirtyFlag();

    /**
     * 销售员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 业务合作伙伴/客户
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务合作伙伴/客户]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 销售团队
     */
    Integer getTeam_id();

    void setTeam_id(Integer team_id);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_idDirtyFlag();

}
