package cn.ibizlab.odoo.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_resourceSearchContext;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_resourceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_resource.client.resource_resourceOdooClient;
import cn.ibizlab.odoo.core.odoo_resource.clientmodel.resource_resourceClientModel;

/**
 * 实体[资源] 服务对象接口实现
 */
@Slf4j
@Service
public class Resource_resourceServiceImpl implements IResource_resourceService {

    @Autowired
    resource_resourceOdooClient resource_resourceOdooClient;


    @Override
    public boolean update(Resource_resource et) {
        resource_resourceClientModel clientModel = convert2Model(et,null);
		resource_resourceOdooClient.update(clientModel);
        Resource_resource rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Resource_resource> list){
    }

    @Override
    public Resource_resource get(Integer id) {
        resource_resourceClientModel clientModel = new resource_resourceClientModel();
        clientModel.setId(id);
		resource_resourceOdooClient.get(clientModel);
        Resource_resource et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Resource_resource();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Resource_resource et) {
        resource_resourceClientModel clientModel = convert2Model(et,null);
		resource_resourceOdooClient.create(clientModel);
        Resource_resource rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Resource_resource> list){
    }

    @Override
    public boolean remove(Integer id) {
        resource_resourceClientModel clientModel = new resource_resourceClientModel();
        clientModel.setId(id);
		resource_resourceOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Resource_resource> searchDefault(Resource_resourceSearchContext context) {
        List<Resource_resource> list = new ArrayList<Resource_resource>();
        Page<resource_resourceClientModel> clientModelList = resource_resourceOdooClient.search(context);
        for(resource_resourceClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Resource_resource>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public resource_resourceClientModel convert2Model(Resource_resource domain , resource_resourceClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new resource_resourceClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("time_efficiencydirtyflag"))
                model.setTime_efficiency(domain.getTimeEfficiency());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("resource_typedirtyflag"))
                model.setResource_type(domain.getResourceType());
            if((Boolean) domain.getExtensionparams().get("tzdirtyflag"))
                model.setTz(domain.getTz());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("calendar_id_textdirtyflag"))
                model.setCalendar_id_text(domain.getCalendarIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("calendar_iddirtyflag"))
                model.setCalendar_id(domain.getCalendarId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Resource_resource convert2Domain( resource_resourceClientModel model ,Resource_resource domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Resource_resource();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getTime_efficiencyDirtyFlag())
            domain.setTimeEfficiency(model.getTime_efficiency());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getResource_typeDirtyFlag())
            domain.setResourceType(model.getResource_type());
        if(model.getTzDirtyFlag())
            domain.setTz(model.getTz());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCalendar_id_textDirtyFlag())
            domain.setCalendarIdText(model.getCalendar_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCalendar_idDirtyFlag())
            domain.setCalendarId(model.getCalendar_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



