package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_routing;

/**
 * 实体[mrp_routing] 服务对象接口
 */
public interface mrp_routingRepository{


    public mrp_routing createPO() ;
        public void update(mrp_routing mrp_routing);

        public void get(String id);

        public void create(mrp_routing mrp_routing);

        public void createBatch(mrp_routing mrp_routing);

        public List<mrp_routing> search();

        public void removeBatch(String id);

        public void remove(String id);

        public void updateBatch(mrp_routing mrp_routing);


}
