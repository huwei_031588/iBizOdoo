package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_categorySearchContext;

/**
 * 实体 [产品种类] 存储对象
 */
public interface Product_categoryRepository extends Repository<Product_category> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_category> searchDefault(Product_categorySearchContext context);

    Product_category convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_category domain , Product_category po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_category convert2Domain( Product_category po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_category domain) ;

}
