package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_category;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_partner_categoryOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_partner_categoryClientModel;

/**
 * 实体[业务伙伴标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_partner_categoryServiceImpl implements IRes_partner_categoryService {

    @Autowired
    res_partner_categoryOdooClient res_partner_categoryOdooClient;


    @Override
    public Res_partner_category get(Integer id) {
        res_partner_categoryClientModel clientModel = new res_partner_categoryClientModel();
        clientModel.setId(id);
		res_partner_categoryOdooClient.get(clientModel);
        Res_partner_category et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_partner_category();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        res_partner_categoryClientModel clientModel = new res_partner_categoryClientModel();
        clientModel.setId(id);
		res_partner_categoryOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Res_partner_category et) {
        res_partner_categoryClientModel clientModel = convert2Model(et,null);
		res_partner_categoryOdooClient.create(clientModel);
        Res_partner_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_partner_category> list){
    }

    @Override
    public boolean update(Res_partner_category et) {
        res_partner_categoryClientModel clientModel = convert2Model(et,null);
		res_partner_categoryOdooClient.update(clientModel);
        Res_partner_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_partner_category> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_partner_category> searchDefault(Res_partner_categorySearchContext context) {
        List<Res_partner_category> list = new ArrayList<Res_partner_category>();
        Page<res_partner_categoryClientModel> clientModelList = res_partner_categoryOdooClient.search(context);
        for(res_partner_categoryClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_partner_category>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_partner_categoryClientModel convert2Model(Res_partner_category domain , res_partner_categoryClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_partner_categoryClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("partner_idsdirtyflag"))
                model.setPartner_ids(domain.getPartnerIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("parent_pathdirtyflag"))
                model.setParent_path(domain.getParentPath());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_partner_category convert2Domain( res_partner_categoryClientModel model ,Res_partner_category domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_partner_category();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getPartner_idsDirtyFlag())
            domain.setPartnerIds(model.getPartner_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getParent_pathDirtyFlag())
            domain.setParentPath(model.getParent_path());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



