package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_companySearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_companyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_companyOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_companyClientModel;

/**
 * 实体[公司] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_companyServiceImpl implements IRes_companyService {

    @Autowired
    res_companyOdooClient res_companyOdooClient;


    @Override
    public Res_company get(Integer id) {
        res_companyClientModel clientModel = new res_companyClientModel();
        clientModel.setId(id);
		res_companyOdooClient.get(clientModel);
        Res_company et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_company();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        res_companyClientModel clientModel = new res_companyClientModel();
        clientModel.setId(id);
		res_companyOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Res_company et) {
        res_companyClientModel clientModel = convert2Model(et,null);
		res_companyOdooClient.create(clientModel);
        Res_company rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_company> list){
    }

    @Override
    public boolean update(Res_company et) {
        res_companyClientModel clientModel = convert2Model(et,null);
		res_companyOdooClient.update(clientModel);
        Res_company rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_company> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_company> searchDefault(Res_companySearchContext context) {
        List<Res_company> list = new ArrayList<Res_company>();
        Page<res_companyClientModel> clientModelList = res_companyOdooClient.search(context);
        for(res_companyClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_company>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_companyClientModel convert2Model(Res_company domain , res_companyClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_companyClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("report_headerdirtyflag"))
                model.setReport_header(domain.getReportHeader());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            if((Boolean) domain.getExtensionparams().get("sale_quotation_onboarding_statedirtyflag"))
                model.setSale_quotation_onboarding_state(domain.getSaleQuotationOnboardingState());
            if((Boolean) domain.getExtensionparams().get("quotation_validity_daysdirtyflag"))
                model.setQuotation_validity_days(domain.getQuotationValidityDays());
            if((Boolean) domain.getExtensionparams().get("account_onboarding_invoice_layout_statedirtyflag"))
                model.setAccount_onboarding_invoice_layout_state(domain.getAccountOnboardingInvoiceLayoutState());
            if((Boolean) domain.getExtensionparams().get("account_nodirtyflag"))
                model.setAccount_no(domain.getAccountNo());
            if((Boolean) domain.getExtensionparams().get("bank_account_code_prefixdirtyflag"))
                model.setBank_account_code_prefix(domain.getBankAccountCodePrefix());
            if((Boolean) domain.getExtensionparams().get("bank_journal_idsdirtyflag"))
                model.setBank_journal_ids(domain.getBankJournalIds());
            if((Boolean) domain.getExtensionparams().get("zipdirtyflag"))
                model.setZip(domain.getZip());
            if((Boolean) domain.getExtensionparams().get("period_lock_datedirtyflag"))
                model.setPeriod_lock_date(domain.getPeriodLockDate());
            if((Boolean) domain.getExtensionparams().get("state_iddirtyflag"))
                model.setState_id(domain.getStateId());
            if((Boolean) domain.getExtensionparams().get("tax_exigibilitydirtyflag"))
                model.setTax_exigibility(domain.getTaxExigibility());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_idsdirtyflag"))
                model.setResource_calendar_ids(domain.getResourceCalendarIds());
            if((Boolean) domain.getExtensionparams().get("bank_idsdirtyflag"))
                model.setBank_ids(domain.getBankIds());
            if((Boolean) domain.getExtensionparams().get("account_dashboard_onboarding_statedirtyflag"))
                model.setAccount_dashboard_onboarding_state(domain.getAccountDashboardOnboardingState());
            if((Boolean) domain.getExtensionparams().get("propagation_minimum_deltadirtyflag"))
                model.setPropagation_minimum_delta(domain.getPropagationMinimumDelta());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("snailmail_colordirtyflag"))
                model.setSnailmail_color(domain.getSnailmailColor());
            if((Boolean) domain.getExtensionparams().get("overdue_msgdirtyflag"))
                model.setOverdue_msg(domain.getOverdueMsg());
            if((Boolean) domain.getExtensionparams().get("citydirtyflag"))
                model.setCity(domain.getCity());
            if((Boolean) domain.getExtensionparams().get("account_setup_coa_statedirtyflag"))
                model.setAccount_setup_coa_state(domain.getAccountSetupCoaState());
            if((Boolean) domain.getExtensionparams().get("invoice_reference_typedirtyflag"))
                model.setInvoice_reference_type(domain.getInvoiceReferenceType());
            if((Boolean) domain.getExtensionparams().get("catchalldirtyflag"))
                model.setCatchall(domain.getCatchall());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("anglo_saxon_accountingdirtyflag"))
                model.setAnglo_saxon_accounting(domain.getAngloSaxonAccounting());
            if((Boolean) domain.getExtensionparams().get("snailmail_duplexdirtyflag"))
                model.setSnailmail_duplex(domain.getSnailmailDuplex());
            if((Boolean) domain.getExtensionparams().get("social_githubdirtyflag"))
                model.setSocial_github(domain.getSocialGithub());
            if((Boolean) domain.getExtensionparams().get("account_setup_bank_data_statedirtyflag"))
                model.setAccount_setup_bank_data_state(domain.getAccountSetupBankDataState());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("street2dirtyflag"))
                model.setStreet2(domain.getStreet2());
            if((Boolean) domain.getExtensionparams().get("expects_chart_of_accountsdirtyflag"))
                model.setExpects_chart_of_accounts(domain.getExpectsChartOfAccounts());
            if((Boolean) domain.getExtensionparams().get("transfer_account_code_prefixdirtyflag"))
                model.setTransfer_account_code_prefix(domain.getTransferAccountCodePrefix());
            if((Boolean) domain.getExtensionparams().get("fiscalyear_last_daydirtyflag"))
                model.setFiscalyear_last_day(domain.getFiscalyearLastDay());
            if((Boolean) domain.getExtensionparams().get("user_idsdirtyflag"))
                model.setUser_ids(domain.getUserIds());
            if((Boolean) domain.getExtensionparams().get("account_bank_reconciliation_startdirtyflag"))
                model.setAccount_bank_reconciliation_start(domain.getAccountBankReconciliationStart());
            if((Boolean) domain.getExtensionparams().get("portal_confirmation_paydirtyflag"))
                model.setPortal_confirmation_pay(domain.getPortalConfirmationPay());
            if((Boolean) domain.getExtensionparams().get("qr_codedirtyflag"))
                model.setQr_code(domain.getQrCode());
            if((Boolean) domain.getExtensionparams().get("streetdirtyflag"))
                model.setStreet(domain.getStreet());
            if((Boolean) domain.getExtensionparams().get("account_invoice_onboarding_statedirtyflag"))
                model.setAccount_invoice_onboarding_state(domain.getAccountInvoiceOnboardingState());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("nomenclature_iddirtyflag"))
                model.setNomenclature_id(domain.getNomenclatureId());
            if((Boolean) domain.getExtensionparams().get("social_googleplusdirtyflag"))
                model.setSocial_googleplus(domain.getSocialGoogleplus());
            if((Boolean) domain.getExtensionparams().get("payment_acquirer_onboarding_statedirtyflag"))
                model.setPayment_acquirer_onboarding_state(domain.getPaymentAcquirerOnboardingState());
            if((Boolean) domain.getExtensionparams().get("report_footerdirtyflag"))
                model.setReport_footer(domain.getReportFooter());
            if((Boolean) domain.getExtensionparams().get("payment_onboarding_payment_methoddirtyflag"))
                model.setPayment_onboarding_payment_method(domain.getPaymentOnboardingPaymentMethod());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("po_double_validationdirtyflag"))
                model.setPo_double_validation(domain.getPoDoubleValidation());
            if((Boolean) domain.getExtensionparams().get("po_leaddirtyflag"))
                model.setPo_lead(domain.getPoLead());
            if((Boolean) domain.getExtensionparams().get("sale_onboarding_sample_quotation_statedirtyflag"))
                model.setSale_onboarding_sample_quotation_state(domain.getSaleOnboardingSampleQuotationState());
            if((Boolean) domain.getExtensionparams().get("sale_onboarding_order_confirmation_statedirtyflag"))
                model.setSale_onboarding_order_confirmation_state(domain.getSaleOnboardingOrderConfirmationState());
            if((Boolean) domain.getExtensionparams().get("external_report_layout_iddirtyflag"))
                model.setExternal_report_layout_id(domain.getExternalReportLayoutId());
            if((Boolean) domain.getExtensionparams().get("sale_onboarding_payment_methoddirtyflag"))
                model.setSale_onboarding_payment_method(domain.getSaleOnboardingPaymentMethod());
            if((Boolean) domain.getExtensionparams().get("account_onboarding_sample_invoice_statedirtyflag"))
                model.setAccount_onboarding_sample_invoice_state(domain.getAccountOnboardingSampleInvoiceState());
            if((Boolean) domain.getExtensionparams().get("base_onboarding_company_statedirtyflag"))
                model.setBase_onboarding_company_state(domain.getBaseOnboardingCompanyState());
            if((Boolean) domain.getExtensionparams().get("social_linkedindirtyflag"))
                model.setSocial_linkedin(domain.getSocialLinkedin());
            if((Boolean) domain.getExtensionparams().get("manufacturing_leaddirtyflag"))
                model.setManufacturing_lead(domain.getManufacturingLead());
            if((Boolean) domain.getExtensionparams().get("sale_notedirtyflag"))
                model.setSale_note(domain.getSaleNote());
            if((Boolean) domain.getExtensionparams().get("po_double_validation_amountdirtyflag"))
                model.setPo_double_validation_amount(domain.getPoDoubleValidationAmount());
            if((Boolean) domain.getExtensionparams().get("po_lockdirtyflag"))
                model.setPo_lock(domain.getPoLock());
            if((Boolean) domain.getExtensionparams().get("social_twitterdirtyflag"))
                model.setSocial_twitter(domain.getSocialTwitter());
            if((Boolean) domain.getExtensionparams().get("social_instagramdirtyflag"))
                model.setSocial_instagram(domain.getSocialInstagram());
            if((Boolean) domain.getExtensionparams().get("account_setup_fy_data_statedirtyflag"))
                model.setAccount_setup_fy_data_state(domain.getAccountSetupFyDataState());
            if((Boolean) domain.getExtensionparams().get("tax_calculation_rounding_methoddirtyflag"))
                model.setTax_calculation_rounding_method(domain.getTaxCalculationRoundingMethod());
            if((Boolean) domain.getExtensionparams().get("cash_account_code_prefixdirtyflag"))
                model.setCash_account_code_prefix(domain.getCashAccountCodePrefix());
            if((Boolean) domain.getExtensionparams().get("account_onboarding_sale_tax_statedirtyflag"))
                model.setAccount_onboarding_sale_tax_state(domain.getAccountOnboardingSaleTaxState());
            if((Boolean) domain.getExtensionparams().get("security_leaddirtyflag"))
                model.setSecurity_lead(domain.getSecurityLead());
            if((Boolean) domain.getExtensionparams().get("website_theme_onboarding_donedirtyflag"))
                model.setWebsite_theme_onboarding_done(domain.getWebsiteThemeOnboardingDone());
            if((Boolean) domain.getExtensionparams().get("invoice_is_printdirtyflag"))
                model.setInvoice_is_print(domain.getInvoiceIsPrint());
            if((Boolean) domain.getExtensionparams().get("company_registrydirtyflag"))
                model.setCompany_registry(domain.getCompanyRegistry());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("logo_webdirtyflag"))
                model.setLogo_web(domain.getLogoWeb());
            if((Boolean) domain.getExtensionparams().get("fiscalyear_lock_datedirtyflag"))
                model.setFiscalyear_lock_date(domain.getFiscalyearLockDate());
            if((Boolean) domain.getExtensionparams().get("invoice_is_snailmaildirtyflag"))
                model.setInvoice_is_snailmail(domain.getInvoiceIsSnailmail());
            if((Boolean) domain.getExtensionparams().get("website_sale_onboarding_payment_acquirer_statedirtyflag"))
                model.setWebsite_sale_onboarding_payment_acquirer_state(domain.getWebsiteSaleOnboardingPaymentAcquirerState());
            if((Boolean) domain.getExtensionparams().get("social_facebookdirtyflag"))
                model.setSocial_facebook(domain.getSocialFacebook());
            if((Boolean) domain.getExtensionparams().get("portal_confirmation_signdirtyflag"))
                model.setPortal_confirmation_sign(domain.getPortalConfirmationSign());
            if((Boolean) domain.getExtensionparams().get("paperformat_iddirtyflag"))
                model.setPaperformat_id(domain.getPaperformatId());
            if((Boolean) domain.getExtensionparams().get("fiscalyear_last_monthdirtyflag"))
                model.setFiscalyear_last_month(domain.getFiscalyearLastMonth());
            if((Boolean) domain.getExtensionparams().get("invoice_is_emaildirtyflag"))
                model.setInvoice_is_email(domain.getInvoiceIsEmail());
            if((Boolean) domain.getExtensionparams().get("social_youtubedirtyflag"))
                model.setSocial_youtube(domain.getSocialYoutube());
            if((Boolean) domain.getExtensionparams().get("expense_currency_exchange_account_iddirtyflag"))
                model.setExpense_currency_exchange_account_id(domain.getExpenseCurrencyExchangeAccountId());
            if((Boolean) domain.getExtensionparams().get("partner_giddirtyflag"))
                model.setPartner_gid(domain.getPartnerGid());
            if((Boolean) domain.getExtensionparams().get("phonedirtyflag"))
                model.setPhone(domain.getPhone());
            if((Boolean) domain.getExtensionparams().get("logodirtyflag"))
                model.setLogo(domain.getLogo());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_input_categ_id_textdirtyflag"))
                model.setProperty_stock_account_input_categ_id_text(domain.getPropertyStockAccountInputCategIdText());
            if((Boolean) domain.getExtensionparams().get("account_purchase_tax_id_textdirtyflag"))
                model.setAccount_purchase_tax_id_text(domain.getAccountPurchaseTaxIdText());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("incoterm_id_textdirtyflag"))
                model.setIncoterm_id_text(domain.getIncotermIdText());
            if((Boolean) domain.getExtensionparams().get("account_opening_journal_iddirtyflag"))
                model.setAccount_opening_journal_id(domain.getAccountOpeningJournalId());
            if((Boolean) domain.getExtensionparams().get("transfer_account_id_textdirtyflag"))
                model.setTransfer_account_id_text(domain.getTransferAccountIdText());
            if((Boolean) domain.getExtensionparams().get("income_currency_exchange_account_iddirtyflag"))
                model.setIncome_currency_exchange_account_id(domain.getIncomeCurrencyExchangeAccountId());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("account_opening_datedirtyflag"))
                model.setAccount_opening_date(domain.getAccountOpeningDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("chart_template_id_textdirtyflag"))
                model.setChart_template_id_text(domain.getChartTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("account_sale_tax_id_textdirtyflag"))
                model.setAccount_sale_tax_id_text(domain.getAccountSaleTaxIdText());
            if((Boolean) domain.getExtensionparams().get("account_opening_move_id_textdirtyflag"))
                model.setAccount_opening_move_id_text(domain.getAccountOpeningMoveIdText());
            if((Boolean) domain.getExtensionparams().get("emaildirtyflag"))
                model.setEmail(domain.getEmail());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_output_categ_id_textdirtyflag"))
                model.setProperty_stock_account_output_categ_id_text(domain.getPropertyStockAccountOutputCategIdText());
            if((Boolean) domain.getExtensionparams().get("property_stock_valuation_account_id_textdirtyflag"))
                model.setProperty_stock_valuation_account_id_text(domain.getPropertyStockValuationAccountIdText());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("tax_cash_basis_journal_id_textdirtyflag"))
                model.setTax_cash_basis_journal_id_text(domain.getTaxCashBasisJournalIdText());
            if((Boolean) domain.getExtensionparams().get("internal_transit_location_id_textdirtyflag"))
                model.setInternal_transit_location_id_text(domain.getInternalTransitLocationIdText());
            if((Boolean) domain.getExtensionparams().get("websitedirtyflag"))
                model.setWebsite(domain.getWebsite());
            if((Boolean) domain.getExtensionparams().get("vatdirtyflag"))
                model.setVat(domain.getVat());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_id_textdirtyflag"))
                model.setResource_calendar_id_text(domain.getResourceCalendarIdText());
            if((Boolean) domain.getExtensionparams().get("currency_exchange_journal_id_textdirtyflag"))
                model.setCurrency_exchange_journal_id_text(domain.getCurrencyExchangeJournalIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_output_categ_iddirtyflag"))
                model.setProperty_stock_account_output_categ_id(domain.getPropertyStockAccountOutputCategId());
            if((Boolean) domain.getExtensionparams().get("property_stock_valuation_account_iddirtyflag"))
                model.setProperty_stock_valuation_account_id(domain.getPropertyStockValuationAccountId());
            if((Boolean) domain.getExtensionparams().get("account_opening_move_iddirtyflag"))
                model.setAccount_opening_move_id(domain.getAccountOpeningMoveId());
            if((Boolean) domain.getExtensionparams().get("internal_transit_location_iddirtyflag"))
                model.setInternal_transit_location_id(domain.getInternalTransitLocationId());
            if((Boolean) domain.getExtensionparams().get("account_purchase_tax_iddirtyflag"))
                model.setAccount_purchase_tax_id(domain.getAccountPurchaseTaxId());
            if((Boolean) domain.getExtensionparams().get("chart_template_iddirtyflag"))
                model.setChart_template_id(domain.getChartTemplateId());
            if((Boolean) domain.getExtensionparams().get("account_sale_tax_iddirtyflag"))
                model.setAccount_sale_tax_id(domain.getAccountSaleTaxId());
            if((Boolean) domain.getExtensionparams().get("tax_cash_basis_journal_iddirtyflag"))
                model.setTax_cash_basis_journal_id(domain.getTaxCashBasisJournalId());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_input_categ_iddirtyflag"))
                model.setProperty_stock_account_input_categ_id(domain.getPropertyStockAccountInputCategId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("incoterm_iddirtyflag"))
                model.setIncoterm_id(domain.getIncotermId());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_iddirtyflag"))
                model.setResource_calendar_id(domain.getResourceCalendarId());
            if((Boolean) domain.getExtensionparams().get("transfer_account_iddirtyflag"))
                model.setTransfer_account_id(domain.getTransferAccountId());
            if((Boolean) domain.getExtensionparams().get("currency_exchange_journal_iddirtyflag"))
                model.setCurrency_exchange_journal_id(domain.getCurrencyExchangeJournalId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_company convert2Domain( res_companyClientModel model ,Res_company domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_company();
        }

        if(model.getReport_headerDirtyFlag())
            domain.setReportHeader(model.getReport_header());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        if(model.getSale_quotation_onboarding_stateDirtyFlag())
            domain.setSaleQuotationOnboardingState(model.getSale_quotation_onboarding_state());
        if(model.getQuotation_validity_daysDirtyFlag())
            domain.setQuotationValidityDays(model.getQuotation_validity_days());
        if(model.getAccount_onboarding_invoice_layout_stateDirtyFlag())
            domain.setAccountOnboardingInvoiceLayoutState(model.getAccount_onboarding_invoice_layout_state());
        if(model.getAccount_noDirtyFlag())
            domain.setAccountNo(model.getAccount_no());
        if(model.getBank_account_code_prefixDirtyFlag())
            domain.setBankAccountCodePrefix(model.getBank_account_code_prefix());
        if(model.getBank_journal_idsDirtyFlag())
            domain.setBankJournalIds(model.getBank_journal_ids());
        if(model.getZipDirtyFlag())
            domain.setZip(model.getZip());
        if(model.getPeriod_lock_dateDirtyFlag())
            domain.setPeriodLockDate(model.getPeriod_lock_date());
        if(model.getState_idDirtyFlag())
            domain.setStateId(model.getState_id());
        if(model.getTax_exigibilityDirtyFlag())
            domain.setTaxExigibility(model.getTax_exigibility());
        if(model.getResource_calendar_idsDirtyFlag())
            domain.setResourceCalendarIds(model.getResource_calendar_ids());
        if(model.getBank_idsDirtyFlag())
            domain.setBankIds(model.getBank_ids());
        if(model.getAccount_dashboard_onboarding_stateDirtyFlag())
            domain.setAccountDashboardOnboardingState(model.getAccount_dashboard_onboarding_state());
        if(model.getPropagation_minimum_deltaDirtyFlag())
            domain.setPropagationMinimumDelta(model.getPropagation_minimum_delta());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getSnailmail_colorDirtyFlag())
            domain.setSnailmailColor(model.getSnailmail_color());
        if(model.getOverdue_msgDirtyFlag())
            domain.setOverdueMsg(model.getOverdue_msg());
        if(model.getCityDirtyFlag())
            domain.setCity(model.getCity());
        if(model.getAccount_setup_coa_stateDirtyFlag())
            domain.setAccountSetupCoaState(model.getAccount_setup_coa_state());
        if(model.getInvoice_reference_typeDirtyFlag())
            domain.setInvoiceReferenceType(model.getInvoice_reference_type());
        if(model.getCatchallDirtyFlag())
            domain.setCatchall(model.getCatchall());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getAnglo_saxon_accountingDirtyFlag())
            domain.setAngloSaxonAccounting(model.getAnglo_saxon_accounting());
        if(model.getSnailmail_duplexDirtyFlag())
            domain.setSnailmailDuplex(model.getSnailmail_duplex());
        if(model.getSocial_githubDirtyFlag())
            domain.setSocialGithub(model.getSocial_github());
        if(model.getAccount_setup_bank_data_stateDirtyFlag())
            domain.setAccountSetupBankDataState(model.getAccount_setup_bank_data_state());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getStreet2DirtyFlag())
            domain.setStreet2(model.getStreet2());
        if(model.getExpects_chart_of_accountsDirtyFlag())
            domain.setExpectsChartOfAccounts(model.getExpects_chart_of_accounts());
        if(model.getTransfer_account_code_prefixDirtyFlag())
            domain.setTransferAccountCodePrefix(model.getTransfer_account_code_prefix());
        if(model.getFiscalyear_last_dayDirtyFlag())
            domain.setFiscalyearLastDay(model.getFiscalyear_last_day());
        if(model.getUser_idsDirtyFlag())
            domain.setUserIds(model.getUser_ids());
        if(model.getAccount_bank_reconciliation_startDirtyFlag())
            domain.setAccountBankReconciliationStart(model.getAccount_bank_reconciliation_start());
        if(model.getPortal_confirmation_payDirtyFlag())
            domain.setPortalConfirmationPay(model.getPortal_confirmation_pay());
        if(model.getQr_codeDirtyFlag())
            domain.setQrCode(model.getQr_code());
        if(model.getStreetDirtyFlag())
            domain.setStreet(model.getStreet());
        if(model.getAccount_invoice_onboarding_stateDirtyFlag())
            domain.setAccountInvoiceOnboardingState(model.getAccount_invoice_onboarding_state());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getNomenclature_idDirtyFlag())
            domain.setNomenclatureId(model.getNomenclature_id());
        if(model.getSocial_googleplusDirtyFlag())
            domain.setSocialGoogleplus(model.getSocial_googleplus());
        if(model.getPayment_acquirer_onboarding_stateDirtyFlag())
            domain.setPaymentAcquirerOnboardingState(model.getPayment_acquirer_onboarding_state());
        if(model.getReport_footerDirtyFlag())
            domain.setReportFooter(model.getReport_footer());
        if(model.getPayment_onboarding_payment_methodDirtyFlag())
            domain.setPaymentOnboardingPaymentMethod(model.getPayment_onboarding_payment_method());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getPo_double_validationDirtyFlag())
            domain.setPoDoubleValidation(model.getPo_double_validation());
        if(model.getPo_leadDirtyFlag())
            domain.setPoLead(model.getPo_lead());
        if(model.getSale_onboarding_sample_quotation_stateDirtyFlag())
            domain.setSaleOnboardingSampleQuotationState(model.getSale_onboarding_sample_quotation_state());
        if(model.getSale_onboarding_order_confirmation_stateDirtyFlag())
            domain.setSaleOnboardingOrderConfirmationState(model.getSale_onboarding_order_confirmation_state());
        if(model.getExternal_report_layout_idDirtyFlag())
            domain.setExternalReportLayoutId(model.getExternal_report_layout_id());
        if(model.getSale_onboarding_payment_methodDirtyFlag())
            domain.setSaleOnboardingPaymentMethod(model.getSale_onboarding_payment_method());
        if(model.getAccount_onboarding_sample_invoice_stateDirtyFlag())
            domain.setAccountOnboardingSampleInvoiceState(model.getAccount_onboarding_sample_invoice_state());
        if(model.getBase_onboarding_company_stateDirtyFlag())
            domain.setBaseOnboardingCompanyState(model.getBase_onboarding_company_state());
        if(model.getSocial_linkedinDirtyFlag())
            domain.setSocialLinkedin(model.getSocial_linkedin());
        if(model.getManufacturing_leadDirtyFlag())
            domain.setManufacturingLead(model.getManufacturing_lead());
        if(model.getSale_noteDirtyFlag())
            domain.setSaleNote(model.getSale_note());
        if(model.getPo_double_validation_amountDirtyFlag())
            domain.setPoDoubleValidationAmount(model.getPo_double_validation_amount());
        if(model.getPo_lockDirtyFlag())
            domain.setPoLock(model.getPo_lock());
        if(model.getSocial_twitterDirtyFlag())
            domain.setSocialTwitter(model.getSocial_twitter());
        if(model.getSocial_instagramDirtyFlag())
            domain.setSocialInstagram(model.getSocial_instagram());
        if(model.getAccount_setup_fy_data_stateDirtyFlag())
            domain.setAccountSetupFyDataState(model.getAccount_setup_fy_data_state());
        if(model.getTax_calculation_rounding_methodDirtyFlag())
            domain.setTaxCalculationRoundingMethod(model.getTax_calculation_rounding_method());
        if(model.getCash_account_code_prefixDirtyFlag())
            domain.setCashAccountCodePrefix(model.getCash_account_code_prefix());
        if(model.getAccount_onboarding_sale_tax_stateDirtyFlag())
            domain.setAccountOnboardingSaleTaxState(model.getAccount_onboarding_sale_tax_state());
        if(model.getSecurity_leadDirtyFlag())
            domain.setSecurityLead(model.getSecurity_lead());
        if(model.getWebsite_theme_onboarding_doneDirtyFlag())
            domain.setWebsiteThemeOnboardingDone(model.getWebsite_theme_onboarding_done());
        if(model.getInvoice_is_printDirtyFlag())
            domain.setInvoiceIsPrint(model.getInvoice_is_print());
        if(model.getCompany_registryDirtyFlag())
            domain.setCompanyRegistry(model.getCompany_registry());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getLogo_webDirtyFlag())
            domain.setLogoWeb(model.getLogo_web());
        if(model.getFiscalyear_lock_dateDirtyFlag())
            domain.setFiscalyearLockDate(model.getFiscalyear_lock_date());
        if(model.getInvoice_is_snailmailDirtyFlag())
            domain.setInvoiceIsSnailmail(model.getInvoice_is_snailmail());
        if(model.getWebsite_sale_onboarding_payment_acquirer_stateDirtyFlag())
            domain.setWebsiteSaleOnboardingPaymentAcquirerState(model.getWebsite_sale_onboarding_payment_acquirer_state());
        if(model.getSocial_facebookDirtyFlag())
            domain.setSocialFacebook(model.getSocial_facebook());
        if(model.getPortal_confirmation_signDirtyFlag())
            domain.setPortalConfirmationSign(model.getPortal_confirmation_sign());
        if(model.getPaperformat_idDirtyFlag())
            domain.setPaperformatId(model.getPaperformat_id());
        if(model.getFiscalyear_last_monthDirtyFlag())
            domain.setFiscalyearLastMonth(model.getFiscalyear_last_month());
        if(model.getInvoice_is_emailDirtyFlag())
            domain.setInvoiceIsEmail(model.getInvoice_is_email());
        if(model.getSocial_youtubeDirtyFlag())
            domain.setSocialYoutube(model.getSocial_youtube());
        if(model.getExpense_currency_exchange_account_idDirtyFlag())
            domain.setExpenseCurrencyExchangeAccountId(model.getExpense_currency_exchange_account_id());
        if(model.getPartner_gidDirtyFlag())
            domain.setPartnerGid(model.getPartner_gid());
        if(model.getPhoneDirtyFlag())
            domain.setPhone(model.getPhone());
        if(model.getLogoDirtyFlag())
            domain.setLogo(model.getLogo());
        if(model.getProperty_stock_account_input_categ_id_textDirtyFlag())
            domain.setPropertyStockAccountInputCategIdText(model.getProperty_stock_account_input_categ_id_text());
        if(model.getAccount_purchase_tax_id_textDirtyFlag())
            domain.setAccountPurchaseTaxIdText(model.getAccount_purchase_tax_id_text());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIncoterm_id_textDirtyFlag())
            domain.setIncotermIdText(model.getIncoterm_id_text());
        if(model.getAccount_opening_journal_idDirtyFlag())
            domain.setAccountOpeningJournalId(model.getAccount_opening_journal_id());
        if(model.getTransfer_account_id_textDirtyFlag())
            domain.setTransferAccountIdText(model.getTransfer_account_id_text());
        if(model.getIncome_currency_exchange_account_idDirtyFlag())
            domain.setIncomeCurrencyExchangeAccountId(model.getIncome_currency_exchange_account_id());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getAccount_opening_dateDirtyFlag())
            domain.setAccountOpeningDate(model.getAccount_opening_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getChart_template_id_textDirtyFlag())
            domain.setChartTemplateIdText(model.getChart_template_id_text());
        if(model.getAccount_sale_tax_id_textDirtyFlag())
            domain.setAccountSaleTaxIdText(model.getAccount_sale_tax_id_text());
        if(model.getAccount_opening_move_id_textDirtyFlag())
            domain.setAccountOpeningMoveIdText(model.getAccount_opening_move_id_text());
        if(model.getEmailDirtyFlag())
            domain.setEmail(model.getEmail());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProperty_stock_account_output_categ_id_textDirtyFlag())
            domain.setPropertyStockAccountOutputCategIdText(model.getProperty_stock_account_output_categ_id_text());
        if(model.getProperty_stock_valuation_account_id_textDirtyFlag())
            domain.setPropertyStockValuationAccountIdText(model.getProperty_stock_valuation_account_id_text());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getTax_cash_basis_journal_id_textDirtyFlag())
            domain.setTaxCashBasisJournalIdText(model.getTax_cash_basis_journal_id_text());
        if(model.getInternal_transit_location_id_textDirtyFlag())
            domain.setInternalTransitLocationIdText(model.getInternal_transit_location_id_text());
        if(model.getWebsiteDirtyFlag())
            domain.setWebsite(model.getWebsite());
        if(model.getVatDirtyFlag())
            domain.setVat(model.getVat());
        if(model.getResource_calendar_id_textDirtyFlag())
            domain.setResourceCalendarIdText(model.getResource_calendar_id_text());
        if(model.getCurrency_exchange_journal_id_textDirtyFlag())
            domain.setCurrencyExchangeJournalIdText(model.getCurrency_exchange_journal_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProperty_stock_account_output_categ_idDirtyFlag())
            domain.setPropertyStockAccountOutputCategId(model.getProperty_stock_account_output_categ_id());
        if(model.getProperty_stock_valuation_account_idDirtyFlag())
            domain.setPropertyStockValuationAccountId(model.getProperty_stock_valuation_account_id());
        if(model.getAccount_opening_move_idDirtyFlag())
            domain.setAccountOpeningMoveId(model.getAccount_opening_move_id());
        if(model.getInternal_transit_location_idDirtyFlag())
            domain.setInternalTransitLocationId(model.getInternal_transit_location_id());
        if(model.getAccount_purchase_tax_idDirtyFlag())
            domain.setAccountPurchaseTaxId(model.getAccount_purchase_tax_id());
        if(model.getChart_template_idDirtyFlag())
            domain.setChartTemplateId(model.getChart_template_id());
        if(model.getAccount_sale_tax_idDirtyFlag())
            domain.setAccountSaleTaxId(model.getAccount_sale_tax_id());
        if(model.getTax_cash_basis_journal_idDirtyFlag())
            domain.setTaxCashBasisJournalId(model.getTax_cash_basis_journal_id());
        if(model.getProperty_stock_account_input_categ_idDirtyFlag())
            domain.setPropertyStockAccountInputCategId(model.getProperty_stock_account_input_categ_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getIncoterm_idDirtyFlag())
            domain.setIncotermId(model.getIncoterm_id());
        if(model.getResource_calendar_idDirtyFlag())
            domain.setResourceCalendarId(model.getResource_calendar_id());
        if(model.getTransfer_account_idDirtyFlag())
            domain.setTransferAccountId(model.getTransfer_account_id());
        if(model.getCurrency_exchange_journal_idDirtyFlag())
            domain.setCurrencyExchangeJournalId(model.getCurrency_exchange_journal_id());
        return domain ;
    }

}

    



