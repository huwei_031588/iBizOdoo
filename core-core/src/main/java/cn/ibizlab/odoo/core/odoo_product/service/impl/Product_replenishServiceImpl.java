package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_replenishSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_replenishService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_replenishOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_replenishClientModel;

/**
 * 实体[补料] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_replenishServiceImpl implements IProduct_replenishService {

    @Autowired
    product_replenishOdooClient product_replenishOdooClient;


    @Override
    public Product_replenish get(Integer id) {
        product_replenishClientModel clientModel = new product_replenishClientModel();
        clientModel.setId(id);
		product_replenishOdooClient.get(clientModel);
        Product_replenish et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_replenish();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        product_replenishClientModel clientModel = new product_replenishClientModel();
        clientModel.setId(id);
		product_replenishOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Product_replenish et) {
        product_replenishClientModel clientModel = convert2Model(et,null);
		product_replenishOdooClient.create(clientModel);
        Product_replenish rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_replenish> list){
    }

    @Override
    public boolean update(Product_replenish et) {
        product_replenishClientModel clientModel = convert2Model(et,null);
		product_replenishOdooClient.update(clientModel);
        Product_replenish rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_replenish> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_replenish> searchDefault(Product_replenishSearchContext context) {
        List<Product_replenish> list = new ArrayList<Product_replenish>();
        Page<product_replenishClientModel> clientModelList = product_replenishOdooClient.search(context);
        for(product_replenishClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_replenish>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_replenishClientModel convert2Model(Product_replenish domain , product_replenishClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_replenishClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("product_has_variantsdirtyflag"))
                model.setProduct_has_variants(domain.getProductHasVariants());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("quantitydirtyflag"))
                model.setQuantity(domain.getQuantity());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("product_uom_category_iddirtyflag"))
                model.setProduct_uom_category_id(domain.getProductUomCategoryId());
            if((Boolean) domain.getExtensionparams().get("date_planneddirtyflag"))
                model.setDate_planned(domain.getDatePlanned());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("route_idsdirtyflag"))
                model.setRoute_ids(domain.getRouteIds());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_id_textdirtyflag"))
                model.setProduct_tmpl_id_text(domain.getProductTmplIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("warehouse_id_textdirtyflag"))
                model.setWarehouse_id_text(domain.getWarehouseIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("warehouse_iddirtyflag"))
                model.setWarehouse_id(domain.getWarehouseId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_replenish convert2Domain( product_replenishClientModel model ,Product_replenish domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_replenish();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getProduct_has_variantsDirtyFlag())
            domain.setProductHasVariants(model.getProduct_has_variants());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getQuantityDirtyFlag())
            domain.setQuantity(model.getQuantity());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getProduct_uom_category_idDirtyFlag())
            domain.setProductUomCategoryId(model.getProduct_uom_category_id());
        if(model.getDate_plannedDirtyFlag())
            domain.setDatePlanned(model.getDate_planned());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getRoute_idsDirtyFlag())
            domain.setRouteIds(model.getRoute_ids());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getProduct_tmpl_id_textDirtyFlag())
            domain.setProductTmplIdText(model.getProduct_tmpl_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWarehouse_id_textDirtyFlag())
            domain.setWarehouseIdText(model.getWarehouse_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getWarehouse_idDirtyFlag())
            domain.setWarehouseId(model.getWarehouse_id());
        return domain ;
    }

}

    



