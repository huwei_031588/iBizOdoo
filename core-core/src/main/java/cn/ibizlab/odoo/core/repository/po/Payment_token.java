package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_tokenSearchContext;

/**
 * 实体 [付款令牌] 存储模型
 */
public interface Payment_token{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 收单方参照
     */
    String getAcquirer_ref();

    void setAcquirer_ref(String acquirer_ref);

    /**
     * 获取 [收单方参照]脏标记
     */
    boolean getAcquirer_refDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 已验证
     */
    String getVerified();

    void setVerified(String verified);

    /**
     * 获取 [已验证]脏标记
     */
    boolean getVerifiedDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 简名
     */
    String getShort_name();

    void setShort_name(String short_name);

    /**
     * 获取 [简名]脏标记
     */
    boolean getShort_nameDirtyFlag();

    /**
     * 付款交易
     */
    String getPayment_ids();

    void setPayment_ids(String payment_ids);

    /**
     * 获取 [付款交易]脏标记
     */
    boolean getPayment_idsDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 收单方帐号
     */
    String getAcquirer_id_text();

    void setAcquirer_id_text(String acquirer_id_text);

    /**
     * 获取 [收单方帐号]脏标记
     */
    boolean getAcquirer_id_textDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 收单方帐号
     */
    Integer getAcquirer_id();

    void setAcquirer_id(Integer acquirer_id);

    /**
     * 获取 [收单方帐号]脏标记
     */
    boolean getAcquirer_idDirtyFlag();

}
