package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet_register_payment_wizard;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheet_register_payment_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expense_sheet_register_payment_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_expense_sheet_register_payment_wizardOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_expense_sheet_register_payment_wizardClientModel;

/**
 * 实体[费用登记付款向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_expense_sheet_register_payment_wizardServiceImpl implements IHr_expense_sheet_register_payment_wizardService {

    @Autowired
    hr_expense_sheet_register_payment_wizardOdooClient hr_expense_sheet_register_payment_wizardOdooClient;


    @Override
    public boolean create(Hr_expense_sheet_register_payment_wizard et) {
        hr_expense_sheet_register_payment_wizardClientModel clientModel = convert2Model(et,null);
		hr_expense_sheet_register_payment_wizardOdooClient.create(clientModel);
        Hr_expense_sheet_register_payment_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_expense_sheet_register_payment_wizard> list){
    }

    @Override
    public Hr_expense_sheet_register_payment_wizard get(Integer id) {
        hr_expense_sheet_register_payment_wizardClientModel clientModel = new hr_expense_sheet_register_payment_wizardClientModel();
        clientModel.setId(id);
		hr_expense_sheet_register_payment_wizardOdooClient.get(clientModel);
        Hr_expense_sheet_register_payment_wizard et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_expense_sheet_register_payment_wizard();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Hr_expense_sheet_register_payment_wizard et) {
        hr_expense_sheet_register_payment_wizardClientModel clientModel = convert2Model(et,null);
		hr_expense_sheet_register_payment_wizardOdooClient.update(clientModel);
        Hr_expense_sheet_register_payment_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_expense_sheet_register_payment_wizard> list){
    }

    @Override
    public boolean remove(Integer id) {
        hr_expense_sheet_register_payment_wizardClientModel clientModel = new hr_expense_sheet_register_payment_wizardClientModel();
        clientModel.setId(id);
		hr_expense_sheet_register_payment_wizardOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_expense_sheet_register_payment_wizard> searchDefault(Hr_expense_sheet_register_payment_wizardSearchContext context) {
        List<Hr_expense_sheet_register_payment_wizard> list = new ArrayList<Hr_expense_sheet_register_payment_wizard>();
        Page<hr_expense_sheet_register_payment_wizardClientModel> clientModelList = hr_expense_sheet_register_payment_wizardOdooClient.search(context);
        for(hr_expense_sheet_register_payment_wizardClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_expense_sheet_register_payment_wizard>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_expense_sheet_register_payment_wizardClientModel convert2Model(Hr_expense_sheet_register_payment_wizard domain , hr_expense_sheet_register_payment_wizardClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_expense_sheet_register_payment_wizardClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("communicationdirtyflag"))
                model.setCommunication(domain.getCommunication());
            if((Boolean) domain.getExtensionparams().get("hide_payment_methoddirtyflag"))
                model.setHide_payment_method(domain.getHidePaymentMethod());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("show_partner_bank_accountdirtyflag"))
                model.setShow_partner_bank_account(domain.getShowPartnerBankAccount());
            if((Boolean) domain.getExtensionparams().get("payment_datedirtyflag"))
                model.setPayment_date(domain.getPaymentDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("payment_method_id_textdirtyflag"))
                model.setPayment_method_id_text(domain.getPaymentMethodIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("payment_method_iddirtyflag"))
                model.setPayment_method_id(domain.getPaymentMethodId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("partner_bank_account_iddirtyflag"))
                model.setPartner_bank_account_id(domain.getPartnerBankAccountId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_expense_sheet_register_payment_wizard convert2Domain( hr_expense_sheet_register_payment_wizardClientModel model ,Hr_expense_sheet_register_payment_wizard domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_expense_sheet_register_payment_wizard();
        }

        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCommunicationDirtyFlag())
            domain.setCommunication(model.getCommunication());
        if(model.getHide_payment_methodDirtyFlag())
            domain.setHidePaymentMethod(model.getHide_payment_method());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getShow_partner_bank_accountDirtyFlag())
            domain.setShowPartnerBankAccount(model.getShow_partner_bank_account());
        if(model.getPayment_dateDirtyFlag())
            domain.setPaymentDate(model.getPayment_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getPayment_method_id_textDirtyFlag())
            domain.setPaymentMethodIdText(model.getPayment_method_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPayment_method_idDirtyFlag())
            domain.setPaymentMethodId(model.getPayment_method_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPartner_bank_account_idDirtyFlag())
            domain.setPartnerBankAccountId(model.getPartner_bank_account_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        return domain ;
    }

}

    



