package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partner_massSearchContext;

/**
 * 实体 [转化线索为商机（批量）] 存储模型
 */
public interface Crm_lead2opportunity_partner_mass{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 商机
     */
    String getOpportunity_ids();

    void setOpportunity_ids(String opportunity_ids);

    /**
     * 获取 [商机]脏标记
     */
    boolean getOpportunity_idsDirtyFlag();

    /**
     * 转换动作
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [转换动作]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 相关客户
     */
    String getAction();

    void setAction(String action);

    /**
     * 获取 [相关客户]脏标记
     */
    boolean getActionDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 强制分配
     */
    String getForce_assignation();

    void setForce_assignation(String force_assignation);

    /**
     * 获取 [强制分配]脏标记
     */
    boolean getForce_assignationDirtyFlag();

    /**
     * 删除重复内容
     */
    String getDeduplicate();

    void setDeduplicate(String deduplicate);

    /**
     * 获取 [删除重复内容]脏标记
     */
    boolean getDeduplicateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 销售员
     */
    String getUser_ids();

    void setUser_ids(String user_ids);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_idsDirtyFlag();

    /**
     * 销售团队
     */
    String getTeam_id_text();

    void setTeam_id_text(String team_id_text);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 客户
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 销售员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 最后更新
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 销售员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 客户
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 销售团队
     */
    Integer getTeam_id();

    void setTeam_id(Integer team_id);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_idDirtyFlag();

}
