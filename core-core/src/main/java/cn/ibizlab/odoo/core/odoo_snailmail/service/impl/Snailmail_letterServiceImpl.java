package cn.ibizlab.odoo.core.odoo_snailmail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.odoo.core.odoo_snailmail.filter.Snailmail_letterSearchContext;
import cn.ibizlab.odoo.core.odoo_snailmail.service.ISnailmail_letterService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_snailmail.client.snailmail_letterOdooClient;
import cn.ibizlab.odoo.core.odoo_snailmail.clientmodel.snailmail_letterClientModel;

/**
 * 实体[Snailmail 信纸] 服务对象接口实现
 */
@Slf4j
@Service
public class Snailmail_letterServiceImpl implements ISnailmail_letterService {

    @Autowired
    snailmail_letterOdooClient snailmail_letterOdooClient;


    @Override
    public boolean remove(Integer id) {
        snailmail_letterClientModel clientModel = new snailmail_letterClientModel();
        clientModel.setId(id);
		snailmail_letterOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Snailmail_letter get(Integer id) {
        snailmail_letterClientModel clientModel = new snailmail_letterClientModel();
        clientModel.setId(id);
		snailmail_letterOdooClient.get(clientModel);
        Snailmail_letter et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Snailmail_letter();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Snailmail_letter et) {
        snailmail_letterClientModel clientModel = convert2Model(et,null);
		snailmail_letterOdooClient.update(clientModel);
        Snailmail_letter rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Snailmail_letter> list){
    }

    @Override
    public boolean create(Snailmail_letter et) {
        snailmail_letterClientModel clientModel = convert2Model(et,null);
		snailmail_letterOdooClient.create(clientModel);
        Snailmail_letter rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Snailmail_letter> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Snailmail_letter> searchDefault(Snailmail_letterSearchContext context) {
        List<Snailmail_letter> list = new ArrayList<Snailmail_letter>();
        Page<snailmail_letterClientModel> clientModelList = snailmail_letterOdooClient.search(context);
        for(snailmail_letterClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Snailmail_letter>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public snailmail_letterClientModel convert2Model(Snailmail_letter domain , snailmail_letterClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new snailmail_letterClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("duplexdirtyflag"))
                model.setDuplex(domain.getDuplex());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("modeldirtyflag"))
                model.setModel(domain.getModel());
            if((Boolean) domain.getExtensionparams().get("attachment_iddirtyflag"))
                model.setAttachment_id(domain.getAttachmentId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("report_templatedirtyflag"))
                model.setReport_template(domain.getReportTemplate());
            if((Boolean) domain.getExtensionparams().get("info_msgdirtyflag"))
                model.setInfo_msg(domain.getInfoMsg());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Snailmail_letter convert2Domain( snailmail_letterClientModel model ,Snailmail_letter domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Snailmail_letter();
        }

        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDuplexDirtyFlag())
            domain.setDuplex(model.getDuplex());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getModelDirtyFlag())
            domain.setModel(model.getModel());
        if(model.getAttachment_idDirtyFlag())
            domain.setAttachmentId(model.getAttachment_id());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getReport_templateDirtyFlag())
            domain.setReportTemplate(model.getReport_template());
        if(model.getInfo_msgDirtyFlag())
            domain.setInfoMsg(model.getInfo_msg());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        return domain ;
    }

}

    



