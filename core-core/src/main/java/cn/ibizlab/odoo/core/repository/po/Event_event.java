package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_event.filter.Event_eventSearchContext;

/**
 * 实体 [活动] 存储模型
 */
public interface Event_event{

    /**
     * 参与者数目
     */
    Integer getSeats_used();

    void setSeats_used(Integer seats_used);

    /**
     * 获取 [参与者数目]脏标记
     */
    boolean getSeats_usedDirtyFlag();

    /**
     * SEO优化
     */
    String getIs_seo_optimized();

    void setIs_seo_optimized(String is_seo_optimized);

    /**
     * 获取 [SEO优化]脏标记
     */
    boolean getIs_seo_optimizedDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 徽章字体
     */
    String getBadge_front();

    void setBadge_front(String badge_front);

    /**
     * 获取 [徽章字体]脏标记
     */
    boolean getBadge_frontDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 邮件排程
     */
    String getEvent_mail_ids();

    void setEvent_mail_ids(String event_mail_ids);

    /**
     * 获取 [邮件排程]脏标记
     */
    boolean getEvent_mail_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 预期的与会人员数量
     */
    Integer getSeats_expected();

    void setSeats_expected(Integer seats_expected);

    /**
     * 获取 [预期的与会人员数量]脏标记
     */
    boolean getSeats_expectedDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 与会者最多人数
     */
    String getSeats_availability();

    void setSeats_availability(String seats_availability);

    /**
     * 获取 [与会者最多人数]脏标记
     */
    boolean getSeats_availabilityDirtyFlag();

    /**
     * 活动入场券
     */
    String getEvent_ticket_ids();

    void setEvent_ticket_ids(String event_ticket_ids);

    /**
     * 获取 [活动入场券]脏标记
     */
    boolean getEvent_ticket_idsDirtyFlag();

    /**
     * 未确认的席位预订
     */
    Integer getSeats_unconfirmed();

    void setSeats_unconfirmed(Integer seats_unconfirmed);

    /**
     * 获取 [未确认的席位预订]脏标记
     */
    boolean getSeats_unconfirmedDirtyFlag();

    /**
     * 正在参加
     */
    String getIs_participating();

    void setIs_participating(String is_participating);

    /**
     * 获取 [正在参加]脏标记
     */
    boolean getIs_participatingDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * Twitter主题标签
     */
    String getTwitter_hashtag();

    void setTwitter_hashtag(String twitter_hashtag);

    /**
     * 获取 [Twitter主题标签]脏标记
     */
    boolean getTwitter_hashtagDirtyFlag();

    /**
     * 与会者最多人数
     */
    Integer getSeats_max();

    void setSeats_max(Integer seats_max);

    /**
     * 获取 [与会者最多人数]脏标记
     */
    boolean getSeats_maxDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_begin();

    void setDate_begin(Timestamp date_begin);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_beginDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 网站opengraph图像
     */
    String getWebsite_meta_og_img();

    void setWebsite_meta_og_img(String website_meta_og_img);

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    boolean getWebsite_meta_og_imgDirtyFlag();

    /**
     * 在当前网站显示
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [在当前网站显示]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 网站meta标题
     */
    String getWebsite_meta_title();

    void setWebsite_meta_title(String website_meta_title);

    /**
     * 获取 [网站meta标题]脏标记
     */
    boolean getWebsite_meta_titleDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 与会者
     */
    String getRegistration_ids();

    void setRegistration_ids(String registration_ids);

    /**
     * 获取 [与会者]脏标记
     */
    boolean getRegistration_idsDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 徽章内部右边
     */
    String getBadge_innerright();

    void setBadge_innerright(String badge_innerright);

    /**
     * 获取 [徽章内部右边]脏标记
     */
    boolean getBadge_innerrightDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 网站元说明
     */
    String getWebsite_meta_description();

    void setWebsite_meta_description(String website_meta_description);

    /**
     * 获取 [网站元说明]脏标记
     */
    boolean getWebsite_meta_descriptionDirtyFlag();

    /**
     * 与会者最少数量
     */
    Integer getSeats_min();

    void setSeats_min(Integer seats_min);

    /**
     * 获取 [与会者最少数量]脏标记
     */
    boolean getSeats_minDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 徽章背面
     */
    String getBadge_back();

    void setBadge_back(String badge_back);

    /**
     * 获取 [徽章背面]脏标记
     */
    boolean getBadge_backDirtyFlag();

    /**
     * 网站meta关键词
     */
    String getWebsite_meta_keywords();

    void setWebsite_meta_keywords(String website_meta_keywords);

    /**
     * 获取 [网站meta关键词]脏标记
     */
    boolean getWebsite_meta_keywordsDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 自动确认注册
     */
    String getAuto_confirm();

    void setAuto_confirm(String auto_confirm);

    /**
     * 获取 [自动确认注册]脏标记
     */
    boolean getAuto_confirmDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getDate_end();

    void setDate_end(Timestamp date_end);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getDate_endDirtyFlag();

    /**
     * 活动
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [活动]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 徽章内部左边
     */
    String getBadge_innerleft();

    void setBadge_innerleft(String badge_innerleft);

    /**
     * 获取 [徽章内部左边]脏标记
     */
    boolean getBadge_innerleftDirtyFlag();

    /**
     * 预订席位
     */
    Integer getSeats_reserved();

    void setSeats_reserved(Integer seats_reserved);

    /**
     * 获取 [预订席位]脏标记
     */
    boolean getSeats_reservedDirtyFlag();

    /**
     * 专用菜单
     */
    String getWebsite_menu();

    void setWebsite_menu(String website_menu);

    /**
     * 获取 [专用菜单]脏标记
     */
    boolean getWebsite_menuDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 定位最后日期
     */
    String getDate_end_located();

    void setDate_end_located(String date_end_located);

    /**
     * 获取 [定位最后日期]脏标记
     */
    boolean getDate_end_locatedDirtyFlag();

    /**
     * 可用席位
     */
    Integer getSeats_available();

    void setSeats_available(Integer seats_available);

    /**
     * 获取 [可用席位]脏标记
     */
    boolean getSeats_availableDirtyFlag();

    /**
     * 已发布
     */
    String getIs_published();

    void setIs_published(String is_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getIs_publishedDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 在线活动
     */
    String getIs_online();

    void setIs_online(String is_online);

    /**
     * 获取 [在线活动]脏标记
     */
    boolean getIs_onlineDirtyFlag();

    /**
     * 时区
     */
    String getDate_tz();

    void setDate_tz(String date_tz);

    /**
     * 获取 [时区]脏标记
     */
    boolean getDate_tzDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 定位开始日期
     */
    String getDate_begin_located();

    void setDate_begin_located(String date_begin_located);

    /**
     * 获取 [定位开始日期]脏标记
     */
    boolean getDate_begin_locatedDirtyFlag();

    /**
     * 事件图标
     */
    String getEvent_logo();

    void setEvent_logo(String event_logo);

    /**
     * 获取 [事件图标]脏标记
     */
    boolean getEvent_logoDirtyFlag();

    /**
     * 网站网址
     */
    String getWebsite_url();

    void setWebsite_url(String website_url);

    /**
     * 获取 [网站网址]脏标记
     */
    boolean getWebsite_urlDirtyFlag();

    /**
     * 看板颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [看板颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 类别
     */
    String getEvent_type_id_text();

    void setEvent_type_id_text(String event_type_id_text);

    /**
     * 获取 [类别]脏标记
     */
    boolean getEvent_type_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 地点
     */
    String getAddress_id_text();

    void setAddress_id_text(String address_id_text);

    /**
     * 获取 [地点]脏标记
     */
    boolean getAddress_id_textDirtyFlag();

    /**
     * 国家
     */
    String getCountry_id_text();

    void setCountry_id_text(String country_id_text);

    /**
     * 获取 [国家]脏标记
     */
    boolean getCountry_id_textDirtyFlag();

    /**
     * 活动菜单
     */
    String getMenu_id_text();

    void setMenu_id_text(String menu_id_text);

    /**
     * 获取 [活动菜单]脏标记
     */
    boolean getMenu_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 负责人
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 组织者
     */
    String getOrganizer_id_text();

    void setOrganizer_id_text(String organizer_id_text);

    /**
     * 获取 [组织者]脏标记
     */
    boolean getOrganizer_id_textDirtyFlag();

    /**
     * 负责人
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 国家
     */
    Integer getCountry_id();

    void setCountry_id(Integer country_id);

    /**
     * 获取 [国家]脏标记
     */
    boolean getCountry_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 组织者
     */
    Integer getOrganizer_id();

    void setOrganizer_id(Integer organizer_id);

    /**
     * 获取 [组织者]脏标记
     */
    boolean getOrganizer_idDirtyFlag();

    /**
     * 活动菜单
     */
    Integer getMenu_id();

    void setMenu_id(Integer menu_id);

    /**
     * 获取 [活动菜单]脏标记
     */
    boolean getMenu_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 地点
     */
    Integer getAddress_id();

    void setAddress_id(Integer address_id);

    /**
     * 获取 [地点]脏标记
     */
    boolean getAddress_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 类别
     */
    Integer getEvent_type_id();

    void setEvent_type_id(Integer event_type_id);

    /**
     * 获取 [类别]脏标记
     */
    boolean getEvent_type_idDirtyFlag();

}
