package cn.ibizlab.odoo.core.odoo_im_livechat.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_report_operatorSearchContext;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_report_operatorService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_im_livechat.client.im_livechat_report_operatorOdooClient;
import cn.ibizlab.odoo.core.odoo_im_livechat.clientmodel.im_livechat_report_operatorClientModel;

/**
 * 实体[实时聊天支持操作员报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Im_livechat_report_operatorServiceImpl implements IIm_livechat_report_operatorService {

    @Autowired
    im_livechat_report_operatorOdooClient im_livechat_report_operatorOdooClient;


    @Override
    public boolean create(Im_livechat_report_operator et) {
        im_livechat_report_operatorClientModel clientModel = convert2Model(et,null);
		im_livechat_report_operatorOdooClient.create(clientModel);
        Im_livechat_report_operator rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Im_livechat_report_operator> list){
    }

    @Override
    public Im_livechat_report_operator get(Integer id) {
        im_livechat_report_operatorClientModel clientModel = new im_livechat_report_operatorClientModel();
        clientModel.setId(id);
		im_livechat_report_operatorOdooClient.get(clientModel);
        Im_livechat_report_operator et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Im_livechat_report_operator();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Im_livechat_report_operator et) {
        im_livechat_report_operatorClientModel clientModel = convert2Model(et,null);
		im_livechat_report_operatorOdooClient.update(clientModel);
        Im_livechat_report_operator rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Im_livechat_report_operator> list){
    }

    @Override
    public boolean remove(Integer id) {
        im_livechat_report_operatorClientModel clientModel = new im_livechat_report_operatorClientModel();
        clientModel.setId(id);
		im_livechat_report_operatorOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Im_livechat_report_operator> searchDefault(Im_livechat_report_operatorSearchContext context) {
        List<Im_livechat_report_operator> list = new ArrayList<Im_livechat_report_operator>();
        Page<im_livechat_report_operatorClientModel> clientModelList = im_livechat_report_operatorOdooClient.search(context);
        for(im_livechat_report_operatorClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Im_livechat_report_operator>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public im_livechat_report_operatorClientModel convert2Model(Im_livechat_report_operator domain , im_livechat_report_operatorClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new im_livechat_report_operatorClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("time_to_answerdirtyflag"))
                model.setTime_to_answer(domain.getTimeToAnswer());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("start_datedirtyflag"))
                model.setStart_date(domain.getStartDate());
            if((Boolean) domain.getExtensionparams().get("nbr_channeldirtyflag"))
                model.setNbr_channel(domain.getNbrChannel());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("durationdirtyflag"))
                model.setDuration(domain.getDuration());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("livechat_channel_id_textdirtyflag"))
                model.setLivechat_channel_id_text(domain.getLivechatChannelIdText());
            if((Boolean) domain.getExtensionparams().get("channel_id_textdirtyflag"))
                model.setChannel_id_text(domain.getChannelIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("channel_iddirtyflag"))
                model.setChannel_id(domain.getChannelId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("livechat_channel_iddirtyflag"))
                model.setLivechat_channel_id(domain.getLivechatChannelId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Im_livechat_report_operator convert2Domain( im_livechat_report_operatorClientModel model ,Im_livechat_report_operator domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Im_livechat_report_operator();
        }

        if(model.getTime_to_answerDirtyFlag())
            domain.setTimeToAnswer(model.getTime_to_answer());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getStart_dateDirtyFlag())
            domain.setStartDate(model.getStart_date());
        if(model.getNbr_channelDirtyFlag())
            domain.setNbrChannel(model.getNbr_channel());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDurationDirtyFlag())
            domain.setDuration(model.getDuration());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getLivechat_channel_id_textDirtyFlag())
            domain.setLivechatChannelIdText(model.getLivechat_channel_id_text());
        if(model.getChannel_id_textDirtyFlag())
            domain.setChannelIdText(model.getChannel_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getChannel_idDirtyFlag())
            domain.setChannelId(model.getChannel_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getLivechat_channel_idDirtyFlag())
            domain.setLivechatChannelId(model.getLivechat_channel_id());
        return domain ;
    }

}

    



