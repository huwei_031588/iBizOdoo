package cn.ibizlab.odoo.core.odoo_mro.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [Maintenance Planned Parts] 对象
 */
@Data
public class Mro_task_parts_line extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 数量
     */
    @DEField(name = "parts_qty")
    @JSONField(name = "parts_qty")
    @JsonProperty("parts_qty")
    private Double partsQty;

    /**
     * 说明
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * Maintenance Task
     */
    @JSONField(name = "task_id_text")
    @JsonProperty("task_id_text")
    private String taskIdText;

    /**
     * 零件
     */
    @JSONField(name = "parts_id_text")
    @JsonProperty("parts_id_text")
    private String partsIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 单位
     */
    @JSONField(name = "parts_uom_text")
    @JsonProperty("parts_uom_text")
    private String partsUomText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 单位
     */
    @DEField(name = "parts_uom")
    @JSONField(name = "parts_uom")
    @JsonProperty("parts_uom")
    private Integer partsUom;

    /**
     * 零件
     */
    @DEField(name = "parts_id")
    @JSONField(name = "parts_id")
    @JsonProperty("parts_id")
    private Integer partsId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * Maintenance Task
     */
    @DEField(name = "task_id")
    @JSONField(name = "task_id")
    @JsonProperty("task_id")
    private Integer taskId;


    /**
     * 
     */
    @JSONField(name = "odootask")
    @JsonProperty("odootask")
    private cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task odooTask;

    /**
     * 
     */
    @JSONField(name = "odooparts")
    @JsonProperty("odooparts")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooParts;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoopartsuom")
    @JsonProperty("odoopartsuom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooPartsUom;




    /**
     * 设置 [数量]
     */
    public void setPartsQty(Double partsQty){
        this.partsQty = partsQty ;
        this.modify("parts_qty",partsQty);
    }
    /**
     * 设置 [说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [单位]
     */
    public void setPartsUom(Integer partsUom){
        this.partsUom = partsUom ;
        this.modify("parts_uom",partsUom);
    }
    /**
     * 设置 [零件]
     */
    public void setPartsId(Integer partsId){
        this.partsId = partsId ;
        this.modify("parts_id",partsId);
    }
    /**
     * 设置 [Maintenance Task]
     */
    public void setTaskId(Integer taskId){
        this.taskId = taskId ;
        this.modify("task_id",taskId);
    }

}


