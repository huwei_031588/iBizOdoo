package cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_wizard_invite;

import cn.ibizlab.odoo.core.odoo_mail.valuerule.validator.mail_wizard_invite.Mail_wizard_inviteIdDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Mail_wizard_invite
 * 属性：Id
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Mail_wizard_inviteIdDefaultValidator.class})
public @interface Mail_wizard_inviteIdDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
