package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_followers;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_followersSearchContext;

/**
 * 实体 [文档关注者] 存储对象
 */
public interface Mail_followersRepository extends Repository<Mail_followers> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_followers> searchDefault(Mail_followersSearchContext context);

    Mail_followers convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_followers domain , Mail_followers po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_followers convert2Domain( Mail_followers po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_followers domain) ;

}
