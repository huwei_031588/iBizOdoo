package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.sale_order_line;

/**
 * 实体[sale_order_line] 服务对象接口
 */
public interface sale_order_lineRepository{


    public sale_order_line createPO() ;
        public List<sale_order_line> search();

        public void removeBatch(String id);

        public void update(sale_order_line sale_order_line);

        public void createBatch(sale_order_line sale_order_line);

        public void remove(String id);

        public void get(String id);

        public void create(sale_order_line sale_order_line);

        public void updateBatch(sale_order_line sale_order_line);


}
