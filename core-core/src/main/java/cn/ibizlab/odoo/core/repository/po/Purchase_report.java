package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_reportSearchContext;

/**
 * 实体 [采购报表] 存储模型
 */
public interface Purchase_report{

    /**
     * 交货天数
     */
    Double getDelay_pass();

    void setDelay_pass(Double delay_pass);

    /**
     * 获取 [交货天数]脏标记
     */
    boolean getDelay_passDirtyFlag();

    /**
     * 批准日期
     */
    Timestamp getDate_approve();

    void setDate_approve(Timestamp date_approve);

    /**
     * 获取 [批准日期]脏标记
     */
    boolean getDate_approveDirtyFlag();

    /**
     * 体积
     */
    Double getVolume();

    void setVolume(Double volume);

    /**
     * 获取 [体积]脏标记
     */
    boolean getVolumeDirtyFlag();

    /**
     * # 明细行
     */
    Integer getNbr_lines();

    void setNbr_lines(Integer nbr_lines);

    /**
     * 获取 [# 明细行]脏标记
     */
    boolean getNbr_linesDirtyFlag();

    /**
     * 采购 - 标准单价
     */
    Double getNegociation();

    void setNegociation(Double negociation);

    /**
     * 获取 [采购 - 标准单价]脏标记
     */
    boolean getNegociationDirtyFlag();

    /**
     * 订单状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [订单状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 总价
     */
    Double getPrice_total();

    void setPrice_total(Double price_total);

    /**
     * 获取 [总价]脏标记
     */
    boolean getPrice_totalDirtyFlag();

    /**
     * 毛重
     */
    Double getWeight();

    void setWeight(Double weight);

    /**
     * 获取 [毛重]脏标记
     */
    boolean getWeightDirtyFlag();

    /**
     * 单据日期
     */
    Timestamp getDate_order();

    void setDate_order(Timestamp date_order);

    /**
     * 获取 [单据日期]脏标记
     */
    boolean getDate_orderDirtyFlag();

    /**
     * 平均价格
     */
    Double getPrice_average();

    void setPrice_average(Double price_average);

    /**
     * 获取 [平均价格]脏标记
     */
    boolean getPrice_averageDirtyFlag();

    /**
     * 验证天数
     */
    Double getDelay();

    void setDelay(Double delay);

    /**
     * 获取 [验证天数]脏标记
     */
    boolean getDelayDirtyFlag();

    /**
     * 数量
     */
    Double getUnit_quantity();

    void setUnit_quantity(Double unit_quantity);

    /**
     * 获取 [数量]脏标记
     */
    boolean getUnit_quantityDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 产品价值
     */
    Double getPrice_standard();

    void setPrice_standard(Double price_standard);

    /**
     * 获取 [产品价值]脏标记
     */
    boolean getPrice_standardDirtyFlag();

    /**
     * 产品种类
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [产品种类]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 供应商
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 产品模板
     */
    String getProduct_tmpl_id_text();

    void setProduct_tmpl_id_text(String product_tmpl_id_text);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_id_textDirtyFlag();

    /**
     * 业务伙伴国家
     */
    String getCountry_id_text();

    void setCountry_id_text(String country_id_text);

    /**
     * 获取 [业务伙伴国家]脏标记
     */
    boolean getCountry_id_textDirtyFlag();

    /**
     * 仓库
     */
    String getPicking_type_id_text();

    void setPicking_type_id_text(String picking_type_id_text);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getPicking_type_id_textDirtyFlag();

    /**
     * 税科目调整
     */
    String getFiscal_position_id_text();

    void setFiscal_position_id_text(String fiscal_position_id_text);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getFiscal_position_id_textDirtyFlag();

    /**
     * 分析账户
     */
    String getAccount_analytic_id_text();

    void setAccount_analytic_id_text(String account_analytic_id_text);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_analytic_id_textDirtyFlag();

    /**
     * 采购员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [采购员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 商业实体
     */
    String getCommercial_partner_id_text();

    void setCommercial_partner_id_text(String commercial_partner_id_text);

    /**
     * 获取 [商业实体]脏标记
     */
    boolean getCommercial_partner_id_textDirtyFlag();

    /**
     * 参考计量单位
     */
    String getProduct_uom_text();

    void setProduct_uom_text(String product_uom_text);

    /**
     * 获取 [参考计量单位]脏标记
     */
    boolean getProduct_uom_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 产品种类
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [产品种类]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 仓库
     */
    Integer getPicking_type_id();

    void setPicking_type_id(Integer picking_type_id);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getPicking_type_idDirtyFlag();

    /**
     * 采购员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [采购员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 商业实体
     */
    Integer getCommercial_partner_id();

    void setCommercial_partner_id(Integer commercial_partner_id);

    /**
     * 获取 [商业实体]脏标记
     */
    boolean getCommercial_partner_idDirtyFlag();

    /**
     * 产品模板
     */
    Integer getProduct_tmpl_id();

    void setProduct_tmpl_id(Integer product_tmpl_id);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_idDirtyFlag();

    /**
     * 供应商
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 参考计量单位
     */
    Integer getProduct_uom();

    void setProduct_uom(Integer product_uom);

    /**
     * 获取 [参考计量单位]脏标记
     */
    boolean getProduct_uomDirtyFlag();

    /**
     * 业务伙伴国家
     */
    Integer getCountry_id();

    void setCountry_id(Integer country_id);

    /**
     * 获取 [业务伙伴国家]脏标记
     */
    boolean getCountry_idDirtyFlag();

    /**
     * 税科目调整
     */
    Integer getFiscal_position_id();

    void setFiscal_position_id(Integer fiscal_position_id);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getFiscal_position_idDirtyFlag();

    /**
     * 分析账户
     */
    Integer getAccount_analytic_id();

    void setAccount_analytic_id(Integer account_analytic_id);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_analytic_idDirtyFlag();

}
