package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_expense_sheet;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheetSearchContext;

/**
 * 实体 [费用报表] 存储对象
 */
public interface Hr_expense_sheetRepository extends Repository<Hr_expense_sheet> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_expense_sheet> searchDefault(Hr_expense_sheetSearchContext context);

    Hr_expense_sheet convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet domain , Hr_expense_sheet po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet convert2Domain( Hr_expense_sheet po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet domain) ;

}
