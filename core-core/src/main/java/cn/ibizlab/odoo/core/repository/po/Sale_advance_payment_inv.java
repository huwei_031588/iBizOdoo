package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;

/**
 * 实体 [销售预付款发票] 存储模型
 */
public interface Sale_advance_payment_inv{

    /**
     * 销项税
     */
    String getDeposit_taxes_id();

    void setDeposit_taxes_id(String deposit_taxes_id);

    /**
     * 获取 [销项税]脏标记
     */
    boolean getDeposit_taxes_idDirtyFlag();

    /**
     * 预付定金总额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [预付定金总额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 订购数量
     */
    Integer getCount();

    void setCount(Integer count);

    /**
     * 获取 [订购数量]脏标记
     */
    boolean getCountDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 你要开什么发票？
     */
    String getAdvance_payment_method();

    void setAdvance_payment_method(String advance_payment_method);

    /**
     * 获取 [你要开什么发票？]脏标记
     */
    boolean getAdvance_payment_methodDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 收入科目
     */
    String getDeposit_account_id_text();

    void setDeposit_account_id_text(String deposit_account_id_text);

    /**
     * 获取 [收入科目]脏标记
     */
    boolean getDeposit_account_id_textDirtyFlag();

    /**
     * 预付定金产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [预付定金产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 预付定金产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [预付定金产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 收入科目
     */
    Integer getDeposit_account_id();

    void setDeposit_account_id(Integer deposit_account_id);

    /**
     * 获取 [收入科目]脏标记
     */
    boolean getDeposit_account_idDirtyFlag();

}
