package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.lunch_order;

/**
 * 实体[lunch_order] 服务对象接口
 */
public interface lunch_orderRepository{


    public lunch_order createPO() ;
        public void removeBatch(String id);

        public void createBatch(lunch_order lunch_order);

        public void remove(String id);

        public void create(lunch_order lunch_order);

        public void updateBatch(lunch_order lunch_order);

        public void get(String id);

        public List<lunch_order> search();

        public void update(lunch_order lunch_order);


}
