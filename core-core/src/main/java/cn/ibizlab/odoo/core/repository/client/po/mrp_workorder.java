package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mrp_workorder] 对象
 */
public interface mrp_workorder {

    public String getActive_move_line_ids();

    public void setActive_move_line_ids(String active_move_line_ids);

    public Double getCapacity();

    public void setCapacity(Double capacity);

    public Integer getColor();

    public void setColor(Integer color);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_finished();

    public void setDate_finished(Timestamp date_finished);

    public Timestamp getDate_planned_finished();

    public void setDate_planned_finished(Timestamp date_planned_finished);

    public Timestamp getDate_planned_start();

    public void setDate_planned_start(Timestamp date_planned_start);

    public Timestamp getDate_start();

    public void setDate_start(Timestamp date_start);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Double getDuration();

    public void setDuration(Double duration);

    public Double getDuration_expected();

    public void setDuration_expected(Double duration_expected);

    public Integer getDuration_percent();

    public void setDuration_percent(Integer duration_percent);

    public Double getDuration_unit();

    public void setDuration_unit(Double duration_unit);

    public Integer getFinal_lot_id();

    public void setFinal_lot_id(Integer final_lot_id);

    public String getFinal_lot_id_text();

    public void setFinal_lot_id_text(String final_lot_id_text);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_first_wo();

    public void setIs_first_wo(String is_first_wo);

    public String getIs_produced();

    public void setIs_produced(String is_produced);

    public String getIs_user_working();

    public void setIs_user_working(String is_user_working);

    public String getLast_working_user_id();

    public void setLast_working_user_id(String last_working_user_id);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getMove_line_ids();

    public void setMove_line_ids(String move_line_ids);

    public String getMove_raw_ids();

    public void setMove_raw_ids(String move_raw_ids);

    public String getName();

    public void setName(String name);

    public Integer getNext_work_order_id();

    public void setNext_work_order_id(Integer next_work_order_id);

    public String getNext_work_order_id_text();

    public void setNext_work_order_id_text(String next_work_order_id_text);

    public Integer getOperation_id();

    public void setOperation_id(Integer operation_id);

    public String getOperation_id_text();

    public void setOperation_id_text(String operation_id_text);

    public String getProduction_availability();

    public void setProduction_availability(String production_availability);

    public Timestamp getProduction_date();

    public void setProduction_date(Timestamp production_date);

    public Integer getProduction_id();

    public void setProduction_id(Integer production_id);

    public String getProduction_id_text();

    public void setProduction_id_text(String production_id_text);

    public String getProduction_state();

    public void setProduction_state(String production_state);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public String getProduct_tracking();

    public void setProduct_tracking(String product_tracking);

    public Integer getProduct_uom_id();

    public void setProduct_uom_id(Integer product_uom_id);

    public String getProduct_uom_id_text();

    public void setProduct_uom_id_text(String product_uom_id_text);

    public Double getQty_produced();

    public void setQty_produced(Double qty_produced);

    public Double getQty_producing();

    public void setQty_producing(Double qty_producing);

    public Double getQty_production();

    public void setQty_production(Double qty_production);

    public Double getQty_remaining();

    public void setQty_remaining(Double qty_remaining);

    public Integer getScrap_count();

    public void setScrap_count(Integer scrap_count);

    public String getScrap_ids();

    public void setScrap_ids(String scrap_ids);

    public String getState();

    public void setState(String state);

    public String getTime_ids();

    public void setTime_ids(String time_ids);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Integer getWorkcenter_id();

    public void setWorkcenter_id(Integer workcenter_id);

    public String getWorkcenter_id_text();

    public void setWorkcenter_id_text(String workcenter_id_text);

    public String getWorking_state();

    public void setWorking_state(String working_state);

    public String getWorking_user_ids();

    public void setWorking_user_ids(String working_user_ids);

    public byte[] getWorksheet();

    public void setWorksheet(byte[] worksheet);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
