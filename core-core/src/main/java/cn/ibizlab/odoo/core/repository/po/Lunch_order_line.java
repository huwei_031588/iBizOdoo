package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_lineSearchContext;

/**
 * 实体 [工作餐订单行] 存储模型
 */
public interface Lunch_order_line{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 笔记
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [笔记]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 现金划拨
     */
    String getCashmove();

    void setCashmove(String cashmove);

    /**
     * 获取 [现金划拨]脏标记
     */
    boolean getCashmoveDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 供应商
     */
    String getSupplier_text();

    void setSupplier_text(String supplier_text);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getSupplier_textDirtyFlag();

    /**
     * 用户
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 产品种类
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [产品种类]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 价格
     */
    Double getPrice();

    void setPrice(Double price);

    /**
     * 获取 [价格]脏标记
     */
    boolean getPriceDirtyFlag();

    /**
     * 产品名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [产品名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 用户
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 供应商
     */
    Integer getSupplier();

    void setSupplier(Integer supplier);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getSupplierDirtyFlag();

    /**
     * 产品种类
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [产品种类]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 订单
     */
    Integer getOrder_id();

    void setOrder_id(Integer order_id);

    /**
     * 获取 [订单]脏标记
     */
    boolean getOrder_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
