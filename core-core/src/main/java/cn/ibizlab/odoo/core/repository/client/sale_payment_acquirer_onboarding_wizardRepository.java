package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.sale_payment_acquirer_onboarding_wizard;

/**
 * 实体[sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface sale_payment_acquirer_onboarding_wizardRepository{


    public sale_payment_acquirer_onboarding_wizard createPO() ;
        public void update(sale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

        public void createBatch(sale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

        public void remove(String id);

        public void get(String id);

        public void removeBatch(String id);

        public List<sale_payment_acquirer_onboarding_wizard> search();

        public void create(sale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

        public void updateBatch(sale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);


}
