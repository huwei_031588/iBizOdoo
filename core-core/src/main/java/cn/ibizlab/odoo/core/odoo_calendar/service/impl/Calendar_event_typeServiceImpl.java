package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_event_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_event_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_event_typeOdooClient;
import cn.ibizlab.odoo.core.odoo_calendar.clientmodel.calendar_event_typeClientModel;

/**
 * 实体[事件会议类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_event_typeServiceImpl implements ICalendar_event_typeService {

    @Autowired
    calendar_event_typeOdooClient calendar_event_typeOdooClient;


    @Override
    public Calendar_event_type get(Integer id) {
        calendar_event_typeClientModel clientModel = new calendar_event_typeClientModel();
        clientModel.setId(id);
		calendar_event_typeOdooClient.get(clientModel);
        Calendar_event_type et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Calendar_event_type();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Calendar_event_type et) {
        calendar_event_typeClientModel clientModel = convert2Model(et,null);
		calendar_event_typeOdooClient.create(clientModel);
        Calendar_event_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_event_type> list){
    }

    @Override
    public boolean remove(Integer id) {
        calendar_event_typeClientModel clientModel = new calendar_event_typeClientModel();
        clientModel.setId(id);
		calendar_event_typeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Calendar_event_type et) {
        calendar_event_typeClientModel clientModel = convert2Model(et,null);
		calendar_event_typeOdooClient.update(clientModel);
        Calendar_event_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Calendar_event_type> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_event_type> searchDefault(Calendar_event_typeSearchContext context) {
        List<Calendar_event_type> list = new ArrayList<Calendar_event_type>();
        Page<calendar_event_typeClientModel> clientModelList = calendar_event_typeOdooClient.search(context);
        for(calendar_event_typeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Calendar_event_type>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public calendar_event_typeClientModel convert2Model(Calendar_event_type domain , calendar_event_typeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new calendar_event_typeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Calendar_event_type convert2Domain( calendar_event_typeClientModel model ,Calendar_event_type domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Calendar_event_type();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



