package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;

/**
 * 实体 [物料清单明细行] 存储模型
 */
public interface Mrp_bom_line{

    /**
     * 应用于变体
     */
    String getAttribute_value_ids();

    void setAttribute_value_ids(String attribute_value_ids);

    /**
     * 获取 [应用于变体]脏标记
     */
    boolean getAttribute_value_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 数量
     */
    Double getProduct_qty();

    void setProduct_qty(Double product_qty);

    /**
     * 获取 [数量]脏标记
     */
    boolean getProduct_qtyDirtyFlag();

    /**
     * 参考BOM中的BOM行
     */
    String getChild_line_ids();

    void setChild_line_ids(String child_line_ids);

    /**
     * 获取 [参考BOM中的BOM行]脏标记
     */
    boolean getChild_line_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 有效的产品属性值
     */
    String getValid_product_attribute_value_ids();

    void setValid_product_attribute_value_ids(String valid_product_attribute_value_ids);

    /**
     * 获取 [有效的产品属性值]脏标记
     */
    boolean getValid_product_attribute_value_idsDirtyFlag();

    /**
     * 有附件
     */
    String getHas_attachments();

    void setHas_attachments(String has_attachments);

    /**
     * 获取 [有附件]脏标记
     */
    boolean getHas_attachmentsDirtyFlag();

    /**
     * 子 BOM
     */
    Integer getChild_bom_id();

    void setChild_bom_id(Integer child_bom_id);

    /**
     * 获取 [子 BOM]脏标记
     */
    boolean getChild_bom_idDirtyFlag();

    /**
     * 产品模板
     */
    Integer getProduct_tmpl_id();

    void setProduct_tmpl_id(Integer product_tmpl_id);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_idDirtyFlag();

    /**
     * 计量单位
     */
    String getProduct_uom_id_text();

    void setProduct_uom_id_text(String product_uom_id_text);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_id_textDirtyFlag();

    /**
     * 工艺
     */
    String getRouting_id_text();

    void setRouting_id_text(String routing_id_text);

    /**
     * 获取 [工艺]脏标记
     */
    boolean getRouting_id_textDirtyFlag();

    /**
     * 父产品模板
     */
    Integer getParent_product_tmpl_id();

    void setParent_product_tmpl_id(Integer parent_product_tmpl_id);

    /**
     * 获取 [父产品模板]脏标记
     */
    boolean getParent_product_tmpl_idDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 零件
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [零件]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 投料作业
     */
    String getOperation_id_text();

    void setOperation_id_text(String operation_id_text);

    /**
     * 获取 [投料作业]脏标记
     */
    boolean getOperation_id_textDirtyFlag();

    /**
     * 投料作业
     */
    Integer getOperation_id();

    void setOperation_id(Integer operation_id);

    /**
     * 获取 [投料作业]脏标记
     */
    boolean getOperation_idDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 父级 BoM
     */
    Integer getBom_id();

    void setBom_id(Integer bom_id);

    /**
     * 获取 [父级 BoM]脏标记
     */
    boolean getBom_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 工艺
     */
    Integer getRouting_id();

    void setRouting_id(Integer routing_id);

    /**
     * 获取 [工艺]脏标记
     */
    boolean getRouting_idDirtyFlag();

    /**
     * 零件
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [零件]脏标记
     */
    boolean getProduct_idDirtyFlag();

}
