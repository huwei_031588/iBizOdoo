package cn.ibizlab.odoo.core.odoo_mail.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_mail] 对象
 */
public class mail_mailClientModel implements Serializable{

    /**
     * 添加签名
     */
    public String add_sign;

    @JsonIgnore
    public boolean add_signDirtyFlag;
    
    /**
     * 附件
     */
    public String attachment_ids;

    @JsonIgnore
    public boolean attachment_idsDirtyFlag;
    
    /**
     * 作者头像
     */
    public byte[] author_avatar;

    @JsonIgnore
    public boolean author_avatarDirtyFlag;
    
    /**
     * 作者
     */
    public Integer author_id;

    @JsonIgnore
    public boolean author_idDirtyFlag;
    
    /**
     * 自动删除
     */
    public String auto_delete;

    @JsonIgnore
    public boolean auto_deleteDirtyFlag;
    
    /**
     * 内容
     */
    public String body;

    @JsonIgnore
    public boolean bodyDirtyFlag;
    
    /**
     * 富文本内容
     */
    public String body_html;

    @JsonIgnore
    public boolean body_htmlDirtyFlag;
    
    /**
     * 渠道
     */
    public String channel_ids;

    @JsonIgnore
    public boolean channel_idsDirtyFlag;
    
    /**
     * 下级消息
     */
    public String child_ids;

    @JsonIgnore
    public boolean child_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 抄送
     */
    public String email_cc;

    @JsonIgnore
    public boolean email_ccDirtyFlag;
    
    /**
     * 从
     */
    public String email_from;

    @JsonIgnore
    public boolean email_fromDirtyFlag;
    
    /**
     * 至
     */
    public String email_to;

    @JsonIgnore
    public boolean email_toDirtyFlag;
    
    /**
     * 失败原因
     */
    public String failure_reason;

    @JsonIgnore
    public boolean failure_reasonDirtyFlag;
    
    /**
     * 收件服务器
     */
    public Integer fetchmail_server_id;

    @JsonIgnore
    public boolean fetchmail_server_idDirtyFlag;
    
    /**
     * 收件服务器
     */
    public String fetchmail_server_id_text;

    @JsonIgnore
    public boolean fetchmail_server_id_textDirtyFlag;
    
    /**
     * 有误差
     */
    public String has_error;

    @JsonIgnore
    public boolean has_errorDirtyFlag;
    
    /**
     * 标题
     */
    public String headers;

    @JsonIgnore
    public boolean headersDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 布局
     */
    public String layout;

    @JsonIgnore
    public boolean layoutDirtyFlag;
    
    /**
     * 群发邮件
     */
    public Integer mailing_id;

    @JsonIgnore
    public boolean mailing_idDirtyFlag;
    
    /**
     * 群发邮件
     */
    public String mailing_id_text;

    @JsonIgnore
    public boolean mailing_id_textDirtyFlag;
    
    /**
     * 邮件活动类型
     */
    public Integer mail_activity_type_id;

    @JsonIgnore
    public boolean mail_activity_type_idDirtyFlag;
    
    /**
     * 消息
     */
    public Integer mail_message_id;

    @JsonIgnore
    public boolean mail_message_idDirtyFlag;
    
    /**
     * 邮件发送服务器
     */
    public Integer mail_server_id;

    @JsonIgnore
    public boolean mail_server_idDirtyFlag;
    
    /**
     * 消息ID
     */
    public String message_id;

    @JsonIgnore
    public boolean message_idDirtyFlag;
    
    /**
     * 类型
     */
    public String message_type;

    @JsonIgnore
    public boolean message_typeDirtyFlag;
    
    /**
     * 相关的文档模型
     */
    public String model;

    @JsonIgnore
    public boolean modelDirtyFlag;
    
    /**
     * 管理状态
     */
    public String moderation_status;

    @JsonIgnore
    public boolean moderation_statusDirtyFlag;
    
    /**
     * 管理员
     */
    public Integer moderator_id;

    @JsonIgnore
    public boolean moderator_idDirtyFlag;
    
    /**
     * 待处理
     */
    public String needaction;

    @JsonIgnore
    public boolean needactionDirtyFlag;
    
    /**
     * 待处理的业务伙伴
     */
    public String needaction_partner_ids;

    @JsonIgnore
    public boolean needaction_partner_idsDirtyFlag;
    
    /**
     * 需审核
     */
    public String need_moderation;

    @JsonIgnore
    public boolean need_moderationDirtyFlag;
    
    /**
     * 通知
     */
    public String notification;

    @JsonIgnore
    public boolean notificationDirtyFlag;
    
    /**
     * 通知
     */
    public String notification_ids;

    @JsonIgnore
    public boolean notification_idsDirtyFlag;
    
    /**
     * 无响应
     */
    public String no_auto_thread;

    @JsonIgnore
    public boolean no_auto_threadDirtyFlag;
    
    /**
     * 上级消息
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 收件人
     */
    public String partner_ids;

    @JsonIgnore
    public boolean partner_idsDirtyFlag;
    
    /**
     * 相关评级
     */
    public String rating_ids;

    @JsonIgnore
    public boolean rating_idsDirtyFlag;
    
    /**
     * 评级值
     */
    public Double rating_value;

    @JsonIgnore
    public boolean rating_valueDirtyFlag;
    
    /**
     * 至（合作伙伴）
     */
    public String recipient_ids;

    @JsonIgnore
    public boolean recipient_idsDirtyFlag;
    
    /**
     * 消息记录名称
     */
    public String record_name;

    @JsonIgnore
    public boolean record_nameDirtyFlag;
    
    /**
     * 参考
     */
    public String references;

    @JsonIgnore
    public boolean referencesDirtyFlag;
    
    /**
     * 回复 至
     */
    public String reply_to;

    @JsonIgnore
    public boolean reply_toDirtyFlag;
    
    /**
     * 相关文档编号
     */
    public Integer res_id;

    @JsonIgnore
    public boolean res_idDirtyFlag;
    
    /**
     * 安排的发送日期
     */
    public String scheduled_date;

    @JsonIgnore
    public boolean scheduled_dateDirtyFlag;
    
    /**
     * 加星的邮件
     */
    public String starred;

    @JsonIgnore
    public boolean starredDirtyFlag;
    
    /**
     * 收藏夹
     */
    public String starred_partner_ids;

    @JsonIgnore
    public boolean starred_partner_idsDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 统计信息
     */
    public String statistics_ids;

    @JsonIgnore
    public boolean statistics_idsDirtyFlag;
    
    /**
     * 主题
     */
    public String subject;

    @JsonIgnore
    public boolean subjectDirtyFlag;
    
    /**
     * 子类型
     */
    public Integer subtype_id;

    @JsonIgnore
    public boolean subtype_idDirtyFlag;
    
    /**
     * 追踪值
     */
    public String tracking_value_ids;

    @JsonIgnore
    public boolean tracking_value_idsDirtyFlag;
    
    /**
     * 已发布
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [添加签名]
     */
    @JsonProperty("add_sign")
    public String getAdd_sign(){
        return this.add_sign ;
    }

    /**
     * 设置 [添加签名]
     */
    @JsonProperty("add_sign")
    public void setAdd_sign(String  add_sign){
        this.add_sign = add_sign ;
        this.add_signDirtyFlag = true ;
    }

     /**
     * 获取 [添加签名]脏标记
     */
    @JsonIgnore
    public boolean getAdd_signDirtyFlag(){
        return this.add_signDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("attachment_ids")
    public String getAttachment_ids(){
        return this.attachment_ids ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("attachment_ids")
    public void setAttachment_ids(String  attachment_ids){
        this.attachment_ids = attachment_ids ;
        this.attachment_idsDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_idsDirtyFlag(){
        return this.attachment_idsDirtyFlag ;
    }   

    /**
     * 获取 [作者头像]
     */
    @JsonProperty("author_avatar")
    public byte[] getAuthor_avatar(){
        return this.author_avatar ;
    }

    /**
     * 设置 [作者头像]
     */
    @JsonProperty("author_avatar")
    public void setAuthor_avatar(byte[]  author_avatar){
        this.author_avatar = author_avatar ;
        this.author_avatarDirtyFlag = true ;
    }

     /**
     * 获取 [作者头像]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_avatarDirtyFlag(){
        return this.author_avatarDirtyFlag ;
    }   

    /**
     * 获取 [作者]
     */
    @JsonProperty("author_id")
    public Integer getAuthor_id(){
        return this.author_id ;
    }

    /**
     * 设置 [作者]
     */
    @JsonProperty("author_id")
    public void setAuthor_id(Integer  author_id){
        this.author_id = author_id ;
        this.author_idDirtyFlag = true ;
    }

     /**
     * 获取 [作者]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_idDirtyFlag(){
        return this.author_idDirtyFlag ;
    }   

    /**
     * 获取 [自动删除]
     */
    @JsonProperty("auto_delete")
    public String getAuto_delete(){
        return this.auto_delete ;
    }

    /**
     * 设置 [自动删除]
     */
    @JsonProperty("auto_delete")
    public void setAuto_delete(String  auto_delete){
        this.auto_delete = auto_delete ;
        this.auto_deleteDirtyFlag = true ;
    }

     /**
     * 获取 [自动删除]脏标记
     */
    @JsonIgnore
    public boolean getAuto_deleteDirtyFlag(){
        return this.auto_deleteDirtyFlag ;
    }   

    /**
     * 获取 [内容]
     */
    @JsonProperty("body")
    public String getBody(){
        return this.body ;
    }

    /**
     * 设置 [内容]
     */
    @JsonProperty("body")
    public void setBody(String  body){
        this.body = body ;
        this.bodyDirtyFlag = true ;
    }

     /**
     * 获取 [内容]脏标记
     */
    @JsonIgnore
    public boolean getBodyDirtyFlag(){
        return this.bodyDirtyFlag ;
    }   

    /**
     * 获取 [富文本内容]
     */
    @JsonProperty("body_html")
    public String getBody_html(){
        return this.body_html ;
    }

    /**
     * 设置 [富文本内容]
     */
    @JsonProperty("body_html")
    public void setBody_html(String  body_html){
        this.body_html = body_html ;
        this.body_htmlDirtyFlag = true ;
    }

     /**
     * 获取 [富文本内容]脏标记
     */
    @JsonIgnore
    public boolean getBody_htmlDirtyFlag(){
        return this.body_htmlDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return this.channel_ids ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return this.channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [下级消息]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return this.child_ids ;
    }

    /**
     * 设置 [下级消息]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

     /**
     * 获取 [下级消息]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return this.child_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [抄送]
     */
    @JsonProperty("email_cc")
    public String getEmail_cc(){
        return this.email_cc ;
    }

    /**
     * 设置 [抄送]
     */
    @JsonProperty("email_cc")
    public void setEmail_cc(String  email_cc){
        this.email_cc = email_cc ;
        this.email_ccDirtyFlag = true ;
    }

     /**
     * 获取 [抄送]脏标记
     */
    @JsonIgnore
    public boolean getEmail_ccDirtyFlag(){
        return this.email_ccDirtyFlag ;
    }   

    /**
     * 获取 [从]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return this.email_from ;
    }

    /**
     * 设置 [从]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

     /**
     * 获取 [从]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return this.email_fromDirtyFlag ;
    }   

    /**
     * 获取 [至]
     */
    @JsonProperty("email_to")
    public String getEmail_to(){
        return this.email_to ;
    }

    /**
     * 设置 [至]
     */
    @JsonProperty("email_to")
    public void setEmail_to(String  email_to){
        this.email_to = email_to ;
        this.email_toDirtyFlag = true ;
    }

     /**
     * 获取 [至]脏标记
     */
    @JsonIgnore
    public boolean getEmail_toDirtyFlag(){
        return this.email_toDirtyFlag ;
    }   

    /**
     * 获取 [失败原因]
     */
    @JsonProperty("failure_reason")
    public String getFailure_reason(){
        return this.failure_reason ;
    }

    /**
     * 设置 [失败原因]
     */
    @JsonProperty("failure_reason")
    public void setFailure_reason(String  failure_reason){
        this.failure_reason = failure_reason ;
        this.failure_reasonDirtyFlag = true ;
    }

     /**
     * 获取 [失败原因]脏标记
     */
    @JsonIgnore
    public boolean getFailure_reasonDirtyFlag(){
        return this.failure_reasonDirtyFlag ;
    }   

    /**
     * 获取 [收件服务器]
     */
    @JsonProperty("fetchmail_server_id")
    public Integer getFetchmail_server_id(){
        return this.fetchmail_server_id ;
    }

    /**
     * 设置 [收件服务器]
     */
    @JsonProperty("fetchmail_server_id")
    public void setFetchmail_server_id(Integer  fetchmail_server_id){
        this.fetchmail_server_id = fetchmail_server_id ;
        this.fetchmail_server_idDirtyFlag = true ;
    }

     /**
     * 获取 [收件服务器]脏标记
     */
    @JsonIgnore
    public boolean getFetchmail_server_idDirtyFlag(){
        return this.fetchmail_server_idDirtyFlag ;
    }   

    /**
     * 获取 [收件服务器]
     */
    @JsonProperty("fetchmail_server_id_text")
    public String getFetchmail_server_id_text(){
        return this.fetchmail_server_id_text ;
    }

    /**
     * 设置 [收件服务器]
     */
    @JsonProperty("fetchmail_server_id_text")
    public void setFetchmail_server_id_text(String  fetchmail_server_id_text){
        this.fetchmail_server_id_text = fetchmail_server_id_text ;
        this.fetchmail_server_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [收件服务器]脏标记
     */
    @JsonIgnore
    public boolean getFetchmail_server_id_textDirtyFlag(){
        return this.fetchmail_server_id_textDirtyFlag ;
    }   

    /**
     * 获取 [有误差]
     */
    @JsonProperty("has_error")
    public String getHas_error(){
        return this.has_error ;
    }

    /**
     * 设置 [有误差]
     */
    @JsonProperty("has_error")
    public void setHas_error(String  has_error){
        this.has_error = has_error ;
        this.has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [有误差]脏标记
     */
    @JsonIgnore
    public boolean getHas_errorDirtyFlag(){
        return this.has_errorDirtyFlag ;
    }   

    /**
     * 获取 [标题]
     */
    @JsonProperty("headers")
    public String getHeaders(){
        return this.headers ;
    }

    /**
     * 设置 [标题]
     */
    @JsonProperty("headers")
    public void setHeaders(String  headers){
        this.headers = headers ;
        this.headersDirtyFlag = true ;
    }

     /**
     * 获取 [标题]脏标记
     */
    @JsonIgnore
    public boolean getHeadersDirtyFlag(){
        return this.headersDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [布局]
     */
    @JsonProperty("layout")
    public String getLayout(){
        return this.layout ;
    }

    /**
     * 设置 [布局]
     */
    @JsonProperty("layout")
    public void setLayout(String  layout){
        this.layout = layout ;
        this.layoutDirtyFlag = true ;
    }

     /**
     * 获取 [布局]脏标记
     */
    @JsonIgnore
    public boolean getLayoutDirtyFlag(){
        return this.layoutDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件]
     */
    @JsonProperty("mailing_id")
    public Integer getMailing_id(){
        return this.mailing_id ;
    }

    /**
     * 设置 [群发邮件]
     */
    @JsonProperty("mailing_id")
    public void setMailing_id(Integer  mailing_id){
        this.mailing_id = mailing_id ;
        this.mailing_idDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件]脏标记
     */
    @JsonIgnore
    public boolean getMailing_idDirtyFlag(){
        return this.mailing_idDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件]
     */
    @JsonProperty("mailing_id_text")
    public String getMailing_id_text(){
        return this.mailing_id_text ;
    }

    /**
     * 设置 [群发邮件]
     */
    @JsonProperty("mailing_id_text")
    public void setMailing_id_text(String  mailing_id_text){
        this.mailing_id_text = mailing_id_text ;
        this.mailing_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件]脏标记
     */
    @JsonIgnore
    public boolean getMailing_id_textDirtyFlag(){
        return this.mailing_id_textDirtyFlag ;
    }   

    /**
     * 获取 [邮件活动类型]
     */
    @JsonProperty("mail_activity_type_id")
    public Integer getMail_activity_type_id(){
        return this.mail_activity_type_id ;
    }

    /**
     * 设置 [邮件活动类型]
     */
    @JsonProperty("mail_activity_type_id")
    public void setMail_activity_type_id(Integer  mail_activity_type_id){
        this.mail_activity_type_id = mail_activity_type_id ;
        this.mail_activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件活动类型]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_idDirtyFlag(){
        return this.mail_activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("mail_message_id")
    public Integer getMail_message_id(){
        return this.mail_message_id ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("mail_message_id")
    public void setMail_message_id(Integer  mail_message_id){
        this.mail_message_id = mail_message_id ;
        this.mail_message_idDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMail_message_idDirtyFlag(){
        return this.mail_message_idDirtyFlag ;
    }   

    /**
     * 获取 [邮件发送服务器]
     */
    @JsonProperty("mail_server_id")
    public Integer getMail_server_id(){
        return this.mail_server_id ;
    }

    /**
     * 设置 [邮件发送服务器]
     */
    @JsonProperty("mail_server_id")
    public void setMail_server_id(Integer  mail_server_id){
        this.mail_server_id = mail_server_id ;
        this.mail_server_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件发送服务器]脏标记
     */
    @JsonIgnore
    public boolean getMail_server_idDirtyFlag(){
        return this.mail_server_idDirtyFlag ;
    }   

    /**
     * 获取 [消息ID]
     */
    @JsonProperty("message_id")
    public String getMessage_id(){
        return this.message_id ;
    }

    /**
     * 设置 [消息ID]
     */
    @JsonProperty("message_id")
    public void setMessage_id(String  message_id){
        this.message_id = message_id ;
        this.message_idDirtyFlag = true ;
    }

     /**
     * 获取 [消息ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idDirtyFlag(){
        return this.message_idDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("message_type")
    public String getMessage_type(){
        return this.message_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("message_type")
    public void setMessage_type(String  message_type){
        this.message_type = message_type ;
        this.message_typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getMessage_typeDirtyFlag(){
        return this.message_typeDirtyFlag ;
    }   

    /**
     * 获取 [相关的文档模型]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [相关的文档模型]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

     /**
     * 获取 [相关的文档模型]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }   

    /**
     * 获取 [管理状态]
     */
    @JsonProperty("moderation_status")
    public String getModeration_status(){
        return this.moderation_status ;
    }

    /**
     * 设置 [管理状态]
     */
    @JsonProperty("moderation_status")
    public void setModeration_status(String  moderation_status){
        this.moderation_status = moderation_status ;
        this.moderation_statusDirtyFlag = true ;
    }

     /**
     * 获取 [管理状态]脏标记
     */
    @JsonIgnore
    public boolean getModeration_statusDirtyFlag(){
        return this.moderation_statusDirtyFlag ;
    }   

    /**
     * 获取 [管理员]
     */
    @JsonProperty("moderator_id")
    public Integer getModerator_id(){
        return this.moderator_id ;
    }

    /**
     * 设置 [管理员]
     */
    @JsonProperty("moderator_id")
    public void setModerator_id(Integer  moderator_id){
        this.moderator_id = moderator_id ;
        this.moderator_idDirtyFlag = true ;
    }

     /**
     * 获取 [管理员]脏标记
     */
    @JsonIgnore
    public boolean getModerator_idDirtyFlag(){
        return this.moderator_idDirtyFlag ;
    }   

    /**
     * 获取 [待处理]
     */
    @JsonProperty("needaction")
    public String getNeedaction(){
        return this.needaction ;
    }

    /**
     * 设置 [待处理]
     */
    @JsonProperty("needaction")
    public void setNeedaction(String  needaction){
        this.needaction = needaction ;
        this.needactionDirtyFlag = true ;
    }

     /**
     * 获取 [待处理]脏标记
     */
    @JsonIgnore
    public boolean getNeedactionDirtyFlag(){
        return this.needactionDirtyFlag ;
    }   

    /**
     * 获取 [待处理的业务伙伴]
     */
    @JsonProperty("needaction_partner_ids")
    public String getNeedaction_partner_ids(){
        return this.needaction_partner_ids ;
    }

    /**
     * 设置 [待处理的业务伙伴]
     */
    @JsonProperty("needaction_partner_ids")
    public void setNeedaction_partner_ids(String  needaction_partner_ids){
        this.needaction_partner_ids = needaction_partner_ids ;
        this.needaction_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [待处理的业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getNeedaction_partner_idsDirtyFlag(){
        return this.needaction_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [需审核]
     */
    @JsonProperty("need_moderation")
    public String getNeed_moderation(){
        return this.need_moderation ;
    }

    /**
     * 设置 [需审核]
     */
    @JsonProperty("need_moderation")
    public void setNeed_moderation(String  need_moderation){
        this.need_moderation = need_moderation ;
        this.need_moderationDirtyFlag = true ;
    }

     /**
     * 获取 [需审核]脏标记
     */
    @JsonIgnore
    public boolean getNeed_moderationDirtyFlag(){
        return this.need_moderationDirtyFlag ;
    }   

    /**
     * 获取 [通知]
     */
    @JsonProperty("notification")
    public String getNotification(){
        return this.notification ;
    }

    /**
     * 设置 [通知]
     */
    @JsonProperty("notification")
    public void setNotification(String  notification){
        this.notification = notification ;
        this.notificationDirtyFlag = true ;
    }

     /**
     * 获取 [通知]脏标记
     */
    @JsonIgnore
    public boolean getNotificationDirtyFlag(){
        return this.notificationDirtyFlag ;
    }   

    /**
     * 获取 [通知]
     */
    @JsonProperty("notification_ids")
    public String getNotification_ids(){
        return this.notification_ids ;
    }

    /**
     * 设置 [通知]
     */
    @JsonProperty("notification_ids")
    public void setNotification_ids(String  notification_ids){
        this.notification_ids = notification_ids ;
        this.notification_idsDirtyFlag = true ;
    }

     /**
     * 获取 [通知]脏标记
     */
    @JsonIgnore
    public boolean getNotification_idsDirtyFlag(){
        return this.notification_idsDirtyFlag ;
    }   

    /**
     * 获取 [无响应]
     */
    @JsonProperty("no_auto_thread")
    public String getNo_auto_thread(){
        return this.no_auto_thread ;
    }

    /**
     * 设置 [无响应]
     */
    @JsonProperty("no_auto_thread")
    public void setNo_auto_thread(String  no_auto_thread){
        this.no_auto_thread = no_auto_thread ;
        this.no_auto_threadDirtyFlag = true ;
    }

     /**
     * 获取 [无响应]脏标记
     */
    @JsonIgnore
    public boolean getNo_auto_threadDirtyFlag(){
        return this.no_auto_threadDirtyFlag ;
    }   

    /**
     * 获取 [上级消息]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级消息]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级消息]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [收件人]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return this.partner_ids ;
    }

    /**
     * 设置 [收件人]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [收件人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return this.partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [相关评级]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return this.rating_ids ;
    }

    /**
     * 设置 [相关评级]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

     /**
     * 获取 [相关评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return this.rating_idsDirtyFlag ;
    }   

    /**
     * 获取 [评级值]
     */
    @JsonProperty("rating_value")
    public Double getRating_value(){
        return this.rating_value ;
    }

    /**
     * 设置 [评级值]
     */
    @JsonProperty("rating_value")
    public void setRating_value(Double  rating_value){
        this.rating_value = rating_value ;
        this.rating_valueDirtyFlag = true ;
    }

     /**
     * 获取 [评级值]脏标记
     */
    @JsonIgnore
    public boolean getRating_valueDirtyFlag(){
        return this.rating_valueDirtyFlag ;
    }   

    /**
     * 获取 [至（合作伙伴）]
     */
    @JsonProperty("recipient_ids")
    public String getRecipient_ids(){
        return this.recipient_ids ;
    }

    /**
     * 设置 [至（合作伙伴）]
     */
    @JsonProperty("recipient_ids")
    public void setRecipient_ids(String  recipient_ids){
        this.recipient_ids = recipient_ids ;
        this.recipient_idsDirtyFlag = true ;
    }

     /**
     * 获取 [至（合作伙伴）]脏标记
     */
    @JsonIgnore
    public boolean getRecipient_idsDirtyFlag(){
        return this.recipient_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息记录名称]
     */
    @JsonProperty("record_name")
    public String getRecord_name(){
        return this.record_name ;
    }

    /**
     * 设置 [消息记录名称]
     */
    @JsonProperty("record_name")
    public void setRecord_name(String  record_name){
        this.record_name = record_name ;
        this.record_nameDirtyFlag = true ;
    }

     /**
     * 获取 [消息记录名称]脏标记
     */
    @JsonIgnore
    public boolean getRecord_nameDirtyFlag(){
        return this.record_nameDirtyFlag ;
    }   

    /**
     * 获取 [参考]
     */
    @JsonProperty("references")
    public String getReferences(){
        return this.references ;
    }

    /**
     * 设置 [参考]
     */
    @JsonProperty("references")
    public void setReferences(String  references){
        this.references = references ;
        this.referencesDirtyFlag = true ;
    }

     /**
     * 获取 [参考]脏标记
     */
    @JsonIgnore
    public boolean getReferencesDirtyFlag(){
        return this.referencesDirtyFlag ;
    }   

    /**
     * 获取 [回复 至]
     */
    @JsonProperty("reply_to")
    public String getReply_to(){
        return this.reply_to ;
    }

    /**
     * 设置 [回复 至]
     */
    @JsonProperty("reply_to")
    public void setReply_to(String  reply_to){
        this.reply_to = reply_to ;
        this.reply_toDirtyFlag = true ;
    }

     /**
     * 获取 [回复 至]脏标记
     */
    @JsonIgnore
    public boolean getReply_toDirtyFlag(){
        return this.reply_toDirtyFlag ;
    }   

    /**
     * 获取 [相关文档编号]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [相关文档编号]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

     /**
     * 获取 [相关文档编号]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }   

    /**
     * 获取 [安排的发送日期]
     */
    @JsonProperty("scheduled_date")
    public String getScheduled_date(){
        return this.scheduled_date ;
    }

    /**
     * 设置 [安排的发送日期]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(String  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

     /**
     * 获取 [安排的发送日期]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return this.scheduled_dateDirtyFlag ;
    }   

    /**
     * 获取 [加星的邮件]
     */
    @JsonProperty("starred")
    public String getStarred(){
        return this.starred ;
    }

    /**
     * 设置 [加星的邮件]
     */
    @JsonProperty("starred")
    public void setStarred(String  starred){
        this.starred = starred ;
        this.starredDirtyFlag = true ;
    }

     /**
     * 获取 [加星的邮件]脏标记
     */
    @JsonIgnore
    public boolean getStarredDirtyFlag(){
        return this.starredDirtyFlag ;
    }   

    /**
     * 获取 [收藏夹]
     */
    @JsonProperty("starred_partner_ids")
    public String getStarred_partner_ids(){
        return this.starred_partner_ids ;
    }

    /**
     * 设置 [收藏夹]
     */
    @JsonProperty("starred_partner_ids")
    public void setStarred_partner_ids(String  starred_partner_ids){
        this.starred_partner_ids = starred_partner_ids ;
        this.starred_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [收藏夹]脏标记
     */
    @JsonIgnore
    public boolean getStarred_partner_idsDirtyFlag(){
        return this.starred_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [统计信息]
     */
    @JsonProperty("statistics_ids")
    public String getStatistics_ids(){
        return this.statistics_ids ;
    }

    /**
     * 设置 [统计信息]
     */
    @JsonProperty("statistics_ids")
    public void setStatistics_ids(String  statistics_ids){
        this.statistics_ids = statistics_ids ;
        this.statistics_idsDirtyFlag = true ;
    }

     /**
     * 获取 [统计信息]脏标记
     */
    @JsonIgnore
    public boolean getStatistics_idsDirtyFlag(){
        return this.statistics_idsDirtyFlag ;
    }   

    /**
     * 获取 [主题]
     */
    @JsonProperty("subject")
    public String getSubject(){
        return this.subject ;
    }

    /**
     * 设置 [主题]
     */
    @JsonProperty("subject")
    public void setSubject(String  subject){
        this.subject = subject ;
        this.subjectDirtyFlag = true ;
    }

     /**
     * 获取 [主题]脏标记
     */
    @JsonIgnore
    public boolean getSubjectDirtyFlag(){
        return this.subjectDirtyFlag ;
    }   

    /**
     * 获取 [子类型]
     */
    @JsonProperty("subtype_id")
    public Integer getSubtype_id(){
        return this.subtype_id ;
    }

    /**
     * 设置 [子类型]
     */
    @JsonProperty("subtype_id")
    public void setSubtype_id(Integer  subtype_id){
        this.subtype_id = subtype_id ;
        this.subtype_idDirtyFlag = true ;
    }

     /**
     * 获取 [子类型]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_idDirtyFlag(){
        return this.subtype_idDirtyFlag ;
    }   

    /**
     * 获取 [追踪值]
     */
    @JsonProperty("tracking_value_ids")
    public String getTracking_value_ids(){
        return this.tracking_value_ids ;
    }

    /**
     * 设置 [追踪值]
     */
    @JsonProperty("tracking_value_ids")
    public void setTracking_value_ids(String  tracking_value_ids){
        this.tracking_value_ids = tracking_value_ids ;
        this.tracking_value_idsDirtyFlag = true ;
    }

     /**
     * 获取 [追踪值]脏标记
     */
    @JsonIgnore
    public boolean getTracking_value_idsDirtyFlag(){
        return this.tracking_value_idsDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("add_sign") instanceof Boolean){
			this.setAdd_sign(((Boolean)map.get("add_sign"))? "true" : "false");
		}
		if(!(map.get("attachment_ids") instanceof Boolean)&& map.get("attachment_ids")!=null){
			Object[] objs = (Object[])map.get("attachment_ids");
			if(objs.length > 0){
				Integer[] attachment_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAttachment_ids(Arrays.toString(attachment_ids).replace(" ",""));
			}
		}
		if(!(map.get("author_avatar") instanceof Boolean)&& map.get("author_avatar")!=null){
			//暂时忽略
			//this.setAuthor_avatar(((String)map.get("author_avatar")).getBytes("UTF-8"));
		}
		if(!(map.get("author_id") instanceof Boolean)&& map.get("author_id")!=null){
			Object[] objs = (Object[])map.get("author_id");
			if(objs.length > 0){
				this.setAuthor_id((Integer)objs[0]);
			}
		}
		if(map.get("auto_delete") instanceof Boolean){
			this.setAuto_delete(((Boolean)map.get("auto_delete"))? "true" : "false");
		}
		if(!(map.get("body") instanceof Boolean)&& map.get("body")!=null){
			this.setBody((String)map.get("body"));
		}
		if(!(map.get("body_html") instanceof Boolean)&& map.get("body_html")!=null){
			this.setBody_html((String)map.get("body_html"));
		}
		if(!(map.get("channel_ids") instanceof Boolean)&& map.get("channel_ids")!=null){
			Object[] objs = (Object[])map.get("channel_ids");
			if(objs.length > 0){
				Integer[] channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChannel_ids(Arrays.toString(channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("child_ids") instanceof Boolean)&& map.get("child_ids")!=null){
			Object[] objs = (Object[])map.get("child_ids");
			if(objs.length > 0){
				Integer[] child_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChild_ids(Arrays.toString(child_ids).replace(" ",""));
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("email_cc") instanceof Boolean)&& map.get("email_cc")!=null){
			this.setEmail_cc((String)map.get("email_cc"));
		}
		if(!(map.get("email_from") instanceof Boolean)&& map.get("email_from")!=null){
			this.setEmail_from((String)map.get("email_from"));
		}
		if(!(map.get("email_to") instanceof Boolean)&& map.get("email_to")!=null){
			this.setEmail_to((String)map.get("email_to"));
		}
		if(!(map.get("failure_reason") instanceof Boolean)&& map.get("failure_reason")!=null){
			this.setFailure_reason((String)map.get("failure_reason"));
		}
		if(!(map.get("fetchmail_server_id") instanceof Boolean)&& map.get("fetchmail_server_id")!=null){
			Object[] objs = (Object[])map.get("fetchmail_server_id");
			if(objs.length > 0){
				this.setFetchmail_server_id((Integer)objs[0]);
			}
		}
		if(!(map.get("fetchmail_server_id") instanceof Boolean)&& map.get("fetchmail_server_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("fetchmail_server_id");
			if(objs.length > 1){
				this.setFetchmail_server_id_text((String)objs[1]);
			}
		}
		if(map.get("has_error") instanceof Boolean){
			this.setHas_error(((Boolean)map.get("has_error"))? "true" : "false");
		}
		if(!(map.get("headers") instanceof Boolean)&& map.get("headers")!=null){
			this.setHeaders((String)map.get("headers"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("layout") instanceof Boolean)&& map.get("layout")!=null){
			this.setLayout((String)map.get("layout"));
		}
		if(!(map.get("mailing_id") instanceof Boolean)&& map.get("mailing_id")!=null){
			Object[] objs = (Object[])map.get("mailing_id");
			if(objs.length > 0){
				this.setMailing_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mailing_id") instanceof Boolean)&& map.get("mailing_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("mailing_id");
			if(objs.length > 1){
				this.setMailing_id_text((String)objs[1]);
			}
		}
		if(!(map.get("mail_activity_type_id") instanceof Boolean)&& map.get("mail_activity_type_id")!=null){
			Object[] objs = (Object[])map.get("mail_activity_type_id");
			if(objs.length > 0){
				this.setMail_activity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mail_message_id") instanceof Boolean)&& map.get("mail_message_id")!=null){
			Object[] objs = (Object[])map.get("mail_message_id");
			if(objs.length > 0){
				this.setMail_message_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mail_server_id") instanceof Boolean)&& map.get("mail_server_id")!=null){
			Object[] objs = (Object[])map.get("mail_server_id");
			if(objs.length > 0){
				this.setMail_server_id((Integer)objs[0]);
			}
		}
		if(!(map.get("message_id") instanceof Boolean)&& map.get("message_id")!=null){
			this.setMessage_id((String)map.get("message_id"));
		}
		if(!(map.get("message_type") instanceof Boolean)&& map.get("message_type")!=null){
			this.setMessage_type((String)map.get("message_type"));
		}
		if(!(map.get("model") instanceof Boolean)&& map.get("model")!=null){
			this.setModel((String)map.get("model"));
		}
		if(!(map.get("moderation_status") instanceof Boolean)&& map.get("moderation_status")!=null){
			this.setModeration_status((String)map.get("moderation_status"));
		}
		if(!(map.get("moderator_id") instanceof Boolean)&& map.get("moderator_id")!=null){
			Object[] objs = (Object[])map.get("moderator_id");
			if(objs.length > 0){
				this.setModerator_id((Integer)objs[0]);
			}
		}
		if(map.get("needaction") instanceof Boolean){
			this.setNeedaction(((Boolean)map.get("needaction"))? "true" : "false");
		}
		if(!(map.get("needaction_partner_ids") instanceof Boolean)&& map.get("needaction_partner_ids")!=null){
			Object[] objs = (Object[])map.get("needaction_partner_ids");
			if(objs.length > 0){
				Integer[] needaction_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setNeedaction_partner_ids(Arrays.toString(needaction_partner_ids).replace(" ",""));
			}
		}
		if(map.get("need_moderation") instanceof Boolean){
			this.setNeed_moderation(((Boolean)map.get("need_moderation"))? "true" : "false");
		}
		if(map.get("notification") instanceof Boolean){
			this.setNotification(((Boolean)map.get("notification"))? "true" : "false");
		}
		if(!(map.get("notification_ids") instanceof Boolean)&& map.get("notification_ids")!=null){
			Object[] objs = (Object[])map.get("notification_ids");
			if(objs.length > 0){
				Integer[] notification_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setNotification_ids(Arrays.toString(notification_ids).replace(" ",""));
			}
		}
		if(map.get("no_auto_thread") instanceof Boolean){
			this.setNo_auto_thread(((Boolean)map.get("no_auto_thread"))? "true" : "false");
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 0){
				this.setParent_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_ids") instanceof Boolean)&& map.get("partner_ids")!=null){
			Object[] objs = (Object[])map.get("partner_ids");
			if(objs.length > 0){
				Integer[] partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPartner_ids(Arrays.toString(partner_ids).replace(" ",""));
			}
		}
		if(!(map.get("rating_ids") instanceof Boolean)&& map.get("rating_ids")!=null){
			Object[] objs = (Object[])map.get("rating_ids");
			if(objs.length > 0){
				Integer[] rating_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRating_ids(Arrays.toString(rating_ids).replace(" ",""));
			}
		}
		if(!(map.get("rating_value") instanceof Boolean)&& map.get("rating_value")!=null){
			this.setRating_value((Double)map.get("rating_value"));
		}
		if(!(map.get("recipient_ids") instanceof Boolean)&& map.get("recipient_ids")!=null){
			Object[] objs = (Object[])map.get("recipient_ids");
			if(objs.length > 0){
				Integer[] recipient_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRecipient_ids(Arrays.toString(recipient_ids).replace(" ",""));
			}
		}
		if(!(map.get("record_name") instanceof Boolean)&& map.get("record_name")!=null){
			this.setRecord_name((String)map.get("record_name"));
		}
		if(!(map.get("references") instanceof Boolean)&& map.get("references")!=null){
			this.setReferences((String)map.get("references"));
		}
		if(!(map.get("reply_to") instanceof Boolean)&& map.get("reply_to")!=null){
			this.setReply_to((String)map.get("reply_to"));
		}
		if(!(map.get("res_id") instanceof Boolean)&& map.get("res_id")!=null){
			this.setRes_id((Integer)map.get("res_id"));
		}
		if(!(map.get("scheduled_date") instanceof Boolean)&& map.get("scheduled_date")!=null){
			this.setScheduled_date((String)map.get("scheduled_date"));
		}
		if(map.get("starred") instanceof Boolean){
			this.setStarred(((Boolean)map.get("starred"))? "true" : "false");
		}
		if(!(map.get("starred_partner_ids") instanceof Boolean)&& map.get("starred_partner_ids")!=null){
			Object[] objs = (Object[])map.get("starred_partner_ids");
			if(objs.length > 0){
				Integer[] starred_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setStarred_partner_ids(Arrays.toString(starred_partner_ids).replace(" ",""));
			}
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("statistics_ids") instanceof Boolean)&& map.get("statistics_ids")!=null){
			Object[] objs = (Object[])map.get("statistics_ids");
			if(objs.length > 0){
				Integer[] statistics_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setStatistics_ids(Arrays.toString(statistics_ids).replace(" ",""));
			}
		}
		if(!(map.get("subject") instanceof Boolean)&& map.get("subject")!=null){
			this.setSubject((String)map.get("subject"));
		}
		if(!(map.get("subtype_id") instanceof Boolean)&& map.get("subtype_id")!=null){
			Object[] objs = (Object[])map.get("subtype_id");
			if(objs.length > 0){
				this.setSubtype_id((Integer)objs[0]);
			}
		}
		if(!(map.get("tracking_value_ids") instanceof Boolean)&& map.get("tracking_value_ids")!=null){
			Object[] objs = (Object[])map.get("tracking_value_ids");
			if(objs.length > 0){
				Integer[] tracking_value_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTracking_value_ids(Arrays.toString(tracking_value_ids).replace(" ",""));
			}
		}
		if(map.get("website_published") instanceof Boolean){
			this.setWebsite_published(((Boolean)map.get("website_published"))? "true" : "false");
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAdd_sign()!=null&&this.getAdd_signDirtyFlag()){
			map.put("add_sign",Boolean.parseBoolean(this.getAdd_sign()));		
		}		if(this.getAttachment_ids()!=null&&this.getAttachment_idsDirtyFlag()){
			map.put("attachment_ids",this.getAttachment_ids());
		}else if(this.getAttachment_idsDirtyFlag()){
			map.put("attachment_ids",false);
		}
		if(this.getAuthor_avatar()!=null&&this.getAuthor_avatarDirtyFlag()){
			//暂不支持binary类型author_avatar
		}else if(this.getAuthor_avatarDirtyFlag()){
			map.put("author_avatar",false);
		}
		if(this.getAuthor_id()!=null&&this.getAuthor_idDirtyFlag()){
			map.put("author_id",this.getAuthor_id());
		}else if(this.getAuthor_idDirtyFlag()){
			map.put("author_id",false);
		}
		if(this.getAuto_delete()!=null&&this.getAuto_deleteDirtyFlag()){
			map.put("auto_delete",Boolean.parseBoolean(this.getAuto_delete()));		
		}		if(this.getBody()!=null&&this.getBodyDirtyFlag()){
			map.put("body",this.getBody());
		}else if(this.getBodyDirtyFlag()){
			map.put("body",false);
		}
		if(this.getBody_html()!=null&&this.getBody_htmlDirtyFlag()){
			map.put("body_html",this.getBody_html());
		}else if(this.getBody_htmlDirtyFlag()){
			map.put("body_html",false);
		}
		if(this.getChannel_ids()!=null&&this.getChannel_idsDirtyFlag()){
			map.put("channel_ids",this.getChannel_ids());
		}else if(this.getChannel_idsDirtyFlag()){
			map.put("channel_ids",false);
		}
		if(this.getChild_ids()!=null&&this.getChild_idsDirtyFlag()){
			map.put("child_ids",this.getChild_ids());
		}else if(this.getChild_idsDirtyFlag()){
			map.put("child_ids",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmail_cc()!=null&&this.getEmail_ccDirtyFlag()){
			map.put("email_cc",this.getEmail_cc());
		}else if(this.getEmail_ccDirtyFlag()){
			map.put("email_cc",false);
		}
		if(this.getEmail_from()!=null&&this.getEmail_fromDirtyFlag()){
			map.put("email_from",this.getEmail_from());
		}else if(this.getEmail_fromDirtyFlag()){
			map.put("email_from",false);
		}
		if(this.getEmail_to()!=null&&this.getEmail_toDirtyFlag()){
			map.put("email_to",this.getEmail_to());
		}else if(this.getEmail_toDirtyFlag()){
			map.put("email_to",false);
		}
		if(this.getFailure_reason()!=null&&this.getFailure_reasonDirtyFlag()){
			map.put("failure_reason",this.getFailure_reason());
		}else if(this.getFailure_reasonDirtyFlag()){
			map.put("failure_reason",false);
		}
		if(this.getFetchmail_server_id()!=null&&this.getFetchmail_server_idDirtyFlag()){
			map.put("fetchmail_server_id",this.getFetchmail_server_id());
		}else if(this.getFetchmail_server_idDirtyFlag()){
			map.put("fetchmail_server_id",false);
		}
		if(this.getFetchmail_server_id_text()!=null&&this.getFetchmail_server_id_textDirtyFlag()){
			//忽略文本外键fetchmail_server_id_text
		}else if(this.getFetchmail_server_id_textDirtyFlag()){
			map.put("fetchmail_server_id",false);
		}
		if(this.getHas_error()!=null&&this.getHas_errorDirtyFlag()){
			map.put("has_error",Boolean.parseBoolean(this.getHas_error()));		
		}		if(this.getHeaders()!=null&&this.getHeadersDirtyFlag()){
			map.put("headers",this.getHeaders());
		}else if(this.getHeadersDirtyFlag()){
			map.put("headers",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLayout()!=null&&this.getLayoutDirtyFlag()){
			map.put("layout",this.getLayout());
		}else if(this.getLayoutDirtyFlag()){
			map.put("layout",false);
		}
		if(this.getMailing_id()!=null&&this.getMailing_idDirtyFlag()){
			map.put("mailing_id",this.getMailing_id());
		}else if(this.getMailing_idDirtyFlag()){
			map.put("mailing_id",false);
		}
		if(this.getMailing_id_text()!=null&&this.getMailing_id_textDirtyFlag()){
			//忽略文本外键mailing_id_text
		}else if(this.getMailing_id_textDirtyFlag()){
			map.put("mailing_id",false);
		}
		if(this.getMail_activity_type_id()!=null&&this.getMail_activity_type_idDirtyFlag()){
			map.put("mail_activity_type_id",this.getMail_activity_type_id());
		}else if(this.getMail_activity_type_idDirtyFlag()){
			map.put("mail_activity_type_id",false);
		}
		if(this.getMail_message_id()!=null&&this.getMail_message_idDirtyFlag()){
			map.put("mail_message_id",this.getMail_message_id());
		}else if(this.getMail_message_idDirtyFlag()){
			map.put("mail_message_id",false);
		}
		if(this.getMail_server_id()!=null&&this.getMail_server_idDirtyFlag()){
			map.put("mail_server_id",this.getMail_server_id());
		}else if(this.getMail_server_idDirtyFlag()){
			map.put("mail_server_id",false);
		}
		if(this.getMessage_id()!=null&&this.getMessage_idDirtyFlag()){
			map.put("message_id",this.getMessage_id());
		}else if(this.getMessage_idDirtyFlag()){
			map.put("message_id",false);
		}
		if(this.getMessage_type()!=null&&this.getMessage_typeDirtyFlag()){
			map.put("message_type",this.getMessage_type());
		}else if(this.getMessage_typeDirtyFlag()){
			map.put("message_type",false);
		}
		if(this.getModel()!=null&&this.getModelDirtyFlag()){
			map.put("model",this.getModel());
		}else if(this.getModelDirtyFlag()){
			map.put("model",false);
		}
		if(this.getModeration_status()!=null&&this.getModeration_statusDirtyFlag()){
			map.put("moderation_status",this.getModeration_status());
		}else if(this.getModeration_statusDirtyFlag()){
			map.put("moderation_status",false);
		}
		if(this.getModerator_id()!=null&&this.getModerator_idDirtyFlag()){
			map.put("moderator_id",this.getModerator_id());
		}else if(this.getModerator_idDirtyFlag()){
			map.put("moderator_id",false);
		}
		if(this.getNeedaction()!=null&&this.getNeedactionDirtyFlag()){
			map.put("needaction",Boolean.parseBoolean(this.getNeedaction()));		
		}		if(this.getNeedaction_partner_ids()!=null&&this.getNeedaction_partner_idsDirtyFlag()){
			map.put("needaction_partner_ids",this.getNeedaction_partner_ids());
		}else if(this.getNeedaction_partner_idsDirtyFlag()){
			map.put("needaction_partner_ids",false);
		}
		if(this.getNeed_moderation()!=null&&this.getNeed_moderationDirtyFlag()){
			map.put("need_moderation",Boolean.parseBoolean(this.getNeed_moderation()));		
		}		if(this.getNotification()!=null&&this.getNotificationDirtyFlag()){
			map.put("notification",Boolean.parseBoolean(this.getNotification()));		
		}		if(this.getNotification_ids()!=null&&this.getNotification_idsDirtyFlag()){
			map.put("notification_ids",this.getNotification_ids());
		}else if(this.getNotification_idsDirtyFlag()){
			map.put("notification_ids",false);
		}
		if(this.getNo_auto_thread()!=null&&this.getNo_auto_threadDirtyFlag()){
			map.put("no_auto_thread",Boolean.parseBoolean(this.getNo_auto_thread()));		
		}		if(this.getParent_id()!=null&&this.getParent_idDirtyFlag()){
			map.put("parent_id",this.getParent_id());
		}else if(this.getParent_idDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getPartner_ids()!=null&&this.getPartner_idsDirtyFlag()){
			map.put("partner_ids",this.getPartner_ids());
		}else if(this.getPartner_idsDirtyFlag()){
			map.put("partner_ids",false);
		}
		if(this.getRating_ids()!=null&&this.getRating_idsDirtyFlag()){
			map.put("rating_ids",this.getRating_ids());
		}else if(this.getRating_idsDirtyFlag()){
			map.put("rating_ids",false);
		}
		if(this.getRating_value()!=null&&this.getRating_valueDirtyFlag()){
			map.put("rating_value",this.getRating_value());
		}else if(this.getRating_valueDirtyFlag()){
			map.put("rating_value",false);
		}
		if(this.getRecipient_ids()!=null&&this.getRecipient_idsDirtyFlag()){
			map.put("recipient_ids",this.getRecipient_ids());
		}else if(this.getRecipient_idsDirtyFlag()){
			map.put("recipient_ids",false);
		}
		if(this.getRecord_name()!=null&&this.getRecord_nameDirtyFlag()){
			map.put("record_name",this.getRecord_name());
		}else if(this.getRecord_nameDirtyFlag()){
			map.put("record_name",false);
		}
		if(this.getReferences()!=null&&this.getReferencesDirtyFlag()){
			map.put("references",this.getReferences());
		}else if(this.getReferencesDirtyFlag()){
			map.put("references",false);
		}
		if(this.getReply_to()!=null&&this.getReply_toDirtyFlag()){
			map.put("reply_to",this.getReply_to());
		}else if(this.getReply_toDirtyFlag()){
			map.put("reply_to",false);
		}
		if(this.getRes_id()!=null&&this.getRes_idDirtyFlag()){
			map.put("res_id",this.getRes_id());
		}else if(this.getRes_idDirtyFlag()){
			map.put("res_id",false);
		}
		if(this.getScheduled_date()!=null&&this.getScheduled_dateDirtyFlag()){
			map.put("scheduled_date",this.getScheduled_date());
		}else if(this.getScheduled_dateDirtyFlag()){
			map.put("scheduled_date",false);
		}
		if(this.getStarred()!=null&&this.getStarredDirtyFlag()){
			map.put("starred",Boolean.parseBoolean(this.getStarred()));		
		}		if(this.getStarred_partner_ids()!=null&&this.getStarred_partner_idsDirtyFlag()){
			map.put("starred_partner_ids",this.getStarred_partner_ids());
		}else if(this.getStarred_partner_idsDirtyFlag()){
			map.put("starred_partner_ids",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getStatistics_ids()!=null&&this.getStatistics_idsDirtyFlag()){
			map.put("statistics_ids",this.getStatistics_ids());
		}else if(this.getStatistics_idsDirtyFlag()){
			map.put("statistics_ids",false);
		}
		if(this.getSubject()!=null&&this.getSubjectDirtyFlag()){
			map.put("subject",this.getSubject());
		}else if(this.getSubjectDirtyFlag()){
			map.put("subject",false);
		}
		if(this.getSubtype_id()!=null&&this.getSubtype_idDirtyFlag()){
			map.put("subtype_id",this.getSubtype_id());
		}else if(this.getSubtype_idDirtyFlag()){
			map.put("subtype_id",false);
		}
		if(this.getTracking_value_ids()!=null&&this.getTracking_value_idsDirtyFlag()){
			map.put("tracking_value_ids",this.getTracking_value_ids());
		}else if(this.getTracking_value_idsDirtyFlag()){
			map.put("tracking_value_ids",false);
		}
		if(this.getWebsite_published()!=null&&this.getWebsite_publishedDirtyFlag()){
			map.put("website_published",Boolean.parseBoolean(this.getWebsite_published()));		
		}		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
