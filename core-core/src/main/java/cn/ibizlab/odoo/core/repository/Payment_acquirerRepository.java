package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Payment_acquirer;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirerSearchContext;

/**
 * 实体 [付款收单方] 存储对象
 */
public interface Payment_acquirerRepository extends Repository<Payment_acquirer> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Payment_acquirer> searchDefault(Payment_acquirerSearchContext context);

    Payment_acquirer convert2PO(cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer domain , Payment_acquirer po) ;

    cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer convert2Domain( Payment_acquirer po ,cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer domain) ;

}
