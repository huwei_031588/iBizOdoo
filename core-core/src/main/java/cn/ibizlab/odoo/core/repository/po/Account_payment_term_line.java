package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_term_lineSearchContext;

/**
 * 实体 [付款条款行] 存储模型
 */
public interface Account_payment_term_line{

    /**
     * 天数
     */
    Integer getDays();

    void setDays(Integer days);

    /**
     * 获取 [天数]脏标记
     */
    boolean getDaysDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 类型
     */
    String getValue();

    void setValue(String value);

    /**
     * 获取 [类型]脏标记
     */
    boolean getValueDirtyFlag();

    /**
     * 值
     */
    Double getValue_amount();

    void setValue_amount(Double value_amount);

    /**
     * 获取 [值]脏标记
     */
    boolean getValue_amountDirtyFlag();

    /**
     * 日期
     */
    Integer getDay_of_the_month();

    void setDay_of_the_month(Integer day_of_the_month);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDay_of_the_monthDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 选项
     */
    String getOption();

    void setOption(String option);

    /**
     * 获取 [选项]脏标记
     */
    boolean getOptionDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 付款条款
     */
    String getPayment_id_text();

    void setPayment_id_text(String payment_id_text);

    /**
     * 获取 [付款条款]脏标记
     */
    boolean getPayment_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 付款条款
     */
    Integer getPayment_id();

    void setPayment_id(Integer payment_id);

    /**
     * 获取 [付款条款]脏标记
     */
    boolean getPayment_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
