package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_bankSearchContext;

/**
 * 实体 [银行账户] 存储模型
 */
public interface Res_partner_bank{

    /**
     * 核对银行账号
     */
    String getSanitized_acc_number();

    void setSanitized_acc_number(String sanitized_acc_number);

    /**
     * 获取 [核对银行账号]脏标记
     */
    boolean getSanitized_acc_numberDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 会计日记账
     */
    String getJournal_id();

    void setJournal_id(String journal_id);

    /**
     * 获取 [会计日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 账户号码
     */
    String getAcc_number();

    void setAcc_number(String acc_number);

    /**
     * 获取 [账户号码]脏标记
     */
    boolean getAcc_numberDirtyFlag();

    /**
     * 类型
     */
    String getAcc_type();

    void setAcc_type(String acc_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getAcc_typeDirtyFlag();

    /**
     * ‎有所有必需的参数‎
     */
    String getQr_code_valid();

    void setQr_code_valid(String qr_code_valid);

    /**
     * 获取 [‎有所有必需的参数‎]脏标记
     */
    boolean getQr_code_validDirtyFlag();

    /**
     * 账户持有人名称
     */
    String getAcc_holder_name();

    void setAcc_holder_name(String acc_holder_name);

    /**
     * 获取 [账户持有人名称]脏标记
     */
    boolean getAcc_holder_nameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 名称
     */
    String getBank_name();

    void setBank_name(String bank_name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getBank_nameDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 账户持有人
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [账户持有人]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 银行识别代码
     */
    String getBank_bic();

    void setBank_bic(String bank_bic);

    /**
     * 获取 [银行识别代码]脏标记
     */
    boolean getBank_bicDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 银行
     */
    Integer getBank_id();

    void setBank_id(Integer bank_id);

    /**
     * 获取 [银行]脏标记
     */
    boolean getBank_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 账户持有人
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [账户持有人]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

}
