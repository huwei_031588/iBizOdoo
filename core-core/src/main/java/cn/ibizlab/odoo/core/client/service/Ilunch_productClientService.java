package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ilunch_product;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_product] 服务对象接口
 */
public interface Ilunch_productClientService{

    public Ilunch_product createModel() ;

    public void create(Ilunch_product lunch_product);

    public void get(Ilunch_product lunch_product);

    public void createBatch(List<Ilunch_product> lunch_products);

    public void updateBatch(List<Ilunch_product> lunch_products);

    public Page<Ilunch_product> search(SearchContext context);

    public void removeBatch(List<Ilunch_product> lunch_products);

    public void update(Ilunch_product lunch_product);

    public void remove(Ilunch_product lunch_product);

    public Page<Ilunch_product> select(SearchContext context);

}
