package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_lead_lost;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_lostSearchContext;

/**
 * 实体 [获取丢失原因] 存储对象
 */
public interface Crm_lead_lostRepository extends Repository<Crm_lead_lost> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_lead_lost> searchDefault(Crm_lead_lostSearchContext context);

    Crm_lead_lost convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_lost domain , Crm_lead_lost po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_lost convert2Domain( Crm_lead_lost po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_lost domain) ;

}
