package cn.ibizlab.odoo.core.odoo_sms.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_apiSearchContext;
import cn.ibizlab.odoo.core.odoo_sms.service.ISms_apiService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_sms.client.sms_apiOdooClient;
import cn.ibizlab.odoo.core.odoo_sms.clientmodel.sms_apiClientModel;

/**
 * 实体[短信API] 服务对象接口实现
 */
@Slf4j
@Service
public class Sms_apiServiceImpl implements ISms_apiService {

    @Autowired
    sms_apiOdooClient sms_apiOdooClient;


    @Override
    public Sms_api get(Integer id) {
        sms_apiClientModel clientModel = new sms_apiClientModel();
        clientModel.setId(id);
		sms_apiOdooClient.get(clientModel);
        Sms_api et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Sms_api();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        sms_apiClientModel clientModel = new sms_apiClientModel();
        clientModel.setId(id);
		sms_apiOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Sms_api et) {
        sms_apiClientModel clientModel = convert2Model(et,null);
		sms_apiOdooClient.create(clientModel);
        Sms_api rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sms_api> list){
    }

    @Override
    public boolean update(Sms_api et) {
        sms_apiClientModel clientModel = convert2Model(et,null);
		sms_apiOdooClient.update(clientModel);
        Sms_api rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Sms_api> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sms_api> searchDefault(Sms_apiSearchContext context) {
        List<Sms_api> list = new ArrayList<Sms_api>();
        Page<sms_apiClientModel> clientModelList = sms_apiOdooClient.search(context);
        for(sms_apiClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Sms_api>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public sms_apiClientModel convert2Model(Sms_api domain , sms_apiClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new sms_apiClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Sms_api convert2Domain( sms_apiClientModel model ,Sms_api domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Sms_api();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        return domain ;
    }

}

    



