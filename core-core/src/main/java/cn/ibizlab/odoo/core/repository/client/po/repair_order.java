package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [repair_order] 对象
 */
public interface repair_order {

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public Integer getAddress_id();

    public void setAddress_id(Integer address_id);

    public String getAddress_id_text();

    public void setAddress_id_text(String address_id_text);

    public Double getAmount_tax();

    public void setAmount_tax(Double amount_tax);

    public Double getAmount_total();

    public void setAmount_total(Double amount_total);

    public Double getAmount_untaxed();

    public void setAmount_untaxed(Double amount_untaxed);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getDefault_address_id();

    public void setDefault_address_id(Integer default_address_id);

    public String getDefault_address_id_text();

    public void setDefault_address_id_text(String default_address_id_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getFees_lines();

    public void setFees_lines(String fees_lines);

    public Timestamp getGuarantee_limit();

    public void setGuarantee_limit(Timestamp guarantee_limit);

    public Integer getId();

    public void setId(Integer id);

    public String getInternal_notes();

    public void setInternal_notes(String internal_notes);

    public String getInvoiced();

    public void setInvoiced(String invoiced);

    public Integer getInvoice_id();

    public void setInvoice_id(Integer invoice_id);

    public String getInvoice_id_text();

    public void setInvoice_id_text(String invoice_id_text);

    public String getInvoice_method();

    public void setInvoice_method(String invoice_method);

    public Integer getLocation_id();

    public void setLocation_id(Integer location_id);

    public String getLocation_id_text();

    public void setLocation_id_text(String location_id_text);

    public Integer getLot_id();

    public void setLot_id(Integer lot_id);

    public String getLot_id_text();

    public void setLot_id_text(String lot_id_text);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public Integer getMove_id();

    public void setMove_id(Integer move_id);

    public String getMove_id_text();

    public void setMove_id_text(String move_id_text);

    public String getName();

    public void setName(String name);

    public String getOperations();

    public void setOperations(String operations);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public Integer getPartner_invoice_id();

    public void setPartner_invoice_id(Integer partner_invoice_id);

    public String getPartner_invoice_id_text();

    public void setPartner_invoice_id_text(String partner_invoice_id_text);

    public Integer getPricelist_id();

    public void setPricelist_id(Integer pricelist_id);

    public String getPricelist_id_text();

    public void setPricelist_id_text(String pricelist_id_text);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public Double getProduct_qty();

    public void setProduct_qty(Double product_qty);

    public Integer getProduct_uom();

    public void setProduct_uom(Integer product_uom);

    public String getProduct_uom_text();

    public void setProduct_uom_text(String product_uom_text);

    public String getQuotation_notes();

    public void setQuotation_notes(String quotation_notes);

    public String getRepaired();

    public void setRepaired(String repaired);

    public String getState();

    public void setState(String state);

    public String getTracking();

    public void setTracking(String tracking);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
