package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_leave_allocation] 对象
 */
public interface hr_leave_allocation {

    public String getAccrual();

    public void setAccrual(String accrual);

    public Integer getAccrual_limit();

    public void setAccrual_limit(Integer accrual_limit);

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public String getCan_approve();

    public void setCan_approve(String can_approve);

    public String getCan_reset();

    public void setCan_reset(String can_reset);

    public Integer getCategory_id();

    public void setCategory_id(Integer category_id);

    public String getCategory_id_text();

    public void setCategory_id_text(String category_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_from();

    public void setDate_from(Timestamp date_from);

    public Timestamp getDate_to();

    public void setDate_to(Timestamp date_to);

    public Integer getDepartment_id();

    public void setDepartment_id(Integer department_id);

    public String getDepartment_id_text();

    public void setDepartment_id_text(String department_id_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getDuration_display();

    public void setDuration_display(String duration_display);

    public Integer getEmployee_id();

    public void setEmployee_id(Integer employee_id);

    public String getEmployee_id_text();

    public void setEmployee_id_text(String employee_id_text);

    public Integer getFirst_approver_id();

    public void setFirst_approver_id(Integer first_approver_id);

    public String getFirst_approver_id_text();

    public void setFirst_approver_id_text(String first_approver_id_text);

    public Integer getHoliday_status_id();

    public void setHoliday_status_id(Integer holiday_status_id);

    public String getHoliday_status_id_text();

    public void setHoliday_status_id_text(String holiday_status_id_text);

    public String getHoliday_type();

    public void setHoliday_type(String holiday_type);

    public Integer getId();

    public void setId(Integer id);

    public Integer getInterval_number();

    public void setInterval_number(Integer interval_number);

    public String getInterval_unit();

    public void setInterval_unit(String interval_unit);

    public String getLinked_request_ids();

    public void setLinked_request_ids(String linked_request_ids);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public Integer getMode_company_id();

    public void setMode_company_id(Integer mode_company_id);

    public String getMode_company_id_text();

    public void setMode_company_id_text(String mode_company_id_text);

    public String getName();

    public void setName(String name);

    public Timestamp getNextcall();

    public void setNextcall(Timestamp nextcall);

    public String getNotes();

    public void setNotes(String notes);

    public Double getNumber_of_days();

    public void setNumber_of_days(Double number_of_days);

    public Double getNumber_of_days_display();

    public void setNumber_of_days_display(Double number_of_days_display);

    public Double getNumber_of_hours_display();

    public void setNumber_of_hours_display(Double number_of_hours_display);

    public Double getNumber_per_interval();

    public void setNumber_per_interval(Double number_per_interval);

    public Integer getParent_id();

    public void setParent_id(Integer parent_id);

    public String getParent_id_text();

    public void setParent_id_text(String parent_id_text);

    public Integer getSecond_approver_id();

    public void setSecond_approver_id(Integer second_approver_id);

    public String getSecond_approver_id_text();

    public void setSecond_approver_id_text(String second_approver_id_text);

    public String getState();

    public void setState(String state);

    public String getType_request_unit();

    public void setType_request_unit(String type_request_unit);

    public String getUnit_per_interval();

    public void setUnit_per_interval(String unit_per_interval);

    public String getValidation_type();

    public void setValidation_type(String validation_type);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
