package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [fleet_vehicle_log_services] 对象
 */
public interface fleet_vehicle_log_services {

    public Double getAmount();

    public void setAmount(Double amount);

    public String getAuto_generated();

    public void setAuto_generated(String auto_generated);

    public Integer getContract_id();

    public void setContract_id(Integer contract_id);

    public String getContract_id_text();

    public void setContract_id_text(String contract_id_text);

    public Double getCost_amount();

    public void setCost_amount(Double cost_amount);

    public Integer getCost_id();

    public void setCost_id(Integer cost_id);

    public String getCost_ids();

    public void setCost_ids(String cost_ids);

    public String getCost_id_text();

    public void setCost_id_text(String cost_id_text);

    public Integer getCost_subtype_id();

    public void setCost_subtype_id(Integer cost_subtype_id);

    public String getCost_subtype_id_text();

    public void setCost_subtype_id_text(String cost_subtype_id_text);

    public String getCost_type();

    public void setCost_type(String cost_type);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate();

    public void setDate(Timestamp date);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getInv_ref();

    public void setInv_ref(String inv_ref);

    public String getName();

    public void setName(String name);

    public String getNotes();

    public void setNotes(String notes);

    public Double getOdometer();

    public void setOdometer(Double odometer);

    public Integer getOdometer_id();

    public void setOdometer_id(Integer odometer_id);

    public String getOdometer_id_text();

    public void setOdometer_id_text(String odometer_id_text);

    public String getOdometer_unit();

    public void setOdometer_unit(String odometer_unit);

    public Integer getParent_id();

    public void setParent_id(Integer parent_id);

    public String getParent_id_text();

    public void setParent_id_text(String parent_id_text);

    public Integer getPurchaser_id();

    public void setPurchaser_id(Integer purchaser_id);

    public String getPurchaser_id_text();

    public void setPurchaser_id_text(String purchaser_id_text);

    public Integer getVehicle_id();

    public void setVehicle_id(Integer vehicle_id);

    public String getVehicle_id_text();

    public void setVehicle_id_text(String vehicle_id_text);

    public Integer getVendor_id();

    public void setVendor_id(Integer vendor_id);

    public String getVendor_id_text();

    public void setVendor_id_text(String vendor_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
