package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [sale_order] 对象
 */
public interface sale_order {

    public String getAccess_token();

    public void setAccess_token(String access_token);

    public String getAccess_url();

    public void setAccess_url(String access_url);

    public String getAccess_warning();

    public void setAccess_warning(String access_warning);

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public byte[] getAmount_by_group();

    public void setAmount_by_group(byte[] amount_by_group);

    public Double getAmount_tax();

    public void setAmount_tax(Double amount_tax);

    public Double getAmount_total();

    public void setAmount_total(Double amount_total);

    public Double getAmount_undiscounted();

    public void setAmount_undiscounted(Double amount_undiscounted);

    public Double getAmount_untaxed();

    public void setAmount_untaxed(Double amount_untaxed);

    public Integer getAnalytic_account_id();

    public void setAnalytic_account_id(Integer analytic_account_id);

    public String getAnalytic_account_id_text();

    public void setAnalytic_account_id_text(String analytic_account_id_text);

    public String getAuthorized_transaction_ids();

    public void setAuthorized_transaction_ids(String authorized_transaction_ids);

    public Integer getCampaign_id();

    public void setCampaign_id(Integer campaign_id);

    public String getCampaign_id_text();

    public void setCampaign_id_text(String campaign_id_text);

    public Integer getCart_quantity();

    public void setCart_quantity(Integer cart_quantity);

    public String getCart_recovery_email_sent();

    public void setCart_recovery_email_sent(String cart_recovery_email_sent);

    public String getClient_order_ref();

    public void setClient_order_ref(String client_order_ref);

    public Timestamp getCommitment_date();

    public void setCommitment_date(Timestamp commitment_date);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getConfirmation_date();

    public void setConfirmation_date(Timestamp confirmation_date);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getCurrency_id();

    public void setCurrency_id(Integer currency_id);

    public String getCurrency_id_text();

    public void setCurrency_id_text(String currency_id_text);

    public Double getCurrency_rate();

    public void setCurrency_rate(Double currency_rate);

    public Timestamp getDate_order();

    public void setDate_order(Timestamp date_order);

    public Integer getDelivery_count();

    public void setDelivery_count(Integer delivery_count);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Timestamp getEffective_date();

    public void setEffective_date(Timestamp effective_date);

    public Timestamp getExpected_date();

    public void setExpected_date(Timestamp expected_date);

    public Integer getExpense_count();

    public void setExpense_count(Integer expense_count);

    public String getExpense_ids();

    public void setExpense_ids(String expense_ids);

    public Integer getFiscal_position_id();

    public void setFiscal_position_id(Integer fiscal_position_id);

    public String getFiscal_position_id_text();

    public void setFiscal_position_id_text(String fiscal_position_id_text);

    public Integer getId();

    public void setId(Integer id);

    public Integer getIncoterm();

    public void setIncoterm(Integer incoterm);

    public String getIncoterm_text();

    public void setIncoterm_text(String incoterm_text);

    public Integer getInvoice_count();

    public void setInvoice_count(Integer invoice_count);

    public String getInvoice_ids();

    public void setInvoice_ids(String invoice_ids);

    public String getInvoice_status();

    public void setInvoice_status(String invoice_status);

    public String getIs_abandoned_cart();

    public void setIs_abandoned_cart(String is_abandoned_cart);

    public String getIs_expired();

    public void setIs_expired(String is_expired);

    public Integer getMedium_id();

    public void setMedium_id(Integer medium_id);

    public String getMedium_id_text();

    public void setMedium_id_text(String medium_id_text);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public String getNote();

    public void setNote(String note);

    public String getOnly_services();

    public void setOnly_services(String only_services);

    public Integer getOpportunity_id();

    public void setOpportunity_id(Integer opportunity_id);

    public String getOpportunity_id_text();

    public void setOpportunity_id_text(String opportunity_id_text);

    public String getOrder_line();

    public void setOrder_line(String order_line);

    public String getOrigin();

    public void setOrigin(String origin);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public Integer getPartner_invoice_id();

    public void setPartner_invoice_id(Integer partner_invoice_id);

    public String getPartner_invoice_id_text();

    public void setPartner_invoice_id_text(String partner_invoice_id_text);

    public Integer getPartner_shipping_id();

    public void setPartner_shipping_id(Integer partner_shipping_id);

    public String getPartner_shipping_id_text();

    public void setPartner_shipping_id_text(String partner_shipping_id_text);

    public Integer getPayment_term_id();

    public void setPayment_term_id(Integer payment_term_id);

    public String getPayment_term_id_text();

    public void setPayment_term_id_text(String payment_term_id_text);

    public String getPicking_ids();

    public void setPicking_ids(String picking_ids);

    public String getPicking_policy();

    public void setPicking_policy(String picking_policy);

    public Integer getPricelist_id();

    public void setPricelist_id(Integer pricelist_id);

    public String getPricelist_id_text();

    public void setPricelist_id_text(String pricelist_id_text);

    public Integer getPurchase_order_count();

    public void setPurchase_order_count(Integer purchase_order_count);

    public String getReference();

    public void setReference(String reference);

    public Integer getRemaining_validity_days();

    public void setRemaining_validity_days(Integer remaining_validity_days);

    public String getRequire_payment();

    public void setRequire_payment(String require_payment);

    public String getRequire_signature();

    public void setRequire_signature(String require_signature);

    public String getSale_order_option_ids();

    public void setSale_order_option_ids(String sale_order_option_ids);

    public Integer getSale_order_template_id();

    public void setSale_order_template_id(Integer sale_order_template_id);

    public String getSale_order_template_id_text();

    public void setSale_order_template_id_text(String sale_order_template_id_text);

    public byte[] getSignature();

    public void setSignature(byte[] signature);

    public String getSigned_by();

    public void setSigned_by(String signed_by);

    public Integer getSource_id();

    public void setSource_id(Integer source_id);

    public String getSource_id_text();

    public void setSource_id_text(String source_id_text);

    public String getState();

    public void setState(String state);

    public String getTag_ids();

    public void setTag_ids(String tag_ids);

    public Integer getTeam_id();

    public void setTeam_id(Integer team_id);

    public String getTeam_id_text();

    public void setTeam_id_text(String team_id_text);

    public String getTransaction_ids();

    public void setTransaction_ids(String transaction_ids);

    public String getType_name();

    public void setType_name(String type_name);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Timestamp getValidity_date();

    public void setValidity_date(Timestamp validity_date);

    public Integer getWarehouse_id();

    public void setWarehouse_id(Integer warehouse_id);

    public String getWarehouse_id_text();

    public void setWarehouse_id_text(String warehouse_id_text);

    public String getWarning_stock();

    public void setWarning_stock(String warning_stock);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public String getWebsite_order_line();

    public void setWebsite_order_line(String website_order_line);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
