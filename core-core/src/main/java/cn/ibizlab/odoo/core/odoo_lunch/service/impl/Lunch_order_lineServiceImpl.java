package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_order_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_order_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_lunch.clientmodel.lunch_order_lineClientModel;

/**
 * 实体[工作餐订单行] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_order_lineServiceImpl implements ILunch_order_lineService {

    @Autowired
    lunch_order_lineOdooClient lunch_order_lineOdooClient;


    @Override
    public boolean update(Lunch_order_line et) {
        lunch_order_lineClientModel clientModel = convert2Model(et,null);
		lunch_order_lineOdooClient.update(clientModel);
        Lunch_order_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Lunch_order_line> list){
    }

    @Override
    public Lunch_order_line get(Integer id) {
        lunch_order_lineClientModel clientModel = new lunch_order_lineClientModel();
        clientModel.setId(id);
		lunch_order_lineOdooClient.get(clientModel);
        Lunch_order_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Lunch_order_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        lunch_order_lineClientModel clientModel = new lunch_order_lineClientModel();
        clientModel.setId(id);
		lunch_order_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Lunch_order_line et) {
        lunch_order_lineClientModel clientModel = convert2Model(et,null);
		lunch_order_lineOdooClient.create(clientModel);
        Lunch_order_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_order_line> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_order_line> searchDefault(Lunch_order_lineSearchContext context) {
        List<Lunch_order_line> list = new ArrayList<Lunch_order_line>();
        Page<lunch_order_lineClientModel> clientModelList = lunch_order_lineOdooClient.search(context);
        for(lunch_order_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Lunch_order_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public lunch_order_lineClientModel convert2Model(Lunch_order_line domain , lunch_order_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new lunch_order_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("cashmovedirtyflag"))
                model.setCashmove(domain.getCashmove());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("supplier_textdirtyflag"))
                model.setSupplier_text(domain.getSupplierText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("pricedirtyflag"))
                model.setPrice(domain.getPrice());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("supplierdirtyflag"))
                model.setSupplier(domain.getSupplier());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("order_iddirtyflag"))
                model.setOrder_id(domain.getOrderId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Lunch_order_line convert2Domain( lunch_order_lineClientModel model ,Lunch_order_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Lunch_order_line();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCashmoveDirtyFlag())
            domain.setCashmove(model.getCashmove());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getSupplier_textDirtyFlag())
            domain.setSupplierText(model.getSupplier_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getPriceDirtyFlag())
            domain.setPrice(model.getPrice());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getSupplierDirtyFlag())
            domain.setSupplier(model.getSupplier());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getOrder_idDirtyFlag())
            domain.setOrderId(model.getOrder_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



