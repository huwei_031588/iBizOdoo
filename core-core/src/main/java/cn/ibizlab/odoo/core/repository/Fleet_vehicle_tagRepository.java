package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_vehicle_tag;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_tagSearchContext;

/**
 * 实体 [车辆标签] 存储对象
 */
public interface Fleet_vehicle_tagRepository extends Repository<Fleet_vehicle_tag> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_vehicle_tag> searchDefault(Fleet_vehicle_tagSearchContext context);

    Fleet_vehicle_tag convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_tag domain , Fleet_vehicle_tag po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_tag convert2Domain( Fleet_vehicle_tag po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_tag domain) ;

}
