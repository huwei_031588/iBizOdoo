package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_expense_sheet_register_payment_wizard] 对象
 */
public interface hr_expense_sheet_register_payment_wizard {

    public Double getAmount();

    public void setAmount(Double amount);

    public String getCommunication();

    public void setCommunication(String communication);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getCurrency_id();

    public void setCurrency_id(Integer currency_id);

    public String getCurrency_id_text();

    public void setCurrency_id_text(String currency_id_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getHide_payment_method();

    public void setHide_payment_method(String hide_payment_method);

    public Integer getId();

    public void setId(Integer id);

    public Integer getJournal_id();

    public void setJournal_id(Integer journal_id);

    public String getJournal_id_text();

    public void setJournal_id_text(String journal_id_text);

    public Integer getPartner_bank_account_id();

    public void setPartner_bank_account_id(Integer partner_bank_account_id);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public Timestamp getPayment_date();

    public void setPayment_date(Timestamp payment_date);

    public Integer getPayment_method_id();

    public void setPayment_method_id(Integer payment_method_id);

    public String getPayment_method_id_text();

    public void setPayment_method_id_text(String payment_method_id_text);

    public String getShow_partner_bank_account();

    public void setShow_partner_bank_account(String show_partner_bank_account);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
