package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_event.client.event_typeOdooClient;
import cn.ibizlab.odoo.core.odoo_event.clientmodel.event_typeClientModel;

/**
 * 实体[活动类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_typeServiceImpl implements IEvent_typeService {

    @Autowired
    event_typeOdooClient event_typeOdooClient;


    @Override
    public Event_type get(Integer id) {
        event_typeClientModel clientModel = new event_typeClientModel();
        clientModel.setId(id);
		event_typeOdooClient.get(clientModel);
        Event_type et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Event_type();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        event_typeClientModel clientModel = new event_typeClientModel();
        clientModel.setId(id);
		event_typeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Event_type et) {
        event_typeClientModel clientModel = convert2Model(et,null);
		event_typeOdooClient.update(clientModel);
        Event_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Event_type> list){
    }

    @Override
    public boolean create(Event_type et) {
        event_typeClientModel clientModel = convert2Model(et,null);
		event_typeOdooClient.create(clientModel);
        Event_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_type> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_type> searchDefault(Event_typeSearchContext context) {
        List<Event_type> list = new ArrayList<Event_type>();
        Page<event_typeClientModel> clientModelList = event_typeOdooClient.search(context);
        for(event_typeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Event_type>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public event_typeClientModel convert2Model(Event_type domain , event_typeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new event_typeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("default_registration_maxdirtyflag"))
                model.setDefault_registration_max(domain.getDefaultRegistrationMax());
            if((Boolean) domain.getExtensionparams().get("event_type_mail_idsdirtyflag"))
                model.setEvent_type_mail_ids(domain.getEventTypeMailIds());
            if((Boolean) domain.getExtensionparams().get("default_hashtagdirtyflag"))
                model.setDefault_hashtag(domain.getDefaultHashtag());
            if((Boolean) domain.getExtensionparams().get("default_timezonedirtyflag"))
                model.setDefault_timezone(domain.getDefaultTimezone());
            if((Boolean) domain.getExtensionparams().get("default_registration_mindirtyflag"))
                model.setDefault_registration_min(domain.getDefaultRegistrationMin());
            if((Boolean) domain.getExtensionparams().get("event_ticket_idsdirtyflag"))
                model.setEvent_ticket_ids(domain.getEventTicketIds());
            if((Boolean) domain.getExtensionparams().get("use_ticketingdirtyflag"))
                model.setUse_ticketing(domain.getUseTicketing());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("is_onlinedirtyflag"))
                model.setIs_online(domain.getIsOnline());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("auto_confirmdirtyflag"))
                model.setAuto_confirm(domain.getAutoConfirm());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("website_menudirtyflag"))
                model.setWebsite_menu(domain.getWebsiteMenu());
            if((Boolean) domain.getExtensionparams().get("use_hashtagdirtyflag"))
                model.setUse_hashtag(domain.getUseHashtag());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("has_seats_limitationdirtyflag"))
                model.setHas_seats_limitation(domain.getHasSeatsLimitation());
            if((Boolean) domain.getExtensionparams().get("use_mail_scheduledirtyflag"))
                model.setUse_mail_schedule(domain.getUseMailSchedule());
            if((Boolean) domain.getExtensionparams().get("use_timezonedirtyflag"))
                model.setUse_timezone(domain.getUseTimezone());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Event_type convert2Domain( event_typeClientModel model ,Event_type domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Event_type();
        }

        if(model.getDefault_registration_maxDirtyFlag())
            domain.setDefaultRegistrationMax(model.getDefault_registration_max());
        if(model.getEvent_type_mail_idsDirtyFlag())
            domain.setEventTypeMailIds(model.getEvent_type_mail_ids());
        if(model.getDefault_hashtagDirtyFlag())
            domain.setDefaultHashtag(model.getDefault_hashtag());
        if(model.getDefault_timezoneDirtyFlag())
            domain.setDefaultTimezone(model.getDefault_timezone());
        if(model.getDefault_registration_minDirtyFlag())
            domain.setDefaultRegistrationMin(model.getDefault_registration_min());
        if(model.getEvent_ticket_idsDirtyFlag())
            domain.setEventTicketIds(model.getEvent_ticket_ids());
        if(model.getUse_ticketingDirtyFlag())
            domain.setUseTicketing(model.getUse_ticketing());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getIs_onlineDirtyFlag())
            domain.setIsOnline(model.getIs_online());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAuto_confirmDirtyFlag())
            domain.setAutoConfirm(model.getAuto_confirm());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWebsite_menuDirtyFlag())
            domain.setWebsiteMenu(model.getWebsite_menu());
        if(model.getUse_hashtagDirtyFlag())
            domain.setUseHashtag(model.getUse_hashtag());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getHas_seats_limitationDirtyFlag())
            domain.setHasSeatsLimitation(model.getHas_seats_limitation());
        if(model.getUse_mail_scheduleDirtyFlag())
            domain.setUseMailSchedule(model.getUse_mail_schedule());
        if(model.getUse_timezoneDirtyFlag())
            domain.setUseTimezone(model.getUse_timezone());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



