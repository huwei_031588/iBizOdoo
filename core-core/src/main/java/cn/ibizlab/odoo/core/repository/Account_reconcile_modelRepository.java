package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_reconcile_model;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_modelSearchContext;

/**
 * 实体 [请在发票和付款匹配期间创建日记账分录] 存储对象
 */
public interface Account_reconcile_modelRepository extends Repository<Account_reconcile_model> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_reconcile_model> searchDefault(Account_reconcile_modelSearchContext context);

    Account_reconcile_model convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model domain , Account_reconcile_model po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model convert2Domain( Account_reconcile_model po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model domain) ;

}
