package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_mass_mailing_contact;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_contactSearchContext;

/**
 * 实体 [群发邮件联系人] 存储对象
 */
public interface Mail_mass_mailing_contactRepository extends Repository<Mail_mass_mailing_contact> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_mass_mailing_contact> searchDefault(Mail_mass_mailing_contactSearchContext context);

    Mail_mass_mailing_contact convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_contact domain , Mail_mass_mailing_contact po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_contact convert2Domain( Mail_mass_mailing_contact po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_contact domain) ;

}
