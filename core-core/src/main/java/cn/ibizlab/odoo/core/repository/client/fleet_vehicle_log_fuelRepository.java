package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_vehicle_log_fuel;

/**
 * 实体[fleet_vehicle_log_fuel] 服务对象接口
 */
public interface fleet_vehicle_log_fuelRepository{


    public fleet_vehicle_log_fuel createPO() ;
        public void removeBatch(String id);

        public void update(fleet_vehicle_log_fuel fleet_vehicle_log_fuel);

        public void create(fleet_vehicle_log_fuel fleet_vehicle_log_fuel);

        public void updateBatch(fleet_vehicle_log_fuel fleet_vehicle_log_fuel);

        public void remove(String id);

        public void get(String id);

        public List<fleet_vehicle_log_fuel> search();

        public void createBatch(fleet_vehicle_log_fuel fleet_vehicle_log_fuel);


}
