package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_document;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_documentSearchContext;

/**
 * 实体 [生产文档] 存储对象
 */
public interface Mrp_documentRepository extends Repository<Mrp_document> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_document> searchDefault(Mrp_documentSearchContext context);

    Mrp_document convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document domain , Mrp_document po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document convert2Domain( Mrp_document po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document domain) ;

}
