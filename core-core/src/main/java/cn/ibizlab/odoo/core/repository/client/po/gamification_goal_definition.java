package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [gamification_goal_definition] 对象
 */
public interface gamification_goal_definition {

    public String getBatch_mode();

    public void setBatch_mode(String batch_mode);

    public String getBatch_user_expression();

    public void setBatch_user_expression(String batch_user_expression);

    public String getComputation_mode();

    public void setComputation_mode(String computation_mode);

    public String getCompute_code();

    public void setCompute_code(String compute_code);

    public String getCondition();

    public void setCondition(String condition);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_mode();

    public void setDisplay_mode(String display_mode);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getDomain();

    public void setDomain(String domain);

    public String getFull_suffix();

    public void setFull_suffix(String full_suffix);

    public Integer getId();

    public void setId(Integer id);

    public String getMonetary();

    public void setMonetary(String monetary);

    public String getName();

    public void setName(String name);

    public String getRes_id_field();

    public void setRes_id_field(String res_id_field);

    public String getSuffix();

    public void setSuffix(String suffix);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
