package cn.ibizlab.odoo.core.odoo_payment.valuerule.anno.payment_token;

import cn.ibizlab.odoo.core.odoo_payment.valuerule.validator.payment_token.Payment_tokenShort_nameDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Payment_token
 * 属性：Short_name
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Payment_tokenShort_nameDefaultValidator.class})
public @interface Payment_tokenShort_nameDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
