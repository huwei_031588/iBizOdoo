package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ipurchase_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[purchase_report] 服务对象接口
 */
public interface Ipurchase_reportClientService{

    public Ipurchase_report createModel() ;

    public void removeBatch(List<Ipurchase_report> purchase_reports);

    public void update(Ipurchase_report purchase_report);

    public void updateBatch(List<Ipurchase_report> purchase_reports);

    public void get(Ipurchase_report purchase_report);

    public void create(Ipurchase_report purchase_report);

    public void createBatch(List<Ipurchase_report> purchase_reports);

    public Page<Ipurchase_report> search(SearchContext context);

    public void remove(Ipurchase_report purchase_report);

    public Page<Ipurchase_report> select(SearchContext context);

}
