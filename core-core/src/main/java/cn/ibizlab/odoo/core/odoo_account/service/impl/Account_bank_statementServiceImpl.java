package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statementSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statementService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_bank_statementOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_bank_statementClientModel;

/**
 * 实体[银行对账单] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_bank_statementServiceImpl implements IAccount_bank_statementService {

    @Autowired
    account_bank_statementOdooClient account_bank_statementOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_bank_statementClientModel clientModel = new account_bank_statementClientModel();
        clientModel.setId(id);
		account_bank_statementOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_bank_statement et) {
        account_bank_statementClientModel clientModel = convert2Model(et,null);
		account_bank_statementOdooClient.create(clientModel);
        Account_bank_statement rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_bank_statement> list){
    }

    @Override
    public Account_bank_statement get(Integer id) {
        account_bank_statementClientModel clientModel = new account_bank_statementClientModel();
        clientModel.setId(id);
		account_bank_statementOdooClient.get(clientModel);
        Account_bank_statement et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_bank_statement();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Account_bank_statement et) {
        account_bank_statementClientModel clientModel = convert2Model(et,null);
		account_bank_statementOdooClient.update(clientModel);
        Account_bank_statement rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_bank_statement> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_bank_statement> searchDefault(Account_bank_statementSearchContext context) {
        List<Account_bank_statement> list = new ArrayList<Account_bank_statement>();
        Page<account_bank_statementClientModel> clientModelList = account_bank_statementOdooClient.search(context);
        for(account_bank_statementClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_bank_statement>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_bank_statementClientModel convert2Model(Account_bank_statement domain , account_bank_statementClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_bank_statementClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("move_line_idsdirtyflag"))
                model.setMove_line_ids(domain.getMoveLineIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("balance_end_realdirtyflag"))
                model.setBalance_end_real(domain.getBalanceEndReal());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("pos_session_iddirtyflag"))
                model.setPos_session_id(domain.getPosSessionId());
            if((Boolean) domain.getExtensionparams().get("balance_enddirtyflag"))
                model.setBalance_end(domain.getBalanceEnd());
            if((Boolean) domain.getExtensionparams().get("total_entry_encodingdirtyflag"))
                model.setTotal_entry_encoding(domain.getTotalEntryEncoding());
            if((Boolean) domain.getExtensionparams().get("date_donedirtyflag"))
                model.setDate_done(domain.getDateDone());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("all_lines_reconcileddirtyflag"))
                model.setAll_lines_reconciled(domain.getAllLinesReconciled());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("line_idsdirtyflag"))
                model.setLine_ids(domain.getLineIds());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("move_line_countdirtyflag"))
                model.setMove_line_count(domain.getMoveLineCount());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("is_difference_zerodirtyflag"))
                model.setIs_difference_zero(domain.getIsDifferenceZero());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("differencedirtyflag"))
                model.setDifference(domain.getDifference());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("referencedirtyflag"))
                model.setReference(domain.getReference());
            if((Boolean) domain.getExtensionparams().get("balance_startdirtyflag"))
                model.setBalance_start(domain.getBalanceStart());
            if((Boolean) domain.getExtensionparams().get("accounting_datedirtyflag"))
                model.setAccounting_date(domain.getAccountingDate());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("journal_typedirtyflag"))
                model.setJournal_type(domain.getJournalType());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("cashbox_start_iddirtyflag"))
                model.setCashbox_start_id(domain.getCashboxStartId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("cashbox_end_iddirtyflag"))
                model.setCashbox_end_id(domain.getCashboxEndId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_bank_statement convert2Domain( account_bank_statementClientModel model ,Account_bank_statement domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_bank_statement();
        }

        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMove_line_idsDirtyFlag())
            domain.setMoveLineIds(model.getMove_line_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getBalance_end_realDirtyFlag())
            domain.setBalanceEndReal(model.getBalance_end_real());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getPos_session_idDirtyFlag())
            domain.setPosSessionId(model.getPos_session_id());
        if(model.getBalance_endDirtyFlag())
            domain.setBalanceEnd(model.getBalance_end());
        if(model.getTotal_entry_encodingDirtyFlag())
            domain.setTotalEntryEncoding(model.getTotal_entry_encoding());
        if(model.getDate_doneDirtyFlag())
            domain.setDateDone(model.getDate_done());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getAll_lines_reconciledDirtyFlag())
            domain.setAllLinesReconciled(model.getAll_lines_reconciled());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getLine_idsDirtyFlag())
            domain.setLineIds(model.getLine_ids());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMove_line_countDirtyFlag())
            domain.setMoveLineCount(model.getMove_line_count());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getIs_difference_zeroDirtyFlag())
            domain.setIsDifferenceZero(model.getIs_difference_zero());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getDifferenceDirtyFlag())
            domain.setDifference(model.getDifference());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getReferenceDirtyFlag())
            domain.setReference(model.getReference());
        if(model.getBalance_startDirtyFlag())
            domain.setBalanceStart(model.getBalance_start());
        if(model.getAccounting_dateDirtyFlag())
            domain.setAccountingDate(model.getAccounting_date());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getJournal_typeDirtyFlag())
            domain.setJournalType(model.getJournal_type());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getCashbox_start_idDirtyFlag())
            domain.setCashboxStartId(model.getCashbox_start_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCashbox_end_idDirtyFlag())
            domain.setCashboxEndId(model.getCashbox_end_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



