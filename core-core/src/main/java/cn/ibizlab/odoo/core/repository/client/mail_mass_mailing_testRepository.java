package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mail_mass_mailing_test;

/**
 * 实体[mail_mass_mailing_test] 服务对象接口
 */
public interface mail_mass_mailing_testRepository{


    public mail_mass_mailing_test createPO() ;
        public void update(mail_mass_mailing_test mail_mass_mailing_test);

        public void create(mail_mass_mailing_test mail_mass_mailing_test);

        public List<mail_mass_mailing_test> search();

        public void createBatch(mail_mass_mailing_test mail_mass_mailing_test);

        public void removeBatch(String id);

        public void updateBatch(mail_mass_mailing_test mail_mass_mailing_test);

        public void get(String id);

        public void remove(String id);


}
