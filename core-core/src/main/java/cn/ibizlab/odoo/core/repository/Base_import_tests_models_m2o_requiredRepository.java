package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_import_tests_models_m2o_required;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2o_requiredSearchContext;

/**
 * 实体 [测试:基本导入模型，多对一必选] 存储对象
 */
public interface Base_import_tests_models_m2o_requiredRepository extends Repository<Base_import_tests_models_m2o_required> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_import_tests_models_m2o_required> searchDefault(Base_import_tests_models_m2o_requiredSearchContext context);

    Base_import_tests_models_m2o_required convert2PO(cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o_required domain , Base_import_tests_models_m2o_required po) ;

    cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o_required convert2Domain( Base_import_tests_models_m2o_required po ,cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o_required domain) ;

}
