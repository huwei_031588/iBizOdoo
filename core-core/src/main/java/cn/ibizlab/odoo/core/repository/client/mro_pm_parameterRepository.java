package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mro_pm_parameter;

/**
 * 实体[mro_pm_parameter] 服务对象接口
 */
public interface mro_pm_parameterRepository{


    public mro_pm_parameter createPO() ;
        public void removeBatch(String id);

        public void get(String id);

        public void create(mro_pm_parameter mro_pm_parameter);

        public void updateBatch(mro_pm_parameter mro_pm_parameter);

        public void createBatch(mro_pm_parameter mro_pm_parameter);

        public void remove(String id);

        public List<mro_pm_parameter> search();

        public void update(mro_pm_parameter mro_pm_parameter);


}
