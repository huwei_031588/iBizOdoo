package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_tracking_value;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_tracking_value] 服务对象接口
 */
public interface Imail_tracking_valueClientService{

    public Imail_tracking_value createModel() ;

    public Page<Imail_tracking_value> search(SearchContext context);

    public void get(Imail_tracking_value mail_tracking_value);

    public void update(Imail_tracking_value mail_tracking_value);

    public void removeBatch(List<Imail_tracking_value> mail_tracking_values);

    public void remove(Imail_tracking_value mail_tracking_value);

    public void createBatch(List<Imail_tracking_value> mail_tracking_values);

    public void create(Imail_tracking_value mail_tracking_value);

    public void updateBatch(List<Imail_tracking_value> mail_tracking_values);

    public Page<Imail_tracking_value> select(SearchContext context);

}
