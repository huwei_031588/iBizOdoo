package cn.ibizlab.odoo.core.odoo_crm.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[crm_stage] 对象
 */
public class crm_stageClientModel implements Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 在漏斗中折叠
     */
    public String fold;

    @JsonIgnore
    public boolean foldDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 优先级管理解释
     */
    public String legend_priority;

    @JsonIgnore
    public boolean legend_priorityDirtyFlag;
    
    /**
     * 阶段名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 自动修改概率
     */
    public String on_change;

    @JsonIgnore
    public boolean on_changeDirtyFlag;
    
    /**
     * 概率(%)
     */
    public Double probability;

    @JsonIgnore
    public boolean probabilityDirtyFlag;
    
    /**
     * 要求
     */
    public String requirements;

    @JsonIgnore
    public boolean requirementsDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * team_count
     */
    public Integer team_count;

    @JsonIgnore
    public boolean team_countDirtyFlag;
    
    /**
     * 销售团队
     */
    public Integer team_id;

    @JsonIgnore
    public boolean team_idDirtyFlag;
    
    /**
     * 销售团队
     */
    public String team_id_text;

    @JsonIgnore
    public boolean team_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [在漏斗中折叠]
     */
    @JsonProperty("fold")
    public String getFold(){
        return this.fold ;
    }

    /**
     * 设置 [在漏斗中折叠]
     */
    @JsonProperty("fold")
    public void setFold(String  fold){
        this.fold = fold ;
        this.foldDirtyFlag = true ;
    }

     /**
     * 获取 [在漏斗中折叠]脏标记
     */
    @JsonIgnore
    public boolean getFoldDirtyFlag(){
        return this.foldDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [优先级管理解释]
     */
    @JsonProperty("legend_priority")
    public String getLegend_priority(){
        return this.legend_priority ;
    }

    /**
     * 设置 [优先级管理解释]
     */
    @JsonProperty("legend_priority")
    public void setLegend_priority(String  legend_priority){
        this.legend_priority = legend_priority ;
        this.legend_priorityDirtyFlag = true ;
    }

     /**
     * 获取 [优先级管理解释]脏标记
     */
    @JsonIgnore
    public boolean getLegend_priorityDirtyFlag(){
        return this.legend_priorityDirtyFlag ;
    }   

    /**
     * 获取 [阶段名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [阶段名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [阶段名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [自动修改概率]
     */
    @JsonProperty("on_change")
    public String getOn_change(){
        return this.on_change ;
    }

    /**
     * 设置 [自动修改概率]
     */
    @JsonProperty("on_change")
    public void setOn_change(String  on_change){
        this.on_change = on_change ;
        this.on_changeDirtyFlag = true ;
    }

     /**
     * 获取 [自动修改概率]脏标记
     */
    @JsonIgnore
    public boolean getOn_changeDirtyFlag(){
        return this.on_changeDirtyFlag ;
    }   

    /**
     * 获取 [概率(%)]
     */
    @JsonProperty("probability")
    public Double getProbability(){
        return this.probability ;
    }

    /**
     * 设置 [概率(%)]
     */
    @JsonProperty("probability")
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.probabilityDirtyFlag = true ;
    }

     /**
     * 获取 [概率(%)]脏标记
     */
    @JsonIgnore
    public boolean getProbabilityDirtyFlag(){
        return this.probabilityDirtyFlag ;
    }   

    /**
     * 获取 [要求]
     */
    @JsonProperty("requirements")
    public String getRequirements(){
        return this.requirements ;
    }

    /**
     * 设置 [要求]
     */
    @JsonProperty("requirements")
    public void setRequirements(String  requirements){
        this.requirements = requirements ;
        this.requirementsDirtyFlag = true ;
    }

     /**
     * 获取 [要求]脏标记
     */
    @JsonIgnore
    public boolean getRequirementsDirtyFlag(){
        return this.requirementsDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [team_count]
     */
    @JsonProperty("team_count")
    public Integer getTeam_count(){
        return this.team_count ;
    }

    /**
     * 设置 [team_count]
     */
    @JsonProperty("team_count")
    public void setTeam_count(Integer  team_count){
        this.team_count = team_count ;
        this.team_countDirtyFlag = true ;
    }

     /**
     * 获取 [team_count]脏标记
     */
    @JsonIgnore
    public boolean getTeam_countDirtyFlag(){
        return this.team_countDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("fold") instanceof Boolean){
			this.setFold(((Boolean)map.get("fold"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("legend_priority") instanceof Boolean)&& map.get("legend_priority")!=null){
			this.setLegend_priority((String)map.get("legend_priority"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(map.get("on_change") instanceof Boolean){
			this.setOn_change(((Boolean)map.get("on_change"))? "true" : "false");
		}
		if(!(map.get("probability") instanceof Boolean)&& map.get("probability")!=null){
			this.setProbability((Double)map.get("probability"));
		}
		if(!(map.get("requirements") instanceof Boolean)&& map.get("requirements")!=null){
			this.setRequirements((String)map.get("requirements"));
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("team_count") instanceof Boolean)&& map.get("team_count")!=null){
			this.setTeam_count((Integer)map.get("team_count"));
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 0){
				this.setTeam_id((Integer)objs[0]);
			}
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 1){
				this.setTeam_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFold()!=null&&this.getFoldDirtyFlag()){
			map.put("fold",Boolean.parseBoolean(this.getFold()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLegend_priority()!=null&&this.getLegend_priorityDirtyFlag()){
			map.put("legend_priority",this.getLegend_priority());
		}else if(this.getLegend_priorityDirtyFlag()){
			map.put("legend_priority",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getOn_change()!=null&&this.getOn_changeDirtyFlag()){
			map.put("on_change",Boolean.parseBoolean(this.getOn_change()));		
		}		if(this.getProbability()!=null&&this.getProbabilityDirtyFlag()){
			map.put("probability",this.getProbability());
		}else if(this.getProbabilityDirtyFlag()){
			map.put("probability",false);
		}
		if(this.getRequirements()!=null&&this.getRequirementsDirtyFlag()){
			map.put("requirements",this.getRequirements());
		}else if(this.getRequirementsDirtyFlag()){
			map.put("requirements",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getTeam_count()!=null&&this.getTeam_countDirtyFlag()){
			map.put("team_count",this.getTeam_count());
		}else if(this.getTeam_countDirtyFlag()){
			map.put("team_count",false);
		}
		if(this.getTeam_id()!=null&&this.getTeam_idDirtyFlag()){
			map.put("team_id",this.getTeam_id());
		}else if(this.getTeam_idDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getTeam_id_text()!=null&&this.getTeam_id_textDirtyFlag()){
			//忽略文本外键team_id_text
		}else if(this.getTeam_id_textDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
