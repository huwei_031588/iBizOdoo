package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_partner_title;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_titleSearchContext;

/**
 * 实体 [业务伙伴称谓] 存储对象
 */
public interface Res_partner_titleRepository extends Repository<Res_partner_title> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_partner_title> searchDefault(Res_partner_titleSearchContext context);

    Res_partner_title convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_title domain , Res_partner_title po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_title convert2Domain( Res_partner_title po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_title domain) ;

}
