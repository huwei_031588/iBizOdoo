package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_request_rejectSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_request_rejectService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mro.client.mro_request_rejectOdooClient;
import cn.ibizlab.odoo.core.odoo_mro.clientmodel.mro_request_rejectClientModel;

/**
 * 实体[Reject Request] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_request_rejectServiceImpl implements IMro_request_rejectService {

    @Autowired
    mro_request_rejectOdooClient mro_request_rejectOdooClient;


    @Override
    public boolean create(Mro_request_reject et) {
        mro_request_rejectClientModel clientModel = convert2Model(et,null);
		mro_request_rejectOdooClient.create(clientModel);
        Mro_request_reject rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_request_reject> list){
    }

    @Override
    public boolean remove(Integer id) {
        mro_request_rejectClientModel clientModel = new mro_request_rejectClientModel();
        clientModel.setId(id);
		mro_request_rejectOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mro_request_reject get(Integer id) {
        mro_request_rejectClientModel clientModel = new mro_request_rejectClientModel();
        clientModel.setId(id);
		mro_request_rejectOdooClient.get(clientModel);
        Mro_request_reject et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mro_request_reject();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mro_request_reject et) {
        mro_request_rejectClientModel clientModel = convert2Model(et,null);
		mro_request_rejectOdooClient.update(clientModel);
        Mro_request_reject rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mro_request_reject> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_request_reject> searchDefault(Mro_request_rejectSearchContext context) {
        List<Mro_request_reject> list = new ArrayList<Mro_request_reject>();
        Page<mro_request_rejectClientModel> clientModelList = mro_request_rejectOdooClient.search(context);
        for(mro_request_rejectClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mro_request_reject>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mro_request_rejectClientModel convert2Model(Mro_request_reject domain , mro_request_rejectClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mro_request_rejectClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("reject_reasondirtyflag"))
                model.setReject_reason(domain.getRejectReason());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mro_request_reject convert2Domain( mro_request_rejectClientModel model ,Mro_request_reject domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mro_request_reject();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getReject_reasonDirtyFlag())
            domain.setRejectReason(model.getReject_reason());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



