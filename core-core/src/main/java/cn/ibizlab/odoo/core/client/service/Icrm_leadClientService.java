package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Icrm_lead;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_lead] 服务对象接口
 */
public interface Icrm_leadClientService{

    public Icrm_lead createModel() ;

    public void update(Icrm_lead crm_lead);

    public void updateBatch(List<Icrm_lead> crm_leads);

    public Page<Icrm_lead> search(SearchContext context);

    public void create(Icrm_lead crm_lead);

    public void createBatch(List<Icrm_lead> crm_leads);

    public void get(Icrm_lead crm_lead);

    public void removeBatch(List<Icrm_lead> crm_leads);

    public void remove(Icrm_lead crm_lead);

    public Page<Icrm_lead> select(SearchContext context);

}
