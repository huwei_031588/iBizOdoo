package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_sale.client.sale_order_templateOdooClient;
import cn.ibizlab.odoo.core.odoo_sale.clientmodel.sale_order_templateClientModel;

/**
 * 实体[报价单模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_order_templateServiceImpl implements ISale_order_templateService {

    @Autowired
    sale_order_templateOdooClient sale_order_templateOdooClient;


    @Override
    public boolean create(Sale_order_template et) {
        sale_order_templateClientModel clientModel = convert2Model(et,null);
		sale_order_templateOdooClient.create(clientModel);
        Sale_order_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_order_template> list){
    }

    @Override
    public boolean update(Sale_order_template et) {
        sale_order_templateClientModel clientModel = convert2Model(et,null);
		sale_order_templateOdooClient.update(clientModel);
        Sale_order_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Sale_order_template> list){
    }

    @Override
    public Sale_order_template get(Integer id) {
        sale_order_templateClientModel clientModel = new sale_order_templateClientModel();
        clientModel.setId(id);
		sale_order_templateOdooClient.get(clientModel);
        Sale_order_template et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Sale_order_template();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        sale_order_templateClientModel clientModel = new sale_order_templateClientModel();
        clientModel.setId(id);
		sale_order_templateOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_order_template> searchDefault(Sale_order_templateSearchContext context) {
        List<Sale_order_template> list = new ArrayList<Sale_order_template>();
        Page<sale_order_templateClientModel> clientModelList = sale_order_templateOdooClient.search(context);
        for(sale_order_templateClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Sale_order_template>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public sale_order_templateClientModel convert2Model(Sale_order_template domain , sale_order_templateClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new sale_order_templateClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("require_signaturedirtyflag"))
                model.setRequire_signature(domain.getRequireSignature());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("require_paymentdirtyflag"))
                model.setRequire_payment(domain.getRequirePayment());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("number_of_daysdirtyflag"))
                model.setNumber_of_days(domain.getNumberOfDays());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("sale_order_template_option_idsdirtyflag"))
                model.setSale_order_template_option_ids(domain.getSaleOrderTemplateOptionIds());
            if((Boolean) domain.getExtensionparams().get("sale_order_template_line_idsdirtyflag"))
                model.setSale_order_template_line_ids(domain.getSaleOrderTemplateLineIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("mail_template_id_textdirtyflag"))
                model.setMail_template_id_text(domain.getMailTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("mail_template_iddirtyflag"))
                model.setMail_template_id(domain.getMailTemplateId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Sale_order_template convert2Domain( sale_order_templateClientModel model ,Sale_order_template domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Sale_order_template();
        }

        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getRequire_signatureDirtyFlag())
            domain.setRequireSignature(model.getRequire_signature());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getRequire_paymentDirtyFlag())
            domain.setRequirePayment(model.getRequire_payment());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNumber_of_daysDirtyFlag())
            domain.setNumberOfDays(model.getNumber_of_days());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getSale_order_template_option_idsDirtyFlag())
            domain.setSaleOrderTemplateOptionIds(model.getSale_order_template_option_ids());
        if(model.getSale_order_template_line_idsDirtyFlag())
            domain.setSaleOrderTemplateLineIds(model.getSale_order_template_line_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getMail_template_id_textDirtyFlag())
            domain.setMailTemplateIdText(model.getMail_template_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getMail_template_idDirtyFlag())
            domain.setMailTemplateId(model.getMail_template_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



