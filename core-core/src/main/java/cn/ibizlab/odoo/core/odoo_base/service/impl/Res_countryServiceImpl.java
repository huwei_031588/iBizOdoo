package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_countrySearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_countryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_countryOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_countryClientModel;

/**
 * 实体[国家/地区] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_countryServiceImpl implements IRes_countryService {

    @Autowired
    res_countryOdooClient res_countryOdooClient;


    @Override
    public boolean update(Res_country et) {
        res_countryClientModel clientModel = convert2Model(et,null);
		res_countryOdooClient.update(clientModel);
        Res_country rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_country> list){
    }

    @Override
    public Res_country get(Integer id) {
        res_countryClientModel clientModel = new res_countryClientModel();
        clientModel.setId(id);
		res_countryOdooClient.get(clientModel);
        Res_country et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_country();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Res_country et) {
        res_countryClientModel clientModel = convert2Model(et,null);
		res_countryOdooClient.create(clientModel);
        Res_country rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_country> list){
    }

    @Override
    public boolean remove(Integer id) {
        res_countryClientModel clientModel = new res_countryClientModel();
        clientModel.setId(id);
		res_countryOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_country> searchDefault(Res_countrySearchContext context) {
        List<Res_country> list = new ArrayList<Res_country>();
        Page<res_countryClientModel> clientModelList = res_countryOdooClient.search(context);
        for(res_countryClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_country>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_countryClientModel convert2Model(Res_country domain , res_countryClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_countryClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("address_view_iddirtyflag"))
                model.setAddress_view_id(domain.getAddressViewId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("country_group_idsdirtyflag"))
                model.setCountry_group_ids(domain.getCountryGroupIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("state_idsdirtyflag"))
                model.setState_ids(domain.getStateIds());
            if((Boolean) domain.getExtensionparams().get("name_positiondirtyflag"))
                model.setName_position(domain.getNamePosition());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("phone_codedirtyflag"))
                model.setPhone_code(domain.getPhoneCode());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("vat_labeldirtyflag"))
                model.setVat_label(domain.getVatLabel());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("address_formatdirtyflag"))
                model.setAddress_format(domain.getAddressFormat());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_country convert2Domain( res_countryClientModel model ,Res_country domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_country();
        }

        if(model.getAddress_view_idDirtyFlag())
            domain.setAddressViewId(model.getAddress_view_id());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCountry_group_idsDirtyFlag())
            domain.setCountryGroupIds(model.getCountry_group_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getState_idsDirtyFlag())
            domain.setStateIds(model.getState_ids());
        if(model.getName_positionDirtyFlag())
            domain.setNamePosition(model.getName_position());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getPhone_codeDirtyFlag())
            domain.setPhoneCode(model.getPhone_code());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getVat_labelDirtyFlag())
            domain.setVatLabel(model.getVat_label());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getAddress_formatDirtyFlag())
            domain.setAddressFormat(model.getAddress_format());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        return domain ;
    }

}

    



