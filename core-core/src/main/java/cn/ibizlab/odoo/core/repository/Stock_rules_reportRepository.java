package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_rules_report;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_rules_reportSearchContext;

/**
 * 实体 [库存规则报告] 存储对象
 */
public interface Stock_rules_reportRepository extends Repository<Stock_rules_report> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_rules_report> searchDefault(Stock_rules_reportSearchContext context);

    Stock_rules_report convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rules_report domain , Stock_rules_report po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rules_report convert2Domain( Stock_rules_report po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rules_report domain) ;

}
