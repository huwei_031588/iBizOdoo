package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_followers;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_followersSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_followersService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_followersOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_followersClientModel;

/**
 * 实体[文档关注者] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_followersServiceImpl implements IMail_followersService {

    @Autowired
    mail_followersOdooClient mail_followersOdooClient;


    @Override
    public Mail_followers get(Integer id) {
        mail_followersClientModel clientModel = new mail_followersClientModel();
        clientModel.setId(id);
		mail_followersOdooClient.get(clientModel);
        Mail_followers et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_followers();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mail_followers et) {
        mail_followersClientModel clientModel = convert2Model(et,null);
		mail_followersOdooClient.create(clientModel);
        Mail_followers rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_followers> list){
    }

    @Override
    public boolean update(Mail_followers et) {
        mail_followersClientModel clientModel = convert2Model(et,null);
		mail_followersOdooClient.update(clientModel);
        Mail_followers rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_followers> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_followersClientModel clientModel = new mail_followersClientModel();
        clientModel.setId(id);
		mail_followersOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_followers> searchDefault(Mail_followersSearchContext context) {
        List<Mail_followers> list = new ArrayList<Mail_followers>();
        Page<mail_followersClientModel> clientModelList = mail_followersOdooClient.search(context);
        for(mail_followersClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_followers>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_followersClientModel convert2Model(Mail_followers domain , mail_followersClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_followersClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("subtype_idsdirtyflag"))
                model.setSubtype_ids(domain.getSubtypeIds());
            if((Boolean) domain.getExtensionparams().get("res_modeldirtyflag"))
                model.setRes_model(domain.getResModel());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("channel_id_textdirtyflag"))
                model.setChannel_id_text(domain.getChannelIdText());
            if((Boolean) domain.getExtensionparams().get("channel_iddirtyflag"))
                model.setChannel_id(domain.getChannelId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_followers convert2Domain( mail_followersClientModel model ,Mail_followers domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_followers();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getSubtype_idsDirtyFlag())
            domain.setSubtypeIds(model.getSubtype_ids());
        if(model.getRes_modelDirtyFlag())
            domain.setResModel(model.getRes_model());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getChannel_id_textDirtyFlag())
            domain.setChannelIdText(model.getChannel_id_text());
        if(model.getChannel_idDirtyFlag())
            domain.setChannelId(model.getChannel_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        return domain ;
    }

}

    



