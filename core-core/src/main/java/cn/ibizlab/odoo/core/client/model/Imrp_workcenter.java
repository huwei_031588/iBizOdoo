package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mrp_workcenter] 对象
 */
public interface Imrp_workcenter {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [阻塞时间]
     */
    public void setBlocked_time(Double blocked_time);
    
    /**
     * 设置 [阻塞时间]
     */
    public Double getBlocked_time();

    /**
     * 获取 [阻塞时间]脏标记
     */
    public boolean getBlocked_timeDirtyFlag();
    /**
     * 获取 [容量]
     */
    public void setCapacity(Double capacity);
    
    /**
     * 设置 [容量]
     */
    public Double getCapacity();

    /**
     * 获取 [容量]脏标记
     */
    public boolean getCapacityDirtyFlag();
    /**
     * 获取 [代码]
     */
    public void setCode(String code);
    
    /**
     * 设置 [代码]
     */
    public String getCode();

    /**
     * 获取 [代码]脏标记
     */
    public boolean getCodeDirtyFlag();
    /**
     * 获取 [颜色]
     */
    public void setColor(Integer color);
    
    /**
     * 设置 [颜色]
     */
    public Integer getColor();

    /**
     * 获取 [颜色]脏标记
     */
    public boolean getColorDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [每小时成本]
     */
    public void setCosts_hour(Double costs_hour);
    
    /**
     * 设置 [每小时成本]
     */
    public Double getCosts_hour();

    /**
     * 获取 [每小时成本]脏标记
     */
    public boolean getCosts_hourDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [工作中心]
     */
    public void setName(String name);
    
    /**
     * 设置 [工作中心]
     */
    public String getName();

    /**
     * 获取 [工作中心]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setNote(String note);
    
    /**
     * 设置 [说明]
     */
    public String getNote();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [OEE]
     */
    public void setOee(Double oee);
    
    /**
     * 设置 [OEE]
     */
    public Double getOee();

    /**
     * 获取 [OEE]脏标记
     */
    public boolean getOeeDirtyFlag();
    /**
     * 获取 [OEE 目标]
     */
    public void setOee_target(Double oee_target);
    
    /**
     * 设置 [OEE 目标]
     */
    public Double getOee_target();

    /**
     * 获取 [OEE 目标]脏标记
     */
    public boolean getOee_targetDirtyFlag();
    /**
     * 获取 [订单]
     */
    public void setOrder_ids(String order_ids);
    
    /**
     * 设置 [订单]
     */
    public String getOrder_ids();

    /**
     * 获取 [订单]脏标记
     */
    public boolean getOrder_idsDirtyFlag();
    /**
     * 获取 [效能]
     */
    public void setPerformance(Integer performance);
    
    /**
     * 设置 [效能]
     */
    public Integer getPerformance();

    /**
     * 获取 [效能]脏标记
     */
    public boolean getPerformanceDirtyFlag();
    /**
     * 获取 [生产性时间]
     */
    public void setProductive_time(Double productive_time);
    
    /**
     * 设置 [生产性时间]
     */
    public Double getProductive_time();

    /**
     * 获取 [生产性时间]脏标记
     */
    public boolean getProductive_timeDirtyFlag();
    /**
     * 获取 [工作时间]
     */
    public void setResource_calendar_id(Integer resource_calendar_id);
    
    /**
     * 设置 [工作时间]
     */
    public Integer getResource_calendar_id();

    /**
     * 获取 [工作时间]脏标记
     */
    public boolean getResource_calendar_idDirtyFlag();
    /**
     * 获取 [工作时间]
     */
    public void setResource_calendar_id_text(String resource_calendar_id_text);
    
    /**
     * 设置 [工作时间]
     */
    public String getResource_calendar_id_text();

    /**
     * 获取 [工作时间]脏标记
     */
    public boolean getResource_calendar_id_textDirtyFlag();
    /**
     * 获取 [资源]
     */
    public void setResource_id(Integer resource_id);
    
    /**
     * 设置 [资源]
     */
    public Integer getResource_id();

    /**
     * 获取 [资源]脏标记
     */
    public boolean getResource_idDirtyFlag();
    /**
     * 获取 [工艺行]
     */
    public void setRouting_line_ids(String routing_line_ids);
    
    /**
     * 设置 [工艺行]
     */
    public String getRouting_line_ids();

    /**
     * 获取 [工艺行]脏标记
     */
    public boolean getRouting_line_idsDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [时间效率]
     */
    public void setTime_efficiency(Double time_efficiency);
    
    /**
     * 设置 [时间效率]
     */
    public Double getTime_efficiency();

    /**
     * 获取 [时间效率]脏标记
     */
    public boolean getTime_efficiencyDirtyFlag();
    /**
     * 获取 [时间日志]
     */
    public void setTime_ids(String time_ids);
    
    /**
     * 设置 [时间日志]
     */
    public String getTime_ids();

    /**
     * 获取 [时间日志]脏标记
     */
    public boolean getTime_idsDirtyFlag();
    /**
     * 获取 [生产前时间]
     */
    public void setTime_start(Double time_start);
    
    /**
     * 设置 [生产前时间]
     */
    public Double getTime_start();

    /**
     * 获取 [生产前时间]脏标记
     */
    public boolean getTime_startDirtyFlag();
    /**
     * 获取 [生产后时间]
     */
    public void setTime_stop(Double time_stop);
    
    /**
     * 设置 [生产后时间]
     */
    public Double getTime_stop();

    /**
     * 获取 [生产后时间]脏标记
     */
    public boolean getTime_stopDirtyFlag();
    /**
     * 获取 [时区]
     */
    public void setTz(String tz);
    
    /**
     * 设置 [时区]
     */
    public String getTz();

    /**
     * 获取 [时区]脏标记
     */
    public boolean getTzDirtyFlag();
    /**
     * 获取 [工作中心负载]
     */
    public void setWorkcenter_load(Double workcenter_load);
    
    /**
     * 设置 [工作中心负载]
     */
    public Double getWorkcenter_load();

    /**
     * 获取 [工作中心负载]脏标记
     */
    public boolean getWorkcenter_loadDirtyFlag();
    /**
     * 获取 [工作中心状态]
     */
    public void setWorking_state(String working_state);
    
    /**
     * 设置 [工作中心状态]
     */
    public String getWorking_state();

    /**
     * 获取 [工作中心状态]脏标记
     */
    public boolean getWorking_stateDirtyFlag();
    /**
     * 获取 [# 工单]
     */
    public void setWorkorder_count(Integer workorder_count);
    
    /**
     * 设置 [# 工单]
     */
    public Integer getWorkorder_count();

    /**
     * 获取 [# 工单]脏标记
     */
    public boolean getWorkorder_countDirtyFlag();
    /**
     * 获取 [延迟的订单合计]
     */
    public void setWorkorder_late_count(Integer workorder_late_count);
    
    /**
     * 设置 [延迟的订单合计]
     */
    public Integer getWorkorder_late_count();

    /**
     * 获取 [延迟的订单合计]脏标记
     */
    public boolean getWorkorder_late_countDirtyFlag();
    /**
     * 获取 [待定的订单合计]
     */
    public void setWorkorder_pending_count(Integer workorder_pending_count);
    
    /**
     * 设置 [待定的订单合计]
     */
    public Integer getWorkorder_pending_count();

    /**
     * 获取 [待定的订单合计]脏标记
     */
    public boolean getWorkorder_pending_countDirtyFlag();
    /**
     * 获取 [运行的订单合计]
     */
    public void setWorkorder_progress_count(Integer workorder_progress_count);
    
    /**
     * 设置 [运行的订单合计]
     */
    public Integer getWorkorder_progress_count();

    /**
     * 获取 [运行的订单合计]脏标记
     */
    public boolean getWorkorder_progress_countDirtyFlag();
    /**
     * 获取 [# 读取工单]
     */
    public void setWorkorder_ready_count(Integer workorder_ready_count);
    
    /**
     * 设置 [# 读取工单]
     */
    public Integer getWorkorder_ready_count();

    /**
     * 获取 [# 读取工单]脏标记
     */
    public boolean getWorkorder_ready_countDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
