package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_orderSearchContext;

/**
 * 实体 [维修单] 存储模型
 */
public interface Repair_order{

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 内部备注
     */
    String getInternal_notes();

    void setInternal_notes(String internal_notes);

    /**
     * 获取 [内部备注]脏标记
     */
    boolean getInternal_notesDirtyFlag();

    /**
     * 默认地址
     */
    Integer getDefault_address_id();

    void setDefault_address_id(Integer default_address_id);

    /**
     * 获取 [默认地址]脏标记
     */
    boolean getDefault_address_idDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 开票方式
     */
    String getInvoice_method();

    void setInvoice_method(String invoice_method);

    /**
     * 获取 [开票方式]脏标记
     */
    boolean getInvoice_methodDirtyFlag();

    /**
     * 已开票
     */
    String getInvoiced();

    void setInvoiced(String invoiced);

    /**
     * 获取 [已开票]脏标记
     */
    boolean getInvoicedDirtyFlag();

    /**
     * 报价单说明
     */
    String getQuotation_notes();

    void setQuotation_notes(String quotation_notes);

    /**
     * 获取 [报价单说明]脏标记
     */
    boolean getQuotation_notesDirtyFlag();

    /**
     * 维修参照
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [维修参照]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 合计
     */
    Double getAmount_total();

    void setAmount_total(Double amount_total);

    /**
     * 获取 [合计]脏标记
     */
    boolean getAmount_totalDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 零件
     */
    String getOperations();

    void setOperations(String operations);

    /**
     * 获取 [零件]脏标记
     */
    boolean getOperationsDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 作业
     */
    String getFees_lines();

    void setFees_lines(String fees_lines);

    /**
     * 获取 [作业]脏标记
     */
    boolean getFees_linesDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 已维修
     */
    String getRepaired();

    void setRepaired(String repaired);

    /**
     * 获取 [已维修]脏标记
     */
    boolean getRepairedDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 未税金额
     */
    Double getAmount_untaxed();

    void setAmount_untaxed(Double amount_untaxed);

    /**
     * 获取 [未税金额]脏标记
     */
    boolean getAmount_untaxedDirtyFlag();

    /**
     * 质保到期
     */
    Timestamp getGuarantee_limit();

    void setGuarantee_limit(Timestamp guarantee_limit);

    /**
     * 获取 [质保到期]脏标记
     */
    boolean getGuarantee_limitDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改时间
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改时间]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 税
     */
    Double getAmount_tax();

    void setAmount_tax(Double amount_tax);

    /**
     * 获取 [税]脏标记
     */
    boolean getAmount_taxDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 数量
     */
    Double getProduct_qty();

    void setProduct_qty(Double product_qty);

    /**
     * 获取 [数量]脏标记
     */
    boolean getProduct_qtyDirtyFlag();

    /**
     * 价格表
     */
    String getPricelist_id_text();

    void setPricelist_id_text(String pricelist_id_text);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPricelist_id_textDirtyFlag();

    /**
     * 批次/序列号
     */
    String getLot_id_text();

    void setLot_id_text(String lot_id_text);

    /**
     * 获取 [批次/序列号]脏标记
     */
    boolean getLot_id_textDirtyFlag();

    /**
     * 客户
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 待维修产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [待维修产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 发票
     */
    String getInvoice_id_text();

    void setInvoice_id_text(String invoice_id_text);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_id_textDirtyFlag();

    /**
     * 产品量度单位
     */
    String getProduct_uom_text();

    void setProduct_uom_text(String product_uom_text);

    /**
     * 获取 [产品量度单位]脏标记
     */
    boolean getProduct_uom_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 创建者
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建者]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 开票地址
     */
    String getPartner_invoice_id_text();

    void setPartner_invoice_id_text(String partner_invoice_id_text);

    /**
     * 获取 [开票地址]脏标记
     */
    boolean getPartner_invoice_id_textDirtyFlag();

    /**
     * 收货地址
     */
    String getAddress_id_text();

    void setAddress_id_text(String address_id_text);

    /**
     * 获取 [收货地址]脏标记
     */
    boolean getAddress_id_textDirtyFlag();

    /**
     * 地点
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [地点]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 追踪
     */
    String getTracking();

    void setTracking(String tracking);

    /**
     * 获取 [追踪]脏标记
     */
    boolean getTrackingDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 移动
     */
    String getMove_id_text();

    void setMove_id_text(String move_id_text);

    /**
     * 获取 [移动]脏标记
     */
    boolean getMove_id_textDirtyFlag();

    /**
     * 移动
     */
    Integer getMove_id();

    void setMove_id(Integer move_id);

    /**
     * 获取 [移动]脏标记
     */
    boolean getMove_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 客户
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 价格表
     */
    Integer getPricelist_id();

    void setPricelist_id(Integer pricelist_id);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPricelist_idDirtyFlag();

    /**
     * 待维修产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [待维修产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 开票地址
     */
    Integer getPartner_invoice_id();

    void setPartner_invoice_id(Integer partner_invoice_id);

    /**
     * 获取 [开票地址]脏标记
     */
    boolean getPartner_invoice_idDirtyFlag();

    /**
     * 地点
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [地点]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 产品量度单位
     */
    Integer getProduct_uom();

    void setProduct_uom(Integer product_uom);

    /**
     * 获取 [产品量度单位]脏标记
     */
    boolean getProduct_uomDirtyFlag();

    /**
     * 创建者
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建者]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 收货地址
     */
    Integer getAddress_id();

    void setAddress_id(Integer address_id);

    /**
     * 获取 [收货地址]脏标记
     */
    boolean getAddress_idDirtyFlag();

    /**
     * 发票
     */
    Integer getInvoice_id();

    void setInvoice_id(Integer invoice_id);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_idDirtyFlag();

    /**
     * 批次/序列号
     */
    Integer getLot_id();

    void setLot_id(Integer lot_id);

    /**
     * 获取 [批次/序列号]脏标记
     */
    boolean getLot_idDirtyFlag();

}
