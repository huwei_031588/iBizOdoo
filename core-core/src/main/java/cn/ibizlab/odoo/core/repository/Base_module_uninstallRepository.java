package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_module_uninstall;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_uninstallSearchContext;

/**
 * 实体 [模块卸载] 存储对象
 */
public interface Base_module_uninstallRepository extends Repository<Base_module_uninstall> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_module_uninstall> searchDefault(Base_module_uninstallSearchContext context);

    Base_module_uninstall convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall domain , Base_module_uninstall po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall convert2Domain( Base_module_uninstall po ,cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall domain) ;

}
