package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_partial_reconcile] 对象
 */
public interface Iaccount_partial_reconcile {

    /**
     * 获取 [金额]
     */
    public void setAmount(Double amount);
    
    /**
     * 设置 [金额]
     */
    public Double getAmount();

    /**
     * 获取 [金额]脏标记
     */
    public boolean getAmountDirtyFlag();
    /**
     * 获取 [外币金额]
     */
    public void setAmount_currency(Double amount_currency);
    
    /**
     * 设置 [外币金额]
     */
    public Double getAmount_currency();

    /**
     * 获取 [外币金额]脏标记
     */
    public boolean getAmount_currencyDirtyFlag();
    /**
     * 获取 [公司货币]
     */
    public void setCompany_currency_id(Integer company_currency_id);
    
    /**
     * 设置 [公司货币]
     */
    public Integer getCompany_currency_id();

    /**
     * 获取 [公司货币]脏标记
     */
    public boolean getCompany_currency_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [贷方凭证]
     */
    public void setCredit_move_id(Integer credit_move_id);
    
    /**
     * 设置 [贷方凭证]
     */
    public Integer getCredit_move_id();

    /**
     * 获取 [贷方凭证]脏标记
     */
    public boolean getCredit_move_idDirtyFlag();
    /**
     * 获取 [贷方凭证]
     */
    public void setCredit_move_id_text(String credit_move_id_text);
    
    /**
     * 设置 [贷方凭证]
     */
    public String getCredit_move_id_text();

    /**
     * 获取 [贷方凭证]脏标记
     */
    public boolean getCredit_move_id_textDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id_text(String currency_id_text);
    
    /**
     * 设置 [币种]
     */
    public String getCurrency_id_text();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_id_textDirtyFlag();
    /**
     * 获取 [借方移动]
     */
    public void setDebit_move_id(Integer debit_move_id);
    
    /**
     * 设置 [借方移动]
     */
    public Integer getDebit_move_id();

    /**
     * 获取 [借方移动]脏标记
     */
    public boolean getDebit_move_idDirtyFlag();
    /**
     * 获取 [借方移动]
     */
    public void setDebit_move_id_text(String debit_move_id_text);
    
    /**
     * 设置 [借方移动]
     */
    public String getDebit_move_id_text();

    /**
     * 获取 [借方移动]脏标记
     */
    public boolean getDebit_move_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [完全核销]
     */
    public void setFull_reconcile_id(Integer full_reconcile_id);
    
    /**
     * 设置 [完全核销]
     */
    public Integer getFull_reconcile_id();

    /**
     * 获取 [完全核销]脏标记
     */
    public boolean getFull_reconcile_idDirtyFlag();
    /**
     * 获取 [完全核销]
     */
    public void setFull_reconcile_id_text(String full_reconcile_id_text);
    
    /**
     * 设置 [完全核销]
     */
    public String getFull_reconcile_id_text();

    /**
     * 获取 [完全核销]脏标记
     */
    public boolean getFull_reconcile_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [匹配明细的最大时间]
     */
    public void setMax_date(Timestamp max_date);
    
    /**
     * 设置 [匹配明细的最大时间]
     */
    public Timestamp getMax_date();

    /**
     * 获取 [匹配明细的最大时间]脏标记
     */
    public boolean getMax_dateDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
