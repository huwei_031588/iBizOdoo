package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_user_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_badge_user_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_badge_user_wizardOdooClient;
import cn.ibizlab.odoo.core.odoo_gamification.clientmodel.gamification_badge_user_wizardClientModel;

/**
 * 实体[游戏化用户徽章向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_badge_user_wizardServiceImpl implements IGamification_badge_user_wizardService {

    @Autowired
    gamification_badge_user_wizardOdooClient gamification_badge_user_wizardOdooClient;


    @Override
    public boolean update(Gamification_badge_user_wizard et) {
        gamification_badge_user_wizardClientModel clientModel = convert2Model(et,null);
		gamification_badge_user_wizardOdooClient.update(clientModel);
        Gamification_badge_user_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Gamification_badge_user_wizard> list){
    }

    @Override
    public Gamification_badge_user_wizard get(Integer id) {
        gamification_badge_user_wizardClientModel clientModel = new gamification_badge_user_wizardClientModel();
        clientModel.setId(id);
		gamification_badge_user_wizardOdooClient.get(clientModel);
        Gamification_badge_user_wizard et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Gamification_badge_user_wizard();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Gamification_badge_user_wizard et) {
        gamification_badge_user_wizardClientModel clientModel = convert2Model(et,null);
		gamification_badge_user_wizardOdooClient.create(clientModel);
        Gamification_badge_user_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_badge_user_wizard> list){
    }

    @Override
    public boolean remove(Integer id) {
        gamification_badge_user_wizardClientModel clientModel = new gamification_badge_user_wizardClientModel();
        clientModel.setId(id);
		gamification_badge_user_wizardOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_badge_user_wizard> searchDefault(Gamification_badge_user_wizardSearchContext context) {
        List<Gamification_badge_user_wizard> list = new ArrayList<Gamification_badge_user_wizard>();
        Page<gamification_badge_user_wizardClientModel> clientModelList = gamification_badge_user_wizardOdooClient.search(context);
        for(gamification_badge_user_wizardClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Gamification_badge_user_wizard>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public gamification_badge_user_wizardClientModel convert2Model(Gamification_badge_user_wizard domain , gamification_badge_user_wizardClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new gamification_badge_user_wizardClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("commentdirtyflag"))
                model.setComment(domain.getComment());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("employee_id_textdirtyflag"))
                model.setEmployee_id_text(domain.getEmployeeIdText());
            if((Boolean) domain.getExtensionparams().get("badge_id_textdirtyflag"))
                model.setBadge_id_text(domain.getBadgeIdText());
            if((Boolean) domain.getExtensionparams().get("employee_iddirtyflag"))
                model.setEmployee_id(domain.getEmployeeId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("badge_iddirtyflag"))
                model.setBadge_id(domain.getBadgeId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Gamification_badge_user_wizard convert2Domain( gamification_badge_user_wizardClientModel model ,Gamification_badge_user_wizard domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Gamification_badge_user_wizard();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCommentDirtyFlag())
            domain.setComment(model.getComment());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getEmployee_id_textDirtyFlag())
            domain.setEmployeeIdText(model.getEmployee_id_text());
        if(model.getBadge_id_textDirtyFlag())
            domain.setBadgeIdText(model.getBadge_id_text());
        if(model.getEmployee_idDirtyFlag())
            domain.setEmployeeId(model.getEmployee_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getBadge_idDirtyFlag())
            domain.setBadgeId(model.getBadge_id());
        return domain ;
    }

}

    



