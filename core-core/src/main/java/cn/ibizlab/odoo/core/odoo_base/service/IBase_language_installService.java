package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_install;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_installSearchContext;


/**
 * 实体[Base_language_install] 服务对象接口
 */
public interface IBase_language_installService{

    boolean create(Base_language_install et) ;
    void createBatch(List<Base_language_install> list) ;
    boolean update(Base_language_install et) ;
    void updateBatch(List<Base_language_install> list) ;
    Base_language_install get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Base_language_install> searchDefault(Base_language_installSearchContext context) ;

}



