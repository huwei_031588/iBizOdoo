package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_web_tour.filter.Web_tour_tourSearchContext;

/**
 * 实体 [向导] 存储模型
 */
public interface Web_tour_tour{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 向导名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [向导名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 修改日期
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [修改日期]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 消耗于
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [消耗于]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 消耗于
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [消耗于]脏标记
     */
    boolean getUser_idDirtyFlag();

}
