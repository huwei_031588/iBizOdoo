package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Event_type_mail;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_type_mailSearchContext;

/**
 * 实体 [基于活动分类的邮件调度] 存储对象
 */
public interface Event_type_mailRepository extends Repository<Event_type_mail> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Event_type_mail> searchDefault(Event_type_mailSearchContext context);

    Event_type_mail convert2PO(cn.ibizlab.odoo.core.odoo_event.domain.Event_type_mail domain , Event_type_mail po) ;

    cn.ibizlab.odoo.core.odoo_event.domain.Event_type_mail convert2Domain( Event_type_mail po ,cn.ibizlab.odoo.core.odoo_event.domain.Event_type_mail domain) ;

}
