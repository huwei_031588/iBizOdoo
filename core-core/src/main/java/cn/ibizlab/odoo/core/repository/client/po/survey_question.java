package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [survey_question] 对象
 */
public interface survey_question {

    public String getColumn_nb();

    public void setColumn_nb(String column_nb);

    public String getComments_allowed();

    public void setComments_allowed(String comments_allowed);

    public String getComments_message();

    public void setComments_message(String comments_message);

    public String getComment_count_as_answer();

    public void setComment_count_as_answer(String comment_count_as_answer);

    public String getConstr_error_msg();

    public void setConstr_error_msg(String constr_error_msg);

    public String getConstr_mandatory();

    public void setConstr_mandatory(String constr_mandatory);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_mode();

    public void setDisplay_mode(String display_mode);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getLabels_ids();

    public void setLabels_ids(String labels_ids);

    public String getLabels_ids_2();

    public void setLabels_ids_2(String labels_ids_2);

    public String getMatrix_subtype();

    public void setMatrix_subtype(String matrix_subtype);

    public Integer getPage_id();

    public void setPage_id(Integer page_id);

    public String getQuestion();

    public void setQuestion(String question);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public Integer getSurvey_id();

    public void setSurvey_id(Integer survey_id);

    public String getType();

    public void setType(String type);

    public String getUser_input_line_ids();

    public void setUser_input_line_ids(String user_input_line_ids);

    public String getValidation_email();

    public void setValidation_email(String validation_email);

    public String getValidation_error_msg();

    public void setValidation_error_msg(String validation_error_msg);

    public Integer getValidation_length_max();

    public void setValidation_length_max(Integer validation_length_max);

    public Integer getValidation_length_min();

    public void setValidation_length_min(Integer validation_length_min);

    public Timestamp getValidation_max_date();

    public void setValidation_max_date(Timestamp validation_max_date);

    public Double getValidation_max_float_value();

    public void setValidation_max_float_value(Double validation_max_float_value);

    public Timestamp getValidation_min_date();

    public void setValidation_min_date(Timestamp validation_min_date);

    public Double getValidation_min_float_value();

    public void setValidation_min_float_value(Double validation_min_float_value);

    public String getValidation_required();

    public void setValidation_required(String validation_required);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
