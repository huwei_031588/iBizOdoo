package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Gamification_goal;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goalSearchContext;

/**
 * 实体 [游戏化目标] 存储对象
 */
public interface Gamification_goalRepository extends Repository<Gamification_goal> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Gamification_goal> searchDefault(Gamification_goalSearchContext context);

    Gamification_goal convert2PO(cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal domain , Gamification_goal po) ;

    cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal convert2Domain( Gamification_goal po ,cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal domain) ;

}
