package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_applicant;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicantSearchContext;

/**
 * 实体 [申请人] 存储对象
 */
public interface Hr_applicantRepository extends Repository<Hr_applicant> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_applicant> searchDefault(Hr_applicantSearchContext context);

    Hr_applicant convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant domain , Hr_applicant po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant convert2Domain( Hr_applicant po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant domain) ;

}
