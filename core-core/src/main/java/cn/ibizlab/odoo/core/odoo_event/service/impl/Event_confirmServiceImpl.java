package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_confirm;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_confirmSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_confirmService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_event.client.event_confirmOdooClient;
import cn.ibizlab.odoo.core.odoo_event.clientmodel.event_confirmClientModel;

/**
 * 实体[活动确认] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_confirmServiceImpl implements IEvent_confirmService {

    @Autowired
    event_confirmOdooClient event_confirmOdooClient;


    @Override
    public boolean remove(Integer id) {
        event_confirmClientModel clientModel = new event_confirmClientModel();
        clientModel.setId(id);
		event_confirmOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Event_confirm et) {
        event_confirmClientModel clientModel = convert2Model(et,null);
		event_confirmOdooClient.update(clientModel);
        Event_confirm rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Event_confirm> list){
    }

    @Override
    public Event_confirm get(Integer id) {
        event_confirmClientModel clientModel = new event_confirmClientModel();
        clientModel.setId(id);
		event_confirmOdooClient.get(clientModel);
        Event_confirm et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Event_confirm();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Event_confirm et) {
        event_confirmClientModel clientModel = convert2Model(et,null);
		event_confirmOdooClient.create(clientModel);
        Event_confirm rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_confirm> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_confirm> searchDefault(Event_confirmSearchContext context) {
        List<Event_confirm> list = new ArrayList<Event_confirm>();
        Page<event_confirmClientModel> clientModelList = event_confirmOdooClient.search(context);
        for(event_confirmClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Event_confirm>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public event_confirmClientModel convert2Model(Event_confirm domain , event_confirmClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new event_confirmClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Event_confirm convert2Domain( event_confirmClientModel model ,Event_confirm domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Event_confirm();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



