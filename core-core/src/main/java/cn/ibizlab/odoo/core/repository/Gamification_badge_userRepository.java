package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Gamification_badge_user;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_userSearchContext;

/**
 * 实体 [游戏化用户徽章] 存储对象
 */
public interface Gamification_badge_userRepository extends Repository<Gamification_badge_user> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Gamification_badge_user> searchDefault(Gamification_badge_userSearchContext context);

    Gamification_badge_user convert2PO(cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user domain , Gamification_badge_user po) ;

    cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user convert2Domain( Gamification_badge_user po ,cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user domain) ;

}
