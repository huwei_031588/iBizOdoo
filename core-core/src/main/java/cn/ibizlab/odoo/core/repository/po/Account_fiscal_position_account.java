package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_accountSearchContext;

/**
 * 实体 [会计税科目映射] 存储模型
 */
public interface Account_fiscal_position_account{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 税科目调整
     */
    String getPosition_id_text();

    void setPosition_id_text(String position_id_text);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getPosition_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 产品上的科目
     */
    String getAccount_src_id_text();

    void setAccount_src_id_text(String account_src_id_text);

    /**
     * 获取 [产品上的科目]脏标记
     */
    boolean getAccount_src_id_textDirtyFlag();

    /**
     * 替代使用的科目
     */
    String getAccount_dest_id_text();

    void setAccount_dest_id_text(String account_dest_id_text);

    /**
     * 获取 [替代使用的科目]脏标记
     */
    boolean getAccount_dest_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 税科目调整
     */
    Integer getPosition_id();

    void setPosition_id(Integer position_id);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getPosition_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 替代使用的科目
     */
    Integer getAccount_dest_id();

    void setAccount_dest_id(Integer account_dest_id);

    /**
     * 获取 [替代使用的科目]脏标记
     */
    boolean getAccount_dest_idDirtyFlag();

    /**
     * 产品上的科目
     */
    Integer getAccount_src_id();

    void setAccount_src_id(Integer account_src_id);

    /**
     * 获取 [产品上的科目]脏标记
     */
    boolean getAccount_src_idDirtyFlag();

}
