package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_order_parts_lineSearchContext;

/**
 * 实体 [Maintenance Planned Parts] 存储模型
 */
public interface Mro_order_parts_line{

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 数量
     */
    Double getParts_qty();

    void setParts_qty(Double parts_qty);

    /**
     * 获取 [数量]脏标记
     */
    boolean getParts_qtyDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 单位
     */
    String getParts_uom_text();

    void setParts_uom_text(String parts_uom_text);

    /**
     * 获取 [单位]脏标记
     */
    boolean getParts_uom_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 零件
     */
    String getParts_id_text();

    void setParts_id_text(String parts_id_text);

    /**
     * 获取 [零件]脏标记
     */
    boolean getParts_id_textDirtyFlag();

    /**
     * Maintenance Order
     */
    String getMaintenance_id_text();

    void setMaintenance_id_text(String maintenance_id_text);

    /**
     * 获取 [Maintenance Order]脏标记
     */
    boolean getMaintenance_id_textDirtyFlag();

    /**
     * 零件
     */
    Integer getParts_id();

    void setParts_id(Integer parts_id);

    /**
     * 获取 [零件]脏标记
     */
    boolean getParts_idDirtyFlag();

    /**
     * Maintenance Order
     */
    Integer getMaintenance_id();

    void setMaintenance_id(Integer maintenance_id);

    /**
     * 获取 [Maintenance Order]脏标记
     */
    boolean getMaintenance_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 单位
     */
    Integer getParts_uom();

    void setParts_uom(Integer parts_uom);

    /**
     * 获取 [单位]脏标记
     */
    boolean getParts_uomDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
