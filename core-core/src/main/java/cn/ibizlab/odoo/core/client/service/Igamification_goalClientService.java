package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Igamification_goal;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_goal] 服务对象接口
 */
public interface Igamification_goalClientService{

    public Igamification_goal createModel() ;

    public void updateBatch(List<Igamification_goal> gamification_goals);

    public void removeBatch(List<Igamification_goal> gamification_goals);

    public Page<Igamification_goal> search(SearchContext context);

    public void get(Igamification_goal gamification_goal);

    public void createBatch(List<Igamification_goal> gamification_goals);

    public void update(Igamification_goal gamification_goal);

    public void create(Igamification_goal gamification_goal);

    public void remove(Igamification_goal gamification_goal);

    public Page<Igamification_goal> select(SearchContext context);

}
