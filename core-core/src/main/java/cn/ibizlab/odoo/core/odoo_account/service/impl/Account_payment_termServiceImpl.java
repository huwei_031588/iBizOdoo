package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_termSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_termService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_payment_termOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_payment_termClientModel;

/**
 * 实体[付款条款] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_payment_termServiceImpl implements IAccount_payment_termService {

    @Autowired
    account_payment_termOdooClient account_payment_termOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_payment_termClientModel clientModel = new account_payment_termClientModel();
        clientModel.setId(id);
		account_payment_termOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_payment_term et) {
        account_payment_termClientModel clientModel = convert2Model(et,null);
		account_payment_termOdooClient.create(clientModel);
        Account_payment_term rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_payment_term> list){
    }

    @Override
    public Account_payment_term get(Integer id) {
        account_payment_termClientModel clientModel = new account_payment_termClientModel();
        clientModel.setId(id);
		account_payment_termOdooClient.get(clientModel);
        Account_payment_term et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_payment_term();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Account_payment_term et) {
        account_payment_termClientModel clientModel = convert2Model(et,null);
		account_payment_termOdooClient.update(clientModel);
        Account_payment_term rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_payment_term> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_payment_term> searchDefault(Account_payment_termSearchContext context) {
        List<Account_payment_term> list = new ArrayList<Account_payment_term>();
        Page<account_payment_termClientModel> clientModelList = account_payment_termOdooClient.search(context);
        for(account_payment_termClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_payment_term>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_payment_termClientModel convert2Model(Account_payment_term domain , account_payment_termClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_payment_termClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("line_idsdirtyflag"))
                model.setLine_ids(domain.getLineIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_payment_term convert2Domain( account_payment_termClientModel model ,Account_payment_term domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_payment_term();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getLine_idsDirtyFlag())
            domain.setLineIds(model.getLine_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



