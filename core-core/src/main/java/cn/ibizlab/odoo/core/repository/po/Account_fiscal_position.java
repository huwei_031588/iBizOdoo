package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_positionSearchContext;

/**
 * 实体 [税科目调整] 存储模型
 */
public interface Account_fiscal_position{

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 税科目调整
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * VAT必须
     */
    String getVat_required();

    void setVat_required(String vat_required);

    /**
     * 获取 [VAT必须]脏标记
     */
    boolean getVat_requiredDirtyFlag();

    /**
     * 科目映射
     */
    String getAccount_ids();

    void setAccount_ids(String account_ids);

    /**
     * 获取 [科目映射]脏标记
     */
    boolean getAccount_idsDirtyFlag();

    /**
     * 状态数
     */
    Integer getStates_count();

    void setStates_count(Integer states_count);

    /**
     * 获取 [状态数]脏标记
     */
    boolean getStates_countDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 联邦政府
     */
    String getState_ids();

    void setState_ids(String state_ids);

    /**
     * 获取 [联邦政府]脏标记
     */
    boolean getState_idsDirtyFlag();

    /**
     * 自动检测
     */
    String getAuto_apply();

    void setAuto_apply(String auto_apply);

    /**
     * 获取 [自动检测]脏标记
     */
    boolean getAuto_applyDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 备注
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [备注]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 邮编范围到
     */
    Integer getZip_to();

    void setZip_to(Integer zip_to);

    /**
     * 获取 [邮编范围到]脏标记
     */
    boolean getZip_toDirtyFlag();

    /**
     * 税映射
     */
    String getTax_ids();

    void setTax_ids(String tax_ids);

    /**
     * 获取 [税映射]脏标记
     */
    boolean getTax_idsDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 邮编范围从
     */
    Integer getZip_from();

    void setZip_from(Integer zip_from);

    /**
     * 获取 [邮编范围从]脏标记
     */
    boolean getZip_fromDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 国家群组
     */
    String getCountry_group_id_text();

    void setCountry_group_id_text(String country_group_id_text);

    /**
     * 获取 [国家群组]脏标记
     */
    boolean getCountry_group_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 国家
     */
    String getCountry_id_text();

    void setCountry_id_text(String country_id_text);

    /**
     * 获取 [国家]脏标记
     */
    boolean getCountry_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 国家
     */
    Integer getCountry_id();

    void setCountry_id(Integer country_id);

    /**
     * 获取 [国家]脏标记
     */
    boolean getCountry_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 国家群组
     */
    Integer getCountry_group_id();

    void setCountry_group_id(Integer country_group_id);

    /**
     * 获取 [国家群组]脏标记
     */
    boolean getCountry_group_idDirtyFlag();

}
