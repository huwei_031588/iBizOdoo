package cn.ibizlab.odoo.core.odoo_project.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_project;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_projectSearchContext;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_projectService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_project.client.project_projectOdooClient;
import cn.ibizlab.odoo.core.odoo_project.clientmodel.project_projectClientModel;

/**
 * 实体[项目] 服务对象接口实现
 */
@Slf4j
@Service
public class Project_projectServiceImpl implements IProject_projectService {

    @Autowired
    project_projectOdooClient project_projectOdooClient;


    @Override
    public Project_project get(Integer id) {
        project_projectClientModel clientModel = new project_projectClientModel();
        clientModel.setId(id);
		project_projectOdooClient.get(clientModel);
        Project_project et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Project_project();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Project_project et) {
        project_projectClientModel clientModel = convert2Model(et,null);
		project_projectOdooClient.update(clientModel);
        Project_project rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Project_project> list){
    }

    @Override
    public boolean create(Project_project et) {
        project_projectClientModel clientModel = convert2Model(et,null);
		project_projectOdooClient.create(clientModel);
        Project_project rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Project_project> list){
    }

    @Override
    public boolean remove(Integer id) {
        project_projectClientModel clientModel = new project_projectClientModel();
        clientModel.setId(id);
		project_projectOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Project_project> searchDefault(Project_projectSearchContext context) {
        List<Project_project> list = new ArrayList<Project_project>();
        Page<project_projectClientModel> clientModelList = project_projectOdooClient.search(context);
        for(project_projectClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Project_project>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public project_projectClientModel convert2Model(Project_project domain , project_projectClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new project_projectClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("access_warningdirtyflag"))
                model.setAccess_warning(domain.getAccessWarning());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("tasksdirtyflag"))
                model.setTasks(domain.getTasks());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("percentage_satisfaction_taskdirtyflag"))
                model.setPercentage_satisfaction_task(domain.getPercentageSatisfactionTask());
            if((Boolean) domain.getExtensionparams().get("access_urldirtyflag"))
                model.setAccess_url(domain.getAccessUrl());
            if((Boolean) domain.getExtensionparams().get("type_idsdirtyflag"))
                model.setType_ids(domain.getTypeIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("access_tokendirtyflag"))
                model.setAccess_token(domain.getAccessToken());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("privacy_visibilitydirtyflag"))
                model.setPrivacy_visibility(domain.getPrivacyVisibility());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("rating_status_perioddirtyflag"))
                model.setRating_status_period(domain.getRatingStatusPeriod());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("percentage_satisfaction_projectdirtyflag"))
                model.setPercentage_satisfaction_project(domain.getPercentageSatisfactionProject());
            if((Boolean) domain.getExtensionparams().get("doc_countdirtyflag"))
                model.setDoc_count(domain.getDocCount());
            if((Boolean) domain.getExtensionparams().get("favorite_user_idsdirtyflag"))
                model.setFavorite_user_ids(domain.getFavoriteUserIds());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("task_countdirtyflag"))
                model.setTask_count(domain.getTaskCount());
            if((Boolean) domain.getExtensionparams().get("is_favoritedirtyflag"))
                model.setIs_favorite(domain.getIsFavorite());
            if((Boolean) domain.getExtensionparams().get("portal_show_ratingdirtyflag"))
                model.setPortal_show_rating(domain.getPortalShowRating());
            if((Boolean) domain.getExtensionparams().get("rating_request_deadlinedirtyflag"))
                model.setRating_request_deadline(domain.getRatingRequestDeadline());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("rating_statusdirtyflag"))
                model.setRating_status(domain.getRatingStatus());
            if((Boolean) domain.getExtensionparams().get("label_tasksdirtyflag"))
                model.setLabel_tasks(domain.getLabelTasks());
            if((Boolean) domain.getExtensionparams().get("task_idsdirtyflag"))
                model.setTask_ids(domain.getTaskIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("date_startdirtyflag"))
                model.setDate_start(domain.getDateStart());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("alias_parent_thread_iddirtyflag"))
                model.setAlias_parent_thread_id(domain.getAliasParentThreadId());
            if((Boolean) domain.getExtensionparams().get("alias_force_thread_iddirtyflag"))
                model.setAlias_force_thread_id(domain.getAliasForceThreadId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("alias_domaindirtyflag"))
                model.setAlias_domain(domain.getAliasDomain());
            if((Boolean) domain.getExtensionparams().get("alias_contactdirtyflag"))
                model.setAlias_contact(domain.getAliasContact());
            if((Boolean) domain.getExtensionparams().get("alias_parent_model_iddirtyflag"))
                model.setAlias_parent_model_id(domain.getAliasParentModelId());
            if((Boolean) domain.getExtensionparams().get("alias_model_iddirtyflag"))
                model.setAlias_model_id(domain.getAliasModelId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("alias_user_iddirtyflag"))
                model.setAlias_user_id(domain.getAliasUserId());
            if((Boolean) domain.getExtensionparams().get("alias_defaultsdirtyflag"))
                model.setAlias_defaults(domain.getAliasDefaults());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_id_textdirtyflag"))
                model.setResource_calendar_id_text(domain.getResourceCalendarIdText());
            if((Boolean) domain.getExtensionparams().get("subtask_project_id_textdirtyflag"))
                model.setSubtask_project_id_text(domain.getSubtaskProjectIdText());
            if((Boolean) domain.getExtensionparams().get("alias_namedirtyflag"))
                model.setAlias_name(domain.getAliasName());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("subtask_project_iddirtyflag"))
                model.setSubtask_project_id(domain.getSubtaskProjectId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_iddirtyflag"))
                model.setResource_calendar_id(domain.getResourceCalendarId());
            if((Boolean) domain.getExtensionparams().get("alias_iddirtyflag"))
                model.setAlias_id(domain.getAliasId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Project_project convert2Domain( project_projectClientModel model ,Project_project domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Project_project();
        }

        if(model.getAccess_warningDirtyFlag())
            domain.setAccessWarning(model.getAccess_warning());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getTasksDirtyFlag())
            domain.setTasks(model.getTasks());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getPercentage_satisfaction_taskDirtyFlag())
            domain.setPercentageSatisfactionTask(model.getPercentage_satisfaction_task());
        if(model.getAccess_urlDirtyFlag())
            domain.setAccessUrl(model.getAccess_url());
        if(model.getType_idsDirtyFlag())
            domain.setTypeIds(model.getType_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getAccess_tokenDirtyFlag())
            domain.setAccessToken(model.getAccess_token());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getPrivacy_visibilityDirtyFlag())
            domain.setPrivacyVisibility(model.getPrivacy_visibility());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getRating_status_periodDirtyFlag())
            domain.setRatingStatusPeriod(model.getRating_status_period());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getPercentage_satisfaction_projectDirtyFlag())
            domain.setPercentageSatisfactionProject(model.getPercentage_satisfaction_project());
        if(model.getDoc_countDirtyFlag())
            domain.setDocCount(model.getDoc_count());
        if(model.getFavorite_user_idsDirtyFlag())
            domain.setFavoriteUserIds(model.getFavorite_user_ids());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getTask_countDirtyFlag())
            domain.setTaskCount(model.getTask_count());
        if(model.getIs_favoriteDirtyFlag())
            domain.setIsFavorite(model.getIs_favorite());
        if(model.getPortal_show_ratingDirtyFlag())
            domain.setPortalShowRating(model.getPortal_show_rating());
        if(model.getRating_request_deadlineDirtyFlag())
            domain.setRatingRequestDeadline(model.getRating_request_deadline());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getRating_statusDirtyFlag())
            domain.setRatingStatus(model.getRating_status());
        if(model.getLabel_tasksDirtyFlag())
            domain.setLabelTasks(model.getLabel_tasks());
        if(model.getTask_idsDirtyFlag())
            domain.setTaskIds(model.getTask_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getDate_startDirtyFlag())
            domain.setDateStart(model.getDate_start());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getAlias_parent_thread_idDirtyFlag())
            domain.setAliasParentThreadId(model.getAlias_parent_thread_id());
        if(model.getAlias_force_thread_idDirtyFlag())
            domain.setAliasForceThreadId(model.getAlias_force_thread_id());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getAlias_domainDirtyFlag())
            domain.setAliasDomain(model.getAlias_domain());
        if(model.getAlias_contactDirtyFlag())
            domain.setAliasContact(model.getAlias_contact());
        if(model.getAlias_parent_model_idDirtyFlag())
            domain.setAliasParentModelId(model.getAlias_parent_model_id());
        if(model.getAlias_model_idDirtyFlag())
            domain.setAliasModelId(model.getAlias_model_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getAlias_user_idDirtyFlag())
            domain.setAliasUserId(model.getAlias_user_id());
        if(model.getAlias_defaultsDirtyFlag())
            domain.setAliasDefaults(model.getAlias_defaults());
        if(model.getResource_calendar_id_textDirtyFlag())
            domain.setResourceCalendarIdText(model.getResource_calendar_id_text());
        if(model.getSubtask_project_id_textDirtyFlag())
            domain.setSubtaskProjectIdText(model.getSubtask_project_id_text());
        if(model.getAlias_nameDirtyFlag())
            domain.setAliasName(model.getAlias_name());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getSubtask_project_idDirtyFlag())
            domain.setSubtaskProjectId(model.getSubtask_project_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getResource_calendar_idDirtyFlag())
            domain.setResourceCalendarId(model.getResource_calendar_id());
        if(model.getAlias_idDirtyFlag())
            domain.setAliasId(model.getAlias_id());
        return domain ;
    }

}

    



