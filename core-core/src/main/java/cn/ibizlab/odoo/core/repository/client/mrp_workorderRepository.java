package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_workorder;

/**
 * 实体[mrp_workorder] 服务对象接口
 */
public interface mrp_workorderRepository{


    public mrp_workorder createPO() ;
        public void updateBatch(mrp_workorder mrp_workorder);

        public List<mrp_workorder> search();

        public void removeBatch(String id);

        public void create(mrp_workorder mrp_workorder);

        public void createBatch(mrp_workorder mrp_workorder);

        public void remove(String id);

        public void update(mrp_workorder mrp_workorder);

        public void get(String id);


}
