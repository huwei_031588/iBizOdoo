package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;

/**
 * 实体 [付款获得onboarding向导] 存储模型
 */
public interface Payment_acquirer_onboarding_wizard{

    /**
     * Paypal 销售商 ID
     */
    String getPaypal_seller_account();

    void setPaypal_seller_account(String paypal_seller_account);

    /**
     * 获取 [Paypal 销售商 ID]脏标记
     */
    boolean getPaypal_seller_accountDirtyFlag();

    /**
     * Paypal 付款数据传输标记
     */
    String getPaypal_pdt_token();

    void setPaypal_pdt_token(String paypal_pdt_token);

    /**
     * 获取 [Paypal 付款数据传输标记]脏标记
     */
    boolean getPaypal_pdt_tokenDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 付款方法
     */
    String getPayment_method();

    void setPayment_method(String payment_method);

    /**
     * 获取 [付款方法]脏标记
     */
    boolean getPayment_methodDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * Stripe 公钥
     */
    String getStripe_publishable_key();

    void setStripe_publishable_key(String stripe_publishable_key);

    /**
     * 获取 [Stripe 公钥]脏标记
     */
    boolean getStripe_publishable_keyDirtyFlag();

    /**
     * 方法
     */
    String getManual_name();

    void setManual_name(String manual_name);

    /**
     * 获取 [方法]脏标记
     */
    boolean getManual_nameDirtyFlag();

    /**
     * 银行名称
     */
    String getJournal_name();

    void setJournal_name(String journal_name);

    /**
     * 获取 [银行名称]脏标记
     */
    boolean getJournal_nameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 账户号码
     */
    String getAcc_number();

    void setAcc_number(String acc_number);

    /**
     * 获取 [账户号码]脏标记
     */
    boolean getAcc_numberDirtyFlag();

    /**
     * Stripe 密钥
     */
    String getStripe_secret_key();

    void setStripe_secret_key(String stripe_secret_key);

    /**
     * 获取 [Stripe 密钥]脏标记
     */
    boolean getStripe_secret_keyDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 支付说明
     */
    String getManual_post_msg();

    void setManual_post_msg(String manual_post_msg);

    /**
     * 获取 [支付说明]脏标记
     */
    boolean getManual_post_msgDirtyFlag();

    /**
     * PayPal EMailID
     */
    String getPaypal_email_account();

    void setPaypal_email_account(String paypal_email_account);

    /**
     * 获取 [PayPal EMailID]脏标记
     */
    boolean getPaypal_email_accountDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
