package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_templateSearchContext;

/**
 * 实体 [科目模板] 存储模型
 */
public interface Account_account_template{

    /**
     * 代码
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [代码]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 科目标签
     */
    String getTag_ids();

    void setTag_ids(String tag_ids);

    /**
     * 获取 [科目标签]脏标记
     */
    boolean getTag_idsDirtyFlag();

    /**
     * 选项创建
     */
    String getNocreate();

    void setNocreate(String nocreate);

    /**
     * 获取 [选项创建]脏标记
     */
    boolean getNocreateDirtyFlag();

    /**
     * 默认税
     */
    String getTax_ids();

    void setTax_ids(String tax_ids);

    /**
     * 获取 [默认税]脏标记
     */
    boolean getTax_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 备注
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [备注]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 允许发票和付款匹配
     */
    String getReconcile();

    void setReconcile(String reconcile);

    /**
     * 获取 [允许发票和付款匹配]脏标记
     */
    boolean getReconcileDirtyFlag();

    /**
     * 表模板
     */
    String getChart_template_id_text();

    void setChart_template_id_text(String chart_template_id_text);

    /**
     * 获取 [表模板]脏标记
     */
    boolean getChart_template_id_textDirtyFlag();

    /**
     * 科目币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [科目币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 类型
     */
    String getUser_type_id_text();

    void setUser_type_id_text(String user_type_id_text);

    /**
     * 获取 [类型]脏标记
     */
    boolean getUser_type_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 组
     */
    String getGroup_id_text();

    void setGroup_id_text(String group_id_text);

    /**
     * 获取 [组]脏标记
     */
    boolean getGroup_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 表模板
     */
    Integer getChart_template_id();

    void setChart_template_id(Integer chart_template_id);

    /**
     * 获取 [表模板]脏标记
     */
    boolean getChart_template_idDirtyFlag();

    /**
     * 科目币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [科目币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 类型
     */
    Integer getUser_type_id();

    void setUser_type_id(Integer user_type_id);

    /**
     * 获取 [类型]脏标记
     */
    boolean getUser_type_idDirtyFlag();

    /**
     * 组
     */
    Integer getGroup_id();

    void setGroup_id(Integer group_id);

    /**
     * 获取 [组]脏标记
     */
    boolean getGroup_idDirtyFlag();

}
