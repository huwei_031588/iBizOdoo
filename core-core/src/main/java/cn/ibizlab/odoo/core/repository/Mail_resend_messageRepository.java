package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_resend_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_messageSearchContext;

/**
 * 实体 [EMail重发向导] 存储对象
 */
public interface Mail_resend_messageRepository extends Repository<Mail_resend_message> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_resend_message> searchDefault(Mail_resend_messageSearchContext context);

    Mail_resend_message convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_message domain , Mail_resend_message po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_message convert2Domain( Mail_resend_message po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_message domain) ;

}
