package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mail_statistics_report;

/**
 * 实体[mail_statistics_report] 服务对象接口
 */
public interface mail_statistics_reportRepository{


    public mail_statistics_report createPO() ;
        public void get(String id);

        public void remove(String id);

        public List<mail_statistics_report> search();

        public void updateBatch(mail_statistics_report mail_statistics_report);

        public void removeBatch(String id);

        public void create(mail_statistics_report mail_statistics_report);

        public void createBatch(mail_statistics_report mail_statistics_report);

        public void update(mail_statistics_report mail_statistics_report);


}
