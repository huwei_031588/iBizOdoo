package cn.ibizlab.odoo.core.odoo_base.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[res_bank] 对象
 */
public class res_bankClientModel implements Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 银行识别代码
     */
    public String bic;

    @JsonIgnore
    public boolean bicDirtyFlag;
    
    /**
     * 城市
     */
    public String city;

    @JsonIgnore
    public boolean cityDirtyFlag;
    
    /**
     * 国家/地区
     */
    public Integer country;

    @JsonIgnore
    public boolean countryDirtyFlag;
    
    /**
     * 国家/地区
     */
    public String country_text;

    @JsonIgnore
    public boolean country_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * EMail
     */
    public String email;

    @JsonIgnore
    public boolean emailDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 电话
     */
    public String phone;

    @JsonIgnore
    public boolean phoneDirtyFlag;
    
    /**
     * 状态
     */
    public Integer state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 状态
     */
    public String state_text;

    @JsonIgnore
    public boolean state_textDirtyFlag;
    
    /**
     * 街道
     */
    public String street;

    @JsonIgnore
    public boolean streetDirtyFlag;
    
    /**
     * 街道 2
     */
    public String street2;

    @JsonIgnore
    public boolean street2DirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 邮政编码
     */
    public String zip;

    @JsonIgnore
    public boolean zipDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [银行识别代码]
     */
    @JsonProperty("bic")
    public String getBic(){
        return this.bic ;
    }

    /**
     * 设置 [银行识别代码]
     */
    @JsonProperty("bic")
    public void setBic(String  bic){
        this.bic = bic ;
        this.bicDirtyFlag = true ;
    }

     /**
     * 获取 [银行识别代码]脏标记
     */
    @JsonIgnore
    public boolean getBicDirtyFlag(){
        return this.bicDirtyFlag ;
    }   

    /**
     * 获取 [城市]
     */
    @JsonProperty("city")
    public String getCity(){
        return this.city ;
    }

    /**
     * 设置 [城市]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

     /**
     * 获取 [城市]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return this.cityDirtyFlag ;
    }   

    /**
     * 获取 [国家/地区]
     */
    @JsonProperty("country")
    public Integer getCountry(){
        return this.country ;
    }

    /**
     * 设置 [国家/地区]
     */
    @JsonProperty("country")
    public void setCountry(Integer  country){
        this.country = country ;
        this.countryDirtyFlag = true ;
    }

     /**
     * 获取 [国家/地区]脏标记
     */
    @JsonIgnore
    public boolean getCountryDirtyFlag(){
        return this.countryDirtyFlag ;
    }   

    /**
     * 获取 [国家/地区]
     */
    @JsonProperty("country_text")
    public String getCountry_text(){
        return this.country_text ;
    }

    /**
     * 设置 [国家/地区]
     */
    @JsonProperty("country_text")
    public void setCountry_text(String  country_text){
        this.country_text = country_text ;
        this.country_textDirtyFlag = true ;
    }

     /**
     * 获取 [国家/地区]脏标记
     */
    @JsonIgnore
    public boolean getCountry_textDirtyFlag(){
        return this.country_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [EMail]
     */
    @JsonProperty("email")
    public String getEmail(){
        return this.email ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

     /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return this.emailDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [电话]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return this.phone ;
    }

    /**
     * 设置 [电话]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

     /**
     * 获取 [电话]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return this.phoneDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public Integer getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(Integer  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state_text")
    public String getState_text(){
        return this.state_text ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state_text")
    public void setState_text(String  state_text){
        this.state_text = state_text ;
        this.state_textDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getState_textDirtyFlag(){
        return this.state_textDirtyFlag ;
    }   

    /**
     * 获取 [街道]
     */
    @JsonProperty("street")
    public String getStreet(){
        return this.street ;
    }

    /**
     * 设置 [街道]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

     /**
     * 获取 [街道]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return this.streetDirtyFlag ;
    }   

    /**
     * 获取 [街道 2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return this.street2 ;
    }

    /**
     * 设置 [街道 2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

     /**
     * 获取 [街道 2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return this.street2DirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [邮政编码]
     */
    @JsonProperty("zip")
    public String getZip(){
        return this.zip ;
    }

    /**
     * 设置 [邮政编码]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

     /**
     * 获取 [邮政编码]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return this.zipDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("bic") instanceof Boolean)&& map.get("bic")!=null){
			this.setBic((String)map.get("bic"));
		}
		if(!(map.get("city") instanceof Boolean)&& map.get("city")!=null){
			this.setCity((String)map.get("city"));
		}
		if(!(map.get("country") instanceof Boolean)&& map.get("country")!=null){
			Object[] objs = (Object[])map.get("country");
			if(objs.length > 0){
				this.setCountry((Integer)objs[0]);
			}
		}
		if(!(map.get("country") instanceof Boolean)&& map.get("country")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("country");
			if(objs.length > 1){
				this.setCountry_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("email") instanceof Boolean)&& map.get("email")!=null){
			this.setEmail((String)map.get("email"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("phone") instanceof Boolean)&& map.get("phone")!=null){
			this.setPhone((String)map.get("phone"));
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			Object[] objs = (Object[])map.get("state");
			if(objs.length > 0){
				this.setState((Integer)objs[0]);
			}
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("state");
			if(objs.length > 1){
				this.setState_text((String)objs[1]);
			}
		}
		if(!(map.get("street") instanceof Boolean)&& map.get("street")!=null){
			this.setStreet((String)map.get("street"));
		}
		if(!(map.get("street2") instanceof Boolean)&& map.get("street2")!=null){
			this.setStreet2((String)map.get("street2"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("zip") instanceof Boolean)&& map.get("zip")!=null){
			this.setZip((String)map.get("zip"));
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getBic()!=null&&this.getBicDirtyFlag()){
			map.put("bic",this.getBic());
		}else if(this.getBicDirtyFlag()){
			map.put("bic",false);
		}
		if(this.getCity()!=null&&this.getCityDirtyFlag()){
			map.put("city",this.getCity());
		}else if(this.getCityDirtyFlag()){
			map.put("city",false);
		}
		if(this.getCountry()!=null&&this.getCountryDirtyFlag()){
			map.put("country",this.getCountry());
		}else if(this.getCountryDirtyFlag()){
			map.put("country",false);
		}
		if(this.getCountry_text()!=null&&this.getCountry_textDirtyFlag()){
			//忽略文本外键country_text
		}else if(this.getCountry_textDirtyFlag()){
			map.put("country",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmail()!=null&&this.getEmailDirtyFlag()){
			map.put("email",this.getEmail());
		}else if(this.getEmailDirtyFlag()){
			map.put("email",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPhone()!=null&&this.getPhoneDirtyFlag()){
			map.put("phone",this.getPhone());
		}else if(this.getPhoneDirtyFlag()){
			map.put("phone",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getState_text()!=null&&this.getState_textDirtyFlag()){
			//忽略文本外键state_text
		}else if(this.getState_textDirtyFlag()){
			map.put("state",false);
		}
		if(this.getStreet()!=null&&this.getStreetDirtyFlag()){
			map.put("street",this.getStreet());
		}else if(this.getStreetDirtyFlag()){
			map.put("street",false);
		}
		if(this.getStreet2()!=null&&this.getStreet2DirtyFlag()){
			map.put("street2",this.getStreet2());
		}else if(this.getStreet2DirtyFlag()){
			map.put("street2",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getZip()!=null&&this.getZipDirtyFlag()){
			map.put("zip",this.getZip());
		}else if(this.getZipDirtyFlag()){
			map.put("zip",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
