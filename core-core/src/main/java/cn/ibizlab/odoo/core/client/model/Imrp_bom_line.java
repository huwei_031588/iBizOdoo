package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mrp_bom_line] 对象
 */
public interface Imrp_bom_line {

    /**
     * 获取 [应用于变体]
     */
    public void setAttribute_value_ids(String attribute_value_ids);
    
    /**
     * 设置 [应用于变体]
     */
    public String getAttribute_value_ids();

    /**
     * 获取 [应用于变体]脏标记
     */
    public boolean getAttribute_value_idsDirtyFlag();
    /**
     * 获取 [父级 BoM]
     */
    public void setBom_id(Integer bom_id);
    
    /**
     * 设置 [父级 BoM]
     */
    public Integer getBom_id();

    /**
     * 获取 [父级 BoM]脏标记
     */
    public boolean getBom_idDirtyFlag();
    /**
     * 获取 [子 BOM]
     */
    public void setChild_bom_id(Integer child_bom_id);
    
    /**
     * 设置 [子 BOM]
     */
    public Integer getChild_bom_id();

    /**
     * 获取 [子 BOM]脏标记
     */
    public boolean getChild_bom_idDirtyFlag();
    /**
     * 获取 [参考BOM中的BOM行]
     */
    public void setChild_line_ids(String child_line_ids);
    
    /**
     * 设置 [参考BOM中的BOM行]
     */
    public String getChild_line_ids();

    /**
     * 获取 [参考BOM中的BOM行]脏标记
     */
    public boolean getChild_line_idsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [有附件]
     */
    public void setHas_attachments(String has_attachments);
    
    /**
     * 设置 [有附件]
     */
    public String getHas_attachments();

    /**
     * 获取 [有附件]脏标记
     */
    public boolean getHas_attachmentsDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [投料作业]
     */
    public void setOperation_id(Integer operation_id);
    
    /**
     * 设置 [投料作业]
     */
    public Integer getOperation_id();

    /**
     * 获取 [投料作业]脏标记
     */
    public boolean getOperation_idDirtyFlag();
    /**
     * 获取 [投料作业]
     */
    public void setOperation_id_text(String operation_id_text);
    
    /**
     * 设置 [投料作业]
     */
    public String getOperation_id_text();

    /**
     * 获取 [投料作业]脏标记
     */
    public boolean getOperation_id_textDirtyFlag();
    /**
     * 获取 [父产品模板]
     */
    public void setParent_product_tmpl_id(Integer parent_product_tmpl_id);
    
    /**
     * 设置 [父产品模板]
     */
    public Integer getParent_product_tmpl_id();

    /**
     * 获取 [父产品模板]脏标记
     */
    public boolean getParent_product_tmpl_idDirtyFlag();
    /**
     * 获取 [零件]
     */
    public void setProduct_id(Integer product_id);
    
    /**
     * 设置 [零件]
     */
    public Integer getProduct_id();

    /**
     * 获取 [零件]脏标记
     */
    public boolean getProduct_idDirtyFlag();
    /**
     * 获取 [零件]
     */
    public void setProduct_id_text(String product_id_text);
    
    /**
     * 设置 [零件]
     */
    public String getProduct_id_text();

    /**
     * 获取 [零件]脏标记
     */
    public boolean getProduct_id_textDirtyFlag();
    /**
     * 获取 [数量]
     */
    public void setProduct_qty(Double product_qty);
    
    /**
     * 设置 [数量]
     */
    public Double getProduct_qty();

    /**
     * 获取 [数量]脏标记
     */
    public boolean getProduct_qtyDirtyFlag();
    /**
     * 获取 [产品模板]
     */
    public void setProduct_tmpl_id(Integer product_tmpl_id);
    
    /**
     * 设置 [产品模板]
     */
    public Integer getProduct_tmpl_id();

    /**
     * 获取 [产品模板]脏标记
     */
    public boolean getProduct_tmpl_idDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_id(Integer product_uom_id);
    
    /**
     * 设置 [计量单位]
     */
    public Integer getProduct_uom_id();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_idDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setProduct_uom_id_text(String product_uom_id_text);
    
    /**
     * 设置 [计量单位]
     */
    public String getProduct_uom_id_text();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getProduct_uom_id_textDirtyFlag();
    /**
     * 获取 [工艺]
     */
    public void setRouting_id(Integer routing_id);
    
    /**
     * 设置 [工艺]
     */
    public Integer getRouting_id();

    /**
     * 获取 [工艺]脏标记
     */
    public boolean getRouting_idDirtyFlag();
    /**
     * 获取 [工艺]
     */
    public void setRouting_id_text(String routing_id_text);
    
    /**
     * 设置 [工艺]
     */
    public String getRouting_id_text();

    /**
     * 获取 [工艺]脏标记
     */
    public boolean getRouting_id_textDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [有效的产品属性值]
     */
    public void setValid_product_attribute_value_ids(String valid_product_attribute_value_ids);
    
    /**
     * 设置 [有效的产品属性值]
     */
    public String getValid_product_attribute_value_ids();

    /**
     * 获取 [有效的产品属性值]脏标记
     */
    public boolean getValid_product_attribute_value_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
