package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_removal;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_removalSearchContext;


/**
 * 实体[Product_removal] 服务对象接口
 */
public interface IProduct_removalService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Product_removal et) ;
    void updateBatch(List<Product_removal> list) ;
    boolean create(Product_removal et) ;
    void createBatch(List<Product_removal> list) ;
    Product_removal get(Integer key) ;
    Page<Product_removal> searchDefault(Product_removalSearchContext context) ;

}



