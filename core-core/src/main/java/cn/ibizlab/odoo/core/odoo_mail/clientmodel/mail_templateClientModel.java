package cn.ibizlab.odoo.core.odoo_mail.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_template] 对象
 */
public class mail_templateClientModel implements Serializable{

    /**
     * 附件
     */
    public String attachment_ids;

    @JsonIgnore
    public boolean attachment_idsDirtyFlag;
    
    /**
     * 自动删除
     */
    public String auto_delete;

    @JsonIgnore
    public boolean auto_deleteDirtyFlag;
    
    /**
     * 正文
     */
    public String body_html;

    @JsonIgnore
    public boolean body_htmlDirtyFlag;
    
    /**
     * 占位符表达式
     */
    public String copyvalue;

    @JsonIgnore
    public boolean copyvalueDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 抄送
     */
    public String email_cc;

    @JsonIgnore
    public boolean email_ccDirtyFlag;
    
    /**
     * 从
     */
    public String email_from;

    @JsonIgnore
    public boolean email_fromDirtyFlag;
    
    /**
     * 至（EMail）
     */
    public String email_to;

    @JsonIgnore
    public boolean email_toDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 语言
     */
    public String lang;

    @JsonIgnore
    public boolean langDirtyFlag;
    
    /**
     * 邮件发送服务器
     */
    public Integer mail_server_id;

    @JsonIgnore
    public boolean mail_server_idDirtyFlag;
    
    /**
     * 相关的文档模型
     */
    public String model;

    @JsonIgnore
    public boolean modelDirtyFlag;
    
    /**
     * 应用于
     */
    public Integer model_id;

    @JsonIgnore
    public boolean model_idDirtyFlag;
    
    /**
     * 字段
     */
    public Integer model_object_field;

    @JsonIgnore
    public boolean model_object_fieldDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 默认值
     */
    public String null_value;

    @JsonIgnore
    public boolean null_valueDirtyFlag;
    
    /**
     * 至（合作伙伴）
     */
    public String partner_to;

    @JsonIgnore
    public boolean partner_toDirtyFlag;
    
    /**
     * 边栏操作
     */
    public Integer ref_ir_act_window;

    @JsonIgnore
    public boolean ref_ir_act_windowDirtyFlag;
    
    /**
     * 回复 至
     */
    public String reply_to;

    @JsonIgnore
    public boolean reply_toDirtyFlag;
    
    /**
     * 报告文件名
     */
    public String report_name;

    @JsonIgnore
    public boolean report_nameDirtyFlag;
    
    /**
     * 可选的打印和附加报表
     */
    public Integer report_template;

    @JsonIgnore
    public boolean report_templateDirtyFlag;
    
    /**
     * 计划日期
     */
    public String scheduled_date;

    @JsonIgnore
    public boolean scheduled_dateDirtyFlag;
    
    /**
     * 主题
     */
    public String subject;

    @JsonIgnore
    public boolean subjectDirtyFlag;
    
    /**
     * 子字段
     */
    public Integer sub_model_object_field;

    @JsonIgnore
    public boolean sub_model_object_fieldDirtyFlag;
    
    /**
     * 子模型
     */
    public Integer sub_object;

    @JsonIgnore
    public boolean sub_objectDirtyFlag;
    
    /**
     * 添加签名
     */
    public String user_signature;

    @JsonIgnore
    public boolean user_signatureDirtyFlag;
    
    /**
     * 默认收件人
     */
    public String use_default_to;

    @JsonIgnore
    public boolean use_default_toDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [附件]
     */
    @JsonProperty("attachment_ids")
    public String getAttachment_ids(){
        return this.attachment_ids ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("attachment_ids")
    public void setAttachment_ids(String  attachment_ids){
        this.attachment_ids = attachment_ids ;
        this.attachment_idsDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_idsDirtyFlag(){
        return this.attachment_idsDirtyFlag ;
    }   

    /**
     * 获取 [自动删除]
     */
    @JsonProperty("auto_delete")
    public String getAuto_delete(){
        return this.auto_delete ;
    }

    /**
     * 设置 [自动删除]
     */
    @JsonProperty("auto_delete")
    public void setAuto_delete(String  auto_delete){
        this.auto_delete = auto_delete ;
        this.auto_deleteDirtyFlag = true ;
    }

     /**
     * 获取 [自动删除]脏标记
     */
    @JsonIgnore
    public boolean getAuto_deleteDirtyFlag(){
        return this.auto_deleteDirtyFlag ;
    }   

    /**
     * 获取 [正文]
     */
    @JsonProperty("body_html")
    public String getBody_html(){
        return this.body_html ;
    }

    /**
     * 设置 [正文]
     */
    @JsonProperty("body_html")
    public void setBody_html(String  body_html){
        this.body_html = body_html ;
        this.body_htmlDirtyFlag = true ;
    }

     /**
     * 获取 [正文]脏标记
     */
    @JsonIgnore
    public boolean getBody_htmlDirtyFlag(){
        return this.body_htmlDirtyFlag ;
    }   

    /**
     * 获取 [占位符表达式]
     */
    @JsonProperty("copyvalue")
    public String getCopyvalue(){
        return this.copyvalue ;
    }

    /**
     * 设置 [占位符表达式]
     */
    @JsonProperty("copyvalue")
    public void setCopyvalue(String  copyvalue){
        this.copyvalue = copyvalue ;
        this.copyvalueDirtyFlag = true ;
    }

     /**
     * 获取 [占位符表达式]脏标记
     */
    @JsonIgnore
    public boolean getCopyvalueDirtyFlag(){
        return this.copyvalueDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [抄送]
     */
    @JsonProperty("email_cc")
    public String getEmail_cc(){
        return this.email_cc ;
    }

    /**
     * 设置 [抄送]
     */
    @JsonProperty("email_cc")
    public void setEmail_cc(String  email_cc){
        this.email_cc = email_cc ;
        this.email_ccDirtyFlag = true ;
    }

     /**
     * 获取 [抄送]脏标记
     */
    @JsonIgnore
    public boolean getEmail_ccDirtyFlag(){
        return this.email_ccDirtyFlag ;
    }   

    /**
     * 获取 [从]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return this.email_from ;
    }

    /**
     * 设置 [从]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

     /**
     * 获取 [从]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return this.email_fromDirtyFlag ;
    }   

    /**
     * 获取 [至（EMail）]
     */
    @JsonProperty("email_to")
    public String getEmail_to(){
        return this.email_to ;
    }

    /**
     * 设置 [至（EMail）]
     */
    @JsonProperty("email_to")
    public void setEmail_to(String  email_to){
        this.email_to = email_to ;
        this.email_toDirtyFlag = true ;
    }

     /**
     * 获取 [至（EMail）]脏标记
     */
    @JsonIgnore
    public boolean getEmail_toDirtyFlag(){
        return this.email_toDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [语言]
     */
    @JsonProperty("lang")
    public String getLang(){
        return this.lang ;
    }

    /**
     * 设置 [语言]
     */
    @JsonProperty("lang")
    public void setLang(String  lang){
        this.lang = lang ;
        this.langDirtyFlag = true ;
    }

     /**
     * 获取 [语言]脏标记
     */
    @JsonIgnore
    public boolean getLangDirtyFlag(){
        return this.langDirtyFlag ;
    }   

    /**
     * 获取 [邮件发送服务器]
     */
    @JsonProperty("mail_server_id")
    public Integer getMail_server_id(){
        return this.mail_server_id ;
    }

    /**
     * 设置 [邮件发送服务器]
     */
    @JsonProperty("mail_server_id")
    public void setMail_server_id(Integer  mail_server_id){
        this.mail_server_id = mail_server_id ;
        this.mail_server_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件发送服务器]脏标记
     */
    @JsonIgnore
    public boolean getMail_server_idDirtyFlag(){
        return this.mail_server_idDirtyFlag ;
    }   

    /**
     * 获取 [相关的文档模型]
     */
    @JsonProperty("model")
    public String getModel(){
        return this.model ;
    }

    /**
     * 设置 [相关的文档模型]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

     /**
     * 获取 [相关的文档模型]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return this.modelDirtyFlag ;
    }   

    /**
     * 获取 [应用于]
     */
    @JsonProperty("model_id")
    public Integer getModel_id(){
        return this.model_id ;
    }

    /**
     * 设置 [应用于]
     */
    @JsonProperty("model_id")
    public void setModel_id(Integer  model_id){
        this.model_id = model_id ;
        this.model_idDirtyFlag = true ;
    }

     /**
     * 获取 [应用于]脏标记
     */
    @JsonIgnore
    public boolean getModel_idDirtyFlag(){
        return this.model_idDirtyFlag ;
    }   

    /**
     * 获取 [字段]
     */
    @JsonProperty("model_object_field")
    public Integer getModel_object_field(){
        return this.model_object_field ;
    }

    /**
     * 设置 [字段]
     */
    @JsonProperty("model_object_field")
    public void setModel_object_field(Integer  model_object_field){
        this.model_object_field = model_object_field ;
        this.model_object_fieldDirtyFlag = true ;
    }

     /**
     * 获取 [字段]脏标记
     */
    @JsonIgnore
    public boolean getModel_object_fieldDirtyFlag(){
        return this.model_object_fieldDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [默认值]
     */
    @JsonProperty("null_value")
    public String getNull_value(){
        return this.null_value ;
    }

    /**
     * 设置 [默认值]
     */
    @JsonProperty("null_value")
    public void setNull_value(String  null_value){
        this.null_value = null_value ;
        this.null_valueDirtyFlag = true ;
    }

     /**
     * 获取 [默认值]脏标记
     */
    @JsonIgnore
    public boolean getNull_valueDirtyFlag(){
        return this.null_valueDirtyFlag ;
    }   

    /**
     * 获取 [至（合作伙伴）]
     */
    @JsonProperty("partner_to")
    public String getPartner_to(){
        return this.partner_to ;
    }

    /**
     * 设置 [至（合作伙伴）]
     */
    @JsonProperty("partner_to")
    public void setPartner_to(String  partner_to){
        this.partner_to = partner_to ;
        this.partner_toDirtyFlag = true ;
    }

     /**
     * 获取 [至（合作伙伴）]脏标记
     */
    @JsonIgnore
    public boolean getPartner_toDirtyFlag(){
        return this.partner_toDirtyFlag ;
    }   

    /**
     * 获取 [边栏操作]
     */
    @JsonProperty("ref_ir_act_window")
    public Integer getRef_ir_act_window(){
        return this.ref_ir_act_window ;
    }

    /**
     * 设置 [边栏操作]
     */
    @JsonProperty("ref_ir_act_window")
    public void setRef_ir_act_window(Integer  ref_ir_act_window){
        this.ref_ir_act_window = ref_ir_act_window ;
        this.ref_ir_act_windowDirtyFlag = true ;
    }

     /**
     * 获取 [边栏操作]脏标记
     */
    @JsonIgnore
    public boolean getRef_ir_act_windowDirtyFlag(){
        return this.ref_ir_act_windowDirtyFlag ;
    }   

    /**
     * 获取 [回复 至]
     */
    @JsonProperty("reply_to")
    public String getReply_to(){
        return this.reply_to ;
    }

    /**
     * 设置 [回复 至]
     */
    @JsonProperty("reply_to")
    public void setReply_to(String  reply_to){
        this.reply_to = reply_to ;
        this.reply_toDirtyFlag = true ;
    }

     /**
     * 获取 [回复 至]脏标记
     */
    @JsonIgnore
    public boolean getReply_toDirtyFlag(){
        return this.reply_toDirtyFlag ;
    }   

    /**
     * 获取 [报告文件名]
     */
    @JsonProperty("report_name")
    public String getReport_name(){
        return this.report_name ;
    }

    /**
     * 设置 [报告文件名]
     */
    @JsonProperty("report_name")
    public void setReport_name(String  report_name){
        this.report_name = report_name ;
        this.report_nameDirtyFlag = true ;
    }

     /**
     * 获取 [报告文件名]脏标记
     */
    @JsonIgnore
    public boolean getReport_nameDirtyFlag(){
        return this.report_nameDirtyFlag ;
    }   

    /**
     * 获取 [可选的打印和附加报表]
     */
    @JsonProperty("report_template")
    public Integer getReport_template(){
        return this.report_template ;
    }

    /**
     * 设置 [可选的打印和附加报表]
     */
    @JsonProperty("report_template")
    public void setReport_template(Integer  report_template){
        this.report_template = report_template ;
        this.report_templateDirtyFlag = true ;
    }

     /**
     * 获取 [可选的打印和附加报表]脏标记
     */
    @JsonIgnore
    public boolean getReport_templateDirtyFlag(){
        return this.report_templateDirtyFlag ;
    }   

    /**
     * 获取 [计划日期]
     */
    @JsonProperty("scheduled_date")
    public String getScheduled_date(){
        return this.scheduled_date ;
    }

    /**
     * 设置 [计划日期]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(String  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

     /**
     * 获取 [计划日期]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return this.scheduled_dateDirtyFlag ;
    }   

    /**
     * 获取 [主题]
     */
    @JsonProperty("subject")
    public String getSubject(){
        return this.subject ;
    }

    /**
     * 设置 [主题]
     */
    @JsonProperty("subject")
    public void setSubject(String  subject){
        this.subject = subject ;
        this.subjectDirtyFlag = true ;
    }

     /**
     * 获取 [主题]脏标记
     */
    @JsonIgnore
    public boolean getSubjectDirtyFlag(){
        return this.subjectDirtyFlag ;
    }   

    /**
     * 获取 [子字段]
     */
    @JsonProperty("sub_model_object_field")
    public Integer getSub_model_object_field(){
        return this.sub_model_object_field ;
    }

    /**
     * 设置 [子字段]
     */
    @JsonProperty("sub_model_object_field")
    public void setSub_model_object_field(Integer  sub_model_object_field){
        this.sub_model_object_field = sub_model_object_field ;
        this.sub_model_object_fieldDirtyFlag = true ;
    }

     /**
     * 获取 [子字段]脏标记
     */
    @JsonIgnore
    public boolean getSub_model_object_fieldDirtyFlag(){
        return this.sub_model_object_fieldDirtyFlag ;
    }   

    /**
     * 获取 [子模型]
     */
    @JsonProperty("sub_object")
    public Integer getSub_object(){
        return this.sub_object ;
    }

    /**
     * 设置 [子模型]
     */
    @JsonProperty("sub_object")
    public void setSub_object(Integer  sub_object){
        this.sub_object = sub_object ;
        this.sub_objectDirtyFlag = true ;
    }

     /**
     * 获取 [子模型]脏标记
     */
    @JsonIgnore
    public boolean getSub_objectDirtyFlag(){
        return this.sub_objectDirtyFlag ;
    }   

    /**
     * 获取 [添加签名]
     */
    @JsonProperty("user_signature")
    public String getUser_signature(){
        return this.user_signature ;
    }

    /**
     * 设置 [添加签名]
     */
    @JsonProperty("user_signature")
    public void setUser_signature(String  user_signature){
        this.user_signature = user_signature ;
        this.user_signatureDirtyFlag = true ;
    }

     /**
     * 获取 [添加签名]脏标记
     */
    @JsonIgnore
    public boolean getUser_signatureDirtyFlag(){
        return this.user_signatureDirtyFlag ;
    }   

    /**
     * 获取 [默认收件人]
     */
    @JsonProperty("use_default_to")
    public String getUse_default_to(){
        return this.use_default_to ;
    }

    /**
     * 设置 [默认收件人]
     */
    @JsonProperty("use_default_to")
    public void setUse_default_to(String  use_default_to){
        this.use_default_to = use_default_to ;
        this.use_default_toDirtyFlag = true ;
    }

     /**
     * 获取 [默认收件人]脏标记
     */
    @JsonIgnore
    public boolean getUse_default_toDirtyFlag(){
        return this.use_default_toDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("attachment_ids") instanceof Boolean)&& map.get("attachment_ids")!=null){
			Object[] objs = (Object[])map.get("attachment_ids");
			if(objs.length > 0){
				Integer[] attachment_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAttachment_ids(Arrays.toString(attachment_ids).replace(" ",""));
			}
		}
		if(map.get("auto_delete") instanceof Boolean){
			this.setAuto_delete(((Boolean)map.get("auto_delete"))? "true" : "false");
		}
		if(!(map.get("body_html") instanceof Boolean)&& map.get("body_html")!=null){
			this.setBody_html((String)map.get("body_html"));
		}
		if(!(map.get("copyvalue") instanceof Boolean)&& map.get("copyvalue")!=null){
			this.setCopyvalue((String)map.get("copyvalue"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("email_cc") instanceof Boolean)&& map.get("email_cc")!=null){
			this.setEmail_cc((String)map.get("email_cc"));
		}
		if(!(map.get("email_from") instanceof Boolean)&& map.get("email_from")!=null){
			this.setEmail_from((String)map.get("email_from"));
		}
		if(!(map.get("email_to") instanceof Boolean)&& map.get("email_to")!=null){
			this.setEmail_to((String)map.get("email_to"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("lang") instanceof Boolean)&& map.get("lang")!=null){
			this.setLang((String)map.get("lang"));
		}
		if(!(map.get("mail_server_id") instanceof Boolean)&& map.get("mail_server_id")!=null){
			Object[] objs = (Object[])map.get("mail_server_id");
			if(objs.length > 0){
				this.setMail_server_id((Integer)objs[0]);
			}
		}
		if(!(map.get("model") instanceof Boolean)&& map.get("model")!=null){
			this.setModel((String)map.get("model"));
		}
		if(!(map.get("model_id") instanceof Boolean)&& map.get("model_id")!=null){
			Object[] objs = (Object[])map.get("model_id");
			if(objs.length > 0){
				this.setModel_id((Integer)objs[0]);
			}
		}
		if(!(map.get("model_object_field") instanceof Boolean)&& map.get("model_object_field")!=null){
			Object[] objs = (Object[])map.get("model_object_field");
			if(objs.length > 0){
				this.setModel_object_field((Integer)objs[0]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("null_value") instanceof Boolean)&& map.get("null_value")!=null){
			this.setNull_value((String)map.get("null_value"));
		}
		if(!(map.get("partner_to") instanceof Boolean)&& map.get("partner_to")!=null){
			this.setPartner_to((String)map.get("partner_to"));
		}
		if(!(map.get("ref_ir_act_window") instanceof Boolean)&& map.get("ref_ir_act_window")!=null){
			Object[] objs = (Object[])map.get("ref_ir_act_window");
			if(objs.length > 0){
				this.setRef_ir_act_window((Integer)objs[0]);
			}
		}
		if(!(map.get("reply_to") instanceof Boolean)&& map.get("reply_to")!=null){
			this.setReply_to((String)map.get("reply_to"));
		}
		if(!(map.get("report_name") instanceof Boolean)&& map.get("report_name")!=null){
			this.setReport_name((String)map.get("report_name"));
		}
		if(!(map.get("report_template") instanceof Boolean)&& map.get("report_template")!=null){
			Object[] objs = (Object[])map.get("report_template");
			if(objs.length > 0){
				this.setReport_template((Integer)objs[0]);
			}
		}
		if(!(map.get("scheduled_date") instanceof Boolean)&& map.get("scheduled_date")!=null){
			this.setScheduled_date((String)map.get("scheduled_date"));
		}
		if(!(map.get("subject") instanceof Boolean)&& map.get("subject")!=null){
			this.setSubject((String)map.get("subject"));
		}
		if(!(map.get("sub_model_object_field") instanceof Boolean)&& map.get("sub_model_object_field")!=null){
			Object[] objs = (Object[])map.get("sub_model_object_field");
			if(objs.length > 0){
				this.setSub_model_object_field((Integer)objs[0]);
			}
		}
		if(!(map.get("sub_object") instanceof Boolean)&& map.get("sub_object")!=null){
			Object[] objs = (Object[])map.get("sub_object");
			if(objs.length > 0){
				this.setSub_object((Integer)objs[0]);
			}
		}
		if(map.get("user_signature") instanceof Boolean){
			this.setUser_signature(((Boolean)map.get("user_signature"))? "true" : "false");
		}
		if(map.get("use_default_to") instanceof Boolean){
			this.setUse_default_to(((Boolean)map.get("use_default_to"))? "true" : "false");
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAttachment_ids()!=null&&this.getAttachment_idsDirtyFlag()){
			map.put("attachment_ids",this.getAttachment_ids());
		}else if(this.getAttachment_idsDirtyFlag()){
			map.put("attachment_ids",false);
		}
		if(this.getAuto_delete()!=null&&this.getAuto_deleteDirtyFlag()){
			map.put("auto_delete",Boolean.parseBoolean(this.getAuto_delete()));		
		}		if(this.getBody_html()!=null&&this.getBody_htmlDirtyFlag()){
			map.put("body_html",this.getBody_html());
		}else if(this.getBody_htmlDirtyFlag()){
			map.put("body_html",false);
		}
		if(this.getCopyvalue()!=null&&this.getCopyvalueDirtyFlag()){
			map.put("copyvalue",this.getCopyvalue());
		}else if(this.getCopyvalueDirtyFlag()){
			map.put("copyvalue",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmail_cc()!=null&&this.getEmail_ccDirtyFlag()){
			map.put("email_cc",this.getEmail_cc());
		}else if(this.getEmail_ccDirtyFlag()){
			map.put("email_cc",false);
		}
		if(this.getEmail_from()!=null&&this.getEmail_fromDirtyFlag()){
			map.put("email_from",this.getEmail_from());
		}else if(this.getEmail_fromDirtyFlag()){
			map.put("email_from",false);
		}
		if(this.getEmail_to()!=null&&this.getEmail_toDirtyFlag()){
			map.put("email_to",this.getEmail_to());
		}else if(this.getEmail_toDirtyFlag()){
			map.put("email_to",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLang()!=null&&this.getLangDirtyFlag()){
			map.put("lang",this.getLang());
		}else if(this.getLangDirtyFlag()){
			map.put("lang",false);
		}
		if(this.getMail_server_id()!=null&&this.getMail_server_idDirtyFlag()){
			map.put("mail_server_id",this.getMail_server_id());
		}else if(this.getMail_server_idDirtyFlag()){
			map.put("mail_server_id",false);
		}
		if(this.getModel()!=null&&this.getModelDirtyFlag()){
			map.put("model",this.getModel());
		}else if(this.getModelDirtyFlag()){
			map.put("model",false);
		}
		if(this.getModel_id()!=null&&this.getModel_idDirtyFlag()){
			map.put("model_id",this.getModel_id());
		}else if(this.getModel_idDirtyFlag()){
			map.put("model_id",false);
		}
		if(this.getModel_object_field()!=null&&this.getModel_object_fieldDirtyFlag()){
			map.put("model_object_field",this.getModel_object_field());
		}else if(this.getModel_object_fieldDirtyFlag()){
			map.put("model_object_field",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNull_value()!=null&&this.getNull_valueDirtyFlag()){
			map.put("null_value",this.getNull_value());
		}else if(this.getNull_valueDirtyFlag()){
			map.put("null_value",false);
		}
		if(this.getPartner_to()!=null&&this.getPartner_toDirtyFlag()){
			map.put("partner_to",this.getPartner_to());
		}else if(this.getPartner_toDirtyFlag()){
			map.put("partner_to",false);
		}
		if(this.getRef_ir_act_window()!=null&&this.getRef_ir_act_windowDirtyFlag()){
			map.put("ref_ir_act_window",this.getRef_ir_act_window());
		}else if(this.getRef_ir_act_windowDirtyFlag()){
			map.put("ref_ir_act_window",false);
		}
		if(this.getReply_to()!=null&&this.getReply_toDirtyFlag()){
			map.put("reply_to",this.getReply_to());
		}else if(this.getReply_toDirtyFlag()){
			map.put("reply_to",false);
		}
		if(this.getReport_name()!=null&&this.getReport_nameDirtyFlag()){
			map.put("report_name",this.getReport_name());
		}else if(this.getReport_nameDirtyFlag()){
			map.put("report_name",false);
		}
		if(this.getReport_template()!=null&&this.getReport_templateDirtyFlag()){
			map.put("report_template",this.getReport_template());
		}else if(this.getReport_templateDirtyFlag()){
			map.put("report_template",false);
		}
		if(this.getScheduled_date()!=null&&this.getScheduled_dateDirtyFlag()){
			map.put("scheduled_date",this.getScheduled_date());
		}else if(this.getScheduled_dateDirtyFlag()){
			map.put("scheduled_date",false);
		}
		if(this.getSubject()!=null&&this.getSubjectDirtyFlag()){
			map.put("subject",this.getSubject());
		}else if(this.getSubjectDirtyFlag()){
			map.put("subject",false);
		}
		if(this.getSub_model_object_field()!=null&&this.getSub_model_object_fieldDirtyFlag()){
			map.put("sub_model_object_field",this.getSub_model_object_field());
		}else if(this.getSub_model_object_fieldDirtyFlag()){
			map.put("sub_model_object_field",false);
		}
		if(this.getSub_object()!=null&&this.getSub_objectDirtyFlag()){
			map.put("sub_object",this.getSub_object());
		}else if(this.getSub_objectDirtyFlag()){
			map.put("sub_object",false);
		}
		if(this.getUser_signature()!=null&&this.getUser_signatureDirtyFlag()){
			map.put("user_signature",Boolean.parseBoolean(this.getUser_signature()));		
		}		if(this.getUse_default_to()!=null&&this.getUse_default_toDirtyFlag()){
			map.put("use_default_to",Boolean.parseBoolean(this.getUse_default_to()));		
		}		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
