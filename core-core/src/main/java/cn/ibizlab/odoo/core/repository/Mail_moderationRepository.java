package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_moderation;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_moderationSearchContext;

/**
 * 实体 [渠道黑/白名单] 存储对象
 */
public interface Mail_moderationRepository extends Repository<Mail_moderation> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_moderation> searchDefault(Mail_moderationSearchContext context);

    Mail_moderation convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_moderation domain , Mail_moderation po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_moderation convert2Domain( Mail_moderation po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_moderation domain) ;

}
