package cn.ibizlab.odoo.core.odoo_utm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_sourceSearchContext;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_sourceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_utm.client.utm_sourceOdooClient;
import cn.ibizlab.odoo.core.odoo_utm.clientmodel.utm_sourceClientModel;

/**
 * 实体[UTM来源] 服务对象接口实现
 */
@Slf4j
@Service
public class Utm_sourceServiceImpl implements IUtm_sourceService {

    @Autowired
    utm_sourceOdooClient utm_sourceOdooClient;


    @Override
    public Utm_source get(Integer id) {
        utm_sourceClientModel clientModel = new utm_sourceClientModel();
        clientModel.setId(id);
		utm_sourceOdooClient.get(clientModel);
        Utm_source et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Utm_source();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Utm_source et) {
        utm_sourceClientModel clientModel = convert2Model(et,null);
		utm_sourceOdooClient.create(clientModel);
        Utm_source rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Utm_source> list){
    }

    @Override
    public boolean remove(Integer id) {
        utm_sourceClientModel clientModel = new utm_sourceClientModel();
        clientModel.setId(id);
		utm_sourceOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Utm_source et) {
        utm_sourceClientModel clientModel = convert2Model(et,null);
		utm_sourceOdooClient.update(clientModel);
        Utm_source rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Utm_source> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Utm_source> searchDefault(Utm_sourceSearchContext context) {
        List<Utm_source> list = new ArrayList<Utm_source>();
        Page<utm_sourceClientModel> clientModelList = utm_sourceOdooClient.search(context);
        for(utm_sourceClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Utm_source>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public utm_sourceClientModel convert2Model(Utm_source domain , utm_sourceClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new utm_sourceClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Utm_source convert2Domain( utm_sourceClientModel model ,Utm_source domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Utm_source();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



