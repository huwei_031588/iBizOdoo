package cn.ibizlab.odoo.core.odoo_stock.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[stock_picking] 对象
 */
public class stock_pickingClientModel implements Serializable{

    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 欠单
     */
    public Integer backorder_id;

    @JsonIgnore
    public boolean backorder_idDirtyFlag;
    
    /**
     * 欠单
     */
    public String backorder_ids;

    @JsonIgnore
    public boolean backorder_idsDirtyFlag;
    
    /**
     * 欠单
     */
    public String backorder_id_text;

    @JsonIgnore
    public boolean backorder_id_textDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 创建日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 调拨日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_done;

    @JsonIgnore
    public boolean date_doneDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 补货组
     */
    public Integer group_id;

    @JsonIgnore
    public boolean group_idDirtyFlag;
    
    /**
     * 有包裹
     */
    public String has_packages;

    @JsonIgnore
    public boolean has_packagesDirtyFlag;
    
    /**
     * 有报废移动
     */
    public String has_scrap_move;

    @JsonIgnore
    public boolean has_scrap_moveDirtyFlag;
    
    /**
     * 有跟踪
     */
    public String has_tracking;

    @JsonIgnore
    public boolean has_trackingDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 立即调拨
     */
    public String immediate_transfer;

    @JsonIgnore
    public boolean immediate_transferDirtyFlag;
    
    /**
     * 是锁定
     */
    public String is_locked;

    @JsonIgnore
    public boolean is_lockedDirtyFlag;
    
    /**
     * 目的位置
     */
    public Integer location_dest_id;

    @JsonIgnore
    public boolean location_dest_idDirtyFlag;
    
    /**
     * 目的位置
     */
    public String location_dest_id_text;

    @JsonIgnore
    public boolean location_dest_id_textDirtyFlag;
    
    /**
     * 源位置
     */
    public Integer location_id;

    @JsonIgnore
    public boolean location_idDirtyFlag;
    
    /**
     * 源位置
     */
    public String location_id_text;

    @JsonIgnore
    public boolean location_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 库存移动不在包裹里
     */
    public String move_ids_without_package;

    @JsonIgnore
    public boolean move_ids_without_packageDirtyFlag;
    
    /**
     * 库存移动
     */
    public String move_lines;

    @JsonIgnore
    public boolean move_linesDirtyFlag;
    
    /**
     * 有包裹作业
     */
    public String move_line_exist;

    @JsonIgnore
    public boolean move_line_existDirtyFlag;
    
    /**
     * 作业
     */
    public String move_line_ids;

    @JsonIgnore
    public boolean move_line_idsDirtyFlag;
    
    /**
     * 无包裹作业
     */
    public String move_line_ids_without_package;

    @JsonIgnore
    public boolean move_line_ids_without_packageDirtyFlag;
    
    /**
     * 送货策略
     */
    public String move_type;

    @JsonIgnore
    public boolean move_typeDirtyFlag;
    
    /**
     * 编号
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 备注
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 源文档
     */
    public String origin;

    @JsonIgnore
    public boolean originDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer owner_id;

    @JsonIgnore
    public boolean owner_idDirtyFlag;
    
    /**
     * 所有者
     */
    public String owner_id_text;

    @JsonIgnore
    public boolean owner_id_textDirtyFlag;
    
    /**
     * 包裹层级
     */
    public String package_level_ids;

    @JsonIgnore
    public boolean package_level_idsDirtyFlag;
    
    /**
     * 包裹层级ids 详情
     */
    public String package_level_ids_details;

    @JsonIgnore
    public boolean package_level_ids_detailsDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 作业的类型
     */
    public String picking_type_code;

    @JsonIgnore
    public boolean picking_type_codeDirtyFlag;
    
    /**
     * 移动整个包裹
     */
    public String picking_type_entire_packs;

    @JsonIgnore
    public boolean picking_type_entire_packsDirtyFlag;
    
    /**
     * 作业类型
     */
    public Integer picking_type_id;

    @JsonIgnore
    public boolean picking_type_idDirtyFlag;
    
    /**
     * 作业类型
     */
    public String picking_type_id_text;

    @JsonIgnore
    public boolean picking_type_id_textDirtyFlag;
    
    /**
     * 已打印
     */
    public String printed;

    @JsonIgnore
    public boolean printedDirtyFlag;
    
    /**
     * 优先级
     */
    public String priority;

    @JsonIgnore
    public boolean priorityDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 采购订单
     */
    public Integer purchase_id;

    @JsonIgnore
    public boolean purchase_idDirtyFlag;
    
    /**
     * 销售订单
     */
    public Integer sale_id;

    @JsonIgnore
    public boolean sale_idDirtyFlag;
    
    /**
     * 销售订单
     */
    public String sale_id_text;

    @JsonIgnore
    public boolean sale_id_textDirtyFlag;
    
    /**
     * 预定交货日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp scheduled_date;

    @JsonIgnore
    public boolean scheduled_dateDirtyFlag;
    
    /**
     * 显示检查可用
     */
    public String show_check_availability;

    @JsonIgnore
    public boolean show_check_availabilityDirtyFlag;
    
    /**
     * 显示批次文本
     */
    public String show_lots_text;

    @JsonIgnore
    public boolean show_lots_textDirtyFlag;
    
    /**
     * 显示标记为代办
     */
    public String show_mark_as_todo;

    @JsonIgnore
    public boolean show_mark_as_todoDirtyFlag;
    
    /**
     * 显示作业
     */
    public String show_operations;

    @JsonIgnore
    public boolean show_operationsDirtyFlag;
    
    /**
     * 显示验证
     */
    public String show_validate;

    @JsonIgnore
    public boolean show_validateDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [欠单]
     */
    @JsonProperty("backorder_id")
    public Integer getBackorder_id(){
        return this.backorder_id ;
    }

    /**
     * 设置 [欠单]
     */
    @JsonProperty("backorder_id")
    public void setBackorder_id(Integer  backorder_id){
        this.backorder_id = backorder_id ;
        this.backorder_idDirtyFlag = true ;
    }

     /**
     * 获取 [欠单]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_idDirtyFlag(){
        return this.backorder_idDirtyFlag ;
    }   

    /**
     * 获取 [欠单]
     */
    @JsonProperty("backorder_ids")
    public String getBackorder_ids(){
        return this.backorder_ids ;
    }

    /**
     * 设置 [欠单]
     */
    @JsonProperty("backorder_ids")
    public void setBackorder_ids(String  backorder_ids){
        this.backorder_ids = backorder_ids ;
        this.backorder_idsDirtyFlag = true ;
    }

     /**
     * 获取 [欠单]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_idsDirtyFlag(){
        return this.backorder_idsDirtyFlag ;
    }   

    /**
     * 获取 [欠单]
     */
    @JsonProperty("backorder_id_text")
    public String getBackorder_id_text(){
        return this.backorder_id_text ;
    }

    /**
     * 设置 [欠单]
     */
    @JsonProperty("backorder_id_text")
    public void setBackorder_id_text(String  backorder_id_text){
        this.backorder_id_text = backorder_id_text ;
        this.backorder_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [欠单]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_id_textDirtyFlag(){
        return this.backorder_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [创建日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [创建日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [调拨日期]
     */
    @JsonProperty("date_done")
    public Timestamp getDate_done(){
        return this.date_done ;
    }

    /**
     * 设置 [调拨日期]
     */
    @JsonProperty("date_done")
    public void setDate_done(Timestamp  date_done){
        this.date_done = date_done ;
        this.date_doneDirtyFlag = true ;
    }

     /**
     * 获取 [调拨日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_doneDirtyFlag(){
        return this.date_doneDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [补货组]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return this.group_id ;
    }

    /**
     * 设置 [补货组]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

     /**
     * 获取 [补货组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return this.group_idDirtyFlag ;
    }   

    /**
     * 获取 [有包裹]
     */
    @JsonProperty("has_packages")
    public String getHas_packages(){
        return this.has_packages ;
    }

    /**
     * 设置 [有包裹]
     */
    @JsonProperty("has_packages")
    public void setHas_packages(String  has_packages){
        this.has_packages = has_packages ;
        this.has_packagesDirtyFlag = true ;
    }

     /**
     * 获取 [有包裹]脏标记
     */
    @JsonIgnore
    public boolean getHas_packagesDirtyFlag(){
        return this.has_packagesDirtyFlag ;
    }   

    /**
     * 获取 [有报废移动]
     */
    @JsonProperty("has_scrap_move")
    public String getHas_scrap_move(){
        return this.has_scrap_move ;
    }

    /**
     * 设置 [有报废移动]
     */
    @JsonProperty("has_scrap_move")
    public void setHas_scrap_move(String  has_scrap_move){
        this.has_scrap_move = has_scrap_move ;
        this.has_scrap_moveDirtyFlag = true ;
    }

     /**
     * 获取 [有报废移动]脏标记
     */
    @JsonIgnore
    public boolean getHas_scrap_moveDirtyFlag(){
        return this.has_scrap_moveDirtyFlag ;
    }   

    /**
     * 获取 [有跟踪]
     */
    @JsonProperty("has_tracking")
    public String getHas_tracking(){
        return this.has_tracking ;
    }

    /**
     * 设置 [有跟踪]
     */
    @JsonProperty("has_tracking")
    public void setHas_tracking(String  has_tracking){
        this.has_tracking = has_tracking ;
        this.has_trackingDirtyFlag = true ;
    }

     /**
     * 获取 [有跟踪]脏标记
     */
    @JsonIgnore
    public boolean getHas_trackingDirtyFlag(){
        return this.has_trackingDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [立即调拨]
     */
    @JsonProperty("immediate_transfer")
    public String getImmediate_transfer(){
        return this.immediate_transfer ;
    }

    /**
     * 设置 [立即调拨]
     */
    @JsonProperty("immediate_transfer")
    public void setImmediate_transfer(String  immediate_transfer){
        this.immediate_transfer = immediate_transfer ;
        this.immediate_transferDirtyFlag = true ;
    }

     /**
     * 获取 [立即调拨]脏标记
     */
    @JsonIgnore
    public boolean getImmediate_transferDirtyFlag(){
        return this.immediate_transferDirtyFlag ;
    }   

    /**
     * 获取 [是锁定]
     */
    @JsonProperty("is_locked")
    public String getIs_locked(){
        return this.is_locked ;
    }

    /**
     * 设置 [是锁定]
     */
    @JsonProperty("is_locked")
    public void setIs_locked(String  is_locked){
        this.is_locked = is_locked ;
        this.is_lockedDirtyFlag = true ;
    }

     /**
     * 获取 [是锁定]脏标记
     */
    @JsonIgnore
    public boolean getIs_lockedDirtyFlag(){
        return this.is_lockedDirtyFlag ;
    }   

    /**
     * 获取 [目的位置]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return this.location_dest_id ;
    }

    /**
     * 设置 [目的位置]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

     /**
     * 获取 [目的位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return this.location_dest_idDirtyFlag ;
    }   

    /**
     * 获取 [目的位置]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return this.location_dest_id_text ;
    }

    /**
     * 设置 [目的位置]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目的位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return this.location_dest_id_textDirtyFlag ;
    }   

    /**
     * 获取 [源位置]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return this.location_id ;
    }

    /**
     * 设置 [源位置]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

     /**
     * 获取 [源位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return this.location_idDirtyFlag ;
    }   

    /**
     * 获取 [源位置]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return this.location_id_text ;
    }

    /**
     * 设置 [源位置]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [源位置]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return this.location_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [库存移动不在包裹里]
     */
    @JsonProperty("move_ids_without_package")
    public String getMove_ids_without_package(){
        return this.move_ids_without_package ;
    }

    /**
     * 设置 [库存移动不在包裹里]
     */
    @JsonProperty("move_ids_without_package")
    public void setMove_ids_without_package(String  move_ids_without_package){
        this.move_ids_without_package = move_ids_without_package ;
        this.move_ids_without_packageDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动不在包裹里]脏标记
     */
    @JsonIgnore
    public boolean getMove_ids_without_packageDirtyFlag(){
        return this.move_ids_without_packageDirtyFlag ;
    }   

    /**
     * 获取 [库存移动]
     */
    @JsonProperty("move_lines")
    public String getMove_lines(){
        return this.move_lines ;
    }

    /**
     * 设置 [库存移动]
     */
    @JsonProperty("move_lines")
    public void setMove_lines(String  move_lines){
        this.move_lines = move_lines ;
        this.move_linesDirtyFlag = true ;
    }

     /**
     * 获取 [库存移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_linesDirtyFlag(){
        return this.move_linesDirtyFlag ;
    }   

    /**
     * 获取 [有包裹作业]
     */
    @JsonProperty("move_line_exist")
    public String getMove_line_exist(){
        return this.move_line_exist ;
    }

    /**
     * 设置 [有包裹作业]
     */
    @JsonProperty("move_line_exist")
    public void setMove_line_exist(String  move_line_exist){
        this.move_line_exist = move_line_exist ;
        this.move_line_existDirtyFlag = true ;
    }

     /**
     * 获取 [有包裹作业]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_existDirtyFlag(){
        return this.move_line_existDirtyFlag ;
    }   

    /**
     * 获取 [作业]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return this.move_line_ids ;
    }

    /**
     * 设置 [作业]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [作业]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return this.move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [无包裹作业]
     */
    @JsonProperty("move_line_ids_without_package")
    public String getMove_line_ids_without_package(){
        return this.move_line_ids_without_package ;
    }

    /**
     * 设置 [无包裹作业]
     */
    @JsonProperty("move_line_ids_without_package")
    public void setMove_line_ids_without_package(String  move_line_ids_without_package){
        this.move_line_ids_without_package = move_line_ids_without_package ;
        this.move_line_ids_without_packageDirtyFlag = true ;
    }

     /**
     * 获取 [无包裹作业]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_ids_without_packageDirtyFlag(){
        return this.move_line_ids_without_packageDirtyFlag ;
    }   

    /**
     * 获取 [送货策略]
     */
    @JsonProperty("move_type")
    public String getMove_type(){
        return this.move_type ;
    }

    /**
     * 设置 [送货策略]
     */
    @JsonProperty("move_type")
    public void setMove_type(String  move_type){
        this.move_type = move_type ;
        this.move_typeDirtyFlag = true ;
    }

     /**
     * 获取 [送货策略]脏标记
     */
    @JsonIgnore
    public boolean getMove_typeDirtyFlag(){
        return this.move_typeDirtyFlag ;
    }   

    /**
     * 获取 [编号]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [编号]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [编号]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [源文档]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return this.origin ;
    }

    /**
     * 设置 [源文档]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

     /**
     * 获取 [源文档]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return this.originDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_id")
    public Integer getOwner_id(){
        return this.owner_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_id")
    public void setOwner_id(Integer  owner_id){
        this.owner_id = owner_id ;
        this.owner_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_idDirtyFlag(){
        return this.owner_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("owner_id_text")
    public String getOwner_id_text(){
        return this.owner_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("owner_id_text")
    public void setOwner_id_text(String  owner_id_text){
        this.owner_id_text = owner_id_text ;
        this.owner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getOwner_id_textDirtyFlag(){
        return this.owner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [包裹层级]
     */
    @JsonProperty("package_level_ids")
    public String getPackage_level_ids(){
        return this.package_level_ids ;
    }

    /**
     * 设置 [包裹层级]
     */
    @JsonProperty("package_level_ids")
    public void setPackage_level_ids(String  package_level_ids){
        this.package_level_ids = package_level_ids ;
        this.package_level_idsDirtyFlag = true ;
    }

     /**
     * 获取 [包裹层级]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_idsDirtyFlag(){
        return this.package_level_idsDirtyFlag ;
    }   

    /**
     * 获取 [包裹层级ids 详情]
     */
    @JsonProperty("package_level_ids_details")
    public String getPackage_level_ids_details(){
        return this.package_level_ids_details ;
    }

    /**
     * 设置 [包裹层级ids 详情]
     */
    @JsonProperty("package_level_ids_details")
    public void setPackage_level_ids_details(String  package_level_ids_details){
        this.package_level_ids_details = package_level_ids_details ;
        this.package_level_ids_detailsDirtyFlag = true ;
    }

     /**
     * 获取 [包裹层级ids 详情]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_ids_detailsDirtyFlag(){
        return this.package_level_ids_detailsDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [作业的类型]
     */
    @JsonProperty("picking_type_code")
    public String getPicking_type_code(){
        return this.picking_type_code ;
    }

    /**
     * 设置 [作业的类型]
     */
    @JsonProperty("picking_type_code")
    public void setPicking_type_code(String  picking_type_code){
        this.picking_type_code = picking_type_code ;
        this.picking_type_codeDirtyFlag = true ;
    }

     /**
     * 获取 [作业的类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_codeDirtyFlag(){
        return this.picking_type_codeDirtyFlag ;
    }   

    /**
     * 获取 [移动整个包裹]
     */
    @JsonProperty("picking_type_entire_packs")
    public String getPicking_type_entire_packs(){
        return this.picking_type_entire_packs ;
    }

    /**
     * 设置 [移动整个包裹]
     */
    @JsonProperty("picking_type_entire_packs")
    public void setPicking_type_entire_packs(String  picking_type_entire_packs){
        this.picking_type_entire_packs = picking_type_entire_packs ;
        this.picking_type_entire_packsDirtyFlag = true ;
    }

     /**
     * 获取 [移动整个包裹]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_entire_packsDirtyFlag(){
        return this.picking_type_entire_packsDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return this.picking_type_id ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return this.picking_type_idDirtyFlag ;
    }   

    /**
     * 获取 [作业类型]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return this.picking_type_id_text ;
    }

    /**
     * 设置 [作业类型]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [作业类型]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return this.picking_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [已打印]
     */
    @JsonProperty("printed")
    public String getPrinted(){
        return this.printed ;
    }

    /**
     * 设置 [已打印]
     */
    @JsonProperty("printed")
    public void setPrinted(String  printed){
        this.printed = printed ;
        this.printedDirtyFlag = true ;
    }

     /**
     * 获取 [已打印]脏标记
     */
    @JsonIgnore
    public boolean getPrintedDirtyFlag(){
        return this.printedDirtyFlag ;
    }   

    /**
     * 获取 [优先级]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [优先级]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

     /**
     * 获取 [优先级]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [采购订单]
     */
    @JsonProperty("purchase_id")
    public Integer getPurchase_id(){
        return this.purchase_id ;
    }

    /**
     * 设置 [采购订单]
     */
    @JsonProperty("purchase_id")
    public void setPurchase_id(Integer  purchase_id){
        this.purchase_id = purchase_id ;
        this.purchase_idDirtyFlag = true ;
    }

     /**
     * 获取 [采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_idDirtyFlag(){
        return this.purchase_idDirtyFlag ;
    }   

    /**
     * 获取 [销售订单]
     */
    @JsonProperty("sale_id")
    public Integer getSale_id(){
        return this.sale_id ;
    }

    /**
     * 设置 [销售订单]
     */
    @JsonProperty("sale_id")
    public void setSale_id(Integer  sale_id){
        this.sale_id = sale_id ;
        this.sale_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单]脏标记
     */
    @JsonIgnore
    public boolean getSale_idDirtyFlag(){
        return this.sale_idDirtyFlag ;
    }   

    /**
     * 获取 [销售订单]
     */
    @JsonProperty("sale_id_text")
    public String getSale_id_text(){
        return this.sale_id_text ;
    }

    /**
     * 设置 [销售订单]
     */
    @JsonProperty("sale_id_text")
    public void setSale_id_text(String  sale_id_text){
        this.sale_id_text = sale_id_text ;
        this.sale_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单]脏标记
     */
    @JsonIgnore
    public boolean getSale_id_textDirtyFlag(){
        return this.sale_id_textDirtyFlag ;
    }   

    /**
     * 获取 [预定交货日期]
     */
    @JsonProperty("scheduled_date")
    public Timestamp getScheduled_date(){
        return this.scheduled_date ;
    }

    /**
     * 设置 [预定交货日期]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(Timestamp  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

     /**
     * 获取 [预定交货日期]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return this.scheduled_dateDirtyFlag ;
    }   

    /**
     * 获取 [显示检查可用]
     */
    @JsonProperty("show_check_availability")
    public String getShow_check_availability(){
        return this.show_check_availability ;
    }

    /**
     * 设置 [显示检查可用]
     */
    @JsonProperty("show_check_availability")
    public void setShow_check_availability(String  show_check_availability){
        this.show_check_availability = show_check_availability ;
        this.show_check_availabilityDirtyFlag = true ;
    }

     /**
     * 获取 [显示检查可用]脏标记
     */
    @JsonIgnore
    public boolean getShow_check_availabilityDirtyFlag(){
        return this.show_check_availabilityDirtyFlag ;
    }   

    /**
     * 获取 [显示批次文本]
     */
    @JsonProperty("show_lots_text")
    public String getShow_lots_text(){
        return this.show_lots_text ;
    }

    /**
     * 设置 [显示批次文本]
     */
    @JsonProperty("show_lots_text")
    public void setShow_lots_text(String  show_lots_text){
        this.show_lots_text = show_lots_text ;
        this.show_lots_textDirtyFlag = true ;
    }

     /**
     * 获取 [显示批次文本]脏标记
     */
    @JsonIgnore
    public boolean getShow_lots_textDirtyFlag(){
        return this.show_lots_textDirtyFlag ;
    }   

    /**
     * 获取 [显示标记为代办]
     */
    @JsonProperty("show_mark_as_todo")
    public String getShow_mark_as_todo(){
        return this.show_mark_as_todo ;
    }

    /**
     * 设置 [显示标记为代办]
     */
    @JsonProperty("show_mark_as_todo")
    public void setShow_mark_as_todo(String  show_mark_as_todo){
        this.show_mark_as_todo = show_mark_as_todo ;
        this.show_mark_as_todoDirtyFlag = true ;
    }

     /**
     * 获取 [显示标记为代办]脏标记
     */
    @JsonIgnore
    public boolean getShow_mark_as_todoDirtyFlag(){
        return this.show_mark_as_todoDirtyFlag ;
    }   

    /**
     * 获取 [显示作业]
     */
    @JsonProperty("show_operations")
    public String getShow_operations(){
        return this.show_operations ;
    }

    /**
     * 设置 [显示作业]
     */
    @JsonProperty("show_operations")
    public void setShow_operations(String  show_operations){
        this.show_operations = show_operations ;
        this.show_operationsDirtyFlag = true ;
    }

     /**
     * 获取 [显示作业]脏标记
     */
    @JsonIgnore
    public boolean getShow_operationsDirtyFlag(){
        return this.show_operationsDirtyFlag ;
    }   

    /**
     * 获取 [显示验证]
     */
    @JsonProperty("show_validate")
    public String getShow_validate(){
        return this.show_validate ;
    }

    /**
     * 设置 [显示验证]
     */
    @JsonProperty("show_validate")
    public void setShow_validate(String  show_validate){
        this.show_validate = show_validate ;
        this.show_validateDirtyFlag = true ;
    }

     /**
     * 获取 [显示验证]脏标记
     */
    @JsonIgnore
    public boolean getShow_validateDirtyFlag(){
        return this.show_validateDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("activity_date_deadline") instanceof Boolean)&& map.get("activity_date_deadline")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("activity_date_deadline"));
   			this.setActivity_date_deadline(new Timestamp(parse.getTime()));
		}
		if(!(map.get("activity_ids") instanceof Boolean)&& map.get("activity_ids")!=null){
			Object[] objs = (Object[])map.get("activity_ids");
			if(objs.length > 0){
				Integer[] activity_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setActivity_ids(Arrays.toString(activity_ids).replace(" ",""));
			}
		}
		if(!(map.get("activity_state") instanceof Boolean)&& map.get("activity_state")!=null){
			this.setActivity_state((String)map.get("activity_state"));
		}
		if(!(map.get("activity_summary") instanceof Boolean)&& map.get("activity_summary")!=null){
			this.setActivity_summary((String)map.get("activity_summary"));
		}
		if(!(map.get("activity_type_id") instanceof Boolean)&& map.get("activity_type_id")!=null){
			Object[] objs = (Object[])map.get("activity_type_id");
			if(objs.length > 0){
				this.setActivity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("activity_user_id") instanceof Boolean)&& map.get("activity_user_id")!=null){
			Object[] objs = (Object[])map.get("activity_user_id");
			if(objs.length > 0){
				this.setActivity_user_id((Integer)objs[0]);
			}
		}
		if(!(map.get("backorder_id") instanceof Boolean)&& map.get("backorder_id")!=null){
			Object[] objs = (Object[])map.get("backorder_id");
			if(objs.length > 0){
				this.setBackorder_id((Integer)objs[0]);
			}
		}
		if(!(map.get("backorder_ids") instanceof Boolean)&& map.get("backorder_ids")!=null){
			Object[] objs = (Object[])map.get("backorder_ids");
			if(objs.length > 0){
				Integer[] backorder_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setBackorder_ids(Arrays.toString(backorder_ids).replace(" ",""));
			}
		}
		if(!(map.get("backorder_id") instanceof Boolean)&& map.get("backorder_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("backorder_id");
			if(objs.length > 1){
				this.setBackorder_id_text((String)objs[1]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_done") instanceof Boolean)&& map.get("date_done")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_done"));
   			this.setDate_done(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("group_id") instanceof Boolean)&& map.get("group_id")!=null){
			Object[] objs = (Object[])map.get("group_id");
			if(objs.length > 0){
				this.setGroup_id((Integer)objs[0]);
			}
		}
		if(map.get("has_packages") instanceof Boolean){
			this.setHas_packages(((Boolean)map.get("has_packages"))? "true" : "false");
		}
		if(map.get("has_scrap_move") instanceof Boolean){
			this.setHas_scrap_move(((Boolean)map.get("has_scrap_move"))? "true" : "false");
		}
		if(map.get("has_tracking") instanceof Boolean){
			this.setHas_tracking(((Boolean)map.get("has_tracking"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("immediate_transfer") instanceof Boolean){
			this.setImmediate_transfer(((Boolean)map.get("immediate_transfer"))? "true" : "false");
		}
		if(map.get("is_locked") instanceof Boolean){
			this.setIs_locked(((Boolean)map.get("is_locked"))? "true" : "false");
		}
		if(!(map.get("location_dest_id") instanceof Boolean)&& map.get("location_dest_id")!=null){
			Object[] objs = (Object[])map.get("location_dest_id");
			if(objs.length > 0){
				this.setLocation_dest_id((Integer)objs[0]);
			}
		}
		if(!(map.get("location_dest_id") instanceof Boolean)&& map.get("location_dest_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("location_dest_id");
			if(objs.length > 1){
				this.setLocation_dest_id_text((String)objs[1]);
			}
		}
		if(!(map.get("location_id") instanceof Boolean)&& map.get("location_id")!=null){
			Object[] objs = (Object[])map.get("location_id");
			if(objs.length > 0){
				this.setLocation_id((Integer)objs[0]);
			}
		}
		if(!(map.get("location_id") instanceof Boolean)&& map.get("location_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("location_id");
			if(objs.length > 1){
				this.setLocation_id_text((String)objs[1]);
			}
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids).replace(" ",""));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids).replace(" ",""));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids).replace(" ",""));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("move_ids_without_package") instanceof Boolean)&& map.get("move_ids_without_package")!=null){
			Object[] objs = (Object[])map.get("move_ids_without_package");
			if(objs.length > 0){
				Integer[] move_ids_without_package = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_ids_without_package(Arrays.toString(move_ids_without_package).replace(" ",""));
			}
		}
		if(!(map.get("move_lines") instanceof Boolean)&& map.get("move_lines")!=null){
			Object[] objs = (Object[])map.get("move_lines");
			if(objs.length > 0){
				Integer[] move_lines = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_lines(Arrays.toString(move_lines).replace(" ",""));
			}
		}
		if(map.get("move_line_exist") instanceof Boolean){
			this.setMove_line_exist(((Boolean)map.get("move_line_exist"))? "true" : "false");
		}
		if(!(map.get("move_line_ids") instanceof Boolean)&& map.get("move_line_ids")!=null){
			Object[] objs = (Object[])map.get("move_line_ids");
			if(objs.length > 0){
				Integer[] move_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_line_ids(Arrays.toString(move_line_ids).replace(" ",""));
			}
		}
		if(!(map.get("move_line_ids_without_package") instanceof Boolean)&& map.get("move_line_ids_without_package")!=null){
			Object[] objs = (Object[])map.get("move_line_ids_without_package");
			if(objs.length > 0){
				Integer[] move_line_ids_without_package = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_line_ids_without_package(Arrays.toString(move_line_ids_without_package).replace(" ",""));
			}
		}
		if(!(map.get("move_type") instanceof Boolean)&& map.get("move_type")!=null){
			this.setMove_type((String)map.get("move_type"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("note") instanceof Boolean)&& map.get("note")!=null){
			this.setNote((String)map.get("note"));
		}
		if(!(map.get("origin") instanceof Boolean)&& map.get("origin")!=null){
			this.setOrigin((String)map.get("origin"));
		}
		if(!(map.get("owner_id") instanceof Boolean)&& map.get("owner_id")!=null){
			Object[] objs = (Object[])map.get("owner_id");
			if(objs.length > 0){
				this.setOwner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("owner_id") instanceof Boolean)&& map.get("owner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("owner_id");
			if(objs.length > 1){
				this.setOwner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("package_level_ids") instanceof Boolean)&& map.get("package_level_ids")!=null){
			Object[] objs = (Object[])map.get("package_level_ids");
			if(objs.length > 0){
				Integer[] package_level_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPackage_level_ids(Arrays.toString(package_level_ids).replace(" ",""));
			}
		}
		if(!(map.get("package_level_ids_details") instanceof Boolean)&& map.get("package_level_ids_details")!=null){
			Object[] objs = (Object[])map.get("package_level_ids_details");
			if(objs.length > 0){
				Integer[] package_level_ids_details = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPackage_level_ids_details(Arrays.toString(package_level_ids_details).replace(" ",""));
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("picking_type_code") instanceof Boolean)&& map.get("picking_type_code")!=null){
			this.setPicking_type_code((String)map.get("picking_type_code"));
		}
		if(map.get("picking_type_entire_packs") instanceof Boolean){
			this.setPicking_type_entire_packs(((Boolean)map.get("picking_type_entire_packs"))? "true" : "false");
		}
		if(!(map.get("picking_type_id") instanceof Boolean)&& map.get("picking_type_id")!=null){
			Object[] objs = (Object[])map.get("picking_type_id");
			if(objs.length > 0){
				this.setPicking_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("picking_type_id") instanceof Boolean)&& map.get("picking_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("picking_type_id");
			if(objs.length > 1){
				this.setPicking_type_id_text((String)objs[1]);
			}
		}
		if(map.get("printed") instanceof Boolean){
			this.setPrinted(((Boolean)map.get("printed"))? "true" : "false");
		}
		if(!(map.get("priority") instanceof Boolean)&& map.get("priority")!=null){
			this.setPriority((String)map.get("priority"));
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("purchase_id") instanceof Boolean)&& map.get("purchase_id")!=null){
			Object[] objs = (Object[])map.get("purchase_id");
			if(objs.length > 0){
				this.setPurchase_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sale_id") instanceof Boolean)&& map.get("sale_id")!=null){
			Object[] objs = (Object[])map.get("sale_id");
			if(objs.length > 0){
				this.setSale_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sale_id") instanceof Boolean)&& map.get("sale_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("sale_id");
			if(objs.length > 1){
				this.setSale_id_text((String)objs[1]);
			}
		}
		if(!(map.get("scheduled_date") instanceof Boolean)&& map.get("scheduled_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("scheduled_date"));
   			this.setScheduled_date(new Timestamp(parse.getTime()));
		}
		if(map.get("show_check_availability") instanceof Boolean){
			this.setShow_check_availability(((Boolean)map.get("show_check_availability"))? "true" : "false");
		}
		if(map.get("show_lots_text") instanceof Boolean){
			this.setShow_lots_text(((Boolean)map.get("show_lots_text"))? "true" : "false");
		}
		if(map.get("show_mark_as_todo") instanceof Boolean){
			this.setShow_mark_as_todo(((Boolean)map.get("show_mark_as_todo"))? "true" : "false");
		}
		if(map.get("show_operations") instanceof Boolean){
			this.setShow_operations(((Boolean)map.get("show_operations"))? "true" : "false");
		}
		if(map.get("show_validate") instanceof Boolean){
			this.setShow_validate(((Boolean)map.get("show_validate"))? "true" : "false");
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("website_id") instanceof Boolean)&& map.get("website_id")!=null){
			Object[] objs = (Object[])map.get("website_id");
			if(objs.length > 0){
				this.setWebsite_id((Integer)objs[0]);
			}
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActivity_date_deadline()!=null&&this.getActivity_date_deadlineDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getActivity_date_deadline());
			map.put("activity_date_deadline",datetimeStr);
		}else if(this.getActivity_date_deadlineDirtyFlag()){
			map.put("activity_date_deadline",false);
		}
		if(this.getActivity_ids()!=null&&this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",this.getActivity_ids());
		}else if(this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",false);
		}
		if(this.getActivity_state()!=null&&this.getActivity_stateDirtyFlag()){
			map.put("activity_state",this.getActivity_state());
		}else if(this.getActivity_stateDirtyFlag()){
			map.put("activity_state",false);
		}
		if(this.getActivity_summary()!=null&&this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",this.getActivity_summary());
		}else if(this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",false);
		}
		if(this.getActivity_type_id()!=null&&this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",this.getActivity_type_id());
		}else if(this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",false);
		}
		if(this.getActivity_user_id()!=null&&this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",this.getActivity_user_id());
		}else if(this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",false);
		}
		if(this.getBackorder_id()!=null&&this.getBackorder_idDirtyFlag()){
			map.put("backorder_id",this.getBackorder_id());
		}else if(this.getBackorder_idDirtyFlag()){
			map.put("backorder_id",false);
		}
		if(this.getBackorder_ids()!=null&&this.getBackorder_idsDirtyFlag()){
			map.put("backorder_ids",this.getBackorder_ids());
		}else if(this.getBackorder_idsDirtyFlag()){
			map.put("backorder_ids",false);
		}
		if(this.getBackorder_id_text()!=null&&this.getBackorder_id_textDirtyFlag()){
			//忽略文本外键backorder_id_text
		}else if(this.getBackorder_id_textDirtyFlag()){
			map.put("backorder_id",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDate_done()!=null&&this.getDate_doneDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_done());
			map.put("date_done",datetimeStr);
		}else if(this.getDate_doneDirtyFlag()){
			map.put("date_done",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getGroup_id()!=null&&this.getGroup_idDirtyFlag()){
			map.put("group_id",this.getGroup_id());
		}else if(this.getGroup_idDirtyFlag()){
			map.put("group_id",false);
		}
		if(this.getHas_packages()!=null&&this.getHas_packagesDirtyFlag()){
			map.put("has_packages",Boolean.parseBoolean(this.getHas_packages()));		
		}		if(this.getHas_scrap_move()!=null&&this.getHas_scrap_moveDirtyFlag()){
			map.put("has_scrap_move",Boolean.parseBoolean(this.getHas_scrap_move()));		
		}		if(this.getHas_tracking()!=null&&this.getHas_trackingDirtyFlag()){
			map.put("has_tracking",Boolean.parseBoolean(this.getHas_tracking()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getImmediate_transfer()!=null&&this.getImmediate_transferDirtyFlag()){
			map.put("immediate_transfer",Boolean.parseBoolean(this.getImmediate_transfer()));		
		}		if(this.getIs_locked()!=null&&this.getIs_lockedDirtyFlag()){
			map.put("is_locked",Boolean.parseBoolean(this.getIs_locked()));		
		}		if(this.getLocation_dest_id()!=null&&this.getLocation_dest_idDirtyFlag()){
			map.put("location_dest_id",this.getLocation_dest_id());
		}else if(this.getLocation_dest_idDirtyFlag()){
			map.put("location_dest_id",false);
		}
		if(this.getLocation_dest_id_text()!=null&&this.getLocation_dest_id_textDirtyFlag()){
			//忽略文本外键location_dest_id_text
		}else if(this.getLocation_dest_id_textDirtyFlag()){
			map.put("location_dest_id",false);
		}
		if(this.getLocation_id()!=null&&this.getLocation_idDirtyFlag()){
			map.put("location_id",this.getLocation_id());
		}else if(this.getLocation_idDirtyFlag()){
			map.put("location_id",false);
		}
		if(this.getLocation_id_text()!=null&&this.getLocation_id_textDirtyFlag()){
			//忽略文本外键location_id_text
		}else if(this.getLocation_id_textDirtyFlag()){
			map.put("location_id",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getMove_ids_without_package()!=null&&this.getMove_ids_without_packageDirtyFlag()){
			map.put("move_ids_without_package",this.getMove_ids_without_package());
		}else if(this.getMove_ids_without_packageDirtyFlag()){
			map.put("move_ids_without_package",false);
		}
		if(this.getMove_lines()!=null&&this.getMove_linesDirtyFlag()){
			map.put("move_lines",this.getMove_lines());
		}else if(this.getMove_linesDirtyFlag()){
			map.put("move_lines",false);
		}
		if(this.getMove_line_exist()!=null&&this.getMove_line_existDirtyFlag()){
			map.put("move_line_exist",Boolean.parseBoolean(this.getMove_line_exist()));		
		}		if(this.getMove_line_ids()!=null&&this.getMove_line_idsDirtyFlag()){
			map.put("move_line_ids",this.getMove_line_ids());
		}else if(this.getMove_line_idsDirtyFlag()){
			map.put("move_line_ids",false);
		}
		if(this.getMove_line_ids_without_package()!=null&&this.getMove_line_ids_without_packageDirtyFlag()){
			map.put("move_line_ids_without_package",this.getMove_line_ids_without_package());
		}else if(this.getMove_line_ids_without_packageDirtyFlag()){
			map.put("move_line_ids_without_package",false);
		}
		if(this.getMove_type()!=null&&this.getMove_typeDirtyFlag()){
			map.put("move_type",this.getMove_type());
		}else if(this.getMove_typeDirtyFlag()){
			map.put("move_type",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNote()!=null&&this.getNoteDirtyFlag()){
			map.put("note",this.getNote());
		}else if(this.getNoteDirtyFlag()){
			map.put("note",false);
		}
		if(this.getOrigin()!=null&&this.getOriginDirtyFlag()){
			map.put("origin",this.getOrigin());
		}else if(this.getOriginDirtyFlag()){
			map.put("origin",false);
		}
		if(this.getOwner_id()!=null&&this.getOwner_idDirtyFlag()){
			map.put("owner_id",this.getOwner_id());
		}else if(this.getOwner_idDirtyFlag()){
			map.put("owner_id",false);
		}
		if(this.getOwner_id_text()!=null&&this.getOwner_id_textDirtyFlag()){
			//忽略文本外键owner_id_text
		}else if(this.getOwner_id_textDirtyFlag()){
			map.put("owner_id",false);
		}
		if(this.getPackage_level_ids()!=null&&this.getPackage_level_idsDirtyFlag()){
			map.put("package_level_ids",this.getPackage_level_ids());
		}else if(this.getPackage_level_idsDirtyFlag()){
			map.put("package_level_ids",false);
		}
		if(this.getPackage_level_ids_details()!=null&&this.getPackage_level_ids_detailsDirtyFlag()){
			map.put("package_level_ids_details",this.getPackage_level_ids_details());
		}else if(this.getPackage_level_ids_detailsDirtyFlag()){
			map.put("package_level_ids_details",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPicking_type_code()!=null&&this.getPicking_type_codeDirtyFlag()){
			map.put("picking_type_code",this.getPicking_type_code());
		}else if(this.getPicking_type_codeDirtyFlag()){
			map.put("picking_type_code",false);
		}
		if(this.getPicking_type_entire_packs()!=null&&this.getPicking_type_entire_packsDirtyFlag()){
			map.put("picking_type_entire_packs",Boolean.parseBoolean(this.getPicking_type_entire_packs()));		
		}		if(this.getPicking_type_id()!=null&&this.getPicking_type_idDirtyFlag()){
			map.put("picking_type_id",this.getPicking_type_id());
		}else if(this.getPicking_type_idDirtyFlag()){
			map.put("picking_type_id",false);
		}
		if(this.getPicking_type_id_text()!=null&&this.getPicking_type_id_textDirtyFlag()){
			//忽略文本外键picking_type_id_text
		}else if(this.getPicking_type_id_textDirtyFlag()){
			map.put("picking_type_id",false);
		}
		if(this.getPrinted()!=null&&this.getPrintedDirtyFlag()){
			map.put("printed",Boolean.parseBoolean(this.getPrinted()));		
		}		if(this.getPriority()!=null&&this.getPriorityDirtyFlag()){
			map.put("priority",this.getPriority());
		}else if(this.getPriorityDirtyFlag()){
			map.put("priority",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getPurchase_id()!=null&&this.getPurchase_idDirtyFlag()){
			map.put("purchase_id",this.getPurchase_id());
		}else if(this.getPurchase_idDirtyFlag()){
			map.put("purchase_id",false);
		}
		if(this.getSale_id()!=null&&this.getSale_idDirtyFlag()){
			map.put("sale_id",this.getSale_id());
		}else if(this.getSale_idDirtyFlag()){
			map.put("sale_id",false);
		}
		if(this.getSale_id_text()!=null&&this.getSale_id_textDirtyFlag()){
			//忽略文本外键sale_id_text
		}else if(this.getSale_id_textDirtyFlag()){
			map.put("sale_id",false);
		}
		if(this.getScheduled_date()!=null&&this.getScheduled_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getScheduled_date());
			map.put("scheduled_date",datetimeStr);
		}else if(this.getScheduled_dateDirtyFlag()){
			map.put("scheduled_date",false);
		}
		if(this.getShow_check_availability()!=null&&this.getShow_check_availabilityDirtyFlag()){
			map.put("show_check_availability",Boolean.parseBoolean(this.getShow_check_availability()));		
		}		if(this.getShow_lots_text()!=null&&this.getShow_lots_textDirtyFlag()){
			map.put("show_lots_text",Boolean.parseBoolean(this.getShow_lots_text()));		
		}		if(this.getShow_mark_as_todo()!=null&&this.getShow_mark_as_todoDirtyFlag()){
			map.put("show_mark_as_todo",Boolean.parseBoolean(this.getShow_mark_as_todo()));		
		}		if(this.getShow_operations()!=null&&this.getShow_operationsDirtyFlag()){
			map.put("show_operations",Boolean.parseBoolean(this.getShow_operations()));		
		}		if(this.getShow_validate()!=null&&this.getShow_validateDirtyFlag()){
			map.put("show_validate",Boolean.parseBoolean(this.getShow_validate()));		
		}		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getWebsite_id()!=null&&this.getWebsite_idDirtyFlag()){
			map.put("website_id",this.getWebsite_id());
		}else if(this.getWebsite_idDirtyFlag()){
			map.put("website_id",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
