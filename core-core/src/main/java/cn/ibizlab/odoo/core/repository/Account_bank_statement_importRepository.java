package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_bank_statement_import;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_importSearchContext;

/**
 * 实体 [导入银行对账单] 存储对象
 */
public interface Account_bank_statement_importRepository extends Repository<Account_bank_statement_import> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_bank_statement_import> searchDefault(Account_bank_statement_importSearchContext context);

    Account_bank_statement_import convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import domain , Account_bank_statement_import po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import convert2Domain( Account_bank_statement_import po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import domain) ;

}
