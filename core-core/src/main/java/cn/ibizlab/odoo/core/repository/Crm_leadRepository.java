package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_lead;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_leadSearchContext;

/**
 * 实体 [线索/商机] 存储对象
 */
public interface Crm_leadRepository extends Repository<Crm_lead> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_lead> searchDefault(Crm_leadSearchContext context);

    Crm_lead convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead domain , Crm_lead po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead convert2Domain( Crm_lead po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead domain) ;

}
