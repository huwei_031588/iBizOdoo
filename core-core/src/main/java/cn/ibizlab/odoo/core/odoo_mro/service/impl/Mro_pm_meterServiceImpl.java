package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meterSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meterService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_meterOdooClient;
import cn.ibizlab.odoo.core.odoo_mro.clientmodel.mro_pm_meterClientModel;

/**
 * 实体[Asset Meters] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_meterServiceImpl implements IMro_pm_meterService {

    @Autowired
    mro_pm_meterOdooClient mro_pm_meterOdooClient;


    @Override
    public boolean update(Mro_pm_meter et) {
        mro_pm_meterClientModel clientModel = convert2Model(et,null);
		mro_pm_meterOdooClient.update(clientModel);
        Mro_pm_meter rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mro_pm_meter> list){
    }

    @Override
    public boolean remove(Integer id) {
        mro_pm_meterClientModel clientModel = new mro_pm_meterClientModel();
        clientModel.setId(id);
		mro_pm_meterOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mro_pm_meter get(Integer id) {
        mro_pm_meterClientModel clientModel = new mro_pm_meterClientModel();
        clientModel.setId(id);
		mro_pm_meterOdooClient.get(clientModel);
        Mro_pm_meter et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mro_pm_meter();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mro_pm_meter et) {
        mro_pm_meterClientModel clientModel = convert2Model(et,null);
		mro_pm_meterOdooClient.create(clientModel);
        Mro_pm_meter rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_meter> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_meter> searchDefault(Mro_pm_meterSearchContext context) {
        List<Mro_pm_meter> list = new ArrayList<Mro_pm_meter>();
        Page<mro_pm_meterClientModel> clientModelList = mro_pm_meterOdooClient.search(context);
        for(mro_pm_meterClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mro_pm_meter>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mro_pm_meterClientModel convert2Model(Mro_pm_meter domain , mro_pm_meterClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mro_pm_meterClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("min_utilizationdirtyflag"))
                model.setMin_utilization(domain.getMinUtilization());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("reading_typedirtyflag"))
                model.setReading_type(domain.getReadingType());
            if((Boolean) domain.getExtensionparams().get("valuedirtyflag"))
                model.setValue(domain.getValue());
            if((Boolean) domain.getExtensionparams().get("meter_line_idsdirtyflag"))
                model.setMeter_line_ids(domain.getMeterLineIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("view_line_idsdirtyflag"))
                model.setView_line_ids(domain.getViewLineIds());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("utilizationdirtyflag"))
                model.setUtilization(domain.getUtilization());
            if((Boolean) domain.getExtensionparams().get("new_valuedirtyflag"))
                model.setNew_value(domain.getNewValue());
            if((Boolean) domain.getExtensionparams().get("av_timedirtyflag"))
                model.setAv_time(domain.getAvTime());
            if((Boolean) domain.getExtensionparams().get("total_valuedirtyflag"))
                model.setTotal_value(domain.getTotalValue());
            if((Boolean) domain.getExtensionparams().get("name_textdirtyflag"))
                model.setName_text(domain.getNameText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("meter_uomdirtyflag"))
                model.setMeter_uom(domain.getMeterUom());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("asset_id_textdirtyflag"))
                model.setAsset_id_text(domain.getAssetIdText());
            if((Boolean) domain.getExtensionparams().get("parent_ratio_id_textdirtyflag"))
                model.setParent_ratio_id_text(domain.getParentRatioIdText());
            if((Boolean) domain.getExtensionparams().get("parent_meter_iddirtyflag"))
                model.setParent_meter_id(domain.getParentMeterId());
            if((Boolean) domain.getExtensionparams().get("parent_ratio_iddirtyflag"))
                model.setParent_ratio_id(domain.getParentRatioId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("asset_iddirtyflag"))
                model.setAsset_id(domain.getAssetId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mro_pm_meter convert2Domain( mro_pm_meterClientModel model ,Mro_pm_meter domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mro_pm_meter();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMin_utilizationDirtyFlag())
            domain.setMinUtilization(model.getMin_utilization());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getReading_typeDirtyFlag())
            domain.setReadingType(model.getReading_type());
        if(model.getValueDirtyFlag())
            domain.setValue(model.getValue());
        if(model.getMeter_line_idsDirtyFlag())
            domain.setMeterLineIds(model.getMeter_line_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getView_line_idsDirtyFlag())
            domain.setViewLineIds(model.getView_line_ids());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getUtilizationDirtyFlag())
            domain.setUtilization(model.getUtilization());
        if(model.getNew_valueDirtyFlag())
            domain.setNewValue(model.getNew_value());
        if(model.getAv_timeDirtyFlag())
            domain.setAvTime(model.getAv_time());
        if(model.getTotal_valueDirtyFlag())
            domain.setTotalValue(model.getTotal_value());
        if(model.getName_textDirtyFlag())
            domain.setNameText(model.getName_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getMeter_uomDirtyFlag())
            domain.setMeterUom(model.getMeter_uom());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getAsset_id_textDirtyFlag())
            domain.setAssetIdText(model.getAsset_id_text());
        if(model.getParent_ratio_id_textDirtyFlag())
            domain.setParentRatioIdText(model.getParent_ratio_id_text());
        if(model.getParent_meter_idDirtyFlag())
            domain.setParentMeterId(model.getParent_meter_id());
        if(model.getParent_ratio_idDirtyFlag())
            domain.setParentRatioId(model.getParent_ratio_id());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getAsset_idDirtyFlag())
            domain.setAssetId(model.getAsset_id());
        return domain ;
    }

}

    



