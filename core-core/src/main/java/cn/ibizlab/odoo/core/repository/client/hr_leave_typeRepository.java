package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_leave_type;

/**
 * 实体[hr_leave_type] 服务对象接口
 */
public interface hr_leave_typeRepository{


    public hr_leave_type createPO() ;
        public void updateBatch(hr_leave_type hr_leave_type);

        public List<hr_leave_type> search();

        public void remove(String id);

        public void create(hr_leave_type hr_leave_type);

        public void get(String id);

        public void update(hr_leave_type hr_leave_type);

        public void createBatch(hr_leave_type hr_leave_type);

        public void removeBatch(String id);


}
