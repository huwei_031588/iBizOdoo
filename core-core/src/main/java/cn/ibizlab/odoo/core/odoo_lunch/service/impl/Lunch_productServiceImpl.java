package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_productSearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_productService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_productOdooClient;
import cn.ibizlab.odoo.core.odoo_lunch.clientmodel.lunch_productClientModel;

/**
 * 实体[工作餐产品] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_productServiceImpl implements ILunch_productService {

    @Autowired
    lunch_productOdooClient lunch_productOdooClient;


    @Override
    public Lunch_product get(Integer id) {
        lunch_productClientModel clientModel = new lunch_productClientModel();
        clientModel.setId(id);
		lunch_productOdooClient.get(clientModel);
        Lunch_product et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Lunch_product();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Lunch_product et) {
        lunch_productClientModel clientModel = convert2Model(et,null);
		lunch_productOdooClient.update(clientModel);
        Lunch_product rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Lunch_product> list){
    }

    @Override
    public boolean create(Lunch_product et) {
        lunch_productClientModel clientModel = convert2Model(et,null);
		lunch_productOdooClient.create(clientModel);
        Lunch_product rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_product> list){
    }

    @Override
    public boolean remove(Integer id) {
        lunch_productClientModel clientModel = new lunch_productClientModel();
        clientModel.setId(id);
		lunch_productOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_product> searchDefault(Lunch_productSearchContext context) {
        List<Lunch_product> list = new ArrayList<Lunch_product>();
        Page<lunch_productClientModel> clientModelList = lunch_productOdooClient.search(context);
        for(lunch_productClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Lunch_product>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public lunch_productClientModel convert2Model(Lunch_product domain , lunch_productClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new lunch_productClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("availabledirtyflag"))
                model.setAvailable(domain.getAvailable());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("pricedirtyflag"))
                model.setPrice(domain.getPrice());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("supplier_textdirtyflag"))
                model.setSupplier_text(domain.getSupplierText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("supplierdirtyflag"))
                model.setSupplier(domain.getSupplier());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Lunch_product convert2Domain( lunch_productClientModel model ,Lunch_product domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Lunch_product();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getAvailableDirtyFlag())
            domain.setAvailable(model.getAvailable());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPriceDirtyFlag())
            domain.setPrice(model.getPrice());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getSupplier_textDirtyFlag())
            domain.setSupplierText(model.getSupplier_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getSupplierDirtyFlag())
            domain.setSupplier(model.getSupplier());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        return domain ;
    }

}

    



