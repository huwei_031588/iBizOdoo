package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_employee;

/**
 * 实体[hr_employee] 服务对象接口
 */
public interface hr_employeeRepository{


    public hr_employee createPO() ;
        public void createBatch(hr_employee hr_employee);

        public void update(hr_employee hr_employee);

        public void updateBatch(hr_employee hr_employee);

        public List<hr_employee> search();

        public void removeBatch(String id);

        public void remove(String id);

        public void create(hr_employee hr_employee);

        public void get(String id);


}
