package cn.ibizlab.odoo.core.odoo_lunch.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_cashmoveService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_lunch.client.lunch_cashmoveOdooClient;
import cn.ibizlab.odoo.core.odoo_lunch.clientmodel.lunch_cashmoveClientModel;

/**
 * 实体[午餐费的现金划转] 服务对象接口实现
 */
@Slf4j
@Service
public class Lunch_cashmoveServiceImpl implements ILunch_cashmoveService {

    @Autowired
    lunch_cashmoveOdooClient lunch_cashmoveOdooClient;


    @Override
    public boolean remove(Integer id) {
        lunch_cashmoveClientModel clientModel = new lunch_cashmoveClientModel();
        clientModel.setId(id);
		lunch_cashmoveOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Lunch_cashmove et) {
        lunch_cashmoveClientModel clientModel = convert2Model(et,null);
		lunch_cashmoveOdooClient.update(clientModel);
        Lunch_cashmove rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Lunch_cashmove> list){
    }

    @Override
    public boolean create(Lunch_cashmove et) {
        lunch_cashmoveClientModel clientModel = convert2Model(et,null);
		lunch_cashmoveOdooClient.create(clientModel);
        Lunch_cashmove rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Lunch_cashmove> list){
    }

    @Override
    public Lunch_cashmove get(Integer id) {
        lunch_cashmoveClientModel clientModel = new lunch_cashmoveClientModel();
        clientModel.setId(id);
		lunch_cashmoveOdooClient.get(clientModel);
        Lunch_cashmove et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Lunch_cashmove();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Lunch_cashmove> searchDefault(Lunch_cashmoveSearchContext context) {
        List<Lunch_cashmove> list = new ArrayList<Lunch_cashmove>();
        Page<lunch_cashmoveClientModel> clientModelList = lunch_cashmoveOdooClient.search(context);
        for(lunch_cashmoveClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Lunch_cashmove>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public lunch_cashmoveClientModel convert2Model(Lunch_cashmove domain , lunch_cashmoveClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new lunch_cashmoveClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("order_id_textdirtyflag"))
                model.setOrder_id_text(domain.getOrderIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("order_iddirtyflag"))
                model.setOrder_id(domain.getOrderId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Lunch_cashmove convert2Domain( lunch_cashmoveClientModel model ,Lunch_cashmove domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Lunch_cashmove();
        }

        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getOrder_id_textDirtyFlag())
            domain.setOrderIdText(model.getOrder_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getOrder_idDirtyFlag())
            domain.setOrderId(model.getOrder_id());
        return domain ;
    }

}

    



