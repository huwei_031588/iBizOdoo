package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_import_import;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_importSearchContext;

/**
 * 实体 [基础导入] 存储对象
 */
public interface Base_import_importRepository extends Repository<Base_import_import> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_import_import> searchDefault(Base_import_importSearchContext context);

    Base_import_import convert2PO(cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_import domain , Base_import_import po) ;

    cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_import convert2Domain( Base_import_import po ,cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_import domain) ;

}
