package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;

/**
 * 实体 [应聘者来源] 存储模型
 */
public interface Hr_recruitment_source{

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 网址参数
     */
    String getUrl();

    void setUrl(String url);

    /**
     * 获取 [网址参数]脏标记
     */
    boolean getUrlDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 招聘ID
     */
    String getJob_id_text();

    void setJob_id_text(String job_id_text);

    /**
     * 获取 [招聘ID]脏标记
     */
    boolean getJob_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * EMail
     */
    String getEmail();

    void setEmail(String email);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getEmailDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 来源名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [来源名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 来源
     */
    Integer getSource_id();

    void setSource_id(Integer source_id);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_idDirtyFlag();

    /**
     * 别名ID
     */
    Integer getAlias_id();

    void setAlias_id(Integer alias_id);

    /**
     * 获取 [别名ID]脏标记
     */
    boolean getAlias_idDirtyFlag();

    /**
     * 招聘ID
     */
    Integer getJob_id();

    void setJob_id(Integer job_id);

    /**
     * 获取 [招聘ID]脏标记
     */
    boolean getJob_idDirtyFlag();

}
