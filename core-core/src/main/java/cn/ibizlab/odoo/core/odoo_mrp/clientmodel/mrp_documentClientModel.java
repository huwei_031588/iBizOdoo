package cn.ibizlab.odoo.core.odoo_mrp.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mrp_document] 对象
 */
public class mrp_documentClientModel implements Serializable{

    /**
     * 访问令牌
     */
    public String access_token;

    @JsonIgnore
    public boolean access_tokenDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 校验和/SHA1
     */
    public String checksum;

    @JsonIgnore
    public boolean checksumDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 文件内容
     */
    public byte[] datas;

    @JsonIgnore
    public boolean datasDirtyFlag;
    
    /**
     * 文件名
     */
    public String datas_fname;

    @JsonIgnore
    public boolean datas_fnameDirtyFlag;
    
    /**
     * 数据库数据
     */
    public byte[] db_datas;

    @JsonIgnore
    public boolean db_datasDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 文件大小
     */
    public Integer file_size;

    @JsonIgnore
    public boolean file_sizeDirtyFlag;
    
    /**
     * 是公开文档
     */
    public String ibizpublic;

    @JsonIgnore
    public boolean ibizpublicDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 索引的内容
     */
    public String index_content;

    @JsonIgnore
    public boolean index_contentDirtyFlag;
    
    /**
     * 相关的附件
     */
    public Integer ir_attachment_id;

    @JsonIgnore
    public boolean ir_attachment_idDirtyFlag;
    
    /**
     * 键
     */
    public String key;

    @JsonIgnore
    public boolean keyDirtyFlag;
    
    /**
     * 附件网址
     */
    public String local_url;

    @JsonIgnore
    public boolean local_urlDirtyFlag;
    
    /**
     * MIME 类型
     */
    public String mimetype;

    @JsonIgnore
    public boolean mimetypeDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 优先级
     */
    public String priority;

    @JsonIgnore
    public boolean priorityDirtyFlag;
    
    /**
     * 资源字段
     */
    public String res_field;

    @JsonIgnore
    public boolean res_fieldDirtyFlag;
    
    /**
     * 资源ID
     */
    public Integer res_id;

    @JsonIgnore
    public boolean res_idDirtyFlag;
    
    /**
     * 资源模型
     */
    public String res_model;

    @JsonIgnore
    public boolean res_modelDirtyFlag;
    
    /**
     * RES型号名称
     */
    public String res_model_name;

    @JsonIgnore
    public boolean res_model_nameDirtyFlag;
    
    /**
     * 资源名称
     */
    public String res_name;

    @JsonIgnore
    public boolean res_nameDirtyFlag;
    
    /**
     * 存储的文件名
     */
    public String store_fname;

    @JsonIgnore
    public boolean store_fnameDirtyFlag;
    
    /**
     * 主题模板
     */
    public Integer theme_template_id;

    @JsonIgnore
    public boolean theme_template_idDirtyFlag;
    
    /**
     * 略所图
     */
    public byte[] thumbnail;

    @JsonIgnore
    public boolean thumbnailDirtyFlag;
    
    /**
     * 类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * Url网址
     */
    public String url;

    @JsonIgnore
    public boolean urlDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 网站网址
     */
    public String website_url;

    @JsonIgnore
    public boolean website_urlDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [访问令牌]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return this.access_token ;
    }

    /**
     * 设置 [访问令牌]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

     /**
     * 获取 [访问令牌]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return this.access_tokenDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [校验和/SHA1]
     */
    @JsonProperty("checksum")
    public String getChecksum(){
        return this.checksum ;
    }

    /**
     * 设置 [校验和/SHA1]
     */
    @JsonProperty("checksum")
    public void setChecksum(String  checksum){
        this.checksum = checksum ;
        this.checksumDirtyFlag = true ;
    }

     /**
     * 获取 [校验和/SHA1]脏标记
     */
    @JsonIgnore
    public boolean getChecksumDirtyFlag(){
        return this.checksumDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [文件内容]
     */
    @JsonProperty("datas")
    public byte[] getDatas(){
        return this.datas ;
    }

    /**
     * 设置 [文件内容]
     */
    @JsonProperty("datas")
    public void setDatas(byte[]  datas){
        this.datas = datas ;
        this.datasDirtyFlag = true ;
    }

     /**
     * 获取 [文件内容]脏标记
     */
    @JsonIgnore
    public boolean getDatasDirtyFlag(){
        return this.datasDirtyFlag ;
    }   

    /**
     * 获取 [文件名]
     */
    @JsonProperty("datas_fname")
    public String getDatas_fname(){
        return this.datas_fname ;
    }

    /**
     * 设置 [文件名]
     */
    @JsonProperty("datas_fname")
    public void setDatas_fname(String  datas_fname){
        this.datas_fname = datas_fname ;
        this.datas_fnameDirtyFlag = true ;
    }

     /**
     * 获取 [文件名]脏标记
     */
    @JsonIgnore
    public boolean getDatas_fnameDirtyFlag(){
        return this.datas_fnameDirtyFlag ;
    }   

    /**
     * 获取 [数据库数据]
     */
    @JsonProperty("db_datas")
    public byte[] getDb_datas(){
        return this.db_datas ;
    }

    /**
     * 设置 [数据库数据]
     */
    @JsonProperty("db_datas")
    public void setDb_datas(byte[]  db_datas){
        this.db_datas = db_datas ;
        this.db_datasDirtyFlag = true ;
    }

     /**
     * 获取 [数据库数据]脏标记
     */
    @JsonIgnore
    public boolean getDb_datasDirtyFlag(){
        return this.db_datasDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [文件大小]
     */
    @JsonProperty("file_size")
    public Integer getFile_size(){
        return this.file_size ;
    }

    /**
     * 设置 [文件大小]
     */
    @JsonProperty("file_size")
    public void setFile_size(Integer  file_size){
        this.file_size = file_size ;
        this.file_sizeDirtyFlag = true ;
    }

     /**
     * 获取 [文件大小]脏标记
     */
    @JsonIgnore
    public boolean getFile_sizeDirtyFlag(){
        return this.file_sizeDirtyFlag ;
    }   

    /**
     * 获取 [是公开文档]
     */
    @JsonProperty("ibizpublic")
    public String getIbizpublic(){
        return this.ibizpublic ;
    }

    /**
     * 设置 [是公开文档]
     */
    @JsonProperty("ibizpublic")
    public void setIbizpublic(String  ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.ibizpublicDirtyFlag = true ;
    }

     /**
     * 获取 [是公开文档]脏标记
     */
    @JsonIgnore
    public boolean getIbizpublicDirtyFlag(){
        return this.ibizpublicDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [索引的内容]
     */
    @JsonProperty("index_content")
    public String getIndex_content(){
        return this.index_content ;
    }

    /**
     * 设置 [索引的内容]
     */
    @JsonProperty("index_content")
    public void setIndex_content(String  index_content){
        this.index_content = index_content ;
        this.index_contentDirtyFlag = true ;
    }

     /**
     * 获取 [索引的内容]脏标记
     */
    @JsonIgnore
    public boolean getIndex_contentDirtyFlag(){
        return this.index_contentDirtyFlag ;
    }   

    /**
     * 获取 [相关的附件]
     */
    @JsonProperty("ir_attachment_id")
    public Integer getIr_attachment_id(){
        return this.ir_attachment_id ;
    }

    /**
     * 设置 [相关的附件]
     */
    @JsonProperty("ir_attachment_id")
    public void setIr_attachment_id(Integer  ir_attachment_id){
        this.ir_attachment_id = ir_attachment_id ;
        this.ir_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [相关的附件]脏标记
     */
    @JsonIgnore
    public boolean getIr_attachment_idDirtyFlag(){
        return this.ir_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [键]
     */
    @JsonProperty("key")
    public String getKey(){
        return this.key ;
    }

    /**
     * 设置 [键]
     */
    @JsonProperty("key")
    public void setKey(String  key){
        this.key = key ;
        this.keyDirtyFlag = true ;
    }

     /**
     * 获取 [键]脏标记
     */
    @JsonIgnore
    public boolean getKeyDirtyFlag(){
        return this.keyDirtyFlag ;
    }   

    /**
     * 获取 [附件网址]
     */
    @JsonProperty("local_url")
    public String getLocal_url(){
        return this.local_url ;
    }

    /**
     * 设置 [附件网址]
     */
    @JsonProperty("local_url")
    public void setLocal_url(String  local_url){
        this.local_url = local_url ;
        this.local_urlDirtyFlag = true ;
    }

     /**
     * 获取 [附件网址]脏标记
     */
    @JsonIgnore
    public boolean getLocal_urlDirtyFlag(){
        return this.local_urlDirtyFlag ;
    }   

    /**
     * 获取 [MIME 类型]
     */
    @JsonProperty("mimetype")
    public String getMimetype(){
        return this.mimetype ;
    }

    /**
     * 设置 [MIME 类型]
     */
    @JsonProperty("mimetype")
    public void setMimetype(String  mimetype){
        this.mimetype = mimetype ;
        this.mimetypeDirtyFlag = true ;
    }

     /**
     * 获取 [MIME 类型]脏标记
     */
    @JsonIgnore
    public boolean getMimetypeDirtyFlag(){
        return this.mimetypeDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [优先级]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return this.priority ;
    }

    /**
     * 设置 [优先级]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

     /**
     * 获取 [优先级]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return this.priorityDirtyFlag ;
    }   

    /**
     * 获取 [资源字段]
     */
    @JsonProperty("res_field")
    public String getRes_field(){
        return this.res_field ;
    }

    /**
     * 设置 [资源字段]
     */
    @JsonProperty("res_field")
    public void setRes_field(String  res_field){
        this.res_field = res_field ;
        this.res_fieldDirtyFlag = true ;
    }

     /**
     * 获取 [资源字段]脏标记
     */
    @JsonIgnore
    public boolean getRes_fieldDirtyFlag(){
        return this.res_fieldDirtyFlag ;
    }   

    /**
     * 获取 [资源ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [资源ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

     /**
     * 获取 [资源ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }   

    /**
     * 获取 [资源模型]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return this.res_model ;
    }

    /**
     * 设置 [资源模型]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

     /**
     * 获取 [资源模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return this.res_modelDirtyFlag ;
    }   

    /**
     * 获取 [RES型号名称]
     */
    @JsonProperty("res_model_name")
    public String getRes_model_name(){
        return this.res_model_name ;
    }

    /**
     * 设置 [RES型号名称]
     */
    @JsonProperty("res_model_name")
    public void setRes_model_name(String  res_model_name){
        this.res_model_name = res_model_name ;
        this.res_model_nameDirtyFlag = true ;
    }

     /**
     * 获取 [RES型号名称]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_nameDirtyFlag(){
        return this.res_model_nameDirtyFlag ;
    }   

    /**
     * 获取 [资源名称]
     */
    @JsonProperty("res_name")
    public String getRes_name(){
        return this.res_name ;
    }

    /**
     * 设置 [资源名称]
     */
    @JsonProperty("res_name")
    public void setRes_name(String  res_name){
        this.res_name = res_name ;
        this.res_nameDirtyFlag = true ;
    }

     /**
     * 获取 [资源名称]脏标记
     */
    @JsonIgnore
    public boolean getRes_nameDirtyFlag(){
        return this.res_nameDirtyFlag ;
    }   

    /**
     * 获取 [存储的文件名]
     */
    @JsonProperty("store_fname")
    public String getStore_fname(){
        return this.store_fname ;
    }

    /**
     * 设置 [存储的文件名]
     */
    @JsonProperty("store_fname")
    public void setStore_fname(String  store_fname){
        this.store_fname = store_fname ;
        this.store_fnameDirtyFlag = true ;
    }

     /**
     * 获取 [存储的文件名]脏标记
     */
    @JsonIgnore
    public boolean getStore_fnameDirtyFlag(){
        return this.store_fnameDirtyFlag ;
    }   

    /**
     * 获取 [主题模板]
     */
    @JsonProperty("theme_template_id")
    public Integer getTheme_template_id(){
        return this.theme_template_id ;
    }

    /**
     * 设置 [主题模板]
     */
    @JsonProperty("theme_template_id")
    public void setTheme_template_id(Integer  theme_template_id){
        this.theme_template_id = theme_template_id ;
        this.theme_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [主题模板]脏标记
     */
    @JsonIgnore
    public boolean getTheme_template_idDirtyFlag(){
        return this.theme_template_idDirtyFlag ;
    }   

    /**
     * 获取 [略所图]
     */
    @JsonProperty("thumbnail")
    public byte[] getThumbnail(){
        return this.thumbnail ;
    }

    /**
     * 设置 [略所图]
     */
    @JsonProperty("thumbnail")
    public void setThumbnail(byte[]  thumbnail){
        this.thumbnail = thumbnail ;
        this.thumbnailDirtyFlag = true ;
    }

     /**
     * 获取 [略所图]脏标记
     */
    @JsonIgnore
    public boolean getThumbnailDirtyFlag(){
        return this.thumbnailDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [Url网址]
     */
    @JsonProperty("url")
    public String getUrl(){
        return this.url ;
    }

    /**
     * 设置 [Url网址]
     */
    @JsonProperty("url")
    public void setUrl(String  url){
        this.url = url ;
        this.urlDirtyFlag = true ;
    }

     /**
     * 获取 [Url网址]脏标记
     */
    @JsonIgnore
    public boolean getUrlDirtyFlag(){
        return this.urlDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("access_token") instanceof Boolean)&& map.get("access_token")!=null){
			this.setAccess_token((String)map.get("access_token"));
		}
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("checksum") instanceof Boolean)&& map.get("checksum")!=null){
			this.setChecksum((String)map.get("checksum"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("datas") instanceof Boolean)&& map.get("datas")!=null){
			//暂时忽略
			//this.setDatas(((String)map.get("datas")).getBytes("UTF-8"));
		}
		if(!(map.get("datas_fname") instanceof Boolean)&& map.get("datas_fname")!=null){
			this.setDatas_fname((String)map.get("datas_fname"));
		}
		if(!(map.get("db_datas") instanceof Boolean)&& map.get("db_datas")!=null){
			//暂时忽略
			//this.setDb_datas(((String)map.get("db_datas")).getBytes("UTF-8"));
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("file_size") instanceof Boolean)&& map.get("file_size")!=null){
			this.setFile_size((Integer)map.get("file_size"));
		}
		if(map.get("ibizpublic") instanceof Boolean){
			this.setIbizpublic(((Boolean)map.get("public"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("index_content") instanceof Boolean)&& map.get("index_content")!=null){
			this.setIndex_content((String)map.get("index_content"));
		}
		if(!(map.get("ir_attachment_id") instanceof Boolean)&& map.get("ir_attachment_id")!=null){
			Object[] objs = (Object[])map.get("ir_attachment_id");
			if(objs.length > 0){
				this.setIr_attachment_id((Integer)objs[0]);
			}
		}
		if(!(map.get("key") instanceof Boolean)&& map.get("key")!=null){
			this.setKey((String)map.get("key"));
		}
		if(!(map.get("local_url") instanceof Boolean)&& map.get("local_url")!=null){
			this.setLocal_url((String)map.get("local_url"));
		}
		if(!(map.get("mimetype") instanceof Boolean)&& map.get("mimetype")!=null){
			this.setMimetype((String)map.get("mimetype"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("priority") instanceof Boolean)&& map.get("priority")!=null){
			this.setPriority((String)map.get("priority"));
		}
		if(!(map.get("res_field") instanceof Boolean)&& map.get("res_field")!=null){
			this.setRes_field((String)map.get("res_field"));
		}
		if(!(map.get("res_id") instanceof Boolean)&& map.get("res_id")!=null){
			this.setRes_id((Integer)map.get("res_id"));
		}
		if(!(map.get("res_model") instanceof Boolean)&& map.get("res_model")!=null){
			this.setRes_model((String)map.get("res_model"));
		}
		if(!(map.get("res_model_name") instanceof Boolean)&& map.get("res_model_name")!=null){
			this.setRes_model_name((String)map.get("res_model_name"));
		}
		if(!(map.get("res_name") instanceof Boolean)&& map.get("res_name")!=null){
			this.setRes_name((String)map.get("res_name"));
		}
		if(!(map.get("store_fname") instanceof Boolean)&& map.get("store_fname")!=null){
			this.setStore_fname((String)map.get("store_fname"));
		}
		if(!(map.get("theme_template_id") instanceof Boolean)&& map.get("theme_template_id")!=null){
			Object[] objs = (Object[])map.get("theme_template_id");
			if(objs.length > 0){
				this.setTheme_template_id((Integer)objs[0]);
			}
		}
		if(!(map.get("thumbnail") instanceof Boolean)&& map.get("thumbnail")!=null){
			//暂时忽略
			//this.setThumbnail(((String)map.get("thumbnail")).getBytes("UTF-8"));
		}
		if(!(map.get("type") instanceof Boolean)&& map.get("type")!=null){
			this.setType((String)map.get("type"));
		}
		if(!(map.get("url") instanceof Boolean)&& map.get("url")!=null){
			this.setUrl((String)map.get("url"));
		}
		if(!(map.get("website_id") instanceof Boolean)&& map.get("website_id")!=null){
			Object[] objs = (Object[])map.get("website_id");
			if(objs.length > 0){
				this.setWebsite_id((Integer)objs[0]);
			}
		}
		if(!(map.get("website_url") instanceof Boolean)&& map.get("website_url")!=null){
			this.setWebsite_url((String)map.get("website_url"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccess_token()!=null&&this.getAccess_tokenDirtyFlag()){
			map.put("access_token",this.getAccess_token());
		}else if(this.getAccess_tokenDirtyFlag()){
			map.put("access_token",false);
		}
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getChecksum()!=null&&this.getChecksumDirtyFlag()){
			map.put("checksum",this.getChecksum());
		}else if(this.getChecksumDirtyFlag()){
			map.put("checksum",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDatas()!=null&&this.getDatasDirtyFlag()){
			//暂不支持binary类型datas
		}else if(this.getDatasDirtyFlag()){
			map.put("datas",false);
		}
		if(this.getDatas_fname()!=null&&this.getDatas_fnameDirtyFlag()){
			map.put("datas_fname",this.getDatas_fname());
		}else if(this.getDatas_fnameDirtyFlag()){
			map.put("datas_fname",false);
		}
		if(this.getDb_datas()!=null&&this.getDb_datasDirtyFlag()){
			//暂不支持binary类型db_datas
		}else if(this.getDb_datasDirtyFlag()){
			map.put("db_datas",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFile_size()!=null&&this.getFile_sizeDirtyFlag()){
			map.put("file_size",this.getFile_size());
		}else if(this.getFile_sizeDirtyFlag()){
			map.put("file_size",false);
		}
		if(this.getIbizpublic()!=null&&this.getIbizpublicDirtyFlag()){
			map.put("public",Boolean.parseBoolean(this.getIbizpublic()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIndex_content()!=null&&this.getIndex_contentDirtyFlag()){
			map.put("index_content",this.getIndex_content());
		}else if(this.getIndex_contentDirtyFlag()){
			map.put("index_content",false);
		}
		if(this.getIr_attachment_id()!=null&&this.getIr_attachment_idDirtyFlag()){
			map.put("ir_attachment_id",this.getIr_attachment_id());
		}else if(this.getIr_attachment_idDirtyFlag()){
			map.put("ir_attachment_id",false);
		}
		if(this.getKey()!=null&&this.getKeyDirtyFlag()){
			map.put("key",this.getKey());
		}else if(this.getKeyDirtyFlag()){
			map.put("key",false);
		}
		if(this.getLocal_url()!=null&&this.getLocal_urlDirtyFlag()){
			map.put("local_url",this.getLocal_url());
		}else if(this.getLocal_urlDirtyFlag()){
			map.put("local_url",false);
		}
		if(this.getMimetype()!=null&&this.getMimetypeDirtyFlag()){
			map.put("mimetype",this.getMimetype());
		}else if(this.getMimetypeDirtyFlag()){
			map.put("mimetype",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPriority()!=null&&this.getPriorityDirtyFlag()){
			map.put("priority",this.getPriority());
		}else if(this.getPriorityDirtyFlag()){
			map.put("priority",false);
		}
		if(this.getRes_field()!=null&&this.getRes_fieldDirtyFlag()){
			map.put("res_field",this.getRes_field());
		}else if(this.getRes_fieldDirtyFlag()){
			map.put("res_field",false);
		}
		if(this.getRes_id()!=null&&this.getRes_idDirtyFlag()){
			map.put("res_id",this.getRes_id());
		}else if(this.getRes_idDirtyFlag()){
			map.put("res_id",false);
		}
		if(this.getRes_model()!=null&&this.getRes_modelDirtyFlag()){
			map.put("res_model",this.getRes_model());
		}else if(this.getRes_modelDirtyFlag()){
			map.put("res_model",false);
		}
		if(this.getRes_model_name()!=null&&this.getRes_model_nameDirtyFlag()){
			map.put("res_model_name",this.getRes_model_name());
		}else if(this.getRes_model_nameDirtyFlag()){
			map.put("res_model_name",false);
		}
		if(this.getRes_name()!=null&&this.getRes_nameDirtyFlag()){
			map.put("res_name",this.getRes_name());
		}else if(this.getRes_nameDirtyFlag()){
			map.put("res_name",false);
		}
		if(this.getStore_fname()!=null&&this.getStore_fnameDirtyFlag()){
			map.put("store_fname",this.getStore_fname());
		}else if(this.getStore_fnameDirtyFlag()){
			map.put("store_fname",false);
		}
		if(this.getTheme_template_id()!=null&&this.getTheme_template_idDirtyFlag()){
			map.put("theme_template_id",this.getTheme_template_id());
		}else if(this.getTheme_template_idDirtyFlag()){
			map.put("theme_template_id",false);
		}
		if(this.getThumbnail()!=null&&this.getThumbnailDirtyFlag()){
			//暂不支持binary类型thumbnail
		}else if(this.getThumbnailDirtyFlag()){
			map.put("thumbnail",false);
		}
		if(this.getType()!=null&&this.getTypeDirtyFlag()){
			map.put("type",this.getType());
		}else if(this.getTypeDirtyFlag()){
			map.put("type",false);
		}
		if(this.getUrl()!=null&&this.getUrlDirtyFlag()){
			map.put("url",this.getUrl());
		}else if(this.getUrlDirtyFlag()){
			map.put("url",false);
		}
		if(this.getWebsite_id()!=null&&this.getWebsite_idDirtyFlag()){
			map.put("website_id",this.getWebsite_id());
		}else if(this.getWebsite_idDirtyFlag()){
			map.put("website_id",false);
		}
		if(this.getWebsite_url()!=null&&this.getWebsite_urlDirtyFlag()){
			map.put("website_url",this.getWebsite_url());
		}else if(this.getWebsite_urlDirtyFlag()){
			map.put("website_url",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
