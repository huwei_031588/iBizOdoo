package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenterSearchContext;

/**
 * 实体 [工作中心] 存储模型
 */
public interface Mrp_workcenter{

    /**
     * 每小时成本
     */
    Double getCosts_hour();

    void setCosts_hour(Double costs_hour);

    /**
     * 获取 [每小时成本]脏标记
     */
    boolean getCosts_hourDirtyFlag();

    /**
     * 工作中心负载
     */
    Double getWorkcenter_load();

    void setWorkcenter_load(Double workcenter_load);

    /**
     * 获取 [工作中心负载]脏标记
     */
    boolean getWorkcenter_loadDirtyFlag();

    /**
     * 工作中心状态
     */
    String getWorking_state();

    void setWorking_state(String working_state);

    /**
     * 获取 [工作中心状态]脏标记
     */
    boolean getWorking_stateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 延迟的订单合计
     */
    Integer getWorkorder_late_count();

    void setWorkorder_late_count(Integer workorder_late_count);

    /**
     * 获取 [延迟的订单合计]脏标记
     */
    boolean getWorkorder_late_countDirtyFlag();

    /**
     * 效能
     */
    Integer getPerformance();

    void setPerformance(Integer performance);

    /**
     * 获取 [效能]脏标记
     */
    boolean getPerformanceDirtyFlag();

    /**
     * OEE 目标
     */
    Double getOee_target();

    void setOee_target(Double oee_target);

    /**
     * 获取 [OEE 目标]脏标记
     */
    boolean getOee_targetDirtyFlag();

    /**
     * 容量
     */
    Double getCapacity();

    void setCapacity(Double capacity);

    /**
     * 获取 [容量]脏标记
     */
    boolean getCapacityDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * # 工单
     */
    Integer getWorkorder_count();

    void setWorkorder_count(Integer workorder_count);

    /**
     * 获取 [# 工单]脏标记
     */
    boolean getWorkorder_countDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 时间日志
     */
    String getTime_ids();

    void setTime_ids(String time_ids);

    /**
     * 获取 [时间日志]脏标记
     */
    boolean getTime_idsDirtyFlag();

    /**
     * 生产性时间
     */
    Double getProductive_time();

    void setProductive_time(Double productive_time);

    /**
     * 获取 [生产性时间]脏标记
     */
    boolean getProductive_timeDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 工艺行
     */
    String getRouting_line_ids();

    void setRouting_line_ids(String routing_line_ids);

    /**
     * 获取 [工艺行]脏标记
     */
    boolean getRouting_line_idsDirtyFlag();

    /**
     * OEE
     */
    Double getOee();

    void setOee(Double oee);

    /**
     * 获取 [OEE]脏标记
     */
    boolean getOeeDirtyFlag();

    /**
     * 说明
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 代码
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [代码]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 颜色
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 生产后时间
     */
    Double getTime_stop();

    void setTime_stop(Double time_stop);

    /**
     * 获取 [生产后时间]脏标记
     */
    boolean getTime_stopDirtyFlag();

    /**
     * 生产前时间
     */
    Double getTime_start();

    void setTime_start(Double time_start);

    /**
     * 获取 [生产前时间]脏标记
     */
    boolean getTime_startDirtyFlag();

    /**
     * 待定的订单合计
     */
    Integer getWorkorder_pending_count();

    void setWorkorder_pending_count(Integer workorder_pending_count);

    /**
     * 获取 [待定的订单合计]脏标记
     */
    boolean getWorkorder_pending_countDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 阻塞时间
     */
    Double getBlocked_time();

    void setBlocked_time(Double blocked_time);

    /**
     * 获取 [阻塞时间]脏标记
     */
    boolean getBlocked_timeDirtyFlag();

    /**
     * # 读取工单
     */
    Integer getWorkorder_ready_count();

    void setWorkorder_ready_count(Integer workorder_ready_count);

    /**
     * 获取 [# 读取工单]脏标记
     */
    boolean getWorkorder_ready_countDirtyFlag();

    /**
     * 运行的订单合计
     */
    Integer getWorkorder_progress_count();

    void setWorkorder_progress_count(Integer workorder_progress_count);

    /**
     * 获取 [运行的订单合计]脏标记
     */
    boolean getWorkorder_progress_countDirtyFlag();

    /**
     * 订单
     */
    String getOrder_ids();

    void setOrder_ids(String order_ids);

    /**
     * 获取 [订单]脏标记
     */
    boolean getOrder_idsDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 时区
     */
    String getTz();

    void setTz(String tz);

    /**
     * 获取 [时区]脏标记
     */
    boolean getTzDirtyFlag();

    /**
     * 时间效率
     */
    Double getTime_efficiency();

    void setTime_efficiency(Double time_efficiency);

    /**
     * 获取 [时间效率]脏标记
     */
    boolean getTime_efficiencyDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 工作时间
     */
    String getResource_calendar_id_text();

    void setResource_calendar_id_text(String resource_calendar_id_text);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getResource_calendar_id_textDirtyFlag();

    /**
     * 工作中心
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [工作中心]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 工作时间
     */
    Integer getResource_calendar_id();

    void setResource_calendar_id(Integer resource_calendar_id);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getResource_calendar_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 资源
     */
    Integer getResource_id();

    void setResource_id(Integer resource_id);

    /**
     * 获取 [资源]脏标记
     */
    boolean getResource_idDirtyFlag();

}
