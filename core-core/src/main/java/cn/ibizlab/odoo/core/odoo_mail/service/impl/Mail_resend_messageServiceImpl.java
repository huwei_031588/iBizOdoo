package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_messageSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_resend_messageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_resend_messageOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_resend_messageClientModel;

/**
 * 实体[EMail重发向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_resend_messageServiceImpl implements IMail_resend_messageService {

    @Autowired
    mail_resend_messageOdooClient mail_resend_messageOdooClient;


    @Override
    public Mail_resend_message get(Integer id) {
        mail_resend_messageClientModel clientModel = new mail_resend_messageClientModel();
        clientModel.setId(id);
		mail_resend_messageOdooClient.get(clientModel);
        Mail_resend_message et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_resend_message();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mail_resend_messageClientModel clientModel = new mail_resend_messageClientModel();
        clientModel.setId(id);
		mail_resend_messageOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mail_resend_message et) {
        mail_resend_messageClientModel clientModel = convert2Model(et,null);
		mail_resend_messageOdooClient.create(clientModel);
        Mail_resend_message rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_resend_message> list){
    }

    @Override
    public boolean update(Mail_resend_message et) {
        mail_resend_messageClientModel clientModel = convert2Model(et,null);
		mail_resend_messageOdooClient.update(clientModel);
        Mail_resend_message rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_resend_message> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_resend_message> searchDefault(Mail_resend_messageSearchContext context) {
        List<Mail_resend_message> list = new ArrayList<Mail_resend_message>();
        Page<mail_resend_messageClientModel> clientModelList = mail_resend_messageOdooClient.search(context);
        for(mail_resend_messageClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_resend_message>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_resend_messageClientModel convert2Model(Mail_resend_message domain , mail_resend_messageClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_resend_messageClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("notification_idsdirtyflag"))
                model.setNotification_ids(domain.getNotificationIds());
            if((Boolean) domain.getExtensionparams().get("has_canceldirtyflag"))
                model.setHas_cancel(domain.getHasCancel());
            if((Boolean) domain.getExtensionparams().get("partner_readonlydirtyflag"))
                model.setPartner_readonly(domain.getPartnerReadonly());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("partner_idsdirtyflag"))
                model.setPartner_ids(domain.getPartnerIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("mail_message_iddirtyflag"))
                model.setMail_message_id(domain.getMailMessageId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_resend_message convert2Domain( mail_resend_messageClientModel model ,Mail_resend_message domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_resend_message();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNotification_idsDirtyFlag())
            domain.setNotificationIds(model.getNotification_ids());
        if(model.getHas_cancelDirtyFlag())
            domain.setHasCancel(model.getHas_cancel());
        if(model.getPartner_readonlyDirtyFlag())
            domain.setPartnerReadonly(model.getPartner_readonly());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getPartner_idsDirtyFlag())
            domain.setPartnerIds(model.getPartner_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getMail_message_idDirtyFlag())
            domain.setMailMessageId(model.getMail_message_id());
        return domain ;
    }

}

    



