package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [付款] 对象
 */
@Data
public class Account_payment extends EntityClient implements Serializable {

    /**
     * 发票
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;

    /**
     * 需要采取行动
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 操作编号
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 付款类型
     */
    @DEField(name = "payment_type")
    @JSONField(name = "payment_type")
    @JsonProperty("payment_type")
    private String paymentType;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 有发票
     */
    @JSONField(name = "has_invoices")
    @JsonProperty("has_invoices")
    private String hasInvoices;

    /**
     * 付款日期
     */
    @DEField(name = "payment_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "payment_date" , format="yyyy-MM-dd")
    @JsonProperty("payment_date")
    private Timestamp paymentDate;

    /**
     * 多
     */
    @JSONField(name = "multi")
    @JsonProperty("multi")
    private String multi;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 凭证已核销
     */
    @JSONField(name = "move_reconciled")
    @JsonProperty("move_reconciled")
    private String moveReconciled;

    /**
     * 目标账户
     */
    @JSONField(name = "destination_account_id")
    @JsonProperty("destination_account_id")
    private Integer destinationAccountId;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 付款差异
     */
    @JSONField(name = "payment_difference")
    @JsonProperty("payment_difference")
    private Double paymentDifference;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 需要一个动作消息的编码
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 付款差异处理
     */
    @DEField(name = "payment_difference_handling")
    @JSONField(name = "payment_difference_handling")
    @JsonProperty("payment_difference_handling")
    private String paymentDifferenceHandling;

    /**
     * 已核销的发票
     */
    @JSONField(name = "reconciled_invoice_ids")
    @JsonProperty("reconciled_invoice_ids")
    private String reconciledInvoiceIds;

    /**
     * 付款参考
     */
    @DEField(name = "payment_reference")
    @JSONField(name = "payment_reference")
    @JsonProperty("payment_reference")
    private String paymentReference;

    /**
     * 日记账项目标签
     */
    @DEField(name = "writeoff_label")
    @JSONField(name = "writeoff_label")
    @JsonProperty("writeoff_label")
    private String writeoffLabel;

    /**
     * 分录行
     */
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;

    /**
     * 网站信息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 付款金额
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 显示合作伙伴银行账户
     */
    @JSONField(name = "show_partner_bank_account")
    @JsonProperty("show_partner_bank_account")
    private String showPartnerBankAccount;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 业务伙伴类型
     */
    @DEField(name = "partner_type")
    @JSONField(name = "partner_type")
    @JsonProperty("partner_type")
    private String partnerType;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 备忘
     */
    @JSONField(name = "communication")
    @JsonProperty("communication")
    private String communication;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 是关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 隐藏付款方式
     */
    @JSONField(name = "hide_payment_method")
    @JsonProperty("hide_payment_method")
    private String hidePaymentMethod;

    /**
     * 日记账分录名称
     */
    @DEField(name = "move_name")
    @JSONField(name = "move_name")
    @JsonProperty("move_name")
    private String moveName;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 转账到
     */
    @JSONField(name = "destination_journal_id_text")
    @JsonProperty("destination_journal_id_text")
    private String destinationJournalIdText;

    /**
     * 代码
     */
    @JSONField(name = "payment_method_code")
    @JsonProperty("payment_method_code")
    private String paymentMethodCode;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 付款日记账
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 差异科目
     */
    @JSONField(name = "writeoff_account_id_text")
    @JsonProperty("writeoff_account_id_text")
    private String writeoffAccountIdText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 付款方法类型
     */
    @JSONField(name = "payment_method_id_text")
    @JsonProperty("payment_method_id_text")
    private String paymentMethodIdText;

    /**
     * 保存的付款令牌
     */
    @JSONField(name = "payment_token_id_text")
    @JsonProperty("payment_token_id_text")
    private String paymentTokenIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 业务伙伴
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 保存的付款令牌
     */
    @DEField(name = "payment_token_id")
    @JSONField(name = "payment_token_id")
    @JsonProperty("payment_token_id")
    private Integer paymentTokenId;

    /**
     * 付款交易
     */
    @DEField(name = "payment_transaction_id")
    @JSONField(name = "payment_transaction_id")
    @JsonProperty("payment_transaction_id")
    private Integer paymentTransactionId;

    /**
     * 差异科目
     */
    @DEField(name = "writeoff_account_id")
    @JSONField(name = "writeoff_account_id")
    @JsonProperty("writeoff_account_id")
    private Integer writeoffAccountId;

    /**
     * 收款银行账号
     */
    @DEField(name = "partner_bank_account_id")
    @JSONField(name = "partner_bank_account_id")
    @JsonProperty("partner_bank_account_id")
    private Integer partnerBankAccountId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 付款方法类型
     */
    @DEField(name = "payment_method_id")
    @JSONField(name = "payment_method_id")
    @JsonProperty("payment_method_id")
    private Integer paymentMethodId;

    /**
     * 转账到
     */
    @DEField(name = "destination_journal_id")
    @JSONField(name = "destination_journal_id")
    @JsonProperty("destination_journal_id")
    private Integer destinationJournalId;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 付款日记账
     */
    @DEField(name = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;


    /**
     * 
     */
    @JSONField(name = "odoowriteoffaccount")
    @JsonProperty("odoowriteoffaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooWriteoffAccount;

    /**
     * 
     */
    @JSONField(name = "odoodestinationjournal")
    @JsonProperty("odoodestinationjournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooDestinationJournal;

    /**
     * 
     */
    @JSONField(name = "odoojournal")
    @JsonProperty("odoojournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JSONField(name = "odoopaymentmethod")
    @JsonProperty("odoopaymentmethod")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method odooPaymentMethod;

    /**
     * 
     */
    @JSONField(name = "odoopaymenttoken")
    @JsonProperty("odoopaymenttoken")
    private cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token odooPaymentToken;

    /**
     * 
     */
    @JSONField(name = "odoopaymenttransaction")
    @JsonProperty("odoopaymenttransaction")
    private cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction odooPaymentTransaction;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoopartnerbankaccount")
    @JsonProperty("odoopartnerbankaccount")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank odooPartnerBankAccount;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [付款类型]
     */
    public void setPaymentType(String paymentType){
        this.paymentType = paymentType ;
        this.modify("payment_type",paymentType);
    }
    /**
     * 设置 [付款日期]
     */
    public void setPaymentDate(Timestamp paymentDate){
        this.paymentDate = paymentDate ;
        this.modify("payment_date",paymentDate);
    }
    /**
     * 设置 [多]
     */
    public void setMulti(String multi){
        this.multi = multi ;
        this.modify("multi",multi);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [付款差异处理]
     */
    public void setPaymentDifferenceHandling(String paymentDifferenceHandling){
        this.paymentDifferenceHandling = paymentDifferenceHandling ;
        this.modify("payment_difference_handling",paymentDifferenceHandling);
    }
    /**
     * 设置 [付款参考]
     */
    public void setPaymentReference(String paymentReference){
        this.paymentReference = paymentReference ;
        this.modify("payment_reference",paymentReference);
    }
    /**
     * 设置 [日记账项目标签]
     */
    public void setWriteoffLabel(String writeoffLabel){
        this.writeoffLabel = writeoffLabel ;
        this.modify("writeoff_label",writeoffLabel);
    }
    /**
     * 设置 [付款金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }
    /**
     * 设置 [业务伙伴类型]
     */
    public void setPartnerType(String partnerType){
        this.partnerType = partnerType ;
        this.modify("partner_type",partnerType);
    }
    /**
     * 设置 [备忘]
     */
    public void setCommunication(String communication){
        this.communication = communication ;
        this.modify("communication",communication);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [日记账分录名称]
     */
    public void setMoveName(String moveName){
        this.moveName = moveName ;
        this.modify("move_name",moveName);
    }
    /**
     * 设置 [保存的付款令牌]
     */
    public void setPaymentTokenId(Integer paymentTokenId){
        this.paymentTokenId = paymentTokenId ;
        this.modify("payment_token_id",paymentTokenId);
    }
    /**
     * 设置 [付款交易]
     */
    public void setPaymentTransactionId(Integer paymentTransactionId){
        this.paymentTransactionId = paymentTransactionId ;
        this.modify("payment_transaction_id",paymentTransactionId);
    }
    /**
     * 设置 [差异科目]
     */
    public void setWriteoffAccountId(Integer writeoffAccountId){
        this.writeoffAccountId = writeoffAccountId ;
        this.modify("writeoff_account_id",writeoffAccountId);
    }
    /**
     * 设置 [收款银行账号]
     */
    public void setPartnerBankAccountId(Integer partnerBankAccountId){
        this.partnerBankAccountId = partnerBankAccountId ;
        this.modify("partner_bank_account_id",partnerBankAccountId);
    }
    /**
     * 设置 [付款方法类型]
     */
    public void setPaymentMethodId(Integer paymentMethodId){
        this.paymentMethodId = paymentMethodId ;
        this.modify("payment_method_id",paymentMethodId);
    }
    /**
     * 设置 [转账到]
     */
    public void setDestinationJournalId(Integer destinationJournalId){
        this.destinationJournalId = destinationJournalId ;
        this.modify("destination_journal_id",destinationJournalId);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [付款日记账]
     */
    public void setJournalId(Integer journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

}


