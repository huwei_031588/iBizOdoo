package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_product_produce_line;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produce_lineSearchContext;

/**
 * 实体 [记录生产明细] 存储对象
 */
public interface Mrp_product_produce_lineRepository extends Repository<Mrp_product_produce_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_product_produce_line> searchDefault(Mrp_product_produce_lineSearchContext context);

    Mrp_product_produce_line convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce_line domain , Mrp_product_produce_line po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce_line convert2Domain( Mrp_product_produce_line po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce_line domain) ;

}
