package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.event_mail;

/**
 * 实体[event_mail] 服务对象接口
 */
public interface event_mailRepository{


    public event_mail createPO() ;
        public void removeBatch(String id);

        public void remove(String id);

        public void create(event_mail event_mail);

        public void createBatch(event_mail event_mail);

        public void updateBatch(event_mail event_mail);

        public List<event_mail> search();

        public void get(String id);

        public void update(event_mail event_mail);


}
