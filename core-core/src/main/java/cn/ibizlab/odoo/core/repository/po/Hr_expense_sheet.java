package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheetSearchContext;

/**
 * 实体 [费用报表] 存储模型
 */
public interface Hr_expense_sheet{

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 可重置
     */
    String getCan_reset();

    void setCan_reset(String can_reset);

    /**
     * 获取 [可重置]脏标记
     */
    boolean getCan_resetDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 附件数量
     */
    Integer getAttachment_number();

    void setAttachment_number(Integer attachment_number);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getAttachment_numberDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 处理行中不同币种
     */
    String getIs_multiple_currency();

    void setIs_multiple_currency(String is_multiple_currency);

    /**
     * 获取 [处理行中不同币种]脏标记
     */
    boolean getIs_multiple_currencyDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 信息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [信息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 支付
     */
    String getPayment_mode();

    void setPayment_mode(String payment_mode);

    /**
     * 获取 [支付]脏标记
     */
    boolean getPayment_modeDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 费用行
     */
    String getExpense_line_ids();

    void setExpense_line_ids(String expense_line_ids);

    /**
     * 获取 [费用行]脏标记
     */
    boolean getExpense_line_idsDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 日期
     */
    Timestamp getAccounting_date();

    void setAccounting_date(Timestamp accounting_date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getAccounting_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 金额总计
     */
    Double getTotal_amount();

    void setTotal_amount(Double total_amount);

    /**
     * 获取 [金额总计]脏标记
     */
    boolean getTotal_amountDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 费用报告摘要
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [费用报告摘要]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 费用日记账
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [费用日记账]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 经理
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [经理]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 部门
     */
    String getDepartment_id_text();

    void setDepartment_id_text(String department_id_text);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_id_textDirtyFlag();

    /**
     * 银行日记账
     */
    String getBank_journal_id_text();

    void setBank_journal_id_text(String bank_journal_id_text);

    /**
     * 获取 [银行日记账]脏标记
     */
    boolean getBank_journal_id_textDirtyFlag();

    /**
     * 员工
     */
    String getEmployee_id_text();

    void setEmployee_id_text(String employee_id_text);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 日记账分录
     */
    String getAccount_move_id_text();

    void setAccount_move_id_text(String account_move_id_text);

    /**
     * 获取 [日记账分录]脏标记
     */
    boolean getAccount_move_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 员工家庭地址
     */
    String getAddress_id_text();

    void setAddress_id_text(String address_id_text);

    /**
     * 获取 [员工家庭地址]脏标记
     */
    boolean getAddress_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 银行日记账
     */
    Integer getBank_journal_id();

    void setBank_journal_id(Integer bank_journal_id);

    /**
     * 获取 [银行日记账]脏标记
     */
    boolean getBank_journal_idDirtyFlag();

    /**
     * 日记账分录
     */
    Integer getAccount_move_id();

    void setAccount_move_id(Integer account_move_id);

    /**
     * 获取 [日记账分录]脏标记
     */
    boolean getAccount_move_idDirtyFlag();

    /**
     * 员工家庭地址
     */
    Integer getAddress_id();

    void setAddress_id(Integer address_id);

    /**
     * 获取 [员工家庭地址]脏标记
     */
    boolean getAddress_idDirtyFlag();

    /**
     * 经理
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [经理]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 员工
     */
    Integer getEmployee_id();

    void setEmployee_id(Integer employee_id);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 费用日记账
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [费用日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * 部门
     */
    Integer getDepartment_id();

    void setDepartment_id(Integer department_id);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

}
