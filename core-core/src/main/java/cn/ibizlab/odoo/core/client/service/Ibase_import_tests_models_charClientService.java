package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_char] 服务对象接口
 */
public interface Ibase_import_tests_models_charClientService{

    public Ibase_import_tests_models_char createModel() ;

    public void create(Ibase_import_tests_models_char base_import_tests_models_char);

    public void createBatch(List<Ibase_import_tests_models_char> base_import_tests_models_chars);

    public void remove(Ibase_import_tests_models_char base_import_tests_models_char);

    public void get(Ibase_import_tests_models_char base_import_tests_models_char);

    public void removeBatch(List<Ibase_import_tests_models_char> base_import_tests_models_chars);

    public void update(Ibase_import_tests_models_char base_import_tests_models_char);

    public void updateBatch(List<Ibase_import_tests_models_char> base_import_tests_models_chars);

    public Page<Ibase_import_tests_models_char> search(SearchContext context);

    public Page<Ibase_import_tests_models_char> select(SearchContext context);

}
