package cn.ibizlab.odoo.core.odoo_portal.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_share;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_shareSearchContext;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_shareService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_portal.client.portal_shareOdooClient;
import cn.ibizlab.odoo.core.odoo_portal.clientmodel.portal_shareClientModel;

/**
 * 实体[门户分享] 服务对象接口实现
 */
@Slf4j
@Service
public class Portal_shareServiceImpl implements IPortal_shareService {

    @Autowired
    portal_shareOdooClient portal_shareOdooClient;


    @Override
    public boolean update(Portal_share et) {
        portal_shareClientModel clientModel = convert2Model(et,null);
		portal_shareOdooClient.update(clientModel);
        Portal_share rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Portal_share> list){
    }

    @Override
    public Portal_share get(Integer id) {
        portal_shareClientModel clientModel = new portal_shareClientModel();
        clientModel.setId(id);
		portal_shareOdooClient.get(clientModel);
        Portal_share et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Portal_share();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Portal_share et) {
        portal_shareClientModel clientModel = convert2Model(et,null);
		portal_shareOdooClient.create(clientModel);
        Portal_share rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Portal_share> list){
    }

    @Override
    public boolean remove(Integer id) {
        portal_shareClientModel clientModel = new portal_shareClientModel();
        clientModel.setId(id);
		portal_shareOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Portal_share> searchDefault(Portal_shareSearchContext context) {
        List<Portal_share> list = new ArrayList<Portal_share>();
        Page<portal_shareClientModel> clientModelList = portal_shareOdooClient.search(context);
        for(portal_shareClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Portal_share>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public portal_shareClientModel convert2Model(Portal_share domain , portal_shareClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new portal_shareClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("share_linkdirtyflag"))
                model.setShare_link(domain.getShareLink());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("access_warningdirtyflag"))
                model.setAccess_warning(domain.getAccessWarning());
            if((Boolean) domain.getExtensionparams().get("partner_idsdirtyflag"))
                model.setPartner_ids(domain.getPartnerIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("res_modeldirtyflag"))
                model.setRes_model(domain.getResModel());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Portal_share convert2Domain( portal_shareClientModel model ,Portal_share domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Portal_share();
        }

        if(model.getShare_linkDirtyFlag())
            domain.setShareLink(model.getShare_link());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getAccess_warningDirtyFlag())
            domain.setAccessWarning(model.getAccess_warning());
        if(model.getPartner_idsDirtyFlag())
            domain.setPartnerIds(model.getPartner_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getRes_modelDirtyFlag())
            domain.setResModel(model.getRes_model());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



