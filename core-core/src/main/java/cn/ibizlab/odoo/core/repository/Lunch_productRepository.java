package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Lunch_product;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_productSearchContext;

/**
 * 实体 [工作餐产品] 存储对象
 */
public interface Lunch_productRepository extends Repository<Lunch_product> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Lunch_product> searchDefault(Lunch_productSearchContext context);

    Lunch_product convert2PO(cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product domain , Lunch_product po) ;

    cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product convert2Domain( Lunch_product po ,cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product domain) ;

}
