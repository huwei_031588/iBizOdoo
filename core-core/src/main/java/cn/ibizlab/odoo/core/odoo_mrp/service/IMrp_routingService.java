package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routingSearchContext;


/**
 * 实体[Mrp_routing] 服务对象接口
 */
public interface IMrp_routingService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mrp_routing get(Integer key) ;
    boolean create(Mrp_routing et) ;
    void createBatch(List<Mrp_routing> list) ;
    boolean update(Mrp_routing et) ;
    void updateBatch(List<Mrp_routing> list) ;
    Page<Mrp_routing> searchDefault(Mrp_routingSearchContext context) ;

}



