package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_public_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_public_categorySearchContext;

/**
 * 实体 [网站产品目录] 存储对象
 */
public interface Product_public_categoryRepository extends Repository<Product_public_category> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_public_category> searchDefault(Product_public_categorySearchContext context);

    Product_public_category convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category domain , Product_public_category po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category convert2Domain( Product_public_category po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category domain) ;

}
