package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mro_pm_meter_interval;

/**
 * 实体[mro_pm_meter_interval] 服务对象接口
 */
public interface mro_pm_meter_intervalRepository{


    public mro_pm_meter_interval createPO() ;
        public void update(mro_pm_meter_interval mro_pm_meter_interval);

        public void removeBatch(String id);

        public void remove(String id);

        public void createBatch(mro_pm_meter_interval mro_pm_meter_interval);

        public void create(mro_pm_meter_interval mro_pm_meter_interval);

        public List<mro_pm_meter_interval> search();

        public void get(String id);

        public void updateBatch(mro_pm_meter_interval mro_pm_meter_interval);


}
