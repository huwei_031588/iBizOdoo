package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [account_fiscal_position_template] 对象
 */
public interface Iaccount_fiscal_position_template {

    /**
     * 获取 [科目映射]
     */
    public void setAccount_ids(String account_ids);
    
    /**
     * 设置 [科目映射]
     */
    public String getAccount_ids();

    /**
     * 获取 [科目映射]脏标记
     */
    public boolean getAccount_idsDirtyFlag();
    /**
     * 获取 [自动检测]
     */
    public void setAuto_apply(String auto_apply);
    
    /**
     * 设置 [自动检测]
     */
    public String getAuto_apply();

    /**
     * 获取 [自动检测]脏标记
     */
    public boolean getAuto_applyDirtyFlag();
    /**
     * 获取 [表模板]
     */
    public void setChart_template_id(Integer chart_template_id);
    
    /**
     * 设置 [表模板]
     */
    public Integer getChart_template_id();

    /**
     * 获取 [表模板]脏标记
     */
    public boolean getChart_template_idDirtyFlag();
    /**
     * 获取 [表模板]
     */
    public void setChart_template_id_text(String chart_template_id_text);
    
    /**
     * 设置 [表模板]
     */
    public String getChart_template_id_text();

    /**
     * 获取 [表模板]脏标记
     */
    public boolean getChart_template_id_textDirtyFlag();
    /**
     * 获取 [国家群组]
     */
    public void setCountry_group_id(Integer country_group_id);
    
    /**
     * 设置 [国家群组]
     */
    public Integer getCountry_group_id();

    /**
     * 获取 [国家群组]脏标记
     */
    public boolean getCountry_group_idDirtyFlag();
    /**
     * 获取 [国家群组]
     */
    public void setCountry_group_id_text(String country_group_id_text);
    
    /**
     * 设置 [国家群组]
     */
    public String getCountry_group_id_text();

    /**
     * 获取 [国家群组]脏标记
     */
    public boolean getCountry_group_id_textDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setCountry_id(Integer country_id);
    
    /**
     * 设置 [国家]
     */
    public Integer getCountry_id();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getCountry_idDirtyFlag();
    /**
     * 获取 [国家]
     */
    public void setCountry_id_text(String country_id_text);
    
    /**
     * 设置 [国家]
     */
    public String getCountry_id_text();

    /**
     * 获取 [国家]脏标记
     */
    public boolean getCountry_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [税科目调整模版]
     */
    public void setName(String name);
    
    /**
     * 设置 [税科目调整模版]
     */
    public String getName();

    /**
     * 获取 [税科目调整模版]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [备注]
     */
    public void setNote(String note);
    
    /**
     * 设置 [备注]
     */
    public String getNote();

    /**
     * 获取 [备注]脏标记
     */
    public boolean getNoteDirtyFlag();
    /**
     * 获取 [序号]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [序号]
     */
    public Integer getSequence();

    /**
     * 获取 [序号]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [联邦政府]
     */
    public void setState_ids(String state_ids);
    
    /**
     * 设置 [联邦政府]
     */
    public String getState_ids();

    /**
     * 获取 [联邦政府]脏标记
     */
    public boolean getState_idsDirtyFlag();
    /**
     * 获取 [税映射]
     */
    public void setTax_ids(String tax_ids);
    
    /**
     * 设置 [税映射]
     */
    public String getTax_ids();

    /**
     * 获取 [税映射]脏标记
     */
    public boolean getTax_idsDirtyFlag();
    /**
     * 获取 [VAT必须]
     */
    public void setVat_required(String vat_required);
    
    /**
     * 设置 [VAT必须]
     */
    public String getVat_required();

    /**
     * 获取 [VAT必须]脏标记
     */
    public boolean getVat_requiredDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [邮编范围从]
     */
    public void setZip_from(Integer zip_from);
    
    /**
     * 设置 [邮编范围从]
     */
    public Integer getZip_from();

    /**
     * 获取 [邮编范围从]脏标记
     */
    public boolean getZip_fromDirtyFlag();
    /**
     * 获取 [邮编范围到]
     */
    public void setZip_to(Integer zip_to);
    
    /**
     * 设置 [邮编范围到]
     */
    public Integer getZip_to();

    /**
     * 获取 [邮编范围到]脏标记
     */
    public boolean getZip_toDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
