package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [sale_order_template_option] 对象
 */
public interface sale_order_template_option {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Double getDiscount();

    public void setDiscount(Double discount);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getName();

    public void setName(String name);

    public Double getPrice_unit();

    public void setPrice_unit(Double price_unit);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public Double getQuantity();

    public void setQuantity(Double quantity);

    public Integer getSale_order_template_id();

    public void setSale_order_template_id(Integer sale_order_template_id);

    public String getSale_order_template_id_text();

    public void setSale_order_template_id_text(String sale_order_template_id_text);

    public Integer getUom_id();

    public void setUom_id(Integer uom_id);

    public String getUom_id_text();

    public void setUom_id_text(String uom_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
