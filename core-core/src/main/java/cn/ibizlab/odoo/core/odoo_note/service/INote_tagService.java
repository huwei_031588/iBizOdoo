package cn.ibizlab.odoo.core.odoo_note.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_note.domain.Note_tag;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_tagSearchContext;


/**
 * 实体[Note_tag] 服务对象接口
 */
public interface INote_tagService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Note_tag et) ;
    void updateBatch(List<Note_tag> list) ;
    boolean create(Note_tag et) ;
    void createBatch(List<Note_tag> list) ;
    Note_tag get(Integer key) ;
    Page<Note_tag> searchDefault(Note_tagSearchContext context) ;

}



