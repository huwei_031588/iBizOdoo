package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_financial_year_opSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_financial_year_opService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_financial_year_opOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_financial_year_opClientModel;

/**
 * 实体[上个财政年的期初] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_financial_year_opServiceImpl implements IAccount_financial_year_opService {

    @Autowired
    account_financial_year_opOdooClient account_financial_year_opOdooClient;


    @Override
    public boolean update(Account_financial_year_op et) {
        account_financial_year_opClientModel clientModel = convert2Model(et,null);
		account_financial_year_opOdooClient.update(clientModel);
        Account_financial_year_op rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_financial_year_op> list){
    }

    @Override
    public boolean create(Account_financial_year_op et) {
        account_financial_year_opClientModel clientModel = convert2Model(et,null);
		account_financial_year_opOdooClient.create(clientModel);
        Account_financial_year_op rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_financial_year_op> list){
    }

    @Override
    public Account_financial_year_op get(Integer id) {
        account_financial_year_opClientModel clientModel = new account_financial_year_opClientModel();
        clientModel.setId(id);
		account_financial_year_opOdooClient.get(clientModel);
        Account_financial_year_op et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_financial_year_op();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_financial_year_opClientModel clientModel = new account_financial_year_opClientModel();
        clientModel.setId(id);
		account_financial_year_opOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_financial_year_op> searchDefault(Account_financial_year_opSearchContext context) {
        List<Account_financial_year_op> list = new ArrayList<Account_financial_year_op>();
        Page<account_financial_year_opClientModel> clientModelList = account_financial_year_opOdooClient.search(context);
        for(account_financial_year_opClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_financial_year_op>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_financial_year_opClientModel convert2Model(Account_financial_year_op domain , account_financial_year_opClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_financial_year_opClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("opening_move_posteddirtyflag"))
                model.setOpening_move_posted(domain.getOpeningMovePosted());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("fiscalyear_last_daydirtyflag"))
                model.setFiscalyear_last_day(domain.getFiscalyearLastDay());
            if((Boolean) domain.getExtensionparams().get("fiscalyear_last_monthdirtyflag"))
                model.setFiscalyear_last_month(domain.getFiscalyearLastMonth());
            if((Boolean) domain.getExtensionparams().get("opening_datedirtyflag"))
                model.setOpening_date(domain.getOpeningDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_financial_year_op convert2Domain( account_financial_year_opClientModel model ,Account_financial_year_op domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_financial_year_op();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getOpening_move_postedDirtyFlag())
            domain.setOpeningMovePosted(model.getOpening_move_posted());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getFiscalyear_last_dayDirtyFlag())
            domain.setFiscalyearLastDay(model.getFiscalyear_last_day());
        if(model.getFiscalyear_last_monthDirtyFlag())
            domain.setFiscalyearLastMonth(model.getFiscalyear_last_month());
        if(model.getOpening_dateDirtyFlag())
            domain.setOpeningDate(model.getOpening_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



