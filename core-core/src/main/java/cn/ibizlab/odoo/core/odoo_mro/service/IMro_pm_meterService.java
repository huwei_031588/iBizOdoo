package cn.ibizlab.odoo.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meterSearchContext;


/**
 * 实体[Mro_pm_meter] 服务对象接口
 */
public interface IMro_pm_meterService{

    boolean update(Mro_pm_meter et) ;
    void updateBatch(List<Mro_pm_meter> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mro_pm_meter get(Integer key) ;
    boolean create(Mro_pm_meter et) ;
    void createBatch(List<Mro_pm_meter> list) ;
    Page<Mro_pm_meter> searchDefault(Mro_pm_meterSearchContext context) ;

}



