package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_style;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_styleSearchContext;

/**
 * 实体 [产品风格] 存储对象
 */
public interface Product_styleRepository extends Repository<Product_style> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_style> searchDefault(Product_styleSearchContext context);

    Product_style convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_style domain , Product_style po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_style convert2Domain( Product_style po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_style domain) ;

}
