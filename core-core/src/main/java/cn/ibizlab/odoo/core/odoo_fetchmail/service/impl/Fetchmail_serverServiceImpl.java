package cn.ibizlab.odoo.core.odoo_fetchmail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.odoo.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;
import cn.ibizlab.odoo.core.odoo_fetchmail.service.IFetchmail_serverService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_fetchmail.client.fetchmail_serverOdooClient;
import cn.ibizlab.odoo.core.odoo_fetchmail.clientmodel.fetchmail_serverClientModel;

/**
 * 实体[Incoming Mail Server] 服务对象接口实现
 */
@Slf4j
@Service
public class Fetchmail_serverServiceImpl implements IFetchmail_serverService {

    @Autowired
    fetchmail_serverOdooClient fetchmail_serverOdooClient;


    @Override
    public Fetchmail_server get(Integer id) {
        fetchmail_serverClientModel clientModel = new fetchmail_serverClientModel();
        clientModel.setId(id);
		fetchmail_serverOdooClient.get(clientModel);
        Fetchmail_server et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Fetchmail_server();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Fetchmail_server et) {
        fetchmail_serverClientModel clientModel = convert2Model(et,null);
		fetchmail_serverOdooClient.update(clientModel);
        Fetchmail_server rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Fetchmail_server> list){
    }

    @Override
    public boolean create(Fetchmail_server et) {
        fetchmail_serverClientModel clientModel = convert2Model(et,null);
		fetchmail_serverOdooClient.create(clientModel);
        Fetchmail_server rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fetchmail_server> list){
    }

    @Override
    public boolean remove(Integer id) {
        fetchmail_serverClientModel clientModel = new fetchmail_serverClientModel();
        clientModel.setId(id);
		fetchmail_serverOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fetchmail_server> searchDefault(Fetchmail_serverSearchContext context) {
        List<Fetchmail_server> list = new ArrayList<Fetchmail_server>();
        Page<fetchmail_serverClientModel> clientModelList = fetchmail_serverOdooClient.search(context);
        for(fetchmail_serverClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Fetchmail_server>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public fetchmail_serverClientModel convert2Model(Fetchmail_server domain , fetchmail_serverClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new fetchmail_serverClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("userdirtyflag"))
                model.setUser(domain.getUser());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("scriptdirtyflag"))
                model.setScript(domain.getScript());
            if((Boolean) domain.getExtensionparams().get("configurationdirtyflag"))
                model.setConfiguration(domain.getConfiguration());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("passworddirtyflag"))
                model.setPassword(domain.getPassword());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("serverdirtyflag"))
                model.setServer(domain.getServer());
            if((Boolean) domain.getExtensionparams().get("portdirtyflag"))
                model.setPort(domain.getPort());
            if((Boolean) domain.getExtensionparams().get("is_ssldirtyflag"))
                model.setIs_ssl(domain.getIsSsl());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("prioritydirtyflag"))
                model.setPriority(domain.getPriority());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("object_iddirtyflag"))
                model.setObject_id(domain.getObjectId());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("attachdirtyflag"))
                model.setAttach(domain.getAttach());
            if((Boolean) domain.getExtensionparams().get("originaldirtyflag"))
                model.setOriginal(domain.getOriginal());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Fetchmail_server convert2Domain( fetchmail_serverClientModel model ,Fetchmail_server domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Fetchmail_server();
        }

        if(model.getUserDirtyFlag())
            domain.setUser(model.getUser());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getScriptDirtyFlag())
            domain.setScript(model.getScript());
        if(model.getConfigurationDirtyFlag())
            domain.setConfiguration(model.getConfiguration());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getPasswordDirtyFlag())
            domain.setPassword(model.getPassword());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getServerDirtyFlag())
            domain.setServer(model.getServer());
        if(model.getPortDirtyFlag())
            domain.setPort(model.getPort());
        if(model.getIs_sslDirtyFlag())
            domain.setIsSsl(model.getIs_ssl());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getPriorityDirtyFlag())
            domain.setPriority(model.getPriority());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getObject_idDirtyFlag())
            domain.setObjectId(model.getObject_id());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAttachDirtyFlag())
            domain.setAttach(model.getAttach());
        if(model.getOriginalDirtyFlag())
            domain.setOriginal(model.getOriginal());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



