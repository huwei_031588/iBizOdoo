package cn.ibizlab.odoo.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_definition;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;


/**
 * 实体[Gamification_goal_definition] 服务对象接口
 */
public interface IGamification_goal_definitionService{

    Gamification_goal_definition get(Integer key) ;
    boolean update(Gamification_goal_definition et) ;
    void updateBatch(List<Gamification_goal_definition> list) ;
    boolean create(Gamification_goal_definition et) ;
    void createBatch(List<Gamification_goal_definition> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Gamification_goal_definition> searchDefault(Gamification_goal_definitionSearchContext context) ;

}



