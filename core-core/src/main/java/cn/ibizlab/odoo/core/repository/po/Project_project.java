package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_project.filter.Project_projectSearchContext;

/**
 * 实体 [项目] 存储模型
 */
public interface Project_project{

    /**
     * 访问警告
     */
    String getAccess_warning();

    void setAccess_warning(String access_warning);

    /**
     * 获取 [访问警告]脏标记
     */
    boolean getAccess_warningDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 过期日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [过期日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 任务活动
     */
    String getTasks();

    void setTasks(String tasks);

    /**
     * 获取 [任务活动]脏标记
     */
    boolean getTasksDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 好 % 任务
     */
    Integer getPercentage_satisfaction_task();

    void setPercentage_satisfaction_task(Integer percentage_satisfaction_task);

    /**
     * 获取 [好 % 任务]脏标记
     */
    boolean getPercentage_satisfaction_taskDirtyFlag();

    /**
     * 门户访问网址
     */
    String getAccess_url();

    void setAccess_url(String access_url);

    /**
     * 获取 [门户访问网址]脏标记
     */
    boolean getAccess_urlDirtyFlag();

    /**
     * 任务阶段
     */
    String getType_ids();

    void setType_ids(String type_ids);

    /**
     * 获取 [任务阶段]脏标记
     */
    boolean getType_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 安全令牌
     */
    String getAccess_token();

    void setAccess_token(String access_token);

    /**
     * 获取 [安全令牌]脏标记
     */
    boolean getAccess_tokenDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 隐私
     */
    String getPrivacy_visibility();

    void setPrivacy_visibility(String privacy_visibility);

    /**
     * 获取 [隐私]脏标记
     */
    boolean getPrivacy_visibilityDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 点评频率
     */
    String getRating_status_period();

    void setRating_status_period(String rating_status_period);

    /**
     * 获取 [点评频率]脏标记
     */
    boolean getRating_status_periodDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 好 % 项目
     */
    Integer getPercentage_satisfaction_project();

    void setPercentage_satisfaction_project(Integer percentage_satisfaction_project);

    /**
     * 获取 [好 % 项目]脏标记
     */
    boolean getPercentage_satisfaction_projectDirtyFlag();

    /**
     * 附件数量
     */
    Integer getDoc_count();

    void setDoc_count(Integer doc_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getDoc_countDirtyFlag();

    /**
     * 会员
     */
    String getFavorite_user_ids();

    void setFavorite_user_ids(String favorite_user_ids);

    /**
     * 获取 [会员]脏标记
     */
    boolean getFavorite_user_idsDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 任务统计
     */
    Integer getTask_count();

    void setTask_count(Integer task_count);

    /**
     * 获取 [任务统计]脏标记
     */
    boolean getTask_countDirtyFlag();

    /**
     * 在仪表板显示项目
     */
    String getIs_favorite();

    void setIs_favorite(String is_favorite);

    /**
     * 获取 [在仪表板显示项目]脏标记
     */
    boolean getIs_favoriteDirtyFlag();

    /**
     * 公开评级
     */
    String getPortal_show_rating();

    void setPortal_show_rating(String portal_show_rating);

    /**
     * 获取 [公开评级]脏标记
     */
    boolean getPortal_show_ratingDirtyFlag();

    /**
     * 评级请求截止日期
     */
    Timestamp getRating_request_deadline();

    void setRating_request_deadline(Timestamp rating_request_deadline);

    /**
     * 获取 [评级请求截止日期]脏标记
     */
    boolean getRating_request_deadlineDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 客户点评
     */
    String getRating_status();

    void setRating_status(String rating_status);

    /**
     * 获取 [客户点评]脏标记
     */
    boolean getRating_statusDirtyFlag();

    /**
     * 用任务来
     */
    String getLabel_tasks();

    void setLabel_tasks(String label_tasks);

    /**
     * 获取 [用任务来]脏标记
     */
    boolean getLabel_tasksDirtyFlag();

    /**
     * 任务
     */
    String getTask_ids();

    void setTask_ids(String task_ids);

    /**
     * 获取 [任务]脏标记
     */
    boolean getTask_idsDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_start();

    void setDate_start(Timestamp date_start);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_startDirtyFlag();

    /**
     * 客户
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 上级记录线程ID
     */
    Integer getAlias_parent_thread_id();

    void setAlias_parent_thread_id(Integer alias_parent_thread_id);

    /**
     * 获取 [上级记录线程ID]脏标记
     */
    boolean getAlias_parent_thread_idDirtyFlag();

    /**
     * 记录线索ID
     */
    Integer getAlias_force_thread_id();

    void setAlias_force_thread_id(Integer alias_force_thread_id);

    /**
     * 获取 [记录线索ID]脏标记
     */
    boolean getAlias_force_thread_idDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 别名网域
     */
    String getAlias_domain();

    void setAlias_domain(String alias_domain);

    /**
     * 获取 [别名网域]脏标记
     */
    boolean getAlias_domainDirtyFlag();

    /**
     * 别名联系人安全
     */
    String getAlias_contact();

    void setAlias_contact(String alias_contact);

    /**
     * 获取 [别名联系人安全]脏标记
     */
    boolean getAlias_contactDirtyFlag();

    /**
     * 上级模型
     */
    Integer getAlias_parent_model_id();

    void setAlias_parent_model_id(Integer alias_parent_model_id);

    /**
     * 获取 [上级模型]脏标记
     */
    boolean getAlias_parent_model_idDirtyFlag();

    /**
     * 别名的模型
     */
    Integer getAlias_model_id();

    void setAlias_model_id(Integer alias_model_id);

    /**
     * 获取 [别名的模型]脏标记
     */
    boolean getAlias_model_idDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 所有者
     */
    Integer getAlias_user_id();

    void setAlias_user_id(Integer alias_user_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getAlias_user_idDirtyFlag();

    /**
     * 默认值
     */
    String getAlias_defaults();

    void setAlias_defaults(String alias_defaults);

    /**
     * 获取 [默认值]脏标记
     */
    boolean getAlias_defaultsDirtyFlag();

    /**
     * 工作时间
     */
    String getResource_calendar_id_text();

    void setResource_calendar_id_text(String resource_calendar_id_text);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getResource_calendar_id_textDirtyFlag();

    /**
     * 子任务项目
     */
    String getSubtask_project_id_text();

    void setSubtask_project_id_text(String subtask_project_id_text);

    /**
     * 获取 [子任务项目]脏标记
     */
    boolean getSubtask_project_id_textDirtyFlag();

    /**
     * 别名
     */
    String getAlias_name();

    void setAlias_name(String alias_name);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_nameDirtyFlag();

    /**
     * 项目管理员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [项目管理员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 项目管理员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [项目管理员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 客户
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 子任务项目
     */
    Integer getSubtask_project_id();

    void setSubtask_project_id(Integer subtask_project_id);

    /**
     * 获取 [子任务项目]脏标记
     */
    boolean getSubtask_project_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 工作时间
     */
    Integer getResource_calendar_id();

    void setResource_calendar_id(Integer resource_calendar_id);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getResource_calendar_idDirtyFlag();

    /**
     * 别名
     */
    Integer getAlias_id();

    void setAlias_id(Integer alias_id);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_idDirtyFlag();

}
