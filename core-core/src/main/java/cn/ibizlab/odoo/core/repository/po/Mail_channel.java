package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channelSearchContext;

/**
 * 实体 [讨论频道] 存储模型
 */
public interface Mail_channel{

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 中等尺寸照片
     */
    byte[] getImage_medium();

    void setImage_medium(byte[] image_medium);

    /**
     * 获取 [中等尺寸照片]脏标记
     */
    boolean getImage_mediumDirtyFlag();

    /**
     * 管理EMail账户
     */
    Integer getModeration_count();

    void setModeration_count(Integer moderation_count);

    /**
     * 获取 [管理EMail账户]脏标记
     */
    boolean getModeration_countDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 自动通知
     */
    String getModeration_notify();

    void setModeration_notify(String moderation_notify);

    /**
     * 获取 [自动通知]脏标记
     */
    boolean getModeration_notifyDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 管理频道
     */
    String getModeration();

    void setModeration(String moderation);

    /**
     * 获取 [管理频道]脏标记
     */
    boolean getModerationDirtyFlag();

    /**
     * 管理员
     */
    String getIs_moderator();

    void setIs_moderator(String is_moderator);

    /**
     * 获取 [管理员]脏标记
     */
    boolean getIs_moderatorDirtyFlag();

    /**
     * 通知消息
     */
    String getModeration_notify_msg();

    void setModeration_notify_msg(String moderation_notify_msg);

    /**
     * 获取 [通知消息]脏标记
     */
    boolean getModeration_notify_msgDirtyFlag();

    /**
     * 管理EMail
     */
    String getModeration_ids();

    void setModeration_ids(String moderation_ids);

    /**
     * 获取 [管理EMail]脏标记
     */
    boolean getModeration_idsDirtyFlag();

    /**
     * 最新图像评级
     */
    byte[] getRating_last_image();

    void setRating_last_image(byte[] rating_last_image);

    /**
     * 获取 [最新图像评级]脏标记
     */
    boolean getRating_last_imageDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 向新用户发送订阅指南
     */
    String getModeration_guidelines();

    void setModeration_guidelines(String moderation_guidelines);

    /**
     * 获取 [向新用户发送订阅指南]脏标记
     */
    boolean getModeration_guidelinesDirtyFlag();

    /**
     * 最新反馈评级
     */
    String getRating_last_feedback();

    void setRating_last_feedback(String rating_last_feedback);

    /**
     * 获取 [最新反馈评级]脏标记
     */
    boolean getRating_last_feedbackDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 人力资源部门
     */
    String getSubscription_department_ids();

    void setSubscription_department_ids(String subscription_department_ids);

    /**
     * 获取 [人力资源部门]脏标记
     */
    boolean getSubscription_department_idsDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 隐私
     */
    String getIbizpublic();

    void setIbizpublic(String ibizpublic);

    /**
     * 获取 [隐私]脏标记
     */
    boolean getIbizpublicDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 评级
     */
    String getRating_ids();

    void setRating_ids(String rating_ids);

    /**
     * 获取 [评级]脏标记
     */
    boolean getRating_idsDirtyFlag();

    /**
     * 匿名用户姓名
     */
    String getAnonymous_name();

    void setAnonymous_name(String anonymous_name);

    /**
     * 获取 [匿名用户姓名]脏标记
     */
    boolean getAnonymous_nameDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 成员
     */
    String getIs_member();

    void setIs_member(String is_member);

    /**
     * 获取 [成员]脏标记
     */
    boolean getIs_memberDirtyFlag();

    /**
     * 已订阅
     */
    String getIs_subscribed();

    void setIs_subscribed(String is_subscribed);

    /**
     * 获取 [已订阅]脏标记
     */
    boolean getIs_subscribedDirtyFlag();

    /**
     * 方针
     */
    String getModeration_guidelines_msg();

    void setModeration_guidelines_msg(String moderation_guidelines_msg);

    /**
     * 获取 [方针]脏标记
     */
    boolean getModeration_guidelines_msgDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 照片
     */
    byte[] getImage();

    void setImage(byte[] image);

    /**
     * 获取 [照片]脏标记
     */
    boolean getImageDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 小尺寸照片
     */
    byte[] getImage_small();

    void setImage_small(byte[] image_small);

    /**
     * 获取 [小尺寸照片]脏标记
     */
    boolean getImage_smallDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 错误个数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误个数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 监听器
     */
    String getChannel_partner_ids();

    void setChannel_partner_ids(String channel_partner_ids);

    /**
     * 获取 [监听器]脏标记
     */
    boolean getChannel_partner_idsDirtyFlag();

    /**
     * 管理员
     */
    String getModerator_ids();

    void setModerator_ids(String moderator_ids);

    /**
     * 获取 [管理员]脏标记
     */
    boolean getModerator_idsDirtyFlag();

    /**
     * 自动订阅
     */
    String getGroup_ids();

    void setGroup_ids(String group_ids);

    /**
     * 获取 [自动订阅]脏标记
     */
    boolean getGroup_idsDirtyFlag();

    /**
     * 以邮件形式发送
     */
    String getEmail_send();

    void setEmail_send(String email_send);

    /**
     * 获取 [以邮件形式发送]脏标记
     */
    boolean getEmail_sendDirtyFlag();

    /**
     * 最近一次查阅
     */
    String getChannel_last_seen_partner_ids();

    void setChannel_last_seen_partner_ids(String channel_last_seen_partner_ids);

    /**
     * 获取 [最近一次查阅]脏标记
     */
    boolean getChannel_last_seen_partner_idsDirtyFlag();

    /**
     * 前置操作
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [前置操作]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * UUID
     */
    String getUuid();

    void setUuid(String uuid);

    /**
     * 获取 [UUID]脏标记
     */
    boolean getUuidDirtyFlag();

    /**
     * 评级数
     */
    Integer getRating_count();

    void setRating_count(Integer rating_count);

    /**
     * 获取 [评级数]脏标记
     */
    boolean getRating_countDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 操作次数
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [操作次数]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 渠道消息
     */
    String getChannel_message_ids();

    void setChannel_message_ids(String channel_message_ids);

    /**
     * 获取 [渠道消息]脏标记
     */
    boolean getChannel_message_idsDirtyFlag();

    /**
     * 渠道类型
     */
    String getChannel_type();

    void setChannel_type(String channel_type);

    /**
     * 获取 [渠道类型]脏标记
     */
    boolean getChannel_typeDirtyFlag();

    /**
     * 是聊天
     */
    String getIs_chat();

    void setIs_chat(String is_chat);

    /**
     * 获取 [是聊天]脏标记
     */
    boolean getIs_chatDirtyFlag();

    /**
     * 最新值评级
     */
    Double getRating_last_value();

    void setRating_last_value(Double rating_last_value);

    /**
     * 获取 [最新值评级]脏标记
     */
    boolean getRating_last_valueDirtyFlag();

    /**
     * 记录线索ID
     */
    Integer getAlias_force_thread_id();

    void setAlias_force_thread_id(Integer alias_force_thread_id);

    /**
     * 获取 [记录线索ID]脏标记
     */
    boolean getAlias_force_thread_idDirtyFlag();

    /**
     * 默认值
     */
    String getAlias_defaults();

    void setAlias_defaults(String alias_defaults);

    /**
     * 获取 [默认值]脏标记
     */
    boolean getAlias_defaultsDirtyFlag();

    /**
     * 所有者
     */
    Integer getAlias_user_id();

    void setAlias_user_id(Integer alias_user_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getAlias_user_idDirtyFlag();

    /**
     * 上级模型
     */
    Integer getAlias_parent_model_id();

    void setAlias_parent_model_id(Integer alias_parent_model_id);

    /**
     * 获取 [上级模型]脏标记
     */
    boolean getAlias_parent_model_idDirtyFlag();

    /**
     * 经授权的群组
     */
    String getGroup_public_id_text();

    void setGroup_public_id_text(String group_public_id_text);

    /**
     * 获取 [经授权的群组]脏标记
     */
    boolean getGroup_public_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 上级记录ID
     */
    Integer getAlias_parent_thread_id();

    void setAlias_parent_thread_id(Integer alias_parent_thread_id);

    /**
     * 获取 [上级记录ID]脏标记
     */
    boolean getAlias_parent_thread_idDirtyFlag();

    /**
     * 别名
     */
    String getAlias_name();

    void setAlias_name(String alias_name);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_nameDirtyFlag();

    /**
     * 模型别名
     */
    Integer getAlias_model_id();

    void setAlias_model_id(Integer alias_model_id);

    /**
     * 获取 [模型别名]脏标记
     */
    boolean getAlias_model_idDirtyFlag();

    /**
     * 渠道
     */
    String getLivechat_channel_id_text();

    void setLivechat_channel_id_text(String livechat_channel_id_text);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getLivechat_channel_id_textDirtyFlag();

    /**
     * 网域别名
     */
    String getAlias_domain();

    void setAlias_domain(String alias_domain);

    /**
     * 获取 [网域别名]脏标记
     */
    boolean getAlias_domainDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 安全联系人别名
     */
    String getAlias_contact();

    void setAlias_contact(String alias_contact);

    /**
     * 获取 [安全联系人别名]脏标记
     */
    boolean getAlias_contactDirtyFlag();

    /**
     * 经授权的群组
     */
    Integer getGroup_public_id();

    void setGroup_public_id(Integer group_public_id);

    /**
     * 获取 [经授权的群组]脏标记
     */
    boolean getGroup_public_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 别名
     */
    Integer getAlias_id();

    void setAlias_id(Integer alias_id);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_idDirtyFlag();

    /**
     * 渠道
     */
    Integer getLivechat_channel_id();

    void setLivechat_channel_id(Integer livechat_channel_id);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getLivechat_channel_idDirtyFlag();

}
