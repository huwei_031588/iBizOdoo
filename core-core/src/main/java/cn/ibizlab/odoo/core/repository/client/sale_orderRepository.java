package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.sale_order;

/**
 * 实体[sale_order] 服务对象接口
 */
public interface sale_orderRepository{


    public sale_order createPO() ;
        public void updateBatch(sale_order sale_order);

        public void update(sale_order sale_order);

        public void removeBatch(String id);

        public void create(sale_order sale_order);

        public List<sale_order> search();

        public void createBatch(sale_order sale_order);

        public void get(String id);

        public void remove(String id);


}
