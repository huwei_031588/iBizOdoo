package cn.ibizlab.odoo.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [语言] 对象
 */
@Data
public class Res_lang extends EntityClient implements Serializable {

    /**
     * 可翻译
     */
    @JSONField(name = "translatable")
    @JsonProperty("translatable")
    private String translatable;

    /**
     * 千位分隔符
     */
    @DEField(name = "thousands_sep")
    @JSONField(name = "thousands_sep")
    @JsonProperty("thousands_sep")
    private String thousandsSep;

    /**
     * ISO代码
     */
    @DEField(name = "iso_code")
    @JSONField(name = "iso_code")
    @JsonProperty("iso_code")
    private String isoCode;

    /**
     * 小数分割符
     */
    @DEField(name = "decimal_point")
    @JSONField(name = "decimal_point")
    @JsonProperty("decimal_point")
    private String decimalPoint;

    /**
     * 分割符格式
     */
    @JSONField(name = "grouping")
    @JsonProperty("grouping")
    private String grouping;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 方向
     */
    @JSONField(name = "direction")
    @JsonProperty("direction")
    private String direction;

    /**
     * 日期格式
     */
    @DEField(name = "date_format")
    @JSONField(name = "date_format")
    @JsonProperty("date_format")
    private String dateFormat;

    /**
     * 时间格式
     */
    @DEField(name = "time_format")
    @JSONField(name = "time_format")
    @JsonProperty("time_format")
    private String timeFormat;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 地区代码
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 一个星期的第一天是
     */
    @DEField(name = "week_start")
    @JSONField(name = "week_start")
    @JsonProperty("week_start")
    private String weekStart;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [可翻译]
     */
    public void setTranslatable(String translatable){
        this.translatable = translatable ;
        this.modify("translatable",translatable);
    }
    /**
     * 设置 [千位分隔符]
     */
    public void setThousandsSep(String thousandsSep){
        this.thousandsSep = thousandsSep ;
        this.modify("thousands_sep",thousandsSep);
    }
    /**
     * 设置 [ISO代码]
     */
    public void setIsoCode(String isoCode){
        this.isoCode = isoCode ;
        this.modify("iso_code",isoCode);
    }
    /**
     * 设置 [小数分割符]
     */
    public void setDecimalPoint(String decimalPoint){
        this.decimalPoint = decimalPoint ;
        this.modify("decimal_point",decimalPoint);
    }
    /**
     * 设置 [分割符格式]
     */
    public void setGrouping(String grouping){
        this.grouping = grouping ;
        this.modify("grouping",grouping);
    }
    /**
     * 设置 [方向]
     */
    public void setDirection(String direction){
        this.direction = direction ;
        this.modify("direction",direction);
    }
    /**
     * 设置 [日期格式]
     */
    public void setDateFormat(String dateFormat){
        this.dateFormat = dateFormat ;
        this.modify("date_format",dateFormat);
    }
    /**
     * 设置 [时间格式]
     */
    public void setTimeFormat(String timeFormat){
        this.timeFormat = timeFormat ;
        this.modify("time_format",timeFormat);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [地区代码]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }
    /**
     * 设置 [一个星期的第一天是]
     */
    public void setWeekStart(String weekStart){
        this.weekStart = weekStart ;
        this.modify("week_start",weekStart);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

}


