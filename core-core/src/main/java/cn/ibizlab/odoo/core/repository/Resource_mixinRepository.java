package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Resource_mixin;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_mixinSearchContext;

/**
 * 实体 [资源装饰] 存储对象
 */
public interface Resource_mixinRepository extends Repository<Resource_mixin> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Resource_mixin> searchDefault(Resource_mixinSearchContext context);

    Resource_mixin convert2PO(cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin domain , Resource_mixin po) ;

    cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin convert2Domain( Resource_mixin po ,cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin domain) ;

}
