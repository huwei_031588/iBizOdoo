package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [im_livechat_report_operator] 对象
 */
public interface im_livechat_report_operator {

    public Integer getChannel_id();

    public void setChannel_id(Integer channel_id);

    public String getChannel_id_text();

    public void setChannel_id_text(String channel_id_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Double getDuration();

    public void setDuration(Double duration);

    public Integer getId();

    public void setId(Integer id);

    public Integer getLivechat_channel_id();

    public void setLivechat_channel_id(Integer livechat_channel_id);

    public String getLivechat_channel_id_text();

    public void setLivechat_channel_id_text(String livechat_channel_id_text);

    public Integer getNbr_channel();

    public void setNbr_channel(Integer nbr_channel);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public Timestamp getStart_date();

    public void setStart_date(Timestamp start_date);

    public Double getTime_to_answer();

    public void setTime_to_answer(Double time_to_answer);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
