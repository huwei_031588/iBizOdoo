package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [im_livechat_channel] 对象
 */
public interface im_livechat_channel {

    public String getAre_you_inside();

    public void setAre_you_inside(String are_you_inside);

    public String getButton_text();

    public void setButton_text(String button_text);

    public String getChannel_ids();

    public void setChannel_ids(String channel_ids);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDefault_message();

    public void setDefault_message(String default_message);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public byte[] getImage();

    public void setImage(byte[] image);

    public byte[] getImage_medium();

    public void setImage_medium(byte[] image_medium);

    public byte[] getImage_small();

    public void setImage_small(byte[] image_small);

    public String getInput_placeholder();

    public void setInput_placeholder(String input_placeholder);

    public String getIs_published();

    public void setIs_published(String is_published);

    public String getName();

    public void setName(String name);

    public Integer getNbr_channel();

    public void setNbr_channel(Integer nbr_channel);

    public Integer getRating_percentage_satisfaction();

    public void setRating_percentage_satisfaction(Integer rating_percentage_satisfaction);

    public String getRule_ids();

    public void setRule_ids(String rule_ids);

    public String getScript_external();

    public void setScript_external(String script_external);

    public String getUser_ids();

    public void setUser_ids(String user_ids);

    public String getWebsite_description();

    public void setWebsite_description(String website_description);

    public String getWebsite_published();

    public void setWebsite_published(String website_published);

    public String getWebsite_url();

    public void setWebsite_url(String website_url);

    public String getWeb_page();

    public void setWeb_page(String web_page);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
