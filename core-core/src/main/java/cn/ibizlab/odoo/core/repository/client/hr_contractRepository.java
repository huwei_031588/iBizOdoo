package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_contract;

/**
 * 实体[hr_contract] 服务对象接口
 */
public interface hr_contractRepository{


    public hr_contract createPO() ;
        public void remove(String id);

        public void get(String id);

        public void createBatch(hr_contract hr_contract);

        public List<hr_contract> search();

        public void updateBatch(hr_contract hr_contract);

        public void create(hr_contract hr_contract);

        public void update(hr_contract hr_contract);

        public void removeBatch(String id);


}
