package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_recruitment_source;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;

/**
 * 实体 [应聘者来源] 存储对象
 */
public interface Hr_recruitment_sourceRepository extends Repository<Hr_recruitment_source> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_recruitment_source> searchDefault(Hr_recruitment_sourceSearchContext context);

    Hr_recruitment_source convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_source domain , Hr_recruitment_source po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_source convert2Domain( Hr_recruitment_source po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_source domain) ;

}
