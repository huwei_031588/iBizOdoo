package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_payment;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_payment] 服务对象接口
 */
public interface Iaccount_paymentClientService{

    public Iaccount_payment createModel() ;

    public Page<Iaccount_payment> search(SearchContext context);

    public void updateBatch(List<Iaccount_payment> account_payments);

    public void create(Iaccount_payment account_payment);

    public void update(Iaccount_payment account_payment);

    public void remove(Iaccount_payment account_payment);

    public void createBatch(List<Iaccount_payment> account_payments);

    public void removeBatch(List<Iaccount_payment> account_payments);

    public void get(Iaccount_payment account_payment);

    public Page<Iaccount_payment> select(SearchContext context);

}
