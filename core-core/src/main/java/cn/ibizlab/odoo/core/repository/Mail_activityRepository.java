package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_activity;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activitySearchContext;

/**
 * 实体 [活动] 存储对象
 */
public interface Mail_activityRepository extends Repository<Mail_activity> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_activity> searchDefault(Mail_activitySearchContext context);

    Mail_activity convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity domain , Mail_activity po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity convert2Domain( Mail_activity po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity domain) ;

}
