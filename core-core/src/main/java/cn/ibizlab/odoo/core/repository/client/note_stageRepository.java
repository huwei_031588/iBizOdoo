package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.note_stage;

/**
 * 实体[note_stage] 服务对象接口
 */
public interface note_stageRepository{


    public note_stage createPO() ;
        public void removeBatch(String id);

        public void get(String id);

        public void update(note_stage note_stage);

        public void create(note_stage note_stage);

        public void remove(String id);

        public void updateBatch(note_stage note_stage);

        public void createBatch(note_stage note_stage);

        public List<note_stage> search();


}
