package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_payment] 对象
 */
public class account_paymentClientModel implements Serializable{

    /**
     * 付款金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 备忘
     */
    public String communication;

    @JsonIgnore
    public boolean communicationDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 目标账户
     */
    public Integer destination_account_id;

    @JsonIgnore
    public boolean destination_account_idDirtyFlag;
    
    /**
     * 转账到
     */
    public Integer destination_journal_id;

    @JsonIgnore
    public boolean destination_journal_idDirtyFlag;
    
    /**
     * 转账到
     */
    public String destination_journal_id_text;

    @JsonIgnore
    public boolean destination_journal_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 有发票
     */
    public String has_invoices;

    @JsonIgnore
    public boolean has_invoicesDirtyFlag;
    
    /**
     * 隐藏付款方式
     */
    public String hide_payment_method;

    @JsonIgnore
    public boolean hide_payment_methodDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_ids;

    @JsonIgnore
    public boolean invoice_idsDirtyFlag;
    
    /**
     * 付款日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 付款日记账
     */
    public String journal_id_text;

    @JsonIgnore
    public boolean journal_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 需要一个动作消息的编码
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 操作编号
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 分录行
     */
    public String move_line_ids;

    @JsonIgnore
    public boolean move_line_idsDirtyFlag;
    
    /**
     * 日记账分录名称
     */
    public String move_name;

    @JsonIgnore
    public boolean move_nameDirtyFlag;
    
    /**
     * 凭证已核销
     */
    public String move_reconciled;

    @JsonIgnore
    public boolean move_reconciledDirtyFlag;
    
    /**
     * 多
     */
    public String multi;

    @JsonIgnore
    public boolean multiDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 收款银行账号
     */
    public Integer partner_bank_account_id;

    @JsonIgnore
    public boolean partner_bank_account_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 业务伙伴类型
     */
    public String partner_type;

    @JsonIgnore
    public boolean partner_typeDirtyFlag;
    
    /**
     * 付款日期
     */
    public Timestamp payment_date;

    @JsonIgnore
    public boolean payment_dateDirtyFlag;
    
    /**
     * 付款差异
     */
    public Double payment_difference;

    @JsonIgnore
    public boolean payment_differenceDirtyFlag;
    
    /**
     * 付款差异处理
     */
    public String payment_difference_handling;

    @JsonIgnore
    public boolean payment_difference_handlingDirtyFlag;
    
    /**
     * 代码
     */
    public String payment_method_code;

    @JsonIgnore
    public boolean payment_method_codeDirtyFlag;
    
    /**
     * 付款方法类型
     */
    public Integer payment_method_id;

    @JsonIgnore
    public boolean payment_method_idDirtyFlag;
    
    /**
     * 付款方法类型
     */
    public String payment_method_id_text;

    @JsonIgnore
    public boolean payment_method_id_textDirtyFlag;
    
    /**
     * 付款参考
     */
    public String payment_reference;

    @JsonIgnore
    public boolean payment_referenceDirtyFlag;
    
    /**
     * 保存的付款令牌
     */
    public Integer payment_token_id;

    @JsonIgnore
    public boolean payment_token_idDirtyFlag;
    
    /**
     * 保存的付款令牌
     */
    public String payment_token_id_text;

    @JsonIgnore
    public boolean payment_token_id_textDirtyFlag;
    
    /**
     * 付款交易
     */
    public Integer payment_transaction_id;

    @JsonIgnore
    public boolean payment_transaction_idDirtyFlag;
    
    /**
     * 付款类型
     */
    public String payment_type;

    @JsonIgnore
    public boolean payment_typeDirtyFlag;
    
    /**
     * 已核销的发票
     */
    public String reconciled_invoice_ids;

    @JsonIgnore
    public boolean reconciled_invoice_idsDirtyFlag;
    
    /**
     * 显示合作伙伴银行账户
     */
    public String show_partner_bank_account;

    @JsonIgnore
    public boolean show_partner_bank_accountDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 差异科目
     */
    public Integer writeoff_account_id;

    @JsonIgnore
    public boolean writeoff_account_idDirtyFlag;
    
    /**
     * 差异科目
     */
    public String writeoff_account_id_text;

    @JsonIgnore
    public boolean writeoff_account_id_textDirtyFlag;
    
    /**
     * 日记账项目标签
     */
    public String writeoff_label;

    @JsonIgnore
    public boolean writeoff_labelDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [付款金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [付款金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [付款金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [备忘]
     */
    @JsonProperty("communication")
    public String getCommunication(){
        return this.communication ;
    }

    /**
     * 设置 [备忘]
     */
    @JsonProperty("communication")
    public void setCommunication(String  communication){
        this.communication = communication ;
        this.communicationDirtyFlag = true ;
    }

     /**
     * 获取 [备忘]脏标记
     */
    @JsonIgnore
    public boolean getCommunicationDirtyFlag(){
        return this.communicationDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [目标账户]
     */
    @JsonProperty("destination_account_id")
    public Integer getDestination_account_id(){
        return this.destination_account_id ;
    }

    /**
     * 设置 [目标账户]
     */
    @JsonProperty("destination_account_id")
    public void setDestination_account_id(Integer  destination_account_id){
        this.destination_account_id = destination_account_id ;
        this.destination_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [目标账户]脏标记
     */
    @JsonIgnore
    public boolean getDestination_account_idDirtyFlag(){
        return this.destination_account_idDirtyFlag ;
    }   

    /**
     * 获取 [转账到]
     */
    @JsonProperty("destination_journal_id")
    public Integer getDestination_journal_id(){
        return this.destination_journal_id ;
    }

    /**
     * 设置 [转账到]
     */
    @JsonProperty("destination_journal_id")
    public void setDestination_journal_id(Integer  destination_journal_id){
        this.destination_journal_id = destination_journal_id ;
        this.destination_journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [转账到]脏标记
     */
    @JsonIgnore
    public boolean getDestination_journal_idDirtyFlag(){
        return this.destination_journal_idDirtyFlag ;
    }   

    /**
     * 获取 [转账到]
     */
    @JsonProperty("destination_journal_id_text")
    public String getDestination_journal_id_text(){
        return this.destination_journal_id_text ;
    }

    /**
     * 设置 [转账到]
     */
    @JsonProperty("destination_journal_id_text")
    public void setDestination_journal_id_text(String  destination_journal_id_text){
        this.destination_journal_id_text = destination_journal_id_text ;
        this.destination_journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [转账到]脏标记
     */
    @JsonIgnore
    public boolean getDestination_journal_id_textDirtyFlag(){
        return this.destination_journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [有发票]
     */
    @JsonProperty("has_invoices")
    public String getHas_invoices(){
        return this.has_invoices ;
    }

    /**
     * 设置 [有发票]
     */
    @JsonProperty("has_invoices")
    public void setHas_invoices(String  has_invoices){
        this.has_invoices = has_invoices ;
        this.has_invoicesDirtyFlag = true ;
    }

     /**
     * 获取 [有发票]脏标记
     */
    @JsonIgnore
    public boolean getHas_invoicesDirtyFlag(){
        return this.has_invoicesDirtyFlag ;
    }   

    /**
     * 获取 [隐藏付款方式]
     */
    @JsonProperty("hide_payment_method")
    public String getHide_payment_method(){
        return this.hide_payment_method ;
    }

    /**
     * 设置 [隐藏付款方式]
     */
    @JsonProperty("hide_payment_method")
    public void setHide_payment_method(String  hide_payment_method){
        this.hide_payment_method = hide_payment_method ;
        this.hide_payment_methodDirtyFlag = true ;
    }

     /**
     * 获取 [隐藏付款方式]脏标记
     */
    @JsonIgnore
    public boolean getHide_payment_methodDirtyFlag(){
        return this.hide_payment_methodDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return this.invoice_ids ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return this.invoice_idsDirtyFlag ;
    }   

    /**
     * 获取 [付款日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [付款日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [付款日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [付款日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [需要一个动作消息的编码]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [需要一个动作消息的编码]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [操作编号]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [操作编号]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [操作编号]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [分录行]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return this.move_line_ids ;
    }

    /**
     * 设置 [分录行]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [分录行]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return this.move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [日记账分录名称]
     */
    @JsonProperty("move_name")
    public String getMove_name(){
        return this.move_name ;
    }

    /**
     * 设置 [日记账分录名称]
     */
    @JsonProperty("move_name")
    public void setMove_name(String  move_name){
        this.move_name = move_name ;
        this.move_nameDirtyFlag = true ;
    }

     /**
     * 获取 [日记账分录名称]脏标记
     */
    @JsonIgnore
    public boolean getMove_nameDirtyFlag(){
        return this.move_nameDirtyFlag ;
    }   

    /**
     * 获取 [凭证已核销]
     */
    @JsonProperty("move_reconciled")
    public String getMove_reconciled(){
        return this.move_reconciled ;
    }

    /**
     * 设置 [凭证已核销]
     */
    @JsonProperty("move_reconciled")
    public void setMove_reconciled(String  move_reconciled){
        this.move_reconciled = move_reconciled ;
        this.move_reconciledDirtyFlag = true ;
    }

     /**
     * 获取 [凭证已核销]脏标记
     */
    @JsonIgnore
    public boolean getMove_reconciledDirtyFlag(){
        return this.move_reconciledDirtyFlag ;
    }   

    /**
     * 获取 [多]
     */
    @JsonProperty("multi")
    public String getMulti(){
        return this.multi ;
    }

    /**
     * 设置 [多]
     */
    @JsonProperty("multi")
    public void setMulti(String  multi){
        this.multi = multi ;
        this.multiDirtyFlag = true ;
    }

     /**
     * 获取 [多]脏标记
     */
    @JsonIgnore
    public boolean getMultiDirtyFlag(){
        return this.multiDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [收款银行账号]
     */
    @JsonProperty("partner_bank_account_id")
    public Integer getPartner_bank_account_id(){
        return this.partner_bank_account_id ;
    }

    /**
     * 设置 [收款银行账号]
     */
    @JsonProperty("partner_bank_account_id")
    public void setPartner_bank_account_id(Integer  partner_bank_account_id){
        this.partner_bank_account_id = partner_bank_account_id ;
        this.partner_bank_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [收款银行账号]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_account_idDirtyFlag(){
        return this.partner_bank_account_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴类型]
     */
    @JsonProperty("partner_type")
    public String getPartner_type(){
        return this.partner_type ;
    }

    /**
     * 设置 [业务伙伴类型]
     */
    @JsonProperty("partner_type")
    public void setPartner_type(String  partner_type){
        this.partner_type = partner_type ;
        this.partner_typeDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴类型]脏标记
     */
    @JsonIgnore
    public boolean getPartner_typeDirtyFlag(){
        return this.partner_typeDirtyFlag ;
    }   

    /**
     * 获取 [付款日期]
     */
    @JsonProperty("payment_date")
    public Timestamp getPayment_date(){
        return this.payment_date ;
    }

    /**
     * 设置 [付款日期]
     */
    @JsonProperty("payment_date")
    public void setPayment_date(Timestamp  payment_date){
        this.payment_date = payment_date ;
        this.payment_dateDirtyFlag = true ;
    }

     /**
     * 获取 [付款日期]脏标记
     */
    @JsonIgnore
    public boolean getPayment_dateDirtyFlag(){
        return this.payment_dateDirtyFlag ;
    }   

    /**
     * 获取 [付款差异]
     */
    @JsonProperty("payment_difference")
    public Double getPayment_difference(){
        return this.payment_difference ;
    }

    /**
     * 设置 [付款差异]
     */
    @JsonProperty("payment_difference")
    public void setPayment_difference(Double  payment_difference){
        this.payment_difference = payment_difference ;
        this.payment_differenceDirtyFlag = true ;
    }

     /**
     * 获取 [付款差异]脏标记
     */
    @JsonIgnore
    public boolean getPayment_differenceDirtyFlag(){
        return this.payment_differenceDirtyFlag ;
    }   

    /**
     * 获取 [付款差异处理]
     */
    @JsonProperty("payment_difference_handling")
    public String getPayment_difference_handling(){
        return this.payment_difference_handling ;
    }

    /**
     * 设置 [付款差异处理]
     */
    @JsonProperty("payment_difference_handling")
    public void setPayment_difference_handling(String  payment_difference_handling){
        this.payment_difference_handling = payment_difference_handling ;
        this.payment_difference_handlingDirtyFlag = true ;
    }

     /**
     * 获取 [付款差异处理]脏标记
     */
    @JsonIgnore
    public boolean getPayment_difference_handlingDirtyFlag(){
        return this.payment_difference_handlingDirtyFlag ;
    }   

    /**
     * 获取 [代码]
     */
    @JsonProperty("payment_method_code")
    public String getPayment_method_code(){
        return this.payment_method_code ;
    }

    /**
     * 设置 [代码]
     */
    @JsonProperty("payment_method_code")
    public void setPayment_method_code(String  payment_method_code){
        this.payment_method_code = payment_method_code ;
        this.payment_method_codeDirtyFlag = true ;
    }

     /**
     * 获取 [代码]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_codeDirtyFlag(){
        return this.payment_method_codeDirtyFlag ;
    }   

    /**
     * 获取 [付款方法类型]
     */
    @JsonProperty("payment_method_id")
    public Integer getPayment_method_id(){
        return this.payment_method_id ;
    }

    /**
     * 设置 [付款方法类型]
     */
    @JsonProperty("payment_method_id")
    public void setPayment_method_id(Integer  payment_method_id){
        this.payment_method_id = payment_method_id ;
        this.payment_method_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款方法类型]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_idDirtyFlag(){
        return this.payment_method_idDirtyFlag ;
    }   

    /**
     * 获取 [付款方法类型]
     */
    @JsonProperty("payment_method_id_text")
    public String getPayment_method_id_text(){
        return this.payment_method_id_text ;
    }

    /**
     * 设置 [付款方法类型]
     */
    @JsonProperty("payment_method_id_text")
    public void setPayment_method_id_text(String  payment_method_id_text){
        this.payment_method_id_text = payment_method_id_text ;
        this.payment_method_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款方法类型]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_id_textDirtyFlag(){
        return this.payment_method_id_textDirtyFlag ;
    }   

    /**
     * 获取 [付款参考]
     */
    @JsonProperty("payment_reference")
    public String getPayment_reference(){
        return this.payment_reference ;
    }

    /**
     * 设置 [付款参考]
     */
    @JsonProperty("payment_reference")
    public void setPayment_reference(String  payment_reference){
        this.payment_reference = payment_reference ;
        this.payment_referenceDirtyFlag = true ;
    }

     /**
     * 获取 [付款参考]脏标记
     */
    @JsonIgnore
    public boolean getPayment_referenceDirtyFlag(){
        return this.payment_referenceDirtyFlag ;
    }   

    /**
     * 获取 [保存的付款令牌]
     */
    @JsonProperty("payment_token_id")
    public Integer getPayment_token_id(){
        return this.payment_token_id ;
    }

    /**
     * 设置 [保存的付款令牌]
     */
    @JsonProperty("payment_token_id")
    public void setPayment_token_id(Integer  payment_token_id){
        this.payment_token_id = payment_token_id ;
        this.payment_token_idDirtyFlag = true ;
    }

     /**
     * 获取 [保存的付款令牌]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_idDirtyFlag(){
        return this.payment_token_idDirtyFlag ;
    }   

    /**
     * 获取 [保存的付款令牌]
     */
    @JsonProperty("payment_token_id_text")
    public String getPayment_token_id_text(){
        return this.payment_token_id_text ;
    }

    /**
     * 设置 [保存的付款令牌]
     */
    @JsonProperty("payment_token_id_text")
    public void setPayment_token_id_text(String  payment_token_id_text){
        this.payment_token_id_text = payment_token_id_text ;
        this.payment_token_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [保存的付款令牌]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_id_textDirtyFlag(){
        return this.payment_token_id_textDirtyFlag ;
    }   

    /**
     * 获取 [付款交易]
     */
    @JsonProperty("payment_transaction_id")
    public Integer getPayment_transaction_id(){
        return this.payment_transaction_id ;
    }

    /**
     * 设置 [付款交易]
     */
    @JsonProperty("payment_transaction_id")
    public void setPayment_transaction_id(Integer  payment_transaction_id){
        this.payment_transaction_id = payment_transaction_id ;
        this.payment_transaction_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款交易]脏标记
     */
    @JsonIgnore
    public boolean getPayment_transaction_idDirtyFlag(){
        return this.payment_transaction_idDirtyFlag ;
    }   

    /**
     * 获取 [付款类型]
     */
    @JsonProperty("payment_type")
    public String getPayment_type(){
        return this.payment_type ;
    }

    /**
     * 设置 [付款类型]
     */
    @JsonProperty("payment_type")
    public void setPayment_type(String  payment_type){
        this.payment_type = payment_type ;
        this.payment_typeDirtyFlag = true ;
    }

     /**
     * 获取 [付款类型]脏标记
     */
    @JsonIgnore
    public boolean getPayment_typeDirtyFlag(){
        return this.payment_typeDirtyFlag ;
    }   

    /**
     * 获取 [已核销的发票]
     */
    @JsonProperty("reconciled_invoice_ids")
    public String getReconciled_invoice_ids(){
        return this.reconciled_invoice_ids ;
    }

    /**
     * 设置 [已核销的发票]
     */
    @JsonProperty("reconciled_invoice_ids")
    public void setReconciled_invoice_ids(String  reconciled_invoice_ids){
        this.reconciled_invoice_ids = reconciled_invoice_ids ;
        this.reconciled_invoice_idsDirtyFlag = true ;
    }

     /**
     * 获取 [已核销的发票]脏标记
     */
    @JsonIgnore
    public boolean getReconciled_invoice_idsDirtyFlag(){
        return this.reconciled_invoice_idsDirtyFlag ;
    }   

    /**
     * 获取 [显示合作伙伴银行账户]
     */
    @JsonProperty("show_partner_bank_account")
    public String getShow_partner_bank_account(){
        return this.show_partner_bank_account ;
    }

    /**
     * 设置 [显示合作伙伴银行账户]
     */
    @JsonProperty("show_partner_bank_account")
    public void setShow_partner_bank_account(String  show_partner_bank_account){
        this.show_partner_bank_account = show_partner_bank_account ;
        this.show_partner_bank_accountDirtyFlag = true ;
    }

     /**
     * 获取 [显示合作伙伴银行账户]脏标记
     */
    @JsonIgnore
    public boolean getShow_partner_bank_accountDirtyFlag(){
        return this.show_partner_bank_accountDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [差异科目]
     */
    @JsonProperty("writeoff_account_id")
    public Integer getWriteoff_account_id(){
        return this.writeoff_account_id ;
    }

    /**
     * 设置 [差异科目]
     */
    @JsonProperty("writeoff_account_id")
    public void setWriteoff_account_id(Integer  writeoff_account_id){
        this.writeoff_account_id = writeoff_account_id ;
        this.writeoff_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [差异科目]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_idDirtyFlag(){
        return this.writeoff_account_idDirtyFlag ;
    }   

    /**
     * 获取 [差异科目]
     */
    @JsonProperty("writeoff_account_id_text")
    public String getWriteoff_account_id_text(){
        return this.writeoff_account_id_text ;
    }

    /**
     * 设置 [差异科目]
     */
    @JsonProperty("writeoff_account_id_text")
    public void setWriteoff_account_id_text(String  writeoff_account_id_text){
        this.writeoff_account_id_text = writeoff_account_id_text ;
        this.writeoff_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [差异科目]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_id_textDirtyFlag(){
        return this.writeoff_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日记账项目标签]
     */
    @JsonProperty("writeoff_label")
    public String getWriteoff_label(){
        return this.writeoff_label ;
    }

    /**
     * 设置 [日记账项目标签]
     */
    @JsonProperty("writeoff_label")
    public void setWriteoff_label(String  writeoff_label){
        this.writeoff_label = writeoff_label ;
        this.writeoff_labelDirtyFlag = true ;
    }

     /**
     * 获取 [日记账项目标签]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_labelDirtyFlag(){
        return this.writeoff_labelDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("amount") instanceof Boolean)&& map.get("amount")!=null){
			this.setAmount((Double)map.get("amount"));
		}
		if(!(map.get("communication") instanceof Boolean)&& map.get("communication")!=null){
			this.setCommunication((String)map.get("communication"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 1){
				this.setCurrency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("destination_account_id") instanceof Boolean)&& map.get("destination_account_id")!=null){
			Object[] objs = (Object[])map.get("destination_account_id");
			if(objs.length > 0){
				this.setDestination_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("destination_journal_id") instanceof Boolean)&& map.get("destination_journal_id")!=null){
			Object[] objs = (Object[])map.get("destination_journal_id");
			if(objs.length > 0){
				this.setDestination_journal_id((Integer)objs[0]);
			}
		}
		if(!(map.get("destination_journal_id") instanceof Boolean)&& map.get("destination_journal_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("destination_journal_id");
			if(objs.length > 1){
				this.setDestination_journal_id_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("has_invoices") instanceof Boolean){
			this.setHas_invoices(((Boolean)map.get("has_invoices"))? "true" : "false");
		}
		if(map.get("hide_payment_method") instanceof Boolean){
			this.setHide_payment_method(((Boolean)map.get("hide_payment_method"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("invoice_ids") instanceof Boolean)&& map.get("invoice_ids")!=null){
			Object[] objs = (Object[])map.get("invoice_ids");
			if(objs.length > 0){
				Integer[] invoice_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setInvoice_ids(Arrays.toString(invoice_ids).replace(" ",""));
			}
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 0){
				this.setJournal_id((Integer)objs[0]);
			}
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 1){
				this.setJournal_id_text((String)objs[1]);
			}
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids).replace(" ",""));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids).replace(" ",""));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids).replace(" ",""));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("move_line_ids") instanceof Boolean)&& map.get("move_line_ids")!=null){
			Object[] objs = (Object[])map.get("move_line_ids");
			if(objs.length > 0){
				Integer[] move_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_line_ids(Arrays.toString(move_line_ids).replace(" ",""));
			}
		}
		if(!(map.get("move_name") instanceof Boolean)&& map.get("move_name")!=null){
			this.setMove_name((String)map.get("move_name"));
		}
		if(map.get("move_reconciled") instanceof Boolean){
			this.setMove_reconciled(((Boolean)map.get("move_reconciled"))? "true" : "false");
		}
		if(map.get("multi") instanceof Boolean){
			this.setMulti(((Boolean)map.get("multi"))? "true" : "false");
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("partner_bank_account_id") instanceof Boolean)&& map.get("partner_bank_account_id")!=null){
			Object[] objs = (Object[])map.get("partner_bank_account_id");
			if(objs.length > 0){
				this.setPartner_bank_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("partner_type") instanceof Boolean)&& map.get("partner_type")!=null){
			this.setPartner_type((String)map.get("partner_type"));
		}
		if(!(map.get("payment_date") instanceof Boolean)&& map.get("payment_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("payment_date"));
   			this.setPayment_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("payment_difference") instanceof Boolean)&& map.get("payment_difference")!=null){
			this.setPayment_difference((Double)map.get("payment_difference"));
		}
		if(!(map.get("payment_difference_handling") instanceof Boolean)&& map.get("payment_difference_handling")!=null){
			this.setPayment_difference_handling((String)map.get("payment_difference_handling"));
		}
		if(!(map.get("payment_method_code") instanceof Boolean)&& map.get("payment_method_code")!=null){
			this.setPayment_method_code((String)map.get("payment_method_code"));
		}
		if(!(map.get("payment_method_id") instanceof Boolean)&& map.get("payment_method_id")!=null){
			Object[] objs = (Object[])map.get("payment_method_id");
			if(objs.length > 0){
				this.setPayment_method_id((Integer)objs[0]);
			}
		}
		if(!(map.get("payment_method_id") instanceof Boolean)&& map.get("payment_method_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("payment_method_id");
			if(objs.length > 1){
				this.setPayment_method_id_text((String)objs[1]);
			}
		}
		if(!(map.get("payment_reference") instanceof Boolean)&& map.get("payment_reference")!=null){
			this.setPayment_reference((String)map.get("payment_reference"));
		}
		if(!(map.get("payment_token_id") instanceof Boolean)&& map.get("payment_token_id")!=null){
			Object[] objs = (Object[])map.get("payment_token_id");
			if(objs.length > 0){
				this.setPayment_token_id((Integer)objs[0]);
			}
		}
		if(!(map.get("payment_token_id") instanceof Boolean)&& map.get("payment_token_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("payment_token_id");
			if(objs.length > 1){
				this.setPayment_token_id_text((String)objs[1]);
			}
		}
		if(!(map.get("payment_transaction_id") instanceof Boolean)&& map.get("payment_transaction_id")!=null){
			Object[] objs = (Object[])map.get("payment_transaction_id");
			if(objs.length > 0){
				this.setPayment_transaction_id((Integer)objs[0]);
			}
		}
		if(!(map.get("payment_type") instanceof Boolean)&& map.get("payment_type")!=null){
			this.setPayment_type((String)map.get("payment_type"));
		}
		if(!(map.get("reconciled_invoice_ids") instanceof Boolean)&& map.get("reconciled_invoice_ids")!=null){
			Object[] objs = (Object[])map.get("reconciled_invoice_ids");
			if(objs.length > 0){
				Integer[] reconciled_invoice_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setReconciled_invoice_ids(Arrays.toString(reconciled_invoice_ids).replace(" ",""));
			}
		}
		if(map.get("show_partner_bank_account") instanceof Boolean){
			this.setShow_partner_bank_account(((Boolean)map.get("show_partner_bank_account"))? "true" : "false");
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids).replace(" ",""));
			}
		}
		if(!(map.get("writeoff_account_id") instanceof Boolean)&& map.get("writeoff_account_id")!=null){
			Object[] objs = (Object[])map.get("writeoff_account_id");
			if(objs.length > 0){
				this.setWriteoff_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("writeoff_account_id") instanceof Boolean)&& map.get("writeoff_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("writeoff_account_id");
			if(objs.length > 1){
				this.setWriteoff_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("writeoff_label") instanceof Boolean)&& map.get("writeoff_label")!=null){
			this.setWriteoff_label((String)map.get("writeoff_label"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAmount()!=null&&this.getAmountDirtyFlag()){
			map.put("amount",this.getAmount());
		}else if(this.getAmountDirtyFlag()){
			map.put("amount",false);
		}
		if(this.getCommunication()!=null&&this.getCommunicationDirtyFlag()){
			map.put("communication",this.getCommunication());
		}else if(this.getCommunicationDirtyFlag()){
			map.put("communication",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_id_text()!=null&&this.getCurrency_id_textDirtyFlag()){
			//忽略文本外键currency_id_text
		}else if(this.getCurrency_id_textDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDestination_account_id()!=null&&this.getDestination_account_idDirtyFlag()){
			map.put("destination_account_id",this.getDestination_account_id());
		}else if(this.getDestination_account_idDirtyFlag()){
			map.put("destination_account_id",false);
		}
		if(this.getDestination_journal_id()!=null&&this.getDestination_journal_idDirtyFlag()){
			map.put("destination_journal_id",this.getDestination_journal_id());
		}else if(this.getDestination_journal_idDirtyFlag()){
			map.put("destination_journal_id",false);
		}
		if(this.getDestination_journal_id_text()!=null&&this.getDestination_journal_id_textDirtyFlag()){
			//忽略文本外键destination_journal_id_text
		}else if(this.getDestination_journal_id_textDirtyFlag()){
			map.put("destination_journal_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getHas_invoices()!=null&&this.getHas_invoicesDirtyFlag()){
			map.put("has_invoices",Boolean.parseBoolean(this.getHas_invoices()));		
		}		if(this.getHide_payment_method()!=null&&this.getHide_payment_methodDirtyFlag()){
			map.put("hide_payment_method",Boolean.parseBoolean(this.getHide_payment_method()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInvoice_ids()!=null&&this.getInvoice_idsDirtyFlag()){
			map.put("invoice_ids",this.getInvoice_ids());
		}else if(this.getInvoice_idsDirtyFlag()){
			map.put("invoice_ids",false);
		}
		if(this.getJournal_id()!=null&&this.getJournal_idDirtyFlag()){
			map.put("journal_id",this.getJournal_id());
		}else if(this.getJournal_idDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getJournal_id_text()!=null&&this.getJournal_id_textDirtyFlag()){
			//忽略文本外键journal_id_text
		}else if(this.getJournal_id_textDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getMove_line_ids()!=null&&this.getMove_line_idsDirtyFlag()){
			map.put("move_line_ids",this.getMove_line_ids());
		}else if(this.getMove_line_idsDirtyFlag()){
			map.put("move_line_ids",false);
		}
		if(this.getMove_name()!=null&&this.getMove_nameDirtyFlag()){
			map.put("move_name",this.getMove_name());
		}else if(this.getMove_nameDirtyFlag()){
			map.put("move_name",false);
		}
		if(this.getMove_reconciled()!=null&&this.getMove_reconciledDirtyFlag()){
			map.put("move_reconciled",Boolean.parseBoolean(this.getMove_reconciled()));		
		}		if(this.getMulti()!=null&&this.getMultiDirtyFlag()){
			map.put("multi",Boolean.parseBoolean(this.getMulti()));		
		}		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPartner_bank_account_id()!=null&&this.getPartner_bank_account_idDirtyFlag()){
			map.put("partner_bank_account_id",this.getPartner_bank_account_id());
		}else if(this.getPartner_bank_account_idDirtyFlag()){
			map.put("partner_bank_account_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_type()!=null&&this.getPartner_typeDirtyFlag()){
			map.put("partner_type",this.getPartner_type());
		}else if(this.getPartner_typeDirtyFlag()){
			map.put("partner_type",false);
		}
		if(this.getPayment_date()!=null&&this.getPayment_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getPayment_date());
			map.put("payment_date",datetimeStr);
		}else if(this.getPayment_dateDirtyFlag()){
			map.put("payment_date",false);
		}
		if(this.getPayment_difference()!=null&&this.getPayment_differenceDirtyFlag()){
			map.put("payment_difference",this.getPayment_difference());
		}else if(this.getPayment_differenceDirtyFlag()){
			map.put("payment_difference",false);
		}
		if(this.getPayment_difference_handling()!=null&&this.getPayment_difference_handlingDirtyFlag()){
			map.put("payment_difference_handling",this.getPayment_difference_handling());
		}else if(this.getPayment_difference_handlingDirtyFlag()){
			map.put("payment_difference_handling",false);
		}
		if(this.getPayment_method_code()!=null&&this.getPayment_method_codeDirtyFlag()){
			map.put("payment_method_code",this.getPayment_method_code());
		}else if(this.getPayment_method_codeDirtyFlag()){
			map.put("payment_method_code",false);
		}
		if(this.getPayment_method_id()!=null&&this.getPayment_method_idDirtyFlag()){
			map.put("payment_method_id",this.getPayment_method_id());
		}else if(this.getPayment_method_idDirtyFlag()){
			map.put("payment_method_id",false);
		}
		if(this.getPayment_method_id_text()!=null&&this.getPayment_method_id_textDirtyFlag()){
			//忽略文本外键payment_method_id_text
		}else if(this.getPayment_method_id_textDirtyFlag()){
			map.put("payment_method_id",false);
		}
		if(this.getPayment_reference()!=null&&this.getPayment_referenceDirtyFlag()){
			map.put("payment_reference",this.getPayment_reference());
		}else if(this.getPayment_referenceDirtyFlag()){
			map.put("payment_reference",false);
		}
		if(this.getPayment_token_id()!=null&&this.getPayment_token_idDirtyFlag()){
			map.put("payment_token_id",this.getPayment_token_id());
		}else if(this.getPayment_token_idDirtyFlag()){
			map.put("payment_token_id",false);
		}
		if(this.getPayment_token_id_text()!=null&&this.getPayment_token_id_textDirtyFlag()){
			//忽略文本外键payment_token_id_text
		}else if(this.getPayment_token_id_textDirtyFlag()){
			map.put("payment_token_id",false);
		}
		if(this.getPayment_transaction_id()!=null&&this.getPayment_transaction_idDirtyFlag()){
			map.put("payment_transaction_id",this.getPayment_transaction_id());
		}else if(this.getPayment_transaction_idDirtyFlag()){
			map.put("payment_transaction_id",false);
		}
		if(this.getPayment_type()!=null&&this.getPayment_typeDirtyFlag()){
			map.put("payment_type",this.getPayment_type());
		}else if(this.getPayment_typeDirtyFlag()){
			map.put("payment_type",false);
		}
		if(this.getReconciled_invoice_ids()!=null&&this.getReconciled_invoice_idsDirtyFlag()){
			map.put("reconciled_invoice_ids",this.getReconciled_invoice_ids());
		}else if(this.getReconciled_invoice_idsDirtyFlag()){
			map.put("reconciled_invoice_ids",false);
		}
		if(this.getShow_partner_bank_account()!=null&&this.getShow_partner_bank_accountDirtyFlag()){
			map.put("show_partner_bank_account",Boolean.parseBoolean(this.getShow_partner_bank_account()));		
		}		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWriteoff_account_id()!=null&&this.getWriteoff_account_idDirtyFlag()){
			map.put("writeoff_account_id",this.getWriteoff_account_id());
		}else if(this.getWriteoff_account_idDirtyFlag()){
			map.put("writeoff_account_id",false);
		}
		if(this.getWriteoff_account_id_text()!=null&&this.getWriteoff_account_id_textDirtyFlag()){
			//忽略文本外键writeoff_account_id_text
		}else if(this.getWriteoff_account_id_textDirtyFlag()){
			map.put("writeoff_account_id",false);
		}
		if(this.getWriteoff_label()!=null&&this.getWriteoff_labelDirtyFlag()){
			map.put("writeoff_label",this.getWriteoff_label());
		}else if(this.getWriteoff_labelDirtyFlag()){
			map.put("writeoff_label",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
