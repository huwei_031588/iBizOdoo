package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_abstract_payment] 对象
 */
public class account_abstract_paymentClientModel implements Serializable{

    /**
     * 付款金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 备忘
     */
    public String communication;

    @JsonIgnore
    public boolean communicationDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 隐藏付款方式
     */
    public String hide_payment_method;

    @JsonIgnore
    public boolean hide_payment_methodDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_ids;

    @JsonIgnore
    public boolean invoice_idsDirtyFlag;
    
    /**
     * 付款日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 付款日记账
     */
    public String journal_id_text;

    @JsonIgnore
    public boolean journal_id_textDirtyFlag;
    
    /**
     * 多
     */
    public String multi;

    @JsonIgnore
    public boolean multiDirtyFlag;
    
    /**
     * 收款银行账号
     */
    public Integer partner_bank_account_id;

    @JsonIgnore
    public boolean partner_bank_account_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 业务伙伴类型
     */
    public String partner_type;

    @JsonIgnore
    public boolean partner_typeDirtyFlag;
    
    /**
     * 付款日期
     */
    public Timestamp payment_date;

    @JsonIgnore
    public boolean payment_dateDirtyFlag;
    
    /**
     * 付款差异
     */
    public Double payment_difference;

    @JsonIgnore
    public boolean payment_differenceDirtyFlag;
    
    /**
     * 付款差异处理
     */
    public String payment_difference_handling;

    @JsonIgnore
    public boolean payment_difference_handlingDirtyFlag;
    
    /**
     * 代码
     */
    public String payment_method_code;

    @JsonIgnore
    public boolean payment_method_codeDirtyFlag;
    
    /**
     * 付款方法类型
     */
    public Integer payment_method_id;

    @JsonIgnore
    public boolean payment_method_idDirtyFlag;
    
    /**
     * 付款方法类型
     */
    public String payment_method_id_text;

    @JsonIgnore
    public boolean payment_method_id_textDirtyFlag;
    
    /**
     * 付款类型
     */
    public String payment_type;

    @JsonIgnore
    public boolean payment_typeDirtyFlag;
    
    /**
     * 显示合作伙伴银行账户
     */
    public String show_partner_bank_account;

    @JsonIgnore
    public boolean show_partner_bank_accountDirtyFlag;
    
    /**
     * 差异科目
     */
    public Integer writeoff_account_id;

    @JsonIgnore
    public boolean writeoff_account_idDirtyFlag;
    
    /**
     * 差异科目
     */
    public String writeoff_account_id_text;

    @JsonIgnore
    public boolean writeoff_account_id_textDirtyFlag;
    
    /**
     * 日记账项目标签
     */
    public String writeoff_label;

    @JsonIgnore
    public boolean writeoff_labelDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [付款金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [付款金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [付款金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [备忘]
     */
    @JsonProperty("communication")
    public String getCommunication(){
        return this.communication ;
    }

    /**
     * 设置 [备忘]
     */
    @JsonProperty("communication")
    public void setCommunication(String  communication){
        this.communication = communication ;
        this.communicationDirtyFlag = true ;
    }

     /**
     * 获取 [备忘]脏标记
     */
    @JsonIgnore
    public boolean getCommunicationDirtyFlag(){
        return this.communicationDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [隐藏付款方式]
     */
    @JsonProperty("hide_payment_method")
    public String getHide_payment_method(){
        return this.hide_payment_method ;
    }

    /**
     * 设置 [隐藏付款方式]
     */
    @JsonProperty("hide_payment_method")
    public void setHide_payment_method(String  hide_payment_method){
        this.hide_payment_method = hide_payment_method ;
        this.hide_payment_methodDirtyFlag = true ;
    }

     /**
     * 获取 [隐藏付款方式]脏标记
     */
    @JsonIgnore
    public boolean getHide_payment_methodDirtyFlag(){
        return this.hide_payment_methodDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return this.invoice_ids ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return this.invoice_idsDirtyFlag ;
    }   

    /**
     * 获取 [付款日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [付款日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [付款日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [付款日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [多]
     */
    @JsonProperty("multi")
    public String getMulti(){
        return this.multi ;
    }

    /**
     * 设置 [多]
     */
    @JsonProperty("multi")
    public void setMulti(String  multi){
        this.multi = multi ;
        this.multiDirtyFlag = true ;
    }

     /**
     * 获取 [多]脏标记
     */
    @JsonIgnore
    public boolean getMultiDirtyFlag(){
        return this.multiDirtyFlag ;
    }   

    /**
     * 获取 [收款银行账号]
     */
    @JsonProperty("partner_bank_account_id")
    public Integer getPartner_bank_account_id(){
        return this.partner_bank_account_id ;
    }

    /**
     * 设置 [收款银行账号]
     */
    @JsonProperty("partner_bank_account_id")
    public void setPartner_bank_account_id(Integer  partner_bank_account_id){
        this.partner_bank_account_id = partner_bank_account_id ;
        this.partner_bank_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [收款银行账号]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_account_idDirtyFlag(){
        return this.partner_bank_account_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴类型]
     */
    @JsonProperty("partner_type")
    public String getPartner_type(){
        return this.partner_type ;
    }

    /**
     * 设置 [业务伙伴类型]
     */
    @JsonProperty("partner_type")
    public void setPartner_type(String  partner_type){
        this.partner_type = partner_type ;
        this.partner_typeDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴类型]脏标记
     */
    @JsonIgnore
    public boolean getPartner_typeDirtyFlag(){
        return this.partner_typeDirtyFlag ;
    }   

    /**
     * 获取 [付款日期]
     */
    @JsonProperty("payment_date")
    public Timestamp getPayment_date(){
        return this.payment_date ;
    }

    /**
     * 设置 [付款日期]
     */
    @JsonProperty("payment_date")
    public void setPayment_date(Timestamp  payment_date){
        this.payment_date = payment_date ;
        this.payment_dateDirtyFlag = true ;
    }

     /**
     * 获取 [付款日期]脏标记
     */
    @JsonIgnore
    public boolean getPayment_dateDirtyFlag(){
        return this.payment_dateDirtyFlag ;
    }   

    /**
     * 获取 [付款差异]
     */
    @JsonProperty("payment_difference")
    public Double getPayment_difference(){
        return this.payment_difference ;
    }

    /**
     * 设置 [付款差异]
     */
    @JsonProperty("payment_difference")
    public void setPayment_difference(Double  payment_difference){
        this.payment_difference = payment_difference ;
        this.payment_differenceDirtyFlag = true ;
    }

     /**
     * 获取 [付款差异]脏标记
     */
    @JsonIgnore
    public boolean getPayment_differenceDirtyFlag(){
        return this.payment_differenceDirtyFlag ;
    }   

    /**
     * 获取 [付款差异处理]
     */
    @JsonProperty("payment_difference_handling")
    public String getPayment_difference_handling(){
        return this.payment_difference_handling ;
    }

    /**
     * 设置 [付款差异处理]
     */
    @JsonProperty("payment_difference_handling")
    public void setPayment_difference_handling(String  payment_difference_handling){
        this.payment_difference_handling = payment_difference_handling ;
        this.payment_difference_handlingDirtyFlag = true ;
    }

     /**
     * 获取 [付款差异处理]脏标记
     */
    @JsonIgnore
    public boolean getPayment_difference_handlingDirtyFlag(){
        return this.payment_difference_handlingDirtyFlag ;
    }   

    /**
     * 获取 [代码]
     */
    @JsonProperty("payment_method_code")
    public String getPayment_method_code(){
        return this.payment_method_code ;
    }

    /**
     * 设置 [代码]
     */
    @JsonProperty("payment_method_code")
    public void setPayment_method_code(String  payment_method_code){
        this.payment_method_code = payment_method_code ;
        this.payment_method_codeDirtyFlag = true ;
    }

     /**
     * 获取 [代码]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_codeDirtyFlag(){
        return this.payment_method_codeDirtyFlag ;
    }   

    /**
     * 获取 [付款方法类型]
     */
    @JsonProperty("payment_method_id")
    public Integer getPayment_method_id(){
        return this.payment_method_id ;
    }

    /**
     * 设置 [付款方法类型]
     */
    @JsonProperty("payment_method_id")
    public void setPayment_method_id(Integer  payment_method_id){
        this.payment_method_id = payment_method_id ;
        this.payment_method_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款方法类型]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_idDirtyFlag(){
        return this.payment_method_idDirtyFlag ;
    }   

    /**
     * 获取 [付款方法类型]
     */
    @JsonProperty("payment_method_id_text")
    public String getPayment_method_id_text(){
        return this.payment_method_id_text ;
    }

    /**
     * 设置 [付款方法类型]
     */
    @JsonProperty("payment_method_id_text")
    public void setPayment_method_id_text(String  payment_method_id_text){
        this.payment_method_id_text = payment_method_id_text ;
        this.payment_method_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款方法类型]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_id_textDirtyFlag(){
        return this.payment_method_id_textDirtyFlag ;
    }   

    /**
     * 获取 [付款类型]
     */
    @JsonProperty("payment_type")
    public String getPayment_type(){
        return this.payment_type ;
    }

    /**
     * 设置 [付款类型]
     */
    @JsonProperty("payment_type")
    public void setPayment_type(String  payment_type){
        this.payment_type = payment_type ;
        this.payment_typeDirtyFlag = true ;
    }

     /**
     * 获取 [付款类型]脏标记
     */
    @JsonIgnore
    public boolean getPayment_typeDirtyFlag(){
        return this.payment_typeDirtyFlag ;
    }   

    /**
     * 获取 [显示合作伙伴银行账户]
     */
    @JsonProperty("show_partner_bank_account")
    public String getShow_partner_bank_account(){
        return this.show_partner_bank_account ;
    }

    /**
     * 设置 [显示合作伙伴银行账户]
     */
    @JsonProperty("show_partner_bank_account")
    public void setShow_partner_bank_account(String  show_partner_bank_account){
        this.show_partner_bank_account = show_partner_bank_account ;
        this.show_partner_bank_accountDirtyFlag = true ;
    }

     /**
     * 获取 [显示合作伙伴银行账户]脏标记
     */
    @JsonIgnore
    public boolean getShow_partner_bank_accountDirtyFlag(){
        return this.show_partner_bank_accountDirtyFlag ;
    }   

    /**
     * 获取 [差异科目]
     */
    @JsonProperty("writeoff_account_id")
    public Integer getWriteoff_account_id(){
        return this.writeoff_account_id ;
    }

    /**
     * 设置 [差异科目]
     */
    @JsonProperty("writeoff_account_id")
    public void setWriteoff_account_id(Integer  writeoff_account_id){
        this.writeoff_account_id = writeoff_account_id ;
        this.writeoff_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [差异科目]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_idDirtyFlag(){
        return this.writeoff_account_idDirtyFlag ;
    }   

    /**
     * 获取 [差异科目]
     */
    @JsonProperty("writeoff_account_id_text")
    public String getWriteoff_account_id_text(){
        return this.writeoff_account_id_text ;
    }

    /**
     * 设置 [差异科目]
     */
    @JsonProperty("writeoff_account_id_text")
    public void setWriteoff_account_id_text(String  writeoff_account_id_text){
        this.writeoff_account_id_text = writeoff_account_id_text ;
        this.writeoff_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [差异科目]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_id_textDirtyFlag(){
        return this.writeoff_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [日记账项目标签]
     */
    @JsonProperty("writeoff_label")
    public String getWriteoff_label(){
        return this.writeoff_label ;
    }

    /**
     * 设置 [日记账项目标签]
     */
    @JsonProperty("writeoff_label")
    public void setWriteoff_label(String  writeoff_label){
        this.writeoff_label = writeoff_label ;
        this.writeoff_labelDirtyFlag = true ;
    }

     /**
     * 获取 [日记账项目标签]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_labelDirtyFlag(){
        return this.writeoff_labelDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("amount") instanceof Boolean)&& map.get("amount")!=null){
			this.setAmount((Double)map.get("amount"));
		}
		if(!(map.get("communication") instanceof Boolean)&& map.get("communication")!=null){
			this.setCommunication((String)map.get("communication"));
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 1){
				this.setCurrency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("hide_payment_method") instanceof Boolean){
			this.setHide_payment_method(((Boolean)map.get("hide_payment_method"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("invoice_ids") instanceof Boolean)&& map.get("invoice_ids")!=null){
			Object[] objs = (Object[])map.get("invoice_ids");
			if(objs.length > 0){
				Integer[] invoice_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setInvoice_ids(Arrays.toString(invoice_ids).replace(" ",""));
			}
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 0){
				this.setJournal_id((Integer)objs[0]);
			}
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 1){
				this.setJournal_id_text((String)objs[1]);
			}
		}
		if(map.get("multi") instanceof Boolean){
			this.setMulti(((Boolean)map.get("multi"))? "true" : "false");
		}
		if(!(map.get("partner_bank_account_id") instanceof Boolean)&& map.get("partner_bank_account_id")!=null){
			Object[] objs = (Object[])map.get("partner_bank_account_id");
			if(objs.length > 0){
				this.setPartner_bank_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("partner_type") instanceof Boolean)&& map.get("partner_type")!=null){
			this.setPartner_type((String)map.get("partner_type"));
		}
		if(!(map.get("payment_date") instanceof Boolean)&& map.get("payment_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("payment_date"));
   			this.setPayment_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("payment_difference") instanceof Boolean)&& map.get("payment_difference")!=null){
			this.setPayment_difference((Double)map.get("payment_difference"));
		}
		if(!(map.get("payment_difference_handling") instanceof Boolean)&& map.get("payment_difference_handling")!=null){
			this.setPayment_difference_handling((String)map.get("payment_difference_handling"));
		}
		if(!(map.get("payment_method_code") instanceof Boolean)&& map.get("payment_method_code")!=null){
			this.setPayment_method_code((String)map.get("payment_method_code"));
		}
		if(!(map.get("payment_method_id") instanceof Boolean)&& map.get("payment_method_id")!=null){
			Object[] objs = (Object[])map.get("payment_method_id");
			if(objs.length > 0){
				this.setPayment_method_id((Integer)objs[0]);
			}
		}
		if(!(map.get("payment_method_id") instanceof Boolean)&& map.get("payment_method_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("payment_method_id");
			if(objs.length > 1){
				this.setPayment_method_id_text((String)objs[1]);
			}
		}
		if(!(map.get("payment_type") instanceof Boolean)&& map.get("payment_type")!=null){
			this.setPayment_type((String)map.get("payment_type"));
		}
		if(map.get("show_partner_bank_account") instanceof Boolean){
			this.setShow_partner_bank_account(((Boolean)map.get("show_partner_bank_account"))? "true" : "false");
		}
		if(!(map.get("writeoff_account_id") instanceof Boolean)&& map.get("writeoff_account_id")!=null){
			Object[] objs = (Object[])map.get("writeoff_account_id");
			if(objs.length > 0){
				this.setWriteoff_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("writeoff_account_id") instanceof Boolean)&& map.get("writeoff_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("writeoff_account_id");
			if(objs.length > 1){
				this.setWriteoff_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("writeoff_label") instanceof Boolean)&& map.get("writeoff_label")!=null){
			this.setWriteoff_label((String)map.get("writeoff_label"));
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAmount()!=null&&this.getAmountDirtyFlag()){
			map.put("amount",this.getAmount());
		}else if(this.getAmountDirtyFlag()){
			map.put("amount",false);
		}
		if(this.getCommunication()!=null&&this.getCommunicationDirtyFlag()){
			map.put("communication",this.getCommunication());
		}else if(this.getCommunicationDirtyFlag()){
			map.put("communication",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_id_text()!=null&&this.getCurrency_id_textDirtyFlag()){
			//忽略文本外键currency_id_text
		}else if(this.getCurrency_id_textDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getHide_payment_method()!=null&&this.getHide_payment_methodDirtyFlag()){
			map.put("hide_payment_method",Boolean.parseBoolean(this.getHide_payment_method()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInvoice_ids()!=null&&this.getInvoice_idsDirtyFlag()){
			map.put("invoice_ids",this.getInvoice_ids());
		}else if(this.getInvoice_idsDirtyFlag()){
			map.put("invoice_ids",false);
		}
		if(this.getJournal_id()!=null&&this.getJournal_idDirtyFlag()){
			map.put("journal_id",this.getJournal_id());
		}else if(this.getJournal_idDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getJournal_id_text()!=null&&this.getJournal_id_textDirtyFlag()){
			//忽略文本外键journal_id_text
		}else if(this.getJournal_id_textDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getMulti()!=null&&this.getMultiDirtyFlag()){
			map.put("multi",Boolean.parseBoolean(this.getMulti()));		
		}		if(this.getPartner_bank_account_id()!=null&&this.getPartner_bank_account_idDirtyFlag()){
			map.put("partner_bank_account_id",this.getPartner_bank_account_id());
		}else if(this.getPartner_bank_account_idDirtyFlag()){
			map.put("partner_bank_account_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_type()!=null&&this.getPartner_typeDirtyFlag()){
			map.put("partner_type",this.getPartner_type());
		}else if(this.getPartner_typeDirtyFlag()){
			map.put("partner_type",false);
		}
		if(this.getPayment_date()!=null&&this.getPayment_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getPayment_date());
			map.put("payment_date",datetimeStr);
		}else if(this.getPayment_dateDirtyFlag()){
			map.put("payment_date",false);
		}
		if(this.getPayment_difference()!=null&&this.getPayment_differenceDirtyFlag()){
			map.put("payment_difference",this.getPayment_difference());
		}else if(this.getPayment_differenceDirtyFlag()){
			map.put("payment_difference",false);
		}
		if(this.getPayment_difference_handling()!=null&&this.getPayment_difference_handlingDirtyFlag()){
			map.put("payment_difference_handling",this.getPayment_difference_handling());
		}else if(this.getPayment_difference_handlingDirtyFlag()){
			map.put("payment_difference_handling",false);
		}
		if(this.getPayment_method_code()!=null&&this.getPayment_method_codeDirtyFlag()){
			map.put("payment_method_code",this.getPayment_method_code());
		}else if(this.getPayment_method_codeDirtyFlag()){
			map.put("payment_method_code",false);
		}
		if(this.getPayment_method_id()!=null&&this.getPayment_method_idDirtyFlag()){
			map.put("payment_method_id",this.getPayment_method_id());
		}else if(this.getPayment_method_idDirtyFlag()){
			map.put("payment_method_id",false);
		}
		if(this.getPayment_method_id_text()!=null&&this.getPayment_method_id_textDirtyFlag()){
			//忽略文本外键payment_method_id_text
		}else if(this.getPayment_method_id_textDirtyFlag()){
			map.put("payment_method_id",false);
		}
		if(this.getPayment_type()!=null&&this.getPayment_typeDirtyFlag()){
			map.put("payment_type",this.getPayment_type());
		}else if(this.getPayment_typeDirtyFlag()){
			map.put("payment_type",false);
		}
		if(this.getShow_partner_bank_account()!=null&&this.getShow_partner_bank_accountDirtyFlag()){
			map.put("show_partner_bank_account",Boolean.parseBoolean(this.getShow_partner_bank_account()));		
		}		if(this.getWriteoff_account_id()!=null&&this.getWriteoff_account_idDirtyFlag()){
			map.put("writeoff_account_id",this.getWriteoff_account_id());
		}else if(this.getWriteoff_account_idDirtyFlag()){
			map.put("writeoff_account_id",false);
		}
		if(this.getWriteoff_account_id_text()!=null&&this.getWriteoff_account_id_textDirtyFlag()){
			//忽略文本外键writeoff_account_id_text
		}else if(this.getWriteoff_account_id_textDirtyFlag()){
			map.put("writeoff_account_id",false);
		}
		if(this.getWriteoff_label()!=null&&this.getWriteoff_labelDirtyFlag()){
			map.put("writeoff_label",this.getWriteoff_label());
		}else if(this.getWriteoff_labelDirtyFlag()){
			map.put("writeoff_label",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
