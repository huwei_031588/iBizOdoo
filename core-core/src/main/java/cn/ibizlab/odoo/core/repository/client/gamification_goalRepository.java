package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.gamification_goal;

/**
 * 实体[gamification_goal] 服务对象接口
 */
public interface gamification_goalRepository{


    public gamification_goal createPO() ;
        public void updateBatch(gamification_goal gamification_goal);

        public void removeBatch(String id);

        public List<gamification_goal> search();

        public void get(String id);

        public void createBatch(gamification_goal gamification_goal);

        public void update(gamification_goal gamification_goal);

        public void create(gamification_goal gamification_goal);

        public void remove(String id);


}
