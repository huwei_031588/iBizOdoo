package cn.ibizlab.odoo.core.odoo_gamification.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [游戏化用户徽章] 对象
 */
@Data
public class Gamification_badge_user extends EntityClient implements Serializable {

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 备注
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 用户
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 发送者
     */
    @JSONField(name = "sender_id_text")
    @JsonProperty("sender_id_text")
    private String senderIdText;

    /**
     * 论坛徽章等级
     */
    @JSONField(name = "level")
    @JsonProperty("level")
    private String level;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 徽章名称
     */
    @JSONField(name = "badge_name")
    @JsonProperty("badge_name")
    private String badgeName;

    /**
     * 挑战源于
     */
    @JSONField(name = "challenge_id_text")
    @JsonProperty("challenge_id_text")
    private String challengeIdText;

    /**
     * 员工
     */
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;

    /**
     * 发送者
     */
    @DEField(name = "sender_id")
    @JSONField(name = "sender_id")
    @JsonProperty("sender_id")
    private Integer senderId;

    /**
     * 徽章
     */
    @DEField(name = "badge_id")
    @JSONField(name = "badge_id")
    @JsonProperty("badge_id")
    private Integer badgeId;

    /**
     * 挑战源于
     */
    @DEField(name = "challenge_id")
    @JSONField(name = "challenge_id")
    @JsonProperty("challenge_id")
    private Integer challengeId;

    /**
     * 员工
     */
    @DEField(name = "employee_id")
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Integer employeeId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 用户
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;


    /**
     * 
     */
    @JSONField(name = "odoobadge")
    @JsonProperty("odoobadge")
    private cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge odooBadge;

    /**
     * 
     */
    @JSONField(name = "odoochallenge")
    @JsonProperty("odoochallenge")
    private cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge odooChallenge;

    /**
     * 
     */
    @JSONField(name = "odooemployee")
    @JsonProperty("odooemployee")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee odooEmployee;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoosender")
    @JsonProperty("odoosender")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooSender;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [备注]
     */
    public void setComment(String comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }
    /**
     * 设置 [发送者]
     */
    public void setSenderId(Integer senderId){
        this.senderId = senderId ;
        this.modify("sender_id",senderId);
    }
    /**
     * 设置 [徽章]
     */
    public void setBadgeId(Integer badgeId){
        this.badgeId = badgeId ;
        this.modify("badge_id",badgeId);
    }
    /**
     * 设置 [挑战源于]
     */
    public void setChallengeId(Integer challengeId){
        this.challengeId = challengeId ;
        this.modify("challenge_id",challengeId);
    }
    /**
     * 设置 [员工]
     */
    public void setEmployeeId(Integer employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }
    /**
     * 设置 [用户]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

}


