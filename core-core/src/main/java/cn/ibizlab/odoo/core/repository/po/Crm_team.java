package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_teamSearchContext;

/**
 * 实体 [销售渠道] 存储模型
 */
public interface Crm_team{

    /**
     * 商机收入
     */
    Integer getOpportunities_amount();

    void setOpportunities_amount(Integer opportunities_amount);

    /**
     * 获取 [商机收入]脏标记
     */
    boolean getOpportunities_amountDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 渠道
     */
    String getUse_opportunities();

    void setUse_opportunities(String use_opportunities);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getUse_opportunitiesDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 发票报价单
     */
    Integer getQuotations_count();

    void setQuotations_count(Integer quotations_count);

    /**
     * 获取 [发票报价单]脏标记
     */
    boolean getQuotations_countDirtyFlag();

    /**
     * 类型
     */
    String getDashboard_graph_type();

    void setDashboard_graph_type(String dashboard_graph_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getDashboard_graph_typeDirtyFlag();

    /**
     * 显示仪表
     */
    String getIs_favorite();

    void setIs_favorite(String is_favorite);

    /**
     * 获取 [显示仪表]脏标记
     */
    boolean getIs_favoriteDirtyFlag();

    /**
     * 开启的商机数
     */
    Integer getOpportunities_count();

    void setOpportunities_count(Integer opportunities_count);

    /**
     * 获取 [开启的商机数]脏标记
     */
    boolean getOpportunities_countDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 报价单
     */
    String getUse_quotations();

    void setUse_quotations(String use_quotations);

    /**
     * 获取 [报价单]脏标记
     */
    boolean getUse_quotationsDirtyFlag();

    /**
     * 回复 至
     */
    String getReply_to();

    void setReply_to(String reply_to);

    /**
     * 获取 [回复 至]脏标记
     */
    boolean getReply_toDirtyFlag();

    /**
     * 最喜欢的成员
     */
    String getFavorite_user_ids();

    void setFavorite_user_ids(String favorite_user_ids);

    /**
     * 获取 [最喜欢的成员]脏标记
     */
    boolean getFavorite_user_idsDirtyFlag();

    /**
     * 团队类型
     */
    String getTeam_type();

    void setTeam_type(String team_type);

    /**
     * 获取 [团队类型]脏标记
     */
    boolean getTeam_typeDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 内容
     */
    String getDashboard_graph_model();

    void setDashboard_graph_model(String dashboard_graph_model);

    /**
     * 获取 [内容]脏标记
     */
    boolean getDashboard_graph_modelDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 预计关闭
     */
    String getDashboard_graph_period_pipeline();

    void setDashboard_graph_period_pipeline(String dashboard_graph_period_pipeline);

    /**
     * 获取 [预计关闭]脏标记
     */
    boolean getDashboard_graph_period_pipelineDirtyFlag();

    /**
     * 遗弃购物车数量
     */
    Integer getAbandoned_carts_count();

    void setAbandoned_carts_count(Integer abandoned_carts_count);

    /**
     * 获取 [遗弃购物车数量]脏标记
     */
    boolean getAbandoned_carts_countDirtyFlag();

    /**
     * POS
     */
    String getPos_config_ids();

    void setPos_config_ids(String pos_config_ids);

    /**
     * 获取 [POS]脏标记
     */
    boolean getPos_config_idsDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 线索
     */
    String getUse_leads();

    void setUse_leads(String use_leads);

    /**
     * 获取 [线索]脏标记
     */
    boolean getUse_leadsDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 网站
     */
    String getWebsite_ids();

    void setWebsite_ids(String website_ids);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idsDirtyFlag();

    /**
     * 发票金额
     */
    Integer getQuotations_amount();

    void setQuotations_amount(Integer quotations_amount);

    /**
     * 获取 [发票金额]脏标记
     */
    boolean getQuotations_amountDirtyFlag();

    /**
     * 本月已开发票
     */
    Integer getInvoiced();

    void setInvoiced(Integer invoiced);

    /**
     * 获取 [本月已开发票]脏标记
     */
    boolean getInvoicedDirtyFlag();

    /**
     * 遗弃购物车数量
     */
    Integer getAbandoned_carts_amount();

    void setAbandoned_carts_amount(Integer abandoned_carts_amount);

    /**
     * 获取 [遗弃购物车数量]脏标记
     */
    boolean getAbandoned_carts_amountDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 发票销售单
     */
    Integer getSales_to_invoice_count();

    void setSales_to_invoice_count(Integer sales_to_invoice_count);

    /**
     * 获取 [发票销售单]脏标记
     */
    boolean getSales_to_invoice_countDirtyFlag();

    /**
     * 需要激活
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要激活]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 未分派线索
     */
    Integer getUnassigned_leads_count();

    void setUnassigned_leads_count(Integer unassigned_leads_count);

    /**
     * 获取 [未分派线索]脏标记
     */
    boolean getUnassigned_leads_countDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * POS分组
     */
    String getDashboard_graph_group_pos();

    void setDashboard_graph_group_pos(String dashboard_graph_group_pos);

    /**
     * 获取 [POS分组]脏标记
     */
    boolean getDashboard_graph_group_posDirtyFlag();

    /**
     * 渠道人员
     */
    String getMember_ids();

    void setMember_ids(String member_ids);

    /**
     * 获取 [渠道人员]脏标记
     */
    boolean getMember_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 发票目标
     */
    Integer getInvoiced_target();

    void setInvoiced_target(Integer invoiced_target);

    /**
     * 获取 [发票目标]脏标记
     */
    boolean getInvoiced_targetDirtyFlag();

    /**
     * 分组
     */
    String getDashboard_graph_group();

    void setDashboard_graph_group(String dashboard_graph_group);

    /**
     * 获取 [分组]脏标记
     */
    boolean getDashboard_graph_groupDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 数据仪表图
     */
    String getDashboard_graph_data();

    void setDashboard_graph_data(String dashboard_graph_data);

    /**
     * 获取 [数据仪表图]脏标记
     */
    boolean getDashboard_graph_dataDirtyFlag();

    /**
     * 比例
     */
    String getDashboard_graph_period();

    void setDashboard_graph_period(String dashboard_graph_period);

    /**
     * 获取 [比例]脏标记
     */
    boolean getDashboard_graph_periodDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 开放POS会议
     */
    Integer getPos_sessions_open_count();

    void setPos_sessions_open_count(Integer pos_sessions_open_count);

    /**
     * 获取 [开放POS会议]脏标记
     */
    boolean getPos_sessions_open_countDirtyFlag();

    /**
     * 设定开票目标
     */
    String getUse_invoices();

    void setUse_invoices(String use_invoices);

    /**
     * 获取 [设定开票目标]脏标记
     */
    boolean getUse_invoicesDirtyFlag();

    /**
     * 仪表板按钮
     */
    String getDashboard_button_name();

    void setDashboard_button_name(String dashboard_button_name);

    /**
     * 获取 [仪表板按钮]脏标记
     */
    boolean getDashboard_button_nameDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 分组方式
     */
    String getDashboard_graph_group_pipeline();

    void setDashboard_graph_group_pipeline(String dashboard_graph_group_pipeline);

    /**
     * 获取 [分组方式]脏标记
     */
    boolean getDashboard_graph_group_pipelineDirtyFlag();

    /**
     * 会议销售金额
     */
    Double getPos_order_amount_total();

    void setPos_order_amount_total(Double pos_order_amount_total);

    /**
     * 获取 [会议销售金额]脏标记
     */
    boolean getPos_order_amount_totalDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 销售团队
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 网域别名
     */
    String getAlias_domain();

    void setAlias_domain(String alias_domain);

    /**
     * 获取 [网域别名]脏标记
     */
    boolean getAlias_domainDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 模型别名
     */
    Integer getAlias_model_id();

    void setAlias_model_id(Integer alias_model_id);

    /**
     * 获取 [模型别名]脏标记
     */
    boolean getAlias_model_idDirtyFlag();

    /**
     * 上级模型
     */
    Integer getAlias_parent_model_id();

    void setAlias_parent_model_id(Integer alias_parent_model_id);

    /**
     * 获取 [上级模型]脏标记
     */
    boolean getAlias_parent_model_idDirtyFlag();

    /**
     * 安全联系人别名
     */
    String getAlias_contact();

    void setAlias_contact(String alias_contact);

    /**
     * 获取 [安全联系人别名]脏标记
     */
    boolean getAlias_contactDirtyFlag();

    /**
     * 所有者
     */
    Integer getAlias_user_id();

    void setAlias_user_id(Integer alias_user_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getAlias_user_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 默认值
     */
    String getAlias_defaults();

    void setAlias_defaults(String alias_defaults);

    /**
     * 获取 [默认值]脏标记
     */
    boolean getAlias_defaultsDirtyFlag();

    /**
     * 上级记录ID
     */
    Integer getAlias_parent_thread_id();

    void setAlias_parent_thread_id(Integer alias_parent_thread_id);

    /**
     * 获取 [上级记录ID]脏标记
     */
    boolean getAlias_parent_thread_idDirtyFlag();

    /**
     * 记录线索ID
     */
    Integer getAlias_force_thread_id();

    void setAlias_force_thread_id(Integer alias_force_thread_id);

    /**
     * 获取 [记录线索ID]脏标记
     */
    boolean getAlias_force_thread_idDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 团队负责人
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [团队负责人]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 别名
     */
    String getAlias_name();

    void setAlias_name(String alias_name);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_nameDirtyFlag();

    /**
     * 最后更新
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 别名
     */
    Integer getAlias_id();

    void setAlias_id(Integer alias_id);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 团队负责人
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [团队负责人]脏标记
     */
    boolean getUser_idDirtyFlag();

}
