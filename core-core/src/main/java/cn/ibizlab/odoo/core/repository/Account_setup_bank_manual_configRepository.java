package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_setup_bank_manual_config;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_setup_bank_manual_configSearchContext;

/**
 * 实体 [银行设置通用配置] 存储对象
 */
public interface Account_setup_bank_manual_configRepository extends Repository<Account_setup_bank_manual_config> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_setup_bank_manual_config> searchDefault(Account_setup_bank_manual_configSearchContext context);

    Account_setup_bank_manual_config convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config domain , Account_setup_bank_manual_config po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config convert2Domain( Account_setup_bank_manual_config po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config domain) ;

}
