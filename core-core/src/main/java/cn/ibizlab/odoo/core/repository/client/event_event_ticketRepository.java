package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.event_event_ticket;

/**
 * 实体[event_event_ticket] 服务对象接口
 */
public interface event_event_ticketRepository{


    public event_event_ticket createPO() ;
        public void create(event_event_ticket event_event_ticket);

        public void get(String id);

        public void update(event_event_ticket event_event_ticket);

        public void removeBatch(String id);

        public List<event_event_ticket> search();

        public void remove(String id);

        public void createBatch(event_event_ticket event_event_ticket);

        public void updateBatch(event_event_ticket event_event_ticket);


}
