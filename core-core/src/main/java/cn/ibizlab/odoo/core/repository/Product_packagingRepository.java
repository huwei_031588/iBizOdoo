package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_packaging;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_packagingSearchContext;

/**
 * 实体 [产品包装] 存储对象
 */
public interface Product_packagingRepository extends Repository<Product_packaging> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_packaging> searchDefault(Product_packagingSearchContext context);

    Product_packaging convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_packaging domain , Product_packaging po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_packaging convert2Domain( Product_packaging po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_packaging domain) ;

}
