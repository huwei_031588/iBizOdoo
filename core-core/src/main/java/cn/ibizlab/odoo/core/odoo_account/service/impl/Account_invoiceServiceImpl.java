package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoiceSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoiceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_invoiceOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_invoiceClientModel;

/**
 * 实体[发票] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoiceServiceImpl implements IAccount_invoiceService {

    @Autowired
    account_invoiceOdooClient account_invoiceOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_invoiceClientModel clientModel = new account_invoiceClientModel();
        clientModel.setId(id);
		account_invoiceOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_invoice et) {
        account_invoiceClientModel clientModel = convert2Model(et,null);
		account_invoiceOdooClient.create(clientModel);
        Account_invoice rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice> list){
    }

    @Override
    public boolean update(Account_invoice et) {
        account_invoiceClientModel clientModel = convert2Model(et,null);
		account_invoiceOdooClient.update(clientModel);
        Account_invoice rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_invoice> list){
    }

    @Override
    public Account_invoice get(Integer id) {
        account_invoiceClientModel clientModel = new account_invoiceClientModel();
        clientModel.setId(id);
		account_invoiceOdooClient.get(clientModel);
        Account_invoice et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_invoice();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice> searchDefault(Account_invoiceSearchContext context) {
        List<Account_invoice> list = new ArrayList<Account_invoice>();
        Page<account_invoiceClientModel> clientModelList = account_invoiceOdooClient.search(context);
        for(account_invoiceClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_invoice>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_invoiceClientModel convert2Model(Account_invoice domain , account_invoiceClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_invoiceClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("outstanding_credits_debits_widgetdirtyflag"))
                model.setOutstanding_credits_debits_widget(domain.getOutstandingCreditsDebitsWidget());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("move_namedirtyflag"))
                model.setMove_name(domain.getMoveName());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("access_urldirtyflag"))
                model.setAccess_url(domain.getAccessUrl());
            if((Boolean) domain.getExtensionparams().get("payment_move_line_idsdirtyflag"))
                model.setPayment_move_line_ids(domain.getPaymentMoveLineIds());
            if((Boolean) domain.getExtensionparams().get("origindirtyflag"))
                model.setOrigin(domain.getOrigin());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("amount_total_company_signeddirtyflag"))
                model.setAmount_total_company_signed(domain.getAmountTotalCompanySigned());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("amount_untaxed_signeddirtyflag"))
                model.setAmount_untaxed_signed(domain.getAmountUntaxedSigned());
            if((Boolean) domain.getExtensionparams().get("residualdirtyflag"))
                model.setResidual(domain.getResidual());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("has_outstandingdirtyflag"))
                model.setHas_outstanding(domain.getHasOutstanding());
            if((Boolean) domain.getExtensionparams().get("commentdirtyflag"))
                model.setComment(domain.getComment());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("amount_by_groupdirtyflag"))
                model.setAmount_by_group(domain.getAmountByGroup());
            if((Boolean) domain.getExtensionparams().get("payments_widgetdirtyflag"))
                model.setPayments_widget(domain.getPaymentsWidget());
            if((Boolean) domain.getExtensionparams().get("reconcileddirtyflag"))
                model.setReconciled(domain.getReconciled());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("sentdirtyflag"))
                model.setSent(domain.getSent());
            if((Boolean) domain.getExtensionparams().get("amount_total_signeddirtyflag"))
                model.setAmount_total_signed(domain.getAmountTotalSigned());
            if((Boolean) domain.getExtensionparams().get("invoice_line_idsdirtyflag"))
                model.setInvoice_line_ids(domain.getInvoiceLineIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("access_warningdirtyflag"))
                model.setAccess_warning(domain.getAccessWarning());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("residual_signeddirtyflag"))
                model.setResidual_signed(domain.getResidualSigned());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("access_tokendirtyflag"))
                model.setAccess_token(domain.getAccessToken());
            if((Boolean) domain.getExtensionparams().get("vendor_display_namedirtyflag"))
                model.setVendor_display_name(domain.getVendorDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("amount_untaxeddirtyflag"))
                model.setAmount_untaxed(domain.getAmountUntaxed());
            if((Boolean) domain.getExtensionparams().get("transaction_idsdirtyflag"))
                model.setTransaction_ids(domain.getTransactionIds());
            if((Boolean) domain.getExtensionparams().get("date_duedirtyflag"))
                model.setDate_due(domain.getDateDue());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("authorized_transaction_idsdirtyflag"))
                model.setAuthorized_transaction_ids(domain.getAuthorizedTransactionIds());
            if((Boolean) domain.getExtensionparams().get("tax_line_idsdirtyflag"))
                model.setTax_line_ids(domain.getTaxLineIds());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("source_emaildirtyflag"))
                model.setSource_email(domain.getSourceEmail());
            if((Boolean) domain.getExtensionparams().get("amount_taxdirtyflag"))
                model.setAmount_tax(domain.getAmountTax());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("amount_untaxed_invoice_signeddirtyflag"))
                model.setAmount_untaxed_invoice_signed(domain.getAmountUntaxedInvoiceSigned());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("sequence_number_next_prefixdirtyflag"))
                model.setSequence_number_next_prefix(domain.getSequenceNumberNextPrefix());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("refund_invoice_idsdirtyflag"))
                model.setRefund_invoice_ids(domain.getRefundInvoiceIds());
            if((Boolean) domain.getExtensionparams().get("residual_company_signeddirtyflag"))
                model.setResidual_company_signed(domain.getResidualCompanySigned());
            if((Boolean) domain.getExtensionparams().get("date_invoicedirtyflag"))
                model.setDate_invoice(domain.getDateInvoice());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("referencedirtyflag"))
                model.setReference(domain.getReference());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("sequence_number_nextdirtyflag"))
                model.setSequence_number_next(domain.getSequenceNumberNext());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("amount_tax_signeddirtyflag"))
                model.setAmount_tax_signed(domain.getAmountTaxSigned());
            if((Boolean) domain.getExtensionparams().get("invoice_icondirtyflag"))
                model.setInvoice_icon(domain.getInvoiceIcon());
            if((Boolean) domain.getExtensionparams().get("payment_idsdirtyflag"))
                model.setPayment_ids(domain.getPaymentIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("amount_totaldirtyflag"))
                model.setAmount_total(domain.getAmountTotal());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("purchase_id_textdirtyflag"))
                model.setPurchase_id_text(domain.getPurchaseIdText());
            if((Boolean) domain.getExtensionparams().get("vendor_bill_purchase_id_textdirtyflag"))
                model.setVendor_bill_purchase_id_text(domain.getVendorBillPurchaseIdText());
            if((Boolean) domain.getExtensionparams().get("source_id_textdirtyflag"))
                model.setSource_id_text(domain.getSourceIdText());
            if((Boolean) domain.getExtensionparams().get("campaign_id_textdirtyflag"))
                model.setCampaign_id_text(domain.getCampaignIdText());
            if((Boolean) domain.getExtensionparams().get("partner_shipping_id_textdirtyflag"))
                model.setPartner_shipping_id_text(domain.getPartnerShippingIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("incoterms_id_textdirtyflag"))
                model.setIncoterms_id_text(domain.getIncotermsIdText());
            if((Boolean) domain.getExtensionparams().get("company_currency_iddirtyflag"))
                model.setCompany_currency_id(domain.getCompanyCurrencyId());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("incoterm_id_textdirtyflag"))
                model.setIncoterm_id_text(domain.getIncotermIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("medium_id_textdirtyflag"))
                model.setMedium_id_text(domain.getMediumIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("cash_rounding_id_textdirtyflag"))
                model.setCash_rounding_id_text(domain.getCashRoundingIdText());
            if((Boolean) domain.getExtensionparams().get("vendor_bill_id_textdirtyflag"))
                model.setVendor_bill_id_text(domain.getVendorBillIdText());
            if((Boolean) domain.getExtensionparams().get("refund_invoice_id_textdirtyflag"))
                model.setRefund_invoice_id_text(domain.getRefundInvoiceIdText());
            if((Boolean) domain.getExtensionparams().get("fiscal_position_id_textdirtyflag"))
                model.setFiscal_position_id_text(domain.getFiscalPositionIdText());
            if((Boolean) domain.getExtensionparams().get("numberdirtyflag"))
                model.setNumber(domain.getNumber());
            if((Boolean) domain.getExtensionparams().get("commercial_partner_id_textdirtyflag"))
                model.setCommercial_partner_id_text(domain.getCommercialPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("team_id_textdirtyflag"))
                model.setTeam_id_text(domain.getTeamIdText());
            if((Boolean) domain.getExtensionparams().get("payment_term_id_textdirtyflag"))
                model.setPayment_term_id_text(domain.getPaymentTermIdText());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("team_iddirtyflag"))
                model.setTeam_id(domain.getTeamId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("medium_iddirtyflag"))
                model.setMedium_id(domain.getMediumId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("move_iddirtyflag"))
                model.setMove_id(domain.getMoveId());
            if((Boolean) domain.getExtensionparams().get("payment_term_iddirtyflag"))
                model.setPayment_term_id(domain.getPaymentTermId());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("incoterm_iddirtyflag"))
                model.setIncoterm_id(domain.getIncotermId());
            if((Boolean) domain.getExtensionparams().get("purchase_iddirtyflag"))
                model.setPurchase_id(domain.getPurchaseId());
            if((Boolean) domain.getExtensionparams().get("fiscal_position_iddirtyflag"))
                model.setFiscal_position_id(domain.getFiscalPositionId());
            if((Boolean) domain.getExtensionparams().get("refund_invoice_iddirtyflag"))
                model.setRefund_invoice_id(domain.getRefundInvoiceId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("partner_bank_iddirtyflag"))
                model.setPartner_bank_id(domain.getPartnerBankId());
            if((Boolean) domain.getExtensionparams().get("cash_rounding_iddirtyflag"))
                model.setCash_rounding_id(domain.getCashRoundingId());
            if((Boolean) domain.getExtensionparams().get("commercial_partner_iddirtyflag"))
                model.setCommercial_partner_id(domain.getCommercialPartnerId());
            if((Boolean) domain.getExtensionparams().get("partner_shipping_iddirtyflag"))
                model.setPartner_shipping_id(domain.getPartnerShippingId());
            if((Boolean) domain.getExtensionparams().get("incoterms_iddirtyflag"))
                model.setIncoterms_id(domain.getIncotermsId());
            if((Boolean) domain.getExtensionparams().get("source_iddirtyflag"))
                model.setSource_id(domain.getSourceId());
            if((Boolean) domain.getExtensionparams().get("vendor_bill_iddirtyflag"))
                model.setVendor_bill_id(domain.getVendorBillId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("vendor_bill_purchase_iddirtyflag"))
                model.setVendor_bill_purchase_id(domain.getVendorBillPurchaseId());
            if((Boolean) domain.getExtensionparams().get("campaign_iddirtyflag"))
                model.setCampaign_id(domain.getCampaignId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_invoice convert2Domain( account_invoiceClientModel model ,Account_invoice domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_invoice();
        }

        if(model.getOutstanding_credits_debits_widgetDirtyFlag())
            domain.setOutstandingCreditsDebitsWidget(model.getOutstanding_credits_debits_widget());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMove_nameDirtyFlag())
            domain.setMoveName(model.getMove_name());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getAccess_urlDirtyFlag())
            domain.setAccessUrl(model.getAccess_url());
        if(model.getPayment_move_line_idsDirtyFlag())
            domain.setPaymentMoveLineIds(model.getPayment_move_line_ids());
        if(model.getOriginDirtyFlag())
            domain.setOrigin(model.getOrigin());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getAmount_total_company_signedDirtyFlag())
            domain.setAmountTotalCompanySigned(model.getAmount_total_company_signed());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getAmount_untaxed_signedDirtyFlag())
            domain.setAmountUntaxedSigned(model.getAmount_untaxed_signed());
        if(model.getResidualDirtyFlag())
            domain.setResidual(model.getResidual());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getHas_outstandingDirtyFlag())
            domain.setHasOutstanding(model.getHas_outstanding());
        if(model.getCommentDirtyFlag())
            domain.setComment(model.getComment());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getAmount_by_groupDirtyFlag())
            domain.setAmountByGroup(model.getAmount_by_group());
        if(model.getPayments_widgetDirtyFlag())
            domain.setPaymentsWidget(model.getPayments_widget());
        if(model.getReconciledDirtyFlag())
            domain.setReconciled(model.getReconciled());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getSentDirtyFlag())
            domain.setSent(model.getSent());
        if(model.getAmount_total_signedDirtyFlag())
            domain.setAmountTotalSigned(model.getAmount_total_signed());
        if(model.getInvoice_line_idsDirtyFlag())
            domain.setInvoiceLineIds(model.getInvoice_line_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAccess_warningDirtyFlag())
            domain.setAccessWarning(model.getAccess_warning());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getResidual_signedDirtyFlag())
            domain.setResidualSigned(model.getResidual_signed());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getAccess_tokenDirtyFlag())
            domain.setAccessToken(model.getAccess_token());
        if(model.getVendor_display_nameDirtyFlag())
            domain.setVendorDisplayName(model.getVendor_display_name());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getAmount_untaxedDirtyFlag())
            domain.setAmountUntaxed(model.getAmount_untaxed());
        if(model.getTransaction_idsDirtyFlag())
            domain.setTransactionIds(model.getTransaction_ids());
        if(model.getDate_dueDirtyFlag())
            domain.setDateDue(model.getDate_due());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getAuthorized_transaction_idsDirtyFlag())
            domain.setAuthorizedTransactionIds(model.getAuthorized_transaction_ids());
        if(model.getTax_line_idsDirtyFlag())
            domain.setTaxLineIds(model.getTax_line_ids());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getSource_emailDirtyFlag())
            domain.setSourceEmail(model.getSource_email());
        if(model.getAmount_taxDirtyFlag())
            domain.setAmountTax(model.getAmount_tax());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getAmount_untaxed_invoice_signedDirtyFlag())
            domain.setAmountUntaxedInvoiceSigned(model.getAmount_untaxed_invoice_signed());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getSequence_number_next_prefixDirtyFlag())
            domain.setSequenceNumberNextPrefix(model.getSequence_number_next_prefix());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getRefund_invoice_idsDirtyFlag())
            domain.setRefundInvoiceIds(model.getRefund_invoice_ids());
        if(model.getResidual_company_signedDirtyFlag())
            domain.setResidualCompanySigned(model.getResidual_company_signed());
        if(model.getDate_invoiceDirtyFlag())
            domain.setDateInvoice(model.getDate_invoice());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getReferenceDirtyFlag())
            domain.setReference(model.getReference());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getSequence_number_nextDirtyFlag())
            domain.setSequenceNumberNext(model.getSequence_number_next());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getAmount_tax_signedDirtyFlag())
            domain.setAmountTaxSigned(model.getAmount_tax_signed());
        if(model.getInvoice_iconDirtyFlag())
            domain.setInvoiceIcon(model.getInvoice_icon());
        if(model.getPayment_idsDirtyFlag())
            domain.setPaymentIds(model.getPayment_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getAmount_totalDirtyFlag())
            domain.setAmountTotal(model.getAmount_total());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getPurchase_id_textDirtyFlag())
            domain.setPurchaseIdText(model.getPurchase_id_text());
        if(model.getVendor_bill_purchase_id_textDirtyFlag())
            domain.setVendorBillPurchaseIdText(model.getVendor_bill_purchase_id_text());
        if(model.getSource_id_textDirtyFlag())
            domain.setSourceIdText(model.getSource_id_text());
        if(model.getCampaign_id_textDirtyFlag())
            domain.setCampaignIdText(model.getCampaign_id_text());
        if(model.getPartner_shipping_id_textDirtyFlag())
            domain.setPartnerShippingIdText(model.getPartner_shipping_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getIncoterms_id_textDirtyFlag())
            domain.setIncotermsIdText(model.getIncoterms_id_text());
        if(model.getCompany_currency_idDirtyFlag())
            domain.setCompanyCurrencyId(model.getCompany_currency_id());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getIncoterm_id_textDirtyFlag())
            domain.setIncotermIdText(model.getIncoterm_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getMedium_id_textDirtyFlag())
            domain.setMediumIdText(model.getMedium_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCash_rounding_id_textDirtyFlag())
            domain.setCashRoundingIdText(model.getCash_rounding_id_text());
        if(model.getVendor_bill_id_textDirtyFlag())
            domain.setVendorBillIdText(model.getVendor_bill_id_text());
        if(model.getRefund_invoice_id_textDirtyFlag())
            domain.setRefundInvoiceIdText(model.getRefund_invoice_id_text());
        if(model.getFiscal_position_id_textDirtyFlag())
            domain.setFiscalPositionIdText(model.getFiscal_position_id_text());
        if(model.getNumberDirtyFlag())
            domain.setNumber(model.getNumber());
        if(model.getCommercial_partner_id_textDirtyFlag())
            domain.setCommercialPartnerIdText(model.getCommercial_partner_id_text());
        if(model.getTeam_id_textDirtyFlag())
            domain.setTeamIdText(model.getTeam_id_text());
        if(model.getPayment_term_id_textDirtyFlag())
            domain.setPaymentTermIdText(model.getPayment_term_id_text());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getTeam_idDirtyFlag())
            domain.setTeamId(model.getTeam_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getMedium_idDirtyFlag())
            domain.setMediumId(model.getMedium_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getMove_idDirtyFlag())
            domain.setMoveId(model.getMove_id());
        if(model.getPayment_term_idDirtyFlag())
            domain.setPaymentTermId(model.getPayment_term_id());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getIncoterm_idDirtyFlag())
            domain.setIncotermId(model.getIncoterm_id());
        if(model.getPurchase_idDirtyFlag())
            domain.setPurchaseId(model.getPurchase_id());
        if(model.getFiscal_position_idDirtyFlag())
            domain.setFiscalPositionId(model.getFiscal_position_id());
        if(model.getRefund_invoice_idDirtyFlag())
            domain.setRefundInvoiceId(model.getRefund_invoice_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getPartner_bank_idDirtyFlag())
            domain.setPartnerBankId(model.getPartner_bank_id());
        if(model.getCash_rounding_idDirtyFlag())
            domain.setCashRoundingId(model.getCash_rounding_id());
        if(model.getCommercial_partner_idDirtyFlag())
            domain.setCommercialPartnerId(model.getCommercial_partner_id());
        if(model.getPartner_shipping_idDirtyFlag())
            domain.setPartnerShippingId(model.getPartner_shipping_id());
        if(model.getIncoterms_idDirtyFlag())
            domain.setIncotermsId(model.getIncoterms_id());
        if(model.getSource_idDirtyFlag())
            domain.setSourceId(model.getSource_id());
        if(model.getVendor_bill_idDirtyFlag())
            domain.setVendorBillId(model.getVendor_bill_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getVendor_bill_purchase_idDirtyFlag())
            domain.setVendorBillPurchaseId(model.getVendor_bill_purchase_id());
        if(model.getCampaign_idDirtyFlag())
            domain.setCampaignId(model.getCampaign_id());
        return domain ;
    }

}

    



