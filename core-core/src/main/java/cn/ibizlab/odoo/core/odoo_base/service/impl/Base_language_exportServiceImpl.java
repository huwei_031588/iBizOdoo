package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_export;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_exportSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_exportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.base_language_exportOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.base_language_exportClientModel;

/**
 * 实体[语言输出] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_language_exportServiceImpl implements IBase_language_exportService {

    @Autowired
    base_language_exportOdooClient base_language_exportOdooClient;


    @Override
    public boolean remove(Integer id) {
        base_language_exportClientModel clientModel = new base_language_exportClientModel();
        clientModel.setId(id);
		base_language_exportOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Base_language_export get(Integer id) {
        base_language_exportClientModel clientModel = new base_language_exportClientModel();
        clientModel.setId(id);
		base_language_exportOdooClient.get(clientModel);
        Base_language_export et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Base_language_export();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Base_language_export et) {
        base_language_exportClientModel clientModel = convert2Model(et,null);
		base_language_exportOdooClient.update(clientModel);
        Base_language_export rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Base_language_export> list){
    }

    @Override
    public boolean create(Base_language_export et) {
        base_language_exportClientModel clientModel = convert2Model(et,null);
		base_language_exportOdooClient.create(clientModel);
        Base_language_export rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_language_export> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_language_export> searchDefault(Base_language_exportSearchContext context) {
        List<Base_language_export> list = new ArrayList<Base_language_export>();
        Page<base_language_exportClientModel> clientModelList = base_language_exportOdooClient.search(context);
        for(base_language_exportClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Base_language_export>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public base_language_exportClientModel convert2Model(Base_language_export domain , base_language_exportClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new base_language_exportClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("langdirtyflag"))
                model.setLang(domain.getLang());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("formatdirtyflag"))
                model.setFormat(domain.getFormat());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("datadirtyflag"))
                model.setData(domain.getData());
            if((Boolean) domain.getExtensionparams().get("modulesdirtyflag"))
                model.setModules(domain.getModules());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Base_language_export convert2Domain( base_language_exportClientModel model ,Base_language_export domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Base_language_export();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getLangDirtyFlag())
            domain.setLang(model.getLang());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getFormatDirtyFlag())
            domain.setFormat(model.getFormat());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getDataDirtyFlag())
            domain.setData(model.getData());
        if(model.getModulesDirtyFlag())
            domain.setModules(model.getModules());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



