package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_categoryOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_categoryClientModel;

/**
 * 实体[产品种类] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_categoryServiceImpl implements IProduct_categoryService {

    @Autowired
    product_categoryOdooClient product_categoryOdooClient;


    @Override
    public boolean remove(Integer id) {
        product_categoryClientModel clientModel = new product_categoryClientModel();
        clientModel.setId(id);
		product_categoryOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Product_category et) {
        product_categoryClientModel clientModel = convert2Model(et,null);
		product_categoryOdooClient.create(clientModel);
        Product_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_category> list){
    }

    @Override
    public boolean update(Product_category et) {
        product_categoryClientModel clientModel = convert2Model(et,null);
		product_categoryOdooClient.update(clientModel);
        Product_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_category> list){
    }

    @Override
    public Product_category get(Integer id) {
        product_categoryClientModel clientModel = new product_categoryClientModel();
        clientModel.setId(id);
		product_categoryOdooClient.get(clientModel);
        Product_category et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_category();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_category> searchDefault(Product_categorySearchContext context) {
        List<Product_category> list = new ArrayList<Product_category>();
        Page<product_categoryClientModel> clientModelList = product_categoryOdooClient.search(context);
        for(product_categoryClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_category>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_categoryClientModel convert2Model(Product_category domain , product_categoryClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_categoryClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("property_stock_valuation_account_iddirtyflag"))
                model.setProperty_stock_valuation_account_id(domain.getPropertyStockValuationAccountId());
            if((Boolean) domain.getExtensionparams().get("child_iddirtyflag"))
                model.setChild_id(domain.getChildId());
            if((Boolean) domain.getExtensionparams().get("product_countdirtyflag"))
                model.setProduct_count(domain.getProductCount());
            if((Boolean) domain.getExtensionparams().get("route_idsdirtyflag"))
                model.setRoute_ids(domain.getRouteIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("property_valuationdirtyflag"))
                model.setProperty_valuation(domain.getPropertyValuation());
            if((Boolean) domain.getExtensionparams().get("parent_pathdirtyflag"))
                model.setParent_path(domain.getParentPath());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("complete_namedirtyflag"))
                model.setComplete_name(domain.getCompleteName());
            if((Boolean) domain.getExtensionparams().get("property_account_creditor_price_difference_categdirtyflag"))
                model.setProperty_account_creditor_price_difference_categ(domain.getPropertyAccountCreditorPriceDifferenceCateg());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_output_categ_iddirtyflag"))
                model.setProperty_stock_account_output_categ_id(domain.getPropertyStockAccountOutputCategId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("total_route_idsdirtyflag"))
                model.setTotal_route_ids(domain.getTotalRouteIds());
            if((Boolean) domain.getExtensionparams().get("property_cost_methoddirtyflag"))
                model.setProperty_cost_method(domain.getPropertyCostMethod());
            if((Boolean) domain.getExtensionparams().get("property_account_income_categ_iddirtyflag"))
                model.setProperty_account_income_categ_id(domain.getPropertyAccountIncomeCategId());
            if((Boolean) domain.getExtensionparams().get("property_account_expense_categ_iddirtyflag"))
                model.setProperty_account_expense_categ_id(domain.getPropertyAccountExpenseCategId());
            if((Boolean) domain.getExtensionparams().get("property_stock_journaldirtyflag"))
                model.setProperty_stock_journal(domain.getPropertyStockJournal());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_input_categ_iddirtyflag"))
                model.setProperty_stock_account_input_categ_id(domain.getPropertyStockAccountInputCategId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("removal_strategy_id_textdirtyflag"))
                model.setRemoval_strategy_id_text(domain.getRemovalStrategyIdText());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("removal_strategy_iddirtyflag"))
                model.setRemoval_strategy_id(domain.getRemovalStrategyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_category convert2Domain( product_categoryClientModel model ,Product_category domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_category();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getProperty_stock_valuation_account_idDirtyFlag())
            domain.setPropertyStockValuationAccountId(model.getProperty_stock_valuation_account_id());
        if(model.getChild_idDirtyFlag())
            domain.setChildId(model.getChild_id());
        if(model.getProduct_countDirtyFlag())
            domain.setProductCount(model.getProduct_count());
        if(model.getRoute_idsDirtyFlag())
            domain.setRouteIds(model.getRoute_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getProperty_valuationDirtyFlag())
            domain.setPropertyValuation(model.getProperty_valuation());
        if(model.getParent_pathDirtyFlag())
            domain.setParentPath(model.getParent_path());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getComplete_nameDirtyFlag())
            domain.setCompleteName(model.getComplete_name());
        if(model.getProperty_account_creditor_price_difference_categDirtyFlag())
            domain.setPropertyAccountCreditorPriceDifferenceCateg(model.getProperty_account_creditor_price_difference_categ());
        if(model.getProperty_stock_account_output_categ_idDirtyFlag())
            domain.setPropertyStockAccountOutputCategId(model.getProperty_stock_account_output_categ_id());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getTotal_route_idsDirtyFlag())
            domain.setTotalRouteIds(model.getTotal_route_ids());
        if(model.getProperty_cost_methodDirtyFlag())
            domain.setPropertyCostMethod(model.getProperty_cost_method());
        if(model.getProperty_account_income_categ_idDirtyFlag())
            domain.setPropertyAccountIncomeCategId(model.getProperty_account_income_categ_id());
        if(model.getProperty_account_expense_categ_idDirtyFlag())
            domain.setPropertyAccountExpenseCategId(model.getProperty_account_expense_categ_id());
        if(model.getProperty_stock_journalDirtyFlag())
            domain.setPropertyStockJournal(model.getProperty_stock_journal());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getProperty_stock_account_input_categ_idDirtyFlag())
            domain.setPropertyStockAccountInputCategId(model.getProperty_stock_account_input_categ_id());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getRemoval_strategy_id_textDirtyFlag())
            domain.setRemovalStrategyIdText(model.getRemoval_strategy_id_text());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getRemoval_strategy_idDirtyFlag())
            domain.setRemovalStrategyId(model.getRemoval_strategy_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



