package cn.ibizlab.odoo.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipment_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_equipment_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_maintenance.client.maintenance_equipment_categoryOdooClient;
import cn.ibizlab.odoo.core.odoo_maintenance.clientmodel.maintenance_equipment_categoryClientModel;

/**
 * 实体[维护设备类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Maintenance_equipment_categoryServiceImpl implements IMaintenance_equipment_categoryService {

    @Autowired
    maintenance_equipment_categoryOdooClient maintenance_equipment_categoryOdooClient;


    @Override
    public boolean update(Maintenance_equipment_category et) {
        maintenance_equipment_categoryClientModel clientModel = convert2Model(et,null);
		maintenance_equipment_categoryOdooClient.update(clientModel);
        Maintenance_equipment_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Maintenance_equipment_category> list){
    }

    @Override
    public boolean create(Maintenance_equipment_category et) {
        maintenance_equipment_categoryClientModel clientModel = convert2Model(et,null);
		maintenance_equipment_categoryOdooClient.create(clientModel);
        Maintenance_equipment_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Maintenance_equipment_category> list){
    }

    @Override
    public Maintenance_equipment_category get(Integer id) {
        maintenance_equipment_categoryClientModel clientModel = new maintenance_equipment_categoryClientModel();
        clientModel.setId(id);
		maintenance_equipment_categoryOdooClient.get(clientModel);
        Maintenance_equipment_category et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Maintenance_equipment_category();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        maintenance_equipment_categoryClientModel clientModel = new maintenance_equipment_categoryClientModel();
        clientModel.setId(id);
		maintenance_equipment_categoryOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Maintenance_equipment_category> searchDefault(Maintenance_equipment_categorySearchContext context) {
        List<Maintenance_equipment_category> list = new ArrayList<Maintenance_equipment_category>();
        Page<maintenance_equipment_categoryClientModel> clientModelList = maintenance_equipment_categoryOdooClient.search(context);
        for(maintenance_equipment_categoryClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Maintenance_equipment_category>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public maintenance_equipment_categoryClientModel convert2Model(Maintenance_equipment_category domain , maintenance_equipment_categoryClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new maintenance_equipment_categoryClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("equipment_idsdirtyflag"))
                model.setEquipment_ids(domain.getEquipmentIds());
            if((Boolean) domain.getExtensionparams().get("equipment_countdirtyflag"))
                model.setEquipment_count(domain.getEquipmentCount());
            if((Boolean) domain.getExtensionparams().get("maintenance_idsdirtyflag"))
                model.setMaintenance_ids(domain.getMaintenanceIds());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("folddirtyflag"))
                model.setFold(domain.getFold());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("maintenance_countdirtyflag"))
                model.setMaintenance_count(domain.getMaintenanceCount());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("technician_user_id_textdirtyflag"))
                model.setTechnician_user_id_text(domain.getTechnicianUserIdText());
            if((Boolean) domain.getExtensionparams().get("alias_user_iddirtyflag"))
                model.setAlias_user_id(domain.getAliasUserId());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("alias_force_thread_iddirtyflag"))
                model.setAlias_force_thread_id(domain.getAliasForceThreadId());
            if((Boolean) domain.getExtensionparams().get("alias_parent_thread_iddirtyflag"))
                model.setAlias_parent_thread_id(domain.getAliasParentThreadId());
            if((Boolean) domain.getExtensionparams().get("alias_domaindirtyflag"))
                model.setAlias_domain(domain.getAliasDomain());
            if((Boolean) domain.getExtensionparams().get("alias_contactdirtyflag"))
                model.setAlias_contact(domain.getAliasContact());
            if((Boolean) domain.getExtensionparams().get("alias_defaultsdirtyflag"))
                model.setAlias_defaults(domain.getAliasDefaults());
            if((Boolean) domain.getExtensionparams().get("alias_namedirtyflag"))
                model.setAlias_name(domain.getAliasName());
            if((Boolean) domain.getExtensionparams().get("alias_model_iddirtyflag"))
                model.setAlias_model_id(domain.getAliasModelId());
            if((Boolean) domain.getExtensionparams().get("alias_parent_model_iddirtyflag"))
                model.setAlias_parent_model_id(domain.getAliasParentModelId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("alias_iddirtyflag"))
                model.setAlias_id(domain.getAliasId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("technician_user_iddirtyflag"))
                model.setTechnician_user_id(domain.getTechnicianUserId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Maintenance_equipment_category convert2Domain( maintenance_equipment_categoryClientModel model ,Maintenance_equipment_category domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Maintenance_equipment_category();
        }

        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getEquipment_idsDirtyFlag())
            domain.setEquipmentIds(model.getEquipment_ids());
        if(model.getEquipment_countDirtyFlag())
            domain.setEquipmentCount(model.getEquipment_count());
        if(model.getMaintenance_idsDirtyFlag())
            domain.setMaintenanceIds(model.getMaintenance_ids());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getFoldDirtyFlag())
            domain.setFold(model.getFold());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMaintenance_countDirtyFlag())
            domain.setMaintenanceCount(model.getMaintenance_count());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getTechnician_user_id_textDirtyFlag())
            domain.setTechnicianUserIdText(model.getTechnician_user_id_text());
        if(model.getAlias_user_idDirtyFlag())
            domain.setAliasUserId(model.getAlias_user_id());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getAlias_force_thread_idDirtyFlag())
            domain.setAliasForceThreadId(model.getAlias_force_thread_id());
        if(model.getAlias_parent_thread_idDirtyFlag())
            domain.setAliasParentThreadId(model.getAlias_parent_thread_id());
        if(model.getAlias_domainDirtyFlag())
            domain.setAliasDomain(model.getAlias_domain());
        if(model.getAlias_contactDirtyFlag())
            domain.setAliasContact(model.getAlias_contact());
        if(model.getAlias_defaultsDirtyFlag())
            domain.setAliasDefaults(model.getAlias_defaults());
        if(model.getAlias_nameDirtyFlag())
            domain.setAliasName(model.getAlias_name());
        if(model.getAlias_model_idDirtyFlag())
            domain.setAliasModelId(model.getAlias_model_id());
        if(model.getAlias_parent_model_idDirtyFlag())
            domain.setAliasParentModelId(model.getAlias_parent_model_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getAlias_idDirtyFlag())
            domain.setAliasId(model.getAlias_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getTechnician_user_idDirtyFlag())
            domain.setTechnicianUserId(model.getTechnician_user_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



