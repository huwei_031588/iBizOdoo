package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Digest_digest;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_digestSearchContext;

/**
 * 实体 [摘要] 存储对象
 */
public interface Digest_digestRepository extends Repository<Digest_digest> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Digest_digest> searchDefault(Digest_digestSearchContext context);

    Digest_digest convert2PO(cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest domain , Digest_digest po) ;

    cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest convert2Domain( Digest_digest po ,cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest domain) ;

}
