package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_service_type;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_service_typeSearchContext;

/**
 * 实体 [车辆服务类型] 存储对象
 */
public interface Fleet_service_typeRepository extends Repository<Fleet_service_type> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_service_type> searchDefault(Fleet_service_typeSearchContext context);

    Fleet_service_type convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_service_type domain , Fleet_service_type po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_service_type convert2Domain( Fleet_service_type po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_service_type domain) ;

}
