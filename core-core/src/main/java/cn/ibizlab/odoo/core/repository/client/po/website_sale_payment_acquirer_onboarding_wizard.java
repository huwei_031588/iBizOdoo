package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [website_sale_payment_acquirer_onboarding_wizard] 对象
 */
public interface website_sale_payment_acquirer_onboarding_wizard {

    public String getAcc_number();

    public void setAcc_number(String acc_number);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getJournal_name();

    public void setJournal_name(String journal_name);

    public String getManual_name();

    public void setManual_name(String manual_name);

    public String getManual_post_msg();

    public void setManual_post_msg(String manual_post_msg);

    public String getPayment_method();

    public void setPayment_method(String payment_method);

    public String getPaypal_email_account();

    public void setPaypal_email_account(String paypal_email_account);

    public String getPaypal_pdt_token();

    public void setPaypal_pdt_token(String paypal_pdt_token);

    public String getPaypal_seller_account();

    public void setPaypal_seller_account(String paypal_seller_account);

    public String getStripe_publishable_key();

    public void setStripe_publishable_key(String stripe_publishable_key);

    public String getStripe_secret_key();

    public void setStripe_secret_key(String stripe_secret_key);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
