package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventorySearchContext;

/**
 * 实体 [库存] 存储模型
 */
public interface Stock_inventory{

    /**
     * 盘点
     */
    String getLine_ids();

    void setLine_ids(String line_ids);

    /**
     * 获取 [盘点]脏标记
     */
    boolean getLine_idsDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 库存
     */
    String getFilter();

    void setFilter(String filter);

    /**
     * 获取 [库存]脏标记
     */
    boolean getFilterDirtyFlag();

    /**
     * 包含短缺的产品
     */
    String getExhausted();

    void setExhausted(String exhausted);

    /**
     * 获取 [包含短缺的产品]脏标记
     */
    boolean getExhaustedDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 库存编号
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [库存编号]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 数量总计
     */
    Double getTotal_qty();

    void setTotal_qty(Double total_qty);

    /**
     * 获取 [数量总计]脏标记
     */
    boolean getTotal_qtyDirtyFlag();

    /**
     * 会计日期
     */
    Timestamp getAccounting_date();

    void setAccounting_date(Timestamp accounting_date);

    /**
     * 获取 [会计日期]脏标记
     */
    boolean getAccounting_dateDirtyFlag();

    /**
     * 创建的移动
     */
    String getMove_ids();

    void setMove_ids(String move_ids);

    /**
     * 获取 [创建的移动]脏标记
     */
    boolean getMove_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 库存日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [库存日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 产品分类
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [产品分类]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 已盘点批次/序列号
     */
    String getLot_id_text();

    void setLot_id_text(String lot_id_text);

    /**
     * 获取 [已盘点批次/序列号]脏标记
     */
    boolean getLot_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 已盘点位置
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [已盘点位置]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 已盘点所有者
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [已盘点所有者]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 已盘点包裹
     */
    String getPackage_id_text();

    void setPackage_id_text(String package_id_text);

    /**
     * 获取 [已盘点包裹]脏标记
     */
    boolean getPackage_id_textDirtyFlag();

    /**
     * 已盘点产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [已盘点产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 已盘点产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [已盘点产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 已盘点位置
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [已盘点位置]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 已盘点批次/序列号
     */
    Integer getLot_id();

    void setLot_id(Integer lot_id);

    /**
     * 获取 [已盘点批次/序列号]脏标记
     */
    boolean getLot_idDirtyFlag();

    /**
     * 已盘点包裹
     */
    Integer getPackage_id();

    void setPackage_id(Integer package_id);

    /**
     * 获取 [已盘点包裹]脏标记
     */
    boolean getPackage_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 产品分类
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [产品分类]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 已盘点所有者
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [已盘点所有者]脏标记
     */
    boolean getPartner_idDirtyFlag();

}
