package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee_category;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employee_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_employee_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_employee_categoryOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_employee_categoryClientModel;

/**
 * 实体[员工类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_employee_categoryServiceImpl implements IHr_employee_categoryService {

    @Autowired
    hr_employee_categoryOdooClient hr_employee_categoryOdooClient;


    @Override
    public boolean create(Hr_employee_category et) {
        hr_employee_categoryClientModel clientModel = convert2Model(et,null);
		hr_employee_categoryOdooClient.create(clientModel);
        Hr_employee_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_employee_category> list){
    }

    @Override
    public boolean remove(Integer id) {
        hr_employee_categoryClientModel clientModel = new hr_employee_categoryClientModel();
        clientModel.setId(id);
		hr_employee_categoryOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Hr_employee_category et) {
        hr_employee_categoryClientModel clientModel = convert2Model(et,null);
		hr_employee_categoryOdooClient.update(clientModel);
        Hr_employee_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_employee_category> list){
    }

    @Override
    public Hr_employee_category get(Integer id) {
        hr_employee_categoryClientModel clientModel = new hr_employee_categoryClientModel();
        clientModel.setId(id);
		hr_employee_categoryOdooClient.get(clientModel);
        Hr_employee_category et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_employee_category();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_employee_category> searchDefault(Hr_employee_categorySearchContext context) {
        List<Hr_employee_category> list = new ArrayList<Hr_employee_category>();
        Page<hr_employee_categoryClientModel> clientModelList = hr_employee_categoryOdooClient.search(context);
        for(hr_employee_categoryClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_employee_category>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_employee_categoryClientModel convert2Model(Hr_employee_category domain , hr_employee_categoryClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_employee_categoryClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("employee_idsdirtyflag"))
                model.setEmployee_ids(domain.getEmployeeIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_employee_category convert2Domain( hr_employee_categoryClientModel model ,Hr_employee_category domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_employee_category();
        }

        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getEmployee_idsDirtyFlag())
            domain.setEmployeeIds(model.getEmployee_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



