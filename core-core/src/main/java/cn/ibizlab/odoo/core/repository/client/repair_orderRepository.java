package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.repair_order;

/**
 * 实体[repair_order] 服务对象接口
 */
public interface repair_orderRepository{


    public repair_order createPO() ;
        public void createBatch(repair_order repair_order);

        public void update(repair_order repair_order);

        public List<repair_order> search();

        public void removeBatch(String id);

        public void updateBatch(repair_order repair_order);

        public void create(repair_order repair_order);

        public void get(String id);

        public void remove(String id);


}
