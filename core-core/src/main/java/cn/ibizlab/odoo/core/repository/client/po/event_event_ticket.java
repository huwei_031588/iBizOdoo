package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [event_event_ticket] 对象
 */
public interface event_event_ticket {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDeadline();

    public void setDeadline(Timestamp deadline);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getEvent_id();

    public void setEvent_id(Integer event_id);

    public String getEvent_id_text();

    public void setEvent_id_text(String event_id_text);

    public Integer getEvent_type_id();

    public void setEvent_type_id(Integer event_type_id);

    public String getEvent_type_id_text();

    public void setEvent_type_id_text(String event_type_id_text);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_expired();

    public void setIs_expired(String is_expired);

    public String getName();

    public void setName(String name);

    public Double getPrice();

    public void setPrice(Double price);

    public Double getPrice_reduce();

    public void setPrice_reduce(Double price_reduce);

    public Double getPrice_reduce_taxinc();

    public void setPrice_reduce_taxinc(Double price_reduce_taxinc);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public String getRegistration_ids();

    public void setRegistration_ids(String registration_ids);

    public String getSeats_availability();

    public void setSeats_availability(String seats_availability);

    public Integer getSeats_available();

    public void setSeats_available(Integer seats_available);

    public Integer getSeats_max();

    public void setSeats_max(Integer seats_max);

    public Integer getSeats_reserved();

    public void setSeats_reserved(Integer seats_reserved);

    public Integer getSeats_unconfirmed();

    public void setSeats_unconfirmed(Integer seats_unconfirmed);

    public Integer getSeats_used();

    public void setSeats_used(Integer seats_used);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
