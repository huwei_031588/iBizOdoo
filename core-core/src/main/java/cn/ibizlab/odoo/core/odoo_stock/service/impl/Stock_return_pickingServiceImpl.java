package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_pickingSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_return_pickingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_return_pickingOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_return_pickingClientModel;

/**
 * 实体[退回拣货] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_return_pickingServiceImpl implements IStock_return_pickingService {

    @Autowired
    stock_return_pickingOdooClient stock_return_pickingOdooClient;


    @Override
    public boolean update(Stock_return_picking et) {
        stock_return_pickingClientModel clientModel = convert2Model(et,null);
		stock_return_pickingOdooClient.update(clientModel);
        Stock_return_picking rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_return_picking> list){
    }

    @Override
    public boolean create(Stock_return_picking et) {
        stock_return_pickingClientModel clientModel = convert2Model(et,null);
		stock_return_pickingOdooClient.create(clientModel);
        Stock_return_picking rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_return_picking> list){
    }

    @Override
    public Stock_return_picking get(Integer id) {
        stock_return_pickingClientModel clientModel = new stock_return_pickingClientModel();
        clientModel.setId(id);
		stock_return_pickingOdooClient.get(clientModel);
        Stock_return_picking et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_return_picking();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        stock_return_pickingClientModel clientModel = new stock_return_pickingClientModel();
        clientModel.setId(id);
		stock_return_pickingOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_return_picking> searchDefault(Stock_return_pickingSearchContext context) {
        List<Stock_return_picking> list = new ArrayList<Stock_return_picking>();
        Page<stock_return_pickingClientModel> clientModelList = stock_return_pickingOdooClient.search(context);
        for(stock_return_pickingClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_return_picking>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_return_pickingClientModel convert2Model(Stock_return_picking domain , stock_return_pickingClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_return_pickingClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("product_return_movesdirtyflag"))
                model.setProduct_return_moves(domain.getProductReturnMoves());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("move_dest_existsdirtyflag"))
                model.setMove_dest_exists(domain.getMoveDestExists());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("original_location_id_textdirtyflag"))
                model.setOriginal_location_id_text(domain.getOriginalLocationIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("parent_location_id_textdirtyflag"))
                model.setParent_location_id_text(domain.getParentLocationIdText());
            if((Boolean) domain.getExtensionparams().get("picking_id_textdirtyflag"))
                model.setPicking_id_text(domain.getPickingIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("parent_location_iddirtyflag"))
                model.setParent_location_id(domain.getParentLocationId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("original_location_iddirtyflag"))
                model.setOriginal_location_id(domain.getOriginalLocationId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("picking_iddirtyflag"))
                model.setPicking_id(domain.getPickingId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_return_picking convert2Domain( stock_return_pickingClientModel model ,Stock_return_picking domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_return_picking();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getProduct_return_movesDirtyFlag())
            domain.setProductReturnMoves(model.getProduct_return_moves());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMove_dest_existsDirtyFlag())
            domain.setMoveDestExists(model.getMove_dest_exists());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getOriginal_location_id_textDirtyFlag())
            domain.setOriginalLocationIdText(model.getOriginal_location_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getParent_location_id_textDirtyFlag())
            domain.setParentLocationIdText(model.getParent_location_id_text());
        if(model.getPicking_id_textDirtyFlag())
            domain.setPickingIdText(model.getPicking_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getParent_location_idDirtyFlag())
            domain.setParentLocationId(model.getParent_location_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getOriginal_location_idDirtyFlag())
            domain.setOriginalLocationId(model.getOriginal_location_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getPicking_idDirtyFlag())
            domain.setPickingId(model.getPicking_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



