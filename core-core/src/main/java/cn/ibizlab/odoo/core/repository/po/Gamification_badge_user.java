package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_userSearchContext;

/**
 * 实体 [游戏化用户徽章] 存储模型
 */
public interface Gamification_badge_user{

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 备注
     */
    String getComment();

    void setComment(String comment);

    /**
     * 获取 [备注]脏标记
     */
    boolean getCommentDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 用户
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 发送者
     */
    String getSender_id_text();

    void setSender_id_text(String sender_id_text);

    /**
     * 获取 [发送者]脏标记
     */
    boolean getSender_id_textDirtyFlag();

    /**
     * 论坛徽章等级
     */
    String getLevel();

    void setLevel(String level);

    /**
     * 获取 [论坛徽章等级]脏标记
     */
    boolean getLevelDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 徽章名称
     */
    String getBadge_name();

    void setBadge_name(String badge_name);

    /**
     * 获取 [徽章名称]脏标记
     */
    boolean getBadge_nameDirtyFlag();

    /**
     * 挑战源于
     */
    String getChallenge_id_text();

    void setChallenge_id_text(String challenge_id_text);

    /**
     * 获取 [挑战源于]脏标记
     */
    boolean getChallenge_id_textDirtyFlag();

    /**
     * 员工
     */
    String getEmployee_id_text();

    void setEmployee_id_text(String employee_id_text);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_id_textDirtyFlag();

    /**
     * 发送者
     */
    Integer getSender_id();

    void setSender_id(Integer sender_id);

    /**
     * 获取 [发送者]脏标记
     */
    boolean getSender_idDirtyFlag();

    /**
     * 徽章
     */
    Integer getBadge_id();

    void setBadge_id(Integer badge_id);

    /**
     * 获取 [徽章]脏标记
     */
    boolean getBadge_idDirtyFlag();

    /**
     * 挑战源于
     */
    Integer getChallenge_id();

    void setChallenge_id(Integer challenge_id);

    /**
     * 获取 [挑战源于]脏标记
     */
    boolean getChallenge_idDirtyFlag();

    /**
     * 员工
     */
    Integer getEmployee_id();

    void setEmployee_id(Integer employee_id);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 用户
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_idDirtyFlag();

}
