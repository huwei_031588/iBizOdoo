package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_move_lineSearchContext;

/**
 * 实体 [产品移动(移库明细)] 存储模型
 */
public interface Stock_move_line{

    /**
     * 完成
     */
    Double getQty_done();

    void setQty_done(Double qty_done);

    /**
     * 获取 [完成]脏标记
     */
    boolean getQty_doneDirtyFlag();

    /**
     * 使用已有批次/序列号码
     */
    String getPicking_type_use_existing_lots();

    void setPicking_type_use_existing_lots(String picking_type_use_existing_lots);

    /**
     * 获取 [使用已有批次/序列号码]脏标记
     */
    boolean getPicking_type_use_existing_lotsDirtyFlag();

    /**
     * 消耗行
     */
    String getConsume_line_ids();

    void setConsume_line_ids(String consume_line_ids);

    /**
     * 获取 [消耗行]脏标记
     */
    boolean getConsume_line_idsDirtyFlag();

    /**
     * 批次可见
     */
    String getLots_visible();

    void setLots_visible(String lots_visible);

    /**
     * 获取 [批次可见]脏标记
     */
    boolean getLots_visibleDirtyFlag();

    /**
     * 已保留
     */
    Double getProduct_uom_qty();

    void setProduct_uom_qty(Double product_uom_qty);

    /**
     * 获取 [已保留]脏标记
     */
    boolean getProduct_uom_qtyDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 完成工单
     */
    String getDone_wo();

    void setDone_wo(String done_wo);

    /**
     * 获取 [完成工单]脏标记
     */
    boolean getDone_woDirtyFlag();

    /**
     * 创建新批次/序列号码
     */
    String getPicking_type_use_create_lots();

    void setPicking_type_use_create_lots(String picking_type_use_create_lots);

    /**
     * 获取 [创建新批次/序列号码]脏标记
     */
    boolean getPicking_type_use_create_lotsDirtyFlag();

    /**
     * 生产行
     */
    String getProduce_line_ids();

    void setProduce_line_ids(String produce_line_ids);

    /**
     * 获取 [生产行]脏标记
     */
    boolean getProduce_line_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 移动整个包裹
     */
    String getPicking_type_entire_packs();

    void setPicking_type_entire_packs(String picking_type_entire_packs);

    /**
     * 获取 [移动整个包裹]脏标记
     */
    boolean getPicking_type_entire_packsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 批次/序列号 名称
     */
    String getLot_name();

    void setLot_name(String lot_name);

    /**
     * 获取 [批次/序列号 名称]脏标记
     */
    boolean getLot_nameDirtyFlag();

    /**
     * 实际预留数量
     */
    Double getProduct_qty();

    void setProduct_qty(Double product_qty);

    /**
     * 获取 [实际预留数量]脏标记
     */
    boolean getProduct_qtyDirtyFlag();

    /**
     * 产成品数量
     */
    Double getLot_produced_qty();

    void setLot_produced_qty(Double lot_produced_qty);

    /**
     * 获取 [产成品数量]脏标记
     */
    boolean getLot_produced_qtyDirtyFlag();

    /**
     * 源包裹
     */
    String getPackage_id_text();

    void setPackage_id_text(String package_id_text);

    /**
     * 获取 [源包裹]脏标记
     */
    boolean getPackage_id_textDirtyFlag();

    /**
     * 完工批次/序列号
     */
    String getLot_produced_id_text();

    void setLot_produced_id_text(String lot_produced_id_text);

    /**
     * 获取 [完工批次/序列号]脏标记
     */
    boolean getLot_produced_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 工单
     */
    String getWorkorder_id_text();

    void setWorkorder_id_text(String workorder_id_text);

    /**
     * 获取 [工单]脏标记
     */
    boolean getWorkorder_id_textDirtyFlag();

    /**
     * 单位
     */
    String getProduct_uom_id_text();

    void setProduct_uom_id_text(String product_uom_id_text);

    /**
     * 获取 [单位]脏标记
     */
    boolean getProduct_uom_id_textDirtyFlag();

    /**
     * 追踪
     */
    String getTracking();

    void setTracking(String tracking);

    /**
     * 获取 [追踪]脏标记
     */
    boolean getTrackingDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 库存移动
     */
    String getMove_id_text();

    void setMove_id_text(String move_id_text);

    /**
     * 获取 [库存移动]脏标记
     */
    boolean getMove_id_textDirtyFlag();

    /**
     * 至
     */
    String getLocation_dest_id_text();

    void setLocation_dest_id_text(String location_dest_id_text);

    /**
     * 获取 [至]脏标记
     */
    boolean getLocation_dest_id_textDirtyFlag();

    /**
     * 是锁定
     */
    String getIs_locked();

    void setIs_locked(String is_locked);

    /**
     * 获取 [是锁定]脏标记
     */
    boolean getIs_lockedDirtyFlag();

    /**
     * 初始需求是否可以编辑
     */
    String getIs_initial_demand_editable();

    void setIs_initial_demand_editable(String is_initial_demand_editable);

    /**
     * 获取 [初始需求是否可以编辑]脏标记
     */
    boolean getIs_initial_demand_editableDirtyFlag();

    /**
     * 生产单
     */
    String getProduction_id_text();

    void setProduction_id_text(String production_id_text);

    /**
     * 获取 [生产单]脏标记
     */
    boolean getProduction_id_textDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 批次/序列号码
     */
    String getLot_id_text();

    void setLot_id_text(String lot_id_text);

    /**
     * 获取 [批次/序列号码]脏标记
     */
    boolean getLot_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 编号
     */
    String getReference();

    void setReference(String reference);

    /**
     * 获取 [编号]脏标记
     */
    boolean getReferenceDirtyFlag();

    /**
     * 目的地包裹
     */
    String getResult_package_id_text();

    void setResult_package_id_text(String result_package_id_text);

    /**
     * 获取 [目的地包裹]脏标记
     */
    boolean getResult_package_id_textDirtyFlag();

    /**
     * 移动完成
     */
    String getDone_move();

    void setDone_move(String done_move);

    /**
     * 获取 [移动完成]脏标记
     */
    boolean getDone_moveDirtyFlag();

    /**
     * 库存拣货
     */
    String getPicking_id_text();

    void setPicking_id_text(String picking_id_text);

    /**
     * 获取 [库存拣货]脏标记
     */
    boolean getPicking_id_textDirtyFlag();

    /**
     * 所有者
     */
    String getOwner_id_text();

    void setOwner_id_text(String owner_id_text);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_id_textDirtyFlag();

    /**
     * 从
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [从]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 库存移动
     */
    Integer getMove_id();

    void setMove_id(Integer move_id);

    /**
     * 获取 [库存移动]脏标记
     */
    boolean getMove_idDirtyFlag();

    /**
     * 目的地包裹
     */
    Integer getResult_package_id();

    void setResult_package_id(Integer result_package_id);

    /**
     * 获取 [目的地包裹]脏标记
     */
    boolean getResult_package_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 完工批次/序列号
     */
    Integer getLot_produced_id();

    void setLot_produced_id(Integer lot_produced_id);

    /**
     * 获取 [完工批次/序列号]脏标记
     */
    boolean getLot_produced_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 批次/序列号码
     */
    Integer getLot_id();

    void setLot_id(Integer lot_id);

    /**
     * 获取 [批次/序列号码]脏标记
     */
    boolean getLot_idDirtyFlag();

    /**
     * 库存拣货
     */
    Integer getPicking_id();

    void setPicking_id(Integer picking_id);

    /**
     * 获取 [库存拣货]脏标记
     */
    boolean getPicking_idDirtyFlag();

    /**
     * 工单
     */
    Integer getWorkorder_id();

    void setWorkorder_id(Integer workorder_id);

    /**
     * 获取 [工单]脏标记
     */
    boolean getWorkorder_idDirtyFlag();

    /**
     * 从
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [从]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 源包裹
     */
    Integer getPackage_id();

    void setPackage_id(Integer package_id);

    /**
     * 获取 [源包裹]脏标记
     */
    boolean getPackage_idDirtyFlag();

    /**
     * 所有者
     */
    Integer getOwner_id();

    void setOwner_id(Integer owner_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_idDirtyFlag();

    /**
     * 包裹层级
     */
    Integer getPackage_level_id();

    void setPackage_level_id(Integer package_level_id);

    /**
     * 获取 [包裹层级]脏标记
     */
    boolean getPackage_level_idDirtyFlag();

    /**
     * 至
     */
    Integer getLocation_dest_id();

    void setLocation_dest_id(Integer location_dest_id);

    /**
     * 获取 [至]脏标记
     */
    boolean getLocation_dest_idDirtyFlag();

    /**
     * 生产单
     */
    Integer getProduction_id();

    void setProduction_id(Integer production_id);

    /**
     * 获取 [生产单]脏标记
     */
    boolean getProduction_idDirtyFlag();

}
