package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_expense;

/**
 * 实体[hr_expense] 服务对象接口
 */
public interface hr_expenseRepository{


    public hr_expense createPO() ;
        public void removeBatch(String id);

        public void create(hr_expense hr_expense);

        public void get(String id);

        public void createBatch(hr_expense hr_expense);

        public void updateBatch(hr_expense hr_expense);

        public List<hr_expense> search();

        public void remove(String id);

        public void update(hr_expense hr_expense);


}
