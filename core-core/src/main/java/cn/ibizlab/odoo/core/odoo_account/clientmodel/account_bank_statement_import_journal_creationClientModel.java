package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_bank_statement_import_journal_creation] 对象
 */
public class account_bank_statement_import_journal_creationClientModel implements Serializable{

    /**
     * 允许的科目
     */
    public String account_control_ids;

    @JsonIgnore
    public boolean account_control_idsDirtyFlag;
    
    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 别名域
     */
    public String alias_domain;

    @JsonIgnore
    public boolean alias_domainDirtyFlag;
    
    /**
     * 别名
     */
    public Integer alias_id;

    @JsonIgnore
    public boolean alias_idDirtyFlag;
    
    /**
     * 供应商账单的别名
     */
    public String alias_name;

    @JsonIgnore
    public boolean alias_nameDirtyFlag;
    
    /**
     * 至少一个转入
     */
    public String at_least_one_inbound;

    @JsonIgnore
    public boolean at_least_one_inboundDirtyFlag;
    
    /**
     * 至少一个转出
     */
    public String at_least_one_outbound;

    @JsonIgnore
    public boolean at_least_one_outboundDirtyFlag;
    
    /**
     * 银行账户
     */
    public Integer bank_account_id;

    @JsonIgnore
    public boolean bank_account_idDirtyFlag;
    
    /**
     * 账户号码
     */
    public String bank_acc_number;

    @JsonIgnore
    public boolean bank_acc_numberDirtyFlag;
    
    /**
     * 银行
     */
    public Integer bank_id;

    @JsonIgnore
    public boolean bank_idDirtyFlag;
    
    /**
     * 银行费用
     */
    public String bank_statements_source;

    @JsonIgnore
    public boolean bank_statements_sourceDirtyFlag;
    
    /**
     * 属于用户的当前公司
     */
    public String belongs_to_company;

    @JsonIgnore
    public boolean belongs_to_companyDirtyFlag;
    
    /**
     * 简码
     */
    public String code;

    @JsonIgnore
    public boolean codeDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 账户持有人
     */
    public Integer company_partner_id;

    @JsonIgnore
    public boolean company_partner_idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 默认贷方科目
     */
    public Integer default_credit_account_id;

    @JsonIgnore
    public boolean default_credit_account_idDirtyFlag;
    
    /**
     * 默认借方科目
     */
    public Integer default_debit_account_id;

    @JsonIgnore
    public boolean default_debit_account_idDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 分组发票明细行
     */
    public String group_invoice_lines;

    @JsonIgnore
    public boolean group_invoice_linesDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 为收款
     */
    public String inbound_payment_method_ids;

    @JsonIgnore
    public boolean inbound_payment_method_idsDirtyFlag;
    
    /**
     * 日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 看板仪表板
     */
    public String kanban_dashboard;

    @JsonIgnore
    public boolean kanban_dashboardDirtyFlag;
    
    /**
     * 看板仪表板图表
     */
    public String kanban_dashboard_graph;

    @JsonIgnore
    public boolean kanban_dashboard_graphDirtyFlag;
    
    /**
     * 损失科目
     */
    public Integer loss_account_id;

    @JsonIgnore
    public boolean loss_account_idDirtyFlag;
    
    /**
     * 日记账名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 为付款
     */
    public String outbound_payment_method_ids;

    @JsonIgnore
    public boolean outbound_payment_method_idsDirtyFlag;
    
    /**
     * 银行核销时过账
     */
    public String post_at_bank_rec;

    @JsonIgnore
    public boolean post_at_bank_recDirtyFlag;
    
    /**
     * 利润科目
     */
    public Integer profit_account_id;

    @JsonIgnore
    public boolean profit_account_idDirtyFlag;
    
    /**
     * 专用的信用票序列
     */
    public String refund_sequence;

    @JsonIgnore
    public boolean refund_sequenceDirtyFlag;
    
    /**
     * 信用票分录序列
     */
    public Integer refund_sequence_id;

    @JsonIgnore
    public boolean refund_sequence_idDirtyFlag;
    
    /**
     * 信用票：下一号码
     */
    public Integer refund_sequence_number_next;

    @JsonIgnore
    public boolean refund_sequence_number_nextDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 分录序列
     */
    public Integer sequence_id;

    @JsonIgnore
    public boolean sequence_idDirtyFlag;
    
    /**
     * 下一号码
     */
    public Integer sequence_number_next;

    @JsonIgnore
    public boolean sequence_number_nextDirtyFlag;
    
    /**
     * 在仪表板显示日记账
     */
    public String show_on_dashboard;

    @JsonIgnore
    public boolean show_on_dashboardDirtyFlag;
    
    /**
     * 类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 允许的科目类型
     */
    public String type_control_ids;

    @JsonIgnore
    public boolean type_control_idsDirtyFlag;
    
    /**
     * 允许取消分录
     */
    public String update_posted;

    @JsonIgnore
    public boolean update_postedDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [允许的科目]
     */
    @JsonProperty("account_control_ids")
    public String getAccount_control_ids(){
        return this.account_control_ids ;
    }

    /**
     * 设置 [允许的科目]
     */
    @JsonProperty("account_control_ids")
    public void setAccount_control_ids(String  account_control_ids){
        this.account_control_ids = account_control_ids ;
        this.account_control_idsDirtyFlag = true ;
    }

     /**
     * 获取 [允许的科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_control_idsDirtyFlag(){
        return this.account_control_idsDirtyFlag ;
    }   

    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [别名域]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return this.alias_domain ;
    }

    /**
     * 设置 [别名域]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

     /**
     * 获取 [别名域]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return this.alias_domainDirtyFlag ;
    }   

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return this.alias_id ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

     /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return this.alias_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商账单的别名]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return this.alias_name ;
    }

    /**
     * 设置 [供应商账单的别名]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

     /**
     * 获取 [供应商账单的别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return this.alias_nameDirtyFlag ;
    }   

    /**
     * 获取 [至少一个转入]
     */
    @JsonProperty("at_least_one_inbound")
    public String getAt_least_one_inbound(){
        return this.at_least_one_inbound ;
    }

    /**
     * 设置 [至少一个转入]
     */
    @JsonProperty("at_least_one_inbound")
    public void setAt_least_one_inbound(String  at_least_one_inbound){
        this.at_least_one_inbound = at_least_one_inbound ;
        this.at_least_one_inboundDirtyFlag = true ;
    }

     /**
     * 获取 [至少一个转入]脏标记
     */
    @JsonIgnore
    public boolean getAt_least_one_inboundDirtyFlag(){
        return this.at_least_one_inboundDirtyFlag ;
    }   

    /**
     * 获取 [至少一个转出]
     */
    @JsonProperty("at_least_one_outbound")
    public String getAt_least_one_outbound(){
        return this.at_least_one_outbound ;
    }

    /**
     * 设置 [至少一个转出]
     */
    @JsonProperty("at_least_one_outbound")
    public void setAt_least_one_outbound(String  at_least_one_outbound){
        this.at_least_one_outbound = at_least_one_outbound ;
        this.at_least_one_outboundDirtyFlag = true ;
    }

     /**
     * 获取 [至少一个转出]脏标记
     */
    @JsonIgnore
    public boolean getAt_least_one_outboundDirtyFlag(){
        return this.at_least_one_outboundDirtyFlag ;
    }   

    /**
     * 获取 [银行账户]
     */
    @JsonProperty("bank_account_id")
    public Integer getBank_account_id(){
        return this.bank_account_id ;
    }

    /**
     * 设置 [银行账户]
     */
    @JsonProperty("bank_account_id")
    public void setBank_account_id(Integer  bank_account_id){
        this.bank_account_id = bank_account_id ;
        this.bank_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [银行账户]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_idDirtyFlag(){
        return this.bank_account_idDirtyFlag ;
    }   

    /**
     * 获取 [账户号码]
     */
    @JsonProperty("bank_acc_number")
    public String getBank_acc_number(){
        return this.bank_acc_number ;
    }

    /**
     * 设置 [账户号码]
     */
    @JsonProperty("bank_acc_number")
    public void setBank_acc_number(String  bank_acc_number){
        this.bank_acc_number = bank_acc_number ;
        this.bank_acc_numberDirtyFlag = true ;
    }

     /**
     * 获取 [账户号码]脏标记
     */
    @JsonIgnore
    public boolean getBank_acc_numberDirtyFlag(){
        return this.bank_acc_numberDirtyFlag ;
    }   

    /**
     * 获取 [银行]
     */
    @JsonProperty("bank_id")
    public Integer getBank_id(){
        return this.bank_id ;
    }

    /**
     * 设置 [银行]
     */
    @JsonProperty("bank_id")
    public void setBank_id(Integer  bank_id){
        this.bank_id = bank_id ;
        this.bank_idDirtyFlag = true ;
    }

     /**
     * 获取 [银行]脏标记
     */
    @JsonIgnore
    public boolean getBank_idDirtyFlag(){
        return this.bank_idDirtyFlag ;
    }   

    /**
     * 获取 [银行费用]
     */
    @JsonProperty("bank_statements_source")
    public String getBank_statements_source(){
        return this.bank_statements_source ;
    }

    /**
     * 设置 [银行费用]
     */
    @JsonProperty("bank_statements_source")
    public void setBank_statements_source(String  bank_statements_source){
        this.bank_statements_source = bank_statements_source ;
        this.bank_statements_sourceDirtyFlag = true ;
    }

     /**
     * 获取 [银行费用]脏标记
     */
    @JsonIgnore
    public boolean getBank_statements_sourceDirtyFlag(){
        return this.bank_statements_sourceDirtyFlag ;
    }   

    /**
     * 获取 [属于用户的当前公司]
     */
    @JsonProperty("belongs_to_company")
    public String getBelongs_to_company(){
        return this.belongs_to_company ;
    }

    /**
     * 设置 [属于用户的当前公司]
     */
    @JsonProperty("belongs_to_company")
    public void setBelongs_to_company(String  belongs_to_company){
        this.belongs_to_company = belongs_to_company ;
        this.belongs_to_companyDirtyFlag = true ;
    }

     /**
     * 获取 [属于用户的当前公司]脏标记
     */
    @JsonIgnore
    public boolean getBelongs_to_companyDirtyFlag(){
        return this.belongs_to_companyDirtyFlag ;
    }   

    /**
     * 获取 [简码]
     */
    @JsonProperty("code")
    public String getCode(){
        return this.code ;
    }

    /**
     * 设置 [简码]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

     /**
     * 获取 [简码]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return this.codeDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [账户持有人]
     */
    @JsonProperty("company_partner_id")
    public Integer getCompany_partner_id(){
        return this.company_partner_id ;
    }

    /**
     * 设置 [账户持有人]
     */
    @JsonProperty("company_partner_id")
    public void setCompany_partner_id(Integer  company_partner_id){
        this.company_partner_id = company_partner_id ;
        this.company_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [账户持有人]脏标记
     */
    @JsonIgnore
    public boolean getCompany_partner_idDirtyFlag(){
        return this.company_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [默认贷方科目]
     */
    @JsonProperty("default_credit_account_id")
    public Integer getDefault_credit_account_id(){
        return this.default_credit_account_id ;
    }

    /**
     * 设置 [默认贷方科目]
     */
    @JsonProperty("default_credit_account_id")
    public void setDefault_credit_account_id(Integer  default_credit_account_id){
        this.default_credit_account_id = default_credit_account_id ;
        this.default_credit_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认贷方科目]脏标记
     */
    @JsonIgnore
    public boolean getDefault_credit_account_idDirtyFlag(){
        return this.default_credit_account_idDirtyFlag ;
    }   

    /**
     * 获取 [默认借方科目]
     */
    @JsonProperty("default_debit_account_id")
    public Integer getDefault_debit_account_id(){
        return this.default_debit_account_id ;
    }

    /**
     * 设置 [默认借方科目]
     */
    @JsonProperty("default_debit_account_id")
    public void setDefault_debit_account_id(Integer  default_debit_account_id){
        this.default_debit_account_id = default_debit_account_id ;
        this.default_debit_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [默认借方科目]脏标记
     */
    @JsonIgnore
    public boolean getDefault_debit_account_idDirtyFlag(){
        return this.default_debit_account_idDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [分组发票明细行]
     */
    @JsonProperty("group_invoice_lines")
    public String getGroup_invoice_lines(){
        return this.group_invoice_lines ;
    }

    /**
     * 设置 [分组发票明细行]
     */
    @JsonProperty("group_invoice_lines")
    public void setGroup_invoice_lines(String  group_invoice_lines){
        this.group_invoice_lines = group_invoice_lines ;
        this.group_invoice_linesDirtyFlag = true ;
    }

     /**
     * 获取 [分组发票明细行]脏标记
     */
    @JsonIgnore
    public boolean getGroup_invoice_linesDirtyFlag(){
        return this.group_invoice_linesDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [为收款]
     */
    @JsonProperty("inbound_payment_method_ids")
    public String getInbound_payment_method_ids(){
        return this.inbound_payment_method_ids ;
    }

    /**
     * 设置 [为收款]
     */
    @JsonProperty("inbound_payment_method_ids")
    public void setInbound_payment_method_ids(String  inbound_payment_method_ids){
        this.inbound_payment_method_ids = inbound_payment_method_ids ;
        this.inbound_payment_method_idsDirtyFlag = true ;
    }

     /**
     * 获取 [为收款]脏标记
     */
    @JsonIgnore
    public boolean getInbound_payment_method_idsDirtyFlag(){
        return this.inbound_payment_method_idsDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [看板仪表板]
     */
    @JsonProperty("kanban_dashboard")
    public String getKanban_dashboard(){
        return this.kanban_dashboard ;
    }

    /**
     * 设置 [看板仪表板]
     */
    @JsonProperty("kanban_dashboard")
    public void setKanban_dashboard(String  kanban_dashboard){
        this.kanban_dashboard = kanban_dashboard ;
        this.kanban_dashboardDirtyFlag = true ;
    }

     /**
     * 获取 [看板仪表板]脏标记
     */
    @JsonIgnore
    public boolean getKanban_dashboardDirtyFlag(){
        return this.kanban_dashboardDirtyFlag ;
    }   

    /**
     * 获取 [看板仪表板图表]
     */
    @JsonProperty("kanban_dashboard_graph")
    public String getKanban_dashboard_graph(){
        return this.kanban_dashboard_graph ;
    }

    /**
     * 设置 [看板仪表板图表]
     */
    @JsonProperty("kanban_dashboard_graph")
    public void setKanban_dashboard_graph(String  kanban_dashboard_graph){
        this.kanban_dashboard_graph = kanban_dashboard_graph ;
        this.kanban_dashboard_graphDirtyFlag = true ;
    }

     /**
     * 获取 [看板仪表板图表]脏标记
     */
    @JsonIgnore
    public boolean getKanban_dashboard_graphDirtyFlag(){
        return this.kanban_dashboard_graphDirtyFlag ;
    }   

    /**
     * 获取 [损失科目]
     */
    @JsonProperty("loss_account_id")
    public Integer getLoss_account_id(){
        return this.loss_account_id ;
    }

    /**
     * 设置 [损失科目]
     */
    @JsonProperty("loss_account_id")
    public void setLoss_account_id(Integer  loss_account_id){
        this.loss_account_id = loss_account_id ;
        this.loss_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [损失科目]脏标记
     */
    @JsonIgnore
    public boolean getLoss_account_idDirtyFlag(){
        return this.loss_account_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [日记账名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [日记账名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [为付款]
     */
    @JsonProperty("outbound_payment_method_ids")
    public String getOutbound_payment_method_ids(){
        return this.outbound_payment_method_ids ;
    }

    /**
     * 设置 [为付款]
     */
    @JsonProperty("outbound_payment_method_ids")
    public void setOutbound_payment_method_ids(String  outbound_payment_method_ids){
        this.outbound_payment_method_ids = outbound_payment_method_ids ;
        this.outbound_payment_method_idsDirtyFlag = true ;
    }

     /**
     * 获取 [为付款]脏标记
     */
    @JsonIgnore
    public boolean getOutbound_payment_method_idsDirtyFlag(){
        return this.outbound_payment_method_idsDirtyFlag ;
    }   

    /**
     * 获取 [银行核销时过账]
     */
    @JsonProperty("post_at_bank_rec")
    public String getPost_at_bank_rec(){
        return this.post_at_bank_rec ;
    }

    /**
     * 设置 [银行核销时过账]
     */
    @JsonProperty("post_at_bank_rec")
    public void setPost_at_bank_rec(String  post_at_bank_rec){
        this.post_at_bank_rec = post_at_bank_rec ;
        this.post_at_bank_recDirtyFlag = true ;
    }

     /**
     * 获取 [银行核销时过账]脏标记
     */
    @JsonIgnore
    public boolean getPost_at_bank_recDirtyFlag(){
        return this.post_at_bank_recDirtyFlag ;
    }   

    /**
     * 获取 [利润科目]
     */
    @JsonProperty("profit_account_id")
    public Integer getProfit_account_id(){
        return this.profit_account_id ;
    }

    /**
     * 设置 [利润科目]
     */
    @JsonProperty("profit_account_id")
    public void setProfit_account_id(Integer  profit_account_id){
        this.profit_account_id = profit_account_id ;
        this.profit_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [利润科目]脏标记
     */
    @JsonIgnore
    public boolean getProfit_account_idDirtyFlag(){
        return this.profit_account_idDirtyFlag ;
    }   

    /**
     * 获取 [专用的信用票序列]
     */
    @JsonProperty("refund_sequence")
    public String getRefund_sequence(){
        return this.refund_sequence ;
    }

    /**
     * 设置 [专用的信用票序列]
     */
    @JsonProperty("refund_sequence")
    public void setRefund_sequence(String  refund_sequence){
        this.refund_sequence = refund_sequence ;
        this.refund_sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [专用的信用票序列]脏标记
     */
    @JsonIgnore
    public boolean getRefund_sequenceDirtyFlag(){
        return this.refund_sequenceDirtyFlag ;
    }   

    /**
     * 获取 [信用票分录序列]
     */
    @JsonProperty("refund_sequence_id")
    public Integer getRefund_sequence_id(){
        return this.refund_sequence_id ;
    }

    /**
     * 设置 [信用票分录序列]
     */
    @JsonProperty("refund_sequence_id")
    public void setRefund_sequence_id(Integer  refund_sequence_id){
        this.refund_sequence_id = refund_sequence_id ;
        this.refund_sequence_idDirtyFlag = true ;
    }

     /**
     * 获取 [信用票分录序列]脏标记
     */
    @JsonIgnore
    public boolean getRefund_sequence_idDirtyFlag(){
        return this.refund_sequence_idDirtyFlag ;
    }   

    /**
     * 获取 [信用票：下一号码]
     */
    @JsonProperty("refund_sequence_number_next")
    public Integer getRefund_sequence_number_next(){
        return this.refund_sequence_number_next ;
    }

    /**
     * 设置 [信用票：下一号码]
     */
    @JsonProperty("refund_sequence_number_next")
    public void setRefund_sequence_number_next(Integer  refund_sequence_number_next){
        this.refund_sequence_number_next = refund_sequence_number_next ;
        this.refund_sequence_number_nextDirtyFlag = true ;
    }

     /**
     * 获取 [信用票：下一号码]脏标记
     */
    @JsonIgnore
    public boolean getRefund_sequence_number_nextDirtyFlag(){
        return this.refund_sequence_number_nextDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [分录序列]
     */
    @JsonProperty("sequence_id")
    public Integer getSequence_id(){
        return this.sequence_id ;
    }

    /**
     * 设置 [分录序列]
     */
    @JsonProperty("sequence_id")
    public void setSequence_id(Integer  sequence_id){
        this.sequence_id = sequence_id ;
        this.sequence_idDirtyFlag = true ;
    }

     /**
     * 获取 [分录序列]脏标记
     */
    @JsonIgnore
    public boolean getSequence_idDirtyFlag(){
        return this.sequence_idDirtyFlag ;
    }   

    /**
     * 获取 [下一号码]
     */
    @JsonProperty("sequence_number_next")
    public Integer getSequence_number_next(){
        return this.sequence_number_next ;
    }

    /**
     * 设置 [下一号码]
     */
    @JsonProperty("sequence_number_next")
    public void setSequence_number_next(Integer  sequence_number_next){
        this.sequence_number_next = sequence_number_next ;
        this.sequence_number_nextDirtyFlag = true ;
    }

     /**
     * 获取 [下一号码]脏标记
     */
    @JsonIgnore
    public boolean getSequence_number_nextDirtyFlag(){
        return this.sequence_number_nextDirtyFlag ;
    }   

    /**
     * 获取 [在仪表板显示日记账]
     */
    @JsonProperty("show_on_dashboard")
    public String getShow_on_dashboard(){
        return this.show_on_dashboard ;
    }

    /**
     * 设置 [在仪表板显示日记账]
     */
    @JsonProperty("show_on_dashboard")
    public void setShow_on_dashboard(String  show_on_dashboard){
        this.show_on_dashboard = show_on_dashboard ;
        this.show_on_dashboardDirtyFlag = true ;
    }

     /**
     * 获取 [在仪表板显示日记账]脏标记
     */
    @JsonIgnore
    public boolean getShow_on_dashboardDirtyFlag(){
        return this.show_on_dashboardDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [允许的科目类型]
     */
    @JsonProperty("type_control_ids")
    public String getType_control_ids(){
        return this.type_control_ids ;
    }

    /**
     * 设置 [允许的科目类型]
     */
    @JsonProperty("type_control_ids")
    public void setType_control_ids(String  type_control_ids){
        this.type_control_ids = type_control_ids ;
        this.type_control_idsDirtyFlag = true ;
    }

     /**
     * 获取 [允许的科目类型]脏标记
     */
    @JsonIgnore
    public boolean getType_control_idsDirtyFlag(){
        return this.type_control_idsDirtyFlag ;
    }   

    /**
     * 获取 [允许取消分录]
     */
    @JsonProperty("update_posted")
    public String getUpdate_posted(){
        return this.update_posted ;
    }

    /**
     * 设置 [允许取消分录]
     */
    @JsonProperty("update_posted")
    public void setUpdate_posted(String  update_posted){
        this.update_posted = update_posted ;
        this.update_postedDirtyFlag = true ;
    }

     /**
     * 获取 [允许取消分录]脏标记
     */
    @JsonIgnore
    public boolean getUpdate_postedDirtyFlag(){
        return this.update_postedDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("account_control_ids") instanceof Boolean)&& map.get("account_control_ids")!=null){
			Object[] objs = (Object[])map.get("account_control_ids");
			if(objs.length > 0){
				Integer[] account_control_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAccount_control_ids(Arrays.toString(account_control_ids).replace(" ",""));
			}
		}
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("alias_domain") instanceof Boolean)&& map.get("alias_domain")!=null){
			this.setAlias_domain((String)map.get("alias_domain"));
		}
		if(!(map.get("alias_id") instanceof Boolean)&& map.get("alias_id")!=null){
			Object[] objs = (Object[])map.get("alias_id");
			if(objs.length > 0){
				this.setAlias_id((Integer)objs[0]);
			}
		}
		if(!(map.get("alias_name") instanceof Boolean)&& map.get("alias_name")!=null){
			this.setAlias_name((String)map.get("alias_name"));
		}
		if(map.get("at_least_one_inbound") instanceof Boolean){
			this.setAt_least_one_inbound(((Boolean)map.get("at_least_one_inbound"))? "true" : "false");
		}
		if(map.get("at_least_one_outbound") instanceof Boolean){
			this.setAt_least_one_outbound(((Boolean)map.get("at_least_one_outbound"))? "true" : "false");
		}
		if(!(map.get("bank_account_id") instanceof Boolean)&& map.get("bank_account_id")!=null){
			Object[] objs = (Object[])map.get("bank_account_id");
			if(objs.length > 0){
				this.setBank_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("bank_acc_number") instanceof Boolean)&& map.get("bank_acc_number")!=null){
			this.setBank_acc_number((String)map.get("bank_acc_number"));
		}
		if(!(map.get("bank_id") instanceof Boolean)&& map.get("bank_id")!=null){
			Object[] objs = (Object[])map.get("bank_id");
			if(objs.length > 0){
				this.setBank_id((Integer)objs[0]);
			}
		}
		if(!(map.get("bank_statements_source") instanceof Boolean)&& map.get("bank_statements_source")!=null){
			this.setBank_statements_source((String)map.get("bank_statements_source"));
		}
		if(map.get("belongs_to_company") instanceof Boolean){
			this.setBelongs_to_company(((Boolean)map.get("belongs_to_company"))? "true" : "false");
		}
		if(!(map.get("code") instanceof Boolean)&& map.get("code")!=null){
			this.setCode((String)map.get("code"));
		}
		if(!(map.get("color") instanceof Boolean)&& map.get("color")!=null){
			this.setColor((Integer)map.get("color"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_partner_id") instanceof Boolean)&& map.get("company_partner_id")!=null){
			Object[] objs = (Object[])map.get("company_partner_id");
			if(objs.length > 0){
				this.setCompany_partner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("default_credit_account_id") instanceof Boolean)&& map.get("default_credit_account_id")!=null){
			Object[] objs = (Object[])map.get("default_credit_account_id");
			if(objs.length > 0){
				this.setDefault_credit_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("default_debit_account_id") instanceof Boolean)&& map.get("default_debit_account_id")!=null){
			Object[] objs = (Object[])map.get("default_debit_account_id");
			if(objs.length > 0){
				this.setDefault_debit_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("group_invoice_lines") instanceof Boolean){
			this.setGroup_invoice_lines(((Boolean)map.get("group_invoice_lines"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("inbound_payment_method_ids") instanceof Boolean)&& map.get("inbound_payment_method_ids")!=null){
			Object[] objs = (Object[])map.get("inbound_payment_method_ids");
			if(objs.length > 0){
				Integer[] inbound_payment_method_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setInbound_payment_method_ids(Arrays.toString(inbound_payment_method_ids).replace(" ",""));
			}
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 0){
				this.setJournal_id((Integer)objs[0]);
			}
		}
		if(!(map.get("kanban_dashboard") instanceof Boolean)&& map.get("kanban_dashboard")!=null){
			this.setKanban_dashboard((String)map.get("kanban_dashboard"));
		}
		if(!(map.get("kanban_dashboard_graph") instanceof Boolean)&& map.get("kanban_dashboard_graph")!=null){
			this.setKanban_dashboard_graph((String)map.get("kanban_dashboard_graph"));
		}
		if(!(map.get("loss_account_id") instanceof Boolean)&& map.get("loss_account_id")!=null){
			Object[] objs = (Object[])map.get("loss_account_id");
			if(objs.length > 0){
				this.setLoss_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("outbound_payment_method_ids") instanceof Boolean)&& map.get("outbound_payment_method_ids")!=null){
			Object[] objs = (Object[])map.get("outbound_payment_method_ids");
			if(objs.length > 0){
				Integer[] outbound_payment_method_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setOutbound_payment_method_ids(Arrays.toString(outbound_payment_method_ids).replace(" ",""));
			}
		}
		if(map.get("post_at_bank_rec") instanceof Boolean){
			this.setPost_at_bank_rec(((Boolean)map.get("post_at_bank_rec"))? "true" : "false");
		}
		if(!(map.get("profit_account_id") instanceof Boolean)&& map.get("profit_account_id")!=null){
			Object[] objs = (Object[])map.get("profit_account_id");
			if(objs.length > 0){
				this.setProfit_account_id((Integer)objs[0]);
			}
		}
		if(map.get("refund_sequence") instanceof Boolean){
			this.setRefund_sequence(((Boolean)map.get("refund_sequence"))? "true" : "false");
		}
		if(!(map.get("refund_sequence_id") instanceof Boolean)&& map.get("refund_sequence_id")!=null){
			Object[] objs = (Object[])map.get("refund_sequence_id");
			if(objs.length > 0){
				this.setRefund_sequence_id((Integer)objs[0]);
			}
		}
		if(!(map.get("refund_sequence_number_next") instanceof Boolean)&& map.get("refund_sequence_number_next")!=null){
			this.setRefund_sequence_number_next((Integer)map.get("refund_sequence_number_next"));
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("sequence_id") instanceof Boolean)&& map.get("sequence_id")!=null){
			Object[] objs = (Object[])map.get("sequence_id");
			if(objs.length > 0){
				this.setSequence_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sequence_number_next") instanceof Boolean)&& map.get("sequence_number_next")!=null){
			this.setSequence_number_next((Integer)map.get("sequence_number_next"));
		}
		if(map.get("show_on_dashboard") instanceof Boolean){
			this.setShow_on_dashboard(((Boolean)map.get("show_on_dashboard"))? "true" : "false");
		}
		if(!(map.get("type") instanceof Boolean)&& map.get("type")!=null){
			this.setType((String)map.get("type"));
		}
		if(!(map.get("type_control_ids") instanceof Boolean)&& map.get("type_control_ids")!=null){
			Object[] objs = (Object[])map.get("type_control_ids");
			if(objs.length > 0){
				Integer[] type_control_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setType_control_ids(Arrays.toString(type_control_ids).replace(" ",""));
			}
		}
		if(map.get("update_posted") instanceof Boolean){
			this.setUpdate_posted(((Boolean)map.get("update_posted"))? "true" : "false");
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccount_control_ids()!=null&&this.getAccount_control_idsDirtyFlag()){
			map.put("account_control_ids",this.getAccount_control_ids());
		}else if(this.getAccount_control_idsDirtyFlag()){
			map.put("account_control_ids",false);
		}
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getAlias_domain()!=null&&this.getAlias_domainDirtyFlag()){
			map.put("alias_domain",this.getAlias_domain());
		}else if(this.getAlias_domainDirtyFlag()){
			map.put("alias_domain",false);
		}
		if(this.getAlias_id()!=null&&this.getAlias_idDirtyFlag()){
			map.put("alias_id",this.getAlias_id());
		}else if(this.getAlias_idDirtyFlag()){
			map.put("alias_id",false);
		}
		if(this.getAlias_name()!=null&&this.getAlias_nameDirtyFlag()){
			map.put("alias_name",this.getAlias_name());
		}else if(this.getAlias_nameDirtyFlag()){
			map.put("alias_name",false);
		}
		if(this.getAt_least_one_inbound()!=null&&this.getAt_least_one_inboundDirtyFlag()){
			map.put("at_least_one_inbound",Boolean.parseBoolean(this.getAt_least_one_inbound()));		
		}		if(this.getAt_least_one_outbound()!=null&&this.getAt_least_one_outboundDirtyFlag()){
			map.put("at_least_one_outbound",Boolean.parseBoolean(this.getAt_least_one_outbound()));		
		}		if(this.getBank_account_id()!=null&&this.getBank_account_idDirtyFlag()){
			map.put("bank_account_id",this.getBank_account_id());
		}else if(this.getBank_account_idDirtyFlag()){
			map.put("bank_account_id",false);
		}
		if(this.getBank_acc_number()!=null&&this.getBank_acc_numberDirtyFlag()){
			map.put("bank_acc_number",this.getBank_acc_number());
		}else if(this.getBank_acc_numberDirtyFlag()){
			map.put("bank_acc_number",false);
		}
		if(this.getBank_id()!=null&&this.getBank_idDirtyFlag()){
			map.put("bank_id",this.getBank_id());
		}else if(this.getBank_idDirtyFlag()){
			map.put("bank_id",false);
		}
		if(this.getBank_statements_source()!=null&&this.getBank_statements_sourceDirtyFlag()){
			map.put("bank_statements_source",this.getBank_statements_source());
		}else if(this.getBank_statements_sourceDirtyFlag()){
			map.put("bank_statements_source",false);
		}
		if(this.getBelongs_to_company()!=null&&this.getBelongs_to_companyDirtyFlag()){
			map.put("belongs_to_company",Boolean.parseBoolean(this.getBelongs_to_company()));		
		}		if(this.getCode()!=null&&this.getCodeDirtyFlag()){
			map.put("code",this.getCode());
		}else if(this.getCodeDirtyFlag()){
			map.put("code",false);
		}
		if(this.getColor()!=null&&this.getColorDirtyFlag()){
			map.put("color",this.getColor());
		}else if(this.getColorDirtyFlag()){
			map.put("color",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_partner_id()!=null&&this.getCompany_partner_idDirtyFlag()){
			map.put("company_partner_id",this.getCompany_partner_id());
		}else if(this.getCompany_partner_idDirtyFlag()){
			map.put("company_partner_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDefault_credit_account_id()!=null&&this.getDefault_credit_account_idDirtyFlag()){
			map.put("default_credit_account_id",this.getDefault_credit_account_id());
		}else if(this.getDefault_credit_account_idDirtyFlag()){
			map.put("default_credit_account_id",false);
		}
		if(this.getDefault_debit_account_id()!=null&&this.getDefault_debit_account_idDirtyFlag()){
			map.put("default_debit_account_id",this.getDefault_debit_account_id());
		}else if(this.getDefault_debit_account_idDirtyFlag()){
			map.put("default_debit_account_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getGroup_invoice_lines()!=null&&this.getGroup_invoice_linesDirtyFlag()){
			map.put("group_invoice_lines",Boolean.parseBoolean(this.getGroup_invoice_lines()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInbound_payment_method_ids()!=null&&this.getInbound_payment_method_idsDirtyFlag()){
			map.put("inbound_payment_method_ids",this.getInbound_payment_method_ids());
		}else if(this.getInbound_payment_method_idsDirtyFlag()){
			map.put("inbound_payment_method_ids",false);
		}
		if(this.getJournal_id()!=null&&this.getJournal_idDirtyFlag()){
			map.put("journal_id",this.getJournal_id());
		}else if(this.getJournal_idDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getKanban_dashboard()!=null&&this.getKanban_dashboardDirtyFlag()){
			map.put("kanban_dashboard",this.getKanban_dashboard());
		}else if(this.getKanban_dashboardDirtyFlag()){
			map.put("kanban_dashboard",false);
		}
		if(this.getKanban_dashboard_graph()!=null&&this.getKanban_dashboard_graphDirtyFlag()){
			map.put("kanban_dashboard_graph",this.getKanban_dashboard_graph());
		}else if(this.getKanban_dashboard_graphDirtyFlag()){
			map.put("kanban_dashboard_graph",false);
		}
		if(this.getLoss_account_id()!=null&&this.getLoss_account_idDirtyFlag()){
			map.put("loss_account_id",this.getLoss_account_id());
		}else if(this.getLoss_account_idDirtyFlag()){
			map.put("loss_account_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getOutbound_payment_method_ids()!=null&&this.getOutbound_payment_method_idsDirtyFlag()){
			map.put("outbound_payment_method_ids",this.getOutbound_payment_method_ids());
		}else if(this.getOutbound_payment_method_idsDirtyFlag()){
			map.put("outbound_payment_method_ids",false);
		}
		if(this.getPost_at_bank_rec()!=null&&this.getPost_at_bank_recDirtyFlag()){
			map.put("post_at_bank_rec",Boolean.parseBoolean(this.getPost_at_bank_rec()));		
		}		if(this.getProfit_account_id()!=null&&this.getProfit_account_idDirtyFlag()){
			map.put("profit_account_id",this.getProfit_account_id());
		}else if(this.getProfit_account_idDirtyFlag()){
			map.put("profit_account_id",false);
		}
		if(this.getRefund_sequence()!=null&&this.getRefund_sequenceDirtyFlag()){
			map.put("refund_sequence",Boolean.parseBoolean(this.getRefund_sequence()));		
		}		if(this.getRefund_sequence_id()!=null&&this.getRefund_sequence_idDirtyFlag()){
			map.put("refund_sequence_id",this.getRefund_sequence_id());
		}else if(this.getRefund_sequence_idDirtyFlag()){
			map.put("refund_sequence_id",false);
		}
		if(this.getRefund_sequence_number_next()!=null&&this.getRefund_sequence_number_nextDirtyFlag()){
			map.put("refund_sequence_number_next",this.getRefund_sequence_number_next());
		}else if(this.getRefund_sequence_number_nextDirtyFlag()){
			map.put("refund_sequence_number_next",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getSequence_id()!=null&&this.getSequence_idDirtyFlag()){
			map.put("sequence_id",this.getSequence_id());
		}else if(this.getSequence_idDirtyFlag()){
			map.put("sequence_id",false);
		}
		if(this.getSequence_number_next()!=null&&this.getSequence_number_nextDirtyFlag()){
			map.put("sequence_number_next",this.getSequence_number_next());
		}else if(this.getSequence_number_nextDirtyFlag()){
			map.put("sequence_number_next",false);
		}
		if(this.getShow_on_dashboard()!=null&&this.getShow_on_dashboardDirtyFlag()){
			map.put("show_on_dashboard",Boolean.parseBoolean(this.getShow_on_dashboard()));		
		}		if(this.getType()!=null&&this.getTypeDirtyFlag()){
			map.put("type",this.getType());
		}else if(this.getTypeDirtyFlag()){
			map.put("type",false);
		}
		if(this.getType_control_ids()!=null&&this.getType_control_idsDirtyFlag()){
			map.put("type_control_ids",this.getType_control_ids());
		}else if(this.getType_control_idsDirtyFlag()){
			map.put("type_control_ids",false);
		}
		if(this.getUpdate_posted()!=null&&this.getUpdate_postedDirtyFlag()){
			map.put("update_posted",Boolean.parseBoolean(this.getUpdate_posted()));		
		}		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
