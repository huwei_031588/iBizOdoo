package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Maintenance_team;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_teamSearchContext;

/**
 * 实体 [保养团队] 存储对象
 */
public interface Maintenance_teamRepository extends Repository<Maintenance_team> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Maintenance_team> searchDefault(Maintenance_teamSearchContext context);

    Maintenance_team convert2PO(cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team domain , Maintenance_team po) ;

    cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team convert2Domain( Maintenance_team po ,cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team domain) ;

}
