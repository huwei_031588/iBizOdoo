package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicleSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicleService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicleOdooClient;
import cn.ibizlab.odoo.core.odoo_fleet.clientmodel.fleet_vehicleClientModel;

/**
 * 实体[车辆] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicleServiceImpl implements IFleet_vehicleService {

    @Autowired
    fleet_vehicleOdooClient fleet_vehicleOdooClient;


    @Override
    public boolean remove(Integer id) {
        fleet_vehicleClientModel clientModel = new fleet_vehicleClientModel();
        clientModel.setId(id);
		fleet_vehicleOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Fleet_vehicle et) {
        fleet_vehicleClientModel clientModel = convert2Model(et,null);
		fleet_vehicleOdooClient.create(clientModel);
        Fleet_vehicle rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle> list){
    }

    @Override
    public boolean update(Fleet_vehicle et) {
        fleet_vehicleClientModel clientModel = convert2Model(et,null);
		fleet_vehicleOdooClient.update(clientModel);
        Fleet_vehicle rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Fleet_vehicle> list){
    }

    @Override
    public Fleet_vehicle get(Integer id) {
        fleet_vehicleClientModel clientModel = new fleet_vehicleClientModel();
        clientModel.setId(id);
		fleet_vehicleOdooClient.get(clientModel);
        Fleet_vehicle et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Fleet_vehicle();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle> searchDefault(Fleet_vehicleSearchContext context) {
        List<Fleet_vehicle> list = new ArrayList<Fleet_vehicle>();
        Page<fleet_vehicleClientModel> clientModelList = fleet_vehicleOdooClient.search(context);
        for(fleet_vehicleClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Fleet_vehicle>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public fleet_vehicleClientModel convert2Model(Fleet_vehicle domain , fleet_vehicleClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new fleet_vehicleClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("fuel_typedirtyflag"))
                model.setFuel_type(domain.getFuelType());
            if((Boolean) domain.getExtensionparams().get("tag_idsdirtyflag"))
                model.setTag_ids(domain.getTagIds());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("powerdirtyflag"))
                model.setPower(domain.getPower());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("residual_valuedirtyflag"))
                model.setResidual_value(domain.getResidualValue());
            if((Boolean) domain.getExtensionparams().get("odometerdirtyflag"))
                model.setOdometer(domain.getOdometer());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("fuel_logs_countdirtyflag"))
                model.setFuel_logs_count(domain.getFuelLogsCount());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("model_yeardirtyflag"))
                model.setModel_year(domain.getModelYear());
            if((Boolean) domain.getExtensionparams().get("car_valuedirtyflag"))
                model.setCar_value(domain.getCarValue());
            if((Boolean) domain.getExtensionparams().get("license_platedirtyflag"))
                model.setLicense_plate(domain.getLicensePlate());
            if((Boolean) domain.getExtensionparams().get("contract_renewal_overduedirtyflag"))
                model.setContract_renewal_overdue(domain.getContractRenewalOverdue());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("acquisition_datedirtyflag"))
                model.setAcquisition_date(domain.getAcquisitionDate());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("horsepower_taxdirtyflag"))
                model.setHorsepower_tax(domain.getHorsepowerTax());
            if((Boolean) domain.getExtensionparams().get("locationdirtyflag"))
                model.setLocation(domain.getLocation());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("horsepowerdirtyflag"))
                model.setHorsepower(domain.getHorsepower());
            if((Boolean) domain.getExtensionparams().get("vin_sndirtyflag"))
                model.setVin_sn(domain.getVinSn());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("odometer_countdirtyflag"))
                model.setOdometer_count(domain.getOdometerCount());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("contract_renewal_namedirtyflag"))
                model.setContract_renewal_name(domain.getContractRenewalName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("seatsdirtyflag"))
                model.setSeats(domain.getSeats());
            if((Boolean) domain.getExtensionparams().get("log_fueldirtyflag"))
                model.setLog_fuel(domain.getLogFuel());
            if((Boolean) domain.getExtensionparams().get("cost_countdirtyflag"))
                model.setCost_count(domain.getCostCount());
            if((Boolean) domain.getExtensionparams().get("log_driversdirtyflag"))
                model.setLog_drivers(domain.getLogDrivers());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("contract_renewal_due_soondirtyflag"))
                model.setContract_renewal_due_soon(domain.getContractRenewalDueSoon());
            if((Boolean) domain.getExtensionparams().get("odometer_unitdirtyflag"))
                model.setOdometer_unit(domain.getOdometerUnit());
            if((Boolean) domain.getExtensionparams().get("co2dirtyflag"))
                model.setCo2(domain.getCo2());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("log_contractsdirtyflag"))
                model.setLog_contracts(domain.getLogContracts());
            if((Boolean) domain.getExtensionparams().get("first_contract_datedirtyflag"))
                model.setFirst_contract_date(domain.getFirstContractDate());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("doorsdirtyflag"))
                model.setDoors(domain.getDoors());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("log_servicesdirtyflag"))
                model.setLog_services(domain.getLogServices());
            if((Boolean) domain.getExtensionparams().get("contract_countdirtyflag"))
                model.setContract_count(domain.getContractCount());
            if((Boolean) domain.getExtensionparams().get("transmissiondirtyflag"))
                model.setTransmission(domain.getTransmission());
            if((Boolean) domain.getExtensionparams().get("service_countdirtyflag"))
                model.setService_count(domain.getServiceCount());
            if((Boolean) domain.getExtensionparams().get("contract_renewal_totaldirtyflag"))
                model.setContract_renewal_total(domain.getContractRenewalTotal());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("driver_id_textdirtyflag"))
                model.setDriver_id_text(domain.getDriverIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("model_id_textdirtyflag"))
                model.setModel_id_text(domain.getModelIdText());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("brand_id_textdirtyflag"))
                model.setBrand_id_text(domain.getBrandIdText());
            if((Boolean) domain.getExtensionparams().get("state_id_textdirtyflag"))
                model.setState_id_text(domain.getStateIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("model_iddirtyflag"))
                model.setModel_id(domain.getModelId());
            if((Boolean) domain.getExtensionparams().get("brand_iddirtyflag"))
                model.setBrand_id(domain.getBrandId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("state_iddirtyflag"))
                model.setState_id(domain.getStateId());
            if((Boolean) domain.getExtensionparams().get("driver_iddirtyflag"))
                model.setDriver_id(domain.getDriverId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Fleet_vehicle convert2Domain( fleet_vehicleClientModel model ,Fleet_vehicle domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Fleet_vehicle();
        }

        if(model.getFuel_typeDirtyFlag())
            domain.setFuelType(model.getFuel_type());
        if(model.getTag_idsDirtyFlag())
            domain.setTagIds(model.getTag_ids());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getPowerDirtyFlag())
            domain.setPower(model.getPower());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getResidual_valueDirtyFlag())
            domain.setResidualValue(model.getResidual_value());
        if(model.getOdometerDirtyFlag())
            domain.setOdometer(model.getOdometer());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getFuel_logs_countDirtyFlag())
            domain.setFuelLogsCount(model.getFuel_logs_count());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getModel_yearDirtyFlag())
            domain.setModelYear(model.getModel_year());
        if(model.getCar_valueDirtyFlag())
            domain.setCarValue(model.getCar_value());
        if(model.getLicense_plateDirtyFlag())
            domain.setLicensePlate(model.getLicense_plate());
        if(model.getContract_renewal_overdueDirtyFlag())
            domain.setContractRenewalOverdue(model.getContract_renewal_overdue());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getAcquisition_dateDirtyFlag())
            domain.setAcquisitionDate(model.getAcquisition_date());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getHorsepower_taxDirtyFlag())
            domain.setHorsepowerTax(model.getHorsepower_tax());
        if(model.getLocationDirtyFlag())
            domain.setLocation(model.getLocation());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getHorsepowerDirtyFlag())
            domain.setHorsepower(model.getHorsepower());
        if(model.getVin_snDirtyFlag())
            domain.setVinSn(model.getVin_sn());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getOdometer_countDirtyFlag())
            domain.setOdometerCount(model.getOdometer_count());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getContract_renewal_nameDirtyFlag())
            domain.setContractRenewalName(model.getContract_renewal_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getSeatsDirtyFlag())
            domain.setSeats(model.getSeats());
        if(model.getLog_fuelDirtyFlag())
            domain.setLogFuel(model.getLog_fuel());
        if(model.getCost_countDirtyFlag())
            domain.setCostCount(model.getCost_count());
        if(model.getLog_driversDirtyFlag())
            domain.setLogDrivers(model.getLog_drivers());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getContract_renewal_due_soonDirtyFlag())
            domain.setContractRenewalDueSoon(model.getContract_renewal_due_soon());
        if(model.getOdometer_unitDirtyFlag())
            domain.setOdometerUnit(model.getOdometer_unit());
        if(model.getCo2DirtyFlag())
            domain.setCo2(model.getCo2());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getLog_contractsDirtyFlag())
            domain.setLogContracts(model.getLog_contracts());
        if(model.getFirst_contract_dateDirtyFlag())
            domain.setFirstContractDate(model.getFirst_contract_date());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getDoorsDirtyFlag())
            domain.setDoors(model.getDoors());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getLog_servicesDirtyFlag())
            domain.setLogServices(model.getLog_services());
        if(model.getContract_countDirtyFlag())
            domain.setContractCount(model.getContract_count());
        if(model.getTransmissionDirtyFlag())
            domain.setTransmission(model.getTransmission());
        if(model.getService_countDirtyFlag())
            domain.setServiceCount(model.getService_count());
        if(model.getContract_renewal_totalDirtyFlag())
            domain.setContractRenewalTotal(model.getContract_renewal_total());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getDriver_id_textDirtyFlag())
            domain.setDriverIdText(model.getDriver_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getModel_id_textDirtyFlag())
            domain.setModelIdText(model.getModel_id_text());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getBrand_id_textDirtyFlag())
            domain.setBrandIdText(model.getBrand_id_text());
        if(model.getState_id_textDirtyFlag())
            domain.setStateIdText(model.getState_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getModel_idDirtyFlag())
            domain.setModelId(model.getModel_id());
        if(model.getBrand_idDirtyFlag())
            domain.setBrandId(model.getBrand_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getState_idDirtyFlag())
            domain.setStateId(model.getState_id());
        if(model.getDriver_idDirtyFlag())
            domain.setDriverId(model.getDriver_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



