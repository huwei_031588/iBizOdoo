package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_holidays_summary_dept;

/**
 * 实体[hr_holidays_summary_dept] 服务对象接口
 */
public interface hr_holidays_summary_deptRepository{


    public hr_holidays_summary_dept createPO() ;
        public void remove(String id);

        public void get(String id);

        public List<hr_holidays_summary_dept> search();

        public void createBatch(hr_holidays_summary_dept hr_holidays_summary_dept);

        public void create(hr_holidays_summary_dept hr_holidays_summary_dept);

        public void removeBatch(String id);

        public void update(hr_holidays_summary_dept hr_holidays_summary_dept);

        public void updateBatch(hr_holidays_summary_dept hr_holidays_summary_dept);


}
