package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attributeSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_attributeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_attributeOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_attributeClientModel;

/**
 * 实体[产品属性] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_attributeServiceImpl implements IProduct_attributeService {

    @Autowired
    product_attributeOdooClient product_attributeOdooClient;


    @Override
    public boolean create(Product_attribute et) {
        product_attributeClientModel clientModel = convert2Model(et,null);
		product_attributeOdooClient.create(clientModel);
        Product_attribute rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_attribute> list){
    }

    @Override
    public boolean update(Product_attribute et) {
        product_attributeClientModel clientModel = convert2Model(et,null);
		product_attributeOdooClient.update(clientModel);
        Product_attribute rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_attribute> list){
    }

    @Override
    public boolean remove(Integer id) {
        product_attributeClientModel clientModel = new product_attributeClientModel();
        clientModel.setId(id);
		product_attributeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Product_attribute get(Integer id) {
        product_attributeClientModel clientModel = new product_attributeClientModel();
        clientModel.setId(id);
		product_attributeOdooClient.get(clientModel);
        Product_attribute et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_attribute();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_attribute> searchDefault(Product_attributeSearchContext context) {
        List<Product_attribute> list = new ArrayList<Product_attribute>();
        Page<product_attributeClientModel> clientModelList = product_attributeOdooClient.search(context);
        for(product_attributeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_attribute>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_attributeClientModel convert2Model(Product_attribute domain , product_attributeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_attributeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("attribute_line_idsdirtyflag"))
                model.setAttribute_line_ids(domain.getAttributeLineIds());
            if((Boolean) domain.getExtensionparams().get("create_variantdirtyflag"))
                model.setCreate_variant(domain.getCreateVariant());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("value_idsdirtyflag"))
                model.setValue_ids(domain.getValueIds());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_attribute convert2Domain( product_attributeClientModel model ,Product_attribute domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_attribute();
        }

        if(model.getAttribute_line_idsDirtyFlag())
            domain.setAttributeLineIds(model.getAttribute_line_ids());
        if(model.getCreate_variantDirtyFlag())
            domain.setCreateVariant(model.getCreate_variant());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getValue_idsDirtyFlag())
            domain.setValueIds(model.getValue_ids());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



