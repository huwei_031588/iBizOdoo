package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mail_mass_mailing_list] 对象
 */
public interface mail_mass_mailing_list {

    public String getActive();

    public void setActive(String active);

    public String getContact_ids();

    public void setContact_ids(String contact_ids);

    public Integer getContact_nbr();

    public void setContact_nbr(Integer contact_nbr);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_public();

    public void setIs_public(String is_public);

    public String getName();

    public void setName(String name);

    public String getPopup_content();

    public void setPopup_content(String popup_content);

    public String getPopup_redirect_url();

    public void setPopup_redirect_url(String popup_redirect_url);

    public String getSubscription_contact_ids();

    public void setSubscription_contact_ids(String subscription_contact_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
