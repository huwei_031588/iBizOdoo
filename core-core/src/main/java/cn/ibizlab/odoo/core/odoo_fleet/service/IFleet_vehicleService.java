package cn.ibizlab.odoo.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicleSearchContext;


/**
 * 实体[Fleet_vehicle] 服务对象接口
 */
public interface IFleet_vehicleService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Fleet_vehicle et) ;
    void createBatch(List<Fleet_vehicle> list) ;
    boolean update(Fleet_vehicle et) ;
    void updateBatch(List<Fleet_vehicle> list) ;
    Fleet_vehicle get(Integer key) ;
    Page<Fleet_vehicle> searchDefault(Fleet_vehicleSearchContext context) ;

}



