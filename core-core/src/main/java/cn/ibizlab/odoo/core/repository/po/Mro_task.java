package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_taskSearchContext;

/**
 * 实体 [Maintenance Task] 存储模型
 */
public interface Mro_task{

    /**
     * 零件
     */
    String getParts_lines();

    void setParts_lines(String parts_lines);

    /**
     * 获取 [零件]脏标记
     */
    boolean getParts_linesDirtyFlag();

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * Tools Description
     */
    String getTools_description();

    void setTools_description(String tools_description);

    /**
     * 获取 [Tools Description]脏标记
     */
    boolean getTools_descriptionDirtyFlag();

    /**
     * Labor Description
     */
    String getLabor_description();

    void setLabor_description(String labor_description);

    /**
     * 获取 [Labor Description]脏标记
     */
    boolean getLabor_descriptionDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 保养类型
     */
    String getMaintenance_type();

    void setMaintenance_type(String maintenance_type);

    /**
     * 获取 [保养类型]脏标记
     */
    boolean getMaintenance_typeDirtyFlag();

    /**
     * Operations Description
     */
    String getOperations_description();

    void setOperations_description(String operations_description);

    /**
     * 获取 [Operations Description]脏标记
     */
    boolean getOperations_descriptionDirtyFlag();

    /**
     * Documentation Description
     */
    String getDocumentation_description();

    void setDocumentation_description(String documentation_description);

    /**
     * 获取 [Documentation Description]脏标记
     */
    boolean getDocumentation_descriptionDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * Asset Category
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [Asset Category]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * Asset Category
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [Asset Category]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
