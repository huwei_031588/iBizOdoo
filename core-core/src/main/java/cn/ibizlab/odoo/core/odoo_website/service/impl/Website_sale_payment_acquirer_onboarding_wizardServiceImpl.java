package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_sale_payment_acquirer_onboarding_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_sale_payment_acquirer_onboarding_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_website.client.website_sale_payment_acquirer_onboarding_wizardOdooClient;
import cn.ibizlab.odoo.core.odoo_website.clientmodel.website_sale_payment_acquirer_onboarding_wizardClientModel;

/**
 * 实体[website.sale.payment.acquirer.onboarding.wizard] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_sale_payment_acquirer_onboarding_wizardServiceImpl implements IWebsite_sale_payment_acquirer_onboarding_wizardService {

    @Autowired
    website_sale_payment_acquirer_onboarding_wizardOdooClient website_sale_payment_acquirer_onboarding_wizardOdooClient;


    @Override
    public Website_sale_payment_acquirer_onboarding_wizard get(Integer id) {
        website_sale_payment_acquirer_onboarding_wizardClientModel clientModel = new website_sale_payment_acquirer_onboarding_wizardClientModel();
        clientModel.setId(id);
		website_sale_payment_acquirer_onboarding_wizardOdooClient.get(clientModel);
        Website_sale_payment_acquirer_onboarding_wizard et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Website_sale_payment_acquirer_onboarding_wizard();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Website_sale_payment_acquirer_onboarding_wizard et) {
        website_sale_payment_acquirer_onboarding_wizardClientModel clientModel = convert2Model(et,null);
		website_sale_payment_acquirer_onboarding_wizardOdooClient.update(clientModel);
        Website_sale_payment_acquirer_onboarding_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Website_sale_payment_acquirer_onboarding_wizard> list){
    }

    @Override
    public boolean remove(Integer id) {
        website_sale_payment_acquirer_onboarding_wizardClientModel clientModel = new website_sale_payment_acquirer_onboarding_wizardClientModel();
        clientModel.setId(id);
		website_sale_payment_acquirer_onboarding_wizardOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Website_sale_payment_acquirer_onboarding_wizard et) {
        website_sale_payment_acquirer_onboarding_wizardClientModel clientModel = convert2Model(et,null);
		website_sale_payment_acquirer_onboarding_wizardOdooClient.create(clientModel);
        Website_sale_payment_acquirer_onboarding_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_sale_payment_acquirer_onboarding_wizard> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_sale_payment_acquirer_onboarding_wizard> searchDefault(Website_sale_payment_acquirer_onboarding_wizardSearchContext context) {
        List<Website_sale_payment_acquirer_onboarding_wizard> list = new ArrayList<Website_sale_payment_acquirer_onboarding_wizard>();
        Page<website_sale_payment_acquirer_onboarding_wizardClientModel> clientModelList = website_sale_payment_acquirer_onboarding_wizardOdooClient.search(context);
        for(website_sale_payment_acquirer_onboarding_wizardClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Website_sale_payment_acquirer_onboarding_wizard>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public website_sale_payment_acquirer_onboarding_wizardClientModel convert2Model(Website_sale_payment_acquirer_onboarding_wizard domain , website_sale_payment_acquirer_onboarding_wizardClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new website_sale_payment_acquirer_onboarding_wizardClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("paypal_email_accountdirtyflag"))
                model.setPaypal_email_account(domain.getPaypalEmailAccount());
            if((Boolean) domain.getExtensionparams().get("journal_namedirtyflag"))
                model.setJournal_name(domain.getJournalName());
            if((Boolean) domain.getExtensionparams().get("stripe_secret_keydirtyflag"))
                model.setStripe_secret_key(domain.getStripeSecretKey());
            if((Boolean) domain.getExtensionparams().get("acc_numberdirtyflag"))
                model.setAcc_number(domain.getAccNumber());
            if((Boolean) domain.getExtensionparams().get("manual_post_msgdirtyflag"))
                model.setManual_post_msg(domain.getManualPostMsg());
            if((Boolean) domain.getExtensionparams().get("payment_methoddirtyflag"))
                model.setPayment_method(domain.getPaymentMethod());
            if((Boolean) domain.getExtensionparams().get("paypal_pdt_tokendirtyflag"))
                model.setPaypal_pdt_token(domain.getPaypalPdtToken());
            if((Boolean) domain.getExtensionparams().get("manual_namedirtyflag"))
                model.setManual_name(domain.getManualName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("paypal_seller_accountdirtyflag"))
                model.setPaypal_seller_account(domain.getPaypalSellerAccount());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("stripe_publishable_keydirtyflag"))
                model.setStripe_publishable_key(domain.getStripePublishableKey());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Website_sale_payment_acquirer_onboarding_wizard convert2Domain( website_sale_payment_acquirer_onboarding_wizardClientModel model ,Website_sale_payment_acquirer_onboarding_wizard domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Website_sale_payment_acquirer_onboarding_wizard();
        }

        if(model.getPaypal_email_accountDirtyFlag())
            domain.setPaypalEmailAccount(model.getPaypal_email_account());
        if(model.getJournal_nameDirtyFlag())
            domain.setJournalName(model.getJournal_name());
        if(model.getStripe_secret_keyDirtyFlag())
            domain.setStripeSecretKey(model.getStripe_secret_key());
        if(model.getAcc_numberDirtyFlag())
            domain.setAccNumber(model.getAcc_number());
        if(model.getManual_post_msgDirtyFlag())
            domain.setManualPostMsg(model.getManual_post_msg());
        if(model.getPayment_methodDirtyFlag())
            domain.setPaymentMethod(model.getPayment_method());
        if(model.getPaypal_pdt_tokenDirtyFlag())
            domain.setPaypalPdtToken(model.getPaypal_pdt_token());
        if(model.getManual_nameDirtyFlag())
            domain.setManualName(model.getManual_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getPaypal_seller_accountDirtyFlag())
            domain.setPaypalSellerAccount(model.getPaypal_seller_account());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getStripe_publishable_keyDirtyFlag())
            domain.setStripePublishableKey(model.getStripe_publishable_key());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



