package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isurvey_user_input_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_user_input_line] 服务对象接口
 */
public interface Isurvey_user_input_lineClientService{

    public Isurvey_user_input_line createModel() ;

    public void remove(Isurvey_user_input_line survey_user_input_line);

    public void updateBatch(List<Isurvey_user_input_line> survey_user_input_lines);

    public void get(Isurvey_user_input_line survey_user_input_line);

    public void update(Isurvey_user_input_line survey_user_input_line);

    public void createBatch(List<Isurvey_user_input_line> survey_user_input_lines);

    public void create(Isurvey_user_input_line survey_user_input_line);

    public Page<Isurvey_user_input_line> search(SearchContext context);

    public void removeBatch(List<Isurvey_user_input_line> survey_user_input_lines);

    public Page<Isurvey_user_input_line> select(SearchContext context);

}
