package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Website_seo_metadata;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_seo_metadataSearchContext;

/**
 * 实体 [SEO元数据] 存储对象
 */
public interface Website_seo_metadataRepository extends Repository<Website_seo_metadata> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Website_seo_metadata> searchDefault(Website_seo_metadataSearchContext context);

    Website_seo_metadata convert2PO(cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata domain , Website_seo_metadata po) ;

    cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata convert2Domain( Website_seo_metadata po ,cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata domain) ;

}
