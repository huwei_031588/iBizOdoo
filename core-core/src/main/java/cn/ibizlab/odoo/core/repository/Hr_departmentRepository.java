package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_department;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_departmentSearchContext;

/**
 * 实体 [HR 部门] 存储对象
 */
public interface Hr_departmentRepository extends Repository<Hr_department> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_department> searchDefault(Hr_departmentSearchContext context);

    Hr_department convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department domain , Hr_department po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department convert2Domain( Hr_department po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department domain) ;

}
