package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_vehicle;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicleSearchContext;

/**
 * 实体 [车辆] 存储对象
 */
public interface Fleet_vehicleRepository extends Repository<Fleet_vehicle> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_vehicle> searchDefault(Fleet_vehicleSearchContext context);

    Fleet_vehicle convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle domain , Fleet_vehicle po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle convert2Domain( Fleet_vehicle po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle domain) ;

}
