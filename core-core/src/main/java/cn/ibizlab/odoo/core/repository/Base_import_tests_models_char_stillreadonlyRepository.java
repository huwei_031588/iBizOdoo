package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_import_tests_models_char_stillreadonly;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_stillreadonlySearchContext;

/**
 * 实体 [测试:基本导入模型，字符仍然只读] 存储对象
 */
public interface Base_import_tests_models_char_stillreadonlyRepository extends Repository<Base_import_tests_models_char_stillreadonly> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_import_tests_models_char_stillreadonly> searchDefault(Base_import_tests_models_char_stillreadonlySearchContext context);

    Base_import_tests_models_char_stillreadonly convert2PO(cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_stillreadonly domain , Base_import_tests_models_char_stillreadonly po) ;

    cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_stillreadonly convert2Domain( Base_import_tests_models_char_stillreadonly po ,cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_stillreadonly domain) ;

}
