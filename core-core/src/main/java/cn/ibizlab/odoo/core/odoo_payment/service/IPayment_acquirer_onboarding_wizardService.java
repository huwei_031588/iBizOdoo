package cn.ibizlab.odoo.core.odoo_payment.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;


/**
 * 实体[Payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface IPayment_acquirer_onboarding_wizardService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Payment_acquirer_onboarding_wizard et) ;
    void createBatch(List<Payment_acquirer_onboarding_wizard> list) ;
    Payment_acquirer_onboarding_wizard get(Integer key) ;
    boolean update(Payment_acquirer_onboarding_wizard et) ;
    void updateBatch(List<Payment_acquirer_onboarding_wizard> list) ;
    Page<Payment_acquirer_onboarding_wizard> searchDefault(Payment_acquirer_onboarding_wizardSearchContext context) ;

}



