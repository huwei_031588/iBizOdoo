package cn.ibizlab.odoo.core.odoo_base.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[res_currency] 对象
 */
public class res_currencyClientModel implements Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 货币子单位
     */
    public String currency_subunit_label;

    @JsonIgnore
    public boolean currency_subunit_labelDirtyFlag;
    
    /**
     * 货币单位
     */
    public String currency_unit_label;

    @JsonIgnore
    public boolean currency_unit_labelDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 小数点位置
     */
    public Integer decimal_places;

    @JsonIgnore
    public boolean decimal_placesDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 币种
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 符号位置
     */
    public String position;

    @JsonIgnore
    public boolean positionDirtyFlag;
    
    /**
     * 当前汇率
     */
    public Double rate;

    @JsonIgnore
    public boolean rateDirtyFlag;
    
    /**
     * 比率
     */
    public String rate_ids;

    @JsonIgnore
    public boolean rate_idsDirtyFlag;
    
    /**
     * 舍入系数
     */
    public Double rounding;

    @JsonIgnore
    public boolean roundingDirtyFlag;
    
    /**
     * 货币符号
     */
    public String symbol;

    @JsonIgnore
    public boolean symbolDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [货币子单位]
     */
    @JsonProperty("currency_subunit_label")
    public String getCurrency_subunit_label(){
        return this.currency_subunit_label ;
    }

    /**
     * 设置 [货币子单位]
     */
    @JsonProperty("currency_subunit_label")
    public void setCurrency_subunit_label(String  currency_subunit_label){
        this.currency_subunit_label = currency_subunit_label ;
        this.currency_subunit_labelDirtyFlag = true ;
    }

     /**
     * 获取 [货币子单位]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_subunit_labelDirtyFlag(){
        return this.currency_subunit_labelDirtyFlag ;
    }   

    /**
     * 获取 [货币单位]
     */
    @JsonProperty("currency_unit_label")
    public String getCurrency_unit_label(){
        return this.currency_unit_label ;
    }

    /**
     * 设置 [货币单位]
     */
    @JsonProperty("currency_unit_label")
    public void setCurrency_unit_label(String  currency_unit_label){
        this.currency_unit_label = currency_unit_label ;
        this.currency_unit_labelDirtyFlag = true ;
    }

     /**
     * 获取 [货币单位]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_unit_labelDirtyFlag(){
        return this.currency_unit_labelDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [小数点位置]
     */
    @JsonProperty("decimal_places")
    public Integer getDecimal_places(){
        return this.decimal_places ;
    }

    /**
     * 设置 [小数点位置]
     */
    @JsonProperty("decimal_places")
    public void setDecimal_places(Integer  decimal_places){
        this.decimal_places = decimal_places ;
        this.decimal_placesDirtyFlag = true ;
    }

     /**
     * 获取 [小数点位置]脏标记
     */
    @JsonIgnore
    public boolean getDecimal_placesDirtyFlag(){
        return this.decimal_placesDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [符号位置]
     */
    @JsonProperty("position")
    public String getPosition(){
        return this.position ;
    }

    /**
     * 设置 [符号位置]
     */
    @JsonProperty("position")
    public void setPosition(String  position){
        this.position = position ;
        this.positionDirtyFlag = true ;
    }

     /**
     * 获取 [符号位置]脏标记
     */
    @JsonIgnore
    public boolean getPositionDirtyFlag(){
        return this.positionDirtyFlag ;
    }   

    /**
     * 获取 [当前汇率]
     */
    @JsonProperty("rate")
    public Double getRate(){
        return this.rate ;
    }

    /**
     * 设置 [当前汇率]
     */
    @JsonProperty("rate")
    public void setRate(Double  rate){
        this.rate = rate ;
        this.rateDirtyFlag = true ;
    }

     /**
     * 获取 [当前汇率]脏标记
     */
    @JsonIgnore
    public boolean getRateDirtyFlag(){
        return this.rateDirtyFlag ;
    }   

    /**
     * 获取 [比率]
     */
    @JsonProperty("rate_ids")
    public String getRate_ids(){
        return this.rate_ids ;
    }

    /**
     * 设置 [比率]
     */
    @JsonProperty("rate_ids")
    public void setRate_ids(String  rate_ids){
        this.rate_ids = rate_ids ;
        this.rate_idsDirtyFlag = true ;
    }

     /**
     * 获取 [比率]脏标记
     */
    @JsonIgnore
    public boolean getRate_idsDirtyFlag(){
        return this.rate_idsDirtyFlag ;
    }   

    /**
     * 获取 [舍入系数]
     */
    @JsonProperty("rounding")
    public Double getRounding(){
        return this.rounding ;
    }

    /**
     * 设置 [舍入系数]
     */
    @JsonProperty("rounding")
    public void setRounding(Double  rounding){
        this.rounding = rounding ;
        this.roundingDirtyFlag = true ;
    }

     /**
     * 获取 [舍入系数]脏标记
     */
    @JsonIgnore
    public boolean getRoundingDirtyFlag(){
        return this.roundingDirtyFlag ;
    }   

    /**
     * 获取 [货币符号]
     */
    @JsonProperty("symbol")
    public String getSymbol(){
        return this.symbol ;
    }

    /**
     * 设置 [货币符号]
     */
    @JsonProperty("symbol")
    public void setSymbol(String  symbol){
        this.symbol = symbol ;
        this.symbolDirtyFlag = true ;
    }

     /**
     * 获取 [货币符号]脏标记
     */
    @JsonIgnore
    public boolean getSymbolDirtyFlag(){
        return this.symbolDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_subunit_label") instanceof Boolean)&& map.get("currency_subunit_label")!=null){
			this.setCurrency_subunit_label((String)map.get("currency_subunit_label"));
		}
		if(!(map.get("currency_unit_label") instanceof Boolean)&& map.get("currency_unit_label")!=null){
			this.setCurrency_unit_label((String)map.get("currency_unit_label"));
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("decimal_places") instanceof Boolean)&& map.get("decimal_places")!=null){
			this.setDecimal_places((Integer)map.get("decimal_places"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("position") instanceof Boolean)&& map.get("position")!=null){
			this.setPosition((String)map.get("position"));
		}
		if(!(map.get("rate") instanceof Boolean)&& map.get("rate")!=null){
			this.setRate((Double)map.get("rate"));
		}
		if(!(map.get("rate_ids") instanceof Boolean)&& map.get("rate_ids")!=null){
			Object[] objs = (Object[])map.get("rate_ids");
			if(objs.length > 0){
				Integer[] rate_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRate_ids(Arrays.toString(rate_ids).replace(" ",""));
			}
		}
		if(!(map.get("rounding") instanceof Boolean)&& map.get("rounding")!=null){
			this.setRounding((Double)map.get("rounding"));
		}
		if(!(map.get("symbol") instanceof Boolean)&& map.get("symbol")!=null){
			this.setSymbol((String)map.get("symbol"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_subunit_label()!=null&&this.getCurrency_subunit_labelDirtyFlag()){
			map.put("currency_subunit_label",this.getCurrency_subunit_label());
		}else if(this.getCurrency_subunit_labelDirtyFlag()){
			map.put("currency_subunit_label",false);
		}
		if(this.getCurrency_unit_label()!=null&&this.getCurrency_unit_labelDirtyFlag()){
			map.put("currency_unit_label",this.getCurrency_unit_label());
		}else if(this.getCurrency_unit_labelDirtyFlag()){
			map.put("currency_unit_label",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDecimal_places()!=null&&this.getDecimal_placesDirtyFlag()){
			map.put("decimal_places",this.getDecimal_places());
		}else if(this.getDecimal_placesDirtyFlag()){
			map.put("decimal_places",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPosition()!=null&&this.getPositionDirtyFlag()){
			map.put("position",this.getPosition());
		}else if(this.getPositionDirtyFlag()){
			map.put("position",false);
		}
		if(this.getRate()!=null&&this.getRateDirtyFlag()){
			map.put("rate",this.getRate());
		}else if(this.getRateDirtyFlag()){
			map.put("rate",false);
		}
		if(this.getRate_ids()!=null&&this.getRate_idsDirtyFlag()){
			map.put("rate_ids",this.getRate_ids());
		}else if(this.getRate_idsDirtyFlag()){
			map.put("rate_ids",false);
		}
		if(this.getRounding()!=null&&this.getRoundingDirtyFlag()){
			map.put("rounding",this.getRounding());
		}else if(this.getRoundingDirtyFlag()){
			map.put("rounding",false);
		}
		if(this.getSymbol()!=null&&this.getSymbolDirtyFlag()){
			map.put("symbol",this.getSymbol());
		}else if(this.getSymbolDirtyFlag()){
			map.put("symbol",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
