package cn.ibizlab.odoo.core.odoo_mail.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_alias] 对象
 */
public class mail_aliasClientModel implements Serializable{

    /**
     * 安全联系人别名
     */
    public String alias_contact;

    @JsonIgnore
    public boolean alias_contactDirtyFlag;
    
    /**
     * 默认值
     */
    public String alias_defaults;

    @JsonIgnore
    public boolean alias_defaultsDirtyFlag;
    
    /**
     * 网域别名
     */
    public String alias_domain;

    @JsonIgnore
    public boolean alias_domainDirtyFlag;
    
    /**
     * 记录线索ID
     */
    public Integer alias_force_thread_id;

    @JsonIgnore
    public boolean alias_force_thread_idDirtyFlag;
    
    /**
     * 模型别名
     */
    public Integer alias_model_id;

    @JsonIgnore
    public boolean alias_model_idDirtyFlag;
    
    /**
     * 别名
     */
    public String alias_name;

    @JsonIgnore
    public boolean alias_nameDirtyFlag;
    
    /**
     * 上级模型
     */
    public Integer alias_parent_model_id;

    @JsonIgnore
    public boolean alias_parent_model_idDirtyFlag;
    
    /**
     * 上级记录ID
     */
    public Integer alias_parent_thread_id;

    @JsonIgnore
    public boolean alias_parent_thread_idDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer alias_user_id;

    @JsonIgnore
    public boolean alias_user_idDirtyFlag;
    
    /**
     * 所有者
     */
    public String alias_user_id_text;

    @JsonIgnore
    public boolean alias_user_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [安全联系人别名]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return this.alias_contact ;
    }

    /**
     * 设置 [安全联系人别名]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

     /**
     * 获取 [安全联系人别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return this.alias_contactDirtyFlag ;
    }   

    /**
     * 获取 [默认值]
     */
    @JsonProperty("alias_defaults")
    public String getAlias_defaults(){
        return this.alias_defaults ;
    }

    /**
     * 设置 [默认值]
     */
    @JsonProperty("alias_defaults")
    public void setAlias_defaults(String  alias_defaults){
        this.alias_defaults = alias_defaults ;
        this.alias_defaultsDirtyFlag = true ;
    }

     /**
     * 获取 [默认值]脏标记
     */
    @JsonIgnore
    public boolean getAlias_defaultsDirtyFlag(){
        return this.alias_defaultsDirtyFlag ;
    }   

    /**
     * 获取 [网域别名]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return this.alias_domain ;
    }

    /**
     * 设置 [网域别名]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

     /**
     * 获取 [网域别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return this.alias_domainDirtyFlag ;
    }   

    /**
     * 获取 [记录线索ID]
     */
    @JsonProperty("alias_force_thread_id")
    public Integer getAlias_force_thread_id(){
        return this.alias_force_thread_id ;
    }

    /**
     * 设置 [记录线索ID]
     */
    @JsonProperty("alias_force_thread_id")
    public void setAlias_force_thread_id(Integer  alias_force_thread_id){
        this.alias_force_thread_id = alias_force_thread_id ;
        this.alias_force_thread_idDirtyFlag = true ;
    }

     /**
     * 获取 [记录线索ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_force_thread_idDirtyFlag(){
        return this.alias_force_thread_idDirtyFlag ;
    }   

    /**
     * 获取 [模型别名]
     */
    @JsonProperty("alias_model_id")
    public Integer getAlias_model_id(){
        return this.alias_model_id ;
    }

    /**
     * 设置 [模型别名]
     */
    @JsonProperty("alias_model_id")
    public void setAlias_model_id(Integer  alias_model_id){
        this.alias_model_id = alias_model_id ;
        this.alias_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [模型别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_model_idDirtyFlag(){
        return this.alias_model_idDirtyFlag ;
    }   

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return this.alias_name ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

     /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return this.alias_nameDirtyFlag ;
    }   

    /**
     * 获取 [上级模型]
     */
    @JsonProperty("alias_parent_model_id")
    public Integer getAlias_parent_model_id(){
        return this.alias_parent_model_id ;
    }

    /**
     * 设置 [上级模型]
     */
    @JsonProperty("alias_parent_model_id")
    public void setAlias_parent_model_id(Integer  alias_parent_model_id){
        this.alias_parent_model_id = alias_parent_model_id ;
        this.alias_parent_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级模型]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_model_idDirtyFlag(){
        return this.alias_parent_model_idDirtyFlag ;
    }   

    /**
     * 获取 [上级记录ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public Integer getAlias_parent_thread_id(){
        return this.alias_parent_thread_id ;
    }

    /**
     * 设置 [上级记录ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public void setAlias_parent_thread_id(Integer  alias_parent_thread_id){
        this.alias_parent_thread_id = alias_parent_thread_id ;
        this.alias_parent_thread_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级记录ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_thread_idDirtyFlag(){
        return this.alias_parent_thread_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("alias_user_id")
    public Integer getAlias_user_id(){
        return this.alias_user_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("alias_user_id")
    public void setAlias_user_id(Integer  alias_user_id){
        this.alias_user_id = alias_user_id ;
        this.alias_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getAlias_user_idDirtyFlag(){
        return this.alias_user_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("alias_user_id_text")
    public String getAlias_user_id_text(){
        return this.alias_user_id_text ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("alias_user_id_text")
    public void setAlias_user_id_text(String  alias_user_id_text){
        this.alias_user_id_text = alias_user_id_text ;
        this.alias_user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getAlias_user_id_textDirtyFlag(){
        return this.alias_user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("alias_contact") instanceof Boolean)&& map.get("alias_contact")!=null){
			this.setAlias_contact((String)map.get("alias_contact"));
		}
		if(!(map.get("alias_defaults") instanceof Boolean)&& map.get("alias_defaults")!=null){
			this.setAlias_defaults((String)map.get("alias_defaults"));
		}
		if(!(map.get("alias_domain") instanceof Boolean)&& map.get("alias_domain")!=null){
			this.setAlias_domain((String)map.get("alias_domain"));
		}
		if(!(map.get("alias_force_thread_id") instanceof Boolean)&& map.get("alias_force_thread_id")!=null){
			this.setAlias_force_thread_id((Integer)map.get("alias_force_thread_id"));
		}
		if(!(map.get("alias_model_id") instanceof Boolean)&& map.get("alias_model_id")!=null){
			Object[] objs = (Object[])map.get("alias_model_id");
			if(objs.length > 0){
				this.setAlias_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("alias_name") instanceof Boolean)&& map.get("alias_name")!=null){
			this.setAlias_name((String)map.get("alias_name"));
		}
		if(!(map.get("alias_parent_model_id") instanceof Boolean)&& map.get("alias_parent_model_id")!=null){
			Object[] objs = (Object[])map.get("alias_parent_model_id");
			if(objs.length > 0){
				this.setAlias_parent_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("alias_parent_thread_id") instanceof Boolean)&& map.get("alias_parent_thread_id")!=null){
			this.setAlias_parent_thread_id((Integer)map.get("alias_parent_thread_id"));
		}
		if(!(map.get("alias_user_id") instanceof Boolean)&& map.get("alias_user_id")!=null){
			Object[] objs = (Object[])map.get("alias_user_id");
			if(objs.length > 0){
				this.setAlias_user_id((Integer)objs[0]);
			}
		}
		if(!(map.get("alias_user_id") instanceof Boolean)&& map.get("alias_user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("alias_user_id");
			if(objs.length > 1){
				this.setAlias_user_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAlias_contact()!=null&&this.getAlias_contactDirtyFlag()){
			map.put("alias_contact",this.getAlias_contact());
		}else if(this.getAlias_contactDirtyFlag()){
			map.put("alias_contact",false);
		}
		if(this.getAlias_defaults()!=null&&this.getAlias_defaultsDirtyFlag()){
			map.put("alias_defaults",this.getAlias_defaults());
		}else if(this.getAlias_defaultsDirtyFlag()){
			map.put("alias_defaults",false);
		}
		if(this.getAlias_domain()!=null&&this.getAlias_domainDirtyFlag()){
			map.put("alias_domain",this.getAlias_domain());
		}else if(this.getAlias_domainDirtyFlag()){
			map.put("alias_domain",false);
		}
		if(this.getAlias_force_thread_id()!=null&&this.getAlias_force_thread_idDirtyFlag()){
			map.put("alias_force_thread_id",this.getAlias_force_thread_id());
		}else if(this.getAlias_force_thread_idDirtyFlag()){
			map.put("alias_force_thread_id",false);
		}
		if(this.getAlias_model_id()!=null&&this.getAlias_model_idDirtyFlag()){
			map.put("alias_model_id",this.getAlias_model_id());
		}else if(this.getAlias_model_idDirtyFlag()){
			map.put("alias_model_id",false);
		}
		if(this.getAlias_name()!=null&&this.getAlias_nameDirtyFlag()){
			map.put("alias_name",this.getAlias_name());
		}else if(this.getAlias_nameDirtyFlag()){
			map.put("alias_name",false);
		}
		if(this.getAlias_parent_model_id()!=null&&this.getAlias_parent_model_idDirtyFlag()){
			map.put("alias_parent_model_id",this.getAlias_parent_model_id());
		}else if(this.getAlias_parent_model_idDirtyFlag()){
			map.put("alias_parent_model_id",false);
		}
		if(this.getAlias_parent_thread_id()!=null&&this.getAlias_parent_thread_idDirtyFlag()){
			map.put("alias_parent_thread_id",this.getAlias_parent_thread_id());
		}else if(this.getAlias_parent_thread_idDirtyFlag()){
			map.put("alias_parent_thread_id",false);
		}
		if(this.getAlias_user_id()!=null&&this.getAlias_user_idDirtyFlag()){
			map.put("alias_user_id",this.getAlias_user_id());
		}else if(this.getAlias_user_idDirtyFlag()){
			map.put("alias_user_id",false);
		}
		if(this.getAlias_user_id_text()!=null&&this.getAlias_user_id_textDirtyFlag()){
			//忽略文本外键alias_user_id_text
		}else if(this.getAlias_user_id_textDirtyFlag()){
			map.put("alias_user_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
