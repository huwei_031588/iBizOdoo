package cn.ibizlab.odoo.core.odoo_survey.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [调查用户输入] 对象
 */
@Data
public class Survey_user_input extends EntityClient implements Serializable {

    /**
     * 答案
     */
    @JSONField(name = "user_input_line_ids")
    @JsonProperty("user_input_line_ids")
    private String userInputLineIds;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 测试成绩
     */
    @JSONField(name = "quizz_score")
    @JsonProperty("quizz_score")
    private Double quizzScore;

    /**
     * 标识令牌
     */
    @JSONField(name = "token")
    @JsonProperty("token")
    private String token;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * EMail
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 测试问卷
     */
    @DEField(name = "test_entry")
    @JSONField(name = "test_entry")
    @JsonProperty("test_entry")
    private String testEntry;

    /**
     * 截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "deadline" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("deadline")
    private Timestamp deadline;

    /**
     * 回复类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 创建日期
     */
    @DEField(name = "date_create")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_create" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_create")
    private Timestamp dateCreate;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 空白调查的公开链接
     */
    @JSONField(name = "print_url")
    @JsonProperty("print_url")
    private String printUrl;

    /**
     * 调查结果的公开链接
     */
    @JSONField(name = "result_url")
    @JsonProperty("result_url")
    private String resultUrl;

    /**
     * 业务伙伴
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 问卷
     */
    @DEField(name = "survey_id")
    @JSONField(name = "survey_id")
    @JsonProperty("survey_id")
    private Integer surveyId;

    /**
     * 最后显示页面
     */
    @DEField(name = "last_displayed_page_id")
    @JSONField(name = "last_displayed_page_id")
    @JsonProperty("last_displayed_page_id")
    private Integer lastDisplayedPageId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoolastdisplayedpage")
    @JsonProperty("odoolastdisplayedpage")
    private cn.ibizlab.odoo.core.odoo_survey.domain.Survey_page odooLastDisplayedPage;

    /**
     * 
     */
    @JSONField(name = "odoosurvey")
    @JsonProperty("odoosurvey")
    private cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey odooSurvey;




    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [标识令牌]
     */
    public void setToken(String token){
        this.token = token ;
        this.modify("token",token);
    }
    /**
     * 设置 [EMail]
     */
    public void setEmail(String email){
        this.email = email ;
        this.modify("email",email);
    }
    /**
     * 设置 [测试问卷]
     */
    public void setTestEntry(String testEntry){
        this.testEntry = testEntry ;
        this.modify("test_entry",testEntry);
    }
    /**
     * 设置 [截止日期]
     */
    public void setDeadline(Timestamp deadline){
        this.deadline = deadline ;
        this.modify("deadline",deadline);
    }
    /**
     * 设置 [回复类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }
    /**
     * 设置 [创建日期]
     */
    public void setDateCreate(Timestamp dateCreate){
        this.dateCreate = dateCreate ;
        this.modify("date_create",dateCreate);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [问卷]
     */
    public void setSurveyId(Integer surveyId){
        this.surveyId = surveyId ;
        this.modify("survey_id",surveyId);
    }
    /**
     * 设置 [最后显示页面]
     */
    public void setLastDisplayedPageId(Integer lastDisplayedPageId){
        this.lastDisplayedPageId = lastDisplayedPageId ;
        this.modify("last_displayed_page_id",lastDisplayedPageId);
    }

}


