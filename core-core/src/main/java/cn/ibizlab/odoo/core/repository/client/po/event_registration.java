package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [event_registration] 对象
 */
public interface event_registration {

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_closed();

    public void setDate_closed(Timestamp date_closed);

    public Timestamp getDate_open();

    public void setDate_open(Timestamp date_open);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmail();

    public void setEmail(String email);

    public Timestamp getEvent_begin_date();

    public void setEvent_begin_date(Timestamp event_begin_date);

    public Timestamp getEvent_end_date();

    public void setEvent_end_date(Timestamp event_end_date);

    public Integer getEvent_id();

    public void setEvent_id(Integer event_id);

    public String getEvent_id_text();

    public void setEvent_id_text(String event_id_text);

    public Integer getEvent_ticket_id();

    public void setEvent_ticket_id(Integer event_ticket_id);

    public String getEvent_ticket_id_text();

    public void setEvent_ticket_id_text(String event_ticket_id_text);

    public Integer getId();

    public void setId(Integer id);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public String getOrigin();

    public void setOrigin(String origin);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public String getPhone();

    public void setPhone(String phone);

    public Integer getSale_order_id();

    public void setSale_order_id(Integer sale_order_id);

    public String getSale_order_id_text();

    public void setSale_order_id_text(String sale_order_id_text);

    public Integer getSale_order_line_id();

    public void setSale_order_line_id(Integer sale_order_line_id);

    public String getSale_order_line_id_text();

    public void setSale_order_line_id_text(String sale_order_line_id_text);

    public String getState();

    public void setState(String state);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
