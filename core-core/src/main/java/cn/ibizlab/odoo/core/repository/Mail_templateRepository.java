package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_template;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_templateSearchContext;

/**
 * 实体 [EMail模板] 存储对象
 */
public interface Mail_templateRepository extends Repository<Mail_template> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_template> searchDefault(Mail_templateSearchContext context);

    Mail_template convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template domain , Mail_template po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template convert2Domain( Mail_template po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template domain) ;

}
