package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.lunch_product_category;

/**
 * 实体[lunch_product_category] 服务对象接口
 */
public interface lunch_product_categoryRepository{


    public lunch_product_category createPO() ;
        public void create(lunch_product_category lunch_product_category);

        public void remove(String id);

        public void updateBatch(lunch_product_category lunch_product_category);

        public void update(lunch_product_category lunch_product_category);

        public void get(String id);

        public void createBatch(lunch_product_category lunch_product_category);

        public void removeBatch(String id);

        public List<lunch_product_category> search();


}
