package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_installer;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_installerSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_config_installerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_config_installerOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_config_installerClientModel;

/**
 * 实体[配置安装器] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_config_installerServiceImpl implements IRes_config_installerService {

    @Autowired
    res_config_installerOdooClient res_config_installerOdooClient;


    @Override
    public boolean remove(Integer id) {
        res_config_installerClientModel clientModel = new res_config_installerClientModel();
        clientModel.setId(id);
		res_config_installerOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Res_config_installer et) {
        res_config_installerClientModel clientModel = convert2Model(et,null);
		res_config_installerOdooClient.create(clientModel);
        Res_config_installer rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_config_installer> list){
    }

    @Override
    public boolean update(Res_config_installer et) {
        res_config_installerClientModel clientModel = convert2Model(et,null);
		res_config_installerOdooClient.update(clientModel);
        Res_config_installer rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_config_installer> list){
    }

    @Override
    public Res_config_installer get(Integer id) {
        res_config_installerClientModel clientModel = new res_config_installerClientModel();
        clientModel.setId(id);
		res_config_installerOdooClient.get(clientModel);
        Res_config_installer et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_config_installer();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_config_installer> searchDefault(Res_config_installerSearchContext context) {
        List<Res_config_installer> list = new ArrayList<Res_config_installer>();
        Page<res_config_installerClientModel> clientModelList = res_config_installerOdooClient.search(context);
        for(res_config_installerClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_config_installer>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_config_installerClientModel convert2Model(Res_config_installer domain , res_config_installerClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_config_installerClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_config_installer convert2Domain( res_config_installerClientModel model ,Res_config_installer domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_config_installer();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



