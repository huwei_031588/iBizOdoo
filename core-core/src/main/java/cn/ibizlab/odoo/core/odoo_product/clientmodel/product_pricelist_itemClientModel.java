package cn.ibizlab.odoo.core.odoo_product.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[product_pricelist_item] 对象
 */
public class product_pricelist_itemClientModel implements Serializable{

    /**
     * 应用于
     */
    public String applied_on;

    @JsonIgnore
    public boolean applied_onDirtyFlag;
    
    /**
     * 基于
     */
    public String base;

    @JsonIgnore
    public boolean baseDirtyFlag;
    
    /**
     * 其他价格表
     */
    public Integer base_pricelist_id;

    @JsonIgnore
    public boolean base_pricelist_idDirtyFlag;
    
    /**
     * 其他价格表
     */
    public String base_pricelist_id_text;

    @JsonIgnore
    public boolean base_pricelist_id_textDirtyFlag;
    
    /**
     * 产品种类
     */
    public Integer categ_id;

    @JsonIgnore
    public boolean categ_idDirtyFlag;
    
    /**
     * 产品种类
     */
    public String categ_id_text;

    @JsonIgnore
    public boolean categ_id_textDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 计算价格
     */
    public String compute_price;

    @JsonIgnore
    public boolean compute_priceDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 结束日期
     */
    public Timestamp date_end;

    @JsonIgnore
    public boolean date_endDirtyFlag;
    
    /**
     * 开始日期
     */
    public Timestamp date_start;

    @JsonIgnore
    public boolean date_startDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 固定价格
     */
    public Double fixed_price;

    @JsonIgnore
    public boolean fixed_priceDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 最小数量
     */
    public Integer min_quantity;

    @JsonIgnore
    public boolean min_quantityDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 百分比价格
     */
    public Double percent_price;

    @JsonIgnore
    public boolean percent_priceDirtyFlag;
    
    /**
     * 价格
     */
    public String price;

    @JsonIgnore
    public boolean priceDirtyFlag;
    
    /**
     * 价格表
     */
    public Integer pricelist_id;

    @JsonIgnore
    public boolean pricelist_idDirtyFlag;
    
    /**
     * 价格表
     */
    public String pricelist_id_text;

    @JsonIgnore
    public boolean pricelist_id_textDirtyFlag;
    
    /**
     * 价格折扣
     */
    public Double price_discount;

    @JsonIgnore
    public boolean price_discountDirtyFlag;
    
    /**
     * 最大价格毛利
     */
    public Double price_max_margin;

    @JsonIgnore
    public boolean price_max_marginDirtyFlag;
    
    /**
     * 最小价格毛利
     */
    public Double price_min_margin;

    @JsonIgnore
    public boolean price_min_marginDirtyFlag;
    
    /**
     * 价格舍入
     */
    public Double price_round;

    @JsonIgnore
    public boolean price_roundDirtyFlag;
    
    /**
     * 价格附加费用
     */
    public Double price_surcharge;

    @JsonIgnore
    public boolean price_surchargeDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 产品模板
     */
    public Integer product_tmpl_id;

    @JsonIgnore
    public boolean product_tmpl_idDirtyFlag;
    
    /**
     * 产品模板
     */
    public String product_tmpl_id_text;

    @JsonIgnore
    public boolean product_tmpl_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [应用于]
     */
    @JsonProperty("applied_on")
    public String getApplied_on(){
        return this.applied_on ;
    }

    /**
     * 设置 [应用于]
     */
    @JsonProperty("applied_on")
    public void setApplied_on(String  applied_on){
        this.applied_on = applied_on ;
        this.applied_onDirtyFlag = true ;
    }

     /**
     * 获取 [应用于]脏标记
     */
    @JsonIgnore
    public boolean getApplied_onDirtyFlag(){
        return this.applied_onDirtyFlag ;
    }   

    /**
     * 获取 [基于]
     */
    @JsonProperty("base")
    public String getBase(){
        return this.base ;
    }

    /**
     * 设置 [基于]
     */
    @JsonProperty("base")
    public void setBase(String  base){
        this.base = base ;
        this.baseDirtyFlag = true ;
    }

     /**
     * 获取 [基于]脏标记
     */
    @JsonIgnore
    public boolean getBaseDirtyFlag(){
        return this.baseDirtyFlag ;
    }   

    /**
     * 获取 [其他价格表]
     */
    @JsonProperty("base_pricelist_id")
    public Integer getBase_pricelist_id(){
        return this.base_pricelist_id ;
    }

    /**
     * 设置 [其他价格表]
     */
    @JsonProperty("base_pricelist_id")
    public void setBase_pricelist_id(Integer  base_pricelist_id){
        this.base_pricelist_id = base_pricelist_id ;
        this.base_pricelist_idDirtyFlag = true ;
    }

     /**
     * 获取 [其他价格表]脏标记
     */
    @JsonIgnore
    public boolean getBase_pricelist_idDirtyFlag(){
        return this.base_pricelist_idDirtyFlag ;
    }   

    /**
     * 获取 [其他价格表]
     */
    @JsonProperty("base_pricelist_id_text")
    public String getBase_pricelist_id_text(){
        return this.base_pricelist_id_text ;
    }

    /**
     * 设置 [其他价格表]
     */
    @JsonProperty("base_pricelist_id_text")
    public void setBase_pricelist_id_text(String  base_pricelist_id_text){
        this.base_pricelist_id_text = base_pricelist_id_text ;
        this.base_pricelist_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [其他价格表]脏标记
     */
    @JsonIgnore
    public boolean getBase_pricelist_id_textDirtyFlag(){
        return this.base_pricelist_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品种类]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return this.categ_id ;
    }

    /**
     * 设置 [产品种类]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品种类]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return this.categ_idDirtyFlag ;
    }   

    /**
     * 获取 [产品种类]
     */
    @JsonProperty("categ_id_text")
    public String getCateg_id_text(){
        return this.categ_id_text ;
    }

    /**
     * 设置 [产品种类]
     */
    @JsonProperty("categ_id_text")
    public void setCateg_id_text(String  categ_id_text){
        this.categ_id_text = categ_id_text ;
        this.categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品种类]脏标记
     */
    @JsonIgnore
    public boolean getCateg_id_textDirtyFlag(){
        return this.categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [计算价格]
     */
    @JsonProperty("compute_price")
    public String getCompute_price(){
        return this.compute_price ;
    }

    /**
     * 设置 [计算价格]
     */
    @JsonProperty("compute_price")
    public void setCompute_price(String  compute_price){
        this.compute_price = compute_price ;
        this.compute_priceDirtyFlag = true ;
    }

     /**
     * 获取 [计算价格]脏标记
     */
    @JsonIgnore
    public boolean getCompute_priceDirtyFlag(){
        return this.compute_priceDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("date_end")
    public Timestamp getDate_end(){
        return this.date_end ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("date_end")
    public void setDate_end(Timestamp  date_end){
        this.date_end = date_end ;
        this.date_endDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_endDirtyFlag(){
        return this.date_endDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return this.date_start ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return this.date_startDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [固定价格]
     */
    @JsonProperty("fixed_price")
    public Double getFixed_price(){
        return this.fixed_price ;
    }

    /**
     * 设置 [固定价格]
     */
    @JsonProperty("fixed_price")
    public void setFixed_price(Double  fixed_price){
        this.fixed_price = fixed_price ;
        this.fixed_priceDirtyFlag = true ;
    }

     /**
     * 获取 [固定价格]脏标记
     */
    @JsonIgnore
    public boolean getFixed_priceDirtyFlag(){
        return this.fixed_priceDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [最小数量]
     */
    @JsonProperty("min_quantity")
    public Integer getMin_quantity(){
        return this.min_quantity ;
    }

    /**
     * 设置 [最小数量]
     */
    @JsonProperty("min_quantity")
    public void setMin_quantity(Integer  min_quantity){
        this.min_quantity = min_quantity ;
        this.min_quantityDirtyFlag = true ;
    }

     /**
     * 获取 [最小数量]脏标记
     */
    @JsonIgnore
    public boolean getMin_quantityDirtyFlag(){
        return this.min_quantityDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [百分比价格]
     */
    @JsonProperty("percent_price")
    public Double getPercent_price(){
        return this.percent_price ;
    }

    /**
     * 设置 [百分比价格]
     */
    @JsonProperty("percent_price")
    public void setPercent_price(Double  percent_price){
        this.percent_price = percent_price ;
        this.percent_priceDirtyFlag = true ;
    }

     /**
     * 获取 [百分比价格]脏标记
     */
    @JsonIgnore
    public boolean getPercent_priceDirtyFlag(){
        return this.percent_priceDirtyFlag ;
    }   

    /**
     * 获取 [价格]
     */
    @JsonProperty("price")
    public String getPrice(){
        return this.price ;
    }

    /**
     * 设置 [价格]
     */
    @JsonProperty("price")
    public void setPrice(String  price){
        this.price = price ;
        this.priceDirtyFlag = true ;
    }

     /**
     * 获取 [价格]脏标记
     */
    @JsonIgnore
    public boolean getPriceDirtyFlag(){
        return this.priceDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return this.pricelist_id ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return this.pricelist_idDirtyFlag ;
    }   

    /**
     * 获取 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public String getPricelist_id_text(){
        return this.pricelist_id_text ;
    }

    /**
     * 设置 [价格表]
     */
    @JsonProperty("pricelist_id_text")
    public void setPricelist_id_text(String  pricelist_id_text){
        this.pricelist_id_text = pricelist_id_text ;
        this.pricelist_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [价格表]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_id_textDirtyFlag(){
        return this.pricelist_id_textDirtyFlag ;
    }   

    /**
     * 获取 [价格折扣]
     */
    @JsonProperty("price_discount")
    public Double getPrice_discount(){
        return this.price_discount ;
    }

    /**
     * 设置 [价格折扣]
     */
    @JsonProperty("price_discount")
    public void setPrice_discount(Double  price_discount){
        this.price_discount = price_discount ;
        this.price_discountDirtyFlag = true ;
    }

     /**
     * 获取 [价格折扣]脏标记
     */
    @JsonIgnore
    public boolean getPrice_discountDirtyFlag(){
        return this.price_discountDirtyFlag ;
    }   

    /**
     * 获取 [最大价格毛利]
     */
    @JsonProperty("price_max_margin")
    public Double getPrice_max_margin(){
        return this.price_max_margin ;
    }

    /**
     * 设置 [最大价格毛利]
     */
    @JsonProperty("price_max_margin")
    public void setPrice_max_margin(Double  price_max_margin){
        this.price_max_margin = price_max_margin ;
        this.price_max_marginDirtyFlag = true ;
    }

     /**
     * 获取 [最大价格毛利]脏标记
     */
    @JsonIgnore
    public boolean getPrice_max_marginDirtyFlag(){
        return this.price_max_marginDirtyFlag ;
    }   

    /**
     * 获取 [最小价格毛利]
     */
    @JsonProperty("price_min_margin")
    public Double getPrice_min_margin(){
        return this.price_min_margin ;
    }

    /**
     * 设置 [最小价格毛利]
     */
    @JsonProperty("price_min_margin")
    public void setPrice_min_margin(Double  price_min_margin){
        this.price_min_margin = price_min_margin ;
        this.price_min_marginDirtyFlag = true ;
    }

     /**
     * 获取 [最小价格毛利]脏标记
     */
    @JsonIgnore
    public boolean getPrice_min_marginDirtyFlag(){
        return this.price_min_marginDirtyFlag ;
    }   

    /**
     * 获取 [价格舍入]
     */
    @JsonProperty("price_round")
    public Double getPrice_round(){
        return this.price_round ;
    }

    /**
     * 设置 [价格舍入]
     */
    @JsonProperty("price_round")
    public void setPrice_round(Double  price_round){
        this.price_round = price_round ;
        this.price_roundDirtyFlag = true ;
    }

     /**
     * 获取 [价格舍入]脏标记
     */
    @JsonIgnore
    public boolean getPrice_roundDirtyFlag(){
        return this.price_roundDirtyFlag ;
    }   

    /**
     * 获取 [价格附加费用]
     */
    @JsonProperty("price_surcharge")
    public Double getPrice_surcharge(){
        return this.price_surcharge ;
    }

    /**
     * 设置 [价格附加费用]
     */
    @JsonProperty("price_surcharge")
    public void setPrice_surcharge(Double  price_surcharge){
        this.price_surcharge = price_surcharge ;
        this.price_surchargeDirtyFlag = true ;
    }

     /**
     * 获取 [价格附加费用]脏标记
     */
    @JsonIgnore
    public boolean getPrice_surchargeDirtyFlag(){
        return this.price_surchargeDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return this.product_tmpl_id ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return this.product_tmpl_idDirtyFlag ;
    }   

    /**
     * 获取 [产品模板]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return this.product_tmpl_id_text ;
    }

    /**
     * 设置 [产品模板]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return this.product_tmpl_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("applied_on") instanceof Boolean)&& map.get("applied_on")!=null){
			this.setApplied_on((String)map.get("applied_on"));
		}
		if(!(map.get("base") instanceof Boolean)&& map.get("base")!=null){
			this.setBase((String)map.get("base"));
		}
		if(!(map.get("base_pricelist_id") instanceof Boolean)&& map.get("base_pricelist_id")!=null){
			Object[] objs = (Object[])map.get("base_pricelist_id");
			if(objs.length > 0){
				this.setBase_pricelist_id((Integer)objs[0]);
			}
		}
		if(!(map.get("base_pricelist_id") instanceof Boolean)&& map.get("base_pricelist_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("base_pricelist_id");
			if(objs.length > 1){
				this.setBase_pricelist_id_text((String)objs[1]);
			}
		}
		if(!(map.get("categ_id") instanceof Boolean)&& map.get("categ_id")!=null){
			Object[] objs = (Object[])map.get("categ_id");
			if(objs.length > 0){
				this.setCateg_id((Integer)objs[0]);
			}
		}
		if(!(map.get("categ_id") instanceof Boolean)&& map.get("categ_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("categ_id");
			if(objs.length > 1){
				this.setCateg_id_text((String)objs[1]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("compute_price") instanceof Boolean)&& map.get("compute_price")!=null){
			this.setCompute_price((String)map.get("compute_price"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 1){
				this.setCurrency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("date_end") instanceof Boolean)&& map.get("date_end")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date_end"));
   			this.setDate_end(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_start") instanceof Boolean)&& map.get("date_start")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date_start"));
   			this.setDate_start(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("fixed_price") instanceof Boolean)&& map.get("fixed_price")!=null){
			this.setFixed_price((Double)map.get("fixed_price"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("min_quantity") instanceof Boolean)&& map.get("min_quantity")!=null){
			this.setMin_quantity((Integer)map.get("min_quantity"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("percent_price") instanceof Boolean)&& map.get("percent_price")!=null){
			this.setPercent_price((Double)map.get("percent_price"));
		}
		if(!(map.get("price") instanceof Boolean)&& map.get("price")!=null){
			this.setPrice((String)map.get("price"));
		}
		if(!(map.get("pricelist_id") instanceof Boolean)&& map.get("pricelist_id")!=null){
			Object[] objs = (Object[])map.get("pricelist_id");
			if(objs.length > 0){
				this.setPricelist_id((Integer)objs[0]);
			}
		}
		if(!(map.get("pricelist_id") instanceof Boolean)&& map.get("pricelist_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("pricelist_id");
			if(objs.length > 1){
				this.setPricelist_id_text((String)objs[1]);
			}
		}
		if(!(map.get("price_discount") instanceof Boolean)&& map.get("price_discount")!=null){
			this.setPrice_discount((Double)map.get("price_discount"));
		}
		if(!(map.get("price_max_margin") instanceof Boolean)&& map.get("price_max_margin")!=null){
			this.setPrice_max_margin((Double)map.get("price_max_margin"));
		}
		if(!(map.get("price_min_margin") instanceof Boolean)&& map.get("price_min_margin")!=null){
			this.setPrice_min_margin((Double)map.get("price_min_margin"));
		}
		if(!(map.get("price_round") instanceof Boolean)&& map.get("price_round")!=null){
			this.setPrice_round((Double)map.get("price_round"));
		}
		if(!(map.get("price_surcharge") instanceof Boolean)&& map.get("price_surcharge")!=null){
			this.setPrice_surcharge((Double)map.get("price_surcharge"));
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_tmpl_id") instanceof Boolean)&& map.get("product_tmpl_id")!=null){
			Object[] objs = (Object[])map.get("product_tmpl_id");
			if(objs.length > 0){
				this.setProduct_tmpl_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_tmpl_id") instanceof Boolean)&& map.get("product_tmpl_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_tmpl_id");
			if(objs.length > 1){
				this.setProduct_tmpl_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getApplied_on()!=null&&this.getApplied_onDirtyFlag()){
			map.put("applied_on",this.getApplied_on());
		}else if(this.getApplied_onDirtyFlag()){
			map.put("applied_on",false);
		}
		if(this.getBase()!=null&&this.getBaseDirtyFlag()){
			map.put("base",this.getBase());
		}else if(this.getBaseDirtyFlag()){
			map.put("base",false);
		}
		if(this.getBase_pricelist_id()!=null&&this.getBase_pricelist_idDirtyFlag()){
			map.put("base_pricelist_id",this.getBase_pricelist_id());
		}else if(this.getBase_pricelist_idDirtyFlag()){
			map.put("base_pricelist_id",false);
		}
		if(this.getBase_pricelist_id_text()!=null&&this.getBase_pricelist_id_textDirtyFlag()){
			//忽略文本外键base_pricelist_id_text
		}else if(this.getBase_pricelist_id_textDirtyFlag()){
			map.put("base_pricelist_id",false);
		}
		if(this.getCateg_id()!=null&&this.getCateg_idDirtyFlag()){
			map.put("categ_id",this.getCateg_id());
		}else if(this.getCateg_idDirtyFlag()){
			map.put("categ_id",false);
		}
		if(this.getCateg_id_text()!=null&&this.getCateg_id_textDirtyFlag()){
			//忽略文本外键categ_id_text
		}else if(this.getCateg_id_textDirtyFlag()){
			map.put("categ_id",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompute_price()!=null&&this.getCompute_priceDirtyFlag()){
			map.put("compute_price",this.getCompute_price());
		}else if(this.getCompute_priceDirtyFlag()){
			map.put("compute_price",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_id_text()!=null&&this.getCurrency_id_textDirtyFlag()){
			//忽略文本外键currency_id_text
		}else if(this.getCurrency_id_textDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDate_end()!=null&&this.getDate_endDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate_end());
			map.put("date_end",datetimeStr);
		}else if(this.getDate_endDirtyFlag()){
			map.put("date_end",false);
		}
		if(this.getDate_start()!=null&&this.getDate_startDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate_start());
			map.put("date_start",datetimeStr);
		}else if(this.getDate_startDirtyFlag()){
			map.put("date_start",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFixed_price()!=null&&this.getFixed_priceDirtyFlag()){
			map.put("fixed_price",this.getFixed_price());
		}else if(this.getFixed_priceDirtyFlag()){
			map.put("fixed_price",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getMin_quantity()!=null&&this.getMin_quantityDirtyFlag()){
			map.put("min_quantity",this.getMin_quantity());
		}else if(this.getMin_quantityDirtyFlag()){
			map.put("min_quantity",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPercent_price()!=null&&this.getPercent_priceDirtyFlag()){
			map.put("percent_price",this.getPercent_price());
		}else if(this.getPercent_priceDirtyFlag()){
			map.put("percent_price",false);
		}
		if(this.getPrice()!=null&&this.getPriceDirtyFlag()){
			map.put("price",this.getPrice());
		}else if(this.getPriceDirtyFlag()){
			map.put("price",false);
		}
		if(this.getPricelist_id()!=null&&this.getPricelist_idDirtyFlag()){
			map.put("pricelist_id",this.getPricelist_id());
		}else if(this.getPricelist_idDirtyFlag()){
			map.put("pricelist_id",false);
		}
		if(this.getPricelist_id_text()!=null&&this.getPricelist_id_textDirtyFlag()){
			//忽略文本外键pricelist_id_text
		}else if(this.getPricelist_id_textDirtyFlag()){
			map.put("pricelist_id",false);
		}
		if(this.getPrice_discount()!=null&&this.getPrice_discountDirtyFlag()){
			map.put("price_discount",this.getPrice_discount());
		}else if(this.getPrice_discountDirtyFlag()){
			map.put("price_discount",false);
		}
		if(this.getPrice_max_margin()!=null&&this.getPrice_max_marginDirtyFlag()){
			map.put("price_max_margin",this.getPrice_max_margin());
		}else if(this.getPrice_max_marginDirtyFlag()){
			map.put("price_max_margin",false);
		}
		if(this.getPrice_min_margin()!=null&&this.getPrice_min_marginDirtyFlag()){
			map.put("price_min_margin",this.getPrice_min_margin());
		}else if(this.getPrice_min_marginDirtyFlag()){
			map.put("price_min_margin",false);
		}
		if(this.getPrice_round()!=null&&this.getPrice_roundDirtyFlag()){
			map.put("price_round",this.getPrice_round());
		}else if(this.getPrice_roundDirtyFlag()){
			map.put("price_round",false);
		}
		if(this.getPrice_surcharge()!=null&&this.getPrice_surchargeDirtyFlag()){
			map.put("price_surcharge",this.getPrice_surcharge());
		}else if(this.getPrice_surchargeDirtyFlag()){
			map.put("price_surcharge",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_tmpl_id()!=null&&this.getProduct_tmpl_idDirtyFlag()){
			map.put("product_tmpl_id",this.getProduct_tmpl_id());
		}else if(this.getProduct_tmpl_idDirtyFlag()){
			map.put("product_tmpl_id",false);
		}
		if(this.getProduct_tmpl_id_text()!=null&&this.getProduct_tmpl_id_textDirtyFlag()){
			//忽略文本外键product_tmpl_id_text
		}else if(this.getProduct_tmpl_id_textDirtyFlag()){
			map.put("product_tmpl_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
