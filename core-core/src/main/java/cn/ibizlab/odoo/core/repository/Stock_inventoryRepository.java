package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_inventory;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventorySearchContext;

/**
 * 实体 [库存] 存储对象
 */
public interface Stock_inventoryRepository extends Repository<Stock_inventory> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_inventory> searchDefault(Stock_inventorySearchContext context);

    Stock_inventory convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory domain , Stock_inventory po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory convert2Domain( Stock_inventory po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory domain) ;

}
