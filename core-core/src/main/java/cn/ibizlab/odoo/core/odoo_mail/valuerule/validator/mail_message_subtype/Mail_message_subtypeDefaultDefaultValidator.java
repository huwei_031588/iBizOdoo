package cn.ibizlab.odoo.core.odoo_mail.valuerule.validator.mail_message_subtype;

import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.odoo.util.helper.SpringContextHolder;
import cn.ibizlab.odoo.util.valuerule.SysValueRule;
import cn.ibizlab.odoo.util.valuerule.StringLengthValueRule;
import cn.ibizlab.odoo.util.SearchFieldFilter;
import cn.ibizlab.odoo.util.enums.SearchFieldType;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_message_subtype.Mail_message_subtypeDefaultDefault;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

/**
 * 实体值规则注解解析类
 * 实体：Mail_message_subtype
 * 属性：Default
 * 值规则：Default
 * 值规则信息：默认规则
 */
@Slf4j
@Component("Mail_message_subtypeDefaultDefaultValidator")
public class Mail_message_subtypeDefaultDefaultValidator implements ConstraintValidator<Mail_message_subtypeDefaultDefault, Integer>,Validator {
    private static final String MESSAGE = "默认规则";

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        boolean isValid = doValidate(value);
        if(!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(MESSAGE)
                    .addConstraintViolation();
        }
        return doValidate(value);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public void validate(Object o, Errors errors) {
        if(supports(o.getClass())){
            if (!doValidate((Integer) o)){
                errors.reject(MESSAGE);
            }
        }
    }

    public boolean doValidate(Integer value) {
        boolean isValid = true;

        return isValid;
    }
}

