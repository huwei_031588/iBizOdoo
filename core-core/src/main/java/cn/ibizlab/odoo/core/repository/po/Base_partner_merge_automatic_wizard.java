package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;

/**
 * 实体 [合并业务伙伴向导] 存储模型
 */
public interface Base_partner_merge_automatic_wizard{

    /**
     * 联系人组的最多联系人数量
     */
    Integer getMaximum_group();

    void setMaximum_group(Integer maximum_group);

    /**
     * 获取 [联系人组的最多联系人数量]脏标记
     */
    boolean getMaximum_groupDirtyFlag();

    /**
     * 上级公司
     */
    String getGroup_by_parent_id();

    void setGroup_by_parent_id(String group_by_parent_id);

    /**
     * 获取 [上级公司]脏标记
     */
    boolean getGroup_by_parent_idDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 联系人组
     */
    Integer getNumber_group();

    void setNumber_group(Integer number_group);

    /**
     * 获取 [联系人组]脏标记
     */
    boolean getNumber_groupDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 明细行
     */
    String getLine_ids();

    void setLine_ids(String line_ids);

    /**
     * 获取 [明细行]脏标记
     */
    boolean getLine_idsDirtyFlag();

    /**
     * 增值税
     */
    String getGroup_by_vat();

    void setGroup_by_vat(String group_by_vat);

    /**
     * 获取 [增值税]脏标记
     */
    boolean getGroup_by_vatDirtyFlag();

    /**
     * 与系统用户相关的联系人
     */
    String getExclude_contact();

    void setExclude_contact(String exclude_contact);

    /**
     * 获取 [与系统用户相关的联系人]脏标记
     */
    boolean getExclude_contactDirtyFlag();

    /**
     * 联系人
     */
    String getPartner_ids();

    void setPartner_ids(String partner_ids);

    /**
     * 获取 [联系人]脏标记
     */
    boolean getPartner_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * EMail
     */
    String getGroup_by_email();

    void setGroup_by_email(String group_by_email);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getGroup_by_emailDirtyFlag();

    /**
     * 是公司
     */
    String getGroup_by_is_company();

    void setGroup_by_is_company(String group_by_is_company);

    /**
     * 获取 [是公司]脏标记
     */
    boolean getGroup_by_is_companyDirtyFlag();

    /**
     * 省/ 州
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 与日记账相关的联系人
     */
    String getExclude_journal_item();

    void setExclude_journal_item(String exclude_journal_item);

    /**
     * 获取 [与日记账相关的联系人]脏标记
     */
    boolean getExclude_journal_itemDirtyFlag();

    /**
     * 名称
     */
    String getGroup_by_name();

    void setGroup_by_name(String group_by_name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getGroup_by_nameDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 目的地之联系人
     */
    String getDst_partner_id_text();

    void setDst_partner_id_text(String dst_partner_id_text);

    /**
     * 获取 [目的地之联系人]脏标记
     */
    boolean getDst_partner_id_textDirtyFlag();

    /**
     * 目的地之联系人
     */
    Integer getDst_partner_id();

    void setDst_partner_id(Integer dst_partner_id);

    /**
     * 获取 [目的地之联系人]脏标记
     */
    boolean getDst_partner_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 当前行
     */
    Integer getCurrent_line_id();

    void setCurrent_line_id(Integer current_line_id);

    /**
     * 获取 [当前行]脏标记
     */
    boolean getCurrent_line_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
