package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statementSearchContext;

/**
 * 实体 [银行对账单] 存储模型
 */
public interface Account_bank_statement{

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 分录明细行
     */
    String getMove_line_ids();

    void setMove_line_ids(String move_line_ids);

    /**
     * 获取 [分录明细行]脏标记
     */
    boolean getMove_line_idsDirtyFlag();

    /**
     * 参考
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [参考]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 结束余额
     */
    Double getBalance_end_real();

    void setBalance_end_real(Double balance_end_real);

    /**
     * 获取 [结束余额]脏标记
     */
    boolean getBalance_end_realDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 会话
     */
    Integer getPos_session_id();

    void setPos_session_id(Integer pos_session_id);

    /**
     * 获取 [会话]脏标记
     */
    boolean getPos_session_idDirtyFlag();

    /**
     * 计算余额
     */
    Double getBalance_end();

    void setBalance_end(Double balance_end);

    /**
     * 获取 [计算余额]脏标记
     */
    boolean getBalance_endDirtyFlag();

    /**
     * 交易小计
     */
    Double getTotal_entry_encoding();

    void setTotal_entry_encoding(Double total_entry_encoding);

    /**
     * 获取 [交易小计]脏标记
     */
    boolean getTotal_entry_encodingDirtyFlag();

    /**
     * 关闭在
     */
    Timestamp getDate_done();

    void setDate_done(Timestamp date_done);

    /**
     * 获取 [关闭在]脏标记
     */
    boolean getDate_doneDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 所有的明细行都已核销
     */
    String getAll_lines_reconciled();

    void setAll_lines_reconciled(String all_lines_reconciled);

    /**
     * 获取 [所有的明细行都已核销]脏标记
     */
    boolean getAll_lines_reconciledDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 操作编号
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [操作编号]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 对账单明细行
     */
    String getLine_ids();

    void setLine_ids(String line_ids);

    /**
     * 获取 [对账单明细行]脏标记
     */
    boolean getLine_idsDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 凭证明细数
     */
    Integer getMove_line_count();

    void setMove_line_count(Integer move_line_count);

    /**
     * 获取 [凭证明细数]脏标记
     */
    boolean getMove_line_countDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 是0
     */
    String getIs_difference_zero();

    void setIs_difference_zero(String is_difference_zero);

    /**
     * 获取 [是0]脏标记
     */
    boolean getIs_difference_zeroDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 需要一个动作消息的编码
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 差异
     */
    Double getDifference();

    void setDifference(Double difference);

    /**
     * 获取 [差异]脏标记
     */
    boolean getDifferenceDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 外部引用
     */
    String getReference();

    void setReference(String reference);

    /**
     * 获取 [外部引用]脏标记
     */
    boolean getReferenceDirtyFlag();

    /**
     * 起始余额
     */
    Double getBalance_start();

    void setBalance_start(Double balance_start);

    /**
     * 获取 [起始余额]脏标记
     */
    boolean getBalance_startDirtyFlag();

    /**
     * 会计日期
     */
    Timestamp getAccounting_date();

    void setAccounting_date(Timestamp accounting_date);

    /**
     * 获取 [会计日期]脏标记
     */
    boolean getAccounting_dateDirtyFlag();

    /**
     * 负责人
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 日记账
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 类型
     */
    String getJournal_type();

    void setJournal_type(String journal_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getJournal_typeDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 默认借方科目
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [默认借方科目]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 开始钱箱
     */
    Integer getCashbox_start_id();

    void setCashbox_start_id(Integer cashbox_start_id);

    /**
     * 获取 [开始钱箱]脏标记
     */
    boolean getCashbox_start_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 日记账
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 关闭钱箱
     */
    Integer getCashbox_end_id();

    void setCashbox_end_id(Integer cashbox_end_id);

    /**
     * 获取 [关闭钱箱]脏标记
     */
    boolean getCashbox_end_idDirtyFlag();

    /**
     * 负责人
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
