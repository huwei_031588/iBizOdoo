package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_invoice;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice] 服务对象接口
 */
public interface Iaccount_invoiceClientService{

    public Iaccount_invoice createModel() ;

    public void update(Iaccount_invoice account_invoice);

    public void removeBatch(List<Iaccount_invoice> account_invoices);

    public void createBatch(List<Iaccount_invoice> account_invoices);

    public Page<Iaccount_invoice> search(SearchContext context);

    public void create(Iaccount_invoice account_invoice);

    public void updateBatch(List<Iaccount_invoice> account_invoices);

    public void remove(Iaccount_invoice account_invoice);

    public void get(Iaccount_invoice account_invoice);

    public Page<Iaccount_invoice> select(SearchContext context);

}
