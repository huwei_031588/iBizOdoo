package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_message_subtypeSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_message_subtypeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_message_subtypeOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_message_subtypeClientModel;

/**
 * 实体[消息子类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_message_subtypeServiceImpl implements IMail_message_subtypeService {

    @Autowired
    mail_message_subtypeOdooClient mail_message_subtypeOdooClient;


    @Override
    public Mail_message_subtype get(Integer id) {
        mail_message_subtypeClientModel clientModel = new mail_message_subtypeClientModel();
        clientModel.setId(id);
		mail_message_subtypeOdooClient.get(clientModel);
        Mail_message_subtype et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_message_subtype();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mail_message_subtypeClientModel clientModel = new mail_message_subtypeClientModel();
        clientModel.setId(id);
		mail_message_subtypeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mail_message_subtype et) {
        mail_message_subtypeClientModel clientModel = convert2Model(et,null);
		mail_message_subtypeOdooClient.create(clientModel);
        Mail_message_subtype rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_message_subtype> list){
    }

    @Override
    public boolean update(Mail_message_subtype et) {
        mail_message_subtypeClientModel clientModel = convert2Model(et,null);
		mail_message_subtypeOdooClient.update(clientModel);
        Mail_message_subtype rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_message_subtype> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_message_subtype> searchDefault(Mail_message_subtypeSearchContext context) {
        List<Mail_message_subtype> list = new ArrayList<Mail_message_subtype>();
        Page<mail_message_subtypeClientModel> clientModelList = mail_message_subtypeOdooClient.search(context);
        for(mail_message_subtypeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_message_subtype>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_message_subtypeClientModel convert2Model(Mail_message_subtype domain , mail_message_subtypeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_message_subtypeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("relation_fielddirtyflag"))
                model.setRelation_field(domain.getRelationField());
            if((Boolean) domain.getExtensionparams().get("internaldirtyflag"))
                model.setInternal(domain.getInternal());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("ibizdefaultdirtyflag"))
                model.setIbizdefault(domain.getIbizdefault());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("hiddendirtyflag"))
                model.setHidden(domain.getHidden());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("res_modeldirtyflag"))
                model.setRes_model(domain.getResModel());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_message_subtype convert2Domain( mail_message_subtypeClientModel model ,Mail_message_subtype domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_message_subtype();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getRelation_fieldDirtyFlag())
            domain.setRelationField(model.getRelation_field());
        if(model.getInternalDirtyFlag())
            domain.setInternal(model.getInternal());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIbizdefaultDirtyFlag())
            domain.setIbizdefault(model.getIbizdefault());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getHiddenDirtyFlag())
            domain.setHidden(model.getHidden());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getRes_modelDirtyFlag())
            domain.setResModel(model.getRes_model());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        return domain ;
    }

}

    



