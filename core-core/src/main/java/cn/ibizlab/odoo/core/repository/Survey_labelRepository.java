package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Survey_label;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_labelSearchContext;

/**
 * 实体 [调查标签] 存储对象
 */
public interface Survey_labelRepository extends Repository<Survey_label> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Survey_label> searchDefault(Survey_labelSearchContext context);

    Survey_label convert2PO(cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label domain , Survey_label po) ;

    cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label convert2Domain( Survey_label po ,cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label domain) ;

}
