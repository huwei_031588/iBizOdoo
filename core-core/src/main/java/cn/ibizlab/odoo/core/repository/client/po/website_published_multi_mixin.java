package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [website_published_multi_mixin] 对象
 */
public interface website_published_multi_mixin {

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_published();

    public void setIs_published(String is_published);

    public String getWebsite_published();

    public void setWebsite_published(String website_published);

    public String getWebsite_url();

    public void setWebsite_url(String website_url);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
