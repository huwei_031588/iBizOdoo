package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_resend_partner;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_partnerSearchContext;

/**
 * 实体 [业务伙伴需要额外信息重发EMail] 存储对象
 */
public interface Mail_resend_partnerRepository extends Repository<Mail_resend_partner> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_resend_partner> searchDefault(Mail_resend_partnerSearchContext context);

    Mail_resend_partner convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_partner domain , Mail_resend_partner po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_partner convert2Domain( Mail_resend_partner po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_partner domain) ;

}
