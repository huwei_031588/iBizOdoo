package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_workcenter;

/**
 * 实体[mrp_workcenter] 服务对象接口
 */
public interface mrp_workcenterRepository{


    public mrp_workcenter createPO() ;
        public void updateBatch(mrp_workcenter mrp_workcenter);

        public void createBatch(mrp_workcenter mrp_workcenter);

        public List<mrp_workcenter> search();

        public void create(mrp_workcenter mrp_workcenter);

        public void removeBatch(String id);

        public void get(String id);

        public void remove(String id);

        public void update(mrp_workcenter mrp_workcenter);


}
