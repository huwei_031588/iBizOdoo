package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Payment_token;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_tokenSearchContext;

/**
 * 实体 [付款令牌] 存储对象
 */
public interface Payment_tokenRepository extends Repository<Payment_token> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Payment_token> searchDefault(Payment_tokenSearchContext context);

    Payment_token convert2PO(cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token domain , Payment_token po) ;

    cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token convert2Domain( Payment_token po ,cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token domain) ;

}
