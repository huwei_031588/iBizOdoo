package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.base_automation;

/**
 * 实体[base_automation] 服务对象接口
 */
public interface base_automationRepository{


    public base_automation createPO() ;
        public void removeBatch(String id);

        public void updateBatch(base_automation base_automation);

        public void createBatch(base_automation base_automation);

        public List<base_automation> search();

        public void create(base_automation base_automation);

        public void get(String id);

        public void remove(String id);

        public void update(base_automation base_automation);


}
