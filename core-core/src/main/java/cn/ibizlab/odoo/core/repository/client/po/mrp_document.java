package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mrp_document] 对象
 */
public interface mrp_document {

    public String getAccess_token();

    public void setAccess_token(String access_token);

    public String getActive();

    public void setActive(String active);

    public String getChecksum();

    public void setChecksum(String checksum);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public byte[] getDatas();

    public void setDatas(byte[] datas);

    public String getDatas_fname();

    public void setDatas_fname(String datas_fname);

    public byte[] getDb_datas();

    public void setDb_datas(byte[] db_datas);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getFile_size();

    public void setFile_size(Integer file_size);

    public String getIbizpublic();

    public void setIbizpublic(String ibizpublic);

    public Integer getId();

    public void setId(Integer id);

    public String getIndex_content();

    public void setIndex_content(String index_content);

    public String getKey();

    public void setKey(String key);

    public String getLocal_url();

    public void setLocal_url(String local_url);

    public String getMimetype();

    public void setMimetype(String mimetype);

    public String getName();

    public void setName(String name);

    public String getPriority();

    public void setPriority(String priority);

    public String getRes_field();

    public void setRes_field(String res_field);

    public Integer getRes_id();

    public void setRes_id(Integer res_id);

    public String getRes_model();

    public void setRes_model(String res_model);

    public String getRes_model_name();

    public void setRes_model_name(String res_model_name);

    public String getRes_name();

    public void setRes_name(String res_name);

    public String getStore_fname();

    public void setStore_fname(String store_fname);

    public byte[] getThumbnail();

    public void setThumbnail(byte[] thumbnail);

    public String getType();

    public void setType(String type);

    public String getUrl();

    public void setUrl(String url);

    public String getWebsite_url();

    public void setWebsite_url(String website_url);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
