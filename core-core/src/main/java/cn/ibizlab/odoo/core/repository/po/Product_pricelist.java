package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelistSearchContext;

/**
 * 实体 [价格表] 存储模型
 */
public interface Product_pricelist{

    /**
     * 价格表名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [价格表名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 可选
     */
    String getSelectable();

    void setSelectable(String selectable);

    /**
     * 获取 [可选]脏标记
     */
    boolean getSelectableDirtyFlag();

    /**
     * 折扣政策
     */
    String getDiscount_policy();

    void setDiscount_policy(String discount_policy);

    /**
     * 获取 [折扣政策]脏标记
     */
    boolean getDiscount_policyDirtyFlag();

    /**
     * 国家组
     */
    String getCountry_group_ids();

    void setCountry_group_ids(String country_group_ids);

    /**
     * 获取 [国家组]脏标记
     */
    boolean getCountry_group_idsDirtyFlag();

    /**
     * 电子商务促销代码
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [电子商务促销代码]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 价格表项目
     */
    String getItem_ids();

    void setItem_ids(String item_ids);

    /**
     * 获取 [价格表项目]脏标记
     */
    boolean getItem_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

}
