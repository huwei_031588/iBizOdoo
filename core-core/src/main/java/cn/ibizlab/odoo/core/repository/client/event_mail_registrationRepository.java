package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.event_mail_registration;

/**
 * 实体[event_mail_registration] 服务对象接口
 */
public interface event_mail_registrationRepository{


    public event_mail_registration createPO() ;
        public void update(event_mail_registration event_mail_registration);

        public void remove(String id);

        public void createBatch(event_mail_registration event_mail_registration);

        public void removeBatch(String id);

        public void updateBatch(event_mail_registration event_mail_registration);

        public void create(event_mail_registration event_mail_registration);

        public void get(String id);

        public List<event_mail_registration> search();


}
