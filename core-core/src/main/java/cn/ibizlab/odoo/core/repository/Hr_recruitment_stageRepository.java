package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_recruitment_stage;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;

/**
 * 实体 [招聘阶段] 存储对象
 */
public interface Hr_recruitment_stageRepository extends Repository<Hr_recruitment_stage> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_recruitment_stage> searchDefault(Hr_recruitment_stageSearchContext context);

    Hr_recruitment_stage convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage domain , Hr_recruitment_stage po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage convert2Domain( Hr_recruitment_stage po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage domain) ;

}
