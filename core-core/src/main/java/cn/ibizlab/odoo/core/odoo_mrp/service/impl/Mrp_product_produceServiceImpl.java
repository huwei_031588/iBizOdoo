package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produceSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_product_produceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_product_produceOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_product_produceClientModel;

/**
 * 实体[记录生产] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_product_produceServiceImpl implements IMrp_product_produceService {

    @Autowired
    mrp_product_produceOdooClient mrp_product_produceOdooClient;


    @Override
    public boolean create(Mrp_product_produce et) {
        mrp_product_produceClientModel clientModel = convert2Model(et,null);
		mrp_product_produceOdooClient.create(clientModel);
        Mrp_product_produce rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_product_produce> list){
    }

    @Override
    public boolean update(Mrp_product_produce et) {
        mrp_product_produceClientModel clientModel = convert2Model(et,null);
		mrp_product_produceOdooClient.update(clientModel);
        Mrp_product_produce rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_product_produce> list){
    }

    @Override
    public boolean remove(Integer id) {
        mrp_product_produceClientModel clientModel = new mrp_product_produceClientModel();
        clientModel.setId(id);
		mrp_product_produceOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mrp_product_produce get(Integer id) {
        mrp_product_produceClientModel clientModel = new mrp_product_produceClientModel();
        clientModel.setId(id);
		mrp_product_produceOdooClient.get(clientModel);
        Mrp_product_produce et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_product_produce();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_product_produce> searchDefault(Mrp_product_produceSearchContext context) {
        List<Mrp_product_produce> list = new ArrayList<Mrp_product_produce>();
        Page<mrp_product_produceClientModel> clientModelList = mrp_product_produceOdooClient.search(context);
        for(mrp_product_produceClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_product_produce>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_product_produceClientModel convert2Model(Mrp_product_produce domain , mrp_product_produceClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_product_produceClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("product_qtydirtyflag"))
                model.setProduct_qty(domain.getProductQty());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("serialdirtyflag"))
                model.setSerial(domain.getSerial());
            if((Boolean) domain.getExtensionparams().get("produce_line_idsdirtyflag"))
                model.setProduce_line_ids(domain.getProduceLineIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("production_id_textdirtyflag"))
                model.setProduction_id_text(domain.getProductionIdText());
            if((Boolean) domain.getExtensionparams().get("product_trackingdirtyflag"))
                model.setProduct_tracking(domain.getProductTracking());
            if((Boolean) domain.getExtensionparams().get("lot_id_textdirtyflag"))
                model.setLot_id_text(domain.getLotIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("lot_iddirtyflag"))
                model.setLot_id(domain.getLotId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("production_iddirtyflag"))
                model.setProduction_id(domain.getProductionId());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_product_produce convert2Domain( mrp_product_produceClientModel model ,Mrp_product_produce domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_product_produce();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getProduct_qtyDirtyFlag())
            domain.setProductQty(model.getProduct_qty());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getSerialDirtyFlag())
            domain.setSerial(model.getSerial());
        if(model.getProduce_line_idsDirtyFlag())
            domain.setProduceLineIds(model.getProduce_line_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduction_id_textDirtyFlag())
            domain.setProductionIdText(model.getProduction_id_text());
        if(model.getProduct_trackingDirtyFlag())
            domain.setProductTracking(model.getProduct_tracking());
        if(model.getLot_id_textDirtyFlag())
            domain.setLotIdText(model.getLot_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getLot_idDirtyFlag())
            domain.setLotId(model.getLot_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduction_idDirtyFlag())
            domain.setProductionId(model.getProduction_id());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        return domain ;
    }

}

    



