package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_common_journal_reportSearchContext;

/**
 * 实体 [通用日记账报告] 存储模型
 */
public interface Account_common_journal_report{

    /**
     * 目标分录
     */
    String getTarget_move();

    void setTarget_move(String target_move);

    /**
     * 获取 [目标分录]脏标记
     */
    boolean getTarget_moveDirtyFlag();

    /**
     * 使用货币
     */
    String getAmount_currency();

    void setAmount_currency(String amount_currency);

    /**
     * 获取 [使用货币]脏标记
     */
    boolean getAmount_currencyDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getDate_to();

    void setDate_to(Timestamp date_to);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getDate_toDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 凭证类型
     */
    String getJournal_ids();

    void setJournal_ids(String journal_ids);

    /**
     * 获取 [凭证类型]脏标记
     */
    boolean getJournal_idsDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_from();

    void setDate_from(Timestamp date_from);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_fromDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
