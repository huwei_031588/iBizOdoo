package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_orderSearchContext;

/**
 * 实体 [销售订单] 存储模型
 */
public interface Sale_order{

    /**
     * 单据日期
     */
    Timestamp getDate_order();

    void setDate_order(Timestamp date_order);

    /**
     * 获取 [单据日期]脏标记
     */
    boolean getDate_orderDirtyFlag();

    /**
     * 发票
     */
    String getInvoice_ids();

    void setInvoice_ids(String invoice_ids);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_idsDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 未税金额
     */
    Double getAmount_untaxed();

    void setAmount_untaxed(Double amount_untaxed);

    /**
     * 获取 [未税金额]脏标记
     */
    boolean getAmount_untaxedDirtyFlag();

    /**
     * 验证
     */
    Timestamp getValidity_date();

    void setValidity_date(Timestamp validity_date);

    /**
     * 获取 [验证]脏标记
     */
    boolean getValidity_dateDirtyFlag();

    /**
     * 折扣前金额
     */
    Double getAmount_undiscounted();

    void setAmount_undiscounted(Double amount_undiscounted);

    /**
     * 获取 [折扣前金额]脏标记
     */
    boolean getAmount_undiscountedDirtyFlag();

    /**
     * 发票数
     */
    Integer getInvoice_count();

    void setInvoice_count(Integer invoice_count);

    /**
     * 获取 [发票数]脏标记
     */
    boolean getInvoice_countDirtyFlag();

    /**
     * 访问警告
     */
    String getAccess_warning();

    void setAccess_warning(String access_warning);

    /**
     * 获取 [访问警告]脏标记
     */
    boolean getAccess_warningDirtyFlag();

    /**
     * 警告
     */
    String getWarning_stock();

    void setWarning_stock(String warning_stock);

    /**
     * 获取 [警告]脏标记
     */
    boolean getWarning_stockDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 实际日期
     */
    Timestamp getEffective_date();

    void setEffective_date(Timestamp effective_date);

    /**
     * 获取 [实际日期]脏标记
     */
    boolean getEffective_dateDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 在线签名
     */
    String getRequire_signature();

    void setRequire_signature(String require_signature);

    /**
     * 获取 [在线签名]脏标记
     */
    boolean getRequire_signatureDirtyFlag();

    /**
     * 汇率
     */
    Double getCurrency_rate();

    void setCurrency_rate(Double currency_rate);

    /**
     * 获取 [汇率]脏标记
     */
    boolean getCurrency_rateDirtyFlag();

    /**
     * 送货策略
     */
    String getPicking_policy();

    void setPicking_policy(String picking_policy);

    /**
     * 获取 [送货策略]脏标记
     */
    boolean getPicking_policyDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 签名
     */
    byte[] getSignature();

    void setSignature(byte[] signature);

    /**
     * 获取 [签名]脏标记
     */
    boolean getSignatureDirtyFlag();

    /**
     * 补货组
     */
    Integer getProcurement_group_id();

    void setProcurement_group_id(Integer procurement_group_id);

    /**
     * 获取 [补货组]脏标记
     */
    boolean getProcurement_group_idDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 购物车数量
     */
    Integer getCart_quantity();

    void setCart_quantity(Integer cart_quantity);

    /**
     * 获取 [购物车数量]脏标记
     */
    boolean getCart_quantityDirtyFlag();

    /**
     * 类型名称
     */
    String getType_name();

    void setType_name(String type_name);

    /**
     * 获取 [类型名称]脏标记
     */
    boolean getType_nameDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 出库单
     */
    Integer getDelivery_count();

    void setDelivery_count(Integer delivery_count);

    /**
     * 获取 [出库单]脏标记
     */
    boolean getDelivery_countDirtyFlag();

    /**
     * 已签核
     */
    String getSigned_by();

    void setSigned_by(String signed_by);

    /**
     * 获取 [已签核]脏标记
     */
    boolean getSigned_byDirtyFlag();

    /**
     * 订单行
     */
    String getOrder_line();

    void setOrder_line(String order_line);

    /**
     * 获取 [订单行]脏标记
     */
    boolean getOrder_lineDirtyFlag();

    /**
     * 创建日期
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建日期]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 在线支付
     */
    String getRequire_payment();

    void setRequire_payment(String require_payment);

    /**
     * 获取 [在线支付]脏标记
     */
    boolean getRequire_paymentDirtyFlag();

    /**
     * 只是服务
     */
    String getOnly_services();

    void setOnly_services(String only_services);

    /**
     * 获取 [只是服务]脏标记
     */
    boolean getOnly_servicesDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 网页显示订单明细
     */
    String getWebsite_order_line();

    void setWebsite_order_line(String website_order_line);

    /**
     * 获取 [网页显示订单明细]脏标记
     */
    boolean getWebsite_order_lineDirtyFlag();

    /**
     * 付款参考:
     */
    String getReference();

    void setReference(String reference);

    /**
     * 获取 [付款参考:]脏标记
     */
    boolean getReferenceDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 发票状态
     */
    String getInvoice_status();

    void setInvoice_status(String invoice_status);

    /**
     * 获取 [发票状态]脏标记
     */
    boolean getInvoice_statusDirtyFlag();

    /**
     * 标签
     */
    String getTag_ids();

    void setTag_ids(String tag_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getTag_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 交易
     */
    String getTransaction_ids();

    void setTransaction_ids(String transaction_ids);

    /**
     * 获取 [交易]脏标记
     */
    boolean getTransaction_idsDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 遗弃的购物车
     */
    String getIs_abandoned_cart();

    void setIs_abandoned_cart(String is_abandoned_cart);

    /**
     * 获取 [遗弃的购物车]脏标记
     */
    boolean getIs_abandoned_cartDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * # 费用
     */
    Integer getExpense_count();

    void setExpense_count(Integer expense_count);

    /**
     * 获取 [# 费用]脏标记
     */
    boolean getExpense_countDirtyFlag();

    /**
     * 已授权的交易
     */
    String getAuthorized_transaction_ids();

    void setAuthorized_transaction_ids(String authorized_transaction_ids);

    /**
     * 获取 [已授权的交易]脏标记
     */
    boolean getAuthorized_transaction_idsDirtyFlag();

    /**
     * 承诺日期
     */
    Timestamp getCommitment_date();

    void setCommitment_date(Timestamp commitment_date);

    /**
     * 获取 [承诺日期]脏标记
     */
    boolean getCommitment_dateDirtyFlag();

    /**
     * 过期了
     */
    String getIs_expired();

    void setIs_expired(String is_expired);

    /**
     * 获取 [过期了]脏标记
     */
    boolean getIs_expiredDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 剩余确认天数
     */
    Integer getRemaining_validity_days();

    void setRemaining_validity_days(Integer remaining_validity_days);

    /**
     * 获取 [剩余确认天数]脏标记
     */
    boolean getRemaining_validity_daysDirtyFlag();

    /**
     * 预计日期
     */
    Timestamp getExpected_date();

    void setExpected_date(Timestamp expected_date);

    /**
     * 获取 [预计日期]脏标记
     */
    boolean getExpected_dateDirtyFlag();

    /**
     * 采购订单号
     */
    Integer getPurchase_order_count();

    void setPurchase_order_count(Integer purchase_order_count);

    /**
     * 获取 [采购订单号]脏标记
     */
    boolean getPurchase_order_countDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 门户访问网址
     */
    String getAccess_url();

    void setAccess_url(String access_url);

    /**
     * 获取 [门户访问网址]脏标记
     */
    boolean getAccess_urlDirtyFlag();

    /**
     * 订单关联
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [订单关联]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 可选产品行
     */
    String getSale_order_option_ids();

    void setSale_order_option_ids(String sale_order_option_ids);

    /**
     * 获取 [可选产品行]脏标记
     */
    boolean getSale_order_option_idsDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 源文档
     */
    String getOrigin();

    void setOrigin(String origin);

    /**
     * 获取 [源文档]脏标记
     */
    boolean getOriginDirtyFlag();

    /**
     * 确认日期
     */
    Timestamp getConfirmation_date();

    void setConfirmation_date(Timestamp confirmation_date);

    /**
     * 获取 [确认日期]脏标记
     */
    boolean getConfirmation_dateDirtyFlag();

    /**
     * 条款和条件
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [条款和条件]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 按组分配税额
     */
    byte[] getAmount_by_group();

    void setAmount_by_group(byte[] amount_by_group);

    /**
     * 获取 [按组分配税额]脏标记
     */
    boolean getAmount_by_groupDirtyFlag();

    /**
     * 拣货
     */
    String getPicking_ids();

    void setPicking_ids(String picking_ids);

    /**
     * 获取 [拣货]脏标记
     */
    boolean getPicking_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 安全令牌
     */
    String getAccess_token();

    void setAccess_token(String access_token);

    /**
     * 获取 [安全令牌]脏标记
     */
    boolean getAccess_tokenDirtyFlag();

    /**
     * 总计
     */
    Double getAmount_total();

    void setAmount_total(Double amount_total);

    /**
     * 获取 [总计]脏标记
     */
    boolean getAmount_totalDirtyFlag();

    /**
     * 客户订单号
     */
    String getClient_order_ref();

    void setClient_order_ref(String client_order_ref);

    /**
     * 获取 [客户订单号]脏标记
     */
    boolean getClient_order_refDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 购物车恢复EMail已发送
     */
    String getCart_recovery_email_sent();

    void setCart_recovery_email_sent(String cart_recovery_email_sent);

    /**
     * 获取 [购物车恢复EMail已发送]脏标记
     */
    boolean getCart_recovery_email_sentDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 税率
     */
    Double getAmount_tax();

    void setAmount_tax(Double amount_tax);

    /**
     * 获取 [税率]脏标记
     */
    boolean getAmount_taxDirtyFlag();

    /**
     * 费用
     */
    String getExpense_ids();

    void setExpense_ids(String expense_ids);

    /**
     * 获取 [费用]脏标记
     */
    boolean getExpense_idsDirtyFlag();

    /**
     * 发票地址
     */
    String getPartner_invoice_id_text();

    void setPartner_invoice_id_text(String partner_invoice_id_text);

    /**
     * 获取 [发票地址]脏标记
     */
    boolean getPartner_invoice_id_textDirtyFlag();

    /**
     * 贸易条款
     */
    String getIncoterm_text();

    void setIncoterm_text(String incoterm_text);

    /**
     * 获取 [贸易条款]脏标记
     */
    boolean getIncoterm_textDirtyFlag();

    /**
     * 销售团队
     */
    String getTeam_id_text();

    void setTeam_id_text(String team_id_text);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_id_textDirtyFlag();

    /**
     * 分析账户
     */
    String getAnalytic_account_id_text();

    void setAnalytic_account_id_text(String analytic_account_id_text);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAnalytic_account_id_textDirtyFlag();

    /**
     * 客户
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 营销
     */
    String getCampaign_id_text();

    void setCampaign_id_text(String campaign_id_text);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 送货地址
     */
    String getPartner_shipping_id_text();

    void setPartner_shipping_id_text(String partner_shipping_id_text);

    /**
     * 获取 [送货地址]脏标记
     */
    boolean getPartner_shipping_id_textDirtyFlag();

    /**
     * 仓库
     */
    String getWarehouse_id_text();

    void setWarehouse_id_text(String warehouse_id_text);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_id_textDirtyFlag();

    /**
     * 销售员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 商机
     */
    String getOpportunity_id_text();

    void setOpportunity_id_text(String opportunity_id_text);

    /**
     * 获取 [商机]脏标记
     */
    boolean getOpportunity_id_textDirtyFlag();

    /**
     * 来源
     */
    String getSource_id_text();

    void setSource_id_text(String source_id_text);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_id_textDirtyFlag();

    /**
     * 媒体
     */
    String getMedium_id_text();

    void setMedium_id_text(String medium_id_text);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_id_textDirtyFlag();

    /**
     * 税科目调整
     */
    String getFiscal_position_id_text();

    void setFiscal_position_id_text(String fiscal_position_id_text);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getFiscal_position_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 价格表
     */
    String getPricelist_id_text();

    void setPricelist_id_text(String pricelist_id_text);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPricelist_id_textDirtyFlag();

    /**
     * 付款条款
     */
    String getPayment_term_id_text();

    void setPayment_term_id_text(String payment_term_id_text);

    /**
     * 获取 [付款条款]脏标记
     */
    boolean getPayment_term_id_textDirtyFlag();

    /**
     * 报价单模板
     */
    String getSale_order_template_id_text();

    void setSale_order_template_id_text(String sale_order_template_id_text);

    /**
     * 获取 [报价单模板]脏标记
     */
    boolean getSale_order_template_id_textDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 销售团队
     */
    Integer getTeam_id();

    void setTeam_id(Integer team_id);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_idDirtyFlag();

    /**
     * 发票地址
     */
    Integer getPartner_invoice_id();

    void setPartner_invoice_id(Integer partner_invoice_id);

    /**
     * 获取 [发票地址]脏标记
     */
    boolean getPartner_invoice_idDirtyFlag();

    /**
     * 税科目调整
     */
    Integer getFiscal_position_id();

    void setFiscal_position_id(Integer fiscal_position_id);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getFiscal_position_idDirtyFlag();

    /**
     * 客户
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 营销
     */
    Integer getCampaign_id();

    void setCampaign_id(Integer campaign_id);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_idDirtyFlag();

    /**
     * 来源
     */
    Integer getSource_id();

    void setSource_id(Integer source_id);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_idDirtyFlag();

    /**
     * 报价单模板
     */
    Integer getSale_order_template_id();

    void setSale_order_template_id(Integer sale_order_template_id);

    /**
     * 获取 [报价单模板]脏标记
     */
    boolean getSale_order_template_idDirtyFlag();

    /**
     * 销售员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 媒体
     */
    Integer getMedium_id();

    void setMedium_id(Integer medium_id);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_idDirtyFlag();

    /**
     * 分析账户
     */
    Integer getAnalytic_account_id();

    void setAnalytic_account_id(Integer analytic_account_id);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAnalytic_account_idDirtyFlag();

    /**
     * 付款条款
     */
    Integer getPayment_term_id();

    void setPayment_term_id(Integer payment_term_id);

    /**
     * 获取 [付款条款]脏标记
     */
    boolean getPayment_term_idDirtyFlag();

    /**
     * 送货地址
     */
    Integer getPartner_shipping_id();

    void setPartner_shipping_id(Integer partner_shipping_id);

    /**
     * 获取 [送货地址]脏标记
     */
    boolean getPartner_shipping_idDirtyFlag();

    /**
     * 商机
     */
    Integer getOpportunity_id();

    void setOpportunity_id(Integer opportunity_id);

    /**
     * 获取 [商机]脏标记
     */
    boolean getOpportunity_idDirtyFlag();

    /**
     * 贸易条款
     */
    Integer getIncoterm();

    void setIncoterm(Integer incoterm);

    /**
     * 获取 [贸易条款]脏标记
     */
    boolean getIncotermDirtyFlag();

    /**
     * 价格表
     */
    Integer getPricelist_id();

    void setPricelist_id(Integer pricelist_id);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPricelist_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 仓库
     */
    Integer getWarehouse_id();

    void setWarehouse_id(Integer warehouse_id);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_idDirtyFlag();

}
