package cn.ibizlab.odoo.odoo_web_editor.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_web_editor")
public class odoo_web_editorRestConfiguration {

}
