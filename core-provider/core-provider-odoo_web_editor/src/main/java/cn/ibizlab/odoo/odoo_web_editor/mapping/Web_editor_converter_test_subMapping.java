package cn.ibizlab.odoo.odoo_web_editor.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.odoo.odoo_web_editor.dto.Web_editor_converter_test_subDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Web_editor_converter_test_subMapping extends MappingBase<Web_editor_converter_test_subDTO, Web_editor_converter_test_sub> {


}

