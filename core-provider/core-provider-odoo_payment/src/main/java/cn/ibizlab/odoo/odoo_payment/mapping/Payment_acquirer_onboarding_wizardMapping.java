package cn.ibizlab.odoo.odoo_payment.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.odoo_payment.dto.Payment_acquirer_onboarding_wizardDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Payment_acquirer_onboarding_wizardMapping extends MappingBase<Payment_acquirer_onboarding_wizardDTO, Payment_acquirer_onboarding_wizard> {


}

