package cn.ibizlab.odoo.odoo_payment.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Payment_transactionDTO]
 */
@Data
public class Payment_transactionDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [PARTNER_PHONE]
     *
     */
    @JSONField(name = "partner_phone")
    @JsonProperty("partner_phone")
    private String partnerPhone;

    /**
     * 属性 [PARTNER_EMAIL]
     *
     */
    @JSONField(name = "partner_email")
    @JsonProperty("partner_email")
    private String partnerEmail;

    /**
     * 属性 [CALLBACK_METHOD]
     *
     */
    @JSONField(name = "callback_method")
    @JsonProperty("callback_method")
    private String callbackMethod;

    /**
     * 属性 [RETURN_URL]
     *
     */
    @JSONField(name = "return_url")
    @JsonProperty("return_url")
    private String returnUrl;

    /**
     * 属性 [SALE_ORDER_IDS_NBR]
     *
     */
    @JSONField(name = "sale_order_ids_nbr")
    @JsonProperty("sale_order_ids_nbr")
    private Integer saleOrderIdsNbr;

    /**
     * 属性 [ACQUIRER_REFERENCE]
     *
     */
    @JSONField(name = "acquirer_reference")
    @JsonProperty("acquirer_reference")
    private String acquirerReference;

    /**
     * 属性 [PARTNER_LANG]
     *
     */
    @JSONField(name = "partner_lang")
    @JsonProperty("partner_lang")
    private String partnerLang;

    /**
     * 属性 [PARTNER_ZIP]
     *
     */
    @JSONField(name = "partner_zip")
    @JsonProperty("partner_zip")
    private String partnerZip;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [CALLBACK_MODEL_ID]
     *
     */
    @JSONField(name = "callback_model_id")
    @JsonProperty("callback_model_id")
    private Integer callbackModelId;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;

    /**
     * 属性 [SALE_ORDER_IDS]
     *
     */
    @JSONField(name = "sale_order_ids")
    @JsonProperty("sale_order_ids")
    private String saleOrderIds;

    /**
     * 属性 [FEES]
     *
     */
    @JSONField(name = "fees")
    @JsonProperty("fees")
    private Double fees;

    /**
     * 属性 [STATE_MESSAGE]
     *
     */
    @JSONField(name = "state_message")
    @JsonProperty("state_message")
    private String stateMessage;

    /**
     * 属性 [CALLBACK_RES_ID]
     *
     */
    @JSONField(name = "callback_res_id")
    @JsonProperty("callback_res_id")
    private Integer callbackResId;

    /**
     * 属性 [PARTNER_CITY]
     *
     */
    @JSONField(name = "partner_city")
    @JsonProperty("partner_city")
    private String partnerCity;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [INVOICE_IDS_NBR]
     *
     */
    @JSONField(name = "invoice_ids_nbr")
    @JsonProperty("invoice_ids_nbr")
    private Integer invoiceIdsNbr;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 属性 [HTML_3DS]
     *
     */
    @JSONField(name = "html_3ds")
    @JsonProperty("html_3ds")
    private String html3ds;

    /**
     * 属性 [PARTNER_NAME]
     *
     */
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    private String partnerName;

    /**
     * 属性 [REFERENCE]
     *
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;

    /**
     * 属性 [PARTNER_ADDRESS]
     *
     */
    @JSONField(name = "partner_address")
    @JsonProperty("partner_address")
    private String partnerAddress;

    /**
     * 属性 [IS_PROCESSED]
     *
     */
    @JSONField(name = "is_processed")
    @JsonProperty("is_processed")
    private String isProcessed;

    /**
     * 属性 [CALLBACK_HASH]
     *
     */
    @JSONField(name = "callback_hash")
    @JsonProperty("callback_hash")
    private String callbackHash;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [PAYMENT_ID_TEXT]
     *
     */
    @JSONField(name = "payment_id_text")
    @JsonProperty("payment_id_text")
    private String paymentIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 属性 [PROVIDER]
     *
     */
    @JSONField(name = "provider")
    @JsonProperty("provider")
    private String provider;

    /**
     * 属性 [ACQUIRER_ID_TEXT]
     *
     */
    @JSONField(name = "acquirer_id_text")
    @JsonProperty("acquirer_id_text")
    private String acquirerIdText;

    /**
     * 属性 [PARTNER_COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "partner_country_id_text")
    @JsonProperty("partner_country_id_text")
    private String partnerCountryIdText;

    /**
     * 属性 [PAYMENT_TOKEN_ID_TEXT]
     *
     */
    @JSONField(name = "payment_token_id_text")
    @JsonProperty("payment_token_id_text")
    private String paymentTokenIdText;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [PARTNER_COUNTRY_ID]
     *
     */
    @JSONField(name = "partner_country_id")
    @JsonProperty("partner_country_id")
    private Integer partnerCountryId;

    /**
     * 属性 [PAYMENT_TOKEN_ID]
     *
     */
    @JSONField(name = "payment_token_id")
    @JsonProperty("payment_token_id")
    private Integer paymentTokenId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PAYMENT_ID]
     *
     */
    @JSONField(name = "payment_id")
    @JsonProperty("payment_id")
    private Integer paymentId;

    /**
     * 属性 [ACQUIRER_ID]
     *
     */
    @JSONField(name = "acquirer_id")
    @JsonProperty("acquirer_id")
    private Integer acquirerId;


    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [PARTNER_PHONE]
     */
    public void setPartnerPhone(String  partnerPhone){
        this.partnerPhone = partnerPhone ;
        this.modify("partner_phone",partnerPhone);
    }

    /**
     * 设置 [PARTNER_EMAIL]
     */
    public void setPartnerEmail(String  partnerEmail){
        this.partnerEmail = partnerEmail ;
        this.modify("partner_email",partnerEmail);
    }

    /**
     * 设置 [CALLBACK_METHOD]
     */
    public void setCallbackMethod(String  callbackMethod){
        this.callbackMethod = callbackMethod ;
        this.modify("callback_method",callbackMethod);
    }

    /**
     * 设置 [RETURN_URL]
     */
    public void setReturnUrl(String  returnUrl){
        this.returnUrl = returnUrl ;
        this.modify("return_url",returnUrl);
    }

    /**
     * 设置 [ACQUIRER_REFERENCE]
     */
    public void setAcquirerReference(String  acquirerReference){
        this.acquirerReference = acquirerReference ;
        this.modify("acquirer_reference",acquirerReference);
    }

    /**
     * 设置 [PARTNER_LANG]
     */
    public void setPartnerLang(String  partnerLang){
        this.partnerLang = partnerLang ;
        this.modify("partner_lang",partnerLang);
    }

    /**
     * 设置 [PARTNER_ZIP]
     */
    public void setPartnerZip(String  partnerZip){
        this.partnerZip = partnerZip ;
        this.modify("partner_zip",partnerZip);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [CALLBACK_MODEL_ID]
     */
    public void setCallbackModelId(Integer  callbackModelId){
        this.callbackModelId = callbackModelId ;
        this.modify("callback_model_id",callbackModelId);
    }

    /**
     * 设置 [FEES]
     */
    public void setFees(Double  fees){
        this.fees = fees ;
        this.modify("fees",fees);
    }

    /**
     * 设置 [STATE_MESSAGE]
     */
    public void setStateMessage(String  stateMessage){
        this.stateMessage = stateMessage ;
        this.modify("state_message",stateMessage);
    }

    /**
     * 设置 [CALLBACK_RES_ID]
     */
    public void setCallbackResId(Integer  callbackResId){
        this.callbackResId = callbackResId ;
        this.modify("callback_res_id",callbackResId);
    }

    /**
     * 设置 [PARTNER_CITY]
     */
    public void setPartnerCity(String  partnerCity){
        this.partnerCity = partnerCity ;
        this.modify("partner_city",partnerCity);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [HTML_3DS]
     */
    public void setHtml3ds(String  html3ds){
        this.html3ds = html3ds ;
        this.modify("html_3ds",html3ds);
    }

    /**
     * 设置 [PARTNER_NAME]
     */
    public void setPartnerName(String  partnerName){
        this.partnerName = partnerName ;
        this.modify("partner_name",partnerName);
    }

    /**
     * 设置 [REFERENCE]
     */
    public void setReference(String  reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [PARTNER_ADDRESS]
     */
    public void setPartnerAddress(String  partnerAddress){
        this.partnerAddress = partnerAddress ;
        this.modify("partner_address",partnerAddress);
    }

    /**
     * 设置 [IS_PROCESSED]
     */
    public void setIsProcessed(String  isProcessed){
        this.isProcessed = isProcessed ;
        this.modify("is_processed",isProcessed);
    }

    /**
     * 设置 [CALLBACK_HASH]
     */
    public void setCallbackHash(String  callbackHash){
        this.callbackHash = callbackHash ;
        this.modify("callback_hash",callbackHash);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Integer  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [PARTNER_COUNTRY_ID]
     */
    public void setPartnerCountryId(Integer  partnerCountryId){
        this.partnerCountryId = partnerCountryId ;
        this.modify("partner_country_id",partnerCountryId);
    }

    /**
     * 设置 [PAYMENT_TOKEN_ID]
     */
    public void setPaymentTokenId(Integer  paymentTokenId){
        this.paymentTokenId = paymentTokenId ;
        this.modify("payment_token_id",paymentTokenId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [PAYMENT_ID]
     */
    public void setPaymentId(Integer  paymentId){
        this.paymentId = paymentId ;
        this.modify("payment_id",paymentId);
    }

    /**
     * 设置 [ACQUIRER_ID]
     */
    public void setAcquirerId(Integer  acquirerId){
        this.acquirerId = acquirerId ;
        this.modify("acquirer_id",acquirerId);
    }


}

