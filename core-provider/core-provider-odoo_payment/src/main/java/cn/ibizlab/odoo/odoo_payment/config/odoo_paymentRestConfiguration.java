package cn.ibizlab.odoo.odoo_payment.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_payment")
public class odoo_paymentRestConfiguration {

}
