package cn.ibizlab.odoo.odoo_payment.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.odoo.odoo_payment.dto.Payment_tokenDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Payment_tokenMapping extends MappingBase<Payment_tokenDTO, Payment_token> {


}

