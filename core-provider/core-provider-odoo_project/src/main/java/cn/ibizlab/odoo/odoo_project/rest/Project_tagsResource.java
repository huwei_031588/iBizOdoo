package cn.ibizlab.odoo.odoo_project.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_project.dto.*;
import cn.ibizlab.odoo.odoo_project.mapping.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_tags;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_tagsService;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_tagsSearchContext;




@Slf4j
@Api(tags = {"Project_tags" })
@RestController("odoo_project-project_tags")
@RequestMapping("")
public class Project_tagsResource {

    @Autowired
    private IProject_tagsService project_tagsService;

    @Autowired
    @Lazy
    private Project_tagsMapping project_tagsMapping;




    @PreAuthorize("hasPermission(#project_tags_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Project_tags" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/project_tags/{project_tags_id}")
    public ResponseEntity<Project_tagsDTO> get(@PathVariable("project_tags_id") Integer project_tags_id) {
        Project_tags domain = project_tagsService.get(project_tags_id);
        Project_tagsDTO dto = project_tagsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#project_tags_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Project_tags" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_tags/{project_tags_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("project_tags_id") Integer project_tags_id) {
         return ResponseEntity.status(HttpStatus.OK).body(project_tagsService.remove(project_tags_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Project_tags" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        project_tagsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Project_tags" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tags")

    public ResponseEntity<Project_tagsDTO> create(@RequestBody Project_tagsDTO project_tagsdto) {
        Project_tags domain = project_tagsMapping.toDomain(project_tagsdto);
		project_tagsService.create(domain);
        Project_tagsDTO dto = project_tagsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Project_tags" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Project_tagsDTO> project_tagsdtos) {
        project_tagsService.createBatch(project_tagsMapping.toDomain(project_tagsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#project_tags_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Project_tags" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_tags/{project_tags_id}")

    public ResponseEntity<Project_tagsDTO> update(@PathVariable("project_tags_id") Integer project_tags_id, @RequestBody Project_tagsDTO project_tagsdto) {
		Project_tags domain = project_tagsMapping.toDomain(project_tagsdto);
        domain.setId(project_tags_id);
		project_tagsService.update(domain);
		Project_tagsDTO dto = project_tagsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#project_tags_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Project_tags" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_tagsDTO> project_tagsdtos) {
        project_tagsService.updateBatch(project_tagsMapping.toDomain(project_tagsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Project_tags" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/project_tags/fetchdefault")
	public ResponseEntity<List<Project_tagsDTO>> fetchDefault(Project_tagsSearchContext context) {
        Page<Project_tags> domains = project_tagsService.searchDefault(context) ;
        List<Project_tagsDTO> list = project_tagsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Project_tags" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/project_tags/searchdefault")
	public ResponseEntity<Page<Project_tagsDTO>> searchDefault(Project_tagsSearchContext context) {
        Page<Project_tags> domains = project_tagsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(project_tagsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Project_tags getEntity(){
        return new Project_tags();
    }

}
