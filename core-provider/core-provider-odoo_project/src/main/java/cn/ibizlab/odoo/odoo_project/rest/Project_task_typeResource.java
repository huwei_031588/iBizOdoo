package cn.ibizlab.odoo.odoo_project.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_project.dto.*;
import cn.ibizlab.odoo.odoo_project.mapping.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_task_typeService;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_task_typeSearchContext;




@Slf4j
@Api(tags = {"Project_task_type" })
@RestController("odoo_project-project_task_type")
@RequestMapping("")
public class Project_task_typeResource {

    @Autowired
    private IProject_task_typeService project_task_typeService;

    @Autowired
    @Lazy
    private Project_task_typeMapping project_task_typeMapping;




    @PreAuthorize("hasPermission(#project_task_type_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Project_task_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/project_task_types/{project_task_type_id}")
    public ResponseEntity<Project_task_typeDTO> get(@PathVariable("project_task_type_id") Integer project_task_type_id) {
        Project_task_type domain = project_task_typeService.get(project_task_type_id);
        Project_task_typeDTO dto = project_task_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Project_task_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/project_task_types")

    public ResponseEntity<Project_task_typeDTO> create(@RequestBody Project_task_typeDTO project_task_typedto) {
        Project_task_type domain = project_task_typeMapping.toDomain(project_task_typedto);
		project_task_typeService.create(domain);
        Project_task_typeDTO dto = project_task_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Project_task_type" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/project_task_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Project_task_typeDTO> project_task_typedtos) {
        project_task_typeService.createBatch(project_task_typeMapping.toDomain(project_task_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#project_task_type_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Project_task_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_task_types/{project_task_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("project_task_type_id") Integer project_task_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(project_task_typeService.remove(project_task_type_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Project_task_type" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_task_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        project_task_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#project_task_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Project_task_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_task_types/{project_task_type_id}")

    public ResponseEntity<Project_task_typeDTO> update(@PathVariable("project_task_type_id") Integer project_task_type_id, @RequestBody Project_task_typeDTO project_task_typedto) {
		Project_task_type domain = project_task_typeMapping.toDomain(project_task_typedto);
        domain.setId(project_task_type_id);
		project_task_typeService.update(domain);
		Project_task_typeDTO dto = project_task_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#project_task_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Project_task_type" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_task_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_task_typeDTO> project_task_typedtos) {
        project_task_typeService.updateBatch(project_task_typeMapping.toDomain(project_task_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Project_task_type" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/project_task_types/fetchdefault")
	public ResponseEntity<List<Project_task_typeDTO>> fetchDefault(Project_task_typeSearchContext context) {
        Page<Project_task_type> domains = project_task_typeService.searchDefault(context) ;
        List<Project_task_typeDTO> list = project_task_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Project_task_type" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/project_task_types/searchdefault")
	public ResponseEntity<Page<Project_task_typeDTO>> searchDefault(Project_task_typeSearchContext context) {
        Page<Project_task_type> domains = project_task_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(project_task_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Project_task_type getEntity(){
        return new Project_task_type();
    }

}
