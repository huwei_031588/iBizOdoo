package cn.ibizlab.odoo.odoo_project.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_tags;
import cn.ibizlab.odoo.odoo_project.dto.Project_tagsDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Project_tagsMapping extends MappingBase<Project_tagsDTO, Project_tags> {


}

