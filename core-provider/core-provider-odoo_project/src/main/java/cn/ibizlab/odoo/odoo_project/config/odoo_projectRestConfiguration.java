package cn.ibizlab.odoo.odoo_project.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_project")
public class odoo_projectRestConfiguration {

}
