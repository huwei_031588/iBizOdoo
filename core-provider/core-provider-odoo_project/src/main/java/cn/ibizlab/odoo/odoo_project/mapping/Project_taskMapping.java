package cn.ibizlab.odoo.odoo_project.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task;
import cn.ibizlab.odoo.odoo_project.dto.Project_taskDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Project_taskMapping extends MappingBase<Project_taskDTO, Project_task> {


}

