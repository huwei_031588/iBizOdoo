package cn.ibizlab.odoo.odoo_note.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_note.dto.*;
import cn.ibizlab.odoo.odoo_note.mapping.*;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_stage;
import cn.ibizlab.odoo.core.odoo_note.service.INote_stageService;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_stageSearchContext;




@Slf4j
@Api(tags = {"Note_stage" })
@RestController("odoo_note-note_stage")
@RequestMapping("")
public class Note_stageResource {

    @Autowired
    private INote_stageService note_stageService;

    @Autowired
    @Lazy
    private Note_stageMapping note_stageMapping;




    @PreAuthorize("hasPermission('Remove',{#note_stage_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Note_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_stages/{note_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("note_stage_id") Integer note_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(note_stageService.remove(note_stage_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Note_stage" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        note_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Note_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/note_stages")

    public ResponseEntity<Note_stageDTO> create(@RequestBody Note_stageDTO note_stagedto) {
        Note_stage domain = note_stageMapping.toDomain(note_stagedto);
		note_stageService.create(domain);
        Note_stageDTO dto = note_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Note_stage" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/note_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Note_stageDTO> note_stagedtos) {
        note_stageService.createBatch(note_stageMapping.toDomain(note_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#note_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Note_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_stages/{note_stage_id}")

    public ResponseEntity<Note_stageDTO> update(@PathVariable("note_stage_id") Integer note_stage_id, @RequestBody Note_stageDTO note_stagedto) {
		Note_stage domain = note_stageMapping.toDomain(note_stagedto);
        domain.setId(note_stage_id);
		note_stageService.update(domain);
		Note_stageDTO dto = note_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#note_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Note_stage" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Note_stageDTO> note_stagedtos) {
        note_stageService.updateBatch(note_stageMapping.toDomain(note_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#note_stage_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Note_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/note_stages/{note_stage_id}")
    public ResponseEntity<Note_stageDTO> get(@PathVariable("note_stage_id") Integer note_stage_id) {
        Note_stage domain = note_stageService.get(note_stage_id);
        Note_stageDTO dto = note_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Note_stage" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/note_stages/fetchdefault")
	public ResponseEntity<List<Note_stageDTO>> fetchDefault(Note_stageSearchContext context) {
        Page<Note_stage> domains = note_stageService.searchDefault(context) ;
        List<Note_stageDTO> list = note_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Note_stage" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/note_stages/searchdefault")
	public ResponseEntity<Page<Note_stageDTO>> searchDefault(Note_stageSearchContext context) {
        Page<Note_stage> domains = note_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(note_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Note_stage getEntity(){
        return new Note_stage();
    }

}
