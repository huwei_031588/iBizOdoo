package cn.ibizlab.odoo.odoo_note.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_note;
import cn.ibizlab.odoo.odoo_note.dto.Note_noteDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Note_noteMapping extends MappingBase<Note_noteDTO, Note_note> {


}

