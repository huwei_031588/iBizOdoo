package cn.ibizlab.odoo.odoo_note.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_note.dto.*;
import cn.ibizlab.odoo.odoo_note.mapping.*;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_tag;
import cn.ibizlab.odoo.core.odoo_note.service.INote_tagService;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_tagSearchContext;




@Slf4j
@Api(tags = {"Note_tag" })
@RestController("odoo_note-note_tag")
@RequestMapping("")
public class Note_tagResource {

    @Autowired
    private INote_tagService note_tagService;

    @Autowired
    @Lazy
    private Note_tagMapping note_tagMapping;




    @PreAuthorize("hasPermission('Remove',{#note_tag_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Note_tag" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_tags/{note_tag_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("note_tag_id") Integer note_tag_id) {
         return ResponseEntity.status(HttpStatus.OK).body(note_tagService.remove(note_tag_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Note_tag" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        note_tagService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#note_tag_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Note_tag" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_tags/{note_tag_id}")

    public ResponseEntity<Note_tagDTO> update(@PathVariable("note_tag_id") Integer note_tag_id, @RequestBody Note_tagDTO note_tagdto) {
		Note_tag domain = note_tagMapping.toDomain(note_tagdto);
        domain.setId(note_tag_id);
		note_tagService.update(domain);
		Note_tagDTO dto = note_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#note_tag_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Note_tag" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Note_tagDTO> note_tagdtos) {
        note_tagService.updateBatch(note_tagMapping.toDomain(note_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Note_tag" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/note_tags")

    public ResponseEntity<Note_tagDTO> create(@RequestBody Note_tagDTO note_tagdto) {
        Note_tag domain = note_tagMapping.toDomain(note_tagdto);
		note_tagService.create(domain);
        Note_tagDTO dto = note_tagMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Note_tag" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/note_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Note_tagDTO> note_tagdtos) {
        note_tagService.createBatch(note_tagMapping.toDomain(note_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#note_tag_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Note_tag" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/note_tags/{note_tag_id}")
    public ResponseEntity<Note_tagDTO> get(@PathVariable("note_tag_id") Integer note_tag_id) {
        Note_tag domain = note_tagService.get(note_tag_id);
        Note_tagDTO dto = note_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Note_tag" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/note_tags/fetchdefault")
	public ResponseEntity<List<Note_tagDTO>> fetchDefault(Note_tagSearchContext context) {
        Page<Note_tag> domains = note_tagService.searchDefault(context) ;
        List<Note_tagDTO> list = note_tagMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Note_tag" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/note_tags/searchdefault")
	public ResponseEntity<Page<Note_tagDTO>> searchDefault(Note_tagSearchContext context) {
        Page<Note_tag> domains = note_tagService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(note_tagMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Note_tag getEntity(){
        return new Note_tag();
    }

}
