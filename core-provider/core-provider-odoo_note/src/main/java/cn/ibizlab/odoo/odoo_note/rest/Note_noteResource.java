package cn.ibizlab.odoo.odoo_note.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_note.dto.*;
import cn.ibizlab.odoo.odoo_note.mapping.*;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_note;
import cn.ibizlab.odoo.core.odoo_note.service.INote_noteService;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_noteSearchContext;




@Slf4j
@Api(tags = {"Note_note" })
@RestController("odoo_note-note_note")
@RequestMapping("")
public class Note_noteResource {

    @Autowired
    private INote_noteService note_noteService;

    @Autowired
    @Lazy
    private Note_noteMapping note_noteMapping;







    @PreAuthorize("hasPermission(#note_note_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Note_note" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/note_notes/{note_note_id}")
    public ResponseEntity<Note_noteDTO> get(@PathVariable("note_note_id") Integer note_note_id) {
        Note_note domain = note_noteService.get(note_note_id);
        Note_noteDTO dto = note_noteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#note_note_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Note_note" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_notes/{note_note_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("note_note_id") Integer note_note_id) {
         return ResponseEntity.status(HttpStatus.OK).body(note_noteService.remove(note_note_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Note_note" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/note_notes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        note_noteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Note_note" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/note_notes")

    public ResponseEntity<Note_noteDTO> create(@RequestBody Note_noteDTO note_notedto) {
        Note_note domain = note_noteMapping.toDomain(note_notedto);
		note_noteService.create(domain);
        Note_noteDTO dto = note_noteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Note_note" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/note_notes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Note_noteDTO> note_notedtos) {
        note_noteService.createBatch(note_noteMapping.toDomain(note_notedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#note_note_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Note_note" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_notes/{note_note_id}")

    public ResponseEntity<Note_noteDTO> update(@PathVariable("note_note_id") Integer note_note_id, @RequestBody Note_noteDTO note_notedto) {
		Note_note domain = note_noteMapping.toDomain(note_notedto);
        domain.setId(note_note_id);
		note_noteService.update(domain);
		Note_noteDTO dto = note_noteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#note_note_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Note_note" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/note_notes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Note_noteDTO> note_notedtos) {
        note_noteService.updateBatch(note_noteMapping.toDomain(note_notedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Note_note" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/note_notes/fetchdefault")
	public ResponseEntity<List<Note_noteDTO>> fetchDefault(Note_noteSearchContext context) {
        Page<Note_note> domains = note_noteService.searchDefault(context) ;
        List<Note_noteDTO> list = note_noteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Note_note" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/note_notes/searchdefault")
	public ResponseEntity<Page<Note_noteDTO>> searchDefault(Note_noteSearchContext context) {
        Page<Note_note> domains = note_noteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(note_noteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Note_note getEntity(){
        return new Note_note();
    }

}
