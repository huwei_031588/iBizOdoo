package cn.ibizlab.odoo.odoo_sale.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template;
import cn.ibizlab.odoo.odoo_sale.dto.Sale_order_templateDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Sale_order_templateMapping extends MappingBase<Sale_order_templateDTO, Sale_order_template> {


}

