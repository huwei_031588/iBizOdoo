package cn.ibizlab.odoo.odoo_sale.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.odoo_sale.dto.Sale_payment_acquirer_onboarding_wizardDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Sale_payment_acquirer_onboarding_wizardMapping extends MappingBase<Sale_payment_acquirer_onboarding_wizardDTO, Sale_payment_acquirer_onboarding_wizard> {


}

