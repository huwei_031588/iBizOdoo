package cn.ibizlab.odoo.odoo_sale.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-sale")
@Data
public class odoo_saleServiceProperties {

	private boolean enabled;

	private boolean auth;


}