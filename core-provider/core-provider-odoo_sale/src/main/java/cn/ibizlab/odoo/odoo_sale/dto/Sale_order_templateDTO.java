package cn.ibizlab.odoo.odoo_sale.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Sale_order_templateDTO]
 */
@Data
public class Sale_order_templateDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [REQUIRE_SIGNATURE]
     *
     */
    @JSONField(name = "require_signature")
    @JsonProperty("require_signature")
    private String requireSignature;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [REQUIRE_PAYMENT]
     *
     */
    @JSONField(name = "require_payment")
    @JsonProperty("require_payment")
    private String requirePayment;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [NUMBER_OF_DAYS]
     *
     */
    @JSONField(name = "number_of_days")
    @JsonProperty("number_of_days")
    private Integer numberOfDays;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_OPTION_IDS]
     *
     */
    @JSONField(name = "sale_order_template_option_ids")
    @JsonProperty("sale_order_template_option_ids")
    private String saleOrderTemplateOptionIds;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_LINE_IDS]
     *
     */
    @JSONField(name = "sale_order_template_line_ids")
    @JsonProperty("sale_order_template_line_ids")
    private String saleOrderTemplateLineIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [MAIL_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "mail_template_id_text")
    @JsonProperty("mail_template_id_text")
    private String mailTemplateIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [MAIL_TEMPLATE_ID]
     *
     */
    @JSONField(name = "mail_template_id")
    @JsonProperty("mail_template_id")
    private Integer mailTemplateId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [REQUIRE_SIGNATURE]
     */
    public void setRequireSignature(String  requireSignature){
        this.requireSignature = requireSignature ;
        this.modify("require_signature",requireSignature);
    }

    /**
     * 设置 [REQUIRE_PAYMENT]
     */
    public void setRequirePayment(String  requirePayment){
        this.requirePayment = requirePayment ;
        this.modify("require_payment",requirePayment);
    }

    /**
     * 设置 [NUMBER_OF_DAYS]
     */
    public void setNumberOfDays(Integer  numberOfDays){
        this.numberOfDays = numberOfDays ;
        this.modify("number_of_days",numberOfDays);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [MAIL_TEMPLATE_ID]
     */
    public void setMailTemplateId(Integer  mailTemplateId){
        this.mailTemplateId = mailTemplateId ;
        this.modify("mail_template_id",mailTemplateId);
    }


}

