package cn.ibizlab.odoo.odoo_sale.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Sale_payment_acquirer_onboarding_wizardDTO]
 */
@Data
public class Sale_payment_acquirer_onboarding_wizardDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [MANUAL_NAME]
     *
     */
    @JSONField(name = "manual_name")
    @JsonProperty("manual_name")
    private String manualName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MANUAL_POST_MSG]
     *
     */
    @JSONField(name = "manual_post_msg")
    @JsonProperty("manual_post_msg")
    private String manualPostMsg;

    /**
     * 属性 [ACC_NUMBER]
     *
     */
    @JSONField(name = "acc_number")
    @JsonProperty("acc_number")
    private String accNumber;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [JOURNAL_NAME]
     *
     */
    @JSONField(name = "journal_name")
    @JsonProperty("journal_name")
    private String journalName;

    /**
     * 属性 [PAYPAL_PDT_TOKEN]
     *
     */
    @JSONField(name = "paypal_pdt_token")
    @JsonProperty("paypal_pdt_token")
    private String paypalPdtToken;

    /**
     * 属性 [STRIPE_SECRET_KEY]
     *
     */
    @JSONField(name = "stripe_secret_key")
    @JsonProperty("stripe_secret_key")
    private String stripeSecretKey;

    /**
     * 属性 [STRIPE_PUBLISHABLE_KEY]
     *
     */
    @JSONField(name = "stripe_publishable_key")
    @JsonProperty("stripe_publishable_key")
    private String stripePublishableKey;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [PAYMENT_METHOD]
     *
     */
    @JSONField(name = "payment_method")
    @JsonProperty("payment_method")
    private String paymentMethod;

    /**
     * 属性 [PAYPAL_EMAIL_ACCOUNT]
     *
     */
    @JSONField(name = "paypal_email_account")
    @JsonProperty("paypal_email_account")
    private String paypalEmailAccount;

    /**
     * 属性 [PAYPAL_SELLER_ACCOUNT]
     *
     */
    @JSONField(name = "paypal_seller_account")
    @JsonProperty("paypal_seller_account")
    private String paypalSellerAccount;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [MANUAL_NAME]
     */
    public void setManualName(String  manualName){
        this.manualName = manualName ;
        this.modify("manual_name",manualName);
    }

    /**
     * 设置 [MANUAL_POST_MSG]
     */
    public void setManualPostMsg(String  manualPostMsg){
        this.manualPostMsg = manualPostMsg ;
        this.modify("manual_post_msg",manualPostMsg);
    }

    /**
     * 设置 [ACC_NUMBER]
     */
    public void setAccNumber(String  accNumber){
        this.accNumber = accNumber ;
        this.modify("acc_number",accNumber);
    }

    /**
     * 设置 [JOURNAL_NAME]
     */
    public void setJournalName(String  journalName){
        this.journalName = journalName ;
        this.modify("journal_name",journalName);
    }

    /**
     * 设置 [PAYPAL_PDT_TOKEN]
     */
    public void setPaypalPdtToken(String  paypalPdtToken){
        this.paypalPdtToken = paypalPdtToken ;
        this.modify("paypal_pdt_token",paypalPdtToken);
    }

    /**
     * 设置 [STRIPE_SECRET_KEY]
     */
    public void setStripeSecretKey(String  stripeSecretKey){
        this.stripeSecretKey = stripeSecretKey ;
        this.modify("stripe_secret_key",stripeSecretKey);
    }

    /**
     * 设置 [STRIPE_PUBLISHABLE_KEY]
     */
    public void setStripePublishableKey(String  stripePublishableKey){
        this.stripePublishableKey = stripePublishableKey ;
        this.modify("stripe_publishable_key",stripePublishableKey);
    }

    /**
     * 设置 [PAYMENT_METHOD]
     */
    public void setPaymentMethod(String  paymentMethod){
        this.paymentMethod = paymentMethod ;
        this.modify("payment_method",paymentMethod);
    }

    /**
     * 设置 [PAYPAL_EMAIL_ACCOUNT]
     */
    public void setPaypalEmailAccount(String  paypalEmailAccount){
        this.paypalEmailAccount = paypalEmailAccount ;
        this.modify("paypal_email_account",paypalEmailAccount);
    }

    /**
     * 设置 [PAYPAL_SELLER_ACCOUNT]
     */
    public void setPaypalSellerAccount(String  paypalSellerAccount){
        this.paypalSellerAccount = paypalSellerAccount ;
        this.modify("paypal_seller_account",paypalSellerAccount);
    }


}

