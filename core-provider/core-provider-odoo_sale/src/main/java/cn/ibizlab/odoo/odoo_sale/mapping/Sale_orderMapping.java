package cn.ibizlab.odoo.odoo_sale.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order;
import cn.ibizlab.odoo.odoo_sale.dto.Sale_orderDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Sale_orderMapping extends MappingBase<Sale_orderDTO, Sale_order> {


}

