package cn.ibizlab.odoo.odoo_purchase.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.odoo.odoo_purchase.dto.Purchase_order_lineDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Purchase_order_lineMapping extends MappingBase<Purchase_order_lineDTO, Purchase_order_line> {


}

