package cn.ibizlab.odoo.odoo_purchase.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_purchase.dto.*;
import cn.ibizlab.odoo.odoo_purchase.mapping.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_orderService;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_orderSearchContext;




@Slf4j
@Api(tags = {"Purchase_order" })
@RestController("odoo_purchase-purchase_order")
@RequestMapping("")
public class Purchase_orderResource {

    @Autowired
    private IPurchase_orderService purchase_orderService;

    @Autowired
    @Lazy
    private Purchase_orderMapping purchase_orderMapping;




    @PreAuthorize("hasPermission(#purchase_order_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Purchase_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Purchase_orderDTO> get(@PathVariable("purchase_order_id") Integer purchase_order_id) {
        Purchase_order domain = purchase_orderService.get(purchase_order_id);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('Remove',{#purchase_order_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Purchase_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_orders/{purchase_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("purchase_order_id") Integer purchase_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_orderService.remove(purchase_order_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Purchase_order" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        purchase_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#purchase_order_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Purchase_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_orders/{purchase_order_id}")

    public ResponseEntity<Purchase_orderDTO> update(@PathVariable("purchase_order_id") Integer purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
		Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
        domain.setId(purchase_order_id);
		purchase_orderService.update(domain);
		Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#purchase_order_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Purchase_order" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        purchase_orderService.updateBatch(purchase_orderMapping.toDomain(purchase_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Purchase_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders")

    public ResponseEntity<Purchase_orderDTO> create(@RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_order domain = purchase_orderMapping.toDomain(purchase_orderdto);
		purchase_orderService.create(domain);
        Purchase_orderDTO dto = purchase_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Purchase_order" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        purchase_orderService.createBatch(purchase_orderMapping.toDomain(purchase_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Purchase_order" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_orders/fetchdefault")
	public ResponseEntity<List<Purchase_orderDTO>> fetchDefault(Purchase_orderSearchContext context) {
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
        List<Purchase_orderDTO> list = purchase_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Purchase_order" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_orders/searchdefault")
	public ResponseEntity<Page<Purchase_orderDTO>> searchDefault(Purchase_orderSearchContext context) {
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Purchase_order getEntity(){
        return new Purchase_order();
    }

}
