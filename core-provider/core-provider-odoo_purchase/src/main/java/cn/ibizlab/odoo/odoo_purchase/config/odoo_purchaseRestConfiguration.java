package cn.ibizlab.odoo.odoo_purchase.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_purchase")
public class odoo_purchaseRestConfiguration {

}
