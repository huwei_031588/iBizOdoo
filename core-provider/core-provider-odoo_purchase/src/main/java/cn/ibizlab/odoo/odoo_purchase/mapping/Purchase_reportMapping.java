package cn.ibizlab.odoo.odoo_purchase.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.odoo.odoo_purchase.dto.Purchase_reportDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Purchase_reportMapping extends MappingBase<Purchase_reportDTO, Purchase_report> {


}

