package cn.ibizlab.odoo.odoo_purchase.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_purchase.dto.*;
import cn.ibizlab.odoo.odoo_purchase.mapping.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_order_lineService;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_order_lineSearchContext;




@Slf4j
@Api(tags = {"Purchase_order_line" })
@RestController("odoo_purchase-purchase_order_line")
@RequestMapping("")
public class Purchase_order_lineResource {

    @Autowired
    private IPurchase_order_lineService purchase_order_lineService;

    @Autowired
    @Lazy
    private Purchase_order_lineMapping purchase_order_lineMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Purchase_order_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines")

    public ResponseEntity<Purchase_order_lineDTO> create(@RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
		purchase_order_lineService.create(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Purchase_order_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        purchase_order_lineService.createBatch(purchase_order_lineMapping.toDomain(purchase_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#purchase_order_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Purchase_order_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_order_lines/{purchase_order_line_id}")

    public ResponseEntity<Purchase_order_lineDTO> update(@PathVariable("purchase_order_line_id") Integer purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
		Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setId(purchase_order_line_id);
		purchase_order_lineService.update(domain);
		Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#purchase_order_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Purchase_order_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_order_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        purchase_order_lineService.updateBatch(purchase_order_lineMapping.toDomain(purchase_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#purchase_order_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Purchase_order_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> get(@PathVariable("purchase_order_line_id") Integer purchase_order_line_id) {
        Purchase_order_line domain = purchase_order_lineService.get(purchase_order_line_id);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#purchase_order_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Purchase_order_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_order_lines/{purchase_order_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("purchase_order_line_id") Integer purchase_order_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.remove(purchase_order_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Purchase_order_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_order_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        purchase_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Purchase_order_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_order_lines/fetchdefault")
	public ResponseEntity<List<Purchase_order_lineDTO>> fetchDefault(Purchase_order_lineSearchContext context) {
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
        List<Purchase_order_lineDTO> list = purchase_order_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Purchase_order_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_order_lines/searchdefault")
	public ResponseEntity<Page<Purchase_order_lineDTO>> searchDefault(Purchase_order_lineSearchContext context) {
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Purchase_order_line getEntity(){
        return new Purchase_order_line();
    }

}
