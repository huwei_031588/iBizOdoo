package cn.ibizlab.odoo.odoo_asset.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Asset_assetDTO]
 */
@Data
public class Asset_assetDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [POSITION]
     *
     */
    @JSONField(name = "position")
    @JsonProperty("position")
    private String position;

    /**
     * 属性 [METER_IDS]
     *
     */
    @JSONField(name = "meter_ids")
    @JsonProperty("meter_ids")
    private String meterIds;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [START_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * 属性 [MAINTENANCE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "maintenance_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("maintenance_date")
    private Timestamp maintenanceDate;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [PROPERTY_STOCK_ASSET]
     *
     */
    @JSONField(name = "property_stock_asset")
    @JsonProperty("property_stock_asset")
    private Integer propertyStockAsset;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [MODEL]
     *
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 属性 [ASSET_NUMBER]
     *
     */
    @JSONField(name = "asset_number")
    @JsonProperty("asset_number")
    private String assetNumber;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [SERIAL]
     *
     */
    @JSONField(name = "serial")
    @JsonProperty("serial")
    private String serial;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [PURCHASE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "purchase_date" , format="yyyy-MM-dd")
    @JsonProperty("purchase_date")
    private Timestamp purchaseDate;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CATEGORY_IDS]
     *
     */
    @JSONField(name = "category_ids")
    @JsonProperty("category_ids")
    private String categoryIds;

    /**
     * 属性 [WARRANTY_START_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "warranty_start_date" , format="yyyy-MM-dd")
    @JsonProperty("warranty_start_date")
    private Timestamp warrantyStartDate;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WARRANTY_END_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "warranty_end_date" , format="yyyy-MM-dd")
    @JsonProperty("warranty_end_date")
    private Timestamp warrantyEndDate;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [CRITICALITY]
     *
     */
    @JSONField(name = "criticality")
    @JsonProperty("criticality")
    private String criticality;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MRO_COUNT]
     *
     */
    @JSONField(name = "mro_count")
    @JsonProperty("mro_count")
    private Integer mroCount;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [WAREHOUSE_STATE_ID_TEXT]
     *
     */
    @JSONField(name = "warehouse_state_id_text")
    @JsonProperty("warehouse_state_id_text")
    private String warehouseStateIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [MAINTENANCE_STATE_COLOR]
     *
     */
    @JSONField(name = "maintenance_state_color")
    @JsonProperty("maintenance_state_color")
    private String maintenanceStateColor;

    /**
     * 属性 [ACCOUNTING_STATE_ID_TEXT]
     *
     */
    @JSONField(name = "accounting_state_id_text")
    @JsonProperty("accounting_state_id_text")
    private String accountingStateIdText;

    /**
     * 属性 [MANUFACTURE_STATE_ID_TEXT]
     *
     */
    @JSONField(name = "manufacture_state_id_text")
    @JsonProperty("manufacture_state_id_text")
    private String manufactureStateIdText;

    /**
     * 属性 [FINANCE_STATE_ID_TEXT]
     *
     */
    @JSONField(name = "finance_state_id_text")
    @JsonProperty("finance_state_id_text")
    private String financeStateIdText;

    /**
     * 属性 [MAINTENANCE_STATE_ID_TEXT]
     *
     */
    @JSONField(name = "maintenance_state_id_text")
    @JsonProperty("maintenance_state_id_text")
    private String maintenanceStateIdText;

    /**
     * 属性 [MANUFACTURER_ID_TEXT]
     *
     */
    @JSONField(name = "manufacturer_id_text")
    @JsonProperty("manufacturer_id_text")
    private String manufacturerIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [VENDOR_ID_TEXT]
     *
     */
    @JSONField(name = "vendor_id_text")
    @JsonProperty("vendor_id_text")
    private String vendorIdText;

    /**
     * 属性 [MANUFACTURE_STATE_ID]
     *
     */
    @JSONField(name = "manufacture_state_id")
    @JsonProperty("manufacture_state_id")
    private Integer manufactureStateId;

    /**
     * 属性 [ACCOUNTING_STATE_ID]
     *
     */
    @JSONField(name = "accounting_state_id")
    @JsonProperty("accounting_state_id")
    private Integer accountingStateId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [VENDOR_ID]
     *
     */
    @JSONField(name = "vendor_id")
    @JsonProperty("vendor_id")
    private Integer vendorId;

    /**
     * 属性 [WAREHOUSE_STATE_ID]
     *
     */
    @JSONField(name = "warehouse_state_id")
    @JsonProperty("warehouse_state_id")
    private Integer warehouseStateId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 属性 [FINANCE_STATE_ID]
     *
     */
    @JSONField(name = "finance_state_id")
    @JsonProperty("finance_state_id")
    private Integer financeStateId;

    /**
     * 属性 [MAINTENANCE_STATE_ID]
     *
     */
    @JSONField(name = "maintenance_state_id")
    @JsonProperty("maintenance_state_id")
    private Integer maintenanceStateId;

    /**
     * 属性 [MANUFACTURER_ID]
     *
     */
    @JSONField(name = "manufacturer_id")
    @JsonProperty("manufacturer_id")
    private Integer manufacturerId;


    /**
     * 设置 [POSITION]
     */
    public void setPosition(String  position){
        this.position = position ;
        this.modify("position",position);
    }

    /**
     * 设置 [START_DATE]
     */
    public void setStartDate(Timestamp  startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 设置 [MODEL]
     */
    public void setModel(String  model){
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 设置 [ASSET_NUMBER]
     */
    public void setAssetNumber(String  assetNumber){
        this.assetNumber = assetNumber ;
        this.modify("asset_number",assetNumber);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [SERIAL]
     */
    public void setSerial(String  serial){
        this.serial = serial ;
        this.modify("serial",serial);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [PURCHASE_DATE]
     */
    public void setPurchaseDate(Timestamp  purchaseDate){
        this.purchaseDate = purchaseDate ;
        this.modify("purchase_date",purchaseDate);
    }

    /**
     * 设置 [WARRANTY_START_DATE]
     */
    public void setWarrantyStartDate(Timestamp  warrantyStartDate){
        this.warrantyStartDate = warrantyStartDate ;
        this.modify("warranty_start_date",warrantyStartDate);
    }

    /**
     * 设置 [WARRANTY_END_DATE]
     */
    public void setWarrantyEndDate(Timestamp  warrantyEndDate){
        this.warrantyEndDate = warrantyEndDate ;
        this.modify("warranty_end_date",warrantyEndDate);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [CRITICALITY]
     */
    public void setCriticality(String  criticality){
        this.criticality = criticality ;
        this.modify("criticality",criticality);
    }

    /**
     * 设置 [MANUFACTURE_STATE_ID]
     */
    public void setManufactureStateId(Integer  manufactureStateId){
        this.manufactureStateId = manufactureStateId ;
        this.modify("manufacture_state_id",manufactureStateId);
    }

    /**
     * 设置 [ACCOUNTING_STATE_ID]
     */
    public void setAccountingStateId(Integer  accountingStateId){
        this.accountingStateId = accountingStateId ;
        this.modify("accounting_state_id",accountingStateId);
    }

    /**
     * 设置 [VENDOR_ID]
     */
    public void setVendorId(Integer  vendorId){
        this.vendorId = vendorId ;
        this.modify("vendor_id",vendorId);
    }

    /**
     * 设置 [WAREHOUSE_STATE_ID]
     */
    public void setWarehouseStateId(Integer  warehouseStateId){
        this.warehouseStateId = warehouseStateId ;
        this.modify("warehouse_state_id",warehouseStateId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Integer  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [FINANCE_STATE_ID]
     */
    public void setFinanceStateId(Integer  financeStateId){
        this.financeStateId = financeStateId ;
        this.modify("finance_state_id",financeStateId);
    }

    /**
     * 设置 [MAINTENANCE_STATE_ID]
     */
    public void setMaintenanceStateId(Integer  maintenanceStateId){
        this.maintenanceStateId = maintenanceStateId ;
        this.modify("maintenance_state_id",maintenanceStateId);
    }

    /**
     * 设置 [MANUFACTURER_ID]
     */
    public void setManufacturerId(Integer  manufacturerId){
        this.manufacturerId = manufacturerId ;
        this.modify("manufacturer_id",manufacturerId);
    }


}

