package cn.ibizlab.odoo.odoo_asset.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-asset")
@Data
public class odoo_assetServiceProperties {

	private boolean enabled;

	private boolean auth;


}