package cn.ibizlab.odoo.odoo_asset.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_asset.dto.*;
import cn.ibizlab.odoo.odoo_asset.mapping.*;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_assetService;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_assetSearchContext;




@Slf4j
@Api(tags = {"Asset_asset" })
@RestController("odoo_asset-asset_asset")
@RequestMapping("")
public class Asset_assetResource {

    @Autowired
    private IAsset_assetService asset_assetService;

    @Autowired
    @Lazy
    private Asset_assetMapping asset_assetMapping;




    @PreAuthorize("hasPermission(#asset_asset_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Asset_asset" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_assets/{asset_asset_id}")

    public ResponseEntity<Asset_assetDTO> update(@PathVariable("asset_asset_id") Integer asset_asset_id, @RequestBody Asset_assetDTO asset_assetdto) {
		Asset_asset domain = asset_assetMapping.toDomain(asset_assetdto);
        domain.setId(asset_asset_id);
		asset_assetService.update(domain);
		Asset_assetDTO dto = asset_assetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#asset_asset_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Asset_asset" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_assets/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Asset_assetDTO> asset_assetdtos) {
        asset_assetService.updateBatch(asset_assetMapping.toDomain(asset_assetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#asset_asset_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Asset_asset" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/asset_assets/{asset_asset_id}")
    public ResponseEntity<Asset_assetDTO> get(@PathVariable("asset_asset_id") Integer asset_asset_id) {
        Asset_asset domain = asset_assetService.get(asset_asset_id);
        Asset_assetDTO dto = asset_assetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#asset_asset_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Asset_asset" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_assets/{asset_asset_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("asset_asset_id") Integer asset_asset_id) {
         return ResponseEntity.status(HttpStatus.OK).body(asset_assetService.remove(asset_asset_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Asset_asset" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_assets/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        asset_assetService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Asset_asset" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_assets")

    public ResponseEntity<Asset_assetDTO> create(@RequestBody Asset_assetDTO asset_assetdto) {
        Asset_asset domain = asset_assetMapping.toDomain(asset_assetdto);
		asset_assetService.create(domain);
        Asset_assetDTO dto = asset_assetMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Asset_asset" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_assets/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Asset_assetDTO> asset_assetdtos) {
        asset_assetService.createBatch(asset_assetMapping.toDomain(asset_assetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Asset_asset" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/asset_assets/fetchdefault")
	public ResponseEntity<List<Asset_assetDTO>> fetchDefault(Asset_assetSearchContext context) {
        Page<Asset_asset> domains = asset_assetService.searchDefault(context) ;
        List<Asset_assetDTO> list = asset_assetMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Asset_asset" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/asset_assets/searchdefault")
	public ResponseEntity<Page<Asset_assetDTO>> searchDefault(Asset_assetSearchContext context) {
        Page<Asset_asset> domains = asset_assetService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(asset_assetMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Asset_asset getEntity(){
        return new Asset_asset();
    }

}
