package cn.ibizlab.odoo.odoo_base_import.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base_import.dto.*;
import cn.ibizlab.odoo.odoo_base_import.mapping.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o_related;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_m2o_relatedService;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2o_relatedSearchContext;




@Slf4j
@Api(tags = {"Base_import_tests_models_m2o_related" })
@RestController("odoo_base_import-base_import_tests_models_m2o_related")
@RequestMapping("")
public class Base_import_tests_models_m2o_relatedResource {

    @Autowired
    private IBase_import_tests_models_m2o_relatedService base_import_tests_models_m2o_relatedService;

    @Autowired
    @Lazy
    private Base_import_tests_models_m2o_relatedMapping base_import_tests_models_m2o_relatedMapping;




    @PreAuthorize("hasPermission(#base_import_tests_models_m2o_related_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2o_relateds/{base_import_tests_models_m2o_related_id}")

    public ResponseEntity<Base_import_tests_models_m2o_relatedDTO> update(@PathVariable("base_import_tests_models_m2o_related_id") Integer base_import_tests_models_m2o_related_id, @RequestBody Base_import_tests_models_m2o_relatedDTO base_import_tests_models_m2o_relateddto) {
		Base_import_tests_models_m2o_related domain = base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddto);
        domain.setId(base_import_tests_models_m2o_related_id);
		base_import_tests_models_m2o_relatedService.update(domain);
		Base_import_tests_models_m2o_relatedDTO dto = base_import_tests_models_m2o_relatedMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_import_tests_models_m2o_related_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_import_tests_models_m2o_related" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_m2o_relateds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_m2o_relatedDTO> base_import_tests_models_m2o_relateddtos) {
        base_import_tests_models_m2o_relatedService.updateBatch(base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#base_import_tests_models_m2o_related_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_m2o_relateds/{base_import_tests_models_m2o_related_id}")
    public ResponseEntity<Base_import_tests_models_m2o_relatedDTO> get(@PathVariable("base_import_tests_models_m2o_related_id") Integer base_import_tests_models_m2o_related_id) {
        Base_import_tests_models_m2o_related domain = base_import_tests_models_m2o_relatedService.get(base_import_tests_models_m2o_related_id);
        Base_import_tests_models_m2o_relatedDTO dto = base_import_tests_models_m2o_relatedMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_relateds")

    public ResponseEntity<Base_import_tests_models_m2o_relatedDTO> create(@RequestBody Base_import_tests_models_m2o_relatedDTO base_import_tests_models_m2o_relateddto) {
        Base_import_tests_models_m2o_related domain = base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddto);
		base_import_tests_models_m2o_relatedService.create(domain);
        Base_import_tests_models_m2o_relatedDTO dto = base_import_tests_models_m2o_relatedMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_import_tests_models_m2o_related" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_m2o_relateds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_m2o_relatedDTO> base_import_tests_models_m2o_relateddtos) {
        base_import_tests_models_m2o_relatedService.createBatch(base_import_tests_models_m2o_relatedMapping.toDomain(base_import_tests_models_m2o_relateddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#base_import_tests_models_m2o_related_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2o_relateds/{base_import_tests_models_m2o_related_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_m2o_related_id") Integer base_import_tests_models_m2o_related_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_m2o_relatedService.remove(base_import_tests_models_m2o_related_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_import_tests_models_m2o_related" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_m2o_relateds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_import_tests_models_m2o_relatedService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_import_tests_models_m2o_related" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_m2o_relateds/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_m2o_relatedDTO>> fetchDefault(Base_import_tests_models_m2o_relatedSearchContext context) {
        Page<Base_import_tests_models_m2o_related> domains = base_import_tests_models_m2o_relatedService.searchDefault(context) ;
        List<Base_import_tests_models_m2o_relatedDTO> list = base_import_tests_models_m2o_relatedMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_import_tests_models_m2o_related" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_m2o_relateds/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_m2o_relatedDTO>> searchDefault(Base_import_tests_models_m2o_relatedSearchContext context) {
        Page<Base_import_tests_models_m2o_related> domains = base_import_tests_models_m2o_relatedService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_m2o_relatedMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_import_tests_models_m2o_related getEntity(){
        return new Base_import_tests_models_m2o_related();
    }

}
