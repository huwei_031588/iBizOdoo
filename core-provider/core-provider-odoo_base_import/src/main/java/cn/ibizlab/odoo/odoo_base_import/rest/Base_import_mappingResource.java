package cn.ibizlab.odoo.odoo_base_import.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base_import.dto.*;
import cn.ibizlab.odoo.odoo_base_import.mapping.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_mapping;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_mappingService;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_mappingSearchContext;




@Slf4j
@Api(tags = {"Base_import_mapping" })
@RestController("odoo_base_import-base_import_mapping")
@RequestMapping("")
public class Base_import_mappingResource {

    @Autowired
    private IBase_import_mappingService base_import_mappingService;

    @Autowired
    @Lazy
    private Base_import_mappingMapping base_import_mappingMapping;







    @PreAuthorize("hasPermission(#base_import_mapping_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_import_mapping" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_mappings/{base_import_mapping_id}")

    public ResponseEntity<Base_import_mappingDTO> update(@PathVariable("base_import_mapping_id") Integer base_import_mapping_id, @RequestBody Base_import_mappingDTO base_import_mappingdto) {
		Base_import_mapping domain = base_import_mappingMapping.toDomain(base_import_mappingdto);
        domain.setId(base_import_mapping_id);
		base_import_mappingService.update(domain);
		Base_import_mappingDTO dto = base_import_mappingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_import_mapping_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_import_mapping" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_mappings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_mappingDTO> base_import_mappingdtos) {
        base_import_mappingService.updateBatch(base_import_mappingMapping.toDomain(base_import_mappingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#base_import_mapping_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_import_mapping" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_mappings/{base_import_mapping_id}")
    public ResponseEntity<Base_import_mappingDTO> get(@PathVariable("base_import_mapping_id") Integer base_import_mapping_id) {
        Base_import_mapping domain = base_import_mappingService.get(base_import_mapping_id);
        Base_import_mappingDTO dto = base_import_mappingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_import_mapping" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings")

    public ResponseEntity<Base_import_mappingDTO> create(@RequestBody Base_import_mappingDTO base_import_mappingdto) {
        Base_import_mapping domain = base_import_mappingMapping.toDomain(base_import_mappingdto);
		base_import_mappingService.create(domain);
        Base_import_mappingDTO dto = base_import_mappingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_import_mapping" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_mappingDTO> base_import_mappingdtos) {
        base_import_mappingService.createBatch(base_import_mappingMapping.toDomain(base_import_mappingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#base_import_mapping_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_import_mapping" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_mappings/{base_import_mapping_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_mapping_id") Integer base_import_mapping_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_mappingService.remove(base_import_mapping_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_import_mapping" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_mappings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_import_mappingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_import_mapping" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_mappings/fetchdefault")
	public ResponseEntity<List<Base_import_mappingDTO>> fetchDefault(Base_import_mappingSearchContext context) {
        Page<Base_import_mapping> domains = base_import_mappingService.searchDefault(context) ;
        List<Base_import_mappingDTO> list = base_import_mappingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_import_mapping" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_mappings/searchdefault")
	public ResponseEntity<Page<Base_import_mappingDTO>> searchDefault(Base_import_mappingSearchContext context) {
        Page<Base_import_mapping> domains = base_import_mappingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_mappingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_import_mapping getEntity(){
        return new Base_import_mapping();
    }

}
