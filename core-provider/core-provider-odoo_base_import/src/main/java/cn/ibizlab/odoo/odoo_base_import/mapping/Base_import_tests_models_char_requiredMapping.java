package cn.ibizlab.odoo.odoo_base_import.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_required;
import cn.ibizlab.odoo.odoo_base_import.dto.Base_import_tests_models_char_requiredDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Base_import_tests_models_char_requiredMapping extends MappingBase<Base_import_tests_models_char_requiredDTO, Base_import_tests_models_char_required> {


}

