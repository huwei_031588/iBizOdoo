package cn.ibizlab.odoo.odoo_base_import.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base_import.dto.*;
import cn.ibizlab.odoo.odoo_base_import.mapping.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_required;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_char_requiredService;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_requiredSearchContext;




@Slf4j
@Api(tags = {"Base_import_tests_models_char_required" })
@RestController("odoo_base_import-base_import_tests_models_char_required")
@RequestMapping("")
public class Base_import_tests_models_char_requiredResource {

    @Autowired
    private IBase_import_tests_models_char_requiredService base_import_tests_models_char_requiredService;

    @Autowired
    @Lazy
    private Base_import_tests_models_char_requiredMapping base_import_tests_models_char_requiredMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_char_required" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_requireds")

    public ResponseEntity<Base_import_tests_models_char_requiredDTO> create(@RequestBody Base_import_tests_models_char_requiredDTO base_import_tests_models_char_requireddto) {
        Base_import_tests_models_char_required domain = base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddto);
		base_import_tests_models_char_requiredService.create(domain);
        Base_import_tests_models_char_requiredDTO dto = base_import_tests_models_char_requiredMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_import_tests_models_char_required" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_requireds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_char_requiredDTO> base_import_tests_models_char_requireddtos) {
        base_import_tests_models_char_requiredService.createBatch(base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#base_import_tests_models_char_required_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_char_required" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_requireds/{base_import_tests_models_char_required_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_required_id") Integer base_import_tests_models_char_required_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_requiredService.remove(base_import_tests_models_char_required_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_import_tests_models_char_required" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_requireds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_import_tests_models_char_requiredService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission(#base_import_tests_models_char_required_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_char_required" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_requireds/{base_import_tests_models_char_required_id}")
    public ResponseEntity<Base_import_tests_models_char_requiredDTO> get(@PathVariable("base_import_tests_models_char_required_id") Integer base_import_tests_models_char_required_id) {
        Base_import_tests_models_char_required domain = base_import_tests_models_char_requiredService.get(base_import_tests_models_char_required_id);
        Base_import_tests_models_char_requiredDTO dto = base_import_tests_models_char_requiredMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#base_import_tests_models_char_required_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_char_required" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_requireds/{base_import_tests_models_char_required_id}")

    public ResponseEntity<Base_import_tests_models_char_requiredDTO> update(@PathVariable("base_import_tests_models_char_required_id") Integer base_import_tests_models_char_required_id, @RequestBody Base_import_tests_models_char_requiredDTO base_import_tests_models_char_requireddto) {
		Base_import_tests_models_char_required domain = base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddto);
        domain.setId(base_import_tests_models_char_required_id);
		base_import_tests_models_char_requiredService.update(domain);
		Base_import_tests_models_char_requiredDTO dto = base_import_tests_models_char_requiredMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_import_tests_models_char_required_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_import_tests_models_char_required" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_requireds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_char_requiredDTO> base_import_tests_models_char_requireddtos) {
        base_import_tests_models_char_requiredService.updateBatch(base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_import_tests_models_char_required" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_char_requireds/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_char_requiredDTO>> fetchDefault(Base_import_tests_models_char_requiredSearchContext context) {
        Page<Base_import_tests_models_char_required> domains = base_import_tests_models_char_requiredService.searchDefault(context) ;
        List<Base_import_tests_models_char_requiredDTO> list = base_import_tests_models_char_requiredMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_import_tests_models_char_required" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_char_requireds/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_char_requiredDTO>> searchDefault(Base_import_tests_models_char_requiredSearchContext context) {
        Page<Base_import_tests_models_char_required> domains = base_import_tests_models_char_requiredService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_char_requiredMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_import_tests_models_char_required getEntity(){
        return new Base_import_tests_models_char_required();
    }

}
