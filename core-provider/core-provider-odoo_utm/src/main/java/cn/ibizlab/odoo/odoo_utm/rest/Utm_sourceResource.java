package cn.ibizlab.odoo.odoo_utm.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_utm.dto.*;
import cn.ibizlab.odoo.odoo_utm.mapping.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_sourceService;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_sourceSearchContext;




@Slf4j
@Api(tags = {"Utm_source" })
@RestController("odoo_utm-utm_source")
@RequestMapping("")
public class Utm_sourceResource {

    @Autowired
    private IUtm_sourceService utm_sourceService;

    @Autowired
    @Lazy
    private Utm_sourceMapping utm_sourceMapping;













    @PreAuthorize("hasPermission(#utm_source_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Utm_source" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_sources/{utm_source_id}")
    public ResponseEntity<Utm_sourceDTO> get(@PathVariable("utm_source_id") Integer utm_source_id) {
        Utm_source domain = utm_sourceService.get(utm_source_id);
        Utm_sourceDTO dto = utm_sourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Utm_source" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_sources")

    public ResponseEntity<Utm_sourceDTO> create(@RequestBody Utm_sourceDTO utm_sourcedto) {
        Utm_source domain = utm_sourceMapping.toDomain(utm_sourcedto);
		utm_sourceService.create(domain);
        Utm_sourceDTO dto = utm_sourceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Utm_source" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_sources/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Utm_sourceDTO> utm_sourcedtos) {
        utm_sourceService.createBatch(utm_sourceMapping.toDomain(utm_sourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#utm_source_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Utm_source" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_sources/{utm_source_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("utm_source_id") Integer utm_source_id) {
         return ResponseEntity.status(HttpStatus.OK).body(utm_sourceService.remove(utm_source_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Utm_source" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_sources/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        utm_sourceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#utm_source_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Utm_source" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_sources/{utm_source_id}")

    public ResponseEntity<Utm_sourceDTO> update(@PathVariable("utm_source_id") Integer utm_source_id, @RequestBody Utm_sourceDTO utm_sourcedto) {
		Utm_source domain = utm_sourceMapping.toDomain(utm_sourcedto);
        domain.setId(utm_source_id);
		utm_sourceService.update(domain);
		Utm_sourceDTO dto = utm_sourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#utm_source_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Utm_source" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_sources/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_sourceDTO> utm_sourcedtos) {
        utm_sourceService.updateBatch(utm_sourceMapping.toDomain(utm_sourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Utm_source" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/utm_sources/fetchdefault")
	public ResponseEntity<List<Utm_sourceDTO>> fetchDefault(Utm_sourceSearchContext context) {
        Page<Utm_source> domains = utm_sourceService.searchDefault(context) ;
        List<Utm_sourceDTO> list = utm_sourceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Utm_source" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/utm_sources/searchdefault")
	public ResponseEntity<Page<Utm_sourceDTO>> searchDefault(Utm_sourceSearchContext context) {
        Page<Utm_source> domains = utm_sourceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(utm_sourceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Utm_source getEntity(){
        return new Utm_source();
    }

}
