package cn.ibizlab.odoo.odoo_utm.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium;
import cn.ibizlab.odoo.odoo_utm.dto.Utm_mediumDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Utm_mediumMapping extends MappingBase<Utm_mediumDTO, Utm_medium> {


}

