package cn.ibizlab.odoo.odoo_utm.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign;
import cn.ibizlab.odoo.odoo_utm.dto.Utm_campaignDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Utm_campaignMapping extends MappingBase<Utm_campaignDTO, Utm_campaign> {


}

