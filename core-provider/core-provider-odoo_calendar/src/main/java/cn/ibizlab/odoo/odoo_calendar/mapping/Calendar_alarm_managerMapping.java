package cn.ibizlab.odoo.odoo_calendar.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.odoo.odoo_calendar.dto.Calendar_alarm_managerDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Calendar_alarm_managerMapping extends MappingBase<Calendar_alarm_managerDTO, Calendar_alarm_manager> {


}

