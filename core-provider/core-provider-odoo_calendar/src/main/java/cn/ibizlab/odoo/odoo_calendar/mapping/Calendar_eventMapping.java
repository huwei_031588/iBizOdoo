package cn.ibizlab.odoo.odoo_calendar.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.odoo_calendar.dto.Calendar_eventDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Calendar_eventMapping extends MappingBase<Calendar_eventDTO, Calendar_event> {


}

