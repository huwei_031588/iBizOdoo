package cn.ibizlab.odoo.odoo_calendar.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_calendar.dto.*;
import cn.ibizlab.odoo.odoo_calendar.mapping.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_attendee;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_attendeeService;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_attendeeSearchContext;




@Slf4j
@Api(tags = {"Calendar_attendee" })
@RestController("odoo_calendar-calendar_attendee")
@RequestMapping("")
public class Calendar_attendeeResource {

    @Autowired
    private ICalendar_attendeeService calendar_attendeeService;

    @Autowired
    @Lazy
    private Calendar_attendeeMapping calendar_attendeeMapping;










    @PreAuthorize("hasPermission(#calendar_attendee_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Calendar_attendee" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_attendees/{calendar_attendee_id}")

    public ResponseEntity<Calendar_attendeeDTO> update(@PathVariable("calendar_attendee_id") Integer calendar_attendee_id, @RequestBody Calendar_attendeeDTO calendar_attendeedto) {
		Calendar_attendee domain = calendar_attendeeMapping.toDomain(calendar_attendeedto);
        domain.setId(calendar_attendee_id);
		calendar_attendeeService.update(domain);
		Calendar_attendeeDTO dto = calendar_attendeeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#calendar_attendee_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Calendar_attendee" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_attendees/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_attendeeDTO> calendar_attendeedtos) {
        calendar_attendeeService.updateBatch(calendar_attendeeMapping.toDomain(calendar_attendeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#calendar_attendee_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Calendar_attendee" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_attendees/{calendar_attendee_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_attendee_id") Integer calendar_attendee_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_attendeeService.remove(calendar_attendee_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Calendar_attendee" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_attendees/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        calendar_attendeeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Calendar_attendee" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_attendees")

    public ResponseEntity<Calendar_attendeeDTO> create(@RequestBody Calendar_attendeeDTO calendar_attendeedto) {
        Calendar_attendee domain = calendar_attendeeMapping.toDomain(calendar_attendeedto);
		calendar_attendeeService.create(domain);
        Calendar_attendeeDTO dto = calendar_attendeeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Calendar_attendee" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_attendees/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_attendeeDTO> calendar_attendeedtos) {
        calendar_attendeeService.createBatch(calendar_attendeeMapping.toDomain(calendar_attendeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#calendar_attendee_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Calendar_attendee" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_attendees/{calendar_attendee_id}")
    public ResponseEntity<Calendar_attendeeDTO> get(@PathVariable("calendar_attendee_id") Integer calendar_attendee_id) {
        Calendar_attendee domain = calendar_attendeeService.get(calendar_attendee_id);
        Calendar_attendeeDTO dto = calendar_attendeeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Calendar_attendee" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_attendees/fetchdefault")
	public ResponseEntity<List<Calendar_attendeeDTO>> fetchDefault(Calendar_attendeeSearchContext context) {
        Page<Calendar_attendee> domains = calendar_attendeeService.searchDefault(context) ;
        List<Calendar_attendeeDTO> list = calendar_attendeeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Calendar_attendee" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_attendees/searchdefault")
	public ResponseEntity<Page<Calendar_attendeeDTO>> searchDefault(Calendar_attendeeSearchContext context) {
        Page<Calendar_attendee> domains = calendar_attendeeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_attendeeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Calendar_attendee getEntity(){
        return new Calendar_attendee();
    }

}
