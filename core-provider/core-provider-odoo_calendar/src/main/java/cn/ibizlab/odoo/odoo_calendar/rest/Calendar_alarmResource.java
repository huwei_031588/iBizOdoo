package cn.ibizlab.odoo.odoo_calendar.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_calendar.dto.*;
import cn.ibizlab.odoo.odoo_calendar.mapping.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_alarmService;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarmSearchContext;




@Slf4j
@Api(tags = {"Calendar_alarm" })
@RestController("odoo_calendar-calendar_alarm")
@RequestMapping("")
public class Calendar_alarmResource {

    @Autowired
    private ICalendar_alarmService calendar_alarmService;

    @Autowired
    @Lazy
    private Calendar_alarmMapping calendar_alarmMapping;




    @PreAuthorize("hasPermission(#calendar_alarm_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Calendar_alarm" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_alarms/{calendar_alarm_id}")
    public ResponseEntity<Calendar_alarmDTO> get(@PathVariable("calendar_alarm_id") Integer calendar_alarm_id) {
        Calendar_alarm domain = calendar_alarmService.get(calendar_alarm_id);
        Calendar_alarmDTO dto = calendar_alarmMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('Remove',{#calendar_alarm_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Calendar_alarm" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarms/{calendar_alarm_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_alarm_id") Integer calendar_alarm_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_alarmService.remove(calendar_alarm_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Calendar_alarm" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        calendar_alarmService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Calendar_alarm" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms")

    public ResponseEntity<Calendar_alarmDTO> create(@RequestBody Calendar_alarmDTO calendar_alarmdto) {
        Calendar_alarm domain = calendar_alarmMapping.toDomain(calendar_alarmdto);
		calendar_alarmService.create(domain);
        Calendar_alarmDTO dto = calendar_alarmMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Calendar_alarm" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_alarmDTO> calendar_alarmdtos) {
        calendar_alarmService.createBatch(calendar_alarmMapping.toDomain(calendar_alarmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#calendar_alarm_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Calendar_alarm" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarms/{calendar_alarm_id}")

    public ResponseEntity<Calendar_alarmDTO> update(@PathVariable("calendar_alarm_id") Integer calendar_alarm_id, @RequestBody Calendar_alarmDTO calendar_alarmdto) {
		Calendar_alarm domain = calendar_alarmMapping.toDomain(calendar_alarmdto);
        domain.setId(calendar_alarm_id);
		calendar_alarmService.update(domain);
		Calendar_alarmDTO dto = calendar_alarmMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#calendar_alarm_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Calendar_alarm" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_alarmDTO> calendar_alarmdtos) {
        calendar_alarmService.updateBatch(calendar_alarmMapping.toDomain(calendar_alarmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Calendar_alarm" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_alarms/fetchdefault")
	public ResponseEntity<List<Calendar_alarmDTO>> fetchDefault(Calendar_alarmSearchContext context) {
        Page<Calendar_alarm> domains = calendar_alarmService.searchDefault(context) ;
        List<Calendar_alarmDTO> list = calendar_alarmMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Calendar_alarm" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_alarms/searchdefault")
	public ResponseEntity<Page<Calendar_alarmDTO>> searchDefault(Calendar_alarmSearchContext context) {
        Page<Calendar_alarm> domains = calendar_alarmService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_alarmMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Calendar_alarm getEntity(){
        return new Calendar_alarm();
    }

}
