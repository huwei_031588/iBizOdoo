package cn.ibizlab.odoo.odoo_repair.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_repair.dto.*;
import cn.ibizlab.odoo.odoo_repair.mapping.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_orderService;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_orderSearchContext;




@Slf4j
@Api(tags = {"Repair_order" })
@RestController("odoo_repair-repair_order")
@RequestMapping("")
public class Repair_orderResource {

    @Autowired
    private IRepair_orderService repair_orderService;

    @Autowired
    @Lazy
    private Repair_orderMapping repair_orderMapping;










    @PreAuthorize("hasPermission(#repair_order_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Repair_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_orders/{repair_order_id}")
    public ResponseEntity<Repair_orderDTO> get(@PathVariable("repair_order_id") Integer repair_order_id) {
        Repair_order domain = repair_orderService.get(repair_order_id);
        Repair_orderDTO dto = repair_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#repair_order_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Repair_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_orders/{repair_order_id}")

    public ResponseEntity<Repair_orderDTO> update(@PathVariable("repair_order_id") Integer repair_order_id, @RequestBody Repair_orderDTO repair_orderdto) {
		Repair_order domain = repair_orderMapping.toDomain(repair_orderdto);
        domain.setId(repair_order_id);
		repair_orderService.update(domain);
		Repair_orderDTO dto = repair_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#repair_order_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Repair_order" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_orderDTO> repair_orderdtos) {
        repair_orderService.updateBatch(repair_orderMapping.toDomain(repair_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#repair_order_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Repair_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_orders/{repair_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("repair_order_id") Integer repair_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(repair_orderService.remove(repair_order_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Repair_order" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        repair_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Repair_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_orders")

    public ResponseEntity<Repair_orderDTO> create(@RequestBody Repair_orderDTO repair_orderdto) {
        Repair_order domain = repair_orderMapping.toDomain(repair_orderdto);
		repair_orderService.create(domain);
        Repair_orderDTO dto = repair_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Repair_order" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Repair_orderDTO> repair_orderdtos) {
        repair_orderService.createBatch(repair_orderMapping.toDomain(repair_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Repair_order" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/repair_orders/fetchdefault")
	public ResponseEntity<List<Repair_orderDTO>> fetchDefault(Repair_orderSearchContext context) {
        Page<Repair_order> domains = repair_orderService.searchDefault(context) ;
        List<Repair_orderDTO> list = repair_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Repair_order" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/repair_orders/searchdefault")
	public ResponseEntity<Page<Repair_orderDTO>> searchDefault(Repair_orderSearchContext context) {
        Page<Repair_order> domains = repair_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(repair_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Repair_order getEntity(){
        return new Repair_order();
    }

}
