package cn.ibizlab.odoo.odoo_repair.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.odoo.odoo_repair.dto.Repair_order_make_invoiceDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Repair_order_make_invoiceMapping extends MappingBase<Repair_order_make_invoiceDTO, Repair_order_make_invoice> {


}

