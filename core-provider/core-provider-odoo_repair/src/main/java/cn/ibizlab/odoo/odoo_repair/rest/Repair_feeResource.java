package cn.ibizlab.odoo.odoo_repair.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_repair.dto.*;
import cn.ibizlab.odoo.odoo_repair.mapping.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_feeService;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_feeSearchContext;




@Slf4j
@Api(tags = {"Repair_fee" })
@RestController("odoo_repair-repair_fee")
@RequestMapping("")
public class Repair_feeResource {

    @Autowired
    private IRepair_feeService repair_feeService;

    @Autowired
    @Lazy
    private Repair_feeMapping repair_feeMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Repair_fee" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_fees")

    public ResponseEntity<Repair_feeDTO> create(@RequestBody Repair_feeDTO repair_feedto) {
        Repair_fee domain = repair_feeMapping.toDomain(repair_feedto);
		repair_feeService.create(domain);
        Repair_feeDTO dto = repair_feeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Repair_fee" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_fees/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Repair_feeDTO> repair_feedtos) {
        repair_feeService.createBatch(repair_feeMapping.toDomain(repair_feedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#repair_fee_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Repair_fee" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_fees/{repair_fee_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("repair_fee_id") Integer repair_fee_id) {
         return ResponseEntity.status(HttpStatus.OK).body(repair_feeService.remove(repair_fee_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Repair_fee" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_fees/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        repair_feeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#repair_fee_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Repair_fee" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_fees/{repair_fee_id}")
    public ResponseEntity<Repair_feeDTO> get(@PathVariable("repair_fee_id") Integer repair_fee_id) {
        Repair_fee domain = repair_feeService.get(repair_fee_id);
        Repair_feeDTO dto = repair_feeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission(#repair_fee_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Repair_fee" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_fees/{repair_fee_id}")

    public ResponseEntity<Repair_feeDTO> update(@PathVariable("repair_fee_id") Integer repair_fee_id, @RequestBody Repair_feeDTO repair_feedto) {
		Repair_fee domain = repair_feeMapping.toDomain(repair_feedto);
        domain.setId(repair_fee_id);
		repair_feeService.update(domain);
		Repair_feeDTO dto = repair_feeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#repair_fee_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Repair_fee" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_fees/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_feeDTO> repair_feedtos) {
        repair_feeService.updateBatch(repair_feeMapping.toDomain(repair_feedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Repair_fee" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/repair_fees/fetchdefault")
	public ResponseEntity<List<Repair_feeDTO>> fetchDefault(Repair_feeSearchContext context) {
        Page<Repair_fee> domains = repair_feeService.searchDefault(context) ;
        List<Repair_feeDTO> list = repair_feeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Repair_fee" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/repair_fees/searchdefault")
	public ResponseEntity<Page<Repair_feeDTO>> searchDefault(Repair_feeSearchContext context) {
        Page<Repair_fee> domains = repair_feeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(repair_feeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Repair_fee getEntity(){
        return new Repair_fee();
    }

}
