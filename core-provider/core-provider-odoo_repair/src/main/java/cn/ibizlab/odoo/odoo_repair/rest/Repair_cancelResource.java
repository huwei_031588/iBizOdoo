package cn.ibizlab.odoo.odoo_repair.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_repair.dto.*;
import cn.ibizlab.odoo.odoo_repair.mapping.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_cancelService;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_cancelSearchContext;




@Slf4j
@Api(tags = {"Repair_cancel" })
@RestController("odoo_repair-repair_cancel")
@RequestMapping("")
public class Repair_cancelResource {

    @Autowired
    private IRepair_cancelService repair_cancelService;

    @Autowired
    @Lazy
    private Repair_cancelMapping repair_cancelMapping;










    @PreAuthorize("hasPermission(#repair_cancel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Repair_cancel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_cancels/{repair_cancel_id}")

    public ResponseEntity<Repair_cancelDTO> update(@PathVariable("repair_cancel_id") Integer repair_cancel_id, @RequestBody Repair_cancelDTO repair_canceldto) {
		Repair_cancel domain = repair_cancelMapping.toDomain(repair_canceldto);
        domain.setId(repair_cancel_id);
		repair_cancelService.update(domain);
		Repair_cancelDTO dto = repair_cancelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#repair_cancel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Repair_cancel" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_cancels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_cancelDTO> repair_canceldtos) {
        repair_cancelService.updateBatch(repair_cancelMapping.toDomain(repair_canceldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Repair_cancel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_cancels")

    public ResponseEntity<Repair_cancelDTO> create(@RequestBody Repair_cancelDTO repair_canceldto) {
        Repair_cancel domain = repair_cancelMapping.toDomain(repair_canceldto);
		repair_cancelService.create(domain);
        Repair_cancelDTO dto = repair_cancelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Repair_cancel" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_cancels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Repair_cancelDTO> repair_canceldtos) {
        repair_cancelService.createBatch(repair_cancelMapping.toDomain(repair_canceldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#repair_cancel_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Repair_cancel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_cancels/{repair_cancel_id}")
    public ResponseEntity<Repair_cancelDTO> get(@PathVariable("repair_cancel_id") Integer repair_cancel_id) {
        Repair_cancel domain = repair_cancelService.get(repair_cancel_id);
        Repair_cancelDTO dto = repair_cancelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#repair_cancel_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Repair_cancel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_cancels/{repair_cancel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("repair_cancel_id") Integer repair_cancel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(repair_cancelService.remove(repair_cancel_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Repair_cancel" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_cancels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        repair_cancelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Repair_cancel" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/repair_cancels/fetchdefault")
	public ResponseEntity<List<Repair_cancelDTO>> fetchDefault(Repair_cancelSearchContext context) {
        Page<Repair_cancel> domains = repair_cancelService.searchDefault(context) ;
        List<Repair_cancelDTO> list = repair_cancelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Repair_cancel" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/repair_cancels/searchdefault")
	public ResponseEntity<Page<Repair_cancelDTO>> searchDefault(Repair_cancelSearchContext context) {
        Page<Repair_cancel> domains = repair_cancelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(repair_cancelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Repair_cancel getEntity(){
        return new Repair_cancel();
    }

}
