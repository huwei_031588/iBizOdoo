package cn.ibizlab.odoo.odoo_repair.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_repair.dto.*;
import cn.ibizlab.odoo.odoo_repair.mapping.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_lineService;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_lineSearchContext;




@Slf4j
@Api(tags = {"Repair_line" })
@RestController("odoo_repair-repair_line")
@RequestMapping("")
public class Repair_lineResource {

    @Autowired
    private IRepair_lineService repair_lineService;

    @Autowired
    @Lazy
    private Repair_lineMapping repair_lineMapping;




    @PreAuthorize("hasPermission(#repair_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Repair_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_lines/{repair_line_id}")
    public ResponseEntity<Repair_lineDTO> get(@PathVariable("repair_line_id") Integer repair_line_id) {
        Repair_line domain = repair_lineService.get(repair_line_id);
        Repair_lineDTO dto = repair_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#repair_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Repair_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_lines/{repair_line_id}")

    public ResponseEntity<Repair_lineDTO> update(@PathVariable("repair_line_id") Integer repair_line_id, @RequestBody Repair_lineDTO repair_linedto) {
		Repair_line domain = repair_lineMapping.toDomain(repair_linedto);
        domain.setId(repair_line_id);
		repair_lineService.update(domain);
		Repair_lineDTO dto = repair_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#repair_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Repair_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_lineDTO> repair_linedtos) {
        repair_lineService.updateBatch(repair_lineMapping.toDomain(repair_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#repair_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Repair_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_lines/{repair_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("repair_line_id") Integer repair_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(repair_lineService.remove(repair_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Repair_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        repair_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Repair_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_lines")

    public ResponseEntity<Repair_lineDTO> create(@RequestBody Repair_lineDTO repair_linedto) {
        Repair_line domain = repair_lineMapping.toDomain(repair_linedto);
		repair_lineService.create(domain);
        Repair_lineDTO dto = repair_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Repair_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Repair_lineDTO> repair_linedtos) {
        repair_lineService.createBatch(repair_lineMapping.toDomain(repair_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Repair_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/repair_lines/fetchdefault")
	public ResponseEntity<List<Repair_lineDTO>> fetchDefault(Repair_lineSearchContext context) {
        Page<Repair_line> domains = repair_lineService.searchDefault(context) ;
        List<Repair_lineDTO> list = repair_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Repair_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/repair_lines/searchdefault")
	public ResponseEntity<Page<Repair_lineDTO>> searchDefault(Repair_lineSearchContext context) {
        Page<Repair_line> domains = repair_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(repair_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Repair_line getEntity(){
        return new Repair_line();
    }

}
