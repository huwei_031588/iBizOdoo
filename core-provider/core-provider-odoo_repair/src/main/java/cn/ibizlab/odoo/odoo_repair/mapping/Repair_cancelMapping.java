package cn.ibizlab.odoo.odoo_repair.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.odoo.odoo_repair.dto.Repair_cancelDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Repair_cancelMapping extends MappingBase<Repair_cancelDTO, Repair_cancel> {


}

