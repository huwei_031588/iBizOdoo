package cn.ibizlab.odoo.odoo_digest.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_digest")
public class odoo_digestRestConfiguration {

}
