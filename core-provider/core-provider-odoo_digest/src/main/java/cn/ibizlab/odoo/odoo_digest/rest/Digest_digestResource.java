package cn.ibizlab.odoo.odoo_digest.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_digest.dto.*;
import cn.ibizlab.odoo.odoo_digest.mapping.*;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest;
import cn.ibizlab.odoo.core.odoo_digest.service.IDigest_digestService;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_digestSearchContext;




@Slf4j
@Api(tags = {"Digest_digest" })
@RestController("odoo_digest-digest_digest")
@RequestMapping("")
public class Digest_digestResource {

    @Autowired
    private IDigest_digestService digest_digestService;

    @Autowired
    @Lazy
    private Digest_digestMapping digest_digestMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Digest_digest" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_digests")

    public ResponseEntity<Digest_digestDTO> create(@RequestBody Digest_digestDTO digest_digestdto) {
        Digest_digest domain = digest_digestMapping.toDomain(digest_digestdto);
		digest_digestService.create(domain);
        Digest_digestDTO dto = digest_digestMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Digest_digest" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_digests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Digest_digestDTO> digest_digestdtos) {
        digest_digestService.createBatch(digest_digestMapping.toDomain(digest_digestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#digest_digest_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Digest_digest" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/digest_digests/{digest_digest_id}")
    public ResponseEntity<Digest_digestDTO> get(@PathVariable("digest_digest_id") Integer digest_digest_id) {
        Digest_digest domain = digest_digestService.get(digest_digest_id);
        Digest_digestDTO dto = digest_digestMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#digest_digest_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Digest_digest" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/digest_digests/{digest_digest_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("digest_digest_id") Integer digest_digest_id) {
         return ResponseEntity.status(HttpStatus.OK).body(digest_digestService.remove(digest_digest_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Digest_digest" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/digest_digests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        digest_digestService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#digest_digest_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Digest_digest" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/digest_digests/{digest_digest_id}")

    public ResponseEntity<Digest_digestDTO> update(@PathVariable("digest_digest_id") Integer digest_digest_id, @RequestBody Digest_digestDTO digest_digestdto) {
		Digest_digest domain = digest_digestMapping.toDomain(digest_digestdto);
        domain.setId(digest_digest_id);
		digest_digestService.update(domain);
		Digest_digestDTO dto = digest_digestMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#digest_digest_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Digest_digest" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/digest_digests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Digest_digestDTO> digest_digestdtos) {
        digest_digestService.updateBatch(digest_digestMapping.toDomain(digest_digestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Digest_digest" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/digest_digests/fetchdefault")
	public ResponseEntity<List<Digest_digestDTO>> fetchDefault(Digest_digestSearchContext context) {
        Page<Digest_digest> domains = digest_digestService.searchDefault(context) ;
        List<Digest_digestDTO> list = digest_digestMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Digest_digest" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/digest_digests/searchdefault")
	public ResponseEntity<Page<Digest_digestDTO>> searchDefault(Digest_digestSearchContext context) {
        Page<Digest_digest> domains = digest_digestService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(digest_digestMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Digest_digest getEntity(){
        return new Digest_digest();
    }

}
