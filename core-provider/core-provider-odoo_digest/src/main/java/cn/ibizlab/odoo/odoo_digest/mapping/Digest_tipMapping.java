package cn.ibizlab.odoo.odoo_digest.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.odoo.odoo_digest.dto.Digest_tipDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Digest_tipMapping extends MappingBase<Digest_tipDTO, Digest_tip> {


}

