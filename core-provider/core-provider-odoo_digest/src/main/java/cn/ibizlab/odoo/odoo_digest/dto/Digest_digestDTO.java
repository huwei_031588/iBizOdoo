package cn.ibizlab.odoo.odoo_digest.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Digest_digestDTO]
 */
@Data
public class Digest_digestDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [KPI_WEBSITE_SALE_TOTAL_VALUE]
     *
     */
    @JSONField(name = "kpi_website_sale_total_value")
    @JsonProperty("kpi_website_sale_total_value")
    private Double kpiWebsiteSaleTotalValue;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [KPI_CRM_LEAD_CREATED_VALUE]
     *
     */
    @JSONField(name = "kpi_crm_lead_created_value")
    @JsonProperty("kpi_crm_lead_created_value")
    private Integer kpiCrmLeadCreatedValue;

    /**
     * 属性 [KPI_CRM_OPPORTUNITIES_WON]
     *
     */
    @JSONField(name = "kpi_crm_opportunities_won")
    @JsonProperty("kpi_crm_opportunities_won")
    private String kpiCrmOpportunitiesWon;

    /**
     * 属性 [AVAILABLE_FIELDS]
     *
     */
    @JSONField(name = "available_fields")
    @JsonProperty("available_fields")
    private String availableFields;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [USER_IDS]
     *
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;

    /**
     * 属性 [KPI_CRM_OPPORTUNITIES_WON_VALUE]
     *
     */
    @JSONField(name = "kpi_crm_opportunities_won_value")
    @JsonProperty("kpi_crm_opportunities_won_value")
    private Integer kpiCrmOpportunitiesWonValue;

    /**
     * 属性 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES]
     *
     */
    @JSONField(name = "kpi_hr_recruitment_new_colleagues")
    @JsonProperty("kpi_hr_recruitment_new_colleagues")
    private String kpiHrRecruitmentNewColleagues;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [KPI_POS_TOTAL]
     *
     */
    @JSONField(name = "kpi_pos_total")
    @JsonProperty("kpi_pos_total")
    private String kpiPosTotal;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [KPI_POS_TOTAL_VALUE]
     *
     */
    @JSONField(name = "kpi_pos_total_value")
    @JsonProperty("kpi_pos_total_value")
    private Double kpiPosTotalValue;

    /**
     * 属性 [KPI_WEBSITE_SALE_TOTAL]
     *
     */
    @JSONField(name = "kpi_website_sale_total")
    @JsonProperty("kpi_website_sale_total")
    private String kpiWebsiteSaleTotal;

    /**
     * 属性 [KPI_ALL_SALE_TOTAL]
     *
     */
    @JSONField(name = "kpi_all_sale_total")
    @JsonProperty("kpi_all_sale_total")
    private String kpiAllSaleTotal;

    /**
     * 属性 [NEXT_RUN_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_run_date" , format="yyyy-MM-dd")
    @JsonProperty("next_run_date")
    private Timestamp nextRunDate;

    /**
     * 属性 [KPI_RES_USERS_CONNECTED]
     *
     */
    @JSONField(name = "kpi_res_users_connected")
    @JsonProperty("kpi_res_users_connected")
    private String kpiResUsersConnected;

    /**
     * 属性 [KPI_RES_USERS_CONNECTED_VALUE]
     *
     */
    @JSONField(name = "kpi_res_users_connected_value")
    @JsonProperty("kpi_res_users_connected_value")
    private Integer kpiResUsersConnectedValue;

    /**
     * 属性 [PERIODICITY]
     *
     */
    @JSONField(name = "periodicity")
    @JsonProperty("periodicity")
    private String periodicity;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES_VALUE]
     *
     */
    @JSONField(name = "kpi_hr_recruitment_new_colleagues_value")
    @JsonProperty("kpi_hr_recruitment_new_colleagues_value")
    private Integer kpiHrRecruitmentNewColleaguesValue;

    /**
     * 属性 [KPI_ACCOUNT_TOTAL_REVENUE]
     *
     */
    @JSONField(name = "kpi_account_total_revenue")
    @JsonProperty("kpi_account_total_revenue")
    private String kpiAccountTotalRevenue;

    /**
     * 属性 [KPI_CRM_LEAD_CREATED]
     *
     */
    @JSONField(name = "kpi_crm_lead_created")
    @JsonProperty("kpi_crm_lead_created")
    private String kpiCrmLeadCreated;

    /**
     * 属性 [KPI_PROJECT_TASK_OPENED]
     *
     */
    @JSONField(name = "kpi_project_task_opened")
    @JsonProperty("kpi_project_task_opened")
    private String kpiProjectTaskOpened;

    /**
     * 属性 [KPI_ALL_SALE_TOTAL_VALUE]
     *
     */
    @JSONField(name = "kpi_all_sale_total_value")
    @JsonProperty("kpi_all_sale_total_value")
    private Double kpiAllSaleTotalValue;

    /**
     * 属性 [IS_SUBSCRIBED]
     *
     */
    @JSONField(name = "is_subscribed")
    @JsonProperty("is_subscribed")
    private String isSubscribed;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [KPI_PROJECT_TASK_OPENED_VALUE]
     *
     */
    @JSONField(name = "kpi_project_task_opened_value")
    @JsonProperty("kpi_project_task_opened_value")
    private Integer kpiProjectTaskOpenedValue;

    /**
     * 属性 [KPI_ACCOUNT_TOTAL_REVENUE_VALUE]
     *
     */
    @JSONField(name = "kpi_account_total_revenue_value")
    @JsonProperty("kpi_account_total_revenue_value")
    private Double kpiAccountTotalRevenueValue;

    /**
     * 属性 [KPI_MAIL_MESSAGE_TOTAL_VALUE]
     *
     */
    @JSONField(name = "kpi_mail_message_total_value")
    @JsonProperty("kpi_mail_message_total_value")
    private Integer kpiMailMessageTotalValue;

    /**
     * 属性 [KPI_MAIL_MESSAGE_TOTAL]
     *
     */
    @JSONField(name = "kpi_mail_message_total")
    @JsonProperty("kpi_mail_message_total")
    private String kpiMailMessageTotal;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Integer templateId;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [KPI_CRM_OPPORTUNITIES_WON]
     */
    public void setKpiCrmOpportunitiesWon(String  kpiCrmOpportunitiesWon){
        this.kpiCrmOpportunitiesWon = kpiCrmOpportunitiesWon ;
        this.modify("kpi_crm_opportunities_won",kpiCrmOpportunitiesWon);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES]
     */
    public void setKpiHrRecruitmentNewColleagues(String  kpiHrRecruitmentNewColleagues){
        this.kpiHrRecruitmentNewColleagues = kpiHrRecruitmentNewColleagues ;
        this.modify("kpi_hr_recruitment_new_colleagues",kpiHrRecruitmentNewColleagues);
    }

    /**
     * 设置 [KPI_POS_TOTAL]
     */
    public void setKpiPosTotal(String  kpiPosTotal){
        this.kpiPosTotal = kpiPosTotal ;
        this.modify("kpi_pos_total",kpiPosTotal);
    }

    /**
     * 设置 [KPI_WEBSITE_SALE_TOTAL]
     */
    public void setKpiWebsiteSaleTotal(String  kpiWebsiteSaleTotal){
        this.kpiWebsiteSaleTotal = kpiWebsiteSaleTotal ;
        this.modify("kpi_website_sale_total",kpiWebsiteSaleTotal);
    }

    /**
     * 设置 [KPI_ALL_SALE_TOTAL]
     */
    public void setKpiAllSaleTotal(String  kpiAllSaleTotal){
        this.kpiAllSaleTotal = kpiAllSaleTotal ;
        this.modify("kpi_all_sale_total",kpiAllSaleTotal);
    }

    /**
     * 设置 [NEXT_RUN_DATE]
     */
    public void setNextRunDate(Timestamp  nextRunDate){
        this.nextRunDate = nextRunDate ;
        this.modify("next_run_date",nextRunDate);
    }

    /**
     * 设置 [KPI_RES_USERS_CONNECTED]
     */
    public void setKpiResUsersConnected(String  kpiResUsersConnected){
        this.kpiResUsersConnected = kpiResUsersConnected ;
        this.modify("kpi_res_users_connected",kpiResUsersConnected);
    }

    /**
     * 设置 [PERIODICITY]
     */
    public void setPeriodicity(String  periodicity){
        this.periodicity = periodicity ;
        this.modify("periodicity",periodicity);
    }

    /**
     * 设置 [KPI_ACCOUNT_TOTAL_REVENUE]
     */
    public void setKpiAccountTotalRevenue(String  kpiAccountTotalRevenue){
        this.kpiAccountTotalRevenue = kpiAccountTotalRevenue ;
        this.modify("kpi_account_total_revenue",kpiAccountTotalRevenue);
    }

    /**
     * 设置 [KPI_CRM_LEAD_CREATED]
     */
    public void setKpiCrmLeadCreated(String  kpiCrmLeadCreated){
        this.kpiCrmLeadCreated = kpiCrmLeadCreated ;
        this.modify("kpi_crm_lead_created",kpiCrmLeadCreated);
    }

    /**
     * 设置 [KPI_PROJECT_TASK_OPENED]
     */
    public void setKpiProjectTaskOpened(String  kpiProjectTaskOpened){
        this.kpiProjectTaskOpened = kpiProjectTaskOpened ;
        this.modify("kpi_project_task_opened",kpiProjectTaskOpened);
    }

    /**
     * 设置 [KPI_MAIL_MESSAGE_TOTAL]
     */
    public void setKpiMailMessageTotal(String  kpiMailMessageTotal){
        this.kpiMailMessageTotal = kpiMailMessageTotal ;
        this.modify("kpi_mail_message_total",kpiMailMessageTotal);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    public void setTemplateId(Integer  templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }


}

