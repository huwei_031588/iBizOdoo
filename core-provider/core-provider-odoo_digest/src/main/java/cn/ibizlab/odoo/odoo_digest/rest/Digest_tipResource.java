package cn.ibizlab.odoo.odoo_digest.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_digest.dto.*;
import cn.ibizlab.odoo.odoo_digest.mapping.*;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.odoo.core.odoo_digest.service.IDigest_tipService;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_tipSearchContext;




@Slf4j
@Api(tags = {"Digest_tip" })
@RestController("odoo_digest-digest_tip")
@RequestMapping("")
public class Digest_tipResource {

    @Autowired
    private IDigest_tipService digest_tipService;

    @Autowired
    @Lazy
    private Digest_tipMapping digest_tipMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Digest_tip" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_tips")

    public ResponseEntity<Digest_tipDTO> create(@RequestBody Digest_tipDTO digest_tipdto) {
        Digest_tip domain = digest_tipMapping.toDomain(digest_tipdto);
		digest_tipService.create(domain);
        Digest_tipDTO dto = digest_tipMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Digest_tip" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_tips/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Digest_tipDTO> digest_tipdtos) {
        digest_tipService.createBatch(digest_tipMapping.toDomain(digest_tipdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#digest_tip_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Digest_tip" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/digest_tips/{digest_tip_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("digest_tip_id") Integer digest_tip_id) {
         return ResponseEntity.status(HttpStatus.OK).body(digest_tipService.remove(digest_tip_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Digest_tip" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/digest_tips/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        digest_tipService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#digest_tip_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Digest_tip" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/digest_tips/{digest_tip_id}")

    public ResponseEntity<Digest_tipDTO> update(@PathVariable("digest_tip_id") Integer digest_tip_id, @RequestBody Digest_tipDTO digest_tipdto) {
		Digest_tip domain = digest_tipMapping.toDomain(digest_tipdto);
        domain.setId(digest_tip_id);
		digest_tipService.update(domain);
		Digest_tipDTO dto = digest_tipMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#digest_tip_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Digest_tip" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/digest_tips/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Digest_tipDTO> digest_tipdtos) {
        digest_tipService.updateBatch(digest_tipMapping.toDomain(digest_tipdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#digest_tip_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Digest_tip" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/digest_tips/{digest_tip_id}")
    public ResponseEntity<Digest_tipDTO> get(@PathVariable("digest_tip_id") Integer digest_tip_id) {
        Digest_tip domain = digest_tipService.get(digest_tip_id);
        Digest_tipDTO dto = digest_tipMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Digest_tip" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/digest_tips/fetchdefault")
	public ResponseEntity<List<Digest_tipDTO>> fetchDefault(Digest_tipSearchContext context) {
        Page<Digest_tip> domains = digest_tipService.searchDefault(context) ;
        List<Digest_tipDTO> list = digest_tipMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Digest_tip" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/digest_tips/searchdefault")
	public ResponseEntity<Page<Digest_tipDTO>> searchDefault(Digest_tipSearchContext context) {
        Page<Digest_tip> domains = digest_tipService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(digest_tipMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Digest_tip getEntity(){
        return new Digest_tip();
    }

}
