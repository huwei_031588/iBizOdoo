package cn.ibizlab.odoo.odoo_crm.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.odoo.odoo_crm.dto.Crm_partner_bindingDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_partner_bindingMapping extends MappingBase<Crm_partner_bindingDTO, Crm_partner_binding> {


}

