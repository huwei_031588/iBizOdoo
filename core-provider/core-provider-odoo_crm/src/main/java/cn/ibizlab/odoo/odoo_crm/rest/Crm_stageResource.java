package cn.ibizlab.odoo.odoo_crm.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_crm.dto.*;
import cn.ibizlab.odoo.odoo_crm.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_stageService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_stageSearchContext;




@Slf4j
@Api(tags = {"Crm_stage" })
@RestController("odoo_crm-crm_stage")
@RequestMapping("")
public class Crm_stageResource {

    @Autowired
    private ICrm_stageService crm_stageService;

    @Autowired
    @Lazy
    private Crm_stageMapping crm_stageMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_stages")

    public ResponseEntity<Crm_stageDTO> create(@RequestBody Crm_stageDTO crm_stagedto) {
        Crm_stage domain = crm_stageMapping.toDomain(crm_stagedto);
		crm_stageService.create(domain);
        Crm_stageDTO dto = crm_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_stage" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_stageDTO> crm_stagedtos) {
        crm_stageService.createBatch(crm_stageMapping.toDomain(crm_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#crm_stage_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_stages/{crm_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_stage_id") Integer crm_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_stageService.remove(crm_stage_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_stage" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_stages/{crm_stage_id}")

    public ResponseEntity<Crm_stageDTO> update(@PathVariable("crm_stage_id") Integer crm_stage_id, @RequestBody Crm_stageDTO crm_stagedto) {
		Crm_stage domain = crm_stageMapping.toDomain(crm_stagedto);
        domain.setId(crm_stage_id);
		crm_stageService.update(domain);
		Crm_stageDTO dto = crm_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_stage" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_stageDTO> crm_stagedtos) {
        crm_stageService.updateBatch(crm_stageMapping.toDomain(crm_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_stage_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_stages/{crm_stage_id}")
    public ResponseEntity<Crm_stageDTO> get(@PathVariable("crm_stage_id") Integer crm_stage_id) {
        Crm_stage domain = crm_stageService.get(crm_stage_id);
        Crm_stageDTO dto = crm_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_stage" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_stages/fetchdefault")
	public ResponseEntity<List<Crm_stageDTO>> fetchDefault(Crm_stageSearchContext context) {
        Page<Crm_stage> domains = crm_stageService.searchDefault(context) ;
        List<Crm_stageDTO> list = crm_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Crm_stage" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_stages/searchdefault")
	public ResponseEntity<Page<Crm_stageDTO>> searchDefault(Crm_stageSearchContext context) {
        Page<Crm_stage> domains = crm_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_stage getEntity(){
        return new Crm_stage();
    }

}
