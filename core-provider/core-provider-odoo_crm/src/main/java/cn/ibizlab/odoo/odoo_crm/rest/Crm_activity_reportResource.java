package cn.ibizlab.odoo.odoo_crm.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_crm.dto.*;
import cn.ibizlab.odoo.odoo_crm.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_activity_reportService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_activity_reportSearchContext;




@Slf4j
@Api(tags = {"Crm_activity_report" })
@RestController("odoo_crm-crm_activity_report")
@RequestMapping("")
public class Crm_activity_reportResource {

    @Autowired
    private ICrm_activity_reportService crm_activity_reportService;

    @Autowired
    @Lazy
    private Crm_activity_reportMapping crm_activity_reportMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_activity_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_activity_reports")

    public ResponseEntity<Crm_activity_reportDTO> create(@RequestBody Crm_activity_reportDTO crm_activity_reportdto) {
        Crm_activity_report domain = crm_activity_reportMapping.toDomain(crm_activity_reportdto);
		crm_activity_reportService.create(domain);
        Crm_activity_reportDTO dto = crm_activity_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_activity_report" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_activity_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_activity_reportDTO> crm_activity_reportdtos) {
        crm_activity_reportService.createBatch(crm_activity_reportMapping.toDomain(crm_activity_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_activity_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_activity_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_activity_reports/{crm_activity_report_id}")

    public ResponseEntity<Crm_activity_reportDTO> update(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id, @RequestBody Crm_activity_reportDTO crm_activity_reportdto) {
		Crm_activity_report domain = crm_activity_reportMapping.toDomain(crm_activity_reportdto);
        domain.setId(crm_activity_report_id);
		crm_activity_reportService.update(domain);
		Crm_activity_reportDTO dto = crm_activity_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_activity_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_activity_report" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_activity_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_activity_reportDTO> crm_activity_reportdtos) {
        crm_activity_reportService.updateBatch(crm_activity_reportMapping.toDomain(crm_activity_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#crm_activity_report_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_activity_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_activity_reports/{crm_activity_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_activity_reportService.remove(crm_activity_report_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_activity_report" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_activity_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_activity_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_activity_report_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_activity_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_activity_reports/{crm_activity_report_id}")
    public ResponseEntity<Crm_activity_reportDTO> get(@PathVariable("crm_activity_report_id") Integer crm_activity_report_id) {
        Crm_activity_report domain = crm_activity_reportService.get(crm_activity_report_id);
        Crm_activity_reportDTO dto = crm_activity_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_activity_report" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_activity_reports/fetchdefault")
	public ResponseEntity<List<Crm_activity_reportDTO>> fetchDefault(Crm_activity_reportSearchContext context) {
        Page<Crm_activity_report> domains = crm_activity_reportService.searchDefault(context) ;
        List<Crm_activity_reportDTO> list = crm_activity_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Crm_activity_report" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_activity_reports/searchdefault")
	public ResponseEntity<Page<Crm_activity_reportDTO>> searchDefault(Crm_activity_reportSearchContext context) {
        Page<Crm_activity_report> domains = crm_activity_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_activity_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_activity_report getEntity(){
        return new Crm_activity_report();
    }

}
