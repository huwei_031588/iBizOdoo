package cn.ibizlab.odoo.odoo_crm.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.odoo.odoo_crm.dto.Crm_lead_lostDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_lead_lostMapping extends MappingBase<Crm_lead_lostDTO, Crm_lead_lost> {


}

