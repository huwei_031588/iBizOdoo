package cn.ibizlab.odoo.odoo_gamification.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_gamification.dto.*;
import cn.ibizlab.odoo.odoo_gamification.mapping.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_badgeService;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badgeSearchContext;




@Slf4j
@Api(tags = {"Gamification_badge" })
@RestController("odoo_gamification-gamification_badge")
@RequestMapping("")
public class Gamification_badgeResource {

    @Autowired
    private IGamification_badgeService gamification_badgeService;

    @Autowired
    @Lazy
    private Gamification_badgeMapping gamification_badgeMapping;




    @PreAuthorize("hasPermission('Remove',{#gamification_badge_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Gamification_badge" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badges/{gamification_badge_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_badge_id") Integer gamification_badge_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_badgeService.remove(gamification_badge_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Gamification_badge" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badges/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        gamification_badgeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#gamification_badge_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Gamification_badge" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badges/{gamification_badge_id}")

    public ResponseEntity<Gamification_badgeDTO> update(@PathVariable("gamification_badge_id") Integer gamification_badge_id, @RequestBody Gamification_badgeDTO gamification_badgedto) {
		Gamification_badge domain = gamification_badgeMapping.toDomain(gamification_badgedto);
        domain.setId(gamification_badge_id);
		gamification_badgeService.update(domain);
		Gamification_badgeDTO dto = gamification_badgeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#gamification_badge_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Gamification_badge" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badges/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_badgeDTO> gamification_badgedtos) {
        gamification_badgeService.updateBatch(gamification_badgeMapping.toDomain(gamification_badgedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Gamification_badge" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badges")

    public ResponseEntity<Gamification_badgeDTO> create(@RequestBody Gamification_badgeDTO gamification_badgedto) {
        Gamification_badge domain = gamification_badgeMapping.toDomain(gamification_badgedto);
		gamification_badgeService.create(domain);
        Gamification_badgeDTO dto = gamification_badgeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Gamification_badge" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_badgeDTO> gamification_badgedtos) {
        gamification_badgeService.createBatch(gamification_badgeMapping.toDomain(gamification_badgedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#gamification_badge_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Gamification_badge" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_badges/{gamification_badge_id}")
    public ResponseEntity<Gamification_badgeDTO> get(@PathVariable("gamification_badge_id") Integer gamification_badge_id) {
        Gamification_badge domain = gamification_badgeService.get(gamification_badge_id);
        Gamification_badgeDTO dto = gamification_badgeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Gamification_badge" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_badges/fetchdefault")
	public ResponseEntity<List<Gamification_badgeDTO>> fetchDefault(Gamification_badgeSearchContext context) {
        Page<Gamification_badge> domains = gamification_badgeService.searchDefault(context) ;
        List<Gamification_badgeDTO> list = gamification_badgeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Gamification_badge" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_badges/searchdefault")
	public ResponseEntity<Page<Gamification_badgeDTO>> searchDefault(Gamification_badgeSearchContext context) {
        Page<Gamification_badge> domains = gamification_badgeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_badgeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Gamification_badge getEntity(){
        return new Gamification_badge();
    }

}
