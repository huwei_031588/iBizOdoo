package cn.ibizlab.odoo.odoo_gamification.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_gamification.dto.*;
import cn.ibizlab.odoo.odoo_gamification.mapping.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_badge_userService;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_userSearchContext;




@Slf4j
@Api(tags = {"Gamification_badge_user" })
@RestController("odoo_gamification-gamification_badge_user")
@RequestMapping("")
public class Gamification_badge_userResource {

    @Autowired
    private IGamification_badge_userService gamification_badge_userService;

    @Autowired
    @Lazy
    private Gamification_badge_userMapping gamification_badge_userMapping;













    @PreAuthorize("hasPermission(#gamification_badge_user_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Gamification_badge_user" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_users/{gamification_badge_user_id}")
    public ResponseEntity<Gamification_badge_userDTO> get(@PathVariable("gamification_badge_user_id") Integer gamification_badge_user_id) {
        Gamification_badge_user domain = gamification_badge_userService.get(gamification_badge_user_id);
        Gamification_badge_userDTO dto = gamification_badge_userMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#gamification_badge_user_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Gamification_badge_user" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_users/{gamification_badge_user_id}")

    public ResponseEntity<Gamification_badge_userDTO> update(@PathVariable("gamification_badge_user_id") Integer gamification_badge_user_id, @RequestBody Gamification_badge_userDTO gamification_badge_userdto) {
		Gamification_badge_user domain = gamification_badge_userMapping.toDomain(gamification_badge_userdto);
        domain.setId(gamification_badge_user_id);
		gamification_badge_userService.update(domain);
		Gamification_badge_userDTO dto = gamification_badge_userMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#gamification_badge_user_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Gamification_badge_user" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_users/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_badge_userDTO> gamification_badge_userdtos) {
        gamification_badge_userService.updateBatch(gamification_badge_userMapping.toDomain(gamification_badge_userdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#gamification_badge_user_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Gamification_badge_user" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_users/{gamification_badge_user_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_badge_user_id") Integer gamification_badge_user_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_badge_userService.remove(gamification_badge_user_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Gamification_badge_user" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_users/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        gamification_badge_userService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Gamification_badge_user" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users")

    public ResponseEntity<Gamification_badge_userDTO> create(@RequestBody Gamification_badge_userDTO gamification_badge_userdto) {
        Gamification_badge_user domain = gamification_badge_userMapping.toDomain(gamification_badge_userdto);
		gamification_badge_userService.create(domain);
        Gamification_badge_userDTO dto = gamification_badge_userMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Gamification_badge_user" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_badge_userDTO> gamification_badge_userdtos) {
        gamification_badge_userService.createBatch(gamification_badge_userMapping.toDomain(gamification_badge_userdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Gamification_badge_user" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_badge_users/fetchdefault")
	public ResponseEntity<List<Gamification_badge_userDTO>> fetchDefault(Gamification_badge_userSearchContext context) {
        Page<Gamification_badge_user> domains = gamification_badge_userService.searchDefault(context) ;
        List<Gamification_badge_userDTO> list = gamification_badge_userMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Gamification_badge_user" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_badge_users/searchdefault")
	public ResponseEntity<Page<Gamification_badge_userDTO>> searchDefault(Gamification_badge_userSearchContext context) {
        Page<Gamification_badge_user> domains = gamification_badge_userService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_badge_userMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Gamification_badge_user getEntity(){
        return new Gamification_badge_user();
    }

}
