package cn.ibizlab.odoo.odoo_gamification.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_gamification.dto.*;
import cn.ibizlab.odoo.odoo_gamification.mapping.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_badge_user_wizardService;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_user_wizardSearchContext;




@Slf4j
@Api(tags = {"Gamification_badge_user_wizard" })
@RestController("odoo_gamification-gamification_badge_user_wizard")
@RequestMapping("")
public class Gamification_badge_user_wizardResource {

    @Autowired
    private IGamification_badge_user_wizardService gamification_badge_user_wizardService;

    @Autowired
    @Lazy
    private Gamification_badge_user_wizardMapping gamification_badge_user_wizardMapping;







    @PreAuthorize("hasPermission(#gamification_badge_user_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Gamification_badge_user_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_user_wizards/{gamification_badge_user_wizard_id}")

    public ResponseEntity<Gamification_badge_user_wizardDTO> update(@PathVariable("gamification_badge_user_wizard_id") Integer gamification_badge_user_wizard_id, @RequestBody Gamification_badge_user_wizardDTO gamification_badge_user_wizarddto) {
		Gamification_badge_user_wizard domain = gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddto);
        domain.setId(gamification_badge_user_wizard_id);
		gamification_badge_user_wizardService.update(domain);
		Gamification_badge_user_wizardDTO dto = gamification_badge_user_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#gamification_badge_user_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Gamification_badge_user_wizard" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_user_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_badge_user_wizardDTO> gamification_badge_user_wizarddtos) {
        gamification_badge_user_wizardService.updateBatch(gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#gamification_badge_user_wizard_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Gamification_badge_user_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_user_wizards/{gamification_badge_user_wizard_id}")
    public ResponseEntity<Gamification_badge_user_wizardDTO> get(@PathVariable("gamification_badge_user_wizard_id") Integer gamification_badge_user_wizard_id) {
        Gamification_badge_user_wizard domain = gamification_badge_user_wizardService.get(gamification_badge_user_wizard_id);
        Gamification_badge_user_wizardDTO dto = gamification_badge_user_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Gamification_badge_user_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards")

    public ResponseEntity<Gamification_badge_user_wizardDTO> create(@RequestBody Gamification_badge_user_wizardDTO gamification_badge_user_wizarddto) {
        Gamification_badge_user_wizard domain = gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddto);
		gamification_badge_user_wizardService.create(domain);
        Gamification_badge_user_wizardDTO dto = gamification_badge_user_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Gamification_badge_user_wizard" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_badge_user_wizardDTO> gamification_badge_user_wizarddtos) {
        gamification_badge_user_wizardService.createBatch(gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#gamification_badge_user_wizard_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Gamification_badge_user_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_user_wizards/{gamification_badge_user_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_badge_user_wizard_id") Integer gamification_badge_user_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_badge_user_wizardService.remove(gamification_badge_user_wizard_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Gamification_badge_user_wizard" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_user_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        gamification_badge_user_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Gamification_badge_user_wizard" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_badge_user_wizards/fetchdefault")
	public ResponseEntity<List<Gamification_badge_user_wizardDTO>> fetchDefault(Gamification_badge_user_wizardSearchContext context) {
        Page<Gamification_badge_user_wizard> domains = gamification_badge_user_wizardService.searchDefault(context) ;
        List<Gamification_badge_user_wizardDTO> list = gamification_badge_user_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Gamification_badge_user_wizard" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_badge_user_wizards/searchdefault")
	public ResponseEntity<Page<Gamification_badge_user_wizardDTO>> searchDefault(Gamification_badge_user_wizardSearchContext context) {
        Page<Gamification_badge_user_wizard> domains = gamification_badge_user_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_badge_user_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Gamification_badge_user_wizard getEntity(){
        return new Gamification_badge_user_wizard();
    }

}
