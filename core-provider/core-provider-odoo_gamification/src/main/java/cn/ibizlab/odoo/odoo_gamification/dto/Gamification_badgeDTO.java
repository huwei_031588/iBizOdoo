package cn.ibizlab.odoo.odoo_gamification.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Gamification_badgeDTO]
 */
@Data
public class Gamification_badgeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RULE_AUTH]
     *
     */
    @JSONField(name = "rule_auth")
    @JsonProperty("rule_auth")
    private String ruleAuth;

    /**
     * 属性 [RULE_MAX_NUMBER]
     *
     */
    @JSONField(name = "rule_max_number")
    @JsonProperty("rule_max_number")
    private Integer ruleMaxNumber;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [RULE_MAX]
     *
     */
    @JSONField(name = "rule_max")
    @JsonProperty("rule_max")
    private String ruleMax;

    /**
     * 属性 [RULE_AUTH_USER_IDS]
     *
     */
    @JSONField(name = "rule_auth_user_ids")
    @JsonProperty("rule_auth_user_ids")
    private String ruleAuthUserIds;

    /**
     * 属性 [RULE_AUTH_BADGE_IDS]
     *
     */
    @JSONField(name = "rule_auth_badge_ids")
    @JsonProperty("rule_auth_badge_ids")
    private String ruleAuthBadgeIds;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [REMAINING_SENDING]
     *
     */
    @JSONField(name = "remaining_sending")
    @JsonProperty("remaining_sending")
    private Integer remainingSending;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [STAT_MY]
     *
     */
    @JSONField(name = "stat_my")
    @JsonProperty("stat_my")
    private Integer statMy;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [UNIQUE_OWNER_IDS]
     *
     */
    @JSONField(name = "unique_owner_ids")
    @JsonProperty("unique_owner_ids")
    private String uniqueOwnerIds;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [LEVEL]
     *
     */
    @JSONField(name = "level")
    @JsonProperty("level")
    private String level;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [OWNER_IDS]
     *
     */
    @JSONField(name = "owner_ids")
    @JsonProperty("owner_ids")
    private String ownerIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [GOAL_DEFINITION_IDS]
     *
     */
    @JSONField(name = "goal_definition_ids")
    @JsonProperty("goal_definition_ids")
    private String goalDefinitionIds;

    /**
     * 属性 [STAT_MY_MONTHLY_SENDING]
     *
     */
    @JSONField(name = "stat_my_monthly_sending")
    @JsonProperty("stat_my_monthly_sending")
    private Integer statMyMonthlySending;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [STAT_COUNT]
     *
     */
    @JSONField(name = "stat_count")
    @JsonProperty("stat_count")
    private Integer statCount;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [STAT_THIS_MONTH]
     *
     */
    @JSONField(name = "stat_this_month")
    @JsonProperty("stat_this_month")
    private Integer statThisMonth;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [STAT_COUNT_DISTINCT]
     *
     */
    @JSONField(name = "stat_count_distinct")
    @JsonProperty("stat_count_distinct")
    private Integer statCountDistinct;

    /**
     * 属性 [STAT_MY_THIS_MONTH]
     *
     */
    @JSONField(name = "stat_my_this_month")
    @JsonProperty("stat_my_this_month")
    private Integer statMyThisMonth;

    /**
     * 属性 [CHALLENGE_IDS]
     *
     */
    @JSONField(name = "challenge_ids")
    @JsonProperty("challenge_ids")
    private String challengeIds;

    /**
     * 属性 [GRANTED_EMPLOYEES_COUNT]
     *
     */
    @JSONField(name = "granted_employees_count")
    @JsonProperty("granted_employees_count")
    private Integer grantedEmployeesCount;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [RULE_AUTH]
     */
    public void setRuleAuth(String  ruleAuth){
        this.ruleAuth = ruleAuth ;
        this.modify("rule_auth",ruleAuth);
    }

    /**
     * 设置 [RULE_MAX_NUMBER]
     */
    public void setRuleMaxNumber(Integer  ruleMaxNumber){
        this.ruleMaxNumber = ruleMaxNumber ;
        this.modify("rule_max_number",ruleMaxNumber);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [RULE_MAX]
     */
    public void setRuleMax(String  ruleMax){
        this.ruleMax = ruleMax ;
        this.modify("rule_max",ruleMax);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [LEVEL]
     */
    public void setLevel(String  level){
        this.level = level ;
        this.modify("level",level);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }


}

