package cn.ibizlab.odoo.odoo_gamification.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line;
import cn.ibizlab.odoo.odoo_gamification.dto.Gamification_challenge_lineDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Gamification_challenge_lineMapping extends MappingBase<Gamification_challenge_lineDTO, Gamification_challenge_line> {


}

