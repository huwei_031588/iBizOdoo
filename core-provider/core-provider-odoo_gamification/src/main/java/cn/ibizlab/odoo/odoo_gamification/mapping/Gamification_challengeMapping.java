package cn.ibizlab.odoo.odoo_gamification.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge;
import cn.ibizlab.odoo.odoo_gamification.dto.Gamification_challengeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Gamification_challengeMapping extends MappingBase<Gamification_challengeDTO, Gamification_challenge> {


}

