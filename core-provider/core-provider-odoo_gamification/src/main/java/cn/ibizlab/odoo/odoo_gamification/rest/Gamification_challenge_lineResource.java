package cn.ibizlab.odoo.odoo_gamification.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_gamification.dto.*;
import cn.ibizlab.odoo.odoo_gamification.mapping.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_challenge_lineService;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challenge_lineSearchContext;




@Slf4j
@Api(tags = {"Gamification_challenge_line" })
@RestController("odoo_gamification-gamification_challenge_line")
@RequestMapping("")
public class Gamification_challenge_lineResource {

    @Autowired
    private IGamification_challenge_lineService gamification_challenge_lineService;

    @Autowired
    @Lazy
    private Gamification_challenge_lineMapping gamification_challenge_lineMapping;




    @PreAuthorize("hasPermission('Remove',{#gamification_challenge_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Gamification_challenge_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenge_lines/{gamification_challenge_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_challenge_line_id") Integer gamification_challenge_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_challenge_lineService.remove(gamification_challenge_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Gamification_challenge_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenge_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        gamification_challenge_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Gamification_challenge_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenge_lines")

    public ResponseEntity<Gamification_challenge_lineDTO> create(@RequestBody Gamification_challenge_lineDTO gamification_challenge_linedto) {
        Gamification_challenge_line domain = gamification_challenge_lineMapping.toDomain(gamification_challenge_linedto);
		gamification_challenge_lineService.create(domain);
        Gamification_challenge_lineDTO dto = gamification_challenge_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Gamification_challenge_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenge_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_challenge_lineDTO> gamification_challenge_linedtos) {
        gamification_challenge_lineService.createBatch(gamification_challenge_lineMapping.toDomain(gamification_challenge_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#gamification_challenge_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Gamification_challenge_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_challenge_lines/{gamification_challenge_line_id}")
    public ResponseEntity<Gamification_challenge_lineDTO> get(@PathVariable("gamification_challenge_line_id") Integer gamification_challenge_line_id) {
        Gamification_challenge_line domain = gamification_challenge_lineService.get(gamification_challenge_line_id);
        Gamification_challenge_lineDTO dto = gamification_challenge_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#gamification_challenge_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Gamification_challenge_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenge_lines/{gamification_challenge_line_id}")

    public ResponseEntity<Gamification_challenge_lineDTO> update(@PathVariable("gamification_challenge_line_id") Integer gamification_challenge_line_id, @RequestBody Gamification_challenge_lineDTO gamification_challenge_linedto) {
		Gamification_challenge_line domain = gamification_challenge_lineMapping.toDomain(gamification_challenge_linedto);
        domain.setId(gamification_challenge_line_id);
		gamification_challenge_lineService.update(domain);
		Gamification_challenge_lineDTO dto = gamification_challenge_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#gamification_challenge_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Gamification_challenge_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenge_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_challenge_lineDTO> gamification_challenge_linedtos) {
        gamification_challenge_lineService.updateBatch(gamification_challenge_lineMapping.toDomain(gamification_challenge_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Gamification_challenge_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_challenge_lines/fetchdefault")
	public ResponseEntity<List<Gamification_challenge_lineDTO>> fetchDefault(Gamification_challenge_lineSearchContext context) {
        Page<Gamification_challenge_line> domains = gamification_challenge_lineService.searchDefault(context) ;
        List<Gamification_challenge_lineDTO> list = gamification_challenge_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Gamification_challenge_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_challenge_lines/searchdefault")
	public ResponseEntity<Page<Gamification_challenge_lineDTO>> searchDefault(Gamification_challenge_lineSearchContext context) {
        Page<Gamification_challenge_line> domains = gamification_challenge_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_challenge_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Gamification_challenge_line getEntity(){
        return new Gamification_challenge_line();
    }

}
