package cn.ibizlab.odoo.odoo_gamification.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_gamification")
public class odoo_gamificationRestConfiguration {

}
