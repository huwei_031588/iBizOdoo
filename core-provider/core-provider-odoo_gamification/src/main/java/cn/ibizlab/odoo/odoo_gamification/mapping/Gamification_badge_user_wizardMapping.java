package cn.ibizlab.odoo.odoo_gamification.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.odoo.odoo_gamification.dto.Gamification_badge_user_wizardDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Gamification_badge_user_wizardMapping extends MappingBase<Gamification_badge_user_wizardDTO, Gamification_badge_user_wizard> {


}

