package cn.ibizlab.odoo.odoo_resource.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_resource.dto.*;
import cn.ibizlab.odoo.odoo_resource.mapping.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendarService;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendarSearchContext;




@Slf4j
@Api(tags = {"Resource_calendar" })
@RestController("odoo_resource-resource_calendar")
@RequestMapping("")
public class Resource_calendarResource {

    @Autowired
    private IResource_calendarService resource_calendarService;

    @Autowired
    @Lazy
    private Resource_calendarMapping resource_calendarMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Resource_calendar" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendars")

    public ResponseEntity<Resource_calendarDTO> create(@RequestBody Resource_calendarDTO resource_calendardto) {
        Resource_calendar domain = resource_calendarMapping.toDomain(resource_calendardto);
		resource_calendarService.create(domain);
        Resource_calendarDTO dto = resource_calendarMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Resource_calendar" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendars/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_calendarDTO> resource_calendardtos) {
        resource_calendarService.createBatch(resource_calendarMapping.toDomain(resource_calendardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#resource_calendar_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Resource_calendar" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_calendars/{resource_calendar_id}")
    public ResponseEntity<Resource_calendarDTO> get(@PathVariable("resource_calendar_id") Integer resource_calendar_id) {
        Resource_calendar domain = resource_calendarService.get(resource_calendar_id);
        Resource_calendarDTO dto = resource_calendarMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#resource_calendar_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Resource_calendar" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendars/{resource_calendar_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_calendar_id") Integer resource_calendar_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_calendarService.remove(resource_calendar_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Resource_calendar" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendars/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        resource_calendarService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#resource_calendar_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Resource_calendar" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendars/{resource_calendar_id}")

    public ResponseEntity<Resource_calendarDTO> update(@PathVariable("resource_calendar_id") Integer resource_calendar_id, @RequestBody Resource_calendarDTO resource_calendardto) {
		Resource_calendar domain = resource_calendarMapping.toDomain(resource_calendardto);
        domain.setId(resource_calendar_id);
		resource_calendarService.update(domain);
		Resource_calendarDTO dto = resource_calendarMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#resource_calendar_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Resource_calendar" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendars/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_calendarDTO> resource_calendardtos) {
        resource_calendarService.updateBatch(resource_calendarMapping.toDomain(resource_calendardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Resource_calendar" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_calendars/fetchdefault")
	public ResponseEntity<List<Resource_calendarDTO>> fetchDefault(Resource_calendarSearchContext context) {
        Page<Resource_calendar> domains = resource_calendarService.searchDefault(context) ;
        List<Resource_calendarDTO> list = resource_calendarMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Resource_calendar" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_calendars/searchdefault")
	public ResponseEntity<Page<Resource_calendarDTO>> searchDefault(Resource_calendarSearchContext context) {
        Page<Resource_calendar> domains = resource_calendarService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_calendarMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Resource_calendar getEntity(){
        return new Resource_calendar();
    }

}
