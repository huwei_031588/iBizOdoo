package cn.ibizlab.odoo.odoo_resource.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_resource.dto.*;
import cn.ibizlab.odoo.odoo_resource.mapping.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_mixinService;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_mixinSearchContext;




@Slf4j
@Api(tags = {"Resource_mixin" })
@RestController("odoo_resource-resource_mixin")
@RequestMapping("")
public class Resource_mixinResource {

    @Autowired
    private IResource_mixinService resource_mixinService;

    @Autowired
    @Lazy
    private Resource_mixinMapping resource_mixinMapping;




    @PreAuthorize("hasPermission(#resource_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Resource_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_mixins/{resource_mixin_id}")

    public ResponseEntity<Resource_mixinDTO> update(@PathVariable("resource_mixin_id") Integer resource_mixin_id, @RequestBody Resource_mixinDTO resource_mixindto) {
		Resource_mixin domain = resource_mixinMapping.toDomain(resource_mixindto);
        domain.setId(resource_mixin_id);
		resource_mixinService.update(domain);
		Resource_mixinDTO dto = resource_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#resource_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Resource_mixin" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_mixinDTO> resource_mixindtos) {
        resource_mixinService.updateBatch(resource_mixinMapping.toDomain(resource_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#resource_mixin_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Resource_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_mixins/{resource_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_mixin_id") Integer resource_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_mixinService.remove(resource_mixin_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Resource_mixin" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        resource_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#resource_mixin_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Resource_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_mixins/{resource_mixin_id}")
    public ResponseEntity<Resource_mixinDTO> get(@PathVariable("resource_mixin_id") Integer resource_mixin_id) {
        Resource_mixin domain = resource_mixinService.get(resource_mixin_id);
        Resource_mixinDTO dto = resource_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Resource_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_mixins")

    public ResponseEntity<Resource_mixinDTO> create(@RequestBody Resource_mixinDTO resource_mixindto) {
        Resource_mixin domain = resource_mixinMapping.toDomain(resource_mixindto);
		resource_mixinService.create(domain);
        Resource_mixinDTO dto = resource_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Resource_mixin" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_mixinDTO> resource_mixindtos) {
        resource_mixinService.createBatch(resource_mixinMapping.toDomain(resource_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Resource_mixin" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_mixins/fetchdefault")
	public ResponseEntity<List<Resource_mixinDTO>> fetchDefault(Resource_mixinSearchContext context) {
        Page<Resource_mixin> domains = resource_mixinService.searchDefault(context) ;
        List<Resource_mixinDTO> list = resource_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Resource_mixin" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_mixins/searchdefault")
	public ResponseEntity<Page<Resource_mixinDTO>> searchDefault(Resource_mixinSearchContext context) {
        Page<Resource_mixin> domains = resource_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Resource_mixin getEntity(){
        return new Resource_mixin();
    }

}
