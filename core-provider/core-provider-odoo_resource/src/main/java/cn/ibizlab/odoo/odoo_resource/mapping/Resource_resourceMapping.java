package cn.ibizlab.odoo.odoo_resource.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.odoo.odoo_resource.dto.Resource_resourceDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Resource_resourceMapping extends MappingBase<Resource_resourceDTO, Resource_resource> {


}

