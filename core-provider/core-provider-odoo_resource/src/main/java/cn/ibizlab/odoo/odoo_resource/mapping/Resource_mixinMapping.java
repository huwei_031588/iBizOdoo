package cn.ibizlab.odoo.odoo_resource.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.odoo.odoo_resource.dto.Resource_mixinDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Resource_mixinMapping extends MappingBase<Resource_mixinDTO, Resource_mixin> {


}

