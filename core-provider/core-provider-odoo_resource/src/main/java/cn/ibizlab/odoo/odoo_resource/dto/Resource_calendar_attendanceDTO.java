package cn.ibizlab.odoo.odoo_resource.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Resource_calendar_attendanceDTO]
 */
@Data
public class Resource_calendar_attendanceDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DAYOFWEEK]
     *
     */
    @JSONField(name = "dayofweek")
    @JsonProperty("dayofweek")
    private String dayofweek;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [HOUR_TO]
     *
     */
    @JSONField(name = "hour_to")
    @JsonProperty("hour_to")
    private Double hourTo;

    /**
     * 属性 [DAY_PERIOD]
     *
     */
    @JSONField(name = "day_period")
    @JsonProperty("day_period")
    private String dayPeriod;

    /**
     * 属性 [HOUR_FROM]
     *
     */
    @JSONField(name = "hour_from")
    @JsonProperty("hour_from")
    private Double hourFrom;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [DATE_TO]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_to" , format="yyyy-MM-dd")
    @JsonProperty("date_to")
    private Timestamp dateTo;

    /**
     * 属性 [DATE_FROM]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_from" , format="yyyy-MM-dd")
    @JsonProperty("date_from")
    private Timestamp dateFrom;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [CALENDAR_ID_TEXT]
     *
     */
    @JSONField(name = "calendar_id_text")
    @JsonProperty("calendar_id_text")
    private String calendarIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CALENDAR_ID]
     *
     */
    @JSONField(name = "calendar_id")
    @JsonProperty("calendar_id")
    private Integer calendarId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [DAYOFWEEK]
     */
    public void setDayofweek(String  dayofweek){
        this.dayofweek = dayofweek ;
        this.modify("dayofweek",dayofweek);
    }

    /**
     * 设置 [HOUR_TO]
     */
    public void setHourTo(Double  hourTo){
        this.hourTo = hourTo ;
        this.modify("hour_to",hourTo);
    }

    /**
     * 设置 [DAY_PERIOD]
     */
    public void setDayPeriod(String  dayPeriod){
        this.dayPeriod = dayPeriod ;
        this.modify("day_period",dayPeriod);
    }

    /**
     * 设置 [HOUR_FROM]
     */
    public void setHourFrom(Double  hourFrom){
        this.hourFrom = hourFrom ;
        this.modify("hour_from",hourFrom);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [DATE_TO]
     */
    public void setDateTo(Timestamp  dateTo){
        this.dateTo = dateTo ;
        this.modify("date_to",dateTo);
    }

    /**
     * 设置 [DATE_FROM]
     */
    public void setDateFrom(Timestamp  dateFrom){
        this.dateFrom = dateFrom ;
        this.modify("date_from",dateFrom);
    }

    /**
     * 设置 [CALENDAR_ID]
     */
    public void setCalendarId(Integer  calendarId){
        this.calendarId = calendarId ;
        this.modify("calendar_id",calendarId);
    }


}

