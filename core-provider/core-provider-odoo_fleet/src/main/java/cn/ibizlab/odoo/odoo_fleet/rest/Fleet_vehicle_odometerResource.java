package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_odometer;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_odometerService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_odometerSearchContext;




@Slf4j
@Api(tags = {"Fleet_vehicle_odometer" })
@RestController("odoo_fleet-fleet_vehicle_odometer")
@RequestMapping("")
public class Fleet_vehicle_odometerResource {

    @Autowired
    private IFleet_vehicle_odometerService fleet_vehicle_odometerService;

    @Autowired
    @Lazy
    private Fleet_vehicle_odometerMapping fleet_vehicle_odometerMapping;










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_odometer" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers")

    public ResponseEntity<Fleet_vehicle_odometerDTO> create(@RequestBody Fleet_vehicle_odometerDTO fleet_vehicle_odometerdto) {
        Fleet_vehicle_odometer domain = fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdto);
		fleet_vehicle_odometerService.create(domain);
        Fleet_vehicle_odometerDTO dto = fleet_vehicle_odometerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_vehicle_odometer" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_odometerDTO> fleet_vehicle_odometerdtos) {
        fleet_vehicle_odometerService.createBatch(fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#fleet_vehicle_odometer_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_odometer" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_odometers/{fleet_vehicle_odometer_id}")

    public ResponseEntity<Fleet_vehicle_odometerDTO> update(@PathVariable("fleet_vehicle_odometer_id") Integer fleet_vehicle_odometer_id, @RequestBody Fleet_vehicle_odometerDTO fleet_vehicle_odometerdto) {
		Fleet_vehicle_odometer domain = fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdto);
        domain.setId(fleet_vehicle_odometer_id);
		fleet_vehicle_odometerService.update(domain);
		Fleet_vehicle_odometerDTO dto = fleet_vehicle_odometerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_vehicle_odometer_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_vehicle_odometer" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_odometers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_odometerDTO> fleet_vehicle_odometerdtos) {
        fleet_vehicle_odometerService.updateBatch(fleet_vehicle_odometerMapping.toDomain(fleet_vehicle_odometerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#fleet_vehicle_odometer_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_odometer" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_odometers/{fleet_vehicle_odometer_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_odometer_id") Integer fleet_vehicle_odometer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_odometerService.remove(fleet_vehicle_odometer_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_vehicle_odometer" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_odometers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_vehicle_odometerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#fleet_vehicle_odometer_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_odometer" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_odometers/{fleet_vehicle_odometer_id}")
    public ResponseEntity<Fleet_vehicle_odometerDTO> get(@PathVariable("fleet_vehicle_odometer_id") Integer fleet_vehicle_odometer_id) {
        Fleet_vehicle_odometer domain = fleet_vehicle_odometerService.get(fleet_vehicle_odometer_id);
        Fleet_vehicle_odometerDTO dto = fleet_vehicle_odometerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_vehicle_odometer" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_odometers/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_odometerDTO>> fetchDefault(Fleet_vehicle_odometerSearchContext context) {
        Page<Fleet_vehicle_odometer> domains = fleet_vehicle_odometerService.searchDefault(context) ;
        List<Fleet_vehicle_odometerDTO> list = fleet_vehicle_odometerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_vehicle_odometer" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_odometers/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_odometerDTO>> searchDefault(Fleet_vehicle_odometerSearchContext context) {
        Page<Fleet_vehicle_odometer> domains = fleet_vehicle_odometerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_odometerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_vehicle_odometer getEntity(){
        return new Fleet_vehicle_odometer();
    }

}
