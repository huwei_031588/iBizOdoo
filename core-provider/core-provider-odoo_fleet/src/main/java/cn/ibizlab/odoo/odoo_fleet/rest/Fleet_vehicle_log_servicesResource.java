package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_services;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_log_servicesService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_servicesSearchContext;




@Slf4j
@Api(tags = {"Fleet_vehicle_log_services" })
@RestController("odoo_fleet-fleet_vehicle_log_services")
@RequestMapping("")
public class Fleet_vehicle_log_servicesResource {

    @Autowired
    private IFleet_vehicle_log_servicesService fleet_vehicle_log_servicesService;

    @Autowired
    @Lazy
    private Fleet_vehicle_log_servicesMapping fleet_vehicle_log_servicesMapping;













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_log_services" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_services")

    public ResponseEntity<Fleet_vehicle_log_servicesDTO> create(@RequestBody Fleet_vehicle_log_servicesDTO fleet_vehicle_log_servicesdto) {
        Fleet_vehicle_log_services domain = fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdto);
		fleet_vehicle_log_servicesService.create(domain);
        Fleet_vehicle_log_servicesDTO dto = fleet_vehicle_log_servicesMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_vehicle_log_services" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_services/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_log_servicesDTO> fleet_vehicle_log_servicesdtos) {
        fleet_vehicle_log_servicesService.createBatch(fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#fleet_vehicle_log_services_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_log_services" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_services/{fleet_vehicle_log_services_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_log_services_id") Integer fleet_vehicle_log_services_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_servicesService.remove(fleet_vehicle_log_services_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_vehicle_log_services" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_services/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_vehicle_log_servicesService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#fleet_vehicle_log_services_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_log_services" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_services/{fleet_vehicle_log_services_id}")

    public ResponseEntity<Fleet_vehicle_log_servicesDTO> update(@PathVariable("fleet_vehicle_log_services_id") Integer fleet_vehicle_log_services_id, @RequestBody Fleet_vehicle_log_servicesDTO fleet_vehicle_log_servicesdto) {
		Fleet_vehicle_log_services domain = fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdto);
        domain.setId(fleet_vehicle_log_services_id);
		fleet_vehicle_log_servicesService.update(domain);
		Fleet_vehicle_log_servicesDTO dto = fleet_vehicle_log_servicesMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_vehicle_log_services_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_vehicle_log_services" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_services/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_log_servicesDTO> fleet_vehicle_log_servicesdtos) {
        fleet_vehicle_log_servicesService.updateBatch(fleet_vehicle_log_servicesMapping.toDomain(fleet_vehicle_log_servicesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#fleet_vehicle_log_services_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_log_services" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_services/{fleet_vehicle_log_services_id}")
    public ResponseEntity<Fleet_vehicle_log_servicesDTO> get(@PathVariable("fleet_vehicle_log_services_id") Integer fleet_vehicle_log_services_id) {
        Fleet_vehicle_log_services domain = fleet_vehicle_log_servicesService.get(fleet_vehicle_log_services_id);
        Fleet_vehicle_log_servicesDTO dto = fleet_vehicle_log_servicesMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_vehicle_log_services" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_log_services/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_log_servicesDTO>> fetchDefault(Fleet_vehicle_log_servicesSearchContext context) {
        Page<Fleet_vehicle_log_services> domains = fleet_vehicle_log_servicesService.searchDefault(context) ;
        List<Fleet_vehicle_log_servicesDTO> list = fleet_vehicle_log_servicesMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_vehicle_log_services" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_log_services/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_log_servicesDTO>> searchDefault(Fleet_vehicle_log_servicesSearchContext context) {
        Page<Fleet_vehicle_log_services> domains = fleet_vehicle_log_servicesService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_log_servicesMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_vehicle_log_services getEntity(){
        return new Fleet_vehicle_log_services();
    }

}
