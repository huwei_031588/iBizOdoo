package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_assignation_log;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_assignation_logService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_assignation_logSearchContext;




@Slf4j
@Api(tags = {"Fleet_vehicle_assignation_log" })
@RestController("odoo_fleet-fleet_vehicle_assignation_log")
@RequestMapping("")
public class Fleet_vehicle_assignation_logResource {

    @Autowired
    private IFleet_vehicle_assignation_logService fleet_vehicle_assignation_logService;

    @Autowired
    @Lazy
    private Fleet_vehicle_assignation_logMapping fleet_vehicle_assignation_logMapping;










    @PreAuthorize("hasPermission(#fleet_vehicle_assignation_log_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_assignation_logs/{fleet_vehicle_assignation_log_id}")
    public ResponseEntity<Fleet_vehicle_assignation_logDTO> get(@PathVariable("fleet_vehicle_assignation_log_id") Integer fleet_vehicle_assignation_log_id) {
        Fleet_vehicle_assignation_log domain = fleet_vehicle_assignation_logService.get(fleet_vehicle_assignation_log_id);
        Fleet_vehicle_assignation_logDTO dto = fleet_vehicle_assignation_logMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs")

    public ResponseEntity<Fleet_vehicle_assignation_logDTO> create(@RequestBody Fleet_vehicle_assignation_logDTO fleet_vehicle_assignation_logdto) {
        Fleet_vehicle_assignation_log domain = fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdto);
		fleet_vehicle_assignation_logService.create(domain);
        Fleet_vehicle_assignation_logDTO dto = fleet_vehicle_assignation_logMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_vehicle_assignation_log" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_assignation_logDTO> fleet_vehicle_assignation_logdtos) {
        fleet_vehicle_assignation_logService.createBatch(fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#fleet_vehicle_assignation_log_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_assignation_logs/{fleet_vehicle_assignation_log_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_assignation_log_id") Integer fleet_vehicle_assignation_log_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_assignation_logService.remove(fleet_vehicle_assignation_log_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_vehicle_assignation_log" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_assignation_logs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_vehicle_assignation_logService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#fleet_vehicle_assignation_log_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_assignation_logs/{fleet_vehicle_assignation_log_id}")

    public ResponseEntity<Fleet_vehicle_assignation_logDTO> update(@PathVariable("fleet_vehicle_assignation_log_id") Integer fleet_vehicle_assignation_log_id, @RequestBody Fleet_vehicle_assignation_logDTO fleet_vehicle_assignation_logdto) {
		Fleet_vehicle_assignation_log domain = fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdto);
        domain.setId(fleet_vehicle_assignation_log_id);
		fleet_vehicle_assignation_logService.update(domain);
		Fleet_vehicle_assignation_logDTO dto = fleet_vehicle_assignation_logMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_vehicle_assignation_log_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_vehicle_assignation_log" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_assignation_logs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_assignation_logDTO> fleet_vehicle_assignation_logdtos) {
        fleet_vehicle_assignation_logService.updateBatch(fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_vehicle_assignation_log" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_assignation_logs/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_assignation_logDTO>> fetchDefault(Fleet_vehicle_assignation_logSearchContext context) {
        Page<Fleet_vehicle_assignation_log> domains = fleet_vehicle_assignation_logService.searchDefault(context) ;
        List<Fleet_vehicle_assignation_logDTO> list = fleet_vehicle_assignation_logMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_vehicle_assignation_log" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_assignation_logs/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_assignation_logDTO>> searchDefault(Fleet_vehicle_assignation_logSearchContext context) {
        Page<Fleet_vehicle_assignation_log> domains = fleet_vehicle_assignation_logService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_assignation_logMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_vehicle_assignation_log getEntity(){
        return new Fleet_vehicle_assignation_log();
    }

}
