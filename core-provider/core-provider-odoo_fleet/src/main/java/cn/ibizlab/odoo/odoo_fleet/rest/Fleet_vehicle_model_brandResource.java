package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model_brand;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_model_brandService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_model_brandSearchContext;




@Slf4j
@Api(tags = {"Fleet_vehicle_model_brand" })
@RestController("odoo_fleet-fleet_vehicle_model_brand")
@RequestMapping("")
public class Fleet_vehicle_model_brandResource {

    @Autowired
    private IFleet_vehicle_model_brandService fleet_vehicle_model_brandService;

    @Autowired
    @Lazy
    private Fleet_vehicle_model_brandMapping fleet_vehicle_model_brandMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_model_brand" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands")

    public ResponseEntity<Fleet_vehicle_model_brandDTO> create(@RequestBody Fleet_vehicle_model_brandDTO fleet_vehicle_model_branddto) {
        Fleet_vehicle_model_brand domain = fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddto);
		fleet_vehicle_model_brandService.create(domain);
        Fleet_vehicle_model_brandDTO dto = fleet_vehicle_model_brandMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_vehicle_model_brand" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_model_brandDTO> fleet_vehicle_model_branddtos) {
        fleet_vehicle_model_brandService.createBatch(fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#fleet_vehicle_model_brand_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_model_brand" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_model_brands/{fleet_vehicle_model_brand_id}")

    public ResponseEntity<Fleet_vehicle_model_brandDTO> update(@PathVariable("fleet_vehicle_model_brand_id") Integer fleet_vehicle_model_brand_id, @RequestBody Fleet_vehicle_model_brandDTO fleet_vehicle_model_branddto) {
		Fleet_vehicle_model_brand domain = fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddto);
        domain.setId(fleet_vehicle_model_brand_id);
		fleet_vehicle_model_brandService.update(domain);
		Fleet_vehicle_model_brandDTO dto = fleet_vehicle_model_brandMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_vehicle_model_brand_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_vehicle_model_brand" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_model_brands/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_model_brandDTO> fleet_vehicle_model_branddtos) {
        fleet_vehicle_model_brandService.updateBatch(fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#fleet_vehicle_model_brand_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_model_brand" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_model_brands/{fleet_vehicle_model_brand_id}")
    public ResponseEntity<Fleet_vehicle_model_brandDTO> get(@PathVariable("fleet_vehicle_model_brand_id") Integer fleet_vehicle_model_brand_id) {
        Fleet_vehicle_model_brand domain = fleet_vehicle_model_brandService.get(fleet_vehicle_model_brand_id);
        Fleet_vehicle_model_brandDTO dto = fleet_vehicle_model_brandMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#fleet_vehicle_model_brand_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_model_brand" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_model_brands/{fleet_vehicle_model_brand_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_model_brand_id") Integer fleet_vehicle_model_brand_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_model_brandService.remove(fleet_vehicle_model_brand_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_vehicle_model_brand" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_model_brands/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_vehicle_model_brandService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_vehicle_model_brand" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_model_brands/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_model_brandDTO>> fetchDefault(Fleet_vehicle_model_brandSearchContext context) {
        Page<Fleet_vehicle_model_brand> domains = fleet_vehicle_model_brandService.searchDefault(context) ;
        List<Fleet_vehicle_model_brandDTO> list = fleet_vehicle_model_brandMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_vehicle_model_brand" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_model_brands/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_model_brandDTO>> searchDefault(Fleet_vehicle_model_brandSearchContext context) {
        Page<Fleet_vehicle_model_brand> domains = fleet_vehicle_model_brandService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_model_brandMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_vehicle_model_brand getEntity(){
        return new Fleet_vehicle_model_brand();
    }

}
