package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicleService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicleSearchContext;




@Slf4j
@Api(tags = {"Fleet_vehicle" })
@RestController("odoo_fleet-fleet_vehicle")
@RequestMapping("")
public class Fleet_vehicleResource {

    @Autowired
    private IFleet_vehicleService fleet_vehicleService;

    @Autowired
    @Lazy
    private Fleet_vehicleMapping fleet_vehicleMapping;




    @PreAuthorize("hasPermission('Remove',{#fleet_vehicle_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicles/{fleet_vehicle_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_id") Integer fleet_vehicle_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicleService.remove(fleet_vehicle_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_vehicle" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_vehicleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles")

    public ResponseEntity<Fleet_vehicleDTO> create(@RequestBody Fleet_vehicleDTO fleet_vehicledto) {
        Fleet_vehicle domain = fleet_vehicleMapping.toDomain(fleet_vehicledto);
		fleet_vehicleService.create(domain);
        Fleet_vehicleDTO dto = fleet_vehicleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_vehicle" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicleDTO> fleet_vehicledtos) {
        fleet_vehicleService.createBatch(fleet_vehicleMapping.toDomain(fleet_vehicledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#fleet_vehicle_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicles/{fleet_vehicle_id}")

    public ResponseEntity<Fleet_vehicleDTO> update(@PathVariable("fleet_vehicle_id") Integer fleet_vehicle_id, @RequestBody Fleet_vehicleDTO fleet_vehicledto) {
		Fleet_vehicle domain = fleet_vehicleMapping.toDomain(fleet_vehicledto);
        domain.setId(fleet_vehicle_id);
		fleet_vehicleService.update(domain);
		Fleet_vehicleDTO dto = fleet_vehicleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_vehicle_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_vehicle" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicleDTO> fleet_vehicledtos) {
        fleet_vehicleService.updateBatch(fleet_vehicleMapping.toDomain(fleet_vehicledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#fleet_vehicle_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicles/{fleet_vehicle_id}")
    public ResponseEntity<Fleet_vehicleDTO> get(@PathVariable("fleet_vehicle_id") Integer fleet_vehicle_id) {
        Fleet_vehicle domain = fleet_vehicleService.get(fleet_vehicle_id);
        Fleet_vehicleDTO dto = fleet_vehicleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_vehicle" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicles/fetchdefault")
	public ResponseEntity<List<Fleet_vehicleDTO>> fetchDefault(Fleet_vehicleSearchContext context) {
        Page<Fleet_vehicle> domains = fleet_vehicleService.searchDefault(context) ;
        List<Fleet_vehicleDTO> list = fleet_vehicleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_vehicle" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicles/searchdefault")
	public ResponseEntity<Page<Fleet_vehicleDTO>> searchDefault(Fleet_vehicleSearchContext context) {
        Page<Fleet_vehicle> domains = fleet_vehicleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_vehicle getEntity(){
        return new Fleet_vehicle();
    }

}
