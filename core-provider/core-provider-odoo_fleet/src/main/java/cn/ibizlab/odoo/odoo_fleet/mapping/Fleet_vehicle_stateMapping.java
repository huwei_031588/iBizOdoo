package cn.ibizlab.odoo.odoo_fleet.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_state;
import cn.ibizlab.odoo.odoo_fleet.dto.Fleet_vehicle_stateDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Fleet_vehicle_stateMapping extends MappingBase<Fleet_vehicle_stateDTO, Fleet_vehicle_state> {


}

