package cn.ibizlab.odoo.odoo_fleet.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_odometer;
import cn.ibizlab.odoo.odoo_fleet.dto.Fleet_vehicle_odometerDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Fleet_vehicle_odometerMapping extends MappingBase<Fleet_vehicle_odometerDTO, Fleet_vehicle_odometer> {


}

