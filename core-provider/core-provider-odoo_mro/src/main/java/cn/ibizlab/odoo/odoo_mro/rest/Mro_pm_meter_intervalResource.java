package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_interval;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meter_intervalService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_intervalSearchContext;




@Slf4j
@Api(tags = {"Mro_pm_meter_interval" })
@RestController("odoo_mro-mro_pm_meter_interval")
@RequestMapping("")
public class Mro_pm_meter_intervalResource {

    @Autowired
    private IMro_pm_meter_intervalService mro_pm_meter_intervalService;

    @Autowired
    @Lazy
    private Mro_pm_meter_intervalMapping mro_pm_meter_intervalMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_pm_meter_interval" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals")

    public ResponseEntity<Mro_pm_meter_intervalDTO> create(@RequestBody Mro_pm_meter_intervalDTO mro_pm_meter_intervaldto) {
        Mro_pm_meter_interval domain = mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldto);
		mro_pm_meter_intervalService.create(domain);
        Mro_pm_meter_intervalDTO dto = mro_pm_meter_intervalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_pm_meter_interval" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_meter_intervalDTO> mro_pm_meter_intervaldtos) {
        mro_pm_meter_intervalService.createBatch(mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mro_pm_meter_interval_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_pm_meter_interval" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_intervals/{mro_pm_meter_interval_id}")
    public ResponseEntity<Mro_pm_meter_intervalDTO> get(@PathVariable("mro_pm_meter_interval_id") Integer mro_pm_meter_interval_id) {
        Mro_pm_meter_interval domain = mro_pm_meter_intervalService.get(mro_pm_meter_interval_id);
        Mro_pm_meter_intervalDTO dto = mro_pm_meter_intervalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#mro_pm_meter_interval_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_pm_meter_interval" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_intervals/{mro_pm_meter_interval_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_meter_interval_id") Integer mro_pm_meter_interval_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meter_intervalService.remove(mro_pm_meter_interval_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_pm_meter_interval" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_intervals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_pm_meter_intervalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mro_pm_meter_interval_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_pm_meter_interval" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_intervals/{mro_pm_meter_interval_id}")

    public ResponseEntity<Mro_pm_meter_intervalDTO> update(@PathVariable("mro_pm_meter_interval_id") Integer mro_pm_meter_interval_id, @RequestBody Mro_pm_meter_intervalDTO mro_pm_meter_intervaldto) {
		Mro_pm_meter_interval domain = mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldto);
        domain.setId(mro_pm_meter_interval_id);
		mro_pm_meter_intervalService.update(domain);
		Mro_pm_meter_intervalDTO dto = mro_pm_meter_intervalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_pm_meter_interval_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_pm_meter_interval" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_intervals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_meter_intervalDTO> mro_pm_meter_intervaldtos) {
        mro_pm_meter_intervalService.updateBatch(mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_pm_meter_interval" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_meter_intervals/fetchdefault")
	public ResponseEntity<List<Mro_pm_meter_intervalDTO>> fetchDefault(Mro_pm_meter_intervalSearchContext context) {
        Page<Mro_pm_meter_interval> domains = mro_pm_meter_intervalService.searchDefault(context) ;
        List<Mro_pm_meter_intervalDTO> list = mro_pm_meter_intervalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_pm_meter_interval" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_meter_intervals/searchdefault")
	public ResponseEntity<Page<Mro_pm_meter_intervalDTO>> searchDefault(Mro_pm_meter_intervalSearchContext context) {
        Page<Mro_pm_meter_interval> domains = mro_pm_meter_intervalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_meter_intervalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_pm_meter_interval getEntity(){
        return new Mro_pm_meter_interval();
    }

}
