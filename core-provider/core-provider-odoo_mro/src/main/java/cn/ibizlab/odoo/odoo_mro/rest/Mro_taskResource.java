package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_taskService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_taskSearchContext;




@Slf4j
@Api(tags = {"Mro_task" })
@RestController("odoo_mro-mro_task")
@RequestMapping("")
public class Mro_taskResource {

    @Autowired
    private IMro_taskService mro_taskService;

    @Autowired
    @Lazy
    private Mro_taskMapping mro_taskMapping;







    @PreAuthorize("hasPermission(#mro_task_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_task" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_tasks/{mro_task_id}")

    public ResponseEntity<Mro_taskDTO> update(@PathVariable("mro_task_id") Integer mro_task_id, @RequestBody Mro_taskDTO mro_taskdto) {
		Mro_task domain = mro_taskMapping.toDomain(mro_taskdto);
        domain.setId(mro_task_id);
		mro_taskService.update(domain);
		Mro_taskDTO dto = mro_taskMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_task_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_task" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_tasks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_taskDTO> mro_taskdtos) {
        mro_taskService.updateBatch(mro_taskMapping.toDomain(mro_taskdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mro_task_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_task" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_tasks/{mro_task_id}")
    public ResponseEntity<Mro_taskDTO> get(@PathVariable("mro_task_id") Integer mro_task_id) {
        Mro_task domain = mro_taskService.get(mro_task_id);
        Mro_taskDTO dto = mro_taskMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#mro_task_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_task" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_tasks/{mro_task_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_task_id") Integer mro_task_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_taskService.remove(mro_task_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_task" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_tasks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_taskService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_task" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_tasks")

    public ResponseEntity<Mro_taskDTO> create(@RequestBody Mro_taskDTO mro_taskdto) {
        Mro_task domain = mro_taskMapping.toDomain(mro_taskdto);
		mro_taskService.create(domain);
        Mro_taskDTO dto = mro_taskMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_task" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_tasks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_taskDTO> mro_taskdtos) {
        mro_taskService.createBatch(mro_taskMapping.toDomain(mro_taskdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_task" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_tasks/fetchdefault")
	public ResponseEntity<List<Mro_taskDTO>> fetchDefault(Mro_taskSearchContext context) {
        Page<Mro_task> domains = mro_taskService.searchDefault(context) ;
        List<Mro_taskDTO> list = mro_taskMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_task" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_tasks/searchdefault")
	public ResponseEntity<Page<Mro_taskDTO>> searchDefault(Mro_taskSearchContext context) {
        Page<Mro_task> domains = mro_taskService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_taskMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_task getEntity(){
        return new Mro_task();
    }

}
