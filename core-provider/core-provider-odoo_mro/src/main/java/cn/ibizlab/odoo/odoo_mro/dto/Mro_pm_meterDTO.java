package cn.ibizlab.odoo.odoo_mro.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mro_pm_meterDTO]
 */
@Data
public class Mro_pm_meterDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [MIN_UTILIZATION]
     *
     */
    @JSONField(name = "min_utilization")
    @JsonProperty("min_utilization")
    private Double minUtilization;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [READING_TYPE]
     *
     */
    @JSONField(name = "reading_type")
    @JsonProperty("reading_type")
    private String readingType;

    /**
     * 属性 [VALUE]
     *
     */
    @JSONField(name = "value")
    @JsonProperty("value")
    private Double value;

    /**
     * 属性 [METER_LINE_IDS]
     *
     */
    @JSONField(name = "meter_line_ids")
    @JsonProperty("meter_line_ids")
    private String meterLineIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [VIEW_LINE_IDS]
     *
     */
    @JSONField(name = "view_line_ids")
    @JsonProperty("view_line_ids")
    private String viewLineIds;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [UTILIZATION]
     *
     */
    @JSONField(name = "utilization")
    @JsonProperty("utilization")
    private Double utilization;

    /**
     * 属性 [NEW_VALUE]
     *
     */
    @JSONField(name = "new_value")
    @JsonProperty("new_value")
    private Double newValue;

    /**
     * 属性 [AV_TIME]
     *
     */
    @JSONField(name = "av_time")
    @JsonProperty("av_time")
    private Double avTime;

    /**
     * 属性 [TOTAL_VALUE]
     *
     */
    @JSONField(name = "total_value")
    @JsonProperty("total_value")
    private Double totalValue;

    /**
     * 属性 [NAME_TEXT]
     *
     */
    @JSONField(name = "name_text")
    @JsonProperty("name_text")
    private String nameText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [METER_UOM]
     *
     */
    @JSONField(name = "meter_uom")
    @JsonProperty("meter_uom")
    private Integer meterUom;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [ASSET_ID_TEXT]
     *
     */
    @JSONField(name = "asset_id_text")
    @JsonProperty("asset_id_text")
    private String assetIdText;

    /**
     * 属性 [PARENT_RATIO_ID_TEXT]
     *
     */
    @JSONField(name = "parent_ratio_id_text")
    @JsonProperty("parent_ratio_id_text")
    private String parentRatioIdText;

    /**
     * 属性 [PARENT_METER_ID]
     *
     */
    @JSONField(name = "parent_meter_id")
    @JsonProperty("parent_meter_id")
    private Integer parentMeterId;

    /**
     * 属性 [PARENT_RATIO_ID]
     *
     */
    @JSONField(name = "parent_ratio_id")
    @JsonProperty("parent_ratio_id")
    private Integer parentRatioId;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private Integer name;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [ASSET_ID]
     *
     */
    @JSONField(name = "asset_id")
    @JsonProperty("asset_id")
    private Integer assetId;


    /**
     * 设置 [MIN_UTILIZATION]
     */
    public void setMinUtilization(Double  minUtilization){
        this.minUtilization = minUtilization ;
        this.modify("min_utilization",minUtilization);
    }

    /**
     * 设置 [READING_TYPE]
     */
    public void setReadingType(String  readingType){
        this.readingType = readingType ;
        this.modify("reading_type",readingType);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [NEW_VALUE]
     */
    public void setNewValue(Double  newValue){
        this.newValue = newValue ;
        this.modify("new_value",newValue);
    }

    /**
     * 设置 [AV_TIME]
     */
    public void setAvTime(Double  avTime){
        this.avTime = avTime ;
        this.modify("av_time",avTime);
    }

    /**
     * 设置 [PARENT_METER_ID]
     */
    public void setParentMeterId(Integer  parentMeterId){
        this.parentMeterId = parentMeterId ;
        this.modify("parent_meter_id",parentMeterId);
    }

    /**
     * 设置 [PARENT_RATIO_ID]
     */
    public void setParentRatioId(Integer  parentRatioId){
        this.parentRatioId = parentRatioId ;
        this.modify("parent_ratio_id",parentRatioId);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(Integer  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [ASSET_ID]
     */
    public void setAssetId(Integer  assetId){
        this.assetId = assetId ;
        this.modify("asset_id",assetId);
    }


}

