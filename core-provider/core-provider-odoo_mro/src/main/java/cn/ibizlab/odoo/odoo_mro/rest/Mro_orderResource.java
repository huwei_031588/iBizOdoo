package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_orderService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_orderSearchContext;




@Slf4j
@Api(tags = {"Mro_order" })
@RestController("odoo_mro-mro_order")
@RequestMapping("")
public class Mro_orderResource {

    @Autowired
    private IMro_orderService mro_orderService;

    @Autowired
    @Lazy
    private Mro_orderMapping mro_orderMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_orders")

    public ResponseEntity<Mro_orderDTO> create(@RequestBody Mro_orderDTO mro_orderdto) {
        Mro_order domain = mro_orderMapping.toDomain(mro_orderdto);
		mro_orderService.create(domain);
        Mro_orderDTO dto = mro_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_order" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_orderDTO> mro_orderdtos) {
        mro_orderService.createBatch(mro_orderMapping.toDomain(mro_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mro_order_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_orders/{mro_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_order_id") Integer mro_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_orderService.remove(mro_order_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_order" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mro_order_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_orders/{mro_order_id}")
    public ResponseEntity<Mro_orderDTO> get(@PathVariable("mro_order_id") Integer mro_order_id) {
        Mro_order domain = mro_orderService.get(mro_order_id);
        Mro_orderDTO dto = mro_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#mro_order_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_orders/{mro_order_id}")

    public ResponseEntity<Mro_orderDTO> update(@PathVariable("mro_order_id") Integer mro_order_id, @RequestBody Mro_orderDTO mro_orderdto) {
		Mro_order domain = mro_orderMapping.toDomain(mro_orderdto);
        domain.setId(mro_order_id);
		mro_orderService.update(domain);
		Mro_orderDTO dto = mro_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_order_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_order" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_orderDTO> mro_orderdtos) {
        mro_orderService.updateBatch(mro_orderMapping.toDomain(mro_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_order" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_orders/fetchdefault")
	public ResponseEntity<List<Mro_orderDTO>> fetchDefault(Mro_orderSearchContext context) {
        Page<Mro_order> domains = mro_orderService.searchDefault(context) ;
        List<Mro_orderDTO> list = mro_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_order" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_orders/searchdefault")
	public ResponseEntity<Page<Mro_orderDTO>> searchDefault(Mro_orderSearchContext context) {
        Page<Mro_order> domains = mro_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_order getEntity(){
        return new Mro_order();
    }

}
