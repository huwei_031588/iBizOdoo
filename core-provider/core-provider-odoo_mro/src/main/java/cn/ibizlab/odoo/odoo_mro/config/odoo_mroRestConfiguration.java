package cn.ibizlab.odoo.odoo_mro.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_mro")
public class odoo_mroRestConfiguration {

}
