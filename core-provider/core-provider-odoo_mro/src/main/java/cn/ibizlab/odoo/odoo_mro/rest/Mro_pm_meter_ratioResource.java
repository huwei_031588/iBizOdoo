package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_ratio;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meter_ratioService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_ratioSearchContext;




@Slf4j
@Api(tags = {"Mro_pm_meter_ratio" })
@RestController("odoo_mro-mro_pm_meter_ratio")
@RequestMapping("")
public class Mro_pm_meter_ratioResource {

    @Autowired
    private IMro_pm_meter_ratioService mro_pm_meter_ratioService;

    @Autowired
    @Lazy
    private Mro_pm_meter_ratioMapping mro_pm_meter_ratioMapping;




    @PreAuthorize("hasPermission(#mro_pm_meter_ratio_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_pm_meter_ratio" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_ratios/{mro_pm_meter_ratio_id}")
    public ResponseEntity<Mro_pm_meter_ratioDTO> get(@PathVariable("mro_pm_meter_ratio_id") Integer mro_pm_meter_ratio_id) {
        Mro_pm_meter_ratio domain = mro_pm_meter_ratioService.get(mro_pm_meter_ratio_id);
        Mro_pm_meter_ratioDTO dto = mro_pm_meter_ratioMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#mro_pm_meter_ratio_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_pm_meter_ratio" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_ratios/{mro_pm_meter_ratio_id}")

    public ResponseEntity<Mro_pm_meter_ratioDTO> update(@PathVariable("mro_pm_meter_ratio_id") Integer mro_pm_meter_ratio_id, @RequestBody Mro_pm_meter_ratioDTO mro_pm_meter_ratiodto) {
		Mro_pm_meter_ratio domain = mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodto);
        domain.setId(mro_pm_meter_ratio_id);
		mro_pm_meter_ratioService.update(domain);
		Mro_pm_meter_ratioDTO dto = mro_pm_meter_ratioMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_pm_meter_ratio_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_pm_meter_ratio" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_ratios/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_meter_ratioDTO> mro_pm_meter_ratiodtos) {
        mro_pm_meter_ratioService.updateBatch(mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mro_pm_meter_ratio_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_pm_meter_ratio" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_ratios/{mro_pm_meter_ratio_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_meter_ratio_id") Integer mro_pm_meter_ratio_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meter_ratioService.remove(mro_pm_meter_ratio_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_pm_meter_ratio" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_ratios/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_pm_meter_ratioService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_pm_meter_ratio" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_ratios")

    public ResponseEntity<Mro_pm_meter_ratioDTO> create(@RequestBody Mro_pm_meter_ratioDTO mro_pm_meter_ratiodto) {
        Mro_pm_meter_ratio domain = mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodto);
		mro_pm_meter_ratioService.create(domain);
        Mro_pm_meter_ratioDTO dto = mro_pm_meter_ratioMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_pm_meter_ratio" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_ratios/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_meter_ratioDTO> mro_pm_meter_ratiodtos) {
        mro_pm_meter_ratioService.createBatch(mro_pm_meter_ratioMapping.toDomain(mro_pm_meter_ratiodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_pm_meter_ratio" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_meter_ratios/fetchdefault")
	public ResponseEntity<List<Mro_pm_meter_ratioDTO>> fetchDefault(Mro_pm_meter_ratioSearchContext context) {
        Page<Mro_pm_meter_ratio> domains = mro_pm_meter_ratioService.searchDefault(context) ;
        List<Mro_pm_meter_ratioDTO> list = mro_pm_meter_ratioMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_pm_meter_ratio" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_meter_ratios/searchdefault")
	public ResponseEntity<Page<Mro_pm_meter_ratioDTO>> searchDefault(Mro_pm_meter_ratioSearchContext context) {
        Page<Mro_pm_meter_ratio> domains = mro_pm_meter_ratioService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_meter_ratioMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_pm_meter_ratio getEntity(){
        return new Mro_pm_meter_ratio();
    }

}
