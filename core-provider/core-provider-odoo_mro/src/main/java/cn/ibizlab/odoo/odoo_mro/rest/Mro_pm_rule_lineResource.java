package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule_line;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_rule_lineService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_rule_lineSearchContext;




@Slf4j
@Api(tags = {"Mro_pm_rule_line" })
@RestController("odoo_mro-mro_pm_rule_line")
@RequestMapping("")
public class Mro_pm_rule_lineResource {

    @Autowired
    private IMro_pm_rule_lineService mro_pm_rule_lineService;

    @Autowired
    @Lazy
    private Mro_pm_rule_lineMapping mro_pm_rule_lineMapping;







    @PreAuthorize("hasPermission(#mro_pm_rule_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_pm_rule_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rule_lines/{mro_pm_rule_line_id}")
    public ResponseEntity<Mro_pm_rule_lineDTO> get(@PathVariable("mro_pm_rule_line_id") Integer mro_pm_rule_line_id) {
        Mro_pm_rule_line domain = mro_pm_rule_lineService.get(mro_pm_rule_line_id);
        Mro_pm_rule_lineDTO dto = mro_pm_rule_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#mro_pm_rule_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_pm_rule_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rule_lines/{mro_pm_rule_line_id}")

    public ResponseEntity<Mro_pm_rule_lineDTO> update(@PathVariable("mro_pm_rule_line_id") Integer mro_pm_rule_line_id, @RequestBody Mro_pm_rule_lineDTO mro_pm_rule_linedto) {
		Mro_pm_rule_line domain = mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedto);
        domain.setId(mro_pm_rule_line_id);
		mro_pm_rule_lineService.update(domain);
		Mro_pm_rule_lineDTO dto = mro_pm_rule_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_pm_rule_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_pm_rule_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rule_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_rule_lineDTO> mro_pm_rule_linedtos) {
        mro_pm_rule_lineService.updateBatch(mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mro_pm_rule_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_pm_rule_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rule_lines/{mro_pm_rule_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_rule_line_id") Integer mro_pm_rule_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_rule_lineService.remove(mro_pm_rule_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_pm_rule_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rule_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_pm_rule_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_pm_rule_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rule_lines")

    public ResponseEntity<Mro_pm_rule_lineDTO> create(@RequestBody Mro_pm_rule_lineDTO mro_pm_rule_linedto) {
        Mro_pm_rule_line domain = mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedto);
		mro_pm_rule_lineService.create(domain);
        Mro_pm_rule_lineDTO dto = mro_pm_rule_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_pm_rule_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rule_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_rule_lineDTO> mro_pm_rule_linedtos) {
        mro_pm_rule_lineService.createBatch(mro_pm_rule_lineMapping.toDomain(mro_pm_rule_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_pm_rule_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_rule_lines/fetchdefault")
	public ResponseEntity<List<Mro_pm_rule_lineDTO>> fetchDefault(Mro_pm_rule_lineSearchContext context) {
        Page<Mro_pm_rule_line> domains = mro_pm_rule_lineService.searchDefault(context) ;
        List<Mro_pm_rule_lineDTO> list = mro_pm_rule_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_pm_rule_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_rule_lines/searchdefault")
	public ResponseEntity<Page<Mro_pm_rule_lineDTO>> searchDefault(Mro_pm_rule_lineSearchContext context) {
        Page<Mro_pm_rule_line> domains = mro_pm_rule_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_rule_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_pm_rule_line getEntity(){
        return new Mro_pm_rule_line();
    }

}
