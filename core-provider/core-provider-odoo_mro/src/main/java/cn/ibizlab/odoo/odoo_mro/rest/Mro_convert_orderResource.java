package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_convert_order;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_convert_orderService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_convert_orderSearchContext;




@Slf4j
@Api(tags = {"Mro_convert_order" })
@RestController("odoo_mro-mro_convert_order")
@RequestMapping("")
public class Mro_convert_orderResource {

    @Autowired
    private IMro_convert_orderService mro_convert_orderService;

    @Autowired
    @Lazy
    private Mro_convert_orderMapping mro_convert_orderMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_convert_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_convert_orders")

    public ResponseEntity<Mro_convert_orderDTO> create(@RequestBody Mro_convert_orderDTO mro_convert_orderdto) {
        Mro_convert_order domain = mro_convert_orderMapping.toDomain(mro_convert_orderdto);
		mro_convert_orderService.create(domain);
        Mro_convert_orderDTO dto = mro_convert_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_convert_order" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_convert_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_convert_orderDTO> mro_convert_orderdtos) {
        mro_convert_orderService.createBatch(mro_convert_orderMapping.toDomain(mro_convert_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mro_convert_order_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_convert_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_convert_orders/{mro_convert_order_id}")
    public ResponseEntity<Mro_convert_orderDTO> get(@PathVariable("mro_convert_order_id") Integer mro_convert_order_id) {
        Mro_convert_order domain = mro_convert_orderService.get(mro_convert_order_id);
        Mro_convert_orderDTO dto = mro_convert_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#mro_convert_order_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_convert_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_convert_orders/{mro_convert_order_id}")

    public ResponseEntity<Mro_convert_orderDTO> update(@PathVariable("mro_convert_order_id") Integer mro_convert_order_id, @RequestBody Mro_convert_orderDTO mro_convert_orderdto) {
		Mro_convert_order domain = mro_convert_orderMapping.toDomain(mro_convert_orderdto);
        domain.setId(mro_convert_order_id);
		mro_convert_orderService.update(domain);
		Mro_convert_orderDTO dto = mro_convert_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_convert_order_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_convert_order" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_convert_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_convert_orderDTO> mro_convert_orderdtos) {
        mro_convert_orderService.updateBatch(mro_convert_orderMapping.toDomain(mro_convert_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#mro_convert_order_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_convert_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_convert_orders/{mro_convert_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_convert_order_id") Integer mro_convert_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_convert_orderService.remove(mro_convert_order_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_convert_order" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_convert_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_convert_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_convert_order" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_convert_orders/fetchdefault")
	public ResponseEntity<List<Mro_convert_orderDTO>> fetchDefault(Mro_convert_orderSearchContext context) {
        Page<Mro_convert_order> domains = mro_convert_orderService.searchDefault(context) ;
        List<Mro_convert_orderDTO> list = mro_convert_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_convert_order" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_convert_orders/searchdefault")
	public ResponseEntity<Page<Mro_convert_orderDTO>> searchDefault(Mro_convert_orderSearchContext context) {
        Page<Mro_convert_order> domains = mro_convert_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_convert_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_convert_order getEntity(){
        return new Mro_convert_order();
    }

}
