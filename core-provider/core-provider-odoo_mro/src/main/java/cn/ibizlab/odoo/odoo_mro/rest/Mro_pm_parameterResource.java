package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_parameter;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_parameterService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_parameterSearchContext;




@Slf4j
@Api(tags = {"Mro_pm_parameter" })
@RestController("odoo_mro-mro_pm_parameter")
@RequestMapping("")
public class Mro_pm_parameterResource {

    @Autowired
    private IMro_pm_parameterService mro_pm_parameterService;

    @Autowired
    @Lazy
    private Mro_pm_parameterMapping mro_pm_parameterMapping;







    @PreAuthorize("hasPermission(#mro_pm_parameter_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_pm_parameter" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_parameters/{mro_pm_parameter_id}")

    public ResponseEntity<Mro_pm_parameterDTO> update(@PathVariable("mro_pm_parameter_id") Integer mro_pm_parameter_id, @RequestBody Mro_pm_parameterDTO mro_pm_parameterdto) {
		Mro_pm_parameter domain = mro_pm_parameterMapping.toDomain(mro_pm_parameterdto);
        domain.setId(mro_pm_parameter_id);
		mro_pm_parameterService.update(domain);
		Mro_pm_parameterDTO dto = mro_pm_parameterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_pm_parameter_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_pm_parameter" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_parameters/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_parameterDTO> mro_pm_parameterdtos) {
        mro_pm_parameterService.updateBatch(mro_pm_parameterMapping.toDomain(mro_pm_parameterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mro_pm_parameter_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_pm_parameter" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_parameters/{mro_pm_parameter_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_parameter_id") Integer mro_pm_parameter_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_parameterService.remove(mro_pm_parameter_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_pm_parameter" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_parameters/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_pm_parameterService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_pm_parameter" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_parameters")

    public ResponseEntity<Mro_pm_parameterDTO> create(@RequestBody Mro_pm_parameterDTO mro_pm_parameterdto) {
        Mro_pm_parameter domain = mro_pm_parameterMapping.toDomain(mro_pm_parameterdto);
		mro_pm_parameterService.create(domain);
        Mro_pm_parameterDTO dto = mro_pm_parameterMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_pm_parameter" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_parameters/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_parameterDTO> mro_pm_parameterdtos) {
        mro_pm_parameterService.createBatch(mro_pm_parameterMapping.toDomain(mro_pm_parameterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mro_pm_parameter_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_pm_parameter" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_parameters/{mro_pm_parameter_id}")
    public ResponseEntity<Mro_pm_parameterDTO> get(@PathVariable("mro_pm_parameter_id") Integer mro_pm_parameter_id) {
        Mro_pm_parameter domain = mro_pm_parameterService.get(mro_pm_parameter_id);
        Mro_pm_parameterDTO dto = mro_pm_parameterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_pm_parameter" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_parameters/fetchdefault")
	public ResponseEntity<List<Mro_pm_parameterDTO>> fetchDefault(Mro_pm_parameterSearchContext context) {
        Page<Mro_pm_parameter> domains = mro_pm_parameterService.searchDefault(context) ;
        List<Mro_pm_parameterDTO> list = mro_pm_parameterMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_pm_parameter" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_parameters/searchdefault")
	public ResponseEntity<Page<Mro_pm_parameterDTO>> searchDefault(Mro_pm_parameterSearchContext context) {
        Page<Mro_pm_parameter> domains = mro_pm_parameterService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_parameterMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_pm_parameter getEntity(){
        return new Mro_pm_parameter();
    }

}
