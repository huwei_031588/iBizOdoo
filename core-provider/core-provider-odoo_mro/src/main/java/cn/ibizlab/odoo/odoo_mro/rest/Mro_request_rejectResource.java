package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_request_rejectService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_request_rejectSearchContext;




@Slf4j
@Api(tags = {"Mro_request_reject" })
@RestController("odoo_mro-mro_request_reject")
@RequestMapping("")
public class Mro_request_rejectResource {

    @Autowired
    private IMro_request_rejectService mro_request_rejectService;

    @Autowired
    @Lazy
    private Mro_request_rejectMapping mro_request_rejectMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_request_reject" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_request_rejects")

    public ResponseEntity<Mro_request_rejectDTO> create(@RequestBody Mro_request_rejectDTO mro_request_rejectdto) {
        Mro_request_reject domain = mro_request_rejectMapping.toDomain(mro_request_rejectdto);
		mro_request_rejectService.create(domain);
        Mro_request_rejectDTO dto = mro_request_rejectMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_request_reject" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_request_rejects/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_request_rejectDTO> mro_request_rejectdtos) {
        mro_request_rejectService.createBatch(mro_request_rejectMapping.toDomain(mro_request_rejectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#mro_request_reject_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_request_reject" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_request_rejects/{mro_request_reject_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_request_reject_id") Integer mro_request_reject_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_request_rejectService.remove(mro_request_reject_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_request_reject" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_request_rejects/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_request_rejectService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mro_request_reject_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_request_reject" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_request_rejects/{mro_request_reject_id}")
    public ResponseEntity<Mro_request_rejectDTO> get(@PathVariable("mro_request_reject_id") Integer mro_request_reject_id) {
        Mro_request_reject domain = mro_request_rejectService.get(mro_request_reject_id);
        Mro_request_rejectDTO dto = mro_request_rejectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#mro_request_reject_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_request_reject" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_request_rejects/{mro_request_reject_id}")

    public ResponseEntity<Mro_request_rejectDTO> update(@PathVariable("mro_request_reject_id") Integer mro_request_reject_id, @RequestBody Mro_request_rejectDTO mro_request_rejectdto) {
		Mro_request_reject domain = mro_request_rejectMapping.toDomain(mro_request_rejectdto);
        domain.setId(mro_request_reject_id);
		mro_request_rejectService.update(domain);
		Mro_request_rejectDTO dto = mro_request_rejectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_request_reject_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_request_reject" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_request_rejects/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_request_rejectDTO> mro_request_rejectdtos) {
        mro_request_rejectService.updateBatch(mro_request_rejectMapping.toDomain(mro_request_rejectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_request_reject" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_request_rejects/fetchdefault")
	public ResponseEntity<List<Mro_request_rejectDTO>> fetchDefault(Mro_request_rejectSearchContext context) {
        Page<Mro_request_reject> domains = mro_request_rejectService.searchDefault(context) ;
        List<Mro_request_rejectDTO> list = mro_request_rejectMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_request_reject" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_request_rejects/searchdefault")
	public ResponseEntity<Page<Mro_request_rejectDTO>> searchDefault(Mro_request_rejectSearchContext context) {
        Page<Mro_request_reject> domains = mro_request_rejectService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_request_rejectMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_request_reject getEntity(){
        return new Mro_request_reject();
    }

}
