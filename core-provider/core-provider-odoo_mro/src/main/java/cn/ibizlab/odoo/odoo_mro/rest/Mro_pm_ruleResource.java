package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_ruleService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_ruleSearchContext;




@Slf4j
@Api(tags = {"Mro_pm_rule" })
@RestController("odoo_mro-mro_pm_rule")
@RequestMapping("")
public class Mro_pm_ruleResource {

    @Autowired
    private IMro_pm_ruleService mro_pm_ruleService;

    @Autowired
    @Lazy
    private Mro_pm_ruleMapping mro_pm_ruleMapping;




    @PreAuthorize("hasPermission('Remove',{#mro_pm_rule_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_pm_rule" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rules/{mro_pm_rule_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_rule_id") Integer mro_pm_rule_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_ruleService.remove(mro_pm_rule_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_pm_rule" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rules/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_pm_ruleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_pm_rule" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rules")

    public ResponseEntity<Mro_pm_ruleDTO> create(@RequestBody Mro_pm_ruleDTO mro_pm_ruledto) {
        Mro_pm_rule domain = mro_pm_ruleMapping.toDomain(mro_pm_ruledto);
		mro_pm_ruleService.create(domain);
        Mro_pm_ruleDTO dto = mro_pm_ruleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_pm_rule" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rules/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_ruleDTO> mro_pm_ruledtos) {
        mro_pm_ruleService.createBatch(mro_pm_ruleMapping.toDomain(mro_pm_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mro_pm_rule_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_pm_rule" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rules/{mro_pm_rule_id}")
    public ResponseEntity<Mro_pm_ruleDTO> get(@PathVariable("mro_pm_rule_id") Integer mro_pm_rule_id) {
        Mro_pm_rule domain = mro_pm_ruleService.get(mro_pm_rule_id);
        Mro_pm_ruleDTO dto = mro_pm_ruleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#mro_pm_rule_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_pm_rule" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rules/{mro_pm_rule_id}")

    public ResponseEntity<Mro_pm_ruleDTO> update(@PathVariable("mro_pm_rule_id") Integer mro_pm_rule_id, @RequestBody Mro_pm_ruleDTO mro_pm_ruledto) {
		Mro_pm_rule domain = mro_pm_ruleMapping.toDomain(mro_pm_ruledto);
        domain.setId(mro_pm_rule_id);
		mro_pm_ruleService.update(domain);
		Mro_pm_ruleDTO dto = mro_pm_ruleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_pm_rule_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_pm_rule" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rules/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_ruleDTO> mro_pm_ruledtos) {
        mro_pm_ruleService.updateBatch(mro_pm_ruleMapping.toDomain(mro_pm_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_pm_rule" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_rules/fetchdefault")
	public ResponseEntity<List<Mro_pm_ruleDTO>> fetchDefault(Mro_pm_ruleSearchContext context) {
        Page<Mro_pm_rule> domains = mro_pm_ruleService.searchDefault(context) ;
        List<Mro_pm_ruleDTO> list = mro_pm_ruleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_pm_rule" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_rules/searchdefault")
	public ResponseEntity<Page<Mro_pm_ruleDTO>> searchDefault(Mro_pm_ruleSearchContext context) {
        Page<Mro_pm_rule> domains = mro_pm_ruleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_ruleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_pm_rule getEntity(){
        return new Mro_pm_rule();
    }

}
