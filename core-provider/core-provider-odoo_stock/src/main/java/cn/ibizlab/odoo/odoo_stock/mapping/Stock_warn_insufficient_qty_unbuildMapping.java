package cn.ibizlab.odoo.odoo_stock.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.odoo.odoo_stock.dto.Stock_warn_insufficient_qty_unbuildDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_warn_insufficient_qty_unbuildMapping extends MappingBase<Stock_warn_insufficient_qty_unbuildDTO, Stock_warn_insufficient_qty_unbuild> {


}

