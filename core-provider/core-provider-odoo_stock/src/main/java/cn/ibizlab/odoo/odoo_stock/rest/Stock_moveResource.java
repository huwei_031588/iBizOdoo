package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_moveService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_moveSearchContext;




@Slf4j
@Api(tags = {"Stock_move" })
@RestController("odoo_stock-stock_move")
@RequestMapping("")
public class Stock_moveResource {

    @Autowired
    private IStock_moveService stock_moveService;

    @Autowired
    @Lazy
    private Stock_moveMapping stock_moveMapping;







    @PreAuthorize("hasPermission(#stock_move_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_move" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_moves/{stock_move_id}")
    public ResponseEntity<Stock_moveDTO> get(@PathVariable("stock_move_id") Integer stock_move_id) {
        Stock_move domain = stock_moveService.get(stock_move_id);
        Stock_moveDTO dto = stock_moveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#stock_move_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_move" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_moves/{stock_move_id}")

    public ResponseEntity<Stock_moveDTO> update(@PathVariable("stock_move_id") Integer stock_move_id, @RequestBody Stock_moveDTO stock_movedto) {
		Stock_move domain = stock_moveMapping.toDomain(stock_movedto);
        domain.setId(stock_move_id);
		stock_moveService.update(domain);
		Stock_moveDTO dto = stock_moveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_move_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_move" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_moves/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_moveDTO> stock_movedtos) {
        stock_moveService.updateBatch(stock_moveMapping.toDomain(stock_movedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_move" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_moves")

    public ResponseEntity<Stock_moveDTO> create(@RequestBody Stock_moveDTO stock_movedto) {
        Stock_move domain = stock_moveMapping.toDomain(stock_movedto);
		stock_moveService.create(domain);
        Stock_moveDTO dto = stock_moveMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_move" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_moves/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_moveDTO> stock_movedtos) {
        stock_moveService.createBatch(stock_moveMapping.toDomain(stock_movedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#stock_move_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_move" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_moves/{stock_move_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_move_id") Integer stock_move_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_moveService.remove(stock_move_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_move" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_moves/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_moveService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_move" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_moves/fetchdefault")
	public ResponseEntity<List<Stock_moveDTO>> fetchDefault(Stock_moveSearchContext context) {
        Page<Stock_move> domains = stock_moveService.searchDefault(context) ;
        List<Stock_moveDTO> list = stock_moveMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_move" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_moves/searchdefault")
	public ResponseEntity<Page<Stock_moveDTO>> searchDefault(Stock_moveSearchContext context) {
        Page<Stock_move> domains = stock_moveService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_moveMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_move getEntity(){
        return new Stock_move();
    }

}
