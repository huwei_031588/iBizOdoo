package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_package_levelService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_levelSearchContext;




@Slf4j
@Api(tags = {"Stock_package_level" })
@RestController("odoo_stock-stock_package_level")
@RequestMapping("")
public class Stock_package_levelResource {

    @Autowired
    private IStock_package_levelService stock_package_levelService;

    @Autowired
    @Lazy
    private Stock_package_levelMapping stock_package_levelMapping;







    @PreAuthorize("hasPermission('Remove',{#stock_package_level_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_package_level" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_levels/{stock_package_level_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_package_level_id") Integer stock_package_level_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_package_levelService.remove(stock_package_level_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_package_level" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_levels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_package_levelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_package_level_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_package_level" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_package_levels/{stock_package_level_id}")
    public ResponseEntity<Stock_package_levelDTO> get(@PathVariable("stock_package_level_id") Integer stock_package_level_id) {
        Stock_package_level domain = stock_package_levelService.get(stock_package_level_id);
        Stock_package_levelDTO dto = stock_package_levelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_package_level" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_levels")

    public ResponseEntity<Stock_package_levelDTO> create(@RequestBody Stock_package_levelDTO stock_package_leveldto) {
        Stock_package_level domain = stock_package_levelMapping.toDomain(stock_package_leveldto);
		stock_package_levelService.create(domain);
        Stock_package_levelDTO dto = stock_package_levelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_package_level" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_levels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_package_levelDTO> stock_package_leveldtos) {
        stock_package_levelService.createBatch(stock_package_levelMapping.toDomain(stock_package_leveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_package_level_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_package_level" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_package_levels/{stock_package_level_id}")

    public ResponseEntity<Stock_package_levelDTO> update(@PathVariable("stock_package_level_id") Integer stock_package_level_id, @RequestBody Stock_package_levelDTO stock_package_leveldto) {
		Stock_package_level domain = stock_package_levelMapping.toDomain(stock_package_leveldto);
        domain.setId(stock_package_level_id);
		stock_package_levelService.update(domain);
		Stock_package_levelDTO dto = stock_package_levelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_package_level_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_package_level" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_package_levels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_package_levelDTO> stock_package_leveldtos) {
        stock_package_levelService.updateBatch(stock_package_levelMapping.toDomain(stock_package_leveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_package_level" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_package_levels/fetchdefault")
	public ResponseEntity<List<Stock_package_levelDTO>> fetchDefault(Stock_package_levelSearchContext context) {
        Page<Stock_package_level> domains = stock_package_levelService.searchDefault(context) ;
        List<Stock_package_levelDTO> list = stock_package_levelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_package_level" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_package_levels/searchdefault")
	public ResponseEntity<Page<Stock_package_levelDTO>> searchDefault(Stock_package_levelSearchContext context) {
        Page<Stock_package_level> domains = stock_package_levelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_package_levelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_package_level getEntity(){
        return new Stock_package_level();
    }

}
