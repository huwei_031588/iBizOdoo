package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_location_routeService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_location_routeSearchContext;




@Slf4j
@Api(tags = {"Stock_location_route" })
@RestController("odoo_stock-stock_location_route")
@RequestMapping("")
public class Stock_location_routeResource {

    @Autowired
    private IStock_location_routeService stock_location_routeService;

    @Autowired
    @Lazy
    private Stock_location_routeMapping stock_location_routeMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_location_route" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes")

    public ResponseEntity<Stock_location_routeDTO> create(@RequestBody Stock_location_routeDTO stock_location_routedto) {
        Stock_location_route domain = stock_location_routeMapping.toDomain(stock_location_routedto);
		stock_location_routeService.create(domain);
        Stock_location_routeDTO dto = stock_location_routeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_location_route" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_location_routeDTO> stock_location_routedtos) {
        stock_location_routeService.createBatch(stock_location_routeMapping.toDomain(stock_location_routedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_location_route_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_location_route" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_location_routes/{stock_location_route_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_location_route_id") Integer stock_location_route_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_location_routeService.remove(stock_location_route_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_location_route" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_location_routes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_location_routeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_location_route_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_location_route" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_location_routes/{stock_location_route_id}")
    public ResponseEntity<Stock_location_routeDTO> get(@PathVariable("stock_location_route_id") Integer stock_location_route_id) {
        Stock_location_route domain = stock_location_routeService.get(stock_location_route_id);
        Stock_location_routeDTO dto = stock_location_routeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#stock_location_route_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_location_route" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_location_routes/{stock_location_route_id}")

    public ResponseEntity<Stock_location_routeDTO> update(@PathVariable("stock_location_route_id") Integer stock_location_route_id, @RequestBody Stock_location_routeDTO stock_location_routedto) {
		Stock_location_route domain = stock_location_routeMapping.toDomain(stock_location_routedto);
        domain.setId(stock_location_route_id);
		stock_location_routeService.update(domain);
		Stock_location_routeDTO dto = stock_location_routeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_location_route_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_location_route" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_location_routes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_location_routeDTO> stock_location_routedtos) {
        stock_location_routeService.updateBatch(stock_location_routeMapping.toDomain(stock_location_routedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_location_route" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_location_routes/fetchdefault")
	public ResponseEntity<List<Stock_location_routeDTO>> fetchDefault(Stock_location_routeSearchContext context) {
        Page<Stock_location_route> domains = stock_location_routeService.searchDefault(context) ;
        List<Stock_location_routeDTO> list = stock_location_routeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_location_route" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_location_routes/searchdefault")
	public ResponseEntity<Page<Stock_location_routeDTO>> searchDefault(Stock_location_routeSearchContext context) {
        Page<Stock_location_route> domains = stock_location_routeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_location_routeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_location_route getEntity(){
        return new Stock_location_route();
    }

}
