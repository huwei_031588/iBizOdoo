package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_standard_price;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_change_standard_priceService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;




@Slf4j
@Api(tags = {"Stock_change_standard_price" })
@RestController("odoo_stock-stock_change_standard_price")
@RequestMapping("")
public class Stock_change_standard_priceResource {

    @Autowired
    private IStock_change_standard_priceService stock_change_standard_priceService;

    @Autowired
    @Lazy
    private Stock_change_standard_priceMapping stock_change_standard_priceMapping;













    @PreAuthorize("hasPermission('Remove',{#stock_change_standard_price_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_change_standard_price" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_standard_prices/{stock_change_standard_price_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_change_standard_price_id") Integer stock_change_standard_price_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_change_standard_priceService.remove(stock_change_standard_price_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_change_standard_price" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_standard_prices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_change_standard_priceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_change_standard_price_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_change_standard_price" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_change_standard_prices/{stock_change_standard_price_id}")
    public ResponseEntity<Stock_change_standard_priceDTO> get(@PathVariable("stock_change_standard_price_id") Integer stock_change_standard_price_id) {
        Stock_change_standard_price domain = stock_change_standard_priceService.get(stock_change_standard_price_id);
        Stock_change_standard_priceDTO dto = stock_change_standard_priceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_change_standard_price" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices")

    public ResponseEntity<Stock_change_standard_priceDTO> create(@RequestBody Stock_change_standard_priceDTO stock_change_standard_pricedto) {
        Stock_change_standard_price domain = stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedto);
		stock_change_standard_priceService.create(domain);
        Stock_change_standard_priceDTO dto = stock_change_standard_priceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_change_standard_price" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_change_standard_priceDTO> stock_change_standard_pricedtos) {
        stock_change_standard_priceService.createBatch(stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_change_standard_price_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_change_standard_price" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_change_standard_prices/{stock_change_standard_price_id}")

    public ResponseEntity<Stock_change_standard_priceDTO> update(@PathVariable("stock_change_standard_price_id") Integer stock_change_standard_price_id, @RequestBody Stock_change_standard_priceDTO stock_change_standard_pricedto) {
		Stock_change_standard_price domain = stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedto);
        domain.setId(stock_change_standard_price_id);
		stock_change_standard_priceService.update(domain);
		Stock_change_standard_priceDTO dto = stock_change_standard_priceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_change_standard_price_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_change_standard_price" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_change_standard_prices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_change_standard_priceDTO> stock_change_standard_pricedtos) {
        stock_change_standard_priceService.updateBatch(stock_change_standard_priceMapping.toDomain(stock_change_standard_pricedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_change_standard_price" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_change_standard_prices/fetchdefault")
	public ResponseEntity<List<Stock_change_standard_priceDTO>> fetchDefault(Stock_change_standard_priceSearchContext context) {
        Page<Stock_change_standard_price> domains = stock_change_standard_priceService.searchDefault(context) ;
        List<Stock_change_standard_priceDTO> list = stock_change_standard_priceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_change_standard_price" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_change_standard_prices/searchdefault")
	public ResponseEntity<Page<Stock_change_standard_priceDTO>> searchDefault(Stock_change_standard_priceSearchContext context) {
        Page<Stock_change_standard_price> domains = stock_change_standard_priceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_change_standard_priceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_change_standard_price getEntity(){
        return new Stock_change_standard_price();
    }

}
