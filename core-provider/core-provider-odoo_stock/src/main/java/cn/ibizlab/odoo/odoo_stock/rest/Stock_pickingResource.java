package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_pickingService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_pickingSearchContext;




@Slf4j
@Api(tags = {"Stock_picking" })
@RestController("odoo_stock-stock_picking")
@RequestMapping("")
public class Stock_pickingResource {

    @Autowired
    private IStock_pickingService stock_pickingService;

    @Autowired
    @Lazy
    private Stock_pickingMapping stock_pickingMapping;




    @PreAuthorize("hasPermission(#stock_picking_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_picking" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_pickings/{stock_picking_id}")
    public ResponseEntity<Stock_pickingDTO> get(@PathVariable("stock_picking_id") Integer stock_picking_id) {
        Stock_picking domain = stock_pickingService.get(stock_picking_id);
        Stock_pickingDTO dto = stock_pickingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_picking_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_picking" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_pickings/{stock_picking_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_picking_id") Integer stock_picking_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_pickingService.remove(stock_picking_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_picking" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_pickings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_pickingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_picking_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_picking" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_pickings/{stock_picking_id}")

    public ResponseEntity<Stock_pickingDTO> update(@PathVariable("stock_picking_id") Integer stock_picking_id, @RequestBody Stock_pickingDTO stock_pickingdto) {
		Stock_picking domain = stock_pickingMapping.toDomain(stock_pickingdto);
        domain.setId(stock_picking_id);
		stock_pickingService.update(domain);
		Stock_pickingDTO dto = stock_pickingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_picking_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_picking" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_pickings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_pickingDTO> stock_pickingdtos) {
        stock_pickingService.updateBatch(stock_pickingMapping.toDomain(stock_pickingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_picking" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_pickings")

    public ResponseEntity<Stock_pickingDTO> create(@RequestBody Stock_pickingDTO stock_pickingdto) {
        Stock_picking domain = stock_pickingMapping.toDomain(stock_pickingdto);
		stock_pickingService.create(domain);
        Stock_pickingDTO dto = stock_pickingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_picking" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_pickings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_pickingDTO> stock_pickingdtos) {
        stock_pickingService.createBatch(stock_pickingMapping.toDomain(stock_pickingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_picking" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_pickings/fetchdefault")
	public ResponseEntity<List<Stock_pickingDTO>> fetchDefault(Stock_pickingSearchContext context) {
        Page<Stock_picking> domains = stock_pickingService.searchDefault(context) ;
        List<Stock_pickingDTO> list = stock_pickingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_picking" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_pickings/searchdefault")
	public ResponseEntity<Page<Stock_pickingDTO>> searchDefault(Stock_pickingSearchContext context) {
        Page<Stock_picking> domains = stock_pickingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_pickingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_picking getEntity(){
        return new Stock_picking();
    }

}
