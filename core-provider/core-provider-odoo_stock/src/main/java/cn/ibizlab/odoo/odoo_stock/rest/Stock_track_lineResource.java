package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_line;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_track_lineService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_lineSearchContext;




@Slf4j
@Api(tags = {"Stock_track_line" })
@RestController("odoo_stock-stock_track_line")
@RequestMapping("")
public class Stock_track_lineResource {

    @Autowired
    private IStock_track_lineService stock_track_lineService;

    @Autowired
    @Lazy
    private Stock_track_lineMapping stock_track_lineMapping;







    @PreAuthorize("hasPermission(#stock_track_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_track_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_track_lines/{stock_track_line_id}")
    public ResponseEntity<Stock_track_lineDTO> get(@PathVariable("stock_track_line_id") Integer stock_track_line_id) {
        Stock_track_line domain = stock_track_lineService.get(stock_track_line_id);
        Stock_track_lineDTO dto = stock_track_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_track_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_lines")

    public ResponseEntity<Stock_track_lineDTO> create(@RequestBody Stock_track_lineDTO stock_track_linedto) {
        Stock_track_line domain = stock_track_lineMapping.toDomain(stock_track_linedto);
		stock_track_lineService.create(domain);
        Stock_track_lineDTO dto = stock_track_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_track_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_track_lineDTO> stock_track_linedtos) {
        stock_track_lineService.createBatch(stock_track_lineMapping.toDomain(stock_track_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#stock_track_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_track_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_lines/{stock_track_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_track_line_id") Integer stock_track_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_track_lineService.remove(stock_track_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_track_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_track_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_track_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_track_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_track_lines/{stock_track_line_id}")

    public ResponseEntity<Stock_track_lineDTO> update(@PathVariable("stock_track_line_id") Integer stock_track_line_id, @RequestBody Stock_track_lineDTO stock_track_linedto) {
		Stock_track_line domain = stock_track_lineMapping.toDomain(stock_track_linedto);
        domain.setId(stock_track_line_id);
		stock_track_lineService.update(domain);
		Stock_track_lineDTO dto = stock_track_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_track_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_track_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_track_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_track_lineDTO> stock_track_linedtos) {
        stock_track_lineService.updateBatch(stock_track_lineMapping.toDomain(stock_track_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_track_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_track_lines/fetchdefault")
	public ResponseEntity<List<Stock_track_lineDTO>> fetchDefault(Stock_track_lineSearchContext context) {
        Page<Stock_track_line> domains = stock_track_lineService.searchDefault(context) ;
        List<Stock_track_lineDTO> list = stock_track_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_track_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_track_lines/searchdefault")
	public ResponseEntity<Page<Stock_track_lineDTO>> searchDefault(Stock_track_lineSearchContext context) {
        Page<Stock_track_line> domains = stock_track_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_track_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_track_line getEntity(){
        return new Stock_track_line();
    }

}
