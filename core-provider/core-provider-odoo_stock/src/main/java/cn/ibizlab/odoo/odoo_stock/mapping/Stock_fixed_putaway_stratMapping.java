package cn.ibizlab.odoo.odoo_stock.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.odoo.odoo_stock.dto.Stock_fixed_putaway_stratDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_fixed_putaway_stratMapping extends MappingBase<Stock_fixed_putaway_stratDTO, Stock_fixed_putaway_strat> {


}

