package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_product_qty;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_change_product_qtyService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_product_qtySearchContext;




@Slf4j
@Api(tags = {"Stock_change_product_qty" })
@RestController("odoo_stock-stock_change_product_qty")
@RequestMapping("")
public class Stock_change_product_qtyResource {

    @Autowired
    private IStock_change_product_qtyService stock_change_product_qtyService;

    @Autowired
    @Lazy
    private Stock_change_product_qtyMapping stock_change_product_qtyMapping;




    @PreAuthorize("hasPermission(#stock_change_product_qty_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_change_product_qty" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_change_product_qties/{stock_change_product_qty_id}")

    public ResponseEntity<Stock_change_product_qtyDTO> update(@PathVariable("stock_change_product_qty_id") Integer stock_change_product_qty_id, @RequestBody Stock_change_product_qtyDTO stock_change_product_qtydto) {
		Stock_change_product_qty domain = stock_change_product_qtyMapping.toDomain(stock_change_product_qtydto);
        domain.setId(stock_change_product_qty_id);
		stock_change_product_qtyService.update(domain);
		Stock_change_product_qtyDTO dto = stock_change_product_qtyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_change_product_qty_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_change_product_qty" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_change_product_qties/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_change_product_qtyDTO> stock_change_product_qtydtos) {
        stock_change_product_qtyService.updateBatch(stock_change_product_qtyMapping.toDomain(stock_change_product_qtydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_change_product_qty" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_product_qties")

    public ResponseEntity<Stock_change_product_qtyDTO> create(@RequestBody Stock_change_product_qtyDTO stock_change_product_qtydto) {
        Stock_change_product_qty domain = stock_change_product_qtyMapping.toDomain(stock_change_product_qtydto);
		stock_change_product_qtyService.create(domain);
        Stock_change_product_qtyDTO dto = stock_change_product_qtyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_change_product_qty" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_change_product_qties/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_change_product_qtyDTO> stock_change_product_qtydtos) {
        stock_change_product_qtyService.createBatch(stock_change_product_qtyMapping.toDomain(stock_change_product_qtydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission(#stock_change_product_qty_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_change_product_qty" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_change_product_qties/{stock_change_product_qty_id}")
    public ResponseEntity<Stock_change_product_qtyDTO> get(@PathVariable("stock_change_product_qty_id") Integer stock_change_product_qty_id) {
        Stock_change_product_qty domain = stock_change_product_qtyService.get(stock_change_product_qty_id);
        Stock_change_product_qtyDTO dto = stock_change_product_qtyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_change_product_qty_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_change_product_qty" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_product_qties/{stock_change_product_qty_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_change_product_qty_id") Integer stock_change_product_qty_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_change_product_qtyService.remove(stock_change_product_qty_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_change_product_qty" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_product_qties/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_change_product_qtyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_change_product_qty" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_change_product_qties/fetchdefault")
	public ResponseEntity<List<Stock_change_product_qtyDTO>> fetchDefault(Stock_change_product_qtySearchContext context) {
        Page<Stock_change_product_qty> domains = stock_change_product_qtyService.searchDefault(context) ;
        List<Stock_change_product_qtyDTO> list = stock_change_product_qtyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_change_product_qty" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_change_product_qties/searchdefault")
	public ResponseEntity<Page<Stock_change_product_qtyDTO>> searchDefault(Stock_change_product_qtySearchContext context) {
        Page<Stock_change_product_qty> domains = stock_change_product_qtyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_change_product_qtyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_change_product_qty getEntity(){
        return new Stock_change_product_qty();
    }

}
