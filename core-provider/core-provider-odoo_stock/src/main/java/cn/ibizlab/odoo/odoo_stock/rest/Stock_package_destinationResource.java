package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_destination;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_package_destinationService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_destinationSearchContext;




@Slf4j
@Api(tags = {"Stock_package_destination" })
@RestController("odoo_stock-stock_package_destination")
@RequestMapping("")
public class Stock_package_destinationResource {

    @Autowired
    private IStock_package_destinationService stock_package_destinationService;

    @Autowired
    @Lazy
    private Stock_package_destinationMapping stock_package_destinationMapping;







    @PreAuthorize("hasPermission('Remove',{#stock_package_destination_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_package_destination" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_destinations/{stock_package_destination_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_package_destination_id") Integer stock_package_destination_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_package_destinationService.remove(stock_package_destination_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_package_destination" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_destinations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_package_destinationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_package_destination_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_package_destination" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_package_destinations/{stock_package_destination_id}")

    public ResponseEntity<Stock_package_destinationDTO> update(@PathVariable("stock_package_destination_id") Integer stock_package_destination_id, @RequestBody Stock_package_destinationDTO stock_package_destinationdto) {
		Stock_package_destination domain = stock_package_destinationMapping.toDomain(stock_package_destinationdto);
        domain.setId(stock_package_destination_id);
		stock_package_destinationService.update(domain);
		Stock_package_destinationDTO dto = stock_package_destinationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_package_destination_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_package_destination" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_package_destinations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_package_destinationDTO> stock_package_destinationdtos) {
        stock_package_destinationService.updateBatch(stock_package_destinationMapping.toDomain(stock_package_destinationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_package_destination_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_package_destination" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_package_destinations/{stock_package_destination_id}")
    public ResponseEntity<Stock_package_destinationDTO> get(@PathVariable("stock_package_destination_id") Integer stock_package_destination_id) {
        Stock_package_destination domain = stock_package_destinationService.get(stock_package_destination_id);
        Stock_package_destinationDTO dto = stock_package_destinationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_package_destination" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations")

    public ResponseEntity<Stock_package_destinationDTO> create(@RequestBody Stock_package_destinationDTO stock_package_destinationdto) {
        Stock_package_destination domain = stock_package_destinationMapping.toDomain(stock_package_destinationdto);
		stock_package_destinationService.create(domain);
        Stock_package_destinationDTO dto = stock_package_destinationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_package_destination" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_package_destinationDTO> stock_package_destinationdtos) {
        stock_package_destinationService.createBatch(stock_package_destinationMapping.toDomain(stock_package_destinationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_package_destination" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_package_destinations/fetchdefault")
	public ResponseEntity<List<Stock_package_destinationDTO>> fetchDefault(Stock_package_destinationSearchContext context) {
        Page<Stock_package_destination> domains = stock_package_destinationService.searchDefault(context) ;
        List<Stock_package_destinationDTO> list = stock_package_destinationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_package_destination" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_package_destinations/searchdefault")
	public ResponseEntity<Page<Stock_package_destinationDTO>> searchDefault(Stock_package_destinationSearchContext context) {
        Page<Stock_package_destination> domains = stock_package_destinationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_package_destinationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_package_destination getEntity(){
        return new Stock_package_destination();
    }

}
