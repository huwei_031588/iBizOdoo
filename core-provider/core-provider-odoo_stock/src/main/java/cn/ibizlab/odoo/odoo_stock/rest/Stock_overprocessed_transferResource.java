package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_overprocessed_transfer;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_overprocessed_transferService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_overprocessed_transferSearchContext;




@Slf4j
@Api(tags = {"Stock_overprocessed_transfer" })
@RestController("odoo_stock-stock_overprocessed_transfer")
@RequestMapping("")
public class Stock_overprocessed_transferResource {

    @Autowired
    private IStock_overprocessed_transferService stock_overprocessed_transferService;

    @Autowired
    @Lazy
    private Stock_overprocessed_transferMapping stock_overprocessed_transferMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_overprocessed_transfer" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_overprocessed_transfers")

    public ResponseEntity<Stock_overprocessed_transferDTO> create(@RequestBody Stock_overprocessed_transferDTO stock_overprocessed_transferdto) {
        Stock_overprocessed_transfer domain = stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdto);
		stock_overprocessed_transferService.create(domain);
        Stock_overprocessed_transferDTO dto = stock_overprocessed_transferMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_overprocessed_transfer" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_overprocessed_transfers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_overprocessed_transferDTO> stock_overprocessed_transferdtos) {
        stock_overprocessed_transferService.createBatch(stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_overprocessed_transfer_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_overprocessed_transfer" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_overprocessed_transfers/{stock_overprocessed_transfer_id}")
    public ResponseEntity<Stock_overprocessed_transferDTO> get(@PathVariable("stock_overprocessed_transfer_id") Integer stock_overprocessed_transfer_id) {
        Stock_overprocessed_transfer domain = stock_overprocessed_transferService.get(stock_overprocessed_transfer_id);
        Stock_overprocessed_transferDTO dto = stock_overprocessed_transferMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#stock_overprocessed_transfer_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_overprocessed_transfer" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_overprocessed_transfers/{stock_overprocessed_transfer_id}")

    public ResponseEntity<Stock_overprocessed_transferDTO> update(@PathVariable("stock_overprocessed_transfer_id") Integer stock_overprocessed_transfer_id, @RequestBody Stock_overprocessed_transferDTO stock_overprocessed_transferdto) {
		Stock_overprocessed_transfer domain = stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdto);
        domain.setId(stock_overprocessed_transfer_id);
		stock_overprocessed_transferService.update(domain);
		Stock_overprocessed_transferDTO dto = stock_overprocessed_transferMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_overprocessed_transfer_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_overprocessed_transfer" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_overprocessed_transfers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_overprocessed_transferDTO> stock_overprocessed_transferdtos) {
        stock_overprocessed_transferService.updateBatch(stock_overprocessed_transferMapping.toDomain(stock_overprocessed_transferdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_overprocessed_transfer_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_overprocessed_transfer" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_overprocessed_transfers/{stock_overprocessed_transfer_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_overprocessed_transfer_id") Integer stock_overprocessed_transfer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_overprocessed_transferService.remove(stock_overprocessed_transfer_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_overprocessed_transfer" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_overprocessed_transfers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_overprocessed_transferService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_overprocessed_transfer" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_overprocessed_transfers/fetchdefault")
	public ResponseEntity<List<Stock_overprocessed_transferDTO>> fetchDefault(Stock_overprocessed_transferSearchContext context) {
        Page<Stock_overprocessed_transfer> domains = stock_overprocessed_transferService.searchDefault(context) ;
        List<Stock_overprocessed_transferDTO> list = stock_overprocessed_transferMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_overprocessed_transfer" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_overprocessed_transfers/searchdefault")
	public ResponseEntity<Page<Stock_overprocessed_transferDTO>> searchDefault(Stock_overprocessed_transferSearchContext context) {
        Page<Stock_overprocessed_transfer> domains = stock_overprocessed_transferService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_overprocessed_transferMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_overprocessed_transfer getEntity(){
        return new Stock_overprocessed_transfer();
    }

}
