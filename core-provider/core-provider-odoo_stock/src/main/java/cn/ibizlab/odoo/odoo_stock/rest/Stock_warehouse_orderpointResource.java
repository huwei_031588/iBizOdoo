package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warehouse_orderpointService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouse_orderpointSearchContext;




@Slf4j
@Api(tags = {"Stock_warehouse_orderpoint" })
@RestController("odoo_stock-stock_warehouse_orderpoint")
@RequestMapping("")
public class Stock_warehouse_orderpointResource {

    @Autowired
    private IStock_warehouse_orderpointService stock_warehouse_orderpointService;

    @Autowired
    @Lazy
    private Stock_warehouse_orderpointMapping stock_warehouse_orderpointMapping;













    @PreAuthorize("hasPermission(#stock_warehouse_orderpoint_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_warehouse_orderpoint" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouse_orderpoints/{stock_warehouse_orderpoint_id}")

    public ResponseEntity<Stock_warehouse_orderpointDTO> update(@PathVariable("stock_warehouse_orderpoint_id") Integer stock_warehouse_orderpoint_id, @RequestBody Stock_warehouse_orderpointDTO stock_warehouse_orderpointdto) {
		Stock_warehouse_orderpoint domain = stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdto);
        domain.setId(stock_warehouse_orderpoint_id);
		stock_warehouse_orderpointService.update(domain);
		Stock_warehouse_orderpointDTO dto = stock_warehouse_orderpointMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_warehouse_orderpoint_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_warehouse_orderpoint" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouse_orderpoints/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warehouse_orderpointDTO> stock_warehouse_orderpointdtos) {
        stock_warehouse_orderpointService.updateBatch(stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_warehouse_orderpoint_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_warehouse_orderpoint" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warehouse_orderpoints/{stock_warehouse_orderpoint_id}")
    public ResponseEntity<Stock_warehouse_orderpointDTO> get(@PathVariable("stock_warehouse_orderpoint_id") Integer stock_warehouse_orderpoint_id) {
        Stock_warehouse_orderpoint domain = stock_warehouse_orderpointService.get(stock_warehouse_orderpoint_id);
        Stock_warehouse_orderpointDTO dto = stock_warehouse_orderpointMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_warehouse_orderpoint_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_warehouse_orderpoint" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouse_orderpoints/{stock_warehouse_orderpoint_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_warehouse_orderpoint_id") Integer stock_warehouse_orderpoint_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_warehouse_orderpointService.remove(stock_warehouse_orderpoint_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_warehouse_orderpoint" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouse_orderpoints/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_warehouse_orderpointService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_warehouse_orderpoint" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouse_orderpoints")

    public ResponseEntity<Stock_warehouse_orderpointDTO> create(@RequestBody Stock_warehouse_orderpointDTO stock_warehouse_orderpointdto) {
        Stock_warehouse_orderpoint domain = stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdto);
		stock_warehouse_orderpointService.create(domain);
        Stock_warehouse_orderpointDTO dto = stock_warehouse_orderpointMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_warehouse_orderpoint" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouse_orderpoints/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_warehouse_orderpointDTO> stock_warehouse_orderpointdtos) {
        stock_warehouse_orderpointService.createBatch(stock_warehouse_orderpointMapping.toDomain(stock_warehouse_orderpointdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_warehouse_orderpoint" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warehouse_orderpoints/fetchdefault")
	public ResponseEntity<List<Stock_warehouse_orderpointDTO>> fetchDefault(Stock_warehouse_orderpointSearchContext context) {
        Page<Stock_warehouse_orderpoint> domains = stock_warehouse_orderpointService.searchDefault(context) ;
        List<Stock_warehouse_orderpointDTO> list = stock_warehouse_orderpointMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_warehouse_orderpoint" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warehouse_orderpoints/searchdefault")
	public ResponseEntity<Page<Stock_warehouse_orderpointDTO>> searchDefault(Stock_warehouse_orderpointSearchContext context) {
        Page<Stock_warehouse_orderpoint> domains = stock_warehouse_orderpointService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_warehouse_orderpointMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_warehouse_orderpoint getEntity(){
        return new Stock_warehouse_orderpoint();
    }

}
