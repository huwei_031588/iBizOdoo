package cn.ibizlab.odoo.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Stock_picking_typeDTO]
 */
@Data
public class Stock_picking_typeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SHOW_OPERATIONS]
     *
     */
    @JSONField(name = "show_operations")
    @JsonProperty("show_operations")
    private String showOperations;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [SEQUENCE_ID]
     *
     */
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    private Integer sequenceId;

    /**
     * 属性 [USE_CREATE_LOTS]
     *
     */
    @JSONField(name = "use_create_lots")
    @JsonProperty("use_create_lots")
    private String useCreateLots;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [BARCODE]
     *
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 属性 [SHOW_RESERVED]
     *
     */
    @JSONField(name = "show_reserved")
    @JsonProperty("show_reserved")
    private String showReserved;

    /**
     * 属性 [RATE_PICKING_BACKORDERS]
     *
     */
    @JSONField(name = "rate_picking_backorders")
    @JsonProperty("rate_picking_backorders")
    private Integer ratePickingBackorders;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [COUNT_PICKING_BACKORDERS]
     *
     */
    @JSONField(name = "count_picking_backorders")
    @JsonProperty("count_picking_backorders")
    private Integer countPickingBackorders;

    /**
     * 属性 [COUNT_MO_TODO]
     *
     */
    @JSONField(name = "count_mo_todo")
    @JsonProperty("count_mo_todo")
    private Integer countMoTodo;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [SHOW_ENTIRE_PACKS]
     *
     */
    @JSONField(name = "show_entire_packs")
    @JsonProperty("show_entire_packs")
    private String showEntirePacks;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [COUNT_MO_WAITING]
     *
     */
    @JSONField(name = "count_mo_waiting")
    @JsonProperty("count_mo_waiting")
    private Integer countMoWaiting;

    /**
     * 属性 [COUNT_PICKING_DRAFT]
     *
     */
    @JSONField(name = "count_picking_draft")
    @JsonProperty("count_picking_draft")
    private Integer countPickingDraft;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [COUNT_PICKING_LATE]
     *
     */
    @JSONField(name = "count_picking_late")
    @JsonProperty("count_picking_late")
    private Integer countPickingLate;

    /**
     * 属性 [COUNT_PICKING]
     *
     */
    @JSONField(name = "count_picking")
    @JsonProperty("count_picking")
    private Integer countPicking;

    /**
     * 属性 [COUNT_PICKING_READY]
     *
     */
    @JSONField(name = "count_picking_ready")
    @JsonProperty("count_picking_ready")
    private Integer countPickingReady;

    /**
     * 属性 [LAST_DONE_PICKING]
     *
     */
    @JSONField(name = "last_done_picking")
    @JsonProperty("last_done_picking")
    private String lastDonePicking;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [COUNT_PICKING_WAITING]
     *
     */
    @JSONField(name = "count_picking_waiting")
    @JsonProperty("count_picking_waiting")
    private Integer countPickingWaiting;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [RATE_PICKING_LATE]
     *
     */
    @JSONField(name = "rate_picking_late")
    @JsonProperty("rate_picking_late")
    private Integer ratePickingLate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [COUNT_MO_LATE]
     *
     */
    @JSONField(name = "count_mo_late")
    @JsonProperty("count_mo_late")
    private Integer countMoLate;

    /**
     * 属性 [USE_EXISTING_LOTS]
     *
     */
    @JSONField(name = "use_existing_lots")
    @JsonProperty("use_existing_lots")
    private String useExistingLots;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [DEFAULT_LOCATION_SRC_ID_TEXT]
     *
     */
    @JSONField(name = "default_location_src_id_text")
    @JsonProperty("default_location_src_id_text")
    private String defaultLocationSrcIdText;

    /**
     * 属性 [RETURN_PICKING_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "return_picking_type_id_text")
    @JsonProperty("return_picking_type_id_text")
    private String returnPickingTypeIdText;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    private String warehouseIdText;

    /**
     * 属性 [DEFAULT_LOCATION_DEST_ID_TEXT]
     *
     */
    @JSONField(name = "default_location_dest_id_text")
    @JsonProperty("default_location_dest_id_text")
    private String defaultLocationDestIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [DEFAULT_LOCATION_DEST_ID]
     *
     */
    @JSONField(name = "default_location_dest_id")
    @JsonProperty("default_location_dest_id")
    private Integer defaultLocationDestId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Integer warehouseId;

    /**
     * 属性 [DEFAULT_LOCATION_SRC_ID]
     *
     */
    @JSONField(name = "default_location_src_id")
    @JsonProperty("default_location_src_id")
    private Integer defaultLocationSrcId;

    /**
     * 属性 [RETURN_PICKING_TYPE_ID]
     *
     */
    @JSONField(name = "return_picking_type_id")
    @JsonProperty("return_picking_type_id")
    private Integer returnPickingTypeId;


    /**
     * 设置 [SHOW_OPERATIONS]
     */
    public void setShowOperations(String  showOperations){
        this.showOperations = showOperations ;
        this.modify("show_operations",showOperations);
    }

    /**
     * 设置 [SEQUENCE_ID]
     */
    public void setSequenceId(Integer  sequenceId){
        this.sequenceId = sequenceId ;
        this.modify("sequence_id",sequenceId);
    }

    /**
     * 设置 [USE_CREATE_LOTS]
     */
    public void setUseCreateLots(String  useCreateLots){
        this.useCreateLots = useCreateLots ;
        this.modify("use_create_lots",useCreateLots);
    }

    /**
     * 设置 [BARCODE]
     */
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [SHOW_RESERVED]
     */
    public void setShowReserved(String  showReserved){
        this.showReserved = showReserved ;
        this.modify("show_reserved",showReserved);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [SHOW_ENTIRE_PACKS]
     */
    public void setShowEntirePacks(String  showEntirePacks){
        this.showEntirePacks = showEntirePacks ;
        this.modify("show_entire_packs",showEntirePacks);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [CODE]
     */
    public void setCode(String  code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [USE_EXISTING_LOTS]
     */
    public void setUseExistingLots(String  useExistingLots){
        this.useExistingLots = useExistingLots ;
        this.modify("use_existing_lots",useExistingLots);
    }

    /**
     * 设置 [DEFAULT_LOCATION_DEST_ID]
     */
    public void setDefaultLocationDestId(Integer  defaultLocationDestId){
        this.defaultLocationDestId = defaultLocationDestId ;
        this.modify("default_location_dest_id",defaultLocationDestId);
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    public void setWarehouseId(Integer  warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }

    /**
     * 设置 [DEFAULT_LOCATION_SRC_ID]
     */
    public void setDefaultLocationSrcId(Integer  defaultLocationSrcId){
        this.defaultLocationSrcId = defaultLocationSrcId ;
        this.modify("default_location_src_id",defaultLocationSrcId);
    }

    /**
     * 设置 [RETURN_PICKING_TYPE_ID]
     */
    public void setReturnPickingTypeId(Integer  returnPickingTypeId){
        this.returnPickingTypeId = returnPickingTypeId ;
        this.modify("return_picking_type_id",returnPickingTypeId);
    }


}

