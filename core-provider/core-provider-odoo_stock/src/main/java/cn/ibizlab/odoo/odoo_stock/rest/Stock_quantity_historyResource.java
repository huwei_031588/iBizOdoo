package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quantity_history;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_quantity_historyService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantity_historySearchContext;




@Slf4j
@Api(tags = {"Stock_quantity_history" })
@RestController("odoo_stock-stock_quantity_history")
@RequestMapping("")
public class Stock_quantity_historyResource {

    @Autowired
    private IStock_quantity_historyService stock_quantity_historyService;

    @Autowired
    @Lazy
    private Stock_quantity_historyMapping stock_quantity_historyMapping;




    @PreAuthorize("hasPermission(#stock_quantity_history_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_quantity_history" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_quantity_histories/{stock_quantity_history_id}")
    public ResponseEntity<Stock_quantity_historyDTO> get(@PathVariable("stock_quantity_history_id") Integer stock_quantity_history_id) {
        Stock_quantity_history domain = stock_quantity_historyService.get(stock_quantity_history_id);
        Stock_quantity_historyDTO dto = stock_quantity_historyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#stock_quantity_history_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_quantity_history" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quantity_histories/{stock_quantity_history_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_quantity_history_id") Integer stock_quantity_history_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_quantity_historyService.remove(stock_quantity_history_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_quantity_history" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quantity_histories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_quantity_historyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_quantity_history" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quantity_histories")

    public ResponseEntity<Stock_quantity_historyDTO> create(@RequestBody Stock_quantity_historyDTO stock_quantity_historydto) {
        Stock_quantity_history domain = stock_quantity_historyMapping.toDomain(stock_quantity_historydto);
		stock_quantity_historyService.create(domain);
        Stock_quantity_historyDTO dto = stock_quantity_historyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_quantity_history" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quantity_histories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_quantity_historyDTO> stock_quantity_historydtos) {
        stock_quantity_historyService.createBatch(stock_quantity_historyMapping.toDomain(stock_quantity_historydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_quantity_history_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_quantity_history" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quantity_histories/{stock_quantity_history_id}")

    public ResponseEntity<Stock_quantity_historyDTO> update(@PathVariable("stock_quantity_history_id") Integer stock_quantity_history_id, @RequestBody Stock_quantity_historyDTO stock_quantity_historydto) {
		Stock_quantity_history domain = stock_quantity_historyMapping.toDomain(stock_quantity_historydto);
        domain.setId(stock_quantity_history_id);
		stock_quantity_historyService.update(domain);
		Stock_quantity_historyDTO dto = stock_quantity_historyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_quantity_history_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_quantity_history" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quantity_histories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_quantity_historyDTO> stock_quantity_historydtos) {
        stock_quantity_historyService.updateBatch(stock_quantity_historyMapping.toDomain(stock_quantity_historydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_quantity_history" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_quantity_histories/fetchdefault")
	public ResponseEntity<List<Stock_quantity_historyDTO>> fetchDefault(Stock_quantity_historySearchContext context) {
        Page<Stock_quantity_history> domains = stock_quantity_historyService.searchDefault(context) ;
        List<Stock_quantity_historyDTO> list = stock_quantity_historyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_quantity_history" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_quantity_histories/searchdefault")
	public ResponseEntity<Page<Stock_quantity_historyDTO>> searchDefault(Stock_quantity_historySearchContext context) {
        Page<Stock_quantity_history> domains = stock_quantity_historyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_quantity_historyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_quantity_history getEntity(){
        return new Stock_quantity_history();
    }

}
