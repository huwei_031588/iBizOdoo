package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_track_confirmationService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_confirmationSearchContext;




@Slf4j
@Api(tags = {"Stock_track_confirmation" })
@RestController("odoo_stock-stock_track_confirmation")
@RequestMapping("")
public class Stock_track_confirmationResource {

    @Autowired
    private IStock_track_confirmationService stock_track_confirmationService;

    @Autowired
    @Lazy
    private Stock_track_confirmationMapping stock_track_confirmationMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_track_confirmation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations")

    public ResponseEntity<Stock_track_confirmationDTO> create(@RequestBody Stock_track_confirmationDTO stock_track_confirmationdto) {
        Stock_track_confirmation domain = stock_track_confirmationMapping.toDomain(stock_track_confirmationdto);
		stock_track_confirmationService.create(domain);
        Stock_track_confirmationDTO dto = stock_track_confirmationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_track_confirmation" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_track_confirmationDTO> stock_track_confirmationdtos) {
        stock_track_confirmationService.createBatch(stock_track_confirmationMapping.toDomain(stock_track_confirmationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_track_confirmation_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_track_confirmation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_track_confirmations/{stock_track_confirmation_id}")
    public ResponseEntity<Stock_track_confirmationDTO> get(@PathVariable("stock_track_confirmation_id") Integer stock_track_confirmation_id) {
        Stock_track_confirmation domain = stock_track_confirmationService.get(stock_track_confirmation_id);
        Stock_track_confirmationDTO dto = stock_track_confirmationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_track_confirmation_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_track_confirmation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_confirmations/{stock_track_confirmation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_track_confirmation_id") Integer stock_track_confirmation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_track_confirmationService.remove(stock_track_confirmation_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_track_confirmation" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_confirmations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_track_confirmationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_track_confirmation_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_track_confirmation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_track_confirmations/{stock_track_confirmation_id}")

    public ResponseEntity<Stock_track_confirmationDTO> update(@PathVariable("stock_track_confirmation_id") Integer stock_track_confirmation_id, @RequestBody Stock_track_confirmationDTO stock_track_confirmationdto) {
		Stock_track_confirmation domain = stock_track_confirmationMapping.toDomain(stock_track_confirmationdto);
        domain.setId(stock_track_confirmation_id);
		stock_track_confirmationService.update(domain);
		Stock_track_confirmationDTO dto = stock_track_confirmationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_track_confirmation_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_track_confirmation" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_track_confirmations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_track_confirmationDTO> stock_track_confirmationdtos) {
        stock_track_confirmationService.updateBatch(stock_track_confirmationMapping.toDomain(stock_track_confirmationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_track_confirmation" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_track_confirmations/fetchdefault")
	public ResponseEntity<List<Stock_track_confirmationDTO>> fetchDefault(Stock_track_confirmationSearchContext context) {
        Page<Stock_track_confirmation> domains = stock_track_confirmationService.searchDefault(context) ;
        List<Stock_track_confirmationDTO> list = stock_track_confirmationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_track_confirmation" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_track_confirmations/searchdefault")
	public ResponseEntity<Page<Stock_track_confirmationDTO>> searchDefault(Stock_track_confirmationSearchContext context) {
        Page<Stock_track_confirmation> domains = stock_track_confirmationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_track_confirmationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_track_confirmation getEntity(){
        return new Stock_track_confirmation();
    }

}
