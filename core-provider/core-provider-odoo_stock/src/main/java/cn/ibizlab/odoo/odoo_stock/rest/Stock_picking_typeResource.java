package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_picking_typeService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_picking_typeSearchContext;




@Slf4j
@Api(tags = {"Stock_picking_type" })
@RestController("odoo_stock-stock_picking_type")
@RequestMapping("")
public class Stock_picking_typeResource {

    @Autowired
    private IStock_picking_typeService stock_picking_typeService;

    @Autowired
    @Lazy
    private Stock_picking_typeMapping stock_picking_typeMapping;




    @PreAuthorize("hasPermission('Remove',{#stock_picking_type_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_picking_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_picking_types/{stock_picking_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_picking_type_id") Integer stock_picking_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_picking_typeService.remove(stock_picking_type_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_picking_type" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_picking_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_picking_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_picking_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_picking_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_picking_types/{stock_picking_type_id}")

    public ResponseEntity<Stock_picking_typeDTO> update(@PathVariable("stock_picking_type_id") Integer stock_picking_type_id, @RequestBody Stock_picking_typeDTO stock_picking_typedto) {
		Stock_picking_type domain = stock_picking_typeMapping.toDomain(stock_picking_typedto);
        domain.setId(stock_picking_type_id);
		stock_picking_typeService.update(domain);
		Stock_picking_typeDTO dto = stock_picking_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_picking_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_picking_type" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_picking_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_picking_typeDTO> stock_picking_typedtos) {
        stock_picking_typeService.updateBatch(stock_picking_typeMapping.toDomain(stock_picking_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_picking_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_picking_types")

    public ResponseEntity<Stock_picking_typeDTO> create(@RequestBody Stock_picking_typeDTO stock_picking_typedto) {
        Stock_picking_type domain = stock_picking_typeMapping.toDomain(stock_picking_typedto);
		stock_picking_typeService.create(domain);
        Stock_picking_typeDTO dto = stock_picking_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_picking_type" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_picking_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_picking_typeDTO> stock_picking_typedtos) {
        stock_picking_typeService.createBatch(stock_picking_typeMapping.toDomain(stock_picking_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_picking_type_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_picking_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_picking_types/{stock_picking_type_id}")
    public ResponseEntity<Stock_picking_typeDTO> get(@PathVariable("stock_picking_type_id") Integer stock_picking_type_id) {
        Stock_picking_type domain = stock_picking_typeService.get(stock_picking_type_id);
        Stock_picking_typeDTO dto = stock_picking_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_picking_type" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_picking_types/fetchdefault")
	public ResponseEntity<List<Stock_picking_typeDTO>> fetchDefault(Stock_picking_typeSearchContext context) {
        Page<Stock_picking_type> domains = stock_picking_typeService.searchDefault(context) ;
        List<Stock_picking_typeDTO> list = stock_picking_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_picking_type" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_picking_types/searchdefault")
	public ResponseEntity<Page<Stock_picking_typeDTO>> searchDefault(Stock_picking_typeSearchContext context) {
        Page<Stock_picking_type> domains = stock_picking_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_picking_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_picking_type getEntity(){
        return new Stock_picking_type();
    }

}
