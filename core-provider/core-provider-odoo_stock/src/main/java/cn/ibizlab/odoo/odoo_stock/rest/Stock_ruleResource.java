package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_ruleService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_ruleSearchContext;




@Slf4j
@Api(tags = {"Stock_rule" })
@RestController("odoo_stock-stock_rule")
@RequestMapping("")
public class Stock_ruleResource {

    @Autowired
    private IStock_ruleService stock_ruleService;

    @Autowired
    @Lazy
    private Stock_ruleMapping stock_ruleMapping;







    @PreAuthorize("hasPermission(#stock_rule_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_rule" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_rules/{stock_rule_id}")
    public ResponseEntity<Stock_ruleDTO> get(@PathVariable("stock_rule_id") Integer stock_rule_id) {
        Stock_rule domain = stock_ruleService.get(stock_rule_id);
        Stock_ruleDTO dto = stock_ruleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_rule_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_rule" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules/{stock_rule_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_rule_id") Integer stock_rule_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_ruleService.remove(stock_rule_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_rule" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_ruleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_rule_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_rule" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_rules/{stock_rule_id}")

    public ResponseEntity<Stock_ruleDTO> update(@PathVariable("stock_rule_id") Integer stock_rule_id, @RequestBody Stock_ruleDTO stock_ruledto) {
		Stock_rule domain = stock_ruleMapping.toDomain(stock_ruledto);
        domain.setId(stock_rule_id);
		stock_ruleService.update(domain);
		Stock_ruleDTO dto = stock_ruleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_rule_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_rule" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_rules/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_ruleDTO> stock_ruledtos) {
        stock_ruleService.updateBatch(stock_ruleMapping.toDomain(stock_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_rule" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules")

    public ResponseEntity<Stock_ruleDTO> create(@RequestBody Stock_ruleDTO stock_ruledto) {
        Stock_rule domain = stock_ruleMapping.toDomain(stock_ruledto);
		stock_ruleService.create(domain);
        Stock_ruleDTO dto = stock_ruleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_rule" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_ruleDTO> stock_ruledtos) {
        stock_ruleService.createBatch(stock_ruleMapping.toDomain(stock_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_rule" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_rules/fetchdefault")
	public ResponseEntity<List<Stock_ruleDTO>> fetchDefault(Stock_ruleSearchContext context) {
        Page<Stock_rule> domains = stock_ruleService.searchDefault(context) ;
        List<Stock_ruleDTO> list = stock_ruleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_rule" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_rules/searchdefault")
	public ResponseEntity<Page<Stock_ruleDTO>> searchDefault(Stock_ruleSearchContext context) {
        Page<Stock_rule> domains = stock_ruleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_ruleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_rule getEntity(){
        return new Stock_rule();
    }

}
