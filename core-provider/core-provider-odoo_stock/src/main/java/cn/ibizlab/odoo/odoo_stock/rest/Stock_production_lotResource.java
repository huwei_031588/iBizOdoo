package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_production_lotService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_production_lotSearchContext;




@Slf4j
@Api(tags = {"Stock_production_lot" })
@RestController("odoo_stock-stock_production_lot")
@RequestMapping("")
public class Stock_production_lotResource {

    @Autowired
    private IStock_production_lotService stock_production_lotService;

    @Autowired
    @Lazy
    private Stock_production_lotMapping stock_production_lotMapping;










    @PreAuthorize("hasPermission(#stock_production_lot_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_production_lot" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_production_lots/{stock_production_lot_id}")

    public ResponseEntity<Stock_production_lotDTO> update(@PathVariable("stock_production_lot_id") Integer stock_production_lot_id, @RequestBody Stock_production_lotDTO stock_production_lotdto) {
		Stock_production_lot domain = stock_production_lotMapping.toDomain(stock_production_lotdto);
        domain.setId(stock_production_lot_id);
		stock_production_lotService.update(domain);
		Stock_production_lotDTO dto = stock_production_lotMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_production_lot_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_production_lot" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_production_lots/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_production_lotDTO> stock_production_lotdtos) {
        stock_production_lotService.updateBatch(stock_production_lotMapping.toDomain(stock_production_lotdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_production_lot_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_production_lot" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_production_lots/{stock_production_lot_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_production_lot_id") Integer stock_production_lot_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_production_lotService.remove(stock_production_lot_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_production_lot" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_production_lots/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_production_lotService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_production_lot_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_production_lot" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_production_lots/{stock_production_lot_id}")
    public ResponseEntity<Stock_production_lotDTO> get(@PathVariable("stock_production_lot_id") Integer stock_production_lot_id) {
        Stock_production_lot domain = stock_production_lotService.get(stock_production_lot_id);
        Stock_production_lotDTO dto = stock_production_lotMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_production_lot" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_production_lots")

    public ResponseEntity<Stock_production_lotDTO> create(@RequestBody Stock_production_lotDTO stock_production_lotdto) {
        Stock_production_lot domain = stock_production_lotMapping.toDomain(stock_production_lotdto);
		stock_production_lotService.create(domain);
        Stock_production_lotDTO dto = stock_production_lotMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_production_lot" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_production_lots/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_production_lotDTO> stock_production_lotdtos) {
        stock_production_lotService.createBatch(stock_production_lotMapping.toDomain(stock_production_lotdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_production_lot" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_production_lots/fetchdefault")
	public ResponseEntity<List<Stock_production_lotDTO>> fetchDefault(Stock_production_lotSearchContext context) {
        Page<Stock_production_lot> domains = stock_production_lotService.searchDefault(context) ;
        List<Stock_production_lotDTO> list = stock_production_lotMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_production_lot" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_production_lots/searchdefault")
	public ResponseEntity<Page<Stock_production_lotDTO>> searchDefault(Stock_production_lotSearchContext context) {
        Page<Stock_production_lot> domains = stock_production_lotService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_production_lotMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_production_lot getEntity(){
        return new Stock_production_lot();
    }

}
