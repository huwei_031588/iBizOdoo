package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_fixed_putaway_stratService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;




@Slf4j
@Api(tags = {"Stock_fixed_putaway_strat" })
@RestController("odoo_stock-stock_fixed_putaway_strat")
@RequestMapping("")
public class Stock_fixed_putaway_stratResource {

    @Autowired
    private IStock_fixed_putaway_stratService stock_fixed_putaway_stratService;

    @Autowired
    @Lazy
    private Stock_fixed_putaway_stratMapping stock_fixed_putaway_stratMapping;







    @PreAuthorize("hasPermission('Remove',{#stock_fixed_putaway_strat_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_fixed_putaway_strat" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_fixed_putaway_strats/{stock_fixed_putaway_strat_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_fixed_putaway_strat_id") Integer stock_fixed_putaway_strat_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_fixed_putaway_stratService.remove(stock_fixed_putaway_strat_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_fixed_putaway_strat" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_fixed_putaway_strats/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_fixed_putaway_stratService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_fixed_putaway_strat_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_fixed_putaway_strat" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_fixed_putaway_strats/{stock_fixed_putaway_strat_id}")
    public ResponseEntity<Stock_fixed_putaway_stratDTO> get(@PathVariable("stock_fixed_putaway_strat_id") Integer stock_fixed_putaway_strat_id) {
        Stock_fixed_putaway_strat domain = stock_fixed_putaway_stratService.get(stock_fixed_putaway_strat_id);
        Stock_fixed_putaway_stratDTO dto = stock_fixed_putaway_stratMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#stock_fixed_putaway_strat_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_fixed_putaway_strat" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_fixed_putaway_strats/{stock_fixed_putaway_strat_id}")

    public ResponseEntity<Stock_fixed_putaway_stratDTO> update(@PathVariable("stock_fixed_putaway_strat_id") Integer stock_fixed_putaway_strat_id, @RequestBody Stock_fixed_putaway_stratDTO stock_fixed_putaway_stratdto) {
		Stock_fixed_putaway_strat domain = stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdto);
        domain.setId(stock_fixed_putaway_strat_id);
		stock_fixed_putaway_stratService.update(domain);
		Stock_fixed_putaway_stratDTO dto = stock_fixed_putaway_stratMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_fixed_putaway_strat_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_fixed_putaway_strat" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_fixed_putaway_strats/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_fixed_putaway_stratDTO> stock_fixed_putaway_stratdtos) {
        stock_fixed_putaway_stratService.updateBatch(stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_fixed_putaway_strat" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats")

    public ResponseEntity<Stock_fixed_putaway_stratDTO> create(@RequestBody Stock_fixed_putaway_stratDTO stock_fixed_putaway_stratdto) {
        Stock_fixed_putaway_strat domain = stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdto);
		stock_fixed_putaway_stratService.create(domain);
        Stock_fixed_putaway_stratDTO dto = stock_fixed_putaway_stratMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_fixed_putaway_strat" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_fixed_putaway_strats/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_fixed_putaway_stratDTO> stock_fixed_putaway_stratdtos) {
        stock_fixed_putaway_stratService.createBatch(stock_fixed_putaway_stratMapping.toDomain(stock_fixed_putaway_stratdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_fixed_putaway_strat" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_fixed_putaway_strats/fetchdefault")
	public ResponseEntity<List<Stock_fixed_putaway_stratDTO>> fetchDefault(Stock_fixed_putaway_stratSearchContext context) {
        Page<Stock_fixed_putaway_strat> domains = stock_fixed_putaway_stratService.searchDefault(context) ;
        List<Stock_fixed_putaway_stratDTO> list = stock_fixed_putaway_stratMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_fixed_putaway_strat" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_fixed_putaway_strats/searchdefault")
	public ResponseEntity<Page<Stock_fixed_putaway_stratDTO>> searchDefault(Stock_fixed_putaway_stratSearchContext context) {
        Page<Stock_fixed_putaway_strat> domains = stock_fixed_putaway_stratService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_fixed_putaway_stratMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_fixed_putaway_strat getEntity(){
        return new Stock_fixed_putaway_strat();
    }

}
