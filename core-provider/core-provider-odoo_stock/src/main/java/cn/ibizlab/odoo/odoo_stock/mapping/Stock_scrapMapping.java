package cn.ibizlab.odoo.odoo_stock.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap;
import cn.ibizlab.odoo.odoo_stock.dto.Stock_scrapDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_scrapMapping extends MappingBase<Stock_scrapDTO, Stock_scrap> {


}

