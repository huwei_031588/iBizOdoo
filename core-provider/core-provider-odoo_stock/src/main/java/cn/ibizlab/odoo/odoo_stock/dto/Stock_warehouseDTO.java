package cn.ibizlab.odoo.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Stock_warehouseDTO]
 */
@Data
public class Stock_warehouseDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RESUPPLY_ROUTE_IDS]
     *
     */
    @JSONField(name = "resupply_route_ids")
    @JsonProperty("resupply_route_ids")
    private String resupplyRouteIds;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [RESUPPLY_WH_IDS]
     *
     */
    @JSONField(name = "resupply_wh_ids")
    @JsonProperty("resupply_wh_ids")
    private String resupplyWhIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [DELIVERY_STEPS]
     *
     */
    @JSONField(name = "delivery_steps")
    @JsonProperty("delivery_steps")
    private String deliverySteps;

    /**
     * 属性 [WAREHOUSE_COUNT]
     *
     */
    @JSONField(name = "warehouse_count")
    @JsonProperty("warehouse_count")
    private Integer warehouseCount;

    /**
     * 属性 [MANUFACTURE_TO_RESUPPLY]
     *
     */
    @JSONField(name = "manufacture_to_resupply")
    @JsonProperty("manufacture_to_resupply")
    private String manufactureToResupply;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [RECEPTION_STEPS]
     *
     */
    @JSONField(name = "reception_steps")
    @JsonProperty("reception_steps")
    private String receptionSteps;

    /**
     * 属性 [BUY_TO_RESUPPLY]
     *
     */
    @JSONField(name = "buy_to_resupply")
    @JsonProperty("buy_to_resupply")
    private String buyToResupply;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 属性 [MANUFACTURE_STEPS]
     *
     */
    @JSONField(name = "manufacture_steps")
    @JsonProperty("manufacture_steps")
    private String manufactureSteps;

    /**
     * 属性 [SHOW_RESUPPLY]
     *
     */
    @JSONField(name = "show_resupply")
    @JsonProperty("show_resupply")
    private String showResupply;

    /**
     * 属性 [VIEW_LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "view_location_id_text")
    @JsonProperty("view_location_id_text")
    private String viewLocationIdText;

    /**
     * 属性 [WH_INPUT_STOCK_LOC_ID_TEXT]
     *
     */
    @JSONField(name = "wh_input_stock_loc_id_text")
    @JsonProperty("wh_input_stock_loc_id_text")
    private String whInputStockLocIdText;

    /**
     * 属性 [SAM_LOC_ID_TEXT]
     *
     */
    @JSONField(name = "sam_loc_id_text")
    @JsonProperty("sam_loc_id_text")
    private String samLocIdText;

    /**
     * 属性 [CROSSDOCK_ROUTE_ID_TEXT]
     *
     */
    @JSONField(name = "crossdock_route_id_text")
    @JsonProperty("crossdock_route_id_text")
    private String crossdockRouteIdText;

    /**
     * 属性 [PBM_ROUTE_ID_TEXT]
     *
     */
    @JSONField(name = "pbm_route_id_text")
    @JsonProperty("pbm_route_id_text")
    private String pbmRouteIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PBM_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "pbm_type_id_text")
    @JsonProperty("pbm_type_id_text")
    private String pbmTypeIdText;

    /**
     * 属性 [PICK_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "pick_type_id_text")
    @JsonProperty("pick_type_id_text")
    private String pickTypeIdText;

    /**
     * 属性 [INT_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "int_type_id_text")
    @JsonProperty("int_type_id_text")
    private String intTypeIdText;

    /**
     * 属性 [MANU_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "manu_type_id_text")
    @JsonProperty("manu_type_id_text")
    private String manuTypeIdText;

    /**
     * 属性 [BUY_PULL_ID_TEXT]
     *
     */
    @JSONField(name = "buy_pull_id_text")
    @JsonProperty("buy_pull_id_text")
    private String buyPullIdText;

    /**
     * 属性 [WH_PACK_STOCK_LOC_ID_TEXT]
     *
     */
    @JSONField(name = "wh_pack_stock_loc_id_text")
    @JsonProperty("wh_pack_stock_loc_id_text")
    private String whPackStockLocIdText;

    /**
     * 属性 [MANUFACTURE_PULL_ID_TEXT]
     *
     */
    @JSONField(name = "manufacture_pull_id_text")
    @JsonProperty("manufacture_pull_id_text")
    private String manufacturePullIdText;

    /**
     * 属性 [RECEPTION_ROUTE_ID_TEXT]
     *
     */
    @JSONField(name = "reception_route_id_text")
    @JsonProperty("reception_route_id_text")
    private String receptionRouteIdText;

    /**
     * 属性 [DELIVERY_ROUTE_ID_TEXT]
     *
     */
    @JSONField(name = "delivery_route_id_text")
    @JsonProperty("delivery_route_id_text")
    private String deliveryRouteIdText;

    /**
     * 属性 [SAM_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "sam_type_id_text")
    @JsonProperty("sam_type_id_text")
    private String samTypeIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 属性 [OUT_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "out_type_id_text")
    @JsonProperty("out_type_id_text")
    private String outTypeIdText;

    /**
     * 属性 [MTO_PULL_ID_TEXT]
     *
     */
    @JSONField(name = "mto_pull_id_text")
    @JsonProperty("mto_pull_id_text")
    private String mtoPullIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [LOT_STOCK_ID_TEXT]
     *
     */
    @JSONField(name = "lot_stock_id_text")
    @JsonProperty("lot_stock_id_text")
    private String lotStockIdText;

    /**
     * 属性 [PBM_MTO_PULL_ID_TEXT]
     *
     */
    @JSONField(name = "pbm_mto_pull_id_text")
    @JsonProperty("pbm_mto_pull_id_text")
    private String pbmMtoPullIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [PBM_LOC_ID_TEXT]
     *
     */
    @JSONField(name = "pbm_loc_id_text")
    @JsonProperty("pbm_loc_id_text")
    private String pbmLocIdText;

    /**
     * 属性 [PACK_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "pack_type_id_text")
    @JsonProperty("pack_type_id_text")
    private String packTypeIdText;

    /**
     * 属性 [SAM_RULE_ID_TEXT]
     *
     */
    @JSONField(name = "sam_rule_id_text")
    @JsonProperty("sam_rule_id_text")
    private String samRuleIdText;

    /**
     * 属性 [IN_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "in_type_id_text")
    @JsonProperty("in_type_id_text")
    private String inTypeIdText;

    /**
     * 属性 [WH_OUTPUT_STOCK_LOC_ID_TEXT]
     *
     */
    @JSONField(name = "wh_output_stock_loc_id_text")
    @JsonProperty("wh_output_stock_loc_id_text")
    private String whOutputStockLocIdText;

    /**
     * 属性 [WH_QC_STOCK_LOC_ID_TEXT]
     *
     */
    @JSONField(name = "wh_qc_stock_loc_id_text")
    @JsonProperty("wh_qc_stock_loc_id_text")
    private String whQcStockLocIdText;

    /**
     * 属性 [WH_OUTPUT_STOCK_LOC_ID]
     *
     */
    @JSONField(name = "wh_output_stock_loc_id")
    @JsonProperty("wh_output_stock_loc_id")
    private Integer whOutputStockLocId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [PBM_LOC_ID]
     *
     */
    @JSONField(name = "pbm_loc_id")
    @JsonProperty("pbm_loc_id")
    private Integer pbmLocId;

    /**
     * 属性 [VIEW_LOCATION_ID]
     *
     */
    @JSONField(name = "view_location_id")
    @JsonProperty("view_location_id")
    private Integer viewLocationId;

    /**
     * 属性 [OUT_TYPE_ID]
     *
     */
    @JSONField(name = "out_type_id")
    @JsonProperty("out_type_id")
    private Integer outTypeId;

    /**
     * 属性 [MANU_TYPE_ID]
     *
     */
    @JSONField(name = "manu_type_id")
    @JsonProperty("manu_type_id")
    private Integer manuTypeId;

    /**
     * 属性 [PBM_MTO_PULL_ID]
     *
     */
    @JSONField(name = "pbm_mto_pull_id")
    @JsonProperty("pbm_mto_pull_id")
    private Integer pbmMtoPullId;

    /**
     * 属性 [MTO_PULL_ID]
     *
     */
    @JSONField(name = "mto_pull_id")
    @JsonProperty("mto_pull_id")
    private Integer mtoPullId;

    /**
     * 属性 [SAM_TYPE_ID]
     *
     */
    @JSONField(name = "sam_type_id")
    @JsonProperty("sam_type_id")
    private Integer samTypeId;

    /**
     * 属性 [MANUFACTURE_PULL_ID]
     *
     */
    @JSONField(name = "manufacture_pull_id")
    @JsonProperty("manufacture_pull_id")
    private Integer manufacturePullId;

    /**
     * 属性 [SAM_LOC_ID]
     *
     */
    @JSONField(name = "sam_loc_id")
    @JsonProperty("sam_loc_id")
    private Integer samLocId;

    /**
     * 属性 [BUY_PULL_ID]
     *
     */
    @JSONField(name = "buy_pull_id")
    @JsonProperty("buy_pull_id")
    private Integer buyPullId;

    /**
     * 属性 [INT_TYPE_ID]
     *
     */
    @JSONField(name = "int_type_id")
    @JsonProperty("int_type_id")
    private Integer intTypeId;

    /**
     * 属性 [WH_QC_STOCK_LOC_ID]
     *
     */
    @JSONField(name = "wh_qc_stock_loc_id")
    @JsonProperty("wh_qc_stock_loc_id")
    private Integer whQcStockLocId;

    /**
     * 属性 [IN_TYPE_ID]
     *
     */
    @JSONField(name = "in_type_id")
    @JsonProperty("in_type_id")
    private Integer inTypeId;

    /**
     * 属性 [PBM_ROUTE_ID]
     *
     */
    @JSONField(name = "pbm_route_id")
    @JsonProperty("pbm_route_id")
    private Integer pbmRouteId;

    /**
     * 属性 [PICK_TYPE_ID]
     *
     */
    @JSONField(name = "pick_type_id")
    @JsonProperty("pick_type_id")
    private Integer pickTypeId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [RECEPTION_ROUTE_ID]
     *
     */
    @JSONField(name = "reception_route_id")
    @JsonProperty("reception_route_id")
    private Integer receptionRouteId;

    /**
     * 属性 [PACK_TYPE_ID]
     *
     */
    @JSONField(name = "pack_type_id")
    @JsonProperty("pack_type_id")
    private Integer packTypeId;

    /**
     * 属性 [PBM_TYPE_ID]
     *
     */
    @JSONField(name = "pbm_type_id")
    @JsonProperty("pbm_type_id")
    private Integer pbmTypeId;

    /**
     * 属性 [DELIVERY_ROUTE_ID]
     *
     */
    @JSONField(name = "delivery_route_id")
    @JsonProperty("delivery_route_id")
    private Integer deliveryRouteId;

    /**
     * 属性 [WH_INPUT_STOCK_LOC_ID]
     *
     */
    @JSONField(name = "wh_input_stock_loc_id")
    @JsonProperty("wh_input_stock_loc_id")
    private Integer whInputStockLocId;

    /**
     * 属性 [LOT_STOCK_ID]
     *
     */
    @JSONField(name = "lot_stock_id")
    @JsonProperty("lot_stock_id")
    private Integer lotStockId;

    /**
     * 属性 [SAM_RULE_ID]
     *
     */
    @JSONField(name = "sam_rule_id")
    @JsonProperty("sam_rule_id")
    private Integer samRuleId;

    /**
     * 属性 [CROSSDOCK_ROUTE_ID]
     *
     */
    @JSONField(name = "crossdock_route_id")
    @JsonProperty("crossdock_route_id")
    private Integer crossdockRouteId;

    /**
     * 属性 [WH_PACK_STOCK_LOC_ID]
     *
     */
    @JSONField(name = "wh_pack_stock_loc_id")
    @JsonProperty("wh_pack_stock_loc_id")
    private Integer whPackStockLocId;


    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [DELIVERY_STEPS]
     */
    public void setDeliverySteps(String  deliverySteps){
        this.deliverySteps = deliverySteps ;
        this.modify("delivery_steps",deliverySteps);
    }

    /**
     * 设置 [MANUFACTURE_TO_RESUPPLY]
     */
    public void setManufactureToResupply(String  manufactureToResupply){
        this.manufactureToResupply = manufactureToResupply ;
        this.modify("manufacture_to_resupply",manufactureToResupply);
    }

    /**
     * 设置 [RECEPTION_STEPS]
     */
    public void setReceptionSteps(String  receptionSteps){
        this.receptionSteps = receptionSteps ;
        this.modify("reception_steps",receptionSteps);
    }

    /**
     * 设置 [BUY_TO_RESUPPLY]
     */
    public void setBuyToResupply(String  buyToResupply){
        this.buyToResupply = buyToResupply ;
        this.modify("buy_to_resupply",buyToResupply);
    }

    /**
     * 设置 [CODE]
     */
    public void setCode(String  code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [MANUFACTURE_STEPS]
     */
    public void setManufactureSteps(String  manufactureSteps){
        this.manufactureSteps = manufactureSteps ;
        this.modify("manufacture_steps",manufactureSteps);
    }

    /**
     * 设置 [WH_OUTPUT_STOCK_LOC_ID]
     */
    public void setWhOutputStockLocId(Integer  whOutputStockLocId){
        this.whOutputStockLocId = whOutputStockLocId ;
        this.modify("wh_output_stock_loc_id",whOutputStockLocId);
    }

    /**
     * 设置 [PBM_LOC_ID]
     */
    public void setPbmLocId(Integer  pbmLocId){
        this.pbmLocId = pbmLocId ;
        this.modify("pbm_loc_id",pbmLocId);
    }

    /**
     * 设置 [VIEW_LOCATION_ID]
     */
    public void setViewLocationId(Integer  viewLocationId){
        this.viewLocationId = viewLocationId ;
        this.modify("view_location_id",viewLocationId);
    }

    /**
     * 设置 [OUT_TYPE_ID]
     */
    public void setOutTypeId(Integer  outTypeId){
        this.outTypeId = outTypeId ;
        this.modify("out_type_id",outTypeId);
    }

    /**
     * 设置 [MANU_TYPE_ID]
     */
    public void setManuTypeId(Integer  manuTypeId){
        this.manuTypeId = manuTypeId ;
        this.modify("manu_type_id",manuTypeId);
    }

    /**
     * 设置 [PBM_MTO_PULL_ID]
     */
    public void setPbmMtoPullId(Integer  pbmMtoPullId){
        this.pbmMtoPullId = pbmMtoPullId ;
        this.modify("pbm_mto_pull_id",pbmMtoPullId);
    }

    /**
     * 设置 [MTO_PULL_ID]
     */
    public void setMtoPullId(Integer  mtoPullId){
        this.mtoPullId = mtoPullId ;
        this.modify("mto_pull_id",mtoPullId);
    }

    /**
     * 设置 [SAM_TYPE_ID]
     */
    public void setSamTypeId(Integer  samTypeId){
        this.samTypeId = samTypeId ;
        this.modify("sam_type_id",samTypeId);
    }

    /**
     * 设置 [MANUFACTURE_PULL_ID]
     */
    public void setManufacturePullId(Integer  manufacturePullId){
        this.manufacturePullId = manufacturePullId ;
        this.modify("manufacture_pull_id",manufacturePullId);
    }

    /**
     * 设置 [SAM_LOC_ID]
     */
    public void setSamLocId(Integer  samLocId){
        this.samLocId = samLocId ;
        this.modify("sam_loc_id",samLocId);
    }

    /**
     * 设置 [BUY_PULL_ID]
     */
    public void setBuyPullId(Integer  buyPullId){
        this.buyPullId = buyPullId ;
        this.modify("buy_pull_id",buyPullId);
    }

    /**
     * 设置 [INT_TYPE_ID]
     */
    public void setIntTypeId(Integer  intTypeId){
        this.intTypeId = intTypeId ;
        this.modify("int_type_id",intTypeId);
    }

    /**
     * 设置 [WH_QC_STOCK_LOC_ID]
     */
    public void setWhQcStockLocId(Integer  whQcStockLocId){
        this.whQcStockLocId = whQcStockLocId ;
        this.modify("wh_qc_stock_loc_id",whQcStockLocId);
    }

    /**
     * 设置 [IN_TYPE_ID]
     */
    public void setInTypeId(Integer  inTypeId){
        this.inTypeId = inTypeId ;
        this.modify("in_type_id",inTypeId);
    }

    /**
     * 设置 [PBM_ROUTE_ID]
     */
    public void setPbmRouteId(Integer  pbmRouteId){
        this.pbmRouteId = pbmRouteId ;
        this.modify("pbm_route_id",pbmRouteId);
    }

    /**
     * 设置 [PICK_TYPE_ID]
     */
    public void setPickTypeId(Integer  pickTypeId){
        this.pickTypeId = pickTypeId ;
        this.modify("pick_type_id",pickTypeId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [RECEPTION_ROUTE_ID]
     */
    public void setReceptionRouteId(Integer  receptionRouteId){
        this.receptionRouteId = receptionRouteId ;
        this.modify("reception_route_id",receptionRouteId);
    }

    /**
     * 设置 [PACK_TYPE_ID]
     */
    public void setPackTypeId(Integer  packTypeId){
        this.packTypeId = packTypeId ;
        this.modify("pack_type_id",packTypeId);
    }

    /**
     * 设置 [PBM_TYPE_ID]
     */
    public void setPbmTypeId(Integer  pbmTypeId){
        this.pbmTypeId = pbmTypeId ;
        this.modify("pbm_type_id",pbmTypeId);
    }

    /**
     * 设置 [DELIVERY_ROUTE_ID]
     */
    public void setDeliveryRouteId(Integer  deliveryRouteId){
        this.deliveryRouteId = deliveryRouteId ;
        this.modify("delivery_route_id",deliveryRouteId);
    }

    /**
     * 设置 [WH_INPUT_STOCK_LOC_ID]
     */
    public void setWhInputStockLocId(Integer  whInputStockLocId){
        this.whInputStockLocId = whInputStockLocId ;
        this.modify("wh_input_stock_loc_id",whInputStockLocId);
    }

    /**
     * 设置 [LOT_STOCK_ID]
     */
    public void setLotStockId(Integer  lotStockId){
        this.lotStockId = lotStockId ;
        this.modify("lot_stock_id",lotStockId);
    }

    /**
     * 设置 [SAM_RULE_ID]
     */
    public void setSamRuleId(Integer  samRuleId){
        this.samRuleId = samRuleId ;
        this.modify("sam_rule_id",samRuleId);
    }

    /**
     * 设置 [CROSSDOCK_ROUTE_ID]
     */
    public void setCrossdockRouteId(Integer  crossdockRouteId){
        this.crossdockRouteId = crossdockRouteId ;
        this.modify("crossdock_route_id",crossdockRouteId);
    }

    /**
     * 设置 [WH_PACK_STOCK_LOC_ID]
     */
    public void setWhPackStockLocId(Integer  whPackStockLocId){
        this.whPackStockLocId = whPackStockLocId ;
        this.modify("wh_pack_stock_loc_id",whPackStockLocId);
    }


}

