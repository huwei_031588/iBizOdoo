package cn.ibizlab.odoo.odoo_stock.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_overprocessed_transfer;
import cn.ibizlab.odoo.odoo_stock.dto.Stock_overprocessed_transferDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_overprocessed_transferMapping extends MappingBase<Stock_overprocessed_transferDTO, Stock_overprocessed_transfer> {


}

