package cn.ibizlab.odoo.odoo_rating.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Rating_ratingDTO]
 */
@Data
public class Rating_ratingDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [FEEDBACK]
     *
     */
    @JSONField(name = "feedback")
    @JsonProperty("feedback")
    private String feedback;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [RATING]
     *
     */
    @JSONField(name = "rating")
    @JsonProperty("rating")
    private Double rating;

    /**
     * 属性 [RATING_TEXT]
     *
     */
    @JSONField(name = "rating_text")
    @JsonProperty("rating_text")
    private String ratingText;

    /**
     * 属性 [PARENT_RES_NAME]
     *
     */
    @JSONField(name = "parent_res_name")
    @JsonProperty("parent_res_name")
    private String parentResName;

    /**
     * 属性 [PARENT_RES_ID]
     *
     */
    @JSONField(name = "parent_res_id")
    @JsonProperty("parent_res_id")
    private Integer parentResId;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [RES_NAME]
     *
     */
    @JSONField(name = "res_name")
    @JsonProperty("res_name")
    private String resName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PARENT_RES_MODEL_ID]
     *
     */
    @JSONField(name = "parent_res_model_id")
    @JsonProperty("parent_res_model_id")
    private Integer parentResModelId;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 属性 [CONSUMED]
     *
     */
    @JSONField(name = "consumed")
    @JsonProperty("consumed")
    private String consumed;

    /**
     * 属性 [RATING_IMAGE]
     *
     */
    @JSONField(name = "rating_image")
    @JsonProperty("rating_image")
    private byte[] ratingImage;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [RES_MODEL_ID]
     *
     */
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    private Integer resModelId;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * 属性 [PARENT_RES_MODEL]
     *
     */
    @JSONField(name = "parent_res_model")
    @JsonProperty("parent_res_model")
    private String parentResModel;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 属性 [RATED_PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "rated_partner_id_text")
    @JsonProperty("rated_partner_id_text")
    private String ratedPartnerIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [MESSAGE_ID]
     *
     */
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private Integer messageId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [RATED_PARTNER_ID]
     *
     */
    @JSONField(name = "rated_partner_id")
    @JsonProperty("rated_partner_id")
    private Integer ratedPartnerId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [FEEDBACK]
     */
    public void setFeedback(String  feedback){
        this.feedback = feedback ;
        this.modify("feedback",feedback);
    }

    /**
     * 设置 [RATING]
     */
    public void setRating(Double  rating){
        this.rating = rating ;
        this.modify("rating",rating);
    }

    /**
     * 设置 [RATING_TEXT]
     */
    public void setRatingText(String  ratingText){
        this.ratingText = ratingText ;
        this.modify("rating_text",ratingText);
    }

    /**
     * 设置 [PARENT_RES_NAME]
     */
    public void setParentResName(String  parentResName){
        this.parentResName = parentResName ;
        this.modify("parent_res_name",parentResName);
    }

    /**
     * 设置 [PARENT_RES_ID]
     */
    public void setParentResId(Integer  parentResId){
        this.parentResId = parentResId ;
        this.modify("parent_res_id",parentResId);
    }

    /**
     * 设置 [RES_NAME]
     */
    public void setResName(String  resName){
        this.resName = resName ;
        this.modify("res_name",resName);
    }

    /**
     * 设置 [PARENT_RES_MODEL_ID]
     */
    public void setParentResModelId(Integer  parentResModelId){
        this.parentResModelId = parentResModelId ;
        this.modify("parent_res_model_id",parentResModelId);
    }

    /**
     * 设置 [RES_ID]
     */
    public void setResId(Integer  resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [CONSUMED]
     */
    public void setConsumed(String  consumed){
        this.consumed = consumed ;
        this.modify("consumed",consumed);
    }

    /**
     * 设置 [RES_MODEL_ID]
     */
    public void setResModelId(Integer  resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }

    /**
     * 设置 [RES_MODEL]
     */
    public void setResModel(String  resModel){
        this.resModel = resModel ;
        this.modify("res_model",resModel);
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    public void setAccessToken(String  accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }

    /**
     * 设置 [PARENT_RES_MODEL]
     */
    public void setParentResModel(String  parentResModel){
        this.parentResModel = parentResModel ;
        this.modify("parent_res_model",parentResModel);
    }

    /**
     * 设置 [MESSAGE_ID]
     */
    public void setMessageId(Integer  messageId){
        this.messageId = messageId ;
        this.modify("message_id",messageId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [RATED_PARTNER_ID]
     */
    public void setRatedPartnerId(Integer  ratedPartnerId){
        this.ratedPartnerId = ratedPartnerId ;
        this.modify("rated_partner_id",ratedPartnerId);
    }


}

