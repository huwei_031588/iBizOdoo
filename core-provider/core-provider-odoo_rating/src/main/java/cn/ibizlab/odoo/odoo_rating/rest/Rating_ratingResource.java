package cn.ibizlab.odoo.odoo_rating.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_rating.dto.*;
import cn.ibizlab.odoo.odoo_rating.mapping.*;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.odoo.core.odoo_rating.service.IRating_ratingService;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_ratingSearchContext;




@Slf4j
@Api(tags = {"Rating_rating" })
@RestController("odoo_rating-rating_rating")
@RequestMapping("")
public class Rating_ratingResource {

    @Autowired
    private IRating_ratingService rating_ratingService;

    @Autowired
    @Lazy
    private Rating_ratingMapping rating_ratingMapping;




    @PreAuthorize("hasPermission(#rating_rating_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Rating_rating" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/rating_ratings/{rating_rating_id}")

    public ResponseEntity<Rating_ratingDTO> update(@PathVariable("rating_rating_id") Integer rating_rating_id, @RequestBody Rating_ratingDTO rating_ratingdto) {
		Rating_rating domain = rating_ratingMapping.toDomain(rating_ratingdto);
        domain.setId(rating_rating_id);
		rating_ratingService.update(domain);
		Rating_ratingDTO dto = rating_ratingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#rating_rating_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Rating_rating" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/rating_ratings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Rating_ratingDTO> rating_ratingdtos) {
        rating_ratingService.updateBatch(rating_ratingMapping.toDomain(rating_ratingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Rating_rating" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_ratings")

    public ResponseEntity<Rating_ratingDTO> create(@RequestBody Rating_ratingDTO rating_ratingdto) {
        Rating_rating domain = rating_ratingMapping.toDomain(rating_ratingdto);
		rating_ratingService.create(domain);
        Rating_ratingDTO dto = rating_ratingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Rating_rating" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_ratings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Rating_ratingDTO> rating_ratingdtos) {
        rating_ratingService.createBatch(rating_ratingMapping.toDomain(rating_ratingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#rating_rating_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Rating_rating" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/rating_ratings/{rating_rating_id}")
    public ResponseEntity<Rating_ratingDTO> get(@PathVariable("rating_rating_id") Integer rating_rating_id) {
        Rating_rating domain = rating_ratingService.get(rating_rating_id);
        Rating_ratingDTO dto = rating_ratingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#rating_rating_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Rating_rating" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/rating_ratings/{rating_rating_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("rating_rating_id") Integer rating_rating_id) {
         return ResponseEntity.status(HttpStatus.OK).body(rating_ratingService.remove(rating_rating_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Rating_rating" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/rating_ratings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        rating_ratingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Rating_rating" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/rating_ratings/fetchdefault")
	public ResponseEntity<List<Rating_ratingDTO>> fetchDefault(Rating_ratingSearchContext context) {
        Page<Rating_rating> domains = rating_ratingService.searchDefault(context) ;
        List<Rating_ratingDTO> list = rating_ratingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Rating_rating" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/rating_ratings/searchdefault")
	public ResponseEntity<Page<Rating_ratingDTO>> searchDefault(Rating_ratingSearchContext context) {
        Page<Rating_rating> domains = rating_ratingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(rating_ratingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Rating_rating getEntity(){
        return new Rating_rating();
    }

}
