package cn.ibizlab.odoo.odoo_rating.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_rating.dto.*;
import cn.ibizlab.odoo.odoo_rating.mapping.*;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.odoo.core.odoo_rating.service.IRating_mixinService;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_mixinSearchContext;




@Slf4j
@Api(tags = {"Rating_mixin" })
@RestController("odoo_rating-rating_mixin")
@RequestMapping("")
public class Rating_mixinResource {

    @Autowired
    private IRating_mixinService rating_mixinService;

    @Autowired
    @Lazy
    private Rating_mixinMapping rating_mixinMapping;







    @PreAuthorize("hasPermission(#rating_mixin_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Rating_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/rating_mixins/{rating_mixin_id}")
    public ResponseEntity<Rating_mixinDTO> get(@PathVariable("rating_mixin_id") Integer rating_mixin_id) {
        Rating_mixin domain = rating_mixinService.get(rating_mixin_id);
        Rating_mixinDTO dto = rating_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Rating_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_mixins")

    public ResponseEntity<Rating_mixinDTO> create(@RequestBody Rating_mixinDTO rating_mixindto) {
        Rating_mixin domain = rating_mixinMapping.toDomain(rating_mixindto);
		rating_mixinService.create(domain);
        Rating_mixinDTO dto = rating_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Rating_mixin" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/rating_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Rating_mixinDTO> rating_mixindtos) {
        rating_mixinService.createBatch(rating_mixinMapping.toDomain(rating_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#rating_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Rating_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/rating_mixins/{rating_mixin_id}")

    public ResponseEntity<Rating_mixinDTO> update(@PathVariable("rating_mixin_id") Integer rating_mixin_id, @RequestBody Rating_mixinDTO rating_mixindto) {
		Rating_mixin domain = rating_mixinMapping.toDomain(rating_mixindto);
        domain.setId(rating_mixin_id);
		rating_mixinService.update(domain);
		Rating_mixinDTO dto = rating_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#rating_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Rating_mixin" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/rating_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Rating_mixinDTO> rating_mixindtos) {
        rating_mixinService.updateBatch(rating_mixinMapping.toDomain(rating_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#rating_mixin_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Rating_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/rating_mixins/{rating_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("rating_mixin_id") Integer rating_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(rating_mixinService.remove(rating_mixin_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Rating_mixin" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/rating_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        rating_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Rating_mixin" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/rating_mixins/fetchdefault")
	public ResponseEntity<List<Rating_mixinDTO>> fetchDefault(Rating_mixinSearchContext context) {
        Page<Rating_mixin> domains = rating_mixinService.searchDefault(context) ;
        List<Rating_mixinDTO> list = rating_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Rating_mixin" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/rating_mixins/searchdefault")
	public ResponseEntity<Page<Rating_mixinDTO>> searchDefault(Rating_mixinSearchContext context) {
        Page<Rating_mixin> domains = rating_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(rating_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Rating_mixin getEntity(){
        return new Rating_mixin();
    }

}
