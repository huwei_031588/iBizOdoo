package cn.ibizlab.odoo.odoo_board.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_board")
public class odoo_boardRestConfiguration {

}
