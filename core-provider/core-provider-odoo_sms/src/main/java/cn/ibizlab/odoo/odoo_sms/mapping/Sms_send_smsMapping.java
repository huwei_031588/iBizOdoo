package cn.ibizlab.odoo.odoo_sms.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_send_sms;
import cn.ibizlab.odoo.odoo_sms.dto.Sms_send_smsDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Sms_send_smsMapping extends MappingBase<Sms_send_smsDTO, Sms_send_sms> {


}

