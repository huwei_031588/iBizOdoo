package cn.ibizlab.odoo.odoo_sms.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_sms.dto.*;
import cn.ibizlab.odoo.odoo_sms.mapping.*;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api;
import cn.ibizlab.odoo.core.odoo_sms.service.ISms_apiService;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_apiSearchContext;




@Slf4j
@Api(tags = {"Sms_api" })
@RestController("odoo_sms-sms_api")
@RequestMapping("")
public class Sms_apiResource {

    @Autowired
    private ISms_apiService sms_apiService;

    @Autowired
    @Lazy
    private Sms_apiMapping sms_apiMapping;




    @PreAuthorize("hasPermission(#sms_api_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Sms_api" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/sms_apis/{sms_api_id}")
    public ResponseEntity<Sms_apiDTO> get(@PathVariable("sms_api_id") Integer sms_api_id) {
        Sms_api domain = sms_apiService.get(sms_api_id);
        Sms_apiDTO dto = sms_apiMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#sms_api_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Sms_api" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sms_apis/{sms_api_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sms_api_id") Integer sms_api_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sms_apiService.remove(sms_api_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Sms_api" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sms_apis/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        sms_apiService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Sms_api" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_apis")

    public ResponseEntity<Sms_apiDTO> create(@RequestBody Sms_apiDTO sms_apidto) {
        Sms_api domain = sms_apiMapping.toDomain(sms_apidto);
		sms_apiService.create(domain);
        Sms_apiDTO dto = sms_apiMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Sms_api" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_apis/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sms_apiDTO> sms_apidtos) {
        sms_apiService.createBatch(sms_apiMapping.toDomain(sms_apidtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#sms_api_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Sms_api" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/sms_apis/{sms_api_id}")

    public ResponseEntity<Sms_apiDTO> update(@PathVariable("sms_api_id") Integer sms_api_id, @RequestBody Sms_apiDTO sms_apidto) {
		Sms_api domain = sms_apiMapping.toDomain(sms_apidto);
        domain.setId(sms_api_id);
		sms_apiService.update(domain);
		Sms_apiDTO dto = sms_apiMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#sms_api_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Sms_api" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/sms_apis/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sms_apiDTO> sms_apidtos) {
        sms_apiService.updateBatch(sms_apiMapping.toDomain(sms_apidtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Sms_api" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sms_apis/fetchdefault")
	public ResponseEntity<List<Sms_apiDTO>> fetchDefault(Sms_apiSearchContext context) {
        Page<Sms_api> domains = sms_apiService.searchDefault(context) ;
        List<Sms_apiDTO> list = sms_apiMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Sms_api" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sms_apis/searchdefault")
	public ResponseEntity<Page<Sms_apiDTO>> searchDefault(Sms_apiSearchContext context) {
        Page<Sms_api> domains = sms_apiService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sms_apiMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Sms_api getEntity(){
        return new Sms_api();
    }

}
