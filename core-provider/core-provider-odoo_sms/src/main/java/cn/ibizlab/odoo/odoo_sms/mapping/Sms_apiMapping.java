package cn.ibizlab.odoo.odoo_sms.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api;
import cn.ibizlab.odoo.odoo_sms.dto.Sms_apiDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Sms_apiMapping extends MappingBase<Sms_apiDTO, Sms_api> {


}

