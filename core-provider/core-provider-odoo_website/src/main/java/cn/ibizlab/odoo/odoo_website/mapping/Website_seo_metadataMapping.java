package cn.ibizlab.odoo.odoo_website.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.odoo.odoo_website.dto.Website_seo_metadataDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Website_seo_metadataMapping extends MappingBase<Website_seo_metadataDTO, Website_seo_metadata> {


}

