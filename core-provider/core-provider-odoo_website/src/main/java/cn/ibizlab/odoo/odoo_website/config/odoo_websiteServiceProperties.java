package cn.ibizlab.odoo.odoo_website.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-website")
@Data
public class odoo_websiteServiceProperties {

	private boolean enabled;

	private boolean auth;


}