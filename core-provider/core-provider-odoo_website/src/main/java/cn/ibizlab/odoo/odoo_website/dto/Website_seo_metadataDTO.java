package cn.ibizlab.odoo.odoo_website.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Website_seo_metadataDTO]
 */
@Data
public class Website_seo_metadataDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;


    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    public void setWebsiteMetaOgImg(String  websiteMetaOgImg){
        this.websiteMetaOgImg = websiteMetaOgImg ;
        this.modify("website_meta_og_img",websiteMetaOgImg);
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    public void setWebsiteMetaTitle(String  websiteMetaTitle){
        this.websiteMetaTitle = websiteMetaTitle ;
        this.modify("website_meta_title",websiteMetaTitle);
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    public void setWebsiteMetaKeywords(String  websiteMetaKeywords){
        this.websiteMetaKeywords = websiteMetaKeywords ;
        this.modify("website_meta_keywords",websiteMetaKeywords);
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    public void setWebsiteMetaDescription(String  websiteMetaDescription){
        this.websiteMetaDescription = websiteMetaDescription ;
        this.modify("website_meta_description",websiteMetaDescription);
    }


}

