package cn.ibizlab.odoo.odoo_website.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_website.dto.*;
import cn.ibizlab.odoo.odoo_website.mapping.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_page;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_pageService;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_pageSearchContext;




@Slf4j
@Api(tags = {"Website_page" })
@RestController("odoo_website-website_page")
@RequestMapping("")
public class Website_pageResource {

    @Autowired
    private IWebsite_pageService website_pageService;

    @Autowired
    @Lazy
    private Website_pageMapping website_pageMapping;




    @PreAuthorize("hasPermission(#website_page_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Website_page" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_pages/{website_page_id}")

    public ResponseEntity<Website_pageDTO> update(@PathVariable("website_page_id") Integer website_page_id, @RequestBody Website_pageDTO website_pagedto) {
		Website_page domain = website_pageMapping.toDomain(website_pagedto);
        domain.setId(website_page_id);
		website_pageService.update(domain);
		Website_pageDTO dto = website_pageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#website_page_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Website_page" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_pages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_pageDTO> website_pagedtos) {
        website_pageService.updateBatch(website_pageMapping.toDomain(website_pagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#website_page_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Website_page" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/website_pages/{website_page_id}")
    public ResponseEntity<Website_pageDTO> get(@PathVariable("website_page_id") Integer website_page_id) {
        Website_page domain = website_pageService.get(website_page_id);
        Website_pageDTO dto = website_pageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#website_page_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Website_page" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_pages/{website_page_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_page_id") Integer website_page_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_pageService.remove(website_page_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Website_page" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_pages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        website_pageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Website_page" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_pages")

    public ResponseEntity<Website_pageDTO> create(@RequestBody Website_pageDTO website_pagedto) {
        Website_page domain = website_pageMapping.toDomain(website_pagedto);
		website_pageService.create(domain);
        Website_pageDTO dto = website_pageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Website_page" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/website_pages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_pageDTO> website_pagedtos) {
        website_pageService.createBatch(website_pageMapping.toDomain(website_pagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Website_page" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_pages/fetchdefault")
	public ResponseEntity<List<Website_pageDTO>> fetchDefault(Website_pageSearchContext context) {
        Page<Website_page> domains = website_pageService.searchDefault(context) ;
        List<Website_pageDTO> list = website_pageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Website_page" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_pages/searchdefault")
	public ResponseEntity<Page<Website_pageDTO>> searchDefault(Website_pageSearchContext context) {
        Page<Website_page> domains = website_pageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_pageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Website_page getEntity(){
        return new Website_page();
    }

}
