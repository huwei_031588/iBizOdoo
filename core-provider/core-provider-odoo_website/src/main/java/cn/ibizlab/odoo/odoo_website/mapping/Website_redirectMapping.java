package cn.ibizlab.odoo.odoo_website.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.odoo.odoo_website.dto.Website_redirectDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Website_redirectMapping extends MappingBase<Website_redirectDTO, Website_redirect> {


}

