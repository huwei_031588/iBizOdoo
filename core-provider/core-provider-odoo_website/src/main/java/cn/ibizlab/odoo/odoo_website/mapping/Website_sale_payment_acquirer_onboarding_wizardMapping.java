package cn.ibizlab.odoo.odoo_website.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.odoo_website.dto.Website_sale_payment_acquirer_onboarding_wizardDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Website_sale_payment_acquirer_onboarding_wizardMapping extends MappingBase<Website_sale_payment_acquirer_onboarding_wizardDTO, Website_sale_payment_acquirer_onboarding_wizard> {


}

