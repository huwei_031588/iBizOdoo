package cn.ibizlab.odoo.odoo_website.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_website.dto.*;
import cn.ibizlab.odoo.odoo_website.mapping.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_sale_payment_acquirer_onboarding_wizardService;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_sale_payment_acquirer_onboarding_wizardSearchContext;




@Slf4j
@Api(tags = {"Website_sale_payment_acquirer_onboarding_wizard" })
@RestController("odoo_website-website_sale_payment_acquirer_onboarding_wizard")
@RequestMapping("")
public class Website_sale_payment_acquirer_onboarding_wizardResource {

    @Autowired
    private IWebsite_sale_payment_acquirer_onboarding_wizardService website_sale_payment_acquirer_onboarding_wizardService;

    @Autowired
    @Lazy
    private Website_sale_payment_acquirer_onboarding_wizardMapping website_sale_payment_acquirer_onboarding_wizardMapping;







    @PreAuthorize("hasPermission(#website_sale_payment_acquirer_onboarding_wizard_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/website_sale_payment_acquirer_onboarding_wizards/{website_sale_payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Website_sale_payment_acquirer_onboarding_wizardDTO> get(@PathVariable("website_sale_payment_acquirer_onboarding_wizard_id") Integer website_sale_payment_acquirer_onboarding_wizard_id) {
        Website_sale_payment_acquirer_onboarding_wizard domain = website_sale_payment_acquirer_onboarding_wizardService.get(website_sale_payment_acquirer_onboarding_wizard_id);
        Website_sale_payment_acquirer_onboarding_wizardDTO dto = website_sale_payment_acquirer_onboarding_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#website_sale_payment_acquirer_onboarding_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_sale_payment_acquirer_onboarding_wizards/{website_sale_payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Website_sale_payment_acquirer_onboarding_wizardDTO> update(@PathVariable("website_sale_payment_acquirer_onboarding_wizard_id") Integer website_sale_payment_acquirer_onboarding_wizard_id, @RequestBody Website_sale_payment_acquirer_onboarding_wizardDTO website_sale_payment_acquirer_onboarding_wizarddto) {
		Website_sale_payment_acquirer_onboarding_wizard domain = website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddto);
        domain.setId(website_sale_payment_acquirer_onboarding_wizard_id);
		website_sale_payment_acquirer_onboarding_wizardService.update(domain);
		Website_sale_payment_acquirer_onboarding_wizardDTO dto = website_sale_payment_acquirer_onboarding_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#website_sale_payment_acquirer_onboarding_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizardDTO> website_sale_payment_acquirer_onboarding_wizarddtos) {
        website_sale_payment_acquirer_onboarding_wizardService.updateBatch(website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#website_sale_payment_acquirer_onboarding_wizard_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_sale_payment_acquirer_onboarding_wizards/{website_sale_payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_sale_payment_acquirer_onboarding_wizard_id") Integer website_sale_payment_acquirer_onboarding_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_sale_payment_acquirer_onboarding_wizardService.remove(website_sale_payment_acquirer_onboarding_wizard_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        website_sale_payment_acquirer_onboarding_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards")

    public ResponseEntity<Website_sale_payment_acquirer_onboarding_wizardDTO> create(@RequestBody Website_sale_payment_acquirer_onboarding_wizardDTO website_sale_payment_acquirer_onboarding_wizarddto) {
        Website_sale_payment_acquirer_onboarding_wizard domain = website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddto);
		website_sale_payment_acquirer_onboarding_wizardService.create(domain);
        Website_sale_payment_acquirer_onboarding_wizardDTO dto = website_sale_payment_acquirer_onboarding_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizardDTO> website_sale_payment_acquirer_onboarding_wizarddtos) {
        website_sale_payment_acquirer_onboarding_wizardService.createBatch(website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Website_sale_payment_acquirer_onboarding_wizard" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_sale_payment_acquirer_onboarding_wizards/fetchdefault")
	public ResponseEntity<List<Website_sale_payment_acquirer_onboarding_wizardDTO>> fetchDefault(Website_sale_payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Website_sale_payment_acquirer_onboarding_wizard> domains = website_sale_payment_acquirer_onboarding_wizardService.searchDefault(context) ;
        List<Website_sale_payment_acquirer_onboarding_wizardDTO> list = website_sale_payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Website_sale_payment_acquirer_onboarding_wizard" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_sale_payment_acquirer_onboarding_wizards/searchdefault")
	public ResponseEntity<Page<Website_sale_payment_acquirer_onboarding_wizardDTO>> searchDefault(Website_sale_payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Website_sale_payment_acquirer_onboarding_wizard> domains = website_sale_payment_acquirer_onboarding_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_sale_payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Website_sale_payment_acquirer_onboarding_wizard getEntity(){
        return new Website_sale_payment_acquirer_onboarding_wizard();
    }

}
