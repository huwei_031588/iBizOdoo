package cn.ibizlab.odoo.odoo_website.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Website_pageDTO]
 */
@Data
public class Website_pageDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [ARCH]
     *
     */
    @JSONField(name = "arch")
    @JsonProperty("arch")
    private String arch;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private Integer priority;

    /**
     * 属性 [XML_ID]
     *
     */
    @JSONField(name = "xml_id")
    @JsonProperty("xml_id")
    private String xmlId;

    /**
     * 属性 [WEBSITE_INDEXED]
     *
     */
    @JSONField(name = "website_indexed")
    @JsonProperty("website_indexed")
    private String websiteIndexed;

    /**
     * 属性 [VIEW_ID]
     *
     */
    @JSONField(name = "view_id")
    @JsonProperty("view_id")
    private Integer viewId;

    /**
     * 属性 [THEME_TEMPLATE_ID]
     *
     */
    @JSONField(name = "theme_template_id")
    @JsonProperty("theme_template_id")
    private Integer themeTemplateId;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 属性 [CUSTOMIZE_SHOW]
     *
     */
    @JSONField(name = "customize_show")
    @JsonProperty("customize_show")
    private String customizeShow;

    /**
     * 属性 [MODEL]
     *
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 属性 [FIELD_PARENT]
     *
     */
    @JSONField(name = "field_parent")
    @JsonProperty("field_parent")
    private String fieldParent;

    /**
     * 属性 [ARCH_DB]
     *
     */
    @JSONField(name = "arch_db")
    @JsonProperty("arch_db")
    private String archDb;

    /**
     * 属性 [IS_HOMEPAGE]
     *
     */
    @JSONField(name = "is_homepage")
    @JsonProperty("is_homepage")
    private String isHomepage;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 属性 [MODEL_DATA_ID]
     *
     */
    @JSONField(name = "model_data_id")
    @JsonProperty("model_data_id")
    private Integer modelDataId;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MODE]
     *
     */
    @JSONField(name = "mode")
    @JsonProperty("mode")
    private String mode;

    /**
     * 属性 [MENU_IDS]
     *
     */
    @JSONField(name = "menu_ids")
    @JsonProperty("menu_ids")
    private String menuIds;

    /**
     * 属性 [INHERIT_CHILDREN_IDS]
     *
     */
    @JSONField(name = "inherit_children_ids")
    @JsonProperty("inherit_children_ids")
    private String inheritChildrenIds;

    /**
     * 属性 [ARCH_BASE]
     *
     */
    @JSONField(name = "arch_base")
    @JsonProperty("arch_base")
    private String archBase;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [IS_VISIBLE]
     *
     */
    @JSONField(name = "is_visible")
    @JsonProperty("is_visible")
    private String isVisible;

    /**
     * 属性 [FIRST_PAGE_ID]
     *
     */
    @JSONField(name = "first_page_id")
    @JsonProperty("first_page_id")
    private Integer firstPageId;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;

    /**
     * 属性 [HEADER_COLOR]
     *
     */
    @JSONField(name = "header_color")
    @JsonProperty("header_color")
    private String headerColor;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 属性 [PAGE_IDS]
     *
     */
    @JSONField(name = "page_ids")
    @JsonProperty("page_ids")
    private String pageIds;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [URL]
     *
     */
    @JSONField(name = "url")
    @JsonProperty("url")
    private String url;

    /**
     * 属性 [HEADER_OVERLAY]
     *
     */
    @JSONField(name = "header_overlay")
    @JsonProperty("header_overlay")
    private String headerOverlay;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 属性 [DATE_PUBLISH]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_publish" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_publish")
    private Timestamp datePublish;

    /**
     * 属性 [GROUPS_ID]
     *
     */
    @JSONField(name = "groups_id")
    @JsonProperty("groups_id")
    private String groupsId;

    /**
     * 属性 [KEY]
     *
     */
    @JSONField(name = "key")
    @JsonProperty("key")
    private String key;

    /**
     * 属性 [INHERIT_ID]
     *
     */
    @JSONField(name = "inherit_id")
    @JsonProperty("inherit_id")
    private Integer inheritId;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [MODEL_IDS]
     *
     */
    @JSONField(name = "model_ids")
    @JsonProperty("model_ids")
    private String modelIds;

    /**
     * 属性 [ARCH_FS]
     *
     */
    @JSONField(name = "arch_fs")
    @JsonProperty("arch_fs")
    private String archFs;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [WEBSITE_INDEXED]
     */
    public void setWebsiteIndexed(String  websiteIndexed){
        this.websiteIndexed = websiteIndexed ;
        this.modify("website_indexed",websiteIndexed);
    }

    /**
     * 设置 [VIEW_ID]
     */
    public void setViewId(Integer  viewId){
        this.viewId = viewId ;
        this.modify("view_id",viewId);
    }

    /**
     * 设置 [THEME_TEMPLATE_ID]
     */
    public void setThemeTemplateId(Integer  themeTemplateId){
        this.themeTemplateId = themeTemplateId ;
        this.modify("theme_template_id",themeTemplateId);
    }

    /**
     * 设置 [HEADER_COLOR]
     */
    public void setHeaderColor(String  headerColor){
        this.headerColor = headerColor ;
        this.modify("header_color",headerColor);
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    public void setIsPublished(String  isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [URL]
     */
    public void setUrl(String  url){
        this.url = url ;
        this.modify("url",url);
    }

    /**
     * 设置 [HEADER_OVERLAY]
     */
    public void setHeaderOverlay(String  headerOverlay){
        this.headerOverlay = headerOverlay ;
        this.modify("header_overlay",headerOverlay);
    }

    /**
     * 设置 [DATE_PUBLISH]
     */
    public void setDatePublish(Timestamp  datePublish){
        this.datePublish = datePublish ;
        this.modify("date_publish",datePublish);
    }


}

