package cn.ibizlab.odoo.odoo_website.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_website.dto.*;
import cn.ibizlab.odoo.odoo_website.mapping.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_seo_metadataService;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_seo_metadataSearchContext;




@Slf4j
@Api(tags = {"Website_seo_metadata" })
@RestController("odoo_website-website_seo_metadata")
@RequestMapping("")
public class Website_seo_metadataResource {

    @Autowired
    private IWebsite_seo_metadataService website_seo_metadataService;

    @Autowired
    @Lazy
    private Website_seo_metadataMapping website_seo_metadataMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Website_seo_metadata" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata")

    public ResponseEntity<Website_seo_metadataDTO> create(@RequestBody Website_seo_metadataDTO website_seo_metadatadto) {
        Website_seo_metadata domain = website_seo_metadataMapping.toDomain(website_seo_metadatadto);
		website_seo_metadataService.create(domain);
        Website_seo_metadataDTO dto = website_seo_metadataMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Website_seo_metadata" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/website_seo_metadata/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_seo_metadataDTO> website_seo_metadatadtos) {
        website_seo_metadataService.createBatch(website_seo_metadataMapping.toDomain(website_seo_metadatadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#website_seo_metadata_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Website_seo_metadata" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_seo_metadata/{website_seo_metadata_id}")

    public ResponseEntity<Website_seo_metadataDTO> update(@PathVariable("website_seo_metadata_id") Integer website_seo_metadata_id, @RequestBody Website_seo_metadataDTO website_seo_metadatadto) {
		Website_seo_metadata domain = website_seo_metadataMapping.toDomain(website_seo_metadatadto);
        domain.setId(website_seo_metadata_id);
		website_seo_metadataService.update(domain);
		Website_seo_metadataDTO dto = website_seo_metadataMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#website_seo_metadata_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Website_seo_metadata" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_seo_metadata/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_seo_metadataDTO> website_seo_metadatadtos) {
        website_seo_metadataService.updateBatch(website_seo_metadataMapping.toDomain(website_seo_metadatadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#website_seo_metadata_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Website_seo_metadata" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/website_seo_metadata/{website_seo_metadata_id}")
    public ResponseEntity<Website_seo_metadataDTO> get(@PathVariable("website_seo_metadata_id") Integer website_seo_metadata_id) {
        Website_seo_metadata domain = website_seo_metadataService.get(website_seo_metadata_id);
        Website_seo_metadataDTO dto = website_seo_metadataMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#website_seo_metadata_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Website_seo_metadata" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_seo_metadata/{website_seo_metadata_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_seo_metadata_id") Integer website_seo_metadata_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_seo_metadataService.remove(website_seo_metadata_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Website_seo_metadata" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_seo_metadata/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        website_seo_metadataService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Website_seo_metadata" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_seo_metadata/fetchdefault")
	public ResponseEntity<List<Website_seo_metadataDTO>> fetchDefault(Website_seo_metadataSearchContext context) {
        Page<Website_seo_metadata> domains = website_seo_metadataService.searchDefault(context) ;
        List<Website_seo_metadataDTO> list = website_seo_metadataMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Website_seo_metadata" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_seo_metadata/searchdefault")
	public ResponseEntity<Page<Website_seo_metadataDTO>> searchDefault(Website_seo_metadataSearchContext context) {
        Page<Website_seo_metadata> domains = website_seo_metadataService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_seo_metadataMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Website_seo_metadata getEntity(){
        return new Website_seo_metadata();
    }

}
