package cn.ibizlab.odoo.odoo_portal.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_portal.dto.*;
import cn.ibizlab.odoo.odoo_portal.mapping.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_mixinService;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_mixinSearchContext;




@Slf4j
@Api(tags = {"Portal_mixin" })
@RestController("odoo_portal-portal_mixin")
@RequestMapping("")
public class Portal_mixinResource {

    @Autowired
    private IPortal_mixinService portal_mixinService;

    @Autowired
    @Lazy
    private Portal_mixinMapping portal_mixinMapping;




    @PreAuthorize("hasPermission(#portal_mixin_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Portal_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_mixins/{portal_mixin_id}")
    public ResponseEntity<Portal_mixinDTO> get(@PathVariable("portal_mixin_id") Integer portal_mixin_id) {
        Portal_mixin domain = portal_mixinService.get(portal_mixin_id);
        Portal_mixinDTO dto = portal_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#portal_mixin_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Portal_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_mixins/{portal_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("portal_mixin_id") Integer portal_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(portal_mixinService.remove(portal_mixin_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Portal_mixin" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        portal_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#portal_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Portal_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_mixins/{portal_mixin_id}")

    public ResponseEntity<Portal_mixinDTO> update(@PathVariable("portal_mixin_id") Integer portal_mixin_id, @RequestBody Portal_mixinDTO portal_mixindto) {
		Portal_mixin domain = portal_mixinMapping.toDomain(portal_mixindto);
        domain.setId(portal_mixin_id);
		portal_mixinService.update(domain);
		Portal_mixinDTO dto = portal_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#portal_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Portal_mixin" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_mixinDTO> portal_mixindtos) {
        portal_mixinService.updateBatch(portal_mixinMapping.toDomain(portal_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Portal_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_mixins")

    public ResponseEntity<Portal_mixinDTO> create(@RequestBody Portal_mixinDTO portal_mixindto) {
        Portal_mixin domain = portal_mixinMapping.toDomain(portal_mixindto);
		portal_mixinService.create(domain);
        Portal_mixinDTO dto = portal_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Portal_mixin" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Portal_mixinDTO> portal_mixindtos) {
        portal_mixinService.createBatch(portal_mixinMapping.toDomain(portal_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Portal_mixin" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/portal_mixins/fetchdefault")
	public ResponseEntity<List<Portal_mixinDTO>> fetchDefault(Portal_mixinSearchContext context) {
        Page<Portal_mixin> domains = portal_mixinService.searchDefault(context) ;
        List<Portal_mixinDTO> list = portal_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Portal_mixin" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/portal_mixins/searchdefault")
	public ResponseEntity<Page<Portal_mixinDTO>> searchDefault(Portal_mixinSearchContext context) {
        Page<Portal_mixin> domains = portal_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(portal_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Portal_mixin getEntity(){
        return new Portal_mixin();
    }

}
