package cn.ibizlab.odoo.odoo_portal.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_portal.dto.*;
import cn.ibizlab.odoo.odoo_portal.mapping.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_wizardService;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizardSearchContext;




@Slf4j
@Api(tags = {"Portal_wizard" })
@RestController("odoo_portal-portal_wizard")
@RequestMapping("")
public class Portal_wizardResource {

    @Autowired
    private IPortal_wizardService portal_wizardService;

    @Autowired
    @Lazy
    private Portal_wizardMapping portal_wizardMapping;




    @PreAuthorize("hasPermission(#portal_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Portal_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_wizards/{portal_wizard_id}")

    public ResponseEntity<Portal_wizardDTO> update(@PathVariable("portal_wizard_id") Integer portal_wizard_id, @RequestBody Portal_wizardDTO portal_wizarddto) {
		Portal_wizard domain = portal_wizardMapping.toDomain(portal_wizarddto);
        domain.setId(portal_wizard_id);
		portal_wizardService.update(domain);
		Portal_wizardDTO dto = portal_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#portal_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Portal_wizard" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_wizardDTO> portal_wizarddtos) {
        portal_wizardService.updateBatch(portal_wizardMapping.toDomain(portal_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#portal_wizard_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Portal_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizards/{portal_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("portal_wizard_id") Integer portal_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(portal_wizardService.remove(portal_wizard_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Portal_wizard" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        portal_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Portal_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizards")

    public ResponseEntity<Portal_wizardDTO> create(@RequestBody Portal_wizardDTO portal_wizarddto) {
        Portal_wizard domain = portal_wizardMapping.toDomain(portal_wizarddto);
		portal_wizardService.create(domain);
        Portal_wizardDTO dto = portal_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Portal_wizard" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Portal_wizardDTO> portal_wizarddtos) {
        portal_wizardService.createBatch(portal_wizardMapping.toDomain(portal_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#portal_wizard_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Portal_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_wizards/{portal_wizard_id}")
    public ResponseEntity<Portal_wizardDTO> get(@PathVariable("portal_wizard_id") Integer portal_wizard_id) {
        Portal_wizard domain = portal_wizardService.get(portal_wizard_id);
        Portal_wizardDTO dto = portal_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Portal_wizard" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/portal_wizards/fetchdefault")
	public ResponseEntity<List<Portal_wizardDTO>> fetchDefault(Portal_wizardSearchContext context) {
        Page<Portal_wizard> domains = portal_wizardService.searchDefault(context) ;
        List<Portal_wizardDTO> list = portal_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Portal_wizard" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/portal_wizards/searchdefault")
	public ResponseEntity<Page<Portal_wizardDTO>> searchDefault(Portal_wizardSearchContext context) {
        Page<Portal_wizard> domains = portal_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(portal_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Portal_wizard getEntity(){
        return new Portal_wizard();
    }

}
