package cn.ibizlab.odoo.odoo_portal.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.odoo.odoo_portal.dto.Portal_wizardDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Portal_wizardMapping extends MappingBase<Portal_wizardDTO, Portal_wizard> {


}

