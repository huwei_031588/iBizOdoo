package cn.ibizlab.odoo.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_mass_mailing_listDTO]
 */
@Data
public class Mail_mass_mailing_listDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CONTACT_NBR]
     *
     */
    @JSONField(name = "contact_nbr")
    @JsonProperty("contact_nbr")
    private Integer contactNbr;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SUBSCRIPTION_CONTACT_IDS]
     *
     */
    @JSONField(name = "subscription_contact_ids")
    @JsonProperty("subscription_contact_ids")
    private String subscriptionContactIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [CONTACT_IDS]
     *
     */
    @JSONField(name = "contact_ids")
    @JsonProperty("contact_ids")
    private String contactIds;

    /**
     * 属性 [POPUP_CONTENT]
     *
     */
    @JSONField(name = "popup_content")
    @JsonProperty("popup_content")
    private String popupContent;

    /**
     * 属性 [IS_PUBLIC]
     *
     */
    @JSONField(name = "is_public")
    @JsonProperty("is_public")
    private String isPublic;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [POPUP_REDIRECT_URL]
     *
     */
    @JSONField(name = "popup_redirect_url")
    @JsonProperty("popup_redirect_url")
    private String popupRedirectUrl;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [POPUP_CONTENT]
     */
    public void setPopupContent(String  popupContent){
        this.popupContent = popupContent ;
        this.modify("popup_content",popupContent);
    }

    /**
     * 设置 [IS_PUBLIC]
     */
    public void setIsPublic(String  isPublic){
        this.isPublic = isPublic ;
        this.modify("is_public",isPublic);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [POPUP_REDIRECT_URL]
     */
    public void setPopupRedirectUrl(String  popupRedirectUrl){
        this.popupRedirectUrl = popupRedirectUrl ;
        this.modify("popup_redirect_url",popupRedirectUrl);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }


}

