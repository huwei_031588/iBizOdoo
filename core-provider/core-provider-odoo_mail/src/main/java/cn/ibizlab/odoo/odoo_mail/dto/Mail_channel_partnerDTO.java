package cn.ibizlab.odoo.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_channel_partnerDTO]
 */
@Data
public class Mail_channel_partnerDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [IS_BLACKLISTED]
     *
     */
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private String isBlacklisted;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [IS_PINNED]
     *
     */
    @JSONField(name = "is_pinned")
    @JsonProperty("is_pinned")
    private String isPinned;

    /**
     * 属性 [FOLD_STATE]
     *
     */
    @JSONField(name = "fold_state")
    @JsonProperty("fold_state")
    private String foldState;

    /**
     * 属性 [IS_MINIMIZED]
     *
     */
    @JSONField(name = "is_minimized")
    @JsonProperty("is_minimized")
    private String isMinimized;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 属性 [CHANNEL_ID_TEXT]
     *
     */
    @JSONField(name = "channel_id_text")
    @JsonProperty("channel_id_text")
    private String channelIdText;

    /**
     * 属性 [PARTNER_EMAIL]
     *
     */
    @JSONField(name = "partner_email")
    @JsonProperty("partner_email")
    private String partnerEmail;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CHANNEL_ID]
     *
     */
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    private Integer channelId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [SEEN_MESSAGE_ID]
     *
     */
    @JSONField(name = "seen_message_id")
    @JsonProperty("seen_message_id")
    private Integer seenMessageId;


    /**
     * 设置 [IS_PINNED]
     */
    public void setIsPinned(String  isPinned){
        this.isPinned = isPinned ;
        this.modify("is_pinned",isPinned);
    }

    /**
     * 设置 [FOLD_STATE]
     */
    public void setFoldState(String  foldState){
        this.foldState = foldState ;
        this.modify("fold_state",foldState);
    }

    /**
     * 设置 [IS_MINIMIZED]
     */
    public void setIsMinimized(String  isMinimized){
        this.isMinimized = isMinimized ;
        this.modify("is_minimized",isMinimized);
    }

    /**
     * 设置 [CHANNEL_ID]
     */
    public void setChannelId(Integer  channelId){
        this.channelId = channelId ;
        this.modify("channel_id",channelId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [SEEN_MESSAGE_ID]
     */
    public void setSeenMessageId(Integer  seenMessageId){
        this.seenMessageId = seenMessageId ;
        this.modify("seen_message_id",seenMessageId);
    }


}

