package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_tracking_value;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_tracking_valueService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_tracking_valueSearchContext;




@Slf4j
@Api(tags = {"Mail_tracking_value" })
@RestController("odoo_mail-mail_tracking_value")
@RequestMapping("")
public class Mail_tracking_valueResource {

    @Autowired
    private IMail_tracking_valueService mail_tracking_valueService;

    @Autowired
    @Lazy
    private Mail_tracking_valueMapping mail_tracking_valueMapping;




    @PreAuthorize("hasPermission(#mail_tracking_value_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_tracking_value" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_tracking_values/{mail_tracking_value_id}")

    public ResponseEntity<Mail_tracking_valueDTO> update(@PathVariable("mail_tracking_value_id") Integer mail_tracking_value_id, @RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
		Mail_tracking_value domain = mail_tracking_valueMapping.toDomain(mail_tracking_valuedto);
        domain.setId(mail_tracking_value_id);
		mail_tracking_valueService.update(domain);
		Mail_tracking_valueDTO dto = mail_tracking_valueMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_tracking_value_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_tracking_value" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_tracking_values/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {
        mail_tracking_valueService.updateBatch(mail_tracking_valueMapping.toDomain(mail_tracking_valuedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_tracking_value" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_tracking_values")

    public ResponseEntity<Mail_tracking_valueDTO> create(@RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
        Mail_tracking_value domain = mail_tracking_valueMapping.toDomain(mail_tracking_valuedto);
		mail_tracking_valueService.create(domain);
        Mail_tracking_valueDTO dto = mail_tracking_valueMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_tracking_value" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_tracking_values/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {
        mail_tracking_valueService.createBatch(mail_tracking_valueMapping.toDomain(mail_tracking_valuedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mail_tracking_value_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_tracking_value" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_tracking_values/{mail_tracking_value_id}")
    public ResponseEntity<Mail_tracking_valueDTO> get(@PathVariable("mail_tracking_value_id") Integer mail_tracking_value_id) {
        Mail_tracking_value domain = mail_tracking_valueService.get(mail_tracking_value_id);
        Mail_tracking_valueDTO dto = mail_tracking_valueMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_tracking_value_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_tracking_value" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_tracking_values/{mail_tracking_value_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_tracking_value_id") Integer mail_tracking_value_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_tracking_valueService.remove(mail_tracking_value_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_tracking_value" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_tracking_values/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_tracking_valueService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_tracking_value" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_tracking_values/fetchdefault")
	public ResponseEntity<List<Mail_tracking_valueDTO>> fetchDefault(Mail_tracking_valueSearchContext context) {
        Page<Mail_tracking_value> domains = mail_tracking_valueService.searchDefault(context) ;
        List<Mail_tracking_valueDTO> list = mail_tracking_valueMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_tracking_value" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_tracking_values/searchdefault")
	public ResponseEntity<Page<Mail_tracking_valueDTO>> searchDefault(Mail_tracking_valueSearchContext context) {
        Page<Mail_tracking_value> domains = mail_tracking_valueService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_tracking_valueMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_tracking_value getEntity(){
        return new Mail_tracking_value();
    }

}
