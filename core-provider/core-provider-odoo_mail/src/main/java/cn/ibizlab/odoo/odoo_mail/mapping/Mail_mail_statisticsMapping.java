package cn.ibizlab.odoo.odoo_mail.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.odoo.odoo_mail.dto.Mail_mail_statisticsDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_mail_statisticsMapping extends MappingBase<Mail_mail_statisticsDTO, Mail_mail_statistics> {


}

