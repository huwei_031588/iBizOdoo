package cn.ibizlab.odoo.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_mass_mailing_campaignDTO]
 */
@Data
public class Mail_mass_mailing_campaignDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TOTAL]
     *
     */
    @JSONField(name = "total")
    @JsonProperty("total")
    private Integer total;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [MASS_MAILING_IDS]
     *
     */
    @JSONField(name = "mass_mailing_ids")
    @JsonProperty("mass_mailing_ids")
    private String massMailingIds;

    /**
     * 属性 [OPENED_RATIO]
     *
     */
    @JSONField(name = "opened_ratio")
    @JsonProperty("opened_ratio")
    private Integer openedRatio;

    /**
     * 属性 [FAILED]
     *
     */
    @JSONField(name = "failed")
    @JsonProperty("failed")
    private Integer failed;

    /**
     * 属性 [SCHEDULED]
     *
     */
    @JSONField(name = "scheduled")
    @JsonProperty("scheduled")
    private Integer scheduled;

    /**
     * 属性 [BOUNCED]
     *
     */
    @JSONField(name = "bounced")
    @JsonProperty("bounced")
    private Integer bounced;

    /**
     * 属性 [CLICKS_RATIO]
     *
     */
    @JSONField(name = "clicks_ratio")
    @JsonProperty("clicks_ratio")
    private Integer clicksRatio;

    /**
     * 属性 [SENT]
     *
     */
    @JSONField(name = "sent")
    @JsonProperty("sent")
    private Integer sent;

    /**
     * 属性 [RECEIVED_RATIO]
     *
     */
    @JSONField(name = "received_ratio")
    @JsonProperty("received_ratio")
    private Integer receivedRatio;

    /**
     * 属性 [IGNORED]
     *
     */
    @JSONField(name = "ignored")
    @JsonProperty("ignored")
    private Integer ignored;

    /**
     * 属性 [UNIQUE_AB_TESTING]
     *
     */
    @JSONField(name = "unique_ab_testing")
    @JsonProperty("unique_ab_testing")
    private String uniqueAbTesting;

    /**
     * 属性 [REPLIED]
     *
     */
    @JSONField(name = "replied")
    @JsonProperty("replied")
    private Integer replied;

    /**
     * 属性 [TOTAL_MAILINGS]
     *
     */
    @JSONField(name = "total_mailings")
    @JsonProperty("total_mailings")
    private Integer totalMailings;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [BOUNCED_RATIO]
     *
     */
    @JSONField(name = "bounced_ratio")
    @JsonProperty("bounced_ratio")
    private Integer bouncedRatio;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;

    /**
     * 属性 [OPENED]
     *
     */
    @JSONField(name = "opened")
    @JsonProperty("opened")
    private Integer opened;

    /**
     * 属性 [REPLIED_RATIO]
     *
     */
    @JSONField(name = "replied_ratio")
    @JsonProperty("replied_ratio")
    private Integer repliedRatio;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DELIVERED]
     *
     */
    @JSONField(name = "delivered")
    @JsonProperty("delivered")
    private Integer delivered;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    private String stageIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Integer campaignId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    private Integer stageId;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Integer sourceId;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Integer mediumId;


    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [UNIQUE_AB_TESTING]
     */
    public void setUniqueAbTesting(String  uniqueAbTesting){
        this.uniqueAbTesting = uniqueAbTesting ;
        this.modify("unique_ab_testing",uniqueAbTesting);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Integer  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    public void setCampaignId(Integer  campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [STAGE_ID]
     */
    public void setStageId(Integer  stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }

    /**
     * 设置 [SOURCE_ID]
     */
    public void setSourceId(Integer  sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    public void setMediumId(Integer  mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }


}

