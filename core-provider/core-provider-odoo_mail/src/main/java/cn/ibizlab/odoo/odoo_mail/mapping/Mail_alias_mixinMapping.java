package cn.ibizlab.odoo.odoo_mail.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias_mixin;
import cn.ibizlab.odoo.odoo_mail.dto.Mail_alias_mixinDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_alias_mixinMapping extends MappingBase<Mail_alias_mixinDTO, Mail_alias_mixin> {


}

