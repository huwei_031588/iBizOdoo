package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_shortcode;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_shortcodeService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_shortcodeSearchContext;




@Slf4j
@Api(tags = {"Mail_shortcode" })
@RestController("odoo_mail-mail_shortcode")
@RequestMapping("")
public class Mail_shortcodeResource {

    @Autowired
    private IMail_shortcodeService mail_shortcodeService;

    @Autowired
    @Lazy
    private Mail_shortcodeMapping mail_shortcodeMapping;




    @PreAuthorize("hasPermission(#mail_shortcode_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_shortcode" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_shortcodes/{mail_shortcode_id}")
    public ResponseEntity<Mail_shortcodeDTO> get(@PathVariable("mail_shortcode_id") Integer mail_shortcode_id) {
        Mail_shortcode domain = mail_shortcodeService.get(mail_shortcode_id);
        Mail_shortcodeDTO dto = mail_shortcodeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_shortcode" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes")

    public ResponseEntity<Mail_shortcodeDTO> create(@RequestBody Mail_shortcodeDTO mail_shortcodedto) {
        Mail_shortcode domain = mail_shortcodeMapping.toDomain(mail_shortcodedto);
		mail_shortcodeService.create(domain);
        Mail_shortcodeDTO dto = mail_shortcodeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_shortcode" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_shortcodes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_shortcodeDTO> mail_shortcodedtos) {
        mail_shortcodeService.createBatch(mail_shortcodeMapping.toDomain(mail_shortcodedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_shortcode_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_shortcode" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_shortcodes/{mail_shortcode_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_shortcode_id") Integer mail_shortcode_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_shortcodeService.remove(mail_shortcode_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_shortcode" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_shortcodes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_shortcodeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mail_shortcode_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_shortcode" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_shortcodes/{mail_shortcode_id}")

    public ResponseEntity<Mail_shortcodeDTO> update(@PathVariable("mail_shortcode_id") Integer mail_shortcode_id, @RequestBody Mail_shortcodeDTO mail_shortcodedto) {
		Mail_shortcode domain = mail_shortcodeMapping.toDomain(mail_shortcodedto);
        domain.setId(mail_shortcode_id);
		mail_shortcodeService.update(domain);
		Mail_shortcodeDTO dto = mail_shortcodeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_shortcode_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_shortcode" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_shortcodes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_shortcodeDTO> mail_shortcodedtos) {
        mail_shortcodeService.updateBatch(mail_shortcodeMapping.toDomain(mail_shortcodedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_shortcode" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_shortcodes/fetchdefault")
	public ResponseEntity<List<Mail_shortcodeDTO>> fetchDefault(Mail_shortcodeSearchContext context) {
        Page<Mail_shortcode> domains = mail_shortcodeService.searchDefault(context) ;
        List<Mail_shortcodeDTO> list = mail_shortcodeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_shortcode" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_shortcodes/searchdefault")
	public ResponseEntity<Page<Mail_shortcodeDTO>> searchDefault(Mail_shortcodeSearchContext context) {
        Page<Mail_shortcode> domains = mail_shortcodeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_shortcodeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_shortcode getEntity(){
        return new Mail_shortcode();
    }

}
