package cn.ibizlab.odoo.odoo_mail.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-mail")
@Data
public class odoo_mailServiceProperties {

	private boolean enabled;

	private boolean auth;


}