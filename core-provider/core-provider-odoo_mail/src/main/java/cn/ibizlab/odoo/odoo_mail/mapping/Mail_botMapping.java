package cn.ibizlab.odoo.odoo_mail.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.odoo.odoo_mail.dto.Mail_botDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_botMapping extends MappingBase<Mail_botDTO, Mail_bot> {


}

