package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_blacklistService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklistSearchContext;




@Slf4j
@Api(tags = {"Mail_blacklist" })
@RestController("odoo_mail-mail_blacklist")
@RequestMapping("")
public class Mail_blacklistResource {

    @Autowired
    private IMail_blacklistService mail_blacklistService;

    @Autowired
    @Lazy
    private Mail_blacklistMapping mail_blacklistMapping;




    @PreAuthorize("hasPermission('Remove',{#mail_blacklist_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_blacklist" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_blacklists/{mail_blacklist_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_blacklist_id") Integer mail_blacklist_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_blacklistService.remove(mail_blacklist_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_blacklist" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_blacklists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_blacklistService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_blacklist_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_blacklist" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_blacklists/{mail_blacklist_id}")
    public ResponseEntity<Mail_blacklistDTO> get(@PathVariable("mail_blacklist_id") Integer mail_blacklist_id) {
        Mail_blacklist domain = mail_blacklistService.get(mail_blacklist_id);
        Mail_blacklistDTO dto = mail_blacklistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#mail_blacklist_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_blacklist" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_blacklists/{mail_blacklist_id}")

    public ResponseEntity<Mail_blacklistDTO> update(@PathVariable("mail_blacklist_id") Integer mail_blacklist_id, @RequestBody Mail_blacklistDTO mail_blacklistdto) {
		Mail_blacklist domain = mail_blacklistMapping.toDomain(mail_blacklistdto);
        domain.setId(mail_blacklist_id);
		mail_blacklistService.update(domain);
		Mail_blacklistDTO dto = mail_blacklistMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_blacklist_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_blacklist" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_blacklists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_blacklistDTO> mail_blacklistdtos) {
        mail_blacklistService.updateBatch(mail_blacklistMapping.toDomain(mail_blacklistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_blacklist" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists")

    public ResponseEntity<Mail_blacklistDTO> create(@RequestBody Mail_blacklistDTO mail_blacklistdto) {
        Mail_blacklist domain = mail_blacklistMapping.toDomain(mail_blacklistdto);
		mail_blacklistService.create(domain);
        Mail_blacklistDTO dto = mail_blacklistMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_blacklist" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_blacklistDTO> mail_blacklistdtos) {
        mail_blacklistService.createBatch(mail_blacklistMapping.toDomain(mail_blacklistdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_blacklist" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_blacklists/fetchdefault")
	public ResponseEntity<List<Mail_blacklistDTO>> fetchDefault(Mail_blacklistSearchContext context) {
        Page<Mail_blacklist> domains = mail_blacklistService.searchDefault(context) ;
        List<Mail_blacklistDTO> list = mail_blacklistMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_blacklist" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_blacklists/searchdefault")
	public ResponseEntity<Page<Mail_blacklistDTO>> searchDefault(Mail_blacklistSearchContext context) {
        Page<Mail_blacklist> domains = mail_blacklistService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_blacklistMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_blacklist getEntity(){
        return new Mail_blacklist();
    }

}
