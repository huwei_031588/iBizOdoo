package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_contact;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_contactService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_contactSearchContext;




@Slf4j
@Api(tags = {"Mail_mass_mailing_contact" })
@RestController("odoo_mail-mail_mass_mailing_contact")
@RequestMapping("")
public class Mail_mass_mailing_contactResource {

    @Autowired
    private IMail_mass_mailing_contactService mail_mass_mailing_contactService;

    @Autowired
    @Lazy
    private Mail_mass_mailing_contactMapping mail_mass_mailing_contactMapping;




    @PreAuthorize("hasPermission('Remove',{#mail_mass_mailing_contact_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_contact" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_contacts/{mail_mass_mailing_contact_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_contact_id") Integer mail_mass_mailing_contact_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_contactService.remove(mail_mass_mailing_contact_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_mass_mailing_contact" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_contacts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_mass_mailing_contactService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_mass_mailing_contact_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_contact" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_contacts/{mail_mass_mailing_contact_id}")
    public ResponseEntity<Mail_mass_mailing_contactDTO> get(@PathVariable("mail_mass_mailing_contact_id") Integer mail_mass_mailing_contact_id) {
        Mail_mass_mailing_contact domain = mail_mass_mailing_contactService.get(mail_mass_mailing_contact_id);
        Mail_mass_mailing_contactDTO dto = mail_mass_mailing_contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#mail_mass_mailing_contact_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_contact" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_contacts/{mail_mass_mailing_contact_id}")

    public ResponseEntity<Mail_mass_mailing_contactDTO> update(@PathVariable("mail_mass_mailing_contact_id") Integer mail_mass_mailing_contact_id, @RequestBody Mail_mass_mailing_contactDTO mail_mass_mailing_contactdto) {
		Mail_mass_mailing_contact domain = mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdto);
        domain.setId(mail_mass_mailing_contact_id);
		mail_mass_mailing_contactService.update(domain);
		Mail_mass_mailing_contactDTO dto = mail_mass_mailing_contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_mass_mailing_contact_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_mass_mailing_contact" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_contacts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_contactDTO> mail_mass_mailing_contactdtos) {
        mail_mass_mailing_contactService.updateBatch(mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_contact" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts")

    public ResponseEntity<Mail_mass_mailing_contactDTO> create(@RequestBody Mail_mass_mailing_contactDTO mail_mass_mailing_contactdto) {
        Mail_mass_mailing_contact domain = mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdto);
		mail_mass_mailing_contactService.create(domain);
        Mail_mass_mailing_contactDTO dto = mail_mass_mailing_contactMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_mass_mailing_contact" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_contactDTO> mail_mass_mailing_contactdtos) {
        mail_mass_mailing_contactService.createBatch(mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_mass_mailing_contact" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_contacts/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_contactDTO>> fetchDefault(Mail_mass_mailing_contactSearchContext context) {
        Page<Mail_mass_mailing_contact> domains = mail_mass_mailing_contactService.searchDefault(context) ;
        List<Mail_mass_mailing_contactDTO> list = mail_mass_mailing_contactMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_mass_mailing_contact" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_contacts/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_contactDTO>> searchDefault(Mail_mass_mailing_contactSearchContext context) {
        Page<Mail_mass_mailing_contact> domains = mail_mass_mailing_contactService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_contactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_mass_mailing_contact getEntity(){
        return new Mail_mass_mailing_contact();
    }

}
