package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_stage;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_stageService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_stageSearchContext;




@Slf4j
@Api(tags = {"Mail_mass_mailing_stage" })
@RestController("odoo_mail-mail_mass_mailing_stage")
@RequestMapping("")
public class Mail_mass_mailing_stageResource {

    @Autowired
    private IMail_mass_mailing_stageService mail_mass_mailing_stageService;

    @Autowired
    @Lazy
    private Mail_mass_mailing_stageMapping mail_mass_mailing_stageMapping;




    @PreAuthorize("hasPermission(#mail_mass_mailing_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_stages/{mail_mass_mailing_stage_id}")

    public ResponseEntity<Mail_mass_mailing_stageDTO> update(@PathVariable("mail_mass_mailing_stage_id") Integer mail_mass_mailing_stage_id, @RequestBody Mail_mass_mailing_stageDTO mail_mass_mailing_stagedto) {
		Mail_mass_mailing_stage domain = mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedto);
        domain.setId(mail_mass_mailing_stage_id);
		mail_mass_mailing_stageService.update(domain);
		Mail_mass_mailing_stageDTO dto = mail_mass_mailing_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_mass_mailing_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_mass_mailing_stage" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_stageDTO> mail_mass_mailing_stagedtos) {
        mail_mass_mailing_stageService.updateBatch(mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages")

    public ResponseEntity<Mail_mass_mailing_stageDTO> create(@RequestBody Mail_mass_mailing_stageDTO mail_mass_mailing_stagedto) {
        Mail_mass_mailing_stage domain = mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedto);
		mail_mass_mailing_stageService.create(domain);
        Mail_mass_mailing_stageDTO dto = mail_mass_mailing_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_mass_mailing_stage" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_stageDTO> mail_mass_mailing_stagedtos) {
        mail_mass_mailing_stageService.createBatch(mail_mass_mailing_stageMapping.toDomain(mail_mass_mailing_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mail_mass_mailing_stage_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_stages/{mail_mass_mailing_stage_id}")
    public ResponseEntity<Mail_mass_mailing_stageDTO> get(@PathVariable("mail_mass_mailing_stage_id") Integer mail_mass_mailing_stage_id) {
        Mail_mass_mailing_stage domain = mail_mass_mailing_stageService.get(mail_mass_mailing_stage_id);
        Mail_mass_mailing_stageDTO dto = mail_mass_mailing_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#mail_mass_mailing_stage_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_stages/{mail_mass_mailing_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_stage_id") Integer mail_mass_mailing_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_stageService.remove(mail_mass_mailing_stage_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_mass_mailing_stage" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_mass_mailing_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_mass_mailing_stage" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_stages/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_stageDTO>> fetchDefault(Mail_mass_mailing_stageSearchContext context) {
        Page<Mail_mass_mailing_stage> domains = mail_mass_mailing_stageService.searchDefault(context) ;
        List<Mail_mass_mailing_stageDTO> list = mail_mass_mailing_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_mass_mailing_stage" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_stages/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_stageDTO>> searchDefault(Mail_mass_mailing_stageSearchContext context) {
        Page<Mail_mass_mailing_stage> domains = mail_mass_mailing_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_mass_mailing_stage getEntity(){
        return new Mail_mass_mailing_stage();
    }

}
