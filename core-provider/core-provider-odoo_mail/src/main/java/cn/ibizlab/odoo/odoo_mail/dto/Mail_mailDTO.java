package cn.ibizlab.odoo.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_mailDTO]
 */
@Data
public class Mail_mailDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NOTIFICATION]
     *
     */
    @JSONField(name = "notification")
    @JsonProperty("notification")
    private String notification;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @JSONField(name = "scheduled_date")
    @JsonProperty("scheduled_date")
    private String scheduledDate;

    /**
     * 属性 [STARRED_PARTNER_IDS]
     *
     */
    @JSONField(name = "starred_partner_ids")
    @JsonProperty("starred_partner_ids")
    private String starredPartnerIds;

    /**
     * 属性 [AUTO_DELETE]
     *
     */
    @JSONField(name = "auto_delete")
    @JsonProperty("auto_delete")
    private String autoDelete;

    /**
     * 属性 [BODY_HTML]
     *
     */
    @JSONField(name = "body_html")
    @JsonProperty("body_html")
    private String bodyHtml;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 属性 [RECIPIENT_IDS]
     *
     */
    @JSONField(name = "recipient_ids")
    @JsonProperty("recipient_ids")
    private String recipientIds;

    /**
     * 属性 [TRACKING_VALUE_IDS]
     *
     */
    @JSONField(name = "tracking_value_ids")
    @JsonProperty("tracking_value_ids")
    private String trackingValueIds;

    /**
     * 属性 [NEEDACTION_PARTNER_IDS]
     *
     */
    @JSONField(name = "needaction_partner_ids")
    @JsonProperty("needaction_partner_ids")
    private String needactionPartnerIds;

    /**
     * 属性 [EMAIL_CC]
     *
     */
    @JSONField(name = "email_cc")
    @JsonProperty("email_cc")
    private String emailCc;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;

    /**
     * 属性 [FAILURE_REASON]
     *
     */
    @JSONField(name = "failure_reason")
    @JsonProperty("failure_reason")
    private String failureReason;

    /**
     * 属性 [STATISTICS_IDS]
     *
     */
    @JSONField(name = "statistics_ids")
    @JsonProperty("statistics_ids")
    private String statisticsIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [HEADERS]
     *
     */
    @JSONField(name = "headers")
    @JsonProperty("headers")
    private String headers;

    /**
     * 属性 [EMAIL_TO]
     *
     */
    @JSONField(name = "email_to")
    @JsonProperty("email_to")
    private String emailTo;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * 属性 [NOTIFICATION_IDS]
     *
     */
    @JSONField(name = "notification_ids")
    @JsonProperty("notification_ids")
    private String notificationIds;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [REFERENCES]
     *
     */
    @JSONField(name = "references")
    @JsonProperty("references")
    private String references;

    /**
     * 属性 [MAILING_ID_TEXT]
     *
     */
    @JSONField(name = "mailing_id_text")
    @JsonProperty("mailing_id_text")
    private String mailingIdText;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;

    /**
     * 属性 [NEED_MODERATION]
     *
     */
    @JSONField(name = "need_moderation")
    @JsonProperty("need_moderation")
    private String needModeration;

    /**
     * 属性 [MESSAGE_ID]
     *
     */
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private String messageId;

    /**
     * 属性 [NO_AUTO_THREAD]
     *
     */
    @JSONField(name = "no_auto_thread")
    @JsonProperty("no_auto_thread")
    private String noAutoThread;

    /**
     * 属性 [AUTHOR_ID]
     *
     */
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    private Integer authorId;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    /**
     * 属性 [MODEL]
     *
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 属性 [RECORD_NAME]
     *
     */
    @JSONField(name = "record_name")
    @JsonProperty("record_name")
    private String recordName;

    /**
     * 属性 [STARRED]
     *
     */
    @JSONField(name = "starred")
    @JsonProperty("starred")
    private String starred;

    /**
     * 属性 [ADD_SIGN]
     *
     */
    @JSONField(name = "add_sign")
    @JsonProperty("add_sign")
    private String addSign;

    /**
     * 属性 [LAYOUT]
     *
     */
    @JSONField(name = "layout")
    @JsonProperty("layout")
    private String layout;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 属性 [RATING_VALUE]
     *
     */
    @JSONField(name = "rating_value")
    @JsonProperty("rating_value")
    private Double ratingValue;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 属性 [BODY]
     *
     */
    @JSONField(name = "body")
    @JsonProperty("body")
    private String body;

    /**
     * 属性 [MAIL_SERVER_ID]
     *
     */
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 属性 [SUBTYPE_ID]
     *
     */
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    private Integer subtypeId;

    /**
     * 属性 [FETCHMAIL_SERVER_ID_TEXT]
     *
     */
    @JSONField(name = "fetchmail_server_id_text")
    @JsonProperty("fetchmail_server_id_text")
    private String fetchmailServerIdText;

    /**
     * 属性 [HAS_ERROR]
     *
     */
    @JSONField(name = "has_error")
    @JsonProperty("has_error")
    private String hasError;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [AUTHOR_AVATAR]
     *
     */
    @JSONField(name = "author_avatar")
    @JsonProperty("author_avatar")
    private byte[] authorAvatar;

    /**
     * 属性 [MODERATION_STATUS]
     *
     */
    @JSONField(name = "moderation_status")
    @JsonProperty("moderation_status")
    private String moderationStatus;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    private Integer mailActivityTypeId;

    /**
     * 属性 [MODERATOR_ID]
     *
     */
    @JSONField(name = "moderator_id")
    @JsonProperty("moderator_id")
    private Integer moderatorId;

    /**
     * 属性 [MESSAGE_TYPE]
     *
     */
    @JSONField(name = "message_type")
    @JsonProperty("message_type")
    private String messageType;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [SUBJECT]
     *
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 属性 [NEEDACTION]
     *
     */
    @JSONField(name = "needaction")
    @JsonProperty("needaction")
    private String needaction;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [MAILING_ID]
     *
     */
    @JSONField(name = "mailing_id")
    @JsonProperty("mailing_id")
    private Integer mailingId;

    /**
     * 属性 [FETCHMAIL_SERVER_ID]
     *
     */
    @JSONField(name = "fetchmail_server_id")
    @JsonProperty("fetchmail_server_id")
    private Integer fetchmailServerId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [MAIL_MESSAGE_ID]
     *
     */
    @JSONField(name = "mail_message_id")
    @JsonProperty("mail_message_id")
    private Integer mailMessageId;


    /**
     * 设置 [NOTIFICATION]
     */
    public void setNotification(String  notification){
        this.notification = notification ;
        this.modify("notification",notification);
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    public void setScheduledDate(String  scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }

    /**
     * 设置 [AUTO_DELETE]
     */
    public void setAutoDelete(String  autoDelete){
        this.autoDelete = autoDelete ;
        this.modify("auto_delete",autoDelete);
    }

    /**
     * 设置 [BODY_HTML]
     */
    public void setBodyHtml(String  bodyHtml){
        this.bodyHtml = bodyHtml ;
        this.modify("body_html",bodyHtml);
    }

    /**
     * 设置 [EMAIL_CC]
     */
    public void setEmailCc(String  emailCc){
        this.emailCc = emailCc ;
        this.modify("email_cc",emailCc);
    }

    /**
     * 设置 [FAILURE_REASON]
     */
    public void setFailureReason(String  failureReason){
        this.failureReason = failureReason ;
        this.modify("failure_reason",failureReason);
    }

    /**
     * 设置 [HEADERS]
     */
    public void setHeaders(String  headers){
        this.headers = headers ;
        this.modify("headers",headers);
    }

    /**
     * 设置 [EMAIL_TO]
     */
    public void setEmailTo(String  emailTo){
        this.emailTo = emailTo ;
        this.modify("email_to",emailTo);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [REFERENCES]
     */
    public void setReferences(String  references){
        this.references = references ;
        this.modify("references",references);
    }

    /**
     * 设置 [MAILING_ID]
     */
    public void setMailingId(Integer  mailingId){
        this.mailingId = mailingId ;
        this.modify("mailing_id",mailingId);
    }

    /**
     * 设置 [FETCHMAIL_SERVER_ID]
     */
    public void setFetchmailServerId(Integer  fetchmailServerId){
        this.fetchmailServerId = fetchmailServerId ;
        this.modify("fetchmail_server_id",fetchmailServerId);
    }

    /**
     * 设置 [MAIL_MESSAGE_ID]
     */
    public void setMailMessageId(Integer  mailMessageId){
        this.mailMessageId = mailMessageId ;
        this.modify("mail_message_id",mailMessageId);
    }


}

