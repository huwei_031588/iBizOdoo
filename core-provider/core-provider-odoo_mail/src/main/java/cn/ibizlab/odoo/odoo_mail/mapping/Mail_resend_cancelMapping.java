package cn.ibizlab.odoo.odoo_mail.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel;
import cn.ibizlab.odoo.odoo_mail.dto.Mail_resend_cancelDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_resend_cancelMapping extends MappingBase<Mail_resend_cancelDTO, Mail_resend_cancel> {


}

