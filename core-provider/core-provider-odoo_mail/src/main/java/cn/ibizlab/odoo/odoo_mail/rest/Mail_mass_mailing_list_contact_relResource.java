package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_list_contact_relService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;




@Slf4j
@Api(tags = {"Mail_mass_mailing_list_contact_rel" })
@RestController("odoo_mail-mail_mass_mailing_list_contact_rel")
@RequestMapping("")
public class Mail_mass_mailing_list_contact_relResource {

    @Autowired
    private IMail_mass_mailing_list_contact_relService mail_mass_mailing_list_contact_relService;

    @Autowired
    @Lazy
    private Mail_mass_mailing_list_contact_relMapping mail_mass_mailing_list_contact_relMapping;




    @PreAuthorize("hasPermission('Remove',{#mail_mass_mailing_list_contact_rel_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_list_contact_rels/{mail_mass_mailing_list_contact_rel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_list_contact_rel_id") Integer mail_mass_mailing_list_contact_rel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_list_contact_relService.remove(mail_mass_mailing_list_contact_rel_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_list_contact_rels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_mass_mailing_list_contact_relService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_list_contact_rels")

    public ResponseEntity<Mail_mass_mailing_list_contact_relDTO> create(@RequestBody Mail_mass_mailing_list_contact_relDTO mail_mass_mailing_list_contact_reldto) {
        Mail_mass_mailing_list_contact_rel domain = mail_mass_mailing_list_contact_relMapping.toDomain(mail_mass_mailing_list_contact_reldto);
		mail_mass_mailing_list_contact_relService.create(domain);
        Mail_mass_mailing_list_contact_relDTO dto = mail_mass_mailing_list_contact_relMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_list_contact_rels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_list_contact_relDTO> mail_mass_mailing_list_contact_reldtos) {
        mail_mass_mailing_list_contact_relService.createBatch(mail_mass_mailing_list_contact_relMapping.toDomain(mail_mass_mailing_list_contact_reldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_mass_mailing_list_contact_rel_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_list_contact_rels/{mail_mass_mailing_list_contact_rel_id}")
    public ResponseEntity<Mail_mass_mailing_list_contact_relDTO> get(@PathVariable("mail_mass_mailing_list_contact_rel_id") Integer mail_mass_mailing_list_contact_rel_id) {
        Mail_mass_mailing_list_contact_rel domain = mail_mass_mailing_list_contact_relService.get(mail_mass_mailing_list_contact_rel_id);
        Mail_mass_mailing_list_contact_relDTO dto = mail_mass_mailing_list_contact_relMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#mail_mass_mailing_list_contact_rel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_list_contact_rels/{mail_mass_mailing_list_contact_rel_id}")

    public ResponseEntity<Mail_mass_mailing_list_contact_relDTO> update(@PathVariable("mail_mass_mailing_list_contact_rel_id") Integer mail_mass_mailing_list_contact_rel_id, @RequestBody Mail_mass_mailing_list_contact_relDTO mail_mass_mailing_list_contact_reldto) {
		Mail_mass_mailing_list_contact_rel domain = mail_mass_mailing_list_contact_relMapping.toDomain(mail_mass_mailing_list_contact_reldto);
        domain.setId(mail_mass_mailing_list_contact_rel_id);
		mail_mass_mailing_list_contact_relService.update(domain);
		Mail_mass_mailing_list_contact_relDTO dto = mail_mass_mailing_list_contact_relMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_mass_mailing_list_contact_rel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_list_contact_rels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_list_contact_relDTO> mail_mass_mailing_list_contact_reldtos) {
        mail_mass_mailing_list_contact_relService.updateBatch(mail_mass_mailing_list_contact_relMapping.toDomain(mail_mass_mailing_list_contact_reldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_mass_mailing_list_contact_rel" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_list_contact_rels/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_list_contact_relDTO>> fetchDefault(Mail_mass_mailing_list_contact_relSearchContext context) {
        Page<Mail_mass_mailing_list_contact_rel> domains = mail_mass_mailing_list_contact_relService.searchDefault(context) ;
        List<Mail_mass_mailing_list_contact_relDTO> list = mail_mass_mailing_list_contact_relMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_mass_mailing_list_contact_rel" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_list_contact_rels/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_list_contact_relDTO>> searchDefault(Mail_mass_mailing_list_contact_relSearchContext context) {
        Page<Mail_mass_mailing_list_contact_rel> domains = mail_mass_mailing_list_contact_relService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_list_contact_relMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_mass_mailing_list_contact_rel getEntity(){
        return new Mail_mass_mailing_list_contact_rel();
    }

}
