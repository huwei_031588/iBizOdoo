package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mail_statisticsService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;




@Slf4j
@Api(tags = {"Mail_mail_statistics" })
@RestController("odoo_mail-mail_mail_statistics")
@RequestMapping("")
public class Mail_mail_statisticsResource {

    @Autowired
    private IMail_mail_statisticsService mail_mail_statisticsService;

    @Autowired
    @Lazy
    private Mail_mail_statisticsMapping mail_mail_statisticsMapping;




    @PreAuthorize("hasPermission(#mail_mail_statistics_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_mail_statistics" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mail_statistics/{mail_mail_statistics_id}")

    public ResponseEntity<Mail_mail_statisticsDTO> update(@PathVariable("mail_mail_statistics_id") Integer mail_mail_statistics_id, @RequestBody Mail_mail_statisticsDTO mail_mail_statisticsdto) {
		Mail_mail_statistics domain = mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdto);
        domain.setId(mail_mail_statistics_id);
		mail_mail_statisticsService.update(domain);
		Mail_mail_statisticsDTO dto = mail_mail_statisticsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_mail_statistics_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_mail_statistics" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mail_statistics/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mail_statisticsDTO> mail_mail_statisticsdtos) {
        mail_mail_statisticsService.updateBatch(mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_mail_statistics_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_mail_statistics" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mail_statistics/{mail_mail_statistics_id}")
    public ResponseEntity<Mail_mail_statisticsDTO> get(@PathVariable("mail_mail_statistics_id") Integer mail_mail_statistics_id) {
        Mail_mail_statistics domain = mail_mail_statisticsService.get(mail_mail_statistics_id);
        Mail_mail_statisticsDTO dto = mail_mail_statisticsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission('Remove',{#mail_mail_statistics_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_mail_statistics" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mail_statistics/{mail_mail_statistics_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mail_statistics_id") Integer mail_mail_statistics_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mail_statisticsService.remove(mail_mail_statistics_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_mail_statistics" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mail_statistics/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_mail_statisticsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_mail_statistics" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics")

    public ResponseEntity<Mail_mail_statisticsDTO> create(@RequestBody Mail_mail_statisticsDTO mail_mail_statisticsdto) {
        Mail_mail_statistics domain = mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdto);
		mail_mail_statisticsService.create(domain);
        Mail_mail_statisticsDTO dto = mail_mail_statisticsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_mail_statistics" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mail_statistics/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mail_statisticsDTO> mail_mail_statisticsdtos) {
        mail_mail_statisticsService.createBatch(mail_mail_statisticsMapping.toDomain(mail_mail_statisticsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_mail_statistics" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mail_statistics/fetchdefault")
	public ResponseEntity<List<Mail_mail_statisticsDTO>> fetchDefault(Mail_mail_statisticsSearchContext context) {
        Page<Mail_mail_statistics> domains = mail_mail_statisticsService.searchDefault(context) ;
        List<Mail_mail_statisticsDTO> list = mail_mail_statisticsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_mail_statistics" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mail_statistics/searchdefault")
	public ResponseEntity<Page<Mail_mail_statisticsDTO>> searchDefault(Mail_mail_statisticsSearchContext context) {
        Page<Mail_mail_statistics> domains = mail_mail_statisticsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mail_statisticsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_mail_statistics getEntity(){
        return new Mail_mail_statistics();
    }

}
