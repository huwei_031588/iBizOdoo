package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_message_subtypeService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_message_subtypeSearchContext;




@Slf4j
@Api(tags = {"Mail_message_subtype" })
@RestController("odoo_mail-mail_message_subtype")
@RequestMapping("")
public class Mail_message_subtypeResource {

    @Autowired
    private IMail_message_subtypeService mail_message_subtypeService;

    @Autowired
    @Lazy
    private Mail_message_subtypeMapping mail_message_subtypeMapping;










    @PreAuthorize("hasPermission(#mail_message_subtype_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_message_subtype" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_message_subtypes/{mail_message_subtype_id}")
    public ResponseEntity<Mail_message_subtypeDTO> get(@PathVariable("mail_message_subtype_id") Integer mail_message_subtype_id) {
        Mail_message_subtype domain = mail_message_subtypeService.get(mail_message_subtype_id);
        Mail_message_subtypeDTO dto = mail_message_subtypeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_message_subtype_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_message_subtype" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_message_subtypes/{mail_message_subtype_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_message_subtype_id") Integer mail_message_subtype_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_message_subtypeService.remove(mail_message_subtype_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_message_subtype" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_message_subtypes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_message_subtypeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_message_subtype" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_message_subtypes")

    public ResponseEntity<Mail_message_subtypeDTO> create(@RequestBody Mail_message_subtypeDTO mail_message_subtypedto) {
        Mail_message_subtype domain = mail_message_subtypeMapping.toDomain(mail_message_subtypedto);
		mail_message_subtypeService.create(domain);
        Mail_message_subtypeDTO dto = mail_message_subtypeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_message_subtype" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_message_subtypes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_message_subtypeDTO> mail_message_subtypedtos) {
        mail_message_subtypeService.createBatch(mail_message_subtypeMapping.toDomain(mail_message_subtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_message_subtype_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_message_subtype" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_message_subtypes/{mail_message_subtype_id}")

    public ResponseEntity<Mail_message_subtypeDTO> update(@PathVariable("mail_message_subtype_id") Integer mail_message_subtype_id, @RequestBody Mail_message_subtypeDTO mail_message_subtypedto) {
		Mail_message_subtype domain = mail_message_subtypeMapping.toDomain(mail_message_subtypedto);
        domain.setId(mail_message_subtype_id);
		mail_message_subtypeService.update(domain);
		Mail_message_subtypeDTO dto = mail_message_subtypeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_message_subtype_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_message_subtype" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_message_subtypes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_message_subtypeDTO> mail_message_subtypedtos) {
        mail_message_subtypeService.updateBatch(mail_message_subtypeMapping.toDomain(mail_message_subtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_message_subtype" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_message_subtypes/fetchdefault")
	public ResponseEntity<List<Mail_message_subtypeDTO>> fetchDefault(Mail_message_subtypeSearchContext context) {
        Page<Mail_message_subtype> domains = mail_message_subtypeService.searchDefault(context) ;
        List<Mail_message_subtypeDTO> list = mail_message_subtypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_message_subtype" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_message_subtypes/searchdefault")
	public ResponseEntity<Page<Mail_message_subtypeDTO>> searchDefault(Mail_message_subtypeSearchContext context) {
        Page<Mail_message_subtype> domains = mail_message_subtypeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_message_subtypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_message_subtype getEntity(){
        return new Mail_message_subtype();
    }

}
