package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailingService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailingSearchContext;




@Slf4j
@Api(tags = {"Mail_mass_mailing" })
@RestController("odoo_mail-mail_mass_mailing")
@RequestMapping("")
public class Mail_mass_mailingResource {

    @Autowired
    private IMail_mass_mailingService mail_mass_mailingService;

    @Autowired
    @Lazy
    private Mail_mass_mailingMapping mail_mass_mailingMapping;




    @PreAuthorize("hasPermission(#mail_mass_mailing_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailings/{mail_mass_mailing_id}")

    public ResponseEntity<Mail_mass_mailingDTO> update(@PathVariable("mail_mass_mailing_id") Integer mail_mass_mailing_id, @RequestBody Mail_mass_mailingDTO mail_mass_mailingdto) {
		Mail_mass_mailing domain = mail_mass_mailingMapping.toDomain(mail_mass_mailingdto);
        domain.setId(mail_mass_mailing_id);
		mail_mass_mailingService.update(domain);
		Mail_mass_mailingDTO dto = mail_mass_mailingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_mass_mailing_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_mass_mailing" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailingDTO> mail_mass_mailingdtos) {
        mail_mass_mailingService.updateBatch(mail_mass_mailingMapping.toDomain(mail_mass_mailingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailings")

    public ResponseEntity<Mail_mass_mailingDTO> create(@RequestBody Mail_mass_mailingDTO mail_mass_mailingdto) {
        Mail_mass_mailing domain = mail_mass_mailingMapping.toDomain(mail_mass_mailingdto);
		mail_mass_mailingService.create(domain);
        Mail_mass_mailingDTO dto = mail_mass_mailingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_mass_mailing" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailingDTO> mail_mass_mailingdtos) {
        mail_mass_mailingService.createBatch(mail_mass_mailingMapping.toDomain(mail_mass_mailingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mail_mass_mailing_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailings/{mail_mass_mailing_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_id") Integer mail_mass_mailing_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailingService.remove(mail_mass_mailing_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_mass_mailing" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_mass_mailingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_mass_mailing_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailings/{mail_mass_mailing_id}")
    public ResponseEntity<Mail_mass_mailingDTO> get(@PathVariable("mail_mass_mailing_id") Integer mail_mass_mailing_id) {
        Mail_mass_mailing domain = mail_mass_mailingService.get(mail_mass_mailing_id);
        Mail_mass_mailingDTO dto = mail_mass_mailingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_mass_mailing" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailings/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailingDTO>> fetchDefault(Mail_mass_mailingSearchContext context) {
        Page<Mail_mass_mailing> domains = mail_mass_mailingService.searchDefault(context) ;
        List<Mail_mass_mailingDTO> list = mail_mass_mailingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_mass_mailing" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailings/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailingDTO>> searchDefault(Mail_mass_mailingSearchContext context) {
        Page<Mail_mass_mailing> domains = mail_mass_mailingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_mass_mailing getEntity(){
        return new Mail_mass_mailing();
    }

}
