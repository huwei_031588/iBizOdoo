package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_wizard_invite;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_wizard_inviteService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_wizard_inviteSearchContext;




@Slf4j
@Api(tags = {"Mail_wizard_invite" })
@RestController("odoo_mail-mail_wizard_invite")
@RequestMapping("")
public class Mail_wizard_inviteResource {

    @Autowired
    private IMail_wizard_inviteService mail_wizard_inviteService;

    @Autowired
    @Lazy
    private Mail_wizard_inviteMapping mail_wizard_inviteMapping;







    @PreAuthorize("hasPermission(#mail_wizard_invite_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_wizard_invite" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_wizard_invites/{mail_wizard_invite_id}")
    public ResponseEntity<Mail_wizard_inviteDTO> get(@PathVariable("mail_wizard_invite_id") Integer mail_wizard_invite_id) {
        Mail_wizard_invite domain = mail_wizard_inviteService.get(mail_wizard_invite_id);
        Mail_wizard_inviteDTO dto = mail_wizard_inviteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_wizard_invite" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_wizard_invites")

    public ResponseEntity<Mail_wizard_inviteDTO> create(@RequestBody Mail_wizard_inviteDTO mail_wizard_invitedto) {
        Mail_wizard_invite domain = mail_wizard_inviteMapping.toDomain(mail_wizard_invitedto);
		mail_wizard_inviteService.create(domain);
        Mail_wizard_inviteDTO dto = mail_wizard_inviteMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_wizard_invite" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_wizard_invites/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_wizard_inviteDTO> mail_wizard_invitedtos) {
        mail_wizard_inviteService.createBatch(mail_wizard_inviteMapping.toDomain(mail_wizard_invitedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_wizard_invite_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_wizard_invite" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_wizard_invites/{mail_wizard_invite_id}")

    public ResponseEntity<Mail_wizard_inviteDTO> update(@PathVariable("mail_wizard_invite_id") Integer mail_wizard_invite_id, @RequestBody Mail_wizard_inviteDTO mail_wizard_invitedto) {
		Mail_wizard_invite domain = mail_wizard_inviteMapping.toDomain(mail_wizard_invitedto);
        domain.setId(mail_wizard_invite_id);
		mail_wizard_inviteService.update(domain);
		Mail_wizard_inviteDTO dto = mail_wizard_inviteMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_wizard_invite_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_wizard_invite" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_wizard_invites/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_wizard_inviteDTO> mail_wizard_invitedtos) {
        mail_wizard_inviteService.updateBatch(mail_wizard_inviteMapping.toDomain(mail_wizard_invitedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mail_wizard_invite_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_wizard_invite" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_wizard_invites/{mail_wizard_invite_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_wizard_invite_id") Integer mail_wizard_invite_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_wizard_inviteService.remove(mail_wizard_invite_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_wizard_invite" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_wizard_invites/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_wizard_inviteService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_wizard_invite" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_wizard_invites/fetchdefault")
	public ResponseEntity<List<Mail_wizard_inviteDTO>> fetchDefault(Mail_wizard_inviteSearchContext context) {
        Page<Mail_wizard_invite> domains = mail_wizard_inviteService.searchDefault(context) ;
        List<Mail_wizard_inviteDTO> list = mail_wizard_inviteMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_wizard_invite" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_wizard_invites/searchdefault")
	public ResponseEntity<Page<Mail_wizard_inviteDTO>> searchDefault(Mail_wizard_inviteSearchContext context) {
        Page<Mail_wizard_invite> domains = mail_wizard_inviteService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_wizard_inviteMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_wizard_invite getEntity(){
        return new Mail_wizard_invite();
    }

}
