package cn.ibizlab.odoo.odoo_mail.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.odoo.odoo_mail.dto.Mail_mass_mailing_campaignDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_mass_mailing_campaignMapping extends MappingBase<Mail_mass_mailing_campaignDTO, Mail_mass_mailing_campaign> {


}

