package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_thread;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_threadService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_threadSearchContext;




@Slf4j
@Api(tags = {"Mail_thread" })
@RestController("odoo_mail-mail_thread")
@RequestMapping("")
public class Mail_threadResource {

    @Autowired
    private IMail_threadService mail_threadService;

    @Autowired
    @Lazy
    private Mail_threadMapping mail_threadMapping;




    @PreAuthorize("hasPermission('Remove',{#mail_thread_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_thread" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_threads/{mail_thread_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_thread_id") Integer mail_thread_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_threadService.remove(mail_thread_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_thread" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_threads/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_threadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_thread" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_threads")

    public ResponseEntity<Mail_threadDTO> create(@RequestBody Mail_threadDTO mail_threaddto) {
        Mail_thread domain = mail_threadMapping.toDomain(mail_threaddto);
		mail_threadService.create(domain);
        Mail_threadDTO dto = mail_threadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_thread" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_threads/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_threadDTO> mail_threaddtos) {
        mail_threadService.createBatch(mail_threadMapping.toDomain(mail_threaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_thread_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_thread" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_threads/{mail_thread_id}")
    public ResponseEntity<Mail_threadDTO> get(@PathVariable("mail_thread_id") Integer mail_thread_id) {
        Mail_thread domain = mail_threadService.get(mail_thread_id);
        Mail_threadDTO dto = mail_threadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission(#mail_thread_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_thread" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_threads/{mail_thread_id}")

    public ResponseEntity<Mail_threadDTO> update(@PathVariable("mail_thread_id") Integer mail_thread_id, @RequestBody Mail_threadDTO mail_threaddto) {
		Mail_thread domain = mail_threadMapping.toDomain(mail_threaddto);
        domain.setId(mail_thread_id);
		mail_threadService.update(domain);
		Mail_threadDTO dto = mail_threadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_thread_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_thread" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_threads/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_threadDTO> mail_threaddtos) {
        mail_threadService.updateBatch(mail_threadMapping.toDomain(mail_threaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_thread" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_threads/fetchdefault")
	public ResponseEntity<List<Mail_threadDTO>> fetchDefault(Mail_threadSearchContext context) {
        Page<Mail_thread> domains = mail_threadService.searchDefault(context) ;
        List<Mail_threadDTO> list = mail_threadMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_thread" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_threads/searchdefault")
	public ResponseEntity<Page<Mail_threadDTO>> searchDefault(Mail_threadSearchContext context) {
        Page<Mail_thread> domains = mail_threadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_threadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_thread getEntity(){
        return new Mail_thread();
    }

}
