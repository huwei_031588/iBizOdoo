package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_followers;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_followersService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_followersSearchContext;




@Slf4j
@Api(tags = {"Mail_followers" })
@RestController("odoo_mail-mail_followers")
@RequestMapping("")
public class Mail_followersResource {

    @Autowired
    private IMail_followersService mail_followersService;

    @Autowired
    @Lazy
    private Mail_followersMapping mail_followersMapping;




    @PreAuthorize("hasPermission(#mail_followers_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_followers" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_followers/{mail_followers_id}")
    public ResponseEntity<Mail_followersDTO> get(@PathVariable("mail_followers_id") Integer mail_followers_id) {
        Mail_followers domain = mail_followersService.get(mail_followers_id);
        Mail_followersDTO dto = mail_followersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_followers" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers")

    public ResponseEntity<Mail_followersDTO> create(@RequestBody Mail_followersDTO mail_followersdto) {
        Mail_followers domain = mail_followersMapping.toDomain(mail_followersdto);
		mail_followersService.create(domain);
        Mail_followersDTO dto = mail_followersMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_followers" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_followers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_followersDTO> mail_followersdtos) {
        mail_followersService.createBatch(mail_followersMapping.toDomain(mail_followersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#mail_followers_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_followers" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_followers/{mail_followers_id}")

    public ResponseEntity<Mail_followersDTO> update(@PathVariable("mail_followers_id") Integer mail_followers_id, @RequestBody Mail_followersDTO mail_followersdto) {
		Mail_followers domain = mail_followersMapping.toDomain(mail_followersdto);
        domain.setId(mail_followers_id);
		mail_followersService.update(domain);
		Mail_followersDTO dto = mail_followersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_followers_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_followers" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_followers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_followersDTO> mail_followersdtos) {
        mail_followersService.updateBatch(mail_followersMapping.toDomain(mail_followersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_followers_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_followers" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_followers/{mail_followers_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_followers_id") Integer mail_followers_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_followersService.remove(mail_followers_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_followers" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_followers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_followersService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_followers" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_followers/fetchdefault")
	public ResponseEntity<List<Mail_followersDTO>> fetchDefault(Mail_followersSearchContext context) {
        Page<Mail_followers> domains = mail_followersService.searchDefault(context) ;
        List<Mail_followersDTO> list = mail_followersMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_followers" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_followers/searchdefault")
	public ResponseEntity<Page<Mail_followersDTO>> searchDefault(Mail_followersSearchContext context) {
        Page<Mail_followers> domains = mail_followersService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_followersMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_followers getEntity(){
        return new Mail_followers();
    }

}
