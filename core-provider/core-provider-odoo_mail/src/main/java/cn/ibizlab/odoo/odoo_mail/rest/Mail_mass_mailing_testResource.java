package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_test;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_testService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_testSearchContext;




@Slf4j
@Api(tags = {"Mail_mass_mailing_test" })
@RestController("odoo_mail-mail_mass_mailing_test")
@RequestMapping("")
public class Mail_mass_mailing_testResource {

    @Autowired
    private IMail_mass_mailing_testService mail_mass_mailing_testService;

    @Autowired
    @Lazy
    private Mail_mass_mailing_testMapping mail_mass_mailing_testMapping;




    @PreAuthorize("hasPermission(#mail_mass_mailing_test_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_test" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tests/{mail_mass_mailing_test_id}")

    public ResponseEntity<Mail_mass_mailing_testDTO> update(@PathVariable("mail_mass_mailing_test_id") Integer mail_mass_mailing_test_id, @RequestBody Mail_mass_mailing_testDTO mail_mass_mailing_testdto) {
		Mail_mass_mailing_test domain = mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdto);
        domain.setId(mail_mass_mailing_test_id);
		mail_mass_mailing_testService.update(domain);
		Mail_mass_mailing_testDTO dto = mail_mass_mailing_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_mass_mailing_test_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_mass_mailing_test" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_testDTO> mail_mass_mailing_testdtos) {
        mail_mass_mailing_testService.updateBatch(mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_test" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tests")

    public ResponseEntity<Mail_mass_mailing_testDTO> create(@RequestBody Mail_mass_mailing_testDTO mail_mass_mailing_testdto) {
        Mail_mass_mailing_test domain = mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdto);
		mail_mass_mailing_testService.create(domain);
        Mail_mass_mailing_testDTO dto = mail_mass_mailing_testMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_mass_mailing_test" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_testDTO> mail_mass_mailing_testdtos) {
        mail_mass_mailing_testService.createBatch(mail_mass_mailing_testMapping.toDomain(mail_mass_mailing_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#mail_mass_mailing_test_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_test" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tests/{mail_mass_mailing_test_id}")
    public ResponseEntity<Mail_mass_mailing_testDTO> get(@PathVariable("mail_mass_mailing_test_id") Integer mail_mass_mailing_test_id) {
        Mail_mass_mailing_test domain = mail_mass_mailing_testService.get(mail_mass_mailing_test_id);
        Mail_mass_mailing_testDTO dto = mail_mass_mailing_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#mail_mass_mailing_test_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_test" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tests/{mail_mass_mailing_test_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_test_id") Integer mail_mass_mailing_test_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_testService.remove(mail_mass_mailing_test_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_mass_mailing_test" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_mass_mailing_testService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_mass_mailing_test" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_tests/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_testDTO>> fetchDefault(Mail_mass_mailing_testSearchContext context) {
        Page<Mail_mass_mailing_test> domains = mail_mass_mailing_testService.searchDefault(context) ;
        List<Mail_mass_mailing_testDTO> list = mail_mass_mailing_testMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_mass_mailing_test" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_tests/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_testDTO>> searchDefault(Mail_mass_mailing_testSearchContext context) {
        Page<Mail_mass_mailing_test> domains = mail_mass_mailing_testService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_testMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_mass_mailing_test getEntity(){
        return new Mail_mass_mailing_test();
    }

}
