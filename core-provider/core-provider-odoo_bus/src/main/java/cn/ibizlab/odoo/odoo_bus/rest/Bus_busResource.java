package cn.ibizlab.odoo.odoo_bus.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_bus.dto.*;
import cn.ibizlab.odoo.odoo_bus.mapping.*;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_bus;
import cn.ibizlab.odoo.core.odoo_bus.service.IBus_busService;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_busSearchContext;




@Slf4j
@Api(tags = {"Bus_bus" })
@RestController("odoo_bus-bus_bus")
@RequestMapping("")
public class Bus_busResource {

    @Autowired
    private IBus_busService bus_busService;

    @Autowired
    @Lazy
    private Bus_busMapping bus_busMapping;




    @PreAuthorize("hasPermission(#bus_bus_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Bus_bus" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/bus_buses/{bus_bus_id}")
    public ResponseEntity<Bus_busDTO> get(@PathVariable("bus_bus_id") Integer bus_bus_id) {
        Bus_bus domain = bus_busService.get(bus_bus_id);
        Bus_busDTO dto = bus_busMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#bus_bus_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Bus_bus" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/bus_buses/{bus_bus_id}")

    public ResponseEntity<Bus_busDTO> update(@PathVariable("bus_bus_id") Integer bus_bus_id, @RequestBody Bus_busDTO bus_busdto) {
		Bus_bus domain = bus_busMapping.toDomain(bus_busdto);
        domain.setId(bus_bus_id);
		bus_busService.update(domain);
		Bus_busDTO dto = bus_busMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#bus_bus_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Bus_bus" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/bus_buses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Bus_busDTO> bus_busdtos) {
        bus_busService.updateBatch(bus_busMapping.toDomain(bus_busdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#bus_bus_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Bus_bus" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/bus_buses/{bus_bus_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("bus_bus_id") Integer bus_bus_id) {
         return ResponseEntity.status(HttpStatus.OK).body(bus_busService.remove(bus_bus_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Bus_bus" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/bus_buses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        bus_busService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Bus_bus" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_buses")

    public ResponseEntity<Bus_busDTO> create(@RequestBody Bus_busDTO bus_busdto) {
        Bus_bus domain = bus_busMapping.toDomain(bus_busdto);
		bus_busService.create(domain);
        Bus_busDTO dto = bus_busMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Bus_bus" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_buses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Bus_busDTO> bus_busdtos) {
        bus_busService.createBatch(bus_busMapping.toDomain(bus_busdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Bus_bus" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/bus_buses/fetchdefault")
	public ResponseEntity<List<Bus_busDTO>> fetchDefault(Bus_busSearchContext context) {
        Page<Bus_bus> domains = bus_busService.searchDefault(context) ;
        List<Bus_busDTO> list = bus_busMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Bus_bus" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/bus_buses/searchdefault")
	public ResponseEntity<Page<Bus_busDTO>> searchDefault(Bus_busSearchContext context) {
        Page<Bus_bus> domains = bus_busService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(bus_busMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Bus_bus getEntity(){
        return new Bus_bus();
    }

}
