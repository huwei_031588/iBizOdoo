package cn.ibizlab.odoo.odoo_im_livechat.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_im_livechat.dto.*;
import cn.ibizlab.odoo.odoo_im_livechat.mapping.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_report_operatorService;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_report_operatorSearchContext;




@Slf4j
@Api(tags = {"Im_livechat_report_operator" })
@RestController("odoo_im_livechat-im_livechat_report_operator")
@RequestMapping("")
public class Im_livechat_report_operatorResource {

    @Autowired
    private IIm_livechat_report_operatorService im_livechat_report_operatorService;

    @Autowired
    @Lazy
    private Im_livechat_report_operatorMapping im_livechat_report_operatorMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Im_livechat_report_operator" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators")

    public ResponseEntity<Im_livechat_report_operatorDTO> create(@RequestBody Im_livechat_report_operatorDTO im_livechat_report_operatordto) {
        Im_livechat_report_operator domain = im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordto);
		im_livechat_report_operatorService.create(domain);
        Im_livechat_report_operatorDTO dto = im_livechat_report_operatorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Im_livechat_report_operator" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_operators/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Im_livechat_report_operatorDTO> im_livechat_report_operatordtos) {
        im_livechat_report_operatorService.createBatch(im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#im_livechat_report_operator_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Im_livechat_report_operator" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_operators/{im_livechat_report_operator_id}")
    public ResponseEntity<Im_livechat_report_operatorDTO> get(@PathVariable("im_livechat_report_operator_id") Integer im_livechat_report_operator_id) {
        Im_livechat_report_operator domain = im_livechat_report_operatorService.get(im_livechat_report_operator_id);
        Im_livechat_report_operatorDTO dto = im_livechat_report_operatorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#im_livechat_report_operator_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Im_livechat_report_operator" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_report_operators/{im_livechat_report_operator_id}")

    public ResponseEntity<Im_livechat_report_operatorDTO> update(@PathVariable("im_livechat_report_operator_id") Integer im_livechat_report_operator_id, @RequestBody Im_livechat_report_operatorDTO im_livechat_report_operatordto) {
		Im_livechat_report_operator domain = im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordto);
        domain.setId(im_livechat_report_operator_id);
		im_livechat_report_operatorService.update(domain);
		Im_livechat_report_operatorDTO dto = im_livechat_report_operatorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#im_livechat_report_operator_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Im_livechat_report_operator" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_report_operators/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Im_livechat_report_operatorDTO> im_livechat_report_operatordtos) {
        im_livechat_report_operatorService.updateBatch(im_livechat_report_operatorMapping.toDomain(im_livechat_report_operatordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('Remove',{#im_livechat_report_operator_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Im_livechat_report_operator" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_report_operators/{im_livechat_report_operator_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("im_livechat_report_operator_id") Integer im_livechat_report_operator_id) {
         return ResponseEntity.status(HttpStatus.OK).body(im_livechat_report_operatorService.remove(im_livechat_report_operator_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Im_livechat_report_operator" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_report_operators/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        im_livechat_report_operatorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Im_livechat_report_operator" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/im_livechat_report_operators/fetchdefault")
	public ResponseEntity<List<Im_livechat_report_operatorDTO>> fetchDefault(Im_livechat_report_operatorSearchContext context) {
        Page<Im_livechat_report_operator> domains = im_livechat_report_operatorService.searchDefault(context) ;
        List<Im_livechat_report_operatorDTO> list = im_livechat_report_operatorMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Im_livechat_report_operator" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/im_livechat_report_operators/searchdefault")
	public ResponseEntity<Page<Im_livechat_report_operatorDTO>> searchDefault(Im_livechat_report_operatorSearchContext context) {
        Page<Im_livechat_report_operator> domains = im_livechat_report_operatorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(im_livechat_report_operatorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Im_livechat_report_operator getEntity(){
        return new Im_livechat_report_operator();
    }

}
