package cn.ibizlab.odoo.odoo_im_livechat.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_im_livechat.dto.*;
import cn.ibizlab.odoo.odoo_im_livechat.mapping.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_channelService;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channelSearchContext;




@Slf4j
@Api(tags = {"Im_livechat_channel" })
@RestController("odoo_im_livechat-im_livechat_channel")
@RequestMapping("")
public class Im_livechat_channelResource {

    @Autowired
    private IIm_livechat_channelService im_livechat_channelService;

    @Autowired
    @Lazy
    private Im_livechat_channelMapping im_livechat_channelMapping;




    @PreAuthorize("hasPermission(#im_livechat_channel_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Im_livechat_channel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_channels/{im_livechat_channel_id}")
    public ResponseEntity<Im_livechat_channelDTO> get(@PathVariable("im_livechat_channel_id") Integer im_livechat_channel_id) {
        Im_livechat_channel domain = im_livechat_channelService.get(im_livechat_channel_id);
        Im_livechat_channelDTO dto = im_livechat_channelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#im_livechat_channel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Im_livechat_channel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_channels/{im_livechat_channel_id}")

    public ResponseEntity<Im_livechat_channelDTO> update(@PathVariable("im_livechat_channel_id") Integer im_livechat_channel_id, @RequestBody Im_livechat_channelDTO im_livechat_channeldto) {
		Im_livechat_channel domain = im_livechat_channelMapping.toDomain(im_livechat_channeldto);
        domain.setId(im_livechat_channel_id);
		im_livechat_channelService.update(domain);
		Im_livechat_channelDTO dto = im_livechat_channelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#im_livechat_channel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Im_livechat_channel" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_channels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Im_livechat_channelDTO> im_livechat_channeldtos) {
        im_livechat_channelService.updateBatch(im_livechat_channelMapping.toDomain(im_livechat_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#im_livechat_channel_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Im_livechat_channel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_channels/{im_livechat_channel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("im_livechat_channel_id") Integer im_livechat_channel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(im_livechat_channelService.remove(im_livechat_channel_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Im_livechat_channel" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_channels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        im_livechat_channelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Im_livechat_channel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channels")

    public ResponseEntity<Im_livechat_channelDTO> create(@RequestBody Im_livechat_channelDTO im_livechat_channeldto) {
        Im_livechat_channel domain = im_livechat_channelMapping.toDomain(im_livechat_channeldto);
		im_livechat_channelService.create(domain);
        Im_livechat_channelDTO dto = im_livechat_channelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Im_livechat_channel" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Im_livechat_channelDTO> im_livechat_channeldtos) {
        im_livechat_channelService.createBatch(im_livechat_channelMapping.toDomain(im_livechat_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Im_livechat_channel" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/im_livechat_channels/fetchdefault")
	public ResponseEntity<List<Im_livechat_channelDTO>> fetchDefault(Im_livechat_channelSearchContext context) {
        Page<Im_livechat_channel> domains = im_livechat_channelService.searchDefault(context) ;
        List<Im_livechat_channelDTO> list = im_livechat_channelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Im_livechat_channel" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/im_livechat_channels/searchdefault")
	public ResponseEntity<Page<Im_livechat_channelDTO>> searchDefault(Im_livechat_channelSearchContext context) {
        Page<Im_livechat_channel> domains = im_livechat_channelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(im_livechat_channelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Im_livechat_channel getEntity(){
        return new Im_livechat_channel();
    }

}
