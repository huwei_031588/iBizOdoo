package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_bank_statement_import_journal_creationDTO]
 */
@Data
public class Account_bank_statement_import_journal_creationDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [OUTBOUND_PAYMENT_METHOD_IDS]
     *
     */
    @JSONField(name = "outbound_payment_method_ids")
    @JsonProperty("outbound_payment_method_ids")
    private String outboundPaymentMethodIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [TYPE_CONTROL_IDS]
     *
     */
    @JSONField(name = "type_control_ids")
    @JsonProperty("type_control_ids")
    private String typeControlIds;

    /**
     * 属性 [INBOUND_PAYMENT_METHOD_IDS]
     *
     */
    @JSONField(name = "inbound_payment_method_ids")
    @JsonProperty("inbound_payment_method_ids")
    private String inboundPaymentMethodIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [ACCOUNT_CONTROL_IDS]
     *
     */
    @JSONField(name = "account_control_ids")
    @JsonProperty("account_control_ids")
    private String accountControlIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 属性 [REFUND_SEQUENCE_ID]
     *
     */
    @JSONField(name = "refund_sequence_id")
    @JsonProperty("refund_sequence_id")
    private Integer refundSequenceId;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [BANK_ID]
     *
     */
    @JSONField(name = "bank_id")
    @JsonProperty("bank_id")
    private Integer bankId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [LOSS_ACCOUNT_ID]
     *
     */
    @JSONField(name = "loss_account_id")
    @JsonProperty("loss_account_id")
    private Integer lossAccountId;

    /**
     * 属性 [DEFAULT_CREDIT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "default_credit_account_id")
    @JsonProperty("default_credit_account_id")
    private Integer defaultCreditAccountId;

    /**
     * 属性 [REFUND_SEQUENCE]
     *
     */
    @JSONField(name = "refund_sequence")
    @JsonProperty("refund_sequence")
    private String refundSequence;

    /**
     * 属性 [AT_LEAST_ONE_OUTBOUND]
     *
     */
    @JSONField(name = "at_least_one_outbound")
    @JsonProperty("at_least_one_outbound")
    private String atLeastOneOutbound;

    /**
     * 属性 [PROFIT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "profit_account_id")
    @JsonProperty("profit_account_id")
    private Integer profitAccountId;

    /**
     * 属性 [UPDATE_POSTED]
     *
     */
    @JSONField(name = "update_posted")
    @JsonProperty("update_posted")
    private String updatePosted;

    /**
     * 属性 [REFUND_SEQUENCE_NUMBER_NEXT]
     *
     */
    @JSONField(name = "refund_sequence_number_next")
    @JsonProperty("refund_sequence_number_next")
    private Integer refundSequenceNumberNext;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Integer aliasId;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 属性 [GROUP_INVOICE_LINES]
     *
     */
    @JSONField(name = "group_invoice_lines")
    @JsonProperty("group_invoice_lines")
    private String groupInvoiceLines;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;

    /**
     * 属性 [BANK_ACCOUNT_ID]
     *
     */
    @JSONField(name = "bank_account_id")
    @JsonProperty("bank_account_id")
    private Integer bankAccountId;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [AT_LEAST_ONE_INBOUND]
     *
     */
    @JSONField(name = "at_least_one_inbound")
    @JsonProperty("at_least_one_inbound")
    private String atLeastOneInbound;

    /**
     * 属性 [BANK_ACC_NUMBER]
     *
     */
    @JSONField(name = "bank_acc_number")
    @JsonProperty("bank_acc_number")
    private String bankAccNumber;

    /**
     * 属性 [SEQUENCE_NUMBER_NEXT]
     *
     */
    @JSONField(name = "sequence_number_next")
    @JsonProperty("sequence_number_next")
    private Integer sequenceNumberNext;

    /**
     * 属性 [COMPANY_PARTNER_ID]
     *
     */
    @JSONField(name = "company_partner_id")
    @JsonProperty("company_partner_id")
    private Integer companyPartnerId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [DEFAULT_DEBIT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "default_debit_account_id")
    @JsonProperty("default_debit_account_id")
    private Integer defaultDebitAccountId;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [BANK_STATEMENTS_SOURCE]
     *
     */
    @JSONField(name = "bank_statements_source")
    @JsonProperty("bank_statements_source")
    private String bankStatementsSource;

    /**
     * 属性 [KANBAN_DASHBOARD]
     *
     */
    @JSONField(name = "kanban_dashboard")
    @JsonProperty("kanban_dashboard")
    private String kanbanDashboard;

    /**
     * 属性 [KANBAN_DASHBOARD_GRAPH]
     *
     */
    @JSONField(name = "kanban_dashboard_graph")
    @JsonProperty("kanban_dashboard_graph")
    private String kanbanDashboardGraph;

    /**
     * 属性 [SHOW_ON_DASHBOARD]
     *
     */
    @JSONField(name = "show_on_dashboard")
    @JsonProperty("show_on_dashboard")
    private String showOnDashboard;

    /**
     * 属性 [POST_AT_BANK_REC]
     *
     */
    @JSONField(name = "post_at_bank_rec")
    @JsonProperty("post_at_bank_rec")
    private String postAtBankRec;

    /**
     * 属性 [BELONGS_TO_COMPANY]
     *
     */
    @JSONField(name = "belongs_to_company")
    @JsonProperty("belongs_to_company")
    private String belongsToCompany;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 属性 [SEQUENCE_ID]
     *
     */
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    private Integer sequenceId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Integer  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }


}

