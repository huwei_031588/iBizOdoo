package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_bank_statement_lineDTO]
 */
@Data
public class Account_bank_statement_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PARTNER_NAME]
     *
     */
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    private String partnerName;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [ACCOUNT_NUMBER]
     *
     */
    @JSONField(name = "account_number")
    @JsonProperty("account_number")
    private String accountNumber;

    /**
     * 属性 [REF]
     *
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [JOURNAL_ENTRY_IDS]
     *
     */
    @JSONField(name = "journal_entry_ids")
    @JsonProperty("journal_entry_ids")
    private String journalEntryIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [MOVE_NAME]
     *
     */
    @JSONField(name = "move_name")
    @JsonProperty("move_name")
    private String moveName;

    /**
     * 属性 [UNIQUE_IMPORT_ID]
     *
     */
    @JSONField(name = "unique_import_id")
    @JsonProperty("unique_import_id")
    private String uniqueImportId;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [POS_STATEMENT_ID]
     *
     */
    @JSONField(name = "pos_statement_id")
    @JsonProperty("pos_statement_id")
    private Integer posStatementId;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [AMOUNT_CURRENCY]
     *
     */
    @JSONField(name = "amount_currency")
    @JsonProperty("amount_currency")
    private Double amountCurrency;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [JOURNAL_CURRENCY_ID]
     *
     */
    @JSONField(name = "journal_currency_id")
    @JsonProperty("journal_currency_id")
    private Integer journalCurrencyId;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 属性 [STATEMENT_ID_TEXT]
     *
     */
    @JSONField(name = "statement_id_text")
    @JsonProperty("statement_id_text")
    private String statementIdText;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [BANK_ACCOUNT_ID]
     *
     */
    @JSONField(name = "bank_account_id")
    @JsonProperty("bank_account_id")
    private Integer bankAccountId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 属性 [STATEMENT_ID]
     *
     */
    @JSONField(name = "statement_id")
    @JsonProperty("statement_id")
    private Integer statementId;


    /**
     * 设置 [PARTNER_NAME]
     */
    public void setPartnerName(String  partnerName){
        this.partnerName = partnerName ;
        this.modify("partner_name",partnerName);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [ACCOUNT_NUMBER]
     */
    public void setAccountNumber(String  accountNumber){
        this.accountNumber = accountNumber ;
        this.modify("account_number",accountNumber);
    }

    /**
     * 设置 [REF]
     */
    public void setRef(String  ref){
        this.ref = ref ;
        this.modify("ref",ref);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [MOVE_NAME]
     */
    public void setMoveName(String  moveName){
        this.moveName = moveName ;
        this.modify("move_name",moveName);
    }

    /**
     * 设置 [UNIQUE_IMPORT_ID]
     */
    public void setUniqueImportId(String  uniqueImportId){
        this.uniqueImportId = uniqueImportId ;
        this.modify("unique_import_id",uniqueImportId);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [POS_STATEMENT_ID]
     */
    public void setPosStatementId(Integer  posStatementId){
        this.posStatementId = posStatementId ;
        this.modify("pos_statement_id",posStatementId);
    }

    /**
     * 设置 [AMOUNT_CURRENCY]
     */
    public void setAmountCurrency(Double  amountCurrency){
        this.amountCurrency = amountCurrency ;
        this.modify("amount_currency",amountCurrency);
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    public void setAccountId(Integer  accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Integer  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [BANK_ACCOUNT_ID]
     */
    public void setBankAccountId(Integer  bankAccountId){
        this.bankAccountId = bankAccountId ;
        this.modify("bank_account_id",bankAccountId);
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Integer  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [STATEMENT_ID]
     */
    public void setStatementId(Integer  statementId){
        this.statementId = statementId ;
        this.modify("statement_id",statementId);
    }


}

