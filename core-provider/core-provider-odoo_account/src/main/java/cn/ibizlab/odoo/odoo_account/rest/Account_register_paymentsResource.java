package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_register_paymentsService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_register_paymentsSearchContext;




@Slf4j
@Api(tags = {"Account_register_payments" })
@RestController("odoo_account-account_register_payments")
@RequestMapping("")
public class Account_register_paymentsResource {

    @Autowired
    private IAccount_register_paymentsService account_register_paymentsService;

    @Autowired
    @Lazy
    private Account_register_paymentsMapping account_register_paymentsMapping;







    @PreAuthorize("hasPermission(#account_register_payments_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_register_payments" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_register_payments/{account_register_payments_id}")
    public ResponseEntity<Account_register_paymentsDTO> get(@PathVariable("account_register_payments_id") Integer account_register_payments_id) {
        Account_register_payments domain = account_register_paymentsService.get(account_register_payments_id);
        Account_register_paymentsDTO dto = account_register_paymentsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_register_payments" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments")

    public ResponseEntity<Account_register_paymentsDTO> create(@RequestBody Account_register_paymentsDTO account_register_paymentsdto) {
        Account_register_payments domain = account_register_paymentsMapping.toDomain(account_register_paymentsdto);
		account_register_paymentsService.create(domain);
        Account_register_paymentsDTO dto = account_register_paymentsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_register_payments" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_register_payments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_register_paymentsDTO> account_register_paymentsdtos) {
        account_register_paymentsService.createBatch(account_register_paymentsMapping.toDomain(account_register_paymentsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_register_payments_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_register_payments" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_register_payments/{account_register_payments_id}")

    public ResponseEntity<Account_register_paymentsDTO> update(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_paymentsDTO account_register_paymentsdto) {
		Account_register_payments domain = account_register_paymentsMapping.toDomain(account_register_paymentsdto);
        domain.setId(account_register_payments_id);
		account_register_paymentsService.update(domain);
		Account_register_paymentsDTO dto = account_register_paymentsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_register_payments_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_register_payments" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_register_payments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_register_paymentsDTO> account_register_paymentsdtos) {
        account_register_paymentsService.updateBatch(account_register_paymentsMapping.toDomain(account_register_paymentsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#account_register_payments_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_register_payments" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_register_payments/{account_register_payments_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_register_payments_id") Integer account_register_payments_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_register_paymentsService.remove(account_register_payments_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_register_payments" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_register_payments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_register_paymentsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_register_payments" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_register_payments/fetchdefault")
	public ResponseEntity<List<Account_register_paymentsDTO>> fetchDefault(Account_register_paymentsSearchContext context) {
        Page<Account_register_payments> domains = account_register_paymentsService.searchDefault(context) ;
        List<Account_register_paymentsDTO> list = account_register_paymentsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_register_payments" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_register_payments/searchdefault")
	public ResponseEntity<Page<Account_register_paymentsDTO>> searchDefault(Account_register_paymentsSearchContext context) {
        Page<Account_register_payments> domains = account_register_paymentsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_register_paymentsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_register_payments getEntity(){
        return new Account_register_payments();
    }

}
