package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_move_lineDTO]
 */
@Data
public class Account_move_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [BALANCE]
     *
     */
    @JSONField(name = "balance")
    @JsonProperty("balance")
    private Double balance;

    /**
     * 属性 [TAX_IDS]
     *
     */
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    private String taxIds;

    /**
     * 属性 [AMOUNT_CURRENCY]
     *
     */
    @JSONField(name = "amount_currency")
    @JsonProperty("amount_currency")
    private Double amountCurrency;

    /**
     * 属性 [TAX_LINE_GROUPING_KEY]
     *
     */
    @JSONField(name = "tax_line_grouping_key")
    @JsonProperty("tax_line_grouping_key")
    private String taxLineGroupingKey;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [RECONCILED]
     *
     */
    @JSONField(name = "reconciled")
    @JsonProperty("reconciled")
    private String reconciled;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [BALANCE_CASH_BASIS]
     *
     */
    @JSONField(name = "balance_cash_basis")
    @JsonProperty("balance_cash_basis")
    private Double balanceCashBasis;

    /**
     * 属性 [CREDIT_CASH_BASIS]
     *
     */
    @JSONField(name = "credit_cash_basis")
    @JsonProperty("credit_cash_basis")
    private Double creditCashBasis;

    /**
     * 属性 [QUANTITY]
     *
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private Double quantity;

    /**
     * 属性 [AMOUNT_RESIDUAL]
     *
     */
    @JSONField(name = "amount_residual")
    @JsonProperty("amount_residual")
    private Double amountResidual;

    /**
     * 属性 [RECOMPUTE_TAX_LINE]
     *
     */
    @JSONField(name = "recompute_tax_line")
    @JsonProperty("recompute_tax_line")
    private String recomputeTaxLine;

    /**
     * 属性 [TAX_EXIGIBLE]
     *
     */
    @JSONField(name = "tax_exigible")
    @JsonProperty("tax_exigible")
    private String taxExigible;

    /**
     * 属性 [AMOUNT_RESIDUAL_CURRENCY]
     *
     */
    @JSONField(name = "amount_residual_currency")
    @JsonProperty("amount_residual_currency")
    private Double amountResidualCurrency;

    /**
     * 属性 [TAX_BASE_AMOUNT]
     *
     */
    @JSONField(name = "tax_base_amount")
    @JsonProperty("tax_base_amount")
    private Double taxBaseAmount;

    /**
     * 属性 [PARENT_STATE]
     *
     */
    @JSONField(name = "parent_state")
    @JsonProperty("parent_state")
    private String parentState;

    /**
     * 属性 [BLOCKED]
     *
     */
    @JSONField(name = "blocked")
    @JsonProperty("blocked")
    private String blocked;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MATCHED_CREDIT_IDS]
     *
     */
    @JSONField(name = "matched_credit_ids")
    @JsonProperty("matched_credit_ids")
    private String matchedCreditIds;

    /**
     * 属性 [ANALYTIC_LINE_IDS]
     *
     */
    @JSONField(name = "analytic_line_ids")
    @JsonProperty("analytic_line_ids")
    private String analyticLineIds;

    /**
     * 属性 [DATE_MATURITY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_maturity" , format="yyyy-MM-dd")
    @JsonProperty("date_maturity")
    private Timestamp dateMaturity;

    /**
     * 属性 [DEBIT]
     *
     */
    @JSONField(name = "debit")
    @JsonProperty("debit")
    private Double debit;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [COUNTERPART]
     *
     */
    @JSONField(name = "counterpart")
    @JsonProperty("counterpart")
    private String counterpart;

    /**
     * 属性 [MATCHED_DEBIT_IDS]
     *
     */
    @JSONField(name = "matched_debit_ids")
    @JsonProperty("matched_debit_ids")
    private String matchedDebitIds;

    /**
     * 属性 [DEBIT_CASH_BASIS]
     *
     */
    @JSONField(name = "debit_cash_basis")
    @JsonProperty("debit_cash_basis")
    private Double debitCashBasis;

    /**
     * 属性 [CREDIT]
     *
     */
    @JSONField(name = "credit")
    @JsonProperty("credit")
    private Double credit;

    /**
     * 属性 [NARRATION]
     *
     */
    @JSONField(name = "narration")
    @JsonProperty("narration")
    private String narration;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 属性 [USER_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "user_type_id_text")
    @JsonProperty("user_type_id_text")
    private String userTypeIdText;

    /**
     * 属性 [PAYMENT_ID_TEXT]
     *
     */
    @JSONField(name = "payment_id_text")
    @JsonProperty("payment_id_text")
    private String paymentIdText;

    /**
     * 属性 [TAX_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "tax_line_id_text")
    @JsonProperty("tax_line_id_text")
    private String taxLineIdText;

    /**
     * 属性 [FULL_RECONCILE_ID_TEXT]
     *
     */
    @JSONField(name = "full_reconcile_id_text")
    @JsonProperty("full_reconcile_id_text")
    private String fullReconcileIdText;

    /**
     * 属性 [EXPENSE_ID_TEXT]
     *
     */
    @JSONField(name = "expense_id_text")
    @JsonProperty("expense_id_text")
    private String expenseIdText;

    /**
     * 属性 [MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 属性 [COMPANY_CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "company_currency_id_text")
    @JsonProperty("company_currency_id_text")
    private String companyCurrencyIdText;

    /**
     * 属性 [INVOICE_ID_TEXT]
     *
     */
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    private String invoiceIdText;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    private String analyticAccountIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 属性 [REF]
     *
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [STATEMENT_ID_TEXT]
     *
     */
    @JSONField(name = "statement_id_text")
    @JsonProperty("statement_id_text")
    private String statementIdText;

    /**
     * 属性 [STATEMENT_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "statement_line_id_text")
    @JsonProperty("statement_line_id_text")
    private String statementLineIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Integer moveId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [PAYMENT_ID]
     *
     */
    @JSONField(name = "payment_id")
    @JsonProperty("payment_id")
    private Integer paymentId;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Integer companyCurrencyId;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID]
     *
     */
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    private Integer analyticAccountId;

    /**
     * 属性 [INVOICE_ID]
     *
     */
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    private Integer invoiceId;

    /**
     * 属性 [TAX_LINE_ID]
     *
     */
    @JSONField(name = "tax_line_id")
    @JsonProperty("tax_line_id")
    private Integer taxLineId;

    /**
     * 属性 [FULL_RECONCILE_ID]
     *
     */
    @JSONField(name = "full_reconcile_id")
    @JsonProperty("full_reconcile_id")
    private Integer fullReconcileId;

    /**
     * 属性 [STATEMENT_LINE_ID]
     *
     */
    @JSONField(name = "statement_line_id")
    @JsonProperty("statement_line_id")
    private Integer statementLineId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 属性 [STATEMENT_ID]
     *
     */
    @JSONField(name = "statement_id")
    @JsonProperty("statement_id")
    private Integer statementId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [USER_TYPE_ID]
     *
     */
    @JSONField(name = "user_type_id")
    @JsonProperty("user_type_id")
    private Integer userTypeId;

    /**
     * 属性 [EXPENSE_ID]
     *
     */
    @JSONField(name = "expense_id")
    @JsonProperty("expense_id")
    private Integer expenseId;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [BALANCE]
     */
    public void setBalance(Double  balance){
        this.balance = balance ;
        this.modify("balance",balance);
    }

    /**
     * 设置 [AMOUNT_CURRENCY]
     */
    public void setAmountCurrency(Double  amountCurrency){
        this.amountCurrency = amountCurrency ;
        this.modify("amount_currency",amountCurrency);
    }

    /**
     * 设置 [RECONCILED]
     */
    public void setReconciled(String  reconciled){
        this.reconciled = reconciled ;
        this.modify("reconciled",reconciled);
    }

    /**
     * 设置 [BALANCE_CASH_BASIS]
     */
    public void setBalanceCashBasis(Double  balanceCashBasis){
        this.balanceCashBasis = balanceCashBasis ;
        this.modify("balance_cash_basis",balanceCashBasis);
    }

    /**
     * 设置 [CREDIT_CASH_BASIS]
     */
    public void setCreditCashBasis(Double  creditCashBasis){
        this.creditCashBasis = creditCashBasis ;
        this.modify("credit_cash_basis",creditCashBasis);
    }

    /**
     * 设置 [QUANTITY]
     */
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [AMOUNT_RESIDUAL]
     */
    public void setAmountResidual(Double  amountResidual){
        this.amountResidual = amountResidual ;
        this.modify("amount_residual",amountResidual);
    }

    /**
     * 设置 [TAX_EXIGIBLE]
     */
    public void setTaxExigible(String  taxExigible){
        this.taxExigible = taxExigible ;
        this.modify("tax_exigible",taxExigible);
    }

    /**
     * 设置 [AMOUNT_RESIDUAL_CURRENCY]
     */
    public void setAmountResidualCurrency(Double  amountResidualCurrency){
        this.amountResidualCurrency = amountResidualCurrency ;
        this.modify("amount_residual_currency",amountResidualCurrency);
    }

    /**
     * 设置 [TAX_BASE_AMOUNT]
     */
    public void setTaxBaseAmount(Double  taxBaseAmount){
        this.taxBaseAmount = taxBaseAmount ;
        this.modify("tax_base_amount",taxBaseAmount);
    }

    /**
     * 设置 [BLOCKED]
     */
    public void setBlocked(String  blocked){
        this.blocked = blocked ;
        this.modify("blocked",blocked);
    }

    /**
     * 设置 [DATE_MATURITY]
     */
    public void setDateMaturity(Timestamp  dateMaturity){
        this.dateMaturity = dateMaturity ;
        this.modify("date_maturity",dateMaturity);
    }

    /**
     * 设置 [DEBIT]
     */
    public void setDebit(Double  debit){
        this.debit = debit ;
        this.modify("debit",debit);
    }

    /**
     * 设置 [DEBIT_CASH_BASIS]
     */
    public void setDebitCashBasis(Double  debitCashBasis){
        this.debitCashBasis = debitCashBasis ;
        this.modify("debit_cash_basis",debitCashBasis);
    }

    /**
     * 设置 [CREDIT]
     */
    public void setCredit(Double  credit){
        this.credit = credit ;
        this.modify("credit",credit);
    }

    /**
     * 设置 [MOVE_ID]
     */
    public void setMoveId(Integer  moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Integer  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [PAYMENT_ID]
     */
    public void setPaymentId(Integer  paymentId){
        this.paymentId = paymentId ;
        this.modify("payment_id",paymentId);
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID]
     */
    public void setCompanyCurrencyId(Integer  companyCurrencyId){
        this.companyCurrencyId = companyCurrencyId ;
        this.modify("company_currency_id",companyCurrencyId);
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Integer  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID]
     */
    public void setAnalyticAccountId(Integer  analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }

    /**
     * 设置 [INVOICE_ID]
     */
    public void setInvoiceId(Integer  invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }

    /**
     * 设置 [TAX_LINE_ID]
     */
    public void setTaxLineId(Integer  taxLineId){
        this.taxLineId = taxLineId ;
        this.modify("tax_line_id",taxLineId);
    }

    /**
     * 设置 [FULL_RECONCILE_ID]
     */
    public void setFullReconcileId(Integer  fullReconcileId){
        this.fullReconcileId = fullReconcileId ;
        this.modify("full_reconcile_id",fullReconcileId);
    }

    /**
     * 设置 [STATEMENT_LINE_ID]
     */
    public void setStatementLineId(Integer  statementLineId){
        this.statementLineId = statementLineId ;
        this.modify("statement_line_id",statementLineId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Integer  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Integer  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    public void setAccountId(Integer  accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [STATEMENT_ID]
     */
    public void setStatementId(Integer  statementId){
        this.statementId = statementId ;
        this.modify("statement_id",statementId);
    }

    /**
     * 设置 [USER_TYPE_ID]
     */
    public void setUserTypeId(Integer  userTypeId){
        this.userTypeId = userTypeId ;
        this.modify("user_type_id",userTypeId);
    }

    /**
     * 设置 [EXPENSE_ID]
     */
    public void setExpenseId(Integer  expenseId){
        this.expenseId = expenseId ;
        this.modify("expense_id",expenseId);
    }


}

