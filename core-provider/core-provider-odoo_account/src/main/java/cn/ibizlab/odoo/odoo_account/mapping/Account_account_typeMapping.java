package cn.ibizlab.odoo.odoo_account.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type;
import cn.ibizlab.odoo.odoo_account.dto.Account_account_typeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_account_typeMapping extends MappingBase<Account_account_typeDTO, Account_account_type> {


}

