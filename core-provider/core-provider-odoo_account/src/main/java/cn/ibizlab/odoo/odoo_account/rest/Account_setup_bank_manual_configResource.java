package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_setup_bank_manual_configService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_setup_bank_manual_configSearchContext;




@Slf4j
@Api(tags = {"Account_setup_bank_manual_config" })
@RestController("odoo_account-account_setup_bank_manual_config")
@RequestMapping("")
public class Account_setup_bank_manual_configResource {

    @Autowired
    private IAccount_setup_bank_manual_configService account_setup_bank_manual_configService;

    @Autowired
    @Lazy
    private Account_setup_bank_manual_configMapping account_setup_bank_manual_configMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_setup_bank_manual_config" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_setup_bank_manual_configs")

    public ResponseEntity<Account_setup_bank_manual_configDTO> create(@RequestBody Account_setup_bank_manual_configDTO account_setup_bank_manual_configdto) {
        Account_setup_bank_manual_config domain = account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdto);
		account_setup_bank_manual_configService.create(domain);
        Account_setup_bank_manual_configDTO dto = account_setup_bank_manual_configMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_setup_bank_manual_config" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_setup_bank_manual_configs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_setup_bank_manual_configDTO> account_setup_bank_manual_configdtos) {
        account_setup_bank_manual_configService.createBatch(account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_setup_bank_manual_config_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_setup_bank_manual_config" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_setup_bank_manual_configs/{account_setup_bank_manual_config_id}")

    public ResponseEntity<Account_setup_bank_manual_configDTO> update(@PathVariable("account_setup_bank_manual_config_id") Integer account_setup_bank_manual_config_id, @RequestBody Account_setup_bank_manual_configDTO account_setup_bank_manual_configdto) {
		Account_setup_bank_manual_config domain = account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdto);
        domain.setId(account_setup_bank_manual_config_id);
		account_setup_bank_manual_configService.update(domain);
		Account_setup_bank_manual_configDTO dto = account_setup_bank_manual_configMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_setup_bank_manual_config_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_setup_bank_manual_config" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_setup_bank_manual_configs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_setup_bank_manual_configDTO> account_setup_bank_manual_configdtos) {
        account_setup_bank_manual_configService.updateBatch(account_setup_bank_manual_configMapping.toDomain(account_setup_bank_manual_configdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#account_setup_bank_manual_config_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_setup_bank_manual_config" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_setup_bank_manual_configs/{account_setup_bank_manual_config_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_setup_bank_manual_config_id") Integer account_setup_bank_manual_config_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_setup_bank_manual_configService.remove(account_setup_bank_manual_config_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_setup_bank_manual_config" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_setup_bank_manual_configs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_setup_bank_manual_configService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_setup_bank_manual_config_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_setup_bank_manual_config" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_setup_bank_manual_configs/{account_setup_bank_manual_config_id}")
    public ResponseEntity<Account_setup_bank_manual_configDTO> get(@PathVariable("account_setup_bank_manual_config_id") Integer account_setup_bank_manual_config_id) {
        Account_setup_bank_manual_config domain = account_setup_bank_manual_configService.get(account_setup_bank_manual_config_id);
        Account_setup_bank_manual_configDTO dto = account_setup_bank_manual_configMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_setup_bank_manual_config" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_setup_bank_manual_configs/fetchdefault")
	public ResponseEntity<List<Account_setup_bank_manual_configDTO>> fetchDefault(Account_setup_bank_manual_configSearchContext context) {
        Page<Account_setup_bank_manual_config> domains = account_setup_bank_manual_configService.searchDefault(context) ;
        List<Account_setup_bank_manual_configDTO> list = account_setup_bank_manual_configMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_setup_bank_manual_config" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_setup_bank_manual_configs/searchdefault")
	public ResponseEntity<Page<Account_setup_bank_manual_configDTO>> searchDefault(Account_setup_bank_manual_configSearchContext context) {
        Page<Account_setup_bank_manual_config> domains = account_setup_bank_manual_configService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_setup_bank_manual_configMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_setup_bank_manual_config getEntity(){
        return new Account_setup_bank_manual_config();
    }

}
