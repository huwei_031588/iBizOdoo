package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_financial_year_opService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_financial_year_opSearchContext;




@Slf4j
@Api(tags = {"Account_financial_year_op" })
@RestController("odoo_account-account_financial_year_op")
@RequestMapping("")
public class Account_financial_year_opResource {

    @Autowired
    private IAccount_financial_year_opService account_financial_year_opService;

    @Autowired
    @Lazy
    private Account_financial_year_opMapping account_financial_year_opMapping;




    @PreAuthorize("hasPermission(#account_financial_year_op_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_financial_year_op" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_financial_year_ops/{account_financial_year_op_id}")

    public ResponseEntity<Account_financial_year_opDTO> update(@PathVariable("account_financial_year_op_id") Integer account_financial_year_op_id, @RequestBody Account_financial_year_opDTO account_financial_year_opdto) {
		Account_financial_year_op domain = account_financial_year_opMapping.toDomain(account_financial_year_opdto);
        domain.setId(account_financial_year_op_id);
		account_financial_year_opService.update(domain);
		Account_financial_year_opDTO dto = account_financial_year_opMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_financial_year_op_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_financial_year_op" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_financial_year_ops/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_financial_year_opDTO> account_financial_year_opdtos) {
        account_financial_year_opService.updateBatch(account_financial_year_opMapping.toDomain(account_financial_year_opdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_financial_year_op" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_financial_year_ops")

    public ResponseEntity<Account_financial_year_opDTO> create(@RequestBody Account_financial_year_opDTO account_financial_year_opdto) {
        Account_financial_year_op domain = account_financial_year_opMapping.toDomain(account_financial_year_opdto);
		account_financial_year_opService.create(domain);
        Account_financial_year_opDTO dto = account_financial_year_opMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_financial_year_op" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_financial_year_ops/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_financial_year_opDTO> account_financial_year_opdtos) {
        account_financial_year_opService.createBatch(account_financial_year_opMapping.toDomain(account_financial_year_opdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_financial_year_op_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_financial_year_op" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_financial_year_ops/{account_financial_year_op_id}")
    public ResponseEntity<Account_financial_year_opDTO> get(@PathVariable("account_financial_year_op_id") Integer account_financial_year_op_id) {
        Account_financial_year_op domain = account_financial_year_opService.get(account_financial_year_op_id);
        Account_financial_year_opDTO dto = account_financial_year_opMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#account_financial_year_op_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_financial_year_op" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_financial_year_ops/{account_financial_year_op_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_financial_year_op_id") Integer account_financial_year_op_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_financial_year_opService.remove(account_financial_year_op_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_financial_year_op" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_financial_year_ops/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_financial_year_opService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_financial_year_op" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_financial_year_ops/fetchdefault")
	public ResponseEntity<List<Account_financial_year_opDTO>> fetchDefault(Account_financial_year_opSearchContext context) {
        Page<Account_financial_year_op> domains = account_financial_year_opService.searchDefault(context) ;
        List<Account_financial_year_opDTO> list = account_financial_year_opMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_financial_year_op" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_financial_year_ops/searchdefault")
	public ResponseEntity<Page<Account_financial_year_opDTO>> searchDefault(Account_financial_year_opSearchContext context) {
        Page<Account_financial_year_op> domains = account_financial_year_opService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_financial_year_opMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_financial_year_op getEntity(){
        return new Account_financial_year_op();
    }

}
