package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_move_lineService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_lineSearchContext;




@Slf4j
@Api(tags = {"Account_move_line" })
@RestController("odoo_account-account_move_line")
@RequestMapping("")
public class Account_move_lineResource {

    @Autowired
    private IAccount_move_lineService account_move_lineService;

    @Autowired
    @Lazy
    private Account_move_lineMapping account_move_lineMapping;




    @PreAuthorize("hasPermission(#account_move_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_move_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_move_lines/{account_move_line_id}")

    public ResponseEntity<Account_move_lineDTO> update(@PathVariable("account_move_line_id") Integer account_move_line_id, @RequestBody Account_move_lineDTO account_move_linedto) {
		Account_move_line domain = account_move_lineMapping.toDomain(account_move_linedto);
        domain.setId(account_move_line_id);
		account_move_lineService.update(domain);
		Account_move_lineDTO dto = account_move_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_move_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_move_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_move_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_move_lineDTO> account_move_linedtos) {
        account_move_lineService.updateBatch(account_move_lineMapping.toDomain(account_move_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_move_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_lines")

    public ResponseEntity<Account_move_lineDTO> create(@RequestBody Account_move_lineDTO account_move_linedto) {
        Account_move_line domain = account_move_lineMapping.toDomain(account_move_linedto);
		account_move_lineService.create(domain);
        Account_move_lineDTO dto = account_move_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_move_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_move_lineDTO> account_move_linedtos) {
        account_move_lineService.createBatch(account_move_lineMapping.toDomain(account_move_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#account_move_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_move_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_move_lines/{account_move_line_id}")
    public ResponseEntity<Account_move_lineDTO> get(@PathVariable("account_move_line_id") Integer account_move_line_id) {
        Account_move_line domain = account_move_lineService.get(account_move_line_id);
        Account_move_lineDTO dto = account_move_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#account_move_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_move_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_move_lines/{account_move_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_move_line_id") Integer account_move_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_move_lineService.remove(account_move_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_move_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_move_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_move_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_move_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_move_lines/fetchdefault")
	public ResponseEntity<List<Account_move_lineDTO>> fetchDefault(Account_move_lineSearchContext context) {
        Page<Account_move_line> domains = account_move_lineService.searchDefault(context) ;
        List<Account_move_lineDTO> list = account_move_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_move_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_move_lines/searchdefault")
	public ResponseEntity<Page<Account_move_lineDTO>> searchDefault(Account_move_lineSearchContext context) {
        Page<Account_move_line> domains = account_move_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_move_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_move_line getEntity(){
        return new Account_move_line();
    }

}
