package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_confirm;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_confirmService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_confirmSearchContext;




@Slf4j
@Api(tags = {"Account_invoice_confirm" })
@RestController("odoo_account-account_invoice_confirm")
@RequestMapping("")
public class Account_invoice_confirmResource {

    @Autowired
    private IAccount_invoice_confirmService account_invoice_confirmService;

    @Autowired
    @Lazy
    private Account_invoice_confirmMapping account_invoice_confirmMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_invoice_confirm" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms")

    public ResponseEntity<Account_invoice_confirmDTO> create(@RequestBody Account_invoice_confirmDTO account_invoice_confirmdto) {
        Account_invoice_confirm domain = account_invoice_confirmMapping.toDomain(account_invoice_confirmdto);
		account_invoice_confirmService.create(domain);
        Account_invoice_confirmDTO dto = account_invoice_confirmMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_invoice_confirm" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_confirmDTO> account_invoice_confirmdtos) {
        account_invoice_confirmService.createBatch(account_invoice_confirmMapping.toDomain(account_invoice_confirmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_invoice_confirm_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_invoice_confirm" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_confirms/{account_invoice_confirm_id}")
    public ResponseEntity<Account_invoice_confirmDTO> get(@PathVariable("account_invoice_confirm_id") Integer account_invoice_confirm_id) {
        Account_invoice_confirm domain = account_invoice_confirmService.get(account_invoice_confirm_id);
        Account_invoice_confirmDTO dto = account_invoice_confirmMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#account_invoice_confirm_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_invoice_confirm" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_confirms/{account_invoice_confirm_id}")

    public ResponseEntity<Account_invoice_confirmDTO> update(@PathVariable("account_invoice_confirm_id") Integer account_invoice_confirm_id, @RequestBody Account_invoice_confirmDTO account_invoice_confirmdto) {
		Account_invoice_confirm domain = account_invoice_confirmMapping.toDomain(account_invoice_confirmdto);
        domain.setId(account_invoice_confirm_id);
		account_invoice_confirmService.update(domain);
		Account_invoice_confirmDTO dto = account_invoice_confirmMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_invoice_confirm_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_invoice_confirm" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_confirms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_confirmDTO> account_invoice_confirmdtos) {
        account_invoice_confirmService.updateBatch(account_invoice_confirmMapping.toDomain(account_invoice_confirmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_invoice_confirm_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_invoice_confirm" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_confirms/{account_invoice_confirm_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_confirm_id") Integer account_invoice_confirm_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_confirmService.remove(account_invoice_confirm_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_invoice_confirm" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_confirms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_invoice_confirmService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_invoice_confirm" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_confirms/fetchdefault")
	public ResponseEntity<List<Account_invoice_confirmDTO>> fetchDefault(Account_invoice_confirmSearchContext context) {
        Page<Account_invoice_confirm> domains = account_invoice_confirmService.searchDefault(context) ;
        List<Account_invoice_confirmDTO> list = account_invoice_confirmMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_invoice_confirm" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_confirms/searchdefault")
	public ResponseEntity<Page<Account_invoice_confirmDTO>> searchDefault(Account_invoice_confirmSearchContext context) {
        Page<Account_invoice_confirm> domains = account_invoice_confirmService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_confirmMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_invoice_confirm getEntity(){
        return new Account_invoice_confirm();
    }

}
