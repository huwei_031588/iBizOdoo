package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_send;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_sendService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_sendSearchContext;




@Slf4j
@Api(tags = {"Account_invoice_send" })
@RestController("odoo_account-account_invoice_send")
@RequestMapping("")
public class Account_invoice_sendResource {

    @Autowired
    private IAccount_invoice_sendService account_invoice_sendService;

    @Autowired
    @Lazy
    private Account_invoice_sendMapping account_invoice_sendMapping;




    @PreAuthorize("hasPermission(#account_invoice_send_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_invoice_send" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_sends/{account_invoice_send_id}")
    public ResponseEntity<Account_invoice_sendDTO> get(@PathVariable("account_invoice_send_id") Integer account_invoice_send_id) {
        Account_invoice_send domain = account_invoice_sendService.get(account_invoice_send_id);
        Account_invoice_sendDTO dto = account_invoice_sendMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission('Remove',{#account_invoice_send_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_invoice_send" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_sends/{account_invoice_send_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_send_id") Integer account_invoice_send_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_sendService.remove(account_invoice_send_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_invoice_send" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_sends/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_invoice_sendService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_invoice_send_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_invoice_send" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_sends/{account_invoice_send_id}")

    public ResponseEntity<Account_invoice_sendDTO> update(@PathVariable("account_invoice_send_id") Integer account_invoice_send_id, @RequestBody Account_invoice_sendDTO account_invoice_senddto) {
		Account_invoice_send domain = account_invoice_sendMapping.toDomain(account_invoice_senddto);
        domain.setId(account_invoice_send_id);
		account_invoice_sendService.update(domain);
		Account_invoice_sendDTO dto = account_invoice_sendMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_invoice_send_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_invoice_send" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_sends/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_sendDTO> account_invoice_senddtos) {
        account_invoice_sendService.updateBatch(account_invoice_sendMapping.toDomain(account_invoice_senddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_invoice_send" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_sends")

    public ResponseEntity<Account_invoice_sendDTO> create(@RequestBody Account_invoice_sendDTO account_invoice_senddto) {
        Account_invoice_send domain = account_invoice_sendMapping.toDomain(account_invoice_senddto);
		account_invoice_sendService.create(domain);
        Account_invoice_sendDTO dto = account_invoice_sendMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_invoice_send" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_sends/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_sendDTO> account_invoice_senddtos) {
        account_invoice_sendService.createBatch(account_invoice_sendMapping.toDomain(account_invoice_senddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_invoice_send" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_sends/fetchdefault")
	public ResponseEntity<List<Account_invoice_sendDTO>> fetchDefault(Account_invoice_sendSearchContext context) {
        Page<Account_invoice_send> domains = account_invoice_sendService.searchDefault(context) ;
        List<Account_invoice_sendDTO> list = account_invoice_sendMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_invoice_send" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_sends/searchdefault")
	public ResponseEntity<Page<Account_invoice_sendDTO>> searchDefault(Account_invoice_sendSearchContext context) {
        Page<Account_invoice_send> domains = account_invoice_sendService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_sendMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_invoice_send getEntity(){
        return new Account_invoice_send();
    }

}
