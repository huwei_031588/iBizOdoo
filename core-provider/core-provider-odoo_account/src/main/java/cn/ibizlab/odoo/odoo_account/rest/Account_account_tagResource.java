package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_tag;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_account_tagService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_tagSearchContext;




@Slf4j
@Api(tags = {"Account_account_tag" })
@RestController("odoo_account-account_account_tag")
@RequestMapping("")
public class Account_account_tagResource {

    @Autowired
    private IAccount_account_tagService account_account_tagService;

    @Autowired
    @Lazy
    private Account_account_tagMapping account_account_tagMapping;




    @PreAuthorize("hasPermission(#account_account_tag_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_account_tag" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_account_tags/{account_account_tag_id}")

    public ResponseEntity<Account_account_tagDTO> update(@PathVariable("account_account_tag_id") Integer account_account_tag_id, @RequestBody Account_account_tagDTO account_account_tagdto) {
		Account_account_tag domain = account_account_tagMapping.toDomain(account_account_tagdto);
        domain.setId(account_account_tag_id);
		account_account_tagService.update(domain);
		Account_account_tagDTO dto = account_account_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_account_tag_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_account_tag" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_account_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_account_tagDTO> account_account_tagdtos) {
        account_account_tagService.updateBatch(account_account_tagMapping.toDomain(account_account_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_account_tag" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_tags")

    public ResponseEntity<Account_account_tagDTO> create(@RequestBody Account_account_tagDTO account_account_tagdto) {
        Account_account_tag domain = account_account_tagMapping.toDomain(account_account_tagdto);
		account_account_tagService.create(domain);
        Account_account_tagDTO dto = account_account_tagMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_account_tag" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_account_tagDTO> account_account_tagdtos) {
        account_account_tagService.createBatch(account_account_tagMapping.toDomain(account_account_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#account_account_tag_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_account_tag" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_account_tags/{account_account_tag_id}")
    public ResponseEntity<Account_account_tagDTO> get(@PathVariable("account_account_tag_id") Integer account_account_tag_id) {
        Account_account_tag domain = account_account_tagService.get(account_account_tag_id);
        Account_account_tagDTO dto = account_account_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#account_account_tag_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_account_tag" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_account_tags/{account_account_tag_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_account_tag_id") Integer account_account_tag_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_account_tagService.remove(account_account_tag_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_account_tag" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_account_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_account_tagService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_account_tag" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_account_tags/fetchdefault")
	public ResponseEntity<List<Account_account_tagDTO>> fetchDefault(Account_account_tagSearchContext context) {
        Page<Account_account_tag> domains = account_account_tagService.searchDefault(context) ;
        List<Account_account_tagDTO> list = account_account_tagMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_account_tag" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_account_tags/searchdefault")
	public ResponseEntity<Page<Account_account_tagDTO>> searchDefault(Account_account_tagSearchContext context) {
        Page<Account_account_tag> domains = account_account_tagService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_account_tagMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_account_tag getEntity(){
        return new Account_account_tag();
    }

}
