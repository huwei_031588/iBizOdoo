package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_partial_reconcileService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_partial_reconcileSearchContext;




@Slf4j
@Api(tags = {"Account_partial_reconcile" })
@RestController("odoo_account-account_partial_reconcile")
@RequestMapping("")
public class Account_partial_reconcileResource {

    @Autowired
    private IAccount_partial_reconcileService account_partial_reconcileService;

    @Autowired
    @Lazy
    private Account_partial_reconcileMapping account_partial_reconcileMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_partial_reconcile" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_partial_reconciles")

    public ResponseEntity<Account_partial_reconcileDTO> create(@RequestBody Account_partial_reconcileDTO account_partial_reconciledto) {
        Account_partial_reconcile domain = account_partial_reconcileMapping.toDomain(account_partial_reconciledto);
		account_partial_reconcileService.create(domain);
        Account_partial_reconcileDTO dto = account_partial_reconcileMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_partial_reconcile" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_partial_reconciles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_partial_reconcileDTO> account_partial_reconciledtos) {
        account_partial_reconcileService.createBatch(account_partial_reconcileMapping.toDomain(account_partial_reconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#account_partial_reconcile_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_partial_reconcile" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_partial_reconciles/{account_partial_reconcile_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_partial_reconcile_id") Integer account_partial_reconcile_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_partial_reconcileService.remove(account_partial_reconcile_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_partial_reconcile" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_partial_reconciles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_partial_reconcileService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_partial_reconcile_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_partial_reconcile" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_partial_reconciles/{account_partial_reconcile_id}")

    public ResponseEntity<Account_partial_reconcileDTO> update(@PathVariable("account_partial_reconcile_id") Integer account_partial_reconcile_id, @RequestBody Account_partial_reconcileDTO account_partial_reconciledto) {
		Account_partial_reconcile domain = account_partial_reconcileMapping.toDomain(account_partial_reconciledto);
        domain.setId(account_partial_reconcile_id);
		account_partial_reconcileService.update(domain);
		Account_partial_reconcileDTO dto = account_partial_reconcileMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_partial_reconcile_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_partial_reconcile" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_partial_reconciles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_partial_reconcileDTO> account_partial_reconciledtos) {
        account_partial_reconcileService.updateBatch(account_partial_reconcileMapping.toDomain(account_partial_reconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_partial_reconcile_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_partial_reconcile" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_partial_reconciles/{account_partial_reconcile_id}")
    public ResponseEntity<Account_partial_reconcileDTO> get(@PathVariable("account_partial_reconcile_id") Integer account_partial_reconcile_id) {
        Account_partial_reconcile domain = account_partial_reconcileService.get(account_partial_reconcile_id);
        Account_partial_reconcileDTO dto = account_partial_reconcileMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_partial_reconcile" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_partial_reconciles/fetchdefault")
	public ResponseEntity<List<Account_partial_reconcileDTO>> fetchDefault(Account_partial_reconcileSearchContext context) {
        Page<Account_partial_reconcile> domains = account_partial_reconcileService.searchDefault(context) ;
        List<Account_partial_reconcileDTO> list = account_partial_reconcileMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_partial_reconcile" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_partial_reconciles/searchdefault")
	public ResponseEntity<Page<Account_partial_reconcileDTO>> searchDefault(Account_partial_reconcileSearchContext context) {
        Page<Account_partial_reconcile> domains = account_partial_reconcileService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_partial_reconcileMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_partial_reconcile getEntity(){
        return new Account_partial_reconcile();
    }

}
