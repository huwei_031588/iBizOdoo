package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconciliation_widgetService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;




@Slf4j
@Api(tags = {"Account_reconciliation_widget" })
@RestController("odoo_account-account_reconciliation_widget")
@RequestMapping("")
public class Account_reconciliation_widgetResource {

    @Autowired
    private IAccount_reconciliation_widgetService account_reconciliation_widgetService;

    @Autowired
    @Lazy
    private Account_reconciliation_widgetMapping account_reconciliation_widgetMapping;




    @PreAuthorize("hasPermission('Remove',{#account_reconciliation_widget_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_reconciliation_widget" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconciliation_widgets/{account_reconciliation_widget_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_reconciliation_widget_id") Integer account_reconciliation_widget_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_reconciliation_widgetService.remove(account_reconciliation_widget_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_reconciliation_widget" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconciliation_widgets/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_reconciliation_widgetService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_reconciliation_widget" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconciliation_widgets")

    public ResponseEntity<Account_reconciliation_widgetDTO> create(@RequestBody Account_reconciliation_widgetDTO account_reconciliation_widgetdto) {
        Account_reconciliation_widget domain = account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdto);
		account_reconciliation_widgetService.create(domain);
        Account_reconciliation_widgetDTO dto = account_reconciliation_widgetMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_reconciliation_widget" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconciliation_widgets/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_reconciliation_widgetDTO> account_reconciliation_widgetdtos) {
        account_reconciliation_widgetService.createBatch(account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_reconciliation_widget_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_reconciliation_widget" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconciliation_widgets/{account_reconciliation_widget_id}")

    public ResponseEntity<Account_reconciliation_widgetDTO> update(@PathVariable("account_reconciliation_widget_id") Integer account_reconciliation_widget_id, @RequestBody Account_reconciliation_widgetDTO account_reconciliation_widgetdto) {
		Account_reconciliation_widget domain = account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdto);
        domain.setId(account_reconciliation_widget_id);
		account_reconciliation_widgetService.update(domain);
		Account_reconciliation_widgetDTO dto = account_reconciliation_widgetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_reconciliation_widget_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_reconciliation_widget" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconciliation_widgets/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_reconciliation_widgetDTO> account_reconciliation_widgetdtos) {
        account_reconciliation_widgetService.updateBatch(account_reconciliation_widgetMapping.toDomain(account_reconciliation_widgetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_reconciliation_widget_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_reconciliation_widget" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_reconciliation_widgets/{account_reconciliation_widget_id}")
    public ResponseEntity<Account_reconciliation_widgetDTO> get(@PathVariable("account_reconciliation_widget_id") Integer account_reconciliation_widget_id) {
        Account_reconciliation_widget domain = account_reconciliation_widgetService.get(account_reconciliation_widget_id);
        Account_reconciliation_widgetDTO dto = account_reconciliation_widgetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_reconciliation_widget" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_reconciliation_widgets/fetchdefault")
	public ResponseEntity<List<Account_reconciliation_widgetDTO>> fetchDefault(Account_reconciliation_widgetSearchContext context) {
        Page<Account_reconciliation_widget> domains = account_reconciliation_widgetService.searchDefault(context) ;
        List<Account_reconciliation_widgetDTO> list = account_reconciliation_widgetMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_reconciliation_widget" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_reconciliation_widgets/searchdefault")
	public ResponseEntity<Page<Account_reconciliation_widgetDTO>> searchDefault(Account_reconciliation_widgetSearchContext context) {
        Page<Account_reconciliation_widget> domains = account_reconciliation_widgetService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_reconciliation_widgetMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_reconciliation_widget getEntity(){
        return new Account_reconciliation_widget();
    }

}
