package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statementService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statementSearchContext;




@Slf4j
@Api(tags = {"Account_bank_statement" })
@RestController("odoo_account-account_bank_statement")
@RequestMapping("")
public class Account_bank_statementResource {

    @Autowired
    private IAccount_bank_statementService account_bank_statementService;

    @Autowired
    @Lazy
    private Account_bank_statementMapping account_bank_statementMapping;




    @PreAuthorize("hasPermission('Remove',{#account_bank_statement_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_bank_statement" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statements/{account_bank_statement_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_id") Integer account_bank_statement_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statementService.remove(account_bank_statement_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_bank_statement" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statements/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_bank_statementService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_bank_statement" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements")

    public ResponseEntity<Account_bank_statementDTO> create(@RequestBody Account_bank_statementDTO account_bank_statementdto) {
        Account_bank_statement domain = account_bank_statementMapping.toDomain(account_bank_statementdto);
		account_bank_statementService.create(domain);
        Account_bank_statementDTO dto = account_bank_statementMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_bank_statement" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statementDTO> account_bank_statementdtos) {
        account_bank_statementService.createBatch(account_bank_statementMapping.toDomain(account_bank_statementdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_bank_statement_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_bank_statement" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statements/{account_bank_statement_id}")
    public ResponseEntity<Account_bank_statementDTO> get(@PathVariable("account_bank_statement_id") Integer account_bank_statement_id) {
        Account_bank_statement domain = account_bank_statementService.get(account_bank_statement_id);
        Account_bank_statementDTO dto = account_bank_statementMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission(#account_bank_statement_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_bank_statement" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statements/{account_bank_statement_id}")

    public ResponseEntity<Account_bank_statementDTO> update(@PathVariable("account_bank_statement_id") Integer account_bank_statement_id, @RequestBody Account_bank_statementDTO account_bank_statementdto) {
		Account_bank_statement domain = account_bank_statementMapping.toDomain(account_bank_statementdto);
        domain.setId(account_bank_statement_id);
		account_bank_statementService.update(domain);
		Account_bank_statementDTO dto = account_bank_statementMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_bank_statement_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_bank_statement" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statements/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statementDTO> account_bank_statementdtos) {
        account_bank_statementService.updateBatch(account_bank_statementMapping.toDomain(account_bank_statementdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_bank_statement" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statements/fetchdefault")
	public ResponseEntity<List<Account_bank_statementDTO>> fetchDefault(Account_bank_statementSearchContext context) {
        Page<Account_bank_statement> domains = account_bank_statementService.searchDefault(context) ;
        List<Account_bank_statementDTO> list = account_bank_statementMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_bank_statement" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statements/searchdefault")
	public ResponseEntity<Page<Account_bank_statementDTO>> searchDefault(Account_bank_statementSearchContext context) {
        Page<Account_bank_statement> domains = account_bank_statementService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statementMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_bank_statement getEntity(){
        return new Account_bank_statement();
    }

}
