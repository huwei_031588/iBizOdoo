package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_template;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_tax_templateService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_templateSearchContext;




@Slf4j
@Api(tags = {"Account_tax_template" })
@RestController("odoo_account-account_tax_template")
@RequestMapping("")
public class Account_tax_templateResource {

    @Autowired
    private IAccount_tax_templateService account_tax_templateService;

    @Autowired
    @Lazy
    private Account_tax_templateMapping account_tax_templateMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_tax_template" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_tax_templates")

    public ResponseEntity<Account_tax_templateDTO> create(@RequestBody Account_tax_templateDTO account_tax_templatedto) {
        Account_tax_template domain = account_tax_templateMapping.toDomain(account_tax_templatedto);
		account_tax_templateService.create(domain);
        Account_tax_templateDTO dto = account_tax_templateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_tax_template" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_tax_templates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_tax_templateDTO> account_tax_templatedtos) {
        account_tax_templateService.createBatch(account_tax_templateMapping.toDomain(account_tax_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#account_tax_template_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_tax_template" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_tax_templates/{account_tax_template_id}")

    public ResponseEntity<Account_tax_templateDTO> update(@PathVariable("account_tax_template_id") Integer account_tax_template_id, @RequestBody Account_tax_templateDTO account_tax_templatedto) {
		Account_tax_template domain = account_tax_templateMapping.toDomain(account_tax_templatedto);
        domain.setId(account_tax_template_id);
		account_tax_templateService.update(domain);
		Account_tax_templateDTO dto = account_tax_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_tax_template_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_tax_template" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_tax_templates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_tax_templateDTO> account_tax_templatedtos) {
        account_tax_templateService.updateBatch(account_tax_templateMapping.toDomain(account_tax_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_tax_template_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_tax_template" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_tax_templates/{account_tax_template_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_tax_template_id") Integer account_tax_template_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_tax_templateService.remove(account_tax_template_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_tax_template" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_tax_templates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_tax_templateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_tax_template_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_tax_template" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_tax_templates/{account_tax_template_id}")
    public ResponseEntity<Account_tax_templateDTO> get(@PathVariable("account_tax_template_id") Integer account_tax_template_id) {
        Account_tax_template domain = account_tax_templateService.get(account_tax_template_id);
        Account_tax_templateDTO dto = account_tax_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_tax_template" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_tax_templates/fetchdefault")
	public ResponseEntity<List<Account_tax_templateDTO>> fetchDefault(Account_tax_templateSearchContext context) {
        Page<Account_tax_template> domains = account_tax_templateService.searchDefault(context) ;
        List<Account_tax_templateDTO> list = account_tax_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_tax_template" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_tax_templates/searchdefault")
	public ResponseEntity<Page<Account_tax_templateDTO>> searchDefault(Account_tax_templateSearchContext context) {
        Page<Account_tax_template> domains = account_tax_templateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_tax_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_tax_template getEntity(){
        return new Account_tax_template();
    }

}
