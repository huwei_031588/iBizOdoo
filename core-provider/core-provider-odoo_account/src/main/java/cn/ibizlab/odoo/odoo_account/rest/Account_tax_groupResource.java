package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_tax_groupService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_groupSearchContext;




@Slf4j
@Api(tags = {"Account_tax_group" })
@RestController("odoo_account-account_tax_group")
@RequestMapping("")
public class Account_tax_groupResource {

    @Autowired
    private IAccount_tax_groupService account_tax_groupService;

    @Autowired
    @Lazy
    private Account_tax_groupMapping account_tax_groupMapping;




    @PreAuthorize("hasPermission(#account_tax_group_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_tax_group" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_tax_groups/{account_tax_group_id}")

    public ResponseEntity<Account_tax_groupDTO> update(@PathVariable("account_tax_group_id") Integer account_tax_group_id, @RequestBody Account_tax_groupDTO account_tax_groupdto) {
		Account_tax_group domain = account_tax_groupMapping.toDomain(account_tax_groupdto);
        domain.setId(account_tax_group_id);
		account_tax_groupService.update(domain);
		Account_tax_groupDTO dto = account_tax_groupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_tax_group_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_tax_group" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_tax_groups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_tax_groupDTO> account_tax_groupdtos) {
        account_tax_groupService.updateBatch(account_tax_groupMapping.toDomain(account_tax_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_tax_group_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_tax_group" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_tax_groups/{account_tax_group_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_tax_group_id") Integer account_tax_group_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_tax_groupService.remove(account_tax_group_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_tax_group" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_tax_groups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_tax_groupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_tax_group_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_tax_group" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_tax_groups/{account_tax_group_id}")
    public ResponseEntity<Account_tax_groupDTO> get(@PathVariable("account_tax_group_id") Integer account_tax_group_id) {
        Account_tax_group domain = account_tax_groupService.get(account_tax_group_id);
        Account_tax_groupDTO dto = account_tax_groupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_tax_group" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_tax_groups")

    public ResponseEntity<Account_tax_groupDTO> create(@RequestBody Account_tax_groupDTO account_tax_groupdto) {
        Account_tax_group domain = account_tax_groupMapping.toDomain(account_tax_groupdto);
		account_tax_groupService.create(domain);
        Account_tax_groupDTO dto = account_tax_groupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_tax_group" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_tax_groups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_tax_groupDTO> account_tax_groupdtos) {
        account_tax_groupService.createBatch(account_tax_groupMapping.toDomain(account_tax_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_tax_group" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_tax_groups/fetchdefault")
	public ResponseEntity<List<Account_tax_groupDTO>> fetchDefault(Account_tax_groupSearchContext context) {
        Page<Account_tax_group> domains = account_tax_groupService.searchDefault(context) ;
        List<Account_tax_groupDTO> list = account_tax_groupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_tax_group" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_tax_groups/searchdefault")
	public ResponseEntity<Page<Account_tax_groupDTO>> searchDefault(Account_tax_groupSearchContext context) {
        Page<Account_tax_group> domains = account_tax_groupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_tax_groupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_tax_group getEntity(){
        return new Account_tax_group();
    }

}
