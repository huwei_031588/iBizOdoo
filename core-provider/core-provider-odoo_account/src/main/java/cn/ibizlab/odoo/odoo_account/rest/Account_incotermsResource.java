package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_incotermsService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_incotermsSearchContext;




@Slf4j
@Api(tags = {"Account_incoterms" })
@RestController("odoo_account-account_incoterms")
@RequestMapping("")
public class Account_incotermsResource {

    @Autowired
    private IAccount_incotermsService account_incotermsService;

    @Autowired
    @Lazy
    private Account_incotermsMapping account_incotermsMapping;







    @PreAuthorize("hasPermission(#account_incoterms_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_incoterms" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_incoterms/{account_incoterms_id}")
    public ResponseEntity<Account_incotermsDTO> get(@PathVariable("account_incoterms_id") Integer account_incoterms_id) {
        Account_incoterms domain = account_incotermsService.get(account_incoterms_id);
        Account_incotermsDTO dto = account_incotermsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_incoterms" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_incoterms")

    public ResponseEntity<Account_incotermsDTO> create(@RequestBody Account_incotermsDTO account_incotermsdto) {
        Account_incoterms domain = account_incotermsMapping.toDomain(account_incotermsdto);
		account_incotermsService.create(domain);
        Account_incotermsDTO dto = account_incotermsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_incoterms" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_incoterms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_incotermsDTO> account_incotermsdtos) {
        account_incotermsService.createBatch(account_incotermsMapping.toDomain(account_incotermsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#account_incoterms_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_incoterms" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_incoterms/{account_incoterms_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_incoterms_id") Integer account_incoterms_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_incotermsService.remove(account_incoterms_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_incoterms" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_incoterms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_incotermsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_incoterms_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_incoterms" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_incoterms/{account_incoterms_id}")

    public ResponseEntity<Account_incotermsDTO> update(@PathVariable("account_incoterms_id") Integer account_incoterms_id, @RequestBody Account_incotermsDTO account_incotermsdto) {
		Account_incoterms domain = account_incotermsMapping.toDomain(account_incotermsdto);
        domain.setId(account_incoterms_id);
		account_incotermsService.update(domain);
		Account_incotermsDTO dto = account_incotermsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_incoterms_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_incoterms" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_incoterms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_incotermsDTO> account_incotermsdtos) {
        account_incotermsService.updateBatch(account_incotermsMapping.toDomain(account_incotermsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_incoterms" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_incoterms/fetchdefault")
	public ResponseEntity<List<Account_incotermsDTO>> fetchDefault(Account_incotermsSearchContext context) {
        Page<Account_incoterms> domains = account_incotermsService.searchDefault(context) ;
        List<Account_incotermsDTO> list = account_incotermsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_incoterms" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_incoterms/searchdefault")
	public ResponseEntity<Page<Account_incotermsDTO>> searchDefault(Account_incotermsSearchContext context) {
        Page<Account_incoterms> domains = account_incotermsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_incotermsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_incoterms getEntity(){
        return new Account_incoterms();
    }

}
