package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_refund;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_refundService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_refundSearchContext;




@Slf4j
@Api(tags = {"Account_invoice_refund" })
@RestController("odoo_account-account_invoice_refund")
@RequestMapping("")
public class Account_invoice_refundResource {

    @Autowired
    private IAccount_invoice_refundService account_invoice_refundService;

    @Autowired
    @Lazy
    private Account_invoice_refundMapping account_invoice_refundMapping;




    @PreAuthorize("hasPermission(#account_invoice_refund_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_invoice_refund" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_refunds/{account_invoice_refund_id}")
    public ResponseEntity<Account_invoice_refundDTO> get(@PathVariable("account_invoice_refund_id") Integer account_invoice_refund_id) {
        Account_invoice_refund domain = account_invoice_refundService.get(account_invoice_refund_id);
        Account_invoice_refundDTO dto = account_invoice_refundMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_invoice_refund" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds")

    public ResponseEntity<Account_invoice_refundDTO> create(@RequestBody Account_invoice_refundDTO account_invoice_refunddto) {
        Account_invoice_refund domain = account_invoice_refundMapping.toDomain(account_invoice_refunddto);
		account_invoice_refundService.create(domain);
        Account_invoice_refundDTO dto = account_invoice_refundMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_invoice_refund" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_refundDTO> account_invoice_refunddtos) {
        account_invoice_refundService.createBatch(account_invoice_refundMapping.toDomain(account_invoice_refunddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_invoice_refund_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_invoice_refund" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_refunds/{account_invoice_refund_id}")

    public ResponseEntity<Account_invoice_refundDTO> update(@PathVariable("account_invoice_refund_id") Integer account_invoice_refund_id, @RequestBody Account_invoice_refundDTO account_invoice_refunddto) {
		Account_invoice_refund domain = account_invoice_refundMapping.toDomain(account_invoice_refunddto);
        domain.setId(account_invoice_refund_id);
		account_invoice_refundService.update(domain);
		Account_invoice_refundDTO dto = account_invoice_refundMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_invoice_refund_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_invoice_refund" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_refunds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_refundDTO> account_invoice_refunddtos) {
        account_invoice_refundService.updateBatch(account_invoice_refundMapping.toDomain(account_invoice_refunddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_invoice_refund_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_invoice_refund" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_refunds/{account_invoice_refund_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_refund_id") Integer account_invoice_refund_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_refundService.remove(account_invoice_refund_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_invoice_refund" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_refunds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_invoice_refundService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_invoice_refund" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_refunds/fetchdefault")
	public ResponseEntity<List<Account_invoice_refundDTO>> fetchDefault(Account_invoice_refundSearchContext context) {
        Page<Account_invoice_refund> domains = account_invoice_refundService.searchDefault(context) ;
        List<Account_invoice_refundDTO> list = account_invoice_refundMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_invoice_refund" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_refunds/searchdefault")
	public ResponseEntity<Page<Account_invoice_refundDTO>> searchDefault(Account_invoice_refundSearchContext context) {
        Page<Account_invoice_refund> domains = account_invoice_refundService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_refundMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_invoice_refund getEntity(){
        return new Account_invoice_refund();
    }

}
