package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_importService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_importSearchContext;




@Slf4j
@Api(tags = {"Account_bank_statement_import" })
@RestController("odoo_account-account_bank_statement_import")
@RequestMapping("")
public class Account_bank_statement_importResource {

    @Autowired
    private IAccount_bank_statement_importService account_bank_statement_importService;

    @Autowired
    @Lazy
    private Account_bank_statement_importMapping account_bank_statement_importMapping;




    @PreAuthorize("hasPermission('Remove',{#account_bank_statement_import_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_bank_statement_import" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_imports/{account_bank_statement_import_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_import_id") Integer account_bank_statement_import_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_importService.remove(account_bank_statement_import_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_bank_statement_import" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_imports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_bank_statement_importService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_bank_statement_import_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_bank_statement_import" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_imports/{account_bank_statement_import_id}")
    public ResponseEntity<Account_bank_statement_importDTO> get(@PathVariable("account_bank_statement_import_id") Integer account_bank_statement_import_id) {
        Account_bank_statement_import domain = account_bank_statement_importService.get(account_bank_statement_import_id);
        Account_bank_statement_importDTO dto = account_bank_statement_importMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_bank_statement_import" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_imports")

    public ResponseEntity<Account_bank_statement_importDTO> create(@RequestBody Account_bank_statement_importDTO account_bank_statement_importdto) {
        Account_bank_statement_import domain = account_bank_statement_importMapping.toDomain(account_bank_statement_importdto);
		account_bank_statement_importService.create(domain);
        Account_bank_statement_importDTO dto = account_bank_statement_importMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_bank_statement_import" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_imports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statement_importDTO> account_bank_statement_importdtos) {
        account_bank_statement_importService.createBatch(account_bank_statement_importMapping.toDomain(account_bank_statement_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_bank_statement_import_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_bank_statement_import" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_imports/{account_bank_statement_import_id}")

    public ResponseEntity<Account_bank_statement_importDTO> update(@PathVariable("account_bank_statement_import_id") Integer account_bank_statement_import_id, @RequestBody Account_bank_statement_importDTO account_bank_statement_importdto) {
		Account_bank_statement_import domain = account_bank_statement_importMapping.toDomain(account_bank_statement_importdto);
        domain.setId(account_bank_statement_import_id);
		account_bank_statement_importService.update(domain);
		Account_bank_statement_importDTO dto = account_bank_statement_importMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_bank_statement_import_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_bank_statement_import" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_imports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_importDTO> account_bank_statement_importdtos) {
        account_bank_statement_importService.updateBatch(account_bank_statement_importMapping.toDomain(account_bank_statement_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_bank_statement_import" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_imports/fetchdefault")
	public ResponseEntity<List<Account_bank_statement_importDTO>> fetchDefault(Account_bank_statement_importSearchContext context) {
        Page<Account_bank_statement_import> domains = account_bank_statement_importService.searchDefault(context) ;
        List<Account_bank_statement_importDTO> list = account_bank_statement_importMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_bank_statement_import" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_imports/searchdefault")
	public ResponseEntity<Page<Account_bank_statement_importDTO>> searchDefault(Account_bank_statement_importSearchContext context) {
        Page<Account_bank_statement_import> domains = account_bank_statement_importService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statement_importMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_bank_statement_import getEntity(){
        return new Account_bank_statement_import();
    }

}
