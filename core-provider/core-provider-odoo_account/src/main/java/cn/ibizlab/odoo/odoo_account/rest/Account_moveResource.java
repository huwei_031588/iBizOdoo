package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_moveService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_moveSearchContext;




@Slf4j
@Api(tags = {"Account_move" })
@RestController("odoo_account-account_move")
@RequestMapping("")
public class Account_moveResource {

    @Autowired
    private IAccount_moveService account_moveService;

    @Autowired
    @Lazy
    private Account_moveMapping account_moveMapping;




    @PreAuthorize("hasPermission(#account_move_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_move" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_moves/{account_move_id}")

    public ResponseEntity<Account_moveDTO> update(@PathVariable("account_move_id") Integer account_move_id, @RequestBody Account_moveDTO account_movedto) {
		Account_move domain = account_moveMapping.toDomain(account_movedto);
        domain.setId(account_move_id);
		account_moveService.update(domain);
		Account_moveDTO dto = account_moveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_move_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_move" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_moves/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_moveDTO> account_movedtos) {
        account_moveService.updateBatch(account_moveMapping.toDomain(account_movedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_move_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_move" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_moves/{account_move_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_move_id") Integer account_move_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_moveService.remove(account_move_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_move" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_moves/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_moveService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission(#account_move_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_move" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_moves/{account_move_id}")
    public ResponseEntity<Account_moveDTO> get(@PathVariable("account_move_id") Integer account_move_id) {
        Account_move domain = account_moveService.get(account_move_id);
        Account_moveDTO dto = account_moveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_move" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_moves")

    public ResponseEntity<Account_moveDTO> create(@RequestBody Account_moveDTO account_movedto) {
        Account_move domain = account_moveMapping.toDomain(account_movedto);
		account_moveService.create(domain);
        Account_moveDTO dto = account_moveMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_move" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_moves/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_moveDTO> account_movedtos) {
        account_moveService.createBatch(account_moveMapping.toDomain(account_movedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_move" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_moves/fetchdefault")
	public ResponseEntity<List<Account_moveDTO>> fetchDefault(Account_moveSearchContext context) {
        Page<Account_move> domains = account_moveService.searchDefault(context) ;
        List<Account_moveDTO> list = account_moveMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_move" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_moves/searchdefault")
	public ResponseEntity<Page<Account_moveDTO>> searchDefault(Account_moveSearchContext context) {
        Page<Account_move> domains = account_moveService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_moveMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_move getEntity(){
        return new Account_move();
    }

}
