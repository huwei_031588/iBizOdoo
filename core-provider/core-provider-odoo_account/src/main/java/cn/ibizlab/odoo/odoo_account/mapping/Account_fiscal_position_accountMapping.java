package cn.ibizlab.odoo.odoo_account.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_account;
import cn.ibizlab.odoo.odoo_account.dto.Account_fiscal_position_accountDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_fiscal_position_accountMapping extends MappingBase<Account_fiscal_position_accountDTO, Account_fiscal_position_account> {


}

