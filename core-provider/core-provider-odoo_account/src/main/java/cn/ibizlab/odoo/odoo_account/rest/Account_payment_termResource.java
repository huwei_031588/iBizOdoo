package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_termService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_termSearchContext;




@Slf4j
@Api(tags = {"Account_payment_term" })
@RestController("odoo_account-account_payment_term")
@RequestMapping("")
public class Account_payment_termResource {

    @Autowired
    private IAccount_payment_termService account_payment_termService;

    @Autowired
    @Lazy
    private Account_payment_termMapping account_payment_termMapping;










    @PreAuthorize("hasPermission('Remove',{#account_payment_term_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_payment_term" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_terms/{account_payment_term_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_term_id") Integer account_payment_term_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_payment_termService.remove(account_payment_term_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_payment_term" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_terms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_payment_termService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_payment_term" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_terms")

    public ResponseEntity<Account_payment_termDTO> create(@RequestBody Account_payment_termDTO account_payment_termdto) {
        Account_payment_term domain = account_payment_termMapping.toDomain(account_payment_termdto);
		account_payment_termService.create(domain);
        Account_payment_termDTO dto = account_payment_termMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_payment_term" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_terms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_payment_termDTO> account_payment_termdtos) {
        account_payment_termService.createBatch(account_payment_termMapping.toDomain(account_payment_termdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_payment_term_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_payment_term" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payment_terms/{account_payment_term_id}")
    public ResponseEntity<Account_payment_termDTO> get(@PathVariable("account_payment_term_id") Integer account_payment_term_id) {
        Account_payment_term domain = account_payment_termService.get(account_payment_term_id);
        Account_payment_termDTO dto = account_payment_termMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#account_payment_term_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_payment_term" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_terms/{account_payment_term_id}")

    public ResponseEntity<Account_payment_termDTO> update(@PathVariable("account_payment_term_id") Integer account_payment_term_id, @RequestBody Account_payment_termDTO account_payment_termdto) {
		Account_payment_term domain = account_payment_termMapping.toDomain(account_payment_termdto);
        domain.setId(account_payment_term_id);
		account_payment_termService.update(domain);
		Account_payment_termDTO dto = account_payment_termMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_payment_term_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_payment_term" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_terms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_payment_termDTO> account_payment_termdtos) {
        account_payment_termService.updateBatch(account_payment_termMapping.toDomain(account_payment_termdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_payment_term" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_payment_terms/fetchdefault")
	public ResponseEntity<List<Account_payment_termDTO>> fetchDefault(Account_payment_termSearchContext context) {
        Page<Account_payment_term> domains = account_payment_termService.searchDefault(context) ;
        List<Account_payment_termDTO> list = account_payment_termMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_payment_term" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_payment_terms/searchdefault")
	public ResponseEntity<Page<Account_payment_termDTO>> searchDefault(Account_payment_termSearchContext context) {
        Page<Account_payment_term> domains = account_payment_termService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_payment_termMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_payment_term getEntity(){
        return new Account_payment_term();
    }

}
