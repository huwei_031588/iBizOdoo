package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_year;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_yearService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_yearSearchContext;




@Slf4j
@Api(tags = {"Account_fiscal_year" })
@RestController("odoo_account-account_fiscal_year")
@RequestMapping("")
public class Account_fiscal_yearResource {

    @Autowired
    private IAccount_fiscal_yearService account_fiscal_yearService;

    @Autowired
    @Lazy
    private Account_fiscal_yearMapping account_fiscal_yearMapping;







    @PreAuthorize("hasPermission('Remove',{#account_fiscal_year_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_fiscal_year" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_years/{account_fiscal_year_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_fiscal_year_id") Integer account_fiscal_year_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_yearService.remove(account_fiscal_year_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_fiscal_year" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_years/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_fiscal_yearService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_fiscal_year_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_fiscal_year" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_years/{account_fiscal_year_id}")

    public ResponseEntity<Account_fiscal_yearDTO> update(@PathVariable("account_fiscal_year_id") Integer account_fiscal_year_id, @RequestBody Account_fiscal_yearDTO account_fiscal_yeardto) {
		Account_fiscal_year domain = account_fiscal_yearMapping.toDomain(account_fiscal_yeardto);
        domain.setId(account_fiscal_year_id);
		account_fiscal_yearService.update(domain);
		Account_fiscal_yearDTO dto = account_fiscal_yearMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_fiscal_year_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_fiscal_year" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_years/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_fiscal_yearDTO> account_fiscal_yeardtos) {
        account_fiscal_yearService.updateBatch(account_fiscal_yearMapping.toDomain(account_fiscal_yeardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_fiscal_year_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_fiscal_year" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_years/{account_fiscal_year_id}")
    public ResponseEntity<Account_fiscal_yearDTO> get(@PathVariable("account_fiscal_year_id") Integer account_fiscal_year_id) {
        Account_fiscal_year domain = account_fiscal_yearService.get(account_fiscal_year_id);
        Account_fiscal_yearDTO dto = account_fiscal_yearMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_fiscal_year" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_years")

    public ResponseEntity<Account_fiscal_yearDTO> create(@RequestBody Account_fiscal_yearDTO account_fiscal_yeardto) {
        Account_fiscal_year domain = account_fiscal_yearMapping.toDomain(account_fiscal_yeardto);
		account_fiscal_yearService.create(domain);
        Account_fiscal_yearDTO dto = account_fiscal_yearMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_fiscal_year" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_years/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_fiscal_yearDTO> account_fiscal_yeardtos) {
        account_fiscal_yearService.createBatch(account_fiscal_yearMapping.toDomain(account_fiscal_yeardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_fiscal_year" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_fiscal_years/fetchdefault")
	public ResponseEntity<List<Account_fiscal_yearDTO>> fetchDefault(Account_fiscal_yearSearchContext context) {
        Page<Account_fiscal_year> domains = account_fiscal_yearService.searchDefault(context) ;
        List<Account_fiscal_yearDTO> list = account_fiscal_yearMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_fiscal_year" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_fiscal_years/searchdefault")
	public ResponseEntity<Page<Account_fiscal_yearDTO>> searchDefault(Account_fiscal_yearSearchContext context) {
        Page<Account_fiscal_year> domains = account_fiscal_yearService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_fiscal_yearMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_fiscal_year getEntity(){
        return new Account_fiscal_year();
    }

}
