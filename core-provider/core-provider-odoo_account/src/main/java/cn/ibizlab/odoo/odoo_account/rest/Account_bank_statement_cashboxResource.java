package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_cashboxService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_cashboxSearchContext;




@Slf4j
@Api(tags = {"Account_bank_statement_cashbox" })
@RestController("odoo_account-account_bank_statement_cashbox")
@RequestMapping("")
public class Account_bank_statement_cashboxResource {

    @Autowired
    private IAccount_bank_statement_cashboxService account_bank_statement_cashboxService;

    @Autowired
    @Lazy
    private Account_bank_statement_cashboxMapping account_bank_statement_cashboxMapping;




    @PreAuthorize("hasPermission('Remove',{#account_bank_statement_cashbox_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_bank_statement_cashbox" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_cashboxes/{account_bank_statement_cashbox_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_cashbox_id") Integer account_bank_statement_cashbox_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_cashboxService.remove(account_bank_statement_cashbox_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_bank_statement_cashbox" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_cashboxes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_bank_statement_cashboxService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_bank_statement_cashbox_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_bank_statement_cashbox" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_cashboxes/{account_bank_statement_cashbox_id}")

    public ResponseEntity<Account_bank_statement_cashboxDTO> update(@PathVariable("account_bank_statement_cashbox_id") Integer account_bank_statement_cashbox_id, @RequestBody Account_bank_statement_cashboxDTO account_bank_statement_cashboxdto) {
		Account_bank_statement_cashbox domain = account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdto);
        domain.setId(account_bank_statement_cashbox_id);
		account_bank_statement_cashboxService.update(domain);
		Account_bank_statement_cashboxDTO dto = account_bank_statement_cashboxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_bank_statement_cashbox_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_bank_statement_cashbox" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_cashboxes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_cashboxDTO> account_bank_statement_cashboxdtos) {
        account_bank_statement_cashboxService.updateBatch(account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_bank_statement_cashbox" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_cashboxes")

    public ResponseEntity<Account_bank_statement_cashboxDTO> create(@RequestBody Account_bank_statement_cashboxDTO account_bank_statement_cashboxdto) {
        Account_bank_statement_cashbox domain = account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdto);
		account_bank_statement_cashboxService.create(domain);
        Account_bank_statement_cashboxDTO dto = account_bank_statement_cashboxMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_bank_statement_cashbox" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_cashboxes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statement_cashboxDTO> account_bank_statement_cashboxdtos) {
        account_bank_statement_cashboxService.createBatch(account_bank_statement_cashboxMapping.toDomain(account_bank_statement_cashboxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_bank_statement_cashbox_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_bank_statement_cashbox" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_cashboxes/{account_bank_statement_cashbox_id}")
    public ResponseEntity<Account_bank_statement_cashboxDTO> get(@PathVariable("account_bank_statement_cashbox_id") Integer account_bank_statement_cashbox_id) {
        Account_bank_statement_cashbox domain = account_bank_statement_cashboxService.get(account_bank_statement_cashbox_id);
        Account_bank_statement_cashboxDTO dto = account_bank_statement_cashboxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_bank_statement_cashbox" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_cashboxes/fetchdefault")
	public ResponseEntity<List<Account_bank_statement_cashboxDTO>> fetchDefault(Account_bank_statement_cashboxSearchContext context) {
        Page<Account_bank_statement_cashbox> domains = account_bank_statement_cashboxService.searchDefault(context) ;
        List<Account_bank_statement_cashboxDTO> list = account_bank_statement_cashboxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_bank_statement_cashbox" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_cashboxes/searchdefault")
	public ResponseEntity<Page<Account_bank_statement_cashboxDTO>> searchDefault(Account_bank_statement_cashboxSearchContext context) {
        Page<Account_bank_statement_cashbox> domains = account_bank_statement_cashboxService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statement_cashboxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_bank_statement_cashbox getEntity(){
        return new Account_bank_statement_cashbox();
    }

}
