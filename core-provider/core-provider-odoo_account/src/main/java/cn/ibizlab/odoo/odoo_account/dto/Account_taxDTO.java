package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_taxDTO]
 */
@Data
public class Account_taxDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [TYPE_TAX_USE]
     *
     */
    @JSONField(name = "type_tax_use")
    @JsonProperty("type_tax_use")
    private String typeTaxUse;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [CHILDREN_TAX_IDS]
     *
     */
    @JSONField(name = "children_tax_ids")
    @JsonProperty("children_tax_ids")
    private String childrenTaxIds;

    /**
     * 属性 [ANALYTIC]
     *
     */
    @JSONField(name = "analytic")
    @JsonProperty("analytic")
    private String analytic;

    /**
     * 属性 [PRICE_INCLUDE]
     *
     */
    @JSONField(name = "price_include")
    @JsonProperty("price_include")
    private String priceInclude;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [TAX_EXIGIBILITY]
     *
     */
    @JSONField(name = "tax_exigibility")
    @JsonProperty("tax_exigibility")
    private String taxExigibility;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;

    /**
     * 属性 [AMOUNT_TYPE]
     *
     */
    @JSONField(name = "amount_type")
    @JsonProperty("amount_type")
    private String amountType;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [INCLUDE_BASE_AMOUNT]
     *
     */
    @JSONField(name = "include_base_amount")
    @JsonProperty("include_base_amount")
    private String includeBaseAmount;

    /**
     * 属性 [CASH_BASIS_BASE_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "cash_basis_base_account_id_text")
    @JsonProperty("cash_basis_base_account_id_text")
    private String cashBasisBaseAccountIdText;

    /**
     * 属性 [CASH_BASIS_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "cash_basis_account_id_text")
    @JsonProperty("cash_basis_account_id_text")
    private String cashBasisAccountIdText;

    /**
     * 属性 [HIDE_TAX_EXIGIBILITY]
     *
     */
    @JSONField(name = "hide_tax_exigibility")
    @JsonProperty("hide_tax_exigibility")
    private String hideTaxExigibility;

    /**
     * 属性 [TAX_GROUP_ID_TEXT]
     *
     */
    @JSONField(name = "tax_group_id_text")
    @JsonProperty("tax_group_id_text")
    private String taxGroupIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [REFUND_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "refund_account_id_text")
    @JsonProperty("refund_account_id_text")
    private String refundAccountIdText;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [REFUND_ACCOUNT_ID]
     *
     */
    @JSONField(name = "refund_account_id")
    @JsonProperty("refund_account_id")
    private Integer refundAccountId;

    /**
     * 属性 [CASH_BASIS_ACCOUNT_ID]
     *
     */
    @JSONField(name = "cash_basis_account_id")
    @JsonProperty("cash_basis_account_id")
    private Integer cashBasisAccountId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 属性 [CASH_BASIS_BASE_ACCOUNT_ID]
     *
     */
    @JSONField(name = "cash_basis_base_account_id")
    @JsonProperty("cash_basis_base_account_id")
    private Integer cashBasisBaseAccountId;

    /**
     * 属性 [TAX_GROUP_ID]
     *
     */
    @JSONField(name = "tax_group_id")
    @JsonProperty("tax_group_id")
    private Integer taxGroupId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;


    /**
     * 设置 [TYPE_TAX_USE]
     */
    public void setTypeTaxUse(String  typeTaxUse){
        this.typeTaxUse = typeTaxUse ;
        this.modify("type_tax_use",typeTaxUse);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [ANALYTIC]
     */
    public void setAnalytic(String  analytic){
        this.analytic = analytic ;
        this.modify("analytic",analytic);
    }

    /**
     * 设置 [PRICE_INCLUDE]
     */
    public void setPriceInclude(String  priceInclude){
        this.priceInclude = priceInclude ;
        this.modify("price_include",priceInclude);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [TAX_EXIGIBILITY]
     */
    public void setTaxExigibility(String  taxExigibility){
        this.taxExigibility = taxExigibility ;
        this.modify("tax_exigibility",taxExigibility);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [AMOUNT_TYPE]
     */
    public void setAmountType(String  amountType){
        this.amountType = amountType ;
        this.modify("amount_type",amountType);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [INCLUDE_BASE_AMOUNT]
     */
    public void setIncludeBaseAmount(String  includeBaseAmount){
        this.includeBaseAmount = includeBaseAmount ;
        this.modify("include_base_amount",includeBaseAmount);
    }

    /**
     * 设置 [REFUND_ACCOUNT_ID]
     */
    public void setRefundAccountId(Integer  refundAccountId){
        this.refundAccountId = refundAccountId ;
        this.modify("refund_account_id",refundAccountId);
    }

    /**
     * 设置 [CASH_BASIS_ACCOUNT_ID]
     */
    public void setCashBasisAccountId(Integer  cashBasisAccountId){
        this.cashBasisAccountId = cashBasisAccountId ;
        this.modify("cash_basis_account_id",cashBasisAccountId);
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    public void setAccountId(Integer  accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [CASH_BASIS_BASE_ACCOUNT_ID]
     */
    public void setCashBasisBaseAccountId(Integer  cashBasisBaseAccountId){
        this.cashBasisBaseAccountId = cashBasisBaseAccountId ;
        this.modify("cash_basis_base_account_id",cashBasisBaseAccountId);
    }

    /**
     * 设置 [TAX_GROUP_ID]
     */
    public void setTaxGroupId(Integer  taxGroupId){
        this.taxGroupId = taxGroupId ;
        this.modify("tax_group_id",taxGroupId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }


}

