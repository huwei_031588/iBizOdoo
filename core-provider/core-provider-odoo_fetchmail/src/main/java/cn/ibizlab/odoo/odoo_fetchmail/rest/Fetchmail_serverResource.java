package cn.ibizlab.odoo.odoo_fetchmail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fetchmail.dto.*;
import cn.ibizlab.odoo.odoo_fetchmail.mapping.*;
import cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.odoo.core.odoo_fetchmail.service.IFetchmail_serverService;
import cn.ibizlab.odoo.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;




@Slf4j
@Api(tags = {"Fetchmail_server" })
@RestController("odoo_fetchmail-fetchmail_server")
@RequestMapping("")
public class Fetchmail_serverResource {

    @Autowired
    private IFetchmail_serverService fetchmail_serverService;

    @Autowired
    @Lazy
    private Fetchmail_serverMapping fetchmail_serverMapping;










    @PreAuthorize("hasPermission(#fetchmail_server_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fetchmail_server" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fetchmail_servers/{fetchmail_server_id}")
    public ResponseEntity<Fetchmail_serverDTO> get(@PathVariable("fetchmail_server_id") Integer fetchmail_server_id) {
        Fetchmail_server domain = fetchmail_serverService.get(fetchmail_server_id);
        Fetchmail_serverDTO dto = fetchmail_serverMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#fetchmail_server_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fetchmail_server" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fetchmail_servers/{fetchmail_server_id}")

    public ResponseEntity<Fetchmail_serverDTO> update(@PathVariable("fetchmail_server_id") Integer fetchmail_server_id, @RequestBody Fetchmail_serverDTO fetchmail_serverdto) {
		Fetchmail_server domain = fetchmail_serverMapping.toDomain(fetchmail_serverdto);
        domain.setId(fetchmail_server_id);
		fetchmail_serverService.update(domain);
		Fetchmail_serverDTO dto = fetchmail_serverMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fetchmail_server_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fetchmail_server" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fetchmail_servers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fetchmail_serverDTO> fetchmail_serverdtos) {
        fetchmail_serverService.updateBatch(fetchmail_serverMapping.toDomain(fetchmail_serverdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fetchmail_server" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fetchmail_servers")

    public ResponseEntity<Fetchmail_serverDTO> create(@RequestBody Fetchmail_serverDTO fetchmail_serverdto) {
        Fetchmail_server domain = fetchmail_serverMapping.toDomain(fetchmail_serverdto);
		fetchmail_serverService.create(domain);
        Fetchmail_serverDTO dto = fetchmail_serverMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fetchmail_server" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fetchmail_servers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fetchmail_serverDTO> fetchmail_serverdtos) {
        fetchmail_serverService.createBatch(fetchmail_serverMapping.toDomain(fetchmail_serverdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#fetchmail_server_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fetchmail_server" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fetchmail_servers/{fetchmail_server_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fetchmail_server_id") Integer fetchmail_server_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fetchmail_serverService.remove(fetchmail_server_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fetchmail_server" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fetchmail_servers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fetchmail_serverService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fetchmail_server" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fetchmail_servers/fetchdefault")
	public ResponseEntity<List<Fetchmail_serverDTO>> fetchDefault(Fetchmail_serverSearchContext context) {
        Page<Fetchmail_server> domains = fetchmail_serverService.searchDefault(context) ;
        List<Fetchmail_serverDTO> list = fetchmail_serverMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fetchmail_server" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fetchmail_servers/searchdefault")
	public ResponseEntity<Page<Fetchmail_serverDTO>> searchDefault(Fetchmail_serverSearchContext context) {
        Page<Fetchmail_server> domains = fetchmail_serverService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fetchmail_serverMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fetchmail_server getEntity(){
        return new Fetchmail_server();
    }

}
