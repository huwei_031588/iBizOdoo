package cn.ibizlab.odoo.odoo_fetchmail.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_fetchmail")
public class odoo_fetchmailRestConfiguration {

}
