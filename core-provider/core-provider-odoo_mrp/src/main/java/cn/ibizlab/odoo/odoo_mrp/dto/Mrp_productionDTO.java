package cn.ibizlab.odoo.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mrp_productionDTO]
 */
@Data
public class Mrp_productionDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 属性 [WORKORDER_DONE_COUNT]
     *
     */
    @JSONField(name = "workorder_done_count")
    @JsonProperty("workorder_done_count")
    private Integer workorderDoneCount;

    /**
     * 属性 [FINISHED_MOVE_LINE_IDS]
     *
     */
    @JSONField(name = "finished_move_line_ids")
    @JsonProperty("finished_move_line_ids")
    private String finishedMoveLineIds;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [QTY_PRODUCED]
     *
     */
    @JSONField(name = "qty_produced")
    @JsonProperty("qty_produced")
    private Double qtyProduced;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [UNRESERVE_VISIBLE]
     *
     */
    @JSONField(name = "unreserve_visible")
    @JsonProperty("unreserve_visible")
    private String unreserveVisible;

    /**
     * 属性 [HAS_MOVES]
     *
     */
    @JSONField(name = "has_moves")
    @JsonProperty("has_moves")
    private String hasMoves;

    /**
     * 属性 [IS_LOCKED]
     *
     */
    @JSONField(name = "is_locked")
    @JsonProperty("is_locked")
    private String isLocked;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [DATE_START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_start")
    private Timestamp dateStart;

    /**
     * 属性 [MOVE_FINISHED_IDS]
     *
     */
    @JSONField(name = "move_finished_ids")
    @JsonProperty("move_finished_ids")
    private String moveFinishedIds;

    /**
     * 属性 [SCRAP_COUNT]
     *
     */
    @JSONField(name = "scrap_count")
    @JsonProperty("scrap_count")
    private Integer scrapCount;

    /**
     * 属性 [SCRAP_IDS]
     *
     */
    @JSONField(name = "scrap_ids")
    @JsonProperty("scrap_ids")
    private String scrapIds;

    /**
     * 属性 [SHOW_FINAL_LOTS]
     *
     */
    @JSONField(name = "show_final_lots")
    @JsonProperty("show_final_lots")
    private String showFinalLots;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [MOVE_DEST_IDS]
     *
     */
    @JSONField(name = "move_dest_ids")
    @JsonProperty("move_dest_ids")
    private String moveDestIds;

    /**
     * 属性 [PROCUREMENT_GROUP_ID]
     *
     */
    @JSONField(name = "procurement_group_id")
    @JsonProperty("procurement_group_id")
    private Integer procurementGroupId;

    /**
     * 属性 [DELIVERY_COUNT]
     *
     */
    @JSONField(name = "delivery_count")
    @JsonProperty("delivery_count")
    private Integer deliveryCount;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 属性 [WORKORDER_COUNT]
     *
     */
    @JSONField(name = "workorder_count")
    @JsonProperty("workorder_count")
    private Integer workorderCount;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [DATE_PLANNED_FINISHED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned_finished" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned_finished")
    private Timestamp datePlannedFinished;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [ORIGIN]
     *
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [DATE_PLANNED_START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned_start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned_start")
    private Timestamp datePlannedStart;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [CHECK_TO_DONE]
     *
     */
    @JSONField(name = "check_to_done")
    @JsonProperty("check_to_done")
    private String checkToDone;

    /**
     * 属性 [POST_VISIBLE]
     *
     */
    @JSONField(name = "post_visible")
    @JsonProperty("post_visible")
    private String postVisible;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PICKING_IDS]
     *
     */
    @JSONField(name = "picking_ids")
    @JsonProperty("picking_ids")
    private String pickingIds;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [CONSUMED_LESS_THAN_PLANNED]
     *
     */
    @JSONField(name = "consumed_less_than_planned")
    @JsonProperty("consumed_less_than_planned")
    private String consumedLessThanPlanned;

    /**
     * 属性 [MOVE_RAW_IDS]
     *
     */
    @JSONField(name = "move_raw_ids")
    @JsonProperty("move_raw_ids")
    private String moveRawIds;

    /**
     * 属性 [AVAILABILITY]
     *
     */
    @JSONField(name = "availability")
    @JsonProperty("availability")
    private String availability;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 属性 [WORKORDER_IDS]
     *
     */
    @JSONField(name = "workorder_ids")
    @JsonProperty("workorder_ids")
    private String workorderIds;

    /**
     * 属性 [DATE_FINISHED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_finished" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_finished")
    private Timestamp dateFinished;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [PROPAGATE]
     *
     */
    @JSONField(name = "propagate")
    @JsonProperty("propagate")
    private String propagate;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    private String locationDestIdText;

    /**
     * 属性 [LOCATION_SRC_ID_TEXT]
     *
     */
    @JSONField(name = "location_src_id_text")
    @JsonProperty("location_src_id_text")
    private String locationSrcIdText;

    /**
     * 属性 [PRODUCTION_LOCATION_ID]
     *
     */
    @JSONField(name = "production_location_id")
    @JsonProperty("production_location_id")
    private Integer productionLocationId;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    private String pickingTypeIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Integer productTmplId;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [ROUTING_ID_TEXT]
     *
     */
    @JSONField(name = "routing_id_text")
    @JsonProperty("routing_id_text")
    private String routingIdText;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 属性 [ROUTING_ID]
     *
     */
    @JSONField(name = "routing_id")
    @JsonProperty("routing_id")
    private Integer routingId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [BOM_ID]
     *
     */
    @JSONField(name = "bom_id")
    @JsonProperty("bom_id")
    private Integer bomId;

    /**
     * 属性 [LOCATION_SRC_ID]
     *
     */
    @JSONField(name = "location_src_id")
    @JsonProperty("location_src_id")
    private Integer locationSrcId;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    private Integer pickingTypeId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    private Integer locationDestId;


    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(String  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [IS_LOCKED]
     */
    public void setIsLocked(String  isLocked){
        this.isLocked = isLocked ;
        this.modify("is_locked",isLocked);
    }

    /**
     * 设置 [DATE_START]
     */
    public void setDateStart(Timestamp  dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PROCUREMENT_GROUP_ID]
     */
    public void setProcurementGroupId(Integer  procurementGroupId){
        this.procurementGroupId = procurementGroupId ;
        this.modify("procurement_group_id",procurementGroupId);
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    public void setProductQty(Double  productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [DATE_PLANNED_FINISHED]
     */
    public void setDatePlannedFinished(Timestamp  datePlannedFinished){
        this.datePlannedFinished = datePlannedFinished ;
        this.modify("date_planned_finished",datePlannedFinished);
    }

    /**
     * 设置 [ORIGIN]
     */
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [DATE_PLANNED_START]
     */
    public void setDatePlannedStart(Timestamp  datePlannedStart){
        this.datePlannedStart = datePlannedStart ;
        this.modify("date_planned_start",datePlannedStart);
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    public void setProductUomQty(Double  productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [AVAILABILITY]
     */
    public void setAvailability(String  availability){
        this.availability = availability ;
        this.modify("availability",availability);
    }

    /**
     * 设置 [DATE_FINISHED]
     */
    public void setDateFinished(Timestamp  dateFinished){
        this.dateFinished = dateFinished ;
        this.modify("date_finished",dateFinished);
    }

    /**
     * 设置 [PROPAGATE]
     */
    public void setPropagate(String  propagate){
        this.propagate = propagate ;
        this.modify("propagate",propagate);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Integer  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Integer  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [ROUTING_ID]
     */
    public void setRoutingId(Integer  routingId){
        this.routingId = routingId ;
        this.modify("routing_id",routingId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Integer  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [BOM_ID]
     */
    public void setBomId(Integer  bomId){
        this.bomId = bomId ;
        this.modify("bom_id",bomId);
    }

    /**
     * 设置 [LOCATION_SRC_ID]
     */
    public void setLocationSrcId(Integer  locationSrcId){
        this.locationSrcId = locationSrcId ;
        this.modify("location_src_id",locationSrcId);
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    public void setPickingTypeId(Integer  pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    public void setLocationDestId(Integer  locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }


}

