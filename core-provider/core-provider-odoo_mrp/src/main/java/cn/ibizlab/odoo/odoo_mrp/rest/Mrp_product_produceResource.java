package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_product_produceService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produceSearchContext;




@Slf4j
@Api(tags = {"Mrp_product_produce" })
@RestController("odoo_mrp-mrp_product_produce")
@RequestMapping("")
public class Mrp_product_produceResource {

    @Autowired
    private IMrp_product_produceService mrp_product_produceService;

    @Autowired
    @Lazy
    private Mrp_product_produceMapping mrp_product_produceMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_product_produce" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produces")

    public ResponseEntity<Mrp_product_produceDTO> create(@RequestBody Mrp_product_produceDTO mrp_product_producedto) {
        Mrp_product_produce domain = mrp_product_produceMapping.toDomain(mrp_product_producedto);
		mrp_product_produceService.create(domain);
        Mrp_product_produceDTO dto = mrp_product_produceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_product_produce" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produces/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_product_produceDTO> mrp_product_producedtos) {
        mrp_product_produceService.createBatch(mrp_product_produceMapping.toDomain(mrp_product_producedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission(#mrp_product_produce_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_product_produce" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produces/{mrp_product_produce_id}")

    public ResponseEntity<Mrp_product_produceDTO> update(@PathVariable("mrp_product_produce_id") Integer mrp_product_produce_id, @RequestBody Mrp_product_produceDTO mrp_product_producedto) {
		Mrp_product_produce domain = mrp_product_produceMapping.toDomain(mrp_product_producedto);
        domain.setId(mrp_product_produce_id);
		mrp_product_produceService.update(domain);
		Mrp_product_produceDTO dto = mrp_product_produceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_product_produce_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_product_produce" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produces/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_product_produceDTO> mrp_product_producedtos) {
        mrp_product_produceService.updateBatch(mrp_product_produceMapping.toDomain(mrp_product_producedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mrp_product_produce_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_product_produce" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produces/{mrp_product_produce_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_product_produce_id") Integer mrp_product_produce_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_product_produceService.remove(mrp_product_produce_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_product_produce" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produces/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_product_produceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_product_produce_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_product_produce" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produces/{mrp_product_produce_id}")
    public ResponseEntity<Mrp_product_produceDTO> get(@PathVariable("mrp_product_produce_id") Integer mrp_product_produce_id) {
        Mrp_product_produce domain = mrp_product_produceService.get(mrp_product_produce_id);
        Mrp_product_produceDTO dto = mrp_product_produceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_product_produce" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_product_produces/fetchdefault")
	public ResponseEntity<List<Mrp_product_produceDTO>> fetchDefault(Mrp_product_produceSearchContext context) {
        Page<Mrp_product_produce> domains = mrp_product_produceService.searchDefault(context) ;
        List<Mrp_product_produceDTO> list = mrp_product_produceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_product_produce" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_product_produces/searchdefault")
	public ResponseEntity<Page<Mrp_product_produceDTO>> searchDefault(Mrp_product_produceSearchContext context) {
        Page<Mrp_product_produce> domains = mrp_product_produceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_product_produceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_product_produce getEntity(){
        return new Mrp_product_produce();
    }

}
