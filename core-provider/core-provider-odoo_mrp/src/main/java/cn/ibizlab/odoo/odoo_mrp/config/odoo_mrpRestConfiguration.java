package cn.ibizlab.odoo.odoo_mrp.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_mrp")
public class odoo_mrpRestConfiguration {

}
