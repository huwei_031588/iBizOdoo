package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_documentService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_documentSearchContext;




@Slf4j
@Api(tags = {"Mrp_document" })
@RestController("odoo_mrp-mrp_document")
@RequestMapping("")
public class Mrp_documentResource {

    @Autowired
    private IMrp_documentService mrp_documentService;

    @Autowired
    @Lazy
    private Mrp_documentMapping mrp_documentMapping;




    @PreAuthorize("hasPermission(#mrp_document_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_document" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_documents/{mrp_document_id}")

    public ResponseEntity<Mrp_documentDTO> update(@PathVariable("mrp_document_id") Integer mrp_document_id, @RequestBody Mrp_documentDTO mrp_documentdto) {
		Mrp_document domain = mrp_documentMapping.toDomain(mrp_documentdto);
        domain.setId(mrp_document_id);
		mrp_documentService.update(domain);
		Mrp_documentDTO dto = mrp_documentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_document_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_document" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_documents/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_documentDTO> mrp_documentdtos) {
        mrp_documentService.updateBatch(mrp_documentMapping.toDomain(mrp_documentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_document" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_documents")

    public ResponseEntity<Mrp_documentDTO> create(@RequestBody Mrp_documentDTO mrp_documentdto) {
        Mrp_document domain = mrp_documentMapping.toDomain(mrp_documentdto);
		mrp_documentService.create(domain);
        Mrp_documentDTO dto = mrp_documentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_document" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_documents/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_documentDTO> mrp_documentdtos) {
        mrp_documentService.createBatch(mrp_documentMapping.toDomain(mrp_documentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#mrp_document_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_document" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_documents/{mrp_document_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_document_id") Integer mrp_document_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_documentService.remove(mrp_document_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_document" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_documents/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_documentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_document_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_document" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_documents/{mrp_document_id}")
    public ResponseEntity<Mrp_documentDTO> get(@PathVariable("mrp_document_id") Integer mrp_document_id) {
        Mrp_document domain = mrp_documentService.get(mrp_document_id);
        Mrp_documentDTO dto = mrp_documentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_document" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_documents/fetchdefault")
	public ResponseEntity<List<Mrp_documentDTO>> fetchDefault(Mrp_documentSearchContext context) {
        Page<Mrp_document> domains = mrp_documentService.searchDefault(context) ;
        List<Mrp_documentDTO> list = mrp_documentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_document" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_documents/searchdefault")
	public ResponseEntity<Page<Mrp_documentDTO>> searchDefault(Mrp_documentSearchContext context) {
        Page<Mrp_document> domains = mrp_documentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_documentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_document getEntity(){
        return new Mrp_document();
    }

}
