package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivityService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;




@Slf4j
@Api(tags = {"Mrp_workcenter_productivity" })
@RestController("odoo_mrp-mrp_workcenter_productivity")
@RequestMapping("")
public class Mrp_workcenter_productivityResource {

    @Autowired
    private IMrp_workcenter_productivityService mrp_workcenter_productivityService;

    @Autowired
    @Lazy
    private Mrp_workcenter_productivityMapping mrp_workcenter_productivityMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_workcenter_productivity" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities")

    public ResponseEntity<Mrp_workcenter_productivityDTO> create(@RequestBody Mrp_workcenter_productivityDTO mrp_workcenter_productivitydto) {
        Mrp_workcenter_productivity domain = mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydto);
		mrp_workcenter_productivityService.create(domain);
        Mrp_workcenter_productivityDTO dto = mrp_workcenter_productivityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_workcenter_productivity" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivities/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_workcenter_productivityDTO> mrp_workcenter_productivitydtos) {
        mrp_workcenter_productivityService.createBatch(mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_workcenter_productivity_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_workcenter_productivity" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivities/{mrp_workcenter_productivity_id}")

    public ResponseEntity<Mrp_workcenter_productivityDTO> update(@PathVariable("mrp_workcenter_productivity_id") Integer mrp_workcenter_productivity_id, @RequestBody Mrp_workcenter_productivityDTO mrp_workcenter_productivitydto) {
		Mrp_workcenter_productivity domain = mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydto);
        domain.setId(mrp_workcenter_productivity_id);
		mrp_workcenter_productivityService.update(domain);
		Mrp_workcenter_productivityDTO dto = mrp_workcenter_productivityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_workcenter_productivity_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_workcenter_productivity" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivities/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenter_productivityDTO> mrp_workcenter_productivitydtos) {
        mrp_workcenter_productivityService.updateBatch(mrp_workcenter_productivityMapping.toDomain(mrp_workcenter_productivitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mrp_workcenter_productivity_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_workcenter_productivity" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivities/{mrp_workcenter_productivity_id}")
    public ResponseEntity<Mrp_workcenter_productivityDTO> get(@PathVariable("mrp_workcenter_productivity_id") Integer mrp_workcenter_productivity_id) {
        Mrp_workcenter_productivity domain = mrp_workcenter_productivityService.get(mrp_workcenter_productivity_id);
        Mrp_workcenter_productivityDTO dto = mrp_workcenter_productivityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#mrp_workcenter_productivity_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_workcenter_productivity" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivities/{mrp_workcenter_productivity_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_productivity_id") Integer mrp_workcenter_productivity_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivityService.remove(mrp_workcenter_productivity_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_workcenter_productivity" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivities/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_workcenter_productivityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_workcenter_productivity" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenter_productivities/fetchdefault")
	public ResponseEntity<List<Mrp_workcenter_productivityDTO>> fetchDefault(Mrp_workcenter_productivitySearchContext context) {
        Page<Mrp_workcenter_productivity> domains = mrp_workcenter_productivityService.searchDefault(context) ;
        List<Mrp_workcenter_productivityDTO> list = mrp_workcenter_productivityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_workcenter_productivity" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenter_productivities/searchdefault")
	public ResponseEntity<Page<Mrp_workcenter_productivityDTO>> searchDefault(Mrp_workcenter_productivitySearchContext context) {
        Page<Mrp_workcenter_productivity> domains = mrp_workcenter_productivityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_workcenter_productivityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_workcenter_productivity getEntity(){
        return new Mrp_workcenter_productivity();
    }

}
