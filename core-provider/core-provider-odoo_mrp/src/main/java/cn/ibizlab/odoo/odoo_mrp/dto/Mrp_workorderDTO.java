package cn.ibizlab.odoo.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mrp_workorderDTO]
 */
@Data
public class Mrp_workorderDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [TIME_IDS]
     *
     */
    @JSONField(name = "time_ids")
    @JsonProperty("time_ids")
    private String timeIds;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [WORKING_USER_IDS]
     *
     */
    @JSONField(name = "working_user_ids")
    @JsonProperty("working_user_ids")
    private String workingUserIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [QTY_PRODUCED]
     *
     */
    @JSONField(name = "qty_produced")
    @JsonProperty("qty_produced")
    private Double qtyProduced;

    /**
     * 属性 [DURATION]
     *
     */
    @JSONField(name = "duration")
    @JsonProperty("duration")
    private Double duration;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [SCRAP_COUNT]
     *
     */
    @JSONField(name = "scrap_count")
    @JsonProperty("scrap_count")
    private Integer scrapCount;

    /**
     * 属性 [DATE_PLANNED_START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned_start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned_start")
    private Timestamp datePlannedStart;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [IS_USER_WORKING]
     *
     */
    @JSONField(name = "is_user_working")
    @JsonProperty("is_user_working")
    private String isUserWorking;

    /**
     * 属性 [MOVE_RAW_IDS]
     *
     */
    @JSONField(name = "move_raw_ids")
    @JsonProperty("move_raw_ids")
    private String moveRawIds;

    /**
     * 属性 [LAST_WORKING_USER_ID]
     *
     */
    @JSONField(name = "last_working_user_id")
    @JsonProperty("last_working_user_id")
    private String lastWorkingUserId;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [IS_FIRST_WO]
     *
     */
    @JSONField(name = "is_first_wo")
    @JsonProperty("is_first_wo")
    private String isFirstWo;

    /**
     * 属性 [IS_PRODUCED]
     *
     */
    @JSONField(name = "is_produced")
    @JsonProperty("is_produced")
    private String isProduced;

    /**
     * 属性 [DATE_START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_start")
    private Timestamp dateStart;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [DURATION_UNIT]
     *
     */
    @JSONField(name = "duration_unit")
    @JsonProperty("duration_unit")
    private Double durationUnit;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [CAPACITY]
     *
     */
    @JSONField(name = "capacity")
    @JsonProperty("capacity")
    private Double capacity;

    /**
     * 属性 [ACTIVE_MOVE_LINE_IDS]
     *
     */
    @JSONField(name = "active_move_line_ids")
    @JsonProperty("active_move_line_ids")
    private String activeMoveLineIds;

    /**
     * 属性 [DATE_PLANNED_FINISHED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned_finished" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned_finished")
    private Timestamp datePlannedFinished;

    /**
     * 属性 [DATE_FINISHED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_finished" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_finished")
    private Timestamp dateFinished;

    /**
     * 属性 [QTY_REMAINING]
     *
     */
    @JSONField(name = "qty_remaining")
    @JsonProperty("qty_remaining")
    private Double qtyRemaining;

    /**
     * 属性 [SCRAP_IDS]
     *
     */
    @JSONField(name = "scrap_ids")
    @JsonProperty("scrap_ids")
    private String scrapIds;

    /**
     * 属性 [DURATION_PERCENT]
     *
     */
    @JSONField(name = "duration_percent")
    @JsonProperty("duration_percent")
    private Integer durationPercent;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DURATION_EXPECTED]
     *
     */
    @JSONField(name = "duration_expected")
    @JsonProperty("duration_expected")
    private Double durationExpected;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [QTY_PRODUCING]
     *
     */
    @JSONField(name = "qty_producing")
    @JsonProperty("qty_producing")
    private Double qtyProducing;

    /**
     * 属性 [PRODUCTION_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "production_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("production_date")
    private Timestamp productionDate;

    /**
     * 属性 [WORKSHEET]
     *
     */
    @JSONField(name = "worksheet")
    @JsonProperty("worksheet")
    private byte[] worksheet;

    /**
     * 属性 [WORKCENTER_ID_TEXT]
     *
     */
    @JSONField(name = "workcenter_id_text")
    @JsonProperty("workcenter_id_text")
    private String workcenterIdText;

    /**
     * 属性 [FINAL_LOT_ID_TEXT]
     *
     */
    @JSONField(name = "final_lot_id_text")
    @JsonProperty("final_lot_id_text")
    private String finalLotIdText;

    /**
     * 属性 [WORKING_STATE]
     *
     */
    @JSONField(name = "working_state")
    @JsonProperty("working_state")
    private String workingState;

    /**
     * 属性 [QTY_PRODUCTION]
     *
     */
    @JSONField(name = "qty_production")
    @JsonProperty("qty_production")
    private Double qtyProduction;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [PRODUCTION_AVAILABILITY]
     *
     */
    @JSONField(name = "production_availability")
    @JsonProperty("production_availability")
    private String productionAvailability;

    /**
     * 属性 [PRODUCTION_STATE]
     *
     */
    @JSONField(name = "production_state")
    @JsonProperty("production_state")
    private String productionState;

    /**
     * 属性 [NEXT_WORK_ORDER_ID_TEXT]
     *
     */
    @JSONField(name = "next_work_order_id_text")
    @JsonProperty("next_work_order_id_text")
    private String nextWorkOrderIdText;

    /**
     * 属性 [OPERATION_ID_TEXT]
     *
     */
    @JSONField(name = "operation_id_text")
    @JsonProperty("operation_id_text")
    private String operationIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PRODUCTION_ID_TEXT]
     *
     */
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    private String productionIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 属性 [PRODUCT_TRACKING]
     *
     */
    @JSONField(name = "product_tracking")
    @JsonProperty("product_tracking")
    private String productTracking;

    /**
     * 属性 [FINAL_LOT_ID]
     *
     */
    @JSONField(name = "final_lot_id")
    @JsonProperty("final_lot_id")
    private Integer finalLotId;

    /**
     * 属性 [WORKCENTER_ID]
     *
     */
    @JSONField(name = "workcenter_id")
    @JsonProperty("workcenter_id")
    private Integer workcenterId;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Integer productionId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [OPERATION_ID]
     *
     */
    @JSONField(name = "operation_id")
    @JsonProperty("operation_id")
    private Integer operationId;

    /**
     * 属性 [NEXT_WORK_ORDER_ID]
     *
     */
    @JSONField(name = "next_work_order_id")
    @JsonProperty("next_work_order_id")
    private Integer nextWorkOrderId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [QTY_PRODUCED]
     */
    public void setQtyProduced(Double  qtyProduced){
        this.qtyProduced = qtyProduced ;
        this.modify("qty_produced",qtyProduced);
    }

    /**
     * 设置 [DURATION]
     */
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.modify("duration",duration);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [DATE_PLANNED_START]
     */
    public void setDatePlannedStart(Timestamp  datePlannedStart){
        this.datePlannedStart = datePlannedStart ;
        this.modify("date_planned_start",datePlannedStart);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [DATE_START]
     */
    public void setDateStart(Timestamp  dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }

    /**
     * 设置 [DURATION_UNIT]
     */
    public void setDurationUnit(Double  durationUnit){
        this.durationUnit = durationUnit ;
        this.modify("duration_unit",durationUnit);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [CAPACITY]
     */
    public void setCapacity(Double  capacity){
        this.capacity = capacity ;
        this.modify("capacity",capacity);
    }

    /**
     * 设置 [DATE_PLANNED_FINISHED]
     */
    public void setDatePlannedFinished(Timestamp  datePlannedFinished){
        this.datePlannedFinished = datePlannedFinished ;
        this.modify("date_planned_finished",datePlannedFinished);
    }

    /**
     * 设置 [DATE_FINISHED]
     */
    public void setDateFinished(Timestamp  dateFinished){
        this.dateFinished = dateFinished ;
        this.modify("date_finished",dateFinished);
    }

    /**
     * 设置 [DURATION_PERCENT]
     */
    public void setDurationPercent(Integer  durationPercent){
        this.durationPercent = durationPercent ;
        this.modify("duration_percent",durationPercent);
    }

    /**
     * 设置 [DURATION_EXPECTED]
     */
    public void setDurationExpected(Double  durationExpected){
        this.durationExpected = durationExpected ;
        this.modify("duration_expected",durationExpected);
    }

    /**
     * 设置 [QTY_PRODUCING]
     */
    public void setQtyProducing(Double  qtyProducing){
        this.qtyProducing = qtyProducing ;
        this.modify("qty_producing",qtyProducing);
    }

    /**
     * 设置 [FINAL_LOT_ID]
     */
    public void setFinalLotId(Integer  finalLotId){
        this.finalLotId = finalLotId ;
        this.modify("final_lot_id",finalLotId);
    }

    /**
     * 设置 [WORKCENTER_ID]
     */
    public void setWorkcenterId(Integer  workcenterId){
        this.workcenterId = workcenterId ;
        this.modify("workcenter_id",workcenterId);
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    public void setProductionId(Integer  productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }

    /**
     * 设置 [OPERATION_ID]
     */
    public void setOperationId(Integer  operationId){
        this.operationId = operationId ;
        this.modify("operation_id",operationId);
    }

    /**
     * 设置 [NEXT_WORK_ORDER_ID]
     */
    public void setNextWorkOrderId(Integer  nextWorkOrderId){
        this.nextWorkOrderId = nextWorkOrderId ;
        this.modify("next_work_order_id",nextWorkOrderId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Integer  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }


}

