package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workorderService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workorderSearchContext;




@Slf4j
@Api(tags = {"Mrp_workorder" })
@RestController("odoo_mrp-mrp_workorder")
@RequestMapping("")
public class Mrp_workorderResource {

    @Autowired
    private IMrp_workorderService mrp_workorderService;

    @Autowired
    @Lazy
    private Mrp_workorderMapping mrp_workorderMapping;







    @PreAuthorize("hasPermission(#mrp_workorder_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_workorder" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workorders/{mrp_workorder_id}")

    public ResponseEntity<Mrp_workorderDTO> update(@PathVariable("mrp_workorder_id") Integer mrp_workorder_id, @RequestBody Mrp_workorderDTO mrp_workorderdto) {
		Mrp_workorder domain = mrp_workorderMapping.toDomain(mrp_workorderdto);
        domain.setId(mrp_workorder_id);
		mrp_workorderService.update(domain);
		Mrp_workorderDTO dto = mrp_workorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_workorder_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_workorder" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workorders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workorderDTO> mrp_workorderdtos) {
        mrp_workorderService.updateBatch(mrp_workorderMapping.toDomain(mrp_workorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mrp_workorder_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_workorder" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workorders/{mrp_workorder_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workorder_id") Integer mrp_workorder_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_workorderService.remove(mrp_workorder_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_workorder" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workorders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_workorderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_workorder_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_workorder" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workorders/{mrp_workorder_id}")
    public ResponseEntity<Mrp_workorderDTO> get(@PathVariable("mrp_workorder_id") Integer mrp_workorder_id) {
        Mrp_workorder domain = mrp_workorderService.get(mrp_workorder_id);
        Mrp_workorderDTO dto = mrp_workorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_workorder" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders")

    public ResponseEntity<Mrp_workorderDTO> create(@RequestBody Mrp_workorderDTO mrp_workorderdto) {
        Mrp_workorder domain = mrp_workorderMapping.toDomain(mrp_workorderdto);
		mrp_workorderService.create(domain);
        Mrp_workorderDTO dto = mrp_workorderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_workorder" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_workorderDTO> mrp_workorderdtos) {
        mrp_workorderService.createBatch(mrp_workorderMapping.toDomain(mrp_workorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_workorder" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workorders/fetchdefault")
	public ResponseEntity<List<Mrp_workorderDTO>> fetchDefault(Mrp_workorderSearchContext context) {
        Page<Mrp_workorder> domains = mrp_workorderService.searchDefault(context) ;
        List<Mrp_workorderDTO> list = mrp_workorderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_workorder" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workorders/searchdefault")
	public ResponseEntity<Page<Mrp_workorderDTO>> searchDefault(Mrp_workorderSearchContext context) {
        Page<Mrp_workorder> domains = mrp_workorderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_workorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_workorder getEntity(){
        return new Mrp_workorder();
    }

}
