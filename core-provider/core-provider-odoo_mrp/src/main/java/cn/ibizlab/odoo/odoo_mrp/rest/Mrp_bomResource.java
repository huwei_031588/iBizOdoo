package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_bomService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bomSearchContext;




@Slf4j
@Api(tags = {"Mrp_bom" })
@RestController("odoo_mrp-mrp_bom")
@RequestMapping("")
public class Mrp_bomResource {

    @Autowired
    private IMrp_bomService mrp_bomService;

    @Autowired
    @Lazy
    private Mrp_bomMapping mrp_bomMapping;




    @PreAuthorize("hasPermission(#mrp_bom_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_bom" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_boms/{mrp_bom_id}")

    public ResponseEntity<Mrp_bomDTO> update(@PathVariable("mrp_bom_id") Integer mrp_bom_id, @RequestBody Mrp_bomDTO mrp_bomdto) {
		Mrp_bom domain = mrp_bomMapping.toDomain(mrp_bomdto);
        domain.setId(mrp_bom_id);
		mrp_bomService.update(domain);
		Mrp_bomDTO dto = mrp_bomMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_bom_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_bom" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_boms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_bomDTO> mrp_bomdtos) {
        mrp_bomService.updateBatch(mrp_bomMapping.toDomain(mrp_bomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mrp_bom_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_bom" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_boms/{mrp_bom_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_bom_id") Integer mrp_bom_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_bomService.remove(mrp_bom_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_bom" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_boms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_bomService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_bom" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_boms")

    public ResponseEntity<Mrp_bomDTO> create(@RequestBody Mrp_bomDTO mrp_bomdto) {
        Mrp_bom domain = mrp_bomMapping.toDomain(mrp_bomdto);
		mrp_bomService.create(domain);
        Mrp_bomDTO dto = mrp_bomMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_bom" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_boms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_bomDTO> mrp_bomdtos) {
        mrp_bomService.createBatch(mrp_bomMapping.toDomain(mrp_bomdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mrp_bom_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_bom" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_boms/{mrp_bom_id}")
    public ResponseEntity<Mrp_bomDTO> get(@PathVariable("mrp_bom_id") Integer mrp_bom_id) {
        Mrp_bom domain = mrp_bomService.get(mrp_bom_id);
        Mrp_bomDTO dto = mrp_bomMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_bom" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_boms/fetchdefault")
	public ResponseEntity<List<Mrp_bomDTO>> fetchDefault(Mrp_bomSearchContext context) {
        Page<Mrp_bom> domains = mrp_bomService.searchDefault(context) ;
        List<Mrp_bomDTO> list = mrp_bomMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_bom" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_boms/searchdefault")
	public ResponseEntity<Page<Mrp_bomDTO>> searchDefault(Mrp_bomSearchContext context) {
        Page<Mrp_bom> domains = mrp_bomService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_bomMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_bom getEntity(){
        return new Mrp_bom();
    }

}
