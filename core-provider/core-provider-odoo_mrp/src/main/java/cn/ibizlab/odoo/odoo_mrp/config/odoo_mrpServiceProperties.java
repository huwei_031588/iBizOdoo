package cn.ibizlab.odoo.odoo_mrp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-mrp")
@Data
public class odoo_mrpServiceProperties {

	private boolean enabled;

	private boolean auth;


}