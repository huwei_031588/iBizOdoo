package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_bom_lineService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;




@Slf4j
@Api(tags = {"Mrp_bom_line" })
@RestController("odoo_mrp-mrp_bom_line")
@RequestMapping("")
public class Mrp_bom_lineResource {

    @Autowired
    private IMrp_bom_lineService mrp_bom_lineService;

    @Autowired
    @Lazy
    private Mrp_bom_lineMapping mrp_bom_lineMapping;




    @PreAuthorize("hasPermission('Remove',{#mrp_bom_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_bom_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_bom_lines/{mrp_bom_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_bom_line_id") Integer mrp_bom_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_bom_lineService.remove(mrp_bom_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_bom_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_bom_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_bom_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_bom_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_bom_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_bom_lines/{mrp_bom_line_id}")

    public ResponseEntity<Mrp_bom_lineDTO> update(@PathVariable("mrp_bom_line_id") Integer mrp_bom_line_id, @RequestBody Mrp_bom_lineDTO mrp_bom_linedto) {
		Mrp_bom_line domain = mrp_bom_lineMapping.toDomain(mrp_bom_linedto);
        domain.setId(mrp_bom_line_id);
		mrp_bom_lineService.update(domain);
		Mrp_bom_lineDTO dto = mrp_bom_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_bom_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_bom_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_bom_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_bom_lineDTO> mrp_bom_linedtos) {
        mrp_bom_lineService.updateBatch(mrp_bom_lineMapping.toDomain(mrp_bom_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_bom_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines")

    public ResponseEntity<Mrp_bom_lineDTO> create(@RequestBody Mrp_bom_lineDTO mrp_bom_linedto) {
        Mrp_bom_line domain = mrp_bom_lineMapping.toDomain(mrp_bom_linedto);
		mrp_bom_lineService.create(domain);
        Mrp_bom_lineDTO dto = mrp_bom_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_bom_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_bom_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_bom_lineDTO> mrp_bom_linedtos) {
        mrp_bom_lineService.createBatch(mrp_bom_lineMapping.toDomain(mrp_bom_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_bom_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_bom_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_bom_lines/{mrp_bom_line_id}")
    public ResponseEntity<Mrp_bom_lineDTO> get(@PathVariable("mrp_bom_line_id") Integer mrp_bom_line_id) {
        Mrp_bom_line domain = mrp_bom_lineService.get(mrp_bom_line_id);
        Mrp_bom_lineDTO dto = mrp_bom_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_bom_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_bom_lines/fetchdefault")
	public ResponseEntity<List<Mrp_bom_lineDTO>> fetchDefault(Mrp_bom_lineSearchContext context) {
        Page<Mrp_bom_line> domains = mrp_bom_lineService.searchDefault(context) ;
        List<Mrp_bom_lineDTO> list = mrp_bom_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_bom_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_bom_lines/searchdefault")
	public ResponseEntity<Page<Mrp_bom_lineDTO>> searchDefault(Mrp_bom_lineSearchContext context) {
        Page<Mrp_bom_line> domains = mrp_bom_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_bom_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_bom_line getEntity(){
        return new Mrp_bom_line();
    }

}
