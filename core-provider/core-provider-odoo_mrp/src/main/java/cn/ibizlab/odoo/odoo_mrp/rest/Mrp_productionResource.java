package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_productionService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_productionSearchContext;




@Slf4j
@Api(tags = {"Mrp_production" })
@RestController("odoo_mrp-mrp_production")
@RequestMapping("")
public class Mrp_productionResource {

    @Autowired
    private IMrp_productionService mrp_productionService;

    @Autowired
    @Lazy
    private Mrp_productionMapping mrp_productionMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_production" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_productions")

    public ResponseEntity<Mrp_productionDTO> create(@RequestBody Mrp_productionDTO mrp_productiondto) {
        Mrp_production domain = mrp_productionMapping.toDomain(mrp_productiondto);
		mrp_productionService.create(domain);
        Mrp_productionDTO dto = mrp_productionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_production" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_productions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_productionDTO> mrp_productiondtos) {
        mrp_productionService.createBatch(mrp_productionMapping.toDomain(mrp_productiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_production_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_production" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_productions/{mrp_production_id}")

    public ResponseEntity<Mrp_productionDTO> update(@PathVariable("mrp_production_id") Integer mrp_production_id, @RequestBody Mrp_productionDTO mrp_productiondto) {
		Mrp_production domain = mrp_productionMapping.toDomain(mrp_productiondto);
        domain.setId(mrp_production_id);
		mrp_productionService.update(domain);
		Mrp_productionDTO dto = mrp_productionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_production_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_production" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_productions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_productionDTO> mrp_productiondtos) {
        mrp_productionService.updateBatch(mrp_productionMapping.toDomain(mrp_productiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mrp_production_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_production" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_productions/{mrp_production_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_production_id") Integer mrp_production_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_productionService.remove(mrp_production_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_production" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_productions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_productionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mrp_production_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_production" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_productions/{mrp_production_id}")
    public ResponseEntity<Mrp_productionDTO> get(@PathVariable("mrp_production_id") Integer mrp_production_id) {
        Mrp_production domain = mrp_productionService.get(mrp_production_id);
        Mrp_productionDTO dto = mrp_productionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_production" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_productions/fetchdefault")
	public ResponseEntity<List<Mrp_productionDTO>> fetchDefault(Mrp_productionSearchContext context) {
        Page<Mrp_production> domains = mrp_productionService.searchDefault(context) ;
        List<Mrp_productionDTO> list = mrp_productionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_production" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_productions/searchdefault")
	public ResponseEntity<Page<Mrp_productionDTO>> searchDefault(Mrp_productionSearchContext context) {
        Page<Mrp_production> domains = mrp_productionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_productionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_production getEntity(){
        return new Mrp_production();
    }

}
