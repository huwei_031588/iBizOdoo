package cn.ibizlab.odoo.odoo_mrp.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.odoo_mrp.dto.Mrp_workcenter_productivity_loss_typeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mrp_workcenter_productivity_loss_typeMapping extends MappingBase<Mrp_workcenter_productivity_loss_typeDTO, Mrp_workcenter_productivity_loss_type> {


}

