package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivity_loss_typeService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_loss_typeSearchContext;




@Slf4j
@Api(tags = {"Mrp_workcenter_productivity_loss_type" })
@RestController("odoo_mrp-mrp_workcenter_productivity_loss_type")
@RequestMapping("")
public class Mrp_workcenter_productivity_loss_typeResource {

    @Autowired
    private IMrp_workcenter_productivity_loss_typeService mrp_workcenter_productivity_loss_typeService;

    @Autowired
    @Lazy
    private Mrp_workcenter_productivity_loss_typeMapping mrp_workcenter_productivity_loss_typeMapping;




    @PreAuthorize("hasPermission(#mrp_workcenter_productivity_loss_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_loss_types/{mrp_workcenter_productivity_loss_type_id}")

    public ResponseEntity<Mrp_workcenter_productivity_loss_typeDTO> update(@PathVariable("mrp_workcenter_productivity_loss_type_id") Integer mrp_workcenter_productivity_loss_type_id, @RequestBody Mrp_workcenter_productivity_loss_typeDTO mrp_workcenter_productivity_loss_typedto) {
		Mrp_workcenter_productivity_loss_type domain = mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedto);
        domain.setId(mrp_workcenter_productivity_loss_type_id);
		mrp_workcenter_productivity_loss_typeService.update(domain);
		Mrp_workcenter_productivity_loss_typeDTO dto = mrp_workcenter_productivity_loss_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_workcenter_productivity_loss_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_loss_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenter_productivity_loss_typeDTO> mrp_workcenter_productivity_loss_typedtos) {
        mrp_workcenter_productivity_loss_typeService.updateBatch(mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types")

    public ResponseEntity<Mrp_workcenter_productivity_loss_typeDTO> create(@RequestBody Mrp_workcenter_productivity_loss_typeDTO mrp_workcenter_productivity_loss_typedto) {
        Mrp_workcenter_productivity_loss_type domain = mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedto);
		mrp_workcenter_productivity_loss_typeService.create(domain);
        Mrp_workcenter_productivity_loss_typeDTO dto = mrp_workcenter_productivity_loss_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_loss_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_workcenter_productivity_loss_typeDTO> mrp_workcenter_productivity_loss_typedtos) {
        mrp_workcenter_productivity_loss_typeService.createBatch(mrp_workcenter_productivity_loss_typeMapping.toDomain(mrp_workcenter_productivity_loss_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mrp_workcenter_productivity_loss_type_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_loss_types/{mrp_workcenter_productivity_loss_type_id}")
    public ResponseEntity<Mrp_workcenter_productivity_loss_typeDTO> get(@PathVariable("mrp_workcenter_productivity_loss_type_id") Integer mrp_workcenter_productivity_loss_type_id) {
        Mrp_workcenter_productivity_loss_type domain = mrp_workcenter_productivity_loss_typeService.get(mrp_workcenter_productivity_loss_type_id);
        Mrp_workcenter_productivity_loss_typeDTO dto = mrp_workcenter_productivity_loss_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#mrp_workcenter_productivity_loss_type_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_loss_types/{mrp_workcenter_productivity_loss_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_productivity_loss_type_id") Integer mrp_workcenter_productivity_loss_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivity_loss_typeService.remove(mrp_workcenter_productivity_loss_type_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_loss_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_workcenter_productivity_loss_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_workcenter_productivity_loss_type" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenter_productivity_loss_types/fetchdefault")
	public ResponseEntity<List<Mrp_workcenter_productivity_loss_typeDTO>> fetchDefault(Mrp_workcenter_productivity_loss_typeSearchContext context) {
        Page<Mrp_workcenter_productivity_loss_type> domains = mrp_workcenter_productivity_loss_typeService.searchDefault(context) ;
        List<Mrp_workcenter_productivity_loss_typeDTO> list = mrp_workcenter_productivity_loss_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_workcenter_productivity_loss_type" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenter_productivity_loss_types/searchdefault")
	public ResponseEntity<Page<Mrp_workcenter_productivity_loss_typeDTO>> searchDefault(Mrp_workcenter_productivity_loss_typeSearchContext context) {
        Page<Mrp_workcenter_productivity_loss_type> domains = mrp_workcenter_productivity_loss_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_workcenter_productivity_loss_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_workcenter_productivity_loss_type getEntity(){
        return new Mrp_workcenter_productivity_loss_type();
    }

}
