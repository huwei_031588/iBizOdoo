package cn.ibizlab.odoo.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mrp_product_produce_lineDTO]
 */
@Data
public class Mrp_product_produce_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [QTY_TO_CONSUME]
     *
     */
    @JSONField(name = "qty_to_consume")
    @JsonProperty("qty_to_consume")
    private Double qtyToConsume;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [QTY_DONE]
     *
     */
    @JSONField(name = "qty_done")
    @JsonProperty("qty_done")
    private Double qtyDone;

    /**
     * 属性 [QTY_RESERVED]
     *
     */
    @JSONField(name = "qty_reserved")
    @JsonProperty("qty_reserved")
    private Double qtyReserved;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [LOT_ID_TEXT]
     *
     */
    @JSONField(name = "lot_id_text")
    @JsonProperty("lot_id_text")
    private String lotIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 属性 [PRODUCT_TRACKING]
     *
     */
    @JSONField(name = "product_tracking")
    @JsonProperty("product_tracking")
    private String productTracking;

    /**
     * 属性 [MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Integer moveId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 属性 [LOT_ID]
     *
     */
    @JSONField(name = "lot_id")
    @JsonProperty("lot_id")
    private Integer lotId;

    /**
     * 属性 [PRODUCT_PRODUCE_ID]
     *
     */
    @JSONField(name = "product_produce_id")
    @JsonProperty("product_produce_id")
    private Integer productProduceId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [QTY_TO_CONSUME]
     */
    public void setQtyToConsume(Double  qtyToConsume){
        this.qtyToConsume = qtyToConsume ;
        this.modify("qty_to_consume",qtyToConsume);
    }

    /**
     * 设置 [QTY_DONE]
     */
    public void setQtyDone(Double  qtyDone){
        this.qtyDone = qtyDone ;
        this.modify("qty_done",qtyDone);
    }

    /**
     * 设置 [QTY_RESERVED]
     */
    public void setQtyReserved(Double  qtyReserved){
        this.qtyReserved = qtyReserved ;
        this.modify("qty_reserved",qtyReserved);
    }

    /**
     * 设置 [MOVE_ID]
     */
    public void setMoveId(Integer  moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Integer  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [LOT_ID]
     */
    public void setLotId(Integer  lotId){
        this.lotId = lotId ;
        this.modify("lot_id",lotId);
    }

    /**
     * 设置 [PRODUCT_PRODUCE_ID]
     */
    public void setProductProduceId(Integer  productProduceId){
        this.productProduceId = productProduceId ;
        this.modify("product_produce_id",productProduceId);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Integer  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }


}

