package cn.ibizlab.odoo.odoo_mrp.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.odoo.odoo_mrp.dto.Mrp_workcenterDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mrp_workcenterMapping extends MappingBase<Mrp_workcenterDTO, Mrp_workcenter> {


}

