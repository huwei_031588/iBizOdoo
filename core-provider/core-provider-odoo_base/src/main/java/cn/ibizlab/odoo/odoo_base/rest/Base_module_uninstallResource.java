package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_uninstallService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_uninstallSearchContext;




@Slf4j
@Api(tags = {"Base_module_uninstall" })
@RestController("odoo_base-base_module_uninstall")
@RequestMapping("")
public class Base_module_uninstallResource {

    @Autowired
    private IBase_module_uninstallService base_module_uninstallService;

    @Autowired
    @Lazy
    private Base_module_uninstallMapping base_module_uninstallMapping;




    @PreAuthorize("hasPermission(#base_module_uninstall_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_module_uninstall" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_uninstalls/{base_module_uninstall_id}")

    public ResponseEntity<Base_module_uninstallDTO> update(@PathVariable("base_module_uninstall_id") Integer base_module_uninstall_id, @RequestBody Base_module_uninstallDTO base_module_uninstalldto) {
		Base_module_uninstall domain = base_module_uninstallMapping.toDomain(base_module_uninstalldto);
        domain.setId(base_module_uninstall_id);
		base_module_uninstallService.update(domain);
		Base_module_uninstallDTO dto = base_module_uninstallMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_module_uninstall_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_module_uninstall" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_uninstalls/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_module_uninstallDTO> base_module_uninstalldtos) {
        base_module_uninstallService.updateBatch(base_module_uninstallMapping.toDomain(base_module_uninstalldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#base_module_uninstall_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_module_uninstall" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_module_uninstalls/{base_module_uninstall_id}")
    public ResponseEntity<Base_module_uninstallDTO> get(@PathVariable("base_module_uninstall_id") Integer base_module_uninstall_id) {
        Base_module_uninstall domain = base_module_uninstallService.get(base_module_uninstall_id);
        Base_module_uninstallDTO dto = base_module_uninstallMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#base_module_uninstall_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_module_uninstall" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_uninstalls/{base_module_uninstall_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_module_uninstall_id") Integer base_module_uninstall_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_module_uninstallService.remove(base_module_uninstall_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_module_uninstall" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_uninstalls/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_module_uninstallService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_module_uninstall" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls")

    public ResponseEntity<Base_module_uninstallDTO> create(@RequestBody Base_module_uninstallDTO base_module_uninstalldto) {
        Base_module_uninstall domain = base_module_uninstallMapping.toDomain(base_module_uninstalldto);
		base_module_uninstallService.create(domain);
        Base_module_uninstallDTO dto = base_module_uninstallMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_module_uninstall" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_module_uninstallDTO> base_module_uninstalldtos) {
        base_module_uninstallService.createBatch(base_module_uninstallMapping.toDomain(base_module_uninstalldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_module_uninstall" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_module_uninstalls/fetchdefault")
	public ResponseEntity<List<Base_module_uninstallDTO>> fetchDefault(Base_module_uninstallSearchContext context) {
        Page<Base_module_uninstall> domains = base_module_uninstallService.searchDefault(context) ;
        List<Base_module_uninstallDTO> list = base_module_uninstallMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_module_uninstall" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_module_uninstalls/searchdefault")
	public ResponseEntity<Page<Base_module_uninstallDTO>> searchDefault(Base_module_uninstallSearchContext context) {
        Page<Base_module_uninstall> domains = base_module_uninstallService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_module_uninstallMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_module_uninstall getEntity(){
        return new Base_module_uninstall();
    }

}
