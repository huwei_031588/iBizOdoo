package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_category;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_categoryService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_categorySearchContext;




@Slf4j
@Api(tags = {"Res_partner_category" })
@RestController("odoo_base-res_partner_category")
@RequestMapping("")
public class Res_partner_categoryResource {

    @Autowired
    private IRes_partner_categoryService res_partner_categoryService;

    @Autowired
    @Lazy
    private Res_partner_categoryMapping res_partner_categoryMapping;




    @PreAuthorize("hasPermission(#res_partner_category_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_partner_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_categories/{res_partner_category_id}")
    public ResponseEntity<Res_partner_categoryDTO> get(@PathVariable("res_partner_category_id") Integer res_partner_category_id) {
        Res_partner_category domain = res_partner_categoryService.get(res_partner_category_id);
        Res_partner_categoryDTO dto = res_partner_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#res_partner_category_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_partner_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_categories/{res_partner_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_category_id") Integer res_partner_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partner_categoryService.remove(res_partner_category_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_partner_category" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_partner_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_partner_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_categories")

    public ResponseEntity<Res_partner_categoryDTO> create(@RequestBody Res_partner_categoryDTO res_partner_categorydto) {
        Res_partner_category domain = res_partner_categoryMapping.toDomain(res_partner_categorydto);
		res_partner_categoryService.create(domain);
        Res_partner_categoryDTO dto = res_partner_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_partner_category" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partner_categoryDTO> res_partner_categorydtos) {
        res_partner_categoryService.createBatch(res_partner_categoryMapping.toDomain(res_partner_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_partner_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_partner_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_categories/{res_partner_category_id}")

    public ResponseEntity<Res_partner_categoryDTO> update(@PathVariable("res_partner_category_id") Integer res_partner_category_id, @RequestBody Res_partner_categoryDTO res_partner_categorydto) {
		Res_partner_category domain = res_partner_categoryMapping.toDomain(res_partner_categorydto);
        domain.setId(res_partner_category_id);
		res_partner_categoryService.update(domain);
		Res_partner_categoryDTO dto = res_partner_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_partner_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_partner_category" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_categoryDTO> res_partner_categorydtos) {
        res_partner_categoryService.updateBatch(res_partner_categoryMapping.toDomain(res_partner_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_partner_category" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_categories/fetchdefault")
	public ResponseEntity<List<Res_partner_categoryDTO>> fetchDefault(Res_partner_categorySearchContext context) {
        Page<Res_partner_category> domains = res_partner_categoryService.searchDefault(context) ;
        List<Res_partner_categoryDTO> list = res_partner_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_partner_category" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_categories/searchdefault")
	public ResponseEntity<Page<Res_partner_categoryDTO>> searchDefault(Res_partner_categorySearchContext context) {
        Page<Res_partner_category> domains = res_partner_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_partner_category getEntity(){
        return new Res_partner_category();
    }

}
