package cn.ibizlab.odoo.odoo_base.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency_rate;
import cn.ibizlab.odoo.odoo_base.dto.Res_currency_rateDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Res_currency_rateMapping extends MappingBase<Res_currency_rateDTO, Res_currency_rate> {


}

