package cn.ibizlab.odoo.odoo_base.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_base")
public class odoo_baseRestConfiguration {

}
