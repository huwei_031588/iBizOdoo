package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country_group;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_country_groupService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_country_groupSearchContext;




@Slf4j
@Api(tags = {"Res_country_group" })
@RestController("odoo_base-res_country_group")
@RequestMapping("")
public class Res_country_groupResource {

    @Autowired
    private IRes_country_groupService res_country_groupService;

    @Autowired
    @Lazy
    private Res_country_groupMapping res_country_groupMapping;













    @PreAuthorize("hasPermission(#res_country_group_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_country_group" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_country_groups/{res_country_group_id}")
    public ResponseEntity<Res_country_groupDTO> get(@PathVariable("res_country_group_id") Integer res_country_group_id) {
        Res_country_group domain = res_country_groupService.get(res_country_group_id);
        Res_country_groupDTO dto = res_country_groupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#res_country_group_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_country_group" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_country_groups/{res_country_group_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_country_group_id") Integer res_country_group_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_country_groupService.remove(res_country_group_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_country_group" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_country_groups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_country_groupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_country_group_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_country_group" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_country_groups/{res_country_group_id}")

    public ResponseEntity<Res_country_groupDTO> update(@PathVariable("res_country_group_id") Integer res_country_group_id, @RequestBody Res_country_groupDTO res_country_groupdto) {
		Res_country_group domain = res_country_groupMapping.toDomain(res_country_groupdto);
        domain.setId(res_country_group_id);
		res_country_groupService.update(domain);
		Res_country_groupDTO dto = res_country_groupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_country_group_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_country_group" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_country_groups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_country_groupDTO> res_country_groupdtos) {
        res_country_groupService.updateBatch(res_country_groupMapping.toDomain(res_country_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_country_group" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_groups")

    public ResponseEntity<Res_country_groupDTO> create(@RequestBody Res_country_groupDTO res_country_groupdto) {
        Res_country_group domain = res_country_groupMapping.toDomain(res_country_groupdto);
		res_country_groupService.create(domain);
        Res_country_groupDTO dto = res_country_groupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_country_group" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_country_groupDTO> res_country_groupdtos) {
        res_country_groupService.createBatch(res_country_groupMapping.toDomain(res_country_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_country_group" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_country_groups/fetchdefault")
	public ResponseEntity<List<Res_country_groupDTO>> fetchDefault(Res_country_groupSearchContext context) {
        Page<Res_country_group> domains = res_country_groupService.searchDefault(context) ;
        List<Res_country_groupDTO> list = res_country_groupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_country_group" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_country_groups/searchdefault")
	public ResponseEntity<Page<Res_country_groupDTO>> searchDefault(Res_country_groupSearchContext context) {
        Page<Res_country_group> domains = res_country_groupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_country_groupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_country_group getEntity(){
        return new Res_country_group();
    }

}
