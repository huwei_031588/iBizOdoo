package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_installer;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_config_installerService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_installerSearchContext;




@Slf4j
@Api(tags = {"Res_config_installer" })
@RestController("odoo_base-res_config_installer")
@RequestMapping("")
public class Res_config_installerResource {

    @Autowired
    private IRes_config_installerService res_config_installerService;

    @Autowired
    @Lazy
    private Res_config_installerMapping res_config_installerMapping;







    @PreAuthorize("hasPermission('Remove',{#res_config_installer_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_config_installer" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_config_installers/{res_config_installer_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_config_installer_id") Integer res_config_installer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_config_installerService.remove(res_config_installer_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_config_installer" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_config_installers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_config_installerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_config_installer" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_installers")

    public ResponseEntity<Res_config_installerDTO> create(@RequestBody Res_config_installerDTO res_config_installerdto) {
        Res_config_installer domain = res_config_installerMapping.toDomain(res_config_installerdto);
		res_config_installerService.create(domain);
        Res_config_installerDTO dto = res_config_installerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_config_installer" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_installers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_config_installerDTO> res_config_installerdtos) {
        res_config_installerService.createBatch(res_config_installerMapping.toDomain(res_config_installerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_config_installer_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_config_installer" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_config_installers/{res_config_installer_id}")

    public ResponseEntity<Res_config_installerDTO> update(@PathVariable("res_config_installer_id") Integer res_config_installer_id, @RequestBody Res_config_installerDTO res_config_installerdto) {
		Res_config_installer domain = res_config_installerMapping.toDomain(res_config_installerdto);
        domain.setId(res_config_installer_id);
		res_config_installerService.update(domain);
		Res_config_installerDTO dto = res_config_installerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_config_installer_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_config_installer" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_config_installers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_config_installerDTO> res_config_installerdtos) {
        res_config_installerService.updateBatch(res_config_installerMapping.toDomain(res_config_installerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_config_installer_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_config_installer" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_config_installers/{res_config_installer_id}")
    public ResponseEntity<Res_config_installerDTO> get(@PathVariable("res_config_installer_id") Integer res_config_installer_id) {
        Res_config_installer domain = res_config_installerService.get(res_config_installer_id);
        Res_config_installerDTO dto = res_config_installerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_config_installer" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_config_installers/fetchdefault")
	public ResponseEntity<List<Res_config_installerDTO>> fetchDefault(Res_config_installerSearchContext context) {
        Page<Res_config_installer> domains = res_config_installerService.searchDefault(context) ;
        List<Res_config_installerDTO> list = res_config_installerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_config_installer" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_config_installers/searchdefault")
	public ResponseEntity<Page<Res_config_installerDTO>> searchDefault(Res_config_installerSearchContext context) {
        Page<Res_config_installer> domains = res_config_installerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_config_installerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_config_installer getEntity(){
        return new Res_config_installer();
    }

}
