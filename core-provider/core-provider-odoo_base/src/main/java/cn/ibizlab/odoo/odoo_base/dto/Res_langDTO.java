package cn.ibizlab.odoo.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Res_langDTO]
 */
@Data
public class Res_langDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TRANSLATABLE]
     *
     */
    @JSONField(name = "translatable")
    @JsonProperty("translatable")
    private String translatable;

    /**
     * 属性 [THOUSANDS_SEP]
     *
     */
    @JSONField(name = "thousands_sep")
    @JsonProperty("thousands_sep")
    private String thousandsSep;

    /**
     * 属性 [ISO_CODE]
     *
     */
    @JSONField(name = "iso_code")
    @JsonProperty("iso_code")
    private String isoCode;

    /**
     * 属性 [DECIMAL_POINT]
     *
     */
    @JSONField(name = "decimal_point")
    @JsonProperty("decimal_point")
    private String decimalPoint;

    /**
     * 属性 [GROUPING]
     *
     */
    @JSONField(name = "grouping")
    @JsonProperty("grouping")
    private String grouping;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DIRECTION]
     *
     */
    @JSONField(name = "direction")
    @JsonProperty("direction")
    private String direction;

    /**
     * 属性 [DATE_FORMAT]
     *
     */
    @JSONField(name = "date_format")
    @JsonProperty("date_format")
    private String dateFormat;

    /**
     * 属性 [TIME_FORMAT]
     *
     */
    @JSONField(name = "time_format")
    @JsonProperty("time_format")
    private String timeFormat;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [WEEK_START]
     *
     */
    @JSONField(name = "week_start")
    @JsonProperty("week_start")
    private String weekStart;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [TRANSLATABLE]
     */
    public void setTranslatable(String  translatable){
        this.translatable = translatable ;
        this.modify("translatable",translatable);
    }

    /**
     * 设置 [THOUSANDS_SEP]
     */
    public void setThousandsSep(String  thousandsSep){
        this.thousandsSep = thousandsSep ;
        this.modify("thousands_sep",thousandsSep);
    }

    /**
     * 设置 [ISO_CODE]
     */
    public void setIsoCode(String  isoCode){
        this.isoCode = isoCode ;
        this.modify("iso_code",isoCode);
    }

    /**
     * 设置 [DECIMAL_POINT]
     */
    public void setDecimalPoint(String  decimalPoint){
        this.decimalPoint = decimalPoint ;
        this.modify("decimal_point",decimalPoint);
    }

    /**
     * 设置 [GROUPING]
     */
    public void setGrouping(String  grouping){
        this.grouping = grouping ;
        this.modify("grouping",grouping);
    }

    /**
     * 设置 [DIRECTION]
     */
    public void setDirection(String  direction){
        this.direction = direction ;
        this.modify("direction",direction);
    }

    /**
     * 设置 [DATE_FORMAT]
     */
    public void setDateFormat(String  dateFormat){
        this.dateFormat = dateFormat ;
        this.modify("date_format",dateFormat);
    }

    /**
     * 设置 [TIME_FORMAT]
     */
    public void setTimeFormat(String  timeFormat){
        this.timeFormat = timeFormat ;
        this.modify("time_format",timeFormat);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [CODE]
     */
    public void setCode(String  code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [WEEK_START]
     */
    public void setWeekStart(String  weekStart){
        this.weekStart = weekStart ;
        this.modify("week_start",weekStart);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }


}

