package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_update;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_updateService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_updateSearchContext;




@Slf4j
@Api(tags = {"Base_module_update" })
@RestController("odoo_base-base_module_update")
@RequestMapping("")
public class Base_module_updateResource {

    @Autowired
    private IBase_module_updateService base_module_updateService;

    @Autowired
    @Lazy
    private Base_module_updateMapping base_module_updateMapping;













    @PreAuthorize("hasPermission(#base_module_update_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_module_update" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_module_updates/{base_module_update_id}")
    public ResponseEntity<Base_module_updateDTO> get(@PathVariable("base_module_update_id") Integer base_module_update_id) {
        Base_module_update domain = base_module_updateService.get(base_module_update_id);
        Base_module_updateDTO dto = base_module_updateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#base_module_update_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_module_update" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_updates/{base_module_update_id}")

    public ResponseEntity<Base_module_updateDTO> update(@PathVariable("base_module_update_id") Integer base_module_update_id, @RequestBody Base_module_updateDTO base_module_updatedto) {
		Base_module_update domain = base_module_updateMapping.toDomain(base_module_updatedto);
        domain.setId(base_module_update_id);
		base_module_updateService.update(domain);
		Base_module_updateDTO dto = base_module_updateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_module_update_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_module_update" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_updates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_module_updateDTO> base_module_updatedtos) {
        base_module_updateService.updateBatch(base_module_updateMapping.toDomain(base_module_updatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_module_update" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_updates")

    public ResponseEntity<Base_module_updateDTO> create(@RequestBody Base_module_updateDTO base_module_updatedto) {
        Base_module_update domain = base_module_updateMapping.toDomain(base_module_updatedto);
		base_module_updateService.create(domain);
        Base_module_updateDTO dto = base_module_updateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_module_update" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_updates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_module_updateDTO> base_module_updatedtos) {
        base_module_updateService.createBatch(base_module_updateMapping.toDomain(base_module_updatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#base_module_update_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_module_update" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_updates/{base_module_update_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_module_update_id") Integer base_module_update_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_module_updateService.remove(base_module_update_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_module_update" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_updates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_module_updateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_module_update" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_module_updates/fetchdefault")
	public ResponseEntity<List<Base_module_updateDTO>> fetchDefault(Base_module_updateSearchContext context) {
        Page<Base_module_update> domains = base_module_updateService.searchDefault(context) ;
        List<Base_module_updateDTO> list = base_module_updateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_module_update" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_module_updates/searchdefault")
	public ResponseEntity<Page<Base_module_updateDTO>> searchDefault(Base_module_updateSearchContext context) {
        Page<Base_module_update> domains = base_module_updateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_module_updateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_module_update getEntity(){
        return new Base_module_update();
    }

}
