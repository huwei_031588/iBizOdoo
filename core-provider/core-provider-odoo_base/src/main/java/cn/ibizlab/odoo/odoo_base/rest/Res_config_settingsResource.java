package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_config_settingsService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_settingsSearchContext;




@Slf4j
@Api(tags = {"Res_config_settings" })
@RestController("odoo_base-res_config_settings")
@RequestMapping("")
public class Res_config_settingsResource {

    @Autowired
    private IRes_config_settingsService res_config_settingsService;

    @Autowired
    @Lazy
    private Res_config_settingsMapping res_config_settingsMapping;







    @PreAuthorize("hasPermission('Remove',{#res_config_settings_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_config_settings" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_config_settings/{res_config_settings_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_config_settings_id") Integer res_config_settings_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_config_settingsService.remove(res_config_settings_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_config_settings" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_config_settings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_config_settingsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#res_config_settings_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_config_settings" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_config_settings/{res_config_settings_id}")
    public ResponseEntity<Res_config_settingsDTO> get(@PathVariable("res_config_settings_id") Integer res_config_settings_id) {
        Res_config_settings domain = res_config_settingsService.get(res_config_settings_id);
        Res_config_settingsDTO dto = res_config_settingsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#res_config_settings_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_config_settings" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_config_settings/{res_config_settings_id}")

    public ResponseEntity<Res_config_settingsDTO> update(@PathVariable("res_config_settings_id") Integer res_config_settings_id, @RequestBody Res_config_settingsDTO res_config_settingsdto) {
		Res_config_settings domain = res_config_settingsMapping.toDomain(res_config_settingsdto);
        domain.setId(res_config_settings_id);
		res_config_settingsService.update(domain);
		Res_config_settingsDTO dto = res_config_settingsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_config_settings_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_config_settings" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_config_settings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_config_settingsDTO> res_config_settingsdtos) {
        res_config_settingsService.updateBatch(res_config_settingsMapping.toDomain(res_config_settingsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_config_settings" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_settings")

    public ResponseEntity<Res_config_settingsDTO> create(@RequestBody Res_config_settingsDTO res_config_settingsdto) {
        Res_config_settings domain = res_config_settingsMapping.toDomain(res_config_settingsdto);
		res_config_settingsService.create(domain);
        Res_config_settingsDTO dto = res_config_settingsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_config_settings" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_config_settingsDTO> res_config_settingsdtos) {
        res_config_settingsService.createBatch(res_config_settingsMapping.toDomain(res_config_settingsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_config_settings" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_config_settings/fetchdefault")
	public ResponseEntity<List<Res_config_settingsDTO>> fetchDefault(Res_config_settingsSearchContext context) {
        Page<Res_config_settings> domains = res_config_settingsService.searchDefault(context) ;
        List<Res_config_settingsDTO> list = res_config_settingsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_config_settings" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_config_settings/searchdefault")
	public ResponseEntity<Page<Res_config_settingsDTO>> searchDefault(Res_config_settingsSearchContext context) {
        Page<Res_config_settings> domains = res_config_settingsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_config_settingsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_config_settings getEntity(){
        return new Res_config_settings();
    }

}
