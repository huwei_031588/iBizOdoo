package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_upgrade;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_upgradeService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_upgradeSearchContext;




@Slf4j
@Api(tags = {"Base_module_upgrade" })
@RestController("odoo_base-base_module_upgrade")
@RequestMapping("")
public class Base_module_upgradeResource {

    @Autowired
    private IBase_module_upgradeService base_module_upgradeService;

    @Autowired
    @Lazy
    private Base_module_upgradeMapping base_module_upgradeMapping;




    @PreAuthorize("hasPermission(#base_module_upgrade_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_module_upgrade" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_module_upgrades/{base_module_upgrade_id}")
    public ResponseEntity<Base_module_upgradeDTO> get(@PathVariable("base_module_upgrade_id") Integer base_module_upgrade_id) {
        Base_module_upgrade domain = base_module_upgradeService.get(base_module_upgrade_id);
        Base_module_upgradeDTO dto = base_module_upgradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#base_module_upgrade_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_module_upgrade" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_upgrades/{base_module_upgrade_id}")

    public ResponseEntity<Base_module_upgradeDTO> update(@PathVariable("base_module_upgrade_id") Integer base_module_upgrade_id, @RequestBody Base_module_upgradeDTO base_module_upgradedto) {
		Base_module_upgrade domain = base_module_upgradeMapping.toDomain(base_module_upgradedto);
        domain.setId(base_module_upgrade_id);
		base_module_upgradeService.update(domain);
		Base_module_upgradeDTO dto = base_module_upgradeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_module_upgrade_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_module_upgrade" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_upgrades/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_module_upgradeDTO> base_module_upgradedtos) {
        base_module_upgradeService.updateBatch(base_module_upgradeMapping.toDomain(base_module_upgradedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_module_upgrade" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_upgrades")

    public ResponseEntity<Base_module_upgradeDTO> create(@RequestBody Base_module_upgradeDTO base_module_upgradedto) {
        Base_module_upgrade domain = base_module_upgradeMapping.toDomain(base_module_upgradedto);
		base_module_upgradeService.create(domain);
        Base_module_upgradeDTO dto = base_module_upgradeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_module_upgrade" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_upgrades/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_module_upgradeDTO> base_module_upgradedtos) {
        base_module_upgradeService.createBatch(base_module_upgradeMapping.toDomain(base_module_upgradedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#base_module_upgrade_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_module_upgrade" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_upgrades/{base_module_upgrade_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_module_upgrade_id") Integer base_module_upgrade_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_module_upgradeService.remove(base_module_upgrade_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_module_upgrade" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_upgrades/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_module_upgradeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_module_upgrade" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_module_upgrades/fetchdefault")
	public ResponseEntity<List<Base_module_upgradeDTO>> fetchDefault(Base_module_upgradeSearchContext context) {
        Page<Base_module_upgrade> domains = base_module_upgradeService.searchDefault(context) ;
        List<Base_module_upgradeDTO> list = base_module_upgradeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_module_upgrade" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_module_upgrades/searchdefault")
	public ResponseEntity<Page<Base_module_upgradeDTO>> searchDefault(Base_module_upgradeSearchContext context) {
        Page<Base_module_upgrade> domains = base_module_upgradeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_module_upgradeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_module_upgrade getEntity(){
        return new Base_module_upgrade();
    }

}
