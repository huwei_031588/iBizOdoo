package cn.ibizlab.odoo.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Res_config_settingsDTO]
 */
@Data
public class Res_config_settingsDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_QIF]
     *
     */
    @JSONField(name = "module_account_bank_statement_import_qif")
    @JsonProperty("module_account_bank_statement_import_qif")
    private String moduleAccountBankStatementImportQif;

    /**
     * 属性 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_OFX]
     *
     */
    @JSONField(name = "module_account_bank_statement_import_ofx")
    @JsonProperty("module_account_bank_statement_import_ofx")
    private String moduleAccountBankStatementImportOfx;

    /**
     * 属性 [HAS_GOOGLE_MAPS]
     *
     */
    @JSONField(name = "has_google_maps")
    @JsonProperty("has_google_maps")
    private String hasGoogleMaps;

    /**
     * 属性 [MODULE_L10N_EU_SERVICE]
     *
     */
    @JSONField(name = "module_l10n_eu_service")
    @JsonProperty("module_l10n_eu_service")
    private String moduleL10nEuService;

    /**
     * 属性 [CDN_ACTIVATED]
     *
     */
    @JSONField(name = "cdn_activated")
    @JsonProperty("cdn_activated")
    private String cdnActivated;

    /**
     * 属性 [WEBSITE_DEFAULT_LANG_CODE]
     *
     */
    @JSONField(name = "website_default_lang_code")
    @JsonProperty("website_default_lang_code")
    private String websiteDefaultLangCode;

    /**
     * 属性 [AUTH_SIGNUP_RESET_PASSWORD]
     *
     */
    @JSONField(name = "auth_signup_reset_password")
    @JsonProperty("auth_signup_reset_password")
    private String authSignupResetPassword;

    /**
     * 属性 [WEBSITE_COUNTRY_GROUP_IDS]
     *
     */
    @JSONField(name = "website_country_group_ids")
    @JsonProperty("website_country_group_ids")
    private String websiteCountryGroupIds;

    /**
     * 属性 [SOCIAL_INSTAGRAM]
     *
     */
    @JSONField(name = "social_instagram")
    @JsonProperty("social_instagram")
    private String socialInstagram;

    /**
     * 属性 [MODULE_PURCHASE_REQUISITION]
     *
     */
    @JSONField(name = "module_purchase_requisition")
    @JsonProperty("module_purchase_requisition")
    private String modulePurchaseRequisition;

    /**
     * 属性 [MODULE_DELIVERY_EASYPOST]
     *
     */
    @JSONField(name = "module_delivery_easypost")
    @JsonProperty("module_delivery_easypost")
    private String moduleDeliveryEasypost;

    /**
     * 属性 [GROUP_DISCOUNT_PER_SO_LINE]
     *
     */
    @JSONField(name = "group_discount_per_so_line")
    @JsonProperty("group_discount_per_so_line")
    private String groupDiscountPerSoLine;

    /**
     * 属性 [AUTOMATIC_INVOICE]
     *
     */
    @JSONField(name = "automatic_invoice")
    @JsonProperty("automatic_invoice")
    private String automaticInvoice;

    /**
     * 属性 [MODULE_ACCOUNT_PLAID]
     *
     */
    @JSONField(name = "module_account_plaid")
    @JsonProperty("module_account_plaid")
    private String moduleAccountPlaid;

    /**
     * 属性 [GOOGLE_MANAGEMENT_CLIENT_ID]
     *
     */
    @JSONField(name = "google_management_client_id")
    @JsonProperty("google_management_client_id")
    private String googleManagementClientId;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [MODULE_ACCOUNT_CHECK_PRINTING]
     *
     */
    @JSONField(name = "module_account_check_printing")
    @JsonProperty("module_account_check_printing")
    private String moduleAccountCheckPrinting;

    /**
     * 属性 [MODULE_PARTNER_AUTOCOMPLETE]
     *
     */
    @JSONField(name = "module_partner_autocomplete")
    @JsonProperty("module_partner_autocomplete")
    private String modulePartnerAutocomplete;

    /**
     * 属性 [LANGUAGE_COUNT]
     *
     */
    @JSONField(name = "language_count")
    @JsonProperty("language_count")
    private Integer languageCount;

    /**
     * 属性 [MODULE_WEBSITE_LINKS]
     *
     */
    @JSONField(name = "module_website_links")
    @JsonProperty("module_website_links")
    private String moduleWebsiteLinks;

    /**
     * 属性 [WEBSITE_FORM_ENABLE_METADATA]
     *
     */
    @JSONField(name = "website_form_enable_metadata")
    @JsonProperty("website_form_enable_metadata")
    private String websiteFormEnableMetadata;

    /**
     * 属性 [HAS_SOCIAL_NETWORK]
     *
     */
    @JSONField(name = "has_social_network")
    @JsonProperty("has_social_network")
    private String hasSocialNetwork;

    /**
     * 属性 [GROUP_SALE_DELIVERY_ADDRESS]
     *
     */
    @JSONField(name = "group_sale_delivery_address")
    @JsonProperty("group_sale_delivery_address")
    private String groupSaleDeliveryAddress;

    /**
     * 属性 [GOOGLE_ANALYTICS_KEY]
     *
     */
    @JSONField(name = "google_analytics_key")
    @JsonProperty("google_analytics_key")
    private String googleAnalyticsKey;

    /**
     * 属性 [MODULE_AUTH_LDAP]
     *
     */
    @JSONField(name = "module_auth_ldap")
    @JsonProperty("module_auth_ldap")
    private String moduleAuthLdap;

    /**
     * 属性 [SPECIFIC_USER_ACCOUNT]
     *
     */
    @JSONField(name = "specific_user_account")
    @JsonProperty("specific_user_account")
    private String specificUserAccount;

    /**
     * 属性 [MODULE_WEBSITE_HR_RECRUITMENT]
     *
     */
    @JSONField(name = "module_website_hr_recruitment")
    @JsonProperty("module_website_hr_recruitment")
    private String moduleWebsiteHrRecruitment;

    /**
     * 属性 [MODULE_PROJECT_FORECAST]
     *
     */
    @JSONField(name = "module_project_forecast")
    @JsonProperty("module_project_forecast")
    private String moduleProjectForecast;

    /**
     * 属性 [GROUP_STOCK_TRACKING_OWNER]
     *
     */
    @JSONField(name = "group_stock_tracking_owner")
    @JsonProperty("group_stock_tracking_owner")
    private String groupStockTrackingOwner;

    /**
     * 属性 [MODULE_GOOGLE_CALENDAR]
     *
     */
    @JSONField(name = "module_google_calendar")
    @JsonProperty("module_google_calendar")
    private String moduleGoogleCalendar;

    /**
     * 属性 [MODULE_ACCOUNT]
     *
     */
    @JSONField(name = "module_account")
    @JsonProperty("module_account")
    private String moduleAccount;

    /**
     * 属性 [MODULE_GOOGLE_DRIVE]
     *
     */
    @JSONField(name = "module_google_drive")
    @JsonProperty("module_google_drive")
    private String moduleGoogleDrive;

    /**
     * 属性 [POS_PRICELIST_SETTING]
     *
     */
    @JSONField(name = "pos_pricelist_setting")
    @JsonProperty("pos_pricelist_setting")
    private String posPricelistSetting;

    /**
     * 属性 [COMPANY_SHARE_PARTNER]
     *
     */
    @JSONField(name = "company_share_partner")
    @JsonProperty("company_share_partner")
    private String companySharePartner;

    /**
     * 属性 [MODULE_CURRENCY_RATE_LIVE]
     *
     */
    @JSONField(name = "module_currency_rate_live")
    @JsonProperty("module_currency_rate_live")
    private String moduleCurrencyRateLive;

    /**
     * 属性 [GROUP_PROFORMA_SALES]
     *
     */
    @JSONField(name = "group_proforma_sales")
    @JsonProperty("group_proforma_sales")
    private String groupProformaSales;

    /**
     * 属性 [MODULE_DELIVERY_FEDEX]
     *
     */
    @JSONField(name = "module_delivery_fedex")
    @JsonProperty("module_delivery_fedex")
    private String moduleDeliveryFedex;

    /**
     * 属性 [MODULE_PRODUCT_EMAIL_TEMPLATE]
     *
     */
    @JSONField(name = "module_product_email_template")
    @JsonProperty("module_product_email_template")
    private String moduleProductEmailTemplate;

    /**
     * 属性 [SHOW_EFFECT]
     *
     */
    @JSONField(name = "show_effect")
    @JsonProperty("show_effect")
    private String showEffect;

    /**
     * 属性 [DEFAULT_PICKING_POLICY]
     *
     */
    @JSONField(name = "default_picking_policy")
    @JsonProperty("default_picking_policy")
    private String defaultPickingPolicy;

    /**
     * 属性 [SOCIAL_YOUTUBE]
     *
     */
    @JSONField(name = "social_youtube")
    @JsonProperty("social_youtube")
    private String socialYoutube;

    /**
     * 属性 [WEBSITE_COMPANY_ID]
     *
     */
    @JSONField(name = "website_company_id")
    @JsonProperty("website_company_id")
    private Integer websiteCompanyId;

    /**
     * 属性 [MODULE_MRP_BYPRODUCT]
     *
     */
    @JSONField(name = "module_mrp_byproduct")
    @JsonProperty("module_mrp_byproduct")
    private String moduleMrpByproduct;

    /**
     * 属性 [MODULE_DELIVERY_USPS]
     *
     */
    @JSONField(name = "module_delivery_usps")
    @JsonProperty("module_delivery_usps")
    private String moduleDeliveryUsps;

    /**
     * 属性 [MODULE_DELIVERY_DHL]
     *
     */
    @JSONField(name = "module_delivery_dhl")
    @JsonProperty("module_delivery_dhl")
    private String moduleDeliveryDhl;

    /**
     * 属性 [GROUP_PROJECT_RATING]
     *
     */
    @JSONField(name = "group_project_rating")
    @JsonProperty("group_project_rating")
    private String groupProjectRating;

    /**
     * 属性 [GOOGLE_MAPS_API_KEY]
     *
     */
    @JSONField(name = "google_maps_api_key")
    @JsonProperty("google_maps_api_key")
    private String googleMapsApiKey;

    /**
     * 属性 [GROUP_USE_LEAD]
     *
     */
    @JSONField(name = "group_use_lead")
    @JsonProperty("group_use_lead")
    private String groupUseLead;

    /**
     * 属性 [GROUP_STOCK_TRACKING_LOT]
     *
     */
    @JSONField(name = "group_stock_tracking_lot")
    @JsonProperty("group_stock_tracking_lot")
    private String groupStockTrackingLot;

    /**
     * 属性 [GROUP_STOCK_ADV_LOCATION]
     *
     */
    @JSONField(name = "group_stock_adv_location")
    @JsonProperty("group_stock_adv_location")
    private String groupStockAdvLocation;

    /**
     * 属性 [POS_SALES_PRICE]
     *
     */
    @JSONField(name = "pos_sales_price")
    @JsonProperty("pos_sales_price")
    private String posSalesPrice;

    /**
     * 属性 [HAS_CHART_OF_ACCOUNTS]
     *
     */
    @JSONField(name = "has_chart_of_accounts")
    @JsonProperty("has_chart_of_accounts")
    private String hasChartOfAccounts;

    /**
     * 属性 [MULTI_SALES_PRICE_METHOD]
     *
     */
    @JSONField(name = "multi_sales_price_method")
    @JsonProperty("multi_sales_price_method")
    private String multiSalesPriceMethod;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MODULE_PROCUREMENT_JIT]
     *
     */
    @JSONField(name = "module_procurement_jit")
    @JsonProperty("module_procurement_jit")
    private String moduleProcurementJit;

    /**
     * 属性 [MODULE_ACCOUNT_ASSET]
     *
     */
    @JSONField(name = "module_account_asset")
    @JsonProperty("module_account_asset")
    private String moduleAccountAsset;

    /**
     * 属性 [USE_MAILGATEWAY]
     *
     */
    @JSONField(name = "use_mailgateway")
    @JsonProperty("use_mailgateway")
    private String useMailgateway;

    /**
     * 属性 [GROUP_SALE_PRICELIST]
     *
     */
    @JSONField(name = "group_sale_pricelist")
    @JsonProperty("group_sale_pricelist")
    private String groupSalePricelist;

    /**
     * 属性 [CRM_DEFAULT_TEAM_ID]
     *
     */
    @JSONField(name = "crm_default_team_id")
    @JsonProperty("crm_default_team_id")
    private Integer crmDefaultTeamId;

    /**
     * 属性 [GROUP_STOCK_MULTI_LOCATIONS]
     *
     */
    @JSONField(name = "group_stock_multi_locations")
    @JsonProperty("group_stock_multi_locations")
    private String groupStockMultiLocations;

    /**
     * 属性 [USE_MANUFACTURING_LEAD]
     *
     */
    @JSONField(name = "use_manufacturing_lead")
    @JsonProperty("use_manufacturing_lead")
    private String useManufacturingLead;

    /**
     * 属性 [MODULE_GOOGLE_SPREADSHEET]
     *
     */
    @JSONField(name = "module_google_spreadsheet")
    @JsonProperty("module_google_spreadsheet")
    private String moduleGoogleSpreadsheet;

    /**
     * 属性 [SHOW_LINE_SUBTOTALS_TAX_SELECTION]
     *
     */
    @JSONField(name = "show_line_subtotals_tax_selection")
    @JsonProperty("show_line_subtotals_tax_selection")
    private String showLineSubtotalsTaxSelection;

    /**
     * 属性 [LANGUAGE_IDS]
     *
     */
    @JSONField(name = "language_ids")
    @JsonProperty("language_ids")
    private String languageIds;

    /**
     * 属性 [MODULE_WEBSITE_SALE_DELIVERY]
     *
     */
    @JSONField(name = "module_website_sale_delivery")
    @JsonProperty("module_website_sale_delivery")
    private String moduleWebsiteSaleDelivery;

    /**
     * 属性 [MODULE_MRP_MPS]
     *
     */
    @JSONField(name = "module_mrp_mps")
    @JsonProperty("module_mrp_mps")
    private String moduleMrpMps;

    /**
     * 属性 [PARTNER_AUTOCOMPLETE_INSUFFICIENT_CREDIT]
     *
     */
    @JSONField(name = "partner_autocomplete_insufficient_credit")
    @JsonProperty("partner_autocomplete_insufficient_credit")
    private String partnerAutocompleteInsufficientCredit;

    /**
     * 属性 [MODULE_HR_ORG_CHART]
     *
     */
    @JSONField(name = "module_hr_org_chart")
    @JsonProperty("module_hr_org_chart")
    private String moduleHrOrgChart;

    /**
     * 属性 [MODULE_PRODUCT_EXPIRY]
     *
     */
    @JSONField(name = "module_product_expiry")
    @JsonProperty("module_product_expiry")
    private String moduleProductExpiry;

    /**
     * 属性 [MODULE_DELIVERY_BPOST]
     *
     */
    @JSONField(name = "module_delivery_bpost")
    @JsonProperty("module_delivery_bpost")
    private String moduleDeliveryBpost;

    /**
     * 属性 [MODULE_STOCK_BARCODE]
     *
     */
    @JSONField(name = "module_stock_barcode")
    @JsonProperty("module_stock_barcode")
    private String moduleStockBarcode;

    /**
     * 属性 [SOCIAL_GITHUB]
     *
     */
    @JSONField(name = "social_github")
    @JsonProperty("social_github")
    private String socialGithub;

    /**
     * 属性 [MODULE_ACCOUNT_INTRASTAT]
     *
     */
    @JSONField(name = "module_account_intrastat")
    @JsonProperty("module_account_intrastat")
    private String moduleAccountIntrastat;

    /**
     * 属性 [AUTO_DONE_SETTING]
     *
     */
    @JSONField(name = "auto_done_setting")
    @JsonProperty("auto_done_setting")
    private String autoDoneSetting;

    /**
     * 属性 [AUTH_SIGNUP_UNINVITED]
     *
     */
    @JSONField(name = "auth_signup_uninvited")
    @JsonProperty("auth_signup_uninvited")
    private String authSignupUninvited;

    /**
     * 属性 [COMPANY_SHARE_PRODUCT]
     *
     */
    @JSONField(name = "company_share_product")
    @JsonProperty("company_share_product")
    private String companyShareProduct;

    /**
     * 属性 [GROUP_SALE_ORDER_TEMPLATE]
     *
     */
    @JSONField(name = "group_sale_order_template")
    @JsonProperty("group_sale_order_template")
    private String groupSaleOrderTemplate;

    /**
     * 属性 [MODULE_ACCOUNT_SEPA_DIRECT_DEBIT]
     *
     */
    @JSONField(name = "module_account_sepa_direct_debit")
    @JsonProperty("module_account_sepa_direct_debit")
    private String moduleAccountSepaDirectDebit;

    /**
     * 属性 [USE_QUOTATION_VALIDITY_DAYS]
     *
     */
    @JSONField(name = "use_quotation_validity_days")
    @JsonProperty("use_quotation_validity_days")
    private String useQuotationValidityDays;

    /**
     * 属性 [MODULE_ACCOUNT_REPORTS_FOLLOWUP]
     *
     */
    @JSONField(name = "module_account_reports_followup")
    @JsonProperty("module_account_reports_followup")
    private String moduleAccountReportsFollowup;

    /**
     * 属性 [MODULE_ACCOUNT_BATCH_PAYMENT]
     *
     */
    @JSONField(name = "module_account_batch_payment")
    @JsonProperty("module_account_batch_payment")
    private String moduleAccountBatchPayment;

    /**
     * 属性 [SOCIAL_TWITTER]
     *
     */
    @JSONField(name = "social_twitter")
    @JsonProperty("social_twitter")
    private String socialTwitter;

    /**
     * 属性 [MODULE_ACCOUNT_BUDGET]
     *
     */
    @JSONField(name = "module_account_budget")
    @JsonProperty("module_account_budget")
    private String moduleAccountBudget;

    /**
     * 属性 [GROUP_MRP_ROUTINGS]
     *
     */
    @JSONField(name = "group_mrp_routings")
    @JsonProperty("group_mrp_routings")
    private String groupMrpRoutings;

    /**
     * 属性 [GROUP_CASH_ROUNDING]
     *
     */
    @JSONField(name = "group_cash_rounding")
    @JsonProperty("group_cash_rounding")
    private String groupCashRounding;

    /**
     * 属性 [MODULE_STOCK_LANDED_COSTS]
     *
     */
    @JSONField(name = "module_stock_landed_costs")
    @JsonProperty("module_stock_landed_costs")
    private String moduleStockLandedCosts;

    /**
     * 属性 [WEBSITE_NAME]
     *
     */
    @JSONField(name = "website_name")
    @JsonProperty("website_name")
    private String websiteName;

    /**
     * 属性 [MODULE_WEBSITE_SALE_STOCK]
     *
     */
    @JSONField(name = "module_website_sale_stock")
    @JsonProperty("module_website_sale_stock")
    private String moduleWebsiteSaleStock;

    /**
     * 属性 [GROUP_WEBSITE_POPUP_ON_EXIT]
     *
     */
    @JSONField(name = "group_website_popup_on_exit")
    @JsonProperty("group_website_popup_on_exit")
    private String groupWebsitePopupOnExit;

    /**
     * 属性 [MODULE_WEBSITE_EVENT_TRACK]
     *
     */
    @JSONField(name = "module_website_event_track")
    @JsonProperty("module_website_event_track")
    private String moduleWebsiteEventTrack;

    /**
     * 属性 [GROUP_MANAGE_VENDOR_PRICE]
     *
     */
    @JSONField(name = "group_manage_vendor_price")
    @JsonProperty("group_manage_vendor_price")
    private String groupManageVendorPrice;

    /**
     * 属性 [MODULE_DELIVERY]
     *
     */
    @JSONField(name = "module_delivery")
    @JsonProperty("module_delivery")
    private String moduleDelivery;

    /**
     * 属性 [MODULE_ACCOUNT_INVOICE_EXTRACT]
     *
     */
    @JSONField(name = "module_account_invoice_extract")
    @JsonProperty("module_account_invoice_extract")
    private String moduleAccountInvoiceExtract;

    /**
     * 属性 [CHANNEL_ID]
     *
     */
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    private Integer channelId;

    /**
     * 属性 [GROUP_WARNING_SALE]
     *
     */
    @JSONField(name = "group_warning_sale")
    @JsonProperty("group_warning_sale")
    private String groupWarningSale;

    /**
     * 属性 [CDN_URL]
     *
     */
    @JSONField(name = "cdn_url")
    @JsonProperty("cdn_url")
    private String cdnUrl;

    /**
     * 属性 [MODULE_EVENT_BARCODE]
     *
     */
    @JSONField(name = "module_event_barcode")
    @JsonProperty("module_event_barcode")
    private String moduleEventBarcode;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 属性 [SOCIAL_LINKEDIN]
     *
     */
    @JSONField(name = "social_linkedin")
    @JsonProperty("social_linkedin")
    private String socialLinkedin;

    /**
     * 属性 [GROUP_STOCK_MULTI_WAREHOUSES]
     *
     */
    @JSONField(name = "group_stock_multi_warehouses")
    @JsonProperty("group_stock_multi_warehouses")
    private String groupStockMultiWarehouses;

    /**
     * 属性 [SALESPERSON_ID]
     *
     */
    @JSONField(name = "salesperson_id")
    @JsonProperty("salesperson_id")
    private Integer salespersonId;

    /**
     * 属性 [MODULE_ACCOUNT_REPORTS]
     *
     */
    @JSONField(name = "module_account_reports")
    @JsonProperty("module_account_reports")
    private String moduleAccountReports;

    /**
     * 属性 [GROUP_PRODUCT_PRICELIST]
     *
     */
    @JSONField(name = "group_product_pricelist")
    @JsonProperty("group_product_pricelist")
    private String groupProductPricelist;

    /**
     * 属性 [MODULE_CRM_PHONE_VALIDATION]
     *
     */
    @JSONField(name = "module_crm_phone_validation")
    @JsonProperty("module_crm_phone_validation")
    private String moduleCrmPhoneValidation;

    /**
     * 属性 [MODULE_WEBSITE_VERSION]
     *
     */
    @JSONField(name = "module_website_version")
    @JsonProperty("module_website_version")
    private String moduleWebsiteVersion;

    /**
     * 属性 [MODULE_BASE_IMPORT]
     *
     */
    @JSONField(name = "module_base_import")
    @JsonProperty("module_base_import")
    private String moduleBaseImport;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CSV]
     *
     */
    @JSONField(name = "module_account_bank_statement_import_csv")
    @JsonProperty("module_account_bank_statement_import_csv")
    private String moduleAccountBankStatementImportCsv;

    /**
     * 属性 [MODULE_ACCOUNT_TAXCLOUD]
     *
     */
    @JSONField(name = "module_account_taxcloud")
    @JsonProperty("module_account_taxcloud")
    private String moduleAccountTaxcloud;

    /**
     * 属性 [USE_SALE_NOTE]
     *
     */
    @JSONField(name = "use_sale_note")
    @JsonProperty("use_sale_note")
    private String useSaleNote;

    /**
     * 属性 [CART_ABANDONED_DELAY]
     *
     */
    @JSONField(name = "cart_abandoned_delay")
    @JsonProperty("cart_abandoned_delay")
    private Double cartAbandonedDelay;

    /**
     * 属性 [WEBSITE_DOMAIN]
     *
     */
    @JSONField(name = "website_domain")
    @JsonProperty("website_domain")
    private String websiteDomain;

    /**
     * 属性 [MODULE_ACCOUNT_ACCOUNTANT]
     *
     */
    @JSONField(name = "module_account_accountant")
    @JsonProperty("module_account_accountant")
    private String moduleAccountAccountant;

    /**
     * 属性 [MODULE_SALE_MARGIN]
     *
     */
    @JSONField(name = "module_sale_margin")
    @JsonProperty("module_sale_margin")
    private String moduleSaleMargin;

    /**
     * 属性 [DIGEST_EMAILS]
     *
     */
    @JSONField(name = "digest_emails")
    @JsonProperty("digest_emails")
    private String digestEmails;

    /**
     * 属性 [MODULE_PAD]
     *
     */
    @JSONField(name = "module_pad")
    @JsonProperty("module_pad")
    private String modulePad;

    /**
     * 属性 [GROUP_WARNING_ACCOUNT]
     *
     */
    @JSONField(name = "group_warning_account")
    @JsonProperty("group_warning_account")
    private String groupWarningAccount;

    /**
     * 属性 [GROUP_DISPLAY_INCOTERM]
     *
     */
    @JSONField(name = "group_display_incoterm")
    @JsonProperty("group_display_incoterm")
    private String groupDisplayIncoterm;

    /**
     * 属性 [MODULE_WEBSITE_SALE_WISHLIST]
     *
     */
    @JSONField(name = "module_website_sale_wishlist")
    @JsonProperty("module_website_sale_wishlist")
    private String moduleWebsiteSaleWishlist;

    /**
     * 属性 [USER_DEFAULT_RIGHTS]
     *
     */
    @JSONField(name = "user_default_rights")
    @JsonProperty("user_default_rights")
    private String userDefaultRights;

    /**
     * 属性 [DEFAULT_PURCHASE_METHOD]
     *
     */
    @JSONField(name = "default_purchase_method")
    @JsonProperty("default_purchase_method")
    private String defaultPurchaseMethod;

    /**
     * 属性 [GROUP_DELIVERY_INVOICE_ADDRESS]
     *
     */
    @JSONField(name = "group_delivery_invoice_address")
    @JsonProperty("group_delivery_invoice_address")
    private String groupDeliveryInvoiceAddress;

    /**
     * 属性 [GROUP_LOT_ON_DELIVERY_SLIP]
     *
     */
    @JSONField(name = "group_lot_on_delivery_slip")
    @JsonProperty("group_lot_on_delivery_slip")
    private String groupLotOnDeliverySlip;

    /**
     * 属性 [MODULE_EVENT_SALE]
     *
     */
    @JSONField(name = "module_event_sale")
    @JsonProperty("module_event_sale")
    private String moduleEventSale;

    /**
     * 属性 [GROUP_SHOW_LINE_SUBTOTALS_TAX_INCLUDED]
     *
     */
    @JSONField(name = "group_show_line_subtotals_tax_included")
    @JsonProperty("group_show_line_subtotals_tax_included")
    private String groupShowLineSubtotalsTaxIncluded;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [GROUP_PRODUCT_VARIANT]
     *
     */
    @JSONField(name = "group_product_variant")
    @JsonProperty("group_product_variant")
    private String groupProductVariant;

    /**
     * 属性 [MODULE_ACCOUNT_SEPA]
     *
     */
    @JSONField(name = "module_account_sepa")
    @JsonProperty("module_account_sepa")
    private String moduleAccountSepa;

    /**
     * 属性 [GROUP_MULTI_CURRENCY]
     *
     */
    @JSONField(name = "group_multi_currency")
    @JsonProperty("group_multi_currency")
    private String groupMultiCurrency;

    /**
     * 属性 [GROUP_PRODUCTS_IN_BILLS]
     *
     */
    @JSONField(name = "group_products_in_bills")
    @JsonProperty("group_products_in_bills")
    private String groupProductsInBills;

    /**
     * 属性 [GROUP_ANALYTIC_ACCOUNTING]
     *
     */
    @JSONField(name = "group_analytic_accounting")
    @JsonProperty("group_analytic_accounting")
    private String groupAnalyticAccounting;

    /**
     * 属性 [GROUP_STOCK_PACKAGING]
     *
     */
    @JSONField(name = "group_stock_packaging")
    @JsonProperty("group_stock_packaging")
    private String groupStockPackaging;

    /**
     * 属性 [WEBSITE_SLIDE_GOOGLE_APP_KEY]
     *
     */
    @JSONField(name = "website_slide_google_app_key")
    @JsonProperty("website_slide_google_app_key")
    private String websiteSlideGoogleAppKey;

    /**
     * 属性 [PO_ORDER_APPROVAL]
     *
     */
    @JSONField(name = "po_order_approval")
    @JsonProperty("po_order_approval")
    private String poOrderApproval;

    /**
     * 属性 [IS_INSTALLED_SALE]
     *
     */
    @JSONField(name = "is_installed_sale")
    @JsonProperty("is_installed_sale")
    private String isInstalledSale;

    /**
     * 属性 [MODULE_ACCOUNT_PAYMENT]
     *
     */
    @JSONField(name = "module_account_payment")
    @JsonProperty("module_account_payment")
    private String moduleAccountPayment;

    /**
     * 属性 [GROUP_ANALYTIC_TAGS]
     *
     */
    @JSONField(name = "group_analytic_tags")
    @JsonProperty("group_analytic_tags")
    private String groupAnalyticTags;

    /**
     * 属性 [GROUP_SALE_ORDER_DATES]
     *
     */
    @JSONField(name = "group_sale_order_dates")
    @JsonProperty("group_sale_order_dates")
    private String groupSaleOrderDates;

    /**
     * 属性 [MODULE_VOIP]
     *
     */
    @JSONField(name = "module_voip")
    @JsonProperty("module_voip")
    private String moduleVoip;

    /**
     * 属性 [CART_RECOVERY_MAIL_TEMPLATE]
     *
     */
    @JSONField(name = "cart_recovery_mail_template")
    @JsonProperty("cart_recovery_mail_template")
    private Integer cartRecoveryMailTemplate;

    /**
     * 属性 [GROUP_MULTI_WEBSITE]
     *
     */
    @JSONField(name = "group_multi_website")
    @JsonProperty("group_multi_website")
    private String groupMultiWebsite;

    /**
     * 属性 [MODULE_AUTH_OAUTH]
     *
     */
    @JSONField(name = "module_auth_oauth")
    @JsonProperty("module_auth_oauth")
    private String moduleAuthOauth;

    /**
     * 属性 [SALE_DELIVERY_SETTINGS]
     *
     */
    @JSONField(name = "sale_delivery_settings")
    @JsonProperty("sale_delivery_settings")
    private String saleDeliverySettings;

    /**
     * 属性 [MODULE_SALE_QUOTATION_BUILDER]
     *
     */
    @JSONField(name = "module_sale_quotation_builder")
    @JsonProperty("module_sale_quotation_builder")
    private String moduleSaleQuotationBuilder;

    /**
     * 属性 [MODULE_INTER_COMPANY_RULES]
     *
     */
    @JSONField(name = "module_inter_company_rules")
    @JsonProperty("module_inter_company_rules")
    private String moduleInterCompanyRules;

    /**
     * 属性 [USE_SECURITY_LEAD]
     *
     */
    @JSONField(name = "use_security_lead")
    @JsonProperty("use_security_lead")
    private String useSecurityLead;

    /**
     * 属性 [DEFAULT_INVOICE_POLICY]
     *
     */
    @JSONField(name = "default_invoice_policy")
    @JsonProperty("default_invoice_policy")
    private String defaultInvoicePolicy;

    /**
     * 属性 [MULTI_SALES_PRICE]
     *
     */
    @JSONField(name = "multi_sales_price")
    @JsonProperty("multi_sales_price")
    private String multiSalesPrice;

    /**
     * 属性 [LOCK_CONFIRMED_PO]
     *
     */
    @JSONField(name = "lock_confirmed_po")
    @JsonProperty("lock_confirmed_po")
    private String lockConfirmedPo;

    /**
     * 属性 [PRODUCT_WEIGHT_IN_LBS]
     *
     */
    @JSONField(name = "product_weight_in_lbs")
    @JsonProperty("product_weight_in_lbs")
    private String productWeightInLbs;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [HAS_GOOGLE_ANALYTICS_DASHBOARD]
     *
     */
    @JSONField(name = "has_google_analytics_dashboard")
    @JsonProperty("has_google_analytics_dashboard")
    private String hasGoogleAnalyticsDashboard;

    /**
     * 属性 [MODULE_STOCK_PICKING_BATCH]
     *
     */
    @JSONField(name = "module_stock_picking_batch")
    @JsonProperty("module_stock_picking_batch")
    private String moduleStockPickingBatch;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [EXPENSE_ALIAS_PREFIX]
     *
     */
    @JSONField(name = "expense_alias_prefix")
    @JsonProperty("expense_alias_prefix")
    private String expenseAliasPrefix;

    /**
     * 属性 [MODULE_ACCOUNT_DEFERRED_REVENUE]
     *
     */
    @JSONField(name = "module_account_deferred_revenue")
    @JsonProperty("module_account_deferred_revenue")
    private String moduleAccountDeferredRevenue;

    /**
     * 属性 [SOCIAL_FACEBOOK]
     *
     */
    @JSONField(name = "social_facebook")
    @JsonProperty("social_facebook")
    private String socialFacebook;

    /**
     * 属性 [MODULE_WEB_UNSPLASH]
     *
     */
    @JSONField(name = "module_web_unsplash")
    @JsonProperty("module_web_unsplash")
    private String moduleWebUnsplash;

    /**
     * 属性 [GROUP_MASS_MAILING_CAMPAIGN]
     *
     */
    @JSONField(name = "group_mass_mailing_campaign")
    @JsonProperty("group_mass_mailing_campaign")
    private String groupMassMailingCampaign;

    /**
     * 属性 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CAMT]
     *
     */
    @JSONField(name = "module_account_bank_statement_import_camt")
    @JsonProperty("module_account_bank_statement_import_camt")
    private String moduleAccountBankStatementImportCamt;

    /**
     * 属性 [MODULE_PRODUCT_MARGIN]
     *
     */
    @JSONField(name = "module_product_margin")
    @JsonProperty("module_product_margin")
    private String moduleProductMargin;

    /**
     * 属性 [GROUP_SUBTASK_PROJECT]
     *
     */
    @JSONField(name = "group_subtask_project")
    @JsonProperty("group_subtask_project")
    private String groupSubtaskProject;

    /**
     * 属性 [MODULE_ACCOUNT_3WAY_MATCH]
     *
     */
    @JSONField(name = "module_account_3way_match")
    @JsonProperty("module_account_3way_match")
    private String moduleAccount3wayMatch;

    /**
     * 属性 [MODULE_WEBSITE_SALE_DIGITAL]
     *
     */
    @JSONField(name = "module_website_sale_digital")
    @JsonProperty("module_website_sale_digital")
    private String moduleWebsiteSaleDigital;

    /**
     * 属性 [MODULE_SALE_COUPON]
     *
     */
    @JSONField(name = "module_sale_coupon")
    @JsonProperty("module_sale_coupon")
    private String moduleSaleCoupon;

    /**
     * 属性 [SHOW_BLACKLIST_BUTTONS]
     *
     */
    @JSONField(name = "show_blacklist_buttons")
    @JsonProperty("show_blacklist_buttons")
    private String showBlacklistButtons;

    /**
     * 属性 [GROUP_WARNING_STOCK]
     *
     */
    @JSONField(name = "group_warning_stock")
    @JsonProperty("group_warning_stock")
    private String groupWarningStock;

    /**
     * 属性 [GROUP_MULTI_COMPANY]
     *
     */
    @JSONField(name = "group_multi_company")
    @JsonProperty("group_multi_company")
    private String groupMultiCompany;

    /**
     * 属性 [GROUP_ATTENDANCE_USE_PIN]
     *
     */
    @JSONField(name = "group_attendance_use_pin")
    @JsonProperty("group_attendance_use_pin")
    private String groupAttendanceUsePin;

    /**
     * 属性 [SALESTEAM_ID]
     *
     */
    @JSONField(name = "salesteam_id")
    @JsonProperty("salesteam_id")
    private Integer salesteamId;

    /**
     * 属性 [FAVICON]
     *
     */
    @JSONField(name = "favicon")
    @JsonProperty("favicon")
    private byte[] favicon;

    /**
     * 属性 [MODULE_WEBSITE_SALE_COMPARISON]
     *
     */
    @JSONField(name = "module_website_sale_comparison")
    @JsonProperty("module_website_sale_comparison")
    private String moduleWebsiteSaleComparison;

    /**
     * 属性 [AVAILABLE_THRESHOLD]
     *
     */
    @JSONField(name = "available_threshold")
    @JsonProperty("available_threshold")
    private Double availableThreshold;

    /**
     * 属性 [USE_PO_LEAD]
     *
     */
    @JSONField(name = "use_po_lead")
    @JsonProperty("use_po_lead")
    private String usePoLead;

    /**
     * 属性 [GROUP_FISCAL_YEAR]
     *
     */
    @JSONField(name = "group_fiscal_year")
    @JsonProperty("group_fiscal_year")
    private String groupFiscalYear;

    /**
     * 属性 [MODULE_HR_TIMESHEET]
     *
     */
    @JSONField(name = "module_hr_timesheet")
    @JsonProperty("module_hr_timesheet")
    private String moduleHrTimesheet;

    /**
     * 属性 [MODULE_MRP_PLM]
     *
     */
    @JSONField(name = "module_mrp_plm")
    @JsonProperty("module_mrp_plm")
    private String moduleMrpPlm;

    /**
     * 属性 [MODULE_ACCOUNT_YODLEE]
     *
     */
    @JSONField(name = "module_account_yodlee")
    @JsonProperty("module_account_yodlee")
    private String moduleAccountYodlee;

    /**
     * 属性 [CDN_FILTERS]
     *
     */
    @JSONField(name = "cdn_filters")
    @JsonProperty("cdn_filters")
    private String cdnFilters;

    /**
     * 属性 [USE_PROPAGATION_MINIMUM_DELTA]
     *
     */
    @JSONField(name = "use_propagation_minimum_delta")
    @JsonProperty("use_propagation_minimum_delta")
    private String usePropagationMinimumDelta;

    /**
     * 属性 [MODULE_DELIVERY_UPS]
     *
     */
    @JSONField(name = "module_delivery_ups")
    @JsonProperty("module_delivery_ups")
    private String moduleDeliveryUps;

    /**
     * 属性 [FAIL_COUNTER]
     *
     */
    @JSONField(name = "fail_counter")
    @JsonProperty("fail_counter")
    private Integer failCounter;

    /**
     * 属性 [GROUP_STOCK_PRODUCTION_LOT]
     *
     */
    @JSONField(name = "group_stock_production_lot")
    @JsonProperty("group_stock_production_lot")
    private String groupStockProductionLot;

    /**
     * 属性 [HAS_ACCOUNTING_ENTRIES]
     *
     */
    @JSONField(name = "has_accounting_entries")
    @JsonProperty("has_accounting_entries")
    private String hasAccountingEntries;

    /**
     * 属性 [GROUP_UOM]
     *
     */
    @JSONField(name = "group_uom")
    @JsonProperty("group_uom")
    private String groupUom;

    /**
     * 属性 [MASS_MAILING_OUTGOING_MAIL_SERVER]
     *
     */
    @JSONField(name = "mass_mailing_outgoing_mail_server")
    @JsonProperty("mass_mailing_outgoing_mail_server")
    private String massMailingOutgoingMailServer;

    /**
     * 属性 [CRM_ALIAS_PREFIX]
     *
     */
    @JSONField(name = "crm_alias_prefix")
    @JsonProperty("crm_alias_prefix")
    private String crmAliasPrefix;

    /**
     * 属性 [SOCIAL_GOOGLEPLUS]
     *
     */
    @JSONField(name = "social_googleplus")
    @JsonProperty("social_googleplus")
    private String socialGoogleplus;

    /**
     * 属性 [GROUP_ROUTE_SO_LINES]
     *
     */
    @JSONField(name = "group_route_so_lines")
    @JsonProperty("group_route_so_lines")
    private String groupRouteSoLines;

    /**
     * 属性 [GOOGLE_MANAGEMENT_CLIENT_SECRET]
     *
     */
    @JSONField(name = "google_management_client_secret")
    @JsonProperty("google_management_client_secret")
    private String googleManagementClientSecret;

    /**
     * 属性 [SOCIAL_DEFAULT_IMAGE]
     *
     */
    @JSONField(name = "social_default_image")
    @JsonProperty("social_default_image")
    private byte[] socialDefaultImage;

    /**
     * 属性 [HAS_GOOGLE_ANALYTICS]
     *
     */
    @JSONField(name = "has_google_analytics")
    @JsonProperty("has_google_analytics")
    private String hasGoogleAnalytics;

    /**
     * 属性 [MODULE_WEBSITE_EVENT_QUESTIONS]
     *
     */
    @JSONField(name = "module_website_event_questions")
    @JsonProperty("module_website_event_questions")
    private String moduleWebsiteEventQuestions;

    /**
     * 属性 [WEBSITE_DEFAULT_LANG_ID]
     *
     */
    @JSONField(name = "website_default_lang_id")
    @JsonProperty("website_default_lang_id")
    private Integer websiteDefaultLangId;

    /**
     * 属性 [INVENTORY_AVAILABILITY]
     *
     */
    @JSONField(name = "inventory_availability")
    @JsonProperty("inventory_availability")
    private String inventoryAvailability;

    /**
     * 属性 [GROUP_WARNING_PURCHASE]
     *
     */
    @JSONField(name = "group_warning_purchase")
    @JsonProperty("group_warning_purchase")
    private String groupWarningPurchase;

    /**
     * 属性 [MODULE_QUALITY_CONTROL]
     *
     */
    @JSONField(name = "module_quality_control")
    @JsonProperty("module_quality_control")
    private String moduleQualityControl;

    /**
     * 属性 [GENERATE_LEAD_FROM_ALIAS]
     *
     */
    @JSONField(name = "generate_lead_from_alias")
    @JsonProperty("generate_lead_from_alias")
    private String generateLeadFromAlias;

    /**
     * 属性 [SALE_PRICELIST_SETTING]
     *
     */
    @JSONField(name = "sale_pricelist_setting")
    @JsonProperty("sale_pricelist_setting")
    private String salePricelistSetting;

    /**
     * 属性 [GROUP_PRICELIST_ITEM]
     *
     */
    @JSONField(name = "group_pricelist_item")
    @JsonProperty("group_pricelist_item")
    private String groupPricelistItem;

    /**
     * 属性 [EXTERNAL_EMAIL_SERVER_DEFAULT]
     *
     */
    @JSONField(name = "external_email_server_default")
    @JsonProperty("external_email_server_default")
    private String externalEmailServerDefault;

    /**
     * 属性 [UNSPLASH_ACCESS_KEY]
     *
     */
    @JSONField(name = "unsplash_access_key")
    @JsonProperty("unsplash_access_key")
    private String unsplashAccessKey;

    /**
     * 属性 [MODULE_BASE_GENGO]
     *
     */
    @JSONField(name = "module_base_gengo")
    @JsonProperty("module_base_gengo")
    private String moduleBaseGengo;

    /**
     * 属性 [MODULE_WEBSITE_EVENT_SALE]
     *
     */
    @JSONField(name = "module_website_event_sale")
    @JsonProperty("module_website_event_sale")
    private String moduleWebsiteEventSale;

    /**
     * 属性 [CRM_DEFAULT_USER_ID]
     *
     */
    @JSONField(name = "crm_default_user_id")
    @JsonProperty("crm_default_user_id")
    private Integer crmDefaultUserId;

    /**
     * 属性 [MODULE_STOCK_DROPSHIPPING]
     *
     */
    @JSONField(name = "module_stock_dropshipping")
    @JsonProperty("module_stock_dropshipping")
    private String moduleStockDropshipping;

    /**
     * 属性 [MODULE_CRM_REVEAL]
     *
     */
    @JSONField(name = "module_crm_reveal")
    @JsonProperty("module_crm_reveal")
    private String moduleCrmReveal;

    /**
     * 属性 [MASS_MAILING_MAIL_SERVER_ID]
     *
     */
    @JSONField(name = "mass_mailing_mail_server_id")
    @JsonProperty("mass_mailing_mail_server_id")
    private Integer massMailingMailServerId;

    /**
     * 属性 [GROUP_SHOW_LINE_SUBTOTALS_TAX_EXCLUDED]
     *
     */
    @JSONField(name = "group_show_line_subtotals_tax_excluded")
    @JsonProperty("group_show_line_subtotals_tax_excluded")
    private String groupShowLineSubtotalsTaxExcluded;

    /**
     * 属性 [MODULE_HR_RECRUITMENT_SURVEY]
     *
     */
    @JSONField(name = "module_hr_recruitment_survey")
    @JsonProperty("module_hr_recruitment_survey")
    private String moduleHrRecruitmentSurvey;

    /**
     * 属性 [MODULE_MRP_WORKORDER]
     *
     */
    @JSONField(name = "module_mrp_workorder")
    @JsonProperty("module_mrp_workorder")
    private String moduleMrpWorkorder;

    /**
     * 属性 [MODULE_POS_MERCURY]
     *
     */
    @JSONField(name = "module_pos_mercury")
    @JsonProperty("module_pos_mercury")
    private String modulePosMercury;

    /**
     * 属性 [INVOICE_REFERENCE_TYPE]
     *
     */
    @JSONField(name = "invoice_reference_type")
    @JsonProperty("invoice_reference_type")
    private String invoiceReferenceType;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Integer companyCurrencyId;

    /**
     * 属性 [TAX_EXIGIBILITY]
     *
     */
    @JSONField(name = "tax_exigibility")
    @JsonProperty("tax_exigibility")
    private String taxExigibility;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [ACCOUNT_BANK_RECONCILIATION_START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "account_bank_reconciliation_start" , format="yyyy-MM-dd")
    @JsonProperty("account_bank_reconciliation_start")
    private Timestamp accountBankReconciliationStart;

    /**
     * 属性 [TAX_CASH_BASIS_JOURNAL_ID]
     *
     */
    @JSONField(name = "tax_cash_basis_journal_id")
    @JsonProperty("tax_cash_basis_journal_id")
    private Integer taxCashBasisJournalId;

    /**
     * 属性 [PO_LEAD]
     *
     */
    @JSONField(name = "po_lead")
    @JsonProperty("po_lead")
    private Double poLead;

    /**
     * 属性 [SNAILMAIL_COLOR]
     *
     */
    @JSONField(name = "snailmail_color")
    @JsonProperty("snailmail_color")
    private String snailmailColor;

    /**
     * 属性 [PAPERFORMAT_ID]
     *
     */
    @JSONField(name = "paperformat_id")
    @JsonProperty("paperformat_id")
    private Integer paperformatId;

    /**
     * 属性 [PORTAL_CONFIRMATION_SIGN]
     *
     */
    @JSONField(name = "portal_confirmation_sign")
    @JsonProperty("portal_confirmation_sign")
    private String portalConfirmationSign;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [DIGEST_ID_TEXT]
     *
     */
    @JSONField(name = "digest_id_text")
    @JsonProperty("digest_id_text")
    private String digestIdText;

    /**
     * 属性 [SALE_NOTE]
     *
     */
    @JSONField(name = "sale_note")
    @JsonProperty("sale_note")
    private String saleNote;

    /**
     * 属性 [AUTH_SIGNUP_TEMPLATE_USER_ID_TEXT]
     *
     */
    @JSONField(name = "auth_signup_template_user_id_text")
    @JsonProperty("auth_signup_template_user_id_text")
    private String authSignupTemplateUserIdText;

    /**
     * 属性 [INVOICE_IS_PRINT]
     *
     */
    @JSONField(name = "invoice_is_print")
    @JsonProperty("invoice_is_print")
    private String invoiceIsPrint;

    /**
     * 属性 [DEPOSIT_DEFAULT_PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "deposit_default_product_id_text")
    @JsonProperty("deposit_default_product_id_text")
    private String depositDefaultProductIdText;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;

    /**
     * 属性 [PO_LOCK]
     *
     */
    @JSONField(name = "po_lock")
    @JsonProperty("po_lock")
    private String poLock;

    /**
     * 属性 [INVOICE_IS_SNAILMAIL]
     *
     */
    @JSONField(name = "invoice_is_snailmail")
    @JsonProperty("invoice_is_snailmail")
    private String invoiceIsSnailmail;

    /**
     * 属性 [MANUFACTURING_LEAD]
     *
     */
    @JSONField(name = "manufacturing_lead")
    @JsonProperty("manufacturing_lead")
    private Double manufacturingLead;

    /**
     * 属性 [PO_DOUBLE_VALIDATION]
     *
     */
    @JSONField(name = "po_double_validation")
    @JsonProperty("po_double_validation")
    private String poDoubleValidation;

    /**
     * 属性 [REPORT_FOOTER]
     *
     */
    @JSONField(name = "report_footer")
    @JsonProperty("report_footer")
    private String reportFooter;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;

    /**
     * 属性 [INVOICE_IS_EMAIL]
     *
     */
    @JSONField(name = "invoice_is_email")
    @JsonProperty("invoice_is_email")
    private String invoiceIsEmail;

    /**
     * 属性 [PORTAL_CONFIRMATION_PAY]
     *
     */
    @JSONField(name = "portal_confirmation_pay")
    @JsonProperty("portal_confirmation_pay")
    private String portalConfirmationPay;

    /**
     * 属性 [QR_CODE]
     *
     */
    @JSONField(name = "qr_code")
    @JsonProperty("qr_code")
    private String qrCode;

    /**
     * 属性 [SNAILMAIL_DUPLEX]
     *
     */
    @JSONField(name = "snailmail_duplex")
    @JsonProperty("snailmail_duplex")
    private String snailmailDuplex;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [SECURITY_LEAD]
     *
     */
    @JSONField(name = "security_lead")
    @JsonProperty("security_lead")
    private Double securityLead;

    /**
     * 属性 [EXTERNAL_REPORT_LAYOUT_ID]
     *
     */
    @JSONField(name = "external_report_layout_id")
    @JsonProperty("external_report_layout_id")
    private Integer externalReportLayoutId;

    /**
     * 属性 [PURCHASE_TAX_ID]
     *
     */
    @JSONField(name = "purchase_tax_id")
    @JsonProperty("purchase_tax_id")
    private Integer purchaseTaxId;

    /**
     * 属性 [PROPAGATION_MINIMUM_DELTA]
     *
     */
    @JSONField(name = "propagation_minimum_delta")
    @JsonProperty("propagation_minimum_delta")
    private Integer propagationMinimumDelta;

    /**
     * 属性 [QUOTATION_VALIDITY_DAYS]
     *
     */
    @JSONField(name = "quotation_validity_days")
    @JsonProperty("quotation_validity_days")
    private Integer quotationValidityDays;

    /**
     * 属性 [TAX_CALCULATION_ROUNDING_METHOD]
     *
     */
    @JSONField(name = "tax_calculation_rounding_method")
    @JsonProperty("tax_calculation_rounding_method")
    private String taxCalculationRoundingMethod;

    /**
     * 属性 [CURRENCY_EXCHANGE_JOURNAL_ID]
     *
     */
    @JSONField(name = "currency_exchange_journal_id")
    @JsonProperty("currency_exchange_journal_id")
    private Integer currencyExchangeJournalId;

    /**
     * 属性 [PO_DOUBLE_VALIDATION_AMOUNT]
     *
     */
    @JSONField(name = "po_double_validation_amount")
    @JsonProperty("po_double_validation_amount")
    private Double poDoubleValidationAmount;

    /**
     * 属性 [CHART_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;

    /**
     * 属性 [SALE_TAX_ID]
     *
     */
    @JSONField(name = "sale_tax_id")
    @JsonProperty("sale_tax_id")
    private Integer saleTaxId;

    /**
     * 属性 [DEFAULT_SALE_ORDER_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "default_sale_order_template_id_text")
    @JsonProperty("default_sale_order_template_id_text")
    private String defaultSaleOrderTemplateIdText;

    /**
     * 属性 [DEFAULT_SALE_ORDER_TEMPLATE_ID]
     *
     */
    @JSONField(name = "default_sale_order_template_id")
    @JsonProperty("default_sale_order_template_id")
    private Integer defaultSaleOrderTemplateId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [DIGEST_ID]
     *
     */
    @JSONField(name = "digest_id")
    @JsonProperty("digest_id")
    private Integer digestId;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Integer templateId;

    /**
     * 属性 [CHART_TEMPLATE_ID]
     *
     */
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Integer chartTemplateId;

    /**
     * 属性 [DEPOSIT_DEFAULT_PRODUCT_ID]
     *
     */
    @JSONField(name = "deposit_default_product_id")
    @JsonProperty("deposit_default_product_id")
    private Integer depositDefaultProductId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [AUTH_SIGNUP_TEMPLATE_USER_ID]
     *
     */
    @JSONField(name = "auth_signup_template_user_id")
    @JsonProperty("auth_signup_template_user_id")
    private Integer authSignupTemplateUserId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_QIF]
     */
    public void setModuleAccountBankStatementImportQif(String  moduleAccountBankStatementImportQif){
        this.moduleAccountBankStatementImportQif = moduleAccountBankStatementImportQif ;
        this.modify("module_account_bank_statement_import_qif",moduleAccountBankStatementImportQif);
    }

    /**
     * 设置 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_OFX]
     */
    public void setModuleAccountBankStatementImportOfx(String  moduleAccountBankStatementImportOfx){
        this.moduleAccountBankStatementImportOfx = moduleAccountBankStatementImportOfx ;
        this.modify("module_account_bank_statement_import_ofx",moduleAccountBankStatementImportOfx);
    }

    /**
     * 设置 [MODULE_L10N_EU_SERVICE]
     */
    public void setModuleL10nEuService(String  moduleL10nEuService){
        this.moduleL10nEuService = moduleL10nEuService ;
        this.modify("module_l10n_eu_service",moduleL10nEuService);
    }

    /**
     * 设置 [AUTH_SIGNUP_RESET_PASSWORD]
     */
    public void setAuthSignupResetPassword(String  authSignupResetPassword){
        this.authSignupResetPassword = authSignupResetPassword ;
        this.modify("auth_signup_reset_password",authSignupResetPassword);
    }

    /**
     * 设置 [MODULE_PURCHASE_REQUISITION]
     */
    public void setModulePurchaseRequisition(String  modulePurchaseRequisition){
        this.modulePurchaseRequisition = modulePurchaseRequisition ;
        this.modify("module_purchase_requisition",modulePurchaseRequisition);
    }

    /**
     * 设置 [MODULE_DELIVERY_EASYPOST]
     */
    public void setModuleDeliveryEasypost(String  moduleDeliveryEasypost){
        this.moduleDeliveryEasypost = moduleDeliveryEasypost ;
        this.modify("module_delivery_easypost",moduleDeliveryEasypost);
    }

    /**
     * 设置 [GROUP_DISCOUNT_PER_SO_LINE]
     */
    public void setGroupDiscountPerSoLine(String  groupDiscountPerSoLine){
        this.groupDiscountPerSoLine = groupDiscountPerSoLine ;
        this.modify("group_discount_per_so_line",groupDiscountPerSoLine);
    }

    /**
     * 设置 [AUTOMATIC_INVOICE]
     */
    public void setAutomaticInvoice(String  automaticInvoice){
        this.automaticInvoice = automaticInvoice ;
        this.modify("automatic_invoice",automaticInvoice);
    }

    /**
     * 设置 [MODULE_ACCOUNT_PLAID]
     */
    public void setModuleAccountPlaid(String  moduleAccountPlaid){
        this.moduleAccountPlaid = moduleAccountPlaid ;
        this.modify("module_account_plaid",moduleAccountPlaid);
    }

    /**
     * 设置 [MODULE_ACCOUNT_CHECK_PRINTING]
     */
    public void setModuleAccountCheckPrinting(String  moduleAccountCheckPrinting){
        this.moduleAccountCheckPrinting = moduleAccountCheckPrinting ;
        this.modify("module_account_check_printing",moduleAccountCheckPrinting);
    }

    /**
     * 设置 [MODULE_PARTNER_AUTOCOMPLETE]
     */
    public void setModulePartnerAutocomplete(String  modulePartnerAutocomplete){
        this.modulePartnerAutocomplete = modulePartnerAutocomplete ;
        this.modify("module_partner_autocomplete",modulePartnerAutocomplete);
    }

    /**
     * 设置 [MODULE_WEBSITE_LINKS]
     */
    public void setModuleWebsiteLinks(String  moduleWebsiteLinks){
        this.moduleWebsiteLinks = moduleWebsiteLinks ;
        this.modify("module_website_links",moduleWebsiteLinks);
    }

    /**
     * 设置 [GROUP_SALE_DELIVERY_ADDRESS]
     */
    public void setGroupSaleDeliveryAddress(String  groupSaleDeliveryAddress){
        this.groupSaleDeliveryAddress = groupSaleDeliveryAddress ;
        this.modify("group_sale_delivery_address",groupSaleDeliveryAddress);
    }

    /**
     * 设置 [MODULE_AUTH_LDAP]
     */
    public void setModuleAuthLdap(String  moduleAuthLdap){
        this.moduleAuthLdap = moduleAuthLdap ;
        this.modify("module_auth_ldap",moduleAuthLdap);
    }

    /**
     * 设置 [MODULE_WEBSITE_HR_RECRUITMENT]
     */
    public void setModuleWebsiteHrRecruitment(String  moduleWebsiteHrRecruitment){
        this.moduleWebsiteHrRecruitment = moduleWebsiteHrRecruitment ;
        this.modify("module_website_hr_recruitment",moduleWebsiteHrRecruitment);
    }

    /**
     * 设置 [MODULE_PROJECT_FORECAST]
     */
    public void setModuleProjectForecast(String  moduleProjectForecast){
        this.moduleProjectForecast = moduleProjectForecast ;
        this.modify("module_project_forecast",moduleProjectForecast);
    }

    /**
     * 设置 [GROUP_STOCK_TRACKING_OWNER]
     */
    public void setGroupStockTrackingOwner(String  groupStockTrackingOwner){
        this.groupStockTrackingOwner = groupStockTrackingOwner ;
        this.modify("group_stock_tracking_owner",groupStockTrackingOwner);
    }

    /**
     * 设置 [MODULE_GOOGLE_CALENDAR]
     */
    public void setModuleGoogleCalendar(String  moduleGoogleCalendar){
        this.moduleGoogleCalendar = moduleGoogleCalendar ;
        this.modify("module_google_calendar",moduleGoogleCalendar);
    }

    /**
     * 设置 [MODULE_ACCOUNT]
     */
    public void setModuleAccount(String  moduleAccount){
        this.moduleAccount = moduleAccount ;
        this.modify("module_account",moduleAccount);
    }

    /**
     * 设置 [MODULE_GOOGLE_DRIVE]
     */
    public void setModuleGoogleDrive(String  moduleGoogleDrive){
        this.moduleGoogleDrive = moduleGoogleDrive ;
        this.modify("module_google_drive",moduleGoogleDrive);
    }

    /**
     * 设置 [POS_PRICELIST_SETTING]
     */
    public void setPosPricelistSetting(String  posPricelistSetting){
        this.posPricelistSetting = posPricelistSetting ;
        this.modify("pos_pricelist_setting",posPricelistSetting);
    }

    /**
     * 设置 [COMPANY_SHARE_PARTNER]
     */
    public void setCompanySharePartner(String  companySharePartner){
        this.companySharePartner = companySharePartner ;
        this.modify("company_share_partner",companySharePartner);
    }

    /**
     * 设置 [MODULE_CURRENCY_RATE_LIVE]
     */
    public void setModuleCurrencyRateLive(String  moduleCurrencyRateLive){
        this.moduleCurrencyRateLive = moduleCurrencyRateLive ;
        this.modify("module_currency_rate_live",moduleCurrencyRateLive);
    }

    /**
     * 设置 [GROUP_PROFORMA_SALES]
     */
    public void setGroupProformaSales(String  groupProformaSales){
        this.groupProformaSales = groupProformaSales ;
        this.modify("group_proforma_sales",groupProformaSales);
    }

    /**
     * 设置 [MODULE_DELIVERY_FEDEX]
     */
    public void setModuleDeliveryFedex(String  moduleDeliveryFedex){
        this.moduleDeliveryFedex = moduleDeliveryFedex ;
        this.modify("module_delivery_fedex",moduleDeliveryFedex);
    }

    /**
     * 设置 [MODULE_PRODUCT_EMAIL_TEMPLATE]
     */
    public void setModuleProductEmailTemplate(String  moduleProductEmailTemplate){
        this.moduleProductEmailTemplate = moduleProductEmailTemplate ;
        this.modify("module_product_email_template",moduleProductEmailTemplate);
    }

    /**
     * 设置 [SHOW_EFFECT]
     */
    public void setShowEffect(String  showEffect){
        this.showEffect = showEffect ;
        this.modify("show_effect",showEffect);
    }

    /**
     * 设置 [DEFAULT_PICKING_POLICY]
     */
    public void setDefaultPickingPolicy(String  defaultPickingPolicy){
        this.defaultPickingPolicy = defaultPickingPolicy ;
        this.modify("default_picking_policy",defaultPickingPolicy);
    }

    /**
     * 设置 [MODULE_MRP_BYPRODUCT]
     */
    public void setModuleMrpByproduct(String  moduleMrpByproduct){
        this.moduleMrpByproduct = moduleMrpByproduct ;
        this.modify("module_mrp_byproduct",moduleMrpByproduct);
    }

    /**
     * 设置 [MODULE_DELIVERY_USPS]
     */
    public void setModuleDeliveryUsps(String  moduleDeliveryUsps){
        this.moduleDeliveryUsps = moduleDeliveryUsps ;
        this.modify("module_delivery_usps",moduleDeliveryUsps);
    }

    /**
     * 设置 [MODULE_DELIVERY_DHL]
     */
    public void setModuleDeliveryDhl(String  moduleDeliveryDhl){
        this.moduleDeliveryDhl = moduleDeliveryDhl ;
        this.modify("module_delivery_dhl",moduleDeliveryDhl);
    }

    /**
     * 设置 [GROUP_PROJECT_RATING]
     */
    public void setGroupProjectRating(String  groupProjectRating){
        this.groupProjectRating = groupProjectRating ;
        this.modify("group_project_rating",groupProjectRating);
    }

    /**
     * 设置 [GROUP_USE_LEAD]
     */
    public void setGroupUseLead(String  groupUseLead){
        this.groupUseLead = groupUseLead ;
        this.modify("group_use_lead",groupUseLead);
    }

    /**
     * 设置 [GROUP_STOCK_TRACKING_LOT]
     */
    public void setGroupStockTrackingLot(String  groupStockTrackingLot){
        this.groupStockTrackingLot = groupStockTrackingLot ;
        this.modify("group_stock_tracking_lot",groupStockTrackingLot);
    }

    /**
     * 设置 [GROUP_STOCK_ADV_LOCATION]
     */
    public void setGroupStockAdvLocation(String  groupStockAdvLocation){
        this.groupStockAdvLocation = groupStockAdvLocation ;
        this.modify("group_stock_adv_location",groupStockAdvLocation);
    }

    /**
     * 设置 [POS_SALES_PRICE]
     */
    public void setPosSalesPrice(String  posSalesPrice){
        this.posSalesPrice = posSalesPrice ;
        this.modify("pos_sales_price",posSalesPrice);
    }

    /**
     * 设置 [MULTI_SALES_PRICE_METHOD]
     */
    public void setMultiSalesPriceMethod(String  multiSalesPriceMethod){
        this.multiSalesPriceMethod = multiSalesPriceMethod ;
        this.modify("multi_sales_price_method",multiSalesPriceMethod);
    }

    /**
     * 设置 [MODULE_PROCUREMENT_JIT]
     */
    public void setModuleProcurementJit(String  moduleProcurementJit){
        this.moduleProcurementJit = moduleProcurementJit ;
        this.modify("module_procurement_jit",moduleProcurementJit);
    }

    /**
     * 设置 [MODULE_ACCOUNT_ASSET]
     */
    public void setModuleAccountAsset(String  moduleAccountAsset){
        this.moduleAccountAsset = moduleAccountAsset ;
        this.modify("module_account_asset",moduleAccountAsset);
    }

    /**
     * 设置 [USE_MAILGATEWAY]
     */
    public void setUseMailgateway(String  useMailgateway){
        this.useMailgateway = useMailgateway ;
        this.modify("use_mailgateway",useMailgateway);
    }

    /**
     * 设置 [GROUP_SALE_PRICELIST]
     */
    public void setGroupSalePricelist(String  groupSalePricelist){
        this.groupSalePricelist = groupSalePricelist ;
        this.modify("group_sale_pricelist",groupSalePricelist);
    }

    /**
     * 设置 [GROUP_STOCK_MULTI_LOCATIONS]
     */
    public void setGroupStockMultiLocations(String  groupStockMultiLocations){
        this.groupStockMultiLocations = groupStockMultiLocations ;
        this.modify("group_stock_multi_locations",groupStockMultiLocations);
    }

    /**
     * 设置 [USE_MANUFACTURING_LEAD]
     */
    public void setUseManufacturingLead(String  useManufacturingLead){
        this.useManufacturingLead = useManufacturingLead ;
        this.modify("use_manufacturing_lead",useManufacturingLead);
    }

    /**
     * 设置 [MODULE_GOOGLE_SPREADSHEET]
     */
    public void setModuleGoogleSpreadsheet(String  moduleGoogleSpreadsheet){
        this.moduleGoogleSpreadsheet = moduleGoogleSpreadsheet ;
        this.modify("module_google_spreadsheet",moduleGoogleSpreadsheet);
    }

    /**
     * 设置 [SHOW_LINE_SUBTOTALS_TAX_SELECTION]
     */
    public void setShowLineSubtotalsTaxSelection(String  showLineSubtotalsTaxSelection){
        this.showLineSubtotalsTaxSelection = showLineSubtotalsTaxSelection ;
        this.modify("show_line_subtotals_tax_selection",showLineSubtotalsTaxSelection);
    }

    /**
     * 设置 [MODULE_WEBSITE_SALE_DELIVERY]
     */
    public void setModuleWebsiteSaleDelivery(String  moduleWebsiteSaleDelivery){
        this.moduleWebsiteSaleDelivery = moduleWebsiteSaleDelivery ;
        this.modify("module_website_sale_delivery",moduleWebsiteSaleDelivery);
    }

    /**
     * 设置 [MODULE_MRP_MPS]
     */
    public void setModuleMrpMps(String  moduleMrpMps){
        this.moduleMrpMps = moduleMrpMps ;
        this.modify("module_mrp_mps",moduleMrpMps);
    }

    /**
     * 设置 [MODULE_HR_ORG_CHART]
     */
    public void setModuleHrOrgChart(String  moduleHrOrgChart){
        this.moduleHrOrgChart = moduleHrOrgChart ;
        this.modify("module_hr_org_chart",moduleHrOrgChart);
    }

    /**
     * 设置 [MODULE_PRODUCT_EXPIRY]
     */
    public void setModuleProductExpiry(String  moduleProductExpiry){
        this.moduleProductExpiry = moduleProductExpiry ;
        this.modify("module_product_expiry",moduleProductExpiry);
    }

    /**
     * 设置 [MODULE_DELIVERY_BPOST]
     */
    public void setModuleDeliveryBpost(String  moduleDeliveryBpost){
        this.moduleDeliveryBpost = moduleDeliveryBpost ;
        this.modify("module_delivery_bpost",moduleDeliveryBpost);
    }

    /**
     * 设置 [MODULE_STOCK_BARCODE]
     */
    public void setModuleStockBarcode(String  moduleStockBarcode){
        this.moduleStockBarcode = moduleStockBarcode ;
        this.modify("module_stock_barcode",moduleStockBarcode);
    }

    /**
     * 设置 [MODULE_ACCOUNT_INTRASTAT]
     */
    public void setModuleAccountIntrastat(String  moduleAccountIntrastat){
        this.moduleAccountIntrastat = moduleAccountIntrastat ;
        this.modify("module_account_intrastat",moduleAccountIntrastat);
    }

    /**
     * 设置 [AUTO_DONE_SETTING]
     */
    public void setAutoDoneSetting(String  autoDoneSetting){
        this.autoDoneSetting = autoDoneSetting ;
        this.modify("auto_done_setting",autoDoneSetting);
    }

    /**
     * 设置 [COMPANY_SHARE_PRODUCT]
     */
    public void setCompanyShareProduct(String  companyShareProduct){
        this.companyShareProduct = companyShareProduct ;
        this.modify("company_share_product",companyShareProduct);
    }

    /**
     * 设置 [GROUP_SALE_ORDER_TEMPLATE]
     */
    public void setGroupSaleOrderTemplate(String  groupSaleOrderTemplate){
        this.groupSaleOrderTemplate = groupSaleOrderTemplate ;
        this.modify("group_sale_order_template",groupSaleOrderTemplate);
    }

    /**
     * 设置 [MODULE_ACCOUNT_SEPA_DIRECT_DEBIT]
     */
    public void setModuleAccountSepaDirectDebit(String  moduleAccountSepaDirectDebit){
        this.moduleAccountSepaDirectDebit = moduleAccountSepaDirectDebit ;
        this.modify("module_account_sepa_direct_debit",moduleAccountSepaDirectDebit);
    }

    /**
     * 设置 [USE_QUOTATION_VALIDITY_DAYS]
     */
    public void setUseQuotationValidityDays(String  useQuotationValidityDays){
        this.useQuotationValidityDays = useQuotationValidityDays ;
        this.modify("use_quotation_validity_days",useQuotationValidityDays);
    }

    /**
     * 设置 [MODULE_ACCOUNT_REPORTS_FOLLOWUP]
     */
    public void setModuleAccountReportsFollowup(String  moduleAccountReportsFollowup){
        this.moduleAccountReportsFollowup = moduleAccountReportsFollowup ;
        this.modify("module_account_reports_followup",moduleAccountReportsFollowup);
    }

    /**
     * 设置 [MODULE_ACCOUNT_BATCH_PAYMENT]
     */
    public void setModuleAccountBatchPayment(String  moduleAccountBatchPayment){
        this.moduleAccountBatchPayment = moduleAccountBatchPayment ;
        this.modify("module_account_batch_payment",moduleAccountBatchPayment);
    }

    /**
     * 设置 [MODULE_ACCOUNT_BUDGET]
     */
    public void setModuleAccountBudget(String  moduleAccountBudget){
        this.moduleAccountBudget = moduleAccountBudget ;
        this.modify("module_account_budget",moduleAccountBudget);
    }

    /**
     * 设置 [GROUP_MRP_ROUTINGS]
     */
    public void setGroupMrpRoutings(String  groupMrpRoutings){
        this.groupMrpRoutings = groupMrpRoutings ;
        this.modify("group_mrp_routings",groupMrpRoutings);
    }

    /**
     * 设置 [GROUP_CASH_ROUNDING]
     */
    public void setGroupCashRounding(String  groupCashRounding){
        this.groupCashRounding = groupCashRounding ;
        this.modify("group_cash_rounding",groupCashRounding);
    }

    /**
     * 设置 [MODULE_STOCK_LANDED_COSTS]
     */
    public void setModuleStockLandedCosts(String  moduleStockLandedCosts){
        this.moduleStockLandedCosts = moduleStockLandedCosts ;
        this.modify("module_stock_landed_costs",moduleStockLandedCosts);
    }

    /**
     * 设置 [MODULE_WEBSITE_SALE_STOCK]
     */
    public void setModuleWebsiteSaleStock(String  moduleWebsiteSaleStock){
        this.moduleWebsiteSaleStock = moduleWebsiteSaleStock ;
        this.modify("module_website_sale_stock",moduleWebsiteSaleStock);
    }

    /**
     * 设置 [GROUP_WEBSITE_POPUP_ON_EXIT]
     */
    public void setGroupWebsitePopupOnExit(String  groupWebsitePopupOnExit){
        this.groupWebsitePopupOnExit = groupWebsitePopupOnExit ;
        this.modify("group_website_popup_on_exit",groupWebsitePopupOnExit);
    }

    /**
     * 设置 [MODULE_WEBSITE_EVENT_TRACK]
     */
    public void setModuleWebsiteEventTrack(String  moduleWebsiteEventTrack){
        this.moduleWebsiteEventTrack = moduleWebsiteEventTrack ;
        this.modify("module_website_event_track",moduleWebsiteEventTrack);
    }

    /**
     * 设置 [GROUP_MANAGE_VENDOR_PRICE]
     */
    public void setGroupManageVendorPrice(String  groupManageVendorPrice){
        this.groupManageVendorPrice = groupManageVendorPrice ;
        this.modify("group_manage_vendor_price",groupManageVendorPrice);
    }

    /**
     * 设置 [MODULE_DELIVERY]
     */
    public void setModuleDelivery(String  moduleDelivery){
        this.moduleDelivery = moduleDelivery ;
        this.modify("module_delivery",moduleDelivery);
    }

    /**
     * 设置 [MODULE_ACCOUNT_INVOICE_EXTRACT]
     */
    public void setModuleAccountInvoiceExtract(String  moduleAccountInvoiceExtract){
        this.moduleAccountInvoiceExtract = moduleAccountInvoiceExtract ;
        this.modify("module_account_invoice_extract",moduleAccountInvoiceExtract);
    }

    /**
     * 设置 [GROUP_WARNING_SALE]
     */
    public void setGroupWarningSale(String  groupWarningSale){
        this.groupWarningSale = groupWarningSale ;
        this.modify("group_warning_sale",groupWarningSale);
    }

    /**
     * 设置 [MODULE_EVENT_BARCODE]
     */
    public void setModuleEventBarcode(String  moduleEventBarcode){
        this.moduleEventBarcode = moduleEventBarcode ;
        this.modify("module_event_barcode",moduleEventBarcode);
    }

    /**
     * 设置 [ALIAS_DOMAIN]
     */
    public void setAliasDomain(String  aliasDomain){
        this.aliasDomain = aliasDomain ;
        this.modify("alias_domain",aliasDomain);
    }

    /**
     * 设置 [GROUP_STOCK_MULTI_WAREHOUSES]
     */
    public void setGroupStockMultiWarehouses(String  groupStockMultiWarehouses){
        this.groupStockMultiWarehouses = groupStockMultiWarehouses ;
        this.modify("group_stock_multi_warehouses",groupStockMultiWarehouses);
    }

    /**
     * 设置 [MODULE_ACCOUNT_REPORTS]
     */
    public void setModuleAccountReports(String  moduleAccountReports){
        this.moduleAccountReports = moduleAccountReports ;
        this.modify("module_account_reports",moduleAccountReports);
    }

    /**
     * 设置 [GROUP_PRODUCT_PRICELIST]
     */
    public void setGroupProductPricelist(String  groupProductPricelist){
        this.groupProductPricelist = groupProductPricelist ;
        this.modify("group_product_pricelist",groupProductPricelist);
    }

    /**
     * 设置 [MODULE_CRM_PHONE_VALIDATION]
     */
    public void setModuleCrmPhoneValidation(String  moduleCrmPhoneValidation){
        this.moduleCrmPhoneValidation = moduleCrmPhoneValidation ;
        this.modify("module_crm_phone_validation",moduleCrmPhoneValidation);
    }

    /**
     * 设置 [MODULE_WEBSITE_VERSION]
     */
    public void setModuleWebsiteVersion(String  moduleWebsiteVersion){
        this.moduleWebsiteVersion = moduleWebsiteVersion ;
        this.modify("module_website_version",moduleWebsiteVersion);
    }

    /**
     * 设置 [MODULE_BASE_IMPORT]
     */
    public void setModuleBaseImport(String  moduleBaseImport){
        this.moduleBaseImport = moduleBaseImport ;
        this.modify("module_base_import",moduleBaseImport);
    }

    /**
     * 设置 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CSV]
     */
    public void setModuleAccountBankStatementImportCsv(String  moduleAccountBankStatementImportCsv){
        this.moduleAccountBankStatementImportCsv = moduleAccountBankStatementImportCsv ;
        this.modify("module_account_bank_statement_import_csv",moduleAccountBankStatementImportCsv);
    }

    /**
     * 设置 [MODULE_ACCOUNT_TAXCLOUD]
     */
    public void setModuleAccountTaxcloud(String  moduleAccountTaxcloud){
        this.moduleAccountTaxcloud = moduleAccountTaxcloud ;
        this.modify("module_account_taxcloud",moduleAccountTaxcloud);
    }

    /**
     * 设置 [USE_SALE_NOTE]
     */
    public void setUseSaleNote(String  useSaleNote){
        this.useSaleNote = useSaleNote ;
        this.modify("use_sale_note",useSaleNote);
    }

    /**
     * 设置 [MODULE_ACCOUNT_ACCOUNTANT]
     */
    public void setModuleAccountAccountant(String  moduleAccountAccountant){
        this.moduleAccountAccountant = moduleAccountAccountant ;
        this.modify("module_account_accountant",moduleAccountAccountant);
    }

    /**
     * 设置 [MODULE_SALE_MARGIN]
     */
    public void setModuleSaleMargin(String  moduleSaleMargin){
        this.moduleSaleMargin = moduleSaleMargin ;
        this.modify("module_sale_margin",moduleSaleMargin);
    }

    /**
     * 设置 [DIGEST_EMAILS]
     */
    public void setDigestEmails(String  digestEmails){
        this.digestEmails = digestEmails ;
        this.modify("digest_emails",digestEmails);
    }

    /**
     * 设置 [MODULE_PAD]
     */
    public void setModulePad(String  modulePad){
        this.modulePad = modulePad ;
        this.modify("module_pad",modulePad);
    }

    /**
     * 设置 [GROUP_WARNING_ACCOUNT]
     */
    public void setGroupWarningAccount(String  groupWarningAccount){
        this.groupWarningAccount = groupWarningAccount ;
        this.modify("group_warning_account",groupWarningAccount);
    }

    /**
     * 设置 [GROUP_DISPLAY_INCOTERM]
     */
    public void setGroupDisplayIncoterm(String  groupDisplayIncoterm){
        this.groupDisplayIncoterm = groupDisplayIncoterm ;
        this.modify("group_display_incoterm",groupDisplayIncoterm);
    }

    /**
     * 设置 [MODULE_WEBSITE_SALE_WISHLIST]
     */
    public void setModuleWebsiteSaleWishlist(String  moduleWebsiteSaleWishlist){
        this.moduleWebsiteSaleWishlist = moduleWebsiteSaleWishlist ;
        this.modify("module_website_sale_wishlist",moduleWebsiteSaleWishlist);
    }

    /**
     * 设置 [USER_DEFAULT_RIGHTS]
     */
    public void setUserDefaultRights(String  userDefaultRights){
        this.userDefaultRights = userDefaultRights ;
        this.modify("user_default_rights",userDefaultRights);
    }

    /**
     * 设置 [DEFAULT_PURCHASE_METHOD]
     */
    public void setDefaultPurchaseMethod(String  defaultPurchaseMethod){
        this.defaultPurchaseMethod = defaultPurchaseMethod ;
        this.modify("default_purchase_method",defaultPurchaseMethod);
    }

    /**
     * 设置 [GROUP_DELIVERY_INVOICE_ADDRESS]
     */
    public void setGroupDeliveryInvoiceAddress(String  groupDeliveryInvoiceAddress){
        this.groupDeliveryInvoiceAddress = groupDeliveryInvoiceAddress ;
        this.modify("group_delivery_invoice_address",groupDeliveryInvoiceAddress);
    }

    /**
     * 设置 [GROUP_LOT_ON_DELIVERY_SLIP]
     */
    public void setGroupLotOnDeliverySlip(String  groupLotOnDeliverySlip){
        this.groupLotOnDeliverySlip = groupLotOnDeliverySlip ;
        this.modify("group_lot_on_delivery_slip",groupLotOnDeliverySlip);
    }

    /**
     * 设置 [MODULE_EVENT_SALE]
     */
    public void setModuleEventSale(String  moduleEventSale){
        this.moduleEventSale = moduleEventSale ;
        this.modify("module_event_sale",moduleEventSale);
    }

    /**
     * 设置 [GROUP_SHOW_LINE_SUBTOTALS_TAX_INCLUDED]
     */
    public void setGroupShowLineSubtotalsTaxIncluded(String  groupShowLineSubtotalsTaxIncluded){
        this.groupShowLineSubtotalsTaxIncluded = groupShowLineSubtotalsTaxIncluded ;
        this.modify("group_show_line_subtotals_tax_included",groupShowLineSubtotalsTaxIncluded);
    }

    /**
     * 设置 [GROUP_PRODUCT_VARIANT]
     */
    public void setGroupProductVariant(String  groupProductVariant){
        this.groupProductVariant = groupProductVariant ;
        this.modify("group_product_variant",groupProductVariant);
    }

    /**
     * 设置 [MODULE_ACCOUNT_SEPA]
     */
    public void setModuleAccountSepa(String  moduleAccountSepa){
        this.moduleAccountSepa = moduleAccountSepa ;
        this.modify("module_account_sepa",moduleAccountSepa);
    }

    /**
     * 设置 [GROUP_MULTI_CURRENCY]
     */
    public void setGroupMultiCurrency(String  groupMultiCurrency){
        this.groupMultiCurrency = groupMultiCurrency ;
        this.modify("group_multi_currency",groupMultiCurrency);
    }

    /**
     * 设置 [GROUP_PRODUCTS_IN_BILLS]
     */
    public void setGroupProductsInBills(String  groupProductsInBills){
        this.groupProductsInBills = groupProductsInBills ;
        this.modify("group_products_in_bills",groupProductsInBills);
    }

    /**
     * 设置 [GROUP_ANALYTIC_ACCOUNTING]
     */
    public void setGroupAnalyticAccounting(String  groupAnalyticAccounting){
        this.groupAnalyticAccounting = groupAnalyticAccounting ;
        this.modify("group_analytic_accounting",groupAnalyticAccounting);
    }

    /**
     * 设置 [GROUP_STOCK_PACKAGING]
     */
    public void setGroupStockPackaging(String  groupStockPackaging){
        this.groupStockPackaging = groupStockPackaging ;
        this.modify("group_stock_packaging",groupStockPackaging);
    }

    /**
     * 设置 [PO_ORDER_APPROVAL]
     */
    public void setPoOrderApproval(String  poOrderApproval){
        this.poOrderApproval = poOrderApproval ;
        this.modify("po_order_approval",poOrderApproval);
    }

    /**
     * 设置 [IS_INSTALLED_SALE]
     */
    public void setIsInstalledSale(String  isInstalledSale){
        this.isInstalledSale = isInstalledSale ;
        this.modify("is_installed_sale",isInstalledSale);
    }

    /**
     * 设置 [MODULE_ACCOUNT_PAYMENT]
     */
    public void setModuleAccountPayment(String  moduleAccountPayment){
        this.moduleAccountPayment = moduleAccountPayment ;
        this.modify("module_account_payment",moduleAccountPayment);
    }

    /**
     * 设置 [GROUP_ANALYTIC_TAGS]
     */
    public void setGroupAnalyticTags(String  groupAnalyticTags){
        this.groupAnalyticTags = groupAnalyticTags ;
        this.modify("group_analytic_tags",groupAnalyticTags);
    }

    /**
     * 设置 [GROUP_SALE_ORDER_DATES]
     */
    public void setGroupSaleOrderDates(String  groupSaleOrderDates){
        this.groupSaleOrderDates = groupSaleOrderDates ;
        this.modify("group_sale_order_dates",groupSaleOrderDates);
    }

    /**
     * 设置 [MODULE_VOIP]
     */
    public void setModuleVoip(String  moduleVoip){
        this.moduleVoip = moduleVoip ;
        this.modify("module_voip",moduleVoip);
    }

    /**
     * 设置 [GROUP_MULTI_WEBSITE]
     */
    public void setGroupMultiWebsite(String  groupMultiWebsite){
        this.groupMultiWebsite = groupMultiWebsite ;
        this.modify("group_multi_website",groupMultiWebsite);
    }

    /**
     * 设置 [MODULE_AUTH_OAUTH]
     */
    public void setModuleAuthOauth(String  moduleAuthOauth){
        this.moduleAuthOauth = moduleAuthOauth ;
        this.modify("module_auth_oauth",moduleAuthOauth);
    }

    /**
     * 设置 [SALE_DELIVERY_SETTINGS]
     */
    public void setSaleDeliverySettings(String  saleDeliverySettings){
        this.saleDeliverySettings = saleDeliverySettings ;
        this.modify("sale_delivery_settings",saleDeliverySettings);
    }

    /**
     * 设置 [MODULE_SALE_QUOTATION_BUILDER]
     */
    public void setModuleSaleQuotationBuilder(String  moduleSaleQuotationBuilder){
        this.moduleSaleQuotationBuilder = moduleSaleQuotationBuilder ;
        this.modify("module_sale_quotation_builder",moduleSaleQuotationBuilder);
    }

    /**
     * 设置 [MODULE_INTER_COMPANY_RULES]
     */
    public void setModuleInterCompanyRules(String  moduleInterCompanyRules){
        this.moduleInterCompanyRules = moduleInterCompanyRules ;
        this.modify("module_inter_company_rules",moduleInterCompanyRules);
    }

    /**
     * 设置 [USE_SECURITY_LEAD]
     */
    public void setUseSecurityLead(String  useSecurityLead){
        this.useSecurityLead = useSecurityLead ;
        this.modify("use_security_lead",useSecurityLead);
    }

    /**
     * 设置 [DEFAULT_INVOICE_POLICY]
     */
    public void setDefaultInvoicePolicy(String  defaultInvoicePolicy){
        this.defaultInvoicePolicy = defaultInvoicePolicy ;
        this.modify("default_invoice_policy",defaultInvoicePolicy);
    }

    /**
     * 设置 [MULTI_SALES_PRICE]
     */
    public void setMultiSalesPrice(String  multiSalesPrice){
        this.multiSalesPrice = multiSalesPrice ;
        this.modify("multi_sales_price",multiSalesPrice);
    }

    /**
     * 设置 [LOCK_CONFIRMED_PO]
     */
    public void setLockConfirmedPo(String  lockConfirmedPo){
        this.lockConfirmedPo = lockConfirmedPo ;
        this.modify("lock_confirmed_po",lockConfirmedPo);
    }

    /**
     * 设置 [PRODUCT_WEIGHT_IN_LBS]
     */
    public void setProductWeightInLbs(String  productWeightInLbs){
        this.productWeightInLbs = productWeightInLbs ;
        this.modify("product_weight_in_lbs",productWeightInLbs);
    }

    /**
     * 设置 [MODULE_STOCK_PICKING_BATCH]
     */
    public void setModuleStockPickingBatch(String  moduleStockPickingBatch){
        this.moduleStockPickingBatch = moduleStockPickingBatch ;
        this.modify("module_stock_picking_batch",moduleStockPickingBatch);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [EXPENSE_ALIAS_PREFIX]
     */
    public void setExpenseAliasPrefix(String  expenseAliasPrefix){
        this.expenseAliasPrefix = expenseAliasPrefix ;
        this.modify("expense_alias_prefix",expenseAliasPrefix);
    }

    /**
     * 设置 [MODULE_ACCOUNT_DEFERRED_REVENUE]
     */
    public void setModuleAccountDeferredRevenue(String  moduleAccountDeferredRevenue){
        this.moduleAccountDeferredRevenue = moduleAccountDeferredRevenue ;
        this.modify("module_account_deferred_revenue",moduleAccountDeferredRevenue);
    }

    /**
     * 设置 [MODULE_WEB_UNSPLASH]
     */
    public void setModuleWebUnsplash(String  moduleWebUnsplash){
        this.moduleWebUnsplash = moduleWebUnsplash ;
        this.modify("module_web_unsplash",moduleWebUnsplash);
    }

    /**
     * 设置 [GROUP_MASS_MAILING_CAMPAIGN]
     */
    public void setGroupMassMailingCampaign(String  groupMassMailingCampaign){
        this.groupMassMailingCampaign = groupMassMailingCampaign ;
        this.modify("group_mass_mailing_campaign",groupMassMailingCampaign);
    }

    /**
     * 设置 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CAMT]
     */
    public void setModuleAccountBankStatementImportCamt(String  moduleAccountBankStatementImportCamt){
        this.moduleAccountBankStatementImportCamt = moduleAccountBankStatementImportCamt ;
        this.modify("module_account_bank_statement_import_camt",moduleAccountBankStatementImportCamt);
    }

    /**
     * 设置 [MODULE_PRODUCT_MARGIN]
     */
    public void setModuleProductMargin(String  moduleProductMargin){
        this.moduleProductMargin = moduleProductMargin ;
        this.modify("module_product_margin",moduleProductMargin);
    }

    /**
     * 设置 [GROUP_SUBTASK_PROJECT]
     */
    public void setGroupSubtaskProject(String  groupSubtaskProject){
        this.groupSubtaskProject = groupSubtaskProject ;
        this.modify("group_subtask_project",groupSubtaskProject);
    }

    /**
     * 设置 [MODULE_ACCOUNT_3WAY_MATCH]
     */
    public void setModuleAccount3wayMatch(String  moduleAccount3wayMatch){
        this.moduleAccount3wayMatch = moduleAccount3wayMatch ;
        this.modify("module_account_3way_match",moduleAccount3wayMatch);
    }

    /**
     * 设置 [MODULE_WEBSITE_SALE_DIGITAL]
     */
    public void setModuleWebsiteSaleDigital(String  moduleWebsiteSaleDigital){
        this.moduleWebsiteSaleDigital = moduleWebsiteSaleDigital ;
        this.modify("module_website_sale_digital",moduleWebsiteSaleDigital);
    }

    /**
     * 设置 [MODULE_SALE_COUPON]
     */
    public void setModuleSaleCoupon(String  moduleSaleCoupon){
        this.moduleSaleCoupon = moduleSaleCoupon ;
        this.modify("module_sale_coupon",moduleSaleCoupon);
    }

    /**
     * 设置 [SHOW_BLACKLIST_BUTTONS]
     */
    public void setShowBlacklistButtons(String  showBlacklistButtons){
        this.showBlacklistButtons = showBlacklistButtons ;
        this.modify("show_blacklist_buttons",showBlacklistButtons);
    }

    /**
     * 设置 [GROUP_WARNING_STOCK]
     */
    public void setGroupWarningStock(String  groupWarningStock){
        this.groupWarningStock = groupWarningStock ;
        this.modify("group_warning_stock",groupWarningStock);
    }

    /**
     * 设置 [GROUP_MULTI_COMPANY]
     */
    public void setGroupMultiCompany(String  groupMultiCompany){
        this.groupMultiCompany = groupMultiCompany ;
        this.modify("group_multi_company",groupMultiCompany);
    }

    /**
     * 设置 [GROUP_ATTENDANCE_USE_PIN]
     */
    public void setGroupAttendanceUsePin(String  groupAttendanceUsePin){
        this.groupAttendanceUsePin = groupAttendanceUsePin ;
        this.modify("group_attendance_use_pin",groupAttendanceUsePin);
    }

    /**
     * 设置 [MODULE_WEBSITE_SALE_COMPARISON]
     */
    public void setModuleWebsiteSaleComparison(String  moduleWebsiteSaleComparison){
        this.moduleWebsiteSaleComparison = moduleWebsiteSaleComparison ;
        this.modify("module_website_sale_comparison",moduleWebsiteSaleComparison);
    }

    /**
     * 设置 [AVAILABLE_THRESHOLD]
     */
    public void setAvailableThreshold(Double  availableThreshold){
        this.availableThreshold = availableThreshold ;
        this.modify("available_threshold",availableThreshold);
    }

    /**
     * 设置 [USE_PO_LEAD]
     */
    public void setUsePoLead(String  usePoLead){
        this.usePoLead = usePoLead ;
        this.modify("use_po_lead",usePoLead);
    }

    /**
     * 设置 [GROUP_FISCAL_YEAR]
     */
    public void setGroupFiscalYear(String  groupFiscalYear){
        this.groupFiscalYear = groupFiscalYear ;
        this.modify("group_fiscal_year",groupFiscalYear);
    }

    /**
     * 设置 [MODULE_HR_TIMESHEET]
     */
    public void setModuleHrTimesheet(String  moduleHrTimesheet){
        this.moduleHrTimesheet = moduleHrTimesheet ;
        this.modify("module_hr_timesheet",moduleHrTimesheet);
    }

    /**
     * 设置 [MODULE_MRP_PLM]
     */
    public void setModuleMrpPlm(String  moduleMrpPlm){
        this.moduleMrpPlm = moduleMrpPlm ;
        this.modify("module_mrp_plm",moduleMrpPlm);
    }

    /**
     * 设置 [MODULE_ACCOUNT_YODLEE]
     */
    public void setModuleAccountYodlee(String  moduleAccountYodlee){
        this.moduleAccountYodlee = moduleAccountYodlee ;
        this.modify("module_account_yodlee",moduleAccountYodlee);
    }

    /**
     * 设置 [USE_PROPAGATION_MINIMUM_DELTA]
     */
    public void setUsePropagationMinimumDelta(String  usePropagationMinimumDelta){
        this.usePropagationMinimumDelta = usePropagationMinimumDelta ;
        this.modify("use_propagation_minimum_delta",usePropagationMinimumDelta);
    }

    /**
     * 设置 [MODULE_DELIVERY_UPS]
     */
    public void setModuleDeliveryUps(String  moduleDeliveryUps){
        this.moduleDeliveryUps = moduleDeliveryUps ;
        this.modify("module_delivery_ups",moduleDeliveryUps);
    }

    /**
     * 设置 [FAIL_COUNTER]
     */
    public void setFailCounter(Integer  failCounter){
        this.failCounter = failCounter ;
        this.modify("fail_counter",failCounter);
    }

    /**
     * 设置 [GROUP_STOCK_PRODUCTION_LOT]
     */
    public void setGroupStockProductionLot(String  groupStockProductionLot){
        this.groupStockProductionLot = groupStockProductionLot ;
        this.modify("group_stock_production_lot",groupStockProductionLot);
    }

    /**
     * 设置 [GROUP_UOM]
     */
    public void setGroupUom(String  groupUom){
        this.groupUom = groupUom ;
        this.modify("group_uom",groupUom);
    }

    /**
     * 设置 [MASS_MAILING_OUTGOING_MAIL_SERVER]
     */
    public void setMassMailingOutgoingMailServer(String  massMailingOutgoingMailServer){
        this.massMailingOutgoingMailServer = massMailingOutgoingMailServer ;
        this.modify("mass_mailing_outgoing_mail_server",massMailingOutgoingMailServer);
    }

    /**
     * 设置 [CRM_ALIAS_PREFIX]
     */
    public void setCrmAliasPrefix(String  crmAliasPrefix){
        this.crmAliasPrefix = crmAliasPrefix ;
        this.modify("crm_alias_prefix",crmAliasPrefix);
    }

    /**
     * 设置 [GROUP_ROUTE_SO_LINES]
     */
    public void setGroupRouteSoLines(String  groupRouteSoLines){
        this.groupRouteSoLines = groupRouteSoLines ;
        this.modify("group_route_so_lines",groupRouteSoLines);
    }

    /**
     * 设置 [MODULE_WEBSITE_EVENT_QUESTIONS]
     */
    public void setModuleWebsiteEventQuestions(String  moduleWebsiteEventQuestions){
        this.moduleWebsiteEventQuestions = moduleWebsiteEventQuestions ;
        this.modify("module_website_event_questions",moduleWebsiteEventQuestions);
    }

    /**
     * 设置 [INVENTORY_AVAILABILITY]
     */
    public void setInventoryAvailability(String  inventoryAvailability){
        this.inventoryAvailability = inventoryAvailability ;
        this.modify("inventory_availability",inventoryAvailability);
    }

    /**
     * 设置 [GROUP_WARNING_PURCHASE]
     */
    public void setGroupWarningPurchase(String  groupWarningPurchase){
        this.groupWarningPurchase = groupWarningPurchase ;
        this.modify("group_warning_purchase",groupWarningPurchase);
    }

    /**
     * 设置 [MODULE_QUALITY_CONTROL]
     */
    public void setModuleQualityControl(String  moduleQualityControl){
        this.moduleQualityControl = moduleQualityControl ;
        this.modify("module_quality_control",moduleQualityControl);
    }

    /**
     * 设置 [GENERATE_LEAD_FROM_ALIAS]
     */
    public void setGenerateLeadFromAlias(String  generateLeadFromAlias){
        this.generateLeadFromAlias = generateLeadFromAlias ;
        this.modify("generate_lead_from_alias",generateLeadFromAlias);
    }

    /**
     * 设置 [SALE_PRICELIST_SETTING]
     */
    public void setSalePricelistSetting(String  salePricelistSetting){
        this.salePricelistSetting = salePricelistSetting ;
        this.modify("sale_pricelist_setting",salePricelistSetting);
    }

    /**
     * 设置 [GROUP_PRICELIST_ITEM]
     */
    public void setGroupPricelistItem(String  groupPricelistItem){
        this.groupPricelistItem = groupPricelistItem ;
        this.modify("group_pricelist_item",groupPricelistItem);
    }

    /**
     * 设置 [EXTERNAL_EMAIL_SERVER_DEFAULT]
     */
    public void setExternalEmailServerDefault(String  externalEmailServerDefault){
        this.externalEmailServerDefault = externalEmailServerDefault ;
        this.modify("external_email_server_default",externalEmailServerDefault);
    }

    /**
     * 设置 [UNSPLASH_ACCESS_KEY]
     */
    public void setUnsplashAccessKey(String  unsplashAccessKey){
        this.unsplashAccessKey = unsplashAccessKey ;
        this.modify("unsplash_access_key",unsplashAccessKey);
    }

    /**
     * 设置 [MODULE_BASE_GENGO]
     */
    public void setModuleBaseGengo(String  moduleBaseGengo){
        this.moduleBaseGengo = moduleBaseGengo ;
        this.modify("module_base_gengo",moduleBaseGengo);
    }

    /**
     * 设置 [MODULE_WEBSITE_EVENT_SALE]
     */
    public void setModuleWebsiteEventSale(String  moduleWebsiteEventSale){
        this.moduleWebsiteEventSale = moduleWebsiteEventSale ;
        this.modify("module_website_event_sale",moduleWebsiteEventSale);
    }

    /**
     * 设置 [MODULE_STOCK_DROPSHIPPING]
     */
    public void setModuleStockDropshipping(String  moduleStockDropshipping){
        this.moduleStockDropshipping = moduleStockDropshipping ;
        this.modify("module_stock_dropshipping",moduleStockDropshipping);
    }

    /**
     * 设置 [MODULE_CRM_REVEAL]
     */
    public void setModuleCrmReveal(String  moduleCrmReveal){
        this.moduleCrmReveal = moduleCrmReveal ;
        this.modify("module_crm_reveal",moduleCrmReveal);
    }

    /**
     * 设置 [MASS_MAILING_MAIL_SERVER_ID]
     */
    public void setMassMailingMailServerId(Integer  massMailingMailServerId){
        this.massMailingMailServerId = massMailingMailServerId ;
        this.modify("mass_mailing_mail_server_id",massMailingMailServerId);
    }

    /**
     * 设置 [GROUP_SHOW_LINE_SUBTOTALS_TAX_EXCLUDED]
     */
    public void setGroupShowLineSubtotalsTaxExcluded(String  groupShowLineSubtotalsTaxExcluded){
        this.groupShowLineSubtotalsTaxExcluded = groupShowLineSubtotalsTaxExcluded ;
        this.modify("group_show_line_subtotals_tax_excluded",groupShowLineSubtotalsTaxExcluded);
    }

    /**
     * 设置 [MODULE_HR_RECRUITMENT_SURVEY]
     */
    public void setModuleHrRecruitmentSurvey(String  moduleHrRecruitmentSurvey){
        this.moduleHrRecruitmentSurvey = moduleHrRecruitmentSurvey ;
        this.modify("module_hr_recruitment_survey",moduleHrRecruitmentSurvey);
    }

    /**
     * 设置 [MODULE_MRP_WORKORDER]
     */
    public void setModuleMrpWorkorder(String  moduleMrpWorkorder){
        this.moduleMrpWorkorder = moduleMrpWorkorder ;
        this.modify("module_mrp_workorder",moduleMrpWorkorder);
    }

    /**
     * 设置 [MODULE_POS_MERCURY]
     */
    public void setModulePosMercury(String  modulePosMercury){
        this.modulePosMercury = modulePosMercury ;
        this.modify("module_pos_mercury",modulePosMercury);
    }

    /**
     * 设置 [DEFAULT_SALE_ORDER_TEMPLATE_ID]
     */
    public void setDefaultSaleOrderTemplateId(Integer  defaultSaleOrderTemplateId){
        this.defaultSaleOrderTemplateId = defaultSaleOrderTemplateId ;
        this.modify("default_sale_order_template_id",defaultSaleOrderTemplateId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [DIGEST_ID]
     */
    public void setDigestId(Integer  digestId){
        this.digestId = digestId ;
        this.modify("digest_id",digestId);
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    public void setTemplateId(Integer  templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }

    /**
     * 设置 [CHART_TEMPLATE_ID]
     */
    public void setChartTemplateId(Integer  chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }

    /**
     * 设置 [DEPOSIT_DEFAULT_PRODUCT_ID]
     */
    public void setDepositDefaultProductId(Integer  depositDefaultProductId){
        this.depositDefaultProductId = depositDefaultProductId ;
        this.modify("deposit_default_product_id",depositDefaultProductId);
    }

    /**
     * 设置 [AUTH_SIGNUP_TEMPLATE_USER_ID]
     */
    public void setAuthSignupTemplateUserId(Integer  authSignupTemplateUserId){
        this.authSignupTemplateUserId = authSignupTemplateUserId ;
        this.modify("auth_signup_template_user_id",authSignupTemplateUserId);
    }


}

