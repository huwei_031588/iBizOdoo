package cn.ibizlab.odoo.odoo_base.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-base")
@Data
public class odoo_baseServiceProperties {

	private boolean enabled;

	private boolean auth;


}