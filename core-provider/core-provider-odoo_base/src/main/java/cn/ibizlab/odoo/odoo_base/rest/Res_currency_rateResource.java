package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency_rate;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_currency_rateService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currency_rateSearchContext;




@Slf4j
@Api(tags = {"Res_currency_rate" })
@RestController("odoo_base-res_currency_rate")
@RequestMapping("")
public class Res_currency_rateResource {

    @Autowired
    private IRes_currency_rateService res_currency_rateService;

    @Autowired
    @Lazy
    private Res_currency_rateMapping res_currency_rateMapping;










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_currency_rate" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currency_rates")

    public ResponseEntity<Res_currency_rateDTO> create(@RequestBody Res_currency_rateDTO res_currency_ratedto) {
        Res_currency_rate domain = res_currency_rateMapping.toDomain(res_currency_ratedto);
		res_currency_rateService.create(domain);
        Res_currency_rateDTO dto = res_currency_rateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_currency_rate" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currency_rates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_currency_rateDTO> res_currency_ratedtos) {
        res_currency_rateService.createBatch(res_currency_rateMapping.toDomain(res_currency_ratedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_currency_rate_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_currency_rate" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_currency_rates/{res_currency_rate_id}")
    public ResponseEntity<Res_currency_rateDTO> get(@PathVariable("res_currency_rate_id") Integer res_currency_rate_id) {
        Res_currency_rate domain = res_currency_rateService.get(res_currency_rate_id);
        Res_currency_rateDTO dto = res_currency_rateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#res_currency_rate_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_currency_rate" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_currency_rates/{res_currency_rate_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_currency_rate_id") Integer res_currency_rate_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_currency_rateService.remove(res_currency_rate_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_currency_rate" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_currency_rates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_currency_rateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_currency_rate_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_currency_rate" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_currency_rates/{res_currency_rate_id}")

    public ResponseEntity<Res_currency_rateDTO> update(@PathVariable("res_currency_rate_id") Integer res_currency_rate_id, @RequestBody Res_currency_rateDTO res_currency_ratedto) {
		Res_currency_rate domain = res_currency_rateMapping.toDomain(res_currency_ratedto);
        domain.setId(res_currency_rate_id);
		res_currency_rateService.update(domain);
		Res_currency_rateDTO dto = res_currency_rateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_currency_rate_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_currency_rate" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_currency_rates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_currency_rateDTO> res_currency_ratedtos) {
        res_currency_rateService.updateBatch(res_currency_rateMapping.toDomain(res_currency_ratedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_currency_rate" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_currency_rates/fetchdefault")
	public ResponseEntity<List<Res_currency_rateDTO>> fetchDefault(Res_currency_rateSearchContext context) {
        Page<Res_currency_rate> domains = res_currency_rateService.searchDefault(context) ;
        List<Res_currency_rateDTO> list = res_currency_rateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_currency_rate" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_currency_rates/searchdefault")
	public ResponseEntity<Page<Res_currency_rateDTO>> searchDefault(Res_currency_rateSearchContext context) {
        Page<Res_currency_rate> domains = res_currency_rateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_currency_rateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_currency_rate getEntity(){
        return new Res_currency_rate();
    }

}
