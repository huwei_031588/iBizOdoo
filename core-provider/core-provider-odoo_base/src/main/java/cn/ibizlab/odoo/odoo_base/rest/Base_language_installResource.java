package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_install;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_installService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_installSearchContext;




@Slf4j
@Api(tags = {"Base_language_install" })
@RestController("odoo_base-base_language_install")
@RequestMapping("")
public class Base_language_installResource {

    @Autowired
    private IBase_language_installService base_language_installService;

    @Autowired
    @Lazy
    private Base_language_installMapping base_language_installMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_language_install" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_installs")

    public ResponseEntity<Base_language_installDTO> create(@RequestBody Base_language_installDTO base_language_installdto) {
        Base_language_install domain = base_language_installMapping.toDomain(base_language_installdto);
		base_language_installService.create(domain);
        Base_language_installDTO dto = base_language_installMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_language_install" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_installs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_language_installDTO> base_language_installdtos) {
        base_language_installService.createBatch(base_language_installMapping.toDomain(base_language_installdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#base_language_install_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_language_install" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_installs/{base_language_install_id}")

    public ResponseEntity<Base_language_installDTO> update(@PathVariable("base_language_install_id") Integer base_language_install_id, @RequestBody Base_language_installDTO base_language_installdto) {
		Base_language_install domain = base_language_installMapping.toDomain(base_language_installdto);
        domain.setId(base_language_install_id);
		base_language_installService.update(domain);
		Base_language_installDTO dto = base_language_installMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_language_install_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_language_install" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_installs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_language_installDTO> base_language_installdtos) {
        base_language_installService.updateBatch(base_language_installMapping.toDomain(base_language_installdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#base_language_install_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_language_install" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_language_installs/{base_language_install_id}")
    public ResponseEntity<Base_language_installDTO> get(@PathVariable("base_language_install_id") Integer base_language_install_id) {
        Base_language_install domain = base_language_installService.get(base_language_install_id);
        Base_language_installDTO dto = base_language_installMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#base_language_install_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_language_install" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_installs/{base_language_install_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_language_install_id") Integer base_language_install_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_language_installService.remove(base_language_install_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_language_install" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_installs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_language_installService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_language_install" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_language_installs/fetchdefault")
	public ResponseEntity<List<Base_language_installDTO>> fetchDefault(Base_language_installSearchContext context) {
        Page<Base_language_install> domains = base_language_installService.searchDefault(context) ;
        List<Base_language_installDTO> list = base_language_installMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_language_install" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_language_installs/searchdefault")
	public ResponseEntity<Page<Base_language_installDTO>> searchDefault(Base_language_installSearchContext context) {
        Page<Base_language_install> domains = base_language_installService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_language_installMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_language_install getEntity(){
        return new Base_language_install();
    }

}
