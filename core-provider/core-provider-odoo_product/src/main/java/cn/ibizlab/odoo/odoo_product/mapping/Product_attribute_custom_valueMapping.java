package cn.ibizlab.odoo.odoo_product.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_custom_value;
import cn.ibizlab.odoo.odoo_product.dto.Product_attribute_custom_valueDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Product_attribute_custom_valueMapping extends MappingBase<Product_attribute_custom_valueDTO, Product_attribute_custom_value> {


}

