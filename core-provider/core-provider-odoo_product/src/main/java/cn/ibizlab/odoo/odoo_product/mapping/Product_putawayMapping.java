package cn.ibizlab.odoo.odoo_product.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway;
import cn.ibizlab.odoo.odoo_product.dto.Product_putawayDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Product_putawayMapping extends MappingBase<Product_putawayDTO, Product_putaway> {


}

