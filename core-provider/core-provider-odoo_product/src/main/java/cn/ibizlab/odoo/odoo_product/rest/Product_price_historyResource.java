package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_history;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_price_historyService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_historySearchContext;




@Slf4j
@Api(tags = {"Product_price_history" })
@RestController("odoo_product-product_price_history")
@RequestMapping("")
public class Product_price_historyResource {

    @Autowired
    private IProduct_price_historyService product_price_historyService;

    @Autowired
    @Lazy
    private Product_price_historyMapping product_price_historyMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_price_history" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_histories")

    public ResponseEntity<Product_price_historyDTO> create(@RequestBody Product_price_historyDTO product_price_historydto) {
        Product_price_history domain = product_price_historyMapping.toDomain(product_price_historydto);
		product_price_historyService.create(domain);
        Product_price_historyDTO dto = product_price_historyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_price_history" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_histories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_price_historyDTO> product_price_historydtos) {
        product_price_historyService.createBatch(product_price_historyMapping.toDomain(product_price_historydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#product_price_history_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_price_history" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_price_histories/{product_price_history_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_price_history_id") Integer product_price_history_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_price_historyService.remove(product_price_history_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_price_history" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_price_histories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_price_historyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#product_price_history_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_price_history" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_price_histories/{product_price_history_id}")
    public ResponseEntity<Product_price_historyDTO> get(@PathVariable("product_price_history_id") Integer product_price_history_id) {
        Product_price_history domain = product_price_historyService.get(product_price_history_id);
        Product_price_historyDTO dto = product_price_historyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#product_price_history_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_price_history" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_price_histories/{product_price_history_id}")

    public ResponseEntity<Product_price_historyDTO> update(@PathVariable("product_price_history_id") Integer product_price_history_id, @RequestBody Product_price_historyDTO product_price_historydto) {
		Product_price_history domain = product_price_historyMapping.toDomain(product_price_historydto);
        domain.setId(product_price_history_id);
		product_price_historyService.update(domain);
		Product_price_historyDTO dto = product_price_historyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_price_history_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_price_history" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_price_histories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_price_historyDTO> product_price_historydtos) {
        product_price_historyService.updateBatch(product_price_historyMapping.toDomain(product_price_historydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_price_history" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_price_histories/fetchdefault")
	public ResponseEntity<List<Product_price_historyDTO>> fetchDefault(Product_price_historySearchContext context) {
        Page<Product_price_history> domains = product_price_historyService.searchDefault(context) ;
        List<Product_price_historyDTO> list = product_price_historyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_price_history" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_price_histories/searchdefault")
	public ResponseEntity<Page<Product_price_historyDTO>> searchDefault(Product_price_historySearchContext context) {
        Page<Product_price_history> domains = product_price_historyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_price_historyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_price_history getEntity(){
        return new Product_price_history();
    }

}
