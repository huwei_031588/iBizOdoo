package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_image;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_imageService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_imageSearchContext;




@Slf4j
@Api(tags = {"Product_image" })
@RestController("odoo_product-product_image")
@RequestMapping("")
public class Product_imageResource {

    @Autowired
    private IProduct_imageService product_imageService;

    @Autowired
    @Lazy
    private Product_imageMapping product_imageMapping;




    @PreAuthorize("hasPermission(#product_image_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_image" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_images/{product_image_id}")

    public ResponseEntity<Product_imageDTO> update(@PathVariable("product_image_id") Integer product_image_id, @RequestBody Product_imageDTO product_imagedto) {
		Product_image domain = product_imageMapping.toDomain(product_imagedto);
        domain.setId(product_image_id);
		product_imageService.update(domain);
		Product_imageDTO dto = product_imageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_image_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_image" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_images/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_imageDTO> product_imagedtos) {
        product_imageService.updateBatch(product_imageMapping.toDomain(product_imagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#product_image_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_image" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_images/{product_image_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_image_id") Integer product_image_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_imageService.remove(product_image_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_image" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_images/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_imageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#product_image_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_image" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_images/{product_image_id}")
    public ResponseEntity<Product_imageDTO> get(@PathVariable("product_image_id") Integer product_image_id) {
        Product_image domain = product_imageService.get(product_image_id);
        Product_imageDTO dto = product_imageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_image" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_images")

    public ResponseEntity<Product_imageDTO> create(@RequestBody Product_imageDTO product_imagedto) {
        Product_image domain = product_imageMapping.toDomain(product_imagedto);
		product_imageService.create(domain);
        Product_imageDTO dto = product_imageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_image" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_images/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_imageDTO> product_imagedtos) {
        product_imageService.createBatch(product_imageMapping.toDomain(product_imagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_image" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_images/fetchdefault")
	public ResponseEntity<List<Product_imageDTO>> fetchDefault(Product_imageSearchContext context) {
        Page<Product_image> domains = product_imageService.searchDefault(context) ;
        List<Product_imageDTO> list = product_imageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_image" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_images/searchdefault")
	public ResponseEntity<Page<Product_imageDTO>> searchDefault(Product_imageSearchContext context) {
        Page<Product_image> domains = product_imageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_imageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_image getEntity(){
        return new Product_image();
    }

}
