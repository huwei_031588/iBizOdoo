package cn.ibizlab.odoo.odoo_product.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template;
import cn.ibizlab.odoo.odoo_product.dto.Product_templateDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Product_templateMapping extends MappingBase<Product_templateDTO, Product_template> {


}

