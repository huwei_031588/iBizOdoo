package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_supplierinfo;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_supplierinfoService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_supplierinfoSearchContext;




@Slf4j
@Api(tags = {"Product_supplierinfo" })
@RestController("odoo_product-product_supplierinfo")
@RequestMapping("")
public class Product_supplierinfoResource {

    @Autowired
    private IProduct_supplierinfoService product_supplierinfoService;

    @Autowired
    @Lazy
    private Product_supplierinfoMapping product_supplierinfoMapping;




    @PreAuthorize("hasPermission('Remove',{#product_supplierinfo_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_supplierinfo" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_supplierinfos/{product_supplierinfo_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_supplierinfo_id") Integer product_supplierinfo_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_supplierinfoService.remove(product_supplierinfo_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_supplierinfo" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_supplierinfos/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_supplierinfoService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_supplierinfo" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_supplierinfos")

    public ResponseEntity<Product_supplierinfoDTO> create(@RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
		product_supplierinfoService.create(domain);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_supplierinfo" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_supplierinfos/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        product_supplierinfoService.createBatch(product_supplierinfoMapping.toDomain(product_supplierinfodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_supplierinfo_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_supplierinfo" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> get(@PathVariable("product_supplierinfo_id") Integer product_supplierinfo_id) {
        Product_supplierinfo domain = product_supplierinfoService.get(product_supplierinfo_id);
        Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission(#product_supplierinfo_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_supplierinfo" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_supplierinfos/{product_supplierinfo_id}")

    public ResponseEntity<Product_supplierinfoDTO> update(@PathVariable("product_supplierinfo_id") Integer product_supplierinfo_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
		Product_supplierinfo domain = product_supplierinfoMapping.toDomain(product_supplierinfodto);
        domain.setId(product_supplierinfo_id);
		product_supplierinfoService.update(domain);
		Product_supplierinfoDTO dto = product_supplierinfoMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_supplierinfo_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_supplierinfo" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_supplierinfos/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        product_supplierinfoService.updateBatch(product_supplierinfoMapping.toDomain(product_supplierinfodtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_supplierinfo" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_supplierinfos/fetchdefault")
	public ResponseEntity<List<Product_supplierinfoDTO>> fetchDefault(Product_supplierinfoSearchContext context) {
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
        List<Product_supplierinfoDTO> list = product_supplierinfoMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_supplierinfo" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_supplierinfos/searchdefault")
	public ResponseEntity<Page<Product_supplierinfoDTO>> searchDefault(Product_supplierinfoSearchContext context) {
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_supplierinfoMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_supplierinfo getEntity(){
        return new Product_supplierinfo();
    }

}
