package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_attributeService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attributeSearchContext;




@Slf4j
@Api(tags = {"Product_attribute" })
@RestController("odoo_product-product_attribute")
@RequestMapping("")
public class Product_attributeResource {

    @Autowired
    private IProduct_attributeService product_attributeService;

    @Autowired
    @Lazy
    private Product_attributeMapping product_attributeMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_attribute" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attributes")

    public ResponseEntity<Product_attributeDTO> create(@RequestBody Product_attributeDTO product_attributedto) {
        Product_attribute domain = product_attributeMapping.toDomain(product_attributedto);
		product_attributeService.create(domain);
        Product_attributeDTO dto = product_attributeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_attribute" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attributes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_attributeDTO> product_attributedtos) {
        product_attributeService.createBatch(product_attributeMapping.toDomain(product_attributedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_attribute_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_attribute" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_attributes/{product_attribute_id}")

    public ResponseEntity<Product_attributeDTO> update(@PathVariable("product_attribute_id") Integer product_attribute_id, @RequestBody Product_attributeDTO product_attributedto) {
		Product_attribute domain = product_attributeMapping.toDomain(product_attributedto);
        domain.setId(product_attribute_id);
		product_attributeService.update(domain);
		Product_attributeDTO dto = product_attributeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_attribute_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_attribute" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_attributes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_attributeDTO> product_attributedtos) {
        product_attributeService.updateBatch(product_attributeMapping.toDomain(product_attributedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#product_attribute_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_attribute" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_attributes/{product_attribute_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_attribute_id") Integer product_attribute_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_attributeService.remove(product_attribute_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_attribute" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_attributes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_attributeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission(#product_attribute_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_attribute" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_attributes/{product_attribute_id}")
    public ResponseEntity<Product_attributeDTO> get(@PathVariable("product_attribute_id") Integer product_attribute_id) {
        Product_attribute domain = product_attributeService.get(product_attribute_id);
        Product_attributeDTO dto = product_attributeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_attribute" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_attributes/fetchdefault")
	public ResponseEntity<List<Product_attributeDTO>> fetchDefault(Product_attributeSearchContext context) {
        Page<Product_attribute> domains = product_attributeService.searchDefault(context) ;
        List<Product_attributeDTO> list = product_attributeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_attribute" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_attributes/searchdefault")
	public ResponseEntity<Page<Product_attributeDTO>> searchDefault(Product_attributeSearchContext context) {
        Page<Product_attribute> domains = product_attributeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_attributeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_attribute getEntity(){
        return new Product_attribute();
    }

}
