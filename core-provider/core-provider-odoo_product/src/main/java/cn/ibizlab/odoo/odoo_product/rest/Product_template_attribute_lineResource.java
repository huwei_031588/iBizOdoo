package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_line;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_template_attribute_lineService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_lineSearchContext;




@Slf4j
@Api(tags = {"Product_template_attribute_line" })
@RestController("odoo_product-product_template_attribute_line")
@RequestMapping("")
public class Product_template_attribute_lineResource {

    @Autowired
    private IProduct_template_attribute_lineService product_template_attribute_lineService;

    @Autowired
    @Lazy
    private Product_template_attribute_lineMapping product_template_attribute_lineMapping;




    @PreAuthorize("hasPermission(#product_template_attribute_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_template_attribute_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_lines/{product_template_attribute_line_id}")
    public ResponseEntity<Product_template_attribute_lineDTO> get(@PathVariable("product_template_attribute_line_id") Integer product_template_attribute_line_id) {
        Product_template_attribute_line domain = product_template_attribute_lineService.get(product_template_attribute_line_id);
        Product_template_attribute_lineDTO dto = product_template_attribute_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#product_template_attribute_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_template_attribute_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_lines/{product_template_attribute_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_template_attribute_line_id") Integer product_template_attribute_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_lineService.remove(product_template_attribute_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_template_attribute_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_template_attribute_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_template_attribute_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_template_attribute_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_lines/{product_template_attribute_line_id}")

    public ResponseEntity<Product_template_attribute_lineDTO> update(@PathVariable("product_template_attribute_line_id") Integer product_template_attribute_line_id, @RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
		Product_template_attribute_line domain = product_template_attribute_lineMapping.toDomain(product_template_attribute_linedto);
        domain.setId(product_template_attribute_line_id);
		product_template_attribute_lineService.update(domain);
		Product_template_attribute_lineDTO dto = product_template_attribute_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_template_attribute_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_template_attribute_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {
        product_template_attribute_lineService.updateBatch(product_template_attribute_lineMapping.toDomain(product_template_attribute_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_template_attribute_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_lines")

    public ResponseEntity<Product_template_attribute_lineDTO> create(@RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
        Product_template_attribute_line domain = product_template_attribute_lineMapping.toDomain(product_template_attribute_linedto);
		product_template_attribute_lineService.create(domain);
        Product_template_attribute_lineDTO dto = product_template_attribute_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_template_attribute_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {
        product_template_attribute_lineService.createBatch(product_template_attribute_lineMapping.toDomain(product_template_attribute_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_template_attribute_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_template_attribute_lines/fetchdefault")
	public ResponseEntity<List<Product_template_attribute_lineDTO>> fetchDefault(Product_template_attribute_lineSearchContext context) {
        Page<Product_template_attribute_line> domains = product_template_attribute_lineService.searchDefault(context) ;
        List<Product_template_attribute_lineDTO> list = product_template_attribute_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_template_attribute_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_template_attribute_lines/searchdefault")
	public ResponseEntity<Page<Product_template_attribute_lineDTO>> searchDefault(Product_template_attribute_lineSearchContext context) {
        Page<Product_template_attribute_line> domains = product_template_attribute_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_template_attribute_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_template_attribute_line getEntity(){
        return new Product_template_attribute_line();
    }

}
