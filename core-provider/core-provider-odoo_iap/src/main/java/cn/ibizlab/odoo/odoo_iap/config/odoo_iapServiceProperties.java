package cn.ibizlab.odoo.odoo_iap.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-iap")
@Data
public class odoo_iapServiceProperties {

	private boolean enabled;

	private boolean auth;


}