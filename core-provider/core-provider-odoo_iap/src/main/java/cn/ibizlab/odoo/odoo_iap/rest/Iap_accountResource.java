package cn.ibizlab.odoo.odoo_iap.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_iap.dto.*;
import cn.ibizlab.odoo.odoo_iap.mapping.*;
import cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account;
import cn.ibizlab.odoo.core.odoo_iap.service.IIap_accountService;
import cn.ibizlab.odoo.core.odoo_iap.filter.Iap_accountSearchContext;




@Slf4j
@Api(tags = {"Iap_account" })
@RestController("odoo_iap-iap_account")
@RequestMapping("")
public class Iap_accountResource {

    @Autowired
    private IIap_accountService iap_accountService;

    @Autowired
    @Lazy
    private Iap_accountMapping iap_accountMapping;




    @PreAuthorize("hasPermission('Remove',{#iap_account_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Iap_account" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/iap_accounts/{iap_account_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("iap_account_id") Integer iap_account_id) {
         return ResponseEntity.status(HttpStatus.OK).body(iap_accountService.remove(iap_account_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Iap_account" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/iap_accounts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        iap_accountService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#iap_account_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Iap_account" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/iap_accounts/{iap_account_id}")
    public ResponseEntity<Iap_accountDTO> get(@PathVariable("iap_account_id") Integer iap_account_id) {
        Iap_account domain = iap_accountService.get(iap_account_id);
        Iap_accountDTO dto = iap_accountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission(#iap_account_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Iap_account" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/iap_accounts/{iap_account_id}")

    public ResponseEntity<Iap_accountDTO> update(@PathVariable("iap_account_id") Integer iap_account_id, @RequestBody Iap_accountDTO iap_accountdto) {
		Iap_account domain = iap_accountMapping.toDomain(iap_accountdto);
        domain.setId(iap_account_id);
		iap_accountService.update(domain);
		Iap_accountDTO dto = iap_accountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#iap_account_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Iap_account" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/iap_accounts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Iap_accountDTO> iap_accountdtos) {
        iap_accountService.updateBatch(iap_accountMapping.toDomain(iap_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Iap_account" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/iap_accounts")

    public ResponseEntity<Iap_accountDTO> create(@RequestBody Iap_accountDTO iap_accountdto) {
        Iap_account domain = iap_accountMapping.toDomain(iap_accountdto);
		iap_accountService.create(domain);
        Iap_accountDTO dto = iap_accountMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Iap_account" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/iap_accounts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Iap_accountDTO> iap_accountdtos) {
        iap_accountService.createBatch(iap_accountMapping.toDomain(iap_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Iap_account" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/iap_accounts/fetchdefault")
	public ResponseEntity<List<Iap_accountDTO>> fetchDefault(Iap_accountSearchContext context) {
        Page<Iap_account> domains = iap_accountService.searchDefault(context) ;
        List<Iap_accountDTO> list = iap_accountMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Iap_account" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/iap_accounts/searchdefault")
	public ResponseEntity<Page<Iap_accountDTO>> searchDefault(Iap_accountSearchContext context) {
        Page<Iap_account> domains = iap_accountService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(iap_accountMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Iap_account getEntity(){
        return new Iap_account();
    }

}
