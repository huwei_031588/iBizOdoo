package cn.ibizlab.odoo.odoo_uom.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_uom")
public class odoo_uomRestConfiguration {

}
