package cn.ibizlab.odoo.odoo_uom.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_uom.dto.*;
import cn.ibizlab.odoo.odoo_uom.mapping.*;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.odoo.core.odoo_uom.service.IUom_categoryService;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_categorySearchContext;




@Slf4j
@Api(tags = {"Uom_category" })
@RestController("odoo_uom-uom_category")
@RequestMapping("")
public class Uom_categoryResource {

    @Autowired
    private IUom_categoryService uom_categoryService;

    @Autowired
    @Lazy
    private Uom_categoryMapping uom_categoryMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Uom_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_categories")

    public ResponseEntity<Uom_categoryDTO> create(@RequestBody Uom_categoryDTO uom_categorydto) {
        Uom_category domain = uom_categoryMapping.toDomain(uom_categorydto);
		uom_categoryService.create(domain);
        Uom_categoryDTO dto = uom_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Uom_category" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/uom_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Uom_categoryDTO> uom_categorydtos) {
        uom_categoryService.createBatch(uom_categoryMapping.toDomain(uom_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#uom_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Uom_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/uom_categories/{uom_category_id}")

    public ResponseEntity<Uom_categoryDTO> update(@PathVariable("uom_category_id") Integer uom_category_id, @RequestBody Uom_categoryDTO uom_categorydto) {
		Uom_category domain = uom_categoryMapping.toDomain(uom_categorydto);
        domain.setId(uom_category_id);
		uom_categoryService.update(domain);
		Uom_categoryDTO dto = uom_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#uom_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Uom_category" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/uom_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Uom_categoryDTO> uom_categorydtos) {
        uom_categoryService.updateBatch(uom_categoryMapping.toDomain(uom_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#uom_category_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Uom_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/uom_categories/{uom_category_id}")
    public ResponseEntity<Uom_categoryDTO> get(@PathVariable("uom_category_id") Integer uom_category_id) {
        Uom_category domain = uom_categoryService.get(uom_category_id);
        Uom_categoryDTO dto = uom_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#uom_category_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Uom_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uom_categories/{uom_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("uom_category_id") Integer uom_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(uom_categoryService.remove(uom_category_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Uom_category" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/uom_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        uom_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Uom_category" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/uom_categories/fetchdefault")
	public ResponseEntity<List<Uom_categoryDTO>> fetchDefault(Uom_categorySearchContext context) {
        Page<Uom_category> domains = uom_categoryService.searchDefault(context) ;
        List<Uom_categoryDTO> list = uom_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Uom_category" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/uom_categories/searchdefault")
	public ResponseEntity<Page<Uom_categoryDTO>> searchDefault(Uom_categorySearchContext context) {
        Page<Uom_category> domains = uom_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(uom_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Uom_category getEntity(){
        return new Uom_category();
    }

}
