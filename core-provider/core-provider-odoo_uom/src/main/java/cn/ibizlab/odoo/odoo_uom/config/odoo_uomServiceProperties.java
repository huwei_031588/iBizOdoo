package cn.ibizlab.odoo.odoo_uom.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-uom")
@Data
public class odoo_uomServiceProperties {

	private boolean enabled;

	private boolean auth;


}