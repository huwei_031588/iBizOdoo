package cn.ibizlab.odoo.odoo_uom.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.odoo.odoo_uom.dto.Uom_categoryDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Uom_categoryMapping extends MappingBase<Uom_categoryDTO, Uom_category> {


}

