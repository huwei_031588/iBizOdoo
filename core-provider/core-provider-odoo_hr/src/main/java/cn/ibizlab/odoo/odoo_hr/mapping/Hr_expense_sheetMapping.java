package cn.ibizlab.odoo.odoo_hr.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet;
import cn.ibizlab.odoo.odoo_hr.dto.Hr_expense_sheetDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_expense_sheetMapping extends MappingBase<Hr_expense_sheetDTO, Hr_expense_sheet> {


}

