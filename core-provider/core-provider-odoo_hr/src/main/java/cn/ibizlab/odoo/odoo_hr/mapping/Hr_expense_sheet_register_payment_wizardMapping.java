package cn.ibizlab.odoo.odoo_hr.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet_register_payment_wizard;
import cn.ibizlab.odoo.odoo_hr.dto.Hr_expense_sheet_register_payment_wizardDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_expense_sheet_register_payment_wizardMapping extends MappingBase<Hr_expense_sheet_register_payment_wizardDTO, Hr_expense_sheet_register_payment_wizard> {


}

