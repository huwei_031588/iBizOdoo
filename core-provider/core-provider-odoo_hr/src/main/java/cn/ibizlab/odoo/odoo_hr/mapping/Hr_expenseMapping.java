package cn.ibizlab.odoo.odoo_hr.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.odoo.odoo_hr.dto.Hr_expenseDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_expenseMapping extends MappingBase<Hr_expenseDTO, Hr_expense> {


}

