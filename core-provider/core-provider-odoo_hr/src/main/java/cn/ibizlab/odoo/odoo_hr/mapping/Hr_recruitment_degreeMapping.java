package cn.ibizlab.odoo.odoo_hr.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_degree;
import cn.ibizlab.odoo.odoo_hr.dto.Hr_recruitment_degreeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_recruitment_degreeMapping extends MappingBase<Hr_recruitment_degreeDTO, Hr_recruitment_degree> {


}

