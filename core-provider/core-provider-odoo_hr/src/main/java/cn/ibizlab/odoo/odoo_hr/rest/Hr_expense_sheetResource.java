package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expense_sheetService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheetSearchContext;




@Slf4j
@Api(tags = {"Hr_expense_sheet" })
@RestController("odoo_hr-hr_expense_sheet")
@RequestMapping("")
public class Hr_expense_sheetResource {

    @Autowired
    private IHr_expense_sheetService hr_expense_sheetService;

    @Autowired
    @Lazy
    private Hr_expense_sheetMapping hr_expense_sheetMapping;




    @PreAuthorize("hasPermission(#hr_expense_sheet_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_expense_sheet" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_sheets/{hr_expense_sheet_id}")

    public ResponseEntity<Hr_expense_sheetDTO> update(@PathVariable("hr_expense_sheet_id") Integer hr_expense_sheet_id, @RequestBody Hr_expense_sheetDTO hr_expense_sheetdto) {
		Hr_expense_sheet domain = hr_expense_sheetMapping.toDomain(hr_expense_sheetdto);
        domain.setId(hr_expense_sheet_id);
		hr_expense_sheetService.update(domain);
		Hr_expense_sheetDTO dto = hr_expense_sheetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_expense_sheet_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_expense_sheet" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_sheets/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_expense_sheetDTO> hr_expense_sheetdtos) {
        hr_expense_sheetService.updateBatch(hr_expense_sheetMapping.toDomain(hr_expense_sheetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_expense_sheet_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_expense_sheet" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_expense_sheets/{hr_expense_sheet_id}")
    public ResponseEntity<Hr_expense_sheetDTO> get(@PathVariable("hr_expense_sheet_id") Integer hr_expense_sheet_id) {
        Hr_expense_sheet domain = hr_expense_sheetService.get(hr_expense_sheet_id);
        Hr_expense_sheetDTO dto = hr_expense_sheetMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_expense_sheet" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets")

    public ResponseEntity<Hr_expense_sheetDTO> create(@RequestBody Hr_expense_sheetDTO hr_expense_sheetdto) {
        Hr_expense_sheet domain = hr_expense_sheetMapping.toDomain(hr_expense_sheetdto);
		hr_expense_sheetService.create(domain);
        Hr_expense_sheetDTO dto = hr_expense_sheetMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_expense_sheet" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_sheets/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_expense_sheetDTO> hr_expense_sheetdtos) {
        hr_expense_sheetService.createBatch(hr_expense_sheetMapping.toDomain(hr_expense_sheetdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#hr_expense_sheet_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_expense_sheet" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_sheets/{hr_expense_sheet_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_expense_sheet_id") Integer hr_expense_sheet_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_expense_sheetService.remove(hr_expense_sheet_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_expense_sheet" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_sheets/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_expense_sheetService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_expense_sheet" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_expense_sheets/fetchdefault")
	public ResponseEntity<List<Hr_expense_sheetDTO>> fetchDefault(Hr_expense_sheetSearchContext context) {
        Page<Hr_expense_sheet> domains = hr_expense_sheetService.searchDefault(context) ;
        List<Hr_expense_sheetDTO> list = hr_expense_sheetMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_expense_sheet" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_expense_sheets/searchdefault")
	public ResponseEntity<Page<Hr_expense_sheetDTO>> searchDefault(Hr_expense_sheetSearchContext context) {
        Page<Hr_expense_sheet> domains = hr_expense_sheetService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_expense_sheetMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_expense_sheet getEntity(){
        return new Hr_expense_sheet();
    }

}
