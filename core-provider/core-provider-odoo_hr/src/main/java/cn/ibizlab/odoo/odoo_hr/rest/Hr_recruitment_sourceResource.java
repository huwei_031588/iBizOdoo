package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_source;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_sourceService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;




@Slf4j
@Api(tags = {"Hr_recruitment_source" })
@RestController("odoo_hr-hr_recruitment_source")
@RequestMapping("")
public class Hr_recruitment_sourceResource {

    @Autowired
    private IHr_recruitment_sourceService hr_recruitment_sourceService;

    @Autowired
    @Lazy
    private Hr_recruitment_sourceMapping hr_recruitment_sourceMapping;













    @PreAuthorize("hasPermission(#hr_recruitment_source_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_recruitment_source" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_sources/{hr_recruitment_source_id}")
    public ResponseEntity<Hr_recruitment_sourceDTO> get(@PathVariable("hr_recruitment_source_id") Integer hr_recruitment_source_id) {
        Hr_recruitment_source domain = hr_recruitment_sourceService.get(hr_recruitment_source_id);
        Hr_recruitment_sourceDTO dto = hr_recruitment_sourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#hr_recruitment_source_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_recruitment_source" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_sources/{hr_recruitment_source_id}")

    public ResponseEntity<Hr_recruitment_sourceDTO> update(@PathVariable("hr_recruitment_source_id") Integer hr_recruitment_source_id, @RequestBody Hr_recruitment_sourceDTO hr_recruitment_sourcedto) {
		Hr_recruitment_source domain = hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedto);
        domain.setId(hr_recruitment_source_id);
		hr_recruitment_sourceService.update(domain);
		Hr_recruitment_sourceDTO dto = hr_recruitment_sourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_recruitment_source_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_recruitment_source" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_sources/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_recruitment_sourceDTO> hr_recruitment_sourcedtos) {
        hr_recruitment_sourceService.updateBatch(hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_recruitment_source" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources")

    public ResponseEntity<Hr_recruitment_sourceDTO> create(@RequestBody Hr_recruitment_sourceDTO hr_recruitment_sourcedto) {
        Hr_recruitment_source domain = hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedto);
		hr_recruitment_sourceService.create(domain);
        Hr_recruitment_sourceDTO dto = hr_recruitment_sourceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_recruitment_source" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_sources/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_recruitment_sourceDTO> hr_recruitment_sourcedtos) {
        hr_recruitment_sourceService.createBatch(hr_recruitment_sourceMapping.toDomain(hr_recruitment_sourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#hr_recruitment_source_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_recruitment_source" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_sources/{hr_recruitment_source_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_recruitment_source_id") Integer hr_recruitment_source_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_sourceService.remove(hr_recruitment_source_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_recruitment_source" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_sources/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_recruitment_sourceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_recruitment_source" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_recruitment_sources/fetchdefault")
	public ResponseEntity<List<Hr_recruitment_sourceDTO>> fetchDefault(Hr_recruitment_sourceSearchContext context) {
        Page<Hr_recruitment_source> domains = hr_recruitment_sourceService.searchDefault(context) ;
        List<Hr_recruitment_sourceDTO> list = hr_recruitment_sourceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_recruitment_source" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_recruitment_sources/searchdefault")
	public ResponseEntity<Page<Hr_recruitment_sourceDTO>> searchDefault(Hr_recruitment_sourceSearchContext context) {
        Page<Hr_recruitment_source> domains = hr_recruitment_sourceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_recruitment_sourceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_recruitment_source getEntity(){
        return new Hr_recruitment_source();
    }

}
