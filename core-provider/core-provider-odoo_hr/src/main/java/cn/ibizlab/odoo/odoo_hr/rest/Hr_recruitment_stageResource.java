package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_stageService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;




@Slf4j
@Api(tags = {"Hr_recruitment_stage" })
@RestController("odoo_hr-hr_recruitment_stage")
@RequestMapping("")
public class Hr_recruitment_stageResource {

    @Autowired
    private IHr_recruitment_stageService hr_recruitment_stageService;

    @Autowired
    @Lazy
    private Hr_recruitment_stageMapping hr_recruitment_stageMapping;




    @PreAuthorize("hasPermission(#hr_recruitment_stage_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_recruitment_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_stages/{hr_recruitment_stage_id}")
    public ResponseEntity<Hr_recruitment_stageDTO> get(@PathVariable("hr_recruitment_stage_id") Integer hr_recruitment_stage_id) {
        Hr_recruitment_stage domain = hr_recruitment_stageService.get(hr_recruitment_stage_id);
        Hr_recruitment_stageDTO dto = hr_recruitment_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_recruitment_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_stages")

    public ResponseEntity<Hr_recruitment_stageDTO> create(@RequestBody Hr_recruitment_stageDTO hr_recruitment_stagedto) {
        Hr_recruitment_stage domain = hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedto);
		hr_recruitment_stageService.create(domain);
        Hr_recruitment_stageDTO dto = hr_recruitment_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_recruitment_stage" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_recruitment_stageDTO> hr_recruitment_stagedtos) {
        hr_recruitment_stageService.createBatch(hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_recruitment_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_recruitment_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_stages/{hr_recruitment_stage_id}")

    public ResponseEntity<Hr_recruitment_stageDTO> update(@PathVariable("hr_recruitment_stage_id") Integer hr_recruitment_stage_id, @RequestBody Hr_recruitment_stageDTO hr_recruitment_stagedto) {
		Hr_recruitment_stage domain = hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedto);
        domain.setId(hr_recruitment_stage_id);
		hr_recruitment_stageService.update(domain);
		Hr_recruitment_stageDTO dto = hr_recruitment_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_recruitment_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_recruitment_stage" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_recruitment_stageDTO> hr_recruitment_stagedtos) {
        hr_recruitment_stageService.updateBatch(hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#hr_recruitment_stage_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_recruitment_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_stages/{hr_recruitment_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_recruitment_stage_id") Integer hr_recruitment_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_stageService.remove(hr_recruitment_stage_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_recruitment_stage" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_recruitment_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_recruitment_stage" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_recruitment_stages/fetchdefault")
	public ResponseEntity<List<Hr_recruitment_stageDTO>> fetchDefault(Hr_recruitment_stageSearchContext context) {
        Page<Hr_recruitment_stage> domains = hr_recruitment_stageService.searchDefault(context) ;
        List<Hr_recruitment_stageDTO> list = hr_recruitment_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_recruitment_stage" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_recruitment_stages/searchdefault")
	public ResponseEntity<Page<Hr_recruitment_stageDTO>> searchDefault(Hr_recruitment_stageSearchContext context) {
        Page<Hr_recruitment_stage> domains = hr_recruitment_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_recruitment_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_recruitment_stage getEntity(){
        return new Hr_recruitment_stage();
    }

}
