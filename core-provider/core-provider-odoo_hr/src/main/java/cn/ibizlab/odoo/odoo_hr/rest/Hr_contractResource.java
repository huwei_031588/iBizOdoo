package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_contractService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contractSearchContext;




@Slf4j
@Api(tags = {"Hr_contract" })
@RestController("odoo_hr-hr_contract")
@RequestMapping("")
public class Hr_contractResource {

    @Autowired
    private IHr_contractService hr_contractService;

    @Autowired
    @Lazy
    private Hr_contractMapping hr_contractMapping;










    @PreAuthorize("hasPermission('Remove',{#hr_contract_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_contract" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_contracts/{hr_contract_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_contract_id") Integer hr_contract_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_contractService.remove(hr_contract_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_contract" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_contracts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_contractService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_contract_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_contract" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_contracts/{hr_contract_id}")

    public ResponseEntity<Hr_contractDTO> update(@PathVariable("hr_contract_id") Integer hr_contract_id, @RequestBody Hr_contractDTO hr_contractdto) {
		Hr_contract domain = hr_contractMapping.toDomain(hr_contractdto);
        domain.setId(hr_contract_id);
		hr_contractService.update(domain);
		Hr_contractDTO dto = hr_contractMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_contract_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_contract" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_contracts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_contractDTO> hr_contractdtos) {
        hr_contractService.updateBatch(hr_contractMapping.toDomain(hr_contractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_contract" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contracts")

    public ResponseEntity<Hr_contractDTO> create(@RequestBody Hr_contractDTO hr_contractdto) {
        Hr_contract domain = hr_contractMapping.toDomain(hr_contractdto);
		hr_contractService.create(domain);
        Hr_contractDTO dto = hr_contractMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_contract" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contracts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_contractDTO> hr_contractdtos) {
        hr_contractService.createBatch(hr_contractMapping.toDomain(hr_contractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_contract_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_contract" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_contracts/{hr_contract_id}")
    public ResponseEntity<Hr_contractDTO> get(@PathVariable("hr_contract_id") Integer hr_contract_id) {
        Hr_contract domain = hr_contractService.get(hr_contract_id);
        Hr_contractDTO dto = hr_contractMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_contract" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_contracts/fetchdefault")
	public ResponseEntity<List<Hr_contractDTO>> fetchDefault(Hr_contractSearchContext context) {
        Page<Hr_contract> domains = hr_contractService.searchDefault(context) ;
        List<Hr_contractDTO> list = hr_contractMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_contract" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_contracts/searchdefault")
	public ResponseEntity<Page<Hr_contractDTO>> searchDefault(Hr_contractSearchContext context) {
        Page<Hr_contract> domains = hr_contractService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_contractMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_contract getEntity(){
        return new Hr_contract();
    }

}
