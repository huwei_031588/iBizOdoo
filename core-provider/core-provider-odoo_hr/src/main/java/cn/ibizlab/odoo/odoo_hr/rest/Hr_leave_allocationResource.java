package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_allocation;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_allocationService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_allocationSearchContext;




@Slf4j
@Api(tags = {"Hr_leave_allocation" })
@RestController("odoo_hr-hr_leave_allocation")
@RequestMapping("")
public class Hr_leave_allocationResource {

    @Autowired
    private IHr_leave_allocationService hr_leave_allocationService;

    @Autowired
    @Lazy
    private Hr_leave_allocationMapping hr_leave_allocationMapping;




    @PreAuthorize("hasPermission(#hr_leave_allocation_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_leave_allocation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leave_allocations/{hr_leave_allocation_id}")
    public ResponseEntity<Hr_leave_allocationDTO> get(@PathVariable("hr_leave_allocation_id") Integer hr_leave_allocation_id) {
        Hr_leave_allocation domain = hr_leave_allocationService.get(hr_leave_allocation_id);
        Hr_leave_allocationDTO dto = hr_leave_allocationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('Remove',{#hr_leave_allocation_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_leave_allocation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_allocations/{hr_leave_allocation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_allocation_id") Integer hr_leave_allocation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_leave_allocationService.remove(hr_leave_allocation_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_leave_allocation" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_allocations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_leave_allocationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#hr_leave_allocation_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_leave_allocation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_allocations/{hr_leave_allocation_id}")

    public ResponseEntity<Hr_leave_allocationDTO> update(@PathVariable("hr_leave_allocation_id") Integer hr_leave_allocation_id, @RequestBody Hr_leave_allocationDTO hr_leave_allocationdto) {
		Hr_leave_allocation domain = hr_leave_allocationMapping.toDomain(hr_leave_allocationdto);
        domain.setId(hr_leave_allocation_id);
		hr_leave_allocationService.update(domain);
		Hr_leave_allocationDTO dto = hr_leave_allocationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_leave_allocation_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_leave_allocation" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_allocations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leave_allocationDTO> hr_leave_allocationdtos) {
        hr_leave_allocationService.updateBatch(hr_leave_allocationMapping.toDomain(hr_leave_allocationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_leave_allocation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_allocations")

    public ResponseEntity<Hr_leave_allocationDTO> create(@RequestBody Hr_leave_allocationDTO hr_leave_allocationdto) {
        Hr_leave_allocation domain = hr_leave_allocationMapping.toDomain(hr_leave_allocationdto);
		hr_leave_allocationService.create(domain);
        Hr_leave_allocationDTO dto = hr_leave_allocationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_leave_allocation" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_allocations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_leave_allocationDTO> hr_leave_allocationdtos) {
        hr_leave_allocationService.createBatch(hr_leave_allocationMapping.toDomain(hr_leave_allocationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_leave_allocation" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leave_allocations/fetchdefault")
	public ResponseEntity<List<Hr_leave_allocationDTO>> fetchDefault(Hr_leave_allocationSearchContext context) {
        Page<Hr_leave_allocation> domains = hr_leave_allocationService.searchDefault(context) ;
        List<Hr_leave_allocationDTO> list = hr_leave_allocationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_leave_allocation" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leave_allocations/searchdefault")
	public ResponseEntity<Page<Hr_leave_allocationDTO>> searchDefault(Hr_leave_allocationSearchContext context) {
        Page<Hr_leave_allocation> domains = hr_leave_allocationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_leave_allocationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_leave_allocation getEntity(){
        return new Hr_leave_allocation();
    }

}
