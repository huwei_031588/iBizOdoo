package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_dept;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_holidays_summary_deptService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_deptSearchContext;




@Slf4j
@Api(tags = {"Hr_holidays_summary_dept" })
@RestController("odoo_hr-hr_holidays_summary_dept")
@RequestMapping("")
public class Hr_holidays_summary_deptResource {

    @Autowired
    private IHr_holidays_summary_deptService hr_holidays_summary_deptService;

    @Autowired
    @Lazy
    private Hr_holidays_summary_deptMapping hr_holidays_summary_deptMapping;










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_holidays_summary_dept" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_depts")

    public ResponseEntity<Hr_holidays_summary_deptDTO> create(@RequestBody Hr_holidays_summary_deptDTO hr_holidays_summary_deptdto) {
        Hr_holidays_summary_dept domain = hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdto);
		hr_holidays_summary_deptService.create(domain);
        Hr_holidays_summary_deptDTO dto = hr_holidays_summary_deptMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_holidays_summary_dept" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_depts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_holidays_summary_deptDTO> hr_holidays_summary_deptdtos) {
        hr_holidays_summary_deptService.createBatch(hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#hr_holidays_summary_dept_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_holidays_summary_dept" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_holidays_summary_depts/{hr_holidays_summary_dept_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_holidays_summary_dept_id") Integer hr_holidays_summary_dept_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_holidays_summary_deptService.remove(hr_holidays_summary_dept_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_holidays_summary_dept" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_holidays_summary_depts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_holidays_summary_deptService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_holidays_summary_dept_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_holidays_summary_dept" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_holidays_summary_depts/{hr_holidays_summary_dept_id}")

    public ResponseEntity<Hr_holidays_summary_deptDTO> update(@PathVariable("hr_holidays_summary_dept_id") Integer hr_holidays_summary_dept_id, @RequestBody Hr_holidays_summary_deptDTO hr_holidays_summary_deptdto) {
		Hr_holidays_summary_dept domain = hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdto);
        domain.setId(hr_holidays_summary_dept_id);
		hr_holidays_summary_deptService.update(domain);
		Hr_holidays_summary_deptDTO dto = hr_holidays_summary_deptMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_holidays_summary_dept_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_holidays_summary_dept" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_holidays_summary_depts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_holidays_summary_deptDTO> hr_holidays_summary_deptdtos) {
        hr_holidays_summary_deptService.updateBatch(hr_holidays_summary_deptMapping.toDomain(hr_holidays_summary_deptdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#hr_holidays_summary_dept_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_holidays_summary_dept" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_depts/{hr_holidays_summary_dept_id}")
    public ResponseEntity<Hr_holidays_summary_deptDTO> get(@PathVariable("hr_holidays_summary_dept_id") Integer hr_holidays_summary_dept_id) {
        Hr_holidays_summary_dept domain = hr_holidays_summary_deptService.get(hr_holidays_summary_dept_id);
        Hr_holidays_summary_deptDTO dto = hr_holidays_summary_deptMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_holidays_summary_dept" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_holidays_summary_depts/fetchdefault")
	public ResponseEntity<List<Hr_holidays_summary_deptDTO>> fetchDefault(Hr_holidays_summary_deptSearchContext context) {
        Page<Hr_holidays_summary_dept> domains = hr_holidays_summary_deptService.searchDefault(context) ;
        List<Hr_holidays_summary_deptDTO> list = hr_holidays_summary_deptMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_holidays_summary_dept" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_holidays_summary_depts/searchdefault")
	public ResponseEntity<Page<Hr_holidays_summary_deptDTO>> searchDefault(Hr_holidays_summary_deptSearchContext context) {
        Page<Hr_holidays_summary_dept> domains = hr_holidays_summary_deptService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_holidays_summary_deptMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_holidays_summary_dept getEntity(){
        return new Hr_holidays_summary_dept();
    }

}
