package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract_type;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_contract_typeService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contract_typeSearchContext;




@Slf4j
@Api(tags = {"Hr_contract_type" })
@RestController("odoo_hr-hr_contract_type")
@RequestMapping("")
public class Hr_contract_typeResource {

    @Autowired
    private IHr_contract_typeService hr_contract_typeService;

    @Autowired
    @Lazy
    private Hr_contract_typeMapping hr_contract_typeMapping;










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_contract_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contract_types")

    public ResponseEntity<Hr_contract_typeDTO> create(@RequestBody Hr_contract_typeDTO hr_contract_typedto) {
        Hr_contract_type domain = hr_contract_typeMapping.toDomain(hr_contract_typedto);
		hr_contract_typeService.create(domain);
        Hr_contract_typeDTO dto = hr_contract_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_contract_type" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_contract_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_contract_typeDTO> hr_contract_typedtos) {
        hr_contract_typeService.createBatch(hr_contract_typeMapping.toDomain(hr_contract_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#hr_contract_type_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_contract_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_contract_types/{hr_contract_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_contract_type_id") Integer hr_contract_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_contract_typeService.remove(hr_contract_type_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_contract_type" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_contract_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_contract_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_contract_type_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_contract_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_contract_types/{hr_contract_type_id}")
    public ResponseEntity<Hr_contract_typeDTO> get(@PathVariable("hr_contract_type_id") Integer hr_contract_type_id) {
        Hr_contract_type domain = hr_contract_typeService.get(hr_contract_type_id);
        Hr_contract_typeDTO dto = hr_contract_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#hr_contract_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_contract_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_contract_types/{hr_contract_type_id}")

    public ResponseEntity<Hr_contract_typeDTO> update(@PathVariable("hr_contract_type_id") Integer hr_contract_type_id, @RequestBody Hr_contract_typeDTO hr_contract_typedto) {
		Hr_contract_type domain = hr_contract_typeMapping.toDomain(hr_contract_typedto);
        domain.setId(hr_contract_type_id);
		hr_contract_typeService.update(domain);
		Hr_contract_typeDTO dto = hr_contract_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_contract_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_contract_type" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_contract_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_contract_typeDTO> hr_contract_typedtos) {
        hr_contract_typeService.updateBatch(hr_contract_typeMapping.toDomain(hr_contract_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_contract_type" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_contract_types/fetchdefault")
	public ResponseEntity<List<Hr_contract_typeDTO>> fetchDefault(Hr_contract_typeSearchContext context) {
        Page<Hr_contract_type> domains = hr_contract_typeService.searchDefault(context) ;
        List<Hr_contract_typeDTO> list = hr_contract_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_contract_type" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_contract_types/searchdefault")
	public ResponseEntity<Page<Hr_contract_typeDTO>> searchDefault(Hr_contract_typeSearchContext context) {
        Page<Hr_contract_type> domains = hr_contract_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_contract_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_contract_type getEntity(){
        return new Hr_contract_type();
    }

}
