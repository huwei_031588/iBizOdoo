package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_degree;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_degreeService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_degreeSearchContext;




@Slf4j
@Api(tags = {"Hr_recruitment_degree" })
@RestController("odoo_hr-hr_recruitment_degree")
@RequestMapping("")
public class Hr_recruitment_degreeResource {

    @Autowired
    private IHr_recruitment_degreeService hr_recruitment_degreeService;

    @Autowired
    @Lazy
    private Hr_recruitment_degreeMapping hr_recruitment_degreeMapping;













    @PreAuthorize("hasPermission(#hr_recruitment_degree_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_recruitment_degree" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_degrees/{hr_recruitment_degree_id}")

    public ResponseEntity<Hr_recruitment_degreeDTO> update(@PathVariable("hr_recruitment_degree_id") Integer hr_recruitment_degree_id, @RequestBody Hr_recruitment_degreeDTO hr_recruitment_degreedto) {
		Hr_recruitment_degree domain = hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedto);
        domain.setId(hr_recruitment_degree_id);
		hr_recruitment_degreeService.update(domain);
		Hr_recruitment_degreeDTO dto = hr_recruitment_degreeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_recruitment_degree_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_recruitment_degree" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_degrees/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_recruitment_degreeDTO> hr_recruitment_degreedtos) {
        hr_recruitment_degreeService.updateBatch(hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#hr_recruitment_degree_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_recruitment_degree" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_degrees/{hr_recruitment_degree_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_recruitment_degree_id") Integer hr_recruitment_degree_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_degreeService.remove(hr_recruitment_degree_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_recruitment_degree" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_degrees/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_recruitment_degreeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_recruitment_degree_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_recruitment_degree" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_degrees/{hr_recruitment_degree_id}")
    public ResponseEntity<Hr_recruitment_degreeDTO> get(@PathVariable("hr_recruitment_degree_id") Integer hr_recruitment_degree_id) {
        Hr_recruitment_degree domain = hr_recruitment_degreeService.get(hr_recruitment_degree_id);
        Hr_recruitment_degreeDTO dto = hr_recruitment_degreeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_recruitment_degree" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees")

    public ResponseEntity<Hr_recruitment_degreeDTO> create(@RequestBody Hr_recruitment_degreeDTO hr_recruitment_degreedto) {
        Hr_recruitment_degree domain = hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedto);
		hr_recruitment_degreeService.create(domain);
        Hr_recruitment_degreeDTO dto = hr_recruitment_degreeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_recruitment_degree" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_recruitment_degreeDTO> hr_recruitment_degreedtos) {
        hr_recruitment_degreeService.createBatch(hr_recruitment_degreeMapping.toDomain(hr_recruitment_degreedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_recruitment_degree" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_recruitment_degrees/fetchdefault")
	public ResponseEntity<List<Hr_recruitment_degreeDTO>> fetchDefault(Hr_recruitment_degreeSearchContext context) {
        Page<Hr_recruitment_degree> domains = hr_recruitment_degreeService.searchDefault(context) ;
        List<Hr_recruitment_degreeDTO> list = hr_recruitment_degreeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_recruitment_degree" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_recruitment_degrees/searchdefault")
	public ResponseEntity<Page<Hr_recruitment_degreeDTO>> searchDefault(Hr_recruitment_degreeSearchContext context) {
        Page<Hr_recruitment_degree> domains = hr_recruitment_degreeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_recruitment_degreeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_recruitment_degree getEntity(){
        return new Hr_recruitment_degree();
    }

}
