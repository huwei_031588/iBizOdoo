package cn.ibizlab.odoo.odoo_hr.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_employee;
import cn.ibizlab.odoo.odoo_hr.dto.Hr_holidays_summary_employeeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_holidays_summary_employeeMapping extends MappingBase<Hr_holidays_summary_employeeDTO, Hr_holidays_summary_employee> {


}

