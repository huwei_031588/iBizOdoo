package cn.ibizlab.odoo.odoo_survey.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_survey.dto.*;
import cn.ibizlab.odoo.odoo_survey.mapping.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_mail_compose_message;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_mail_compose_messageService;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_mail_compose_messageSearchContext;




@Slf4j
@Api(tags = {"Survey_mail_compose_message" })
@RestController("odoo_survey-survey_mail_compose_message")
@RequestMapping("")
public class Survey_mail_compose_messageResource {

    @Autowired
    private ISurvey_mail_compose_messageService survey_mail_compose_messageService;

    @Autowired
    @Lazy
    private Survey_mail_compose_messageMapping survey_mail_compose_messageMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Survey_mail_compose_message" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages")

    public ResponseEntity<Survey_mail_compose_messageDTO> create(@RequestBody Survey_mail_compose_messageDTO survey_mail_compose_messagedto) {
        Survey_mail_compose_message domain = survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedto);
		survey_mail_compose_messageService.create(domain);
        Survey_mail_compose_messageDTO dto = survey_mail_compose_messageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Survey_mail_compose_message" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_mail_compose_messageDTO> survey_mail_compose_messagedtos) {
        survey_mail_compose_messageService.createBatch(survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#survey_mail_compose_message_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Survey_mail_compose_message" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_mail_compose_messages/{survey_mail_compose_message_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_mail_compose_message_id") Integer survey_mail_compose_message_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_mail_compose_messageService.remove(survey_mail_compose_message_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Survey_mail_compose_message" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_mail_compose_messages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        survey_mail_compose_messageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#survey_mail_compose_message_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Survey_mail_compose_message" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_mail_compose_messages/{survey_mail_compose_message_id}")
    public ResponseEntity<Survey_mail_compose_messageDTO> get(@PathVariable("survey_mail_compose_message_id") Integer survey_mail_compose_message_id) {
        Survey_mail_compose_message domain = survey_mail_compose_messageService.get(survey_mail_compose_message_id);
        Survey_mail_compose_messageDTO dto = survey_mail_compose_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#survey_mail_compose_message_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Survey_mail_compose_message" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_mail_compose_messages/{survey_mail_compose_message_id}")

    public ResponseEntity<Survey_mail_compose_messageDTO> update(@PathVariable("survey_mail_compose_message_id") Integer survey_mail_compose_message_id, @RequestBody Survey_mail_compose_messageDTO survey_mail_compose_messagedto) {
		Survey_mail_compose_message domain = survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedto);
        domain.setId(survey_mail_compose_message_id);
		survey_mail_compose_messageService.update(domain);
		Survey_mail_compose_messageDTO dto = survey_mail_compose_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#survey_mail_compose_message_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Survey_mail_compose_message" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_mail_compose_messages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_mail_compose_messageDTO> survey_mail_compose_messagedtos) {
        survey_mail_compose_messageService.updateBatch(survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Survey_mail_compose_message" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_mail_compose_messages/fetchdefault")
	public ResponseEntity<List<Survey_mail_compose_messageDTO>> fetchDefault(Survey_mail_compose_messageSearchContext context) {
        Page<Survey_mail_compose_message> domains = survey_mail_compose_messageService.searchDefault(context) ;
        List<Survey_mail_compose_messageDTO> list = survey_mail_compose_messageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Survey_mail_compose_message" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_mail_compose_messages/searchdefault")
	public ResponseEntity<Page<Survey_mail_compose_messageDTO>> searchDefault(Survey_mail_compose_messageSearchContext context) {
        Page<Survey_mail_compose_message> domains = survey_mail_compose_messageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_mail_compose_messageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Survey_mail_compose_message getEntity(){
        return new Survey_mail_compose_message();
    }

}
