package cn.ibizlab.odoo.odoo_survey.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Survey_labelDTO]
 */
@Data
public class Survey_labelDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [QUIZZ_MARK]
     *
     */
    @JSONField(name = "quizz_mark")
    @JsonProperty("quizz_mark")
    private Double quizzMark;

    /**
     * 属性 [VALUE]
     *
     */
    @JSONField(name = "value")
    @JsonProperty("value")
    private String value;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [QUESTION_ID_2]
     *
     */
    @JSONField(name = "question_id_2")
    @JsonProperty("question_id_2")
    private Integer questionId2;

    /**
     * 属性 [QUESTION_ID]
     *
     */
    @JSONField(name = "question_id")
    @JsonProperty("question_id")
    private Integer questionId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [QUIZZ_MARK]
     */
    public void setQuizzMark(Double  quizzMark){
        this.quizzMark = quizzMark ;
        this.modify("quizz_mark",quizzMark);
    }

    /**
     * 设置 [VALUE]
     */
    public void setValue(String  value){
        this.value = value ;
        this.modify("value",value);
    }

    /**
     * 设置 [QUESTION_ID_2]
     */
    public void setQuestionId2(Integer  questionId2){
        this.questionId2 = questionId2 ;
        this.modify("question_id_2",questionId2);
    }

    /**
     * 设置 [QUESTION_ID]
     */
    public void setQuestionId(Integer  questionId){
        this.questionId = questionId ;
        this.modify("question_id",questionId);
    }


}

