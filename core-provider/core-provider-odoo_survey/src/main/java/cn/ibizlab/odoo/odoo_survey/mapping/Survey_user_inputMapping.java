package cn.ibizlab.odoo.odoo_survey.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.odoo.odoo_survey.dto.Survey_user_inputDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Survey_user_inputMapping extends MappingBase<Survey_user_inputDTO, Survey_user_input> {


}

