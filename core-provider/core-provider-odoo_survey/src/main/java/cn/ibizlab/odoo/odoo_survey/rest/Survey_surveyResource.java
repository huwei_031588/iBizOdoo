package cn.ibizlab.odoo.odoo_survey.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_survey.dto.*;
import cn.ibizlab.odoo.odoo_survey.mapping.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_surveyService;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_surveySearchContext;




@Slf4j
@Api(tags = {"Survey_survey" })
@RestController("odoo_survey-survey_survey")
@RequestMapping("")
public class Survey_surveyResource {

    @Autowired
    private ISurvey_surveyService survey_surveyService;

    @Autowired
    @Lazy
    private Survey_surveyMapping survey_surveyMapping;







    @PreAuthorize("hasPermission(#survey_survey_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Survey_survey" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_surveys/{survey_survey_id}")
    public ResponseEntity<Survey_surveyDTO> get(@PathVariable("survey_survey_id") Integer survey_survey_id) {
        Survey_survey domain = survey_surveyService.get(survey_survey_id);
        Survey_surveyDTO dto = survey_surveyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Survey_survey" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_surveys")

    public ResponseEntity<Survey_surveyDTO> create(@RequestBody Survey_surveyDTO survey_surveydto) {
        Survey_survey domain = survey_surveyMapping.toDomain(survey_surveydto);
		survey_surveyService.create(domain);
        Survey_surveyDTO dto = survey_surveyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Survey_survey" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_surveys/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_surveyDTO> survey_surveydtos) {
        survey_surveyService.createBatch(survey_surveyMapping.toDomain(survey_surveydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#survey_survey_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Survey_survey" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_surveys/{survey_survey_id}")

    public ResponseEntity<Survey_surveyDTO> update(@PathVariable("survey_survey_id") Integer survey_survey_id, @RequestBody Survey_surveyDTO survey_surveydto) {
		Survey_survey domain = survey_surveyMapping.toDomain(survey_surveydto);
        domain.setId(survey_survey_id);
		survey_surveyService.update(domain);
		Survey_surveyDTO dto = survey_surveyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#survey_survey_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Survey_survey" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_surveys/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_surveyDTO> survey_surveydtos) {
        survey_surveyService.updateBatch(survey_surveyMapping.toDomain(survey_surveydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#survey_survey_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Survey_survey" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_surveys/{survey_survey_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_survey_id") Integer survey_survey_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_surveyService.remove(survey_survey_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Survey_survey" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_surveys/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        survey_surveyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Survey_survey" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_surveys/fetchdefault")
	public ResponseEntity<List<Survey_surveyDTO>> fetchDefault(Survey_surveySearchContext context) {
        Page<Survey_survey> domains = survey_surveyService.searchDefault(context) ;
        List<Survey_surveyDTO> list = survey_surveyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Survey_survey" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_surveys/searchdefault")
	public ResponseEntity<Page<Survey_surveyDTO>> searchDefault(Survey_surveySearchContext context) {
        Page<Survey_survey> domains = survey_surveyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_surveyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Survey_survey getEntity(){
        return new Survey_survey();
    }

}
