package cn.ibizlab.odoo.odoo_barcodes.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_barcodes")
public class odoo_barcodesRestConfiguration {

}
