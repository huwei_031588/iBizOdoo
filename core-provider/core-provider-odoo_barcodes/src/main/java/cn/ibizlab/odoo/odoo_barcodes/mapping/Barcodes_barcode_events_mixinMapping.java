package cn.ibizlab.odoo.odoo_barcodes.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.odoo.odoo_barcodes.dto.Barcodes_barcode_events_mixinDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Barcodes_barcode_events_mixinMapping extends MappingBase<Barcodes_barcode_events_mixinDTO, Barcodes_barcode_events_mixin> {


}

