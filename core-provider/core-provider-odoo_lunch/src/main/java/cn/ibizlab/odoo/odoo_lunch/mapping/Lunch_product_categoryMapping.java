package cn.ibizlab.odoo.odoo_lunch.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category;
import cn.ibizlab.odoo.odoo_lunch.dto.Lunch_product_categoryDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Lunch_product_categoryMapping extends MappingBase<Lunch_product_categoryDTO, Lunch_product_category> {


}

