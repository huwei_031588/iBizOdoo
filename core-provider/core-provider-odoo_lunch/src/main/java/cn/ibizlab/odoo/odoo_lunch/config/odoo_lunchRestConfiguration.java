package cn.ibizlab.odoo.odoo_lunch.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_lunch")
public class odoo_lunchRestConfiguration {

}
