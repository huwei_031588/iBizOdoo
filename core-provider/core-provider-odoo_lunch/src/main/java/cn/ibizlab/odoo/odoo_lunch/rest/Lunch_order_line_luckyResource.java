package cn.ibizlab.odoo.odoo_lunch.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_lunch.dto.*;
import cn.ibizlab.odoo.odoo_lunch.mapping.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line_lucky;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_order_line_luckyService;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_line_luckySearchContext;




@Slf4j
@Api(tags = {"Lunch_order_line_lucky" })
@RestController("odoo_lunch-lunch_order_line_lucky")
@RequestMapping("")
public class Lunch_order_line_luckyResource {

    @Autowired
    private ILunch_order_line_luckyService lunch_order_line_luckyService;

    @Autowired
    @Lazy
    private Lunch_order_line_luckyMapping lunch_order_line_luckyMapping;







    @PreAuthorize("hasPermission(#lunch_order_line_lucky_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Lunch_order_line_lucky" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_order_line_luckies/{lunch_order_line_lucky_id}")
    public ResponseEntity<Lunch_order_line_luckyDTO> get(@PathVariable("lunch_order_line_lucky_id") Integer lunch_order_line_lucky_id) {
        Lunch_order_line_lucky domain = lunch_order_line_luckyService.get(lunch_order_line_lucky_id);
        Lunch_order_line_luckyDTO dto = lunch_order_line_luckyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Lunch_order_line_lucky" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_line_luckies")

    public ResponseEntity<Lunch_order_line_luckyDTO> create(@RequestBody Lunch_order_line_luckyDTO lunch_order_line_luckydto) {
        Lunch_order_line_lucky domain = lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydto);
		lunch_order_line_luckyService.create(domain);
        Lunch_order_line_luckyDTO dto = lunch_order_line_luckyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Lunch_order_line_lucky" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_line_luckies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_order_line_luckyDTO> lunch_order_line_luckydtos) {
        lunch_order_line_luckyService.createBatch(lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#lunch_order_line_lucky_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Lunch_order_line_lucky" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_line_luckies/{lunch_order_line_lucky_id}")

    public ResponseEntity<Lunch_order_line_luckyDTO> update(@PathVariable("lunch_order_line_lucky_id") Integer lunch_order_line_lucky_id, @RequestBody Lunch_order_line_luckyDTO lunch_order_line_luckydto) {
		Lunch_order_line_lucky domain = lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydto);
        domain.setId(lunch_order_line_lucky_id);
		lunch_order_line_luckyService.update(domain);
		Lunch_order_line_luckyDTO dto = lunch_order_line_luckyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#lunch_order_line_lucky_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Lunch_order_line_lucky" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_line_luckies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_order_line_luckyDTO> lunch_order_line_luckydtos) {
        lunch_order_line_luckyService.updateBatch(lunch_order_line_luckyMapping.toDomain(lunch_order_line_luckydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#lunch_order_line_lucky_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Lunch_order_line_lucky" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_line_luckies/{lunch_order_line_lucky_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_order_line_lucky_id") Integer lunch_order_line_lucky_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_order_line_luckyService.remove(lunch_order_line_lucky_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Lunch_order_line_lucky" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_line_luckies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        lunch_order_line_luckyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Lunch_order_line_lucky" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_order_line_luckies/fetchdefault")
	public ResponseEntity<List<Lunch_order_line_luckyDTO>> fetchDefault(Lunch_order_line_luckySearchContext context) {
        Page<Lunch_order_line_lucky> domains = lunch_order_line_luckyService.searchDefault(context) ;
        List<Lunch_order_line_luckyDTO> list = lunch_order_line_luckyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Lunch_order_line_lucky" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_order_line_luckies/searchdefault")
	public ResponseEntity<Page<Lunch_order_line_luckyDTO>> searchDefault(Lunch_order_line_luckySearchContext context) {
        Page<Lunch_order_line_lucky> domains = lunch_order_line_luckyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_order_line_luckyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Lunch_order_line_lucky getEntity(){
        return new Lunch_order_line_lucky();
    }

}
