package cn.ibizlab.odoo.odoo_lunch.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-lunch")
@Data
public class odoo_lunchServiceProperties {

	private boolean enabled;

	private boolean auth;


}