package cn.ibizlab.odoo.odoo_lunch.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line;
import cn.ibizlab.odoo.odoo_lunch.dto.Lunch_order_lineDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Lunch_order_lineMapping extends MappingBase<Lunch_order_lineDTO, Lunch_order_line> {


}

