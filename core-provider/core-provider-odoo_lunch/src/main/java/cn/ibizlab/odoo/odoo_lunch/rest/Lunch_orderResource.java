package cn.ibizlab.odoo.odoo_lunch.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_lunch.dto.*;
import cn.ibizlab.odoo.odoo_lunch.mapping.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_orderService;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_orderSearchContext;




@Slf4j
@Api(tags = {"Lunch_order" })
@RestController("odoo_lunch-lunch_order")
@RequestMapping("")
public class Lunch_orderResource {

    @Autowired
    private ILunch_orderService lunch_orderService;

    @Autowired
    @Lazy
    private Lunch_orderMapping lunch_orderMapping;







    @PreAuthorize("hasPermission(#lunch_order_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Lunch_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_orders/{lunch_order_id}")
    public ResponseEntity<Lunch_orderDTO> get(@PathVariable("lunch_order_id") Integer lunch_order_id) {
        Lunch_order domain = lunch_orderService.get(lunch_order_id);
        Lunch_orderDTO dto = lunch_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Lunch_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_orders")

    public ResponseEntity<Lunch_orderDTO> create(@RequestBody Lunch_orderDTO lunch_orderdto) {
        Lunch_order domain = lunch_orderMapping.toDomain(lunch_orderdto);
		lunch_orderService.create(domain);
        Lunch_orderDTO dto = lunch_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Lunch_order" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_orderDTO> lunch_orderdtos) {
        lunch_orderService.createBatch(lunch_orderMapping.toDomain(lunch_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#lunch_order_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Lunch_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_orders/{lunch_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_order_id") Integer lunch_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_orderService.remove(lunch_order_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Lunch_order" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        lunch_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#lunch_order_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Lunch_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_orders/{lunch_order_id}")

    public ResponseEntity<Lunch_orderDTO> update(@PathVariable("lunch_order_id") Integer lunch_order_id, @RequestBody Lunch_orderDTO lunch_orderdto) {
		Lunch_order domain = lunch_orderMapping.toDomain(lunch_orderdto);
        domain.setId(lunch_order_id);
		lunch_orderService.update(domain);
		Lunch_orderDTO dto = lunch_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#lunch_order_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Lunch_order" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_orderDTO> lunch_orderdtos) {
        lunch_orderService.updateBatch(lunch_orderMapping.toDomain(lunch_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Lunch_order" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_orders/fetchdefault")
	public ResponseEntity<List<Lunch_orderDTO>> fetchDefault(Lunch_orderSearchContext context) {
        Page<Lunch_order> domains = lunch_orderService.searchDefault(context) ;
        List<Lunch_orderDTO> list = lunch_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Lunch_order" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_orders/searchdefault")
	public ResponseEntity<Page<Lunch_orderDTO>> searchDefault(Lunch_orderSearchContext context) {
        Page<Lunch_order> domains = lunch_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Lunch_order getEntity(){
        return new Lunch_order();
    }

}
