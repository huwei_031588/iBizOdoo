package cn.ibizlab.odoo.odoo_lunch.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_lunch.dto.*;
import cn.ibizlab.odoo.odoo_lunch.mapping.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_productService;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_productSearchContext;




@Slf4j
@Api(tags = {"Lunch_product" })
@RestController("odoo_lunch-lunch_product")
@RequestMapping("")
public class Lunch_productResource {

    @Autowired
    private ILunch_productService lunch_productService;

    @Autowired
    @Lazy
    private Lunch_productMapping lunch_productMapping;




    @PreAuthorize("hasPermission(#lunch_product_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Lunch_product" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_products/{lunch_product_id}")
    public ResponseEntity<Lunch_productDTO> get(@PathVariable("lunch_product_id") Integer lunch_product_id) {
        Lunch_product domain = lunch_productService.get(lunch_product_id);
        Lunch_productDTO dto = lunch_productMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#lunch_product_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Lunch_product" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_products/{lunch_product_id}")

    public ResponseEntity<Lunch_productDTO> update(@PathVariable("lunch_product_id") Integer lunch_product_id, @RequestBody Lunch_productDTO lunch_productdto) {
		Lunch_product domain = lunch_productMapping.toDomain(lunch_productdto);
        domain.setId(lunch_product_id);
		lunch_productService.update(domain);
		Lunch_productDTO dto = lunch_productMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#lunch_product_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Lunch_product" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_products/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_productDTO> lunch_productdtos) {
        lunch_productService.updateBatch(lunch_productMapping.toDomain(lunch_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Lunch_product" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_products")

    public ResponseEntity<Lunch_productDTO> create(@RequestBody Lunch_productDTO lunch_productdto) {
        Lunch_product domain = lunch_productMapping.toDomain(lunch_productdto);
		lunch_productService.create(domain);
        Lunch_productDTO dto = lunch_productMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Lunch_product" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_products/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_productDTO> lunch_productdtos) {
        lunch_productService.createBatch(lunch_productMapping.toDomain(lunch_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#lunch_product_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Lunch_product" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_products/{lunch_product_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_product_id") Integer lunch_product_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_productService.remove(lunch_product_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Lunch_product" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_products/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        lunch_productService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Lunch_product" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_products/fetchdefault")
	public ResponseEntity<List<Lunch_productDTO>> fetchDefault(Lunch_productSearchContext context) {
        Page<Lunch_product> domains = lunch_productService.searchDefault(context) ;
        List<Lunch_productDTO> list = lunch_productMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Lunch_product" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_products/searchdefault")
	public ResponseEntity<Page<Lunch_productDTO>> searchDefault(Lunch_productSearchContext context) {
        Page<Lunch_product> domains = lunch_productService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_productMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Lunch_product getEntity(){
        return new Lunch_product();
    }

}
