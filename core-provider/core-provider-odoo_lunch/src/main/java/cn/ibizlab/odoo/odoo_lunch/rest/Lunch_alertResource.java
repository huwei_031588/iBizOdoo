package cn.ibizlab.odoo.odoo_lunch.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_lunch.dto.*;
import cn.ibizlab.odoo.odoo_lunch.mapping.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_alert;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_alertService;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_alertSearchContext;




@Slf4j
@Api(tags = {"Lunch_alert" })
@RestController("odoo_lunch-lunch_alert")
@RequestMapping("")
public class Lunch_alertResource {

    @Autowired
    private ILunch_alertService lunch_alertService;

    @Autowired
    @Lazy
    private Lunch_alertMapping lunch_alertMapping;




    @PreAuthorize("hasPermission('Remove',{#lunch_alert_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Lunch_alert" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_alerts/{lunch_alert_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_alert_id") Integer lunch_alert_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_alertService.remove(lunch_alert_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Lunch_alert" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_alerts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        lunch_alertService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#lunch_alert_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Lunch_alert" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_alerts/{lunch_alert_id}")

    public ResponseEntity<Lunch_alertDTO> update(@PathVariable("lunch_alert_id") Integer lunch_alert_id, @RequestBody Lunch_alertDTO lunch_alertdto) {
		Lunch_alert domain = lunch_alertMapping.toDomain(lunch_alertdto);
        domain.setId(lunch_alert_id);
		lunch_alertService.update(domain);
		Lunch_alertDTO dto = lunch_alertMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#lunch_alert_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Lunch_alert" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_alerts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_alertDTO> lunch_alertdtos) {
        lunch_alertService.updateBatch(lunch_alertMapping.toDomain(lunch_alertdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#lunch_alert_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Lunch_alert" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_alerts/{lunch_alert_id}")
    public ResponseEntity<Lunch_alertDTO> get(@PathVariable("lunch_alert_id") Integer lunch_alert_id) {
        Lunch_alert domain = lunch_alertService.get(lunch_alert_id);
        Lunch_alertDTO dto = lunch_alertMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Lunch_alert" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_alerts")

    public ResponseEntity<Lunch_alertDTO> create(@RequestBody Lunch_alertDTO lunch_alertdto) {
        Lunch_alert domain = lunch_alertMapping.toDomain(lunch_alertdto);
		lunch_alertService.create(domain);
        Lunch_alertDTO dto = lunch_alertMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Lunch_alert" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_alerts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_alertDTO> lunch_alertdtos) {
        lunch_alertService.createBatch(lunch_alertMapping.toDomain(lunch_alertdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Lunch_alert" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_alerts/fetchdefault")
	public ResponseEntity<List<Lunch_alertDTO>> fetchDefault(Lunch_alertSearchContext context) {
        Page<Lunch_alert> domains = lunch_alertService.searchDefault(context) ;
        List<Lunch_alertDTO> list = lunch_alertMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Lunch_alert" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_alerts/searchdefault")
	public ResponseEntity<Page<Lunch_alertDTO>> searchDefault(Lunch_alertSearchContext context) {
        Page<Lunch_alert> domains = lunch_alertService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_alertMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Lunch_alert getEntity(){
        return new Lunch_alert();
    }

}
