package cn.ibizlab.odoo.odoo_lunch.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_lunch.dto.*;
import cn.ibizlab.odoo.odoo_lunch.mapping.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_product_categoryService;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_product_categorySearchContext;




@Slf4j
@Api(tags = {"Lunch_product_category" })
@RestController("odoo_lunch-lunch_product_category")
@RequestMapping("")
public class Lunch_product_categoryResource {

    @Autowired
    private ILunch_product_categoryService lunch_product_categoryService;

    @Autowired
    @Lazy
    private Lunch_product_categoryMapping lunch_product_categoryMapping;




    @PreAuthorize("hasPermission(#lunch_product_category_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Lunch_product_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_product_categories/{lunch_product_category_id}")
    public ResponseEntity<Lunch_product_categoryDTO> get(@PathVariable("lunch_product_category_id") Integer lunch_product_category_id) {
        Lunch_product_category domain = lunch_product_categoryService.get(lunch_product_category_id);
        Lunch_product_categoryDTO dto = lunch_product_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission(#lunch_product_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Lunch_product_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_product_categories/{lunch_product_category_id}")

    public ResponseEntity<Lunch_product_categoryDTO> update(@PathVariable("lunch_product_category_id") Integer lunch_product_category_id, @RequestBody Lunch_product_categoryDTO lunch_product_categorydto) {
		Lunch_product_category domain = lunch_product_categoryMapping.toDomain(lunch_product_categorydto);
        domain.setId(lunch_product_category_id);
		lunch_product_categoryService.update(domain);
		Lunch_product_categoryDTO dto = lunch_product_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#lunch_product_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Lunch_product_category" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_product_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_product_categoryDTO> lunch_product_categorydtos) {
        lunch_product_categoryService.updateBatch(lunch_product_categoryMapping.toDomain(lunch_product_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#lunch_product_category_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Lunch_product_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_product_categories/{lunch_product_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_product_category_id") Integer lunch_product_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_product_categoryService.remove(lunch_product_category_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Lunch_product_category" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_product_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        lunch_product_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Lunch_product_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_product_categories")

    public ResponseEntity<Lunch_product_categoryDTO> create(@RequestBody Lunch_product_categoryDTO lunch_product_categorydto) {
        Lunch_product_category domain = lunch_product_categoryMapping.toDomain(lunch_product_categorydto);
		lunch_product_categoryService.create(domain);
        Lunch_product_categoryDTO dto = lunch_product_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Lunch_product_category" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_product_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_product_categoryDTO> lunch_product_categorydtos) {
        lunch_product_categoryService.createBatch(lunch_product_categoryMapping.toDomain(lunch_product_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Lunch_product_category" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_product_categories/fetchdefault")
	public ResponseEntity<List<Lunch_product_categoryDTO>> fetchDefault(Lunch_product_categorySearchContext context) {
        Page<Lunch_product_category> domains = lunch_product_categoryService.searchDefault(context) ;
        List<Lunch_product_categoryDTO> list = lunch_product_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Lunch_product_category" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_product_categories/searchdefault")
	public ResponseEntity<Page<Lunch_product_categoryDTO>> searchDefault(Lunch_product_categorySearchContext context) {
        Page<Lunch_product_category> domains = lunch_product_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_product_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Lunch_product_category getEntity(){
        return new Lunch_product_category();
    }

}
