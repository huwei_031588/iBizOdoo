package cn.ibizlab.odoo.odoo_snailmail.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-snailmail")
@Data
public class odoo_snailmailServiceProperties {

	private boolean enabled;

	private boolean auth;


}