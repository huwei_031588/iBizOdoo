package cn.ibizlab.odoo.odoo_snailmail.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.odoo.odoo_snailmail.dto.Snailmail_letterDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Snailmail_letterMapping extends MappingBase<Snailmail_letterDTO, Snailmail_letter> {


}

