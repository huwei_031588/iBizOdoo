package cn.ibizlab.odoo.odoo_maintenance.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.odoo.odoo_maintenance.dto.Maintenance_requestDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Maintenance_requestMapping extends MappingBase<Maintenance_requestDTO, Maintenance_request> {


}

