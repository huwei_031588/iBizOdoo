package cn.ibizlab.odoo.odoo_maintenance.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_maintenance.dto.*;
import cn.ibizlab.odoo.odoo_maintenance.mapping.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_equipment_categoryService;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipment_categorySearchContext;




@Slf4j
@Api(tags = {"Maintenance_equipment_category" })
@RestController("odoo_maintenance-maintenance_equipment_category")
@RequestMapping("")
public class Maintenance_equipment_categoryResource {

    @Autowired
    private IMaintenance_equipment_categoryService maintenance_equipment_categoryService;

    @Autowired
    @Lazy
    private Maintenance_equipment_categoryMapping maintenance_equipment_categoryMapping;




    @PreAuthorize("hasPermission(#maintenance_equipment_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Maintenance_equipment_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipment_categories/{maintenance_equipment_category_id}")

    public ResponseEntity<Maintenance_equipment_categoryDTO> update(@PathVariable("maintenance_equipment_category_id") Integer maintenance_equipment_category_id, @RequestBody Maintenance_equipment_categoryDTO maintenance_equipment_categorydto) {
		Maintenance_equipment_category domain = maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydto);
        domain.setId(maintenance_equipment_category_id);
		maintenance_equipment_categoryService.update(domain);
		Maintenance_equipment_categoryDTO dto = maintenance_equipment_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#maintenance_equipment_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Maintenance_equipment_category" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipment_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_equipment_categoryDTO> maintenance_equipment_categorydtos) {
        maintenance_equipment_categoryService.updateBatch(maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Maintenance_equipment_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipment_categories")

    public ResponseEntity<Maintenance_equipment_categoryDTO> create(@RequestBody Maintenance_equipment_categoryDTO maintenance_equipment_categorydto) {
        Maintenance_equipment_category domain = maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydto);
		maintenance_equipment_categoryService.create(domain);
        Maintenance_equipment_categoryDTO dto = maintenance_equipment_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Maintenance_equipment_category" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipment_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Maintenance_equipment_categoryDTO> maintenance_equipment_categorydtos) {
        maintenance_equipment_categoryService.createBatch(maintenance_equipment_categoryMapping.toDomain(maintenance_equipment_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#maintenance_equipment_category_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Maintenance_equipment_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipment_categories/{maintenance_equipment_category_id}")
    public ResponseEntity<Maintenance_equipment_categoryDTO> get(@PathVariable("maintenance_equipment_category_id") Integer maintenance_equipment_category_id) {
        Maintenance_equipment_category domain = maintenance_equipment_categoryService.get(maintenance_equipment_category_id);
        Maintenance_equipment_categoryDTO dto = maintenance_equipment_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#maintenance_equipment_category_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Maintenance_equipment_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipment_categories/{maintenance_equipment_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_equipment_category_id") Integer maintenance_equipment_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipment_categoryService.remove(maintenance_equipment_category_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Maintenance_equipment_category" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipment_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        maintenance_equipment_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Maintenance_equipment_category" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_equipment_categories/fetchdefault")
	public ResponseEntity<List<Maintenance_equipment_categoryDTO>> fetchDefault(Maintenance_equipment_categorySearchContext context) {
        Page<Maintenance_equipment_category> domains = maintenance_equipment_categoryService.searchDefault(context) ;
        List<Maintenance_equipment_categoryDTO> list = maintenance_equipment_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Maintenance_equipment_category" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_equipment_categories/searchdefault")
	public ResponseEntity<Page<Maintenance_equipment_categoryDTO>> searchDefault(Maintenance_equipment_categorySearchContext context) {
        Page<Maintenance_equipment_category> domains = maintenance_equipment_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_equipment_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Maintenance_equipment_category getEntity(){
        return new Maintenance_equipment_category();
    }

}
