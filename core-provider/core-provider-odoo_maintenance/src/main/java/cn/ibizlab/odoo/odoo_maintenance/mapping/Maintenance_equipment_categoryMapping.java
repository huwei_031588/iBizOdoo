package cn.ibizlab.odoo.odoo_maintenance.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.odoo.odoo_maintenance.dto.Maintenance_equipment_categoryDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Maintenance_equipment_categoryMapping extends MappingBase<Maintenance_equipment_categoryDTO, Maintenance_equipment_category> {


}

