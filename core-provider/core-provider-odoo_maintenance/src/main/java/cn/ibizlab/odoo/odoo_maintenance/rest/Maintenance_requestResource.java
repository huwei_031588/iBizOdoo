package cn.ibizlab.odoo.odoo_maintenance.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_maintenance.dto.*;
import cn.ibizlab.odoo.odoo_maintenance.mapping.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_requestService;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_requestSearchContext;




@Slf4j
@Api(tags = {"Maintenance_request" })
@RestController("odoo_maintenance-maintenance_request")
@RequestMapping("")
public class Maintenance_requestResource {

    @Autowired
    private IMaintenance_requestService maintenance_requestService;

    @Autowired
    @Lazy
    private Maintenance_requestMapping maintenance_requestMapping;







    @PreAuthorize("hasPermission('Remove',{#maintenance_request_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Maintenance_request" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_requests/{maintenance_request_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_request_id") Integer maintenance_request_id) {
         return ResponseEntity.status(HttpStatus.OK).body(maintenance_requestService.remove(maintenance_request_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Maintenance_request" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_requests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        maintenance_requestService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Maintenance_request" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_requests")

    public ResponseEntity<Maintenance_requestDTO> create(@RequestBody Maintenance_requestDTO maintenance_requestdto) {
        Maintenance_request domain = maintenance_requestMapping.toDomain(maintenance_requestdto);
		maintenance_requestService.create(domain);
        Maintenance_requestDTO dto = maintenance_requestMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Maintenance_request" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_requests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Maintenance_requestDTO> maintenance_requestdtos) {
        maintenance_requestService.createBatch(maintenance_requestMapping.toDomain(maintenance_requestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#maintenance_request_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Maintenance_request" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_requests/{maintenance_request_id}")

    public ResponseEntity<Maintenance_requestDTO> update(@PathVariable("maintenance_request_id") Integer maintenance_request_id, @RequestBody Maintenance_requestDTO maintenance_requestdto) {
		Maintenance_request domain = maintenance_requestMapping.toDomain(maintenance_requestdto);
        domain.setId(maintenance_request_id);
		maintenance_requestService.update(domain);
		Maintenance_requestDTO dto = maintenance_requestMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#maintenance_request_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Maintenance_request" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_requests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_requestDTO> maintenance_requestdtos) {
        maintenance_requestService.updateBatch(maintenance_requestMapping.toDomain(maintenance_requestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#maintenance_request_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Maintenance_request" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_requests/{maintenance_request_id}")
    public ResponseEntity<Maintenance_requestDTO> get(@PathVariable("maintenance_request_id") Integer maintenance_request_id) {
        Maintenance_request domain = maintenance_requestService.get(maintenance_request_id);
        Maintenance_requestDTO dto = maintenance_requestMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Maintenance_request" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_requests/fetchdefault")
	public ResponseEntity<List<Maintenance_requestDTO>> fetchDefault(Maintenance_requestSearchContext context) {
        Page<Maintenance_request> domains = maintenance_requestService.searchDefault(context) ;
        List<Maintenance_requestDTO> list = maintenance_requestMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Maintenance_request" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_requests/searchdefault")
	public ResponseEntity<Page<Maintenance_requestDTO>> searchDefault(Maintenance_requestSearchContext context) {
        Page<Maintenance_request> domains = maintenance_requestService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_requestMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Maintenance_request getEntity(){
        return new Maintenance_request();
    }

}
