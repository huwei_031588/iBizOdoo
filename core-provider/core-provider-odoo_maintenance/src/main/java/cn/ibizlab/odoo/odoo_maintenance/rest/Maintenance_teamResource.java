package cn.ibizlab.odoo.odoo_maintenance.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_maintenance.dto.*;
import cn.ibizlab.odoo.odoo_maintenance.mapping.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_teamService;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_teamSearchContext;




@Slf4j
@Api(tags = {"Maintenance_team" })
@RestController("odoo_maintenance-maintenance_team")
@RequestMapping("")
public class Maintenance_teamResource {

    @Autowired
    private IMaintenance_teamService maintenance_teamService;

    @Autowired
    @Lazy
    private Maintenance_teamMapping maintenance_teamMapping;










    @PreAuthorize("hasPermission('Remove',{#maintenance_team_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Maintenance_team" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_teams/{maintenance_team_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_team_id") Integer maintenance_team_id) {
         return ResponseEntity.status(HttpStatus.OK).body(maintenance_teamService.remove(maintenance_team_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Maintenance_team" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_teams/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        maintenance_teamService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#maintenance_team_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Maintenance_team" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_teams/{maintenance_team_id}")

    public ResponseEntity<Maintenance_teamDTO> update(@PathVariable("maintenance_team_id") Integer maintenance_team_id, @RequestBody Maintenance_teamDTO maintenance_teamdto) {
		Maintenance_team domain = maintenance_teamMapping.toDomain(maintenance_teamdto);
        domain.setId(maintenance_team_id);
		maintenance_teamService.update(domain);
		Maintenance_teamDTO dto = maintenance_teamMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#maintenance_team_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Maintenance_team" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_teams/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_teamDTO> maintenance_teamdtos) {
        maintenance_teamService.updateBatch(maintenance_teamMapping.toDomain(maintenance_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#maintenance_team_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Maintenance_team" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_teams/{maintenance_team_id}")
    public ResponseEntity<Maintenance_teamDTO> get(@PathVariable("maintenance_team_id") Integer maintenance_team_id) {
        Maintenance_team domain = maintenance_teamService.get(maintenance_team_id);
        Maintenance_teamDTO dto = maintenance_teamMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Maintenance_team" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_teams")

    public ResponseEntity<Maintenance_teamDTO> create(@RequestBody Maintenance_teamDTO maintenance_teamdto) {
        Maintenance_team domain = maintenance_teamMapping.toDomain(maintenance_teamdto);
		maintenance_teamService.create(domain);
        Maintenance_teamDTO dto = maintenance_teamMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Maintenance_team" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_teams/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Maintenance_teamDTO> maintenance_teamdtos) {
        maintenance_teamService.createBatch(maintenance_teamMapping.toDomain(maintenance_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Maintenance_team" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_teams/fetchdefault")
	public ResponseEntity<List<Maintenance_teamDTO>> fetchDefault(Maintenance_teamSearchContext context) {
        Page<Maintenance_team> domains = maintenance_teamService.searchDefault(context) ;
        List<Maintenance_teamDTO> list = maintenance_teamMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Maintenance_team" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_teams/searchdefault")
	public ResponseEntity<Page<Maintenance_teamDTO>> searchDefault(Maintenance_teamSearchContext context) {
        Page<Maintenance_team> domains = maintenance_teamService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_teamMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Maintenance_team getEntity(){
        return new Maintenance_team();
    }

}
