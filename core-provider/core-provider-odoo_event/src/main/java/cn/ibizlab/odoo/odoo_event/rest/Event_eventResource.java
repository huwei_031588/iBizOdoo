package cn.ibizlab.odoo.odoo_event.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_event.dto.*;
import cn.ibizlab.odoo.odoo_event.mapping.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_eventService;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_eventSearchContext;




@Slf4j
@Api(tags = {"Event_event" })
@RestController("odoo_event-event_event")
@RequestMapping("")
public class Event_eventResource {

    @Autowired
    private IEvent_eventService event_eventService;

    @Autowired
    @Lazy
    private Event_eventMapping event_eventMapping;




    @PreAuthorize("hasPermission(#event_event_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Event_event" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/event_events/{event_event_id}")
    public ResponseEntity<Event_eventDTO> get(@PathVariable("event_event_id") Integer event_event_id) {
        Event_event domain = event_eventService.get(event_event_id);
        Event_eventDTO dto = event_eventMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Event_event" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/event_events")

    public ResponseEntity<Event_eventDTO> create(@RequestBody Event_eventDTO event_eventdto) {
        Event_event domain = event_eventMapping.toDomain(event_eventdto);
		event_eventService.create(domain);
        Event_eventDTO dto = event_eventMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Event_event" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/event_events/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_eventDTO> event_eventdtos) {
        event_eventService.createBatch(event_eventMapping.toDomain(event_eventdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#event_event_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Event_event" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_events/{event_event_id}")

    public ResponseEntity<Event_eventDTO> update(@PathVariable("event_event_id") Integer event_event_id, @RequestBody Event_eventDTO event_eventdto) {
		Event_event domain = event_eventMapping.toDomain(event_eventdto);
        domain.setId(event_event_id);
		event_eventService.update(domain);
		Event_eventDTO dto = event_eventMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#event_event_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Event_event" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_events/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_eventDTO> event_eventdtos) {
        event_eventService.updateBatch(event_eventMapping.toDomain(event_eventdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#event_event_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Event_event" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_events/{event_event_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_event_id") Integer event_event_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_eventService.remove(event_event_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Event_event" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_events/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        event_eventService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Event_event" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_events/fetchdefault")
	public ResponseEntity<List<Event_eventDTO>> fetchDefault(Event_eventSearchContext context) {
        Page<Event_event> domains = event_eventService.searchDefault(context) ;
        List<Event_eventDTO> list = event_eventMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Event_event" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_events/searchdefault")
	public ResponseEntity<Page<Event_eventDTO>> searchDefault(Event_eventSearchContext context) {
        Page<Event_event> domains = event_eventService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_eventMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Event_event getEntity(){
        return new Event_event();
    }

}
