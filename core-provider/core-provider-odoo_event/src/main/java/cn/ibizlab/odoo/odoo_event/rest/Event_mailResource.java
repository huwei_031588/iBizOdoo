package cn.ibizlab.odoo.odoo_event.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_event.dto.*;
import cn.ibizlab.odoo.odoo_event.mapping.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_mailService;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mailSearchContext;




@Slf4j
@Api(tags = {"Event_mail" })
@RestController("odoo_event-event_mail")
@RequestMapping("")
public class Event_mailResource {

    @Autowired
    private IEvent_mailService event_mailService;

    @Autowired
    @Lazy
    private Event_mailMapping event_mailMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Event_mail" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mails")

    public ResponseEntity<Event_mailDTO> create(@RequestBody Event_mailDTO event_maildto) {
        Event_mail domain = event_mailMapping.toDomain(event_maildto);
		event_mailService.create(domain);
        Event_mailDTO dto = event_mailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Event_mail" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_mailDTO> event_maildtos) {
        event_mailService.createBatch(event_mailMapping.toDomain(event_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#event_mail_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Event_mail" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_mails/{event_mail_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_mail_id") Integer event_mail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_mailService.remove(event_mail_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Event_mail" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_mails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        event_mailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission(#event_mail_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Event_mail" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_mails/{event_mail_id}")

    public ResponseEntity<Event_mailDTO> update(@PathVariable("event_mail_id") Integer event_mail_id, @RequestBody Event_mailDTO event_maildto) {
		Event_mail domain = event_mailMapping.toDomain(event_maildto);
        domain.setId(event_mail_id);
		event_mailService.update(domain);
		Event_mailDTO dto = event_mailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#event_mail_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Event_mail" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_mails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_mailDTO> event_maildtos) {
        event_mailService.updateBatch(event_mailMapping.toDomain(event_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#event_mail_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Event_mail" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/event_mails/{event_mail_id}")
    public ResponseEntity<Event_mailDTO> get(@PathVariable("event_mail_id") Integer event_mail_id) {
        Event_mail domain = event_mailService.get(event_mail_id);
        Event_mailDTO dto = event_mailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Event_mail" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_mails/fetchdefault")
	public ResponseEntity<List<Event_mailDTO>> fetchDefault(Event_mailSearchContext context) {
        Page<Event_mail> domains = event_mailService.searchDefault(context) ;
        List<Event_mailDTO> list = event_mailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Event_mail" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_mails/searchdefault")
	public ResponseEntity<Page<Event_mailDTO>> searchDefault(Event_mailSearchContext context) {
        Page<Event_mail> domains = event_mailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_mailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Event_mail getEntity(){
        return new Event_mail();
    }

}
