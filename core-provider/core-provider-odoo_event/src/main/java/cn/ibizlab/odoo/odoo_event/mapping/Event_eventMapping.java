package cn.ibizlab.odoo.odoo_event.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event;
import cn.ibizlab.odoo.odoo_event.dto.Event_eventDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Event_eventMapping extends MappingBase<Event_eventDTO, Event_event> {


}

