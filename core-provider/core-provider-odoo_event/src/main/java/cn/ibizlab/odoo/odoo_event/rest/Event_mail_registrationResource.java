package cn.ibizlab.odoo.odoo_event.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_event.dto.*;
import cn.ibizlab.odoo.odoo_event.mapping.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail_registration;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_mail_registrationService;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mail_registrationSearchContext;




@Slf4j
@Api(tags = {"Event_mail_registration" })
@RestController("odoo_event-event_mail_registration")
@RequestMapping("")
public class Event_mail_registrationResource {

    @Autowired
    private IEvent_mail_registrationService event_mail_registrationService;

    @Autowired
    @Lazy
    private Event_mail_registrationMapping event_mail_registrationMapping;




    @PreAuthorize("hasPermission('Remove',{#event_mail_registration_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Event_mail_registration" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_mail_registrations/{event_mail_registration_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_mail_registration_id") Integer event_mail_registration_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_mail_registrationService.remove(event_mail_registration_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Event_mail_registration" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_mail_registrations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        event_mail_registrationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#event_mail_registration_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Event_mail_registration" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_mail_registrations/{event_mail_registration_id}")

    public ResponseEntity<Event_mail_registrationDTO> update(@PathVariable("event_mail_registration_id") Integer event_mail_registration_id, @RequestBody Event_mail_registrationDTO event_mail_registrationdto) {
		Event_mail_registration domain = event_mail_registrationMapping.toDomain(event_mail_registrationdto);
        domain.setId(event_mail_registration_id);
		event_mail_registrationService.update(domain);
		Event_mail_registrationDTO dto = event_mail_registrationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#event_mail_registration_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Event_mail_registration" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_mail_registrations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_mail_registrationDTO> event_mail_registrationdtos) {
        event_mail_registrationService.updateBatch(event_mail_registrationMapping.toDomain(event_mail_registrationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Event_mail_registration" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mail_registrations")

    public ResponseEntity<Event_mail_registrationDTO> create(@RequestBody Event_mail_registrationDTO event_mail_registrationdto) {
        Event_mail_registration domain = event_mail_registrationMapping.toDomain(event_mail_registrationdto);
		event_mail_registrationService.create(domain);
        Event_mail_registrationDTO dto = event_mail_registrationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Event_mail_registration" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/event_mail_registrations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_mail_registrationDTO> event_mail_registrationdtos) {
        event_mail_registrationService.createBatch(event_mail_registrationMapping.toDomain(event_mail_registrationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#event_mail_registration_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Event_mail_registration" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/event_mail_registrations/{event_mail_registration_id}")
    public ResponseEntity<Event_mail_registrationDTO> get(@PathVariable("event_mail_registration_id") Integer event_mail_registration_id) {
        Event_mail_registration domain = event_mail_registrationService.get(event_mail_registration_id);
        Event_mail_registrationDTO dto = event_mail_registrationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Event_mail_registration" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_mail_registrations/fetchdefault")
	public ResponseEntity<List<Event_mail_registrationDTO>> fetchDefault(Event_mail_registrationSearchContext context) {
        Page<Event_mail_registration> domains = event_mail_registrationService.searchDefault(context) ;
        List<Event_mail_registrationDTO> list = event_mail_registrationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Event_mail_registration" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_mail_registrations/searchdefault")
	public ResponseEntity<Page<Event_mail_registrationDTO>> searchDefault(Event_mail_registrationSearchContext context) {
        Page<Event_mail_registration> domains = event_mail_registrationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_mail_registrationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Event_mail_registration getEntity(){
        return new Event_mail_registration();
    }

}
