package cn.ibizlab.odoo.odoo_event.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_event.dto.*;
import cn.ibizlab.odoo.odoo_event.mapping.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_typeService;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_typeSearchContext;




@Slf4j
@Api(tags = {"Event_type" })
@RestController("odoo_event-event_type")
@RequestMapping("")
public class Event_typeResource {

    @Autowired
    private IEvent_typeService event_typeService;

    @Autowired
    @Lazy
    private Event_typeMapping event_typeMapping;







    @PreAuthorize("hasPermission(#event_type_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Event_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/event_types/{event_type_id}")
    public ResponseEntity<Event_typeDTO> get(@PathVariable("event_type_id") Integer event_type_id) {
        Event_type domain = event_typeService.get(event_type_id);
        Event_typeDTO dto = event_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#event_type_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Event_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_types/{event_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_type_id") Integer event_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_typeService.remove(event_type_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Event_type" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        event_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#event_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Event_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_types/{event_type_id}")

    public ResponseEntity<Event_typeDTO> update(@PathVariable("event_type_id") Integer event_type_id, @RequestBody Event_typeDTO event_typedto) {
		Event_type domain = event_typeMapping.toDomain(event_typedto);
        domain.setId(event_type_id);
		event_typeService.update(domain);
		Event_typeDTO dto = event_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#event_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Event_type" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_typeDTO> event_typedtos) {
        event_typeService.updateBatch(event_typeMapping.toDomain(event_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Event_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/event_types")

    public ResponseEntity<Event_typeDTO> create(@RequestBody Event_typeDTO event_typedto) {
        Event_type domain = event_typeMapping.toDomain(event_typedto);
		event_typeService.create(domain);
        Event_typeDTO dto = event_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Event_type" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/event_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_typeDTO> event_typedtos) {
        event_typeService.createBatch(event_typeMapping.toDomain(event_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Event_type" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_types/fetchdefault")
	public ResponseEntity<List<Event_typeDTO>> fetchDefault(Event_typeSearchContext context) {
        Page<Event_type> domains = event_typeService.searchDefault(context) ;
        List<Event_typeDTO> list = event_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Event_type" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_types/searchdefault")
	public ResponseEntity<Page<Event_typeDTO>> searchDefault(Event_typeSearchContext context) {
        Page<Event_type> domains = event_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Event_type getEntity(){
        return new Event_type();
    }

}
