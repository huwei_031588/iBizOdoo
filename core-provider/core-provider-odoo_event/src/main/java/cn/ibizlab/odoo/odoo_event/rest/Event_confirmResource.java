package cn.ibizlab.odoo.odoo_event.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_event.dto.*;
import cn.ibizlab.odoo.odoo_event.mapping.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_confirm;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_confirmService;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_confirmSearchContext;




@Slf4j
@Api(tags = {"Event_confirm" })
@RestController("odoo_event-event_confirm")
@RequestMapping("")
public class Event_confirmResource {

    @Autowired
    private IEvent_confirmService event_confirmService;

    @Autowired
    @Lazy
    private Event_confirmMapping event_confirmMapping;










    @PreAuthorize("hasPermission('Remove',{#event_confirm_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Event_confirm" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_confirms/{event_confirm_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_confirm_id") Integer event_confirm_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_confirmService.remove(event_confirm_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Event_confirm" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_confirms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        event_confirmService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#event_confirm_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Event_confirm" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_confirms/{event_confirm_id}")

    public ResponseEntity<Event_confirmDTO> update(@PathVariable("event_confirm_id") Integer event_confirm_id, @RequestBody Event_confirmDTO event_confirmdto) {
		Event_confirm domain = event_confirmMapping.toDomain(event_confirmdto);
        domain.setId(event_confirm_id);
		event_confirmService.update(domain);
		Event_confirmDTO dto = event_confirmMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#event_confirm_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Event_confirm" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_confirms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_confirmDTO> event_confirmdtos) {
        event_confirmService.updateBatch(event_confirmMapping.toDomain(event_confirmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#event_confirm_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Event_confirm" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/event_confirms/{event_confirm_id}")
    public ResponseEntity<Event_confirmDTO> get(@PathVariable("event_confirm_id") Integer event_confirm_id) {
        Event_confirm domain = event_confirmService.get(event_confirm_id);
        Event_confirmDTO dto = event_confirmMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Event_confirm" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/event_confirms")

    public ResponseEntity<Event_confirmDTO> create(@RequestBody Event_confirmDTO event_confirmdto) {
        Event_confirm domain = event_confirmMapping.toDomain(event_confirmdto);
		event_confirmService.create(domain);
        Event_confirmDTO dto = event_confirmMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Event_confirm" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/event_confirms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_confirmDTO> event_confirmdtos) {
        event_confirmService.createBatch(event_confirmMapping.toDomain(event_confirmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Event_confirm" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_confirms/fetchdefault")
	public ResponseEntity<List<Event_confirmDTO>> fetchDefault(Event_confirmSearchContext context) {
        Page<Event_confirm> domains = event_confirmService.searchDefault(context) ;
        List<Event_confirmDTO> list = event_confirmMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Event_confirm" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_confirms/searchdefault")
	public ResponseEntity<Page<Event_confirmDTO>> searchDefault(Event_confirmSearchContext context) {
        Page<Event_confirm> domains = event_confirmService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_confirmMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Event_confirm getEntity(){
        return new Event_confirm();
    }

}
