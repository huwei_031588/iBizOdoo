package cn.ibizlab.odoo.odoo_event.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_event.dto.*;
import cn.ibizlab.odoo.odoo_event.mapping.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_event_ticketService;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_event_ticketSearchContext;




@Slf4j
@Api(tags = {"Event_event_ticket" })
@RestController("odoo_event-event_event_ticket")
@RequestMapping("")
public class Event_event_ticketResource {

    @Autowired
    private IEvent_event_ticketService event_event_ticketService;

    @Autowired
    @Lazy
    private Event_event_ticketMapping event_event_ticketMapping;







    @PreAuthorize("hasPermission(#event_event_ticket_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Event_event_ticket" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_event_tickets/{event_event_ticket_id}")

    public ResponseEntity<Event_event_ticketDTO> update(@PathVariable("event_event_ticket_id") Integer event_event_ticket_id, @RequestBody Event_event_ticketDTO event_event_ticketdto) {
		Event_event_ticket domain = event_event_ticketMapping.toDomain(event_event_ticketdto);
        domain.setId(event_event_ticket_id);
		event_event_ticketService.update(domain);
		Event_event_ticketDTO dto = event_event_ticketMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#event_event_ticket_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Event_event_ticket" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_event_tickets/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_event_ticketDTO> event_event_ticketdtos) {
        event_event_ticketService.updateBatch(event_event_ticketMapping.toDomain(event_event_ticketdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#event_event_ticket_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Event_event_ticket" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/event_event_tickets/{event_event_ticket_id}")
    public ResponseEntity<Event_event_ticketDTO> get(@PathVariable("event_event_ticket_id") Integer event_event_ticket_id) {
        Event_event_ticket domain = event_event_ticketService.get(event_event_ticket_id);
        Event_event_ticketDTO dto = event_event_ticketMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('Remove',{#event_event_ticket_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Event_event_ticket" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_event_tickets/{event_event_ticket_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_event_ticket_id") Integer event_event_ticket_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_event_ticketService.remove(event_event_ticket_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Event_event_ticket" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_event_tickets/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        event_event_ticketService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Event_event_ticket" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets")

    public ResponseEntity<Event_event_ticketDTO> create(@RequestBody Event_event_ticketDTO event_event_ticketdto) {
        Event_event_ticket domain = event_event_ticketMapping.toDomain(event_event_ticketdto);
		event_event_ticketService.create(domain);
        Event_event_ticketDTO dto = event_event_ticketMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Event_event_ticket" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/event_event_tickets/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_event_ticketDTO> event_event_ticketdtos) {
        event_event_ticketService.createBatch(event_event_ticketMapping.toDomain(event_event_ticketdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Event_event_ticket" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_event_tickets/fetchdefault")
	public ResponseEntity<List<Event_event_ticketDTO>> fetchDefault(Event_event_ticketSearchContext context) {
        Page<Event_event_ticket> domains = event_event_ticketService.searchDefault(context) ;
        List<Event_event_ticketDTO> list = event_event_ticketMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Event_event_ticket" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_event_tickets/searchdefault")
	public ResponseEntity<Page<Event_event_ticketDTO>> searchDefault(Event_event_ticketSearchContext context) {
        Page<Event_event_ticket> domains = event_event_ticketService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_event_ticketMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Event_event_ticket getEntity(){
        return new Event_event_ticket();
    }

}
