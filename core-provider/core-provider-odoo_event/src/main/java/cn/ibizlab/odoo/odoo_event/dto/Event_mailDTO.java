package cn.ibizlab.odoo.odoo_event.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Event_mailDTO]
 */
@Data
public class Event_mailDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MAIL_REGISTRATION_IDS]
     *
     */
    @JSONField(name = "mail_registration_ids")
    @JsonProperty("mail_registration_ids")
    private String mailRegistrationIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [INTERVAL_NBR]
     *
     */
    @JSONField(name = "interval_nbr")
    @JsonProperty("interval_nbr")
    private Integer intervalNbr;

    /**
     * 属性 [DONE]
     *
     */
    @JSONField(name = "done")
    @JsonProperty("done")
    private String done;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [INTERVAL_TYPE]
     *
     */
    @JSONField(name = "interval_type")
    @JsonProperty("interval_type")
    private String intervalType;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [MAIL_SENT]
     *
     */
    @JSONField(name = "mail_sent")
    @JsonProperty("mail_sent")
    private String mailSent;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled_date")
    private Timestamp scheduledDate;

    /**
     * 属性 [INTERVAL_UNIT]
     *
     */
    @JSONField(name = "interval_unit")
    @JsonProperty("interval_unit")
    private String intervalUnit;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [EVENT_ID_TEXT]
     *
     */
    @JSONField(name = "event_id_text")
    @JsonProperty("event_id_text")
    private String eventIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [EVENT_ID]
     *
     */
    @JSONField(name = "event_id")
    @JsonProperty("event_id")
    private Integer eventId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Integer templateId;


    /**
     * 设置 [INTERVAL_NBR]
     */
    public void setIntervalNbr(Integer  intervalNbr){
        this.intervalNbr = intervalNbr ;
        this.modify("interval_nbr",intervalNbr);
    }

    /**
     * 设置 [DONE]
     */
    public void setDone(String  done){
        this.done = done ;
        this.modify("done",done);
    }

    /**
     * 设置 [INTERVAL_TYPE]
     */
    public void setIntervalType(String  intervalType){
        this.intervalType = intervalType ;
        this.modify("interval_type",intervalType);
    }

    /**
     * 设置 [MAIL_SENT]
     */
    public void setMailSent(String  mailSent){
        this.mailSent = mailSent ;
        this.modify("mail_sent",mailSent);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    public void setScheduledDate(Timestamp  scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }

    /**
     * 设置 [INTERVAL_UNIT]
     */
    public void setIntervalUnit(String  intervalUnit){
        this.intervalUnit = intervalUnit ;
        this.modify("interval_unit",intervalUnit);
    }

    /**
     * 设置 [EVENT_ID]
     */
    public void setEventId(Integer  eventId){
        this.eventId = eventId ;
        this.modify("event_id",eventId);
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    public void setTemplateId(Integer  templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }


}

