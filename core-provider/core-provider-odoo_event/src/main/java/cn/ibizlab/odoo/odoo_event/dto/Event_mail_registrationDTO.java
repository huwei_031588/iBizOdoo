package cn.ibizlab.odoo.odoo_event.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Event_mail_registrationDTO]
 */
@Data
public class Event_mail_registrationDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MAIL_SENT]
     *
     */
    @JSONField(name = "mail_sent")
    @JsonProperty("mail_sent")
    private String mailSent;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled_date")
    private Timestamp scheduledDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [REGISTRATION_ID_TEXT]
     *
     */
    @JSONField(name = "registration_id_text")
    @JsonProperty("registration_id_text")
    private String registrationIdText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [REGISTRATION_ID]
     *
     */
    @JSONField(name = "registration_id")
    @JsonProperty("registration_id")
    private Integer registrationId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [SCHEDULER_ID]
     *
     */
    @JSONField(name = "scheduler_id")
    @JsonProperty("scheduler_id")
    private Integer schedulerId;


    /**
     * 设置 [MAIL_SENT]
     */
    public void setMailSent(String  mailSent){
        this.mailSent = mailSent ;
        this.modify("mail_sent",mailSent);
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    public void setScheduledDate(Timestamp  scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }

    /**
     * 设置 [REGISTRATION_ID]
     */
    public void setRegistrationId(Integer  registrationId){
        this.registrationId = registrationId ;
        this.modify("registration_id",registrationId);
    }

    /**
     * 设置 [SCHEDULER_ID]
     */
    public void setSchedulerId(Integer  schedulerId){
        this.schedulerId = schedulerId ;
        this.modify("scheduler_id",schedulerId);
    }


}

