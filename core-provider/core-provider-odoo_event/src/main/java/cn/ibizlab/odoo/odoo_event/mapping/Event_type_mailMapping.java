package cn.ibizlab.odoo.odoo_event.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.odoo.odoo_event.dto.Event_type_mailDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Event_type_mailMapping extends MappingBase<Event_type_mailDTO, Event_type_mail> {


}

