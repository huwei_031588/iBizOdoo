package cn.ibizlab.odoo.service.odoo_note.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_note.dto.Note_noteDTO;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_note;
import cn.ibizlab.odoo.core.odoo_note.service.INote_noteService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_noteSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Note_note" })
@RestController
@RequestMapping("")
public class Note_noteResource {

    @Autowired
    private INote_noteService note_noteService;

    public INote_noteService getNote_noteService() {
        return this.note_noteService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Note_note" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_notes/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Note_noteDTO> note_notedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Note_note" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_notes/{note_note_id}")
    public ResponseEntity<Note_noteDTO> get(@PathVariable("note_note_id") Integer note_note_id) {
        Note_noteDTO dto = new Note_noteDTO();
        Note_note domain = note_noteService.get(note_note_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Note_note" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_notes/{note_note_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("note_note_id") Integer note_note_id) {
        Note_noteDTO note_notedto = new Note_noteDTO();
		Note_note domain = new Note_note();
		note_notedto.setId(note_note_id);
		domain.setId(note_note_id);
        Boolean rst = note_noteService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Note_note" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_notes")

    public ResponseEntity<Note_noteDTO> create(@RequestBody Note_noteDTO note_notedto) {
        Note_noteDTO dto = new Note_noteDTO();
        Note_note domain = note_notedto.toDO();
		note_noteService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Note_note" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_notes/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Note_noteDTO> note_notedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Note_note" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_notes/{note_note_id}")

    public ResponseEntity<Note_noteDTO> update(@PathVariable("note_note_id") Integer note_note_id, @RequestBody Note_noteDTO note_notedto) {
		Note_note domain = note_notedto.toDO();
        domain.setId(note_note_id);
		note_noteService.update(domain);
		Note_noteDTO dto = new Note_noteDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Note_note" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_notes/createBatch")
    public ResponseEntity<Boolean> createBatchNote_note(@RequestBody List<Note_noteDTO> note_notedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Note_note" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_note/note_notes/fetchdefault")
	public ResponseEntity<Page<Note_noteDTO>> fetchDefault(Note_noteSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Note_noteDTO> list = new ArrayList<Note_noteDTO>();
        
        Page<Note_note> domains = note_noteService.searchDefault(context) ;
        for(Note_note note_note : domains.getContent()){
            Note_noteDTO dto = new Note_noteDTO();
            dto.fromDO(note_note);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
