package cn.ibizlab.odoo.service.odoo_note.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_note.dto.Note_tagDTO;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_tag;
import cn.ibizlab.odoo.core.odoo_note.service.INote_tagService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_tagSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Note_tag" })
@RestController
@RequestMapping("")
public class Note_tagResource {

    @Autowired
    private INote_tagService note_tagService;

    public INote_tagService getNote_tagService() {
        return this.note_tagService;
    }

    @ApiOperation(value = "删除数据", tags = {"Note_tag" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_tags/{note_tag_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("note_tag_id") Integer note_tag_id) {
        Note_tagDTO note_tagdto = new Note_tagDTO();
		Note_tag domain = new Note_tag();
		note_tagdto.setId(note_tag_id);
		domain.setId(note_tag_id);
        Boolean rst = note_tagService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Note_tag" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_tags/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Note_tagDTO> note_tagdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Note_tag" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_tags/{note_tag_id}")

    public ResponseEntity<Note_tagDTO> update(@PathVariable("note_tag_id") Integer note_tag_id, @RequestBody Note_tagDTO note_tagdto) {
		Note_tag domain = note_tagdto.toDO();
        domain.setId(note_tag_id);
		note_tagService.update(domain);
		Note_tagDTO dto = new Note_tagDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Note_tag" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_tags")

    public ResponseEntity<Note_tagDTO> create(@RequestBody Note_tagDTO note_tagdto) {
        Note_tagDTO dto = new Note_tagDTO();
        Note_tag domain = note_tagdto.toDO();
		note_tagService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Note_tag" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_tags/createBatch")
    public ResponseEntity<Boolean> createBatchNote_tag(@RequestBody List<Note_tagDTO> note_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Note_tag" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_tags/{note_tag_id}")
    public ResponseEntity<Note_tagDTO> get(@PathVariable("note_tag_id") Integer note_tag_id) {
        Note_tagDTO dto = new Note_tagDTO();
        Note_tag domain = note_tagService.get(note_tag_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Note_tag" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_tags/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Note_tagDTO> note_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Note_tag" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_note/note_tags/fetchdefault")
	public ResponseEntity<Page<Note_tagDTO>> fetchDefault(Note_tagSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Note_tagDTO> list = new ArrayList<Note_tagDTO>();
        
        Page<Note_tag> domains = note_tagService.searchDefault(context) ;
        for(Note_tag note_tag : domains.getContent()){
            Note_tagDTO dto = new Note_tagDTO();
            dto.fromDO(note_tag);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
