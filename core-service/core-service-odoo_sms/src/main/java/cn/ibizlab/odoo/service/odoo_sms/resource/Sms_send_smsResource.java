package cn.ibizlab.odoo.service.odoo_sms.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_sms.dto.Sms_send_smsDTO;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_send_sms;
import cn.ibizlab.odoo.core.odoo_sms.service.ISms_send_smsService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_send_smsSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Sms_send_sms" })
@RestController
@RequestMapping("")
public class Sms_send_smsResource {

    @Autowired
    private ISms_send_smsService sms_send_smsService;

    public ISms_send_smsService getSms_send_smsService() {
        return this.sms_send_smsService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Sms_send_sms" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sms/sms_send_sms/createBatch")
    public ResponseEntity<Boolean> createBatchSms_send_sms(@RequestBody List<Sms_send_smsDTO> sms_send_smsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Sms_send_sms" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sms/sms_send_sms/{sms_send_sms_id}")

    public ResponseEntity<Sms_send_smsDTO> update(@PathVariable("sms_send_sms_id") Integer sms_send_sms_id, @RequestBody Sms_send_smsDTO sms_send_smsdto) {
		Sms_send_sms domain = sms_send_smsdto.toDO();
        domain.setId(sms_send_sms_id);
		sms_send_smsService.update(domain);
		Sms_send_smsDTO dto = new Sms_send_smsDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Sms_send_sms" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sms/sms_send_sms")

    public ResponseEntity<Sms_send_smsDTO> create(@RequestBody Sms_send_smsDTO sms_send_smsdto) {
        Sms_send_smsDTO dto = new Sms_send_smsDTO();
        Sms_send_sms domain = sms_send_smsdto.toDO();
		sms_send_smsService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Sms_send_sms" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sms/sms_send_sms/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sms_send_smsDTO> sms_send_smsdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Sms_send_sms" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sms/sms_send_sms/{sms_send_sms_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sms_send_sms_id") Integer sms_send_sms_id) {
        Sms_send_smsDTO sms_send_smsdto = new Sms_send_smsDTO();
		Sms_send_sms domain = new Sms_send_sms();
		sms_send_smsdto.setId(sms_send_sms_id);
		domain.setId(sms_send_sms_id);
        Boolean rst = sms_send_smsService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Sms_send_sms" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sms/sms_send_sms/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sms_send_smsDTO> sms_send_smsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Sms_send_sms" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_sms/sms_send_sms/{sms_send_sms_id}")
    public ResponseEntity<Sms_send_smsDTO> get(@PathVariable("sms_send_sms_id") Integer sms_send_sms_id) {
        Sms_send_smsDTO dto = new Sms_send_smsDTO();
        Sms_send_sms domain = sms_send_smsService.get(sms_send_sms_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sms_send_sms" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_sms/sms_send_sms/fetchdefault")
	public ResponseEntity<Page<Sms_send_smsDTO>> fetchDefault(Sms_send_smsSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sms_send_smsDTO> list = new ArrayList<Sms_send_smsDTO>();
        
        Page<Sms_send_sms> domains = sms_send_smsService.searchDefault(context) ;
        for(Sms_send_sms sms_send_sms : domains.getContent()){
            Sms_send_smsDTO dto = new Sms_send_smsDTO();
            dto.fromDO(sms_send_sms);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
