package cn.ibizlab.odoo.service.odoo_sms.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_sms")
public class odoo_smsRestConfiguration {

}
