package cn.ibizlab.odoo.service.odoo_sms.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.sms")
@Data
public class odoo_smsServiceProperties {

	private boolean enabled;

	private boolean auth;


}