package cn.ibizlab.odoo.service.odoo_maintenance.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_maintenance.dto.Maintenance_stageDTO;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_stageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_stageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Maintenance_stage" })
@RestController
@RequestMapping("")
public class Maintenance_stageResource {

    @Autowired
    private IMaintenance_stageService maintenance_stageService;

    public IMaintenance_stageService getMaintenance_stageService() {
        return this.maintenance_stageService;
    }

    @ApiOperation(value = "建立数据", tags = {"Maintenance_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_stages")

    public ResponseEntity<Maintenance_stageDTO> create(@RequestBody Maintenance_stageDTO maintenance_stagedto) {
        Maintenance_stageDTO dto = new Maintenance_stageDTO();
        Maintenance_stage domain = maintenance_stagedto.toDO();
		maintenance_stageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Maintenance_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_stages/{maintenance_stage_id}")
    public ResponseEntity<Maintenance_stageDTO> get(@PathVariable("maintenance_stage_id") Integer maintenance_stage_id) {
        Maintenance_stageDTO dto = new Maintenance_stageDTO();
        Maintenance_stage domain = maintenance_stageService.get(maintenance_stage_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Maintenance_stage" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_stages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Maintenance_stageDTO> maintenance_stagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Maintenance_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_stages/{maintenance_stage_id}")

    public ResponseEntity<Maintenance_stageDTO> update(@PathVariable("maintenance_stage_id") Integer maintenance_stage_id, @RequestBody Maintenance_stageDTO maintenance_stagedto) {
		Maintenance_stage domain = maintenance_stagedto.toDO();
        domain.setId(maintenance_stage_id);
		maintenance_stageService.update(domain);
		Maintenance_stageDTO dto = new Maintenance_stageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Maintenance_stage" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_stages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_stageDTO> maintenance_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Maintenance_stage" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_stages/createBatch")
    public ResponseEntity<Boolean> createBatchMaintenance_stage(@RequestBody List<Maintenance_stageDTO> maintenance_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Maintenance_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_stages/{maintenance_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_stage_id") Integer maintenance_stage_id) {
        Maintenance_stageDTO maintenance_stagedto = new Maintenance_stageDTO();
		Maintenance_stage domain = new Maintenance_stage();
		maintenance_stagedto.setId(maintenance_stage_id);
		domain.setId(maintenance_stage_id);
        Boolean rst = maintenance_stageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Maintenance_stage" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_maintenance/maintenance_stages/fetchdefault")
	public ResponseEntity<Page<Maintenance_stageDTO>> fetchDefault(Maintenance_stageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Maintenance_stageDTO> list = new ArrayList<Maintenance_stageDTO>();
        
        Page<Maintenance_stage> domains = maintenance_stageService.searchDefault(context) ;
        for(Maintenance_stage maintenance_stage : domains.getContent()){
            Maintenance_stageDTO dto = new Maintenance_stageDTO();
            dto.fromDO(maintenance_stage);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
