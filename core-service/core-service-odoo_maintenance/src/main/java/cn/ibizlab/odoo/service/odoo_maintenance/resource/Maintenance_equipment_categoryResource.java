package cn.ibizlab.odoo.service.odoo_maintenance.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_maintenance.dto.Maintenance_equipment_categoryDTO;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_equipment_categoryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipment_categorySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Maintenance_equipment_category" })
@RestController
@RequestMapping("")
public class Maintenance_equipment_categoryResource {

    @Autowired
    private IMaintenance_equipment_categoryService maintenance_equipment_categoryService;

    public IMaintenance_equipment_categoryService getMaintenance_equipment_categoryService() {
        return this.maintenance_equipment_categoryService;
    }

    @ApiOperation(value = "更新数据", tags = {"Maintenance_equipment_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_equipment_categories/{maintenance_equipment_category_id}")

    public ResponseEntity<Maintenance_equipment_categoryDTO> update(@PathVariable("maintenance_equipment_category_id") Integer maintenance_equipment_category_id, @RequestBody Maintenance_equipment_categoryDTO maintenance_equipment_categorydto) {
		Maintenance_equipment_category domain = maintenance_equipment_categorydto.toDO();
        domain.setId(maintenance_equipment_category_id);
		maintenance_equipment_categoryService.update(domain);
		Maintenance_equipment_categoryDTO dto = new Maintenance_equipment_categoryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Maintenance_equipment_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_equipment_categories")

    public ResponseEntity<Maintenance_equipment_categoryDTO> create(@RequestBody Maintenance_equipment_categoryDTO maintenance_equipment_categorydto) {
        Maintenance_equipment_categoryDTO dto = new Maintenance_equipment_categoryDTO();
        Maintenance_equipment_category domain = maintenance_equipment_categorydto.toDO();
		maintenance_equipment_categoryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Maintenance_equipment_category" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_equipment_categories/createBatch")
    public ResponseEntity<Boolean> createBatchMaintenance_equipment_category(@RequestBody List<Maintenance_equipment_categoryDTO> maintenance_equipment_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Maintenance_equipment_category" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_equipment_categories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_equipment_categoryDTO> maintenance_equipment_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Maintenance_equipment_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_equipment_categories/{maintenance_equipment_category_id}")
    public ResponseEntity<Maintenance_equipment_categoryDTO> get(@PathVariable("maintenance_equipment_category_id") Integer maintenance_equipment_category_id) {
        Maintenance_equipment_categoryDTO dto = new Maintenance_equipment_categoryDTO();
        Maintenance_equipment_category domain = maintenance_equipment_categoryService.get(maintenance_equipment_category_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Maintenance_equipment_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_equipment_categories/{maintenance_equipment_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_equipment_category_id") Integer maintenance_equipment_category_id) {
        Maintenance_equipment_categoryDTO maintenance_equipment_categorydto = new Maintenance_equipment_categoryDTO();
		Maintenance_equipment_category domain = new Maintenance_equipment_category();
		maintenance_equipment_categorydto.setId(maintenance_equipment_category_id);
		domain.setId(maintenance_equipment_category_id);
        Boolean rst = maintenance_equipment_categoryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Maintenance_equipment_category" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_equipment_categories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Maintenance_equipment_categoryDTO> maintenance_equipment_categorydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Maintenance_equipment_category" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_maintenance/maintenance_equipment_categories/fetchdefault")
	public ResponseEntity<Page<Maintenance_equipment_categoryDTO>> fetchDefault(Maintenance_equipment_categorySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Maintenance_equipment_categoryDTO> list = new ArrayList<Maintenance_equipment_categoryDTO>();
        
        Page<Maintenance_equipment_category> domains = maintenance_equipment_categoryService.searchDefault(context) ;
        for(Maintenance_equipment_category maintenance_equipment_category : domains.getContent()){
            Maintenance_equipment_categoryDTO dto = new Maintenance_equipment_categoryDTO();
            dto.fromDO(maintenance_equipment_category);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
