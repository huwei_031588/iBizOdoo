package cn.ibizlab.odoo.service.odoo_maintenance.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_maintenance.dto.Maintenance_equipmentDTO;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_equipmentService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Maintenance_equipment" })
@RestController
@RequestMapping("")
public class Maintenance_equipmentResource {

    @Autowired
    private IMaintenance_equipmentService maintenance_equipmentService;

    public IMaintenance_equipmentService getMaintenance_equipmentService() {
        return this.maintenance_equipmentService;
    }

    @ApiOperation(value = "删除数据", tags = {"Maintenance_equipment" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_equipments/{maintenance_equipment_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_equipment_id") Integer maintenance_equipment_id) {
        Maintenance_equipmentDTO maintenance_equipmentdto = new Maintenance_equipmentDTO();
		Maintenance_equipment domain = new Maintenance_equipment();
		maintenance_equipmentdto.setId(maintenance_equipment_id);
		domain.setId(maintenance_equipment_id);
        Boolean rst = maintenance_equipmentService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Maintenance_equipment" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_equipments/{maintenance_equipment_id}")
    public ResponseEntity<Maintenance_equipmentDTO> get(@PathVariable("maintenance_equipment_id") Integer maintenance_equipment_id) {
        Maintenance_equipmentDTO dto = new Maintenance_equipmentDTO();
        Maintenance_equipment domain = maintenance_equipmentService.get(maintenance_equipment_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Maintenance_equipment" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_equipments/createBatch")
    public ResponseEntity<Boolean> createBatchMaintenance_equipment(@RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Maintenance_equipment" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_equipments/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Maintenance_equipment" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_equipments/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Maintenance_equipment" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_equipments")

    public ResponseEntity<Maintenance_equipmentDTO> create(@RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
        Maintenance_equipmentDTO dto = new Maintenance_equipmentDTO();
        Maintenance_equipment domain = maintenance_equipmentdto.toDO();
		maintenance_equipmentService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Maintenance_equipment" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_equipments/{maintenance_equipment_id}")

    public ResponseEntity<Maintenance_equipmentDTO> update(@PathVariable("maintenance_equipment_id") Integer maintenance_equipment_id, @RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
		Maintenance_equipment domain = maintenance_equipmentdto.toDO();
        domain.setId(maintenance_equipment_id);
		maintenance_equipmentService.update(domain);
		Maintenance_equipmentDTO dto = new Maintenance_equipmentDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Maintenance_equipment" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_maintenance/maintenance_equipments/fetchdefault")
	public ResponseEntity<Page<Maintenance_equipmentDTO>> fetchDefault(Maintenance_equipmentSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Maintenance_equipmentDTO> list = new ArrayList<Maintenance_equipmentDTO>();
        
        Page<Maintenance_equipment> domains = maintenance_equipmentService.searchDefault(context) ;
        for(Maintenance_equipment maintenance_equipment : domains.getContent()){
            Maintenance_equipmentDTO dto = new Maintenance_equipmentDTO();
            dto.fromDO(maintenance_equipment);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
