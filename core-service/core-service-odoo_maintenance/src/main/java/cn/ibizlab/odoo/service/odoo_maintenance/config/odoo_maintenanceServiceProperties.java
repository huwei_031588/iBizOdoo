package cn.ibizlab.odoo.service.odoo_maintenance.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.maintenance")
@Data
public class odoo_maintenanceServiceProperties {

	private boolean enabled;

	private boolean auth;


}