package cn.ibizlab.odoo.service.odoo_maintenance.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_maintenance.dto.Maintenance_requestDTO;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_requestService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_requestSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Maintenance_request" })
@RestController
@RequestMapping("")
public class Maintenance_requestResource {

    @Autowired
    private IMaintenance_requestService maintenance_requestService;

    public IMaintenance_requestService getMaintenance_requestService() {
        return this.maintenance_requestService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Maintenance_request" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_requests/createBatch")
    public ResponseEntity<Boolean> createBatchMaintenance_request(@RequestBody List<Maintenance_requestDTO> maintenance_requestdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Maintenance_request" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_requests/{maintenance_request_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_request_id") Integer maintenance_request_id) {
        Maintenance_requestDTO maintenance_requestdto = new Maintenance_requestDTO();
		Maintenance_request domain = new Maintenance_request();
		maintenance_requestdto.setId(maintenance_request_id);
		domain.setId(maintenance_request_id);
        Boolean rst = maintenance_requestService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Maintenance_request" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_requests")

    public ResponseEntity<Maintenance_requestDTO> create(@RequestBody Maintenance_requestDTO maintenance_requestdto) {
        Maintenance_requestDTO dto = new Maintenance_requestDTO();
        Maintenance_request domain = maintenance_requestdto.toDO();
		maintenance_requestService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Maintenance_request" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_requests/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Maintenance_requestDTO> maintenance_requestdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Maintenance_request" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_requests/{maintenance_request_id}")

    public ResponseEntity<Maintenance_requestDTO> update(@PathVariable("maintenance_request_id") Integer maintenance_request_id, @RequestBody Maintenance_requestDTO maintenance_requestdto) {
		Maintenance_request domain = maintenance_requestdto.toDO();
        domain.setId(maintenance_request_id);
		maintenance_requestService.update(domain);
		Maintenance_requestDTO dto = new Maintenance_requestDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Maintenance_request" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_requests/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_requestDTO> maintenance_requestdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Maintenance_request" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_requests/{maintenance_request_id}")
    public ResponseEntity<Maintenance_requestDTO> get(@PathVariable("maintenance_request_id") Integer maintenance_request_id) {
        Maintenance_requestDTO dto = new Maintenance_requestDTO();
        Maintenance_request domain = maintenance_requestService.get(maintenance_request_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Maintenance_request" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_maintenance/maintenance_requests/fetchdefault")
	public ResponseEntity<Page<Maintenance_requestDTO>> fetchDefault(Maintenance_requestSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Maintenance_requestDTO> list = new ArrayList<Maintenance_requestDTO>();
        
        Page<Maintenance_request> domains = maintenance_requestService.searchDefault(context) ;
        for(Maintenance_request maintenance_request : domains.getContent()){
            Maintenance_requestDTO dto = new Maintenance_requestDTO();
            dto.fromDO(maintenance_request);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
