package cn.ibizlab.odoo.service.odoo_maintenance.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_maintenance.valuerule.anno.maintenance_request.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_request;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Maintenance_requestDTO]
 */
public class Maintenance_requestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Maintenance_requestMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Maintenance_requestNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Maintenance_requestActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Maintenance_requestCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Maintenance_requestWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Maintenance_requestIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Maintenance_requestActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [DURATION]
     *
     */
    @Maintenance_requestDurationDefault(info = "默认规则")
    private Double duration;

    @JsonIgnore
    private boolean durationDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Maintenance_requestMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Maintenance_requestMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Maintenance_requestDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [SCHEDULE_DATE]
     *
     */
    @Maintenance_requestSchedule_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp schedule_date;

    @JsonIgnore
    private boolean schedule_dateDirtyFlag;

    /**
     * 属性 [KANBAN_STATE]
     *
     */
    @Maintenance_requestKanban_stateDefault(info = "默认规则")
    private String kanban_state;

    @JsonIgnore
    private boolean kanban_stateDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Maintenance_requestMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Maintenance_requestMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [MAINTENANCE_TYPE]
     *
     */
    @Maintenance_requestMaintenance_typeDefault(info = "默认规则")
    private String maintenance_type;

    @JsonIgnore
    private boolean maintenance_typeDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Maintenance_requestWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Maintenance_requestMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Maintenance_requestMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [CLOSE_DATE]
     *
     */
    @Maintenance_requestClose_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp close_date;

    @JsonIgnore
    private boolean close_dateDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Maintenance_requestActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Maintenance_requestMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Maintenance_requestActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Maintenance_requestActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Maintenance_requestActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Maintenance_requestMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [REQUEST_DATE]
     *
     */
    @Maintenance_requestRequest_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp request_date;

    @JsonIgnore
    private boolean request_dateDirtyFlag;

    /**
     * 属性 [ARCHIVE]
     *
     */
    @Maintenance_requestArchiveDefault(info = "默认规则")
    private String archive;

    @JsonIgnore
    private boolean archiveDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Maintenance_requestMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Maintenance_requestMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Maintenance_request__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Maintenance_requestColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Maintenance_requestMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Maintenance_requestPriorityDefault(info = "默认规则")
    private String priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Maintenance_requestDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Maintenance_requestMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [OWNER_USER_ID_TEXT]
     *
     */
    @Maintenance_requestOwner_user_id_textDefault(info = "默认规则")
    private String owner_user_id_text;

    @JsonIgnore
    private boolean owner_user_id_textDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID_TEXT]
     *
     */
    @Maintenance_requestEmployee_id_textDefault(info = "默认规则")
    private String employee_id_text;

    @JsonIgnore
    private boolean employee_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Maintenance_requestWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MAINTENANCE_TEAM_ID_TEXT]
     *
     */
    @Maintenance_requestMaintenance_team_id_textDefault(info = "默认规则")
    private String maintenance_team_id_text;

    @JsonIgnore
    private boolean maintenance_team_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Maintenance_requestCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [EQUIPMENT_ID_TEXT]
     *
     */
    @Maintenance_requestEquipment_id_textDefault(info = "默认规则")
    private String equipment_id_text;

    @JsonIgnore
    private boolean equipment_id_textDirtyFlag;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @Maintenance_requestStage_id_textDefault(info = "默认规则")
    private String stage_id_text;

    @JsonIgnore
    private boolean stage_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Maintenance_requestCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @Maintenance_requestCategory_id_textDefault(info = "默认规则")
    private String category_id_text;

    @JsonIgnore
    private boolean category_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Maintenance_requestUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @Maintenance_requestDepartment_id_textDefault(info = "默认规则")
    private String department_id_text;

    @JsonIgnore
    private boolean department_id_textDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @Maintenance_requestEmployee_idDefault(info = "默认规则")
    private Integer employee_id;

    @JsonIgnore
    private boolean employee_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Maintenance_requestCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [EQUIPMENT_ID]
     *
     */
    @Maintenance_requestEquipment_idDefault(info = "默认规则")
    private Integer equipment_id;

    @JsonIgnore
    private boolean equipment_idDirtyFlag;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @Maintenance_requestStage_idDefault(info = "默认规则")
    private Integer stage_id;

    @JsonIgnore
    private boolean stage_idDirtyFlag;

    /**
     * 属性 [OWNER_USER_ID]
     *
     */
    @Maintenance_requestOwner_user_idDefault(info = "默认规则")
    private Integer owner_user_id;

    @JsonIgnore
    private boolean owner_user_idDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Maintenance_requestCategory_idDefault(info = "默认规则")
    private Integer category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @Maintenance_requestDepartment_idDefault(info = "默认规则")
    private Integer department_id;

    @JsonIgnore
    private boolean department_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Maintenance_requestUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Maintenance_requestWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [MAINTENANCE_TEAM_ID]
     *
     */
    @Maintenance_requestMaintenance_team_idDefault(info = "默认规则")
    private Integer maintenance_team_id;

    @JsonIgnore
    private boolean maintenance_team_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Maintenance_requestCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [DURATION]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return duration ;
    }

    /**
     * 设置 [DURATION]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

    /**
     * 获取 [DURATION]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return durationDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [SCHEDULE_DATE]
     */
    @JsonProperty("schedule_date")
    public Timestamp getSchedule_date(){
        return schedule_date ;
    }

    /**
     * 设置 [SCHEDULE_DATE]
     */
    @JsonProperty("schedule_date")
    public void setSchedule_date(Timestamp  schedule_date){
        this.schedule_date = schedule_date ;
        this.schedule_dateDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getSchedule_dateDirtyFlag(){
        return schedule_dateDirtyFlag ;
    }

    /**
     * 获取 [KANBAN_STATE]
     */
    @JsonProperty("kanban_state")
    public String getKanban_state(){
        return kanban_state ;
    }

    /**
     * 设置 [KANBAN_STATE]
     */
    @JsonProperty("kanban_state")
    public void setKanban_state(String  kanban_state){
        this.kanban_state = kanban_state ;
        this.kanban_stateDirtyFlag = true ;
    }

    /**
     * 获取 [KANBAN_STATE]脏标记
     */
    @JsonIgnore
    public boolean getKanban_stateDirtyFlag(){
        return kanban_stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_TYPE]
     */
    @JsonProperty("maintenance_type")
    public String getMaintenance_type(){
        return maintenance_type ;
    }

    /**
     * 设置 [MAINTENANCE_TYPE]
     */
    @JsonProperty("maintenance_type")
    public void setMaintenance_type(String  maintenance_type){
        this.maintenance_type = maintenance_type ;
        this.maintenance_typeDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_typeDirtyFlag(){
        return maintenance_typeDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [CLOSE_DATE]
     */
    @JsonProperty("close_date")
    public Timestamp getClose_date(){
        return close_date ;
    }

    /**
     * 设置 [CLOSE_DATE]
     */
    @JsonProperty("close_date")
    public void setClose_date(Timestamp  close_date){
        this.close_date = close_date ;
        this.close_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CLOSE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getClose_dateDirtyFlag(){
        return close_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [REQUEST_DATE]
     */
    @JsonProperty("request_date")
    public Timestamp getRequest_date(){
        return request_date ;
    }

    /**
     * 设置 [REQUEST_DATE]
     */
    @JsonProperty("request_date")
    public void setRequest_date(Timestamp  request_date){
        this.request_date = request_date ;
        this.request_dateDirtyFlag = true ;
    }

    /**
     * 获取 [REQUEST_DATE]脏标记
     */
    @JsonIgnore
    public boolean getRequest_dateDirtyFlag(){
        return request_dateDirtyFlag ;
    }

    /**
     * 获取 [ARCHIVE]
     */
    @JsonProperty("archive")
    public String getArchive(){
        return archive ;
    }

    /**
     * 设置 [ARCHIVE]
     */
    @JsonProperty("archive")
    public void setArchive(String  archive){
        this.archive = archive ;
        this.archiveDirtyFlag = true ;
    }

    /**
     * 获取 [ARCHIVE]脏标记
     */
    @JsonIgnore
    public boolean getArchiveDirtyFlag(){
        return archiveDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [OWNER_USER_ID_TEXT]
     */
    @JsonProperty("owner_user_id_text")
    public String getOwner_user_id_text(){
        return owner_user_id_text ;
    }

    /**
     * 设置 [OWNER_USER_ID_TEXT]
     */
    @JsonProperty("owner_user_id_text")
    public void setOwner_user_id_text(String  owner_user_id_text){
        this.owner_user_id_text = owner_user_id_text ;
        this.owner_user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOwner_user_id_textDirtyFlag(){
        return owner_user_id_textDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return employee_id_text ;
    }

    /**
     * 设置 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return employee_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_TEAM_ID_TEXT]
     */
    @JsonProperty("maintenance_team_id_text")
    public String getMaintenance_team_id_text(){
        return maintenance_team_id_text ;
    }

    /**
     * 设置 [MAINTENANCE_TEAM_ID_TEXT]
     */
    @JsonProperty("maintenance_team_id_text")
    public void setMaintenance_team_id_text(String  maintenance_team_id_text){
        this.maintenance_team_id_text = maintenance_team_id_text ;
        this.maintenance_team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_team_id_textDirtyFlag(){
        return maintenance_team_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [EQUIPMENT_ID_TEXT]
     */
    @JsonProperty("equipment_id_text")
    public String getEquipment_id_text(){
        return equipment_id_text ;
    }

    /**
     * 设置 [EQUIPMENT_ID_TEXT]
     */
    @JsonProperty("equipment_id_text")
    public void setEquipment_id_text(String  equipment_id_text){
        this.equipment_id_text = equipment_id_text ;
        this.equipment_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EQUIPMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEquipment_id_textDirtyFlag(){
        return equipment_id_textDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return stage_id_text ;
    }

    /**
     * 设置 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return stage_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return category_id_text ;
    }

    /**
     * 设置 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return category_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return department_id_text ;
    }

    /**
     * 设置 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return department_id_textDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return employee_id ;
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return employee_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [EQUIPMENT_ID]
     */
    @JsonProperty("equipment_id")
    public Integer getEquipment_id(){
        return equipment_id ;
    }

    /**
     * 设置 [EQUIPMENT_ID]
     */
    @JsonProperty("equipment_id")
    public void setEquipment_id(Integer  equipment_id){
        this.equipment_id = equipment_id ;
        this.equipment_idDirtyFlag = true ;
    }

    /**
     * 获取 [EQUIPMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getEquipment_idDirtyFlag(){
        return equipment_idDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return stage_id ;
    }

    /**
     * 设置 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return stage_idDirtyFlag ;
    }

    /**
     * 获取 [OWNER_USER_ID]
     */
    @JsonProperty("owner_user_id")
    public Integer getOwner_user_id(){
        return owner_user_id ;
    }

    /**
     * 设置 [OWNER_USER_ID]
     */
    @JsonProperty("owner_user_id")
    public void setOwner_user_id(Integer  owner_user_id){
        this.owner_user_id = owner_user_id ;
        this.owner_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOwner_user_idDirtyFlag(){
        return owner_user_idDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return department_id ;
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return department_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_TEAM_ID]
     */
    @JsonProperty("maintenance_team_id")
    public Integer getMaintenance_team_id(){
        return maintenance_team_id ;
    }

    /**
     * 设置 [MAINTENANCE_TEAM_ID]
     */
    @JsonProperty("maintenance_team_id")
    public void setMaintenance_team_id(Integer  maintenance_team_id){
        this.maintenance_team_id = maintenance_team_id ;
        this.maintenance_team_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_team_idDirtyFlag(){
        return maintenance_team_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Maintenance_request toDO() {
        Maintenance_request srfdomain = new Maintenance_request();
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getDurationDirtyFlag())
            srfdomain.setDuration(duration);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getSchedule_dateDirtyFlag())
            srfdomain.setSchedule_date(schedule_date);
        if(getKanban_stateDirtyFlag())
            srfdomain.setKanban_state(kanban_state);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getMaintenance_typeDirtyFlag())
            srfdomain.setMaintenance_type(maintenance_type);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getClose_dateDirtyFlag())
            srfdomain.setClose_date(close_date);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getRequest_dateDirtyFlag())
            srfdomain.setRequest_date(request_date);
        if(getArchiveDirtyFlag())
            srfdomain.setArchive(archive);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getOwner_user_id_textDirtyFlag())
            srfdomain.setOwner_user_id_text(owner_user_id_text);
        if(getEmployee_id_textDirtyFlag())
            srfdomain.setEmployee_id_text(employee_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getMaintenance_team_id_textDirtyFlag())
            srfdomain.setMaintenance_team_id_text(maintenance_team_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getEquipment_id_textDirtyFlag())
            srfdomain.setEquipment_id_text(equipment_id_text);
        if(getStage_id_textDirtyFlag())
            srfdomain.setStage_id_text(stage_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCategory_id_textDirtyFlag())
            srfdomain.setCategory_id_text(category_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getDepartment_id_textDirtyFlag())
            srfdomain.setDepartment_id_text(department_id_text);
        if(getEmployee_idDirtyFlag())
            srfdomain.setEmployee_id(employee_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getEquipment_idDirtyFlag())
            srfdomain.setEquipment_id(equipment_id);
        if(getStage_idDirtyFlag())
            srfdomain.setStage_id(stage_id);
        if(getOwner_user_idDirtyFlag())
            srfdomain.setOwner_user_id(owner_user_id);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getDepartment_idDirtyFlag())
            srfdomain.setDepartment_id(department_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getMaintenance_team_idDirtyFlag())
            srfdomain.setMaintenance_team_id(maintenance_team_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Maintenance_request srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getDurationDirtyFlag())
            this.setDuration(srfdomain.getDuration());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getSchedule_dateDirtyFlag())
            this.setSchedule_date(srfdomain.getSchedule_date());
        if(srfdomain.getKanban_stateDirtyFlag())
            this.setKanban_state(srfdomain.getKanban_state());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getMaintenance_typeDirtyFlag())
            this.setMaintenance_type(srfdomain.getMaintenance_type());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getClose_dateDirtyFlag())
            this.setClose_date(srfdomain.getClose_date());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getRequest_dateDirtyFlag())
            this.setRequest_date(srfdomain.getRequest_date());
        if(srfdomain.getArchiveDirtyFlag())
            this.setArchive(srfdomain.getArchive());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getOwner_user_id_textDirtyFlag())
            this.setOwner_user_id_text(srfdomain.getOwner_user_id_text());
        if(srfdomain.getEmployee_id_textDirtyFlag())
            this.setEmployee_id_text(srfdomain.getEmployee_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getMaintenance_team_id_textDirtyFlag())
            this.setMaintenance_team_id_text(srfdomain.getMaintenance_team_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getEquipment_id_textDirtyFlag())
            this.setEquipment_id_text(srfdomain.getEquipment_id_text());
        if(srfdomain.getStage_id_textDirtyFlag())
            this.setStage_id_text(srfdomain.getStage_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCategory_id_textDirtyFlag())
            this.setCategory_id_text(srfdomain.getCategory_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getDepartment_id_textDirtyFlag())
            this.setDepartment_id_text(srfdomain.getDepartment_id_text());
        if(srfdomain.getEmployee_idDirtyFlag())
            this.setEmployee_id(srfdomain.getEmployee_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getEquipment_idDirtyFlag())
            this.setEquipment_id(srfdomain.getEquipment_id());
        if(srfdomain.getStage_idDirtyFlag())
            this.setStage_id(srfdomain.getStage_id());
        if(srfdomain.getOwner_user_idDirtyFlag())
            this.setOwner_user_id(srfdomain.getOwner_user_id());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getDepartment_idDirtyFlag())
            this.setDepartment_id(srfdomain.getDepartment_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getMaintenance_team_idDirtyFlag())
            this.setMaintenance_team_id(srfdomain.getMaintenance_team_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Maintenance_requestDTO> fromDOPage(List<Maintenance_request> poPage)   {
        if(poPage == null)
            return null;
        List<Maintenance_requestDTO> dtos=new ArrayList<Maintenance_requestDTO>();
        for(Maintenance_request domain : poPage) {
            Maintenance_requestDTO dto = new Maintenance_requestDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

