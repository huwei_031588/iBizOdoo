package cn.ibizlab.odoo.service.odoo_maintenance.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_maintenance")
public class odoo_maintenanceRestConfiguration {

}
