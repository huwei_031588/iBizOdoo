package cn.ibizlab.odoo.service.odoo_maintenance.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_maintenance.valuerule.anno.maintenance_team.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Maintenance_teamDTO]
 */
public class Maintenance_teamDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TODO_REQUEST_COUNT_BLOCK]
     *
     */
    @Maintenance_teamTodo_request_count_blockDefault(info = "默认规则")
    private Integer todo_request_count_block;

    @JsonIgnore
    private boolean todo_request_count_blockDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Maintenance_teamDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [EQUIPMENT_IDS]
     *
     */
    @Maintenance_teamEquipment_idsDefault(info = "默认规则")
    private String equipment_ids;

    @JsonIgnore
    private boolean equipment_idsDirtyFlag;

    /**
     * 属性 [MEMBER_IDS]
     *
     */
    @Maintenance_teamMember_idsDefault(info = "默认规则")
    private String member_ids;

    @JsonIgnore
    private boolean member_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Maintenance_teamCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [TODO_REQUEST_COUNT_HIGH_PRIORITY]
     *
     */
    @Maintenance_teamTodo_request_count_high_priorityDefault(info = "默认规则")
    private Integer todo_request_count_high_priority;

    @JsonIgnore
    private boolean todo_request_count_high_priorityDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Maintenance_teamColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Maintenance_teamNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Maintenance_teamActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [REQUEST_IDS]
     *
     */
    @Maintenance_teamRequest_idsDefault(info = "默认规则")
    private String request_ids;

    @JsonIgnore
    private boolean request_idsDirtyFlag;

    /**
     * 属性 [TODO_REQUEST_COUNT]
     *
     */
    @Maintenance_teamTodo_request_countDefault(info = "默认规则")
    private Integer todo_request_count;

    @JsonIgnore
    private boolean todo_request_countDirtyFlag;

    /**
     * 属性 [TODO_REQUEST_IDS]
     *
     */
    @Maintenance_teamTodo_request_idsDefault(info = "默认规则")
    private String todo_request_ids;

    @JsonIgnore
    private boolean todo_request_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Maintenance_teamWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [TODO_REQUEST_COUNT_UNSCHEDULED]
     *
     */
    @Maintenance_teamTodo_request_count_unscheduledDefault(info = "默认规则")
    private Integer todo_request_count_unscheduled;

    @JsonIgnore
    private boolean todo_request_count_unscheduledDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Maintenance_teamIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [TODO_REQUEST_COUNT_DATE]
     *
     */
    @Maintenance_teamTodo_request_count_dateDefault(info = "默认规则")
    private Integer todo_request_count_date;

    @JsonIgnore
    private boolean todo_request_count_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Maintenance_team__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Maintenance_teamCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Maintenance_teamWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Maintenance_teamCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Maintenance_teamCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Maintenance_teamWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Maintenance_teamCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [TODO_REQUEST_COUNT_BLOCK]
     */
    @JsonProperty("todo_request_count_block")
    public Integer getTodo_request_count_block(){
        return todo_request_count_block ;
    }

    /**
     * 设置 [TODO_REQUEST_COUNT_BLOCK]
     */
    @JsonProperty("todo_request_count_block")
    public void setTodo_request_count_block(Integer  todo_request_count_block){
        this.todo_request_count_block = todo_request_count_block ;
        this.todo_request_count_blockDirtyFlag = true ;
    }

    /**
     * 获取 [TODO_REQUEST_COUNT_BLOCK]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_count_blockDirtyFlag(){
        return todo_request_count_blockDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [EQUIPMENT_IDS]
     */
    @JsonProperty("equipment_ids")
    public String getEquipment_ids(){
        return equipment_ids ;
    }

    /**
     * 设置 [EQUIPMENT_IDS]
     */
    @JsonProperty("equipment_ids")
    public void setEquipment_ids(String  equipment_ids){
        this.equipment_ids = equipment_ids ;
        this.equipment_idsDirtyFlag = true ;
    }

    /**
     * 获取 [EQUIPMENT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getEquipment_idsDirtyFlag(){
        return equipment_idsDirtyFlag ;
    }

    /**
     * 获取 [MEMBER_IDS]
     */
    @JsonProperty("member_ids")
    public String getMember_ids(){
        return member_ids ;
    }

    /**
     * 设置 [MEMBER_IDS]
     */
    @JsonProperty("member_ids")
    public void setMember_ids(String  member_ids){
        this.member_ids = member_ids ;
        this.member_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MEMBER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMember_idsDirtyFlag(){
        return member_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [TODO_REQUEST_COUNT_HIGH_PRIORITY]
     */
    @JsonProperty("todo_request_count_high_priority")
    public Integer getTodo_request_count_high_priority(){
        return todo_request_count_high_priority ;
    }

    /**
     * 设置 [TODO_REQUEST_COUNT_HIGH_PRIORITY]
     */
    @JsonProperty("todo_request_count_high_priority")
    public void setTodo_request_count_high_priority(Integer  todo_request_count_high_priority){
        this.todo_request_count_high_priority = todo_request_count_high_priority ;
        this.todo_request_count_high_priorityDirtyFlag = true ;
    }

    /**
     * 获取 [TODO_REQUEST_COUNT_HIGH_PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_count_high_priorityDirtyFlag(){
        return todo_request_count_high_priorityDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [REQUEST_IDS]
     */
    @JsonProperty("request_ids")
    public String getRequest_ids(){
        return request_ids ;
    }

    /**
     * 设置 [REQUEST_IDS]
     */
    @JsonProperty("request_ids")
    public void setRequest_ids(String  request_ids){
        this.request_ids = request_ids ;
        this.request_idsDirtyFlag = true ;
    }

    /**
     * 获取 [REQUEST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRequest_idsDirtyFlag(){
        return request_idsDirtyFlag ;
    }

    /**
     * 获取 [TODO_REQUEST_COUNT]
     */
    @JsonProperty("todo_request_count")
    public Integer getTodo_request_count(){
        return todo_request_count ;
    }

    /**
     * 设置 [TODO_REQUEST_COUNT]
     */
    @JsonProperty("todo_request_count")
    public void setTodo_request_count(Integer  todo_request_count){
        this.todo_request_count = todo_request_count ;
        this.todo_request_countDirtyFlag = true ;
    }

    /**
     * 获取 [TODO_REQUEST_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_countDirtyFlag(){
        return todo_request_countDirtyFlag ;
    }

    /**
     * 获取 [TODO_REQUEST_IDS]
     */
    @JsonProperty("todo_request_ids")
    public String getTodo_request_ids(){
        return todo_request_ids ;
    }

    /**
     * 设置 [TODO_REQUEST_IDS]
     */
    @JsonProperty("todo_request_ids")
    public void setTodo_request_ids(String  todo_request_ids){
        this.todo_request_ids = todo_request_ids ;
        this.todo_request_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TODO_REQUEST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_idsDirtyFlag(){
        return todo_request_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [TODO_REQUEST_COUNT_UNSCHEDULED]
     */
    @JsonProperty("todo_request_count_unscheduled")
    public Integer getTodo_request_count_unscheduled(){
        return todo_request_count_unscheduled ;
    }

    /**
     * 设置 [TODO_REQUEST_COUNT_UNSCHEDULED]
     */
    @JsonProperty("todo_request_count_unscheduled")
    public void setTodo_request_count_unscheduled(Integer  todo_request_count_unscheduled){
        this.todo_request_count_unscheduled = todo_request_count_unscheduled ;
        this.todo_request_count_unscheduledDirtyFlag = true ;
    }

    /**
     * 获取 [TODO_REQUEST_COUNT_UNSCHEDULED]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_count_unscheduledDirtyFlag(){
        return todo_request_count_unscheduledDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [TODO_REQUEST_COUNT_DATE]
     */
    @JsonProperty("todo_request_count_date")
    public Integer getTodo_request_count_date(){
        return todo_request_count_date ;
    }

    /**
     * 设置 [TODO_REQUEST_COUNT_DATE]
     */
    @JsonProperty("todo_request_count_date")
    public void setTodo_request_count_date(Integer  todo_request_count_date){
        this.todo_request_count_date = todo_request_count_date ;
        this.todo_request_count_dateDirtyFlag = true ;
    }

    /**
     * 获取 [TODO_REQUEST_COUNT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getTodo_request_count_dateDirtyFlag(){
        return todo_request_count_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Maintenance_team toDO() {
        Maintenance_team srfdomain = new Maintenance_team();
        if(getTodo_request_count_blockDirtyFlag())
            srfdomain.setTodo_request_count_block(todo_request_count_block);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getEquipment_idsDirtyFlag())
            srfdomain.setEquipment_ids(equipment_ids);
        if(getMember_idsDirtyFlag())
            srfdomain.setMember_ids(member_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getTodo_request_count_high_priorityDirtyFlag())
            srfdomain.setTodo_request_count_high_priority(todo_request_count_high_priority);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getRequest_idsDirtyFlag())
            srfdomain.setRequest_ids(request_ids);
        if(getTodo_request_countDirtyFlag())
            srfdomain.setTodo_request_count(todo_request_count);
        if(getTodo_request_idsDirtyFlag())
            srfdomain.setTodo_request_ids(todo_request_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getTodo_request_count_unscheduledDirtyFlag())
            srfdomain.setTodo_request_count_unscheduled(todo_request_count_unscheduled);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getTodo_request_count_dateDirtyFlag())
            srfdomain.setTodo_request_count_date(todo_request_count_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Maintenance_team srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getTodo_request_count_blockDirtyFlag())
            this.setTodo_request_count_block(srfdomain.getTodo_request_count_block());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getEquipment_idsDirtyFlag())
            this.setEquipment_ids(srfdomain.getEquipment_ids());
        if(srfdomain.getMember_idsDirtyFlag())
            this.setMember_ids(srfdomain.getMember_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getTodo_request_count_high_priorityDirtyFlag())
            this.setTodo_request_count_high_priority(srfdomain.getTodo_request_count_high_priority());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getRequest_idsDirtyFlag())
            this.setRequest_ids(srfdomain.getRequest_ids());
        if(srfdomain.getTodo_request_countDirtyFlag())
            this.setTodo_request_count(srfdomain.getTodo_request_count());
        if(srfdomain.getTodo_request_idsDirtyFlag())
            this.setTodo_request_ids(srfdomain.getTodo_request_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getTodo_request_count_unscheduledDirtyFlag())
            this.setTodo_request_count_unscheduled(srfdomain.getTodo_request_count_unscheduled());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getTodo_request_count_dateDirtyFlag())
            this.setTodo_request_count_date(srfdomain.getTodo_request_count_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Maintenance_teamDTO> fromDOPage(List<Maintenance_team> poPage)   {
        if(poPage == null)
            return null;
        List<Maintenance_teamDTO> dtos=new ArrayList<Maintenance_teamDTO>();
        for(Maintenance_team domain : poPage) {
            Maintenance_teamDTO dto = new Maintenance_teamDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

