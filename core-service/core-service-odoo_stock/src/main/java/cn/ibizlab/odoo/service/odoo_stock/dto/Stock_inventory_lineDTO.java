package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_inventory_line.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_inventory_lineDTO]
 */
public class Stock_inventory_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Stock_inventory_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_inventory_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [THEORETICAL_QTY]
     *
     */
    @Stock_inventory_lineTheoretical_qtyDefault(info = "默认规则")
    private Double theoretical_qty;

    @JsonIgnore
    private boolean theoretical_qtyDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_inventory_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_inventory_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_inventory_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @Stock_inventory_lineProduct_qtyDefault(info = "默认规则")
    private Double product_qty;

    @JsonIgnore
    private boolean product_qtyDirtyFlag;

    /**
     * 属性 [PACKAGE_ID_TEXT]
     *
     */
    @Stock_inventory_linePackage_id_textDefault(info = "默认规则")
    private String package_id_text;

    @JsonIgnore
    private boolean package_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_CATEGORY_ID]
     *
     */
    @Stock_inventory_lineProduct_uom_category_idDefault(info = "默认规则")
    private Integer product_uom_category_id;

    @JsonIgnore
    private boolean product_uom_category_idDirtyFlag;

    /**
     * 属性 [INVENTORY_ID_TEXT]
     *
     */
    @Stock_inventory_lineInventory_id_textDefault(info = "默认规则")
    private String inventory_id_text;

    @JsonIgnore
    private boolean inventory_id_textDirtyFlag;

    /**
     * 属性 [INVENTORY_LOCATION_ID]
     *
     */
    @Stock_inventory_lineInventory_location_idDefault(info = "默认规则")
    private Integer inventory_location_id;

    @JsonIgnore
    private boolean inventory_location_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Stock_inventory_linePartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Stock_inventory_lineStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Stock_inventory_lineLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Stock_inventory_lineProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_inventory_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_inventory_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Stock_inventory_lineCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @Stock_inventory_lineProduct_uom_id_textDefault(info = "默认规则")
    private String product_uom_id_text;

    @JsonIgnore
    private boolean product_uom_id_textDirtyFlag;

    /**
     * 属性 [PROD_LOT_ID_TEXT]
     *
     */
    @Stock_inventory_lineProd_lot_id_textDefault(info = "默认规则")
    private String prod_lot_id_text;

    @JsonIgnore
    private boolean prod_lot_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_TRACKING]
     *
     */
    @Stock_inventory_lineProduct_trackingDefault(info = "默认规则")
    private String product_tracking;

    @JsonIgnore
    private boolean product_trackingDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Stock_inventory_lineLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Stock_inventory_lineProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [PROD_LOT_ID]
     *
     */
    @Stock_inventory_lineProd_lot_idDefault(info = "默认规则")
    private Integer prod_lot_id;

    @JsonIgnore
    private boolean prod_lot_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_inventory_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Stock_inventory_lineCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [PACKAGE_ID]
     *
     */
    @Stock_inventory_linePackage_idDefault(info = "默认规则")
    private Integer package_id;

    @JsonIgnore
    private boolean package_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_inventory_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Stock_inventory_linePartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [INVENTORY_ID]
     *
     */
    @Stock_inventory_lineInventory_idDefault(info = "默认规则")
    private Integer inventory_id;

    @JsonIgnore
    private boolean inventory_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @Stock_inventory_lineProduct_uom_idDefault(info = "默认规则")
    private Integer product_uom_id;

    @JsonIgnore
    private boolean product_uom_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [THEORETICAL_QTY]
     */
    @JsonProperty("theoretical_qty")
    public Double getTheoretical_qty(){
        return theoretical_qty ;
    }

    /**
     * 设置 [THEORETICAL_QTY]
     */
    @JsonProperty("theoretical_qty")
    public void setTheoretical_qty(Double  theoretical_qty){
        this.theoretical_qty = theoretical_qty ;
        this.theoretical_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [THEORETICAL_QTY]脏标记
     */
    @JsonIgnore
    public boolean getTheoretical_qtyDirtyFlag(){
        return theoretical_qtyDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return product_qty ;
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return product_qtyDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_ID_TEXT]
     */
    @JsonProperty("package_id_text")
    public String getPackage_id_text(){
        return package_id_text ;
    }

    /**
     * 设置 [PACKAGE_ID_TEXT]
     */
    @JsonProperty("package_id_text")
    public void setPackage_id_text(String  package_id_text){
        this.package_id_text = package_id_text ;
        this.package_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPackage_id_textDirtyFlag(){
        return package_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_CATEGORY_ID]
     */
    @JsonProperty("product_uom_category_id")
    public Integer getProduct_uom_category_id(){
        return product_uom_category_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_CATEGORY_ID]
     */
    @JsonProperty("product_uom_category_id")
    public void setProduct_uom_category_id(Integer  product_uom_category_id){
        this.product_uom_category_id = product_uom_category_id ;
        this.product_uom_category_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_category_idDirtyFlag(){
        return product_uom_category_idDirtyFlag ;
    }

    /**
     * 获取 [INVENTORY_ID_TEXT]
     */
    @JsonProperty("inventory_id_text")
    public String getInventory_id_text(){
        return inventory_id_text ;
    }

    /**
     * 设置 [INVENTORY_ID_TEXT]
     */
    @JsonProperty("inventory_id_text")
    public void setInventory_id_text(String  inventory_id_text){
        this.inventory_id_text = inventory_id_text ;
        this.inventory_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INVENTORY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getInventory_id_textDirtyFlag(){
        return inventory_id_textDirtyFlag ;
    }

    /**
     * 获取 [INVENTORY_LOCATION_ID]
     */
    @JsonProperty("inventory_location_id")
    public Integer getInventory_location_id(){
        return inventory_location_id ;
    }

    /**
     * 设置 [INVENTORY_LOCATION_ID]
     */
    @JsonProperty("inventory_location_id")
    public void setInventory_location_id(Integer  inventory_location_id){
        this.inventory_location_id = inventory_location_id ;
        this.inventory_location_idDirtyFlag = true ;
    }

    /**
     * 获取 [INVENTORY_LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getInventory_location_idDirtyFlag(){
        return inventory_location_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return product_uom_id_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return product_uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROD_LOT_ID_TEXT]
     */
    @JsonProperty("prod_lot_id_text")
    public String getProd_lot_id_text(){
        return prod_lot_id_text ;
    }

    /**
     * 设置 [PROD_LOT_ID_TEXT]
     */
    @JsonProperty("prod_lot_id_text")
    public void setProd_lot_id_text(String  prod_lot_id_text){
        this.prod_lot_id_text = prod_lot_id_text ;
        this.prod_lot_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROD_LOT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProd_lot_id_textDirtyFlag(){
        return prod_lot_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TRACKING]
     */
    @JsonProperty("product_tracking")
    public String getProduct_tracking(){
        return product_tracking ;
    }

    /**
     * 设置 [PRODUCT_TRACKING]
     */
    @JsonProperty("product_tracking")
    public void setProduct_tracking(String  product_tracking){
        this.product_tracking = product_tracking ;
        this.product_trackingDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TRACKING]脏标记
     */
    @JsonIgnore
    public boolean getProduct_trackingDirtyFlag(){
        return product_trackingDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [PROD_LOT_ID]
     */
    @JsonProperty("prod_lot_id")
    public Integer getProd_lot_id(){
        return prod_lot_id ;
    }

    /**
     * 设置 [PROD_LOT_ID]
     */
    @JsonProperty("prod_lot_id")
    public void setProd_lot_id(Integer  prod_lot_id){
        this.prod_lot_id = prod_lot_id ;
        this.prod_lot_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROD_LOT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProd_lot_idDirtyFlag(){
        return prod_lot_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_ID]
     */
    @JsonProperty("package_id")
    public Integer getPackage_id(){
        return package_id ;
    }

    /**
     * 设置 [PACKAGE_ID]
     */
    @JsonProperty("package_id")
    public void setPackage_id(Integer  package_id){
        this.package_id = package_id ;
        this.package_idDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPackage_idDirtyFlag(){
        return package_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [INVENTORY_ID]
     */
    @JsonProperty("inventory_id")
    public Integer getInventory_id(){
        return inventory_id ;
    }

    /**
     * 设置 [INVENTORY_ID]
     */
    @JsonProperty("inventory_id")
    public void setInventory_id(Integer  inventory_id){
        this.inventory_id = inventory_id ;
        this.inventory_idDirtyFlag = true ;
    }

    /**
     * 获取 [INVENTORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getInventory_idDirtyFlag(){
        return inventory_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return product_uom_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return product_uom_idDirtyFlag ;
    }



    public Stock_inventory_line toDO() {
        Stock_inventory_line srfdomain = new Stock_inventory_line();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getTheoretical_qtyDirtyFlag())
            srfdomain.setTheoretical_qty(theoretical_qty);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getProduct_qtyDirtyFlag())
            srfdomain.setProduct_qty(product_qty);
        if(getPackage_id_textDirtyFlag())
            srfdomain.setPackage_id_text(package_id_text);
        if(getProduct_uom_category_idDirtyFlag())
            srfdomain.setProduct_uom_category_id(product_uom_category_id);
        if(getInventory_id_textDirtyFlag())
            srfdomain.setInventory_id_text(inventory_id_text);
        if(getInventory_location_idDirtyFlag())
            srfdomain.setInventory_location_id(inventory_location_id);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getProduct_uom_id_textDirtyFlag())
            srfdomain.setProduct_uom_id_text(product_uom_id_text);
        if(getProd_lot_id_textDirtyFlag())
            srfdomain.setProd_lot_id_text(prod_lot_id_text);
        if(getProduct_trackingDirtyFlag())
            srfdomain.setProduct_tracking(product_tracking);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getProd_lot_idDirtyFlag())
            srfdomain.setProd_lot_id(prod_lot_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getPackage_idDirtyFlag())
            srfdomain.setPackage_id(package_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getInventory_idDirtyFlag())
            srfdomain.setInventory_id(inventory_id);
        if(getProduct_uom_idDirtyFlag())
            srfdomain.setProduct_uom_id(product_uom_id);

        return srfdomain;
    }

    public void fromDO(Stock_inventory_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getTheoretical_qtyDirtyFlag())
            this.setTheoretical_qty(srfdomain.getTheoretical_qty());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getProduct_qtyDirtyFlag())
            this.setProduct_qty(srfdomain.getProduct_qty());
        if(srfdomain.getPackage_id_textDirtyFlag())
            this.setPackage_id_text(srfdomain.getPackage_id_text());
        if(srfdomain.getProduct_uom_category_idDirtyFlag())
            this.setProduct_uom_category_id(srfdomain.getProduct_uom_category_id());
        if(srfdomain.getInventory_id_textDirtyFlag())
            this.setInventory_id_text(srfdomain.getInventory_id_text());
        if(srfdomain.getInventory_location_idDirtyFlag())
            this.setInventory_location_id(srfdomain.getInventory_location_id());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getProduct_uom_id_textDirtyFlag())
            this.setProduct_uom_id_text(srfdomain.getProduct_uom_id_text());
        if(srfdomain.getProd_lot_id_textDirtyFlag())
            this.setProd_lot_id_text(srfdomain.getProd_lot_id_text());
        if(srfdomain.getProduct_trackingDirtyFlag())
            this.setProduct_tracking(srfdomain.getProduct_tracking());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getProd_lot_idDirtyFlag())
            this.setProd_lot_id(srfdomain.getProd_lot_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getPackage_idDirtyFlag())
            this.setPackage_id(srfdomain.getPackage_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getInventory_idDirtyFlag())
            this.setInventory_id(srfdomain.getInventory_id());
        if(srfdomain.getProduct_uom_idDirtyFlag())
            this.setProduct_uom_id(srfdomain.getProduct_uom_id());

    }

    public List<Stock_inventory_lineDTO> fromDOPage(List<Stock_inventory_line> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_inventory_lineDTO> dtos=new ArrayList<Stock_inventory_lineDTO>();
        for(Stock_inventory_line domain : poPage) {
            Stock_inventory_lineDTO dto = new Stock_inventory_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

