package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_ruleDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_ruleService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_ruleSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_rule" })
@RestController
@RequestMapping("")
public class Stock_ruleResource {

    @Autowired
    private IStock_ruleService stock_ruleService;

    public IStock_ruleService getStock_ruleService() {
        return this.stock_ruleService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_rule" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_rules/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_ruleDTO> stock_ruledtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_rule" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_rules/{stock_rule_id}")
    public ResponseEntity<Stock_ruleDTO> get(@PathVariable("stock_rule_id") Integer stock_rule_id) {
        Stock_ruleDTO dto = new Stock_ruleDTO();
        Stock_rule domain = stock_ruleService.get(stock_rule_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_rule" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_rules/{stock_rule_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_rule_id") Integer stock_rule_id) {
        Stock_ruleDTO stock_ruledto = new Stock_ruleDTO();
		Stock_rule domain = new Stock_rule();
		stock_ruledto.setId(stock_rule_id);
		domain.setId(stock_rule_id);
        Boolean rst = stock_ruleService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_rule" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_rules/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_ruleDTO> stock_ruledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_rule" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_rules/{stock_rule_id}")

    public ResponseEntity<Stock_ruleDTO> update(@PathVariable("stock_rule_id") Integer stock_rule_id, @RequestBody Stock_ruleDTO stock_ruledto) {
		Stock_rule domain = stock_ruledto.toDO();
        domain.setId(stock_rule_id);
		stock_ruleService.update(domain);
		Stock_ruleDTO dto = new Stock_ruleDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_rule" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_rules")

    public ResponseEntity<Stock_ruleDTO> create(@RequestBody Stock_ruleDTO stock_ruledto) {
        Stock_ruleDTO dto = new Stock_ruleDTO();
        Stock_rule domain = stock_ruledto.toDO();
		stock_ruleService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_rule" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_rules/createBatch")
    public ResponseEntity<Boolean> createBatchStock_rule(@RequestBody List<Stock_ruleDTO> stock_ruledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_rule" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_rules/fetchdefault")
	public ResponseEntity<Page<Stock_ruleDTO>> fetchDefault(Stock_ruleSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_ruleDTO> list = new ArrayList<Stock_ruleDTO>();
        
        Page<Stock_rule> domains = stock_ruleService.searchDefault(context) ;
        for(Stock_rule stock_rule : domains.getContent()){
            Stock_ruleDTO dto = new Stock_ruleDTO();
            dto.fromDO(stock_rule);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
