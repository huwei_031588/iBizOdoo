package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_move_line.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_move_lineDTO]
 */
public class Stock_move_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [QTY_DONE]
     *
     */
    @Stock_move_lineQty_doneDefault(info = "默认规则")
    private Double qty_done;

    @JsonIgnore
    private boolean qty_doneDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_USE_EXISTING_LOTS]
     *
     */
    @Stock_move_linePicking_type_use_existing_lotsDefault(info = "默认规则")
    private String picking_type_use_existing_lots;

    @JsonIgnore
    private boolean picking_type_use_existing_lotsDirtyFlag;

    /**
     * 属性 [CONSUME_LINE_IDS]
     *
     */
    @Stock_move_lineConsume_line_idsDefault(info = "默认规则")
    private String consume_line_ids;

    @JsonIgnore
    private boolean consume_line_idsDirtyFlag;

    /**
     * 属性 [LOTS_VISIBLE]
     *
     */
    @Stock_move_lineLots_visibleDefault(info = "默认规则")
    private String lots_visible;

    @JsonIgnore
    private boolean lots_visibleDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @Stock_move_lineProduct_uom_qtyDefault(info = "默认规则")
    private Double product_uom_qty;

    @JsonIgnore
    private boolean product_uom_qtyDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_move_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DONE_WO]
     *
     */
    @Stock_move_lineDone_woDefault(info = "默认规则")
    private String done_wo;

    @JsonIgnore
    private boolean done_woDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_USE_CREATE_LOTS]
     *
     */
    @Stock_move_linePicking_type_use_create_lotsDefault(info = "默认规则")
    private String picking_type_use_create_lots;

    @JsonIgnore
    private boolean picking_type_use_create_lotsDirtyFlag;

    /**
     * 属性 [PRODUCE_LINE_IDS]
     *
     */
    @Stock_move_lineProduce_line_idsDefault(info = "默认规则")
    private String produce_line_ids;

    @JsonIgnore
    private boolean produce_line_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_move_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Stock_move_lineDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ENTIRE_PACKS]
     *
     */
    @Stock_move_linePicking_type_entire_packsDefault(info = "默认规则")
    private String picking_type_entire_packs;

    @JsonIgnore
    private boolean picking_type_entire_packsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_move_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_move_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_move_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [LOT_NAME]
     *
     */
    @Stock_move_lineLot_nameDefault(info = "默认规则")
    private String lot_name;

    @JsonIgnore
    private boolean lot_nameDirtyFlag;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @Stock_move_lineProduct_qtyDefault(info = "默认规则")
    private Double product_qty;

    @JsonIgnore
    private boolean product_qtyDirtyFlag;

    /**
     * 属性 [LOT_PRODUCED_QTY]
     *
     */
    @Stock_move_lineLot_produced_qtyDefault(info = "默认规则")
    private Double lot_produced_qty;

    @JsonIgnore
    private boolean lot_produced_qtyDirtyFlag;

    /**
     * 属性 [PACKAGE_ID_TEXT]
     *
     */
    @Stock_move_linePackage_id_textDefault(info = "默认规则")
    private String package_id_text;

    @JsonIgnore
    private boolean package_id_textDirtyFlag;

    /**
     * 属性 [LOT_PRODUCED_ID_TEXT]
     *
     */
    @Stock_move_lineLot_produced_id_textDefault(info = "默认规则")
    private String lot_produced_id_text;

    @JsonIgnore
    private boolean lot_produced_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Stock_move_lineProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [WORKORDER_ID_TEXT]
     *
     */
    @Stock_move_lineWorkorder_id_textDefault(info = "默认规则")
    private String workorder_id_text;

    @JsonIgnore
    private boolean workorder_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @Stock_move_lineProduct_uom_id_textDefault(info = "默认规则")
    private String product_uom_id_text;

    @JsonIgnore
    private boolean product_uom_id_textDirtyFlag;

    /**
     * 属性 [TRACKING]
     *
     */
    @Stock_move_lineTrackingDefault(info = "默认规则")
    private String tracking;

    @JsonIgnore
    private boolean trackingDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_move_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [MOVE_ID_TEXT]
     *
     */
    @Stock_move_lineMove_id_textDefault(info = "默认规则")
    private String move_id_text;

    @JsonIgnore
    private boolean move_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @Stock_move_lineLocation_dest_id_textDefault(info = "默认规则")
    private String location_dest_id_text;

    @JsonIgnore
    private boolean location_dest_id_textDirtyFlag;

    /**
     * 属性 [IS_LOCKED]
     *
     */
    @Stock_move_lineIs_lockedDefault(info = "默认规则")
    private String is_locked;

    @JsonIgnore
    private boolean is_lockedDirtyFlag;

    /**
     * 属性 [IS_INITIAL_DEMAND_EDITABLE]
     *
     */
    @Stock_move_lineIs_initial_demand_editableDefault(info = "默认规则")
    private String is_initial_demand_editable;

    @JsonIgnore
    private boolean is_initial_demand_editableDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID_TEXT]
     *
     */
    @Stock_move_lineProduction_id_textDefault(info = "默认规则")
    private String production_id_text;

    @JsonIgnore
    private boolean production_id_textDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Stock_move_lineStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [LOT_ID_TEXT]
     *
     */
    @Stock_move_lineLot_id_textDefault(info = "默认规则")
    private String lot_id_text;

    @JsonIgnore
    private boolean lot_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_move_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [REFERENCE]
     *
     */
    @Stock_move_lineReferenceDefault(info = "默认规则")
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;

    /**
     * 属性 [RESULT_PACKAGE_ID_TEXT]
     *
     */
    @Stock_move_lineResult_package_id_textDefault(info = "默认规则")
    private String result_package_id_text;

    @JsonIgnore
    private boolean result_package_id_textDirtyFlag;

    /**
     * 属性 [DONE_MOVE]
     *
     */
    @Stock_move_lineDone_moveDefault(info = "默认规则")
    private String done_move;

    @JsonIgnore
    private boolean done_moveDirtyFlag;

    /**
     * 属性 [PICKING_ID_TEXT]
     *
     */
    @Stock_move_linePicking_id_textDefault(info = "默认规则")
    private String picking_id_text;

    @JsonIgnore
    private boolean picking_id_textDirtyFlag;

    /**
     * 属性 [OWNER_ID_TEXT]
     *
     */
    @Stock_move_lineOwner_id_textDefault(info = "默认规则")
    private String owner_id_text;

    @JsonIgnore
    private boolean owner_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Stock_move_lineLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @Stock_move_lineProduct_uom_idDefault(info = "默认规则")
    private Integer product_uom_id;

    @JsonIgnore
    private boolean product_uom_idDirtyFlag;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @Stock_move_lineMove_idDefault(info = "默认规则")
    private Integer move_id;

    @JsonIgnore
    private boolean move_idDirtyFlag;

    /**
     * 属性 [RESULT_PACKAGE_ID]
     *
     */
    @Stock_move_lineResult_package_idDefault(info = "默认规则")
    private Integer result_package_id;

    @JsonIgnore
    private boolean result_package_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_move_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [LOT_PRODUCED_ID]
     *
     */
    @Stock_move_lineLot_produced_idDefault(info = "默认规则")
    private Integer lot_produced_id;

    @JsonIgnore
    private boolean lot_produced_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Stock_move_lineProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_move_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [LOT_ID]
     *
     */
    @Stock_move_lineLot_idDefault(info = "默认规则")
    private Integer lot_id;

    @JsonIgnore
    private boolean lot_idDirtyFlag;

    /**
     * 属性 [PICKING_ID]
     *
     */
    @Stock_move_linePicking_idDefault(info = "默认规则")
    private Integer picking_id;

    @JsonIgnore
    private boolean picking_idDirtyFlag;

    /**
     * 属性 [WORKORDER_ID]
     *
     */
    @Stock_move_lineWorkorder_idDefault(info = "默认规则")
    private Integer workorder_id;

    @JsonIgnore
    private boolean workorder_idDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Stock_move_lineLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [PACKAGE_ID]
     *
     */
    @Stock_move_linePackage_idDefault(info = "默认规则")
    private Integer package_id;

    @JsonIgnore
    private boolean package_idDirtyFlag;

    /**
     * 属性 [OWNER_ID]
     *
     */
    @Stock_move_lineOwner_idDefault(info = "默认规则")
    private Integer owner_id;

    @JsonIgnore
    private boolean owner_idDirtyFlag;

    /**
     * 属性 [PACKAGE_LEVEL_ID]
     *
     */
    @Stock_move_linePackage_level_idDefault(info = "默认规则")
    private Integer package_level_id;

    @JsonIgnore
    private boolean package_level_idDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @Stock_move_lineLocation_dest_idDefault(info = "默认规则")
    private Integer location_dest_id;

    @JsonIgnore
    private boolean location_dest_idDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @Stock_move_lineProduction_idDefault(info = "默认规则")
    private Integer production_id;

    @JsonIgnore
    private boolean production_idDirtyFlag;


    /**
     * 获取 [QTY_DONE]
     */
    @JsonProperty("qty_done")
    public Double getQty_done(){
        return qty_done ;
    }

    /**
     * 设置 [QTY_DONE]
     */
    @JsonProperty("qty_done")
    public void setQty_done(Double  qty_done){
        this.qty_done = qty_done ;
        this.qty_doneDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_DONE]脏标记
     */
    @JsonIgnore
    public boolean getQty_doneDirtyFlag(){
        return qty_doneDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_USE_EXISTING_LOTS]
     */
    @JsonProperty("picking_type_use_existing_lots")
    public String getPicking_type_use_existing_lots(){
        return picking_type_use_existing_lots ;
    }

    /**
     * 设置 [PICKING_TYPE_USE_EXISTING_LOTS]
     */
    @JsonProperty("picking_type_use_existing_lots")
    public void setPicking_type_use_existing_lots(String  picking_type_use_existing_lots){
        this.picking_type_use_existing_lots = picking_type_use_existing_lots ;
        this.picking_type_use_existing_lotsDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_USE_EXISTING_LOTS]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_use_existing_lotsDirtyFlag(){
        return picking_type_use_existing_lotsDirtyFlag ;
    }

    /**
     * 获取 [CONSUME_LINE_IDS]
     */
    @JsonProperty("consume_line_ids")
    public String getConsume_line_ids(){
        return consume_line_ids ;
    }

    /**
     * 设置 [CONSUME_LINE_IDS]
     */
    @JsonProperty("consume_line_ids")
    public void setConsume_line_ids(String  consume_line_ids){
        this.consume_line_ids = consume_line_ids ;
        this.consume_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CONSUME_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getConsume_line_idsDirtyFlag(){
        return consume_line_idsDirtyFlag ;
    }

    /**
     * 获取 [LOTS_VISIBLE]
     */
    @JsonProperty("lots_visible")
    public String getLots_visible(){
        return lots_visible ;
    }

    /**
     * 设置 [LOTS_VISIBLE]
     */
    @JsonProperty("lots_visible")
    public void setLots_visible(String  lots_visible){
        this.lots_visible = lots_visible ;
        this.lots_visibleDirtyFlag = true ;
    }

    /**
     * 获取 [LOTS_VISIBLE]脏标记
     */
    @JsonIgnore
    public boolean getLots_visibleDirtyFlag(){
        return lots_visibleDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return product_uom_qty ;
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return product_uom_qtyDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DONE_WO]
     */
    @JsonProperty("done_wo")
    public String getDone_wo(){
        return done_wo ;
    }

    /**
     * 设置 [DONE_WO]
     */
    @JsonProperty("done_wo")
    public void setDone_wo(String  done_wo){
        this.done_wo = done_wo ;
        this.done_woDirtyFlag = true ;
    }

    /**
     * 获取 [DONE_WO]脏标记
     */
    @JsonIgnore
    public boolean getDone_woDirtyFlag(){
        return done_woDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_USE_CREATE_LOTS]
     */
    @JsonProperty("picking_type_use_create_lots")
    public String getPicking_type_use_create_lots(){
        return picking_type_use_create_lots ;
    }

    /**
     * 设置 [PICKING_TYPE_USE_CREATE_LOTS]
     */
    @JsonProperty("picking_type_use_create_lots")
    public void setPicking_type_use_create_lots(String  picking_type_use_create_lots){
        this.picking_type_use_create_lots = picking_type_use_create_lots ;
        this.picking_type_use_create_lotsDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_USE_CREATE_LOTS]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_use_create_lotsDirtyFlag(){
        return picking_type_use_create_lotsDirtyFlag ;
    }

    /**
     * 获取 [PRODUCE_LINE_IDS]
     */
    @JsonProperty("produce_line_ids")
    public String getProduce_line_ids(){
        return produce_line_ids ;
    }

    /**
     * 设置 [PRODUCE_LINE_IDS]
     */
    @JsonProperty("produce_line_ids")
    public void setProduce_line_ids(String  produce_line_ids){
        this.produce_line_ids = produce_line_ids ;
        this.produce_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getProduce_line_idsDirtyFlag(){
        return produce_line_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ENTIRE_PACKS]
     */
    @JsonProperty("picking_type_entire_packs")
    public String getPicking_type_entire_packs(){
        return picking_type_entire_packs ;
    }

    /**
     * 设置 [PICKING_TYPE_ENTIRE_PACKS]
     */
    @JsonProperty("picking_type_entire_packs")
    public void setPicking_type_entire_packs(String  picking_type_entire_packs){
        this.picking_type_entire_packs = picking_type_entire_packs ;
        this.picking_type_entire_packsDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ENTIRE_PACKS]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_entire_packsDirtyFlag(){
        return picking_type_entire_packsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [LOT_NAME]
     */
    @JsonProperty("lot_name")
    public String getLot_name(){
        return lot_name ;
    }

    /**
     * 设置 [LOT_NAME]
     */
    @JsonProperty("lot_name")
    public void setLot_name(String  lot_name){
        this.lot_name = lot_name ;
        this.lot_nameDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_NAME]脏标记
     */
    @JsonIgnore
    public boolean getLot_nameDirtyFlag(){
        return lot_nameDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return product_qty ;
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return product_qtyDirtyFlag ;
    }

    /**
     * 获取 [LOT_PRODUCED_QTY]
     */
    @JsonProperty("lot_produced_qty")
    public Double getLot_produced_qty(){
        return lot_produced_qty ;
    }

    /**
     * 设置 [LOT_PRODUCED_QTY]
     */
    @JsonProperty("lot_produced_qty")
    public void setLot_produced_qty(Double  lot_produced_qty){
        this.lot_produced_qty = lot_produced_qty ;
        this.lot_produced_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_PRODUCED_QTY]脏标记
     */
    @JsonIgnore
    public boolean getLot_produced_qtyDirtyFlag(){
        return lot_produced_qtyDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_ID_TEXT]
     */
    @JsonProperty("package_id_text")
    public String getPackage_id_text(){
        return package_id_text ;
    }

    /**
     * 设置 [PACKAGE_ID_TEXT]
     */
    @JsonProperty("package_id_text")
    public void setPackage_id_text(String  package_id_text){
        this.package_id_text = package_id_text ;
        this.package_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPackage_id_textDirtyFlag(){
        return package_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOT_PRODUCED_ID_TEXT]
     */
    @JsonProperty("lot_produced_id_text")
    public String getLot_produced_id_text(){
        return lot_produced_id_text ;
    }

    /**
     * 设置 [LOT_PRODUCED_ID_TEXT]
     */
    @JsonProperty("lot_produced_id_text")
    public void setLot_produced_id_text(String  lot_produced_id_text){
        this.lot_produced_id_text = lot_produced_id_text ;
        this.lot_produced_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_PRODUCED_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLot_produced_id_textDirtyFlag(){
        return lot_produced_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_ID_TEXT]
     */
    @JsonProperty("workorder_id_text")
    public String getWorkorder_id_text(){
        return workorder_id_text ;
    }

    /**
     * 设置 [WORKORDER_ID_TEXT]
     */
    @JsonProperty("workorder_id_text")
    public void setWorkorder_id_text(String  workorder_id_text){
        this.workorder_id_text = workorder_id_text ;
        this.workorder_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_id_textDirtyFlag(){
        return workorder_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return product_uom_id_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return product_uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [TRACKING]
     */
    @JsonProperty("tracking")
    public String getTracking(){
        return tracking ;
    }

    /**
     * 设置 [TRACKING]
     */
    @JsonProperty("tracking")
    public void setTracking(String  tracking){
        this.tracking = tracking ;
        this.trackingDirtyFlag = true ;
    }

    /**
     * 获取 [TRACKING]脏标记
     */
    @JsonIgnore
    public boolean getTrackingDirtyFlag(){
        return trackingDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID_TEXT]
     */
    @JsonProperty("move_id_text")
    public String getMove_id_text(){
        return move_id_text ;
    }

    /**
     * 设置 [MOVE_ID_TEXT]
     */
    @JsonProperty("move_id_text")
    public void setMove_id_text(String  move_id_text){
        this.move_id_text = move_id_text ;
        this.move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMove_id_textDirtyFlag(){
        return move_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return location_dest_id_text ;
    }

    /**
     * 设置 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return location_dest_id_textDirtyFlag ;
    }

    /**
     * 获取 [IS_LOCKED]
     */
    @JsonProperty("is_locked")
    public String getIs_locked(){
        return is_locked ;
    }

    /**
     * 设置 [IS_LOCKED]
     */
    @JsonProperty("is_locked")
    public void setIs_locked(String  is_locked){
        this.is_locked = is_locked ;
        this.is_lockedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_LOCKED]脏标记
     */
    @JsonIgnore
    public boolean getIs_lockedDirtyFlag(){
        return is_lockedDirtyFlag ;
    }

    /**
     * 获取 [IS_INITIAL_DEMAND_EDITABLE]
     */
    @JsonProperty("is_initial_demand_editable")
    public String getIs_initial_demand_editable(){
        return is_initial_demand_editable ;
    }

    /**
     * 设置 [IS_INITIAL_DEMAND_EDITABLE]
     */
    @JsonProperty("is_initial_demand_editable")
    public void setIs_initial_demand_editable(String  is_initial_demand_editable){
        this.is_initial_demand_editable = is_initial_demand_editable ;
        this.is_initial_demand_editableDirtyFlag = true ;
    }

    /**
     * 获取 [IS_INITIAL_DEMAND_EDITABLE]脏标记
     */
    @JsonIgnore
    public boolean getIs_initial_demand_editableDirtyFlag(){
        return is_initial_demand_editableDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID_TEXT]
     */
    @JsonProperty("production_id_text")
    public String getProduction_id_text(){
        return production_id_text ;
    }

    /**
     * 设置 [PRODUCTION_ID_TEXT]
     */
    @JsonProperty("production_id_text")
    public void setProduction_id_text(String  production_id_text){
        this.production_id_text = production_id_text ;
        this.production_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduction_id_textDirtyFlag(){
        return production_id_textDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [LOT_ID_TEXT]
     */
    @JsonProperty("lot_id_text")
    public String getLot_id_text(){
        return lot_id_text ;
    }

    /**
     * 设置 [LOT_ID_TEXT]
     */
    @JsonProperty("lot_id_text")
    public void setLot_id_text(String  lot_id_text){
        this.lot_id_text = lot_id_text ;
        this.lot_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLot_id_textDirtyFlag(){
        return lot_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [REFERENCE]
     */
    @JsonProperty("reference")
    public String getReference(){
        return reference ;
    }

    /**
     * 设置 [REFERENCE]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return referenceDirtyFlag ;
    }

    /**
     * 获取 [RESULT_PACKAGE_ID_TEXT]
     */
    @JsonProperty("result_package_id_text")
    public String getResult_package_id_text(){
        return result_package_id_text ;
    }

    /**
     * 设置 [RESULT_PACKAGE_ID_TEXT]
     */
    @JsonProperty("result_package_id_text")
    public void setResult_package_id_text(String  result_package_id_text){
        this.result_package_id_text = result_package_id_text ;
        this.result_package_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RESULT_PACKAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getResult_package_id_textDirtyFlag(){
        return result_package_id_textDirtyFlag ;
    }

    /**
     * 获取 [DONE_MOVE]
     */
    @JsonProperty("done_move")
    public String getDone_move(){
        return done_move ;
    }

    /**
     * 设置 [DONE_MOVE]
     */
    @JsonProperty("done_move")
    public void setDone_move(String  done_move){
        this.done_move = done_move ;
        this.done_moveDirtyFlag = true ;
    }

    /**
     * 获取 [DONE_MOVE]脏标记
     */
    @JsonIgnore
    public boolean getDone_moveDirtyFlag(){
        return done_moveDirtyFlag ;
    }

    /**
     * 获取 [PICKING_ID_TEXT]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return picking_id_text ;
    }

    /**
     * 设置 [PICKING_ID_TEXT]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return picking_id_textDirtyFlag ;
    }

    /**
     * 获取 [OWNER_ID_TEXT]
     */
    @JsonProperty("owner_id_text")
    public String getOwner_id_text(){
        return owner_id_text ;
    }

    /**
     * 设置 [OWNER_ID_TEXT]
     */
    @JsonProperty("owner_id_text")
    public void setOwner_id_text(String  owner_id_text){
        this.owner_id_text = owner_id_text ;
        this.owner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOwner_id_textDirtyFlag(){
        return owner_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return product_uom_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return product_uom_idDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return move_id ;
    }

    /**
     * 设置 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return move_idDirtyFlag ;
    }

    /**
     * 获取 [RESULT_PACKAGE_ID]
     */
    @JsonProperty("result_package_id")
    public Integer getResult_package_id(){
        return result_package_id ;
    }

    /**
     * 设置 [RESULT_PACKAGE_ID]
     */
    @JsonProperty("result_package_id")
    public void setResult_package_id(Integer  result_package_id){
        this.result_package_id = result_package_id ;
        this.result_package_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESULT_PACKAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getResult_package_idDirtyFlag(){
        return result_package_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [LOT_PRODUCED_ID]
     */
    @JsonProperty("lot_produced_id")
    public Integer getLot_produced_id(){
        return lot_produced_id ;
    }

    /**
     * 设置 [LOT_PRODUCED_ID]
     */
    @JsonProperty("lot_produced_id")
    public void setLot_produced_id(Integer  lot_produced_id){
        this.lot_produced_id = lot_produced_id ;
        this.lot_produced_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_PRODUCED_ID]脏标记
     */
    @JsonIgnore
    public boolean getLot_produced_idDirtyFlag(){
        return lot_produced_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [LOT_ID]
     */
    @JsonProperty("lot_id")
    public Integer getLot_id(){
        return lot_id ;
    }

    /**
     * 设置 [LOT_ID]
     */
    @JsonProperty("lot_id")
    public void setLot_id(Integer  lot_id){
        this.lot_id = lot_id ;
        this.lot_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_ID]脏标记
     */
    @JsonIgnore
    public boolean getLot_idDirtyFlag(){
        return lot_idDirtyFlag ;
    }

    /**
     * 获取 [PICKING_ID]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return picking_id ;
    }

    /**
     * 设置 [PICKING_ID]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return picking_idDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_ID]
     */
    @JsonProperty("workorder_id")
    public Integer getWorkorder_id(){
        return workorder_id ;
    }

    /**
     * 设置 [WORKORDER_ID]
     */
    @JsonProperty("workorder_id")
    public void setWorkorder_id(Integer  workorder_id){
        this.workorder_id = workorder_id ;
        this.workorder_idDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idDirtyFlag(){
        return workorder_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_ID]
     */
    @JsonProperty("package_id")
    public Integer getPackage_id(){
        return package_id ;
    }

    /**
     * 设置 [PACKAGE_ID]
     */
    @JsonProperty("package_id")
    public void setPackage_id(Integer  package_id){
        this.package_id = package_id ;
        this.package_idDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPackage_idDirtyFlag(){
        return package_idDirtyFlag ;
    }

    /**
     * 获取 [OWNER_ID]
     */
    @JsonProperty("owner_id")
    public Integer getOwner_id(){
        return owner_id ;
    }

    /**
     * 设置 [OWNER_ID]
     */
    @JsonProperty("owner_id")
    public void setOwner_id(Integer  owner_id){
        this.owner_id = owner_id ;
        this.owner_idDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOwner_idDirtyFlag(){
        return owner_idDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_LEVEL_ID]
     */
    @JsonProperty("package_level_id")
    public Integer getPackage_level_id(){
        return package_level_id ;
    }

    /**
     * 设置 [PACKAGE_LEVEL_ID]
     */
    @JsonProperty("package_level_id")
    public void setPackage_level_id(Integer  package_level_id){
        this.package_level_id = package_level_id ;
        this.package_level_idDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_LEVEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_idDirtyFlag(){
        return package_level_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return location_dest_id ;
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return location_dest_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public Integer getProduction_id(){
        return production_id ;
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public void setProduction_id(Integer  production_id){
        this.production_id = production_id ;
        this.production_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduction_idDirtyFlag(){
        return production_idDirtyFlag ;
    }



    public Stock_move_line toDO() {
        Stock_move_line srfdomain = new Stock_move_line();
        if(getQty_doneDirtyFlag())
            srfdomain.setQty_done(qty_done);
        if(getPicking_type_use_existing_lotsDirtyFlag())
            srfdomain.setPicking_type_use_existing_lots(picking_type_use_existing_lots);
        if(getConsume_line_idsDirtyFlag())
            srfdomain.setConsume_line_ids(consume_line_ids);
        if(getLots_visibleDirtyFlag())
            srfdomain.setLots_visible(lots_visible);
        if(getProduct_uom_qtyDirtyFlag())
            srfdomain.setProduct_uom_qty(product_uom_qty);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDone_woDirtyFlag())
            srfdomain.setDone_wo(done_wo);
        if(getPicking_type_use_create_lotsDirtyFlag())
            srfdomain.setPicking_type_use_create_lots(picking_type_use_create_lots);
        if(getProduce_line_idsDirtyFlag())
            srfdomain.setProduce_line_ids(produce_line_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getPicking_type_entire_packsDirtyFlag())
            srfdomain.setPicking_type_entire_packs(picking_type_entire_packs);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getLot_nameDirtyFlag())
            srfdomain.setLot_name(lot_name);
        if(getProduct_qtyDirtyFlag())
            srfdomain.setProduct_qty(product_qty);
        if(getLot_produced_qtyDirtyFlag())
            srfdomain.setLot_produced_qty(lot_produced_qty);
        if(getPackage_id_textDirtyFlag())
            srfdomain.setPackage_id_text(package_id_text);
        if(getLot_produced_id_textDirtyFlag())
            srfdomain.setLot_produced_id_text(lot_produced_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getWorkorder_id_textDirtyFlag())
            srfdomain.setWorkorder_id_text(workorder_id_text);
        if(getProduct_uom_id_textDirtyFlag())
            srfdomain.setProduct_uom_id_text(product_uom_id_text);
        if(getTrackingDirtyFlag())
            srfdomain.setTracking(tracking);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getMove_id_textDirtyFlag())
            srfdomain.setMove_id_text(move_id_text);
        if(getLocation_dest_id_textDirtyFlag())
            srfdomain.setLocation_dest_id_text(location_dest_id_text);
        if(getIs_lockedDirtyFlag())
            srfdomain.setIs_locked(is_locked);
        if(getIs_initial_demand_editableDirtyFlag())
            srfdomain.setIs_initial_demand_editable(is_initial_demand_editable);
        if(getProduction_id_textDirtyFlag())
            srfdomain.setProduction_id_text(production_id_text);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getLot_id_textDirtyFlag())
            srfdomain.setLot_id_text(lot_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getReferenceDirtyFlag())
            srfdomain.setReference(reference);
        if(getResult_package_id_textDirtyFlag())
            srfdomain.setResult_package_id_text(result_package_id_text);
        if(getDone_moveDirtyFlag())
            srfdomain.setDone_move(done_move);
        if(getPicking_id_textDirtyFlag())
            srfdomain.setPicking_id_text(picking_id_text);
        if(getOwner_id_textDirtyFlag())
            srfdomain.setOwner_id_text(owner_id_text);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getProduct_uom_idDirtyFlag())
            srfdomain.setProduct_uom_id(product_uom_id);
        if(getMove_idDirtyFlag())
            srfdomain.setMove_id(move_id);
        if(getResult_package_idDirtyFlag())
            srfdomain.setResult_package_id(result_package_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getLot_produced_idDirtyFlag())
            srfdomain.setLot_produced_id(lot_produced_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getLot_idDirtyFlag())
            srfdomain.setLot_id(lot_id);
        if(getPicking_idDirtyFlag())
            srfdomain.setPicking_id(picking_id);
        if(getWorkorder_idDirtyFlag())
            srfdomain.setWorkorder_id(workorder_id);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getPackage_idDirtyFlag())
            srfdomain.setPackage_id(package_id);
        if(getOwner_idDirtyFlag())
            srfdomain.setOwner_id(owner_id);
        if(getPackage_level_idDirtyFlag())
            srfdomain.setPackage_level_id(package_level_id);
        if(getLocation_dest_idDirtyFlag())
            srfdomain.setLocation_dest_id(location_dest_id);
        if(getProduction_idDirtyFlag())
            srfdomain.setProduction_id(production_id);

        return srfdomain;
    }

    public void fromDO(Stock_move_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getQty_doneDirtyFlag())
            this.setQty_done(srfdomain.getQty_done());
        if(srfdomain.getPicking_type_use_existing_lotsDirtyFlag())
            this.setPicking_type_use_existing_lots(srfdomain.getPicking_type_use_existing_lots());
        if(srfdomain.getConsume_line_idsDirtyFlag())
            this.setConsume_line_ids(srfdomain.getConsume_line_ids());
        if(srfdomain.getLots_visibleDirtyFlag())
            this.setLots_visible(srfdomain.getLots_visible());
        if(srfdomain.getProduct_uom_qtyDirtyFlag())
            this.setProduct_uom_qty(srfdomain.getProduct_uom_qty());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDone_woDirtyFlag())
            this.setDone_wo(srfdomain.getDone_wo());
        if(srfdomain.getPicking_type_use_create_lotsDirtyFlag())
            this.setPicking_type_use_create_lots(srfdomain.getPicking_type_use_create_lots());
        if(srfdomain.getProduce_line_idsDirtyFlag())
            this.setProduce_line_ids(srfdomain.getProduce_line_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getPicking_type_entire_packsDirtyFlag())
            this.setPicking_type_entire_packs(srfdomain.getPicking_type_entire_packs());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getLot_nameDirtyFlag())
            this.setLot_name(srfdomain.getLot_name());
        if(srfdomain.getProduct_qtyDirtyFlag())
            this.setProduct_qty(srfdomain.getProduct_qty());
        if(srfdomain.getLot_produced_qtyDirtyFlag())
            this.setLot_produced_qty(srfdomain.getLot_produced_qty());
        if(srfdomain.getPackage_id_textDirtyFlag())
            this.setPackage_id_text(srfdomain.getPackage_id_text());
        if(srfdomain.getLot_produced_id_textDirtyFlag())
            this.setLot_produced_id_text(srfdomain.getLot_produced_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getWorkorder_id_textDirtyFlag())
            this.setWorkorder_id_text(srfdomain.getWorkorder_id_text());
        if(srfdomain.getProduct_uom_id_textDirtyFlag())
            this.setProduct_uom_id_text(srfdomain.getProduct_uom_id_text());
        if(srfdomain.getTrackingDirtyFlag())
            this.setTracking(srfdomain.getTracking());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getMove_id_textDirtyFlag())
            this.setMove_id_text(srfdomain.getMove_id_text());
        if(srfdomain.getLocation_dest_id_textDirtyFlag())
            this.setLocation_dest_id_text(srfdomain.getLocation_dest_id_text());
        if(srfdomain.getIs_lockedDirtyFlag())
            this.setIs_locked(srfdomain.getIs_locked());
        if(srfdomain.getIs_initial_demand_editableDirtyFlag())
            this.setIs_initial_demand_editable(srfdomain.getIs_initial_demand_editable());
        if(srfdomain.getProduction_id_textDirtyFlag())
            this.setProduction_id_text(srfdomain.getProduction_id_text());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getLot_id_textDirtyFlag())
            this.setLot_id_text(srfdomain.getLot_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getReferenceDirtyFlag())
            this.setReference(srfdomain.getReference());
        if(srfdomain.getResult_package_id_textDirtyFlag())
            this.setResult_package_id_text(srfdomain.getResult_package_id_text());
        if(srfdomain.getDone_moveDirtyFlag())
            this.setDone_move(srfdomain.getDone_move());
        if(srfdomain.getPicking_id_textDirtyFlag())
            this.setPicking_id_text(srfdomain.getPicking_id_text());
        if(srfdomain.getOwner_id_textDirtyFlag())
            this.setOwner_id_text(srfdomain.getOwner_id_text());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getProduct_uom_idDirtyFlag())
            this.setProduct_uom_id(srfdomain.getProduct_uom_id());
        if(srfdomain.getMove_idDirtyFlag())
            this.setMove_id(srfdomain.getMove_id());
        if(srfdomain.getResult_package_idDirtyFlag())
            this.setResult_package_id(srfdomain.getResult_package_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getLot_produced_idDirtyFlag())
            this.setLot_produced_id(srfdomain.getLot_produced_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getLot_idDirtyFlag())
            this.setLot_id(srfdomain.getLot_id());
        if(srfdomain.getPicking_idDirtyFlag())
            this.setPicking_id(srfdomain.getPicking_id());
        if(srfdomain.getWorkorder_idDirtyFlag())
            this.setWorkorder_id(srfdomain.getWorkorder_id());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getPackage_idDirtyFlag())
            this.setPackage_id(srfdomain.getPackage_id());
        if(srfdomain.getOwner_idDirtyFlag())
            this.setOwner_id(srfdomain.getOwner_id());
        if(srfdomain.getPackage_level_idDirtyFlag())
            this.setPackage_level_id(srfdomain.getPackage_level_id());
        if(srfdomain.getLocation_dest_idDirtyFlag())
            this.setLocation_dest_id(srfdomain.getLocation_dest_id());
        if(srfdomain.getProduction_idDirtyFlag())
            this.setProduction_id(srfdomain.getProduction_id());

    }

    public List<Stock_move_lineDTO> fromDOPage(List<Stock_move_line> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_move_lineDTO> dtos=new ArrayList<Stock_move_lineDTO>();
        for(Stock_move_line domain : poPage) {
            Stock_move_lineDTO dto = new Stock_move_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

