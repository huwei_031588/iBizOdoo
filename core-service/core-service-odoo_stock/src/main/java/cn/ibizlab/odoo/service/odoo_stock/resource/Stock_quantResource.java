package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_quantDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_quantService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_quant" })
@RestController
@RequestMapping("")
public class Stock_quantResource {

    @Autowired
    private IStock_quantService stock_quantService;

    public IStock_quantService getStock_quantService() {
        return this.stock_quantService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_quant" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quants/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_quantDTO> stock_quantdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_quant" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quants/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_quantDTO> stock_quantdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_quant" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quants")

    public ResponseEntity<Stock_quantDTO> create(@RequestBody Stock_quantDTO stock_quantdto) {
        Stock_quantDTO dto = new Stock_quantDTO();
        Stock_quant domain = stock_quantdto.toDO();
		stock_quantService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_quant" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quants/{stock_quant_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_quant_id") Integer stock_quant_id) {
        Stock_quantDTO stock_quantdto = new Stock_quantDTO();
		Stock_quant domain = new Stock_quant();
		stock_quantdto.setId(stock_quant_id);
		domain.setId(stock_quant_id);
        Boolean rst = stock_quantService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_quant" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quants/{stock_quant_id}")

    public ResponseEntity<Stock_quantDTO> update(@PathVariable("stock_quant_id") Integer stock_quant_id, @RequestBody Stock_quantDTO stock_quantdto) {
		Stock_quant domain = stock_quantdto.toDO();
        domain.setId(stock_quant_id);
		stock_quantService.update(domain);
		Stock_quantDTO dto = new Stock_quantDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_quant" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quants/createBatch")
    public ResponseEntity<Boolean> createBatchStock_quant(@RequestBody List<Stock_quantDTO> stock_quantdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_quant" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quants/{stock_quant_id}")
    public ResponseEntity<Stock_quantDTO> get(@PathVariable("stock_quant_id") Integer stock_quant_id) {
        Stock_quantDTO dto = new Stock_quantDTO();
        Stock_quant domain = stock_quantService.get(stock_quant_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_quant" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_quants/fetchdefault")
	public ResponseEntity<Page<Stock_quantDTO>> fetchDefault(Stock_quantSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_quantDTO> list = new ArrayList<Stock_quantDTO>();
        
        Page<Stock_quant> domains = stock_quantService.searchDefault(context) ;
        for(Stock_quant stock_quant : domains.getContent()){
            Stock_quantDTO dto = new Stock_quantDTO();
            dto.fromDO(stock_quant);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
