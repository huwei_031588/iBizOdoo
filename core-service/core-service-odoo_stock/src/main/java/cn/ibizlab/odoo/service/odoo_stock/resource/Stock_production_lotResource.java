package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_production_lotDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_production_lotService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_production_lotSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_production_lot" })
@RestController
@RequestMapping("")
public class Stock_production_lotResource {

    @Autowired
    private IStock_production_lotService stock_production_lotService;

    public IStock_production_lotService getStock_production_lotService() {
        return this.stock_production_lotService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_production_lot" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_production_lots/createBatch")
    public ResponseEntity<Boolean> createBatchStock_production_lot(@RequestBody List<Stock_production_lotDTO> stock_production_lotdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_production_lot" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_production_lots/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_production_lotDTO> stock_production_lotdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_production_lot" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_production_lots/{stock_production_lot_id}")

    public ResponseEntity<Stock_production_lotDTO> update(@PathVariable("stock_production_lot_id") Integer stock_production_lot_id, @RequestBody Stock_production_lotDTO stock_production_lotdto) {
		Stock_production_lot domain = stock_production_lotdto.toDO();
        domain.setId(stock_production_lot_id);
		stock_production_lotService.update(domain);
		Stock_production_lotDTO dto = new Stock_production_lotDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_production_lot" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_production_lots/{stock_production_lot_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_production_lot_id") Integer stock_production_lot_id) {
        Stock_production_lotDTO stock_production_lotdto = new Stock_production_lotDTO();
		Stock_production_lot domain = new Stock_production_lot();
		stock_production_lotdto.setId(stock_production_lot_id);
		domain.setId(stock_production_lot_id);
        Boolean rst = stock_production_lotService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_production_lot" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_production_lots/{stock_production_lot_id}")
    public ResponseEntity<Stock_production_lotDTO> get(@PathVariable("stock_production_lot_id") Integer stock_production_lot_id) {
        Stock_production_lotDTO dto = new Stock_production_lotDTO();
        Stock_production_lot domain = stock_production_lotService.get(stock_production_lot_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_production_lot" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_production_lots")

    public ResponseEntity<Stock_production_lotDTO> create(@RequestBody Stock_production_lotDTO stock_production_lotdto) {
        Stock_production_lotDTO dto = new Stock_production_lotDTO();
        Stock_production_lot domain = stock_production_lotdto.toDO();
		stock_production_lotService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_production_lot" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_production_lots/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_production_lotDTO> stock_production_lotdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_production_lot" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_production_lots/fetchdefault")
	public ResponseEntity<Page<Stock_production_lotDTO>> fetchDefault(Stock_production_lotSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_production_lotDTO> list = new ArrayList<Stock_production_lotDTO>();
        
        Page<Stock_production_lot> domains = stock_production_lotService.searchDefault(context) ;
        for(Stock_production_lot stock_production_lot : domains.getContent()){
            Stock_production_lotDTO dto = new Stock_production_lotDTO();
            dto.fromDO(stock_production_lot);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
