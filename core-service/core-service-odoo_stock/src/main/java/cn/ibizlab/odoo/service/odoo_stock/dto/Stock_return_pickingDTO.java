package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_return_picking.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_return_pickingDTO]
 */
public class Stock_return_pickingDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_return_pickingCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_return_pickingDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [PRODUCT_RETURN_MOVES]
     *
     */
    @Stock_return_pickingProduct_return_movesDefault(info = "默认规则")
    private String product_return_moves;

    @JsonIgnore
    private boolean product_return_movesDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_return_pickingWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_return_pickingIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MOVE_DEST_EXISTS]
     *
     */
    @Stock_return_pickingMove_dest_existsDefault(info = "默认规则")
    private String move_dest_exists;

    @JsonIgnore
    private boolean move_dest_existsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_return_picking__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ORIGINAL_LOCATION_ID_TEXT]
     *
     */
    @Stock_return_pickingOriginal_location_id_textDefault(info = "默认规则")
    private String original_location_id_text;

    @JsonIgnore
    private boolean original_location_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Stock_return_pickingLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [PARENT_LOCATION_ID_TEXT]
     *
     */
    @Stock_return_pickingParent_location_id_textDefault(info = "默认规则")
    private String parent_location_id_text;

    @JsonIgnore
    private boolean parent_location_id_textDirtyFlag;

    /**
     * 属性 [PICKING_ID_TEXT]
     *
     */
    @Stock_return_pickingPicking_id_textDefault(info = "默认规则")
    private String picking_id_text;

    @JsonIgnore
    private boolean picking_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_return_pickingWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_return_pickingCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PARENT_LOCATION_ID]
     *
     */
    @Stock_return_pickingParent_location_idDefault(info = "默认规则")
    private Integer parent_location_id;

    @JsonIgnore
    private boolean parent_location_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_return_pickingCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [ORIGINAL_LOCATION_ID]
     *
     */
    @Stock_return_pickingOriginal_location_idDefault(info = "默认规则")
    private Integer original_location_id;

    @JsonIgnore
    private boolean original_location_idDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Stock_return_pickingLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [PICKING_ID]
     *
     */
    @Stock_return_pickingPicking_idDefault(info = "默认规则")
    private Integer picking_id;

    @JsonIgnore
    private boolean picking_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_return_pickingWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_RETURN_MOVES]
     */
    @JsonProperty("product_return_moves")
    public String getProduct_return_moves(){
        return product_return_moves ;
    }

    /**
     * 设置 [PRODUCT_RETURN_MOVES]
     */
    @JsonProperty("product_return_moves")
    public void setProduct_return_moves(String  product_return_moves){
        this.product_return_moves = product_return_moves ;
        this.product_return_movesDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_RETURN_MOVES]脏标记
     */
    @JsonIgnore
    public boolean getProduct_return_movesDirtyFlag(){
        return product_return_movesDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MOVE_DEST_EXISTS]
     */
    @JsonProperty("move_dest_exists")
    public String getMove_dest_exists(){
        return move_dest_exists ;
    }

    /**
     * 设置 [MOVE_DEST_EXISTS]
     */
    @JsonProperty("move_dest_exists")
    public void setMove_dest_exists(String  move_dest_exists){
        this.move_dest_exists = move_dest_exists ;
        this.move_dest_existsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_DEST_EXISTS]脏标记
     */
    @JsonIgnore
    public boolean getMove_dest_existsDirtyFlag(){
        return move_dest_existsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ORIGINAL_LOCATION_ID_TEXT]
     */
    @JsonProperty("original_location_id_text")
    public String getOriginal_location_id_text(){
        return original_location_id_text ;
    }

    /**
     * 设置 [ORIGINAL_LOCATION_ID_TEXT]
     */
    @JsonProperty("original_location_id_text")
    public void setOriginal_location_id_text(String  original_location_id_text){
        this.original_location_id_text = original_location_id_text ;
        this.original_location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGINAL_LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOriginal_location_id_textDirtyFlag(){
        return original_location_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_LOCATION_ID_TEXT]
     */
    @JsonProperty("parent_location_id_text")
    public String getParent_location_id_text(){
        return parent_location_id_text ;
    }

    /**
     * 设置 [PARENT_LOCATION_ID_TEXT]
     */
    @JsonProperty("parent_location_id_text")
    public void setParent_location_id_text(String  parent_location_id_text){
        this.parent_location_id_text = parent_location_id_text ;
        this.parent_location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_location_id_textDirtyFlag(){
        return parent_location_id_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_ID_TEXT]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return picking_id_text ;
    }

    /**
     * 设置 [PICKING_ID_TEXT]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return picking_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_LOCATION_ID]
     */
    @JsonProperty("parent_location_id")
    public Integer getParent_location_id(){
        return parent_location_id ;
    }

    /**
     * 设置 [PARENT_LOCATION_ID]
     */
    @JsonProperty("parent_location_id")
    public void setParent_location_id(Integer  parent_location_id){
        this.parent_location_id = parent_location_id ;
        this.parent_location_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_location_idDirtyFlag(){
        return parent_location_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [ORIGINAL_LOCATION_ID]
     */
    @JsonProperty("original_location_id")
    public Integer getOriginal_location_id(){
        return original_location_id ;
    }

    /**
     * 设置 [ORIGINAL_LOCATION_ID]
     */
    @JsonProperty("original_location_id")
    public void setOriginal_location_id(Integer  original_location_id){
        this.original_location_id = original_location_id ;
        this.original_location_idDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGINAL_LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getOriginal_location_idDirtyFlag(){
        return original_location_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [PICKING_ID]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return picking_id ;
    }

    /**
     * 设置 [PICKING_ID]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return picking_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Stock_return_picking toDO() {
        Stock_return_picking srfdomain = new Stock_return_picking();
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getProduct_return_movesDirtyFlag())
            srfdomain.setProduct_return_moves(product_return_moves);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMove_dest_existsDirtyFlag())
            srfdomain.setMove_dest_exists(move_dest_exists);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getOriginal_location_id_textDirtyFlag())
            srfdomain.setOriginal_location_id_text(original_location_id_text);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getParent_location_id_textDirtyFlag())
            srfdomain.setParent_location_id_text(parent_location_id_text);
        if(getPicking_id_textDirtyFlag())
            srfdomain.setPicking_id_text(picking_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getParent_location_idDirtyFlag())
            srfdomain.setParent_location_id(parent_location_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getOriginal_location_idDirtyFlag())
            srfdomain.setOriginal_location_id(original_location_id);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getPicking_idDirtyFlag())
            srfdomain.setPicking_id(picking_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Stock_return_picking srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getProduct_return_movesDirtyFlag())
            this.setProduct_return_moves(srfdomain.getProduct_return_moves());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMove_dest_existsDirtyFlag())
            this.setMove_dest_exists(srfdomain.getMove_dest_exists());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getOriginal_location_id_textDirtyFlag())
            this.setOriginal_location_id_text(srfdomain.getOriginal_location_id_text());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getParent_location_id_textDirtyFlag())
            this.setParent_location_id_text(srfdomain.getParent_location_id_text());
        if(srfdomain.getPicking_id_textDirtyFlag())
            this.setPicking_id_text(srfdomain.getPicking_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getParent_location_idDirtyFlag())
            this.setParent_location_id(srfdomain.getParent_location_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getOriginal_location_idDirtyFlag())
            this.setOriginal_location_id(srfdomain.getOriginal_location_id());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getPicking_idDirtyFlag())
            this.setPicking_id(srfdomain.getPicking_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Stock_return_pickingDTO> fromDOPage(List<Stock_return_picking> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_return_pickingDTO> dtos=new ArrayList<Stock_return_pickingDTO>();
        for(Stock_return_picking domain : poPage) {
            Stock_return_pickingDTO dto = new Stock_return_pickingDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

