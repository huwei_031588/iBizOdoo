package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_change_product_qtyDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_product_qty;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_change_product_qtyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_product_qtySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_change_product_qty" })
@RestController
@RequestMapping("")
public class Stock_change_product_qtyResource {

    @Autowired
    private IStock_change_product_qtyService stock_change_product_qtyService;

    public IStock_change_product_qtyService getStock_change_product_qtyService() {
        return this.stock_change_product_qtyService;
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_change_product_qty" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_change_product_qties/{stock_change_product_qty_id}")

    public ResponseEntity<Stock_change_product_qtyDTO> update(@PathVariable("stock_change_product_qty_id") Integer stock_change_product_qty_id, @RequestBody Stock_change_product_qtyDTO stock_change_product_qtydto) {
		Stock_change_product_qty domain = stock_change_product_qtydto.toDO();
        domain.setId(stock_change_product_qty_id);
		stock_change_product_qtyService.update(domain);
		Stock_change_product_qtyDTO dto = new Stock_change_product_qtyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_change_product_qty" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_change_product_qties")

    public ResponseEntity<Stock_change_product_qtyDTO> create(@RequestBody Stock_change_product_qtyDTO stock_change_product_qtydto) {
        Stock_change_product_qtyDTO dto = new Stock_change_product_qtyDTO();
        Stock_change_product_qty domain = stock_change_product_qtydto.toDO();
		stock_change_product_qtyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_change_product_qty" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_change_product_qties/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_change_product_qtyDTO> stock_change_product_qtydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_change_product_qty" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_change_product_qties/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_change_product_qtyDTO> stock_change_product_qtydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_change_product_qty" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_change_product_qties/createBatch")
    public ResponseEntity<Boolean> createBatchStock_change_product_qty(@RequestBody List<Stock_change_product_qtyDTO> stock_change_product_qtydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_change_product_qty" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_change_product_qties/{stock_change_product_qty_id}")
    public ResponseEntity<Stock_change_product_qtyDTO> get(@PathVariable("stock_change_product_qty_id") Integer stock_change_product_qty_id) {
        Stock_change_product_qtyDTO dto = new Stock_change_product_qtyDTO();
        Stock_change_product_qty domain = stock_change_product_qtyService.get(stock_change_product_qty_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_change_product_qty" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_change_product_qties/{stock_change_product_qty_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_change_product_qty_id") Integer stock_change_product_qty_id) {
        Stock_change_product_qtyDTO stock_change_product_qtydto = new Stock_change_product_qtyDTO();
		Stock_change_product_qty domain = new Stock_change_product_qty();
		stock_change_product_qtydto.setId(stock_change_product_qty_id);
		domain.setId(stock_change_product_qty_id);
        Boolean rst = stock_change_product_qtyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_change_product_qty" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_change_product_qties/fetchdefault")
	public ResponseEntity<Page<Stock_change_product_qtyDTO>> fetchDefault(Stock_change_product_qtySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_change_product_qtyDTO> list = new ArrayList<Stock_change_product_qtyDTO>();
        
        Page<Stock_change_product_qty> domains = stock_change_product_qtyService.searchDefault(context) ;
        for(Stock_change_product_qty stock_change_product_qty : domains.getContent()){
            Stock_change_product_qtyDTO dto = new Stock_change_product_qtyDTO();
            dto.fromDO(stock_change_product_qty);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
