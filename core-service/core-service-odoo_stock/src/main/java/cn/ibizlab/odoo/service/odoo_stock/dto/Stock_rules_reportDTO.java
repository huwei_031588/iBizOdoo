package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_rules_report.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rules_report;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_rules_reportDTO]
 */
public class Stock_rules_reportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_rules_report__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_rules_reportDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_rules_reportCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_rules_reportWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [WAREHOUSE_IDS]
     *
     */
    @Stock_rules_reportWarehouse_idsDefault(info = "默认规则")
    private String warehouse_ids;

    @JsonIgnore
    private boolean warehouse_idsDirtyFlag;

    /**
     * 属性 [PRODUCT_HAS_VARIANTS]
     *
     */
    @Stock_rules_reportProduct_has_variantsDefault(info = "默认规则")
    private String product_has_variants;

    @JsonIgnore
    private boolean product_has_variantsDirtyFlag;

    /**
     * 属性 [SO_ROUTE_IDS]
     *
     */
    @Stock_rules_reportSo_route_idsDefault(info = "默认规则")
    private String so_route_ids;

    @JsonIgnore
    private boolean so_route_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_rules_reportIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Stock_rules_reportProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID_TEXT]
     *
     */
    @Stock_rules_reportProduct_tmpl_id_textDefault(info = "默认规则")
    private String product_tmpl_id_text;

    @JsonIgnore
    private boolean product_tmpl_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_rules_reportCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_rules_reportWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_rules_reportWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Stock_rules_reportProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @Stock_rules_reportProduct_tmpl_idDefault(info = "默认规则")
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_rules_reportCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_IDS]
     */
    @JsonProperty("warehouse_ids")
    public String getWarehouse_ids(){
        return warehouse_ids ;
    }

    /**
     * 设置 [WAREHOUSE_IDS]
     */
    @JsonProperty("warehouse_ids")
    public void setWarehouse_ids(String  warehouse_ids){
        this.warehouse_ids = warehouse_ids ;
        this.warehouse_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idsDirtyFlag(){
        return warehouse_idsDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_HAS_VARIANTS]
     */
    @JsonProperty("product_has_variants")
    public String getProduct_has_variants(){
        return product_has_variants ;
    }

    /**
     * 设置 [PRODUCT_HAS_VARIANTS]
     */
    @JsonProperty("product_has_variants")
    public void setProduct_has_variants(String  product_has_variants){
        this.product_has_variants = product_has_variants ;
        this.product_has_variantsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_HAS_VARIANTS]脏标记
     */
    @JsonIgnore
    public boolean getProduct_has_variantsDirtyFlag(){
        return product_has_variantsDirtyFlag ;
    }

    /**
     * 获取 [SO_ROUTE_IDS]
     */
    @JsonProperty("so_route_ids")
    public String getSo_route_ids(){
        return so_route_ids ;
    }

    /**
     * 设置 [SO_ROUTE_IDS]
     */
    @JsonProperty("so_route_ids")
    public void setSo_route_ids(String  so_route_ids){
        this.so_route_ids = so_route_ids ;
        this.so_route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SO_ROUTE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSo_route_idsDirtyFlag(){
        return so_route_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return product_tmpl_id_text ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return product_tmpl_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return product_tmpl_id ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return product_tmpl_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Stock_rules_report toDO() {
        Stock_rules_report srfdomain = new Stock_rules_report();
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getWarehouse_idsDirtyFlag())
            srfdomain.setWarehouse_ids(warehouse_ids);
        if(getProduct_has_variantsDirtyFlag())
            srfdomain.setProduct_has_variants(product_has_variants);
        if(getSo_route_idsDirtyFlag())
            srfdomain.setSo_route_ids(so_route_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getProduct_tmpl_id_textDirtyFlag())
            srfdomain.setProduct_tmpl_id_text(product_tmpl_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getProduct_tmpl_idDirtyFlag())
            srfdomain.setProduct_tmpl_id(product_tmpl_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Stock_rules_report srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getWarehouse_idsDirtyFlag())
            this.setWarehouse_ids(srfdomain.getWarehouse_ids());
        if(srfdomain.getProduct_has_variantsDirtyFlag())
            this.setProduct_has_variants(srfdomain.getProduct_has_variants());
        if(srfdomain.getSo_route_idsDirtyFlag())
            this.setSo_route_ids(srfdomain.getSo_route_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getProduct_tmpl_id_textDirtyFlag())
            this.setProduct_tmpl_id_text(srfdomain.getProduct_tmpl_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getProduct_tmpl_idDirtyFlag())
            this.setProduct_tmpl_id(srfdomain.getProduct_tmpl_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Stock_rules_reportDTO> fromDOPage(List<Stock_rules_report> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_rules_reportDTO> dtos=new ArrayList<Stock_rules_reportDTO>();
        for(Stock_rules_report domain : poPage) {
            Stock_rules_reportDTO dto = new Stock_rules_reportDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

