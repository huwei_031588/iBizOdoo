package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_quantity_historyDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quantity_history;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_quantity_historyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantity_historySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_quantity_history" })
@RestController
@RequestMapping("")
public class Stock_quantity_historyResource {

    @Autowired
    private IStock_quantity_historyService stock_quantity_historyService;

    public IStock_quantity_historyService getStock_quantity_historyService() {
        return this.stock_quantity_historyService;
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_quantity_history" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quantity_histories/{stock_quantity_history_id}")
    public ResponseEntity<Stock_quantity_historyDTO> get(@PathVariable("stock_quantity_history_id") Integer stock_quantity_history_id) {
        Stock_quantity_historyDTO dto = new Stock_quantity_historyDTO();
        Stock_quantity_history domain = stock_quantity_historyService.get(stock_quantity_history_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_quantity_history" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quantity_histories/createBatch")
    public ResponseEntity<Boolean> createBatchStock_quantity_history(@RequestBody List<Stock_quantity_historyDTO> stock_quantity_historydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_quantity_history" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quantity_histories/{stock_quantity_history_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_quantity_history_id") Integer stock_quantity_history_id) {
        Stock_quantity_historyDTO stock_quantity_historydto = new Stock_quantity_historyDTO();
		Stock_quantity_history domain = new Stock_quantity_history();
		stock_quantity_historydto.setId(stock_quantity_history_id);
		domain.setId(stock_quantity_history_id);
        Boolean rst = stock_quantity_historyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_quantity_history" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quantity_histories")

    public ResponseEntity<Stock_quantity_historyDTO> create(@RequestBody Stock_quantity_historyDTO stock_quantity_historydto) {
        Stock_quantity_historyDTO dto = new Stock_quantity_historyDTO();
        Stock_quantity_history domain = stock_quantity_historydto.toDO();
		stock_quantity_historyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_quantity_history" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quantity_histories/{stock_quantity_history_id}")

    public ResponseEntity<Stock_quantity_historyDTO> update(@PathVariable("stock_quantity_history_id") Integer stock_quantity_history_id, @RequestBody Stock_quantity_historyDTO stock_quantity_historydto) {
		Stock_quantity_history domain = stock_quantity_historydto.toDO();
        domain.setId(stock_quantity_history_id);
		stock_quantity_historyService.update(domain);
		Stock_quantity_historyDTO dto = new Stock_quantity_historyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_quantity_history" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quantity_histories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_quantity_historyDTO> stock_quantity_historydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_quantity_history" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quantity_histories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_quantity_historyDTO> stock_quantity_historydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_quantity_history" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_quantity_histories/fetchdefault")
	public ResponseEntity<Page<Stock_quantity_historyDTO>> fetchDefault(Stock_quantity_historySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_quantity_historyDTO> list = new ArrayList<Stock_quantity_historyDTO>();
        
        Page<Stock_quantity_history> domains = stock_quantity_historyService.searchDefault(context) ;
        for(Stock_quantity_history stock_quantity_history : domains.getContent()){
            Stock_quantity_historyDTO dto = new Stock_quantity_historyDTO();
            dto.fromDO(stock_quantity_history);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
