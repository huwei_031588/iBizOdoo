package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_package_level.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_package_levelDTO]
 */
public class Stock_package_levelDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @Stock_package_levelMove_line_idsDefault(info = "默认规则")
    private String move_line_ids;

    @JsonIgnore
    private boolean move_line_idsDirtyFlag;

    /**
     * 属性 [MOVE_IDS]
     *
     */
    @Stock_package_levelMove_idsDefault(info = "默认规则")
    private String move_ids;

    @JsonIgnore
    private boolean move_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_package_levelCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Stock_package_levelLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [SHOW_LOTS_TEXT]
     *
     */
    @Stock_package_levelShow_lots_textDefault(info = "默认规则")
    private String show_lots_text;

    @JsonIgnore
    private boolean show_lots_textDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_package_level__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [IS_DONE]
     *
     */
    @Stock_package_levelIs_doneDefault(info = "默认规则")
    private String is_done;

    @JsonIgnore
    private boolean is_doneDirtyFlag;

    /**
     * 属性 [SHOW_LOTS_M2O]
     *
     */
    @Stock_package_levelShow_lots_m2oDefault(info = "默认规则")
    private String show_lots_m2o;

    @JsonIgnore
    private boolean show_lots_m2oDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Stock_package_levelStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_package_levelIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_package_levelWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [IS_FRESH_PACKAGE]
     *
     */
    @Stock_package_levelIs_fresh_packageDefault(info = "默认规则")
    private String is_fresh_package;

    @JsonIgnore
    private boolean is_fresh_packageDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_package_levelDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [PICKING_ID_TEXT]
     *
     */
    @Stock_package_levelPicking_id_textDefault(info = "默认规则")
    private String picking_id_text;

    @JsonIgnore
    private boolean picking_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_package_levelWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PACKAGE_ID_TEXT]
     *
     */
    @Stock_package_levelPackage_id_textDefault(info = "默认规则")
    private String package_id_text;

    @JsonIgnore
    private boolean package_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @Stock_package_levelLocation_dest_id_textDefault(info = "默认规则")
    private String location_dest_id_text;

    @JsonIgnore
    private boolean location_dest_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_package_levelCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PICKING_SOURCE_LOCATION]
     *
     */
    @Stock_package_levelPicking_source_locationDefault(info = "默认规则")
    private Integer picking_source_location;

    @JsonIgnore
    private boolean picking_source_locationDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_package_levelWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PICKING_ID]
     *
     */
    @Stock_package_levelPicking_idDefault(info = "默认规则")
    private Integer picking_id;

    @JsonIgnore
    private boolean picking_idDirtyFlag;

    /**
     * 属性 [PACKAGE_ID]
     *
     */
    @Stock_package_levelPackage_idDefault(info = "默认规则")
    private Integer package_id;

    @JsonIgnore
    private boolean package_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_package_levelCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @Stock_package_levelLocation_dest_idDefault(info = "默认规则")
    private Integer location_dest_id;

    @JsonIgnore
    private boolean location_dest_idDirtyFlag;


    /**
     * 获取 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return move_line_ids ;
    }

    /**
     * 设置 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [MOVE_IDS]
     */
    @JsonProperty("move_ids")
    public String getMove_ids(){
        return move_ids ;
    }

    /**
     * 设置 [MOVE_IDS]
     */
    @JsonProperty("move_ids")
    public void setMove_ids(String  move_ids){
        this.move_ids = move_ids ;
        this.move_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_idsDirtyFlag(){
        return move_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [SHOW_LOTS_TEXT]
     */
    @JsonProperty("show_lots_text")
    public String getShow_lots_text(){
        return show_lots_text ;
    }

    /**
     * 设置 [SHOW_LOTS_TEXT]
     */
    @JsonProperty("show_lots_text")
    public void setShow_lots_text(String  show_lots_text){
        this.show_lots_text = show_lots_text ;
        this.show_lots_textDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_LOTS_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getShow_lots_textDirtyFlag(){
        return show_lots_textDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [IS_DONE]
     */
    @JsonProperty("is_done")
    public String getIs_done(){
        return is_done ;
    }

    /**
     * 设置 [IS_DONE]
     */
    @JsonProperty("is_done")
    public void setIs_done(String  is_done){
        this.is_done = is_done ;
        this.is_doneDirtyFlag = true ;
    }

    /**
     * 获取 [IS_DONE]脏标记
     */
    @JsonIgnore
    public boolean getIs_doneDirtyFlag(){
        return is_doneDirtyFlag ;
    }

    /**
     * 获取 [SHOW_LOTS_M2O]
     */
    @JsonProperty("show_lots_m2o")
    public String getShow_lots_m2o(){
        return show_lots_m2o ;
    }

    /**
     * 设置 [SHOW_LOTS_M2O]
     */
    @JsonProperty("show_lots_m2o")
    public void setShow_lots_m2o(String  show_lots_m2o){
        this.show_lots_m2o = show_lots_m2o ;
        this.show_lots_m2oDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_LOTS_M2O]脏标记
     */
    @JsonIgnore
    public boolean getShow_lots_m2oDirtyFlag(){
        return show_lots_m2oDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [IS_FRESH_PACKAGE]
     */
    @JsonProperty("is_fresh_package")
    public String getIs_fresh_package(){
        return is_fresh_package ;
    }

    /**
     * 设置 [IS_FRESH_PACKAGE]
     */
    @JsonProperty("is_fresh_package")
    public void setIs_fresh_package(String  is_fresh_package){
        this.is_fresh_package = is_fresh_package ;
        this.is_fresh_packageDirtyFlag = true ;
    }

    /**
     * 获取 [IS_FRESH_PACKAGE]脏标记
     */
    @JsonIgnore
    public boolean getIs_fresh_packageDirtyFlag(){
        return is_fresh_packageDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [PICKING_ID_TEXT]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return picking_id_text ;
    }

    /**
     * 设置 [PICKING_ID_TEXT]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return picking_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_ID_TEXT]
     */
    @JsonProperty("package_id_text")
    public String getPackage_id_text(){
        return package_id_text ;
    }

    /**
     * 设置 [PACKAGE_ID_TEXT]
     */
    @JsonProperty("package_id_text")
    public void setPackage_id_text(String  package_id_text){
        this.package_id_text = package_id_text ;
        this.package_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPackage_id_textDirtyFlag(){
        return package_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return location_dest_id_text ;
    }

    /**
     * 设置 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return location_dest_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_SOURCE_LOCATION]
     */
    @JsonProperty("picking_source_location")
    public Integer getPicking_source_location(){
        return picking_source_location ;
    }

    /**
     * 设置 [PICKING_SOURCE_LOCATION]
     */
    @JsonProperty("picking_source_location")
    public void setPicking_source_location(Integer  picking_source_location){
        this.picking_source_location = picking_source_location ;
        this.picking_source_locationDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_SOURCE_LOCATION]脏标记
     */
    @JsonIgnore
    public boolean getPicking_source_locationDirtyFlag(){
        return picking_source_locationDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PICKING_ID]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return picking_id ;
    }

    /**
     * 设置 [PICKING_ID]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return picking_idDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_ID]
     */
    @JsonProperty("package_id")
    public Integer getPackage_id(){
        return package_id ;
    }

    /**
     * 设置 [PACKAGE_ID]
     */
    @JsonProperty("package_id")
    public void setPackage_id(Integer  package_id){
        this.package_id = package_id ;
        this.package_idDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPackage_idDirtyFlag(){
        return package_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return location_dest_id ;
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return location_dest_idDirtyFlag ;
    }



    public Stock_package_level toDO() {
        Stock_package_level srfdomain = new Stock_package_level();
        if(getMove_line_idsDirtyFlag())
            srfdomain.setMove_line_ids(move_line_ids);
        if(getMove_idsDirtyFlag())
            srfdomain.setMove_ids(move_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getShow_lots_textDirtyFlag())
            srfdomain.setShow_lots_text(show_lots_text);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIs_doneDirtyFlag())
            srfdomain.setIs_done(is_done);
        if(getShow_lots_m2oDirtyFlag())
            srfdomain.setShow_lots_m2o(show_lots_m2o);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIs_fresh_packageDirtyFlag())
            srfdomain.setIs_fresh_package(is_fresh_package);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getPicking_id_textDirtyFlag())
            srfdomain.setPicking_id_text(picking_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getPackage_id_textDirtyFlag())
            srfdomain.setPackage_id_text(package_id_text);
        if(getLocation_dest_id_textDirtyFlag())
            srfdomain.setLocation_dest_id_text(location_dest_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getPicking_source_locationDirtyFlag())
            srfdomain.setPicking_source_location(picking_source_location);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPicking_idDirtyFlag())
            srfdomain.setPicking_id(picking_id);
        if(getPackage_idDirtyFlag())
            srfdomain.setPackage_id(package_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getLocation_dest_idDirtyFlag())
            srfdomain.setLocation_dest_id(location_dest_id);

        return srfdomain;
    }

    public void fromDO(Stock_package_level srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMove_line_idsDirtyFlag())
            this.setMove_line_ids(srfdomain.getMove_line_ids());
        if(srfdomain.getMove_idsDirtyFlag())
            this.setMove_ids(srfdomain.getMove_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getShow_lots_textDirtyFlag())
            this.setShow_lots_text(srfdomain.getShow_lots_text());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIs_doneDirtyFlag())
            this.setIs_done(srfdomain.getIs_done());
        if(srfdomain.getShow_lots_m2oDirtyFlag())
            this.setShow_lots_m2o(srfdomain.getShow_lots_m2o());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIs_fresh_packageDirtyFlag())
            this.setIs_fresh_package(srfdomain.getIs_fresh_package());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getPicking_id_textDirtyFlag())
            this.setPicking_id_text(srfdomain.getPicking_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getPackage_id_textDirtyFlag())
            this.setPackage_id_text(srfdomain.getPackage_id_text());
        if(srfdomain.getLocation_dest_id_textDirtyFlag())
            this.setLocation_dest_id_text(srfdomain.getLocation_dest_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getPicking_source_locationDirtyFlag())
            this.setPicking_source_location(srfdomain.getPicking_source_location());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPicking_idDirtyFlag())
            this.setPicking_id(srfdomain.getPicking_id());
        if(srfdomain.getPackage_idDirtyFlag())
            this.setPackage_id(srfdomain.getPackage_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getLocation_dest_idDirtyFlag())
            this.setLocation_dest_id(srfdomain.getLocation_dest_id());

    }

    public List<Stock_package_levelDTO> fromDOPage(List<Stock_package_level> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_package_levelDTO> dtos=new ArrayList<Stock_package_levelDTO>();
        for(Stock_package_level domain : poPage) {
            Stock_package_levelDTO dto = new Stock_package_levelDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

