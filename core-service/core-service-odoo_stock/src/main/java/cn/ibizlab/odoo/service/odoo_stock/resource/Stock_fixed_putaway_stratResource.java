package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_fixed_putaway_stratDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_fixed_putaway_stratService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_fixed_putaway_strat" })
@RestController
@RequestMapping("")
public class Stock_fixed_putaway_stratResource {

    @Autowired
    private IStock_fixed_putaway_stratService stock_fixed_putaway_stratService;

    public IStock_fixed_putaway_stratService getStock_fixed_putaway_stratService() {
        return this.stock_fixed_putaway_stratService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_fixed_putaway_strat" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_fixed_putaway_strats/createBatch")
    public ResponseEntity<Boolean> createBatchStock_fixed_putaway_strat(@RequestBody List<Stock_fixed_putaway_stratDTO> stock_fixed_putaway_stratdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_fixed_putaway_strat" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_fixed_putaway_strats/{stock_fixed_putaway_strat_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_fixed_putaway_strat_id") Integer stock_fixed_putaway_strat_id) {
        Stock_fixed_putaway_stratDTO stock_fixed_putaway_stratdto = new Stock_fixed_putaway_stratDTO();
		Stock_fixed_putaway_strat domain = new Stock_fixed_putaway_strat();
		stock_fixed_putaway_stratdto.setId(stock_fixed_putaway_strat_id);
		domain.setId(stock_fixed_putaway_strat_id);
        Boolean rst = stock_fixed_putaway_stratService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_fixed_putaway_strat" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_fixed_putaway_strats/{stock_fixed_putaway_strat_id}")
    public ResponseEntity<Stock_fixed_putaway_stratDTO> get(@PathVariable("stock_fixed_putaway_strat_id") Integer stock_fixed_putaway_strat_id) {
        Stock_fixed_putaway_stratDTO dto = new Stock_fixed_putaway_stratDTO();
        Stock_fixed_putaway_strat domain = stock_fixed_putaway_stratService.get(stock_fixed_putaway_strat_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_fixed_putaway_strat" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_fixed_putaway_strats/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_fixed_putaway_stratDTO> stock_fixed_putaway_stratdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_fixed_putaway_strat" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_fixed_putaway_strats/{stock_fixed_putaway_strat_id}")

    public ResponseEntity<Stock_fixed_putaway_stratDTO> update(@PathVariable("stock_fixed_putaway_strat_id") Integer stock_fixed_putaway_strat_id, @RequestBody Stock_fixed_putaway_stratDTO stock_fixed_putaway_stratdto) {
		Stock_fixed_putaway_strat domain = stock_fixed_putaway_stratdto.toDO();
        domain.setId(stock_fixed_putaway_strat_id);
		stock_fixed_putaway_stratService.update(domain);
		Stock_fixed_putaway_stratDTO dto = new Stock_fixed_putaway_stratDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_fixed_putaway_strat" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_fixed_putaway_strats/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_fixed_putaway_stratDTO> stock_fixed_putaway_stratdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_fixed_putaway_strat" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_fixed_putaway_strats")

    public ResponseEntity<Stock_fixed_putaway_stratDTO> create(@RequestBody Stock_fixed_putaway_stratDTO stock_fixed_putaway_stratdto) {
        Stock_fixed_putaway_stratDTO dto = new Stock_fixed_putaway_stratDTO();
        Stock_fixed_putaway_strat domain = stock_fixed_putaway_stratdto.toDO();
		stock_fixed_putaway_stratService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_fixed_putaway_strat" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_fixed_putaway_strats/fetchdefault")
	public ResponseEntity<Page<Stock_fixed_putaway_stratDTO>> fetchDefault(Stock_fixed_putaway_stratSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_fixed_putaway_stratDTO> list = new ArrayList<Stock_fixed_putaway_stratDTO>();
        
        Page<Stock_fixed_putaway_strat> domains = stock_fixed_putaway_stratService.searchDefault(context) ;
        for(Stock_fixed_putaway_strat stock_fixed_putaway_strat : domains.getContent()){
            Stock_fixed_putaway_stratDTO dto = new Stock_fixed_putaway_stratDTO();
            dto.fromDO(stock_fixed_putaway_strat);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
