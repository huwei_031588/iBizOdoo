package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_track_confirmationDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_track_confirmationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_confirmationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_track_confirmation" })
@RestController
@RequestMapping("")
public class Stock_track_confirmationResource {

    @Autowired
    private IStock_track_confirmationService stock_track_confirmationService;

    public IStock_track_confirmationService getStock_track_confirmationService() {
        return this.stock_track_confirmationService;
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_track_confirmation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_track_confirmations")

    public ResponseEntity<Stock_track_confirmationDTO> create(@RequestBody Stock_track_confirmationDTO stock_track_confirmationdto) {
        Stock_track_confirmationDTO dto = new Stock_track_confirmationDTO();
        Stock_track_confirmation domain = stock_track_confirmationdto.toDO();
		stock_track_confirmationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_track_confirmation" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_track_confirmations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_track_confirmationDTO> stock_track_confirmationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_track_confirmation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_track_confirmations/{stock_track_confirmation_id}")
    public ResponseEntity<Stock_track_confirmationDTO> get(@PathVariable("stock_track_confirmation_id") Integer stock_track_confirmation_id) {
        Stock_track_confirmationDTO dto = new Stock_track_confirmationDTO();
        Stock_track_confirmation domain = stock_track_confirmationService.get(stock_track_confirmation_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_track_confirmation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_track_confirmations/{stock_track_confirmation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_track_confirmation_id") Integer stock_track_confirmation_id) {
        Stock_track_confirmationDTO stock_track_confirmationdto = new Stock_track_confirmationDTO();
		Stock_track_confirmation domain = new Stock_track_confirmation();
		stock_track_confirmationdto.setId(stock_track_confirmation_id);
		domain.setId(stock_track_confirmation_id);
        Boolean rst = stock_track_confirmationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_track_confirmation" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_track_confirmations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_track_confirmationDTO> stock_track_confirmationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_track_confirmation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_track_confirmations/{stock_track_confirmation_id}")

    public ResponseEntity<Stock_track_confirmationDTO> update(@PathVariable("stock_track_confirmation_id") Integer stock_track_confirmation_id, @RequestBody Stock_track_confirmationDTO stock_track_confirmationdto) {
		Stock_track_confirmation domain = stock_track_confirmationdto.toDO();
        domain.setId(stock_track_confirmation_id);
		stock_track_confirmationService.update(domain);
		Stock_track_confirmationDTO dto = new Stock_track_confirmationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_track_confirmation" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_track_confirmations/createBatch")
    public ResponseEntity<Boolean> createBatchStock_track_confirmation(@RequestBody List<Stock_track_confirmationDTO> stock_track_confirmationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_track_confirmation" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_track_confirmations/fetchdefault")
	public ResponseEntity<Page<Stock_track_confirmationDTO>> fetchDefault(Stock_track_confirmationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_track_confirmationDTO> list = new ArrayList<Stock_track_confirmationDTO>();
        
        Page<Stock_track_confirmation> domains = stock_track_confirmationService.searchDefault(context) ;
        for(Stock_track_confirmation stock_track_confirmation : domains.getContent()){
            Stock_track_confirmationDTO dto = new Stock_track_confirmationDTO();
            dto.fromDO(stock_track_confirmation);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
