package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_picking_type.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_picking_typeDTO]
 */
public class Stock_picking_typeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SHOW_OPERATIONS]
     *
     */
    @Stock_picking_typeShow_operationsDefault(info = "默认规则")
    private String show_operations;

    @JsonIgnore
    private boolean show_operationsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_picking_typeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [SEQUENCE_ID]
     *
     */
    @Stock_picking_typeSequence_idDefault(info = "默认规则")
    private Integer sequence_id;

    @JsonIgnore
    private boolean sequence_idDirtyFlag;

    /**
     * 属性 [USE_CREATE_LOTS]
     *
     */
    @Stock_picking_typeUse_create_lotsDefault(info = "默认规则")
    private String use_create_lots;

    @JsonIgnore
    private boolean use_create_lotsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_picking_typeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [BARCODE]
     *
     */
    @Stock_picking_typeBarcodeDefault(info = "默认规则")
    private String barcode;

    @JsonIgnore
    private boolean barcodeDirtyFlag;

    /**
     * 属性 [SHOW_RESERVED]
     *
     */
    @Stock_picking_typeShow_reservedDefault(info = "默认规则")
    private String show_reserved;

    @JsonIgnore
    private boolean show_reservedDirtyFlag;

    /**
     * 属性 [RATE_PICKING_BACKORDERS]
     *
     */
    @Stock_picking_typeRate_picking_backordersDefault(info = "默认规则")
    private Integer rate_picking_backorders;

    @JsonIgnore
    private boolean rate_picking_backordersDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Stock_picking_typeSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [COUNT_PICKING_BACKORDERS]
     *
     */
    @Stock_picking_typeCount_picking_backordersDefault(info = "默认规则")
    private Integer count_picking_backorders;

    @JsonIgnore
    private boolean count_picking_backordersDirtyFlag;

    /**
     * 属性 [COUNT_MO_TODO]
     *
     */
    @Stock_picking_typeCount_mo_todoDefault(info = "默认规则")
    private Integer count_mo_todo;

    @JsonIgnore
    private boolean count_mo_todoDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Stock_picking_typeColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [SHOW_ENTIRE_PACKS]
     *
     */
    @Stock_picking_typeShow_entire_packsDefault(info = "默认规则")
    private String show_entire_packs;

    @JsonIgnore
    private boolean show_entire_packsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_picking_typeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [COUNT_MO_WAITING]
     *
     */
    @Stock_picking_typeCount_mo_waitingDefault(info = "默认规则")
    private Integer count_mo_waiting;

    @JsonIgnore
    private boolean count_mo_waitingDirtyFlag;

    /**
     * 属性 [COUNT_PICKING_DRAFT]
     *
     */
    @Stock_picking_typeCount_picking_draftDefault(info = "默认规则")
    private Integer count_picking_draft;

    @JsonIgnore
    private boolean count_picking_draftDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Stock_picking_typeActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [COUNT_PICKING_LATE]
     *
     */
    @Stock_picking_typeCount_picking_lateDefault(info = "默认规则")
    private Integer count_picking_late;

    @JsonIgnore
    private boolean count_picking_lateDirtyFlag;

    /**
     * 属性 [COUNT_PICKING]
     *
     */
    @Stock_picking_typeCount_pickingDefault(info = "默认规则")
    private Integer count_picking;

    @JsonIgnore
    private boolean count_pickingDirtyFlag;

    /**
     * 属性 [COUNT_PICKING_READY]
     *
     */
    @Stock_picking_typeCount_picking_readyDefault(info = "默认规则")
    private Integer count_picking_ready;

    @JsonIgnore
    private boolean count_picking_readyDirtyFlag;

    /**
     * 属性 [LAST_DONE_PICKING]
     *
     */
    @Stock_picking_typeLast_done_pickingDefault(info = "默认规则")
    private String last_done_picking;

    @JsonIgnore
    private boolean last_done_pickingDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_picking_type__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [COUNT_PICKING_WAITING]
     *
     */
    @Stock_picking_typeCount_picking_waitingDefault(info = "默认规则")
    private Integer count_picking_waiting;

    @JsonIgnore
    private boolean count_picking_waitingDirtyFlag;

    /**
     * 属性 [CODE]
     *
     */
    @Stock_picking_typeCodeDefault(info = "默认规则")
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_picking_typeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [RATE_PICKING_LATE]
     *
     */
    @Stock_picking_typeRate_picking_lateDefault(info = "默认规则")
    private Integer rate_picking_late;

    @JsonIgnore
    private boolean rate_picking_lateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Stock_picking_typeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [COUNT_MO_LATE]
     *
     */
    @Stock_picking_typeCount_mo_lateDefault(info = "默认规则")
    private Integer count_mo_late;

    @JsonIgnore
    private boolean count_mo_lateDirtyFlag;

    /**
     * 属性 [USE_EXISTING_LOTS]
     *
     */
    @Stock_picking_typeUse_existing_lotsDefault(info = "默认规则")
    private String use_existing_lots;

    @JsonIgnore
    private boolean use_existing_lotsDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_picking_typeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [DEFAULT_LOCATION_SRC_ID_TEXT]
     *
     */
    @Stock_picking_typeDefault_location_src_id_textDefault(info = "默认规则")
    private String default_location_src_id_text;

    @JsonIgnore
    private boolean default_location_src_id_textDirtyFlag;

    /**
     * 属性 [RETURN_PICKING_TYPE_ID_TEXT]
     *
     */
    @Stock_picking_typeReturn_picking_type_id_textDefault(info = "默认规则")
    private String return_picking_type_id_text;

    @JsonIgnore
    private boolean return_picking_type_id_textDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @Stock_picking_typeWarehouse_id_textDefault(info = "默认规则")
    private String warehouse_id_text;

    @JsonIgnore
    private boolean warehouse_id_textDirtyFlag;

    /**
     * 属性 [DEFAULT_LOCATION_DEST_ID_TEXT]
     *
     */
    @Stock_picking_typeDefault_location_dest_id_textDefault(info = "默认规则")
    private String default_location_dest_id_text;

    @JsonIgnore
    private boolean default_location_dest_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_picking_typeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_picking_typeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [DEFAULT_LOCATION_DEST_ID]
     *
     */
    @Stock_picking_typeDefault_location_dest_idDefault(info = "默认规则")
    private Integer default_location_dest_id;

    @JsonIgnore
    private boolean default_location_dest_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_picking_typeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @Stock_picking_typeWarehouse_idDefault(info = "默认规则")
    private Integer warehouse_id;

    @JsonIgnore
    private boolean warehouse_idDirtyFlag;

    /**
     * 属性 [DEFAULT_LOCATION_SRC_ID]
     *
     */
    @Stock_picking_typeDefault_location_src_idDefault(info = "默认规则")
    private Integer default_location_src_id;

    @JsonIgnore
    private boolean default_location_src_idDirtyFlag;

    /**
     * 属性 [RETURN_PICKING_TYPE_ID]
     *
     */
    @Stock_picking_typeReturn_picking_type_idDefault(info = "默认规则")
    private Integer return_picking_type_id;

    @JsonIgnore
    private boolean return_picking_type_idDirtyFlag;


    /**
     * 获取 [SHOW_OPERATIONS]
     */
    @JsonProperty("show_operations")
    public String getShow_operations(){
        return show_operations ;
    }

    /**
     * 设置 [SHOW_OPERATIONS]
     */
    @JsonProperty("show_operations")
    public void setShow_operations(String  show_operations){
        this.show_operations = show_operations ;
        this.show_operationsDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_OPERATIONS]脏标记
     */
    @JsonIgnore
    public boolean getShow_operationsDirtyFlag(){
        return show_operationsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE_ID]
     */
    @JsonProperty("sequence_id")
    public Integer getSequence_id(){
        return sequence_id ;
    }

    /**
     * 设置 [SEQUENCE_ID]
     */
    @JsonProperty("sequence_id")
    public void setSequence_id(Integer  sequence_id){
        this.sequence_id = sequence_id ;
        this.sequence_idDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSequence_idDirtyFlag(){
        return sequence_idDirtyFlag ;
    }

    /**
     * 获取 [USE_CREATE_LOTS]
     */
    @JsonProperty("use_create_lots")
    public String getUse_create_lots(){
        return use_create_lots ;
    }

    /**
     * 设置 [USE_CREATE_LOTS]
     */
    @JsonProperty("use_create_lots")
    public void setUse_create_lots(String  use_create_lots){
        this.use_create_lots = use_create_lots ;
        this.use_create_lotsDirtyFlag = true ;
    }

    /**
     * 获取 [USE_CREATE_LOTS]脏标记
     */
    @JsonIgnore
    public boolean getUse_create_lotsDirtyFlag(){
        return use_create_lotsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [BARCODE]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return barcode ;
    }

    /**
     * 设置 [BARCODE]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

    /**
     * 获取 [BARCODE]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return barcodeDirtyFlag ;
    }

    /**
     * 获取 [SHOW_RESERVED]
     */
    @JsonProperty("show_reserved")
    public String getShow_reserved(){
        return show_reserved ;
    }

    /**
     * 设置 [SHOW_RESERVED]
     */
    @JsonProperty("show_reserved")
    public void setShow_reserved(String  show_reserved){
        this.show_reserved = show_reserved ;
        this.show_reservedDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_RESERVED]脏标记
     */
    @JsonIgnore
    public boolean getShow_reservedDirtyFlag(){
        return show_reservedDirtyFlag ;
    }

    /**
     * 获取 [RATE_PICKING_BACKORDERS]
     */
    @JsonProperty("rate_picking_backorders")
    public Integer getRate_picking_backorders(){
        return rate_picking_backorders ;
    }

    /**
     * 设置 [RATE_PICKING_BACKORDERS]
     */
    @JsonProperty("rate_picking_backorders")
    public void setRate_picking_backorders(Integer  rate_picking_backorders){
        this.rate_picking_backorders = rate_picking_backorders ;
        this.rate_picking_backordersDirtyFlag = true ;
    }

    /**
     * 获取 [RATE_PICKING_BACKORDERS]脏标记
     */
    @JsonIgnore
    public boolean getRate_picking_backordersDirtyFlag(){
        return rate_picking_backordersDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [COUNT_PICKING_BACKORDERS]
     */
    @JsonProperty("count_picking_backorders")
    public Integer getCount_picking_backorders(){
        return count_picking_backorders ;
    }

    /**
     * 设置 [COUNT_PICKING_BACKORDERS]
     */
    @JsonProperty("count_picking_backorders")
    public void setCount_picking_backorders(Integer  count_picking_backorders){
        this.count_picking_backorders = count_picking_backorders ;
        this.count_picking_backordersDirtyFlag = true ;
    }

    /**
     * 获取 [COUNT_PICKING_BACKORDERS]脏标记
     */
    @JsonIgnore
    public boolean getCount_picking_backordersDirtyFlag(){
        return count_picking_backordersDirtyFlag ;
    }

    /**
     * 获取 [COUNT_MO_TODO]
     */
    @JsonProperty("count_mo_todo")
    public Integer getCount_mo_todo(){
        return count_mo_todo ;
    }

    /**
     * 设置 [COUNT_MO_TODO]
     */
    @JsonProperty("count_mo_todo")
    public void setCount_mo_todo(Integer  count_mo_todo){
        this.count_mo_todo = count_mo_todo ;
        this.count_mo_todoDirtyFlag = true ;
    }

    /**
     * 获取 [COUNT_MO_TODO]脏标记
     */
    @JsonIgnore
    public boolean getCount_mo_todoDirtyFlag(){
        return count_mo_todoDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [SHOW_ENTIRE_PACKS]
     */
    @JsonProperty("show_entire_packs")
    public String getShow_entire_packs(){
        return show_entire_packs ;
    }

    /**
     * 设置 [SHOW_ENTIRE_PACKS]
     */
    @JsonProperty("show_entire_packs")
    public void setShow_entire_packs(String  show_entire_packs){
        this.show_entire_packs = show_entire_packs ;
        this.show_entire_packsDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_ENTIRE_PACKS]脏标记
     */
    @JsonIgnore
    public boolean getShow_entire_packsDirtyFlag(){
        return show_entire_packsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [COUNT_MO_WAITING]
     */
    @JsonProperty("count_mo_waiting")
    public Integer getCount_mo_waiting(){
        return count_mo_waiting ;
    }

    /**
     * 设置 [COUNT_MO_WAITING]
     */
    @JsonProperty("count_mo_waiting")
    public void setCount_mo_waiting(Integer  count_mo_waiting){
        this.count_mo_waiting = count_mo_waiting ;
        this.count_mo_waitingDirtyFlag = true ;
    }

    /**
     * 获取 [COUNT_MO_WAITING]脏标记
     */
    @JsonIgnore
    public boolean getCount_mo_waitingDirtyFlag(){
        return count_mo_waitingDirtyFlag ;
    }

    /**
     * 获取 [COUNT_PICKING_DRAFT]
     */
    @JsonProperty("count_picking_draft")
    public Integer getCount_picking_draft(){
        return count_picking_draft ;
    }

    /**
     * 设置 [COUNT_PICKING_DRAFT]
     */
    @JsonProperty("count_picking_draft")
    public void setCount_picking_draft(Integer  count_picking_draft){
        this.count_picking_draft = count_picking_draft ;
        this.count_picking_draftDirtyFlag = true ;
    }

    /**
     * 获取 [COUNT_PICKING_DRAFT]脏标记
     */
    @JsonIgnore
    public boolean getCount_picking_draftDirtyFlag(){
        return count_picking_draftDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [COUNT_PICKING_LATE]
     */
    @JsonProperty("count_picking_late")
    public Integer getCount_picking_late(){
        return count_picking_late ;
    }

    /**
     * 设置 [COUNT_PICKING_LATE]
     */
    @JsonProperty("count_picking_late")
    public void setCount_picking_late(Integer  count_picking_late){
        this.count_picking_late = count_picking_late ;
        this.count_picking_lateDirtyFlag = true ;
    }

    /**
     * 获取 [COUNT_PICKING_LATE]脏标记
     */
    @JsonIgnore
    public boolean getCount_picking_lateDirtyFlag(){
        return count_picking_lateDirtyFlag ;
    }

    /**
     * 获取 [COUNT_PICKING]
     */
    @JsonProperty("count_picking")
    public Integer getCount_picking(){
        return count_picking ;
    }

    /**
     * 设置 [COUNT_PICKING]
     */
    @JsonProperty("count_picking")
    public void setCount_picking(Integer  count_picking){
        this.count_picking = count_picking ;
        this.count_pickingDirtyFlag = true ;
    }

    /**
     * 获取 [COUNT_PICKING]脏标记
     */
    @JsonIgnore
    public boolean getCount_pickingDirtyFlag(){
        return count_pickingDirtyFlag ;
    }

    /**
     * 获取 [COUNT_PICKING_READY]
     */
    @JsonProperty("count_picking_ready")
    public Integer getCount_picking_ready(){
        return count_picking_ready ;
    }

    /**
     * 设置 [COUNT_PICKING_READY]
     */
    @JsonProperty("count_picking_ready")
    public void setCount_picking_ready(Integer  count_picking_ready){
        this.count_picking_ready = count_picking_ready ;
        this.count_picking_readyDirtyFlag = true ;
    }

    /**
     * 获取 [COUNT_PICKING_READY]脏标记
     */
    @JsonIgnore
    public boolean getCount_picking_readyDirtyFlag(){
        return count_picking_readyDirtyFlag ;
    }

    /**
     * 获取 [LAST_DONE_PICKING]
     */
    @JsonProperty("last_done_picking")
    public String getLast_done_picking(){
        return last_done_picking ;
    }

    /**
     * 设置 [LAST_DONE_PICKING]
     */
    @JsonProperty("last_done_picking")
    public void setLast_done_picking(String  last_done_picking){
        this.last_done_picking = last_done_picking ;
        this.last_done_pickingDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_DONE_PICKING]脏标记
     */
    @JsonIgnore
    public boolean getLast_done_pickingDirtyFlag(){
        return last_done_pickingDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [COUNT_PICKING_WAITING]
     */
    @JsonProperty("count_picking_waiting")
    public Integer getCount_picking_waiting(){
        return count_picking_waiting ;
    }

    /**
     * 设置 [COUNT_PICKING_WAITING]
     */
    @JsonProperty("count_picking_waiting")
    public void setCount_picking_waiting(Integer  count_picking_waiting){
        this.count_picking_waiting = count_picking_waiting ;
        this.count_picking_waitingDirtyFlag = true ;
    }

    /**
     * 获取 [COUNT_PICKING_WAITING]脏标记
     */
    @JsonIgnore
    public boolean getCount_picking_waitingDirtyFlag(){
        return count_picking_waitingDirtyFlag ;
    }

    /**
     * 获取 [CODE]
     */
    @JsonProperty("code")
    public String getCode(){
        return code ;
    }

    /**
     * 设置 [CODE]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [CODE]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return codeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [RATE_PICKING_LATE]
     */
    @JsonProperty("rate_picking_late")
    public Integer getRate_picking_late(){
        return rate_picking_late ;
    }

    /**
     * 设置 [RATE_PICKING_LATE]
     */
    @JsonProperty("rate_picking_late")
    public void setRate_picking_late(Integer  rate_picking_late){
        this.rate_picking_late = rate_picking_late ;
        this.rate_picking_lateDirtyFlag = true ;
    }

    /**
     * 获取 [RATE_PICKING_LATE]脏标记
     */
    @JsonIgnore
    public boolean getRate_picking_lateDirtyFlag(){
        return rate_picking_lateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [COUNT_MO_LATE]
     */
    @JsonProperty("count_mo_late")
    public Integer getCount_mo_late(){
        return count_mo_late ;
    }

    /**
     * 设置 [COUNT_MO_LATE]
     */
    @JsonProperty("count_mo_late")
    public void setCount_mo_late(Integer  count_mo_late){
        this.count_mo_late = count_mo_late ;
        this.count_mo_lateDirtyFlag = true ;
    }

    /**
     * 获取 [COUNT_MO_LATE]脏标记
     */
    @JsonIgnore
    public boolean getCount_mo_lateDirtyFlag(){
        return count_mo_lateDirtyFlag ;
    }

    /**
     * 获取 [USE_EXISTING_LOTS]
     */
    @JsonProperty("use_existing_lots")
    public String getUse_existing_lots(){
        return use_existing_lots ;
    }

    /**
     * 设置 [USE_EXISTING_LOTS]
     */
    @JsonProperty("use_existing_lots")
    public void setUse_existing_lots(String  use_existing_lots){
        this.use_existing_lots = use_existing_lots ;
        this.use_existing_lotsDirtyFlag = true ;
    }

    /**
     * 获取 [USE_EXISTING_LOTS]脏标记
     */
    @JsonIgnore
    public boolean getUse_existing_lotsDirtyFlag(){
        return use_existing_lotsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_LOCATION_SRC_ID_TEXT]
     */
    @JsonProperty("default_location_src_id_text")
    public String getDefault_location_src_id_text(){
        return default_location_src_id_text ;
    }

    /**
     * 设置 [DEFAULT_LOCATION_SRC_ID_TEXT]
     */
    @JsonProperty("default_location_src_id_text")
    public void setDefault_location_src_id_text(String  default_location_src_id_text){
        this.default_location_src_id_text = default_location_src_id_text ;
        this.default_location_src_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_LOCATION_SRC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDefault_location_src_id_textDirtyFlag(){
        return default_location_src_id_textDirtyFlag ;
    }

    /**
     * 获取 [RETURN_PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("return_picking_type_id_text")
    public String getReturn_picking_type_id_text(){
        return return_picking_type_id_text ;
    }

    /**
     * 设置 [RETURN_PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("return_picking_type_id_text")
    public void setReturn_picking_type_id_text(String  return_picking_type_id_text){
        this.return_picking_type_id_text = return_picking_type_id_text ;
        this.return_picking_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RETURN_PICKING_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getReturn_picking_type_id_textDirtyFlag(){
        return return_picking_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return warehouse_id_text ;
    }

    /**
     * 设置 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return warehouse_id_textDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("default_location_dest_id_text")
    public String getDefault_location_dest_id_text(){
        return default_location_dest_id_text ;
    }

    /**
     * 设置 [DEFAULT_LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("default_location_dest_id_text")
    public void setDefault_location_dest_id_text(String  default_location_dest_id_text){
        this.default_location_dest_id_text = default_location_dest_id_text ;
        this.default_location_dest_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_LOCATION_DEST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDefault_location_dest_id_textDirtyFlag(){
        return default_location_dest_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_LOCATION_DEST_ID]
     */
    @JsonProperty("default_location_dest_id")
    public Integer getDefault_location_dest_id(){
        return default_location_dest_id ;
    }

    /**
     * 设置 [DEFAULT_LOCATION_DEST_ID]
     */
    @JsonProperty("default_location_dest_id")
    public void setDefault_location_dest_id(Integer  default_location_dest_id){
        this.default_location_dest_id = default_location_dest_id ;
        this.default_location_dest_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_LOCATION_DEST_ID]脏标记
     */
    @JsonIgnore
    public boolean getDefault_location_dest_idDirtyFlag(){
        return default_location_dest_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return warehouse_id ;
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return warehouse_idDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_LOCATION_SRC_ID]
     */
    @JsonProperty("default_location_src_id")
    public Integer getDefault_location_src_id(){
        return default_location_src_id ;
    }

    /**
     * 设置 [DEFAULT_LOCATION_SRC_ID]
     */
    @JsonProperty("default_location_src_id")
    public void setDefault_location_src_id(Integer  default_location_src_id){
        this.default_location_src_id = default_location_src_id ;
        this.default_location_src_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_LOCATION_SRC_ID]脏标记
     */
    @JsonIgnore
    public boolean getDefault_location_src_idDirtyFlag(){
        return default_location_src_idDirtyFlag ;
    }

    /**
     * 获取 [RETURN_PICKING_TYPE_ID]
     */
    @JsonProperty("return_picking_type_id")
    public Integer getReturn_picking_type_id(){
        return return_picking_type_id ;
    }

    /**
     * 设置 [RETURN_PICKING_TYPE_ID]
     */
    @JsonProperty("return_picking_type_id")
    public void setReturn_picking_type_id(Integer  return_picking_type_id){
        this.return_picking_type_id = return_picking_type_id ;
        this.return_picking_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [RETURN_PICKING_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getReturn_picking_type_idDirtyFlag(){
        return return_picking_type_idDirtyFlag ;
    }



    public Stock_picking_type toDO() {
        Stock_picking_type srfdomain = new Stock_picking_type();
        if(getShow_operationsDirtyFlag())
            srfdomain.setShow_operations(show_operations);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getSequence_idDirtyFlag())
            srfdomain.setSequence_id(sequence_id);
        if(getUse_create_lotsDirtyFlag())
            srfdomain.setUse_create_lots(use_create_lots);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getBarcodeDirtyFlag())
            srfdomain.setBarcode(barcode);
        if(getShow_reservedDirtyFlag())
            srfdomain.setShow_reserved(show_reserved);
        if(getRate_picking_backordersDirtyFlag())
            srfdomain.setRate_picking_backorders(rate_picking_backorders);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getCount_picking_backordersDirtyFlag())
            srfdomain.setCount_picking_backorders(count_picking_backorders);
        if(getCount_mo_todoDirtyFlag())
            srfdomain.setCount_mo_todo(count_mo_todo);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getShow_entire_packsDirtyFlag())
            srfdomain.setShow_entire_packs(show_entire_packs);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCount_mo_waitingDirtyFlag())
            srfdomain.setCount_mo_waiting(count_mo_waiting);
        if(getCount_picking_draftDirtyFlag())
            srfdomain.setCount_picking_draft(count_picking_draft);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getCount_picking_lateDirtyFlag())
            srfdomain.setCount_picking_late(count_picking_late);
        if(getCount_pickingDirtyFlag())
            srfdomain.setCount_picking(count_picking);
        if(getCount_picking_readyDirtyFlag())
            srfdomain.setCount_picking_ready(count_picking_ready);
        if(getLast_done_pickingDirtyFlag())
            srfdomain.setLast_done_picking(last_done_picking);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCount_picking_waitingDirtyFlag())
            srfdomain.setCount_picking_waiting(count_picking_waiting);
        if(getCodeDirtyFlag())
            srfdomain.setCode(code);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getRate_picking_lateDirtyFlag())
            srfdomain.setRate_picking_late(rate_picking_late);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCount_mo_lateDirtyFlag())
            srfdomain.setCount_mo_late(count_mo_late);
        if(getUse_existing_lotsDirtyFlag())
            srfdomain.setUse_existing_lots(use_existing_lots);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getDefault_location_src_id_textDirtyFlag())
            srfdomain.setDefault_location_src_id_text(default_location_src_id_text);
        if(getReturn_picking_type_id_textDirtyFlag())
            srfdomain.setReturn_picking_type_id_text(return_picking_type_id_text);
        if(getWarehouse_id_textDirtyFlag())
            srfdomain.setWarehouse_id_text(warehouse_id_text);
        if(getDefault_location_dest_id_textDirtyFlag())
            srfdomain.setDefault_location_dest_id_text(default_location_dest_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getDefault_location_dest_idDirtyFlag())
            srfdomain.setDefault_location_dest_id(default_location_dest_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWarehouse_idDirtyFlag())
            srfdomain.setWarehouse_id(warehouse_id);
        if(getDefault_location_src_idDirtyFlag())
            srfdomain.setDefault_location_src_id(default_location_src_id);
        if(getReturn_picking_type_idDirtyFlag())
            srfdomain.setReturn_picking_type_id(return_picking_type_id);

        return srfdomain;
    }

    public void fromDO(Stock_picking_type srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getShow_operationsDirtyFlag())
            this.setShow_operations(srfdomain.getShow_operations());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getSequence_idDirtyFlag())
            this.setSequence_id(srfdomain.getSequence_id());
        if(srfdomain.getUse_create_lotsDirtyFlag())
            this.setUse_create_lots(srfdomain.getUse_create_lots());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getBarcodeDirtyFlag())
            this.setBarcode(srfdomain.getBarcode());
        if(srfdomain.getShow_reservedDirtyFlag())
            this.setShow_reserved(srfdomain.getShow_reserved());
        if(srfdomain.getRate_picking_backordersDirtyFlag())
            this.setRate_picking_backorders(srfdomain.getRate_picking_backorders());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getCount_picking_backordersDirtyFlag())
            this.setCount_picking_backorders(srfdomain.getCount_picking_backorders());
        if(srfdomain.getCount_mo_todoDirtyFlag())
            this.setCount_mo_todo(srfdomain.getCount_mo_todo());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getShow_entire_packsDirtyFlag())
            this.setShow_entire_packs(srfdomain.getShow_entire_packs());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCount_mo_waitingDirtyFlag())
            this.setCount_mo_waiting(srfdomain.getCount_mo_waiting());
        if(srfdomain.getCount_picking_draftDirtyFlag())
            this.setCount_picking_draft(srfdomain.getCount_picking_draft());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getCount_picking_lateDirtyFlag())
            this.setCount_picking_late(srfdomain.getCount_picking_late());
        if(srfdomain.getCount_pickingDirtyFlag())
            this.setCount_picking(srfdomain.getCount_picking());
        if(srfdomain.getCount_picking_readyDirtyFlag())
            this.setCount_picking_ready(srfdomain.getCount_picking_ready());
        if(srfdomain.getLast_done_pickingDirtyFlag())
            this.setLast_done_picking(srfdomain.getLast_done_picking());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCount_picking_waitingDirtyFlag())
            this.setCount_picking_waiting(srfdomain.getCount_picking_waiting());
        if(srfdomain.getCodeDirtyFlag())
            this.setCode(srfdomain.getCode());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getRate_picking_lateDirtyFlag())
            this.setRate_picking_late(srfdomain.getRate_picking_late());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCount_mo_lateDirtyFlag())
            this.setCount_mo_late(srfdomain.getCount_mo_late());
        if(srfdomain.getUse_existing_lotsDirtyFlag())
            this.setUse_existing_lots(srfdomain.getUse_existing_lots());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getDefault_location_src_id_textDirtyFlag())
            this.setDefault_location_src_id_text(srfdomain.getDefault_location_src_id_text());
        if(srfdomain.getReturn_picking_type_id_textDirtyFlag())
            this.setReturn_picking_type_id_text(srfdomain.getReturn_picking_type_id_text());
        if(srfdomain.getWarehouse_id_textDirtyFlag())
            this.setWarehouse_id_text(srfdomain.getWarehouse_id_text());
        if(srfdomain.getDefault_location_dest_id_textDirtyFlag())
            this.setDefault_location_dest_id_text(srfdomain.getDefault_location_dest_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getDefault_location_dest_idDirtyFlag())
            this.setDefault_location_dest_id(srfdomain.getDefault_location_dest_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWarehouse_idDirtyFlag())
            this.setWarehouse_id(srfdomain.getWarehouse_id());
        if(srfdomain.getDefault_location_src_idDirtyFlag())
            this.setDefault_location_src_id(srfdomain.getDefault_location_src_id());
        if(srfdomain.getReturn_picking_type_idDirtyFlag())
            this.setReturn_picking_type_id(srfdomain.getReturn_picking_type_id());

    }

    public List<Stock_picking_typeDTO> fromDOPage(List<Stock_picking_type> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_picking_typeDTO> dtos=new ArrayList<Stock_picking_typeDTO>();
        for(Stock_picking_type domain : poPage) {
            Stock_picking_typeDTO dto = new Stock_picking_typeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

