package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_scrap.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_scrapDTO]
 */
public class Stock_scrapDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_scrapWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DATE_EXPECTED]
     *
     */
    @Stock_scrapDate_expectedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_expected;

    @JsonIgnore
    private boolean date_expectedDirtyFlag;

    /**
     * 属性 [SCRAP_QTY]
     *
     */
    @Stock_scrapScrap_qtyDefault(info = "默认规则")
    private Double scrap_qty;

    @JsonIgnore
    private boolean scrap_qtyDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_scrap__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ORIGIN]
     *
     */
    @Stock_scrapOriginDefault(info = "默认规则")
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Stock_scrapStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_scrapIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_scrapCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_scrapDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Stock_scrapNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [WORKORDER_ID_TEXT]
     *
     */
    @Stock_scrapWorkorder_id_textDefault(info = "默认规则")
    private String workorder_id_text;

    @JsonIgnore
    private boolean workorder_id_textDirtyFlag;

    /**
     * 属性 [SCRAP_LOCATION_ID_TEXT]
     *
     */
    @Stock_scrapScrap_location_id_textDefault(info = "默认规则")
    private String scrap_location_id_text;

    @JsonIgnore
    private boolean scrap_location_id_textDirtyFlag;

    /**
     * 属性 [LOT_ID_TEXT]
     *
     */
    @Stock_scrapLot_id_textDefault(info = "默认规则")
    private String lot_id_text;

    @JsonIgnore
    private boolean lot_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_scrapWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MOVE_ID_TEXT]
     *
     */
    @Stock_scrapMove_id_textDefault(info = "默认规则")
    private String move_id_text;

    @JsonIgnore
    private boolean move_id_textDirtyFlag;

    /**
     * 属性 [PICKING_ID_TEXT]
     *
     */
    @Stock_scrapPicking_id_textDefault(info = "默认规则")
    private String picking_id_text;

    @JsonIgnore
    private boolean picking_id_textDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID_TEXT]
     *
     */
    @Stock_scrapProduction_id_textDefault(info = "默认规则")
    private String production_id_text;

    @JsonIgnore
    private boolean production_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Stock_scrapLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @Stock_scrapProduct_uom_id_textDefault(info = "默认规则")
    private String product_uom_id_text;

    @JsonIgnore
    private boolean product_uom_id_textDirtyFlag;

    /**
     * 属性 [TRACKING]
     *
     */
    @Stock_scrapTrackingDefault(info = "默认规则")
    private String tracking;

    @JsonIgnore
    private boolean trackingDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Stock_scrapProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [PACKAGE_ID_TEXT]
     *
     */
    @Stock_scrapPackage_id_textDefault(info = "默认规则")
    private String package_id_text;

    @JsonIgnore
    private boolean package_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_scrapCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [OWNER_ID_TEXT]
     *
     */
    @Stock_scrapOwner_id_textDefault(info = "默认规则")
    private String owner_id_text;

    @JsonIgnore
    private boolean owner_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_scrapCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [LOT_ID]
     *
     */
    @Stock_scrapLot_idDefault(info = "默认规则")
    private Integer lot_id;

    @JsonIgnore
    private boolean lot_idDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @Stock_scrapProduction_idDefault(info = "默认规则")
    private Integer production_id;

    @JsonIgnore
    private boolean production_idDirtyFlag;

    /**
     * 属性 [PICKING_ID]
     *
     */
    @Stock_scrapPicking_idDefault(info = "默认规则")
    private Integer picking_id;

    @JsonIgnore
    private boolean picking_idDirtyFlag;

    /**
     * 属性 [SCRAP_LOCATION_ID]
     *
     */
    @Stock_scrapScrap_location_idDefault(info = "默认规则")
    private Integer scrap_location_id;

    @JsonIgnore
    private boolean scrap_location_idDirtyFlag;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @Stock_scrapMove_idDefault(info = "默认规则")
    private Integer move_id;

    @JsonIgnore
    private boolean move_idDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Stock_scrapLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_scrapWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [WORKORDER_ID]
     *
     */
    @Stock_scrapWorkorder_idDefault(info = "默认规则")
    private Integer workorder_id;

    @JsonIgnore
    private boolean workorder_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @Stock_scrapProduct_uom_idDefault(info = "默认规则")
    private Integer product_uom_id;

    @JsonIgnore
    private boolean product_uom_idDirtyFlag;

    /**
     * 属性 [PACKAGE_ID]
     *
     */
    @Stock_scrapPackage_idDefault(info = "默认规则")
    private Integer package_id;

    @JsonIgnore
    private boolean package_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Stock_scrapProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [OWNER_ID]
     *
     */
    @Stock_scrapOwner_idDefault(info = "默认规则")
    private Integer owner_id;

    @JsonIgnore
    private boolean owner_idDirtyFlag;


    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DATE_EXPECTED]
     */
    @JsonProperty("date_expected")
    public Timestamp getDate_expected(){
        return date_expected ;
    }

    /**
     * 设置 [DATE_EXPECTED]
     */
    @JsonProperty("date_expected")
    public void setDate_expected(Timestamp  date_expected){
        this.date_expected = date_expected ;
        this.date_expectedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_EXPECTED]脏标记
     */
    @JsonIgnore
    public boolean getDate_expectedDirtyFlag(){
        return date_expectedDirtyFlag ;
    }

    /**
     * 获取 [SCRAP_QTY]
     */
    @JsonProperty("scrap_qty")
    public Double getScrap_qty(){
        return scrap_qty ;
    }

    /**
     * 设置 [SCRAP_QTY]
     */
    @JsonProperty("scrap_qty")
    public void setScrap_qty(Double  scrap_qty){
        this.scrap_qty = scrap_qty ;
        this.scrap_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAP_QTY]脏标记
     */
    @JsonIgnore
    public boolean getScrap_qtyDirtyFlag(){
        return scrap_qtyDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return origin ;
    }

    /**
     * 设置 [ORIGIN]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return originDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_ID_TEXT]
     */
    @JsonProperty("workorder_id_text")
    public String getWorkorder_id_text(){
        return workorder_id_text ;
    }

    /**
     * 设置 [WORKORDER_ID_TEXT]
     */
    @JsonProperty("workorder_id_text")
    public void setWorkorder_id_text(String  workorder_id_text){
        this.workorder_id_text = workorder_id_text ;
        this.workorder_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_id_textDirtyFlag(){
        return workorder_id_textDirtyFlag ;
    }

    /**
     * 获取 [SCRAP_LOCATION_ID_TEXT]
     */
    @JsonProperty("scrap_location_id_text")
    public String getScrap_location_id_text(){
        return scrap_location_id_text ;
    }

    /**
     * 设置 [SCRAP_LOCATION_ID_TEXT]
     */
    @JsonProperty("scrap_location_id_text")
    public void setScrap_location_id_text(String  scrap_location_id_text){
        this.scrap_location_id_text = scrap_location_id_text ;
        this.scrap_location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAP_LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getScrap_location_id_textDirtyFlag(){
        return scrap_location_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOT_ID_TEXT]
     */
    @JsonProperty("lot_id_text")
    public String getLot_id_text(){
        return lot_id_text ;
    }

    /**
     * 设置 [LOT_ID_TEXT]
     */
    @JsonProperty("lot_id_text")
    public void setLot_id_text(String  lot_id_text){
        this.lot_id_text = lot_id_text ;
        this.lot_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLot_id_textDirtyFlag(){
        return lot_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID_TEXT]
     */
    @JsonProperty("move_id_text")
    public String getMove_id_text(){
        return move_id_text ;
    }

    /**
     * 设置 [MOVE_ID_TEXT]
     */
    @JsonProperty("move_id_text")
    public void setMove_id_text(String  move_id_text){
        this.move_id_text = move_id_text ;
        this.move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMove_id_textDirtyFlag(){
        return move_id_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_ID_TEXT]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return picking_id_text ;
    }

    /**
     * 设置 [PICKING_ID_TEXT]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return picking_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID_TEXT]
     */
    @JsonProperty("production_id_text")
    public String getProduction_id_text(){
        return production_id_text ;
    }

    /**
     * 设置 [PRODUCTION_ID_TEXT]
     */
    @JsonProperty("production_id_text")
    public void setProduction_id_text(String  production_id_text){
        this.production_id_text = production_id_text ;
        this.production_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduction_id_textDirtyFlag(){
        return production_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return product_uom_id_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return product_uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [TRACKING]
     */
    @JsonProperty("tracking")
    public String getTracking(){
        return tracking ;
    }

    /**
     * 设置 [TRACKING]
     */
    @JsonProperty("tracking")
    public void setTracking(String  tracking){
        this.tracking = tracking ;
        this.trackingDirtyFlag = true ;
    }

    /**
     * 获取 [TRACKING]脏标记
     */
    @JsonIgnore
    public boolean getTrackingDirtyFlag(){
        return trackingDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_ID_TEXT]
     */
    @JsonProperty("package_id_text")
    public String getPackage_id_text(){
        return package_id_text ;
    }

    /**
     * 设置 [PACKAGE_ID_TEXT]
     */
    @JsonProperty("package_id_text")
    public void setPackage_id_text(String  package_id_text){
        this.package_id_text = package_id_text ;
        this.package_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPackage_id_textDirtyFlag(){
        return package_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [OWNER_ID_TEXT]
     */
    @JsonProperty("owner_id_text")
    public String getOwner_id_text(){
        return owner_id_text ;
    }

    /**
     * 设置 [OWNER_ID_TEXT]
     */
    @JsonProperty("owner_id_text")
    public void setOwner_id_text(String  owner_id_text){
        this.owner_id_text = owner_id_text ;
        this.owner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOwner_id_textDirtyFlag(){
        return owner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [LOT_ID]
     */
    @JsonProperty("lot_id")
    public Integer getLot_id(){
        return lot_id ;
    }

    /**
     * 设置 [LOT_ID]
     */
    @JsonProperty("lot_id")
    public void setLot_id(Integer  lot_id){
        this.lot_id = lot_id ;
        this.lot_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_ID]脏标记
     */
    @JsonIgnore
    public boolean getLot_idDirtyFlag(){
        return lot_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public Integer getProduction_id(){
        return production_id ;
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public void setProduction_id(Integer  production_id){
        this.production_id = production_id ;
        this.production_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduction_idDirtyFlag(){
        return production_idDirtyFlag ;
    }

    /**
     * 获取 [PICKING_ID]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return picking_id ;
    }

    /**
     * 设置 [PICKING_ID]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return picking_idDirtyFlag ;
    }

    /**
     * 获取 [SCRAP_LOCATION_ID]
     */
    @JsonProperty("scrap_location_id")
    public Integer getScrap_location_id(){
        return scrap_location_id ;
    }

    /**
     * 设置 [SCRAP_LOCATION_ID]
     */
    @JsonProperty("scrap_location_id")
    public void setScrap_location_id(Integer  scrap_location_id){
        this.scrap_location_id = scrap_location_id ;
        this.scrap_location_idDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAP_LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getScrap_location_idDirtyFlag(){
        return scrap_location_idDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return move_id ;
    }

    /**
     * 设置 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return move_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_ID]
     */
    @JsonProperty("workorder_id")
    public Integer getWorkorder_id(){
        return workorder_id ;
    }

    /**
     * 设置 [WORKORDER_ID]
     */
    @JsonProperty("workorder_id")
    public void setWorkorder_id(Integer  workorder_id){
        this.workorder_id = workorder_id ;
        this.workorder_idDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idDirtyFlag(){
        return workorder_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return product_uom_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return product_uom_idDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_ID]
     */
    @JsonProperty("package_id")
    public Integer getPackage_id(){
        return package_id ;
    }

    /**
     * 设置 [PACKAGE_ID]
     */
    @JsonProperty("package_id")
    public void setPackage_id(Integer  package_id){
        this.package_id = package_id ;
        this.package_idDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPackage_idDirtyFlag(){
        return package_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [OWNER_ID]
     */
    @JsonProperty("owner_id")
    public Integer getOwner_id(){
        return owner_id ;
    }

    /**
     * 设置 [OWNER_ID]
     */
    @JsonProperty("owner_id")
    public void setOwner_id(Integer  owner_id){
        this.owner_id = owner_id ;
        this.owner_idDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOwner_idDirtyFlag(){
        return owner_idDirtyFlag ;
    }



    public Stock_scrap toDO() {
        Stock_scrap srfdomain = new Stock_scrap();
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDate_expectedDirtyFlag())
            srfdomain.setDate_expected(date_expected);
        if(getScrap_qtyDirtyFlag())
            srfdomain.setScrap_qty(scrap_qty);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getOriginDirtyFlag())
            srfdomain.setOrigin(origin);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getWorkorder_id_textDirtyFlag())
            srfdomain.setWorkorder_id_text(workorder_id_text);
        if(getScrap_location_id_textDirtyFlag())
            srfdomain.setScrap_location_id_text(scrap_location_id_text);
        if(getLot_id_textDirtyFlag())
            srfdomain.setLot_id_text(lot_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getMove_id_textDirtyFlag())
            srfdomain.setMove_id_text(move_id_text);
        if(getPicking_id_textDirtyFlag())
            srfdomain.setPicking_id_text(picking_id_text);
        if(getProduction_id_textDirtyFlag())
            srfdomain.setProduction_id_text(production_id_text);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getProduct_uom_id_textDirtyFlag())
            srfdomain.setProduct_uom_id_text(product_uom_id_text);
        if(getTrackingDirtyFlag())
            srfdomain.setTracking(tracking);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getPackage_id_textDirtyFlag())
            srfdomain.setPackage_id_text(package_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getOwner_id_textDirtyFlag())
            srfdomain.setOwner_id_text(owner_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getLot_idDirtyFlag())
            srfdomain.setLot_id(lot_id);
        if(getProduction_idDirtyFlag())
            srfdomain.setProduction_id(production_id);
        if(getPicking_idDirtyFlag())
            srfdomain.setPicking_id(picking_id);
        if(getScrap_location_idDirtyFlag())
            srfdomain.setScrap_location_id(scrap_location_id);
        if(getMove_idDirtyFlag())
            srfdomain.setMove_id(move_id);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getWorkorder_idDirtyFlag())
            srfdomain.setWorkorder_id(workorder_id);
        if(getProduct_uom_idDirtyFlag())
            srfdomain.setProduct_uom_id(product_uom_id);
        if(getPackage_idDirtyFlag())
            srfdomain.setPackage_id(package_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getOwner_idDirtyFlag())
            srfdomain.setOwner_id(owner_id);

        return srfdomain;
    }

    public void fromDO(Stock_scrap srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDate_expectedDirtyFlag())
            this.setDate_expected(srfdomain.getDate_expected());
        if(srfdomain.getScrap_qtyDirtyFlag())
            this.setScrap_qty(srfdomain.getScrap_qty());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getOriginDirtyFlag())
            this.setOrigin(srfdomain.getOrigin());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getWorkorder_id_textDirtyFlag())
            this.setWorkorder_id_text(srfdomain.getWorkorder_id_text());
        if(srfdomain.getScrap_location_id_textDirtyFlag())
            this.setScrap_location_id_text(srfdomain.getScrap_location_id_text());
        if(srfdomain.getLot_id_textDirtyFlag())
            this.setLot_id_text(srfdomain.getLot_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getMove_id_textDirtyFlag())
            this.setMove_id_text(srfdomain.getMove_id_text());
        if(srfdomain.getPicking_id_textDirtyFlag())
            this.setPicking_id_text(srfdomain.getPicking_id_text());
        if(srfdomain.getProduction_id_textDirtyFlag())
            this.setProduction_id_text(srfdomain.getProduction_id_text());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getProduct_uom_id_textDirtyFlag())
            this.setProduct_uom_id_text(srfdomain.getProduct_uom_id_text());
        if(srfdomain.getTrackingDirtyFlag())
            this.setTracking(srfdomain.getTracking());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getPackage_id_textDirtyFlag())
            this.setPackage_id_text(srfdomain.getPackage_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getOwner_id_textDirtyFlag())
            this.setOwner_id_text(srfdomain.getOwner_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getLot_idDirtyFlag())
            this.setLot_id(srfdomain.getLot_id());
        if(srfdomain.getProduction_idDirtyFlag())
            this.setProduction_id(srfdomain.getProduction_id());
        if(srfdomain.getPicking_idDirtyFlag())
            this.setPicking_id(srfdomain.getPicking_id());
        if(srfdomain.getScrap_location_idDirtyFlag())
            this.setScrap_location_id(srfdomain.getScrap_location_id());
        if(srfdomain.getMove_idDirtyFlag())
            this.setMove_id(srfdomain.getMove_id());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getWorkorder_idDirtyFlag())
            this.setWorkorder_id(srfdomain.getWorkorder_id());
        if(srfdomain.getProduct_uom_idDirtyFlag())
            this.setProduct_uom_id(srfdomain.getProduct_uom_id());
        if(srfdomain.getPackage_idDirtyFlag())
            this.setPackage_id(srfdomain.getPackage_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getOwner_idDirtyFlag())
            this.setOwner_id(srfdomain.getOwner_id());

    }

    public List<Stock_scrapDTO> fromDOPage(List<Stock_scrap> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_scrapDTO> dtos=new ArrayList<Stock_scrapDTO>();
        for(Stock_scrap domain : poPage) {
            Stock_scrapDTO dto = new Stock_scrapDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

