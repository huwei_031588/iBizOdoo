package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_move.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_moveDTO]
 */
public class Stock_moveDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RETURNED_MOVE_IDS]
     *
     */
    @Stock_moveReturned_move_idsDefault(info = "默认规则")
    private String returned_move_ids;

    @JsonIgnore
    private boolean returned_move_idsDirtyFlag;

    /**
     * 属性 [ADDITIONAL]
     *
     */
    @Stock_moveAdditionalDefault(info = "默认规则")
    private String additional;

    @JsonIgnore
    private boolean additionalDirtyFlag;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @Stock_movePrice_unitDefault(info = "默认规则")
    private Double price_unit;

    @JsonIgnore
    private boolean price_unitDirtyFlag;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @Stock_moveGroup_idDefault(info = "默认规则")
    private Integer group_id;

    @JsonIgnore
    private boolean group_idDirtyFlag;

    /**
     * 属性 [SCRAP_IDS]
     *
     */
    @Stock_moveScrap_idsDefault(info = "默认规则")
    private String scrap_ids;

    @JsonIgnore
    private boolean scrap_idsDirtyFlag;

    /**
     * 属性 [ACCOUNT_MOVE_IDS]
     *
     */
    @Stock_moveAccount_move_idsDefault(info = "默认规则")
    private String account_move_ids;

    @JsonIgnore
    private boolean account_move_idsDirtyFlag;

    /**
     * 属性 [MOVE_DEST_IDS]
     *
     */
    @Stock_moveMove_dest_idsDefault(info = "默认规则")
    private String move_dest_ids;

    @JsonIgnore
    private boolean move_dest_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_moveIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [TO_REFUND]
     *
     */
    @Stock_moveTo_refundDefault(info = "默认规则")
    private String to_refund;

    @JsonIgnore
    private boolean to_refundDirtyFlag;

    /**
     * 属性 [UNIT_FACTOR]
     *
     */
    @Stock_moveUnit_factorDefault(info = "默认规则")
    private Double unit_factor;

    @JsonIgnore
    private boolean unit_factorDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_move__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [AVAILABILITY]
     *
     */
    @Stock_moveAvailabilityDefault(info = "默认规则")
    private Double availability;

    @JsonIgnore
    private boolean availabilityDirtyFlag;

    /**
     * 属性 [REMAINING_QTY]
     *
     */
    @Stock_moveRemaining_qtyDefault(info = "默认规则")
    private Double remaining_qty;

    @JsonIgnore
    private boolean remaining_qtyDirtyFlag;

    /**
     * 属性 [PROPAGATE]
     *
     */
    @Stock_movePropagateDefault(info = "默认规则")
    private String propagate;

    @JsonIgnore
    private boolean propagateDirtyFlag;

    /**
     * 属性 [REFERENCE]
     *
     */
    @Stock_moveReferenceDefault(info = "默认规则")
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;

    /**
     * 属性 [ACTIVE_MOVE_LINE_IDS]
     *
     */
    @Stock_moveActive_move_line_idsDefault(info = "默认规则")
    private String active_move_line_ids;

    @JsonIgnore
    private boolean active_move_line_idsDirtyFlag;

    /**
     * 属性 [MOVE_LINE_NOSUGGEST_IDS]
     *
     */
    @Stock_moveMove_line_nosuggest_idsDefault(info = "默认规则")
    private String move_line_nosuggest_ids;

    @JsonIgnore
    private boolean move_line_nosuggest_idsDirtyFlag;

    /**
     * 属性 [SHOW_DETAILS_VISIBLE]
     *
     */
    @Stock_moveShow_details_visibleDefault(info = "默认规则")
    private String show_details_visible;

    @JsonIgnore
    private boolean show_details_visibleDirtyFlag;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @Stock_moveProduct_qtyDefault(info = "默认规则")
    private Double product_qty;

    @JsonIgnore
    private boolean product_qtyDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_moveWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ORIGIN]
     *
     */
    @Stock_moveOriginDefault(info = "默认规则")
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @Stock_moveProduct_uom_qtyDefault(info = "默认规则")
    private Double product_uom_qty;

    @JsonIgnore
    private boolean product_uom_qtyDirtyFlag;

    /**
     * 属性 [NEEDS_LOTS]
     *
     */
    @Stock_moveNeeds_lotsDefault(info = "默认规则")
    private String needs_lots;

    @JsonIgnore
    private boolean needs_lotsDirtyFlag;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Stock_movePriorityDefault(info = "默认规则")
    private String priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Stock_moveDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [REMAINING_VALUE]
     *
     */
    @Stock_moveRemaining_valueDefault(info = "默认规则")
    private Double remaining_value;

    @JsonIgnore
    private boolean remaining_valueDirtyFlag;

    /**
     * 属性 [ORDER_FINISHED_LOT_IDS]
     *
     */
    @Stock_moveOrder_finished_lot_idsDefault(info = "默认规则")
    private String order_finished_lot_ids;

    @JsonIgnore
    private boolean order_finished_lot_idsDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Stock_moveSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_moveCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MOVE_ORIG_IDS]
     *
     */
    @Stock_moveMove_orig_idsDefault(info = "默认规则")
    private String move_orig_ids;

    @JsonIgnore
    private boolean move_orig_idsDirtyFlag;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @Stock_moveMove_line_idsDefault(info = "默认规则")
    private String move_line_ids;

    @JsonIgnore
    private boolean move_line_idsDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Stock_moveNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [RESERVED_AVAILABILITY]
     *
     */
    @Stock_moveReserved_availabilityDefault(info = "默认规则")
    private Double reserved_availability;

    @JsonIgnore
    private boolean reserved_availabilityDirtyFlag;

    /**
     * 属性 [IS_INITIAL_DEMAND_EDITABLE]
     *
     */
    @Stock_moveIs_initial_demand_editableDefault(info = "默认规则")
    private String is_initial_demand_editable;

    @JsonIgnore
    private boolean is_initial_demand_editableDirtyFlag;

    /**
     * 属性 [QUANTITY_DONE]
     *
     */
    @Stock_moveQuantity_doneDefault(info = "默认规则")
    private Double quantity_done;

    @JsonIgnore
    private boolean quantity_doneDirtyFlag;

    /**
     * 属性 [IS_LOCKED]
     *
     */
    @Stock_moveIs_lockedDefault(info = "默认规则")
    private String is_locked;

    @JsonIgnore
    private boolean is_lockedDirtyFlag;

    /**
     * 属性 [STRING_AVAILABILITY_INFO]
     *
     */
    @Stock_moveString_availability_infoDefault(info = "默认规则")
    private String string_availability_info;

    @JsonIgnore
    private boolean string_availability_infoDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_moveDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [SHOW_RESERVED_AVAILABILITY]
     *
     */
    @Stock_moveShow_reserved_availabilityDefault(info = "默认规则")
    private String show_reserved_availability;

    @JsonIgnore
    private boolean show_reserved_availabilityDirtyFlag;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @Stock_moveRoute_idsDefault(info = "默认规则")
    private String route_ids;

    @JsonIgnore
    private boolean route_idsDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Stock_moveStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Stock_moveNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [VALUE]
     *
     */
    @Stock_moveValueDefault(info = "默认规则")
    private Double value;

    @JsonIgnore
    private boolean valueDirtyFlag;

    /**
     * 属性 [FINISHED_LOTS_EXIST]
     *
     */
    @Stock_moveFinished_lots_existDefault(info = "默认规则")
    private String finished_lots_exist;

    @JsonIgnore
    private boolean finished_lots_existDirtyFlag;

    /**
     * 属性 [IS_DONE]
     *
     */
    @Stock_moveIs_doneDefault(info = "默认规则")
    private String is_done;

    @JsonIgnore
    private boolean is_doneDirtyFlag;

    /**
     * 属性 [IS_QUANTITY_DONE_EDITABLE]
     *
     */
    @Stock_moveIs_quantity_done_editableDefault(info = "默认规则")
    private String is_quantity_done_editable;

    @JsonIgnore
    private boolean is_quantity_done_editableDirtyFlag;

    /**
     * 属性 [HAS_MOVE_LINES]
     *
     */
    @Stock_moveHas_move_linesDefault(info = "默认规则")
    private String has_move_lines;

    @JsonIgnore
    private boolean has_move_linesDirtyFlag;

    /**
     * 属性 [DATE_EXPECTED]
     *
     */
    @Stock_moveDate_expectedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_expected;

    @JsonIgnore
    private boolean date_expectedDirtyFlag;

    /**
     * 属性 [PROCURE_METHOD]
     *
     */
    @Stock_moveProcure_methodDefault(info = "默认规则")
    private String procure_method;

    @JsonIgnore
    private boolean procure_methodDirtyFlag;

    /**
     * 属性 [PRODUCT_TYPE]
     *
     */
    @Stock_moveProduct_typeDefault(info = "默认规则")
    private String product_type;

    @JsonIgnore
    private boolean product_typeDirtyFlag;

    /**
     * 属性 [UNBUILD_ID_TEXT]
     *
     */
    @Stock_moveUnbuild_id_textDefault(info = "默认规则")
    private String unbuild_id_text;

    @JsonIgnore
    private boolean unbuild_id_textDirtyFlag;

    /**
     * 属性 [SCRAPPED]
     *
     */
    @Stock_moveScrappedDefault(info = "默认规则")
    private String scrapped;

    @JsonIgnore
    private boolean scrappedDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Stock_moveCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID_TEXT]
     *
     */
    @Stock_moveProduction_id_textDefault(info = "默认规则")
    private String production_id_text;

    @JsonIgnore
    private boolean production_id_textDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @Stock_moveWarehouse_id_textDefault(info = "默认规则")
    private String warehouse_id_text;

    @JsonIgnore
    private boolean warehouse_id_textDirtyFlag;

    /**
     * 属性 [SHOW_OPERATIONS]
     *
     */
    @Stock_moveShow_operationsDefault(info = "默认规则")
    private String show_operations;

    @JsonIgnore
    private boolean show_operationsDirtyFlag;

    /**
     * 属性 [CREATED_PRODUCTION_ID_TEXT]
     *
     */
    @Stock_moveCreated_production_id_textDefault(info = "默认规则")
    private String created_production_id_text;

    @JsonIgnore
    private boolean created_production_id_textDirtyFlag;

    /**
     * 属性 [CREATED_PURCHASE_LINE_ID_TEXT]
     *
     */
    @Stock_moveCreated_purchase_line_id_textDefault(info = "默认规则")
    private String created_purchase_line_id_text;

    @JsonIgnore
    private boolean created_purchase_line_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @Stock_moveLocation_dest_id_textDefault(info = "默认规则")
    private String location_dest_id_text;

    @JsonIgnore
    private boolean location_dest_id_textDirtyFlag;

    /**
     * 属性 [HAS_TRACKING]
     *
     */
    @Stock_moveHas_trackingDefault(info = "默认规则")
    private String has_tracking;

    @JsonIgnore
    private boolean has_trackingDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @Stock_movePicking_type_id_textDefault(info = "默认规则")
    private String picking_type_id_text;

    @JsonIgnore
    private boolean picking_type_id_textDirtyFlag;

    /**
     * 属性 [SALE_LINE_ID_TEXT]
     *
     */
    @Stock_moveSale_line_id_textDefault(info = "默认规则")
    private String sale_line_id_text;

    @JsonIgnore
    private boolean sale_line_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Stock_movePartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [PICKING_PARTNER_ID]
     *
     */
    @Stock_movePicking_partner_idDefault(info = "默认规则")
    private Integer picking_partner_id;

    @JsonIgnore
    private boolean picking_partner_idDirtyFlag;

    /**
     * 属性 [RAW_MATERIAL_PRODUCTION_ID_TEXT]
     *
     */
    @Stock_moveRaw_material_production_id_textDefault(info = "默认规则")
    private String raw_material_production_id_text;

    @JsonIgnore
    private boolean raw_material_production_id_textDirtyFlag;

    /**
     * 属性 [ORIGIN_RETURNED_MOVE_ID_TEXT]
     *
     */
    @Stock_moveOrigin_returned_move_id_textDefault(info = "默认规则")
    private String origin_returned_move_id_text;

    @JsonIgnore
    private boolean origin_returned_move_id_textDirtyFlag;

    /**
     * 属性 [REPAIR_ID_TEXT]
     *
     */
    @Stock_moveRepair_id_textDefault(info = "默认规则")
    private String repair_id_text;

    @JsonIgnore
    private boolean repair_id_textDirtyFlag;

    /**
     * 属性 [BACKORDER_ID]
     *
     */
    @Stock_moveBackorder_idDefault(info = "默认规则")
    private Integer backorder_id;

    @JsonIgnore
    private boolean backorder_idDirtyFlag;

    /**
     * 属性 [PURCHASE_LINE_ID_TEXT]
     *
     */
    @Stock_movePurchase_line_id_textDefault(info = "默认规则")
    private String purchase_line_id_text;

    @JsonIgnore
    private boolean purchase_line_id_textDirtyFlag;

    /**
     * 属性 [PICKING_ID_TEXT]
     *
     */
    @Stock_movePicking_id_textDefault(info = "默认规则")
    private String picking_id_text;

    @JsonIgnore
    private boolean picking_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Stock_moveProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Stock_moveLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_moveCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @Stock_moveProduct_uom_textDefault(info = "默认规则")
    private String product_uom_text;

    @JsonIgnore
    private boolean product_uom_textDirtyFlag;

    /**
     * 属性 [OPERATION_ID_TEXT]
     *
     */
    @Stock_moveOperation_id_textDefault(info = "默认规则")
    private String operation_id_text;

    @JsonIgnore
    private boolean operation_id_textDirtyFlag;

    /**
     * 属性 [WORKORDER_ID_TEXT]
     *
     */
    @Stock_moveWorkorder_id_textDefault(info = "默认规则")
    private String workorder_id_text;

    @JsonIgnore
    private boolean workorder_id_textDirtyFlag;

    /**
     * 属性 [CONSUME_UNBUILD_ID_TEXT]
     *
     */
    @Stock_moveConsume_unbuild_id_textDefault(info = "默认规则")
    private String consume_unbuild_id_text;

    @JsonIgnore
    private boolean consume_unbuild_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_PACKAGING_TEXT]
     *
     */
    @Stock_moveProduct_packaging_textDefault(info = "默认规则")
    private String product_packaging_text;

    @JsonIgnore
    private boolean product_packaging_textDirtyFlag;

    /**
     * 属性 [RESTRICT_PARTNER_ID_TEXT]
     *
     */
    @Stock_moveRestrict_partner_id_textDefault(info = "默认规则")
    private String restrict_partner_id_text;

    @JsonIgnore
    private boolean restrict_partner_id_textDirtyFlag;

    /**
     * 属性 [INVENTORY_ID_TEXT]
     *
     */
    @Stock_moveInventory_id_textDefault(info = "默认规则")
    private String inventory_id_text;

    @JsonIgnore
    private boolean inventory_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @Stock_moveProduct_tmpl_idDefault(info = "默认规则")
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;

    /**
     * 属性 [RULE_ID_TEXT]
     *
     */
    @Stock_moveRule_id_textDefault(info = "默认规则")
    private String rule_id_text;

    @JsonIgnore
    private boolean rule_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_moveWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PICKING_CODE]
     *
     */
    @Stock_movePicking_codeDefault(info = "默认规则")
    private String picking_code;

    @JsonIgnore
    private boolean picking_codeDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ENTIRE_PACKS]
     *
     */
    @Stock_movePicking_type_entire_packsDefault(info = "默认规则")
    private String picking_type_entire_packs;

    @JsonIgnore
    private boolean picking_type_entire_packsDirtyFlag;

    /**
     * 属性 [CREATED_PRODUCTION_ID]
     *
     */
    @Stock_moveCreated_production_idDefault(info = "默认规则")
    private Integer created_production_id;

    @JsonIgnore
    private boolean created_production_idDirtyFlag;

    /**
     * 属性 [CONSUME_UNBUILD_ID]
     *
     */
    @Stock_moveConsume_unbuild_idDefault(info = "默认规则")
    private Integer consume_unbuild_id;

    @JsonIgnore
    private boolean consume_unbuild_idDirtyFlag;

    /**
     * 属性 [SALE_LINE_ID]
     *
     */
    @Stock_moveSale_line_idDefault(info = "默认规则")
    private Integer sale_line_id;

    @JsonIgnore
    private boolean sale_line_idDirtyFlag;

    /**
     * 属性 [ORIGIN_RETURNED_MOVE_ID]
     *
     */
    @Stock_moveOrigin_returned_move_idDefault(info = "默认规则")
    private Integer origin_returned_move_id;

    @JsonIgnore
    private boolean origin_returned_move_idDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @Stock_movePicking_type_idDefault(info = "默认规则")
    private Integer picking_type_id;

    @JsonIgnore
    private boolean picking_type_idDirtyFlag;

    /**
     * 属性 [OPERATION_ID]
     *
     */
    @Stock_moveOperation_idDefault(info = "默认规则")
    private Integer operation_id;

    @JsonIgnore
    private boolean operation_idDirtyFlag;

    /**
     * 属性 [BOM_LINE_ID]
     *
     */
    @Stock_moveBom_line_idDefault(info = "默认规则")
    private Integer bom_line_id;

    @JsonIgnore
    private boolean bom_line_idDirtyFlag;

    /**
     * 属性 [REPAIR_ID]
     *
     */
    @Stock_moveRepair_idDefault(info = "默认规则")
    private Integer repair_id;

    @JsonIgnore
    private boolean repair_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Stock_moveProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [UNBUILD_ID]
     *
     */
    @Stock_moveUnbuild_idDefault(info = "默认规则")
    private Integer unbuild_id;

    @JsonIgnore
    private boolean unbuild_idDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @Stock_moveProduction_idDefault(info = "默认规则")
    private Integer production_id;

    @JsonIgnore
    private boolean production_idDirtyFlag;

    /**
     * 属性 [CREATED_PURCHASE_LINE_ID]
     *
     */
    @Stock_moveCreated_purchase_line_idDefault(info = "默认规则")
    private Integer created_purchase_line_id;

    @JsonIgnore
    private boolean created_purchase_line_idDirtyFlag;

    /**
     * 属性 [WORKORDER_ID]
     *
     */
    @Stock_moveWorkorder_idDefault(info = "默认规则")
    private Integer workorder_id;

    @JsonIgnore
    private boolean workorder_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @Stock_moveProduct_uomDefault(info = "默认规则")
    private Integer product_uom;

    @JsonIgnore
    private boolean product_uomDirtyFlag;

    /**
     * 属性 [PICKING_ID]
     *
     */
    @Stock_movePicking_idDefault(info = "默认规则")
    private Integer picking_id;

    @JsonIgnore
    private boolean picking_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Stock_movePartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_moveCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PACKAGE_LEVEL_ID]
     *
     */
    @Stock_movePackage_level_idDefault(info = "默认规则")
    private Integer package_level_id;

    @JsonIgnore
    private boolean package_level_idDirtyFlag;

    /**
     * 属性 [PRODUCT_PACKAGING]
     *
     */
    @Stock_moveProduct_packagingDefault(info = "默认规则")
    private Integer product_packaging;

    @JsonIgnore
    private boolean product_packagingDirtyFlag;

    /**
     * 属性 [RESTRICT_PARTNER_ID]
     *
     */
    @Stock_moveRestrict_partner_idDefault(info = "默认规则")
    private Integer restrict_partner_id;

    @JsonIgnore
    private boolean restrict_partner_idDirtyFlag;

    /**
     * 属性 [RULE_ID]
     *
     */
    @Stock_moveRule_idDefault(info = "默认规则")
    private Integer rule_id;

    @JsonIgnore
    private boolean rule_idDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @Stock_moveWarehouse_idDefault(info = "默认规则")
    private Integer warehouse_id;

    @JsonIgnore
    private boolean warehouse_idDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @Stock_moveLocation_dest_idDefault(info = "默认规则")
    private Integer location_dest_id;

    @JsonIgnore
    private boolean location_dest_idDirtyFlag;

    /**
     * 属性 [RAW_MATERIAL_PRODUCTION_ID]
     *
     */
    @Stock_moveRaw_material_production_idDefault(info = "默认规则")
    private Integer raw_material_production_id;

    @JsonIgnore
    private boolean raw_material_production_idDirtyFlag;

    /**
     * 属性 [INVENTORY_ID]
     *
     */
    @Stock_moveInventory_idDefault(info = "默认规则")
    private Integer inventory_id;

    @JsonIgnore
    private boolean inventory_idDirtyFlag;

    /**
     * 属性 [PURCHASE_LINE_ID]
     *
     */
    @Stock_movePurchase_line_idDefault(info = "默认规则")
    private Integer purchase_line_id;

    @JsonIgnore
    private boolean purchase_line_idDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Stock_moveLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Stock_moveCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_moveWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [RETURNED_MOVE_IDS]
     */
    @JsonProperty("returned_move_ids")
    public String getReturned_move_ids(){
        return returned_move_ids ;
    }

    /**
     * 设置 [RETURNED_MOVE_IDS]
     */
    @JsonProperty("returned_move_ids")
    public void setReturned_move_ids(String  returned_move_ids){
        this.returned_move_ids = returned_move_ids ;
        this.returned_move_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RETURNED_MOVE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getReturned_move_idsDirtyFlag(){
        return returned_move_idsDirtyFlag ;
    }

    /**
     * 获取 [ADDITIONAL]
     */
    @JsonProperty("additional")
    public String getAdditional(){
        return additional ;
    }

    /**
     * 设置 [ADDITIONAL]
     */
    @JsonProperty("additional")
    public void setAdditional(String  additional){
        this.additional = additional ;
        this.additionalDirtyFlag = true ;
    }

    /**
     * 获取 [ADDITIONAL]脏标记
     */
    @JsonIgnore
    public boolean getAdditionalDirtyFlag(){
        return additionalDirtyFlag ;
    }

    /**
     * 获取 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return price_unit ;
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return price_unitDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return group_id ;
    }

    /**
     * 设置 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return group_idDirtyFlag ;
    }

    /**
     * 获取 [SCRAP_IDS]
     */
    @JsonProperty("scrap_ids")
    public String getScrap_ids(){
        return scrap_ids ;
    }

    /**
     * 设置 [SCRAP_IDS]
     */
    @JsonProperty("scrap_ids")
    public void setScrap_ids(String  scrap_ids){
        this.scrap_ids = scrap_ids ;
        this.scrap_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAP_IDS]脏标记
     */
    @JsonIgnore
    public boolean getScrap_idsDirtyFlag(){
        return scrap_idsDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_MOVE_IDS]
     */
    @JsonProperty("account_move_ids")
    public String getAccount_move_ids(){
        return account_move_ids ;
    }

    /**
     * 设置 [ACCOUNT_MOVE_IDS]
     */
    @JsonProperty("account_move_ids")
    public void setAccount_move_ids(String  account_move_ids){
        this.account_move_ids = account_move_ids ;
        this.account_move_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_MOVE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAccount_move_idsDirtyFlag(){
        return account_move_idsDirtyFlag ;
    }

    /**
     * 获取 [MOVE_DEST_IDS]
     */
    @JsonProperty("move_dest_ids")
    public String getMove_dest_ids(){
        return move_dest_ids ;
    }

    /**
     * 设置 [MOVE_DEST_IDS]
     */
    @JsonProperty("move_dest_ids")
    public void setMove_dest_ids(String  move_dest_ids){
        this.move_dest_ids = move_dest_ids ;
        this.move_dest_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_DEST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_dest_idsDirtyFlag(){
        return move_dest_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [TO_REFUND]
     */
    @JsonProperty("to_refund")
    public String getTo_refund(){
        return to_refund ;
    }

    /**
     * 设置 [TO_REFUND]
     */
    @JsonProperty("to_refund")
    public void setTo_refund(String  to_refund){
        this.to_refund = to_refund ;
        this.to_refundDirtyFlag = true ;
    }

    /**
     * 获取 [TO_REFUND]脏标记
     */
    @JsonIgnore
    public boolean getTo_refundDirtyFlag(){
        return to_refundDirtyFlag ;
    }

    /**
     * 获取 [UNIT_FACTOR]
     */
    @JsonProperty("unit_factor")
    public Double getUnit_factor(){
        return unit_factor ;
    }

    /**
     * 设置 [UNIT_FACTOR]
     */
    @JsonProperty("unit_factor")
    public void setUnit_factor(Double  unit_factor){
        this.unit_factor = unit_factor ;
        this.unit_factorDirtyFlag = true ;
    }

    /**
     * 获取 [UNIT_FACTOR]脏标记
     */
    @JsonIgnore
    public boolean getUnit_factorDirtyFlag(){
        return unit_factorDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [AVAILABILITY]
     */
    @JsonProperty("availability")
    public Double getAvailability(){
        return availability ;
    }

    /**
     * 设置 [AVAILABILITY]
     */
    @JsonProperty("availability")
    public void setAvailability(Double  availability){
        this.availability = availability ;
        this.availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getAvailabilityDirtyFlag(){
        return availabilityDirtyFlag ;
    }

    /**
     * 获取 [REMAINING_QTY]
     */
    @JsonProperty("remaining_qty")
    public Double getRemaining_qty(){
        return remaining_qty ;
    }

    /**
     * 设置 [REMAINING_QTY]
     */
    @JsonProperty("remaining_qty")
    public void setRemaining_qty(Double  remaining_qty){
        this.remaining_qty = remaining_qty ;
        this.remaining_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [REMAINING_QTY]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_qtyDirtyFlag(){
        return remaining_qtyDirtyFlag ;
    }

    /**
     * 获取 [PROPAGATE]
     */
    @JsonProperty("propagate")
    public String getPropagate(){
        return propagate ;
    }

    /**
     * 设置 [PROPAGATE]
     */
    @JsonProperty("propagate")
    public void setPropagate(String  propagate){
        this.propagate = propagate ;
        this.propagateDirtyFlag = true ;
    }

    /**
     * 获取 [PROPAGATE]脏标记
     */
    @JsonIgnore
    public boolean getPropagateDirtyFlag(){
        return propagateDirtyFlag ;
    }

    /**
     * 获取 [REFERENCE]
     */
    @JsonProperty("reference")
    public String getReference(){
        return reference ;
    }

    /**
     * 设置 [REFERENCE]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return referenceDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE_MOVE_LINE_IDS]
     */
    @JsonProperty("active_move_line_ids")
    public String getActive_move_line_ids(){
        return active_move_line_ids ;
    }

    /**
     * 设置 [ACTIVE_MOVE_LINE_IDS]
     */
    @JsonProperty("active_move_line_ids")
    public void setActive_move_line_ids(String  active_move_line_ids){
        this.active_move_line_ids = active_move_line_ids ;
        this.active_move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE_MOVE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActive_move_line_idsDirtyFlag(){
        return active_move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [MOVE_LINE_NOSUGGEST_IDS]
     */
    @JsonProperty("move_line_nosuggest_ids")
    public String getMove_line_nosuggest_ids(){
        return move_line_nosuggest_ids ;
    }

    /**
     * 设置 [MOVE_LINE_NOSUGGEST_IDS]
     */
    @JsonProperty("move_line_nosuggest_ids")
    public void setMove_line_nosuggest_ids(String  move_line_nosuggest_ids){
        this.move_line_nosuggest_ids = move_line_nosuggest_ids ;
        this.move_line_nosuggest_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINE_NOSUGGEST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_nosuggest_idsDirtyFlag(){
        return move_line_nosuggest_idsDirtyFlag ;
    }

    /**
     * 获取 [SHOW_DETAILS_VISIBLE]
     */
    @JsonProperty("show_details_visible")
    public String getShow_details_visible(){
        return show_details_visible ;
    }

    /**
     * 设置 [SHOW_DETAILS_VISIBLE]
     */
    @JsonProperty("show_details_visible")
    public void setShow_details_visible(String  show_details_visible){
        this.show_details_visible = show_details_visible ;
        this.show_details_visibleDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_DETAILS_VISIBLE]脏标记
     */
    @JsonIgnore
    public boolean getShow_details_visibleDirtyFlag(){
        return show_details_visibleDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return product_qty ;
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return product_qtyDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return origin ;
    }

    /**
     * 设置 [ORIGIN]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return originDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return product_uom_qty ;
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return product_uom_qtyDirtyFlag ;
    }

    /**
     * 获取 [NEEDS_LOTS]
     */
    @JsonProperty("needs_lots")
    public String getNeeds_lots(){
        return needs_lots ;
    }

    /**
     * 设置 [NEEDS_LOTS]
     */
    @JsonProperty("needs_lots")
    public void setNeeds_lots(String  needs_lots){
        this.needs_lots = needs_lots ;
        this.needs_lotsDirtyFlag = true ;
    }

    /**
     * 获取 [NEEDS_LOTS]脏标记
     */
    @JsonIgnore
    public boolean getNeeds_lotsDirtyFlag(){
        return needs_lotsDirtyFlag ;
    }

    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [REMAINING_VALUE]
     */
    @JsonProperty("remaining_value")
    public Double getRemaining_value(){
        return remaining_value ;
    }

    /**
     * 设置 [REMAINING_VALUE]
     */
    @JsonProperty("remaining_value")
    public void setRemaining_value(Double  remaining_value){
        this.remaining_value = remaining_value ;
        this.remaining_valueDirtyFlag = true ;
    }

    /**
     * 获取 [REMAINING_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_valueDirtyFlag(){
        return remaining_valueDirtyFlag ;
    }

    /**
     * 获取 [ORDER_FINISHED_LOT_IDS]
     */
    @JsonProperty("order_finished_lot_ids")
    public String getOrder_finished_lot_ids(){
        return order_finished_lot_ids ;
    }

    /**
     * 设置 [ORDER_FINISHED_LOT_IDS]
     */
    @JsonProperty("order_finished_lot_ids")
    public void setOrder_finished_lot_ids(String  order_finished_lot_ids){
        this.order_finished_lot_ids = order_finished_lot_ids ;
        this.order_finished_lot_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_FINISHED_LOT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOrder_finished_lot_idsDirtyFlag(){
        return order_finished_lot_idsDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ORIG_IDS]
     */
    @JsonProperty("move_orig_ids")
    public String getMove_orig_ids(){
        return move_orig_ids ;
    }

    /**
     * 设置 [MOVE_ORIG_IDS]
     */
    @JsonProperty("move_orig_ids")
    public void setMove_orig_ids(String  move_orig_ids){
        this.move_orig_ids = move_orig_ids ;
        this.move_orig_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ORIG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_orig_idsDirtyFlag(){
        return move_orig_idsDirtyFlag ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return move_line_ids ;
    }

    /**
     * 设置 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [RESERVED_AVAILABILITY]
     */
    @JsonProperty("reserved_availability")
    public Double getReserved_availability(){
        return reserved_availability ;
    }

    /**
     * 设置 [RESERVED_AVAILABILITY]
     */
    @JsonProperty("reserved_availability")
    public void setReserved_availability(Double  reserved_availability){
        this.reserved_availability = reserved_availability ;
        this.reserved_availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [RESERVED_AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getReserved_availabilityDirtyFlag(){
        return reserved_availabilityDirtyFlag ;
    }

    /**
     * 获取 [IS_INITIAL_DEMAND_EDITABLE]
     */
    @JsonProperty("is_initial_demand_editable")
    public String getIs_initial_demand_editable(){
        return is_initial_demand_editable ;
    }

    /**
     * 设置 [IS_INITIAL_DEMAND_EDITABLE]
     */
    @JsonProperty("is_initial_demand_editable")
    public void setIs_initial_demand_editable(String  is_initial_demand_editable){
        this.is_initial_demand_editable = is_initial_demand_editable ;
        this.is_initial_demand_editableDirtyFlag = true ;
    }

    /**
     * 获取 [IS_INITIAL_DEMAND_EDITABLE]脏标记
     */
    @JsonIgnore
    public boolean getIs_initial_demand_editableDirtyFlag(){
        return is_initial_demand_editableDirtyFlag ;
    }

    /**
     * 获取 [QUANTITY_DONE]
     */
    @JsonProperty("quantity_done")
    public Double getQuantity_done(){
        return quantity_done ;
    }

    /**
     * 设置 [QUANTITY_DONE]
     */
    @JsonProperty("quantity_done")
    public void setQuantity_done(Double  quantity_done){
        this.quantity_done = quantity_done ;
        this.quantity_doneDirtyFlag = true ;
    }

    /**
     * 获取 [QUANTITY_DONE]脏标记
     */
    @JsonIgnore
    public boolean getQuantity_doneDirtyFlag(){
        return quantity_doneDirtyFlag ;
    }

    /**
     * 获取 [IS_LOCKED]
     */
    @JsonProperty("is_locked")
    public String getIs_locked(){
        return is_locked ;
    }

    /**
     * 设置 [IS_LOCKED]
     */
    @JsonProperty("is_locked")
    public void setIs_locked(String  is_locked){
        this.is_locked = is_locked ;
        this.is_lockedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_LOCKED]脏标记
     */
    @JsonIgnore
    public boolean getIs_lockedDirtyFlag(){
        return is_lockedDirtyFlag ;
    }

    /**
     * 获取 [STRING_AVAILABILITY_INFO]
     */
    @JsonProperty("string_availability_info")
    public String getString_availability_info(){
        return string_availability_info ;
    }

    /**
     * 设置 [STRING_AVAILABILITY_INFO]
     */
    @JsonProperty("string_availability_info")
    public void setString_availability_info(String  string_availability_info){
        this.string_availability_info = string_availability_info ;
        this.string_availability_infoDirtyFlag = true ;
    }

    /**
     * 获取 [STRING_AVAILABILITY_INFO]脏标记
     */
    @JsonIgnore
    public boolean getString_availability_infoDirtyFlag(){
        return string_availability_infoDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [SHOW_RESERVED_AVAILABILITY]
     */
    @JsonProperty("show_reserved_availability")
    public String getShow_reserved_availability(){
        return show_reserved_availability ;
    }

    /**
     * 设置 [SHOW_RESERVED_AVAILABILITY]
     */
    @JsonProperty("show_reserved_availability")
    public void setShow_reserved_availability(String  show_reserved_availability){
        this.show_reserved_availability = show_reserved_availability ;
        this.show_reserved_availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_RESERVED_AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getShow_reserved_availabilityDirtyFlag(){
        return show_reserved_availabilityDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_IDS]
     */
    @JsonProperty("route_ids")
    public String getRoute_ids(){
        return route_ids ;
    }

    /**
     * 设置 [ROUTE_IDS]
     */
    @JsonProperty("route_ids")
    public void setRoute_ids(String  route_ids){
        this.route_ids = route_ids ;
        this.route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idsDirtyFlag(){
        return route_idsDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [VALUE]
     */
    @JsonProperty("value")
    public Double getValue(){
        return value ;
    }

    /**
     * 设置 [VALUE]
     */
    @JsonProperty("value")
    public void setValue(Double  value){
        this.value = value ;
        this.valueDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE]脏标记
     */
    @JsonIgnore
    public boolean getValueDirtyFlag(){
        return valueDirtyFlag ;
    }

    /**
     * 获取 [FINISHED_LOTS_EXIST]
     */
    @JsonProperty("finished_lots_exist")
    public String getFinished_lots_exist(){
        return finished_lots_exist ;
    }

    /**
     * 设置 [FINISHED_LOTS_EXIST]
     */
    @JsonProperty("finished_lots_exist")
    public void setFinished_lots_exist(String  finished_lots_exist){
        this.finished_lots_exist = finished_lots_exist ;
        this.finished_lots_existDirtyFlag = true ;
    }

    /**
     * 获取 [FINISHED_LOTS_EXIST]脏标记
     */
    @JsonIgnore
    public boolean getFinished_lots_existDirtyFlag(){
        return finished_lots_existDirtyFlag ;
    }

    /**
     * 获取 [IS_DONE]
     */
    @JsonProperty("is_done")
    public String getIs_done(){
        return is_done ;
    }

    /**
     * 设置 [IS_DONE]
     */
    @JsonProperty("is_done")
    public void setIs_done(String  is_done){
        this.is_done = is_done ;
        this.is_doneDirtyFlag = true ;
    }

    /**
     * 获取 [IS_DONE]脏标记
     */
    @JsonIgnore
    public boolean getIs_doneDirtyFlag(){
        return is_doneDirtyFlag ;
    }

    /**
     * 获取 [IS_QUANTITY_DONE_EDITABLE]
     */
    @JsonProperty("is_quantity_done_editable")
    public String getIs_quantity_done_editable(){
        return is_quantity_done_editable ;
    }

    /**
     * 设置 [IS_QUANTITY_DONE_EDITABLE]
     */
    @JsonProperty("is_quantity_done_editable")
    public void setIs_quantity_done_editable(String  is_quantity_done_editable){
        this.is_quantity_done_editable = is_quantity_done_editable ;
        this.is_quantity_done_editableDirtyFlag = true ;
    }

    /**
     * 获取 [IS_QUANTITY_DONE_EDITABLE]脏标记
     */
    @JsonIgnore
    public boolean getIs_quantity_done_editableDirtyFlag(){
        return is_quantity_done_editableDirtyFlag ;
    }

    /**
     * 获取 [HAS_MOVE_LINES]
     */
    @JsonProperty("has_move_lines")
    public String getHas_move_lines(){
        return has_move_lines ;
    }

    /**
     * 设置 [HAS_MOVE_LINES]
     */
    @JsonProperty("has_move_lines")
    public void setHas_move_lines(String  has_move_lines){
        this.has_move_lines = has_move_lines ;
        this.has_move_linesDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_MOVE_LINES]脏标记
     */
    @JsonIgnore
    public boolean getHas_move_linesDirtyFlag(){
        return has_move_linesDirtyFlag ;
    }

    /**
     * 获取 [DATE_EXPECTED]
     */
    @JsonProperty("date_expected")
    public Timestamp getDate_expected(){
        return date_expected ;
    }

    /**
     * 设置 [DATE_EXPECTED]
     */
    @JsonProperty("date_expected")
    public void setDate_expected(Timestamp  date_expected){
        this.date_expected = date_expected ;
        this.date_expectedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_EXPECTED]脏标记
     */
    @JsonIgnore
    public boolean getDate_expectedDirtyFlag(){
        return date_expectedDirtyFlag ;
    }

    /**
     * 获取 [PROCURE_METHOD]
     */
    @JsonProperty("procure_method")
    public String getProcure_method(){
        return procure_method ;
    }

    /**
     * 设置 [PROCURE_METHOD]
     */
    @JsonProperty("procure_method")
    public void setProcure_method(String  procure_method){
        this.procure_method = procure_method ;
        this.procure_methodDirtyFlag = true ;
    }

    /**
     * 获取 [PROCURE_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getProcure_methodDirtyFlag(){
        return procure_methodDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TYPE]
     */
    @JsonProperty("product_type")
    public String getProduct_type(){
        return product_type ;
    }

    /**
     * 设置 [PRODUCT_TYPE]
     */
    @JsonProperty("product_type")
    public void setProduct_type(String  product_type){
        this.product_type = product_type ;
        this.product_typeDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getProduct_typeDirtyFlag(){
        return product_typeDirtyFlag ;
    }

    /**
     * 获取 [UNBUILD_ID_TEXT]
     */
    @JsonProperty("unbuild_id_text")
    public String getUnbuild_id_text(){
        return unbuild_id_text ;
    }

    /**
     * 设置 [UNBUILD_ID_TEXT]
     */
    @JsonProperty("unbuild_id_text")
    public void setUnbuild_id_text(String  unbuild_id_text){
        this.unbuild_id_text = unbuild_id_text ;
        this.unbuild_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [UNBUILD_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUnbuild_id_textDirtyFlag(){
        return unbuild_id_textDirtyFlag ;
    }

    /**
     * 获取 [SCRAPPED]
     */
    @JsonProperty("scrapped")
    public String getScrapped(){
        return scrapped ;
    }

    /**
     * 设置 [SCRAPPED]
     */
    @JsonProperty("scrapped")
    public void setScrapped(String  scrapped){
        this.scrapped = scrapped ;
        this.scrappedDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAPPED]脏标记
     */
    @JsonIgnore
    public boolean getScrappedDirtyFlag(){
        return scrappedDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID_TEXT]
     */
    @JsonProperty("production_id_text")
    public String getProduction_id_text(){
        return production_id_text ;
    }

    /**
     * 设置 [PRODUCTION_ID_TEXT]
     */
    @JsonProperty("production_id_text")
    public void setProduction_id_text(String  production_id_text){
        this.production_id_text = production_id_text ;
        this.production_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduction_id_textDirtyFlag(){
        return production_id_textDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return warehouse_id_text ;
    }

    /**
     * 设置 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return warehouse_id_textDirtyFlag ;
    }

    /**
     * 获取 [SHOW_OPERATIONS]
     */
    @JsonProperty("show_operations")
    public String getShow_operations(){
        return show_operations ;
    }

    /**
     * 设置 [SHOW_OPERATIONS]
     */
    @JsonProperty("show_operations")
    public void setShow_operations(String  show_operations){
        this.show_operations = show_operations ;
        this.show_operationsDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_OPERATIONS]脏标记
     */
    @JsonIgnore
    public boolean getShow_operationsDirtyFlag(){
        return show_operationsDirtyFlag ;
    }

    /**
     * 获取 [CREATED_PRODUCTION_ID_TEXT]
     */
    @JsonProperty("created_production_id_text")
    public String getCreated_production_id_text(){
        return created_production_id_text ;
    }

    /**
     * 设置 [CREATED_PRODUCTION_ID_TEXT]
     */
    @JsonProperty("created_production_id_text")
    public void setCreated_production_id_text(String  created_production_id_text){
        this.created_production_id_text = created_production_id_text ;
        this.created_production_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATED_PRODUCTION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreated_production_id_textDirtyFlag(){
        return created_production_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATED_PURCHASE_LINE_ID_TEXT]
     */
    @JsonProperty("created_purchase_line_id_text")
    public String getCreated_purchase_line_id_text(){
        return created_purchase_line_id_text ;
    }

    /**
     * 设置 [CREATED_PURCHASE_LINE_ID_TEXT]
     */
    @JsonProperty("created_purchase_line_id_text")
    public void setCreated_purchase_line_id_text(String  created_purchase_line_id_text){
        this.created_purchase_line_id_text = created_purchase_line_id_text ;
        this.created_purchase_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATED_PURCHASE_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreated_purchase_line_id_textDirtyFlag(){
        return created_purchase_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return location_dest_id_text ;
    }

    /**
     * 设置 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return location_dest_id_textDirtyFlag ;
    }

    /**
     * 获取 [HAS_TRACKING]
     */
    @JsonProperty("has_tracking")
    public String getHas_tracking(){
        return has_tracking ;
    }

    /**
     * 设置 [HAS_TRACKING]
     */
    @JsonProperty("has_tracking")
    public void setHas_tracking(String  has_tracking){
        this.has_tracking = has_tracking ;
        this.has_trackingDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_TRACKING]脏标记
     */
    @JsonIgnore
    public boolean getHas_trackingDirtyFlag(){
        return has_trackingDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return picking_type_id_text ;
    }

    /**
     * 设置 [PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return picking_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [SALE_LINE_ID_TEXT]
     */
    @JsonProperty("sale_line_id_text")
    public String getSale_line_id_text(){
        return sale_line_id_text ;
    }

    /**
     * 设置 [SALE_LINE_ID_TEXT]
     */
    @JsonProperty("sale_line_id_text")
    public void setSale_line_id_text(String  sale_line_id_text){
        this.sale_line_id_text = sale_line_id_text ;
        this.sale_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_id_textDirtyFlag(){
        return sale_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_PARTNER_ID]
     */
    @JsonProperty("picking_partner_id")
    public Integer getPicking_partner_id(){
        return picking_partner_id ;
    }

    /**
     * 设置 [PICKING_PARTNER_ID]
     */
    @JsonProperty("picking_partner_id")
    public void setPicking_partner_id(Integer  picking_partner_id){
        this.picking_partner_id = picking_partner_id ;
        this.picking_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_partner_idDirtyFlag(){
        return picking_partner_idDirtyFlag ;
    }

    /**
     * 获取 [RAW_MATERIAL_PRODUCTION_ID_TEXT]
     */
    @JsonProperty("raw_material_production_id_text")
    public String getRaw_material_production_id_text(){
        return raw_material_production_id_text ;
    }

    /**
     * 设置 [RAW_MATERIAL_PRODUCTION_ID_TEXT]
     */
    @JsonProperty("raw_material_production_id_text")
    public void setRaw_material_production_id_text(String  raw_material_production_id_text){
        this.raw_material_production_id_text = raw_material_production_id_text ;
        this.raw_material_production_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RAW_MATERIAL_PRODUCTION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRaw_material_production_id_textDirtyFlag(){
        return raw_material_production_id_textDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN_RETURNED_MOVE_ID_TEXT]
     */
    @JsonProperty("origin_returned_move_id_text")
    public String getOrigin_returned_move_id_text(){
        return origin_returned_move_id_text ;
    }

    /**
     * 设置 [ORIGIN_RETURNED_MOVE_ID_TEXT]
     */
    @JsonProperty("origin_returned_move_id_text")
    public void setOrigin_returned_move_id_text(String  origin_returned_move_id_text){
        this.origin_returned_move_id_text = origin_returned_move_id_text ;
        this.origin_returned_move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN_RETURNED_MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOrigin_returned_move_id_textDirtyFlag(){
        return origin_returned_move_id_textDirtyFlag ;
    }

    /**
     * 获取 [REPAIR_ID_TEXT]
     */
    @JsonProperty("repair_id_text")
    public String getRepair_id_text(){
        return repair_id_text ;
    }

    /**
     * 设置 [REPAIR_ID_TEXT]
     */
    @JsonProperty("repair_id_text")
    public void setRepair_id_text(String  repair_id_text){
        this.repair_id_text = repair_id_text ;
        this.repair_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REPAIR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRepair_id_textDirtyFlag(){
        return repair_id_textDirtyFlag ;
    }

    /**
     * 获取 [BACKORDER_ID]
     */
    @JsonProperty("backorder_id")
    public Integer getBackorder_id(){
        return backorder_id ;
    }

    /**
     * 设置 [BACKORDER_ID]
     */
    @JsonProperty("backorder_id")
    public void setBackorder_id(Integer  backorder_id){
        this.backorder_id = backorder_id ;
        this.backorder_idDirtyFlag = true ;
    }

    /**
     * 获取 [BACKORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_idDirtyFlag(){
        return backorder_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_LINE_ID_TEXT]
     */
    @JsonProperty("purchase_line_id_text")
    public String getPurchase_line_id_text(){
        return purchase_line_id_text ;
    }

    /**
     * 设置 [PURCHASE_LINE_ID_TEXT]
     */
    @JsonProperty("purchase_line_id_text")
    public void setPurchase_line_id_text(String  purchase_line_id_text){
        this.purchase_line_id_text = purchase_line_id_text ;
        this.purchase_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_id_textDirtyFlag(){
        return purchase_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_ID_TEXT]
     */
    @JsonProperty("picking_id_text")
    public String getPicking_id_text(){
        return picking_id_text ;
    }

    /**
     * 设置 [PICKING_ID_TEXT]
     */
    @JsonProperty("picking_id_text")
    public void setPicking_id_text(String  picking_id_text){
        this.picking_id_text = picking_id_text ;
        this.picking_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPicking_id_textDirtyFlag(){
        return picking_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return product_uom_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return product_uom_textDirtyFlag ;
    }

    /**
     * 获取 [OPERATION_ID_TEXT]
     */
    @JsonProperty("operation_id_text")
    public String getOperation_id_text(){
        return operation_id_text ;
    }

    /**
     * 设置 [OPERATION_ID_TEXT]
     */
    @JsonProperty("operation_id_text")
    public void setOperation_id_text(String  operation_id_text){
        this.operation_id_text = operation_id_text ;
        this.operation_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OPERATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOperation_id_textDirtyFlag(){
        return operation_id_textDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_ID_TEXT]
     */
    @JsonProperty("workorder_id_text")
    public String getWorkorder_id_text(){
        return workorder_id_text ;
    }

    /**
     * 设置 [WORKORDER_ID_TEXT]
     */
    @JsonProperty("workorder_id_text")
    public void setWorkorder_id_text(String  workorder_id_text){
        this.workorder_id_text = workorder_id_text ;
        this.workorder_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_id_textDirtyFlag(){
        return workorder_id_textDirtyFlag ;
    }

    /**
     * 获取 [CONSUME_UNBUILD_ID_TEXT]
     */
    @JsonProperty("consume_unbuild_id_text")
    public String getConsume_unbuild_id_text(){
        return consume_unbuild_id_text ;
    }

    /**
     * 设置 [CONSUME_UNBUILD_ID_TEXT]
     */
    @JsonProperty("consume_unbuild_id_text")
    public void setConsume_unbuild_id_text(String  consume_unbuild_id_text){
        this.consume_unbuild_id_text = consume_unbuild_id_text ;
        this.consume_unbuild_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CONSUME_UNBUILD_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getConsume_unbuild_id_textDirtyFlag(){
        return consume_unbuild_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_PACKAGING_TEXT]
     */
    @JsonProperty("product_packaging_text")
    public String getProduct_packaging_text(){
        return product_packaging_text ;
    }

    /**
     * 设置 [PRODUCT_PACKAGING_TEXT]
     */
    @JsonProperty("product_packaging_text")
    public void setProduct_packaging_text(String  product_packaging_text){
        this.product_packaging_text = product_packaging_text ;
        this.product_packaging_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_PACKAGING_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_packaging_textDirtyFlag(){
        return product_packaging_textDirtyFlag ;
    }

    /**
     * 获取 [RESTRICT_PARTNER_ID_TEXT]
     */
    @JsonProperty("restrict_partner_id_text")
    public String getRestrict_partner_id_text(){
        return restrict_partner_id_text ;
    }

    /**
     * 设置 [RESTRICT_PARTNER_ID_TEXT]
     */
    @JsonProperty("restrict_partner_id_text")
    public void setRestrict_partner_id_text(String  restrict_partner_id_text){
        this.restrict_partner_id_text = restrict_partner_id_text ;
        this.restrict_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RESTRICT_PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRestrict_partner_id_textDirtyFlag(){
        return restrict_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [INVENTORY_ID_TEXT]
     */
    @JsonProperty("inventory_id_text")
    public String getInventory_id_text(){
        return inventory_id_text ;
    }

    /**
     * 设置 [INVENTORY_ID_TEXT]
     */
    @JsonProperty("inventory_id_text")
    public void setInventory_id_text(String  inventory_id_text){
        this.inventory_id_text = inventory_id_text ;
        this.inventory_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INVENTORY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getInventory_id_textDirtyFlag(){
        return inventory_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return product_tmpl_id ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return product_tmpl_idDirtyFlag ;
    }

    /**
     * 获取 [RULE_ID_TEXT]
     */
    @JsonProperty("rule_id_text")
    public String getRule_id_text(){
        return rule_id_text ;
    }

    /**
     * 设置 [RULE_ID_TEXT]
     */
    @JsonProperty("rule_id_text")
    public void setRule_id_text(String  rule_id_text){
        this.rule_id_text = rule_id_text ;
        this.rule_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRule_id_textDirtyFlag(){
        return rule_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_CODE]
     */
    @JsonProperty("picking_code")
    public String getPicking_code(){
        return picking_code ;
    }

    /**
     * 设置 [PICKING_CODE]
     */
    @JsonProperty("picking_code")
    public void setPicking_code(String  picking_code){
        this.picking_code = picking_code ;
        this.picking_codeDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_CODE]脏标记
     */
    @JsonIgnore
    public boolean getPicking_codeDirtyFlag(){
        return picking_codeDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ENTIRE_PACKS]
     */
    @JsonProperty("picking_type_entire_packs")
    public String getPicking_type_entire_packs(){
        return picking_type_entire_packs ;
    }

    /**
     * 设置 [PICKING_TYPE_ENTIRE_PACKS]
     */
    @JsonProperty("picking_type_entire_packs")
    public void setPicking_type_entire_packs(String  picking_type_entire_packs){
        this.picking_type_entire_packs = picking_type_entire_packs ;
        this.picking_type_entire_packsDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ENTIRE_PACKS]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_entire_packsDirtyFlag(){
        return picking_type_entire_packsDirtyFlag ;
    }

    /**
     * 获取 [CREATED_PRODUCTION_ID]
     */
    @JsonProperty("created_production_id")
    public Integer getCreated_production_id(){
        return created_production_id ;
    }

    /**
     * 设置 [CREATED_PRODUCTION_ID]
     */
    @JsonProperty("created_production_id")
    public void setCreated_production_id(Integer  created_production_id){
        this.created_production_id = created_production_id ;
        this.created_production_idDirtyFlag = true ;
    }

    /**
     * 获取 [CREATED_PRODUCTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getCreated_production_idDirtyFlag(){
        return created_production_idDirtyFlag ;
    }

    /**
     * 获取 [CONSUME_UNBUILD_ID]
     */
    @JsonProperty("consume_unbuild_id")
    public Integer getConsume_unbuild_id(){
        return consume_unbuild_id ;
    }

    /**
     * 设置 [CONSUME_UNBUILD_ID]
     */
    @JsonProperty("consume_unbuild_id")
    public void setConsume_unbuild_id(Integer  consume_unbuild_id){
        this.consume_unbuild_id = consume_unbuild_id ;
        this.consume_unbuild_idDirtyFlag = true ;
    }

    /**
     * 获取 [CONSUME_UNBUILD_ID]脏标记
     */
    @JsonIgnore
    public boolean getConsume_unbuild_idDirtyFlag(){
        return consume_unbuild_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_LINE_ID]
     */
    @JsonProperty("sale_line_id")
    public Integer getSale_line_id(){
        return sale_line_id ;
    }

    /**
     * 设置 [SALE_LINE_ID]
     */
    @JsonProperty("sale_line_id")
    public void setSale_line_id(Integer  sale_line_id){
        this.sale_line_id = sale_line_id ;
        this.sale_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_idDirtyFlag(){
        return sale_line_idDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN_RETURNED_MOVE_ID]
     */
    @JsonProperty("origin_returned_move_id")
    public Integer getOrigin_returned_move_id(){
        return origin_returned_move_id ;
    }

    /**
     * 设置 [ORIGIN_RETURNED_MOVE_ID]
     */
    @JsonProperty("origin_returned_move_id")
    public void setOrigin_returned_move_id(Integer  origin_returned_move_id){
        this.origin_returned_move_id = origin_returned_move_id ;
        this.origin_returned_move_idDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN_RETURNED_MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getOrigin_returned_move_idDirtyFlag(){
        return origin_returned_move_idDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ID]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return picking_type_id ;
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return picking_type_idDirtyFlag ;
    }

    /**
     * 获取 [OPERATION_ID]
     */
    @JsonProperty("operation_id")
    public Integer getOperation_id(){
        return operation_id ;
    }

    /**
     * 设置 [OPERATION_ID]
     */
    @JsonProperty("operation_id")
    public void setOperation_id(Integer  operation_id){
        this.operation_id = operation_id ;
        this.operation_idDirtyFlag = true ;
    }

    /**
     * 获取 [OPERATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getOperation_idDirtyFlag(){
        return operation_idDirtyFlag ;
    }

    /**
     * 获取 [BOM_LINE_ID]
     */
    @JsonProperty("bom_line_id")
    public Integer getBom_line_id(){
        return bom_line_id ;
    }

    /**
     * 设置 [BOM_LINE_ID]
     */
    @JsonProperty("bom_line_id")
    public void setBom_line_id(Integer  bom_line_id){
        this.bom_line_id = bom_line_id ;
        this.bom_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [BOM_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getBom_line_idDirtyFlag(){
        return bom_line_idDirtyFlag ;
    }

    /**
     * 获取 [REPAIR_ID]
     */
    @JsonProperty("repair_id")
    public Integer getRepair_id(){
        return repair_id ;
    }

    /**
     * 设置 [REPAIR_ID]
     */
    @JsonProperty("repair_id")
    public void setRepair_id(Integer  repair_id){
        this.repair_id = repair_id ;
        this.repair_idDirtyFlag = true ;
    }

    /**
     * 获取 [REPAIR_ID]脏标记
     */
    @JsonIgnore
    public boolean getRepair_idDirtyFlag(){
        return repair_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [UNBUILD_ID]
     */
    @JsonProperty("unbuild_id")
    public Integer getUnbuild_id(){
        return unbuild_id ;
    }

    /**
     * 设置 [UNBUILD_ID]
     */
    @JsonProperty("unbuild_id")
    public void setUnbuild_id(Integer  unbuild_id){
        this.unbuild_id = unbuild_id ;
        this.unbuild_idDirtyFlag = true ;
    }

    /**
     * 获取 [UNBUILD_ID]脏标记
     */
    @JsonIgnore
    public boolean getUnbuild_idDirtyFlag(){
        return unbuild_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public Integer getProduction_id(){
        return production_id ;
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public void setProduction_id(Integer  production_id){
        this.production_id = production_id ;
        this.production_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduction_idDirtyFlag(){
        return production_idDirtyFlag ;
    }

    /**
     * 获取 [CREATED_PURCHASE_LINE_ID]
     */
    @JsonProperty("created_purchase_line_id")
    public Integer getCreated_purchase_line_id(){
        return created_purchase_line_id ;
    }

    /**
     * 设置 [CREATED_PURCHASE_LINE_ID]
     */
    @JsonProperty("created_purchase_line_id")
    public void setCreated_purchase_line_id(Integer  created_purchase_line_id){
        this.created_purchase_line_id = created_purchase_line_id ;
        this.created_purchase_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [CREATED_PURCHASE_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getCreated_purchase_line_idDirtyFlag(){
        return created_purchase_line_idDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_ID]
     */
    @JsonProperty("workorder_id")
    public Integer getWorkorder_id(){
        return workorder_id ;
    }

    /**
     * 设置 [WORKORDER_ID]
     */
    @JsonProperty("workorder_id")
    public void setWorkorder_id(Integer  workorder_id){
        this.workorder_id = workorder_id ;
        this.workorder_idDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idDirtyFlag(){
        return workorder_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return product_uom ;
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return product_uomDirtyFlag ;
    }

    /**
     * 获取 [PICKING_ID]
     */
    @JsonProperty("picking_id")
    public Integer getPicking_id(){
        return picking_id ;
    }

    /**
     * 设置 [PICKING_ID]
     */
    @JsonProperty("picking_id")
    public void setPicking_id(Integer  picking_id){
        this.picking_id = picking_id ;
        this.picking_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idDirtyFlag(){
        return picking_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_LEVEL_ID]
     */
    @JsonProperty("package_level_id")
    public Integer getPackage_level_id(){
        return package_level_id ;
    }

    /**
     * 设置 [PACKAGE_LEVEL_ID]
     */
    @JsonProperty("package_level_id")
    public void setPackage_level_id(Integer  package_level_id){
        this.package_level_id = package_level_id ;
        this.package_level_idDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_LEVEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_idDirtyFlag(){
        return package_level_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_PACKAGING]
     */
    @JsonProperty("product_packaging")
    public Integer getProduct_packaging(){
        return product_packaging ;
    }

    /**
     * 设置 [PRODUCT_PACKAGING]
     */
    @JsonProperty("product_packaging")
    public void setProduct_packaging(Integer  product_packaging){
        this.product_packaging = product_packaging ;
        this.product_packagingDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_PACKAGING]脏标记
     */
    @JsonIgnore
    public boolean getProduct_packagingDirtyFlag(){
        return product_packagingDirtyFlag ;
    }

    /**
     * 获取 [RESTRICT_PARTNER_ID]
     */
    @JsonProperty("restrict_partner_id")
    public Integer getRestrict_partner_id(){
        return restrict_partner_id ;
    }

    /**
     * 设置 [RESTRICT_PARTNER_ID]
     */
    @JsonProperty("restrict_partner_id")
    public void setRestrict_partner_id(Integer  restrict_partner_id){
        this.restrict_partner_id = restrict_partner_id ;
        this.restrict_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESTRICT_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getRestrict_partner_idDirtyFlag(){
        return restrict_partner_idDirtyFlag ;
    }

    /**
     * 获取 [RULE_ID]
     */
    @JsonProperty("rule_id")
    public Integer getRule_id(){
        return rule_id ;
    }

    /**
     * 设置 [RULE_ID]
     */
    @JsonProperty("rule_id")
    public void setRule_id(Integer  rule_id){
        this.rule_id = rule_id ;
        this.rule_idDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_ID]脏标记
     */
    @JsonIgnore
    public boolean getRule_idDirtyFlag(){
        return rule_idDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return warehouse_id ;
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return warehouse_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return location_dest_id ;
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return location_dest_idDirtyFlag ;
    }

    /**
     * 获取 [RAW_MATERIAL_PRODUCTION_ID]
     */
    @JsonProperty("raw_material_production_id")
    public Integer getRaw_material_production_id(){
        return raw_material_production_id ;
    }

    /**
     * 设置 [RAW_MATERIAL_PRODUCTION_ID]
     */
    @JsonProperty("raw_material_production_id")
    public void setRaw_material_production_id(Integer  raw_material_production_id){
        this.raw_material_production_id = raw_material_production_id ;
        this.raw_material_production_idDirtyFlag = true ;
    }

    /**
     * 获取 [RAW_MATERIAL_PRODUCTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getRaw_material_production_idDirtyFlag(){
        return raw_material_production_idDirtyFlag ;
    }

    /**
     * 获取 [INVENTORY_ID]
     */
    @JsonProperty("inventory_id")
    public Integer getInventory_id(){
        return inventory_id ;
    }

    /**
     * 设置 [INVENTORY_ID]
     */
    @JsonProperty("inventory_id")
    public void setInventory_id(Integer  inventory_id){
        this.inventory_id = inventory_id ;
        this.inventory_idDirtyFlag = true ;
    }

    /**
     * 获取 [INVENTORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getInventory_idDirtyFlag(){
        return inventory_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_LINE_ID]
     */
    @JsonProperty("purchase_line_id")
    public Integer getPurchase_line_id(){
        return purchase_line_id ;
    }

    /**
     * 设置 [PURCHASE_LINE_ID]
     */
    @JsonProperty("purchase_line_id")
    public void setPurchase_line_id(Integer  purchase_line_id){
        this.purchase_line_id = purchase_line_id ;
        this.purchase_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_line_idDirtyFlag(){
        return purchase_line_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Stock_move toDO() {
        Stock_move srfdomain = new Stock_move();
        if(getReturned_move_idsDirtyFlag())
            srfdomain.setReturned_move_ids(returned_move_ids);
        if(getAdditionalDirtyFlag())
            srfdomain.setAdditional(additional);
        if(getPrice_unitDirtyFlag())
            srfdomain.setPrice_unit(price_unit);
        if(getGroup_idDirtyFlag())
            srfdomain.setGroup_id(group_id);
        if(getScrap_idsDirtyFlag())
            srfdomain.setScrap_ids(scrap_ids);
        if(getAccount_move_idsDirtyFlag())
            srfdomain.setAccount_move_ids(account_move_ids);
        if(getMove_dest_idsDirtyFlag())
            srfdomain.setMove_dest_ids(move_dest_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getTo_refundDirtyFlag())
            srfdomain.setTo_refund(to_refund);
        if(getUnit_factorDirtyFlag())
            srfdomain.setUnit_factor(unit_factor);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getAvailabilityDirtyFlag())
            srfdomain.setAvailability(availability);
        if(getRemaining_qtyDirtyFlag())
            srfdomain.setRemaining_qty(remaining_qty);
        if(getPropagateDirtyFlag())
            srfdomain.setPropagate(propagate);
        if(getReferenceDirtyFlag())
            srfdomain.setReference(reference);
        if(getActive_move_line_idsDirtyFlag())
            srfdomain.setActive_move_line_ids(active_move_line_ids);
        if(getMove_line_nosuggest_idsDirtyFlag())
            srfdomain.setMove_line_nosuggest_ids(move_line_nosuggest_ids);
        if(getShow_details_visibleDirtyFlag())
            srfdomain.setShow_details_visible(show_details_visible);
        if(getProduct_qtyDirtyFlag())
            srfdomain.setProduct_qty(product_qty);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getOriginDirtyFlag())
            srfdomain.setOrigin(origin);
        if(getProduct_uom_qtyDirtyFlag())
            srfdomain.setProduct_uom_qty(product_uom_qty);
        if(getNeeds_lotsDirtyFlag())
            srfdomain.setNeeds_lots(needs_lots);
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getRemaining_valueDirtyFlag())
            srfdomain.setRemaining_value(remaining_value);
        if(getOrder_finished_lot_idsDirtyFlag())
            srfdomain.setOrder_finished_lot_ids(order_finished_lot_ids);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMove_orig_idsDirtyFlag())
            srfdomain.setMove_orig_ids(move_orig_ids);
        if(getMove_line_idsDirtyFlag())
            srfdomain.setMove_line_ids(move_line_ids);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getReserved_availabilityDirtyFlag())
            srfdomain.setReserved_availability(reserved_availability);
        if(getIs_initial_demand_editableDirtyFlag())
            srfdomain.setIs_initial_demand_editable(is_initial_demand_editable);
        if(getQuantity_doneDirtyFlag())
            srfdomain.setQuantity_done(quantity_done);
        if(getIs_lockedDirtyFlag())
            srfdomain.setIs_locked(is_locked);
        if(getString_availability_infoDirtyFlag())
            srfdomain.setString_availability_info(string_availability_info);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getShow_reserved_availabilityDirtyFlag())
            srfdomain.setShow_reserved_availability(show_reserved_availability);
        if(getRoute_idsDirtyFlag())
            srfdomain.setRoute_ids(route_ids);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getValueDirtyFlag())
            srfdomain.setValue(value);
        if(getFinished_lots_existDirtyFlag())
            srfdomain.setFinished_lots_exist(finished_lots_exist);
        if(getIs_doneDirtyFlag())
            srfdomain.setIs_done(is_done);
        if(getIs_quantity_done_editableDirtyFlag())
            srfdomain.setIs_quantity_done_editable(is_quantity_done_editable);
        if(getHas_move_linesDirtyFlag())
            srfdomain.setHas_move_lines(has_move_lines);
        if(getDate_expectedDirtyFlag())
            srfdomain.setDate_expected(date_expected);
        if(getProcure_methodDirtyFlag())
            srfdomain.setProcure_method(procure_method);
        if(getProduct_typeDirtyFlag())
            srfdomain.setProduct_type(product_type);
        if(getUnbuild_id_textDirtyFlag())
            srfdomain.setUnbuild_id_text(unbuild_id_text);
        if(getScrappedDirtyFlag())
            srfdomain.setScrapped(scrapped);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getProduction_id_textDirtyFlag())
            srfdomain.setProduction_id_text(production_id_text);
        if(getWarehouse_id_textDirtyFlag())
            srfdomain.setWarehouse_id_text(warehouse_id_text);
        if(getShow_operationsDirtyFlag())
            srfdomain.setShow_operations(show_operations);
        if(getCreated_production_id_textDirtyFlag())
            srfdomain.setCreated_production_id_text(created_production_id_text);
        if(getCreated_purchase_line_id_textDirtyFlag())
            srfdomain.setCreated_purchase_line_id_text(created_purchase_line_id_text);
        if(getLocation_dest_id_textDirtyFlag())
            srfdomain.setLocation_dest_id_text(location_dest_id_text);
        if(getHas_trackingDirtyFlag())
            srfdomain.setHas_tracking(has_tracking);
        if(getPicking_type_id_textDirtyFlag())
            srfdomain.setPicking_type_id_text(picking_type_id_text);
        if(getSale_line_id_textDirtyFlag())
            srfdomain.setSale_line_id_text(sale_line_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getPicking_partner_idDirtyFlag())
            srfdomain.setPicking_partner_id(picking_partner_id);
        if(getRaw_material_production_id_textDirtyFlag())
            srfdomain.setRaw_material_production_id_text(raw_material_production_id_text);
        if(getOrigin_returned_move_id_textDirtyFlag())
            srfdomain.setOrigin_returned_move_id_text(origin_returned_move_id_text);
        if(getRepair_id_textDirtyFlag())
            srfdomain.setRepair_id_text(repair_id_text);
        if(getBackorder_idDirtyFlag())
            srfdomain.setBackorder_id(backorder_id);
        if(getPurchase_line_id_textDirtyFlag())
            srfdomain.setPurchase_line_id_text(purchase_line_id_text);
        if(getPicking_id_textDirtyFlag())
            srfdomain.setPicking_id_text(picking_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getProduct_uom_textDirtyFlag())
            srfdomain.setProduct_uom_text(product_uom_text);
        if(getOperation_id_textDirtyFlag())
            srfdomain.setOperation_id_text(operation_id_text);
        if(getWorkorder_id_textDirtyFlag())
            srfdomain.setWorkorder_id_text(workorder_id_text);
        if(getConsume_unbuild_id_textDirtyFlag())
            srfdomain.setConsume_unbuild_id_text(consume_unbuild_id_text);
        if(getProduct_packaging_textDirtyFlag())
            srfdomain.setProduct_packaging_text(product_packaging_text);
        if(getRestrict_partner_id_textDirtyFlag())
            srfdomain.setRestrict_partner_id_text(restrict_partner_id_text);
        if(getInventory_id_textDirtyFlag())
            srfdomain.setInventory_id_text(inventory_id_text);
        if(getProduct_tmpl_idDirtyFlag())
            srfdomain.setProduct_tmpl_id(product_tmpl_id);
        if(getRule_id_textDirtyFlag())
            srfdomain.setRule_id_text(rule_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getPicking_codeDirtyFlag())
            srfdomain.setPicking_code(picking_code);
        if(getPicking_type_entire_packsDirtyFlag())
            srfdomain.setPicking_type_entire_packs(picking_type_entire_packs);
        if(getCreated_production_idDirtyFlag())
            srfdomain.setCreated_production_id(created_production_id);
        if(getConsume_unbuild_idDirtyFlag())
            srfdomain.setConsume_unbuild_id(consume_unbuild_id);
        if(getSale_line_idDirtyFlag())
            srfdomain.setSale_line_id(sale_line_id);
        if(getOrigin_returned_move_idDirtyFlag())
            srfdomain.setOrigin_returned_move_id(origin_returned_move_id);
        if(getPicking_type_idDirtyFlag())
            srfdomain.setPicking_type_id(picking_type_id);
        if(getOperation_idDirtyFlag())
            srfdomain.setOperation_id(operation_id);
        if(getBom_line_idDirtyFlag())
            srfdomain.setBom_line_id(bom_line_id);
        if(getRepair_idDirtyFlag())
            srfdomain.setRepair_id(repair_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getUnbuild_idDirtyFlag())
            srfdomain.setUnbuild_id(unbuild_id);
        if(getProduction_idDirtyFlag())
            srfdomain.setProduction_id(production_id);
        if(getCreated_purchase_line_idDirtyFlag())
            srfdomain.setCreated_purchase_line_id(created_purchase_line_id);
        if(getWorkorder_idDirtyFlag())
            srfdomain.setWorkorder_id(workorder_id);
        if(getProduct_uomDirtyFlag())
            srfdomain.setProduct_uom(product_uom);
        if(getPicking_idDirtyFlag())
            srfdomain.setPicking_id(picking_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPackage_level_idDirtyFlag())
            srfdomain.setPackage_level_id(package_level_id);
        if(getProduct_packagingDirtyFlag())
            srfdomain.setProduct_packaging(product_packaging);
        if(getRestrict_partner_idDirtyFlag())
            srfdomain.setRestrict_partner_id(restrict_partner_id);
        if(getRule_idDirtyFlag())
            srfdomain.setRule_id(rule_id);
        if(getWarehouse_idDirtyFlag())
            srfdomain.setWarehouse_id(warehouse_id);
        if(getLocation_dest_idDirtyFlag())
            srfdomain.setLocation_dest_id(location_dest_id);
        if(getRaw_material_production_idDirtyFlag())
            srfdomain.setRaw_material_production_id(raw_material_production_id);
        if(getInventory_idDirtyFlag())
            srfdomain.setInventory_id(inventory_id);
        if(getPurchase_line_idDirtyFlag())
            srfdomain.setPurchase_line_id(purchase_line_id);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Stock_move srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getReturned_move_idsDirtyFlag())
            this.setReturned_move_ids(srfdomain.getReturned_move_ids());
        if(srfdomain.getAdditionalDirtyFlag())
            this.setAdditional(srfdomain.getAdditional());
        if(srfdomain.getPrice_unitDirtyFlag())
            this.setPrice_unit(srfdomain.getPrice_unit());
        if(srfdomain.getGroup_idDirtyFlag())
            this.setGroup_id(srfdomain.getGroup_id());
        if(srfdomain.getScrap_idsDirtyFlag())
            this.setScrap_ids(srfdomain.getScrap_ids());
        if(srfdomain.getAccount_move_idsDirtyFlag())
            this.setAccount_move_ids(srfdomain.getAccount_move_ids());
        if(srfdomain.getMove_dest_idsDirtyFlag())
            this.setMove_dest_ids(srfdomain.getMove_dest_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getTo_refundDirtyFlag())
            this.setTo_refund(srfdomain.getTo_refund());
        if(srfdomain.getUnit_factorDirtyFlag())
            this.setUnit_factor(srfdomain.getUnit_factor());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getAvailabilityDirtyFlag())
            this.setAvailability(srfdomain.getAvailability());
        if(srfdomain.getRemaining_qtyDirtyFlag())
            this.setRemaining_qty(srfdomain.getRemaining_qty());
        if(srfdomain.getPropagateDirtyFlag())
            this.setPropagate(srfdomain.getPropagate());
        if(srfdomain.getReferenceDirtyFlag())
            this.setReference(srfdomain.getReference());
        if(srfdomain.getActive_move_line_idsDirtyFlag())
            this.setActive_move_line_ids(srfdomain.getActive_move_line_ids());
        if(srfdomain.getMove_line_nosuggest_idsDirtyFlag())
            this.setMove_line_nosuggest_ids(srfdomain.getMove_line_nosuggest_ids());
        if(srfdomain.getShow_details_visibleDirtyFlag())
            this.setShow_details_visible(srfdomain.getShow_details_visible());
        if(srfdomain.getProduct_qtyDirtyFlag())
            this.setProduct_qty(srfdomain.getProduct_qty());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getOriginDirtyFlag())
            this.setOrigin(srfdomain.getOrigin());
        if(srfdomain.getProduct_uom_qtyDirtyFlag())
            this.setProduct_uom_qty(srfdomain.getProduct_uom_qty());
        if(srfdomain.getNeeds_lotsDirtyFlag())
            this.setNeeds_lots(srfdomain.getNeeds_lots());
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getRemaining_valueDirtyFlag())
            this.setRemaining_value(srfdomain.getRemaining_value());
        if(srfdomain.getOrder_finished_lot_idsDirtyFlag())
            this.setOrder_finished_lot_ids(srfdomain.getOrder_finished_lot_ids());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMove_orig_idsDirtyFlag())
            this.setMove_orig_ids(srfdomain.getMove_orig_ids());
        if(srfdomain.getMove_line_idsDirtyFlag())
            this.setMove_line_ids(srfdomain.getMove_line_ids());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getReserved_availabilityDirtyFlag())
            this.setReserved_availability(srfdomain.getReserved_availability());
        if(srfdomain.getIs_initial_demand_editableDirtyFlag())
            this.setIs_initial_demand_editable(srfdomain.getIs_initial_demand_editable());
        if(srfdomain.getQuantity_doneDirtyFlag())
            this.setQuantity_done(srfdomain.getQuantity_done());
        if(srfdomain.getIs_lockedDirtyFlag())
            this.setIs_locked(srfdomain.getIs_locked());
        if(srfdomain.getString_availability_infoDirtyFlag())
            this.setString_availability_info(srfdomain.getString_availability_info());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getShow_reserved_availabilityDirtyFlag())
            this.setShow_reserved_availability(srfdomain.getShow_reserved_availability());
        if(srfdomain.getRoute_idsDirtyFlag())
            this.setRoute_ids(srfdomain.getRoute_ids());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getValueDirtyFlag())
            this.setValue(srfdomain.getValue());
        if(srfdomain.getFinished_lots_existDirtyFlag())
            this.setFinished_lots_exist(srfdomain.getFinished_lots_exist());
        if(srfdomain.getIs_doneDirtyFlag())
            this.setIs_done(srfdomain.getIs_done());
        if(srfdomain.getIs_quantity_done_editableDirtyFlag())
            this.setIs_quantity_done_editable(srfdomain.getIs_quantity_done_editable());
        if(srfdomain.getHas_move_linesDirtyFlag())
            this.setHas_move_lines(srfdomain.getHas_move_lines());
        if(srfdomain.getDate_expectedDirtyFlag())
            this.setDate_expected(srfdomain.getDate_expected());
        if(srfdomain.getProcure_methodDirtyFlag())
            this.setProcure_method(srfdomain.getProcure_method());
        if(srfdomain.getProduct_typeDirtyFlag())
            this.setProduct_type(srfdomain.getProduct_type());
        if(srfdomain.getUnbuild_id_textDirtyFlag())
            this.setUnbuild_id_text(srfdomain.getUnbuild_id_text());
        if(srfdomain.getScrappedDirtyFlag())
            this.setScrapped(srfdomain.getScrapped());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getProduction_id_textDirtyFlag())
            this.setProduction_id_text(srfdomain.getProduction_id_text());
        if(srfdomain.getWarehouse_id_textDirtyFlag())
            this.setWarehouse_id_text(srfdomain.getWarehouse_id_text());
        if(srfdomain.getShow_operationsDirtyFlag())
            this.setShow_operations(srfdomain.getShow_operations());
        if(srfdomain.getCreated_production_id_textDirtyFlag())
            this.setCreated_production_id_text(srfdomain.getCreated_production_id_text());
        if(srfdomain.getCreated_purchase_line_id_textDirtyFlag())
            this.setCreated_purchase_line_id_text(srfdomain.getCreated_purchase_line_id_text());
        if(srfdomain.getLocation_dest_id_textDirtyFlag())
            this.setLocation_dest_id_text(srfdomain.getLocation_dest_id_text());
        if(srfdomain.getHas_trackingDirtyFlag())
            this.setHas_tracking(srfdomain.getHas_tracking());
        if(srfdomain.getPicking_type_id_textDirtyFlag())
            this.setPicking_type_id_text(srfdomain.getPicking_type_id_text());
        if(srfdomain.getSale_line_id_textDirtyFlag())
            this.setSale_line_id_text(srfdomain.getSale_line_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getPicking_partner_idDirtyFlag())
            this.setPicking_partner_id(srfdomain.getPicking_partner_id());
        if(srfdomain.getRaw_material_production_id_textDirtyFlag())
            this.setRaw_material_production_id_text(srfdomain.getRaw_material_production_id_text());
        if(srfdomain.getOrigin_returned_move_id_textDirtyFlag())
            this.setOrigin_returned_move_id_text(srfdomain.getOrigin_returned_move_id_text());
        if(srfdomain.getRepair_id_textDirtyFlag())
            this.setRepair_id_text(srfdomain.getRepair_id_text());
        if(srfdomain.getBackorder_idDirtyFlag())
            this.setBackorder_id(srfdomain.getBackorder_id());
        if(srfdomain.getPurchase_line_id_textDirtyFlag())
            this.setPurchase_line_id_text(srfdomain.getPurchase_line_id_text());
        if(srfdomain.getPicking_id_textDirtyFlag())
            this.setPicking_id_text(srfdomain.getPicking_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getProduct_uom_textDirtyFlag())
            this.setProduct_uom_text(srfdomain.getProduct_uom_text());
        if(srfdomain.getOperation_id_textDirtyFlag())
            this.setOperation_id_text(srfdomain.getOperation_id_text());
        if(srfdomain.getWorkorder_id_textDirtyFlag())
            this.setWorkorder_id_text(srfdomain.getWorkorder_id_text());
        if(srfdomain.getConsume_unbuild_id_textDirtyFlag())
            this.setConsume_unbuild_id_text(srfdomain.getConsume_unbuild_id_text());
        if(srfdomain.getProduct_packaging_textDirtyFlag())
            this.setProduct_packaging_text(srfdomain.getProduct_packaging_text());
        if(srfdomain.getRestrict_partner_id_textDirtyFlag())
            this.setRestrict_partner_id_text(srfdomain.getRestrict_partner_id_text());
        if(srfdomain.getInventory_id_textDirtyFlag())
            this.setInventory_id_text(srfdomain.getInventory_id_text());
        if(srfdomain.getProduct_tmpl_idDirtyFlag())
            this.setProduct_tmpl_id(srfdomain.getProduct_tmpl_id());
        if(srfdomain.getRule_id_textDirtyFlag())
            this.setRule_id_text(srfdomain.getRule_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getPicking_codeDirtyFlag())
            this.setPicking_code(srfdomain.getPicking_code());
        if(srfdomain.getPicking_type_entire_packsDirtyFlag())
            this.setPicking_type_entire_packs(srfdomain.getPicking_type_entire_packs());
        if(srfdomain.getCreated_production_idDirtyFlag())
            this.setCreated_production_id(srfdomain.getCreated_production_id());
        if(srfdomain.getConsume_unbuild_idDirtyFlag())
            this.setConsume_unbuild_id(srfdomain.getConsume_unbuild_id());
        if(srfdomain.getSale_line_idDirtyFlag())
            this.setSale_line_id(srfdomain.getSale_line_id());
        if(srfdomain.getOrigin_returned_move_idDirtyFlag())
            this.setOrigin_returned_move_id(srfdomain.getOrigin_returned_move_id());
        if(srfdomain.getPicking_type_idDirtyFlag())
            this.setPicking_type_id(srfdomain.getPicking_type_id());
        if(srfdomain.getOperation_idDirtyFlag())
            this.setOperation_id(srfdomain.getOperation_id());
        if(srfdomain.getBom_line_idDirtyFlag())
            this.setBom_line_id(srfdomain.getBom_line_id());
        if(srfdomain.getRepair_idDirtyFlag())
            this.setRepair_id(srfdomain.getRepair_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getUnbuild_idDirtyFlag())
            this.setUnbuild_id(srfdomain.getUnbuild_id());
        if(srfdomain.getProduction_idDirtyFlag())
            this.setProduction_id(srfdomain.getProduction_id());
        if(srfdomain.getCreated_purchase_line_idDirtyFlag())
            this.setCreated_purchase_line_id(srfdomain.getCreated_purchase_line_id());
        if(srfdomain.getWorkorder_idDirtyFlag())
            this.setWorkorder_id(srfdomain.getWorkorder_id());
        if(srfdomain.getProduct_uomDirtyFlag())
            this.setProduct_uom(srfdomain.getProduct_uom());
        if(srfdomain.getPicking_idDirtyFlag())
            this.setPicking_id(srfdomain.getPicking_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPackage_level_idDirtyFlag())
            this.setPackage_level_id(srfdomain.getPackage_level_id());
        if(srfdomain.getProduct_packagingDirtyFlag())
            this.setProduct_packaging(srfdomain.getProduct_packaging());
        if(srfdomain.getRestrict_partner_idDirtyFlag())
            this.setRestrict_partner_id(srfdomain.getRestrict_partner_id());
        if(srfdomain.getRule_idDirtyFlag())
            this.setRule_id(srfdomain.getRule_id());
        if(srfdomain.getWarehouse_idDirtyFlag())
            this.setWarehouse_id(srfdomain.getWarehouse_id());
        if(srfdomain.getLocation_dest_idDirtyFlag())
            this.setLocation_dest_id(srfdomain.getLocation_dest_id());
        if(srfdomain.getRaw_material_production_idDirtyFlag())
            this.setRaw_material_production_id(srfdomain.getRaw_material_production_id());
        if(srfdomain.getInventory_idDirtyFlag())
            this.setInventory_id(srfdomain.getInventory_id());
        if(srfdomain.getPurchase_line_idDirtyFlag())
            this.setPurchase_line_id(srfdomain.getPurchase_line_id());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Stock_moveDTO> fromDOPage(List<Stock_move> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_moveDTO> dtos=new ArrayList<Stock_moveDTO>();
        for(Stock_move domain : poPage) {
            Stock_moveDTO dto = new Stock_moveDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

