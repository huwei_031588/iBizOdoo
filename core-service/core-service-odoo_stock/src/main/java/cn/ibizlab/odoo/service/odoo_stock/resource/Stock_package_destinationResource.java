package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_package_destinationDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_destination;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_package_destinationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_destinationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_package_destination" })
@RestController
@RequestMapping("")
public class Stock_package_destinationResource {

    @Autowired
    private IStock_package_destinationService stock_package_destinationService;

    public IStock_package_destinationService getStock_package_destinationService() {
        return this.stock_package_destinationService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_package_destination" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_package_destinations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_package_destinationDTO> stock_package_destinationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_package_destination" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_package_destinations/{stock_package_destination_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_package_destination_id") Integer stock_package_destination_id) {
        Stock_package_destinationDTO stock_package_destinationdto = new Stock_package_destinationDTO();
		Stock_package_destination domain = new Stock_package_destination();
		stock_package_destinationdto.setId(stock_package_destination_id);
		domain.setId(stock_package_destination_id);
        Boolean rst = stock_package_destinationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_package_destination" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_package_destinations/{stock_package_destination_id}")

    public ResponseEntity<Stock_package_destinationDTO> update(@PathVariable("stock_package_destination_id") Integer stock_package_destination_id, @RequestBody Stock_package_destinationDTO stock_package_destinationdto) {
		Stock_package_destination domain = stock_package_destinationdto.toDO();
        domain.setId(stock_package_destination_id);
		stock_package_destinationService.update(domain);
		Stock_package_destinationDTO dto = new Stock_package_destinationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_package_destination" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_package_destinations/{stock_package_destination_id}")
    public ResponseEntity<Stock_package_destinationDTO> get(@PathVariable("stock_package_destination_id") Integer stock_package_destination_id) {
        Stock_package_destinationDTO dto = new Stock_package_destinationDTO();
        Stock_package_destination domain = stock_package_destinationService.get(stock_package_destination_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_package_destination" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_package_destinations/createBatch")
    public ResponseEntity<Boolean> createBatchStock_package_destination(@RequestBody List<Stock_package_destinationDTO> stock_package_destinationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_package_destination" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_package_destinations")

    public ResponseEntity<Stock_package_destinationDTO> create(@RequestBody Stock_package_destinationDTO stock_package_destinationdto) {
        Stock_package_destinationDTO dto = new Stock_package_destinationDTO();
        Stock_package_destination domain = stock_package_destinationdto.toDO();
		stock_package_destinationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_package_destination" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_package_destinations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_package_destinationDTO> stock_package_destinationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_package_destination" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_package_destinations/fetchdefault")
	public ResponseEntity<Page<Stock_package_destinationDTO>> fetchDefault(Stock_package_destinationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_package_destinationDTO> list = new ArrayList<Stock_package_destinationDTO>();
        
        Page<Stock_package_destination> domains = stock_package_destinationService.searchDefault(context) ;
        for(Stock_package_destination stock_package_destination : domains.getContent()){
            Stock_package_destinationDTO dto = new Stock_package_destinationDTO();
            dto.fromDO(stock_package_destination);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
