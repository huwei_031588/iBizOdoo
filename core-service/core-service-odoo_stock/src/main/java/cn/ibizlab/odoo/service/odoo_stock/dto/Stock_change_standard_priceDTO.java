package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_change_standard_price.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_standard_price;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_change_standard_priceDTO]
 */
public class Stock_change_standard_priceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Stock_change_standard_priceIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [COUNTERPART_ACCOUNT_ID_REQUIRED]
     *
     */
    @Stock_change_standard_priceCounterpart_account_id_requiredDefault(info = "默认规则")
    private String counterpart_account_id_required;

    @JsonIgnore
    private boolean counterpart_account_id_requiredDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_change_standard_priceCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [NEW_PRICE]
     *
     */
    @Stock_change_standard_priceNew_priceDefault(info = "默认规则")
    private Double new_price;

    @JsonIgnore
    private boolean new_priceDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_change_standard_price__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_change_standard_priceDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_change_standard_priceWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [COUNTERPART_ACCOUNT_ID_TEXT]
     *
     */
    @Stock_change_standard_priceCounterpart_account_id_textDefault(info = "默认规则")
    private String counterpart_account_id_text;

    @JsonIgnore
    private boolean counterpart_account_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_change_standard_priceCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_change_standard_priceWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_change_standard_priceWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_change_standard_priceCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [COUNTERPART_ACCOUNT_ID]
     *
     */
    @Stock_change_standard_priceCounterpart_account_idDefault(info = "默认规则")
    private Integer counterpart_account_id;

    @JsonIgnore
    private boolean counterpart_account_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [COUNTERPART_ACCOUNT_ID_REQUIRED]
     */
    @JsonProperty("counterpart_account_id_required")
    public String getCounterpart_account_id_required(){
        return counterpart_account_id_required ;
    }

    /**
     * 设置 [COUNTERPART_ACCOUNT_ID_REQUIRED]
     */
    @JsonProperty("counterpart_account_id_required")
    public void setCounterpart_account_id_required(String  counterpart_account_id_required){
        this.counterpart_account_id_required = counterpart_account_id_required ;
        this.counterpart_account_id_requiredDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTERPART_ACCOUNT_ID_REQUIRED]脏标记
     */
    @JsonIgnore
    public boolean getCounterpart_account_id_requiredDirtyFlag(){
        return counterpart_account_id_requiredDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [NEW_PRICE]
     */
    @JsonProperty("new_price")
    public Double getNew_price(){
        return new_price ;
    }

    /**
     * 设置 [NEW_PRICE]
     */
    @JsonProperty("new_price")
    public void setNew_price(Double  new_price){
        this.new_price = new_price ;
        this.new_priceDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getNew_priceDirtyFlag(){
        return new_priceDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [COUNTERPART_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("counterpart_account_id_text")
    public String getCounterpart_account_id_text(){
        return counterpart_account_id_text ;
    }

    /**
     * 设置 [COUNTERPART_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("counterpart_account_id_text")
    public void setCounterpart_account_id_text(String  counterpart_account_id_text){
        this.counterpart_account_id_text = counterpart_account_id_text ;
        this.counterpart_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTERPART_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCounterpart_account_id_textDirtyFlag(){
        return counterpart_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [COUNTERPART_ACCOUNT_ID]
     */
    @JsonProperty("counterpart_account_id")
    public Integer getCounterpart_account_id(){
        return counterpart_account_id ;
    }

    /**
     * 设置 [COUNTERPART_ACCOUNT_ID]
     */
    @JsonProperty("counterpart_account_id")
    public void setCounterpart_account_id(Integer  counterpart_account_id){
        this.counterpart_account_id = counterpart_account_id ;
        this.counterpart_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTERPART_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getCounterpart_account_idDirtyFlag(){
        return counterpart_account_idDirtyFlag ;
    }



    public Stock_change_standard_price toDO() {
        Stock_change_standard_price srfdomain = new Stock_change_standard_price();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCounterpart_account_id_requiredDirtyFlag())
            srfdomain.setCounterpart_account_id_required(counterpart_account_id_required);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getNew_priceDirtyFlag())
            srfdomain.setNew_price(new_price);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCounterpart_account_id_textDirtyFlag())
            srfdomain.setCounterpart_account_id_text(counterpart_account_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCounterpart_account_idDirtyFlag())
            srfdomain.setCounterpart_account_id(counterpart_account_id);

        return srfdomain;
    }

    public void fromDO(Stock_change_standard_price srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCounterpart_account_id_requiredDirtyFlag())
            this.setCounterpart_account_id_required(srfdomain.getCounterpart_account_id_required());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getNew_priceDirtyFlag())
            this.setNew_price(srfdomain.getNew_price());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCounterpart_account_id_textDirtyFlag())
            this.setCounterpart_account_id_text(srfdomain.getCounterpart_account_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCounterpart_account_idDirtyFlag())
            this.setCounterpart_account_id(srfdomain.getCounterpart_account_id());

    }

    public List<Stock_change_standard_priceDTO> fromDOPage(List<Stock_change_standard_price> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_change_standard_priceDTO> dtos=new ArrayList<Stock_change_standard_priceDTO>();
        for(Stock_change_standard_price domain : poPage) {
            Stock_change_standard_priceDTO dto = new Stock_change_standard_priceDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

