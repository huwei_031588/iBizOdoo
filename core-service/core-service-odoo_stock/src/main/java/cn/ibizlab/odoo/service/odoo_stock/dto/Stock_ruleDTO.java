package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_rule.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rule;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_ruleDTO]
 */
public class Stock_ruleDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Stock_ruleSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [PROCURE_METHOD]
     *
     */
    @Stock_ruleProcure_methodDefault(info = "默认规则")
    private String procure_method;

    @JsonIgnore
    private boolean procure_methodDirtyFlag;

    /**
     * 属性 [PROPAGATE]
     *
     */
    @Stock_rulePropagateDefault(info = "默认规则")
    private String propagate;

    @JsonIgnore
    private boolean propagateDirtyFlag;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @Stock_ruleGroup_idDefault(info = "默认规则")
    private Integer group_id;

    @JsonIgnore
    private boolean group_idDirtyFlag;

    /**
     * 属性 [RULE_MESSAGE]
     *
     */
    @Stock_ruleRule_messageDefault(info = "默认规则")
    private String rule_message;

    @JsonIgnore
    private boolean rule_messageDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_ruleCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Stock_ruleNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [AUTO]
     *
     */
    @Stock_ruleAutoDefault(info = "默认规则")
    private String auto;

    @JsonIgnore
    private boolean autoDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_ruleWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DELAY]
     *
     */
    @Stock_ruleDelayDefault(info = "默认规则")
    private Integer delay;

    @JsonIgnore
    private boolean delayDirtyFlag;

    /**
     * 属性 [GROUP_PROPAGATION_OPTION]
     *
     */
    @Stock_ruleGroup_propagation_optionDefault(info = "默认规则")
    private String group_propagation_option;

    @JsonIgnore
    private boolean group_propagation_optionDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Stock_ruleActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_ruleDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_rule__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ACTION]
     *
     */
    @Stock_ruleActionDefault(info = "默认规则")
    private String action;

    @JsonIgnore
    private boolean actionDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_ruleIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Stock_ruleCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_ruleCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PARTNER_ADDRESS_ID_TEXT]
     *
     */
    @Stock_rulePartner_address_id_textDefault(info = "默认规则")
    private String partner_address_id_text;

    @JsonIgnore
    private boolean partner_address_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Stock_ruleLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [ROUTE_ID_TEXT]
     *
     */
    @Stock_ruleRoute_id_textDefault(info = "默认规则")
    private String route_id_text;

    @JsonIgnore
    private boolean route_id_textDirtyFlag;

    /**
     * 属性 [ROUTE_SEQUENCE]
     *
     */
    @Stock_ruleRoute_sequenceDefault(info = "默认规则")
    private Integer route_sequence;

    @JsonIgnore
    private boolean route_sequenceDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @Stock_rulePicking_type_id_textDefault(info = "默认规则")
    private String picking_type_id_text;

    @JsonIgnore
    private boolean picking_type_id_textDirtyFlag;

    /**
     * 属性 [PROPAGATE_WAREHOUSE_ID_TEXT]
     *
     */
    @Stock_rulePropagate_warehouse_id_textDefault(info = "默认规则")
    private String propagate_warehouse_id_text;

    @JsonIgnore
    private boolean propagate_warehouse_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_ruleWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [LOCATION_SRC_ID_TEXT]
     *
     */
    @Stock_ruleLocation_src_id_textDefault(info = "默认规则")
    private String location_src_id_text;

    @JsonIgnore
    private boolean location_src_id_textDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @Stock_ruleWarehouse_id_textDefault(info = "默认规则")
    private String warehouse_id_text;

    @JsonIgnore
    private boolean warehouse_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Stock_ruleLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [PARTNER_ADDRESS_ID]
     *
     */
    @Stock_rulePartner_address_idDefault(info = "默认规则")
    private Integer partner_address_id;

    @JsonIgnore
    private boolean partner_address_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Stock_ruleCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @Stock_ruleWarehouse_idDefault(info = "默认规则")
    private Integer warehouse_id;

    @JsonIgnore
    private boolean warehouse_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_ruleCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @Stock_rulePicking_type_idDefault(info = "默认规则")
    private Integer picking_type_id;

    @JsonIgnore
    private boolean picking_type_idDirtyFlag;

    /**
     * 属性 [ROUTE_ID]
     *
     */
    @Stock_ruleRoute_idDefault(info = "默认规则")
    private Integer route_id;

    @JsonIgnore
    private boolean route_idDirtyFlag;

    /**
     * 属性 [LOCATION_SRC_ID]
     *
     */
    @Stock_ruleLocation_src_idDefault(info = "默认规则")
    private Integer location_src_id;

    @JsonIgnore
    private boolean location_src_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_ruleWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PROPAGATE_WAREHOUSE_ID]
     *
     */
    @Stock_rulePropagate_warehouse_idDefault(info = "默认规则")
    private Integer propagate_warehouse_id;

    @JsonIgnore
    private boolean propagate_warehouse_idDirtyFlag;


    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [PROCURE_METHOD]
     */
    @JsonProperty("procure_method")
    public String getProcure_method(){
        return procure_method ;
    }

    /**
     * 设置 [PROCURE_METHOD]
     */
    @JsonProperty("procure_method")
    public void setProcure_method(String  procure_method){
        this.procure_method = procure_method ;
        this.procure_methodDirtyFlag = true ;
    }

    /**
     * 获取 [PROCURE_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getProcure_methodDirtyFlag(){
        return procure_methodDirtyFlag ;
    }

    /**
     * 获取 [PROPAGATE]
     */
    @JsonProperty("propagate")
    public String getPropagate(){
        return propagate ;
    }

    /**
     * 设置 [PROPAGATE]
     */
    @JsonProperty("propagate")
    public void setPropagate(String  propagate){
        this.propagate = propagate ;
        this.propagateDirtyFlag = true ;
    }

    /**
     * 获取 [PROPAGATE]脏标记
     */
    @JsonIgnore
    public boolean getPropagateDirtyFlag(){
        return propagateDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return group_id ;
    }

    /**
     * 设置 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return group_idDirtyFlag ;
    }

    /**
     * 获取 [RULE_MESSAGE]
     */
    @JsonProperty("rule_message")
    public String getRule_message(){
        return rule_message ;
    }

    /**
     * 设置 [RULE_MESSAGE]
     */
    @JsonProperty("rule_message")
    public void setRule_message(String  rule_message){
        this.rule_message = rule_message ;
        this.rule_messageDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_MESSAGE]脏标记
     */
    @JsonIgnore
    public boolean getRule_messageDirtyFlag(){
        return rule_messageDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [AUTO]
     */
    @JsonProperty("auto")
    public String getAuto(){
        return auto ;
    }

    /**
     * 设置 [AUTO]
     */
    @JsonProperty("auto")
    public void setAuto(String  auto){
        this.auto = auto ;
        this.autoDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO]脏标记
     */
    @JsonIgnore
    public boolean getAutoDirtyFlag(){
        return autoDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DELAY]
     */
    @JsonProperty("delay")
    public Integer getDelay(){
        return delay ;
    }

    /**
     * 设置 [DELAY]
     */
    @JsonProperty("delay")
    public void setDelay(Integer  delay){
        this.delay = delay ;
        this.delayDirtyFlag = true ;
    }

    /**
     * 获取 [DELAY]脏标记
     */
    @JsonIgnore
    public boolean getDelayDirtyFlag(){
        return delayDirtyFlag ;
    }

    /**
     * 获取 [GROUP_PROPAGATION_OPTION]
     */
    @JsonProperty("group_propagation_option")
    public String getGroup_propagation_option(){
        return group_propagation_option ;
    }

    /**
     * 设置 [GROUP_PROPAGATION_OPTION]
     */
    @JsonProperty("group_propagation_option")
    public void setGroup_propagation_option(String  group_propagation_option){
        this.group_propagation_option = group_propagation_option ;
        this.group_propagation_optionDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_PROPAGATION_OPTION]脏标记
     */
    @JsonIgnore
    public boolean getGroup_propagation_optionDirtyFlag(){
        return group_propagation_optionDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ACTION]
     */
    @JsonProperty("action")
    public String getAction(){
        return action ;
    }

    /**
     * 设置 [ACTION]
     */
    @JsonProperty("action")
    public void setAction(String  action){
        this.action = action ;
        this.actionDirtyFlag = true ;
    }

    /**
     * 获取 [ACTION]脏标记
     */
    @JsonIgnore
    public boolean getActionDirtyFlag(){
        return actionDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_ID_TEXT]
     */
    @JsonProperty("partner_address_id_text")
    public String getPartner_address_id_text(){
        return partner_address_id_text ;
    }

    /**
     * 设置 [PARTNER_ADDRESS_ID_TEXT]
     */
    @JsonProperty("partner_address_id_text")
    public void setPartner_address_id_text(String  partner_address_id_text){
        this.partner_address_id_text = partner_address_id_text ;
        this.partner_address_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_id_textDirtyFlag(){
        return partner_address_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_ID_TEXT]
     */
    @JsonProperty("route_id_text")
    public String getRoute_id_text(){
        return route_id_text ;
    }

    /**
     * 设置 [ROUTE_ID_TEXT]
     */
    @JsonProperty("route_id_text")
    public void setRoute_id_text(String  route_id_text){
        this.route_id_text = route_id_text ;
        this.route_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRoute_id_textDirtyFlag(){
        return route_id_textDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_SEQUENCE]
     */
    @JsonProperty("route_sequence")
    public Integer getRoute_sequence(){
        return route_sequence ;
    }

    /**
     * 设置 [ROUTE_SEQUENCE]
     */
    @JsonProperty("route_sequence")
    public void setRoute_sequence(Integer  route_sequence){
        this.route_sequence = route_sequence ;
        this.route_sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getRoute_sequenceDirtyFlag(){
        return route_sequenceDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return picking_type_id_text ;
    }

    /**
     * 设置 [PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return picking_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPAGATE_WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("propagate_warehouse_id_text")
    public String getPropagate_warehouse_id_text(){
        return propagate_warehouse_id_text ;
    }

    /**
     * 设置 [PROPAGATE_WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("propagate_warehouse_id_text")
    public void setPropagate_warehouse_id_text(String  propagate_warehouse_id_text){
        this.propagate_warehouse_id_text = propagate_warehouse_id_text ;
        this.propagate_warehouse_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPAGATE_WAREHOUSE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPropagate_warehouse_id_textDirtyFlag(){
        return propagate_warehouse_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_SRC_ID_TEXT]
     */
    @JsonProperty("location_src_id_text")
    public String getLocation_src_id_text(){
        return location_src_id_text ;
    }

    /**
     * 设置 [LOCATION_SRC_ID_TEXT]
     */
    @JsonProperty("location_src_id_text")
    public void setLocation_src_id_text(String  location_src_id_text){
        this.location_src_id_text = location_src_id_text ;
        this.location_src_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_SRC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_src_id_textDirtyFlag(){
        return location_src_id_textDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return warehouse_id_text ;
    }

    /**
     * 设置 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return warehouse_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_ID]
     */
    @JsonProperty("partner_address_id")
    public Integer getPartner_address_id(){
        return partner_address_id ;
    }

    /**
     * 设置 [PARTNER_ADDRESS_ID]
     */
    @JsonProperty("partner_address_id")
    public void setPartner_address_id(Integer  partner_address_id){
        this.partner_address_id = partner_address_id ;
        this.partner_address_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ADDRESS_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_address_idDirtyFlag(){
        return partner_address_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return warehouse_id ;
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return warehouse_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ID]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return picking_type_id ;
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return picking_type_idDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_ID]
     */
    @JsonProperty("route_id")
    public Integer getRoute_id(){
        return route_id ;
    }

    /**
     * 设置 [ROUTE_ID]
     */
    @JsonProperty("route_id")
    public void setRoute_id(Integer  route_id){
        this.route_id = route_id ;
        this.route_idDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_ID]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idDirtyFlag(){
        return route_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_SRC_ID]
     */
    @JsonProperty("location_src_id")
    public Integer getLocation_src_id(){
        return location_src_id ;
    }

    /**
     * 设置 [LOCATION_SRC_ID]
     */
    @JsonProperty("location_src_id")
    public void setLocation_src_id(Integer  location_src_id){
        this.location_src_id = location_src_id ;
        this.location_src_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_SRC_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_src_idDirtyFlag(){
        return location_src_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PROPAGATE_WAREHOUSE_ID]
     */
    @JsonProperty("propagate_warehouse_id")
    public Integer getPropagate_warehouse_id(){
        return propagate_warehouse_id ;
    }

    /**
     * 设置 [PROPAGATE_WAREHOUSE_ID]
     */
    @JsonProperty("propagate_warehouse_id")
    public void setPropagate_warehouse_id(Integer  propagate_warehouse_id){
        this.propagate_warehouse_id = propagate_warehouse_id ;
        this.propagate_warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPAGATE_WAREHOUSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPropagate_warehouse_idDirtyFlag(){
        return propagate_warehouse_idDirtyFlag ;
    }



    public Stock_rule toDO() {
        Stock_rule srfdomain = new Stock_rule();
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getProcure_methodDirtyFlag())
            srfdomain.setProcure_method(procure_method);
        if(getPropagateDirtyFlag())
            srfdomain.setPropagate(propagate);
        if(getGroup_idDirtyFlag())
            srfdomain.setGroup_id(group_id);
        if(getRule_messageDirtyFlag())
            srfdomain.setRule_message(rule_message);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getAutoDirtyFlag())
            srfdomain.setAuto(auto);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDelayDirtyFlag())
            srfdomain.setDelay(delay);
        if(getGroup_propagation_optionDirtyFlag())
            srfdomain.setGroup_propagation_option(group_propagation_option);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getActionDirtyFlag())
            srfdomain.setAction(action);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getPartner_address_id_textDirtyFlag())
            srfdomain.setPartner_address_id_text(partner_address_id_text);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getRoute_id_textDirtyFlag())
            srfdomain.setRoute_id_text(route_id_text);
        if(getRoute_sequenceDirtyFlag())
            srfdomain.setRoute_sequence(route_sequence);
        if(getPicking_type_id_textDirtyFlag())
            srfdomain.setPicking_type_id_text(picking_type_id_text);
        if(getPropagate_warehouse_id_textDirtyFlag())
            srfdomain.setPropagate_warehouse_id_text(propagate_warehouse_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getLocation_src_id_textDirtyFlag())
            srfdomain.setLocation_src_id_text(location_src_id_text);
        if(getWarehouse_id_textDirtyFlag())
            srfdomain.setWarehouse_id_text(warehouse_id_text);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getPartner_address_idDirtyFlag())
            srfdomain.setPartner_address_id(partner_address_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getWarehouse_idDirtyFlag())
            srfdomain.setWarehouse_id(warehouse_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPicking_type_idDirtyFlag())
            srfdomain.setPicking_type_id(picking_type_id);
        if(getRoute_idDirtyFlag())
            srfdomain.setRoute_id(route_id);
        if(getLocation_src_idDirtyFlag())
            srfdomain.setLocation_src_id(location_src_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPropagate_warehouse_idDirtyFlag())
            srfdomain.setPropagate_warehouse_id(propagate_warehouse_id);

        return srfdomain;
    }

    public void fromDO(Stock_rule srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getProcure_methodDirtyFlag())
            this.setProcure_method(srfdomain.getProcure_method());
        if(srfdomain.getPropagateDirtyFlag())
            this.setPropagate(srfdomain.getPropagate());
        if(srfdomain.getGroup_idDirtyFlag())
            this.setGroup_id(srfdomain.getGroup_id());
        if(srfdomain.getRule_messageDirtyFlag())
            this.setRule_message(srfdomain.getRule_message());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getAutoDirtyFlag())
            this.setAuto(srfdomain.getAuto());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDelayDirtyFlag())
            this.setDelay(srfdomain.getDelay());
        if(srfdomain.getGroup_propagation_optionDirtyFlag())
            this.setGroup_propagation_option(srfdomain.getGroup_propagation_option());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getActionDirtyFlag())
            this.setAction(srfdomain.getAction());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getPartner_address_id_textDirtyFlag())
            this.setPartner_address_id_text(srfdomain.getPartner_address_id_text());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getRoute_id_textDirtyFlag())
            this.setRoute_id_text(srfdomain.getRoute_id_text());
        if(srfdomain.getRoute_sequenceDirtyFlag())
            this.setRoute_sequence(srfdomain.getRoute_sequence());
        if(srfdomain.getPicking_type_id_textDirtyFlag())
            this.setPicking_type_id_text(srfdomain.getPicking_type_id_text());
        if(srfdomain.getPropagate_warehouse_id_textDirtyFlag())
            this.setPropagate_warehouse_id_text(srfdomain.getPropagate_warehouse_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getLocation_src_id_textDirtyFlag())
            this.setLocation_src_id_text(srfdomain.getLocation_src_id_text());
        if(srfdomain.getWarehouse_id_textDirtyFlag())
            this.setWarehouse_id_text(srfdomain.getWarehouse_id_text());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getPartner_address_idDirtyFlag())
            this.setPartner_address_id(srfdomain.getPartner_address_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getWarehouse_idDirtyFlag())
            this.setWarehouse_id(srfdomain.getWarehouse_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPicking_type_idDirtyFlag())
            this.setPicking_type_id(srfdomain.getPicking_type_id());
        if(srfdomain.getRoute_idDirtyFlag())
            this.setRoute_id(srfdomain.getRoute_id());
        if(srfdomain.getLocation_src_idDirtyFlag())
            this.setLocation_src_id(srfdomain.getLocation_src_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPropagate_warehouse_idDirtyFlag())
            this.setPropagate_warehouse_id(srfdomain.getPropagate_warehouse_id());

    }

    public List<Stock_ruleDTO> fromDOPage(List<Stock_rule> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_ruleDTO> dtos=new ArrayList<Stock_ruleDTO>();
        for(Stock_rule domain : poPage) {
            Stock_ruleDTO dto = new Stock_ruleDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

