package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_warehouse_orderpointDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warehouse_orderpointService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouse_orderpointSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_warehouse_orderpoint" })
@RestController
@RequestMapping("")
public class Stock_warehouse_orderpointResource {

    @Autowired
    private IStock_warehouse_orderpointService stock_warehouse_orderpointService;

    public IStock_warehouse_orderpointService getStock_warehouse_orderpointService() {
        return this.stock_warehouse_orderpointService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_warehouse_orderpoint" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warehouse_orderpoints/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_warehouse_orderpointDTO> stock_warehouse_orderpointdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_warehouse_orderpoint" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warehouse_orderpoints/createBatch")
    public ResponseEntity<Boolean> createBatchStock_warehouse_orderpoint(@RequestBody List<Stock_warehouse_orderpointDTO> stock_warehouse_orderpointdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_warehouse_orderpoint" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warehouse_orderpoints/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warehouse_orderpointDTO> stock_warehouse_orderpointdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_warehouse_orderpoint" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warehouse_orderpoints/{stock_warehouse_orderpoint_id}")

    public ResponseEntity<Stock_warehouse_orderpointDTO> update(@PathVariable("stock_warehouse_orderpoint_id") Integer stock_warehouse_orderpoint_id, @RequestBody Stock_warehouse_orderpointDTO stock_warehouse_orderpointdto) {
		Stock_warehouse_orderpoint domain = stock_warehouse_orderpointdto.toDO();
        domain.setId(stock_warehouse_orderpoint_id);
		stock_warehouse_orderpointService.update(domain);
		Stock_warehouse_orderpointDTO dto = new Stock_warehouse_orderpointDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_warehouse_orderpoint" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warehouse_orderpoints/{stock_warehouse_orderpoint_id}")
    public ResponseEntity<Stock_warehouse_orderpointDTO> get(@PathVariable("stock_warehouse_orderpoint_id") Integer stock_warehouse_orderpoint_id) {
        Stock_warehouse_orderpointDTO dto = new Stock_warehouse_orderpointDTO();
        Stock_warehouse_orderpoint domain = stock_warehouse_orderpointService.get(stock_warehouse_orderpoint_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_warehouse_orderpoint" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warehouse_orderpoints/{stock_warehouse_orderpoint_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_warehouse_orderpoint_id") Integer stock_warehouse_orderpoint_id) {
        Stock_warehouse_orderpointDTO stock_warehouse_orderpointdto = new Stock_warehouse_orderpointDTO();
		Stock_warehouse_orderpoint domain = new Stock_warehouse_orderpoint();
		stock_warehouse_orderpointdto.setId(stock_warehouse_orderpoint_id);
		domain.setId(stock_warehouse_orderpoint_id);
        Boolean rst = stock_warehouse_orderpointService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_warehouse_orderpoint" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warehouse_orderpoints")

    public ResponseEntity<Stock_warehouse_orderpointDTO> create(@RequestBody Stock_warehouse_orderpointDTO stock_warehouse_orderpointdto) {
        Stock_warehouse_orderpointDTO dto = new Stock_warehouse_orderpointDTO();
        Stock_warehouse_orderpoint domain = stock_warehouse_orderpointdto.toDO();
		stock_warehouse_orderpointService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_warehouse_orderpoint" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_warehouse_orderpoints/fetchdefault")
	public ResponseEntity<Page<Stock_warehouse_orderpointDTO>> fetchDefault(Stock_warehouse_orderpointSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_warehouse_orderpointDTO> list = new ArrayList<Stock_warehouse_orderpointDTO>();
        
        Page<Stock_warehouse_orderpoint> domains = stock_warehouse_orderpointService.searchDefault(context) ;
        for(Stock_warehouse_orderpoint stock_warehouse_orderpoint : domains.getContent()){
            Stock_warehouse_orderpointDTO dto = new Stock_warehouse_orderpointDTO();
            dto.fromDO(stock_warehouse_orderpoint);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
