package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_locationDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_locationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_locationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_location" })
@RestController
@RequestMapping("")
public class Stock_locationResource {

    @Autowired
    private IStock_locationService stock_locationService;

    public IStock_locationService getStock_locationService() {
        return this.stock_locationService;
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_location" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_locations/{stock_location_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_location_id") Integer stock_location_id) {
        Stock_locationDTO stock_locationdto = new Stock_locationDTO();
		Stock_location domain = new Stock_location();
		stock_locationdto.setId(stock_location_id);
		domain.setId(stock_location_id);
        Boolean rst = stock_locationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_location" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_locations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_locationDTO> stock_locationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_location" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_locations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_locationDTO> stock_locationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_location" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_locations")

    public ResponseEntity<Stock_locationDTO> create(@RequestBody Stock_locationDTO stock_locationdto) {
        Stock_locationDTO dto = new Stock_locationDTO();
        Stock_location domain = stock_locationdto.toDO();
		stock_locationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_location" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_locations/{stock_location_id}")
    public ResponseEntity<Stock_locationDTO> get(@PathVariable("stock_location_id") Integer stock_location_id) {
        Stock_locationDTO dto = new Stock_locationDTO();
        Stock_location domain = stock_locationService.get(stock_location_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_location" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_locations/{stock_location_id}")

    public ResponseEntity<Stock_locationDTO> update(@PathVariable("stock_location_id") Integer stock_location_id, @RequestBody Stock_locationDTO stock_locationdto) {
		Stock_location domain = stock_locationdto.toDO();
        domain.setId(stock_location_id);
		stock_locationService.update(domain);
		Stock_locationDTO dto = new Stock_locationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_location" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_locations/createBatch")
    public ResponseEntity<Boolean> createBatchStock_location(@RequestBody List<Stock_locationDTO> stock_locationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_location" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_locations/fetchdefault")
	public ResponseEntity<Page<Stock_locationDTO>> fetchDefault(Stock_locationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_locationDTO> list = new ArrayList<Stock_locationDTO>();
        
        Page<Stock_location> domains = stock_locationService.searchDefault(context) ;
        for(Stock_location stock_location : domains.getContent()){
            Stock_locationDTO dto = new Stock_locationDTO();
            dto.fromDO(stock_location);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
