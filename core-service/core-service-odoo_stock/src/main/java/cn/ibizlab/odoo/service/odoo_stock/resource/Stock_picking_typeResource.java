package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_picking_typeDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_picking_typeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_picking_typeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_picking_type" })
@RestController
@RequestMapping("")
public class Stock_picking_typeResource {

    @Autowired
    private IStock_picking_typeService stock_picking_typeService;

    public IStock_picking_typeService getStock_picking_typeService() {
        return this.stock_picking_typeService;
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_picking_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_picking_types/{stock_picking_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_picking_type_id") Integer stock_picking_type_id) {
        Stock_picking_typeDTO stock_picking_typedto = new Stock_picking_typeDTO();
		Stock_picking_type domain = new Stock_picking_type();
		stock_picking_typedto.setId(stock_picking_type_id);
		domain.setId(stock_picking_type_id);
        Boolean rst = stock_picking_typeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_picking_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_picking_types/{stock_picking_type_id}")

    public ResponseEntity<Stock_picking_typeDTO> update(@PathVariable("stock_picking_type_id") Integer stock_picking_type_id, @RequestBody Stock_picking_typeDTO stock_picking_typedto) {
		Stock_picking_type domain = stock_picking_typedto.toDO();
        domain.setId(stock_picking_type_id);
		stock_picking_typeService.update(domain);
		Stock_picking_typeDTO dto = new Stock_picking_typeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_picking_type" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_picking_types/createBatch")
    public ResponseEntity<Boolean> createBatchStock_picking_type(@RequestBody List<Stock_picking_typeDTO> stock_picking_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_picking_type" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_picking_types/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_picking_typeDTO> stock_picking_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_picking_type" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_picking_types/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_picking_typeDTO> stock_picking_typedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_picking_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_picking_types")

    public ResponseEntity<Stock_picking_typeDTO> create(@RequestBody Stock_picking_typeDTO stock_picking_typedto) {
        Stock_picking_typeDTO dto = new Stock_picking_typeDTO();
        Stock_picking_type domain = stock_picking_typedto.toDO();
		stock_picking_typeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_picking_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_picking_types/{stock_picking_type_id}")
    public ResponseEntity<Stock_picking_typeDTO> get(@PathVariable("stock_picking_type_id") Integer stock_picking_type_id) {
        Stock_picking_typeDTO dto = new Stock_picking_typeDTO();
        Stock_picking_type domain = stock_picking_typeService.get(stock_picking_type_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_picking_type" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_picking_types/fetchdefault")
	public ResponseEntity<Page<Stock_picking_typeDTO>> fetchDefault(Stock_picking_typeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_picking_typeDTO> list = new ArrayList<Stock_picking_typeDTO>();
        
        Page<Stock_picking_type> domains = stock_picking_typeService.searchDefault(context) ;
        for(Stock_picking_type stock_picking_type : domains.getContent()){
            Stock_picking_typeDTO dto = new Stock_picking_typeDTO();
            dto.fromDO(stock_picking_type);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
