package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_scheduler_computeDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scheduler_compute;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_scheduler_computeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scheduler_computeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_scheduler_compute" })
@RestController
@RequestMapping("")
public class Stock_scheduler_computeResource {

    @Autowired
    private IStock_scheduler_computeService stock_scheduler_computeService;

    public IStock_scheduler_computeService getStock_scheduler_computeService() {
        return this.stock_scheduler_computeService;
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_scheduler_compute" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_scheduler_computes/{stock_scheduler_compute_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_scheduler_compute_id") Integer stock_scheduler_compute_id) {
        Stock_scheduler_computeDTO stock_scheduler_computedto = new Stock_scheduler_computeDTO();
		Stock_scheduler_compute domain = new Stock_scheduler_compute();
		stock_scheduler_computedto.setId(stock_scheduler_compute_id);
		domain.setId(stock_scheduler_compute_id);
        Boolean rst = stock_scheduler_computeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_scheduler_compute" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_scheduler_computes/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_scheduler_computeDTO> stock_scheduler_computedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_scheduler_compute" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_scheduler_computes/{stock_scheduler_compute_id}")

    public ResponseEntity<Stock_scheduler_computeDTO> update(@PathVariable("stock_scheduler_compute_id") Integer stock_scheduler_compute_id, @RequestBody Stock_scheduler_computeDTO stock_scheduler_computedto) {
		Stock_scheduler_compute domain = stock_scheduler_computedto.toDO();
        domain.setId(stock_scheduler_compute_id);
		stock_scheduler_computeService.update(domain);
		Stock_scheduler_computeDTO dto = new Stock_scheduler_computeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_scheduler_compute" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_scheduler_computes")

    public ResponseEntity<Stock_scheduler_computeDTO> create(@RequestBody Stock_scheduler_computeDTO stock_scheduler_computedto) {
        Stock_scheduler_computeDTO dto = new Stock_scheduler_computeDTO();
        Stock_scheduler_compute domain = stock_scheduler_computedto.toDO();
		stock_scheduler_computeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_scheduler_compute" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_scheduler_computes/createBatch")
    public ResponseEntity<Boolean> createBatchStock_scheduler_compute(@RequestBody List<Stock_scheduler_computeDTO> stock_scheduler_computedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_scheduler_compute" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_scheduler_computes/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_scheduler_computeDTO> stock_scheduler_computedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_scheduler_compute" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_scheduler_computes/{stock_scheduler_compute_id}")
    public ResponseEntity<Stock_scheduler_computeDTO> get(@PathVariable("stock_scheduler_compute_id") Integer stock_scheduler_compute_id) {
        Stock_scheduler_computeDTO dto = new Stock_scheduler_computeDTO();
        Stock_scheduler_compute domain = stock_scheduler_computeService.get(stock_scheduler_compute_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_scheduler_compute" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_scheduler_computes/fetchdefault")
	public ResponseEntity<Page<Stock_scheduler_computeDTO>> fetchDefault(Stock_scheduler_computeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_scheduler_computeDTO> list = new ArrayList<Stock_scheduler_computeDTO>();
        
        Page<Stock_scheduler_compute> domains = stock_scheduler_computeService.searchDefault(context) ;
        for(Stock_scheduler_compute stock_scheduler_compute : domains.getContent()){
            Stock_scheduler_computeDTO dto = new Stock_scheduler_computeDTO();
            dto.fromDO(stock_scheduler_compute);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
