package cn.ibizlab.odoo.service.odoo_calendar.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_calendar.dto.Calendar_alarm_managerDTO;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_alarm_managerService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarm_managerSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Calendar_alarm_manager" })
@RestController
@RequestMapping("")
public class Calendar_alarm_managerResource {

    @Autowired
    private ICalendar_alarm_managerService calendar_alarm_managerService;

    public ICalendar_alarm_managerService getCalendar_alarm_managerService() {
        return this.calendar_alarm_managerService;
    }

    @ApiOperation(value = "获取数据", tags = {"Calendar_alarm_manager" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_alarm_managers/{calendar_alarm_manager_id}")
    public ResponseEntity<Calendar_alarm_managerDTO> get(@PathVariable("calendar_alarm_manager_id") Integer calendar_alarm_manager_id) {
        Calendar_alarm_managerDTO dto = new Calendar_alarm_managerDTO();
        Calendar_alarm_manager domain = calendar_alarm_managerService.get(calendar_alarm_manager_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Calendar_alarm_manager" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_alarm_managers/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_alarm_managerDTO> calendar_alarm_managerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Calendar_alarm_manager" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_alarm_managers/{calendar_alarm_manager_id}")

    public ResponseEntity<Calendar_alarm_managerDTO> update(@PathVariable("calendar_alarm_manager_id") Integer calendar_alarm_manager_id, @RequestBody Calendar_alarm_managerDTO calendar_alarm_managerdto) {
		Calendar_alarm_manager domain = calendar_alarm_managerdto.toDO();
        domain.setId(calendar_alarm_manager_id);
		calendar_alarm_managerService.update(domain);
		Calendar_alarm_managerDTO dto = new Calendar_alarm_managerDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Calendar_alarm_manager" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_alarm_managers/createBatch")
    public ResponseEntity<Boolean> createBatchCalendar_alarm_manager(@RequestBody List<Calendar_alarm_managerDTO> calendar_alarm_managerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Calendar_alarm_manager" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_alarm_managers/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Calendar_alarm_managerDTO> calendar_alarm_managerdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Calendar_alarm_manager" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_alarm_managers/{calendar_alarm_manager_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_alarm_manager_id") Integer calendar_alarm_manager_id) {
        Calendar_alarm_managerDTO calendar_alarm_managerdto = new Calendar_alarm_managerDTO();
		Calendar_alarm_manager domain = new Calendar_alarm_manager();
		calendar_alarm_managerdto.setId(calendar_alarm_manager_id);
		domain.setId(calendar_alarm_manager_id);
        Boolean rst = calendar_alarm_managerService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Calendar_alarm_manager" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_alarm_managers")

    public ResponseEntity<Calendar_alarm_managerDTO> create(@RequestBody Calendar_alarm_managerDTO calendar_alarm_managerdto) {
        Calendar_alarm_managerDTO dto = new Calendar_alarm_managerDTO();
        Calendar_alarm_manager domain = calendar_alarm_managerdto.toDO();
		calendar_alarm_managerService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Calendar_alarm_manager" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_calendar/calendar_alarm_managers/fetchdefault")
	public ResponseEntity<Page<Calendar_alarm_managerDTO>> fetchDefault(Calendar_alarm_managerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Calendar_alarm_managerDTO> list = new ArrayList<Calendar_alarm_managerDTO>();
        
        Page<Calendar_alarm_manager> domains = calendar_alarm_managerService.searchDefault(context) ;
        for(Calendar_alarm_manager calendar_alarm_manager : domains.getContent()){
            Calendar_alarm_managerDTO dto = new Calendar_alarm_managerDTO();
            dto.fromDO(calendar_alarm_manager);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
