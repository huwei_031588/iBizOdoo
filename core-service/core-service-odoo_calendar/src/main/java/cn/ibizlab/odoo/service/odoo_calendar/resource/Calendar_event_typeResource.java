package cn.ibizlab.odoo.service.odoo_calendar.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_calendar.dto.Calendar_event_typeDTO;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_event_typeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_event_typeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Calendar_event_type" })
@RestController
@RequestMapping("")
public class Calendar_event_typeResource {

    @Autowired
    private ICalendar_event_typeService calendar_event_typeService;

    public ICalendar_event_typeService getCalendar_event_typeService() {
        return this.calendar_event_typeService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Calendar_event_type" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_event_types/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Calendar_event_typeDTO> calendar_event_typedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Calendar_event_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_event_types/{calendar_event_type_id}")
    public ResponseEntity<Calendar_event_typeDTO> get(@PathVariable("calendar_event_type_id") Integer calendar_event_type_id) {
        Calendar_event_typeDTO dto = new Calendar_event_typeDTO();
        Calendar_event_type domain = calendar_event_typeService.get(calendar_event_type_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Calendar_event_type" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_event_types/createBatch")
    public ResponseEntity<Boolean> createBatchCalendar_event_type(@RequestBody List<Calendar_event_typeDTO> calendar_event_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Calendar_event_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_event_types")

    public ResponseEntity<Calendar_event_typeDTO> create(@RequestBody Calendar_event_typeDTO calendar_event_typedto) {
        Calendar_event_typeDTO dto = new Calendar_event_typeDTO();
        Calendar_event_type domain = calendar_event_typedto.toDO();
		calendar_event_typeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Calendar_event_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_event_types/{calendar_event_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_event_type_id") Integer calendar_event_type_id) {
        Calendar_event_typeDTO calendar_event_typedto = new Calendar_event_typeDTO();
		Calendar_event_type domain = new Calendar_event_type();
		calendar_event_typedto.setId(calendar_event_type_id);
		domain.setId(calendar_event_type_id);
        Boolean rst = calendar_event_typeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Calendar_event_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_event_types/{calendar_event_type_id}")

    public ResponseEntity<Calendar_event_typeDTO> update(@PathVariable("calendar_event_type_id") Integer calendar_event_type_id, @RequestBody Calendar_event_typeDTO calendar_event_typedto) {
		Calendar_event_type domain = calendar_event_typedto.toDO();
        domain.setId(calendar_event_type_id);
		calendar_event_typeService.update(domain);
		Calendar_event_typeDTO dto = new Calendar_event_typeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Calendar_event_type" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_event_types/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_event_typeDTO> calendar_event_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Calendar_event_type" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_calendar/calendar_event_types/fetchdefault")
	public ResponseEntity<Page<Calendar_event_typeDTO>> fetchDefault(Calendar_event_typeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Calendar_event_typeDTO> list = new ArrayList<Calendar_event_typeDTO>();
        
        Page<Calendar_event_type> domains = calendar_event_typeService.searchDefault(context) ;
        for(Calendar_event_type calendar_event_type : domains.getContent()){
            Calendar_event_typeDTO dto = new Calendar_event_typeDTO();
            dto.fromDO(calendar_event_type);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
