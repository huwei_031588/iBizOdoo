package cn.ibizlab.odoo.service.odoo_calendar.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_calendar")
public class odoo_calendarRestConfiguration {

}
