package cn.ibizlab.odoo.service.odoo_rating.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_rating")
public class odoo_ratingRestConfiguration {

}
