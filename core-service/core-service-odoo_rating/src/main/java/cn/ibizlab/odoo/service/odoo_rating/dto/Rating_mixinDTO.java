package cn.ibizlab.odoo.service.odoo_rating.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_rating.valuerule.anno.rating_mixin.*;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Rating_mixinDTO]
 */
public class Rating_mixinDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RATING_LAST_FEEDBACK]
     *
     */
    @Rating_mixinRating_last_feedbackDefault(info = "默认规则")
    private String rating_last_feedback;

    @JsonIgnore
    private boolean rating_last_feedbackDirtyFlag;

    /**
     * 属性 [RATING_COUNT]
     *
     */
    @Rating_mixinRating_countDefault(info = "默认规则")
    private Integer rating_count;

    @JsonIgnore
    private boolean rating_countDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Rating_mixinIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Rating_mixin__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @Rating_mixinRating_idsDefault(info = "默认规则")
    private String rating_ids;

    @JsonIgnore
    private boolean rating_idsDirtyFlag;

    /**
     * 属性 [RATING_LAST_IMAGE]
     *
     */
    @Rating_mixinRating_last_imageDefault(info = "默认规则")
    private byte[] rating_last_image;

    @JsonIgnore
    private boolean rating_last_imageDirtyFlag;

    /**
     * 属性 [RATING_LAST_VALUE]
     *
     */
    @Rating_mixinRating_last_valueDefault(info = "默认规则")
    private Double rating_last_value;

    @JsonIgnore
    private boolean rating_last_valueDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Rating_mixinDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;


    /**
     * 获取 [RATING_LAST_FEEDBACK]
     */
    @JsonProperty("rating_last_feedback")
    public String getRating_last_feedback(){
        return rating_last_feedback ;
    }

    /**
     * 设置 [RATING_LAST_FEEDBACK]
     */
    @JsonProperty("rating_last_feedback")
    public void setRating_last_feedback(String  rating_last_feedback){
        this.rating_last_feedback = rating_last_feedback ;
        this.rating_last_feedbackDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_FEEDBACK]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_feedbackDirtyFlag(){
        return rating_last_feedbackDirtyFlag ;
    }

    /**
     * 获取 [RATING_COUNT]
     */
    @JsonProperty("rating_count")
    public Integer getRating_count(){
        return rating_count ;
    }

    /**
     * 设置 [RATING_COUNT]
     */
    @JsonProperty("rating_count")
    public void setRating_count(Integer  rating_count){
        this.rating_count = rating_count ;
        this.rating_countDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getRating_countDirtyFlag(){
        return rating_countDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [RATING_IDS]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return rating_ids ;
    }

    /**
     * 设置 [RATING_IDS]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return rating_idsDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_IMAGE]
     */
    @JsonProperty("rating_last_image")
    public byte[] getRating_last_image(){
        return rating_last_image ;
    }

    /**
     * 设置 [RATING_LAST_IMAGE]
     */
    @JsonProperty("rating_last_image")
    public void setRating_last_image(byte[]  rating_last_image){
        this.rating_last_image = rating_last_image ;
        this.rating_last_imageDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_imageDirtyFlag(){
        return rating_last_imageDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_VALUE]
     */
    @JsonProperty("rating_last_value")
    public Double getRating_last_value(){
        return rating_last_value ;
    }

    /**
     * 设置 [RATING_LAST_VALUE]
     */
    @JsonProperty("rating_last_value")
    public void setRating_last_value(Double  rating_last_value){
        this.rating_last_value = rating_last_value ;
        this.rating_last_valueDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_valueDirtyFlag(){
        return rating_last_valueDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }



    public Rating_mixin toDO() {
        Rating_mixin srfdomain = new Rating_mixin();
        if(getRating_last_feedbackDirtyFlag())
            srfdomain.setRating_last_feedback(rating_last_feedback);
        if(getRating_countDirtyFlag())
            srfdomain.setRating_count(rating_count);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getRating_idsDirtyFlag())
            srfdomain.setRating_ids(rating_ids);
        if(getRating_last_imageDirtyFlag())
            srfdomain.setRating_last_image(rating_last_image);
        if(getRating_last_valueDirtyFlag())
            srfdomain.setRating_last_value(rating_last_value);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);

        return srfdomain;
    }

    public void fromDO(Rating_mixin srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getRating_last_feedbackDirtyFlag())
            this.setRating_last_feedback(srfdomain.getRating_last_feedback());
        if(srfdomain.getRating_countDirtyFlag())
            this.setRating_count(srfdomain.getRating_count());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getRating_idsDirtyFlag())
            this.setRating_ids(srfdomain.getRating_ids());
        if(srfdomain.getRating_last_imageDirtyFlag())
            this.setRating_last_image(srfdomain.getRating_last_image());
        if(srfdomain.getRating_last_valueDirtyFlag())
            this.setRating_last_value(srfdomain.getRating_last_value());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());

    }

    public List<Rating_mixinDTO> fromDOPage(List<Rating_mixin> poPage)   {
        if(poPage == null)
            return null;
        List<Rating_mixinDTO> dtos=new ArrayList<Rating_mixinDTO>();
        for(Rating_mixin domain : poPage) {
            Rating_mixinDTO dto = new Rating_mixinDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

