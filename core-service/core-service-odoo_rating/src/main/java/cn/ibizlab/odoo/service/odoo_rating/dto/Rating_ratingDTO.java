package cn.ibizlab.odoo.service.odoo_rating.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_rating.valuerule.anno.rating_rating.*;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Rating_ratingDTO]
 */
public class Rating_ratingDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [FEEDBACK]
     *
     */
    @Rating_ratingFeedbackDefault(info = "默认规则")
    private String feedback;

    @JsonIgnore
    private boolean feedbackDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Rating_ratingDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [RATING]
     *
     */
    @Rating_ratingRatingDefault(info = "默认规则")
    private Double rating;

    @JsonIgnore
    private boolean ratingDirtyFlag;

    /**
     * 属性 [RATING_TEXT]
     *
     */
    @Rating_ratingRating_textDefault(info = "默认规则")
    private String rating_text;

    @JsonIgnore
    private boolean rating_textDirtyFlag;

    /**
     * 属性 [PARENT_RES_NAME]
     *
     */
    @Rating_ratingParent_res_nameDefault(info = "默认规则")
    private String parent_res_name;

    @JsonIgnore
    private boolean parent_res_nameDirtyFlag;

    /**
     * 属性 [PARENT_RES_ID]
     *
     */
    @Rating_ratingParent_res_idDefault(info = "默认规则")
    private Integer parent_res_id;

    @JsonIgnore
    private boolean parent_res_idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Rating_ratingCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [RES_NAME]
     *
     */
    @Rating_ratingRes_nameDefault(info = "默认规则")
    private String res_name;

    @JsonIgnore
    private boolean res_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Rating_rating__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PARENT_RES_MODEL_ID]
     *
     */
    @Rating_ratingParent_res_model_idDefault(info = "默认规则")
    private Integer parent_res_model_id;

    @JsonIgnore
    private boolean parent_res_model_idDirtyFlag;

    /**
     * 属性 [RES_ID]
     *
     */
    @Rating_ratingRes_idDefault(info = "默认规则")
    private Integer res_id;

    @JsonIgnore
    private boolean res_idDirtyFlag;

    /**
     * 属性 [CONSUMED]
     *
     */
    @Rating_ratingConsumedDefault(info = "默认规则")
    private String consumed;

    @JsonIgnore
    private boolean consumedDirtyFlag;

    /**
     * 属性 [RATING_IMAGE]
     *
     */
    @Rating_ratingRating_imageDefault(info = "默认规则")
    private byte[] rating_image;

    @JsonIgnore
    private boolean rating_imageDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Rating_ratingWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [RES_MODEL_ID]
     *
     */
    @Rating_ratingRes_model_idDefault(info = "默认规则")
    private Integer res_model_id;

    @JsonIgnore
    private boolean res_model_idDirtyFlag;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @Rating_ratingRes_modelDefault(info = "默认规则")
    private String res_model;

    @JsonIgnore
    private boolean res_modelDirtyFlag;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @Rating_ratingAccess_tokenDefault(info = "默认规则")
    private String access_token;

    @JsonIgnore
    private boolean access_tokenDirtyFlag;

    /**
     * 属性 [PARENT_RES_MODEL]
     *
     */
    @Rating_ratingParent_res_modelDefault(info = "默认规则")
    private String parent_res_model;

    @JsonIgnore
    private boolean parent_res_modelDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Rating_ratingIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Rating_ratingPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [RATED_PARTNER_ID_TEXT]
     *
     */
    @Rating_ratingRated_partner_id_textDefault(info = "默认规则")
    private String rated_partner_id_text;

    @JsonIgnore
    private boolean rated_partner_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Rating_ratingCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Rating_ratingWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Rating_ratingWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MESSAGE_ID]
     *
     */
    @Rating_ratingMessage_idDefault(info = "默认规则")
    private Integer message_id;

    @JsonIgnore
    private boolean message_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Rating_ratingPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [RATED_PARTNER_ID]
     *
     */
    @Rating_ratingRated_partner_idDefault(info = "默认规则")
    private Integer rated_partner_id;

    @JsonIgnore
    private boolean rated_partner_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Rating_ratingCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Rating_ratingWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [FEEDBACK]
     */
    @JsonProperty("feedback")
    public String getFeedback(){
        return feedback ;
    }

    /**
     * 设置 [FEEDBACK]
     */
    @JsonProperty("feedback")
    public void setFeedback(String  feedback){
        this.feedback = feedback ;
        this.feedbackDirtyFlag = true ;
    }

    /**
     * 获取 [FEEDBACK]脏标记
     */
    @JsonIgnore
    public boolean getFeedbackDirtyFlag(){
        return feedbackDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [RATING]
     */
    @JsonProperty("rating")
    public Double getRating(){
        return rating ;
    }

    /**
     * 设置 [RATING]
     */
    @JsonProperty("rating")
    public void setRating(Double  rating){
        this.rating = rating ;
        this.ratingDirtyFlag = true ;
    }

    /**
     * 获取 [RATING]脏标记
     */
    @JsonIgnore
    public boolean getRatingDirtyFlag(){
        return ratingDirtyFlag ;
    }

    /**
     * 获取 [RATING_TEXT]
     */
    @JsonProperty("rating_text")
    public String getRating_text(){
        return rating_text ;
    }

    /**
     * 设置 [RATING_TEXT]
     */
    @JsonProperty("rating_text")
    public void setRating_text(String  rating_text){
        this.rating_text = rating_text ;
        this.rating_textDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRating_textDirtyFlag(){
        return rating_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_RES_NAME]
     */
    @JsonProperty("parent_res_name")
    public String getParent_res_name(){
        return parent_res_name ;
    }

    /**
     * 设置 [PARENT_RES_NAME]
     */
    @JsonProperty("parent_res_name")
    public void setParent_res_name(String  parent_res_name){
        this.parent_res_name = parent_res_name ;
        this.parent_res_nameDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_RES_NAME]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_nameDirtyFlag(){
        return parent_res_nameDirtyFlag ;
    }

    /**
     * 获取 [PARENT_RES_ID]
     */
    @JsonProperty("parent_res_id")
    public Integer getParent_res_id(){
        return parent_res_id ;
    }

    /**
     * 设置 [PARENT_RES_ID]
     */
    @JsonProperty("parent_res_id")
    public void setParent_res_id(Integer  parent_res_id){
        this.parent_res_id = parent_res_id ;
        this.parent_res_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_RES_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_idDirtyFlag(){
        return parent_res_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [RES_NAME]
     */
    @JsonProperty("res_name")
    public String getRes_name(){
        return res_name ;
    }

    /**
     * 设置 [RES_NAME]
     */
    @JsonProperty("res_name")
    public void setRes_name(String  res_name){
        this.res_name = res_name ;
        this.res_nameDirtyFlag = true ;
    }

    /**
     * 获取 [RES_NAME]脏标记
     */
    @JsonIgnore
    public boolean getRes_nameDirtyFlag(){
        return res_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PARENT_RES_MODEL_ID]
     */
    @JsonProperty("parent_res_model_id")
    public Integer getParent_res_model_id(){
        return parent_res_model_id ;
    }

    /**
     * 设置 [PARENT_RES_MODEL_ID]
     */
    @JsonProperty("parent_res_model_id")
    public void setParent_res_model_id(Integer  parent_res_model_id){
        this.parent_res_model_id = parent_res_model_id ;
        this.parent_res_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_RES_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_model_idDirtyFlag(){
        return parent_res_model_idDirtyFlag ;
    }

    /**
     * 获取 [RES_ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return res_id ;
    }

    /**
     * 设置 [RES_ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return res_idDirtyFlag ;
    }

    /**
     * 获取 [CONSUMED]
     */
    @JsonProperty("consumed")
    public String getConsumed(){
        return consumed ;
    }

    /**
     * 设置 [CONSUMED]
     */
    @JsonProperty("consumed")
    public void setConsumed(String  consumed){
        this.consumed = consumed ;
        this.consumedDirtyFlag = true ;
    }

    /**
     * 获取 [CONSUMED]脏标记
     */
    @JsonIgnore
    public boolean getConsumedDirtyFlag(){
        return consumedDirtyFlag ;
    }

    /**
     * 获取 [RATING_IMAGE]
     */
    @JsonProperty("rating_image")
    public byte[] getRating_image(){
        return rating_image ;
    }

    /**
     * 设置 [RATING_IMAGE]
     */
    @JsonProperty("rating_image")
    public void setRating_image(byte[]  rating_image){
        this.rating_image = rating_image ;
        this.rating_imageDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getRating_imageDirtyFlag(){
        return rating_imageDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL_ID]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return res_model_id ;
    }

    /**
     * 设置 [RES_MODEL_ID]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return res_model_idDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return res_model ;
    }

    /**
     * 设置 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return res_modelDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return access_token ;
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return access_tokenDirtyFlag ;
    }

    /**
     * 获取 [PARENT_RES_MODEL]
     */
    @JsonProperty("parent_res_model")
    public String getParent_res_model(){
        return parent_res_model ;
    }

    /**
     * 设置 [PARENT_RES_MODEL]
     */
    @JsonProperty("parent_res_model")
    public void setParent_res_model(String  parent_res_model){
        this.parent_res_model = parent_res_model ;
        this.parent_res_modelDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_RES_MODEL]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_modelDirtyFlag(){
        return parent_res_modelDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [RATED_PARTNER_ID_TEXT]
     */
    @JsonProperty("rated_partner_id_text")
    public String getRated_partner_id_text(){
        return rated_partner_id_text ;
    }

    /**
     * 设置 [RATED_PARTNER_ID_TEXT]
     */
    @JsonProperty("rated_partner_id_text")
    public void setRated_partner_id_text(String  rated_partner_id_text){
        this.rated_partner_id_text = rated_partner_id_text ;
        this.rated_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RATED_PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRated_partner_id_textDirtyFlag(){
        return rated_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ID]
     */
    @JsonProperty("message_id")
    public Integer getMessage_id(){
        return message_id ;
    }

    /**
     * 设置 [MESSAGE_ID]
     */
    @JsonProperty("message_id")
    public void setMessage_id(Integer  message_id){
        this.message_id = message_id ;
        this.message_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idDirtyFlag(){
        return message_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [RATED_PARTNER_ID]
     */
    @JsonProperty("rated_partner_id")
    public Integer getRated_partner_id(){
        return rated_partner_id ;
    }

    /**
     * 设置 [RATED_PARTNER_ID]
     */
    @JsonProperty("rated_partner_id")
    public void setRated_partner_id(Integer  rated_partner_id){
        this.rated_partner_id = rated_partner_id ;
        this.rated_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [RATED_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getRated_partner_idDirtyFlag(){
        return rated_partner_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Rating_rating toDO() {
        Rating_rating srfdomain = new Rating_rating();
        if(getFeedbackDirtyFlag())
            srfdomain.setFeedback(feedback);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getRatingDirtyFlag())
            srfdomain.setRating(rating);
        if(getRating_textDirtyFlag())
            srfdomain.setRating_text(rating_text);
        if(getParent_res_nameDirtyFlag())
            srfdomain.setParent_res_name(parent_res_name);
        if(getParent_res_idDirtyFlag())
            srfdomain.setParent_res_id(parent_res_id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getRes_nameDirtyFlag())
            srfdomain.setRes_name(res_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getParent_res_model_idDirtyFlag())
            srfdomain.setParent_res_model_id(parent_res_model_id);
        if(getRes_idDirtyFlag())
            srfdomain.setRes_id(res_id);
        if(getConsumedDirtyFlag())
            srfdomain.setConsumed(consumed);
        if(getRating_imageDirtyFlag())
            srfdomain.setRating_image(rating_image);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getRes_model_idDirtyFlag())
            srfdomain.setRes_model_id(res_model_id);
        if(getRes_modelDirtyFlag())
            srfdomain.setRes_model(res_model);
        if(getAccess_tokenDirtyFlag())
            srfdomain.setAccess_token(access_token);
        if(getParent_res_modelDirtyFlag())
            srfdomain.setParent_res_model(parent_res_model);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getRated_partner_id_textDirtyFlag())
            srfdomain.setRated_partner_id_text(rated_partner_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getMessage_idDirtyFlag())
            srfdomain.setMessage_id(message_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getRated_partner_idDirtyFlag())
            srfdomain.setRated_partner_id(rated_partner_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Rating_rating srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getFeedbackDirtyFlag())
            this.setFeedback(srfdomain.getFeedback());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getRatingDirtyFlag())
            this.setRating(srfdomain.getRating());
        if(srfdomain.getRating_textDirtyFlag())
            this.setRating_text(srfdomain.getRating_text());
        if(srfdomain.getParent_res_nameDirtyFlag())
            this.setParent_res_name(srfdomain.getParent_res_name());
        if(srfdomain.getParent_res_idDirtyFlag())
            this.setParent_res_id(srfdomain.getParent_res_id());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getRes_nameDirtyFlag())
            this.setRes_name(srfdomain.getRes_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getParent_res_model_idDirtyFlag())
            this.setParent_res_model_id(srfdomain.getParent_res_model_id());
        if(srfdomain.getRes_idDirtyFlag())
            this.setRes_id(srfdomain.getRes_id());
        if(srfdomain.getConsumedDirtyFlag())
            this.setConsumed(srfdomain.getConsumed());
        if(srfdomain.getRating_imageDirtyFlag())
            this.setRating_image(srfdomain.getRating_image());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getRes_model_idDirtyFlag())
            this.setRes_model_id(srfdomain.getRes_model_id());
        if(srfdomain.getRes_modelDirtyFlag())
            this.setRes_model(srfdomain.getRes_model());
        if(srfdomain.getAccess_tokenDirtyFlag())
            this.setAccess_token(srfdomain.getAccess_token());
        if(srfdomain.getParent_res_modelDirtyFlag())
            this.setParent_res_model(srfdomain.getParent_res_model());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getRated_partner_id_textDirtyFlag())
            this.setRated_partner_id_text(srfdomain.getRated_partner_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getMessage_idDirtyFlag())
            this.setMessage_id(srfdomain.getMessage_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getRated_partner_idDirtyFlag())
            this.setRated_partner_id(srfdomain.getRated_partner_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Rating_ratingDTO> fromDOPage(List<Rating_rating> poPage)   {
        if(poPage == null)
            return null;
        List<Rating_ratingDTO> dtos=new ArrayList<Rating_ratingDTO>();
        for(Rating_rating domain : poPage) {
            Rating_ratingDTO dto = new Rating_ratingDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

