package cn.ibizlab.odoo.service.odoo_rating.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_rating.dto.Rating_mixinDTO;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.odoo.core.odoo_rating.service.IRating_mixinService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_mixinSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Rating_mixin" })
@RestController
@RequestMapping("")
public class Rating_mixinResource {

    @Autowired
    private IRating_mixinService rating_mixinService;

    public IRating_mixinService getRating_mixinService() {
        return this.rating_mixinService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Rating_mixin" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_rating/rating_mixins/createBatch")
    public ResponseEntity<Boolean> createBatchRating_mixin(@RequestBody List<Rating_mixinDTO> rating_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Rating_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_rating/rating_mixins/{rating_mixin_id}")
    public ResponseEntity<Rating_mixinDTO> get(@PathVariable("rating_mixin_id") Integer rating_mixin_id) {
        Rating_mixinDTO dto = new Rating_mixinDTO();
        Rating_mixin domain = rating_mixinService.get(rating_mixin_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Rating_mixin" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_rating/rating_mixins/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Rating_mixinDTO> rating_mixindtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Rating_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_rating/rating_mixins")

    public ResponseEntity<Rating_mixinDTO> create(@RequestBody Rating_mixinDTO rating_mixindto) {
        Rating_mixinDTO dto = new Rating_mixinDTO();
        Rating_mixin domain = rating_mixindto.toDO();
		rating_mixinService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Rating_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_rating/rating_mixins/{rating_mixin_id}")

    public ResponseEntity<Rating_mixinDTO> update(@PathVariable("rating_mixin_id") Integer rating_mixin_id, @RequestBody Rating_mixinDTO rating_mixindto) {
		Rating_mixin domain = rating_mixindto.toDO();
        domain.setId(rating_mixin_id);
		rating_mixinService.update(domain);
		Rating_mixinDTO dto = new Rating_mixinDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Rating_mixin" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_rating/rating_mixins/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Rating_mixinDTO> rating_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Rating_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_rating/rating_mixins/{rating_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("rating_mixin_id") Integer rating_mixin_id) {
        Rating_mixinDTO rating_mixindto = new Rating_mixinDTO();
		Rating_mixin domain = new Rating_mixin();
		rating_mixindto.setId(rating_mixin_id);
		domain.setId(rating_mixin_id);
        Boolean rst = rating_mixinService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Rating_mixin" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_rating/rating_mixins/fetchdefault")
	public ResponseEntity<Page<Rating_mixinDTO>> fetchDefault(Rating_mixinSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Rating_mixinDTO> list = new ArrayList<Rating_mixinDTO>();
        
        Page<Rating_mixin> domains = rating_mixinService.searchDefault(context) ;
        for(Rating_mixin rating_mixin : domains.getContent()){
            Rating_mixinDTO dto = new Rating_mixinDTO();
            dto.fromDO(rating_mixin);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
