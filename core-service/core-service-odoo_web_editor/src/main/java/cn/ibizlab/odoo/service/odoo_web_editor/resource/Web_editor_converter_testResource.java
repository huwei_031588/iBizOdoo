package cn.ibizlab.odoo.service.odoo_web_editor.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_web_editor.dto.Web_editor_converter_testDTO;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.odoo.core.odoo_web_editor.service.IWeb_editor_converter_testService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Web_editor_converter_test" })
@RestController
@RequestMapping("")
public class Web_editor_converter_testResource {

    @Autowired
    private IWeb_editor_converter_testService web_editor_converter_testService;

    public IWeb_editor_converter_testService getWeb_editor_converter_testService() {
        return this.web_editor_converter_testService;
    }

    @ApiOperation(value = "建立数据", tags = {"Web_editor_converter_test" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_web_editor/web_editor_converter_tests")

    public ResponseEntity<Web_editor_converter_testDTO> create(@RequestBody Web_editor_converter_testDTO web_editor_converter_testdto) {
        Web_editor_converter_testDTO dto = new Web_editor_converter_testDTO();
        Web_editor_converter_test domain = web_editor_converter_testdto.toDO();
		web_editor_converter_testService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Web_editor_converter_test" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_editor/web_editor_converter_tests/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Web_editor_converter_testDTO> web_editor_converter_testdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Web_editor_converter_test" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_web_editor/web_editor_converter_tests/{web_editor_converter_test_id}")
    public ResponseEntity<Web_editor_converter_testDTO> get(@PathVariable("web_editor_converter_test_id") Integer web_editor_converter_test_id) {
        Web_editor_converter_testDTO dto = new Web_editor_converter_testDTO();
        Web_editor_converter_test domain = web_editor_converter_testService.get(web_editor_converter_test_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Web_editor_converter_test" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_web_editor/web_editor_converter_tests/createBatch")
    public ResponseEntity<Boolean> createBatchWeb_editor_converter_test(@RequestBody List<Web_editor_converter_testDTO> web_editor_converter_testdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Web_editor_converter_test" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_editor/web_editor_converter_tests/{web_editor_converter_test_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("web_editor_converter_test_id") Integer web_editor_converter_test_id) {
        Web_editor_converter_testDTO web_editor_converter_testdto = new Web_editor_converter_testDTO();
		Web_editor_converter_test domain = new Web_editor_converter_test();
		web_editor_converter_testdto.setId(web_editor_converter_test_id);
		domain.setId(web_editor_converter_test_id);
        Boolean rst = web_editor_converter_testService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Web_editor_converter_test" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_editor/web_editor_converter_tests/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Web_editor_converter_testDTO> web_editor_converter_testdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Web_editor_converter_test" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_editor/web_editor_converter_tests/{web_editor_converter_test_id}")

    public ResponseEntity<Web_editor_converter_testDTO> update(@PathVariable("web_editor_converter_test_id") Integer web_editor_converter_test_id, @RequestBody Web_editor_converter_testDTO web_editor_converter_testdto) {
		Web_editor_converter_test domain = web_editor_converter_testdto.toDO();
        domain.setId(web_editor_converter_test_id);
		web_editor_converter_testService.update(domain);
		Web_editor_converter_testDTO dto = new Web_editor_converter_testDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Web_editor_converter_test" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_web_editor/web_editor_converter_tests/fetchdefault")
	public ResponseEntity<Page<Web_editor_converter_testDTO>> fetchDefault(Web_editor_converter_testSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Web_editor_converter_testDTO> list = new ArrayList<Web_editor_converter_testDTO>();
        
        Page<Web_editor_converter_test> domains = web_editor_converter_testService.searchDefault(context) ;
        for(Web_editor_converter_test web_editor_converter_test : domains.getContent()){
            Web_editor_converter_testDTO dto = new Web_editor_converter_testDTO();
            dto.fromDO(web_editor_converter_test);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
