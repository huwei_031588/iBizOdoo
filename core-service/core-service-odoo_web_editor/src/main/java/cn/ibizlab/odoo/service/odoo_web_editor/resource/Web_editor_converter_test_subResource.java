package cn.ibizlab.odoo.service.odoo_web_editor.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_web_editor.dto.Web_editor_converter_test_subDTO;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.odoo.core.odoo_web_editor.service.IWeb_editor_converter_test_subService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_test_subSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Web_editor_converter_test_sub" })
@RestController
@RequestMapping("")
public class Web_editor_converter_test_subResource {

    @Autowired
    private IWeb_editor_converter_test_subService web_editor_converter_test_subService;

    public IWeb_editor_converter_test_subService getWeb_editor_converter_test_subService() {
        return this.web_editor_converter_test_subService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Web_editor_converter_test_sub" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_web_editor/web_editor_converter_test_subs/createBatch")
    public ResponseEntity<Boolean> createBatchWeb_editor_converter_test_sub(@RequestBody List<Web_editor_converter_test_subDTO> web_editor_converter_test_subdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Web_editor_converter_test_sub" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_editor/web_editor_converter_test_subs/{web_editor_converter_test_sub_id}")

    public ResponseEntity<Web_editor_converter_test_subDTO> update(@PathVariable("web_editor_converter_test_sub_id") Integer web_editor_converter_test_sub_id, @RequestBody Web_editor_converter_test_subDTO web_editor_converter_test_subdto) {
		Web_editor_converter_test_sub domain = web_editor_converter_test_subdto.toDO();
        domain.setId(web_editor_converter_test_sub_id);
		web_editor_converter_test_subService.update(domain);
		Web_editor_converter_test_subDTO dto = new Web_editor_converter_test_subDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Web_editor_converter_test_sub" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_web_editor/web_editor_converter_test_subs/{web_editor_converter_test_sub_id}")
    public ResponseEntity<Web_editor_converter_test_subDTO> get(@PathVariable("web_editor_converter_test_sub_id") Integer web_editor_converter_test_sub_id) {
        Web_editor_converter_test_subDTO dto = new Web_editor_converter_test_subDTO();
        Web_editor_converter_test_sub domain = web_editor_converter_test_subService.get(web_editor_converter_test_sub_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Web_editor_converter_test_sub" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_web_editor/web_editor_converter_test_subs")

    public ResponseEntity<Web_editor_converter_test_subDTO> create(@RequestBody Web_editor_converter_test_subDTO web_editor_converter_test_subdto) {
        Web_editor_converter_test_subDTO dto = new Web_editor_converter_test_subDTO();
        Web_editor_converter_test_sub domain = web_editor_converter_test_subdto.toDO();
		web_editor_converter_test_subService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Web_editor_converter_test_sub" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_editor/web_editor_converter_test_subs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Web_editor_converter_test_subDTO> web_editor_converter_test_subdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Web_editor_converter_test_sub" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_editor/web_editor_converter_test_subs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Web_editor_converter_test_subDTO> web_editor_converter_test_subdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Web_editor_converter_test_sub" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_editor/web_editor_converter_test_subs/{web_editor_converter_test_sub_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("web_editor_converter_test_sub_id") Integer web_editor_converter_test_sub_id) {
        Web_editor_converter_test_subDTO web_editor_converter_test_subdto = new Web_editor_converter_test_subDTO();
		Web_editor_converter_test_sub domain = new Web_editor_converter_test_sub();
		web_editor_converter_test_subdto.setId(web_editor_converter_test_sub_id);
		domain.setId(web_editor_converter_test_sub_id);
        Boolean rst = web_editor_converter_test_subService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Web_editor_converter_test_sub" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_web_editor/web_editor_converter_test_subs/fetchdefault")
	public ResponseEntity<Page<Web_editor_converter_test_subDTO>> fetchDefault(Web_editor_converter_test_subSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Web_editor_converter_test_subDTO> list = new ArrayList<Web_editor_converter_test_subDTO>();
        
        Page<Web_editor_converter_test_sub> domains = web_editor_converter_test_subService.searchDefault(context) ;
        for(Web_editor_converter_test_sub web_editor_converter_test_sub : domains.getContent()){
            Web_editor_converter_test_subDTO dto = new Web_editor_converter_test_subDTO();
            dto.fromDO(web_editor_converter_test_sub);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
