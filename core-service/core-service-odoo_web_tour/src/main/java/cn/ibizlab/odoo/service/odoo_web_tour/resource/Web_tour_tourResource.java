package cn.ibizlab.odoo.service.odoo_web_tour.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_web_tour.dto.Web_tour_tourDTO;
import cn.ibizlab.odoo.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.odoo.core.odoo_web_tour.service.IWeb_tour_tourService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_web_tour.filter.Web_tour_tourSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Web_tour_tour" })
@RestController
@RequestMapping("")
public class Web_tour_tourResource {

    @Autowired
    private IWeb_tour_tourService web_tour_tourService;

    public IWeb_tour_tourService getWeb_tour_tourService() {
        return this.web_tour_tourService;
    }

    @ApiOperation(value = "更新数据", tags = {"Web_tour_tour" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_tour/web_tour_tours/{web_tour_tour_id}")

    public ResponseEntity<Web_tour_tourDTO> update(@PathVariable("web_tour_tour_id") Integer web_tour_tour_id, @RequestBody Web_tour_tourDTO web_tour_tourdto) {
		Web_tour_tour domain = web_tour_tourdto.toDO();
        domain.setId(web_tour_tour_id);
		web_tour_tourService.update(domain);
		Web_tour_tourDTO dto = new Web_tour_tourDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Web_tour_tour" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_tour/web_tour_tours/{web_tour_tour_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("web_tour_tour_id") Integer web_tour_tour_id) {
        Web_tour_tourDTO web_tour_tourdto = new Web_tour_tourDTO();
		Web_tour_tour domain = new Web_tour_tour();
		web_tour_tourdto.setId(web_tour_tour_id);
		domain.setId(web_tour_tour_id);
        Boolean rst = web_tour_tourService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Web_tour_tour" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_tour/web_tour_tours/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Web_tour_tourDTO> web_tour_tourdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Web_tour_tour" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_web_tour/web_tour_tours/{web_tour_tour_id}")
    public ResponseEntity<Web_tour_tourDTO> get(@PathVariable("web_tour_tour_id") Integer web_tour_tour_id) {
        Web_tour_tourDTO dto = new Web_tour_tourDTO();
        Web_tour_tour domain = web_tour_tourService.get(web_tour_tour_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Web_tour_tour" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_tour/web_tour_tours/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Web_tour_tourDTO> web_tour_tourdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Web_tour_tour" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_web_tour/web_tour_tours")

    public ResponseEntity<Web_tour_tourDTO> create(@RequestBody Web_tour_tourDTO web_tour_tourdto) {
        Web_tour_tourDTO dto = new Web_tour_tourDTO();
        Web_tour_tour domain = web_tour_tourdto.toDO();
		web_tour_tourService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Web_tour_tour" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_web_tour/web_tour_tours/createBatch")
    public ResponseEntity<Boolean> createBatchWeb_tour_tour(@RequestBody List<Web_tour_tourDTO> web_tour_tourdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Web_tour_tour" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_web_tour/web_tour_tours/fetchdefault")
	public ResponseEntity<Page<Web_tour_tourDTO>> fetchDefault(Web_tour_tourSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Web_tour_tourDTO> list = new ArrayList<Web_tour_tourDTO>();
        
        Page<Web_tour_tour> domains = web_tour_tourService.searchDefault(context) ;
        for(Web_tour_tour web_tour_tour : domains.getContent()){
            Web_tour_tourDTO dto = new Web_tour_tourDTO();
            dto.fromDO(web_tour_tour);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
