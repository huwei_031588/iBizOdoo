package cn.ibizlab.odoo.service.odoo_web_tour.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_web_tour")
public class odoo_web_tourRestConfiguration {

}
