package cn.ibizlab.odoo.service.odoo_asset.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_asset.dto.Asset_assetDTO;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_assetService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_assetSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Asset_asset" })
@RestController
@RequestMapping("")
public class Asset_assetResource {

    @Autowired
    private IAsset_assetService asset_assetService;

    public IAsset_assetService getAsset_assetService() {
        return this.asset_assetService;
    }

    @ApiOperation(value = "更新数据", tags = {"Asset_asset" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_assets/{asset_asset_id}")

    public ResponseEntity<Asset_assetDTO> update(@PathVariable("asset_asset_id") Integer asset_asset_id, @RequestBody Asset_assetDTO asset_assetdto) {
		Asset_asset domain = asset_assetdto.toDO();
        domain.setId(asset_asset_id);
		asset_assetService.update(domain);
		Asset_assetDTO dto = new Asset_assetDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Asset_asset" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_assets/{asset_asset_id}")
    public ResponseEntity<Asset_assetDTO> get(@PathVariable("asset_asset_id") Integer asset_asset_id) {
        Asset_assetDTO dto = new Asset_assetDTO();
        Asset_asset domain = asset_assetService.get(asset_asset_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Asset_asset" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_assets/{asset_asset_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("asset_asset_id") Integer asset_asset_id) {
        Asset_assetDTO asset_assetdto = new Asset_assetDTO();
		Asset_asset domain = new Asset_asset();
		asset_assetdto.setId(asset_asset_id);
		domain.setId(asset_asset_id);
        Boolean rst = asset_assetService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Asset_asset" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_assets/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Asset_assetDTO> asset_assetdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Asset_asset" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_assets/createBatch")
    public ResponseEntity<Boolean> createBatchAsset_asset(@RequestBody List<Asset_assetDTO> asset_assetdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Asset_asset" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_assets")

    public ResponseEntity<Asset_assetDTO> create(@RequestBody Asset_assetDTO asset_assetdto) {
        Asset_assetDTO dto = new Asset_assetDTO();
        Asset_asset domain = asset_assetdto.toDO();
		asset_assetService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Asset_asset" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_assets/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Asset_assetDTO> asset_assetdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Asset_asset" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_asset/asset_assets/fetchdefault")
	public ResponseEntity<Page<Asset_assetDTO>> fetchDefault(Asset_assetSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Asset_assetDTO> list = new ArrayList<Asset_assetDTO>();
        
        Page<Asset_asset> domains = asset_assetService.searchDefault(context) ;
        for(Asset_asset asset_asset : domains.getContent()){
            Asset_assetDTO dto = new Asset_assetDTO();
            dto.fromDO(asset_asset);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
