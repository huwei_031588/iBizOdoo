package cn.ibizlab.odoo.service.odoo_asset.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_asset")
public class odoo_assetRestConfiguration {

}
