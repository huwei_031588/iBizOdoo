package cn.ibizlab.odoo.service.odoo_asset.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_asset.dto.Asset_stateDTO;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_stateService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_stateSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Asset_state" })
@RestController
@RequestMapping("")
public class Asset_stateResource {

    @Autowired
    private IAsset_stateService asset_stateService;

    public IAsset_stateService getAsset_stateService() {
        return this.asset_stateService;
    }

    @ApiOperation(value = "建立数据", tags = {"Asset_state" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_states")

    public ResponseEntity<Asset_stateDTO> create(@RequestBody Asset_stateDTO asset_statedto) {
        Asset_stateDTO dto = new Asset_stateDTO();
        Asset_state domain = asset_statedto.toDO();
		asset_stateService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Asset_state" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_states/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Asset_stateDTO> asset_statedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Asset_state" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_states/{asset_state_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("asset_state_id") Integer asset_state_id) {
        Asset_stateDTO asset_statedto = new Asset_stateDTO();
		Asset_state domain = new Asset_state();
		asset_statedto.setId(asset_state_id);
		domain.setId(asset_state_id);
        Boolean rst = asset_stateService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Asset_state" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_states/createBatch")
    public ResponseEntity<Boolean> createBatchAsset_state(@RequestBody List<Asset_stateDTO> asset_statedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Asset_state" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_states/{asset_state_id}")
    public ResponseEntity<Asset_stateDTO> get(@PathVariable("asset_state_id") Integer asset_state_id) {
        Asset_stateDTO dto = new Asset_stateDTO();
        Asset_state domain = asset_stateService.get(asset_state_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Asset_state" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_states/{asset_state_id}")

    public ResponseEntity<Asset_stateDTO> update(@PathVariable("asset_state_id") Integer asset_state_id, @RequestBody Asset_stateDTO asset_statedto) {
		Asset_state domain = asset_statedto.toDO();
        domain.setId(asset_state_id);
		asset_stateService.update(domain);
		Asset_stateDTO dto = new Asset_stateDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Asset_state" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_states/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Asset_stateDTO> asset_statedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Asset_state" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_asset/asset_states/fetchdefault")
	public ResponseEntity<Page<Asset_stateDTO>> fetchDefault(Asset_stateSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Asset_stateDTO> list = new ArrayList<Asset_stateDTO>();
        
        Page<Asset_state> domains = asset_stateService.searchDefault(context) ;
        for(Asset_state asset_state : domains.getContent()){
            Asset_stateDTO dto = new Asset_stateDTO();
            dto.fromDO(asset_state);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
