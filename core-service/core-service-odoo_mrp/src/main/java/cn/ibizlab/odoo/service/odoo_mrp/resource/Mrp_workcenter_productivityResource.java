package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_workcenter_productivityDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivityService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivitySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_workcenter_productivity" })
@RestController
@RequestMapping("")
public class Mrp_workcenter_productivityResource {

    @Autowired
    private IMrp_workcenter_productivityService mrp_workcenter_productivityService;

    public IMrp_workcenter_productivityService getMrp_workcenter_productivityService() {
        return this.mrp_workcenter_productivityService;
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_workcenter_productivity" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivities")

    public ResponseEntity<Mrp_workcenter_productivityDTO> create(@RequestBody Mrp_workcenter_productivityDTO mrp_workcenter_productivitydto) {
        Mrp_workcenter_productivityDTO dto = new Mrp_workcenter_productivityDTO();
        Mrp_workcenter_productivity domain = mrp_workcenter_productivitydto.toDO();
		mrp_workcenter_productivityService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_workcenter_productivity" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivities/{mrp_workcenter_productivity_id}")

    public ResponseEntity<Mrp_workcenter_productivityDTO> update(@PathVariable("mrp_workcenter_productivity_id") Integer mrp_workcenter_productivity_id, @RequestBody Mrp_workcenter_productivityDTO mrp_workcenter_productivitydto) {
		Mrp_workcenter_productivity domain = mrp_workcenter_productivitydto.toDO();
        domain.setId(mrp_workcenter_productivity_id);
		mrp_workcenter_productivityService.update(domain);
		Mrp_workcenter_productivityDTO dto = new Mrp_workcenter_productivityDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_workcenter_productivity" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivities/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_workcenter_productivity(@RequestBody List<Mrp_workcenter_productivityDTO> mrp_workcenter_productivitydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_workcenter_productivity" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivities/{mrp_workcenter_productivity_id}")
    public ResponseEntity<Mrp_workcenter_productivityDTO> get(@PathVariable("mrp_workcenter_productivity_id") Integer mrp_workcenter_productivity_id) {
        Mrp_workcenter_productivityDTO dto = new Mrp_workcenter_productivityDTO();
        Mrp_workcenter_productivity domain = mrp_workcenter_productivityService.get(mrp_workcenter_productivity_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_workcenter_productivity" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivities/{mrp_workcenter_productivity_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_productivity_id") Integer mrp_workcenter_productivity_id) {
        Mrp_workcenter_productivityDTO mrp_workcenter_productivitydto = new Mrp_workcenter_productivityDTO();
		Mrp_workcenter_productivity domain = new Mrp_workcenter_productivity();
		mrp_workcenter_productivitydto.setId(mrp_workcenter_productivity_id);
		domain.setId(mrp_workcenter_productivity_id);
        Boolean rst = mrp_workcenter_productivityService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_workcenter_productivity" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivities/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_workcenter_productivityDTO> mrp_workcenter_productivitydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_workcenter_productivity" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivities/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenter_productivityDTO> mrp_workcenter_productivitydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_workcenter_productivity" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_workcenter_productivities/fetchdefault")
	public ResponseEntity<Page<Mrp_workcenter_productivityDTO>> fetchDefault(Mrp_workcenter_productivitySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_workcenter_productivityDTO> list = new ArrayList<Mrp_workcenter_productivityDTO>();
        
        Page<Mrp_workcenter_productivity> domains = mrp_workcenter_productivityService.searchDefault(context) ;
        for(Mrp_workcenter_productivity mrp_workcenter_productivity : domains.getContent()){
            Mrp_workcenter_productivityDTO dto = new Mrp_workcenter_productivityDTO();
            dto.fromDO(mrp_workcenter_productivity);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
