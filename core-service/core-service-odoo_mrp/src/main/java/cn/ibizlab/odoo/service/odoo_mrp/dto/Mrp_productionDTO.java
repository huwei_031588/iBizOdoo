package cn.ibizlab.odoo.service.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_production.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mrp_productionDTO]
 */
public class Mrp_productionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Mrp_productionMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Mrp_productionActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [WORKORDER_DONE_COUNT]
     *
     */
    @Mrp_productionWorkorder_done_countDefault(info = "默认规则")
    private Integer workorder_done_count;

    @JsonIgnore
    private boolean workorder_done_countDirtyFlag;

    /**
     * 属性 [FINISHED_MOVE_LINE_IDS]
     *
     */
    @Mrp_productionFinished_move_line_idsDefault(info = "默认规则")
    private String finished_move_line_ids;

    @JsonIgnore
    private boolean finished_move_line_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Mrp_productionWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Mrp_productionMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Mrp_productionMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Mrp_productionMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [QTY_PRODUCED]
     *
     */
    @Mrp_productionQty_producedDefault(info = "默认规则")
    private Double qty_produced;

    @JsonIgnore
    private boolean qty_producedDirtyFlag;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Mrp_productionPriorityDefault(info = "默认规则")
    private String priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Mrp_productionMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [UNRESERVE_VISIBLE]
     *
     */
    @Mrp_productionUnreserve_visibleDefault(info = "默认规则")
    private String unreserve_visible;

    @JsonIgnore
    private boolean unreserve_visibleDirtyFlag;

    /**
     * 属性 [HAS_MOVES]
     *
     */
    @Mrp_productionHas_movesDefault(info = "默认规则")
    private String has_moves;

    @JsonIgnore
    private boolean has_movesDirtyFlag;

    /**
     * 属性 [IS_LOCKED]
     *
     */
    @Mrp_productionIs_lockedDefault(info = "默认规则")
    private String is_locked;

    @JsonIgnore
    private boolean is_lockedDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mrp_productionCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mrp_productionIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Mrp_productionActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [DATE_START]
     *
     */
    @Mrp_productionDate_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_start;

    @JsonIgnore
    private boolean date_startDirtyFlag;

    /**
     * 属性 [MOVE_FINISHED_IDS]
     *
     */
    @Mrp_productionMove_finished_idsDefault(info = "默认规则")
    private String move_finished_ids;

    @JsonIgnore
    private boolean move_finished_idsDirtyFlag;

    /**
     * 属性 [SCRAP_COUNT]
     *
     */
    @Mrp_productionScrap_countDefault(info = "默认规则")
    private Integer scrap_count;

    @JsonIgnore
    private boolean scrap_countDirtyFlag;

    /**
     * 属性 [SCRAP_IDS]
     *
     */
    @Mrp_productionScrap_idsDefault(info = "默认规则")
    private String scrap_ids;

    @JsonIgnore
    private boolean scrap_idsDirtyFlag;

    /**
     * 属性 [SHOW_FINAL_LOTS]
     *
     */
    @Mrp_productionShow_final_lotsDefault(info = "默认规则")
    private String show_final_lots;

    @JsonIgnore
    private boolean show_final_lotsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mrp_productionNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [MOVE_DEST_IDS]
     *
     */
    @Mrp_productionMove_dest_idsDefault(info = "默认规则")
    private String move_dest_ids;

    @JsonIgnore
    private boolean move_dest_idsDirtyFlag;

    /**
     * 属性 [PROCUREMENT_GROUP_ID]
     *
     */
    @Mrp_productionProcurement_group_idDefault(info = "默认规则")
    private Integer procurement_group_id;

    @JsonIgnore
    private boolean procurement_group_idDirtyFlag;

    /**
     * 属性 [DELIVERY_COUNT]
     *
     */
    @Mrp_productionDelivery_countDefault(info = "默认规则")
    private Integer delivery_count;

    @JsonIgnore
    private boolean delivery_countDirtyFlag;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @Mrp_productionProduct_qtyDefault(info = "默认规则")
    private Double product_qty;

    @JsonIgnore
    private boolean product_qtyDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Mrp_productionActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [WORKORDER_COUNT]
     *
     */
    @Mrp_productionWorkorder_countDefault(info = "默认规则")
    private Integer workorder_count;

    @JsonIgnore
    private boolean workorder_countDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Mrp_productionMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mrp_productionWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Mrp_productionMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [DATE_PLANNED_FINISHED]
     *
     */
    @Mrp_productionDate_planned_finishedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_planned_finished;

    @JsonIgnore
    private boolean date_planned_finishedDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Mrp_productionActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [ORIGIN]
     *
     */
    @Mrp_productionOriginDefault(info = "默认规则")
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Mrp_productionStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [DATE_PLANNED_START]
     *
     */
    @Mrp_productionDate_planned_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_planned_start;

    @JsonIgnore
    private boolean date_planned_startDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Mrp_productionMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Mrp_productionMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Mrp_productionMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [CHECK_TO_DONE]
     *
     */
    @Mrp_productionCheck_to_doneDefault(info = "默认规则")
    private String check_to_done;

    @JsonIgnore
    private boolean check_to_doneDirtyFlag;

    /**
     * 属性 [POST_VISIBLE]
     *
     */
    @Mrp_productionPost_visibleDefault(info = "默认规则")
    private String post_visible;

    @JsonIgnore
    private boolean post_visibleDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mrp_production__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PICKING_IDS]
     *
     */
    @Mrp_productionPicking_idsDefault(info = "默认规则")
    private String picking_ids;

    @JsonIgnore
    private boolean picking_idsDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @Mrp_productionProduct_uom_qtyDefault(info = "默认规则")
    private Double product_uom_qty;

    @JsonIgnore
    private boolean product_uom_qtyDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Mrp_productionMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [CONSUMED_LESS_THAN_PLANNED]
     *
     */
    @Mrp_productionConsumed_less_than_plannedDefault(info = "默认规则")
    private String consumed_less_than_planned;

    @JsonIgnore
    private boolean consumed_less_than_plannedDirtyFlag;

    /**
     * 属性 [MOVE_RAW_IDS]
     *
     */
    @Mrp_productionMove_raw_idsDefault(info = "默认规则")
    private String move_raw_ids;

    @JsonIgnore
    private boolean move_raw_idsDirtyFlag;

    /**
     * 属性 [AVAILABILITY]
     *
     */
    @Mrp_productionAvailabilityDefault(info = "默认规则")
    private String availability;

    @JsonIgnore
    private boolean availabilityDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mrp_productionDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Mrp_productionActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [WORKORDER_IDS]
     *
     */
    @Mrp_productionWorkorder_idsDefault(info = "默认规则")
    private String workorder_ids;

    @JsonIgnore
    private boolean workorder_idsDirtyFlag;

    /**
     * 属性 [DATE_FINISHED]
     *
     */
    @Mrp_productionDate_finishedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_finished;

    @JsonIgnore
    private boolean date_finishedDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Mrp_productionMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [PROPAGATE]
     *
     */
    @Mrp_productionPropagateDefault(info = "默认规则")
    private String propagate;

    @JsonIgnore
    private boolean propagateDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Mrp_productionActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Mrp_productionMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Mrp_productionCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @Mrp_productionLocation_dest_id_textDefault(info = "默认规则")
    private String location_dest_id_text;

    @JsonIgnore
    private boolean location_dest_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_SRC_ID_TEXT]
     *
     */
    @Mrp_productionLocation_src_id_textDefault(info = "默认规则")
    private String location_src_id_text;

    @JsonIgnore
    private boolean location_src_id_textDirtyFlag;

    /**
     * 属性 [PRODUCTION_LOCATION_ID]
     *
     */
    @Mrp_productionProduction_location_idDefault(info = "默认规则")
    private Integer production_location_id;

    @JsonIgnore
    private boolean production_location_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Mrp_productionProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mrp_productionWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @Mrp_productionPicking_type_id_textDefault(info = "默认规则")
    private String picking_type_id_text;

    @JsonIgnore
    private boolean picking_type_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Mrp_productionUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @Mrp_productionProduct_tmpl_idDefault(info = "默认规则")
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @Mrp_productionProduct_uom_id_textDefault(info = "默认规则")
    private String product_uom_id_text;

    @JsonIgnore
    private boolean product_uom_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mrp_productionCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ROUTING_ID_TEXT]
     *
     */
    @Mrp_productionRouting_id_textDefault(info = "默认规则")
    private String routing_id_text;

    @JsonIgnore
    private boolean routing_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @Mrp_productionProduct_uom_idDefault(info = "默认规则")
    private Integer product_uom_id;

    @JsonIgnore
    private boolean product_uom_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Mrp_productionProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [ROUTING_ID]
     *
     */
    @Mrp_productionRouting_idDefault(info = "默认规则")
    private Integer routing_id;

    @JsonIgnore
    private boolean routing_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mrp_productionWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Mrp_productionUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mrp_productionCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [BOM_ID]
     *
     */
    @Mrp_productionBom_idDefault(info = "默认规则")
    private Integer bom_id;

    @JsonIgnore
    private boolean bom_idDirtyFlag;

    /**
     * 属性 [LOCATION_SRC_ID]
     *
     */
    @Mrp_productionLocation_src_idDefault(info = "默认规则")
    private Integer location_src_id;

    @JsonIgnore
    private boolean location_src_idDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @Mrp_productionPicking_type_idDefault(info = "默认规则")
    private Integer picking_type_id;

    @JsonIgnore
    private boolean picking_type_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Mrp_productionCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @Mrp_productionLocation_dest_idDefault(info = "默认规则")
    private Integer location_dest_id;

    @JsonIgnore
    private boolean location_dest_idDirtyFlag;


    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_DONE_COUNT]
     */
    @JsonProperty("workorder_done_count")
    public Integer getWorkorder_done_count(){
        return workorder_done_count ;
    }

    /**
     * 设置 [WORKORDER_DONE_COUNT]
     */
    @JsonProperty("workorder_done_count")
    public void setWorkorder_done_count(Integer  workorder_done_count){
        this.workorder_done_count = workorder_done_count ;
        this.workorder_done_countDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_DONE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_done_countDirtyFlag(){
        return workorder_done_countDirtyFlag ;
    }

    /**
     * 获取 [FINISHED_MOVE_LINE_IDS]
     */
    @JsonProperty("finished_move_line_ids")
    public String getFinished_move_line_ids(){
        return finished_move_line_ids ;
    }

    /**
     * 设置 [FINISHED_MOVE_LINE_IDS]
     */
    @JsonProperty("finished_move_line_ids")
    public void setFinished_move_line_ids(String  finished_move_line_ids){
        this.finished_move_line_ids = finished_move_line_ids ;
        this.finished_move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [FINISHED_MOVE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getFinished_move_line_idsDirtyFlag(){
        return finished_move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [QTY_PRODUCED]
     */
    @JsonProperty("qty_produced")
    public Double getQty_produced(){
        return qty_produced ;
    }

    /**
     * 设置 [QTY_PRODUCED]
     */
    @JsonProperty("qty_produced")
    public void setQty_produced(Double  qty_produced){
        this.qty_produced = qty_produced ;
        this.qty_producedDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_PRODUCED]脏标记
     */
    @JsonIgnore
    public boolean getQty_producedDirtyFlag(){
        return qty_producedDirtyFlag ;
    }

    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [UNRESERVE_VISIBLE]
     */
    @JsonProperty("unreserve_visible")
    public String getUnreserve_visible(){
        return unreserve_visible ;
    }

    /**
     * 设置 [UNRESERVE_VISIBLE]
     */
    @JsonProperty("unreserve_visible")
    public void setUnreserve_visible(String  unreserve_visible){
        this.unreserve_visible = unreserve_visible ;
        this.unreserve_visibleDirtyFlag = true ;
    }

    /**
     * 获取 [UNRESERVE_VISIBLE]脏标记
     */
    @JsonIgnore
    public boolean getUnreserve_visibleDirtyFlag(){
        return unreserve_visibleDirtyFlag ;
    }

    /**
     * 获取 [HAS_MOVES]
     */
    @JsonProperty("has_moves")
    public String getHas_moves(){
        return has_moves ;
    }

    /**
     * 设置 [HAS_MOVES]
     */
    @JsonProperty("has_moves")
    public void setHas_moves(String  has_moves){
        this.has_moves = has_moves ;
        this.has_movesDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_MOVES]脏标记
     */
    @JsonIgnore
    public boolean getHas_movesDirtyFlag(){
        return has_movesDirtyFlag ;
    }

    /**
     * 获取 [IS_LOCKED]
     */
    @JsonProperty("is_locked")
    public String getIs_locked(){
        return is_locked ;
    }

    /**
     * 设置 [IS_LOCKED]
     */
    @JsonProperty("is_locked")
    public void setIs_locked(String  is_locked){
        this.is_locked = is_locked ;
        this.is_lockedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_LOCKED]脏标记
     */
    @JsonIgnore
    public boolean getIs_lockedDirtyFlag(){
        return is_lockedDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [DATE_START]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return date_start ;
    }

    /**
     * 设置 [DATE_START]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_START]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return date_startDirtyFlag ;
    }

    /**
     * 获取 [MOVE_FINISHED_IDS]
     */
    @JsonProperty("move_finished_ids")
    public String getMove_finished_ids(){
        return move_finished_ids ;
    }

    /**
     * 设置 [MOVE_FINISHED_IDS]
     */
    @JsonProperty("move_finished_ids")
    public void setMove_finished_ids(String  move_finished_ids){
        this.move_finished_ids = move_finished_ids ;
        this.move_finished_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_FINISHED_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_finished_idsDirtyFlag(){
        return move_finished_idsDirtyFlag ;
    }

    /**
     * 获取 [SCRAP_COUNT]
     */
    @JsonProperty("scrap_count")
    public Integer getScrap_count(){
        return scrap_count ;
    }

    /**
     * 设置 [SCRAP_COUNT]
     */
    @JsonProperty("scrap_count")
    public void setScrap_count(Integer  scrap_count){
        this.scrap_count = scrap_count ;
        this.scrap_countDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAP_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getScrap_countDirtyFlag(){
        return scrap_countDirtyFlag ;
    }

    /**
     * 获取 [SCRAP_IDS]
     */
    @JsonProperty("scrap_ids")
    public String getScrap_ids(){
        return scrap_ids ;
    }

    /**
     * 设置 [SCRAP_IDS]
     */
    @JsonProperty("scrap_ids")
    public void setScrap_ids(String  scrap_ids){
        this.scrap_ids = scrap_ids ;
        this.scrap_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAP_IDS]脏标记
     */
    @JsonIgnore
    public boolean getScrap_idsDirtyFlag(){
        return scrap_idsDirtyFlag ;
    }

    /**
     * 获取 [SHOW_FINAL_LOTS]
     */
    @JsonProperty("show_final_lots")
    public String getShow_final_lots(){
        return show_final_lots ;
    }

    /**
     * 设置 [SHOW_FINAL_LOTS]
     */
    @JsonProperty("show_final_lots")
    public void setShow_final_lots(String  show_final_lots){
        this.show_final_lots = show_final_lots ;
        this.show_final_lotsDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_FINAL_LOTS]脏标记
     */
    @JsonIgnore
    public boolean getShow_final_lotsDirtyFlag(){
        return show_final_lotsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [MOVE_DEST_IDS]
     */
    @JsonProperty("move_dest_ids")
    public String getMove_dest_ids(){
        return move_dest_ids ;
    }

    /**
     * 设置 [MOVE_DEST_IDS]
     */
    @JsonProperty("move_dest_ids")
    public void setMove_dest_ids(String  move_dest_ids){
        this.move_dest_ids = move_dest_ids ;
        this.move_dest_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_DEST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_dest_idsDirtyFlag(){
        return move_dest_idsDirtyFlag ;
    }

    /**
     * 获取 [PROCUREMENT_GROUP_ID]
     */
    @JsonProperty("procurement_group_id")
    public Integer getProcurement_group_id(){
        return procurement_group_id ;
    }

    /**
     * 设置 [PROCUREMENT_GROUP_ID]
     */
    @JsonProperty("procurement_group_id")
    public void setProcurement_group_id(Integer  procurement_group_id){
        this.procurement_group_id = procurement_group_id ;
        this.procurement_group_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROCUREMENT_GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getProcurement_group_idDirtyFlag(){
        return procurement_group_idDirtyFlag ;
    }

    /**
     * 获取 [DELIVERY_COUNT]
     */
    @JsonProperty("delivery_count")
    public Integer getDelivery_count(){
        return delivery_count ;
    }

    /**
     * 设置 [DELIVERY_COUNT]
     */
    @JsonProperty("delivery_count")
    public void setDelivery_count(Integer  delivery_count){
        this.delivery_count = delivery_count ;
        this.delivery_countDirtyFlag = true ;
    }

    /**
     * 获取 [DELIVERY_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_countDirtyFlag(){
        return delivery_countDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return product_qty ;
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return product_qtyDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_COUNT]
     */
    @JsonProperty("workorder_count")
    public Integer getWorkorder_count(){
        return workorder_count ;
    }

    /**
     * 设置 [WORKORDER_COUNT]
     */
    @JsonProperty("workorder_count")
    public void setWorkorder_count(Integer  workorder_count){
        this.workorder_count = workorder_count ;
        this.workorder_countDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_countDirtyFlag(){
        return workorder_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [DATE_PLANNED_FINISHED]
     */
    @JsonProperty("date_planned_finished")
    public Timestamp getDate_planned_finished(){
        return date_planned_finished ;
    }

    /**
     * 设置 [DATE_PLANNED_FINISHED]
     */
    @JsonProperty("date_planned_finished")
    public void setDate_planned_finished(Timestamp  date_planned_finished){
        this.date_planned_finished = date_planned_finished ;
        this.date_planned_finishedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_PLANNED_FINISHED]脏标记
     */
    @JsonIgnore
    public boolean getDate_planned_finishedDirtyFlag(){
        return date_planned_finishedDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return origin ;
    }

    /**
     * 设置 [ORIGIN]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return originDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [DATE_PLANNED_START]
     */
    @JsonProperty("date_planned_start")
    public Timestamp getDate_planned_start(){
        return date_planned_start ;
    }

    /**
     * 设置 [DATE_PLANNED_START]
     */
    @JsonProperty("date_planned_start")
    public void setDate_planned_start(Timestamp  date_planned_start){
        this.date_planned_start = date_planned_start ;
        this.date_planned_startDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_PLANNED_START]脏标记
     */
    @JsonIgnore
    public boolean getDate_planned_startDirtyFlag(){
        return date_planned_startDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [CHECK_TO_DONE]
     */
    @JsonProperty("check_to_done")
    public String getCheck_to_done(){
        return check_to_done ;
    }

    /**
     * 设置 [CHECK_TO_DONE]
     */
    @JsonProperty("check_to_done")
    public void setCheck_to_done(String  check_to_done){
        this.check_to_done = check_to_done ;
        this.check_to_doneDirtyFlag = true ;
    }

    /**
     * 获取 [CHECK_TO_DONE]脏标记
     */
    @JsonIgnore
    public boolean getCheck_to_doneDirtyFlag(){
        return check_to_doneDirtyFlag ;
    }

    /**
     * 获取 [POST_VISIBLE]
     */
    @JsonProperty("post_visible")
    public String getPost_visible(){
        return post_visible ;
    }

    /**
     * 设置 [POST_VISIBLE]
     */
    @JsonProperty("post_visible")
    public void setPost_visible(String  post_visible){
        this.post_visible = post_visible ;
        this.post_visibleDirtyFlag = true ;
    }

    /**
     * 获取 [POST_VISIBLE]脏标记
     */
    @JsonIgnore
    public boolean getPost_visibleDirtyFlag(){
        return post_visibleDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PICKING_IDS]
     */
    @JsonProperty("picking_ids")
    public String getPicking_ids(){
        return picking_ids ;
    }

    /**
     * 设置 [PICKING_IDS]
     */
    @JsonProperty("picking_ids")
    public void setPicking_ids(String  picking_ids){
        this.picking_ids = picking_ids ;
        this.picking_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPicking_idsDirtyFlag(){
        return picking_idsDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return product_uom_qty ;
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return product_uom_qtyDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [CONSUMED_LESS_THAN_PLANNED]
     */
    @JsonProperty("consumed_less_than_planned")
    public String getConsumed_less_than_planned(){
        return consumed_less_than_planned ;
    }

    /**
     * 设置 [CONSUMED_LESS_THAN_PLANNED]
     */
    @JsonProperty("consumed_less_than_planned")
    public void setConsumed_less_than_planned(String  consumed_less_than_planned){
        this.consumed_less_than_planned = consumed_less_than_planned ;
        this.consumed_less_than_plannedDirtyFlag = true ;
    }

    /**
     * 获取 [CONSUMED_LESS_THAN_PLANNED]脏标记
     */
    @JsonIgnore
    public boolean getConsumed_less_than_plannedDirtyFlag(){
        return consumed_less_than_plannedDirtyFlag ;
    }

    /**
     * 获取 [MOVE_RAW_IDS]
     */
    @JsonProperty("move_raw_ids")
    public String getMove_raw_ids(){
        return move_raw_ids ;
    }

    /**
     * 设置 [MOVE_RAW_IDS]
     */
    @JsonProperty("move_raw_ids")
    public void setMove_raw_ids(String  move_raw_ids){
        this.move_raw_ids = move_raw_ids ;
        this.move_raw_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_RAW_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_raw_idsDirtyFlag(){
        return move_raw_idsDirtyFlag ;
    }

    /**
     * 获取 [AVAILABILITY]
     */
    @JsonProperty("availability")
    public String getAvailability(){
        return availability ;
    }

    /**
     * 设置 [AVAILABILITY]
     */
    @JsonProperty("availability")
    public void setAvailability(String  availability){
        this.availability = availability ;
        this.availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getAvailabilityDirtyFlag(){
        return availabilityDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_IDS]
     */
    @JsonProperty("workorder_ids")
    public String getWorkorder_ids(){
        return workorder_ids ;
    }

    /**
     * 设置 [WORKORDER_IDS]
     */
    @JsonProperty("workorder_ids")
    public void setWorkorder_ids(String  workorder_ids){
        this.workorder_ids = workorder_ids ;
        this.workorder_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idsDirtyFlag(){
        return workorder_idsDirtyFlag ;
    }

    /**
     * 获取 [DATE_FINISHED]
     */
    @JsonProperty("date_finished")
    public Timestamp getDate_finished(){
        return date_finished ;
    }

    /**
     * 设置 [DATE_FINISHED]
     */
    @JsonProperty("date_finished")
    public void setDate_finished(Timestamp  date_finished){
        this.date_finished = date_finished ;
        this.date_finishedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_FINISHED]脏标记
     */
    @JsonIgnore
    public boolean getDate_finishedDirtyFlag(){
        return date_finishedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [PROPAGATE]
     */
    @JsonProperty("propagate")
    public String getPropagate(){
        return propagate ;
    }

    /**
     * 设置 [PROPAGATE]
     */
    @JsonProperty("propagate")
    public void setPropagate(String  propagate){
        this.propagate = propagate ;
        this.propagateDirtyFlag = true ;
    }

    /**
     * 获取 [PROPAGATE]脏标记
     */
    @JsonIgnore
    public boolean getPropagateDirtyFlag(){
        return propagateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return location_dest_id_text ;
    }

    /**
     * 设置 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return location_dest_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_SRC_ID_TEXT]
     */
    @JsonProperty("location_src_id_text")
    public String getLocation_src_id_text(){
        return location_src_id_text ;
    }

    /**
     * 设置 [LOCATION_SRC_ID_TEXT]
     */
    @JsonProperty("location_src_id_text")
    public void setLocation_src_id_text(String  location_src_id_text){
        this.location_src_id_text = location_src_id_text ;
        this.location_src_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_SRC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_src_id_textDirtyFlag(){
        return location_src_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_LOCATION_ID]
     */
    @JsonProperty("production_location_id")
    public Integer getProduction_location_id(){
        return production_location_id ;
    }

    /**
     * 设置 [PRODUCTION_LOCATION_ID]
     */
    @JsonProperty("production_location_id")
    public void setProduction_location_id(Integer  production_location_id){
        this.production_location_id = production_location_id ;
        this.production_location_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduction_location_idDirtyFlag(){
        return production_location_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return picking_type_id_text ;
    }

    /**
     * 设置 [PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return picking_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return product_tmpl_id ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return product_tmpl_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return product_uom_id_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return product_uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ROUTING_ID_TEXT]
     */
    @JsonProperty("routing_id_text")
    public String getRouting_id_text(){
        return routing_id_text ;
    }

    /**
     * 设置 [ROUTING_ID_TEXT]
     */
    @JsonProperty("routing_id_text")
    public void setRouting_id_text(String  routing_id_text){
        this.routing_id_text = routing_id_text ;
        this.routing_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRouting_id_textDirtyFlag(){
        return routing_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return product_uom_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return product_uom_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [ROUTING_ID]
     */
    @JsonProperty("routing_id")
    public Integer getRouting_id(){
        return routing_id ;
    }

    /**
     * 设置 [ROUTING_ID]
     */
    @JsonProperty("routing_id")
    public void setRouting_id(Integer  routing_id){
        this.routing_id = routing_id ;
        this.routing_idDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTING_ID]脏标记
     */
    @JsonIgnore
    public boolean getRouting_idDirtyFlag(){
        return routing_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [BOM_ID]
     */
    @JsonProperty("bom_id")
    public Integer getBom_id(){
        return bom_id ;
    }

    /**
     * 设置 [BOM_ID]
     */
    @JsonProperty("bom_id")
    public void setBom_id(Integer  bom_id){
        this.bom_id = bom_id ;
        this.bom_idDirtyFlag = true ;
    }

    /**
     * 获取 [BOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getBom_idDirtyFlag(){
        return bom_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_SRC_ID]
     */
    @JsonProperty("location_src_id")
    public Integer getLocation_src_id(){
        return location_src_id ;
    }

    /**
     * 设置 [LOCATION_SRC_ID]
     */
    @JsonProperty("location_src_id")
    public void setLocation_src_id(Integer  location_src_id){
        this.location_src_id = location_src_id ;
        this.location_src_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_SRC_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_src_idDirtyFlag(){
        return location_src_idDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ID]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return picking_type_id ;
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return picking_type_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return location_dest_id ;
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return location_dest_idDirtyFlag ;
    }



    public Mrp_production toDO() {
        Mrp_production srfdomain = new Mrp_production();
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getWorkorder_done_countDirtyFlag())
            srfdomain.setWorkorder_done_count(workorder_done_count);
        if(getFinished_move_line_idsDirtyFlag())
            srfdomain.setFinished_move_line_ids(finished_move_line_ids);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getQty_producedDirtyFlag())
            srfdomain.setQty_produced(qty_produced);
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getUnreserve_visibleDirtyFlag())
            srfdomain.setUnreserve_visible(unreserve_visible);
        if(getHas_movesDirtyFlag())
            srfdomain.setHas_moves(has_moves);
        if(getIs_lockedDirtyFlag())
            srfdomain.setIs_locked(is_locked);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getDate_startDirtyFlag())
            srfdomain.setDate_start(date_start);
        if(getMove_finished_idsDirtyFlag())
            srfdomain.setMove_finished_ids(move_finished_ids);
        if(getScrap_countDirtyFlag())
            srfdomain.setScrap_count(scrap_count);
        if(getScrap_idsDirtyFlag())
            srfdomain.setScrap_ids(scrap_ids);
        if(getShow_final_lotsDirtyFlag())
            srfdomain.setShow_final_lots(show_final_lots);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getMove_dest_idsDirtyFlag())
            srfdomain.setMove_dest_ids(move_dest_ids);
        if(getProcurement_group_idDirtyFlag())
            srfdomain.setProcurement_group_id(procurement_group_id);
        if(getDelivery_countDirtyFlag())
            srfdomain.setDelivery_count(delivery_count);
        if(getProduct_qtyDirtyFlag())
            srfdomain.setProduct_qty(product_qty);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getWorkorder_countDirtyFlag())
            srfdomain.setWorkorder_count(workorder_count);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getDate_planned_finishedDirtyFlag())
            srfdomain.setDate_planned_finished(date_planned_finished);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getOriginDirtyFlag())
            srfdomain.setOrigin(origin);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getDate_planned_startDirtyFlag())
            srfdomain.setDate_planned_start(date_planned_start);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getCheck_to_doneDirtyFlag())
            srfdomain.setCheck_to_done(check_to_done);
        if(getPost_visibleDirtyFlag())
            srfdomain.setPost_visible(post_visible);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getPicking_idsDirtyFlag())
            srfdomain.setPicking_ids(picking_ids);
        if(getProduct_uom_qtyDirtyFlag())
            srfdomain.setProduct_uom_qty(product_uom_qty);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getConsumed_less_than_plannedDirtyFlag())
            srfdomain.setConsumed_less_than_planned(consumed_less_than_planned);
        if(getMove_raw_idsDirtyFlag())
            srfdomain.setMove_raw_ids(move_raw_ids);
        if(getAvailabilityDirtyFlag())
            srfdomain.setAvailability(availability);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getWorkorder_idsDirtyFlag())
            srfdomain.setWorkorder_ids(workorder_ids);
        if(getDate_finishedDirtyFlag())
            srfdomain.setDate_finished(date_finished);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getPropagateDirtyFlag())
            srfdomain.setPropagate(propagate);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getLocation_dest_id_textDirtyFlag())
            srfdomain.setLocation_dest_id_text(location_dest_id_text);
        if(getLocation_src_id_textDirtyFlag())
            srfdomain.setLocation_src_id_text(location_src_id_text);
        if(getProduction_location_idDirtyFlag())
            srfdomain.setProduction_location_id(production_location_id);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getPicking_type_id_textDirtyFlag())
            srfdomain.setPicking_type_id_text(picking_type_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getProduct_tmpl_idDirtyFlag())
            srfdomain.setProduct_tmpl_id(product_tmpl_id);
        if(getProduct_uom_id_textDirtyFlag())
            srfdomain.setProduct_uom_id_text(product_uom_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getRouting_id_textDirtyFlag())
            srfdomain.setRouting_id_text(routing_id_text);
        if(getProduct_uom_idDirtyFlag())
            srfdomain.setProduct_uom_id(product_uom_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getRouting_idDirtyFlag())
            srfdomain.setRouting_id(routing_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getBom_idDirtyFlag())
            srfdomain.setBom_id(bom_id);
        if(getLocation_src_idDirtyFlag())
            srfdomain.setLocation_src_id(location_src_id);
        if(getPicking_type_idDirtyFlag())
            srfdomain.setPicking_type_id(picking_type_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getLocation_dest_idDirtyFlag())
            srfdomain.setLocation_dest_id(location_dest_id);

        return srfdomain;
    }

    public void fromDO(Mrp_production srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getWorkorder_done_countDirtyFlag())
            this.setWorkorder_done_count(srfdomain.getWorkorder_done_count());
        if(srfdomain.getFinished_move_line_idsDirtyFlag())
            this.setFinished_move_line_ids(srfdomain.getFinished_move_line_ids());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getQty_producedDirtyFlag())
            this.setQty_produced(srfdomain.getQty_produced());
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getUnreserve_visibleDirtyFlag())
            this.setUnreserve_visible(srfdomain.getUnreserve_visible());
        if(srfdomain.getHas_movesDirtyFlag())
            this.setHas_moves(srfdomain.getHas_moves());
        if(srfdomain.getIs_lockedDirtyFlag())
            this.setIs_locked(srfdomain.getIs_locked());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getDate_startDirtyFlag())
            this.setDate_start(srfdomain.getDate_start());
        if(srfdomain.getMove_finished_idsDirtyFlag())
            this.setMove_finished_ids(srfdomain.getMove_finished_ids());
        if(srfdomain.getScrap_countDirtyFlag())
            this.setScrap_count(srfdomain.getScrap_count());
        if(srfdomain.getScrap_idsDirtyFlag())
            this.setScrap_ids(srfdomain.getScrap_ids());
        if(srfdomain.getShow_final_lotsDirtyFlag())
            this.setShow_final_lots(srfdomain.getShow_final_lots());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getMove_dest_idsDirtyFlag())
            this.setMove_dest_ids(srfdomain.getMove_dest_ids());
        if(srfdomain.getProcurement_group_idDirtyFlag())
            this.setProcurement_group_id(srfdomain.getProcurement_group_id());
        if(srfdomain.getDelivery_countDirtyFlag())
            this.setDelivery_count(srfdomain.getDelivery_count());
        if(srfdomain.getProduct_qtyDirtyFlag())
            this.setProduct_qty(srfdomain.getProduct_qty());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getWorkorder_countDirtyFlag())
            this.setWorkorder_count(srfdomain.getWorkorder_count());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getDate_planned_finishedDirtyFlag())
            this.setDate_planned_finished(srfdomain.getDate_planned_finished());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getOriginDirtyFlag())
            this.setOrigin(srfdomain.getOrigin());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getDate_planned_startDirtyFlag())
            this.setDate_planned_start(srfdomain.getDate_planned_start());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getCheck_to_doneDirtyFlag())
            this.setCheck_to_done(srfdomain.getCheck_to_done());
        if(srfdomain.getPost_visibleDirtyFlag())
            this.setPost_visible(srfdomain.getPost_visible());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getPicking_idsDirtyFlag())
            this.setPicking_ids(srfdomain.getPicking_ids());
        if(srfdomain.getProduct_uom_qtyDirtyFlag())
            this.setProduct_uom_qty(srfdomain.getProduct_uom_qty());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getConsumed_less_than_plannedDirtyFlag())
            this.setConsumed_less_than_planned(srfdomain.getConsumed_less_than_planned());
        if(srfdomain.getMove_raw_idsDirtyFlag())
            this.setMove_raw_ids(srfdomain.getMove_raw_ids());
        if(srfdomain.getAvailabilityDirtyFlag())
            this.setAvailability(srfdomain.getAvailability());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getWorkorder_idsDirtyFlag())
            this.setWorkorder_ids(srfdomain.getWorkorder_ids());
        if(srfdomain.getDate_finishedDirtyFlag())
            this.setDate_finished(srfdomain.getDate_finished());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getPropagateDirtyFlag())
            this.setPropagate(srfdomain.getPropagate());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getLocation_dest_id_textDirtyFlag())
            this.setLocation_dest_id_text(srfdomain.getLocation_dest_id_text());
        if(srfdomain.getLocation_src_id_textDirtyFlag())
            this.setLocation_src_id_text(srfdomain.getLocation_src_id_text());
        if(srfdomain.getProduction_location_idDirtyFlag())
            this.setProduction_location_id(srfdomain.getProduction_location_id());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getPicking_type_id_textDirtyFlag())
            this.setPicking_type_id_text(srfdomain.getPicking_type_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getProduct_tmpl_idDirtyFlag())
            this.setProduct_tmpl_id(srfdomain.getProduct_tmpl_id());
        if(srfdomain.getProduct_uom_id_textDirtyFlag())
            this.setProduct_uom_id_text(srfdomain.getProduct_uom_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getRouting_id_textDirtyFlag())
            this.setRouting_id_text(srfdomain.getRouting_id_text());
        if(srfdomain.getProduct_uom_idDirtyFlag())
            this.setProduct_uom_id(srfdomain.getProduct_uom_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getRouting_idDirtyFlag())
            this.setRouting_id(srfdomain.getRouting_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getBom_idDirtyFlag())
            this.setBom_id(srfdomain.getBom_id());
        if(srfdomain.getLocation_src_idDirtyFlag())
            this.setLocation_src_id(srfdomain.getLocation_src_id());
        if(srfdomain.getPicking_type_idDirtyFlag())
            this.setPicking_type_id(srfdomain.getPicking_type_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getLocation_dest_idDirtyFlag())
            this.setLocation_dest_id(srfdomain.getLocation_dest_id());

    }

    public List<Mrp_productionDTO> fromDOPage(List<Mrp_production> poPage)   {
        if(poPage == null)
            return null;
        List<Mrp_productionDTO> dtos=new ArrayList<Mrp_productionDTO>();
        for(Mrp_production domain : poPage) {
            Mrp_productionDTO dto = new Mrp_productionDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

