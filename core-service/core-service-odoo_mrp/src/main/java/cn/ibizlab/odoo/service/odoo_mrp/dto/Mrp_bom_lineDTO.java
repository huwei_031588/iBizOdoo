package cn.ibizlab.odoo.service.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_bom_line.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mrp_bom_lineDTO]
 */
public class Mrp_bom_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ATTRIBUTE_VALUE_IDS]
     *
     */
    @Mrp_bom_lineAttribute_value_idsDefault(info = "默认规则")
    private String attribute_value_ids;

    @JsonIgnore
    private boolean attribute_value_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mrp_bom_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @Mrp_bom_lineProduct_qtyDefault(info = "默认规则")
    private Double product_qty;

    @JsonIgnore
    private boolean product_qtyDirtyFlag;

    /**
     * 属性 [CHILD_LINE_IDS]
     *
     */
    @Mrp_bom_lineChild_line_idsDefault(info = "默认规则")
    private String child_line_ids;

    @JsonIgnore
    private boolean child_line_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mrp_bom_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mrp_bom_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mrp_bom_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mrp_bom_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Mrp_bom_lineSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]
     *
     */
    @Mrp_bom_lineValid_product_attribute_value_idsDefault(info = "默认规则")
    private String valid_product_attribute_value_ids;

    @JsonIgnore
    private boolean valid_product_attribute_value_idsDirtyFlag;

    /**
     * 属性 [HAS_ATTACHMENTS]
     *
     */
    @Mrp_bom_lineHas_attachmentsDefault(info = "默认规则")
    private String has_attachments;

    @JsonIgnore
    private boolean has_attachmentsDirtyFlag;

    /**
     * 属性 [CHILD_BOM_ID]
     *
     */
    @Mrp_bom_lineChild_bom_idDefault(info = "默认规则")
    private Integer child_bom_id;

    @JsonIgnore
    private boolean child_bom_idDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @Mrp_bom_lineProduct_tmpl_idDefault(info = "默认规则")
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @Mrp_bom_lineProduct_uom_id_textDefault(info = "默认规则")
    private String product_uom_id_text;

    @JsonIgnore
    private boolean product_uom_id_textDirtyFlag;

    /**
     * 属性 [ROUTING_ID_TEXT]
     *
     */
    @Mrp_bom_lineRouting_id_textDefault(info = "默认规则")
    private String routing_id_text;

    @JsonIgnore
    private boolean routing_id_textDirtyFlag;

    /**
     * 属性 [PARENT_PRODUCT_TMPL_ID]
     *
     */
    @Mrp_bom_lineParent_product_tmpl_idDefault(info = "默认规则")
    private Integer parent_product_tmpl_id;

    @JsonIgnore
    private boolean parent_product_tmpl_idDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mrp_bom_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Mrp_bom_lineProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mrp_bom_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [OPERATION_ID_TEXT]
     *
     */
    @Mrp_bom_lineOperation_id_textDefault(info = "默认规则")
    private String operation_id_text;

    @JsonIgnore
    private boolean operation_id_textDirtyFlag;

    /**
     * 属性 [OPERATION_ID]
     *
     */
    @Mrp_bom_lineOperation_idDefault(info = "默认规则")
    private Integer operation_id;

    @JsonIgnore
    private boolean operation_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @Mrp_bom_lineProduct_uom_idDefault(info = "默认规则")
    private Integer product_uom_id;

    @JsonIgnore
    private boolean product_uom_idDirtyFlag;

    /**
     * 属性 [BOM_ID]
     *
     */
    @Mrp_bom_lineBom_idDefault(info = "默认规则")
    private Integer bom_id;

    @JsonIgnore
    private boolean bom_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mrp_bom_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mrp_bom_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [ROUTING_ID]
     *
     */
    @Mrp_bom_lineRouting_idDefault(info = "默认规则")
    private Integer routing_id;

    @JsonIgnore
    private boolean routing_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Mrp_bom_lineProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;


    /**
     * 获取 [ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("attribute_value_ids")
    public String getAttribute_value_ids(){
        return attribute_value_ids ;
    }

    /**
     * 设置 [ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("attribute_value_ids")
    public void setAttribute_value_ids(String  attribute_value_ids){
        this.attribute_value_ids = attribute_value_ids ;
        this.attribute_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTRIBUTE_VALUE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_value_idsDirtyFlag(){
        return attribute_value_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return product_qty ;
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return product_qtyDirtyFlag ;
    }

    /**
     * 获取 [CHILD_LINE_IDS]
     */
    @JsonProperty("child_line_ids")
    public String getChild_line_ids(){
        return child_line_ids ;
    }

    /**
     * 设置 [CHILD_LINE_IDS]
     */
    @JsonProperty("child_line_ids")
    public void setChild_line_ids(String  child_line_ids){
        this.child_line_ids = child_line_ids ;
        this.child_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChild_line_idsDirtyFlag(){
        return child_line_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("valid_product_attribute_value_ids")
    public String getValid_product_attribute_value_ids(){
        return valid_product_attribute_value_ids ;
    }

    /**
     * 设置 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]
     */
    @JsonProperty("valid_product_attribute_value_ids")
    public void setValid_product_attribute_value_ids(String  valid_product_attribute_value_ids){
        this.valid_product_attribute_value_ids = valid_product_attribute_value_ids ;
        this.valid_product_attribute_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getValid_product_attribute_value_idsDirtyFlag(){
        return valid_product_attribute_value_idsDirtyFlag ;
    }

    /**
     * 获取 [HAS_ATTACHMENTS]
     */
    @JsonProperty("has_attachments")
    public String getHas_attachments(){
        return has_attachments ;
    }

    /**
     * 设置 [HAS_ATTACHMENTS]
     */
    @JsonProperty("has_attachments")
    public void setHas_attachments(String  has_attachments){
        this.has_attachments = has_attachments ;
        this.has_attachmentsDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_ATTACHMENTS]脏标记
     */
    @JsonIgnore
    public boolean getHas_attachmentsDirtyFlag(){
        return has_attachmentsDirtyFlag ;
    }

    /**
     * 获取 [CHILD_BOM_ID]
     */
    @JsonProperty("child_bom_id")
    public Integer getChild_bom_id(){
        return child_bom_id ;
    }

    /**
     * 设置 [CHILD_BOM_ID]
     */
    @JsonProperty("child_bom_id")
    public void setChild_bom_id(Integer  child_bom_id){
        this.child_bom_id = child_bom_id ;
        this.child_bom_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_BOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getChild_bom_idDirtyFlag(){
        return child_bom_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return product_tmpl_id ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return product_tmpl_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return product_uom_id_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return product_uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [ROUTING_ID_TEXT]
     */
    @JsonProperty("routing_id_text")
    public String getRouting_id_text(){
        return routing_id_text ;
    }

    /**
     * 设置 [ROUTING_ID_TEXT]
     */
    @JsonProperty("routing_id_text")
    public void setRouting_id_text(String  routing_id_text){
        this.routing_id_text = routing_id_text ;
        this.routing_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRouting_id_textDirtyFlag(){
        return routing_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_PRODUCT_TMPL_ID]
     */
    @JsonProperty("parent_product_tmpl_id")
    public Integer getParent_product_tmpl_id(){
        return parent_product_tmpl_id ;
    }

    /**
     * 设置 [PARENT_PRODUCT_TMPL_ID]
     */
    @JsonProperty("parent_product_tmpl_id")
    public void setParent_product_tmpl_id(Integer  parent_product_tmpl_id){
        this.parent_product_tmpl_id = parent_product_tmpl_id ;
        this.parent_product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_product_tmpl_idDirtyFlag(){
        return parent_product_tmpl_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [OPERATION_ID_TEXT]
     */
    @JsonProperty("operation_id_text")
    public String getOperation_id_text(){
        return operation_id_text ;
    }

    /**
     * 设置 [OPERATION_ID_TEXT]
     */
    @JsonProperty("operation_id_text")
    public void setOperation_id_text(String  operation_id_text){
        this.operation_id_text = operation_id_text ;
        this.operation_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OPERATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOperation_id_textDirtyFlag(){
        return operation_id_textDirtyFlag ;
    }

    /**
     * 获取 [OPERATION_ID]
     */
    @JsonProperty("operation_id")
    public Integer getOperation_id(){
        return operation_id ;
    }

    /**
     * 设置 [OPERATION_ID]
     */
    @JsonProperty("operation_id")
    public void setOperation_id(Integer  operation_id){
        this.operation_id = operation_id ;
        this.operation_idDirtyFlag = true ;
    }

    /**
     * 获取 [OPERATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getOperation_idDirtyFlag(){
        return operation_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return product_uom_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return product_uom_idDirtyFlag ;
    }

    /**
     * 获取 [BOM_ID]
     */
    @JsonProperty("bom_id")
    public Integer getBom_id(){
        return bom_id ;
    }

    /**
     * 设置 [BOM_ID]
     */
    @JsonProperty("bom_id")
    public void setBom_id(Integer  bom_id){
        this.bom_id = bom_id ;
        this.bom_idDirtyFlag = true ;
    }

    /**
     * 获取 [BOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getBom_idDirtyFlag(){
        return bom_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [ROUTING_ID]
     */
    @JsonProperty("routing_id")
    public Integer getRouting_id(){
        return routing_id ;
    }

    /**
     * 设置 [ROUTING_ID]
     */
    @JsonProperty("routing_id")
    public void setRouting_id(Integer  routing_id){
        this.routing_id = routing_id ;
        this.routing_idDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTING_ID]脏标记
     */
    @JsonIgnore
    public boolean getRouting_idDirtyFlag(){
        return routing_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }



    public Mrp_bom_line toDO() {
        Mrp_bom_line srfdomain = new Mrp_bom_line();
        if(getAttribute_value_idsDirtyFlag())
            srfdomain.setAttribute_value_ids(attribute_value_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getProduct_qtyDirtyFlag())
            srfdomain.setProduct_qty(product_qty);
        if(getChild_line_idsDirtyFlag())
            srfdomain.setChild_line_ids(child_line_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getValid_product_attribute_value_idsDirtyFlag())
            srfdomain.setValid_product_attribute_value_ids(valid_product_attribute_value_ids);
        if(getHas_attachmentsDirtyFlag())
            srfdomain.setHas_attachments(has_attachments);
        if(getChild_bom_idDirtyFlag())
            srfdomain.setChild_bom_id(child_bom_id);
        if(getProduct_tmpl_idDirtyFlag())
            srfdomain.setProduct_tmpl_id(product_tmpl_id);
        if(getProduct_uom_id_textDirtyFlag())
            srfdomain.setProduct_uom_id_text(product_uom_id_text);
        if(getRouting_id_textDirtyFlag())
            srfdomain.setRouting_id_text(routing_id_text);
        if(getParent_product_tmpl_idDirtyFlag())
            srfdomain.setParent_product_tmpl_id(parent_product_tmpl_id);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getOperation_id_textDirtyFlag())
            srfdomain.setOperation_id_text(operation_id_text);
        if(getOperation_idDirtyFlag())
            srfdomain.setOperation_id(operation_id);
        if(getProduct_uom_idDirtyFlag())
            srfdomain.setProduct_uom_id(product_uom_id);
        if(getBom_idDirtyFlag())
            srfdomain.setBom_id(bom_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getRouting_idDirtyFlag())
            srfdomain.setRouting_id(routing_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);

        return srfdomain;
    }

    public void fromDO(Mrp_bom_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getAttribute_value_idsDirtyFlag())
            this.setAttribute_value_ids(srfdomain.getAttribute_value_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getProduct_qtyDirtyFlag())
            this.setProduct_qty(srfdomain.getProduct_qty());
        if(srfdomain.getChild_line_idsDirtyFlag())
            this.setChild_line_ids(srfdomain.getChild_line_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getValid_product_attribute_value_idsDirtyFlag())
            this.setValid_product_attribute_value_ids(srfdomain.getValid_product_attribute_value_ids());
        if(srfdomain.getHas_attachmentsDirtyFlag())
            this.setHas_attachments(srfdomain.getHas_attachments());
        if(srfdomain.getChild_bom_idDirtyFlag())
            this.setChild_bom_id(srfdomain.getChild_bom_id());
        if(srfdomain.getProduct_tmpl_idDirtyFlag())
            this.setProduct_tmpl_id(srfdomain.getProduct_tmpl_id());
        if(srfdomain.getProduct_uom_id_textDirtyFlag())
            this.setProduct_uom_id_text(srfdomain.getProduct_uom_id_text());
        if(srfdomain.getRouting_id_textDirtyFlag())
            this.setRouting_id_text(srfdomain.getRouting_id_text());
        if(srfdomain.getParent_product_tmpl_idDirtyFlag())
            this.setParent_product_tmpl_id(srfdomain.getParent_product_tmpl_id());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getOperation_id_textDirtyFlag())
            this.setOperation_id_text(srfdomain.getOperation_id_text());
        if(srfdomain.getOperation_idDirtyFlag())
            this.setOperation_id(srfdomain.getOperation_id());
        if(srfdomain.getProduct_uom_idDirtyFlag())
            this.setProduct_uom_id(srfdomain.getProduct_uom_id());
        if(srfdomain.getBom_idDirtyFlag())
            this.setBom_id(srfdomain.getBom_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getRouting_idDirtyFlag())
            this.setRouting_id(srfdomain.getRouting_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());

    }

    public List<Mrp_bom_lineDTO> fromDOPage(List<Mrp_bom_line> poPage)   {
        if(poPage == null)
            return null;
        List<Mrp_bom_lineDTO> dtos=new ArrayList<Mrp_bom_lineDTO>();
        for(Mrp_bom_line domain : poPage) {
            Mrp_bom_lineDTO dto = new Mrp_bom_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

