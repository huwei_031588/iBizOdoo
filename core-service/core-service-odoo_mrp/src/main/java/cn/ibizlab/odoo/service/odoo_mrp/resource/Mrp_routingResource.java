package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_routingDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_routingService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routingSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_routing" })
@RestController
@RequestMapping("")
public class Mrp_routingResource {

    @Autowired
    private IMrp_routingService mrp_routingService;

    public IMrp_routingService getMrp_routingService() {
        return this.mrp_routingService;
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_routing" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_routings/{mrp_routing_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_routing_id") Integer mrp_routing_id) {
        Mrp_routingDTO mrp_routingdto = new Mrp_routingDTO();
		Mrp_routing domain = new Mrp_routing();
		mrp_routingdto.setId(mrp_routing_id);
		domain.setId(mrp_routing_id);
        Boolean rst = mrp_routingService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_routing" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_routings/{mrp_routing_id}")
    public ResponseEntity<Mrp_routingDTO> get(@PathVariable("mrp_routing_id") Integer mrp_routing_id) {
        Mrp_routingDTO dto = new Mrp_routingDTO();
        Mrp_routing domain = mrp_routingService.get(mrp_routing_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_routing" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_routings/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_routingDTO> mrp_routingdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_routing" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_routings/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_routing(@RequestBody List<Mrp_routingDTO> mrp_routingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_routing" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_routings")

    public ResponseEntity<Mrp_routingDTO> create(@RequestBody Mrp_routingDTO mrp_routingdto) {
        Mrp_routingDTO dto = new Mrp_routingDTO();
        Mrp_routing domain = mrp_routingdto.toDO();
		mrp_routingService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_routing" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_routings/{mrp_routing_id}")

    public ResponseEntity<Mrp_routingDTO> update(@PathVariable("mrp_routing_id") Integer mrp_routing_id, @RequestBody Mrp_routingDTO mrp_routingdto) {
		Mrp_routing domain = mrp_routingdto.toDO();
        domain.setId(mrp_routing_id);
		mrp_routingService.update(domain);
		Mrp_routingDTO dto = new Mrp_routingDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_routing" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_routings/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_routingDTO> mrp_routingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_routing" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_routings/fetchdefault")
	public ResponseEntity<Page<Mrp_routingDTO>> fetchDefault(Mrp_routingSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_routingDTO> list = new ArrayList<Mrp_routingDTO>();
        
        Page<Mrp_routing> domains = mrp_routingService.searchDefault(context) ;
        for(Mrp_routing mrp_routing : domains.getContent()){
            Mrp_routingDTO dto = new Mrp_routingDTO();
            dto.fromDO(mrp_routing);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
