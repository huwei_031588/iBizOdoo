package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_workcenter_productivity_lossDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivity_lossService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_workcenter_productivity_loss" })
@RestController
@RequestMapping("")
public class Mrp_workcenter_productivity_lossResource {

    @Autowired
    private IMrp_workcenter_productivity_lossService mrp_workcenter_productivity_lossService;

    public IMrp_workcenter_productivity_lossService getMrp_workcenter_productivity_lossService() {
        return this.mrp_workcenter_productivity_lossService;
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivity_losses")

    public ResponseEntity<Mrp_workcenter_productivity_lossDTO> create(@RequestBody Mrp_workcenter_productivity_lossDTO mrp_workcenter_productivity_lossdto) {
        Mrp_workcenter_productivity_lossDTO dto = new Mrp_workcenter_productivity_lossDTO();
        Mrp_workcenter_productivity_loss domain = mrp_workcenter_productivity_lossdto.toDO();
		mrp_workcenter_productivity_lossService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivity_losses/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenter_productivity_lossDTO> mrp_workcenter_productivity_lossdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivity_losses/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_workcenter_productivity_lossDTO> mrp_workcenter_productivity_lossdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivity_losses/{mrp_workcenter_productivity_loss_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_productivity_loss_id") Integer mrp_workcenter_productivity_loss_id) {
        Mrp_workcenter_productivity_lossDTO mrp_workcenter_productivity_lossdto = new Mrp_workcenter_productivity_lossDTO();
		Mrp_workcenter_productivity_loss domain = new Mrp_workcenter_productivity_loss();
		mrp_workcenter_productivity_lossdto.setId(mrp_workcenter_productivity_loss_id);
		domain.setId(mrp_workcenter_productivity_loss_id);
        Boolean rst = mrp_workcenter_productivity_lossService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivity_losses/{mrp_workcenter_productivity_loss_id}")
    public ResponseEntity<Mrp_workcenter_productivity_lossDTO> get(@PathVariable("mrp_workcenter_productivity_loss_id") Integer mrp_workcenter_productivity_loss_id) {
        Mrp_workcenter_productivity_lossDTO dto = new Mrp_workcenter_productivity_lossDTO();
        Mrp_workcenter_productivity_loss domain = mrp_workcenter_productivity_lossService.get(mrp_workcenter_productivity_loss_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivity_losses/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_workcenter_productivity_loss(@RequestBody List<Mrp_workcenter_productivity_lossDTO> mrp_workcenter_productivity_lossdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivity_losses/{mrp_workcenter_productivity_loss_id}")

    public ResponseEntity<Mrp_workcenter_productivity_lossDTO> update(@PathVariable("mrp_workcenter_productivity_loss_id") Integer mrp_workcenter_productivity_loss_id, @RequestBody Mrp_workcenter_productivity_lossDTO mrp_workcenter_productivity_lossdto) {
		Mrp_workcenter_productivity_loss domain = mrp_workcenter_productivity_lossdto.toDO();
        domain.setId(mrp_workcenter_productivity_loss_id);
		mrp_workcenter_productivity_lossService.update(domain);
		Mrp_workcenter_productivity_lossDTO dto = new Mrp_workcenter_productivity_lossDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_workcenter_productivity_loss" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_workcenter_productivity_losses/fetchdefault")
	public ResponseEntity<Page<Mrp_workcenter_productivity_lossDTO>> fetchDefault(Mrp_workcenter_productivity_lossSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_workcenter_productivity_lossDTO> list = new ArrayList<Mrp_workcenter_productivity_lossDTO>();
        
        Page<Mrp_workcenter_productivity_loss> domains = mrp_workcenter_productivity_lossService.searchDefault(context) ;
        for(Mrp_workcenter_productivity_loss mrp_workcenter_productivity_loss : domains.getContent()){
            Mrp_workcenter_productivity_lossDTO dto = new Mrp_workcenter_productivity_lossDTO();
            dto.fromDO(mrp_workcenter_productivity_loss);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
