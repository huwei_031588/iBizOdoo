package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_productionDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_production;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_productionService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_productionSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_production" })
@RestController
@RequestMapping("")
public class Mrp_productionResource {

    @Autowired
    private IMrp_productionService mrp_productionService;

    public IMrp_productionService getMrp_productionService() {
        return this.mrp_productionService;
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_production" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_productions")

    public ResponseEntity<Mrp_productionDTO> create(@RequestBody Mrp_productionDTO mrp_productiondto) {
        Mrp_productionDTO dto = new Mrp_productionDTO();
        Mrp_production domain = mrp_productiondto.toDO();
		mrp_productionService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_production" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_productions/{mrp_production_id}")

    public ResponseEntity<Mrp_productionDTO> update(@PathVariable("mrp_production_id") Integer mrp_production_id, @RequestBody Mrp_productionDTO mrp_productiondto) {
		Mrp_production domain = mrp_productiondto.toDO();
        domain.setId(mrp_production_id);
		mrp_productionService.update(domain);
		Mrp_productionDTO dto = new Mrp_productionDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_production" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_productions/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_productionDTO> mrp_productiondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_production" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_productions/{mrp_production_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_production_id") Integer mrp_production_id) {
        Mrp_productionDTO mrp_productiondto = new Mrp_productionDTO();
		Mrp_production domain = new Mrp_production();
		mrp_productiondto.setId(mrp_production_id);
		domain.setId(mrp_production_id);
        Boolean rst = mrp_productionService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_production" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_productions/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_production(@RequestBody List<Mrp_productionDTO> mrp_productiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_production" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_productions/{mrp_production_id}")
    public ResponseEntity<Mrp_productionDTO> get(@PathVariable("mrp_production_id") Integer mrp_production_id) {
        Mrp_productionDTO dto = new Mrp_productionDTO();
        Mrp_production domain = mrp_productionService.get(mrp_production_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_production" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_productions/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_productionDTO> mrp_productiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_production" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_productions/fetchdefault")
	public ResponseEntity<Page<Mrp_productionDTO>> fetchDefault(Mrp_productionSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_productionDTO> list = new ArrayList<Mrp_productionDTO>();
        
        Page<Mrp_production> domains = mrp_productionService.searchDefault(context) ;
        for(Mrp_production mrp_production : domains.getContent()){
            Mrp_productionDTO dto = new Mrp_productionDTO();
            dto.fromDO(mrp_production);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
