package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_documentDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_documentService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_documentSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_document" })
@RestController
@RequestMapping("")
public class Mrp_documentResource {

    @Autowired
    private IMrp_documentService mrp_documentService;

    public IMrp_documentService getMrp_documentService() {
        return this.mrp_documentService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_document" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_documents/{mrp_document_id}")

    public ResponseEntity<Mrp_documentDTO> update(@PathVariable("mrp_document_id") Integer mrp_document_id, @RequestBody Mrp_documentDTO mrp_documentdto) {
		Mrp_document domain = mrp_documentdto.toDO();
        domain.setId(mrp_document_id);
		mrp_documentService.update(domain);
		Mrp_documentDTO dto = new Mrp_documentDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_document" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_documents")

    public ResponseEntity<Mrp_documentDTO> create(@RequestBody Mrp_documentDTO mrp_documentdto) {
        Mrp_documentDTO dto = new Mrp_documentDTO();
        Mrp_document domain = mrp_documentdto.toDO();
		mrp_documentService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_document" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_documents/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_documentDTO> mrp_documentdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_document" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_documents/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_document(@RequestBody List<Mrp_documentDTO> mrp_documentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_document" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_documents/{mrp_document_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_document_id") Integer mrp_document_id) {
        Mrp_documentDTO mrp_documentdto = new Mrp_documentDTO();
		Mrp_document domain = new Mrp_document();
		mrp_documentdto.setId(mrp_document_id);
		domain.setId(mrp_document_id);
        Boolean rst = mrp_documentService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_document" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_documents/{mrp_document_id}")
    public ResponseEntity<Mrp_documentDTO> get(@PathVariable("mrp_document_id") Integer mrp_document_id) {
        Mrp_documentDTO dto = new Mrp_documentDTO();
        Mrp_document domain = mrp_documentService.get(mrp_document_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_document" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_documents/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_documentDTO> mrp_documentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_document" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_documents/fetchdefault")
	public ResponseEntity<Page<Mrp_documentDTO>> fetchDefault(Mrp_documentSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_documentDTO> list = new ArrayList<Mrp_documentDTO>();
        
        Page<Mrp_document> domains = mrp_documentService.searchDefault(context) ;
        for(Mrp_document mrp_document : domains.getContent()){
            Mrp_documentDTO dto = new Mrp_documentDTO();
            dto.fromDO(mrp_document);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
