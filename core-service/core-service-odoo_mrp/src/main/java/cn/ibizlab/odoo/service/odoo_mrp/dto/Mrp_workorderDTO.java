package cn.ibizlab.odoo.service.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_workorder.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mrp_workorderDTO]
 */
public class Mrp_workorderDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Mrp_workorderMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Mrp_workorderMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Mrp_workorderMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [TIME_IDS]
     *
     */
    @Mrp_workorderTime_idsDefault(info = "默认规则")
    private String time_ids;

    @JsonIgnore
    private boolean time_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Mrp_workorderMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [WORKING_USER_IDS]
     *
     */
    @Mrp_workorderWorking_user_idsDefault(info = "默认规则")
    private String working_user_ids;

    @JsonIgnore
    private boolean working_user_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mrp_workorderDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [QTY_PRODUCED]
     *
     */
    @Mrp_workorderQty_producedDefault(info = "默认规则")
    private Double qty_produced;

    @JsonIgnore
    private boolean qty_producedDirtyFlag;

    /**
     * 属性 [DURATION]
     *
     */
    @Mrp_workorderDurationDefault(info = "默认规则")
    private Double duration;

    @JsonIgnore
    private boolean durationDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Mrp_workorderMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Mrp_workorderStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [SCRAP_COUNT]
     *
     */
    @Mrp_workorderScrap_countDefault(info = "默认规则")
    private Integer scrap_count;

    @JsonIgnore
    private boolean scrap_countDirtyFlag;

    /**
     * 属性 [DATE_PLANNED_START]
     *
     */
    @Mrp_workorderDate_planned_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_planned_start;

    @JsonIgnore
    private boolean date_planned_startDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Mrp_workorderMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [IS_USER_WORKING]
     *
     */
    @Mrp_workorderIs_user_workingDefault(info = "默认规则")
    private String is_user_working;

    @JsonIgnore
    private boolean is_user_workingDirtyFlag;

    /**
     * 属性 [MOVE_RAW_IDS]
     *
     */
    @Mrp_workorderMove_raw_idsDefault(info = "默认规则")
    private String move_raw_ids;

    @JsonIgnore
    private boolean move_raw_idsDirtyFlag;

    /**
     * 属性 [LAST_WORKING_USER_ID]
     *
     */
    @Mrp_workorderLast_working_user_idDefault(info = "默认规则")
    private String last_working_user_id;

    @JsonIgnore
    private boolean last_working_user_idDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Mrp_workorderMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @Mrp_workorderMove_line_idsDefault(info = "默认规则")
    private String move_line_ids;

    @JsonIgnore
    private boolean move_line_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mrp_workorderNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [IS_FIRST_WO]
     *
     */
    @Mrp_workorderIs_first_woDefault(info = "默认规则")
    private String is_first_wo;

    @JsonIgnore
    private boolean is_first_woDirtyFlag;

    /**
     * 属性 [IS_PRODUCED]
     *
     */
    @Mrp_workorderIs_producedDefault(info = "默认规则")
    private String is_produced;

    @JsonIgnore
    private boolean is_producedDirtyFlag;

    /**
     * 属性 [DATE_START]
     *
     */
    @Mrp_workorderDate_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_start;

    @JsonIgnore
    private boolean date_startDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Mrp_workorderWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mrp_workorder__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Mrp_workorderMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Mrp_workorderColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Mrp_workorderMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mrp_workorderWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Mrp_workorderMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [DURATION_UNIT]
     *
     */
    @Mrp_workorderDuration_unitDefault(info = "默认规则")
    private Double duration_unit;

    @JsonIgnore
    private boolean duration_unitDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Mrp_workorderMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [CAPACITY]
     *
     */
    @Mrp_workorderCapacityDefault(info = "默认规则")
    private Double capacity;

    @JsonIgnore
    private boolean capacityDirtyFlag;

    /**
     * 属性 [ACTIVE_MOVE_LINE_IDS]
     *
     */
    @Mrp_workorderActive_move_line_idsDefault(info = "默认规则")
    private String active_move_line_ids;

    @JsonIgnore
    private boolean active_move_line_idsDirtyFlag;

    /**
     * 属性 [DATE_PLANNED_FINISHED]
     *
     */
    @Mrp_workorderDate_planned_finishedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_planned_finished;

    @JsonIgnore
    private boolean date_planned_finishedDirtyFlag;

    /**
     * 属性 [DATE_FINISHED]
     *
     */
    @Mrp_workorderDate_finishedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_finished;

    @JsonIgnore
    private boolean date_finishedDirtyFlag;

    /**
     * 属性 [QTY_REMAINING]
     *
     */
    @Mrp_workorderQty_remainingDefault(info = "默认规则")
    private Double qty_remaining;

    @JsonIgnore
    private boolean qty_remainingDirtyFlag;

    /**
     * 属性 [SCRAP_IDS]
     *
     */
    @Mrp_workorderScrap_idsDefault(info = "默认规则")
    private String scrap_ids;

    @JsonIgnore
    private boolean scrap_idsDirtyFlag;

    /**
     * 属性 [DURATION_PERCENT]
     *
     */
    @Mrp_workorderDuration_percentDefault(info = "默认规则")
    private Integer duration_percent;

    @JsonIgnore
    private boolean duration_percentDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mrp_workorderIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mrp_workorderCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DURATION_EXPECTED]
     *
     */
    @Mrp_workorderDuration_expectedDefault(info = "默认规则")
    private Double duration_expected;

    @JsonIgnore
    private boolean duration_expectedDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Mrp_workorderMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Mrp_workorderMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [QTY_PRODUCING]
     *
     */
    @Mrp_workorderQty_producingDefault(info = "默认规则")
    private Double qty_producing;

    @JsonIgnore
    private boolean qty_producingDirtyFlag;

    /**
     * 属性 [PRODUCTION_DATE]
     *
     */
    @Mrp_workorderProduction_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp production_date;

    @JsonIgnore
    private boolean production_dateDirtyFlag;

    /**
     * 属性 [WORKSHEET]
     *
     */
    @Mrp_workorderWorksheetDefault(info = "默认规则")
    private byte[] worksheet;

    @JsonIgnore
    private boolean worksheetDirtyFlag;

    /**
     * 属性 [WORKCENTER_ID_TEXT]
     *
     */
    @Mrp_workorderWorkcenter_id_textDefault(info = "默认规则")
    private String workcenter_id_text;

    @JsonIgnore
    private boolean workcenter_id_textDirtyFlag;

    /**
     * 属性 [FINAL_LOT_ID_TEXT]
     *
     */
    @Mrp_workorderFinal_lot_id_textDefault(info = "默认规则")
    private String final_lot_id_text;

    @JsonIgnore
    private boolean final_lot_id_textDirtyFlag;

    /**
     * 属性 [WORKING_STATE]
     *
     */
    @Mrp_workorderWorking_stateDefault(info = "默认规则")
    private String working_state;

    @JsonIgnore
    private boolean working_stateDirtyFlag;

    /**
     * 属性 [QTY_PRODUCTION]
     *
     */
    @Mrp_workorderQty_productionDefault(info = "默认规则")
    private Double qty_production;

    @JsonIgnore
    private boolean qty_productionDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @Mrp_workorderProduct_uom_idDefault(info = "默认规则")
    private Integer product_uom_id;

    @JsonIgnore
    private boolean product_uom_idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mrp_workorderCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCTION_AVAILABILITY]
     *
     */
    @Mrp_workorderProduction_availabilityDefault(info = "默认规则")
    private String production_availability;

    @JsonIgnore
    private boolean production_availabilityDirtyFlag;

    /**
     * 属性 [PRODUCTION_STATE]
     *
     */
    @Mrp_workorderProduction_stateDefault(info = "默认规则")
    private String production_state;

    @JsonIgnore
    private boolean production_stateDirtyFlag;

    /**
     * 属性 [NEXT_WORK_ORDER_ID_TEXT]
     *
     */
    @Mrp_workorderNext_work_order_id_textDefault(info = "默认规则")
    private String next_work_order_id_text;

    @JsonIgnore
    private boolean next_work_order_id_textDirtyFlag;

    /**
     * 属性 [OPERATION_ID_TEXT]
     *
     */
    @Mrp_workorderOperation_id_textDefault(info = "默认规则")
    private String operation_id_text;

    @JsonIgnore
    private boolean operation_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mrp_workorderWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID_TEXT]
     *
     */
    @Mrp_workorderProduction_id_textDefault(info = "默认规则")
    private String production_id_text;

    @JsonIgnore
    private boolean production_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Mrp_workorderProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_TRACKING]
     *
     */
    @Mrp_workorderProduct_trackingDefault(info = "默认规则")
    private String product_tracking;

    @JsonIgnore
    private boolean product_trackingDirtyFlag;

    /**
     * 属性 [FINAL_LOT_ID]
     *
     */
    @Mrp_workorderFinal_lot_idDefault(info = "默认规则")
    private Integer final_lot_id;

    @JsonIgnore
    private boolean final_lot_idDirtyFlag;

    /**
     * 属性 [WORKCENTER_ID]
     *
     */
    @Mrp_workorderWorkcenter_idDefault(info = "默认规则")
    private Integer workcenter_id;

    @JsonIgnore
    private boolean workcenter_idDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @Mrp_workorderProduction_idDefault(info = "默认规则")
    private Integer production_id;

    @JsonIgnore
    private boolean production_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mrp_workorderWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [OPERATION_ID]
     *
     */
    @Mrp_workorderOperation_idDefault(info = "默认规则")
    private Integer operation_id;

    @JsonIgnore
    private boolean operation_idDirtyFlag;

    /**
     * 属性 [NEXT_WORK_ORDER_ID]
     *
     */
    @Mrp_workorderNext_work_order_idDefault(info = "默认规则")
    private Integer next_work_order_id;

    @JsonIgnore
    private boolean next_work_order_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Mrp_workorderProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mrp_workorderCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [TIME_IDS]
     */
    @JsonProperty("time_ids")
    public String getTime_ids(){
        return time_ids ;
    }

    /**
     * 设置 [TIME_IDS]
     */
    @JsonProperty("time_ids")
    public void setTime_ids(String  time_ids){
        this.time_ids = time_ids ;
        this.time_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTime_idsDirtyFlag(){
        return time_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [WORKING_USER_IDS]
     */
    @JsonProperty("working_user_ids")
    public String getWorking_user_ids(){
        return working_user_ids ;
    }

    /**
     * 设置 [WORKING_USER_IDS]
     */
    @JsonProperty("working_user_ids")
    public void setWorking_user_ids(String  working_user_ids){
        this.working_user_ids = working_user_ids ;
        this.working_user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WORKING_USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWorking_user_idsDirtyFlag(){
        return working_user_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [QTY_PRODUCED]
     */
    @JsonProperty("qty_produced")
    public Double getQty_produced(){
        return qty_produced ;
    }

    /**
     * 设置 [QTY_PRODUCED]
     */
    @JsonProperty("qty_produced")
    public void setQty_produced(Double  qty_produced){
        this.qty_produced = qty_produced ;
        this.qty_producedDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_PRODUCED]脏标记
     */
    @JsonIgnore
    public boolean getQty_producedDirtyFlag(){
        return qty_producedDirtyFlag ;
    }

    /**
     * 获取 [DURATION]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return duration ;
    }

    /**
     * 设置 [DURATION]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

    /**
     * 获取 [DURATION]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return durationDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [SCRAP_COUNT]
     */
    @JsonProperty("scrap_count")
    public Integer getScrap_count(){
        return scrap_count ;
    }

    /**
     * 设置 [SCRAP_COUNT]
     */
    @JsonProperty("scrap_count")
    public void setScrap_count(Integer  scrap_count){
        this.scrap_count = scrap_count ;
        this.scrap_countDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAP_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getScrap_countDirtyFlag(){
        return scrap_countDirtyFlag ;
    }

    /**
     * 获取 [DATE_PLANNED_START]
     */
    @JsonProperty("date_planned_start")
    public Timestamp getDate_planned_start(){
        return date_planned_start ;
    }

    /**
     * 设置 [DATE_PLANNED_START]
     */
    @JsonProperty("date_planned_start")
    public void setDate_planned_start(Timestamp  date_planned_start){
        this.date_planned_start = date_planned_start ;
        this.date_planned_startDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_PLANNED_START]脏标记
     */
    @JsonIgnore
    public boolean getDate_planned_startDirtyFlag(){
        return date_planned_startDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [IS_USER_WORKING]
     */
    @JsonProperty("is_user_working")
    public String getIs_user_working(){
        return is_user_working ;
    }

    /**
     * 设置 [IS_USER_WORKING]
     */
    @JsonProperty("is_user_working")
    public void setIs_user_working(String  is_user_working){
        this.is_user_working = is_user_working ;
        this.is_user_workingDirtyFlag = true ;
    }

    /**
     * 获取 [IS_USER_WORKING]脏标记
     */
    @JsonIgnore
    public boolean getIs_user_workingDirtyFlag(){
        return is_user_workingDirtyFlag ;
    }

    /**
     * 获取 [MOVE_RAW_IDS]
     */
    @JsonProperty("move_raw_ids")
    public String getMove_raw_ids(){
        return move_raw_ids ;
    }

    /**
     * 设置 [MOVE_RAW_IDS]
     */
    @JsonProperty("move_raw_ids")
    public void setMove_raw_ids(String  move_raw_ids){
        this.move_raw_ids = move_raw_ids ;
        this.move_raw_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_RAW_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_raw_idsDirtyFlag(){
        return move_raw_idsDirtyFlag ;
    }

    /**
     * 获取 [LAST_WORKING_USER_ID]
     */
    @JsonProperty("last_working_user_id")
    public String getLast_working_user_id(){
        return last_working_user_id ;
    }

    /**
     * 设置 [LAST_WORKING_USER_ID]
     */
    @JsonProperty("last_working_user_id")
    public void setLast_working_user_id(String  last_working_user_id){
        this.last_working_user_id = last_working_user_id ;
        this.last_working_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_WORKING_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getLast_working_user_idDirtyFlag(){
        return last_working_user_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return move_line_ids ;
    }

    /**
     * 设置 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [IS_FIRST_WO]
     */
    @JsonProperty("is_first_wo")
    public String getIs_first_wo(){
        return is_first_wo ;
    }

    /**
     * 设置 [IS_FIRST_WO]
     */
    @JsonProperty("is_first_wo")
    public void setIs_first_wo(String  is_first_wo){
        this.is_first_wo = is_first_wo ;
        this.is_first_woDirtyFlag = true ;
    }

    /**
     * 获取 [IS_FIRST_WO]脏标记
     */
    @JsonIgnore
    public boolean getIs_first_woDirtyFlag(){
        return is_first_woDirtyFlag ;
    }

    /**
     * 获取 [IS_PRODUCED]
     */
    @JsonProperty("is_produced")
    public String getIs_produced(){
        return is_produced ;
    }

    /**
     * 设置 [IS_PRODUCED]
     */
    @JsonProperty("is_produced")
    public void setIs_produced(String  is_produced){
        this.is_produced = is_produced ;
        this.is_producedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PRODUCED]脏标记
     */
    @JsonIgnore
    public boolean getIs_producedDirtyFlag(){
        return is_producedDirtyFlag ;
    }

    /**
     * 获取 [DATE_START]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return date_start ;
    }

    /**
     * 设置 [DATE_START]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_START]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return date_startDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [DURATION_UNIT]
     */
    @JsonProperty("duration_unit")
    public Double getDuration_unit(){
        return duration_unit ;
    }

    /**
     * 设置 [DURATION_UNIT]
     */
    @JsonProperty("duration_unit")
    public void setDuration_unit(Double  duration_unit){
        this.duration_unit = duration_unit ;
        this.duration_unitDirtyFlag = true ;
    }

    /**
     * 获取 [DURATION_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getDuration_unitDirtyFlag(){
        return duration_unitDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [CAPACITY]
     */
    @JsonProperty("capacity")
    public Double getCapacity(){
        return capacity ;
    }

    /**
     * 设置 [CAPACITY]
     */
    @JsonProperty("capacity")
    public void setCapacity(Double  capacity){
        this.capacity = capacity ;
        this.capacityDirtyFlag = true ;
    }

    /**
     * 获取 [CAPACITY]脏标记
     */
    @JsonIgnore
    public boolean getCapacityDirtyFlag(){
        return capacityDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE_MOVE_LINE_IDS]
     */
    @JsonProperty("active_move_line_ids")
    public String getActive_move_line_ids(){
        return active_move_line_ids ;
    }

    /**
     * 设置 [ACTIVE_MOVE_LINE_IDS]
     */
    @JsonProperty("active_move_line_ids")
    public void setActive_move_line_ids(String  active_move_line_ids){
        this.active_move_line_ids = active_move_line_ids ;
        this.active_move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE_MOVE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActive_move_line_idsDirtyFlag(){
        return active_move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [DATE_PLANNED_FINISHED]
     */
    @JsonProperty("date_planned_finished")
    public Timestamp getDate_planned_finished(){
        return date_planned_finished ;
    }

    /**
     * 设置 [DATE_PLANNED_FINISHED]
     */
    @JsonProperty("date_planned_finished")
    public void setDate_planned_finished(Timestamp  date_planned_finished){
        this.date_planned_finished = date_planned_finished ;
        this.date_planned_finishedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_PLANNED_FINISHED]脏标记
     */
    @JsonIgnore
    public boolean getDate_planned_finishedDirtyFlag(){
        return date_planned_finishedDirtyFlag ;
    }

    /**
     * 获取 [DATE_FINISHED]
     */
    @JsonProperty("date_finished")
    public Timestamp getDate_finished(){
        return date_finished ;
    }

    /**
     * 设置 [DATE_FINISHED]
     */
    @JsonProperty("date_finished")
    public void setDate_finished(Timestamp  date_finished){
        this.date_finished = date_finished ;
        this.date_finishedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_FINISHED]脏标记
     */
    @JsonIgnore
    public boolean getDate_finishedDirtyFlag(){
        return date_finishedDirtyFlag ;
    }

    /**
     * 获取 [QTY_REMAINING]
     */
    @JsonProperty("qty_remaining")
    public Double getQty_remaining(){
        return qty_remaining ;
    }

    /**
     * 设置 [QTY_REMAINING]
     */
    @JsonProperty("qty_remaining")
    public void setQty_remaining(Double  qty_remaining){
        this.qty_remaining = qty_remaining ;
        this.qty_remainingDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_REMAINING]脏标记
     */
    @JsonIgnore
    public boolean getQty_remainingDirtyFlag(){
        return qty_remainingDirtyFlag ;
    }

    /**
     * 获取 [SCRAP_IDS]
     */
    @JsonProperty("scrap_ids")
    public String getScrap_ids(){
        return scrap_ids ;
    }

    /**
     * 设置 [SCRAP_IDS]
     */
    @JsonProperty("scrap_ids")
    public void setScrap_ids(String  scrap_ids){
        this.scrap_ids = scrap_ids ;
        this.scrap_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAP_IDS]脏标记
     */
    @JsonIgnore
    public boolean getScrap_idsDirtyFlag(){
        return scrap_idsDirtyFlag ;
    }

    /**
     * 获取 [DURATION_PERCENT]
     */
    @JsonProperty("duration_percent")
    public Integer getDuration_percent(){
        return duration_percent ;
    }

    /**
     * 设置 [DURATION_PERCENT]
     */
    @JsonProperty("duration_percent")
    public void setDuration_percent(Integer  duration_percent){
        this.duration_percent = duration_percent ;
        this.duration_percentDirtyFlag = true ;
    }

    /**
     * 获取 [DURATION_PERCENT]脏标记
     */
    @JsonIgnore
    public boolean getDuration_percentDirtyFlag(){
        return duration_percentDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DURATION_EXPECTED]
     */
    @JsonProperty("duration_expected")
    public Double getDuration_expected(){
        return duration_expected ;
    }

    /**
     * 设置 [DURATION_EXPECTED]
     */
    @JsonProperty("duration_expected")
    public void setDuration_expected(Double  duration_expected){
        this.duration_expected = duration_expected ;
        this.duration_expectedDirtyFlag = true ;
    }

    /**
     * 获取 [DURATION_EXPECTED]脏标记
     */
    @JsonIgnore
    public boolean getDuration_expectedDirtyFlag(){
        return duration_expectedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [QTY_PRODUCING]
     */
    @JsonProperty("qty_producing")
    public Double getQty_producing(){
        return qty_producing ;
    }

    /**
     * 设置 [QTY_PRODUCING]
     */
    @JsonProperty("qty_producing")
    public void setQty_producing(Double  qty_producing){
        this.qty_producing = qty_producing ;
        this.qty_producingDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_PRODUCING]脏标记
     */
    @JsonIgnore
    public boolean getQty_producingDirtyFlag(){
        return qty_producingDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_DATE]
     */
    @JsonProperty("production_date")
    public Timestamp getProduction_date(){
        return production_date ;
    }

    /**
     * 设置 [PRODUCTION_DATE]
     */
    @JsonProperty("production_date")
    public void setProduction_date(Timestamp  production_date){
        this.production_date = production_date ;
        this.production_dateDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_DATE]脏标记
     */
    @JsonIgnore
    public boolean getProduction_dateDirtyFlag(){
        return production_dateDirtyFlag ;
    }

    /**
     * 获取 [WORKSHEET]
     */
    @JsonProperty("worksheet")
    public byte[] getWorksheet(){
        return worksheet ;
    }

    /**
     * 设置 [WORKSHEET]
     */
    @JsonProperty("worksheet")
    public void setWorksheet(byte[]  worksheet){
        this.worksheet = worksheet ;
        this.worksheetDirtyFlag = true ;
    }

    /**
     * 获取 [WORKSHEET]脏标记
     */
    @JsonIgnore
    public boolean getWorksheetDirtyFlag(){
        return worksheetDirtyFlag ;
    }

    /**
     * 获取 [WORKCENTER_ID_TEXT]
     */
    @JsonProperty("workcenter_id_text")
    public String getWorkcenter_id_text(){
        return workcenter_id_text ;
    }

    /**
     * 设置 [WORKCENTER_ID_TEXT]
     */
    @JsonProperty("workcenter_id_text")
    public void setWorkcenter_id_text(String  workcenter_id_text){
        this.workcenter_id_text = workcenter_id_text ;
        this.workcenter_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WORKCENTER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_id_textDirtyFlag(){
        return workcenter_id_textDirtyFlag ;
    }

    /**
     * 获取 [FINAL_LOT_ID_TEXT]
     */
    @JsonProperty("final_lot_id_text")
    public String getFinal_lot_id_text(){
        return final_lot_id_text ;
    }

    /**
     * 设置 [FINAL_LOT_ID_TEXT]
     */
    @JsonProperty("final_lot_id_text")
    public void setFinal_lot_id_text(String  final_lot_id_text){
        this.final_lot_id_text = final_lot_id_text ;
        this.final_lot_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [FINAL_LOT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getFinal_lot_id_textDirtyFlag(){
        return final_lot_id_textDirtyFlag ;
    }

    /**
     * 获取 [WORKING_STATE]
     */
    @JsonProperty("working_state")
    public String getWorking_state(){
        return working_state ;
    }

    /**
     * 设置 [WORKING_STATE]
     */
    @JsonProperty("working_state")
    public void setWorking_state(String  working_state){
        this.working_state = working_state ;
        this.working_stateDirtyFlag = true ;
    }

    /**
     * 获取 [WORKING_STATE]脏标记
     */
    @JsonIgnore
    public boolean getWorking_stateDirtyFlag(){
        return working_stateDirtyFlag ;
    }

    /**
     * 获取 [QTY_PRODUCTION]
     */
    @JsonProperty("qty_production")
    public Double getQty_production(){
        return qty_production ;
    }

    /**
     * 设置 [QTY_PRODUCTION]
     */
    @JsonProperty("qty_production")
    public void setQty_production(Double  qty_production){
        this.qty_production = qty_production ;
        this.qty_productionDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_PRODUCTION]脏标记
     */
    @JsonIgnore
    public boolean getQty_productionDirtyFlag(){
        return qty_productionDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return product_uom_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return product_uom_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_AVAILABILITY]
     */
    @JsonProperty("production_availability")
    public String getProduction_availability(){
        return production_availability ;
    }

    /**
     * 设置 [PRODUCTION_AVAILABILITY]
     */
    @JsonProperty("production_availability")
    public void setProduction_availability(String  production_availability){
        this.production_availability = production_availability ;
        this.production_availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getProduction_availabilityDirtyFlag(){
        return production_availabilityDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_STATE]
     */
    @JsonProperty("production_state")
    public String getProduction_state(){
        return production_state ;
    }

    /**
     * 设置 [PRODUCTION_STATE]
     */
    @JsonProperty("production_state")
    public void setProduction_state(String  production_state){
        this.production_state = production_state ;
        this.production_stateDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_STATE]脏标记
     */
    @JsonIgnore
    public boolean getProduction_stateDirtyFlag(){
        return production_stateDirtyFlag ;
    }

    /**
     * 获取 [NEXT_WORK_ORDER_ID_TEXT]
     */
    @JsonProperty("next_work_order_id_text")
    public String getNext_work_order_id_text(){
        return next_work_order_id_text ;
    }

    /**
     * 设置 [NEXT_WORK_ORDER_ID_TEXT]
     */
    @JsonProperty("next_work_order_id_text")
    public void setNext_work_order_id_text(String  next_work_order_id_text){
        this.next_work_order_id_text = next_work_order_id_text ;
        this.next_work_order_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [NEXT_WORK_ORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getNext_work_order_id_textDirtyFlag(){
        return next_work_order_id_textDirtyFlag ;
    }

    /**
     * 获取 [OPERATION_ID_TEXT]
     */
    @JsonProperty("operation_id_text")
    public String getOperation_id_text(){
        return operation_id_text ;
    }

    /**
     * 设置 [OPERATION_ID_TEXT]
     */
    @JsonProperty("operation_id_text")
    public void setOperation_id_text(String  operation_id_text){
        this.operation_id_text = operation_id_text ;
        this.operation_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OPERATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOperation_id_textDirtyFlag(){
        return operation_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID_TEXT]
     */
    @JsonProperty("production_id_text")
    public String getProduction_id_text(){
        return production_id_text ;
    }

    /**
     * 设置 [PRODUCTION_ID_TEXT]
     */
    @JsonProperty("production_id_text")
    public void setProduction_id_text(String  production_id_text){
        this.production_id_text = production_id_text ;
        this.production_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduction_id_textDirtyFlag(){
        return production_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TRACKING]
     */
    @JsonProperty("product_tracking")
    public String getProduct_tracking(){
        return product_tracking ;
    }

    /**
     * 设置 [PRODUCT_TRACKING]
     */
    @JsonProperty("product_tracking")
    public void setProduct_tracking(String  product_tracking){
        this.product_tracking = product_tracking ;
        this.product_trackingDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TRACKING]脏标记
     */
    @JsonIgnore
    public boolean getProduct_trackingDirtyFlag(){
        return product_trackingDirtyFlag ;
    }

    /**
     * 获取 [FINAL_LOT_ID]
     */
    @JsonProperty("final_lot_id")
    public Integer getFinal_lot_id(){
        return final_lot_id ;
    }

    /**
     * 设置 [FINAL_LOT_ID]
     */
    @JsonProperty("final_lot_id")
    public void setFinal_lot_id(Integer  final_lot_id){
        this.final_lot_id = final_lot_id ;
        this.final_lot_idDirtyFlag = true ;
    }

    /**
     * 获取 [FINAL_LOT_ID]脏标记
     */
    @JsonIgnore
    public boolean getFinal_lot_idDirtyFlag(){
        return final_lot_idDirtyFlag ;
    }

    /**
     * 获取 [WORKCENTER_ID]
     */
    @JsonProperty("workcenter_id")
    public Integer getWorkcenter_id(){
        return workcenter_id ;
    }

    /**
     * 设置 [WORKCENTER_ID]
     */
    @JsonProperty("workcenter_id")
    public void setWorkcenter_id(Integer  workcenter_id){
        this.workcenter_id = workcenter_id ;
        this.workcenter_idDirtyFlag = true ;
    }

    /**
     * 获取 [WORKCENTER_ID]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_idDirtyFlag(){
        return workcenter_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public Integer getProduction_id(){
        return production_id ;
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public void setProduction_id(Integer  production_id){
        this.production_id = production_id ;
        this.production_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduction_idDirtyFlag(){
        return production_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [OPERATION_ID]
     */
    @JsonProperty("operation_id")
    public Integer getOperation_id(){
        return operation_id ;
    }

    /**
     * 设置 [OPERATION_ID]
     */
    @JsonProperty("operation_id")
    public void setOperation_id(Integer  operation_id){
        this.operation_id = operation_id ;
        this.operation_idDirtyFlag = true ;
    }

    /**
     * 获取 [OPERATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getOperation_idDirtyFlag(){
        return operation_idDirtyFlag ;
    }

    /**
     * 获取 [NEXT_WORK_ORDER_ID]
     */
    @JsonProperty("next_work_order_id")
    public Integer getNext_work_order_id(){
        return next_work_order_id ;
    }

    /**
     * 设置 [NEXT_WORK_ORDER_ID]
     */
    @JsonProperty("next_work_order_id")
    public void setNext_work_order_id(Integer  next_work_order_id){
        this.next_work_order_id = next_work_order_id ;
        this.next_work_order_idDirtyFlag = true ;
    }

    /**
     * 获取 [NEXT_WORK_ORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getNext_work_order_idDirtyFlag(){
        return next_work_order_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Mrp_workorder toDO() {
        Mrp_workorder srfdomain = new Mrp_workorder();
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getTime_idsDirtyFlag())
            srfdomain.setTime_ids(time_ids);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getWorking_user_idsDirtyFlag())
            srfdomain.setWorking_user_ids(working_user_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getQty_producedDirtyFlag())
            srfdomain.setQty_produced(qty_produced);
        if(getDurationDirtyFlag())
            srfdomain.setDuration(duration);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getScrap_countDirtyFlag())
            srfdomain.setScrap_count(scrap_count);
        if(getDate_planned_startDirtyFlag())
            srfdomain.setDate_planned_start(date_planned_start);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getIs_user_workingDirtyFlag())
            srfdomain.setIs_user_working(is_user_working);
        if(getMove_raw_idsDirtyFlag())
            srfdomain.setMove_raw_ids(move_raw_ids);
        if(getLast_working_user_idDirtyFlag())
            srfdomain.setLast_working_user_id(last_working_user_id);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMove_line_idsDirtyFlag())
            srfdomain.setMove_line_ids(move_line_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getIs_first_woDirtyFlag())
            srfdomain.setIs_first_wo(is_first_wo);
        if(getIs_producedDirtyFlag())
            srfdomain.setIs_produced(is_produced);
        if(getDate_startDirtyFlag())
            srfdomain.setDate_start(date_start);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getDuration_unitDirtyFlag())
            srfdomain.setDuration_unit(duration_unit);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getCapacityDirtyFlag())
            srfdomain.setCapacity(capacity);
        if(getActive_move_line_idsDirtyFlag())
            srfdomain.setActive_move_line_ids(active_move_line_ids);
        if(getDate_planned_finishedDirtyFlag())
            srfdomain.setDate_planned_finished(date_planned_finished);
        if(getDate_finishedDirtyFlag())
            srfdomain.setDate_finished(date_finished);
        if(getQty_remainingDirtyFlag())
            srfdomain.setQty_remaining(qty_remaining);
        if(getScrap_idsDirtyFlag())
            srfdomain.setScrap_ids(scrap_ids);
        if(getDuration_percentDirtyFlag())
            srfdomain.setDuration_percent(duration_percent);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDuration_expectedDirtyFlag())
            srfdomain.setDuration_expected(duration_expected);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getQty_producingDirtyFlag())
            srfdomain.setQty_producing(qty_producing);
        if(getProduction_dateDirtyFlag())
            srfdomain.setProduction_date(production_date);
        if(getWorksheetDirtyFlag())
            srfdomain.setWorksheet(worksheet);
        if(getWorkcenter_id_textDirtyFlag())
            srfdomain.setWorkcenter_id_text(workcenter_id_text);
        if(getFinal_lot_id_textDirtyFlag())
            srfdomain.setFinal_lot_id_text(final_lot_id_text);
        if(getWorking_stateDirtyFlag())
            srfdomain.setWorking_state(working_state);
        if(getQty_productionDirtyFlag())
            srfdomain.setQty_production(qty_production);
        if(getProduct_uom_idDirtyFlag())
            srfdomain.setProduct_uom_id(product_uom_id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getProduction_availabilityDirtyFlag())
            srfdomain.setProduction_availability(production_availability);
        if(getProduction_stateDirtyFlag())
            srfdomain.setProduction_state(production_state);
        if(getNext_work_order_id_textDirtyFlag())
            srfdomain.setNext_work_order_id_text(next_work_order_id_text);
        if(getOperation_id_textDirtyFlag())
            srfdomain.setOperation_id_text(operation_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getProduction_id_textDirtyFlag())
            srfdomain.setProduction_id_text(production_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getProduct_trackingDirtyFlag())
            srfdomain.setProduct_tracking(product_tracking);
        if(getFinal_lot_idDirtyFlag())
            srfdomain.setFinal_lot_id(final_lot_id);
        if(getWorkcenter_idDirtyFlag())
            srfdomain.setWorkcenter_id(workcenter_id);
        if(getProduction_idDirtyFlag())
            srfdomain.setProduction_id(production_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getOperation_idDirtyFlag())
            srfdomain.setOperation_id(operation_id);
        if(getNext_work_order_idDirtyFlag())
            srfdomain.setNext_work_order_id(next_work_order_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Mrp_workorder srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getTime_idsDirtyFlag())
            this.setTime_ids(srfdomain.getTime_ids());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getWorking_user_idsDirtyFlag())
            this.setWorking_user_ids(srfdomain.getWorking_user_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getQty_producedDirtyFlag())
            this.setQty_produced(srfdomain.getQty_produced());
        if(srfdomain.getDurationDirtyFlag())
            this.setDuration(srfdomain.getDuration());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getScrap_countDirtyFlag())
            this.setScrap_count(srfdomain.getScrap_count());
        if(srfdomain.getDate_planned_startDirtyFlag())
            this.setDate_planned_start(srfdomain.getDate_planned_start());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getIs_user_workingDirtyFlag())
            this.setIs_user_working(srfdomain.getIs_user_working());
        if(srfdomain.getMove_raw_idsDirtyFlag())
            this.setMove_raw_ids(srfdomain.getMove_raw_ids());
        if(srfdomain.getLast_working_user_idDirtyFlag())
            this.setLast_working_user_id(srfdomain.getLast_working_user_id());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMove_line_idsDirtyFlag())
            this.setMove_line_ids(srfdomain.getMove_line_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getIs_first_woDirtyFlag())
            this.setIs_first_wo(srfdomain.getIs_first_wo());
        if(srfdomain.getIs_producedDirtyFlag())
            this.setIs_produced(srfdomain.getIs_produced());
        if(srfdomain.getDate_startDirtyFlag())
            this.setDate_start(srfdomain.getDate_start());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getDuration_unitDirtyFlag())
            this.setDuration_unit(srfdomain.getDuration_unit());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getCapacityDirtyFlag())
            this.setCapacity(srfdomain.getCapacity());
        if(srfdomain.getActive_move_line_idsDirtyFlag())
            this.setActive_move_line_ids(srfdomain.getActive_move_line_ids());
        if(srfdomain.getDate_planned_finishedDirtyFlag())
            this.setDate_planned_finished(srfdomain.getDate_planned_finished());
        if(srfdomain.getDate_finishedDirtyFlag())
            this.setDate_finished(srfdomain.getDate_finished());
        if(srfdomain.getQty_remainingDirtyFlag())
            this.setQty_remaining(srfdomain.getQty_remaining());
        if(srfdomain.getScrap_idsDirtyFlag())
            this.setScrap_ids(srfdomain.getScrap_ids());
        if(srfdomain.getDuration_percentDirtyFlag())
            this.setDuration_percent(srfdomain.getDuration_percent());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDuration_expectedDirtyFlag())
            this.setDuration_expected(srfdomain.getDuration_expected());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getQty_producingDirtyFlag())
            this.setQty_producing(srfdomain.getQty_producing());
        if(srfdomain.getProduction_dateDirtyFlag())
            this.setProduction_date(srfdomain.getProduction_date());
        if(srfdomain.getWorksheetDirtyFlag())
            this.setWorksheet(srfdomain.getWorksheet());
        if(srfdomain.getWorkcenter_id_textDirtyFlag())
            this.setWorkcenter_id_text(srfdomain.getWorkcenter_id_text());
        if(srfdomain.getFinal_lot_id_textDirtyFlag())
            this.setFinal_lot_id_text(srfdomain.getFinal_lot_id_text());
        if(srfdomain.getWorking_stateDirtyFlag())
            this.setWorking_state(srfdomain.getWorking_state());
        if(srfdomain.getQty_productionDirtyFlag())
            this.setQty_production(srfdomain.getQty_production());
        if(srfdomain.getProduct_uom_idDirtyFlag())
            this.setProduct_uom_id(srfdomain.getProduct_uom_id());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getProduction_availabilityDirtyFlag())
            this.setProduction_availability(srfdomain.getProduction_availability());
        if(srfdomain.getProduction_stateDirtyFlag())
            this.setProduction_state(srfdomain.getProduction_state());
        if(srfdomain.getNext_work_order_id_textDirtyFlag())
            this.setNext_work_order_id_text(srfdomain.getNext_work_order_id_text());
        if(srfdomain.getOperation_id_textDirtyFlag())
            this.setOperation_id_text(srfdomain.getOperation_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getProduction_id_textDirtyFlag())
            this.setProduction_id_text(srfdomain.getProduction_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getProduct_trackingDirtyFlag())
            this.setProduct_tracking(srfdomain.getProduct_tracking());
        if(srfdomain.getFinal_lot_idDirtyFlag())
            this.setFinal_lot_id(srfdomain.getFinal_lot_id());
        if(srfdomain.getWorkcenter_idDirtyFlag())
            this.setWorkcenter_id(srfdomain.getWorkcenter_id());
        if(srfdomain.getProduction_idDirtyFlag())
            this.setProduction_id(srfdomain.getProduction_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getOperation_idDirtyFlag())
            this.setOperation_id(srfdomain.getOperation_id());
        if(srfdomain.getNext_work_order_idDirtyFlag())
            this.setNext_work_order_id(srfdomain.getNext_work_order_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Mrp_workorderDTO> fromDOPage(List<Mrp_workorder> poPage)   {
        if(poPage == null)
            return null;
        List<Mrp_workorderDTO> dtos=new ArrayList<Mrp_workorderDTO>();
        for(Mrp_workorder domain : poPage) {
            Mrp_workorderDTO dto = new Mrp_workorderDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

