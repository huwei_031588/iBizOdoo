package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_product_produce_lineDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce_line;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_product_produce_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produce_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_product_produce_line" })
@RestController
@RequestMapping("")
public class Mrp_product_produce_lineResource {

    @Autowired
    private IMrp_product_produce_lineService mrp_product_produce_lineService;

    public IMrp_product_produce_lineService getMrp_product_produce_lineService() {
        return this.mrp_product_produce_lineService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_product_produce_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_product_produce_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_product_produce_lineDTO> mrp_product_produce_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_product_produce_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_product_produce_lines/{mrp_product_produce_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_product_produce_line_id") Integer mrp_product_produce_line_id) {
        Mrp_product_produce_lineDTO mrp_product_produce_linedto = new Mrp_product_produce_lineDTO();
		Mrp_product_produce_line domain = new Mrp_product_produce_line();
		mrp_product_produce_linedto.setId(mrp_product_produce_line_id);
		domain.setId(mrp_product_produce_line_id);
        Boolean rst = mrp_product_produce_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_product_produce_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_product_produce_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_product_produce_lineDTO> mrp_product_produce_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_product_produce_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_product_produce_lines/{mrp_product_produce_line_id}")
    public ResponseEntity<Mrp_product_produce_lineDTO> get(@PathVariable("mrp_product_produce_line_id") Integer mrp_product_produce_line_id) {
        Mrp_product_produce_lineDTO dto = new Mrp_product_produce_lineDTO();
        Mrp_product_produce_line domain = mrp_product_produce_lineService.get(mrp_product_produce_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_product_produce_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_product_produce_lines/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_product_produce_line(@RequestBody List<Mrp_product_produce_lineDTO> mrp_product_produce_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_product_produce_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_product_produce_lines/{mrp_product_produce_line_id}")

    public ResponseEntity<Mrp_product_produce_lineDTO> update(@PathVariable("mrp_product_produce_line_id") Integer mrp_product_produce_line_id, @RequestBody Mrp_product_produce_lineDTO mrp_product_produce_linedto) {
		Mrp_product_produce_line domain = mrp_product_produce_linedto.toDO();
        domain.setId(mrp_product_produce_line_id);
		mrp_product_produce_lineService.update(domain);
		Mrp_product_produce_lineDTO dto = new Mrp_product_produce_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_product_produce_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_product_produce_lines")

    public ResponseEntity<Mrp_product_produce_lineDTO> create(@RequestBody Mrp_product_produce_lineDTO mrp_product_produce_linedto) {
        Mrp_product_produce_lineDTO dto = new Mrp_product_produce_lineDTO();
        Mrp_product_produce_line domain = mrp_product_produce_linedto.toDO();
		mrp_product_produce_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_product_produce_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_product_produce_lines/fetchdefault")
	public ResponseEntity<Page<Mrp_product_produce_lineDTO>> fetchDefault(Mrp_product_produce_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_product_produce_lineDTO> list = new ArrayList<Mrp_product_produce_lineDTO>();
        
        Page<Mrp_product_produce_line> domains = mrp_product_produce_lineService.searchDefault(context) ;
        for(Mrp_product_produce_line mrp_product_produce_line : domains.getContent()){
            Mrp_product_produce_lineDTO dto = new Mrp_product_produce_lineDTO();
            dto.fromDO(mrp_product_produce_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
