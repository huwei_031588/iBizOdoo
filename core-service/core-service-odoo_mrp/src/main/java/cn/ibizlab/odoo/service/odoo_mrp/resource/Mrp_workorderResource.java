package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_workorderDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workorderService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workorderSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_workorder" })
@RestController
@RequestMapping("")
public class Mrp_workorderResource {

    @Autowired
    private IMrp_workorderService mrp_workorderService;

    public IMrp_workorderService getMrp_workorderService() {
        return this.mrp_workorderService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_workorder" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workorders/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_workorder(@RequestBody List<Mrp_workorderDTO> mrp_workorderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_workorder" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workorders/{mrp_workorder_id}")

    public ResponseEntity<Mrp_workorderDTO> update(@PathVariable("mrp_workorder_id") Integer mrp_workorder_id, @RequestBody Mrp_workorderDTO mrp_workorderdto) {
		Mrp_workorder domain = mrp_workorderdto.toDO();
        domain.setId(mrp_workorder_id);
		mrp_workorderService.update(domain);
		Mrp_workorderDTO dto = new Mrp_workorderDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_workorder" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workorders/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workorderDTO> mrp_workorderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_workorder" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workorders/{mrp_workorder_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workorder_id") Integer mrp_workorder_id) {
        Mrp_workorderDTO mrp_workorderdto = new Mrp_workorderDTO();
		Mrp_workorder domain = new Mrp_workorder();
		mrp_workorderdto.setId(mrp_workorder_id);
		domain.setId(mrp_workorder_id);
        Boolean rst = mrp_workorderService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_workorder" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workorders/{mrp_workorder_id}")
    public ResponseEntity<Mrp_workorderDTO> get(@PathVariable("mrp_workorder_id") Integer mrp_workorder_id) {
        Mrp_workorderDTO dto = new Mrp_workorderDTO();
        Mrp_workorder domain = mrp_workorderService.get(mrp_workorder_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_workorder" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workorders")

    public ResponseEntity<Mrp_workorderDTO> create(@RequestBody Mrp_workorderDTO mrp_workorderdto) {
        Mrp_workorderDTO dto = new Mrp_workorderDTO();
        Mrp_workorder domain = mrp_workorderdto.toDO();
		mrp_workorderService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_workorder" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workorders/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_workorderDTO> mrp_workorderdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_workorder" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_workorders/fetchdefault")
	public ResponseEntity<Page<Mrp_workorderDTO>> fetchDefault(Mrp_workorderSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_workorderDTO> list = new ArrayList<Mrp_workorderDTO>();
        
        Page<Mrp_workorder> domains = mrp_workorderService.searchDefault(context) ;
        for(Mrp_workorder mrp_workorder : domains.getContent()){
            Mrp_workorderDTO dto = new Mrp_workorderDTO();
            dto.fromDO(mrp_workorder);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
