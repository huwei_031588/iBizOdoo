package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_bom_lineDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_bom_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_bom_line" })
@RestController
@RequestMapping("")
public class Mrp_bom_lineResource {

    @Autowired
    private IMrp_bom_lineService mrp_bom_lineService;

    public IMrp_bom_lineService getMrp_bom_lineService() {
        return this.mrp_bom_lineService;
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_bom_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_bom_lines/{mrp_bom_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_bom_line_id") Integer mrp_bom_line_id) {
        Mrp_bom_lineDTO mrp_bom_linedto = new Mrp_bom_lineDTO();
		Mrp_bom_line domain = new Mrp_bom_line();
		mrp_bom_linedto.setId(mrp_bom_line_id);
		domain.setId(mrp_bom_line_id);
        Boolean rst = mrp_bom_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_bom_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_bom_lines/{mrp_bom_line_id}")

    public ResponseEntity<Mrp_bom_lineDTO> update(@PathVariable("mrp_bom_line_id") Integer mrp_bom_line_id, @RequestBody Mrp_bom_lineDTO mrp_bom_linedto) {
		Mrp_bom_line domain = mrp_bom_linedto.toDO();
        domain.setId(mrp_bom_line_id);
		mrp_bom_lineService.update(domain);
		Mrp_bom_lineDTO dto = new Mrp_bom_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_bom_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_bom_lines/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_bom_line(@RequestBody List<Mrp_bom_lineDTO> mrp_bom_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_bom_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_bom_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_bom_lineDTO> mrp_bom_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_bom_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_bom_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_bom_lineDTO> mrp_bom_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_bom_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_bom_lines")

    public ResponseEntity<Mrp_bom_lineDTO> create(@RequestBody Mrp_bom_lineDTO mrp_bom_linedto) {
        Mrp_bom_lineDTO dto = new Mrp_bom_lineDTO();
        Mrp_bom_line domain = mrp_bom_linedto.toDO();
		mrp_bom_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_bom_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_bom_lines/{mrp_bom_line_id}")
    public ResponseEntity<Mrp_bom_lineDTO> get(@PathVariable("mrp_bom_line_id") Integer mrp_bom_line_id) {
        Mrp_bom_lineDTO dto = new Mrp_bom_lineDTO();
        Mrp_bom_line domain = mrp_bom_lineService.get(mrp_bom_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_bom_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_bom_lines/fetchdefault")
	public ResponseEntity<Page<Mrp_bom_lineDTO>> fetchDefault(Mrp_bom_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_bom_lineDTO> list = new ArrayList<Mrp_bom_lineDTO>();
        
        Page<Mrp_bom_line> domains = mrp_bom_lineService.searchDefault(context) ;
        for(Mrp_bom_line mrp_bom_line : domains.getContent()){
            Mrp_bom_lineDTO dto = new Mrp_bom_lineDTO();
            dto.fromDO(mrp_bom_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
