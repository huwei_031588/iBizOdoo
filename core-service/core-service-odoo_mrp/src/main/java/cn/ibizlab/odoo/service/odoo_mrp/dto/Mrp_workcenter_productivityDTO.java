package cn.ibizlab.odoo.service.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_workcenter_productivity.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mrp_workcenter_productivityDTO]
 */
public class Mrp_workcenter_productivityDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mrp_workcenter_productivityCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mrp_workcenter_productivityIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Mrp_workcenter_productivityDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mrp_workcenter_productivityDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [DURATION]
     *
     */
    @Mrp_workcenter_productivityDurationDefault(info = "默认规则")
    private Double duration;

    @JsonIgnore
    private boolean durationDirtyFlag;

    /**
     * 属性 [DATE_END]
     *
     */
    @Mrp_workcenter_productivityDate_endDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_end;

    @JsonIgnore
    private boolean date_endDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mrp_workcenter_productivityWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DATE_START]
     *
     */
    @Mrp_workcenter_productivityDate_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_start;

    @JsonIgnore
    private boolean date_startDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mrp_workcenter_productivity__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mrp_workcenter_productivityCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WORKCENTER_ID_TEXT]
     *
     */
    @Mrp_workcenter_productivityWorkcenter_id_textDefault(info = "默认规则")
    private String workcenter_id_text;

    @JsonIgnore
    private boolean workcenter_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Mrp_workcenter_productivityUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [LOSS_TYPE]
     *
     */
    @Mrp_workcenter_productivityLoss_typeDefault(info = "默认规则")
    private String loss_type;

    @JsonIgnore
    private boolean loss_typeDirtyFlag;

    /**
     * 属性 [WORKORDER_ID_TEXT]
     *
     */
    @Mrp_workcenter_productivityWorkorder_id_textDefault(info = "默认规则")
    private String workorder_id_text;

    @JsonIgnore
    private boolean workorder_id_textDirtyFlag;

    /**
     * 属性 [LOSS_ID_TEXT]
     *
     */
    @Mrp_workcenter_productivityLoss_id_textDefault(info = "默认规则")
    private String loss_id_text;

    @JsonIgnore
    private boolean loss_id_textDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @Mrp_workcenter_productivityProduction_idDefault(info = "默认规则")
    private Integer production_id;

    @JsonIgnore
    private boolean production_idDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mrp_workcenter_productivityWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Mrp_workcenter_productivityUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [WORKORDER_ID]
     *
     */
    @Mrp_workcenter_productivityWorkorder_idDefault(info = "默认规则")
    private Integer workorder_id;

    @JsonIgnore
    private boolean workorder_idDirtyFlag;

    /**
     * 属性 [WORKCENTER_ID]
     *
     */
    @Mrp_workcenter_productivityWorkcenter_idDefault(info = "默认规则")
    private Integer workcenter_id;

    @JsonIgnore
    private boolean workcenter_idDirtyFlag;

    /**
     * 属性 [LOSS_ID]
     *
     */
    @Mrp_workcenter_productivityLoss_idDefault(info = "默认规则")
    private Integer loss_id;

    @JsonIgnore
    private boolean loss_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mrp_workcenter_productivityWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mrp_workcenter_productivityCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [DURATION]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return duration ;
    }

    /**
     * 设置 [DURATION]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

    /**
     * 获取 [DURATION]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return durationDirtyFlag ;
    }

    /**
     * 获取 [DATE_END]
     */
    @JsonProperty("date_end")
    public Timestamp getDate_end(){
        return date_end ;
    }

    /**
     * 设置 [DATE_END]
     */
    @JsonProperty("date_end")
    public void setDate_end(Timestamp  date_end){
        this.date_end = date_end ;
        this.date_endDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_END]脏标记
     */
    @JsonIgnore
    public boolean getDate_endDirtyFlag(){
        return date_endDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DATE_START]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return date_start ;
    }

    /**
     * 设置 [DATE_START]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_START]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return date_startDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WORKCENTER_ID_TEXT]
     */
    @JsonProperty("workcenter_id_text")
    public String getWorkcenter_id_text(){
        return workcenter_id_text ;
    }

    /**
     * 设置 [WORKCENTER_ID_TEXT]
     */
    @JsonProperty("workcenter_id_text")
    public void setWorkcenter_id_text(String  workcenter_id_text){
        this.workcenter_id_text = workcenter_id_text ;
        this.workcenter_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WORKCENTER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_id_textDirtyFlag(){
        return workcenter_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOSS_TYPE]
     */
    @JsonProperty("loss_type")
    public String getLoss_type(){
        return loss_type ;
    }

    /**
     * 设置 [LOSS_TYPE]
     */
    @JsonProperty("loss_type")
    public void setLoss_type(String  loss_type){
        this.loss_type = loss_type ;
        this.loss_typeDirtyFlag = true ;
    }

    /**
     * 获取 [LOSS_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getLoss_typeDirtyFlag(){
        return loss_typeDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_ID_TEXT]
     */
    @JsonProperty("workorder_id_text")
    public String getWorkorder_id_text(){
        return workorder_id_text ;
    }

    /**
     * 设置 [WORKORDER_ID_TEXT]
     */
    @JsonProperty("workorder_id_text")
    public void setWorkorder_id_text(String  workorder_id_text){
        this.workorder_id_text = workorder_id_text ;
        this.workorder_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_id_textDirtyFlag(){
        return workorder_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOSS_ID_TEXT]
     */
    @JsonProperty("loss_id_text")
    public String getLoss_id_text(){
        return loss_id_text ;
    }

    /**
     * 设置 [LOSS_ID_TEXT]
     */
    @JsonProperty("loss_id_text")
    public void setLoss_id_text(String  loss_id_text){
        this.loss_id_text = loss_id_text ;
        this.loss_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOSS_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLoss_id_textDirtyFlag(){
        return loss_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public Integer getProduction_id(){
        return production_id ;
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public void setProduction_id(Integer  production_id){
        this.production_id = production_id ;
        this.production_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduction_idDirtyFlag(){
        return production_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_ID]
     */
    @JsonProperty("workorder_id")
    public Integer getWorkorder_id(){
        return workorder_id ;
    }

    /**
     * 设置 [WORKORDER_ID]
     */
    @JsonProperty("workorder_id")
    public void setWorkorder_id(Integer  workorder_id){
        this.workorder_id = workorder_id ;
        this.workorder_idDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idDirtyFlag(){
        return workorder_idDirtyFlag ;
    }

    /**
     * 获取 [WORKCENTER_ID]
     */
    @JsonProperty("workcenter_id")
    public Integer getWorkcenter_id(){
        return workcenter_id ;
    }

    /**
     * 设置 [WORKCENTER_ID]
     */
    @JsonProperty("workcenter_id")
    public void setWorkcenter_id(Integer  workcenter_id){
        this.workcenter_id = workcenter_id ;
        this.workcenter_idDirtyFlag = true ;
    }

    /**
     * 获取 [WORKCENTER_ID]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_idDirtyFlag(){
        return workcenter_idDirtyFlag ;
    }

    /**
     * 获取 [LOSS_ID]
     */
    @JsonProperty("loss_id")
    public Integer getLoss_id(){
        return loss_id ;
    }

    /**
     * 设置 [LOSS_ID]
     */
    @JsonProperty("loss_id")
    public void setLoss_id(Integer  loss_id){
        this.loss_id = loss_id ;
        this.loss_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOSS_ID]脏标记
     */
    @JsonIgnore
    public boolean getLoss_idDirtyFlag(){
        return loss_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Mrp_workcenter_productivity toDO() {
        Mrp_workcenter_productivity srfdomain = new Mrp_workcenter_productivity();
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getDurationDirtyFlag())
            srfdomain.setDuration(duration);
        if(getDate_endDirtyFlag())
            srfdomain.setDate_end(date_end);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDate_startDirtyFlag())
            srfdomain.setDate_start(date_start);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWorkcenter_id_textDirtyFlag())
            srfdomain.setWorkcenter_id_text(workcenter_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getLoss_typeDirtyFlag())
            srfdomain.setLoss_type(loss_type);
        if(getWorkorder_id_textDirtyFlag())
            srfdomain.setWorkorder_id_text(workorder_id_text);
        if(getLoss_id_textDirtyFlag())
            srfdomain.setLoss_id_text(loss_id_text);
        if(getProduction_idDirtyFlag())
            srfdomain.setProduction_id(production_id);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getWorkorder_idDirtyFlag())
            srfdomain.setWorkorder_id(workorder_id);
        if(getWorkcenter_idDirtyFlag())
            srfdomain.setWorkcenter_id(workcenter_id);
        if(getLoss_idDirtyFlag())
            srfdomain.setLoss_id(loss_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Mrp_workcenter_productivity srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getDurationDirtyFlag())
            this.setDuration(srfdomain.getDuration());
        if(srfdomain.getDate_endDirtyFlag())
            this.setDate_end(srfdomain.getDate_end());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDate_startDirtyFlag())
            this.setDate_start(srfdomain.getDate_start());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWorkcenter_id_textDirtyFlag())
            this.setWorkcenter_id_text(srfdomain.getWorkcenter_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getLoss_typeDirtyFlag())
            this.setLoss_type(srfdomain.getLoss_type());
        if(srfdomain.getWorkorder_id_textDirtyFlag())
            this.setWorkorder_id_text(srfdomain.getWorkorder_id_text());
        if(srfdomain.getLoss_id_textDirtyFlag())
            this.setLoss_id_text(srfdomain.getLoss_id_text());
        if(srfdomain.getProduction_idDirtyFlag())
            this.setProduction_id(srfdomain.getProduction_id());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getWorkorder_idDirtyFlag())
            this.setWorkorder_id(srfdomain.getWorkorder_id());
        if(srfdomain.getWorkcenter_idDirtyFlag())
            this.setWorkcenter_id(srfdomain.getWorkcenter_id());
        if(srfdomain.getLoss_idDirtyFlag())
            this.setLoss_id(srfdomain.getLoss_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Mrp_workcenter_productivityDTO> fromDOPage(List<Mrp_workcenter_productivity> poPage)   {
        if(poPage == null)
            return null;
        List<Mrp_workcenter_productivityDTO> dtos=new ArrayList<Mrp_workcenter_productivityDTO>();
        for(Mrp_workcenter_productivity domain : poPage) {
            Mrp_workcenter_productivityDTO dto = new Mrp_workcenter_productivityDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

