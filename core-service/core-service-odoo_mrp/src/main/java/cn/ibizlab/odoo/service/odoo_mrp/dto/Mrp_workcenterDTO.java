package cn.ibizlab.odoo.service.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_workcenter.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mrp_workcenterDTO]
 */
public class Mrp_workcenterDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [COSTS_HOUR]
     *
     */
    @Mrp_workcenterCosts_hourDefault(info = "默认规则")
    private Double costs_hour;

    @JsonIgnore
    private boolean costs_hourDirtyFlag;

    /**
     * 属性 [WORKCENTER_LOAD]
     *
     */
    @Mrp_workcenterWorkcenter_loadDefault(info = "默认规则")
    private Double workcenter_load;

    @JsonIgnore
    private boolean workcenter_loadDirtyFlag;

    /**
     * 属性 [WORKING_STATE]
     *
     */
    @Mrp_workcenterWorking_stateDefault(info = "默认规则")
    private String working_state;

    @JsonIgnore
    private boolean working_stateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mrp_workcenter__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mrp_workcenterWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [WORKORDER_LATE_COUNT]
     *
     */
    @Mrp_workcenterWorkorder_late_countDefault(info = "默认规则")
    private Integer workorder_late_count;

    @JsonIgnore
    private boolean workorder_late_countDirtyFlag;

    /**
     * 属性 [PERFORMANCE]
     *
     */
    @Mrp_workcenterPerformanceDefault(info = "默认规则")
    private Integer performance;

    @JsonIgnore
    private boolean performanceDirtyFlag;

    /**
     * 属性 [OEE_TARGET]
     *
     */
    @Mrp_workcenterOee_targetDefault(info = "默认规则")
    private Double oee_target;

    @JsonIgnore
    private boolean oee_targetDirtyFlag;

    /**
     * 属性 [CAPACITY]
     *
     */
    @Mrp_workcenterCapacityDefault(info = "默认规则")
    private Double capacity;

    @JsonIgnore
    private boolean capacityDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mrp_workcenterIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WORKORDER_COUNT]
     *
     */
    @Mrp_workcenterWorkorder_countDefault(info = "默认规则")
    private Integer workorder_count;

    @JsonIgnore
    private boolean workorder_countDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mrp_workcenterDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [TIME_IDS]
     *
     */
    @Mrp_workcenterTime_idsDefault(info = "默认规则")
    private String time_ids;

    @JsonIgnore
    private boolean time_idsDirtyFlag;

    /**
     * 属性 [PRODUCTIVE_TIME]
     *
     */
    @Mrp_workcenterProductive_timeDefault(info = "默认规则")
    private Double productive_time;

    @JsonIgnore
    private boolean productive_timeDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Mrp_workcenterSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [ROUTING_LINE_IDS]
     *
     */
    @Mrp_workcenterRouting_line_idsDefault(info = "默认规则")
    private String routing_line_ids;

    @JsonIgnore
    private boolean routing_line_idsDirtyFlag;

    /**
     * 属性 [OEE]
     *
     */
    @Mrp_workcenterOeeDefault(info = "默认规则")
    private Double oee;

    @JsonIgnore
    private boolean oeeDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Mrp_workcenterNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [CODE]
     *
     */
    @Mrp_workcenterCodeDefault(info = "默认规则")
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Mrp_workcenterColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [TIME_STOP]
     *
     */
    @Mrp_workcenterTime_stopDefault(info = "默认规则")
    private Double time_stop;

    @JsonIgnore
    private boolean time_stopDirtyFlag;

    /**
     * 属性 [TIME_START]
     *
     */
    @Mrp_workcenterTime_startDefault(info = "默认规则")
    private Double time_start;

    @JsonIgnore
    private boolean time_startDirtyFlag;

    /**
     * 属性 [WORKORDER_PENDING_COUNT]
     *
     */
    @Mrp_workcenterWorkorder_pending_countDefault(info = "默认规则")
    private Integer workorder_pending_count;

    @JsonIgnore
    private boolean workorder_pending_countDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mrp_workcenterCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [BLOCKED_TIME]
     *
     */
    @Mrp_workcenterBlocked_timeDefault(info = "默认规则")
    private Double blocked_time;

    @JsonIgnore
    private boolean blocked_timeDirtyFlag;

    /**
     * 属性 [WORKORDER_READY_COUNT]
     *
     */
    @Mrp_workcenterWorkorder_ready_countDefault(info = "默认规则")
    private Integer workorder_ready_count;

    @JsonIgnore
    private boolean workorder_ready_countDirtyFlag;

    /**
     * 属性 [WORKORDER_PROGRESS_COUNT]
     *
     */
    @Mrp_workcenterWorkorder_progress_countDefault(info = "默认规则")
    private Integer workorder_progress_count;

    @JsonIgnore
    private boolean workorder_progress_countDirtyFlag;

    /**
     * 属性 [ORDER_IDS]
     *
     */
    @Mrp_workcenterOrder_idsDefault(info = "默认规则")
    private String order_ids;

    @JsonIgnore
    private boolean order_idsDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mrp_workcenterCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Mrp_workcenterCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [TZ]
     *
     */
    @Mrp_workcenterTzDefault(info = "默认规则")
    private String tz;

    @JsonIgnore
    private boolean tzDirtyFlag;

    /**
     * 属性 [TIME_EFFICIENCY]
     *
     */
    @Mrp_workcenterTime_efficiencyDefault(info = "默认规则")
    private Double time_efficiency;

    @JsonIgnore
    private boolean time_efficiencyDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Mrp_workcenterActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mrp_workcenterWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID_TEXT]
     *
     */
    @Mrp_workcenterResource_calendar_id_textDefault(info = "默认规则")
    private String resource_calendar_id_text;

    @JsonIgnore
    private boolean resource_calendar_id_textDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mrp_workcenterNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @Mrp_workcenterResource_calendar_idDefault(info = "默认规则")
    private Integer resource_calendar_id;

    @JsonIgnore
    private boolean resource_calendar_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mrp_workcenterCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mrp_workcenterWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Mrp_workcenterCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [RESOURCE_ID]
     *
     */
    @Mrp_workcenterResource_idDefault(info = "默认规则")
    private Integer resource_id;

    @JsonIgnore
    private boolean resource_idDirtyFlag;


    /**
     * 获取 [COSTS_HOUR]
     */
    @JsonProperty("costs_hour")
    public Double getCosts_hour(){
        return costs_hour ;
    }

    /**
     * 设置 [COSTS_HOUR]
     */
    @JsonProperty("costs_hour")
    public void setCosts_hour(Double  costs_hour){
        this.costs_hour = costs_hour ;
        this.costs_hourDirtyFlag = true ;
    }

    /**
     * 获取 [COSTS_HOUR]脏标记
     */
    @JsonIgnore
    public boolean getCosts_hourDirtyFlag(){
        return costs_hourDirtyFlag ;
    }

    /**
     * 获取 [WORKCENTER_LOAD]
     */
    @JsonProperty("workcenter_load")
    public Double getWorkcenter_load(){
        return workcenter_load ;
    }

    /**
     * 设置 [WORKCENTER_LOAD]
     */
    @JsonProperty("workcenter_load")
    public void setWorkcenter_load(Double  workcenter_load){
        this.workcenter_load = workcenter_load ;
        this.workcenter_loadDirtyFlag = true ;
    }

    /**
     * 获取 [WORKCENTER_LOAD]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_loadDirtyFlag(){
        return workcenter_loadDirtyFlag ;
    }

    /**
     * 获取 [WORKING_STATE]
     */
    @JsonProperty("working_state")
    public String getWorking_state(){
        return working_state ;
    }

    /**
     * 设置 [WORKING_STATE]
     */
    @JsonProperty("working_state")
    public void setWorking_state(String  working_state){
        this.working_state = working_state ;
        this.working_stateDirtyFlag = true ;
    }

    /**
     * 获取 [WORKING_STATE]脏标记
     */
    @JsonIgnore
    public boolean getWorking_stateDirtyFlag(){
        return working_stateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_LATE_COUNT]
     */
    @JsonProperty("workorder_late_count")
    public Integer getWorkorder_late_count(){
        return workorder_late_count ;
    }

    /**
     * 设置 [WORKORDER_LATE_COUNT]
     */
    @JsonProperty("workorder_late_count")
    public void setWorkorder_late_count(Integer  workorder_late_count){
        this.workorder_late_count = workorder_late_count ;
        this.workorder_late_countDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_LATE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_late_countDirtyFlag(){
        return workorder_late_countDirtyFlag ;
    }

    /**
     * 获取 [PERFORMANCE]
     */
    @JsonProperty("performance")
    public Integer getPerformance(){
        return performance ;
    }

    /**
     * 设置 [PERFORMANCE]
     */
    @JsonProperty("performance")
    public void setPerformance(Integer  performance){
        this.performance = performance ;
        this.performanceDirtyFlag = true ;
    }

    /**
     * 获取 [PERFORMANCE]脏标记
     */
    @JsonIgnore
    public boolean getPerformanceDirtyFlag(){
        return performanceDirtyFlag ;
    }

    /**
     * 获取 [OEE_TARGET]
     */
    @JsonProperty("oee_target")
    public Double getOee_target(){
        return oee_target ;
    }

    /**
     * 设置 [OEE_TARGET]
     */
    @JsonProperty("oee_target")
    public void setOee_target(Double  oee_target){
        this.oee_target = oee_target ;
        this.oee_targetDirtyFlag = true ;
    }

    /**
     * 获取 [OEE_TARGET]脏标记
     */
    @JsonIgnore
    public boolean getOee_targetDirtyFlag(){
        return oee_targetDirtyFlag ;
    }

    /**
     * 获取 [CAPACITY]
     */
    @JsonProperty("capacity")
    public Double getCapacity(){
        return capacity ;
    }

    /**
     * 设置 [CAPACITY]
     */
    @JsonProperty("capacity")
    public void setCapacity(Double  capacity){
        this.capacity = capacity ;
        this.capacityDirtyFlag = true ;
    }

    /**
     * 获取 [CAPACITY]脏标记
     */
    @JsonIgnore
    public boolean getCapacityDirtyFlag(){
        return capacityDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_COUNT]
     */
    @JsonProperty("workorder_count")
    public Integer getWorkorder_count(){
        return workorder_count ;
    }

    /**
     * 设置 [WORKORDER_COUNT]
     */
    @JsonProperty("workorder_count")
    public void setWorkorder_count(Integer  workorder_count){
        this.workorder_count = workorder_count ;
        this.workorder_countDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_countDirtyFlag(){
        return workorder_countDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [TIME_IDS]
     */
    @JsonProperty("time_ids")
    public String getTime_ids(){
        return time_ids ;
    }

    /**
     * 设置 [TIME_IDS]
     */
    @JsonProperty("time_ids")
    public void setTime_ids(String  time_ids){
        this.time_ids = time_ids ;
        this.time_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTime_idsDirtyFlag(){
        return time_idsDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTIVE_TIME]
     */
    @JsonProperty("productive_time")
    public Double getProductive_time(){
        return productive_time ;
    }

    /**
     * 设置 [PRODUCTIVE_TIME]
     */
    @JsonProperty("productive_time")
    public void setProductive_time(Double  productive_time){
        this.productive_time = productive_time ;
        this.productive_timeDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTIVE_TIME]脏标记
     */
    @JsonIgnore
    public boolean getProductive_timeDirtyFlag(){
        return productive_timeDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [ROUTING_LINE_IDS]
     */
    @JsonProperty("routing_line_ids")
    public String getRouting_line_ids(){
        return routing_line_ids ;
    }

    /**
     * 设置 [ROUTING_LINE_IDS]
     */
    @JsonProperty("routing_line_ids")
    public void setRouting_line_ids(String  routing_line_ids){
        this.routing_line_ids = routing_line_ids ;
        this.routing_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTING_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRouting_line_idsDirtyFlag(){
        return routing_line_idsDirtyFlag ;
    }

    /**
     * 获取 [OEE]
     */
    @JsonProperty("oee")
    public Double getOee(){
        return oee ;
    }

    /**
     * 设置 [OEE]
     */
    @JsonProperty("oee")
    public void setOee(Double  oee){
        this.oee = oee ;
        this.oeeDirtyFlag = true ;
    }

    /**
     * 获取 [OEE]脏标记
     */
    @JsonIgnore
    public boolean getOeeDirtyFlag(){
        return oeeDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [CODE]
     */
    @JsonProperty("code")
    public String getCode(){
        return code ;
    }

    /**
     * 设置 [CODE]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [CODE]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return codeDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [TIME_STOP]
     */
    @JsonProperty("time_stop")
    public Double getTime_stop(){
        return time_stop ;
    }

    /**
     * 设置 [TIME_STOP]
     */
    @JsonProperty("time_stop")
    public void setTime_stop(Double  time_stop){
        this.time_stop = time_stop ;
        this.time_stopDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_STOP]脏标记
     */
    @JsonIgnore
    public boolean getTime_stopDirtyFlag(){
        return time_stopDirtyFlag ;
    }

    /**
     * 获取 [TIME_START]
     */
    @JsonProperty("time_start")
    public Double getTime_start(){
        return time_start ;
    }

    /**
     * 设置 [TIME_START]
     */
    @JsonProperty("time_start")
    public void setTime_start(Double  time_start){
        this.time_start = time_start ;
        this.time_startDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_START]脏标记
     */
    @JsonIgnore
    public boolean getTime_startDirtyFlag(){
        return time_startDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_PENDING_COUNT]
     */
    @JsonProperty("workorder_pending_count")
    public Integer getWorkorder_pending_count(){
        return workorder_pending_count ;
    }

    /**
     * 设置 [WORKORDER_PENDING_COUNT]
     */
    @JsonProperty("workorder_pending_count")
    public void setWorkorder_pending_count(Integer  workorder_pending_count){
        this.workorder_pending_count = workorder_pending_count ;
        this.workorder_pending_countDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_PENDING_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_pending_countDirtyFlag(){
        return workorder_pending_countDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [BLOCKED_TIME]
     */
    @JsonProperty("blocked_time")
    public Double getBlocked_time(){
        return blocked_time ;
    }

    /**
     * 设置 [BLOCKED_TIME]
     */
    @JsonProperty("blocked_time")
    public void setBlocked_time(Double  blocked_time){
        this.blocked_time = blocked_time ;
        this.blocked_timeDirtyFlag = true ;
    }

    /**
     * 获取 [BLOCKED_TIME]脏标记
     */
    @JsonIgnore
    public boolean getBlocked_timeDirtyFlag(){
        return blocked_timeDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_READY_COUNT]
     */
    @JsonProperty("workorder_ready_count")
    public Integer getWorkorder_ready_count(){
        return workorder_ready_count ;
    }

    /**
     * 设置 [WORKORDER_READY_COUNT]
     */
    @JsonProperty("workorder_ready_count")
    public void setWorkorder_ready_count(Integer  workorder_ready_count){
        this.workorder_ready_count = workorder_ready_count ;
        this.workorder_ready_countDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_READY_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_ready_countDirtyFlag(){
        return workorder_ready_countDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_PROGRESS_COUNT]
     */
    @JsonProperty("workorder_progress_count")
    public Integer getWorkorder_progress_count(){
        return workorder_progress_count ;
    }

    /**
     * 设置 [WORKORDER_PROGRESS_COUNT]
     */
    @JsonProperty("workorder_progress_count")
    public void setWorkorder_progress_count(Integer  workorder_progress_count){
        this.workorder_progress_count = workorder_progress_count ;
        this.workorder_progress_countDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_PROGRESS_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_progress_countDirtyFlag(){
        return workorder_progress_countDirtyFlag ;
    }

    /**
     * 获取 [ORDER_IDS]
     */
    @JsonProperty("order_ids")
    public String getOrder_ids(){
        return order_ids ;
    }

    /**
     * 设置 [ORDER_IDS]
     */
    @JsonProperty("order_ids")
    public void setOrder_ids(String  order_ids){
        this.order_ids = order_ids ;
        this.order_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idsDirtyFlag(){
        return order_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [TZ]
     */
    @JsonProperty("tz")
    public String getTz(){
        return tz ;
    }

    /**
     * 设置 [TZ]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

    /**
     * 获取 [TZ]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return tzDirtyFlag ;
    }

    /**
     * 获取 [TIME_EFFICIENCY]
     */
    @JsonProperty("time_efficiency")
    public Double getTime_efficiency(){
        return time_efficiency ;
    }

    /**
     * 设置 [TIME_EFFICIENCY]
     */
    @JsonProperty("time_efficiency")
    public void setTime_efficiency(Double  time_efficiency){
        this.time_efficiency = time_efficiency ;
        this.time_efficiencyDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_EFFICIENCY]脏标记
     */
    @JsonIgnore
    public boolean getTime_efficiencyDirtyFlag(){
        return time_efficiencyDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("resource_calendar_id_text")
    public String getResource_calendar_id_text(){
        return resource_calendar_id_text ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("resource_calendar_id_text")
    public void setResource_calendar_id_text(String  resource_calendar_id_text){
        this.resource_calendar_id_text = resource_calendar_id_text ;
        this.resource_calendar_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_id_textDirtyFlag(){
        return resource_calendar_id_textDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return resource_calendar_id ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return resource_calendar_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_ID]
     */
    @JsonProperty("resource_id")
    public Integer getResource_id(){
        return resource_id ;
    }

    /**
     * 设置 [RESOURCE_ID]
     */
    @JsonProperty("resource_id")
    public void setResource_id(Integer  resource_id){
        this.resource_id = resource_id ;
        this.resource_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_idDirtyFlag(){
        return resource_idDirtyFlag ;
    }



    public Mrp_workcenter toDO() {
        Mrp_workcenter srfdomain = new Mrp_workcenter();
        if(getCosts_hourDirtyFlag())
            srfdomain.setCosts_hour(costs_hour);
        if(getWorkcenter_loadDirtyFlag())
            srfdomain.setWorkcenter_load(workcenter_load);
        if(getWorking_stateDirtyFlag())
            srfdomain.setWorking_state(working_state);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getWorkorder_late_countDirtyFlag())
            srfdomain.setWorkorder_late_count(workorder_late_count);
        if(getPerformanceDirtyFlag())
            srfdomain.setPerformance(performance);
        if(getOee_targetDirtyFlag())
            srfdomain.setOee_target(oee_target);
        if(getCapacityDirtyFlag())
            srfdomain.setCapacity(capacity);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWorkorder_countDirtyFlag())
            srfdomain.setWorkorder_count(workorder_count);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getTime_idsDirtyFlag())
            srfdomain.setTime_ids(time_ids);
        if(getProductive_timeDirtyFlag())
            srfdomain.setProductive_time(productive_time);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getRouting_line_idsDirtyFlag())
            srfdomain.setRouting_line_ids(routing_line_ids);
        if(getOeeDirtyFlag())
            srfdomain.setOee(oee);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getCodeDirtyFlag())
            srfdomain.setCode(code);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getTime_stopDirtyFlag())
            srfdomain.setTime_stop(time_stop);
        if(getTime_startDirtyFlag())
            srfdomain.setTime_start(time_start);
        if(getWorkorder_pending_countDirtyFlag())
            srfdomain.setWorkorder_pending_count(workorder_pending_count);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getBlocked_timeDirtyFlag())
            srfdomain.setBlocked_time(blocked_time);
        if(getWorkorder_ready_countDirtyFlag())
            srfdomain.setWorkorder_ready_count(workorder_ready_count);
        if(getWorkorder_progress_countDirtyFlag())
            srfdomain.setWorkorder_progress_count(workorder_progress_count);
        if(getOrder_idsDirtyFlag())
            srfdomain.setOrder_ids(order_ids);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getTzDirtyFlag())
            srfdomain.setTz(tz);
        if(getTime_efficiencyDirtyFlag())
            srfdomain.setTime_efficiency(time_efficiency);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getResource_calendar_id_textDirtyFlag())
            srfdomain.setResource_calendar_id_text(resource_calendar_id_text);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getResource_calendar_idDirtyFlag())
            srfdomain.setResource_calendar_id(resource_calendar_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getResource_idDirtyFlag())
            srfdomain.setResource_id(resource_id);

        return srfdomain;
    }

    public void fromDO(Mrp_workcenter srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCosts_hourDirtyFlag())
            this.setCosts_hour(srfdomain.getCosts_hour());
        if(srfdomain.getWorkcenter_loadDirtyFlag())
            this.setWorkcenter_load(srfdomain.getWorkcenter_load());
        if(srfdomain.getWorking_stateDirtyFlag())
            this.setWorking_state(srfdomain.getWorking_state());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getWorkorder_late_countDirtyFlag())
            this.setWorkorder_late_count(srfdomain.getWorkorder_late_count());
        if(srfdomain.getPerformanceDirtyFlag())
            this.setPerformance(srfdomain.getPerformance());
        if(srfdomain.getOee_targetDirtyFlag())
            this.setOee_target(srfdomain.getOee_target());
        if(srfdomain.getCapacityDirtyFlag())
            this.setCapacity(srfdomain.getCapacity());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWorkorder_countDirtyFlag())
            this.setWorkorder_count(srfdomain.getWorkorder_count());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getTime_idsDirtyFlag())
            this.setTime_ids(srfdomain.getTime_ids());
        if(srfdomain.getProductive_timeDirtyFlag())
            this.setProductive_time(srfdomain.getProductive_time());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getRouting_line_idsDirtyFlag())
            this.setRouting_line_ids(srfdomain.getRouting_line_ids());
        if(srfdomain.getOeeDirtyFlag())
            this.setOee(srfdomain.getOee());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getCodeDirtyFlag())
            this.setCode(srfdomain.getCode());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getTime_stopDirtyFlag())
            this.setTime_stop(srfdomain.getTime_stop());
        if(srfdomain.getTime_startDirtyFlag())
            this.setTime_start(srfdomain.getTime_start());
        if(srfdomain.getWorkorder_pending_countDirtyFlag())
            this.setWorkorder_pending_count(srfdomain.getWorkorder_pending_count());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getBlocked_timeDirtyFlag())
            this.setBlocked_time(srfdomain.getBlocked_time());
        if(srfdomain.getWorkorder_ready_countDirtyFlag())
            this.setWorkorder_ready_count(srfdomain.getWorkorder_ready_count());
        if(srfdomain.getWorkorder_progress_countDirtyFlag())
            this.setWorkorder_progress_count(srfdomain.getWorkorder_progress_count());
        if(srfdomain.getOrder_idsDirtyFlag())
            this.setOrder_ids(srfdomain.getOrder_ids());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getTzDirtyFlag())
            this.setTz(srfdomain.getTz());
        if(srfdomain.getTime_efficiencyDirtyFlag())
            this.setTime_efficiency(srfdomain.getTime_efficiency());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getResource_calendar_id_textDirtyFlag())
            this.setResource_calendar_id_text(srfdomain.getResource_calendar_id_text());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getResource_calendar_idDirtyFlag())
            this.setResource_calendar_id(srfdomain.getResource_calendar_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getResource_idDirtyFlag())
            this.setResource_id(srfdomain.getResource_id());

    }

    public List<Mrp_workcenterDTO> fromDOPage(List<Mrp_workcenter> poPage)   {
        if(poPage == null)
            return null;
        List<Mrp_workcenterDTO> dtos=new ArrayList<Mrp_workcenterDTO>();
        for(Mrp_workcenter domain : poPage) {
            Mrp_workcenterDTO dto = new Mrp_workcenterDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

