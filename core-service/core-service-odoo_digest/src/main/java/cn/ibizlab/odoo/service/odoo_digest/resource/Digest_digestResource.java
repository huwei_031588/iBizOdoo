package cn.ibizlab.odoo.service.odoo_digest.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_digest.dto.Digest_digestDTO;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest;
import cn.ibizlab.odoo.core.odoo_digest.service.IDigest_digestService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_digestSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Digest_digest" })
@RestController
@RequestMapping("")
public class Digest_digestResource {

    @Autowired
    private IDigest_digestService digest_digestService;

    public IDigest_digestService getDigest_digestService() {
        return this.digest_digestService;
    }

    @ApiOperation(value = "建立数据", tags = {"Digest_digest" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_digest/digest_digests")

    public ResponseEntity<Digest_digestDTO> create(@RequestBody Digest_digestDTO digest_digestdto) {
        Digest_digestDTO dto = new Digest_digestDTO();
        Digest_digest domain = digest_digestdto.toDO();
		digest_digestService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Digest_digest" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_digest/digest_digests/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Digest_digestDTO> digest_digestdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Digest_digest" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_digest/digest_digests/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Digest_digestDTO> digest_digestdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Digest_digest" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_digest/digest_digests/{digest_digest_id}")
    public ResponseEntity<Digest_digestDTO> get(@PathVariable("digest_digest_id") Integer digest_digest_id) {
        Digest_digestDTO dto = new Digest_digestDTO();
        Digest_digest domain = digest_digestService.get(digest_digest_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Digest_digest" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_digest/digest_digests/{digest_digest_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("digest_digest_id") Integer digest_digest_id) {
        Digest_digestDTO digest_digestdto = new Digest_digestDTO();
		Digest_digest domain = new Digest_digest();
		digest_digestdto.setId(digest_digest_id);
		domain.setId(digest_digest_id);
        Boolean rst = digest_digestService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Digest_digest" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_digest/digest_digests/{digest_digest_id}")

    public ResponseEntity<Digest_digestDTO> update(@PathVariable("digest_digest_id") Integer digest_digest_id, @RequestBody Digest_digestDTO digest_digestdto) {
		Digest_digest domain = digest_digestdto.toDO();
        domain.setId(digest_digest_id);
		digest_digestService.update(domain);
		Digest_digestDTO dto = new Digest_digestDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Digest_digest" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_digest/digest_digests/createBatch")
    public ResponseEntity<Boolean> createBatchDigest_digest(@RequestBody List<Digest_digestDTO> digest_digestdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Digest_digest" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_digest/digest_digests/fetchdefault")
	public ResponseEntity<Page<Digest_digestDTO>> fetchDefault(Digest_digestSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Digest_digestDTO> list = new ArrayList<Digest_digestDTO>();
        
        Page<Digest_digest> domains = digest_digestService.searchDefault(context) ;
        for(Digest_digest digest_digest : domains.getContent()){
            Digest_digestDTO dto = new Digest_digestDTO();
            dto.fromDO(digest_digest);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
