package cn.ibizlab.odoo.service.odoo_digest.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_digest")
public class odoo_digestRestConfiguration {

}
