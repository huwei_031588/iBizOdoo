package cn.ibizlab.odoo.service.odoo_digest.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.digest")
@Data
public class odoo_digestServiceProperties {

	private boolean enabled;

	private boolean auth;


}