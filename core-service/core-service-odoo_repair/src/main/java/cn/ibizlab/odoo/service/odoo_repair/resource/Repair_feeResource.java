package cn.ibizlab.odoo.service.odoo_repair.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_repair.dto.Repair_feeDTO;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_feeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_feeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Repair_fee" })
@RestController
@RequestMapping("")
public class Repair_feeResource {

    @Autowired
    private IRepair_feeService repair_feeService;

    public IRepair_feeService getRepair_feeService() {
        return this.repair_feeService;
    }

    @ApiOperation(value = "建立数据", tags = {"Repair_fee" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_fees")

    public ResponseEntity<Repair_feeDTO> create(@RequestBody Repair_feeDTO repair_feedto) {
        Repair_feeDTO dto = new Repair_feeDTO();
        Repair_fee domain = repair_feedto.toDO();
		repair_feeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Repair_fee" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_fees/createBatch")
    public ResponseEntity<Boolean> createBatchRepair_fee(@RequestBody List<Repair_feeDTO> repair_feedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Repair_fee" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_fees/{repair_fee_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("repair_fee_id") Integer repair_fee_id) {
        Repair_feeDTO repair_feedto = new Repair_feeDTO();
		Repair_fee domain = new Repair_fee();
		repair_feedto.setId(repair_fee_id);
		domain.setId(repair_fee_id);
        Boolean rst = repair_feeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Repair_fee" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_fees/{repair_fee_id}")
    public ResponseEntity<Repair_feeDTO> get(@PathVariable("repair_fee_id") Integer repair_fee_id) {
        Repair_feeDTO dto = new Repair_feeDTO();
        Repair_fee domain = repair_feeService.get(repair_fee_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Repair_fee" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_fees/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Repair_feeDTO> repair_feedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Repair_fee" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_fees/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_feeDTO> repair_feedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Repair_fee" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_fees/{repair_fee_id}")

    public ResponseEntity<Repair_feeDTO> update(@PathVariable("repair_fee_id") Integer repair_fee_id, @RequestBody Repair_feeDTO repair_feedto) {
		Repair_fee domain = repair_feedto.toDO();
        domain.setId(repair_fee_id);
		repair_feeService.update(domain);
		Repair_feeDTO dto = new Repair_feeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Repair_fee" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_repair/repair_fees/fetchdefault")
	public ResponseEntity<Page<Repair_feeDTO>> fetchDefault(Repair_feeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Repair_feeDTO> list = new ArrayList<Repair_feeDTO>();
        
        Page<Repair_fee> domains = repair_feeService.searchDefault(context) ;
        for(Repair_fee repair_fee : domains.getContent()){
            Repair_feeDTO dto = new Repair_feeDTO();
            dto.fromDO(repair_fee);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
