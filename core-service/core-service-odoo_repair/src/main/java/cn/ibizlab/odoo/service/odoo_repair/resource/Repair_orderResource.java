package cn.ibizlab.odoo.service.odoo_repair.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_repair.dto.Repair_orderDTO;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_orderService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_orderSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Repair_order" })
@RestController
@RequestMapping("")
public class Repair_orderResource {

    @Autowired
    private IRepair_orderService repair_orderService;

    public IRepair_orderService getRepair_orderService() {
        return this.repair_orderService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Repair_order" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_orders/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Repair_orderDTO> repair_orderdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Repair_order" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_orders/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_orderDTO> repair_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Repair_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_orders/{repair_order_id}")
    public ResponseEntity<Repair_orderDTO> get(@PathVariable("repair_order_id") Integer repair_order_id) {
        Repair_orderDTO dto = new Repair_orderDTO();
        Repair_order domain = repair_orderService.get(repair_order_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Repair_order" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_orders/createBatch")
    public ResponseEntity<Boolean> createBatchRepair_order(@RequestBody List<Repair_orderDTO> repair_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Repair_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_orders/{repair_order_id}")

    public ResponseEntity<Repair_orderDTO> update(@PathVariable("repair_order_id") Integer repair_order_id, @RequestBody Repair_orderDTO repair_orderdto) {
		Repair_order domain = repair_orderdto.toDO();
        domain.setId(repair_order_id);
		repair_orderService.update(domain);
		Repair_orderDTO dto = new Repair_orderDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Repair_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_orders/{repair_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("repair_order_id") Integer repair_order_id) {
        Repair_orderDTO repair_orderdto = new Repair_orderDTO();
		Repair_order domain = new Repair_order();
		repair_orderdto.setId(repair_order_id);
		domain.setId(repair_order_id);
        Boolean rst = repair_orderService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Repair_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_orders")

    public ResponseEntity<Repair_orderDTO> create(@RequestBody Repair_orderDTO repair_orderdto) {
        Repair_orderDTO dto = new Repair_orderDTO();
        Repair_order domain = repair_orderdto.toDO();
		repair_orderService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Repair_order" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_repair/repair_orders/fetchdefault")
	public ResponseEntity<Page<Repair_orderDTO>> fetchDefault(Repair_orderSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Repair_orderDTO> list = new ArrayList<Repair_orderDTO>();
        
        Page<Repair_order> domains = repair_orderService.searchDefault(context) ;
        for(Repair_order repair_order : domains.getContent()){
            Repair_orderDTO dto = new Repair_orderDTO();
            dto.fromDO(repair_order);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
