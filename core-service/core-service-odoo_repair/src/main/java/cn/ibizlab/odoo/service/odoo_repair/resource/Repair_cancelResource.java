package cn.ibizlab.odoo.service.odoo_repair.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_repair.dto.Repair_cancelDTO;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_cancelService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_cancelSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Repair_cancel" })
@RestController
@RequestMapping("")
public class Repair_cancelResource {

    @Autowired
    private IRepair_cancelService repair_cancelService;

    public IRepair_cancelService getRepair_cancelService() {
        return this.repair_cancelService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Repair_cancel" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_cancels/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Repair_cancelDTO> repair_canceldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Repair_cancel" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_cancels/createBatch")
    public ResponseEntity<Boolean> createBatchRepair_cancel(@RequestBody List<Repair_cancelDTO> repair_canceldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Repair_cancel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_cancels/{repair_cancel_id}")

    public ResponseEntity<Repair_cancelDTO> update(@PathVariable("repair_cancel_id") Integer repair_cancel_id, @RequestBody Repair_cancelDTO repair_canceldto) {
		Repair_cancel domain = repair_canceldto.toDO();
        domain.setId(repair_cancel_id);
		repair_cancelService.update(domain);
		Repair_cancelDTO dto = new Repair_cancelDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Repair_cancel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_cancels")

    public ResponseEntity<Repair_cancelDTO> create(@RequestBody Repair_cancelDTO repair_canceldto) {
        Repair_cancelDTO dto = new Repair_cancelDTO();
        Repair_cancel domain = repair_canceldto.toDO();
		repair_cancelService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Repair_cancel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_cancels/{repair_cancel_id}")
    public ResponseEntity<Repair_cancelDTO> get(@PathVariable("repair_cancel_id") Integer repair_cancel_id) {
        Repair_cancelDTO dto = new Repair_cancelDTO();
        Repair_cancel domain = repair_cancelService.get(repair_cancel_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Repair_cancel" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_cancels/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_cancelDTO> repair_canceldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Repair_cancel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_cancels/{repair_cancel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("repair_cancel_id") Integer repair_cancel_id) {
        Repair_cancelDTO repair_canceldto = new Repair_cancelDTO();
		Repair_cancel domain = new Repair_cancel();
		repair_canceldto.setId(repair_cancel_id);
		domain.setId(repair_cancel_id);
        Boolean rst = repair_cancelService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Repair_cancel" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_repair/repair_cancels/fetchdefault")
	public ResponseEntity<Page<Repair_cancelDTO>> fetchDefault(Repair_cancelSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Repair_cancelDTO> list = new ArrayList<Repair_cancelDTO>();
        
        Page<Repair_cancel> domains = repair_cancelService.searchDefault(context) ;
        for(Repair_cancel repair_cancel : domains.getContent()){
            Repair_cancelDTO dto = new Repair_cancelDTO();
            dto.fromDO(repair_cancel);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
