package cn.ibizlab.odoo.service.odoo_repair.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_repair.valuerule.anno.repair_order.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Repair_orderDTO]
 */
public class Repair_orderDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Repair_orderWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [INTERNAL_NOTES]
     *
     */
    @Repair_orderInternal_notesDefault(info = "默认规则")
    private String internal_notes;

    @JsonIgnore
    private boolean internal_notesDirtyFlag;

    /**
     * 属性 [DEFAULT_ADDRESS_ID]
     *
     */
    @Repair_orderDefault_address_idDefault(info = "默认规则")
    private Integer default_address_id;

    @JsonIgnore
    private boolean default_address_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Repair_orderActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Repair_orderMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Repair_orderMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Repair_orderMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [INVOICE_METHOD]
     *
     */
    @Repair_orderInvoice_methodDefault(info = "默认规则")
    private String invoice_method;

    @JsonIgnore
    private boolean invoice_methodDirtyFlag;

    /**
     * 属性 [INVOICED]
     *
     */
    @Repair_orderInvoicedDefault(info = "默认规则")
    private String invoiced;

    @JsonIgnore
    private boolean invoicedDirtyFlag;

    /**
     * 属性 [QUOTATION_NOTES]
     *
     */
    @Repair_orderQuotation_notesDefault(info = "默认规则")
    private String quotation_notes;

    @JsonIgnore
    private boolean quotation_notesDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Repair_orderNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Repair_orderIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Repair_orderMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [AMOUNT_TOTAL]
     *
     */
    @Repair_orderAmount_totalDefault(info = "默认规则")
    private Double amount_total;

    @JsonIgnore
    private boolean amount_totalDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Repair_orderActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Repair_orderMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Repair_orderMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Repair_orderStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Repair_orderWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Repair_orderActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Repair_orderMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Repair_orderActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [OPERATIONS]
     *
     */
    @Repair_orderOperationsDefault(info = "默认规则")
    private String operations;

    @JsonIgnore
    private boolean operationsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Repair_orderMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Repair_orderDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Repair_orderMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [FEES_LINES]
     *
     */
    @Repair_orderFees_linesDefault(info = "默认规则")
    private String fees_lines;

    @JsonIgnore
    private boolean fees_linesDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Repair_orderMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Repair_orderActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [REPAIRED]
     *
     */
    @Repair_orderRepairedDefault(info = "默认规则")
    private String repaired;

    @JsonIgnore
    private boolean repairedDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Repair_orderMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [AMOUNT_UNTAXED]
     *
     */
    @Repair_orderAmount_untaxedDefault(info = "默认规则")
    private Double amount_untaxed;

    @JsonIgnore
    private boolean amount_untaxedDirtyFlag;

    /**
     * 属性 [GUARANTEE_LIMIT]
     *
     */
    @Repair_orderGuarantee_limitDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp guarantee_limit;

    @JsonIgnore
    private boolean guarantee_limitDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Repair_orderCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Repair_order__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [AMOUNT_TAX]
     *
     */
    @Repair_orderAmount_taxDefault(info = "默认规则")
    private Double amount_tax;

    @JsonIgnore
    private boolean amount_taxDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Repair_orderActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Repair_orderMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Repair_orderMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @Repair_orderProduct_qtyDefault(info = "默认规则")
    private Double product_qty;

    @JsonIgnore
    private boolean product_qtyDirtyFlag;

    /**
     * 属性 [PRICELIST_ID_TEXT]
     *
     */
    @Repair_orderPricelist_id_textDefault(info = "默认规则")
    private String pricelist_id_text;

    @JsonIgnore
    private boolean pricelist_id_textDirtyFlag;

    /**
     * 属性 [LOT_ID_TEXT]
     *
     */
    @Repair_orderLot_id_textDefault(info = "默认规则")
    private String lot_id_text;

    @JsonIgnore
    private boolean lot_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Repair_orderPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Repair_orderProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [INVOICE_ID_TEXT]
     *
     */
    @Repair_orderInvoice_id_textDefault(info = "默认规则")
    private String invoice_id_text;

    @JsonIgnore
    private boolean invoice_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @Repair_orderProduct_uom_textDefault(info = "默认规则")
    private String product_uom_text;

    @JsonIgnore
    private boolean product_uom_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Repair_orderCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Repair_orderCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PARTNER_INVOICE_ID_TEXT]
     *
     */
    @Repair_orderPartner_invoice_id_textDefault(info = "默认规则")
    private String partner_invoice_id_text;

    @JsonIgnore
    private boolean partner_invoice_id_textDirtyFlag;

    /**
     * 属性 [ADDRESS_ID_TEXT]
     *
     */
    @Repair_orderAddress_id_textDefault(info = "默认规则")
    private String address_id_text;

    @JsonIgnore
    private boolean address_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Repair_orderLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [TRACKING]
     *
     */
    @Repair_orderTrackingDefault(info = "默认规则")
    private String tracking;

    @JsonIgnore
    private boolean trackingDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Repair_orderWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MOVE_ID_TEXT]
     *
     */
    @Repair_orderMove_id_textDefault(info = "默认规则")
    private String move_id_text;

    @JsonIgnore
    private boolean move_id_textDirtyFlag;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @Repair_orderMove_idDefault(info = "默认规则")
    private Integer move_id;

    @JsonIgnore
    private boolean move_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Repair_orderCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Repair_orderWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Repair_orderPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [PRICELIST_ID]
     *
     */
    @Repair_orderPricelist_idDefault(info = "默认规则")
    private Integer pricelist_id;

    @JsonIgnore
    private boolean pricelist_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Repair_orderProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [PARTNER_INVOICE_ID]
     *
     */
    @Repair_orderPartner_invoice_idDefault(info = "默认规则")
    private Integer partner_invoice_id;

    @JsonIgnore
    private boolean partner_invoice_idDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Repair_orderLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @Repair_orderProduct_uomDefault(info = "默认规则")
    private Integer product_uom;

    @JsonIgnore
    private boolean product_uomDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Repair_orderCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [ADDRESS_ID]
     *
     */
    @Repair_orderAddress_idDefault(info = "默认规则")
    private Integer address_id;

    @JsonIgnore
    private boolean address_idDirtyFlag;

    /**
     * 属性 [INVOICE_ID]
     *
     */
    @Repair_orderInvoice_idDefault(info = "默认规则")
    private Integer invoice_id;

    @JsonIgnore
    private boolean invoice_idDirtyFlag;

    /**
     * 属性 [LOT_ID]
     *
     */
    @Repair_orderLot_idDefault(info = "默认规则")
    private Integer lot_id;

    @JsonIgnore
    private boolean lot_idDirtyFlag;


    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [INTERNAL_NOTES]
     */
    @JsonProperty("internal_notes")
    public String getInternal_notes(){
        return internal_notes ;
    }

    /**
     * 设置 [INTERNAL_NOTES]
     */
    @JsonProperty("internal_notes")
    public void setInternal_notes(String  internal_notes){
        this.internal_notes = internal_notes ;
        this.internal_notesDirtyFlag = true ;
    }

    /**
     * 获取 [INTERNAL_NOTES]脏标记
     */
    @JsonIgnore
    public boolean getInternal_notesDirtyFlag(){
        return internal_notesDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_ADDRESS_ID]
     */
    @JsonProperty("default_address_id")
    public Integer getDefault_address_id(){
        return default_address_id ;
    }

    /**
     * 设置 [DEFAULT_ADDRESS_ID]
     */
    @JsonProperty("default_address_id")
    public void setDefault_address_id(Integer  default_address_id){
        this.default_address_id = default_address_id ;
        this.default_address_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_ADDRESS_ID]脏标记
     */
    @JsonIgnore
    public boolean getDefault_address_idDirtyFlag(){
        return default_address_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_METHOD]
     */
    @JsonProperty("invoice_method")
    public String getInvoice_method(){
        return invoice_method ;
    }

    /**
     * 设置 [INVOICE_METHOD]
     */
    @JsonProperty("invoice_method")
    public void setInvoice_method(String  invoice_method){
        this.invoice_method = invoice_method ;
        this.invoice_methodDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_methodDirtyFlag(){
        return invoice_methodDirtyFlag ;
    }

    /**
     * 获取 [INVOICED]
     */
    @JsonProperty("invoiced")
    public String getInvoiced(){
        return invoiced ;
    }

    /**
     * 设置 [INVOICED]
     */
    @JsonProperty("invoiced")
    public void setInvoiced(String  invoiced){
        this.invoiced = invoiced ;
        this.invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getInvoicedDirtyFlag(){
        return invoicedDirtyFlag ;
    }

    /**
     * 获取 [QUOTATION_NOTES]
     */
    @JsonProperty("quotation_notes")
    public String getQuotation_notes(){
        return quotation_notes ;
    }

    /**
     * 设置 [QUOTATION_NOTES]
     */
    @JsonProperty("quotation_notes")
    public void setQuotation_notes(String  quotation_notes){
        this.quotation_notes = quotation_notes ;
        this.quotation_notesDirtyFlag = true ;
    }

    /**
     * 获取 [QUOTATION_NOTES]脏标记
     */
    @JsonIgnore
    public boolean getQuotation_notesDirtyFlag(){
        return quotation_notesDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TOTAL]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return amount_total ;
    }

    /**
     * 设置 [AMOUNT_TOTAL]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return amount_totalDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [OPERATIONS]
     */
    @JsonProperty("operations")
    public String getOperations(){
        return operations ;
    }

    /**
     * 设置 [OPERATIONS]
     */
    @JsonProperty("operations")
    public void setOperations(String  operations){
        this.operations = operations ;
        this.operationsDirtyFlag = true ;
    }

    /**
     * 获取 [OPERATIONS]脏标记
     */
    @JsonIgnore
    public boolean getOperationsDirtyFlag(){
        return operationsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [FEES_LINES]
     */
    @JsonProperty("fees_lines")
    public String getFees_lines(){
        return fees_lines ;
    }

    /**
     * 设置 [FEES_LINES]
     */
    @JsonProperty("fees_lines")
    public void setFees_lines(String  fees_lines){
        this.fees_lines = fees_lines ;
        this.fees_linesDirtyFlag = true ;
    }

    /**
     * 获取 [FEES_LINES]脏标记
     */
    @JsonIgnore
    public boolean getFees_linesDirtyFlag(){
        return fees_linesDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [REPAIRED]
     */
    @JsonProperty("repaired")
    public String getRepaired(){
        return repaired ;
    }

    /**
     * 设置 [REPAIRED]
     */
    @JsonProperty("repaired")
    public void setRepaired(String  repaired){
        this.repaired = repaired ;
        this.repairedDirtyFlag = true ;
    }

    /**
     * 获取 [REPAIRED]脏标记
     */
    @JsonIgnore
    public boolean getRepairedDirtyFlag(){
        return repairedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_UNTAXED]
     */
    @JsonProperty("amount_untaxed")
    public Double getAmount_untaxed(){
        return amount_untaxed ;
    }

    /**
     * 设置 [AMOUNT_UNTAXED]
     */
    @JsonProperty("amount_untaxed")
    public void setAmount_untaxed(Double  amount_untaxed){
        this.amount_untaxed = amount_untaxed ;
        this.amount_untaxedDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_UNTAXED]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxedDirtyFlag(){
        return amount_untaxedDirtyFlag ;
    }

    /**
     * 获取 [GUARANTEE_LIMIT]
     */
    @JsonProperty("guarantee_limit")
    public Timestamp getGuarantee_limit(){
        return guarantee_limit ;
    }

    /**
     * 设置 [GUARANTEE_LIMIT]
     */
    @JsonProperty("guarantee_limit")
    public void setGuarantee_limit(Timestamp  guarantee_limit){
        this.guarantee_limit = guarantee_limit ;
        this.guarantee_limitDirtyFlag = true ;
    }

    /**
     * 获取 [GUARANTEE_LIMIT]脏标记
     */
    @JsonIgnore
    public boolean getGuarantee_limitDirtyFlag(){
        return guarantee_limitDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TAX]
     */
    @JsonProperty("amount_tax")
    public Double getAmount_tax(){
        return amount_tax ;
    }

    /**
     * 设置 [AMOUNT_TAX]
     */
    @JsonProperty("amount_tax")
    public void setAmount_tax(Double  amount_tax){
        this.amount_tax = amount_tax ;
        this.amount_taxDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TAX]脏标记
     */
    @JsonIgnore
    public boolean getAmount_taxDirtyFlag(){
        return amount_taxDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return product_qty ;
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return product_qtyDirtyFlag ;
    }

    /**
     * 获取 [PRICELIST_ID_TEXT]
     */
    @JsonProperty("pricelist_id_text")
    public String getPricelist_id_text(){
        return pricelist_id_text ;
    }

    /**
     * 设置 [PRICELIST_ID_TEXT]
     */
    @JsonProperty("pricelist_id_text")
    public void setPricelist_id_text(String  pricelist_id_text){
        this.pricelist_id_text = pricelist_id_text ;
        this.pricelist_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRICELIST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_id_textDirtyFlag(){
        return pricelist_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOT_ID_TEXT]
     */
    @JsonProperty("lot_id_text")
    public String getLot_id_text(){
        return lot_id_text ;
    }

    /**
     * 设置 [LOT_ID_TEXT]
     */
    @JsonProperty("lot_id_text")
    public void setLot_id_text(String  lot_id_text){
        this.lot_id_text = lot_id_text ;
        this.lot_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLot_id_textDirtyFlag(){
        return lot_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_ID_TEXT]
     */
    @JsonProperty("invoice_id_text")
    public String getInvoice_id_text(){
        return invoice_id_text ;
    }

    /**
     * 设置 [INVOICE_ID_TEXT]
     */
    @JsonProperty("invoice_id_text")
    public void setInvoice_id_text(String  invoice_id_text){
        this.invoice_id_text = invoice_id_text ;
        this.invoice_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_id_textDirtyFlag(){
        return invoice_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return product_uom_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return product_uom_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_INVOICE_ID_TEXT]
     */
    @JsonProperty("partner_invoice_id_text")
    public String getPartner_invoice_id_text(){
        return partner_invoice_id_text ;
    }

    /**
     * 设置 [PARTNER_INVOICE_ID_TEXT]
     */
    @JsonProperty("partner_invoice_id_text")
    public void setPartner_invoice_id_text(String  partner_invoice_id_text){
        this.partner_invoice_id_text = partner_invoice_id_text ;
        this.partner_invoice_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_INVOICE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_invoice_id_textDirtyFlag(){
        return partner_invoice_id_textDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_ID_TEXT]
     */
    @JsonProperty("address_id_text")
    public String getAddress_id_text(){
        return address_id_text ;
    }

    /**
     * 设置 [ADDRESS_ID_TEXT]
     */
    @JsonProperty("address_id_text")
    public void setAddress_id_text(String  address_id_text){
        this.address_id_text = address_id_text ;
        this.address_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAddress_id_textDirtyFlag(){
        return address_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [TRACKING]
     */
    @JsonProperty("tracking")
    public String getTracking(){
        return tracking ;
    }

    /**
     * 设置 [TRACKING]
     */
    @JsonProperty("tracking")
    public void setTracking(String  tracking){
        this.tracking = tracking ;
        this.trackingDirtyFlag = true ;
    }

    /**
     * 获取 [TRACKING]脏标记
     */
    @JsonIgnore
    public boolean getTrackingDirtyFlag(){
        return trackingDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID_TEXT]
     */
    @JsonProperty("move_id_text")
    public String getMove_id_text(){
        return move_id_text ;
    }

    /**
     * 设置 [MOVE_ID_TEXT]
     */
    @JsonProperty("move_id_text")
    public void setMove_id_text(String  move_id_text){
        this.move_id_text = move_id_text ;
        this.move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMove_id_textDirtyFlag(){
        return move_id_textDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return move_id ;
    }

    /**
     * 设置 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return move_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [PRICELIST_ID]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return pricelist_id ;
    }

    /**
     * 设置 [PRICELIST_ID]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRICELIST_ID]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return pricelist_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_INVOICE_ID]
     */
    @JsonProperty("partner_invoice_id")
    public Integer getPartner_invoice_id(){
        return partner_invoice_id ;
    }

    /**
     * 设置 [PARTNER_INVOICE_ID]
     */
    @JsonProperty("partner_invoice_id")
    public void setPartner_invoice_id(Integer  partner_invoice_id){
        this.partner_invoice_id = partner_invoice_id ;
        this.partner_invoice_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_INVOICE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_invoice_idDirtyFlag(){
        return partner_invoice_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return product_uom ;
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return product_uomDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_ID]
     */
    @JsonProperty("address_id")
    public Integer getAddress_id(){
        return address_id ;
    }

    /**
     * 设置 [ADDRESS_ID]
     */
    @JsonProperty("address_id")
    public void setAddress_id(Integer  address_id){
        this.address_id = address_id ;
        this.address_idDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_ID]脏标记
     */
    @JsonIgnore
    public boolean getAddress_idDirtyFlag(){
        return address_idDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_ID]
     */
    @JsonProperty("invoice_id")
    public Integer getInvoice_id(){
        return invoice_id ;
    }

    /**
     * 设置 [INVOICE_ID]
     */
    @JsonProperty("invoice_id")
    public void setInvoice_id(Integer  invoice_id){
        this.invoice_id = invoice_id ;
        this.invoice_idDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_ID]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idDirtyFlag(){
        return invoice_idDirtyFlag ;
    }

    /**
     * 获取 [LOT_ID]
     */
    @JsonProperty("lot_id")
    public Integer getLot_id(){
        return lot_id ;
    }

    /**
     * 设置 [LOT_ID]
     */
    @JsonProperty("lot_id")
    public void setLot_id(Integer  lot_id){
        this.lot_id = lot_id ;
        this.lot_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_ID]脏标记
     */
    @JsonIgnore
    public boolean getLot_idDirtyFlag(){
        return lot_idDirtyFlag ;
    }



    public Repair_order toDO() {
        Repair_order srfdomain = new Repair_order();
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getInternal_notesDirtyFlag())
            srfdomain.setInternal_notes(internal_notes);
        if(getDefault_address_idDirtyFlag())
            srfdomain.setDefault_address_id(default_address_id);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getInvoice_methodDirtyFlag())
            srfdomain.setInvoice_method(invoice_method);
        if(getInvoicedDirtyFlag())
            srfdomain.setInvoiced(invoiced);
        if(getQuotation_notesDirtyFlag())
            srfdomain.setQuotation_notes(quotation_notes);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getAmount_totalDirtyFlag())
            srfdomain.setAmount_total(amount_total);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getOperationsDirtyFlag())
            srfdomain.setOperations(operations);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getFees_linesDirtyFlag())
            srfdomain.setFees_lines(fees_lines);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getRepairedDirtyFlag())
            srfdomain.setRepaired(repaired);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getAmount_untaxedDirtyFlag())
            srfdomain.setAmount_untaxed(amount_untaxed);
        if(getGuarantee_limitDirtyFlag())
            srfdomain.setGuarantee_limit(guarantee_limit);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getAmount_taxDirtyFlag())
            srfdomain.setAmount_tax(amount_tax);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getProduct_qtyDirtyFlag())
            srfdomain.setProduct_qty(product_qty);
        if(getPricelist_id_textDirtyFlag())
            srfdomain.setPricelist_id_text(pricelist_id_text);
        if(getLot_id_textDirtyFlag())
            srfdomain.setLot_id_text(lot_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getInvoice_id_textDirtyFlag())
            srfdomain.setInvoice_id_text(invoice_id_text);
        if(getProduct_uom_textDirtyFlag())
            srfdomain.setProduct_uom_text(product_uom_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getPartner_invoice_id_textDirtyFlag())
            srfdomain.setPartner_invoice_id_text(partner_invoice_id_text);
        if(getAddress_id_textDirtyFlag())
            srfdomain.setAddress_id_text(address_id_text);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getTrackingDirtyFlag())
            srfdomain.setTracking(tracking);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getMove_id_textDirtyFlag())
            srfdomain.setMove_id_text(move_id_text);
        if(getMove_idDirtyFlag())
            srfdomain.setMove_id(move_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getPricelist_idDirtyFlag())
            srfdomain.setPricelist_id(pricelist_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getPartner_invoice_idDirtyFlag())
            srfdomain.setPartner_invoice_id(partner_invoice_id);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getProduct_uomDirtyFlag())
            srfdomain.setProduct_uom(product_uom);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getAddress_idDirtyFlag())
            srfdomain.setAddress_id(address_id);
        if(getInvoice_idDirtyFlag())
            srfdomain.setInvoice_id(invoice_id);
        if(getLot_idDirtyFlag())
            srfdomain.setLot_id(lot_id);

        return srfdomain;
    }

    public void fromDO(Repair_order srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getInternal_notesDirtyFlag())
            this.setInternal_notes(srfdomain.getInternal_notes());
        if(srfdomain.getDefault_address_idDirtyFlag())
            this.setDefault_address_id(srfdomain.getDefault_address_id());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getInvoice_methodDirtyFlag())
            this.setInvoice_method(srfdomain.getInvoice_method());
        if(srfdomain.getInvoicedDirtyFlag())
            this.setInvoiced(srfdomain.getInvoiced());
        if(srfdomain.getQuotation_notesDirtyFlag())
            this.setQuotation_notes(srfdomain.getQuotation_notes());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getAmount_totalDirtyFlag())
            this.setAmount_total(srfdomain.getAmount_total());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getOperationsDirtyFlag())
            this.setOperations(srfdomain.getOperations());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getFees_linesDirtyFlag())
            this.setFees_lines(srfdomain.getFees_lines());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getRepairedDirtyFlag())
            this.setRepaired(srfdomain.getRepaired());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getAmount_untaxedDirtyFlag())
            this.setAmount_untaxed(srfdomain.getAmount_untaxed());
        if(srfdomain.getGuarantee_limitDirtyFlag())
            this.setGuarantee_limit(srfdomain.getGuarantee_limit());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getAmount_taxDirtyFlag())
            this.setAmount_tax(srfdomain.getAmount_tax());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getProduct_qtyDirtyFlag())
            this.setProduct_qty(srfdomain.getProduct_qty());
        if(srfdomain.getPricelist_id_textDirtyFlag())
            this.setPricelist_id_text(srfdomain.getPricelist_id_text());
        if(srfdomain.getLot_id_textDirtyFlag())
            this.setLot_id_text(srfdomain.getLot_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getInvoice_id_textDirtyFlag())
            this.setInvoice_id_text(srfdomain.getInvoice_id_text());
        if(srfdomain.getProduct_uom_textDirtyFlag())
            this.setProduct_uom_text(srfdomain.getProduct_uom_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getPartner_invoice_id_textDirtyFlag())
            this.setPartner_invoice_id_text(srfdomain.getPartner_invoice_id_text());
        if(srfdomain.getAddress_id_textDirtyFlag())
            this.setAddress_id_text(srfdomain.getAddress_id_text());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getTrackingDirtyFlag())
            this.setTracking(srfdomain.getTracking());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getMove_id_textDirtyFlag())
            this.setMove_id_text(srfdomain.getMove_id_text());
        if(srfdomain.getMove_idDirtyFlag())
            this.setMove_id(srfdomain.getMove_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getPricelist_idDirtyFlag())
            this.setPricelist_id(srfdomain.getPricelist_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getPartner_invoice_idDirtyFlag())
            this.setPartner_invoice_id(srfdomain.getPartner_invoice_id());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getProduct_uomDirtyFlag())
            this.setProduct_uom(srfdomain.getProduct_uom());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getAddress_idDirtyFlag())
            this.setAddress_id(srfdomain.getAddress_id());
        if(srfdomain.getInvoice_idDirtyFlag())
            this.setInvoice_id(srfdomain.getInvoice_id());
        if(srfdomain.getLot_idDirtyFlag())
            this.setLot_id(srfdomain.getLot_id());

    }

    public List<Repair_orderDTO> fromDOPage(List<Repair_order> poPage)   {
        if(poPage == null)
            return null;
        List<Repair_orderDTO> dtos=new ArrayList<Repair_orderDTO>();
        for(Repair_order domain : poPage) {
            Repair_orderDTO dto = new Repair_orderDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

