package cn.ibizlab.odoo.service.odoo_repair.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_repair.dto.Repair_lineDTO;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Repair_line" })
@RestController
@RequestMapping("")
public class Repair_lineResource {

    @Autowired
    private IRepair_lineService repair_lineService;

    public IRepair_lineService getRepair_lineService() {
        return this.repair_lineService;
    }

    @ApiOperation(value = "获取数据", tags = {"Repair_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_lines/{repair_line_id}")
    public ResponseEntity<Repair_lineDTO> get(@PathVariable("repair_line_id") Integer repair_line_id) {
        Repair_lineDTO dto = new Repair_lineDTO();
        Repair_line domain = repair_lineService.get(repair_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Repair_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Repair_lineDTO> repair_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Repair_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_lines/{repair_line_id}")

    public ResponseEntity<Repair_lineDTO> update(@PathVariable("repair_line_id") Integer repair_line_id, @RequestBody Repair_lineDTO repair_linedto) {
		Repair_line domain = repair_linedto.toDO();
        domain.setId(repair_line_id);
		repair_lineService.update(domain);
		Repair_lineDTO dto = new Repair_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Repair_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_lines/createBatch")
    public ResponseEntity<Boolean> createBatchRepair_line(@RequestBody List<Repair_lineDTO> repair_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Repair_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_lines/{repair_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("repair_line_id") Integer repair_line_id) {
        Repair_lineDTO repair_linedto = new Repair_lineDTO();
		Repair_line domain = new Repair_line();
		repair_linedto.setId(repair_line_id);
		domain.setId(repair_line_id);
        Boolean rst = repair_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Repair_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_lineDTO> repair_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Repair_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_lines")

    public ResponseEntity<Repair_lineDTO> create(@RequestBody Repair_lineDTO repair_linedto) {
        Repair_lineDTO dto = new Repair_lineDTO();
        Repair_line domain = repair_linedto.toDO();
		repair_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Repair_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_repair/repair_lines/fetchdefault")
	public ResponseEntity<Page<Repair_lineDTO>> fetchDefault(Repair_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Repair_lineDTO> list = new ArrayList<Repair_lineDTO>();
        
        Page<Repair_line> domains = repair_lineService.searchDefault(context) ;
        for(Repair_line repair_line : domains.getContent()){
            Repair_lineDTO dto = new Repair_lineDTO();
            dto.fromDO(repair_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
