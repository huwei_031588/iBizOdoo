package cn.ibizlab.odoo.service.odoo_portal.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_portal.dto.Portal_wizardDTO;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_wizardService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizardSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Portal_wizard" })
@RestController
@RequestMapping("")
public class Portal_wizardResource {

    @Autowired
    private IPortal_wizardService portal_wizardService;

    public IPortal_wizardService getPortal_wizardService() {
        return this.portal_wizardService;
    }

    @ApiOperation(value = "更新数据", tags = {"Portal_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_wizards/{portal_wizard_id}")

    public ResponseEntity<Portal_wizardDTO> update(@PathVariable("portal_wizard_id") Integer portal_wizard_id, @RequestBody Portal_wizardDTO portal_wizarddto) {
		Portal_wizard domain = portal_wizarddto.toDO();
        domain.setId(portal_wizard_id);
		portal_wizardService.update(domain);
		Portal_wizardDTO dto = new Portal_wizardDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Portal_wizard" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_wizards/createBatch")
    public ResponseEntity<Boolean> createBatchPortal_wizard(@RequestBody List<Portal_wizardDTO> portal_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Portal_wizard" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_wizards/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_wizardDTO> portal_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Portal_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_wizards/{portal_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("portal_wizard_id") Integer portal_wizard_id) {
        Portal_wizardDTO portal_wizarddto = new Portal_wizardDTO();
		Portal_wizard domain = new Portal_wizard();
		portal_wizarddto.setId(portal_wizard_id);
		domain.setId(portal_wizard_id);
        Boolean rst = portal_wizardService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Portal_wizard" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_wizards/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Portal_wizardDTO> portal_wizarddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Portal_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_wizards")

    public ResponseEntity<Portal_wizardDTO> create(@RequestBody Portal_wizardDTO portal_wizarddto) {
        Portal_wizardDTO dto = new Portal_wizardDTO();
        Portal_wizard domain = portal_wizarddto.toDO();
		portal_wizardService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Portal_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_wizards/{portal_wizard_id}")
    public ResponseEntity<Portal_wizardDTO> get(@PathVariable("portal_wizard_id") Integer portal_wizard_id) {
        Portal_wizardDTO dto = new Portal_wizardDTO();
        Portal_wizard domain = portal_wizardService.get(portal_wizard_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Portal_wizard" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_portal/portal_wizards/fetchdefault")
	public ResponseEntity<Page<Portal_wizardDTO>> fetchDefault(Portal_wizardSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Portal_wizardDTO> list = new ArrayList<Portal_wizardDTO>();
        
        Page<Portal_wizard> domains = portal_wizardService.searchDefault(context) ;
        for(Portal_wizard portal_wizard : domains.getContent()){
            Portal_wizardDTO dto = new Portal_wizardDTO();
            dto.fromDO(portal_wizard);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
