package cn.ibizlab.odoo.service.odoo_portal.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_portal")
public class odoo_portalRestConfiguration {

}
