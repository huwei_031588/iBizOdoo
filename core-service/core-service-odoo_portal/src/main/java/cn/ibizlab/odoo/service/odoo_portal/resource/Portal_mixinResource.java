package cn.ibizlab.odoo.service.odoo_portal.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_portal.dto.Portal_mixinDTO;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_mixinService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_mixinSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Portal_mixin" })
@RestController
@RequestMapping("")
public class Portal_mixinResource {

    @Autowired
    private IPortal_mixinService portal_mixinService;

    public IPortal_mixinService getPortal_mixinService() {
        return this.portal_mixinService;
    }

    @ApiOperation(value = "获取数据", tags = {"Portal_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_mixins/{portal_mixin_id}")
    public ResponseEntity<Portal_mixinDTO> get(@PathVariable("portal_mixin_id") Integer portal_mixin_id) {
        Portal_mixinDTO dto = new Portal_mixinDTO();
        Portal_mixin domain = portal_mixinService.get(portal_mixin_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Portal_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_mixins/{portal_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("portal_mixin_id") Integer portal_mixin_id) {
        Portal_mixinDTO portal_mixindto = new Portal_mixinDTO();
		Portal_mixin domain = new Portal_mixin();
		portal_mixindto.setId(portal_mixin_id);
		domain.setId(portal_mixin_id);
        Boolean rst = portal_mixinService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Portal_mixin" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_mixins/createBatch")
    public ResponseEntity<Boolean> createBatchPortal_mixin(@RequestBody List<Portal_mixinDTO> portal_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Portal_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_mixins/{portal_mixin_id}")

    public ResponseEntity<Portal_mixinDTO> update(@PathVariable("portal_mixin_id") Integer portal_mixin_id, @RequestBody Portal_mixinDTO portal_mixindto) {
		Portal_mixin domain = portal_mixindto.toDO();
        domain.setId(portal_mixin_id);
		portal_mixinService.update(domain);
		Portal_mixinDTO dto = new Portal_mixinDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Portal_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_mixins")

    public ResponseEntity<Portal_mixinDTO> create(@RequestBody Portal_mixinDTO portal_mixindto) {
        Portal_mixinDTO dto = new Portal_mixinDTO();
        Portal_mixin domain = portal_mixindto.toDO();
		portal_mixinService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Portal_mixin" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_mixins/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_mixinDTO> portal_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Portal_mixin" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_mixins/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Portal_mixinDTO> portal_mixindtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Portal_mixin" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_portal/portal_mixins/fetchdefault")
	public ResponseEntity<Page<Portal_mixinDTO>> fetchDefault(Portal_mixinSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Portal_mixinDTO> list = new ArrayList<Portal_mixinDTO>();
        
        Page<Portal_mixin> domains = portal_mixinService.searchDefault(context) ;
        for(Portal_mixin portal_mixin : domains.getContent()){
            Portal_mixinDTO dto = new Portal_mixinDTO();
            dto.fromDO(portal_mixin);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
