package cn.ibizlab.odoo.service.odoo_portal.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_portal.dto.Portal_shareDTO;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_share;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_shareService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_shareSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Portal_share" })
@RestController
@RequestMapping("")
public class Portal_shareResource {

    @Autowired
    private IPortal_shareService portal_shareService;

    public IPortal_shareService getPortal_shareService() {
        return this.portal_shareService;
    }

    @ApiOperation(value = "更新数据", tags = {"Portal_share" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_shares/{portal_share_id}")

    public ResponseEntity<Portal_shareDTO> update(@PathVariable("portal_share_id") Integer portal_share_id, @RequestBody Portal_shareDTO portal_sharedto) {
		Portal_share domain = portal_sharedto.toDO();
        domain.setId(portal_share_id);
		portal_shareService.update(domain);
		Portal_shareDTO dto = new Portal_shareDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Portal_share" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_shares/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_shareDTO> portal_sharedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Portal_share" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_shares/{portal_share_id}")
    public ResponseEntity<Portal_shareDTO> get(@PathVariable("portal_share_id") Integer portal_share_id) {
        Portal_shareDTO dto = new Portal_shareDTO();
        Portal_share domain = portal_shareService.get(portal_share_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Portal_share" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_shares/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Portal_shareDTO> portal_sharedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Portal_share" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_shares")

    public ResponseEntity<Portal_shareDTO> create(@RequestBody Portal_shareDTO portal_sharedto) {
        Portal_shareDTO dto = new Portal_shareDTO();
        Portal_share domain = portal_sharedto.toDO();
		portal_shareService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Portal_share" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_shares/createBatch")
    public ResponseEntity<Boolean> createBatchPortal_share(@RequestBody List<Portal_shareDTO> portal_sharedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Portal_share" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_shares/{portal_share_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("portal_share_id") Integer portal_share_id) {
        Portal_shareDTO portal_sharedto = new Portal_shareDTO();
		Portal_share domain = new Portal_share();
		portal_sharedto.setId(portal_share_id);
		domain.setId(portal_share_id);
        Boolean rst = portal_shareService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Portal_share" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_portal/portal_shares/fetchdefault")
	public ResponseEntity<Page<Portal_shareDTO>> fetchDefault(Portal_shareSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Portal_shareDTO> list = new ArrayList<Portal_shareDTO>();
        
        Page<Portal_share> domains = portal_shareService.searchDefault(context) ;
        for(Portal_share portal_share : domains.getContent()){
            Portal_shareDTO dto = new Portal_shareDTO();
            dto.fromDO(portal_share);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
