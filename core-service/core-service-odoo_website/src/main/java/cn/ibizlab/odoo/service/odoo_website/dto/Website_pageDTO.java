package cn.ibizlab.odoo.service.odoo_website.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_website.valuerule.anno.website_page.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_page;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Website_pageDTO]
 */
public class Website_pageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TYPE]
     *
     */
    @Website_pageTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Website_pageIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [ARCH]
     *
     */
    @Website_pageArchDefault(info = "默认规则")
    private String arch;

    @JsonIgnore
    private boolean archDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Website_page__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @Website_pageWebsite_meta_descriptionDefault(info = "默认规则")
    private String website_meta_description;

    @JsonIgnore
    private boolean website_meta_descriptionDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Website_pageNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Website_pageDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @Website_pageWebsite_urlDefault(info = "默认规则")
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Website_pagePriorityDefault(info = "默认规则")
    private Integer priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [XML_ID]
     *
     */
    @Website_pageXml_idDefault(info = "默认规则")
    private String xml_id;

    @JsonIgnore
    private boolean xml_idDirtyFlag;

    /**
     * 属性 [WEBSITE_INDEXED]
     *
     */
    @Website_pageWebsite_indexedDefault(info = "默认规则")
    private String website_indexed;

    @JsonIgnore
    private boolean website_indexedDirtyFlag;

    /**
     * 属性 [VIEW_ID]
     *
     */
    @Website_pageView_idDefault(info = "默认规则")
    private Integer view_id;

    @JsonIgnore
    private boolean view_idDirtyFlag;

    /**
     * 属性 [THEME_TEMPLATE_ID]
     *
     */
    @Website_pageTheme_template_idDefault(info = "默认规则")
    private Integer theme_template_id;

    @JsonIgnore
    private boolean theme_template_idDirtyFlag;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @Website_pageWebsite_meta_og_imgDefault(info = "默认规则")
    private String website_meta_og_img;

    @JsonIgnore
    private boolean website_meta_og_imgDirtyFlag;

    /**
     * 属性 [CUSTOMIZE_SHOW]
     *
     */
    @Website_pageCustomize_showDefault(info = "默认规则")
    private String customize_show;

    @JsonIgnore
    private boolean customize_showDirtyFlag;

    /**
     * 属性 [MODEL]
     *
     */
    @Website_pageModelDefault(info = "默认规则")
    private String model;

    @JsonIgnore
    private boolean modelDirtyFlag;

    /**
     * 属性 [FIELD_PARENT]
     *
     */
    @Website_pageField_parentDefault(info = "默认规则")
    private String field_parent;

    @JsonIgnore
    private boolean field_parentDirtyFlag;

    /**
     * 属性 [ARCH_DB]
     *
     */
    @Website_pageArch_dbDefault(info = "默认规则")
    private String arch_db;

    @JsonIgnore
    private boolean arch_dbDirtyFlag;

    /**
     * 属性 [IS_HOMEPAGE]
     *
     */
    @Website_pageIs_homepageDefault(info = "默认规则")
    private String is_homepage;

    @JsonIgnore
    private boolean is_homepageDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Website_pageWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @Website_pageWebsite_meta_titleDefault(info = "默认规则")
    private String website_meta_title;

    @JsonIgnore
    private boolean website_meta_titleDirtyFlag;

    /**
     * 属性 [MODEL_DATA_ID]
     *
     */
    @Website_pageModel_data_idDefault(info = "默认规则")
    private Integer model_data_id;

    @JsonIgnore
    private boolean model_data_idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Website_pageWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MODE]
     *
     */
    @Website_pageModeDefault(info = "默认规则")
    private String mode;

    @JsonIgnore
    private boolean modeDirtyFlag;

    /**
     * 属性 [MENU_IDS]
     *
     */
    @Website_pageMenu_idsDefault(info = "默认规则")
    private String menu_ids;

    @JsonIgnore
    private boolean menu_idsDirtyFlag;

    /**
     * 属性 [INHERIT_CHILDREN_IDS]
     *
     */
    @Website_pageInherit_children_idsDefault(info = "默认规则")
    private String inherit_children_ids;

    @JsonIgnore
    private boolean inherit_children_idsDirtyFlag;

    /**
     * 属性 [ARCH_BASE]
     *
     */
    @Website_pageArch_baseDefault(info = "默认规则")
    private String arch_base;

    @JsonIgnore
    private boolean arch_baseDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Website_pageCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [IS_VISIBLE]
     *
     */
    @Website_pageIs_visibleDefault(info = "默认规则")
    private String is_visible;

    @JsonIgnore
    private boolean is_visibleDirtyFlag;

    /**
     * 属性 [FIRST_PAGE_ID]
     *
     */
    @Website_pageFirst_page_idDefault(info = "默认规则")
    private Integer first_page_id;

    @JsonIgnore
    private boolean first_page_idDirtyFlag;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @Website_pageIs_seo_optimizedDefault(info = "默认规则")
    private String is_seo_optimized;

    @JsonIgnore
    private boolean is_seo_optimizedDirtyFlag;

    /**
     * 属性 [HEADER_COLOR]
     *
     */
    @Website_pageHeader_colorDefault(info = "默认规则")
    private String header_color;

    @JsonIgnore
    private boolean header_colorDirtyFlag;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @Website_pageIs_publishedDefault(info = "默认规则")
    private String is_published;

    @JsonIgnore
    private boolean is_publishedDirtyFlag;

    /**
     * 属性 [PAGE_IDS]
     *
     */
    @Website_pagePage_idsDefault(info = "默认规则")
    private String page_ids;

    @JsonIgnore
    private boolean page_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Website_pageWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [URL]
     *
     */
    @Website_pageUrlDefault(info = "默认规则")
    private String url;

    @JsonIgnore
    private boolean urlDirtyFlag;

    /**
     * 属性 [HEADER_OVERLAY]
     *
     */
    @Website_pageHeader_overlayDefault(info = "默认规则")
    private String header_overlay;

    @JsonIgnore
    private boolean header_overlayDirtyFlag;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @Website_pageWebsite_meta_keywordsDefault(info = "默认规则")
    private String website_meta_keywords;

    @JsonIgnore
    private boolean website_meta_keywordsDirtyFlag;

    /**
     * 属性 [DATE_PUBLISH]
     *
     */
    @Website_pageDate_publishDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_publish;

    @JsonIgnore
    private boolean date_publishDirtyFlag;

    /**
     * 属性 [GROUPS_ID]
     *
     */
    @Website_pageGroups_idDefault(info = "默认规则")
    private String groups_id;

    @JsonIgnore
    private boolean groups_idDirtyFlag;

    /**
     * 属性 [KEY]
     *
     */
    @Website_pageKeyDefault(info = "默认规则")
    private String key;

    @JsonIgnore
    private boolean keyDirtyFlag;

    /**
     * 属性 [INHERIT_ID]
     *
     */
    @Website_pageInherit_idDefault(info = "默认规则")
    private Integer inherit_id;

    @JsonIgnore
    private boolean inherit_idDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Website_pageActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [MODEL_IDS]
     *
     */
    @Website_pageModel_idsDefault(info = "默认规则")
    private String model_ids;

    @JsonIgnore
    private boolean model_idsDirtyFlag;

    /**
     * 属性 [ARCH_FS]
     *
     */
    @Website_pageArch_fsDefault(info = "默认规则")
    private String arch_fs;

    @JsonIgnore
    private boolean arch_fsDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Website_pageWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Website_pageCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Website_pageCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Website_pageWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [ARCH]
     */
    @JsonProperty("arch")
    public String getArch(){
        return arch ;
    }

    /**
     * 设置 [ARCH]
     */
    @JsonProperty("arch")
    public void setArch(String  arch){
        this.arch = arch ;
        this.archDirtyFlag = true ;
    }

    /**
     * 获取 [ARCH]脏标记
     */
    @JsonIgnore
    public boolean getArchDirtyFlag(){
        return archDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return website_meta_description ;
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return website_meta_descriptionDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return website_url ;
    }

    /**
     * 设置 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_URL]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return website_urlDirtyFlag ;
    }

    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public Integer getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(Integer  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [XML_ID]
     */
    @JsonProperty("xml_id")
    public String getXml_id(){
        return xml_id ;
    }

    /**
     * 设置 [XML_ID]
     */
    @JsonProperty("xml_id")
    public void setXml_id(String  xml_id){
        this.xml_id = xml_id ;
        this.xml_idDirtyFlag = true ;
    }

    /**
     * 获取 [XML_ID]脏标记
     */
    @JsonIgnore
    public boolean getXml_idDirtyFlag(){
        return xml_idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_INDEXED]
     */
    @JsonProperty("website_indexed")
    public String getWebsite_indexed(){
        return website_indexed ;
    }

    /**
     * 设置 [WEBSITE_INDEXED]
     */
    @JsonProperty("website_indexed")
    public void setWebsite_indexed(String  website_indexed){
        this.website_indexed = website_indexed ;
        this.website_indexedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_INDEXED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_indexedDirtyFlag(){
        return website_indexedDirtyFlag ;
    }

    /**
     * 获取 [VIEW_ID]
     */
    @JsonProperty("view_id")
    public Integer getView_id(){
        return view_id ;
    }

    /**
     * 设置 [VIEW_ID]
     */
    @JsonProperty("view_id")
    public void setView_id(Integer  view_id){
        this.view_id = view_id ;
        this.view_idDirtyFlag = true ;
    }

    /**
     * 获取 [VIEW_ID]脏标记
     */
    @JsonIgnore
    public boolean getView_idDirtyFlag(){
        return view_idDirtyFlag ;
    }

    /**
     * 获取 [THEME_TEMPLATE_ID]
     */
    @JsonProperty("theme_template_id")
    public Integer getTheme_template_id(){
        return theme_template_id ;
    }

    /**
     * 设置 [THEME_TEMPLATE_ID]
     */
    @JsonProperty("theme_template_id")
    public void setTheme_template_id(Integer  theme_template_id){
        this.theme_template_id = theme_template_id ;
        this.theme_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [THEME_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTheme_template_idDirtyFlag(){
        return theme_template_idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return website_meta_og_img ;
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return website_meta_og_imgDirtyFlag ;
    }

    /**
     * 获取 [CUSTOMIZE_SHOW]
     */
    @JsonProperty("customize_show")
    public String getCustomize_show(){
        return customize_show ;
    }

    /**
     * 设置 [CUSTOMIZE_SHOW]
     */
    @JsonProperty("customize_show")
    public void setCustomize_show(String  customize_show){
        this.customize_show = customize_show ;
        this.customize_showDirtyFlag = true ;
    }

    /**
     * 获取 [CUSTOMIZE_SHOW]脏标记
     */
    @JsonIgnore
    public boolean getCustomize_showDirtyFlag(){
        return customize_showDirtyFlag ;
    }

    /**
     * 获取 [MODEL]
     */
    @JsonProperty("model")
    public String getModel(){
        return model ;
    }

    /**
     * 设置 [MODEL]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return modelDirtyFlag ;
    }

    /**
     * 获取 [FIELD_PARENT]
     */
    @JsonProperty("field_parent")
    public String getField_parent(){
        return field_parent ;
    }

    /**
     * 设置 [FIELD_PARENT]
     */
    @JsonProperty("field_parent")
    public void setField_parent(String  field_parent){
        this.field_parent = field_parent ;
        this.field_parentDirtyFlag = true ;
    }

    /**
     * 获取 [FIELD_PARENT]脏标记
     */
    @JsonIgnore
    public boolean getField_parentDirtyFlag(){
        return field_parentDirtyFlag ;
    }

    /**
     * 获取 [ARCH_DB]
     */
    @JsonProperty("arch_db")
    public String getArch_db(){
        return arch_db ;
    }

    /**
     * 设置 [ARCH_DB]
     */
    @JsonProperty("arch_db")
    public void setArch_db(String  arch_db){
        this.arch_db = arch_db ;
        this.arch_dbDirtyFlag = true ;
    }

    /**
     * 获取 [ARCH_DB]脏标记
     */
    @JsonIgnore
    public boolean getArch_dbDirtyFlag(){
        return arch_dbDirtyFlag ;
    }

    /**
     * 获取 [IS_HOMEPAGE]
     */
    @JsonProperty("is_homepage")
    public String getIs_homepage(){
        return is_homepage ;
    }

    /**
     * 设置 [IS_HOMEPAGE]
     */
    @JsonProperty("is_homepage")
    public void setIs_homepage(String  is_homepage){
        this.is_homepage = is_homepage ;
        this.is_homepageDirtyFlag = true ;
    }

    /**
     * 获取 [IS_HOMEPAGE]脏标记
     */
    @JsonIgnore
    public boolean getIs_homepageDirtyFlag(){
        return is_homepageDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return website_meta_title ;
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return website_meta_titleDirtyFlag ;
    }

    /**
     * 获取 [MODEL_DATA_ID]
     */
    @JsonProperty("model_data_id")
    public Integer getModel_data_id(){
        return model_data_id ;
    }

    /**
     * 设置 [MODEL_DATA_ID]
     */
    @JsonProperty("model_data_id")
    public void setModel_data_id(Integer  model_data_id){
        this.model_data_id = model_data_id ;
        this.model_data_idDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_DATA_ID]脏标记
     */
    @JsonIgnore
    public boolean getModel_data_idDirtyFlag(){
        return model_data_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MODE]
     */
    @JsonProperty("mode")
    public String getMode(){
        return mode ;
    }

    /**
     * 设置 [MODE]
     */
    @JsonProperty("mode")
    public void setMode(String  mode){
        this.mode = mode ;
        this.modeDirtyFlag = true ;
    }

    /**
     * 获取 [MODE]脏标记
     */
    @JsonIgnore
    public boolean getModeDirtyFlag(){
        return modeDirtyFlag ;
    }

    /**
     * 获取 [MENU_IDS]
     */
    @JsonProperty("menu_ids")
    public String getMenu_ids(){
        return menu_ids ;
    }

    /**
     * 设置 [MENU_IDS]
     */
    @JsonProperty("menu_ids")
    public void setMenu_ids(String  menu_ids){
        this.menu_ids = menu_ids ;
        this.menu_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MENU_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMenu_idsDirtyFlag(){
        return menu_idsDirtyFlag ;
    }

    /**
     * 获取 [INHERIT_CHILDREN_IDS]
     */
    @JsonProperty("inherit_children_ids")
    public String getInherit_children_ids(){
        return inherit_children_ids ;
    }

    /**
     * 设置 [INHERIT_CHILDREN_IDS]
     */
    @JsonProperty("inherit_children_ids")
    public void setInherit_children_ids(String  inherit_children_ids){
        this.inherit_children_ids = inherit_children_ids ;
        this.inherit_children_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INHERIT_CHILDREN_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInherit_children_idsDirtyFlag(){
        return inherit_children_idsDirtyFlag ;
    }

    /**
     * 获取 [ARCH_BASE]
     */
    @JsonProperty("arch_base")
    public String getArch_base(){
        return arch_base ;
    }

    /**
     * 设置 [ARCH_BASE]
     */
    @JsonProperty("arch_base")
    public void setArch_base(String  arch_base){
        this.arch_base = arch_base ;
        this.arch_baseDirtyFlag = true ;
    }

    /**
     * 获取 [ARCH_BASE]脏标记
     */
    @JsonIgnore
    public boolean getArch_baseDirtyFlag(){
        return arch_baseDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [IS_VISIBLE]
     */
    @JsonProperty("is_visible")
    public String getIs_visible(){
        return is_visible ;
    }

    /**
     * 设置 [IS_VISIBLE]
     */
    @JsonProperty("is_visible")
    public void setIs_visible(String  is_visible){
        this.is_visible = is_visible ;
        this.is_visibleDirtyFlag = true ;
    }

    /**
     * 获取 [IS_VISIBLE]脏标记
     */
    @JsonIgnore
    public boolean getIs_visibleDirtyFlag(){
        return is_visibleDirtyFlag ;
    }

    /**
     * 获取 [FIRST_PAGE_ID]
     */
    @JsonProperty("first_page_id")
    public Integer getFirst_page_id(){
        return first_page_id ;
    }

    /**
     * 设置 [FIRST_PAGE_ID]
     */
    @JsonProperty("first_page_id")
    public void setFirst_page_id(Integer  first_page_id){
        this.first_page_id = first_page_id ;
        this.first_page_idDirtyFlag = true ;
    }

    /**
     * 获取 [FIRST_PAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getFirst_page_idDirtyFlag(){
        return first_page_idDirtyFlag ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return is_seo_optimized ;
    }

    /**
     * 设置 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return is_seo_optimizedDirtyFlag ;
    }

    /**
     * 获取 [HEADER_COLOR]
     */
    @JsonProperty("header_color")
    public String getHeader_color(){
        return header_color ;
    }

    /**
     * 设置 [HEADER_COLOR]
     */
    @JsonProperty("header_color")
    public void setHeader_color(String  header_color){
        this.header_color = header_color ;
        this.header_colorDirtyFlag = true ;
    }

    /**
     * 获取 [HEADER_COLOR]脏标记
     */
    @JsonIgnore
    public boolean getHeader_colorDirtyFlag(){
        return header_colorDirtyFlag ;
    }

    /**
     * 获取 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return is_published ;
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return is_publishedDirtyFlag ;
    }

    /**
     * 获取 [PAGE_IDS]
     */
    @JsonProperty("page_ids")
    public String getPage_ids(){
        return page_ids ;
    }

    /**
     * 设置 [PAGE_IDS]
     */
    @JsonProperty("page_ids")
    public void setPage_ids(String  page_ids){
        this.page_ids = page_ids ;
        this.page_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPage_idsDirtyFlag(){
        return page_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [URL]
     */
    @JsonProperty("url")
    public String getUrl(){
        return url ;
    }

    /**
     * 设置 [URL]
     */
    @JsonProperty("url")
    public void setUrl(String  url){
        this.url = url ;
        this.urlDirtyFlag = true ;
    }

    /**
     * 获取 [URL]脏标记
     */
    @JsonIgnore
    public boolean getUrlDirtyFlag(){
        return urlDirtyFlag ;
    }

    /**
     * 获取 [HEADER_OVERLAY]
     */
    @JsonProperty("header_overlay")
    public String getHeader_overlay(){
        return header_overlay ;
    }

    /**
     * 设置 [HEADER_OVERLAY]
     */
    @JsonProperty("header_overlay")
    public void setHeader_overlay(String  header_overlay){
        this.header_overlay = header_overlay ;
        this.header_overlayDirtyFlag = true ;
    }

    /**
     * 获取 [HEADER_OVERLAY]脏标记
     */
    @JsonIgnore
    public boolean getHeader_overlayDirtyFlag(){
        return header_overlayDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return website_meta_keywords ;
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return website_meta_keywordsDirtyFlag ;
    }

    /**
     * 获取 [DATE_PUBLISH]
     */
    @JsonProperty("date_publish")
    public Timestamp getDate_publish(){
        return date_publish ;
    }

    /**
     * 设置 [DATE_PUBLISH]
     */
    @JsonProperty("date_publish")
    public void setDate_publish(Timestamp  date_publish){
        this.date_publish = date_publish ;
        this.date_publishDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_PUBLISH]脏标记
     */
    @JsonIgnore
    public boolean getDate_publishDirtyFlag(){
        return date_publishDirtyFlag ;
    }

    /**
     * 获取 [GROUPS_ID]
     */
    @JsonProperty("groups_id")
    public String getGroups_id(){
        return groups_id ;
    }

    /**
     * 设置 [GROUPS_ID]
     */
    @JsonProperty("groups_id")
    public void setGroups_id(String  groups_id){
        this.groups_id = groups_id ;
        this.groups_idDirtyFlag = true ;
    }

    /**
     * 获取 [GROUPS_ID]脏标记
     */
    @JsonIgnore
    public boolean getGroups_idDirtyFlag(){
        return groups_idDirtyFlag ;
    }

    /**
     * 获取 [KEY]
     */
    @JsonProperty("key")
    public String getKey(){
        return key ;
    }

    /**
     * 设置 [KEY]
     */
    @JsonProperty("key")
    public void setKey(String  key){
        this.key = key ;
        this.keyDirtyFlag = true ;
    }

    /**
     * 获取 [KEY]脏标记
     */
    @JsonIgnore
    public boolean getKeyDirtyFlag(){
        return keyDirtyFlag ;
    }

    /**
     * 获取 [INHERIT_ID]
     */
    @JsonProperty("inherit_id")
    public Integer getInherit_id(){
        return inherit_id ;
    }

    /**
     * 设置 [INHERIT_ID]
     */
    @JsonProperty("inherit_id")
    public void setInherit_id(Integer  inherit_id){
        this.inherit_id = inherit_id ;
        this.inherit_idDirtyFlag = true ;
    }

    /**
     * 获取 [INHERIT_ID]脏标记
     */
    @JsonIgnore
    public boolean getInherit_idDirtyFlag(){
        return inherit_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [MODEL_IDS]
     */
    @JsonProperty("model_ids")
    public String getModel_ids(){
        return model_ids ;
    }

    /**
     * 设置 [MODEL_IDS]
     */
    @JsonProperty("model_ids")
    public void setModel_ids(String  model_ids){
        this.model_ids = model_ids ;
        this.model_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getModel_idsDirtyFlag(){
        return model_idsDirtyFlag ;
    }

    /**
     * 获取 [ARCH_FS]
     */
    @JsonProperty("arch_fs")
    public String getArch_fs(){
        return arch_fs ;
    }

    /**
     * 设置 [ARCH_FS]
     */
    @JsonProperty("arch_fs")
    public void setArch_fs(String  arch_fs){
        this.arch_fs = arch_fs ;
        this.arch_fsDirtyFlag = true ;
    }

    /**
     * 获取 [ARCH_FS]脏标记
     */
    @JsonIgnore
    public boolean getArch_fsDirtyFlag(){
        return arch_fsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Website_page toDO() {
        Website_page srfdomain = new Website_page();
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getArchDirtyFlag())
            srfdomain.setArch(arch);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWebsite_meta_descriptionDirtyFlag())
            srfdomain.setWebsite_meta_description(website_meta_description);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWebsite_urlDirtyFlag())
            srfdomain.setWebsite_url(website_url);
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(getXml_idDirtyFlag())
            srfdomain.setXml_id(xml_id);
        if(getWebsite_indexedDirtyFlag())
            srfdomain.setWebsite_indexed(website_indexed);
        if(getView_idDirtyFlag())
            srfdomain.setView_id(view_id);
        if(getTheme_template_idDirtyFlag())
            srfdomain.setTheme_template_id(theme_template_id);
        if(getWebsite_meta_og_imgDirtyFlag())
            srfdomain.setWebsite_meta_og_img(website_meta_og_img);
        if(getCustomize_showDirtyFlag())
            srfdomain.setCustomize_show(customize_show);
        if(getModelDirtyFlag())
            srfdomain.setModel(model);
        if(getField_parentDirtyFlag())
            srfdomain.setField_parent(field_parent);
        if(getArch_dbDirtyFlag())
            srfdomain.setArch_db(arch_db);
        if(getIs_homepageDirtyFlag())
            srfdomain.setIs_homepage(is_homepage);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(getWebsite_meta_titleDirtyFlag())
            srfdomain.setWebsite_meta_title(website_meta_title);
        if(getModel_data_idDirtyFlag())
            srfdomain.setModel_data_id(model_data_id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getModeDirtyFlag())
            srfdomain.setMode(mode);
        if(getMenu_idsDirtyFlag())
            srfdomain.setMenu_ids(menu_ids);
        if(getInherit_children_idsDirtyFlag())
            srfdomain.setInherit_children_ids(inherit_children_ids);
        if(getArch_baseDirtyFlag())
            srfdomain.setArch_base(arch_base);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIs_visibleDirtyFlag())
            srfdomain.setIs_visible(is_visible);
        if(getFirst_page_idDirtyFlag())
            srfdomain.setFirst_page_id(first_page_id);
        if(getIs_seo_optimizedDirtyFlag())
            srfdomain.setIs_seo_optimized(is_seo_optimized);
        if(getHeader_colorDirtyFlag())
            srfdomain.setHeader_color(header_color);
        if(getIs_publishedDirtyFlag())
            srfdomain.setIs_published(is_published);
        if(getPage_idsDirtyFlag())
            srfdomain.setPage_ids(page_ids);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getUrlDirtyFlag())
            srfdomain.setUrl(url);
        if(getHeader_overlayDirtyFlag())
            srfdomain.setHeader_overlay(header_overlay);
        if(getWebsite_meta_keywordsDirtyFlag())
            srfdomain.setWebsite_meta_keywords(website_meta_keywords);
        if(getDate_publishDirtyFlag())
            srfdomain.setDate_publish(date_publish);
        if(getGroups_idDirtyFlag())
            srfdomain.setGroups_id(groups_id);
        if(getKeyDirtyFlag())
            srfdomain.setKey(key);
        if(getInherit_idDirtyFlag())
            srfdomain.setInherit_id(inherit_id);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getModel_idsDirtyFlag())
            srfdomain.setModel_ids(model_ids);
        if(getArch_fsDirtyFlag())
            srfdomain.setArch_fs(arch_fs);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Website_page srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getArchDirtyFlag())
            this.setArch(srfdomain.getArch());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWebsite_meta_descriptionDirtyFlag())
            this.setWebsite_meta_description(srfdomain.getWebsite_meta_description());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWebsite_urlDirtyFlag())
            this.setWebsite_url(srfdomain.getWebsite_url());
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.getXml_idDirtyFlag())
            this.setXml_id(srfdomain.getXml_id());
        if(srfdomain.getWebsite_indexedDirtyFlag())
            this.setWebsite_indexed(srfdomain.getWebsite_indexed());
        if(srfdomain.getView_idDirtyFlag())
            this.setView_id(srfdomain.getView_id());
        if(srfdomain.getTheme_template_idDirtyFlag())
            this.setTheme_template_id(srfdomain.getTheme_template_id());
        if(srfdomain.getWebsite_meta_og_imgDirtyFlag())
            this.setWebsite_meta_og_img(srfdomain.getWebsite_meta_og_img());
        if(srfdomain.getCustomize_showDirtyFlag())
            this.setCustomize_show(srfdomain.getCustomize_show());
        if(srfdomain.getModelDirtyFlag())
            this.setModel(srfdomain.getModel());
        if(srfdomain.getField_parentDirtyFlag())
            this.setField_parent(srfdomain.getField_parent());
        if(srfdomain.getArch_dbDirtyFlag())
            this.setArch_db(srfdomain.getArch_db());
        if(srfdomain.getIs_homepageDirtyFlag())
            this.setIs_homepage(srfdomain.getIs_homepage());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.getWebsite_meta_titleDirtyFlag())
            this.setWebsite_meta_title(srfdomain.getWebsite_meta_title());
        if(srfdomain.getModel_data_idDirtyFlag())
            this.setModel_data_id(srfdomain.getModel_data_id());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getModeDirtyFlag())
            this.setMode(srfdomain.getMode());
        if(srfdomain.getMenu_idsDirtyFlag())
            this.setMenu_ids(srfdomain.getMenu_ids());
        if(srfdomain.getInherit_children_idsDirtyFlag())
            this.setInherit_children_ids(srfdomain.getInherit_children_ids());
        if(srfdomain.getArch_baseDirtyFlag())
            this.setArch_base(srfdomain.getArch_base());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIs_visibleDirtyFlag())
            this.setIs_visible(srfdomain.getIs_visible());
        if(srfdomain.getFirst_page_idDirtyFlag())
            this.setFirst_page_id(srfdomain.getFirst_page_id());
        if(srfdomain.getIs_seo_optimizedDirtyFlag())
            this.setIs_seo_optimized(srfdomain.getIs_seo_optimized());
        if(srfdomain.getHeader_colorDirtyFlag())
            this.setHeader_color(srfdomain.getHeader_color());
        if(srfdomain.getIs_publishedDirtyFlag())
            this.setIs_published(srfdomain.getIs_published());
        if(srfdomain.getPage_idsDirtyFlag())
            this.setPage_ids(srfdomain.getPage_ids());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getUrlDirtyFlag())
            this.setUrl(srfdomain.getUrl());
        if(srfdomain.getHeader_overlayDirtyFlag())
            this.setHeader_overlay(srfdomain.getHeader_overlay());
        if(srfdomain.getWebsite_meta_keywordsDirtyFlag())
            this.setWebsite_meta_keywords(srfdomain.getWebsite_meta_keywords());
        if(srfdomain.getDate_publishDirtyFlag())
            this.setDate_publish(srfdomain.getDate_publish());
        if(srfdomain.getGroups_idDirtyFlag())
            this.setGroups_id(srfdomain.getGroups_id());
        if(srfdomain.getKeyDirtyFlag())
            this.setKey(srfdomain.getKey());
        if(srfdomain.getInherit_idDirtyFlag())
            this.setInherit_id(srfdomain.getInherit_id());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getModel_idsDirtyFlag())
            this.setModel_ids(srfdomain.getModel_ids());
        if(srfdomain.getArch_fsDirtyFlag())
            this.setArch_fs(srfdomain.getArch_fs());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Website_pageDTO> fromDOPage(List<Website_page> poPage)   {
        if(poPage == null)
            return null;
        List<Website_pageDTO> dtos=new ArrayList<Website_pageDTO>();
        for(Website_page domain : poPage) {
            Website_pageDTO dto = new Website_pageDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

