package cn.ibizlab.odoo.service.odoo_website.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_website.dto.Website_sale_payment_acquirer_onboarding_wizardDTO;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_sale_payment_acquirer_onboarding_wizardService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_sale_payment_acquirer_onboarding_wizardSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Website_sale_payment_acquirer_onboarding_wizard" })
@RestController
@RequestMapping("")
public class Website_sale_payment_acquirer_onboarding_wizardResource {

    @Autowired
    private IWebsite_sale_payment_acquirer_onboarding_wizardService website_sale_payment_acquirer_onboarding_wizardService;

    public IWebsite_sale_payment_acquirer_onboarding_wizardService getWebsite_sale_payment_acquirer_onboarding_wizardService() {
        return this.website_sale_payment_acquirer_onboarding_wizardService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/createBatch")
    public ResponseEntity<Boolean> createBatchWebsite_sale_payment_acquirer_onboarding_wizard(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizardDTO> website_sale_payment_acquirer_onboarding_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/{website_sale_payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Website_sale_payment_acquirer_onboarding_wizardDTO> get(@PathVariable("website_sale_payment_acquirer_onboarding_wizard_id") Integer website_sale_payment_acquirer_onboarding_wizard_id) {
        Website_sale_payment_acquirer_onboarding_wizardDTO dto = new Website_sale_payment_acquirer_onboarding_wizardDTO();
        Website_sale_payment_acquirer_onboarding_wizard domain = website_sale_payment_acquirer_onboarding_wizardService.get(website_sale_payment_acquirer_onboarding_wizard_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizardDTO> website_sale_payment_acquirer_onboarding_wizarddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/{website_sale_payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Website_sale_payment_acquirer_onboarding_wizardDTO> update(@PathVariable("website_sale_payment_acquirer_onboarding_wizard_id") Integer website_sale_payment_acquirer_onboarding_wizard_id, @RequestBody Website_sale_payment_acquirer_onboarding_wizardDTO website_sale_payment_acquirer_onboarding_wizarddto) {
		Website_sale_payment_acquirer_onboarding_wizard domain = website_sale_payment_acquirer_onboarding_wizarddto.toDO();
        domain.setId(website_sale_payment_acquirer_onboarding_wizard_id);
		website_sale_payment_acquirer_onboarding_wizardService.update(domain);
		Website_sale_payment_acquirer_onboarding_wizardDTO dto = new Website_sale_payment_acquirer_onboarding_wizardDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/{website_sale_payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_sale_payment_acquirer_onboarding_wizard_id") Integer website_sale_payment_acquirer_onboarding_wizard_id) {
        Website_sale_payment_acquirer_onboarding_wizardDTO website_sale_payment_acquirer_onboarding_wizarddto = new Website_sale_payment_acquirer_onboarding_wizardDTO();
		Website_sale_payment_acquirer_onboarding_wizard domain = new Website_sale_payment_acquirer_onboarding_wizard();
		website_sale_payment_acquirer_onboarding_wizarddto.setId(website_sale_payment_acquirer_onboarding_wizard_id);
		domain.setId(website_sale_payment_acquirer_onboarding_wizard_id);
        Boolean rst = website_sale_payment_acquirer_onboarding_wizardService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizardDTO> website_sale_payment_acquirer_onboarding_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Website_sale_payment_acquirer_onboarding_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_sale_payment_acquirer_onboarding_wizards")

    public ResponseEntity<Website_sale_payment_acquirer_onboarding_wizardDTO> create(@RequestBody Website_sale_payment_acquirer_onboarding_wizardDTO website_sale_payment_acquirer_onboarding_wizarddto) {
        Website_sale_payment_acquirer_onboarding_wizardDTO dto = new Website_sale_payment_acquirer_onboarding_wizardDTO();
        Website_sale_payment_acquirer_onboarding_wizard domain = website_sale_payment_acquirer_onboarding_wizarddto.toDO();
		website_sale_payment_acquirer_onboarding_wizardService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Website_sale_payment_acquirer_onboarding_wizard" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_website/website_sale_payment_acquirer_onboarding_wizards/fetchdefault")
	public ResponseEntity<Page<Website_sale_payment_acquirer_onboarding_wizardDTO>> fetchDefault(Website_sale_payment_acquirer_onboarding_wizardSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Website_sale_payment_acquirer_onboarding_wizardDTO> list = new ArrayList<Website_sale_payment_acquirer_onboarding_wizardDTO>();
        
        Page<Website_sale_payment_acquirer_onboarding_wizard> domains = website_sale_payment_acquirer_onboarding_wizardService.searchDefault(context) ;
        for(Website_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard : domains.getContent()){
            Website_sale_payment_acquirer_onboarding_wizardDTO dto = new Website_sale_payment_acquirer_onboarding_wizardDTO();
            dto.fromDO(website_sale_payment_acquirer_onboarding_wizard);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
