package cn.ibizlab.odoo.service.odoo_website.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_website.dto.Website_published_multi_mixinDTO;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_published_multi_mixin;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_published_multi_mixinService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_published_multi_mixinSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Website_published_multi_mixin" })
@RestController
@RequestMapping("")
public class Website_published_multi_mixinResource {

    @Autowired
    private IWebsite_published_multi_mixinService website_published_multi_mixinService;

    public IWebsite_published_multi_mixinService getWebsite_published_multi_mixinService() {
        return this.website_published_multi_mixinService;
    }

    @ApiOperation(value = "建立数据", tags = {"Website_published_multi_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_published_multi_mixins")

    public ResponseEntity<Website_published_multi_mixinDTO> create(@RequestBody Website_published_multi_mixinDTO website_published_multi_mixindto) {
        Website_published_multi_mixinDTO dto = new Website_published_multi_mixinDTO();
        Website_published_multi_mixin domain = website_published_multi_mixindto.toDO();
		website_published_multi_mixinService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Website_published_multi_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_published_multi_mixins/{website_published_multi_mixin_id}")
    public ResponseEntity<Website_published_multi_mixinDTO> get(@PathVariable("website_published_multi_mixin_id") Integer website_published_multi_mixin_id) {
        Website_published_multi_mixinDTO dto = new Website_published_multi_mixinDTO();
        Website_published_multi_mixin domain = website_published_multi_mixinService.get(website_published_multi_mixin_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Website_published_multi_mixin" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_published_multi_mixins/createBatch")
    public ResponseEntity<Boolean> createBatchWebsite_published_multi_mixin(@RequestBody List<Website_published_multi_mixinDTO> website_published_multi_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Website_published_multi_mixin" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_published_multi_mixins/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Website_published_multi_mixinDTO> website_published_multi_mixindtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Website_published_multi_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_published_multi_mixins/{website_published_multi_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_published_multi_mixin_id") Integer website_published_multi_mixin_id) {
        Website_published_multi_mixinDTO website_published_multi_mixindto = new Website_published_multi_mixinDTO();
		Website_published_multi_mixin domain = new Website_published_multi_mixin();
		website_published_multi_mixindto.setId(website_published_multi_mixin_id);
		domain.setId(website_published_multi_mixin_id);
        Boolean rst = website_published_multi_mixinService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Website_published_multi_mixin" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_published_multi_mixins/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_published_multi_mixinDTO> website_published_multi_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Website_published_multi_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_published_multi_mixins/{website_published_multi_mixin_id}")

    public ResponseEntity<Website_published_multi_mixinDTO> update(@PathVariable("website_published_multi_mixin_id") Integer website_published_multi_mixin_id, @RequestBody Website_published_multi_mixinDTO website_published_multi_mixindto) {
		Website_published_multi_mixin domain = website_published_multi_mixindto.toDO();
        domain.setId(website_published_multi_mixin_id);
		website_published_multi_mixinService.update(domain);
		Website_published_multi_mixinDTO dto = new Website_published_multi_mixinDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Website_published_multi_mixin" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_website/website_published_multi_mixins/fetchdefault")
	public ResponseEntity<Page<Website_published_multi_mixinDTO>> fetchDefault(Website_published_multi_mixinSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Website_published_multi_mixinDTO> list = new ArrayList<Website_published_multi_mixinDTO>();
        
        Page<Website_published_multi_mixin> domains = website_published_multi_mixinService.searchDefault(context) ;
        for(Website_published_multi_mixin website_published_multi_mixin : domains.getContent()){
            Website_published_multi_mixinDTO dto = new Website_published_multi_mixinDTO();
            dto.fromDO(website_published_multi_mixin);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
