package cn.ibizlab.odoo.service.odoo_website.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_website.dto.Website_menuDTO;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_menu;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_menuService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_menuSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Website_menu" })
@RestController
@RequestMapping("")
public class Website_menuResource {

    @Autowired
    private IWebsite_menuService website_menuService;

    public IWebsite_menuService getWebsite_menuService() {
        return this.website_menuService;
    }

    @ApiOperation(value = "更新数据", tags = {"Website_menu" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_menus/{website_menu_id}")

    public ResponseEntity<Website_menuDTO> update(@PathVariable("website_menu_id") Integer website_menu_id, @RequestBody Website_menuDTO website_menudto) {
		Website_menu domain = website_menudto.toDO();
        domain.setId(website_menu_id);
		website_menuService.update(domain);
		Website_menuDTO dto = new Website_menuDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Website_menu" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_menus/{website_menu_id}")
    public ResponseEntity<Website_menuDTO> get(@PathVariable("website_menu_id") Integer website_menu_id) {
        Website_menuDTO dto = new Website_menuDTO();
        Website_menu domain = website_menuService.get(website_menu_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Website_menu" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_menus/{website_menu_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_menu_id") Integer website_menu_id) {
        Website_menuDTO website_menudto = new Website_menuDTO();
		Website_menu domain = new Website_menu();
		website_menudto.setId(website_menu_id);
		domain.setId(website_menu_id);
        Boolean rst = website_menuService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Website_menu" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_menus/createBatch")
    public ResponseEntity<Boolean> createBatchWebsite_menu(@RequestBody List<Website_menuDTO> website_menudtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Website_menu" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_menus/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Website_menuDTO> website_menudtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Website_menu" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_menus/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_menuDTO> website_menudtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Website_menu" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_menus")

    public ResponseEntity<Website_menuDTO> create(@RequestBody Website_menuDTO website_menudto) {
        Website_menuDTO dto = new Website_menuDTO();
        Website_menu domain = website_menudto.toDO();
		website_menuService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Website_menu" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_website/website_menus/fetchdefault")
	public ResponseEntity<Page<Website_menuDTO>> fetchDefault(Website_menuSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Website_menuDTO> list = new ArrayList<Website_menuDTO>();
        
        Page<Website_menu> domains = website_menuService.searchDefault(context) ;
        for(Website_menu website_menu : domains.getContent()){
            Website_menuDTO dto = new Website_menuDTO();
            dto.fromDO(website_menu);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
