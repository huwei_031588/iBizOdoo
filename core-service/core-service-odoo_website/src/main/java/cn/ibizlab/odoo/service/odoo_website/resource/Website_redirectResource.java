package cn.ibizlab.odoo.service.odoo_website.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_website.dto.Website_redirectDTO;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_redirectService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_redirectSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Website_redirect" })
@RestController
@RequestMapping("")
public class Website_redirectResource {

    @Autowired
    private IWebsite_redirectService website_redirectService;

    public IWebsite_redirectService getWebsite_redirectService() {
        return this.website_redirectService;
    }

    @ApiOperation(value = "更新数据", tags = {"Website_redirect" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_redirects/{website_redirect_id}")

    public ResponseEntity<Website_redirectDTO> update(@PathVariable("website_redirect_id") Integer website_redirect_id, @RequestBody Website_redirectDTO website_redirectdto) {
		Website_redirect domain = website_redirectdto.toDO();
        domain.setId(website_redirect_id);
		website_redirectService.update(domain);
		Website_redirectDTO dto = new Website_redirectDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Website_redirect" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_redirects/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_redirectDTO> website_redirectdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Website_redirect" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_redirects/{website_redirect_id}")
    public ResponseEntity<Website_redirectDTO> get(@PathVariable("website_redirect_id") Integer website_redirect_id) {
        Website_redirectDTO dto = new Website_redirectDTO();
        Website_redirect domain = website_redirectService.get(website_redirect_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Website_redirect" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_redirects/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Website_redirectDTO> website_redirectdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Website_redirect" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_redirects")

    public ResponseEntity<Website_redirectDTO> create(@RequestBody Website_redirectDTO website_redirectdto) {
        Website_redirectDTO dto = new Website_redirectDTO();
        Website_redirect domain = website_redirectdto.toDO();
		website_redirectService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Website_redirect" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_redirects/{website_redirect_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_redirect_id") Integer website_redirect_id) {
        Website_redirectDTO website_redirectdto = new Website_redirectDTO();
		Website_redirect domain = new Website_redirect();
		website_redirectdto.setId(website_redirect_id);
		domain.setId(website_redirect_id);
        Boolean rst = website_redirectService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Website_redirect" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_redirects/createBatch")
    public ResponseEntity<Boolean> createBatchWebsite_redirect(@RequestBody List<Website_redirectDTO> website_redirectdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Website_redirect" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_website/website_redirects/fetchdefault")
	public ResponseEntity<Page<Website_redirectDTO>> fetchDefault(Website_redirectSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Website_redirectDTO> list = new ArrayList<Website_redirectDTO>();
        
        Page<Website_redirect> domains = website_redirectService.searchDefault(context) ;
        for(Website_redirect website_redirect : domains.getContent()){
            Website_redirectDTO dto = new Website_redirectDTO();
            dto.fromDO(website_redirect);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
