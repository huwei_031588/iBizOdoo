package cn.ibizlab.odoo.service.odoo_website.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_website.dto.Website_pageDTO;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_page;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_pageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_pageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Website_page" })
@RestController
@RequestMapping("")
public class Website_pageResource {

    @Autowired
    private IWebsite_pageService website_pageService;

    public IWebsite_pageService getWebsite_pageService() {
        return this.website_pageService;
    }

    @ApiOperation(value = "更新数据", tags = {"Website_page" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_pages/{website_page_id}")

    public ResponseEntity<Website_pageDTO> update(@PathVariable("website_page_id") Integer website_page_id, @RequestBody Website_pageDTO website_pagedto) {
		Website_page domain = website_pagedto.toDO();
        domain.setId(website_page_id);
		website_pageService.update(domain);
		Website_pageDTO dto = new Website_pageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Website_page" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_pages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Website_pageDTO> website_pagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Website_page" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_pages/{website_page_id}")
    public ResponseEntity<Website_pageDTO> get(@PathVariable("website_page_id") Integer website_page_id) {
        Website_pageDTO dto = new Website_pageDTO();
        Website_page domain = website_pageService.get(website_page_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Website_page" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_pages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_pageDTO> website_pagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Website_page" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_pages/{website_page_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_page_id") Integer website_page_id) {
        Website_pageDTO website_pagedto = new Website_pageDTO();
		Website_page domain = new Website_page();
		website_pagedto.setId(website_page_id);
		domain.setId(website_page_id);
        Boolean rst = website_pageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Website_page" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_pages")

    public ResponseEntity<Website_pageDTO> create(@RequestBody Website_pageDTO website_pagedto) {
        Website_pageDTO dto = new Website_pageDTO();
        Website_page domain = website_pagedto.toDO();
		website_pageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Website_page" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_pages/createBatch")
    public ResponseEntity<Boolean> createBatchWebsite_page(@RequestBody List<Website_pageDTO> website_pagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Website_page" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_website/website_pages/fetchdefault")
	public ResponseEntity<Page<Website_pageDTO>> fetchDefault(Website_pageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Website_pageDTO> list = new ArrayList<Website_pageDTO>();
        
        Page<Website_page> domains = website_pageService.searchDefault(context) ;
        for(Website_page website_page : domains.getContent()){
            Website_pageDTO dto = new Website_pageDTO();
            dto.fromDO(website_page);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
