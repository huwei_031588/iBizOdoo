package cn.ibizlab.odoo.service.odoo_uom.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_uom")
public class odoo_uomRestConfiguration {

}
