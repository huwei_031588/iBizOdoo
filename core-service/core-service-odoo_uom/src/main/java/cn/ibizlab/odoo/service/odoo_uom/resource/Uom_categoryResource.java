package cn.ibizlab.odoo.service.odoo_uom.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_uom.dto.Uom_categoryDTO;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.odoo.core.odoo_uom.service.IUom_categoryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_categorySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Uom_category" })
@RestController
@RequestMapping("")
public class Uom_categoryResource {

    @Autowired
    private IUom_categoryService uom_categoryService;

    public IUom_categoryService getUom_categoryService() {
        return this.uom_categoryService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Uom_category" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_uom/uom_categories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Uom_categoryDTO> uom_categorydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Uom_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_uom/uom_categories")

    public ResponseEntity<Uom_categoryDTO> create(@RequestBody Uom_categoryDTO uom_categorydto) {
        Uom_categoryDTO dto = new Uom_categoryDTO();
        Uom_category domain = uom_categorydto.toDO();
		uom_categoryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Uom_category" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_uom/uom_categories/createBatch")
    public ResponseEntity<Boolean> createBatchUom_category(@RequestBody List<Uom_categoryDTO> uom_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Uom_category" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_uom/uom_categories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Uom_categoryDTO> uom_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Uom_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_uom/uom_categories/{uom_category_id}")

    public ResponseEntity<Uom_categoryDTO> update(@PathVariable("uom_category_id") Integer uom_category_id, @RequestBody Uom_categoryDTO uom_categorydto) {
		Uom_category domain = uom_categorydto.toDO();
        domain.setId(uom_category_id);
		uom_categoryService.update(domain);
		Uom_categoryDTO dto = new Uom_categoryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Uom_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_uom/uom_categories/{uom_category_id}")
    public ResponseEntity<Uom_categoryDTO> get(@PathVariable("uom_category_id") Integer uom_category_id) {
        Uom_categoryDTO dto = new Uom_categoryDTO();
        Uom_category domain = uom_categoryService.get(uom_category_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Uom_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_uom/uom_categories/{uom_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("uom_category_id") Integer uom_category_id) {
        Uom_categoryDTO uom_categorydto = new Uom_categoryDTO();
		Uom_category domain = new Uom_category();
		uom_categorydto.setId(uom_category_id);
		domain.setId(uom_category_id);
        Boolean rst = uom_categoryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Uom_category" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_uom/uom_categories/fetchdefault")
	public ResponseEntity<Page<Uom_categoryDTO>> fetchDefault(Uom_categorySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Uom_categoryDTO> list = new ArrayList<Uom_categoryDTO>();
        
        Page<Uom_category> domains = uom_categoryService.searchDefault(context) ;
        for(Uom_category uom_category : domains.getContent()){
            Uom_categoryDTO dto = new Uom_categoryDTO();
            dto.fromDO(uom_category);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
