package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_language_exportDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_export;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_exportService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_exportSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_language_export" })
@RestController
@RequestMapping("")
public class Base_language_exportResource {

    @Autowired
    private IBase_language_exportService base_language_exportService;

    public IBase_language_exportService getBase_language_exportService() {
        return this.base_language_exportService;
    }

    @ApiOperation(value = "删除数据", tags = {"Base_language_export" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_exports/{base_language_export_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_language_export_id") Integer base_language_export_id) {
        Base_language_exportDTO base_language_exportdto = new Base_language_exportDTO();
		Base_language_export domain = new Base_language_export();
		base_language_exportdto.setId(base_language_export_id);
		domain.setId(base_language_export_id);
        Boolean rst = base_language_exportService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Base_language_export" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_exports/{base_language_export_id}")
    public ResponseEntity<Base_language_exportDTO> get(@PathVariable("base_language_export_id") Integer base_language_export_id) {
        Base_language_exportDTO dto = new Base_language_exportDTO();
        Base_language_export domain = base_language_exportService.get(base_language_export_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_language_export" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_exports/{base_language_export_id}")

    public ResponseEntity<Base_language_exportDTO> update(@PathVariable("base_language_export_id") Integer base_language_export_id, @RequestBody Base_language_exportDTO base_language_exportdto) {
		Base_language_export domain = base_language_exportdto.toDO();
        domain.setId(base_language_export_id);
		base_language_exportService.update(domain);
		Base_language_exportDTO dto = new Base_language_exportDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_language_export" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_exports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_language_exportDTO> base_language_exportdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_language_export" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_exports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_language_exportDTO> base_language_exportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_language_export" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_exports")

    public ResponseEntity<Base_language_exportDTO> create(@RequestBody Base_language_exportDTO base_language_exportdto) {
        Base_language_exportDTO dto = new Base_language_exportDTO();
        Base_language_export domain = base_language_exportdto.toDO();
		base_language_exportService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_language_export" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_exports/createBatch")
    public ResponseEntity<Boolean> createBatchBase_language_export(@RequestBody List<Base_language_exportDTO> base_language_exportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_language_export" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_language_exports/fetchdefault")
	public ResponseEntity<Page<Base_language_exportDTO>> fetchDefault(Base_language_exportSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_language_exportDTO> list = new ArrayList<Base_language_exportDTO>();
        
        Page<Base_language_export> domains = base_language_exportService.searchDefault(context) ;
        for(Base_language_export base_language_export : domains.getContent()){
            Base_language_exportDTO dto = new Base_language_exportDTO();
            dto.fromDO(base_language_export);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
