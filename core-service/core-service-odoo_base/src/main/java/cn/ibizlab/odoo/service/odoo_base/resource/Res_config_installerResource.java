package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_config_installerDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_installer;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_config_installerService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_installerSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_config_installer" })
@RestController
@RequestMapping("")
public class Res_config_installerResource {

    @Autowired
    private IRes_config_installerService res_config_installerService;

    public IRes_config_installerService getRes_config_installerService() {
        return this.res_config_installerService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_config_installer" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_config_installers/createBatch")
    public ResponseEntity<Boolean> createBatchRes_config_installer(@RequestBody List<Res_config_installerDTO> res_config_installerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_config_installer" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_config_installers/{res_config_installer_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_config_installer_id") Integer res_config_installer_id) {
        Res_config_installerDTO res_config_installerdto = new Res_config_installerDTO();
		Res_config_installer domain = new Res_config_installer();
		res_config_installerdto.setId(res_config_installer_id);
		domain.setId(res_config_installer_id);
        Boolean rst = res_config_installerService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_config_installer" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_config_installers/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_config_installerDTO> res_config_installerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_config_installer" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_config_installers/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_config_installerDTO> res_config_installerdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_config_installer" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_config_installers")

    public ResponseEntity<Res_config_installerDTO> create(@RequestBody Res_config_installerDTO res_config_installerdto) {
        Res_config_installerDTO dto = new Res_config_installerDTO();
        Res_config_installer domain = res_config_installerdto.toDO();
		res_config_installerService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_config_installer" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_config_installers/{res_config_installer_id}")

    public ResponseEntity<Res_config_installerDTO> update(@PathVariable("res_config_installer_id") Integer res_config_installer_id, @RequestBody Res_config_installerDTO res_config_installerdto) {
		Res_config_installer domain = res_config_installerdto.toDO();
        domain.setId(res_config_installer_id);
		res_config_installerService.update(domain);
		Res_config_installerDTO dto = new Res_config_installerDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_config_installer" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_config_installers/{res_config_installer_id}")
    public ResponseEntity<Res_config_installerDTO> get(@PathVariable("res_config_installer_id") Integer res_config_installer_id) {
        Res_config_installerDTO dto = new Res_config_installerDTO();
        Res_config_installer domain = res_config_installerService.get(res_config_installer_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_config_installer" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_config_installers/fetchdefault")
	public ResponseEntity<Page<Res_config_installerDTO>> fetchDefault(Res_config_installerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_config_installerDTO> list = new ArrayList<Res_config_installerDTO>();
        
        Page<Res_config_installer> domains = res_config_installerService.searchDefault(context) ;
        for(Res_config_installer res_config_installer : domains.getContent()){
            Res_config_installerDTO dto = new Res_config_installerDTO();
            dto.fromDO(res_config_installer);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
