package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_partnerDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partnerService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partnerSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_partner" })
@RestController
@RequestMapping("")
public class Res_partnerResource {

    @Autowired
    private IRes_partnerService res_partnerService;

    public IRes_partnerService getRes_partnerService() {
        return this.res_partnerService;
    }

    @ApiOperation(value = "删除数据", tags = {"Res_partner" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partners/{res_partner_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_id") Integer res_partner_id) {
        Res_partnerDTO res_partnerdto = new Res_partnerDTO();
		Res_partner domain = new Res_partner();
		res_partnerdto.setId(res_partner_id);
		domain.setId(res_partner_id);
        Boolean rst = res_partnerService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Res_partner" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partners/{res_partner_id}")
    public ResponseEntity<Res_partnerDTO> get(@PathVariable("res_partner_id") Integer res_partner_id) {
        Res_partnerDTO dto = new Res_partnerDTO();
        Res_partner domain = res_partnerService.get(res_partner_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_partner" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partners/{res_partner_id}")

    public ResponseEntity<Res_partnerDTO> update(@PathVariable("res_partner_id") Integer res_partner_id, @RequestBody Res_partnerDTO res_partnerdto) {
		Res_partner domain = res_partnerdto.toDO();
        domain.setId(res_partner_id);
		res_partnerService.update(domain);
		Res_partnerDTO dto = new Res_partnerDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_partner" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partners/createBatch")
    public ResponseEntity<Boolean> createBatchRes_partner(@RequestBody List<Res_partnerDTO> res_partnerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_partner" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partners/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partnerDTO> res_partnerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_partner" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partners")

    public ResponseEntity<Res_partnerDTO> create(@RequestBody Res_partnerDTO res_partnerdto) {
        Res_partnerDTO dto = new Res_partnerDTO();
        Res_partner domain = res_partnerdto.toDO();
		res_partnerService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_partner" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partners/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_partnerDTO> res_partnerdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_partner" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_partners/fetchdefault")
	public ResponseEntity<Page<Res_partnerDTO>> fetchDefault(Res_partnerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partnerDTO> list = new ArrayList<Res_partnerDTO>();
        
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
        for(Res_partner res_partner : domains.getContent()){
            Res_partnerDTO dto = new Res_partnerDTO();
            dto.fromDO(res_partner);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
