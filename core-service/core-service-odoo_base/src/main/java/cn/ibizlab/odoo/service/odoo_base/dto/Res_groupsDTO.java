package cn.ibizlab.odoo.service.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_groups.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_groups;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Res_groupsDTO]
 */
public class Res_groupsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [USERS]
     *
     */
    @Res_groupsUsersDefault(info = "默认规则")
    private String users;

    @JsonIgnore
    private boolean usersDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Res_groupsNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [IMPLIED_IDS]
     *
     */
    @Res_groupsImplied_idsDefault(info = "默认规则")
    private String implied_ids;

    @JsonIgnore
    private boolean implied_idsDirtyFlag;

    /**
     * 属性 [TRANS_IMPLIED_IDS]
     *
     */
    @Res_groupsTrans_implied_idsDefault(info = "默认规则")
    private String trans_implied_ids;

    @JsonIgnore
    private boolean trans_implied_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Res_groupsDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [VIEW_ACCESS]
     *
     */
    @Res_groupsView_accessDefault(info = "默认规则")
    private String view_access;

    @JsonIgnore
    private boolean view_accessDirtyFlag;

    /**
     * 属性 [RULE_GROUPS]
     *
     */
    @Res_groupsRule_groupsDefault(info = "默认规则")
    private String rule_groups;

    @JsonIgnore
    private boolean rule_groupsDirtyFlag;

    /**
     * 属性 [MODEL_ACCESS]
     *
     */
    @Res_groupsModel_accessDefault(info = "默认规则")
    private String model_access;

    @JsonIgnore
    private boolean model_accessDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Res_groupsCategory_idDefault(info = "默认规则")
    private Integer category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Res_groupsIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MENU_ACCESS]
     *
     */
    @Res_groupsMenu_accessDefault(info = "默认规则")
    private String menu_access;

    @JsonIgnore
    private boolean menu_accessDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Res_groups__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Res_groupsCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [COMMENT]
     *
     */
    @Res_groupsCommentDefault(info = "默认规则")
    private String comment;

    @JsonIgnore
    private boolean commentDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Res_groupsWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [SHARE]
     *
     */
    @Res_groupsShareDefault(info = "默认规则")
    private String share;

    @JsonIgnore
    private boolean shareDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Res_groupsColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [FULL_NAME]
     *
     */
    @Res_groupsFull_nameDefault(info = "默认规则")
    private String full_name;

    @JsonIgnore
    private boolean full_nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Res_groupsCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Res_groupsWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Res_groupsCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Res_groupsWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [USERS]
     */
    @JsonProperty("users")
    public String getUsers(){
        return users ;
    }

    /**
     * 设置 [USERS]
     */
    @JsonProperty("users")
    public void setUsers(String  users){
        this.users = users ;
        this.usersDirtyFlag = true ;
    }

    /**
     * 获取 [USERS]脏标记
     */
    @JsonIgnore
    public boolean getUsersDirtyFlag(){
        return usersDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [IMPLIED_IDS]
     */
    @JsonProperty("implied_ids")
    public String getImplied_ids(){
        return implied_ids ;
    }

    /**
     * 设置 [IMPLIED_IDS]
     */
    @JsonProperty("implied_ids")
    public void setImplied_ids(String  implied_ids){
        this.implied_ids = implied_ids ;
        this.implied_idsDirtyFlag = true ;
    }

    /**
     * 获取 [IMPLIED_IDS]脏标记
     */
    @JsonIgnore
    public boolean getImplied_idsDirtyFlag(){
        return implied_idsDirtyFlag ;
    }

    /**
     * 获取 [TRANS_IMPLIED_IDS]
     */
    @JsonProperty("trans_implied_ids")
    public String getTrans_implied_ids(){
        return trans_implied_ids ;
    }

    /**
     * 设置 [TRANS_IMPLIED_IDS]
     */
    @JsonProperty("trans_implied_ids")
    public void setTrans_implied_ids(String  trans_implied_ids){
        this.trans_implied_ids = trans_implied_ids ;
        this.trans_implied_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TRANS_IMPLIED_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTrans_implied_idsDirtyFlag(){
        return trans_implied_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [VIEW_ACCESS]
     */
    @JsonProperty("view_access")
    public String getView_access(){
        return view_access ;
    }

    /**
     * 设置 [VIEW_ACCESS]
     */
    @JsonProperty("view_access")
    public void setView_access(String  view_access){
        this.view_access = view_access ;
        this.view_accessDirtyFlag = true ;
    }

    /**
     * 获取 [VIEW_ACCESS]脏标记
     */
    @JsonIgnore
    public boolean getView_accessDirtyFlag(){
        return view_accessDirtyFlag ;
    }

    /**
     * 获取 [RULE_GROUPS]
     */
    @JsonProperty("rule_groups")
    public String getRule_groups(){
        return rule_groups ;
    }

    /**
     * 设置 [RULE_GROUPS]
     */
    @JsonProperty("rule_groups")
    public void setRule_groups(String  rule_groups){
        this.rule_groups = rule_groups ;
        this.rule_groupsDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_GROUPS]脏标记
     */
    @JsonIgnore
    public boolean getRule_groupsDirtyFlag(){
        return rule_groupsDirtyFlag ;
    }

    /**
     * 获取 [MODEL_ACCESS]
     */
    @JsonProperty("model_access")
    public String getModel_access(){
        return model_access ;
    }

    /**
     * 设置 [MODEL_ACCESS]
     */
    @JsonProperty("model_access")
    public void setModel_access(String  model_access){
        this.model_access = model_access ;
        this.model_accessDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_ACCESS]脏标记
     */
    @JsonIgnore
    public boolean getModel_accessDirtyFlag(){
        return model_accessDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MENU_ACCESS]
     */
    @JsonProperty("menu_access")
    public String getMenu_access(){
        return menu_access ;
    }

    /**
     * 设置 [MENU_ACCESS]
     */
    @JsonProperty("menu_access")
    public void setMenu_access(String  menu_access){
        this.menu_access = menu_access ;
        this.menu_accessDirtyFlag = true ;
    }

    /**
     * 获取 [MENU_ACCESS]脏标记
     */
    @JsonIgnore
    public boolean getMenu_accessDirtyFlag(){
        return menu_accessDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [COMMENT]
     */
    @JsonProperty("comment")
    public String getComment(){
        return comment ;
    }

    /**
     * 设置 [COMMENT]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

    /**
     * 获取 [COMMENT]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return commentDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [SHARE]
     */
    @JsonProperty("share")
    public String getShare(){
        return share ;
    }

    /**
     * 设置 [SHARE]
     */
    @JsonProperty("share")
    public void setShare(String  share){
        this.share = share ;
        this.shareDirtyFlag = true ;
    }

    /**
     * 获取 [SHARE]脏标记
     */
    @JsonIgnore
    public boolean getShareDirtyFlag(){
        return shareDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [FULL_NAME]
     */
    @JsonProperty("full_name")
    public String getFull_name(){
        return full_name ;
    }

    /**
     * 设置 [FULL_NAME]
     */
    @JsonProperty("full_name")
    public void setFull_name(String  full_name){
        this.full_name = full_name ;
        this.full_nameDirtyFlag = true ;
    }

    /**
     * 获取 [FULL_NAME]脏标记
     */
    @JsonIgnore
    public boolean getFull_nameDirtyFlag(){
        return full_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Res_groups toDO() {
        Res_groups srfdomain = new Res_groups();
        if(getUsersDirtyFlag())
            srfdomain.setUsers(users);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getImplied_idsDirtyFlag())
            srfdomain.setImplied_ids(implied_ids);
        if(getTrans_implied_idsDirtyFlag())
            srfdomain.setTrans_implied_ids(trans_implied_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getView_accessDirtyFlag())
            srfdomain.setView_access(view_access);
        if(getRule_groupsDirtyFlag())
            srfdomain.setRule_groups(rule_groups);
        if(getModel_accessDirtyFlag())
            srfdomain.setModel_access(model_access);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMenu_accessDirtyFlag())
            srfdomain.setMenu_access(menu_access);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getCommentDirtyFlag())
            srfdomain.setComment(comment);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getShareDirtyFlag())
            srfdomain.setShare(share);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getFull_nameDirtyFlag())
            srfdomain.setFull_name(full_name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Res_groups srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getUsersDirtyFlag())
            this.setUsers(srfdomain.getUsers());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getImplied_idsDirtyFlag())
            this.setImplied_ids(srfdomain.getImplied_ids());
        if(srfdomain.getTrans_implied_idsDirtyFlag())
            this.setTrans_implied_ids(srfdomain.getTrans_implied_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getView_accessDirtyFlag())
            this.setView_access(srfdomain.getView_access());
        if(srfdomain.getRule_groupsDirtyFlag())
            this.setRule_groups(srfdomain.getRule_groups());
        if(srfdomain.getModel_accessDirtyFlag())
            this.setModel_access(srfdomain.getModel_access());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMenu_accessDirtyFlag())
            this.setMenu_access(srfdomain.getMenu_access());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getCommentDirtyFlag())
            this.setComment(srfdomain.getComment());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getShareDirtyFlag())
            this.setShare(srfdomain.getShare());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getFull_nameDirtyFlag())
            this.setFull_name(srfdomain.getFull_name());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Res_groupsDTO> fromDOPage(List<Res_groups> poPage)   {
        if(poPage == null)
            return null;
        List<Res_groupsDTO> dtos=new ArrayList<Res_groupsDTO>();
        for(Res_groups domain : poPage) {
            Res_groupsDTO dto = new Res_groupsDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

