package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_module_updateDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_update;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_updateService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_updateSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_module_update" })
@RestController
@RequestMapping("")
public class Base_module_updateResource {

    @Autowired
    private IBase_module_updateService base_module_updateService;

    public IBase_module_updateService getBase_module_updateService() {
        return this.base_module_updateService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_module_update" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_updates/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_module_updateDTO> base_module_updatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_module_update" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_updates/createBatch")
    public ResponseEntity<Boolean> createBatchBase_module_update(@RequestBody List<Base_module_updateDTO> base_module_updatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_module_update" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_updates/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_module_updateDTO> base_module_updatedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_module_update" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_updates/{base_module_update_id}")
    public ResponseEntity<Base_module_updateDTO> get(@PathVariable("base_module_update_id") Integer base_module_update_id) {
        Base_module_updateDTO dto = new Base_module_updateDTO();
        Base_module_update domain = base_module_updateService.get(base_module_update_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_module_update" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_updates/{base_module_update_id}")

    public ResponseEntity<Base_module_updateDTO> update(@PathVariable("base_module_update_id") Integer base_module_update_id, @RequestBody Base_module_updateDTO base_module_updatedto) {
		Base_module_update domain = base_module_updatedto.toDO();
        domain.setId(base_module_update_id);
		base_module_updateService.update(domain);
		Base_module_updateDTO dto = new Base_module_updateDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_module_update" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_updates")

    public ResponseEntity<Base_module_updateDTO> create(@RequestBody Base_module_updateDTO base_module_updatedto) {
        Base_module_updateDTO dto = new Base_module_updateDTO();
        Base_module_update domain = base_module_updatedto.toDO();
		base_module_updateService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_module_update" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_updates/{base_module_update_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_module_update_id") Integer base_module_update_id) {
        Base_module_updateDTO base_module_updatedto = new Base_module_updateDTO();
		Base_module_update domain = new Base_module_update();
		base_module_updatedto.setId(base_module_update_id);
		domain.setId(base_module_update_id);
        Boolean rst = base_module_updateService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_module_update" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_module_updates/fetchdefault")
	public ResponseEntity<Page<Base_module_updateDTO>> fetchDefault(Base_module_updateSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_module_updateDTO> list = new ArrayList<Base_module_updateDTO>();
        
        Page<Base_module_update> domains = base_module_updateService.searchDefault(context) ;
        for(Base_module_update base_module_update : domains.getContent()){
            Base_module_updateDTO dto = new Base_module_updateDTO();
            dto.fromDO(base_module_update);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
