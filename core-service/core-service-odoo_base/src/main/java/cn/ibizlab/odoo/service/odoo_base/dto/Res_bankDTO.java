package cn.ibizlab.odoo.service.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_bank.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_bank;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Res_bankDTO]
 */
public class Res_bankDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [STREET2]
     *
     */
    @Res_bankStreet2Default(info = "默认规则")
    private String street2;

    @JsonIgnore
    private boolean street2DirtyFlag;

    /**
     * 属性 [PHONE]
     *
     */
    @Res_bankPhoneDefault(info = "默认规则")
    private String phone;

    @JsonIgnore
    private boolean phoneDirtyFlag;

    /**
     * 属性 [CITY]
     *
     */
    @Res_bankCityDefault(info = "默认规则")
    private String city;

    @JsonIgnore
    private boolean cityDirtyFlag;

    /**
     * 属性 [STREET]
     *
     */
    @Res_bankStreetDefault(info = "默认规则")
    private String street;

    @JsonIgnore
    private boolean streetDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Res_bankNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [EMAIL]
     *
     */
    @Res_bankEmailDefault(info = "默认规则")
    private String email;

    @JsonIgnore
    private boolean emailDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Res_bankActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Res_bankDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Res_bank__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Res_bankIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [BIC]
     *
     */
    @Res_bankBicDefault(info = "默认规则")
    private String bic;

    @JsonIgnore
    private boolean bicDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Res_bankCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ZIP]
     *
     */
    @Res_bankZipDefault(info = "默认规则")
    private String zip;

    @JsonIgnore
    private boolean zipDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Res_bankWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Res_bankWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Res_bankCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COUNTRY_TEXT]
     *
     */
    @Res_bankCountry_textDefault(info = "默认规则")
    private String country_text;

    @JsonIgnore
    private boolean country_textDirtyFlag;

    /**
     * 属性 [STATE_TEXT]
     *
     */
    @Res_bankState_textDefault(info = "默认规则")
    private String state_text;

    @JsonIgnore
    private boolean state_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Res_bankCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [COUNTRY]
     *
     */
    @Res_bankCountryDefault(info = "默认规则")
    private Integer country;

    @JsonIgnore
    private boolean countryDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Res_bankStateDefault(info = "默认规则")
    private Integer state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Res_bankWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [STREET2]
     */
    @JsonProperty("street2")
    public String getStreet2(){
        return street2 ;
    }

    /**
     * 设置 [STREET2]
     */
    @JsonProperty("street2")
    public void setStreet2(String  street2){
        this.street2 = street2 ;
        this.street2DirtyFlag = true ;
    }

    /**
     * 获取 [STREET2]脏标记
     */
    @JsonIgnore
    public boolean getStreet2DirtyFlag(){
        return street2DirtyFlag ;
    }

    /**
     * 获取 [PHONE]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return phone ;
    }

    /**
     * 设置 [PHONE]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

    /**
     * 获取 [PHONE]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return phoneDirtyFlag ;
    }

    /**
     * 获取 [CITY]
     */
    @JsonProperty("city")
    public String getCity(){
        return city ;
    }

    /**
     * 设置 [CITY]
     */
    @JsonProperty("city")
    public void setCity(String  city){
        this.city = city ;
        this.cityDirtyFlag = true ;
    }

    /**
     * 获取 [CITY]脏标记
     */
    @JsonIgnore
    public boolean getCityDirtyFlag(){
        return cityDirtyFlag ;
    }

    /**
     * 获取 [STREET]
     */
    @JsonProperty("street")
    public String getStreet(){
        return street ;
    }

    /**
     * 设置 [STREET]
     */
    @JsonProperty("street")
    public void setStreet(String  street){
        this.street = street ;
        this.streetDirtyFlag = true ;
    }

    /**
     * 获取 [STREET]脏标记
     */
    @JsonIgnore
    public boolean getStreetDirtyFlag(){
        return streetDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [EMAIL]
     */
    @JsonProperty("email")
    public String getEmail(){
        return email ;
    }

    /**
     * 设置 [EMAIL]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return emailDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [BIC]
     */
    @JsonProperty("bic")
    public String getBic(){
        return bic ;
    }

    /**
     * 设置 [BIC]
     */
    @JsonProperty("bic")
    public void setBic(String  bic){
        this.bic = bic ;
        this.bicDirtyFlag = true ;
    }

    /**
     * 获取 [BIC]脏标记
     */
    @JsonIgnore
    public boolean getBicDirtyFlag(){
        return bicDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ZIP]
     */
    @JsonProperty("zip")
    public String getZip(){
        return zip ;
    }

    /**
     * 设置 [ZIP]
     */
    @JsonProperty("zip")
    public void setZip(String  zip){
        this.zip = zip ;
        this.zipDirtyFlag = true ;
    }

    /**
     * 获取 [ZIP]脏标记
     */
    @JsonIgnore
    public boolean getZipDirtyFlag(){
        return zipDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_TEXT]
     */
    @JsonProperty("country_text")
    public String getCountry_text(){
        return country_text ;
    }

    /**
     * 设置 [COUNTRY_TEXT]
     */
    @JsonProperty("country_text")
    public void setCountry_text(String  country_text){
        this.country_text = country_text ;
        this.country_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_textDirtyFlag(){
        return country_textDirtyFlag ;
    }

    /**
     * 获取 [STATE_TEXT]
     */
    @JsonProperty("state_text")
    public String getState_text(){
        return state_text ;
    }

    /**
     * 设置 [STATE_TEXT]
     */
    @JsonProperty("state_text")
    public void setState_text(String  state_text){
        this.state_text = state_text ;
        this.state_textDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getState_textDirtyFlag(){
        return state_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY]
     */
    @JsonProperty("country")
    public Integer getCountry(){
        return country ;
    }

    /**
     * 设置 [COUNTRY]
     */
    @JsonProperty("country")
    public void setCountry(Integer  country){
        this.country = country ;
        this.countryDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY]脏标记
     */
    @JsonIgnore
    public boolean getCountryDirtyFlag(){
        return countryDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public Integer getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(Integer  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Res_bank toDO() {
        Res_bank srfdomain = new Res_bank();
        if(getStreet2DirtyFlag())
            srfdomain.setStreet2(street2);
        if(getPhoneDirtyFlag())
            srfdomain.setPhone(phone);
        if(getCityDirtyFlag())
            srfdomain.setCity(city);
        if(getStreetDirtyFlag())
            srfdomain.setStreet(street);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getEmailDirtyFlag())
            srfdomain.setEmail(email);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getBicDirtyFlag())
            srfdomain.setBic(bic);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getZipDirtyFlag())
            srfdomain.setZip(zip);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCountry_textDirtyFlag())
            srfdomain.setCountry_text(country_text);
        if(getState_textDirtyFlag())
            srfdomain.setState_text(state_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCountryDirtyFlag())
            srfdomain.setCountry(country);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Res_bank srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getStreet2DirtyFlag())
            this.setStreet2(srfdomain.getStreet2());
        if(srfdomain.getPhoneDirtyFlag())
            this.setPhone(srfdomain.getPhone());
        if(srfdomain.getCityDirtyFlag())
            this.setCity(srfdomain.getCity());
        if(srfdomain.getStreetDirtyFlag())
            this.setStreet(srfdomain.getStreet());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getEmailDirtyFlag())
            this.setEmail(srfdomain.getEmail());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getBicDirtyFlag())
            this.setBic(srfdomain.getBic());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getZipDirtyFlag())
            this.setZip(srfdomain.getZip());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCountry_textDirtyFlag())
            this.setCountry_text(srfdomain.getCountry_text());
        if(srfdomain.getState_textDirtyFlag())
            this.setState_text(srfdomain.getState_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCountryDirtyFlag())
            this.setCountry(srfdomain.getCountry());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Res_bankDTO> fromDOPage(List<Res_bank> poPage)   {
        if(poPage == null)
            return null;
        List<Res_bankDTO> dtos=new ArrayList<Res_bankDTO>();
        for(Res_bank domain : poPage) {
            Res_bankDTO dto = new Res_bankDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

