package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_partner_titleDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_title;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_titleService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_titleSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_partner_title" })
@RestController
@RequestMapping("")
public class Res_partner_titleResource {

    @Autowired
    private IRes_partner_titleService res_partner_titleService;

    public IRes_partner_titleService getRes_partner_titleService() {
        return this.res_partner_titleService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_partner_title" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_titles/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_partner_titleDTO> res_partner_titledtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_partner_title" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_titles/createBatch")
    public ResponseEntity<Boolean> createBatchRes_partner_title(@RequestBody List<Res_partner_titleDTO> res_partner_titledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_partner_title" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_titles/{res_partner_title_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_title_id") Integer res_partner_title_id) {
        Res_partner_titleDTO res_partner_titledto = new Res_partner_titleDTO();
		Res_partner_title domain = new Res_partner_title();
		res_partner_titledto.setId(res_partner_title_id);
		domain.setId(res_partner_title_id);
        Boolean rst = res_partner_titleService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Res_partner_title" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_titles/{res_partner_title_id}")
    public ResponseEntity<Res_partner_titleDTO> get(@PathVariable("res_partner_title_id") Integer res_partner_title_id) {
        Res_partner_titleDTO dto = new Res_partner_titleDTO();
        Res_partner_title domain = res_partner_titleService.get(res_partner_title_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_partner_title" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_titles/{res_partner_title_id}")

    public ResponseEntity<Res_partner_titleDTO> update(@PathVariable("res_partner_title_id") Integer res_partner_title_id, @RequestBody Res_partner_titleDTO res_partner_titledto) {
		Res_partner_title domain = res_partner_titledto.toDO();
        domain.setId(res_partner_title_id);
		res_partner_titleService.update(domain);
		Res_partner_titleDTO dto = new Res_partner_titleDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_partner_title" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_titles/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_titleDTO> res_partner_titledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_partner_title" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_titles")

    public ResponseEntity<Res_partner_titleDTO> create(@RequestBody Res_partner_titleDTO res_partner_titledto) {
        Res_partner_titleDTO dto = new Res_partner_titleDTO();
        Res_partner_title domain = res_partner_titledto.toDO();
		res_partner_titleService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_partner_title" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_partner_titles/fetchdefault")
	public ResponseEntity<Page<Res_partner_titleDTO>> fetchDefault(Res_partner_titleSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_titleDTO> list = new ArrayList<Res_partner_titleDTO>();
        
        Page<Res_partner_title> domains = res_partner_titleService.searchDefault(context) ;
        for(Res_partner_title res_partner_title : domains.getContent()){
            Res_partner_titleDTO dto = new Res_partner_titleDTO();
            dto.fromDO(res_partner_title);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
