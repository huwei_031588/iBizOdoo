package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_country_stateDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country_state;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_country_stateService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_country_stateSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_country_state" })
@RestController
@RequestMapping("")
public class Res_country_stateResource {

    @Autowired
    private IRes_country_stateService res_country_stateService;

    public IRes_country_stateService getRes_country_stateService() {
        return this.res_country_stateService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_country_state" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_country_states/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_country_stateDTO> res_country_statedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_country_state" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_country_states/{res_country_state_id}")
    public ResponseEntity<Res_country_stateDTO> get(@PathVariable("res_country_state_id") Integer res_country_state_id) {
        Res_country_stateDTO dto = new Res_country_stateDTO();
        Res_country_state domain = res_country_stateService.get(res_country_state_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_country_state" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_country_states/{res_country_state_id}")

    public ResponseEntity<Res_country_stateDTO> update(@PathVariable("res_country_state_id") Integer res_country_state_id, @RequestBody Res_country_stateDTO res_country_statedto) {
		Res_country_state domain = res_country_statedto.toDO();
        domain.setId(res_country_state_id);
		res_country_stateService.update(domain);
		Res_country_stateDTO dto = new Res_country_stateDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_country_state" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_country_states/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_country_stateDTO> res_country_statedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_country_state" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_country_states")

    public ResponseEntity<Res_country_stateDTO> create(@RequestBody Res_country_stateDTO res_country_statedto) {
        Res_country_stateDTO dto = new Res_country_stateDTO();
        Res_country_state domain = res_country_statedto.toDO();
		res_country_stateService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_country_state" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_country_states/{res_country_state_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_country_state_id") Integer res_country_state_id) {
        Res_country_stateDTO res_country_statedto = new Res_country_stateDTO();
		Res_country_state domain = new Res_country_state();
		res_country_statedto.setId(res_country_state_id);
		domain.setId(res_country_state_id);
        Boolean rst = res_country_stateService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_country_state" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_country_states/createBatch")
    public ResponseEntity<Boolean> createBatchRes_country_state(@RequestBody List<Res_country_stateDTO> res_country_statedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_country_state" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_country_states/fetchdefault")
	public ResponseEntity<Page<Res_country_stateDTO>> fetchDefault(Res_country_stateSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_country_stateDTO> list = new ArrayList<Res_country_stateDTO>();
        
        Page<Res_country_state> domains = res_country_stateService.searchDefault(context) ;
        for(Res_country_state res_country_state : domains.getContent()){
            Res_country_stateDTO dto = new Res_country_stateDTO();
            dto.fromDO(res_country_state);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
