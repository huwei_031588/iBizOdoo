package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_usersDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_usersService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_usersSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_users" })
@RestController
@RequestMapping("")
public class Res_usersResource {

    @Autowired
    private IRes_usersService res_usersService;

    public IRes_usersService getRes_usersService() {
        return this.res_usersService;
    }

    @ApiOperation(value = "更新数据", tags = {"Res_users" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_users/{res_users_id}")

    public ResponseEntity<Res_usersDTO> update(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_usersDTO res_usersdto) {
		Res_users domain = res_usersdto.toDO();
        domain.setId(res_users_id);
		res_usersService.update(domain);
		Res_usersDTO dto = new Res_usersDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_users" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_users")

    public ResponseEntity<Res_usersDTO> create(@RequestBody Res_usersDTO res_usersdto) {
        Res_usersDTO dto = new Res_usersDTO();
        Res_users domain = res_usersdto.toDO();
		res_usersService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_users" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_users/{res_users_id}")
    public ResponseEntity<Res_usersDTO> get(@PathVariable("res_users_id") Integer res_users_id) {
        Res_usersDTO dto = new Res_usersDTO();
        Res_users domain = res_usersService.get(res_users_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_users" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_users/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_users" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_users/createBatch")
    public ResponseEntity<Boolean> createBatchRes_users(@RequestBody List<Res_usersDTO> res_usersdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_users" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_users/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_users" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_users/{res_users_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_users_id") Integer res_users_id) {
        Res_usersDTO res_usersdto = new Res_usersDTO();
		Res_users domain = new Res_users();
		res_usersdto.setId(res_users_id);
		domain.setId(res_users_id);
        Boolean rst = res_usersService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_users" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_users/fetchdefault")
	public ResponseEntity<Page<Res_usersDTO>> fetchDefault(Res_usersSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_usersDTO> list = new ArrayList<Res_usersDTO>();
        
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
        for(Res_users res_users : domains.getContent()){
            Res_usersDTO dto = new Res_usersDTO();
            dto.fromDO(res_users);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
