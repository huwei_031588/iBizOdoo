package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_langDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_lang;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_langService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_langSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_lang" })
@RestController
@RequestMapping("")
public class Res_langResource {

    @Autowired
    private IRes_langService res_langService;

    public IRes_langService getRes_langService() {
        return this.res_langService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_lang" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_langs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_langDTO> res_langdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_lang" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_langs/{res_lang_id}")

    public ResponseEntity<Res_langDTO> update(@PathVariable("res_lang_id") Integer res_lang_id, @RequestBody Res_langDTO res_langdto) {
		Res_lang domain = res_langdto.toDO();
        domain.setId(res_lang_id);
		res_langService.update(domain);
		Res_langDTO dto = new Res_langDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_lang" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_langs")

    public ResponseEntity<Res_langDTO> create(@RequestBody Res_langDTO res_langdto) {
        Res_langDTO dto = new Res_langDTO();
        Res_lang domain = res_langdto.toDO();
		res_langService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_lang" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_langs/{res_lang_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_lang_id") Integer res_lang_id) {
        Res_langDTO res_langdto = new Res_langDTO();
		Res_lang domain = new Res_lang();
		res_langdto.setId(res_lang_id);
		domain.setId(res_lang_id);
        Boolean rst = res_langService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_lang" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_langs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_langDTO> res_langdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_lang" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_langs/createBatch")
    public ResponseEntity<Boolean> createBatchRes_lang(@RequestBody List<Res_langDTO> res_langdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_lang" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_langs/{res_lang_id}")
    public ResponseEntity<Res_langDTO> get(@PathVariable("res_lang_id") Integer res_lang_id) {
        Res_langDTO dto = new Res_langDTO();
        Res_lang domain = res_langService.get(res_lang_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_lang" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_langs/fetchdefault")
	public ResponseEntity<Page<Res_langDTO>> fetchDefault(Res_langSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_langDTO> list = new ArrayList<Res_langDTO>();
        
        Page<Res_lang> domains = res_langService.searchDefault(context) ;
        for(Res_lang res_lang : domains.getContent()){
            Res_langDTO dto = new Res_langDTO();
            dto.fromDO(res_lang);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
