package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_partner_autocomplete_syncDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_autocomplete_sync;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_autocomplete_syncService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_autocomplete_syncSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_partner_autocomplete_sync" })
@RestController
@RequestMapping("")
public class Res_partner_autocomplete_syncResource {

    @Autowired
    private IRes_partner_autocomplete_syncService res_partner_autocomplete_syncService;

    public IRes_partner_autocomplete_syncService getRes_partner_autocomplete_syncService() {
        return this.res_partner_autocomplete_syncService;
    }

    @ApiOperation(value = "删除数据", tags = {"Res_partner_autocomplete_sync" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_autocomplete_syncs/{res_partner_autocomplete_sync_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_autocomplete_sync_id") Integer res_partner_autocomplete_sync_id) {
        Res_partner_autocomplete_syncDTO res_partner_autocomplete_syncdto = new Res_partner_autocomplete_syncDTO();
		Res_partner_autocomplete_sync domain = new Res_partner_autocomplete_sync();
		res_partner_autocomplete_syncdto.setId(res_partner_autocomplete_sync_id);
		domain.setId(res_partner_autocomplete_sync_id);
        Boolean rst = res_partner_autocomplete_syncService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_partner_autocomplete_sync" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_autocomplete_syncs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_autocomplete_syncDTO> res_partner_autocomplete_syncdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_partner_autocomplete_sync" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_autocomplete_syncs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_partner_autocomplete_syncDTO> res_partner_autocomplete_syncdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_partner_autocomplete_sync" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_autocomplete_syncs/{res_partner_autocomplete_sync_id}")

    public ResponseEntity<Res_partner_autocomplete_syncDTO> update(@PathVariable("res_partner_autocomplete_sync_id") Integer res_partner_autocomplete_sync_id, @RequestBody Res_partner_autocomplete_syncDTO res_partner_autocomplete_syncdto) {
		Res_partner_autocomplete_sync domain = res_partner_autocomplete_syncdto.toDO();
        domain.setId(res_partner_autocomplete_sync_id);
		res_partner_autocomplete_syncService.update(domain);
		Res_partner_autocomplete_syncDTO dto = new Res_partner_autocomplete_syncDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_partner_autocomplete_sync" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_autocomplete_syncs/createBatch")
    public ResponseEntity<Boolean> createBatchRes_partner_autocomplete_sync(@RequestBody List<Res_partner_autocomplete_syncDTO> res_partner_autocomplete_syncdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_partner_autocomplete_sync" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_autocomplete_syncs/{res_partner_autocomplete_sync_id}")
    public ResponseEntity<Res_partner_autocomplete_syncDTO> get(@PathVariable("res_partner_autocomplete_sync_id") Integer res_partner_autocomplete_sync_id) {
        Res_partner_autocomplete_syncDTO dto = new Res_partner_autocomplete_syncDTO();
        Res_partner_autocomplete_sync domain = res_partner_autocomplete_syncService.get(res_partner_autocomplete_sync_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_partner_autocomplete_sync" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_autocomplete_syncs")

    public ResponseEntity<Res_partner_autocomplete_syncDTO> create(@RequestBody Res_partner_autocomplete_syncDTO res_partner_autocomplete_syncdto) {
        Res_partner_autocomplete_syncDTO dto = new Res_partner_autocomplete_syncDTO();
        Res_partner_autocomplete_sync domain = res_partner_autocomplete_syncdto.toDO();
		res_partner_autocomplete_syncService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_partner_autocomplete_sync" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_partner_autocomplete_syncs/fetchdefault")
	public ResponseEntity<Page<Res_partner_autocomplete_syncDTO>> fetchDefault(Res_partner_autocomplete_syncSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_autocomplete_syncDTO> list = new ArrayList<Res_partner_autocomplete_syncDTO>();
        
        Page<Res_partner_autocomplete_sync> domains = res_partner_autocomplete_syncService.searchDefault(context) ;
        for(Res_partner_autocomplete_sync res_partner_autocomplete_sync : domains.getContent()){
            Res_partner_autocomplete_syncDTO dto = new Res_partner_autocomplete_syncDTO();
            dto.fromDO(res_partner_autocomplete_sync);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
