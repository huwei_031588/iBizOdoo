package cn.ibizlab.odoo.service.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.base_module_uninstall.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Base_module_uninstallDTO]
 */
public class Base_module_uninstallDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MODULE_IDS]
     *
     */
    @Base_module_uninstallModule_idsDefault(info = "默认规则")
    private String module_ids;

    @JsonIgnore
    private boolean module_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Base_module_uninstallIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MODEL_IDS]
     *
     */
    @Base_module_uninstallModel_idsDefault(info = "默认规则")
    private String model_ids;

    @JsonIgnore
    private boolean model_idsDirtyFlag;

    /**
     * 属性 [SHOW_ALL]
     *
     */
    @Base_module_uninstallShow_allDefault(info = "默认规则")
    private String show_all;

    @JsonIgnore
    private boolean show_allDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Base_module_uninstall__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Base_module_uninstallWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Base_module_uninstallCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MODULE_ID]
     *
     */
    @Base_module_uninstallModule_idDefault(info = "默认规则")
    private Integer module_id;

    @JsonIgnore
    private boolean module_idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Base_module_uninstallDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Base_module_uninstallCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Base_module_uninstallWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Base_module_uninstallCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Base_module_uninstallWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [MODULE_IDS]
     */
    @JsonProperty("module_ids")
    public String getModule_ids(){
        return module_ids ;
    }

    /**
     * 设置 [MODULE_IDS]
     */
    @JsonProperty("module_ids")
    public void setModule_ids(String  module_ids){
        this.module_ids = module_ids ;
        this.module_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getModule_idsDirtyFlag(){
        return module_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MODEL_IDS]
     */
    @JsonProperty("model_ids")
    public String getModel_ids(){
        return model_ids ;
    }

    /**
     * 设置 [MODEL_IDS]
     */
    @JsonProperty("model_ids")
    public void setModel_ids(String  model_ids){
        this.model_ids = model_ids ;
        this.model_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getModel_idsDirtyFlag(){
        return model_idsDirtyFlag ;
    }

    /**
     * 获取 [SHOW_ALL]
     */
    @JsonProperty("show_all")
    public String getShow_all(){
        return show_all ;
    }

    /**
     * 设置 [SHOW_ALL]
     */
    @JsonProperty("show_all")
    public void setShow_all(String  show_all){
        this.show_all = show_all ;
        this.show_allDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_ALL]脏标记
     */
    @JsonIgnore
    public boolean getShow_allDirtyFlag(){
        return show_allDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ID]
     */
    @JsonProperty("module_id")
    public Integer getModule_id(){
        return module_id ;
    }

    /**
     * 设置 [MODULE_ID]
     */
    @JsonProperty("module_id")
    public void setModule_id(Integer  module_id){
        this.module_id = module_id ;
        this.module_idDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ID]脏标记
     */
    @JsonIgnore
    public boolean getModule_idDirtyFlag(){
        return module_idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Base_module_uninstall toDO() {
        Base_module_uninstall srfdomain = new Base_module_uninstall();
        if(getModule_idsDirtyFlag())
            srfdomain.setModule_ids(module_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getModel_idsDirtyFlag())
            srfdomain.setModel_ids(model_ids);
        if(getShow_allDirtyFlag())
            srfdomain.setShow_all(show_all);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getModule_idDirtyFlag())
            srfdomain.setModule_id(module_id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Base_module_uninstall srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getModule_idsDirtyFlag())
            this.setModule_ids(srfdomain.getModule_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getModel_idsDirtyFlag())
            this.setModel_ids(srfdomain.getModel_ids());
        if(srfdomain.getShow_allDirtyFlag())
            this.setShow_all(srfdomain.getShow_all());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getModule_idDirtyFlag())
            this.setModule_id(srfdomain.getModule_id());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Base_module_uninstallDTO> fromDOPage(List<Base_module_uninstall> poPage)   {
        if(poPage == null)
            return null;
        List<Base_module_uninstallDTO> dtos=new ArrayList<Base_module_uninstallDTO>();
        for(Base_module_uninstall domain : poPage) {
            Base_module_uninstallDTO dto = new Base_module_uninstallDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

