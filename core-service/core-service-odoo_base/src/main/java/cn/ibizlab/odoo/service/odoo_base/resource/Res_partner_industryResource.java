package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_partner_industryDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_industry;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_industryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_industrySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_partner_industry" })
@RestController
@RequestMapping("")
public class Res_partner_industryResource {

    @Autowired
    private IRes_partner_industryService res_partner_industryService;

    public IRes_partner_industryService getRes_partner_industryService() {
        return this.res_partner_industryService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_partner_industry" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_industries/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_partner_industryDTO> res_partner_industrydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_partner_industry" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_industries/{res_partner_industry_id}")

    public ResponseEntity<Res_partner_industryDTO> update(@PathVariable("res_partner_industry_id") Integer res_partner_industry_id, @RequestBody Res_partner_industryDTO res_partner_industrydto) {
		Res_partner_industry domain = res_partner_industrydto.toDO();
        domain.setId(res_partner_industry_id);
		res_partner_industryService.update(domain);
		Res_partner_industryDTO dto = new Res_partner_industryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_partner_industry" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_industries")

    public ResponseEntity<Res_partner_industryDTO> create(@RequestBody Res_partner_industryDTO res_partner_industrydto) {
        Res_partner_industryDTO dto = new Res_partner_industryDTO();
        Res_partner_industry domain = res_partner_industrydto.toDO();
		res_partner_industryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_partner_industry" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_industries/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_industryDTO> res_partner_industrydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_partner_industry" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_industries/{res_partner_industry_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_industry_id") Integer res_partner_industry_id) {
        Res_partner_industryDTO res_partner_industrydto = new Res_partner_industryDTO();
		Res_partner_industry domain = new Res_partner_industry();
		res_partner_industrydto.setId(res_partner_industry_id);
		domain.setId(res_partner_industry_id);
        Boolean rst = res_partner_industryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Res_partner_industry" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_industries/{res_partner_industry_id}")
    public ResponseEntity<Res_partner_industryDTO> get(@PathVariable("res_partner_industry_id") Integer res_partner_industry_id) {
        Res_partner_industryDTO dto = new Res_partner_industryDTO();
        Res_partner_industry domain = res_partner_industryService.get(res_partner_industry_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_partner_industry" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_industries/createBatch")
    public ResponseEntity<Boolean> createBatchRes_partner_industry(@RequestBody List<Res_partner_industryDTO> res_partner_industrydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_partner_industry" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_partner_industries/fetchdefault")
	public ResponseEntity<Page<Res_partner_industryDTO>> fetchDefault(Res_partner_industrySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_industryDTO> list = new ArrayList<Res_partner_industryDTO>();
        
        Page<Res_partner_industry> domains = res_partner_industryService.searchDefault(context) ;
        for(Res_partner_industry res_partner_industry : domains.getContent()){
            Res_partner_industryDTO dto = new Res_partner_industryDTO();
            dto.fromDO(res_partner_industry);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
