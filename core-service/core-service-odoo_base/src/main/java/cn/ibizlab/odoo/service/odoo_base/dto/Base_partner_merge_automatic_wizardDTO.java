package cn.ibizlab.odoo.service.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.base_partner_merge_automatic_wizard.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Base_partner_merge_automatic_wizardDTO]
 */
public class Base_partner_merge_automatic_wizardDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MAXIMUM_GROUP]
     *
     */
    @Base_partner_merge_automatic_wizardMaximum_groupDefault(info = "默认规则")
    private Integer maximum_group;

    @JsonIgnore
    private boolean maximum_groupDirtyFlag;

    /**
     * 属性 [GROUP_BY_PARENT_ID]
     *
     */
    @Base_partner_merge_automatic_wizardGroup_by_parent_idDefault(info = "默认规则")
    private String group_by_parent_id;

    @JsonIgnore
    private boolean group_by_parent_idDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Base_partner_merge_automatic_wizard__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Base_partner_merge_automatic_wizardCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [NUMBER_GROUP]
     *
     */
    @Base_partner_merge_automatic_wizardNumber_groupDefault(info = "默认规则")
    private Integer number_group;

    @JsonIgnore
    private boolean number_groupDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Base_partner_merge_automatic_wizardWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @Base_partner_merge_automatic_wizardLine_idsDefault(info = "默认规则")
    private String line_ids;

    @JsonIgnore
    private boolean line_idsDirtyFlag;

    /**
     * 属性 [GROUP_BY_VAT]
     *
     */
    @Base_partner_merge_automatic_wizardGroup_by_vatDefault(info = "默认规则")
    private String group_by_vat;

    @JsonIgnore
    private boolean group_by_vatDirtyFlag;

    /**
     * 属性 [EXCLUDE_CONTACT]
     *
     */
    @Base_partner_merge_automatic_wizardExclude_contactDefault(info = "默认规则")
    private String exclude_contact;

    @JsonIgnore
    private boolean exclude_contactDirtyFlag;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @Base_partner_merge_automatic_wizardPartner_idsDefault(info = "默认规则")
    private String partner_ids;

    @JsonIgnore
    private boolean partner_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Base_partner_merge_automatic_wizardIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [GROUP_BY_EMAIL]
     *
     */
    @Base_partner_merge_automatic_wizardGroup_by_emailDefault(info = "默认规则")
    private String group_by_email;

    @JsonIgnore
    private boolean group_by_emailDirtyFlag;

    /**
     * 属性 [GROUP_BY_IS_COMPANY]
     *
     */
    @Base_partner_merge_automatic_wizardGroup_by_is_companyDefault(info = "默认规则")
    private String group_by_is_company;

    @JsonIgnore
    private boolean group_by_is_companyDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Base_partner_merge_automatic_wizardStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Base_partner_merge_automatic_wizardDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [EXCLUDE_JOURNAL_ITEM]
     *
     */
    @Base_partner_merge_automatic_wizardExclude_journal_itemDefault(info = "默认规则")
    private String exclude_journal_item;

    @JsonIgnore
    private boolean exclude_journal_itemDirtyFlag;

    /**
     * 属性 [GROUP_BY_NAME]
     *
     */
    @Base_partner_merge_automatic_wizardGroup_by_nameDefault(info = "默认规则")
    private String group_by_name;

    @JsonIgnore
    private boolean group_by_nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Base_partner_merge_automatic_wizardCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Base_partner_merge_automatic_wizardWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [DST_PARTNER_ID_TEXT]
     *
     */
    @Base_partner_merge_automatic_wizardDst_partner_id_textDefault(info = "默认规则")
    private String dst_partner_id_text;

    @JsonIgnore
    private boolean dst_partner_id_textDirtyFlag;

    /**
     * 属性 [DST_PARTNER_ID]
     *
     */
    @Base_partner_merge_automatic_wizardDst_partner_idDefault(info = "默认规则")
    private Integer dst_partner_id;

    @JsonIgnore
    private boolean dst_partner_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Base_partner_merge_automatic_wizardWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CURRENT_LINE_ID]
     *
     */
    @Base_partner_merge_automatic_wizardCurrent_line_idDefault(info = "默认规则")
    private Integer current_line_id;

    @JsonIgnore
    private boolean current_line_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Base_partner_merge_automatic_wizardCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [MAXIMUM_GROUP]
     */
    @JsonProperty("maximum_group")
    public Integer getMaximum_group(){
        return maximum_group ;
    }

    /**
     * 设置 [MAXIMUM_GROUP]
     */
    @JsonProperty("maximum_group")
    public void setMaximum_group(Integer  maximum_group){
        this.maximum_group = maximum_group ;
        this.maximum_groupDirtyFlag = true ;
    }

    /**
     * 获取 [MAXIMUM_GROUP]脏标记
     */
    @JsonIgnore
    public boolean getMaximum_groupDirtyFlag(){
        return maximum_groupDirtyFlag ;
    }

    /**
     * 获取 [GROUP_BY_PARENT_ID]
     */
    @JsonProperty("group_by_parent_id")
    public String getGroup_by_parent_id(){
        return group_by_parent_id ;
    }

    /**
     * 设置 [GROUP_BY_PARENT_ID]
     */
    @JsonProperty("group_by_parent_id")
    public void setGroup_by_parent_id(String  group_by_parent_id){
        this.group_by_parent_id = group_by_parent_id ;
        this.group_by_parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_BY_PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_parent_idDirtyFlag(){
        return group_by_parent_idDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [NUMBER_GROUP]
     */
    @JsonProperty("number_group")
    public Integer getNumber_group(){
        return number_group ;
    }

    /**
     * 设置 [NUMBER_GROUP]
     */
    @JsonProperty("number_group")
    public void setNumber_group(Integer  number_group){
        this.number_group = number_group ;
        this.number_groupDirtyFlag = true ;
    }

    /**
     * 获取 [NUMBER_GROUP]脏标记
     */
    @JsonIgnore
    public boolean getNumber_groupDirtyFlag(){
        return number_groupDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [LINE_IDS]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return line_ids ;
    }

    /**
     * 设置 [LINE_IDS]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return line_idsDirtyFlag ;
    }

    /**
     * 获取 [GROUP_BY_VAT]
     */
    @JsonProperty("group_by_vat")
    public String getGroup_by_vat(){
        return group_by_vat ;
    }

    /**
     * 设置 [GROUP_BY_VAT]
     */
    @JsonProperty("group_by_vat")
    public void setGroup_by_vat(String  group_by_vat){
        this.group_by_vat = group_by_vat ;
        this.group_by_vatDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_BY_VAT]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_vatDirtyFlag(){
        return group_by_vatDirtyFlag ;
    }

    /**
     * 获取 [EXCLUDE_CONTACT]
     */
    @JsonProperty("exclude_contact")
    public String getExclude_contact(){
        return exclude_contact ;
    }

    /**
     * 设置 [EXCLUDE_CONTACT]
     */
    @JsonProperty("exclude_contact")
    public void setExclude_contact(String  exclude_contact){
        this.exclude_contact = exclude_contact ;
        this.exclude_contactDirtyFlag = true ;
    }

    /**
     * 获取 [EXCLUDE_CONTACT]脏标记
     */
    @JsonIgnore
    public boolean getExclude_contactDirtyFlag(){
        return exclude_contactDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_IDS]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return partner_ids ;
    }

    /**
     * 设置 [PARTNER_IDS]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return partner_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [GROUP_BY_EMAIL]
     */
    @JsonProperty("group_by_email")
    public String getGroup_by_email(){
        return group_by_email ;
    }

    /**
     * 设置 [GROUP_BY_EMAIL]
     */
    @JsonProperty("group_by_email")
    public void setGroup_by_email(String  group_by_email){
        this.group_by_email = group_by_email ;
        this.group_by_emailDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_BY_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_emailDirtyFlag(){
        return group_by_emailDirtyFlag ;
    }

    /**
     * 获取 [GROUP_BY_IS_COMPANY]
     */
    @JsonProperty("group_by_is_company")
    public String getGroup_by_is_company(){
        return group_by_is_company ;
    }

    /**
     * 设置 [GROUP_BY_IS_COMPANY]
     */
    @JsonProperty("group_by_is_company")
    public void setGroup_by_is_company(String  group_by_is_company){
        this.group_by_is_company = group_by_is_company ;
        this.group_by_is_companyDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_BY_IS_COMPANY]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_is_companyDirtyFlag(){
        return group_by_is_companyDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [EXCLUDE_JOURNAL_ITEM]
     */
    @JsonProperty("exclude_journal_item")
    public String getExclude_journal_item(){
        return exclude_journal_item ;
    }

    /**
     * 设置 [EXCLUDE_JOURNAL_ITEM]
     */
    @JsonProperty("exclude_journal_item")
    public void setExclude_journal_item(String  exclude_journal_item){
        this.exclude_journal_item = exclude_journal_item ;
        this.exclude_journal_itemDirtyFlag = true ;
    }

    /**
     * 获取 [EXCLUDE_JOURNAL_ITEM]脏标记
     */
    @JsonIgnore
    public boolean getExclude_journal_itemDirtyFlag(){
        return exclude_journal_itemDirtyFlag ;
    }

    /**
     * 获取 [GROUP_BY_NAME]
     */
    @JsonProperty("group_by_name")
    public String getGroup_by_name(){
        return group_by_name ;
    }

    /**
     * 设置 [GROUP_BY_NAME]
     */
    @JsonProperty("group_by_name")
    public void setGroup_by_name(String  group_by_name){
        this.group_by_name = group_by_name ;
        this.group_by_nameDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_BY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getGroup_by_nameDirtyFlag(){
        return group_by_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [DST_PARTNER_ID_TEXT]
     */
    @JsonProperty("dst_partner_id_text")
    public String getDst_partner_id_text(){
        return dst_partner_id_text ;
    }

    /**
     * 设置 [DST_PARTNER_ID_TEXT]
     */
    @JsonProperty("dst_partner_id_text")
    public void setDst_partner_id_text(String  dst_partner_id_text){
        this.dst_partner_id_text = dst_partner_id_text ;
        this.dst_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DST_PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDst_partner_id_textDirtyFlag(){
        return dst_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [DST_PARTNER_ID]
     */
    @JsonProperty("dst_partner_id")
    public Integer getDst_partner_id(){
        return dst_partner_id ;
    }

    /**
     * 设置 [DST_PARTNER_ID]
     */
    @JsonProperty("dst_partner_id")
    public void setDst_partner_id(Integer  dst_partner_id){
        this.dst_partner_id = dst_partner_id ;
        this.dst_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [DST_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getDst_partner_idDirtyFlag(){
        return dst_partner_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CURRENT_LINE_ID]
     */
    @JsonProperty("current_line_id")
    public Integer getCurrent_line_id(){
        return current_line_id ;
    }

    /**
     * 设置 [CURRENT_LINE_ID]
     */
    @JsonProperty("current_line_id")
    public void setCurrent_line_id(Integer  current_line_id){
        this.current_line_id = current_line_id ;
        this.current_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENT_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrent_line_idDirtyFlag(){
        return current_line_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Base_partner_merge_automatic_wizard toDO() {
        Base_partner_merge_automatic_wizard srfdomain = new Base_partner_merge_automatic_wizard();
        if(getMaximum_groupDirtyFlag())
            srfdomain.setMaximum_group(maximum_group);
        if(getGroup_by_parent_idDirtyFlag())
            srfdomain.setGroup_by_parent_id(group_by_parent_id);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getNumber_groupDirtyFlag())
            srfdomain.setNumber_group(number_group);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getLine_idsDirtyFlag())
            srfdomain.setLine_ids(line_ids);
        if(getGroup_by_vatDirtyFlag())
            srfdomain.setGroup_by_vat(group_by_vat);
        if(getExclude_contactDirtyFlag())
            srfdomain.setExclude_contact(exclude_contact);
        if(getPartner_idsDirtyFlag())
            srfdomain.setPartner_ids(partner_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getGroup_by_emailDirtyFlag())
            srfdomain.setGroup_by_email(group_by_email);
        if(getGroup_by_is_companyDirtyFlag())
            srfdomain.setGroup_by_is_company(group_by_is_company);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getExclude_journal_itemDirtyFlag())
            srfdomain.setExclude_journal_item(exclude_journal_item);
        if(getGroup_by_nameDirtyFlag())
            srfdomain.setGroup_by_name(group_by_name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getDst_partner_id_textDirtyFlag())
            srfdomain.setDst_partner_id_text(dst_partner_id_text);
        if(getDst_partner_idDirtyFlag())
            srfdomain.setDst_partner_id(dst_partner_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCurrent_line_idDirtyFlag())
            srfdomain.setCurrent_line_id(current_line_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Base_partner_merge_automatic_wizard srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMaximum_groupDirtyFlag())
            this.setMaximum_group(srfdomain.getMaximum_group());
        if(srfdomain.getGroup_by_parent_idDirtyFlag())
            this.setGroup_by_parent_id(srfdomain.getGroup_by_parent_id());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getNumber_groupDirtyFlag())
            this.setNumber_group(srfdomain.getNumber_group());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getLine_idsDirtyFlag())
            this.setLine_ids(srfdomain.getLine_ids());
        if(srfdomain.getGroup_by_vatDirtyFlag())
            this.setGroup_by_vat(srfdomain.getGroup_by_vat());
        if(srfdomain.getExclude_contactDirtyFlag())
            this.setExclude_contact(srfdomain.getExclude_contact());
        if(srfdomain.getPartner_idsDirtyFlag())
            this.setPartner_ids(srfdomain.getPartner_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getGroup_by_emailDirtyFlag())
            this.setGroup_by_email(srfdomain.getGroup_by_email());
        if(srfdomain.getGroup_by_is_companyDirtyFlag())
            this.setGroup_by_is_company(srfdomain.getGroup_by_is_company());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getExclude_journal_itemDirtyFlag())
            this.setExclude_journal_item(srfdomain.getExclude_journal_item());
        if(srfdomain.getGroup_by_nameDirtyFlag())
            this.setGroup_by_name(srfdomain.getGroup_by_name());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getDst_partner_id_textDirtyFlag())
            this.setDst_partner_id_text(srfdomain.getDst_partner_id_text());
        if(srfdomain.getDst_partner_idDirtyFlag())
            this.setDst_partner_id(srfdomain.getDst_partner_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCurrent_line_idDirtyFlag())
            this.setCurrent_line_id(srfdomain.getCurrent_line_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Base_partner_merge_automatic_wizardDTO> fromDOPage(List<Base_partner_merge_automatic_wizard> poPage)   {
        if(poPage == null)
            return null;
        List<Base_partner_merge_automatic_wizardDTO> dtos=new ArrayList<Base_partner_merge_automatic_wizardDTO>();
        for(Base_partner_merge_automatic_wizard domain : poPage) {
            Base_partner_merge_automatic_wizardDTO dto = new Base_partner_merge_automatic_wizardDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

