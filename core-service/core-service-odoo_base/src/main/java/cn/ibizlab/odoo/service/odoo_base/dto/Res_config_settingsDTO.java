package cn.ibizlab.odoo.service.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_config_settings.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_settings;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Res_config_settingsDTO]
 */
public class Res_config_settingsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_QIF]
     *
     */
    @Res_config_settingsModule_account_bank_statement_import_qifDefault(info = "默认规则")
    private String module_account_bank_statement_import_qif;

    @JsonIgnore
    private boolean module_account_bank_statement_import_qifDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_OFX]
     *
     */
    @Res_config_settingsModule_account_bank_statement_import_ofxDefault(info = "默认规则")
    private String module_account_bank_statement_import_ofx;

    @JsonIgnore
    private boolean module_account_bank_statement_import_ofxDirtyFlag;

    /**
     * 属性 [HAS_GOOGLE_MAPS]
     *
     */
    @Res_config_settingsHas_google_mapsDefault(info = "默认规则")
    private String has_google_maps;

    @JsonIgnore
    private boolean has_google_mapsDirtyFlag;

    /**
     * 属性 [MODULE_L10N_EU_SERVICE]
     *
     */
    @Res_config_settingsModule_l10n_eu_serviceDefault(info = "默认规则")
    private String module_l10n_eu_service;

    @JsonIgnore
    private boolean module_l10n_eu_serviceDirtyFlag;

    /**
     * 属性 [CDN_ACTIVATED]
     *
     */
    @Res_config_settingsCdn_activatedDefault(info = "默认规则")
    private String cdn_activated;

    @JsonIgnore
    private boolean cdn_activatedDirtyFlag;

    /**
     * 属性 [WEBSITE_DEFAULT_LANG_CODE]
     *
     */
    @Res_config_settingsWebsite_default_lang_codeDefault(info = "默认规则")
    private String website_default_lang_code;

    @JsonIgnore
    private boolean website_default_lang_codeDirtyFlag;

    /**
     * 属性 [AUTH_SIGNUP_RESET_PASSWORD]
     *
     */
    @Res_config_settingsAuth_signup_reset_passwordDefault(info = "默认规则")
    private String auth_signup_reset_password;

    @JsonIgnore
    private boolean auth_signup_reset_passwordDirtyFlag;

    /**
     * 属性 [WEBSITE_COUNTRY_GROUP_IDS]
     *
     */
    @Res_config_settingsWebsite_country_group_idsDefault(info = "默认规则")
    private String website_country_group_ids;

    @JsonIgnore
    private boolean website_country_group_idsDirtyFlag;

    /**
     * 属性 [SOCIAL_INSTAGRAM]
     *
     */
    @Res_config_settingsSocial_instagramDefault(info = "默认规则")
    private String social_instagram;

    @JsonIgnore
    private boolean social_instagramDirtyFlag;

    /**
     * 属性 [MODULE_PURCHASE_REQUISITION]
     *
     */
    @Res_config_settingsModule_purchase_requisitionDefault(info = "默认规则")
    private String module_purchase_requisition;

    @JsonIgnore
    private boolean module_purchase_requisitionDirtyFlag;

    /**
     * 属性 [MODULE_DELIVERY_EASYPOST]
     *
     */
    @Res_config_settingsModule_delivery_easypostDefault(info = "默认规则")
    private String module_delivery_easypost;

    @JsonIgnore
    private boolean module_delivery_easypostDirtyFlag;

    /**
     * 属性 [GROUP_DISCOUNT_PER_SO_LINE]
     *
     */
    @Res_config_settingsGroup_discount_per_so_lineDefault(info = "默认规则")
    private String group_discount_per_so_line;

    @JsonIgnore
    private boolean group_discount_per_so_lineDirtyFlag;

    /**
     * 属性 [AUTOMATIC_INVOICE]
     *
     */
    @Res_config_settingsAutomatic_invoiceDefault(info = "默认规则")
    private String automatic_invoice;

    @JsonIgnore
    private boolean automatic_invoiceDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_PLAID]
     *
     */
    @Res_config_settingsModule_account_plaidDefault(info = "默认规则")
    private String module_account_plaid;

    @JsonIgnore
    private boolean module_account_plaidDirtyFlag;

    /**
     * 属性 [GOOGLE_MANAGEMENT_CLIENT_ID]
     *
     */
    @Res_config_settingsGoogle_management_client_idDefault(info = "默认规则")
    private String google_management_client_id;

    @JsonIgnore
    private boolean google_management_client_idDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Res_config_settingsIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_CHECK_PRINTING]
     *
     */
    @Res_config_settingsModule_account_check_printingDefault(info = "默认规则")
    private String module_account_check_printing;

    @JsonIgnore
    private boolean module_account_check_printingDirtyFlag;

    /**
     * 属性 [MODULE_PARTNER_AUTOCOMPLETE]
     *
     */
    @Res_config_settingsModule_partner_autocompleteDefault(info = "默认规则")
    private String module_partner_autocomplete;

    @JsonIgnore
    private boolean module_partner_autocompleteDirtyFlag;

    /**
     * 属性 [LANGUAGE_COUNT]
     *
     */
    @Res_config_settingsLanguage_countDefault(info = "默认规则")
    private Integer language_count;

    @JsonIgnore
    private boolean language_countDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_LINKS]
     *
     */
    @Res_config_settingsModule_website_linksDefault(info = "默认规则")
    private String module_website_links;

    @JsonIgnore
    private boolean module_website_linksDirtyFlag;

    /**
     * 属性 [WEBSITE_FORM_ENABLE_METADATA]
     *
     */
    @Res_config_settingsWebsite_form_enable_metadataDefault(info = "默认规则")
    private String website_form_enable_metadata;

    @JsonIgnore
    private boolean website_form_enable_metadataDirtyFlag;

    /**
     * 属性 [HAS_SOCIAL_NETWORK]
     *
     */
    @Res_config_settingsHas_social_networkDefault(info = "默认规则")
    private String has_social_network;

    @JsonIgnore
    private boolean has_social_networkDirtyFlag;

    /**
     * 属性 [GROUP_SALE_DELIVERY_ADDRESS]
     *
     */
    @Res_config_settingsGroup_sale_delivery_addressDefault(info = "默认规则")
    private String group_sale_delivery_address;

    @JsonIgnore
    private boolean group_sale_delivery_addressDirtyFlag;

    /**
     * 属性 [GOOGLE_ANALYTICS_KEY]
     *
     */
    @Res_config_settingsGoogle_analytics_keyDefault(info = "默认规则")
    private String google_analytics_key;

    @JsonIgnore
    private boolean google_analytics_keyDirtyFlag;

    /**
     * 属性 [MODULE_AUTH_LDAP]
     *
     */
    @Res_config_settingsModule_auth_ldapDefault(info = "默认规则")
    private String module_auth_ldap;

    @JsonIgnore
    private boolean module_auth_ldapDirtyFlag;

    /**
     * 属性 [SPECIFIC_USER_ACCOUNT]
     *
     */
    @Res_config_settingsSpecific_user_accountDefault(info = "默认规则")
    private String specific_user_account;

    @JsonIgnore
    private boolean specific_user_accountDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_HR_RECRUITMENT]
     *
     */
    @Res_config_settingsModule_website_hr_recruitmentDefault(info = "默认规则")
    private String module_website_hr_recruitment;

    @JsonIgnore
    private boolean module_website_hr_recruitmentDirtyFlag;

    /**
     * 属性 [MODULE_PROJECT_FORECAST]
     *
     */
    @Res_config_settingsModule_project_forecastDefault(info = "默认规则")
    private String module_project_forecast;

    @JsonIgnore
    private boolean module_project_forecastDirtyFlag;

    /**
     * 属性 [GROUP_STOCK_TRACKING_OWNER]
     *
     */
    @Res_config_settingsGroup_stock_tracking_ownerDefault(info = "默认规则")
    private String group_stock_tracking_owner;

    @JsonIgnore
    private boolean group_stock_tracking_ownerDirtyFlag;

    /**
     * 属性 [MODULE_GOOGLE_CALENDAR]
     *
     */
    @Res_config_settingsModule_google_calendarDefault(info = "默认规则")
    private String module_google_calendar;

    @JsonIgnore
    private boolean module_google_calendarDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT]
     *
     */
    @Res_config_settingsModule_accountDefault(info = "默认规则")
    private String module_account;

    @JsonIgnore
    private boolean module_accountDirtyFlag;

    /**
     * 属性 [MODULE_GOOGLE_DRIVE]
     *
     */
    @Res_config_settingsModule_google_driveDefault(info = "默认规则")
    private String module_google_drive;

    @JsonIgnore
    private boolean module_google_driveDirtyFlag;

    /**
     * 属性 [POS_PRICELIST_SETTING]
     *
     */
    @Res_config_settingsPos_pricelist_settingDefault(info = "默认规则")
    private String pos_pricelist_setting;

    @JsonIgnore
    private boolean pos_pricelist_settingDirtyFlag;

    /**
     * 属性 [COMPANY_SHARE_PARTNER]
     *
     */
    @Res_config_settingsCompany_share_partnerDefault(info = "默认规则")
    private String company_share_partner;

    @JsonIgnore
    private boolean company_share_partnerDirtyFlag;

    /**
     * 属性 [MODULE_CURRENCY_RATE_LIVE]
     *
     */
    @Res_config_settingsModule_currency_rate_liveDefault(info = "默认规则")
    private String module_currency_rate_live;

    @JsonIgnore
    private boolean module_currency_rate_liveDirtyFlag;

    /**
     * 属性 [GROUP_PROFORMA_SALES]
     *
     */
    @Res_config_settingsGroup_proforma_salesDefault(info = "默认规则")
    private String group_proforma_sales;

    @JsonIgnore
    private boolean group_proforma_salesDirtyFlag;

    /**
     * 属性 [MODULE_DELIVERY_FEDEX]
     *
     */
    @Res_config_settingsModule_delivery_fedexDefault(info = "默认规则")
    private String module_delivery_fedex;

    @JsonIgnore
    private boolean module_delivery_fedexDirtyFlag;

    /**
     * 属性 [MODULE_PRODUCT_EMAIL_TEMPLATE]
     *
     */
    @Res_config_settingsModule_product_email_templateDefault(info = "默认规则")
    private String module_product_email_template;

    @JsonIgnore
    private boolean module_product_email_templateDirtyFlag;

    /**
     * 属性 [SHOW_EFFECT]
     *
     */
    @Res_config_settingsShow_effectDefault(info = "默认规则")
    private String show_effect;

    @JsonIgnore
    private boolean show_effectDirtyFlag;

    /**
     * 属性 [DEFAULT_PICKING_POLICY]
     *
     */
    @Res_config_settingsDefault_picking_policyDefault(info = "默认规则")
    private String default_picking_policy;

    @JsonIgnore
    private boolean default_picking_policyDirtyFlag;

    /**
     * 属性 [SOCIAL_YOUTUBE]
     *
     */
    @Res_config_settingsSocial_youtubeDefault(info = "默认规则")
    private String social_youtube;

    @JsonIgnore
    private boolean social_youtubeDirtyFlag;

    /**
     * 属性 [WEBSITE_COMPANY_ID]
     *
     */
    @Res_config_settingsWebsite_company_idDefault(info = "默认规则")
    private Integer website_company_id;

    @JsonIgnore
    private boolean website_company_idDirtyFlag;

    /**
     * 属性 [MODULE_MRP_BYPRODUCT]
     *
     */
    @Res_config_settingsModule_mrp_byproductDefault(info = "默认规则")
    private String module_mrp_byproduct;

    @JsonIgnore
    private boolean module_mrp_byproductDirtyFlag;

    /**
     * 属性 [MODULE_DELIVERY_USPS]
     *
     */
    @Res_config_settingsModule_delivery_uspsDefault(info = "默认规则")
    private String module_delivery_usps;

    @JsonIgnore
    private boolean module_delivery_uspsDirtyFlag;

    /**
     * 属性 [MODULE_DELIVERY_DHL]
     *
     */
    @Res_config_settingsModule_delivery_dhlDefault(info = "默认规则")
    private String module_delivery_dhl;

    @JsonIgnore
    private boolean module_delivery_dhlDirtyFlag;

    /**
     * 属性 [GROUP_PROJECT_RATING]
     *
     */
    @Res_config_settingsGroup_project_ratingDefault(info = "默认规则")
    private String group_project_rating;

    @JsonIgnore
    private boolean group_project_ratingDirtyFlag;

    /**
     * 属性 [GOOGLE_MAPS_API_KEY]
     *
     */
    @Res_config_settingsGoogle_maps_api_keyDefault(info = "默认规则")
    private String google_maps_api_key;

    @JsonIgnore
    private boolean google_maps_api_keyDirtyFlag;

    /**
     * 属性 [GROUP_USE_LEAD]
     *
     */
    @Res_config_settingsGroup_use_leadDefault(info = "默认规则")
    private String group_use_lead;

    @JsonIgnore
    private boolean group_use_leadDirtyFlag;

    /**
     * 属性 [GROUP_STOCK_TRACKING_LOT]
     *
     */
    @Res_config_settingsGroup_stock_tracking_lotDefault(info = "默认规则")
    private String group_stock_tracking_lot;

    @JsonIgnore
    private boolean group_stock_tracking_lotDirtyFlag;

    /**
     * 属性 [GROUP_STOCK_ADV_LOCATION]
     *
     */
    @Res_config_settingsGroup_stock_adv_locationDefault(info = "默认规则")
    private String group_stock_adv_location;

    @JsonIgnore
    private boolean group_stock_adv_locationDirtyFlag;

    /**
     * 属性 [POS_SALES_PRICE]
     *
     */
    @Res_config_settingsPos_sales_priceDefault(info = "默认规则")
    private String pos_sales_price;

    @JsonIgnore
    private boolean pos_sales_priceDirtyFlag;

    /**
     * 属性 [HAS_CHART_OF_ACCOUNTS]
     *
     */
    @Res_config_settingsHas_chart_of_accountsDefault(info = "默认规则")
    private String has_chart_of_accounts;

    @JsonIgnore
    private boolean has_chart_of_accountsDirtyFlag;

    /**
     * 属性 [MULTI_SALES_PRICE_METHOD]
     *
     */
    @Res_config_settingsMulti_sales_price_methodDefault(info = "默认规则")
    private String multi_sales_price_method;

    @JsonIgnore
    private boolean multi_sales_price_methodDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Res_config_settingsWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MODULE_PROCUREMENT_JIT]
     *
     */
    @Res_config_settingsModule_procurement_jitDefault(info = "默认规则")
    private String module_procurement_jit;

    @JsonIgnore
    private boolean module_procurement_jitDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_ASSET]
     *
     */
    @Res_config_settingsModule_account_assetDefault(info = "默认规则")
    private String module_account_asset;

    @JsonIgnore
    private boolean module_account_assetDirtyFlag;

    /**
     * 属性 [USE_MAILGATEWAY]
     *
     */
    @Res_config_settingsUse_mailgatewayDefault(info = "默认规则")
    private String use_mailgateway;

    @JsonIgnore
    private boolean use_mailgatewayDirtyFlag;

    /**
     * 属性 [GROUP_SALE_PRICELIST]
     *
     */
    @Res_config_settingsGroup_sale_pricelistDefault(info = "默认规则")
    private String group_sale_pricelist;

    @JsonIgnore
    private boolean group_sale_pricelistDirtyFlag;

    /**
     * 属性 [CRM_DEFAULT_TEAM_ID]
     *
     */
    @Res_config_settingsCrm_default_team_idDefault(info = "默认规则")
    private Integer crm_default_team_id;

    @JsonIgnore
    private boolean crm_default_team_idDirtyFlag;

    /**
     * 属性 [GROUP_STOCK_MULTI_LOCATIONS]
     *
     */
    @Res_config_settingsGroup_stock_multi_locationsDefault(info = "默认规则")
    private String group_stock_multi_locations;

    @JsonIgnore
    private boolean group_stock_multi_locationsDirtyFlag;

    /**
     * 属性 [USE_MANUFACTURING_LEAD]
     *
     */
    @Res_config_settingsUse_manufacturing_leadDefault(info = "默认规则")
    private String use_manufacturing_lead;

    @JsonIgnore
    private boolean use_manufacturing_leadDirtyFlag;

    /**
     * 属性 [MODULE_GOOGLE_SPREADSHEET]
     *
     */
    @Res_config_settingsModule_google_spreadsheetDefault(info = "默认规则")
    private String module_google_spreadsheet;

    @JsonIgnore
    private boolean module_google_spreadsheetDirtyFlag;

    /**
     * 属性 [SHOW_LINE_SUBTOTALS_TAX_SELECTION]
     *
     */
    @Res_config_settingsShow_line_subtotals_tax_selectionDefault(info = "默认规则")
    private String show_line_subtotals_tax_selection;

    @JsonIgnore
    private boolean show_line_subtotals_tax_selectionDirtyFlag;

    /**
     * 属性 [LANGUAGE_IDS]
     *
     */
    @Res_config_settingsLanguage_idsDefault(info = "默认规则")
    private String language_ids;

    @JsonIgnore
    private boolean language_idsDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_SALE_DELIVERY]
     *
     */
    @Res_config_settingsModule_website_sale_deliveryDefault(info = "默认规则")
    private String module_website_sale_delivery;

    @JsonIgnore
    private boolean module_website_sale_deliveryDirtyFlag;

    /**
     * 属性 [MODULE_MRP_MPS]
     *
     */
    @Res_config_settingsModule_mrp_mpsDefault(info = "默认规则")
    private String module_mrp_mps;

    @JsonIgnore
    private boolean module_mrp_mpsDirtyFlag;

    /**
     * 属性 [PARTNER_AUTOCOMPLETE_INSUFFICIENT_CREDIT]
     *
     */
    @Res_config_settingsPartner_autocomplete_insufficient_creditDefault(info = "默认规则")
    private String partner_autocomplete_insufficient_credit;

    @JsonIgnore
    private boolean partner_autocomplete_insufficient_creditDirtyFlag;

    /**
     * 属性 [MODULE_HR_ORG_CHART]
     *
     */
    @Res_config_settingsModule_hr_org_chartDefault(info = "默认规则")
    private String module_hr_org_chart;

    @JsonIgnore
    private boolean module_hr_org_chartDirtyFlag;

    /**
     * 属性 [MODULE_PRODUCT_EXPIRY]
     *
     */
    @Res_config_settingsModule_product_expiryDefault(info = "默认规则")
    private String module_product_expiry;

    @JsonIgnore
    private boolean module_product_expiryDirtyFlag;

    /**
     * 属性 [MODULE_DELIVERY_BPOST]
     *
     */
    @Res_config_settingsModule_delivery_bpostDefault(info = "默认规则")
    private String module_delivery_bpost;

    @JsonIgnore
    private boolean module_delivery_bpostDirtyFlag;

    /**
     * 属性 [MODULE_STOCK_BARCODE]
     *
     */
    @Res_config_settingsModule_stock_barcodeDefault(info = "默认规则")
    private String module_stock_barcode;

    @JsonIgnore
    private boolean module_stock_barcodeDirtyFlag;

    /**
     * 属性 [SOCIAL_GITHUB]
     *
     */
    @Res_config_settingsSocial_githubDefault(info = "默认规则")
    private String social_github;

    @JsonIgnore
    private boolean social_githubDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_INTRASTAT]
     *
     */
    @Res_config_settingsModule_account_intrastatDefault(info = "默认规则")
    private String module_account_intrastat;

    @JsonIgnore
    private boolean module_account_intrastatDirtyFlag;

    /**
     * 属性 [AUTO_DONE_SETTING]
     *
     */
    @Res_config_settingsAuto_done_settingDefault(info = "默认规则")
    private String auto_done_setting;

    @JsonIgnore
    private boolean auto_done_settingDirtyFlag;

    /**
     * 属性 [AUTH_SIGNUP_UNINVITED]
     *
     */
    @Res_config_settingsAuth_signup_uninvitedDefault(info = "默认规则")
    private String auth_signup_uninvited;

    @JsonIgnore
    private boolean auth_signup_uninvitedDirtyFlag;

    /**
     * 属性 [COMPANY_SHARE_PRODUCT]
     *
     */
    @Res_config_settingsCompany_share_productDefault(info = "默认规则")
    private String company_share_product;

    @JsonIgnore
    private boolean company_share_productDirtyFlag;

    /**
     * 属性 [GROUP_SALE_ORDER_TEMPLATE]
     *
     */
    @Res_config_settingsGroup_sale_order_templateDefault(info = "默认规则")
    private String group_sale_order_template;

    @JsonIgnore
    private boolean group_sale_order_templateDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_SEPA_DIRECT_DEBIT]
     *
     */
    @Res_config_settingsModule_account_sepa_direct_debitDefault(info = "默认规则")
    private String module_account_sepa_direct_debit;

    @JsonIgnore
    private boolean module_account_sepa_direct_debitDirtyFlag;

    /**
     * 属性 [USE_QUOTATION_VALIDITY_DAYS]
     *
     */
    @Res_config_settingsUse_quotation_validity_daysDefault(info = "默认规则")
    private String use_quotation_validity_days;

    @JsonIgnore
    private boolean use_quotation_validity_daysDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_REPORTS_FOLLOWUP]
     *
     */
    @Res_config_settingsModule_account_reports_followupDefault(info = "默认规则")
    private String module_account_reports_followup;

    @JsonIgnore
    private boolean module_account_reports_followupDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_BATCH_PAYMENT]
     *
     */
    @Res_config_settingsModule_account_batch_paymentDefault(info = "默认规则")
    private String module_account_batch_payment;

    @JsonIgnore
    private boolean module_account_batch_paymentDirtyFlag;

    /**
     * 属性 [SOCIAL_TWITTER]
     *
     */
    @Res_config_settingsSocial_twitterDefault(info = "默认规则")
    private String social_twitter;

    @JsonIgnore
    private boolean social_twitterDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_BUDGET]
     *
     */
    @Res_config_settingsModule_account_budgetDefault(info = "默认规则")
    private String module_account_budget;

    @JsonIgnore
    private boolean module_account_budgetDirtyFlag;

    /**
     * 属性 [GROUP_MRP_ROUTINGS]
     *
     */
    @Res_config_settingsGroup_mrp_routingsDefault(info = "默认规则")
    private String group_mrp_routings;

    @JsonIgnore
    private boolean group_mrp_routingsDirtyFlag;

    /**
     * 属性 [GROUP_CASH_ROUNDING]
     *
     */
    @Res_config_settingsGroup_cash_roundingDefault(info = "默认规则")
    private String group_cash_rounding;

    @JsonIgnore
    private boolean group_cash_roundingDirtyFlag;

    /**
     * 属性 [MODULE_STOCK_LANDED_COSTS]
     *
     */
    @Res_config_settingsModule_stock_landed_costsDefault(info = "默认规则")
    private String module_stock_landed_costs;

    @JsonIgnore
    private boolean module_stock_landed_costsDirtyFlag;

    /**
     * 属性 [WEBSITE_NAME]
     *
     */
    @Res_config_settingsWebsite_nameDefault(info = "默认规则")
    private String website_name;

    @JsonIgnore
    private boolean website_nameDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_SALE_STOCK]
     *
     */
    @Res_config_settingsModule_website_sale_stockDefault(info = "默认规则")
    private String module_website_sale_stock;

    @JsonIgnore
    private boolean module_website_sale_stockDirtyFlag;

    /**
     * 属性 [GROUP_WEBSITE_POPUP_ON_EXIT]
     *
     */
    @Res_config_settingsGroup_website_popup_on_exitDefault(info = "默认规则")
    private String group_website_popup_on_exit;

    @JsonIgnore
    private boolean group_website_popup_on_exitDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_EVENT_TRACK]
     *
     */
    @Res_config_settingsModule_website_event_trackDefault(info = "默认规则")
    private String module_website_event_track;

    @JsonIgnore
    private boolean module_website_event_trackDirtyFlag;

    /**
     * 属性 [GROUP_MANAGE_VENDOR_PRICE]
     *
     */
    @Res_config_settingsGroup_manage_vendor_priceDefault(info = "默认规则")
    private String group_manage_vendor_price;

    @JsonIgnore
    private boolean group_manage_vendor_priceDirtyFlag;

    /**
     * 属性 [MODULE_DELIVERY]
     *
     */
    @Res_config_settingsModule_deliveryDefault(info = "默认规则")
    private String module_delivery;

    @JsonIgnore
    private boolean module_deliveryDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_INVOICE_EXTRACT]
     *
     */
    @Res_config_settingsModule_account_invoice_extractDefault(info = "默认规则")
    private String module_account_invoice_extract;

    @JsonIgnore
    private boolean module_account_invoice_extractDirtyFlag;

    /**
     * 属性 [CHANNEL_ID]
     *
     */
    @Res_config_settingsChannel_idDefault(info = "默认规则")
    private Integer channel_id;

    @JsonIgnore
    private boolean channel_idDirtyFlag;

    /**
     * 属性 [GROUP_WARNING_SALE]
     *
     */
    @Res_config_settingsGroup_warning_saleDefault(info = "默认规则")
    private String group_warning_sale;

    @JsonIgnore
    private boolean group_warning_saleDirtyFlag;

    /**
     * 属性 [CDN_URL]
     *
     */
    @Res_config_settingsCdn_urlDefault(info = "默认规则")
    private String cdn_url;

    @JsonIgnore
    private boolean cdn_urlDirtyFlag;

    /**
     * 属性 [MODULE_EVENT_BARCODE]
     *
     */
    @Res_config_settingsModule_event_barcodeDefault(info = "默认规则")
    private String module_event_barcode;

    @JsonIgnore
    private boolean module_event_barcodeDirtyFlag;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @Res_config_settingsAlias_domainDefault(info = "默认规则")
    private String alias_domain;

    @JsonIgnore
    private boolean alias_domainDirtyFlag;

    /**
     * 属性 [SOCIAL_LINKEDIN]
     *
     */
    @Res_config_settingsSocial_linkedinDefault(info = "默认规则")
    private String social_linkedin;

    @JsonIgnore
    private boolean social_linkedinDirtyFlag;

    /**
     * 属性 [GROUP_STOCK_MULTI_WAREHOUSES]
     *
     */
    @Res_config_settingsGroup_stock_multi_warehousesDefault(info = "默认规则")
    private String group_stock_multi_warehouses;

    @JsonIgnore
    private boolean group_stock_multi_warehousesDirtyFlag;

    /**
     * 属性 [SALESPERSON_ID]
     *
     */
    @Res_config_settingsSalesperson_idDefault(info = "默认规则")
    private Integer salesperson_id;

    @JsonIgnore
    private boolean salesperson_idDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_REPORTS]
     *
     */
    @Res_config_settingsModule_account_reportsDefault(info = "默认规则")
    private String module_account_reports;

    @JsonIgnore
    private boolean module_account_reportsDirtyFlag;

    /**
     * 属性 [GROUP_PRODUCT_PRICELIST]
     *
     */
    @Res_config_settingsGroup_product_pricelistDefault(info = "默认规则")
    private String group_product_pricelist;

    @JsonIgnore
    private boolean group_product_pricelistDirtyFlag;

    /**
     * 属性 [MODULE_CRM_PHONE_VALIDATION]
     *
     */
    @Res_config_settingsModule_crm_phone_validationDefault(info = "默认规则")
    private String module_crm_phone_validation;

    @JsonIgnore
    private boolean module_crm_phone_validationDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_VERSION]
     *
     */
    @Res_config_settingsModule_website_versionDefault(info = "默认规则")
    private String module_website_version;

    @JsonIgnore
    private boolean module_website_versionDirtyFlag;

    /**
     * 属性 [MODULE_BASE_IMPORT]
     *
     */
    @Res_config_settingsModule_base_importDefault(info = "默认规则")
    private String module_base_import;

    @JsonIgnore
    private boolean module_base_importDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Res_config_settings__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CSV]
     *
     */
    @Res_config_settingsModule_account_bank_statement_import_csvDefault(info = "默认规则")
    private String module_account_bank_statement_import_csv;

    @JsonIgnore
    private boolean module_account_bank_statement_import_csvDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_TAXCLOUD]
     *
     */
    @Res_config_settingsModule_account_taxcloudDefault(info = "默认规则")
    private String module_account_taxcloud;

    @JsonIgnore
    private boolean module_account_taxcloudDirtyFlag;

    /**
     * 属性 [USE_SALE_NOTE]
     *
     */
    @Res_config_settingsUse_sale_noteDefault(info = "默认规则")
    private String use_sale_note;

    @JsonIgnore
    private boolean use_sale_noteDirtyFlag;

    /**
     * 属性 [CART_ABANDONED_DELAY]
     *
     */
    @Res_config_settingsCart_abandoned_delayDefault(info = "默认规则")
    private Double cart_abandoned_delay;

    @JsonIgnore
    private boolean cart_abandoned_delayDirtyFlag;

    /**
     * 属性 [WEBSITE_DOMAIN]
     *
     */
    @Res_config_settingsWebsite_domainDefault(info = "默认规则")
    private String website_domain;

    @JsonIgnore
    private boolean website_domainDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_ACCOUNTANT]
     *
     */
    @Res_config_settingsModule_account_accountantDefault(info = "默认规则")
    private String module_account_accountant;

    @JsonIgnore
    private boolean module_account_accountantDirtyFlag;

    /**
     * 属性 [MODULE_SALE_MARGIN]
     *
     */
    @Res_config_settingsModule_sale_marginDefault(info = "默认规则")
    private String module_sale_margin;

    @JsonIgnore
    private boolean module_sale_marginDirtyFlag;

    /**
     * 属性 [DIGEST_EMAILS]
     *
     */
    @Res_config_settingsDigest_emailsDefault(info = "默认规则")
    private String digest_emails;

    @JsonIgnore
    private boolean digest_emailsDirtyFlag;

    /**
     * 属性 [MODULE_PAD]
     *
     */
    @Res_config_settingsModule_padDefault(info = "默认规则")
    private String module_pad;

    @JsonIgnore
    private boolean module_padDirtyFlag;

    /**
     * 属性 [GROUP_WARNING_ACCOUNT]
     *
     */
    @Res_config_settingsGroup_warning_accountDefault(info = "默认规则")
    private String group_warning_account;

    @JsonIgnore
    private boolean group_warning_accountDirtyFlag;

    /**
     * 属性 [GROUP_DISPLAY_INCOTERM]
     *
     */
    @Res_config_settingsGroup_display_incotermDefault(info = "默认规则")
    private String group_display_incoterm;

    @JsonIgnore
    private boolean group_display_incotermDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_SALE_WISHLIST]
     *
     */
    @Res_config_settingsModule_website_sale_wishlistDefault(info = "默认规则")
    private String module_website_sale_wishlist;

    @JsonIgnore
    private boolean module_website_sale_wishlistDirtyFlag;

    /**
     * 属性 [USER_DEFAULT_RIGHTS]
     *
     */
    @Res_config_settingsUser_default_rightsDefault(info = "默认规则")
    private String user_default_rights;

    @JsonIgnore
    private boolean user_default_rightsDirtyFlag;

    /**
     * 属性 [DEFAULT_PURCHASE_METHOD]
     *
     */
    @Res_config_settingsDefault_purchase_methodDefault(info = "默认规则")
    private String default_purchase_method;

    @JsonIgnore
    private boolean default_purchase_methodDirtyFlag;

    /**
     * 属性 [GROUP_DELIVERY_INVOICE_ADDRESS]
     *
     */
    @Res_config_settingsGroup_delivery_invoice_addressDefault(info = "默认规则")
    private String group_delivery_invoice_address;

    @JsonIgnore
    private boolean group_delivery_invoice_addressDirtyFlag;

    /**
     * 属性 [GROUP_LOT_ON_DELIVERY_SLIP]
     *
     */
    @Res_config_settingsGroup_lot_on_delivery_slipDefault(info = "默认规则")
    private String group_lot_on_delivery_slip;

    @JsonIgnore
    private boolean group_lot_on_delivery_slipDirtyFlag;

    /**
     * 属性 [MODULE_EVENT_SALE]
     *
     */
    @Res_config_settingsModule_event_saleDefault(info = "默认规则")
    private String module_event_sale;

    @JsonIgnore
    private boolean module_event_saleDirtyFlag;

    /**
     * 属性 [GROUP_SHOW_LINE_SUBTOTALS_TAX_INCLUDED]
     *
     */
    @Res_config_settingsGroup_show_line_subtotals_tax_includedDefault(info = "默认规则")
    private String group_show_line_subtotals_tax_included;

    @JsonIgnore
    private boolean group_show_line_subtotals_tax_includedDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Res_config_settingsDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [GROUP_PRODUCT_VARIANT]
     *
     */
    @Res_config_settingsGroup_product_variantDefault(info = "默认规则")
    private String group_product_variant;

    @JsonIgnore
    private boolean group_product_variantDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_SEPA]
     *
     */
    @Res_config_settingsModule_account_sepaDefault(info = "默认规则")
    private String module_account_sepa;

    @JsonIgnore
    private boolean module_account_sepaDirtyFlag;

    /**
     * 属性 [GROUP_MULTI_CURRENCY]
     *
     */
    @Res_config_settingsGroup_multi_currencyDefault(info = "默认规则")
    private String group_multi_currency;

    @JsonIgnore
    private boolean group_multi_currencyDirtyFlag;

    /**
     * 属性 [GROUP_PRODUCTS_IN_BILLS]
     *
     */
    @Res_config_settingsGroup_products_in_billsDefault(info = "默认规则")
    private String group_products_in_bills;

    @JsonIgnore
    private boolean group_products_in_billsDirtyFlag;

    /**
     * 属性 [GROUP_ANALYTIC_ACCOUNTING]
     *
     */
    @Res_config_settingsGroup_analytic_accountingDefault(info = "默认规则")
    private String group_analytic_accounting;

    @JsonIgnore
    private boolean group_analytic_accountingDirtyFlag;

    /**
     * 属性 [GROUP_STOCK_PACKAGING]
     *
     */
    @Res_config_settingsGroup_stock_packagingDefault(info = "默认规则")
    private String group_stock_packaging;

    @JsonIgnore
    private boolean group_stock_packagingDirtyFlag;

    /**
     * 属性 [WEBSITE_SLIDE_GOOGLE_APP_KEY]
     *
     */
    @Res_config_settingsWebsite_slide_google_app_keyDefault(info = "默认规则")
    private String website_slide_google_app_key;

    @JsonIgnore
    private boolean website_slide_google_app_keyDirtyFlag;

    /**
     * 属性 [PO_ORDER_APPROVAL]
     *
     */
    @Res_config_settingsPo_order_approvalDefault(info = "默认规则")
    private String po_order_approval;

    @JsonIgnore
    private boolean po_order_approvalDirtyFlag;

    /**
     * 属性 [IS_INSTALLED_SALE]
     *
     */
    @Res_config_settingsIs_installed_saleDefault(info = "默认规则")
    private String is_installed_sale;

    @JsonIgnore
    private boolean is_installed_saleDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_PAYMENT]
     *
     */
    @Res_config_settingsModule_account_paymentDefault(info = "默认规则")
    private String module_account_payment;

    @JsonIgnore
    private boolean module_account_paymentDirtyFlag;

    /**
     * 属性 [GROUP_ANALYTIC_TAGS]
     *
     */
    @Res_config_settingsGroup_analytic_tagsDefault(info = "默认规则")
    private String group_analytic_tags;

    @JsonIgnore
    private boolean group_analytic_tagsDirtyFlag;

    /**
     * 属性 [GROUP_SALE_ORDER_DATES]
     *
     */
    @Res_config_settingsGroup_sale_order_datesDefault(info = "默认规则")
    private String group_sale_order_dates;

    @JsonIgnore
    private boolean group_sale_order_datesDirtyFlag;

    /**
     * 属性 [MODULE_VOIP]
     *
     */
    @Res_config_settingsModule_voipDefault(info = "默认规则")
    private String module_voip;

    @JsonIgnore
    private boolean module_voipDirtyFlag;

    /**
     * 属性 [CART_RECOVERY_MAIL_TEMPLATE]
     *
     */
    @Res_config_settingsCart_recovery_mail_templateDefault(info = "默认规则")
    private Integer cart_recovery_mail_template;

    @JsonIgnore
    private boolean cart_recovery_mail_templateDirtyFlag;

    /**
     * 属性 [GROUP_MULTI_WEBSITE]
     *
     */
    @Res_config_settingsGroup_multi_websiteDefault(info = "默认规则")
    private String group_multi_website;

    @JsonIgnore
    private boolean group_multi_websiteDirtyFlag;

    /**
     * 属性 [MODULE_AUTH_OAUTH]
     *
     */
    @Res_config_settingsModule_auth_oauthDefault(info = "默认规则")
    private String module_auth_oauth;

    @JsonIgnore
    private boolean module_auth_oauthDirtyFlag;

    /**
     * 属性 [SALE_DELIVERY_SETTINGS]
     *
     */
    @Res_config_settingsSale_delivery_settingsDefault(info = "默认规则")
    private String sale_delivery_settings;

    @JsonIgnore
    private boolean sale_delivery_settingsDirtyFlag;

    /**
     * 属性 [MODULE_SALE_QUOTATION_BUILDER]
     *
     */
    @Res_config_settingsModule_sale_quotation_builderDefault(info = "默认规则")
    private String module_sale_quotation_builder;

    @JsonIgnore
    private boolean module_sale_quotation_builderDirtyFlag;

    /**
     * 属性 [MODULE_INTER_COMPANY_RULES]
     *
     */
    @Res_config_settingsModule_inter_company_rulesDefault(info = "默认规则")
    private String module_inter_company_rules;

    @JsonIgnore
    private boolean module_inter_company_rulesDirtyFlag;

    /**
     * 属性 [USE_SECURITY_LEAD]
     *
     */
    @Res_config_settingsUse_security_leadDefault(info = "默认规则")
    private String use_security_lead;

    @JsonIgnore
    private boolean use_security_leadDirtyFlag;

    /**
     * 属性 [DEFAULT_INVOICE_POLICY]
     *
     */
    @Res_config_settingsDefault_invoice_policyDefault(info = "默认规则")
    private String default_invoice_policy;

    @JsonIgnore
    private boolean default_invoice_policyDirtyFlag;

    /**
     * 属性 [MULTI_SALES_PRICE]
     *
     */
    @Res_config_settingsMulti_sales_priceDefault(info = "默认规则")
    private String multi_sales_price;

    @JsonIgnore
    private boolean multi_sales_priceDirtyFlag;

    /**
     * 属性 [LOCK_CONFIRMED_PO]
     *
     */
    @Res_config_settingsLock_confirmed_poDefault(info = "默认规则")
    private String lock_confirmed_po;

    @JsonIgnore
    private boolean lock_confirmed_poDirtyFlag;

    /**
     * 属性 [PRODUCT_WEIGHT_IN_LBS]
     *
     */
    @Res_config_settingsProduct_weight_in_lbsDefault(info = "默认规则")
    private String product_weight_in_lbs;

    @JsonIgnore
    private boolean product_weight_in_lbsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Res_config_settingsCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [HAS_GOOGLE_ANALYTICS_DASHBOARD]
     *
     */
    @Res_config_settingsHas_google_analytics_dashboardDefault(info = "默认规则")
    private String has_google_analytics_dashboard;

    @JsonIgnore
    private boolean has_google_analytics_dashboardDirtyFlag;

    /**
     * 属性 [MODULE_STOCK_PICKING_BATCH]
     *
     */
    @Res_config_settingsModule_stock_picking_batchDefault(info = "默认规则")
    private String module_stock_picking_batch;

    @JsonIgnore
    private boolean module_stock_picking_batchDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Res_config_settingsWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [EXPENSE_ALIAS_PREFIX]
     *
     */
    @Res_config_settingsExpense_alias_prefixDefault(info = "默认规则")
    private String expense_alias_prefix;

    @JsonIgnore
    private boolean expense_alias_prefixDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_DEFERRED_REVENUE]
     *
     */
    @Res_config_settingsModule_account_deferred_revenueDefault(info = "默认规则")
    private String module_account_deferred_revenue;

    @JsonIgnore
    private boolean module_account_deferred_revenueDirtyFlag;

    /**
     * 属性 [SOCIAL_FACEBOOK]
     *
     */
    @Res_config_settingsSocial_facebookDefault(info = "默认规则")
    private String social_facebook;

    @JsonIgnore
    private boolean social_facebookDirtyFlag;

    /**
     * 属性 [MODULE_WEB_UNSPLASH]
     *
     */
    @Res_config_settingsModule_web_unsplashDefault(info = "默认规则")
    private String module_web_unsplash;

    @JsonIgnore
    private boolean module_web_unsplashDirtyFlag;

    /**
     * 属性 [GROUP_MASS_MAILING_CAMPAIGN]
     *
     */
    @Res_config_settingsGroup_mass_mailing_campaignDefault(info = "默认规则")
    private String group_mass_mailing_campaign;

    @JsonIgnore
    private boolean group_mass_mailing_campaignDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CAMT]
     *
     */
    @Res_config_settingsModule_account_bank_statement_import_camtDefault(info = "默认规则")
    private String module_account_bank_statement_import_camt;

    @JsonIgnore
    private boolean module_account_bank_statement_import_camtDirtyFlag;

    /**
     * 属性 [MODULE_PRODUCT_MARGIN]
     *
     */
    @Res_config_settingsModule_product_marginDefault(info = "默认规则")
    private String module_product_margin;

    @JsonIgnore
    private boolean module_product_marginDirtyFlag;

    /**
     * 属性 [GROUP_SUBTASK_PROJECT]
     *
     */
    @Res_config_settingsGroup_subtask_projectDefault(info = "默认规则")
    private String group_subtask_project;

    @JsonIgnore
    private boolean group_subtask_projectDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_3WAY_MATCH]
     *
     */
    @Res_config_settingsModule_account_3way_matchDefault(info = "默认规则")
    private String module_account_3way_match;

    @JsonIgnore
    private boolean module_account_3way_matchDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_SALE_DIGITAL]
     *
     */
    @Res_config_settingsModule_website_sale_digitalDefault(info = "默认规则")
    private String module_website_sale_digital;

    @JsonIgnore
    private boolean module_website_sale_digitalDirtyFlag;

    /**
     * 属性 [MODULE_SALE_COUPON]
     *
     */
    @Res_config_settingsModule_sale_couponDefault(info = "默认规则")
    private String module_sale_coupon;

    @JsonIgnore
    private boolean module_sale_couponDirtyFlag;

    /**
     * 属性 [SHOW_BLACKLIST_BUTTONS]
     *
     */
    @Res_config_settingsShow_blacklist_buttonsDefault(info = "默认规则")
    private String show_blacklist_buttons;

    @JsonIgnore
    private boolean show_blacklist_buttonsDirtyFlag;

    /**
     * 属性 [GROUP_WARNING_STOCK]
     *
     */
    @Res_config_settingsGroup_warning_stockDefault(info = "默认规则")
    private String group_warning_stock;

    @JsonIgnore
    private boolean group_warning_stockDirtyFlag;

    /**
     * 属性 [GROUP_MULTI_COMPANY]
     *
     */
    @Res_config_settingsGroup_multi_companyDefault(info = "默认规则")
    private String group_multi_company;

    @JsonIgnore
    private boolean group_multi_companyDirtyFlag;

    /**
     * 属性 [GROUP_ATTENDANCE_USE_PIN]
     *
     */
    @Res_config_settingsGroup_attendance_use_pinDefault(info = "默认规则")
    private String group_attendance_use_pin;

    @JsonIgnore
    private boolean group_attendance_use_pinDirtyFlag;

    /**
     * 属性 [SALESTEAM_ID]
     *
     */
    @Res_config_settingsSalesteam_idDefault(info = "默认规则")
    private Integer salesteam_id;

    @JsonIgnore
    private boolean salesteam_idDirtyFlag;

    /**
     * 属性 [FAVICON]
     *
     */
    @Res_config_settingsFaviconDefault(info = "默认规则")
    private byte[] favicon;

    @JsonIgnore
    private boolean faviconDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_SALE_COMPARISON]
     *
     */
    @Res_config_settingsModule_website_sale_comparisonDefault(info = "默认规则")
    private String module_website_sale_comparison;

    @JsonIgnore
    private boolean module_website_sale_comparisonDirtyFlag;

    /**
     * 属性 [AVAILABLE_THRESHOLD]
     *
     */
    @Res_config_settingsAvailable_thresholdDefault(info = "默认规则")
    private Double available_threshold;

    @JsonIgnore
    private boolean available_thresholdDirtyFlag;

    /**
     * 属性 [USE_PO_LEAD]
     *
     */
    @Res_config_settingsUse_po_leadDefault(info = "默认规则")
    private String use_po_lead;

    @JsonIgnore
    private boolean use_po_leadDirtyFlag;

    /**
     * 属性 [GROUP_FISCAL_YEAR]
     *
     */
    @Res_config_settingsGroup_fiscal_yearDefault(info = "默认规则")
    private String group_fiscal_year;

    @JsonIgnore
    private boolean group_fiscal_yearDirtyFlag;

    /**
     * 属性 [MODULE_HR_TIMESHEET]
     *
     */
    @Res_config_settingsModule_hr_timesheetDefault(info = "默认规则")
    private String module_hr_timesheet;

    @JsonIgnore
    private boolean module_hr_timesheetDirtyFlag;

    /**
     * 属性 [MODULE_MRP_PLM]
     *
     */
    @Res_config_settingsModule_mrp_plmDefault(info = "默认规则")
    private String module_mrp_plm;

    @JsonIgnore
    private boolean module_mrp_plmDirtyFlag;

    /**
     * 属性 [MODULE_ACCOUNT_YODLEE]
     *
     */
    @Res_config_settingsModule_account_yodleeDefault(info = "默认规则")
    private String module_account_yodlee;

    @JsonIgnore
    private boolean module_account_yodleeDirtyFlag;

    /**
     * 属性 [CDN_FILTERS]
     *
     */
    @Res_config_settingsCdn_filtersDefault(info = "默认规则")
    private String cdn_filters;

    @JsonIgnore
    private boolean cdn_filtersDirtyFlag;

    /**
     * 属性 [USE_PROPAGATION_MINIMUM_DELTA]
     *
     */
    @Res_config_settingsUse_propagation_minimum_deltaDefault(info = "默认规则")
    private String use_propagation_minimum_delta;

    @JsonIgnore
    private boolean use_propagation_minimum_deltaDirtyFlag;

    /**
     * 属性 [MODULE_DELIVERY_UPS]
     *
     */
    @Res_config_settingsModule_delivery_upsDefault(info = "默认规则")
    private String module_delivery_ups;

    @JsonIgnore
    private boolean module_delivery_upsDirtyFlag;

    /**
     * 属性 [FAIL_COUNTER]
     *
     */
    @Res_config_settingsFail_counterDefault(info = "默认规则")
    private Integer fail_counter;

    @JsonIgnore
    private boolean fail_counterDirtyFlag;

    /**
     * 属性 [GROUP_STOCK_PRODUCTION_LOT]
     *
     */
    @Res_config_settingsGroup_stock_production_lotDefault(info = "默认规则")
    private String group_stock_production_lot;

    @JsonIgnore
    private boolean group_stock_production_lotDirtyFlag;

    /**
     * 属性 [HAS_ACCOUNTING_ENTRIES]
     *
     */
    @Res_config_settingsHas_accounting_entriesDefault(info = "默认规则")
    private String has_accounting_entries;

    @JsonIgnore
    private boolean has_accounting_entriesDirtyFlag;

    /**
     * 属性 [GROUP_UOM]
     *
     */
    @Res_config_settingsGroup_uomDefault(info = "默认规则")
    private String group_uom;

    @JsonIgnore
    private boolean group_uomDirtyFlag;

    /**
     * 属性 [MASS_MAILING_OUTGOING_MAIL_SERVER]
     *
     */
    @Res_config_settingsMass_mailing_outgoing_mail_serverDefault(info = "默认规则")
    private String mass_mailing_outgoing_mail_server;

    @JsonIgnore
    private boolean mass_mailing_outgoing_mail_serverDirtyFlag;

    /**
     * 属性 [CRM_ALIAS_PREFIX]
     *
     */
    @Res_config_settingsCrm_alias_prefixDefault(info = "默认规则")
    private String crm_alias_prefix;

    @JsonIgnore
    private boolean crm_alias_prefixDirtyFlag;

    /**
     * 属性 [SOCIAL_GOOGLEPLUS]
     *
     */
    @Res_config_settingsSocial_googleplusDefault(info = "默认规则")
    private String social_googleplus;

    @JsonIgnore
    private boolean social_googleplusDirtyFlag;

    /**
     * 属性 [GROUP_ROUTE_SO_LINES]
     *
     */
    @Res_config_settingsGroup_route_so_linesDefault(info = "默认规则")
    private String group_route_so_lines;

    @JsonIgnore
    private boolean group_route_so_linesDirtyFlag;

    /**
     * 属性 [GOOGLE_MANAGEMENT_CLIENT_SECRET]
     *
     */
    @Res_config_settingsGoogle_management_client_secretDefault(info = "默认规则")
    private String google_management_client_secret;

    @JsonIgnore
    private boolean google_management_client_secretDirtyFlag;

    /**
     * 属性 [SOCIAL_DEFAULT_IMAGE]
     *
     */
    @Res_config_settingsSocial_default_imageDefault(info = "默认规则")
    private byte[] social_default_image;

    @JsonIgnore
    private boolean social_default_imageDirtyFlag;

    /**
     * 属性 [HAS_GOOGLE_ANALYTICS]
     *
     */
    @Res_config_settingsHas_google_analyticsDefault(info = "默认规则")
    private String has_google_analytics;

    @JsonIgnore
    private boolean has_google_analyticsDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_EVENT_QUESTIONS]
     *
     */
    @Res_config_settingsModule_website_event_questionsDefault(info = "默认规则")
    private String module_website_event_questions;

    @JsonIgnore
    private boolean module_website_event_questionsDirtyFlag;

    /**
     * 属性 [WEBSITE_DEFAULT_LANG_ID]
     *
     */
    @Res_config_settingsWebsite_default_lang_idDefault(info = "默认规则")
    private Integer website_default_lang_id;

    @JsonIgnore
    private boolean website_default_lang_idDirtyFlag;

    /**
     * 属性 [INVENTORY_AVAILABILITY]
     *
     */
    @Res_config_settingsInventory_availabilityDefault(info = "默认规则")
    private String inventory_availability;

    @JsonIgnore
    private boolean inventory_availabilityDirtyFlag;

    /**
     * 属性 [GROUP_WARNING_PURCHASE]
     *
     */
    @Res_config_settingsGroup_warning_purchaseDefault(info = "默认规则")
    private String group_warning_purchase;

    @JsonIgnore
    private boolean group_warning_purchaseDirtyFlag;

    /**
     * 属性 [MODULE_QUALITY_CONTROL]
     *
     */
    @Res_config_settingsModule_quality_controlDefault(info = "默认规则")
    private String module_quality_control;

    @JsonIgnore
    private boolean module_quality_controlDirtyFlag;

    /**
     * 属性 [GENERATE_LEAD_FROM_ALIAS]
     *
     */
    @Res_config_settingsGenerate_lead_from_aliasDefault(info = "默认规则")
    private String generate_lead_from_alias;

    @JsonIgnore
    private boolean generate_lead_from_aliasDirtyFlag;

    /**
     * 属性 [SALE_PRICELIST_SETTING]
     *
     */
    @Res_config_settingsSale_pricelist_settingDefault(info = "默认规则")
    private String sale_pricelist_setting;

    @JsonIgnore
    private boolean sale_pricelist_settingDirtyFlag;

    /**
     * 属性 [GROUP_PRICELIST_ITEM]
     *
     */
    @Res_config_settingsGroup_pricelist_itemDefault(info = "默认规则")
    private String group_pricelist_item;

    @JsonIgnore
    private boolean group_pricelist_itemDirtyFlag;

    /**
     * 属性 [EXTERNAL_EMAIL_SERVER_DEFAULT]
     *
     */
    @Res_config_settingsExternal_email_server_defaultDefault(info = "默认规则")
    private String external_email_server_default;

    @JsonIgnore
    private boolean external_email_server_defaultDirtyFlag;

    /**
     * 属性 [UNSPLASH_ACCESS_KEY]
     *
     */
    @Res_config_settingsUnsplash_access_keyDefault(info = "默认规则")
    private String unsplash_access_key;

    @JsonIgnore
    private boolean unsplash_access_keyDirtyFlag;

    /**
     * 属性 [MODULE_BASE_GENGO]
     *
     */
    @Res_config_settingsModule_base_gengoDefault(info = "默认规则")
    private String module_base_gengo;

    @JsonIgnore
    private boolean module_base_gengoDirtyFlag;

    /**
     * 属性 [MODULE_WEBSITE_EVENT_SALE]
     *
     */
    @Res_config_settingsModule_website_event_saleDefault(info = "默认规则")
    private String module_website_event_sale;

    @JsonIgnore
    private boolean module_website_event_saleDirtyFlag;

    /**
     * 属性 [CRM_DEFAULT_USER_ID]
     *
     */
    @Res_config_settingsCrm_default_user_idDefault(info = "默认规则")
    private Integer crm_default_user_id;

    @JsonIgnore
    private boolean crm_default_user_idDirtyFlag;

    /**
     * 属性 [MODULE_STOCK_DROPSHIPPING]
     *
     */
    @Res_config_settingsModule_stock_dropshippingDefault(info = "默认规则")
    private String module_stock_dropshipping;

    @JsonIgnore
    private boolean module_stock_dropshippingDirtyFlag;

    /**
     * 属性 [MODULE_CRM_REVEAL]
     *
     */
    @Res_config_settingsModule_crm_revealDefault(info = "默认规则")
    private String module_crm_reveal;

    @JsonIgnore
    private boolean module_crm_revealDirtyFlag;

    /**
     * 属性 [MASS_MAILING_MAIL_SERVER_ID]
     *
     */
    @Res_config_settingsMass_mailing_mail_server_idDefault(info = "默认规则")
    private Integer mass_mailing_mail_server_id;

    @JsonIgnore
    private boolean mass_mailing_mail_server_idDirtyFlag;

    /**
     * 属性 [GROUP_SHOW_LINE_SUBTOTALS_TAX_EXCLUDED]
     *
     */
    @Res_config_settingsGroup_show_line_subtotals_tax_excludedDefault(info = "默认规则")
    private String group_show_line_subtotals_tax_excluded;

    @JsonIgnore
    private boolean group_show_line_subtotals_tax_excludedDirtyFlag;

    /**
     * 属性 [MODULE_HR_RECRUITMENT_SURVEY]
     *
     */
    @Res_config_settingsModule_hr_recruitment_surveyDefault(info = "默认规则")
    private String module_hr_recruitment_survey;

    @JsonIgnore
    private boolean module_hr_recruitment_surveyDirtyFlag;

    /**
     * 属性 [MODULE_MRP_WORKORDER]
     *
     */
    @Res_config_settingsModule_mrp_workorderDefault(info = "默认规则")
    private String module_mrp_workorder;

    @JsonIgnore
    private boolean module_mrp_workorderDirtyFlag;

    /**
     * 属性 [MODULE_POS_MERCURY]
     *
     */
    @Res_config_settingsModule_pos_mercuryDefault(info = "默认规则")
    private String module_pos_mercury;

    @JsonIgnore
    private boolean module_pos_mercuryDirtyFlag;

    /**
     * 属性 [INVOICE_REFERENCE_TYPE]
     *
     */
    @Res_config_settingsInvoice_reference_typeDefault(info = "默认规则")
    private String invoice_reference_type;

    @JsonIgnore
    private boolean invoice_reference_typeDirtyFlag;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @Res_config_settingsCompany_currency_idDefault(info = "默认规则")
    private Integer company_currency_id;

    @JsonIgnore
    private boolean company_currency_idDirtyFlag;

    /**
     * 属性 [TAX_EXIGIBILITY]
     *
     */
    @Res_config_settingsTax_exigibilityDefault(info = "默认规则")
    private String tax_exigibility;

    @JsonIgnore
    private boolean tax_exigibilityDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Res_config_settingsWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_BANK_RECONCILIATION_START]
     *
     */
    @Res_config_settingsAccount_bank_reconciliation_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp account_bank_reconciliation_start;

    @JsonIgnore
    private boolean account_bank_reconciliation_startDirtyFlag;

    /**
     * 属性 [TAX_CASH_BASIS_JOURNAL_ID]
     *
     */
    @Res_config_settingsTax_cash_basis_journal_idDefault(info = "默认规则")
    private Integer tax_cash_basis_journal_id;

    @JsonIgnore
    private boolean tax_cash_basis_journal_idDirtyFlag;

    /**
     * 属性 [PO_LEAD]
     *
     */
    @Res_config_settingsPo_leadDefault(info = "默认规则")
    private Double po_lead;

    @JsonIgnore
    private boolean po_leadDirtyFlag;

    /**
     * 属性 [SNAILMAIL_COLOR]
     *
     */
    @Res_config_settingsSnailmail_colorDefault(info = "默认规则")
    private String snailmail_color;

    @JsonIgnore
    private boolean snailmail_colorDirtyFlag;

    /**
     * 属性 [PAPERFORMAT_ID]
     *
     */
    @Res_config_settingsPaperformat_idDefault(info = "默认规则")
    private Integer paperformat_id;

    @JsonIgnore
    private boolean paperformat_idDirtyFlag;

    /**
     * 属性 [PORTAL_CONFIRMATION_SIGN]
     *
     */
    @Res_config_settingsPortal_confirmation_signDefault(info = "默认规则")
    private String portal_confirmation_sign;

    @JsonIgnore
    private boolean portal_confirmation_signDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Res_config_settingsCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [DIGEST_ID_TEXT]
     *
     */
    @Res_config_settingsDigest_id_textDefault(info = "默认规则")
    private String digest_id_text;

    @JsonIgnore
    private boolean digest_id_textDirtyFlag;

    /**
     * 属性 [SALE_NOTE]
     *
     */
    @Res_config_settingsSale_noteDefault(info = "默认规则")
    private String sale_note;

    @JsonIgnore
    private boolean sale_noteDirtyFlag;

    /**
     * 属性 [AUTH_SIGNUP_TEMPLATE_USER_ID_TEXT]
     *
     */
    @Res_config_settingsAuth_signup_template_user_id_textDefault(info = "默认规则")
    private String auth_signup_template_user_id_text;

    @JsonIgnore
    private boolean auth_signup_template_user_id_textDirtyFlag;

    /**
     * 属性 [INVOICE_IS_PRINT]
     *
     */
    @Res_config_settingsInvoice_is_printDefault(info = "默认规则")
    private String invoice_is_print;

    @JsonIgnore
    private boolean invoice_is_printDirtyFlag;

    /**
     * 属性 [DEPOSIT_DEFAULT_PRODUCT_ID_TEXT]
     *
     */
    @Res_config_settingsDeposit_default_product_id_textDefault(info = "默认规则")
    private String deposit_default_product_id_text;

    @JsonIgnore
    private boolean deposit_default_product_id_textDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @Res_config_settingsResource_calendar_idDefault(info = "默认规则")
    private Integer resource_calendar_id;

    @JsonIgnore
    private boolean resource_calendar_idDirtyFlag;

    /**
     * 属性 [PO_LOCK]
     *
     */
    @Res_config_settingsPo_lockDefault(info = "默认规则")
    private String po_lock;

    @JsonIgnore
    private boolean po_lockDirtyFlag;

    /**
     * 属性 [INVOICE_IS_SNAILMAIL]
     *
     */
    @Res_config_settingsInvoice_is_snailmailDefault(info = "默认规则")
    private String invoice_is_snailmail;

    @JsonIgnore
    private boolean invoice_is_snailmailDirtyFlag;

    /**
     * 属性 [MANUFACTURING_LEAD]
     *
     */
    @Res_config_settingsManufacturing_leadDefault(info = "默认规则")
    private Double manufacturing_lead;

    @JsonIgnore
    private boolean manufacturing_leadDirtyFlag;

    /**
     * 属性 [PO_DOUBLE_VALIDATION]
     *
     */
    @Res_config_settingsPo_double_validationDefault(info = "默认规则")
    private String po_double_validation;

    @JsonIgnore
    private boolean po_double_validationDirtyFlag;

    /**
     * 属性 [REPORT_FOOTER]
     *
     */
    @Res_config_settingsReport_footerDefault(info = "默认规则")
    private String report_footer;

    @JsonIgnore
    private boolean report_footerDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @Res_config_settingsTemplate_id_textDefault(info = "默认规则")
    private String template_id_text;

    @JsonIgnore
    private boolean template_id_textDirtyFlag;

    /**
     * 属性 [INVOICE_IS_EMAIL]
     *
     */
    @Res_config_settingsInvoice_is_emailDefault(info = "默认规则")
    private String invoice_is_email;

    @JsonIgnore
    private boolean invoice_is_emailDirtyFlag;

    /**
     * 属性 [PORTAL_CONFIRMATION_PAY]
     *
     */
    @Res_config_settingsPortal_confirmation_payDefault(info = "默认规则")
    private String portal_confirmation_pay;

    @JsonIgnore
    private boolean portal_confirmation_payDirtyFlag;

    /**
     * 属性 [QR_CODE]
     *
     */
    @Res_config_settingsQr_codeDefault(info = "默认规则")
    private String qr_code;

    @JsonIgnore
    private boolean qr_codeDirtyFlag;

    /**
     * 属性 [SNAILMAIL_DUPLEX]
     *
     */
    @Res_config_settingsSnailmail_duplexDefault(info = "默认规则")
    private String snailmail_duplex;

    @JsonIgnore
    private boolean snailmail_duplexDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Res_config_settingsCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Res_config_settingsCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [SECURITY_LEAD]
     *
     */
    @Res_config_settingsSecurity_leadDefault(info = "默认规则")
    private Double security_lead;

    @JsonIgnore
    private boolean security_leadDirtyFlag;

    /**
     * 属性 [EXTERNAL_REPORT_LAYOUT_ID]
     *
     */
    @Res_config_settingsExternal_report_layout_idDefault(info = "默认规则")
    private Integer external_report_layout_id;

    @JsonIgnore
    private boolean external_report_layout_idDirtyFlag;

    /**
     * 属性 [PURCHASE_TAX_ID]
     *
     */
    @Res_config_settingsPurchase_tax_idDefault(info = "默认规则")
    private Integer purchase_tax_id;

    @JsonIgnore
    private boolean purchase_tax_idDirtyFlag;

    /**
     * 属性 [PROPAGATION_MINIMUM_DELTA]
     *
     */
    @Res_config_settingsPropagation_minimum_deltaDefault(info = "默认规则")
    private Integer propagation_minimum_delta;

    @JsonIgnore
    private boolean propagation_minimum_deltaDirtyFlag;

    /**
     * 属性 [QUOTATION_VALIDITY_DAYS]
     *
     */
    @Res_config_settingsQuotation_validity_daysDefault(info = "默认规则")
    private Integer quotation_validity_days;

    @JsonIgnore
    private boolean quotation_validity_daysDirtyFlag;

    /**
     * 属性 [TAX_CALCULATION_ROUNDING_METHOD]
     *
     */
    @Res_config_settingsTax_calculation_rounding_methodDefault(info = "默认规则")
    private String tax_calculation_rounding_method;

    @JsonIgnore
    private boolean tax_calculation_rounding_methodDirtyFlag;

    /**
     * 属性 [CURRENCY_EXCHANGE_JOURNAL_ID]
     *
     */
    @Res_config_settingsCurrency_exchange_journal_idDefault(info = "默认规则")
    private Integer currency_exchange_journal_id;

    @JsonIgnore
    private boolean currency_exchange_journal_idDirtyFlag;

    /**
     * 属性 [PO_DOUBLE_VALIDATION_AMOUNT]
     *
     */
    @Res_config_settingsPo_double_validation_amountDefault(info = "默认规则")
    private Double po_double_validation_amount;

    @JsonIgnore
    private boolean po_double_validation_amountDirtyFlag;

    /**
     * 属性 [CHART_TEMPLATE_ID_TEXT]
     *
     */
    @Res_config_settingsChart_template_id_textDefault(info = "默认规则")
    private String chart_template_id_text;

    @JsonIgnore
    private boolean chart_template_id_textDirtyFlag;

    /**
     * 属性 [SALE_TAX_ID]
     *
     */
    @Res_config_settingsSale_tax_idDefault(info = "默认规则")
    private Integer sale_tax_id;

    @JsonIgnore
    private boolean sale_tax_idDirtyFlag;

    /**
     * 属性 [DEFAULT_SALE_ORDER_TEMPLATE_ID_TEXT]
     *
     */
    @Res_config_settingsDefault_sale_order_template_id_textDefault(info = "默认规则")
    private String default_sale_order_template_id_text;

    @JsonIgnore
    private boolean default_sale_order_template_id_textDirtyFlag;

    /**
     * 属性 [DEFAULT_SALE_ORDER_TEMPLATE_ID]
     *
     */
    @Res_config_settingsDefault_sale_order_template_idDefault(info = "默认规则")
    private Integer default_sale_order_template_id;

    @JsonIgnore
    private boolean default_sale_order_template_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Res_config_settingsCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [DIGEST_ID]
     *
     */
    @Res_config_settingsDigest_idDefault(info = "默认规则")
    private Integer digest_id;

    @JsonIgnore
    private boolean digest_idDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @Res_config_settingsTemplate_idDefault(info = "默认规则")
    private Integer template_id;

    @JsonIgnore
    private boolean template_idDirtyFlag;

    /**
     * 属性 [CHART_TEMPLATE_ID]
     *
     */
    @Res_config_settingsChart_template_idDefault(info = "默认规则")
    private Integer chart_template_id;

    @JsonIgnore
    private boolean chart_template_idDirtyFlag;

    /**
     * 属性 [DEPOSIT_DEFAULT_PRODUCT_ID]
     *
     */
    @Res_config_settingsDeposit_default_product_idDefault(info = "默认规则")
    private Integer deposit_default_product_id;

    @JsonIgnore
    private boolean deposit_default_product_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Res_config_settingsCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [AUTH_SIGNUP_TEMPLATE_USER_ID]
     *
     */
    @Res_config_settingsAuth_signup_template_user_idDefault(info = "默认规则")
    private Integer auth_signup_template_user_id;

    @JsonIgnore
    private boolean auth_signup_template_user_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Res_config_settingsWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_QIF]
     */
    @JsonProperty("module_account_bank_statement_import_qif")
    public String getModule_account_bank_statement_import_qif(){
        return module_account_bank_statement_import_qif ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_QIF]
     */
    @JsonProperty("module_account_bank_statement_import_qif")
    public void setModule_account_bank_statement_import_qif(String  module_account_bank_statement_import_qif){
        this.module_account_bank_statement_import_qif = module_account_bank_statement_import_qif ;
        this.module_account_bank_statement_import_qifDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_QIF]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_bank_statement_import_qifDirtyFlag(){
        return module_account_bank_statement_import_qifDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_OFX]
     */
    @JsonProperty("module_account_bank_statement_import_ofx")
    public String getModule_account_bank_statement_import_ofx(){
        return module_account_bank_statement_import_ofx ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_OFX]
     */
    @JsonProperty("module_account_bank_statement_import_ofx")
    public void setModule_account_bank_statement_import_ofx(String  module_account_bank_statement_import_ofx){
        this.module_account_bank_statement_import_ofx = module_account_bank_statement_import_ofx ;
        this.module_account_bank_statement_import_ofxDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_OFX]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_bank_statement_import_ofxDirtyFlag(){
        return module_account_bank_statement_import_ofxDirtyFlag ;
    }

    /**
     * 获取 [HAS_GOOGLE_MAPS]
     */
    @JsonProperty("has_google_maps")
    public String getHas_google_maps(){
        return has_google_maps ;
    }

    /**
     * 设置 [HAS_GOOGLE_MAPS]
     */
    @JsonProperty("has_google_maps")
    public void setHas_google_maps(String  has_google_maps){
        this.has_google_maps = has_google_maps ;
        this.has_google_mapsDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_GOOGLE_MAPS]脏标记
     */
    @JsonIgnore
    public boolean getHas_google_mapsDirtyFlag(){
        return has_google_mapsDirtyFlag ;
    }

    /**
     * 获取 [MODULE_L10N_EU_SERVICE]
     */
    @JsonProperty("module_l10n_eu_service")
    public String getModule_l10n_eu_service(){
        return module_l10n_eu_service ;
    }

    /**
     * 设置 [MODULE_L10N_EU_SERVICE]
     */
    @JsonProperty("module_l10n_eu_service")
    public void setModule_l10n_eu_service(String  module_l10n_eu_service){
        this.module_l10n_eu_service = module_l10n_eu_service ;
        this.module_l10n_eu_serviceDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_L10N_EU_SERVICE]脏标记
     */
    @JsonIgnore
    public boolean getModule_l10n_eu_serviceDirtyFlag(){
        return module_l10n_eu_serviceDirtyFlag ;
    }

    /**
     * 获取 [CDN_ACTIVATED]
     */
    @JsonProperty("cdn_activated")
    public String getCdn_activated(){
        return cdn_activated ;
    }

    /**
     * 设置 [CDN_ACTIVATED]
     */
    @JsonProperty("cdn_activated")
    public void setCdn_activated(String  cdn_activated){
        this.cdn_activated = cdn_activated ;
        this.cdn_activatedDirtyFlag = true ;
    }

    /**
     * 获取 [CDN_ACTIVATED]脏标记
     */
    @JsonIgnore
    public boolean getCdn_activatedDirtyFlag(){
        return cdn_activatedDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_DEFAULT_LANG_CODE]
     */
    @JsonProperty("website_default_lang_code")
    public String getWebsite_default_lang_code(){
        return website_default_lang_code ;
    }

    /**
     * 设置 [WEBSITE_DEFAULT_LANG_CODE]
     */
    @JsonProperty("website_default_lang_code")
    public void setWebsite_default_lang_code(String  website_default_lang_code){
        this.website_default_lang_code = website_default_lang_code ;
        this.website_default_lang_codeDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_DEFAULT_LANG_CODE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_default_lang_codeDirtyFlag(){
        return website_default_lang_codeDirtyFlag ;
    }

    /**
     * 获取 [AUTH_SIGNUP_RESET_PASSWORD]
     */
    @JsonProperty("auth_signup_reset_password")
    public String getAuth_signup_reset_password(){
        return auth_signup_reset_password ;
    }

    /**
     * 设置 [AUTH_SIGNUP_RESET_PASSWORD]
     */
    @JsonProperty("auth_signup_reset_password")
    public void setAuth_signup_reset_password(String  auth_signup_reset_password){
        this.auth_signup_reset_password = auth_signup_reset_password ;
        this.auth_signup_reset_passwordDirtyFlag = true ;
    }

    /**
     * 获取 [AUTH_SIGNUP_RESET_PASSWORD]脏标记
     */
    @JsonIgnore
    public boolean getAuth_signup_reset_passwordDirtyFlag(){
        return auth_signup_reset_passwordDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_COUNTRY_GROUP_IDS]
     */
    @JsonProperty("website_country_group_ids")
    public String getWebsite_country_group_ids(){
        return website_country_group_ids ;
    }

    /**
     * 设置 [WEBSITE_COUNTRY_GROUP_IDS]
     */
    @JsonProperty("website_country_group_ids")
    public void setWebsite_country_group_ids(String  website_country_group_ids){
        this.website_country_group_ids = website_country_group_ids ;
        this.website_country_group_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_COUNTRY_GROUP_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_country_group_idsDirtyFlag(){
        return website_country_group_idsDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_INSTAGRAM]
     */
    @JsonProperty("social_instagram")
    public String getSocial_instagram(){
        return social_instagram ;
    }

    /**
     * 设置 [SOCIAL_INSTAGRAM]
     */
    @JsonProperty("social_instagram")
    public void setSocial_instagram(String  social_instagram){
        this.social_instagram = social_instagram ;
        this.social_instagramDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_INSTAGRAM]脏标记
     */
    @JsonIgnore
    public boolean getSocial_instagramDirtyFlag(){
        return social_instagramDirtyFlag ;
    }

    /**
     * 获取 [MODULE_PURCHASE_REQUISITION]
     */
    @JsonProperty("module_purchase_requisition")
    public String getModule_purchase_requisition(){
        return module_purchase_requisition ;
    }

    /**
     * 设置 [MODULE_PURCHASE_REQUISITION]
     */
    @JsonProperty("module_purchase_requisition")
    public void setModule_purchase_requisition(String  module_purchase_requisition){
        this.module_purchase_requisition = module_purchase_requisition ;
        this.module_purchase_requisitionDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_PURCHASE_REQUISITION]脏标记
     */
    @JsonIgnore
    public boolean getModule_purchase_requisitionDirtyFlag(){
        return module_purchase_requisitionDirtyFlag ;
    }

    /**
     * 获取 [MODULE_DELIVERY_EASYPOST]
     */
    @JsonProperty("module_delivery_easypost")
    public String getModule_delivery_easypost(){
        return module_delivery_easypost ;
    }

    /**
     * 设置 [MODULE_DELIVERY_EASYPOST]
     */
    @JsonProperty("module_delivery_easypost")
    public void setModule_delivery_easypost(String  module_delivery_easypost){
        this.module_delivery_easypost = module_delivery_easypost ;
        this.module_delivery_easypostDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_DELIVERY_EASYPOST]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_easypostDirtyFlag(){
        return module_delivery_easypostDirtyFlag ;
    }

    /**
     * 获取 [GROUP_DISCOUNT_PER_SO_LINE]
     */
    @JsonProperty("group_discount_per_so_line")
    public String getGroup_discount_per_so_line(){
        return group_discount_per_so_line ;
    }

    /**
     * 设置 [GROUP_DISCOUNT_PER_SO_LINE]
     */
    @JsonProperty("group_discount_per_so_line")
    public void setGroup_discount_per_so_line(String  group_discount_per_so_line){
        this.group_discount_per_so_line = group_discount_per_so_line ;
        this.group_discount_per_so_lineDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_DISCOUNT_PER_SO_LINE]脏标记
     */
    @JsonIgnore
    public boolean getGroup_discount_per_so_lineDirtyFlag(){
        return group_discount_per_so_lineDirtyFlag ;
    }

    /**
     * 获取 [AUTOMATIC_INVOICE]
     */
    @JsonProperty("automatic_invoice")
    public String getAutomatic_invoice(){
        return automatic_invoice ;
    }

    /**
     * 设置 [AUTOMATIC_INVOICE]
     */
    @JsonProperty("automatic_invoice")
    public void setAutomatic_invoice(String  automatic_invoice){
        this.automatic_invoice = automatic_invoice ;
        this.automatic_invoiceDirtyFlag = true ;
    }

    /**
     * 获取 [AUTOMATIC_INVOICE]脏标记
     */
    @JsonIgnore
    public boolean getAutomatic_invoiceDirtyFlag(){
        return automatic_invoiceDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_PLAID]
     */
    @JsonProperty("module_account_plaid")
    public String getModule_account_plaid(){
        return module_account_plaid ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_PLAID]
     */
    @JsonProperty("module_account_plaid")
    public void setModule_account_plaid(String  module_account_plaid){
        this.module_account_plaid = module_account_plaid ;
        this.module_account_plaidDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_PLAID]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_plaidDirtyFlag(){
        return module_account_plaidDirtyFlag ;
    }

    /**
     * 获取 [GOOGLE_MANAGEMENT_CLIENT_ID]
     */
    @JsonProperty("google_management_client_id")
    public String getGoogle_management_client_id(){
        return google_management_client_id ;
    }

    /**
     * 设置 [GOOGLE_MANAGEMENT_CLIENT_ID]
     */
    @JsonProperty("google_management_client_id")
    public void setGoogle_management_client_id(String  google_management_client_id){
        this.google_management_client_id = google_management_client_id ;
        this.google_management_client_idDirtyFlag = true ;
    }

    /**
     * 获取 [GOOGLE_MANAGEMENT_CLIENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_management_client_idDirtyFlag(){
        return google_management_client_idDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_CHECK_PRINTING]
     */
    @JsonProperty("module_account_check_printing")
    public String getModule_account_check_printing(){
        return module_account_check_printing ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_CHECK_PRINTING]
     */
    @JsonProperty("module_account_check_printing")
    public void setModule_account_check_printing(String  module_account_check_printing){
        this.module_account_check_printing = module_account_check_printing ;
        this.module_account_check_printingDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_CHECK_PRINTING]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_check_printingDirtyFlag(){
        return module_account_check_printingDirtyFlag ;
    }

    /**
     * 获取 [MODULE_PARTNER_AUTOCOMPLETE]
     */
    @JsonProperty("module_partner_autocomplete")
    public String getModule_partner_autocomplete(){
        return module_partner_autocomplete ;
    }

    /**
     * 设置 [MODULE_PARTNER_AUTOCOMPLETE]
     */
    @JsonProperty("module_partner_autocomplete")
    public void setModule_partner_autocomplete(String  module_partner_autocomplete){
        this.module_partner_autocomplete = module_partner_autocomplete ;
        this.module_partner_autocompleteDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_PARTNER_AUTOCOMPLETE]脏标记
     */
    @JsonIgnore
    public boolean getModule_partner_autocompleteDirtyFlag(){
        return module_partner_autocompleteDirtyFlag ;
    }

    /**
     * 获取 [LANGUAGE_COUNT]
     */
    @JsonProperty("language_count")
    public Integer getLanguage_count(){
        return language_count ;
    }

    /**
     * 设置 [LANGUAGE_COUNT]
     */
    @JsonProperty("language_count")
    public void setLanguage_count(Integer  language_count){
        this.language_count = language_count ;
        this.language_countDirtyFlag = true ;
    }

    /**
     * 获取 [LANGUAGE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getLanguage_countDirtyFlag(){
        return language_countDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_LINKS]
     */
    @JsonProperty("module_website_links")
    public String getModule_website_links(){
        return module_website_links ;
    }

    /**
     * 设置 [MODULE_WEBSITE_LINKS]
     */
    @JsonProperty("module_website_links")
    public void setModule_website_links(String  module_website_links){
        this.module_website_links = module_website_links ;
        this.module_website_linksDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_LINKS]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_linksDirtyFlag(){
        return module_website_linksDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_FORM_ENABLE_METADATA]
     */
    @JsonProperty("website_form_enable_metadata")
    public String getWebsite_form_enable_metadata(){
        return website_form_enable_metadata ;
    }

    /**
     * 设置 [WEBSITE_FORM_ENABLE_METADATA]
     */
    @JsonProperty("website_form_enable_metadata")
    public void setWebsite_form_enable_metadata(String  website_form_enable_metadata){
        this.website_form_enable_metadata = website_form_enable_metadata ;
        this.website_form_enable_metadataDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_FORM_ENABLE_METADATA]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_form_enable_metadataDirtyFlag(){
        return website_form_enable_metadataDirtyFlag ;
    }

    /**
     * 获取 [HAS_SOCIAL_NETWORK]
     */
    @JsonProperty("has_social_network")
    public String getHas_social_network(){
        return has_social_network ;
    }

    /**
     * 设置 [HAS_SOCIAL_NETWORK]
     */
    @JsonProperty("has_social_network")
    public void setHas_social_network(String  has_social_network){
        this.has_social_network = has_social_network ;
        this.has_social_networkDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_SOCIAL_NETWORK]脏标记
     */
    @JsonIgnore
    public boolean getHas_social_networkDirtyFlag(){
        return has_social_networkDirtyFlag ;
    }

    /**
     * 获取 [GROUP_SALE_DELIVERY_ADDRESS]
     */
    @JsonProperty("group_sale_delivery_address")
    public String getGroup_sale_delivery_address(){
        return group_sale_delivery_address ;
    }

    /**
     * 设置 [GROUP_SALE_DELIVERY_ADDRESS]
     */
    @JsonProperty("group_sale_delivery_address")
    public void setGroup_sale_delivery_address(String  group_sale_delivery_address){
        this.group_sale_delivery_address = group_sale_delivery_address ;
        this.group_sale_delivery_addressDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_SALE_DELIVERY_ADDRESS]脏标记
     */
    @JsonIgnore
    public boolean getGroup_sale_delivery_addressDirtyFlag(){
        return group_sale_delivery_addressDirtyFlag ;
    }

    /**
     * 获取 [GOOGLE_ANALYTICS_KEY]
     */
    @JsonProperty("google_analytics_key")
    public String getGoogle_analytics_key(){
        return google_analytics_key ;
    }

    /**
     * 设置 [GOOGLE_ANALYTICS_KEY]
     */
    @JsonProperty("google_analytics_key")
    public void setGoogle_analytics_key(String  google_analytics_key){
        this.google_analytics_key = google_analytics_key ;
        this.google_analytics_keyDirtyFlag = true ;
    }

    /**
     * 获取 [GOOGLE_ANALYTICS_KEY]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_analytics_keyDirtyFlag(){
        return google_analytics_keyDirtyFlag ;
    }

    /**
     * 获取 [MODULE_AUTH_LDAP]
     */
    @JsonProperty("module_auth_ldap")
    public String getModule_auth_ldap(){
        return module_auth_ldap ;
    }

    /**
     * 设置 [MODULE_AUTH_LDAP]
     */
    @JsonProperty("module_auth_ldap")
    public void setModule_auth_ldap(String  module_auth_ldap){
        this.module_auth_ldap = module_auth_ldap ;
        this.module_auth_ldapDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_AUTH_LDAP]脏标记
     */
    @JsonIgnore
    public boolean getModule_auth_ldapDirtyFlag(){
        return module_auth_ldapDirtyFlag ;
    }

    /**
     * 获取 [SPECIFIC_USER_ACCOUNT]
     */
    @JsonProperty("specific_user_account")
    public String getSpecific_user_account(){
        return specific_user_account ;
    }

    /**
     * 设置 [SPECIFIC_USER_ACCOUNT]
     */
    @JsonProperty("specific_user_account")
    public void setSpecific_user_account(String  specific_user_account){
        this.specific_user_account = specific_user_account ;
        this.specific_user_accountDirtyFlag = true ;
    }

    /**
     * 获取 [SPECIFIC_USER_ACCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getSpecific_user_accountDirtyFlag(){
        return specific_user_accountDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_HR_RECRUITMENT]
     */
    @JsonProperty("module_website_hr_recruitment")
    public String getModule_website_hr_recruitment(){
        return module_website_hr_recruitment ;
    }

    /**
     * 设置 [MODULE_WEBSITE_HR_RECRUITMENT]
     */
    @JsonProperty("module_website_hr_recruitment")
    public void setModule_website_hr_recruitment(String  module_website_hr_recruitment){
        this.module_website_hr_recruitment = module_website_hr_recruitment ;
        this.module_website_hr_recruitmentDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_HR_RECRUITMENT]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_hr_recruitmentDirtyFlag(){
        return module_website_hr_recruitmentDirtyFlag ;
    }

    /**
     * 获取 [MODULE_PROJECT_FORECAST]
     */
    @JsonProperty("module_project_forecast")
    public String getModule_project_forecast(){
        return module_project_forecast ;
    }

    /**
     * 设置 [MODULE_PROJECT_FORECAST]
     */
    @JsonProperty("module_project_forecast")
    public void setModule_project_forecast(String  module_project_forecast){
        this.module_project_forecast = module_project_forecast ;
        this.module_project_forecastDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_PROJECT_FORECAST]脏标记
     */
    @JsonIgnore
    public boolean getModule_project_forecastDirtyFlag(){
        return module_project_forecastDirtyFlag ;
    }

    /**
     * 获取 [GROUP_STOCK_TRACKING_OWNER]
     */
    @JsonProperty("group_stock_tracking_owner")
    public String getGroup_stock_tracking_owner(){
        return group_stock_tracking_owner ;
    }

    /**
     * 设置 [GROUP_STOCK_TRACKING_OWNER]
     */
    @JsonProperty("group_stock_tracking_owner")
    public void setGroup_stock_tracking_owner(String  group_stock_tracking_owner){
        this.group_stock_tracking_owner = group_stock_tracking_owner ;
        this.group_stock_tracking_ownerDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_STOCK_TRACKING_OWNER]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_tracking_ownerDirtyFlag(){
        return group_stock_tracking_ownerDirtyFlag ;
    }

    /**
     * 获取 [MODULE_GOOGLE_CALENDAR]
     */
    @JsonProperty("module_google_calendar")
    public String getModule_google_calendar(){
        return module_google_calendar ;
    }

    /**
     * 设置 [MODULE_GOOGLE_CALENDAR]
     */
    @JsonProperty("module_google_calendar")
    public void setModule_google_calendar(String  module_google_calendar){
        this.module_google_calendar = module_google_calendar ;
        this.module_google_calendarDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_GOOGLE_CALENDAR]脏标记
     */
    @JsonIgnore
    public boolean getModule_google_calendarDirtyFlag(){
        return module_google_calendarDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT]
     */
    @JsonProperty("module_account")
    public String getModule_account(){
        return module_account ;
    }

    /**
     * 设置 [MODULE_ACCOUNT]
     */
    @JsonProperty("module_account")
    public void setModule_account(String  module_account){
        this.module_account = module_account ;
        this.module_accountDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getModule_accountDirtyFlag(){
        return module_accountDirtyFlag ;
    }

    /**
     * 获取 [MODULE_GOOGLE_DRIVE]
     */
    @JsonProperty("module_google_drive")
    public String getModule_google_drive(){
        return module_google_drive ;
    }

    /**
     * 设置 [MODULE_GOOGLE_DRIVE]
     */
    @JsonProperty("module_google_drive")
    public void setModule_google_drive(String  module_google_drive){
        this.module_google_drive = module_google_drive ;
        this.module_google_driveDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_GOOGLE_DRIVE]脏标记
     */
    @JsonIgnore
    public boolean getModule_google_driveDirtyFlag(){
        return module_google_driveDirtyFlag ;
    }

    /**
     * 获取 [POS_PRICELIST_SETTING]
     */
    @JsonProperty("pos_pricelist_setting")
    public String getPos_pricelist_setting(){
        return pos_pricelist_setting ;
    }

    /**
     * 设置 [POS_PRICELIST_SETTING]
     */
    @JsonProperty("pos_pricelist_setting")
    public void setPos_pricelist_setting(String  pos_pricelist_setting){
        this.pos_pricelist_setting = pos_pricelist_setting ;
        this.pos_pricelist_settingDirtyFlag = true ;
    }

    /**
     * 获取 [POS_PRICELIST_SETTING]脏标记
     */
    @JsonIgnore
    public boolean getPos_pricelist_settingDirtyFlag(){
        return pos_pricelist_settingDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_SHARE_PARTNER]
     */
    @JsonProperty("company_share_partner")
    public String getCompany_share_partner(){
        return company_share_partner ;
    }

    /**
     * 设置 [COMPANY_SHARE_PARTNER]
     */
    @JsonProperty("company_share_partner")
    public void setCompany_share_partner(String  company_share_partner){
        this.company_share_partner = company_share_partner ;
        this.company_share_partnerDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_SHARE_PARTNER]脏标记
     */
    @JsonIgnore
    public boolean getCompany_share_partnerDirtyFlag(){
        return company_share_partnerDirtyFlag ;
    }

    /**
     * 获取 [MODULE_CURRENCY_RATE_LIVE]
     */
    @JsonProperty("module_currency_rate_live")
    public String getModule_currency_rate_live(){
        return module_currency_rate_live ;
    }

    /**
     * 设置 [MODULE_CURRENCY_RATE_LIVE]
     */
    @JsonProperty("module_currency_rate_live")
    public void setModule_currency_rate_live(String  module_currency_rate_live){
        this.module_currency_rate_live = module_currency_rate_live ;
        this.module_currency_rate_liveDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_CURRENCY_RATE_LIVE]脏标记
     */
    @JsonIgnore
    public boolean getModule_currency_rate_liveDirtyFlag(){
        return module_currency_rate_liveDirtyFlag ;
    }

    /**
     * 获取 [GROUP_PROFORMA_SALES]
     */
    @JsonProperty("group_proforma_sales")
    public String getGroup_proforma_sales(){
        return group_proforma_sales ;
    }

    /**
     * 设置 [GROUP_PROFORMA_SALES]
     */
    @JsonProperty("group_proforma_sales")
    public void setGroup_proforma_sales(String  group_proforma_sales){
        this.group_proforma_sales = group_proforma_sales ;
        this.group_proforma_salesDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_PROFORMA_SALES]脏标记
     */
    @JsonIgnore
    public boolean getGroup_proforma_salesDirtyFlag(){
        return group_proforma_salesDirtyFlag ;
    }

    /**
     * 获取 [MODULE_DELIVERY_FEDEX]
     */
    @JsonProperty("module_delivery_fedex")
    public String getModule_delivery_fedex(){
        return module_delivery_fedex ;
    }

    /**
     * 设置 [MODULE_DELIVERY_FEDEX]
     */
    @JsonProperty("module_delivery_fedex")
    public void setModule_delivery_fedex(String  module_delivery_fedex){
        this.module_delivery_fedex = module_delivery_fedex ;
        this.module_delivery_fedexDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_DELIVERY_FEDEX]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_fedexDirtyFlag(){
        return module_delivery_fedexDirtyFlag ;
    }

    /**
     * 获取 [MODULE_PRODUCT_EMAIL_TEMPLATE]
     */
    @JsonProperty("module_product_email_template")
    public String getModule_product_email_template(){
        return module_product_email_template ;
    }

    /**
     * 设置 [MODULE_PRODUCT_EMAIL_TEMPLATE]
     */
    @JsonProperty("module_product_email_template")
    public void setModule_product_email_template(String  module_product_email_template){
        this.module_product_email_template = module_product_email_template ;
        this.module_product_email_templateDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_PRODUCT_EMAIL_TEMPLATE]脏标记
     */
    @JsonIgnore
    public boolean getModule_product_email_templateDirtyFlag(){
        return module_product_email_templateDirtyFlag ;
    }

    /**
     * 获取 [SHOW_EFFECT]
     */
    @JsonProperty("show_effect")
    public String getShow_effect(){
        return show_effect ;
    }

    /**
     * 设置 [SHOW_EFFECT]
     */
    @JsonProperty("show_effect")
    public void setShow_effect(String  show_effect){
        this.show_effect = show_effect ;
        this.show_effectDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_EFFECT]脏标记
     */
    @JsonIgnore
    public boolean getShow_effectDirtyFlag(){
        return show_effectDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_PICKING_POLICY]
     */
    @JsonProperty("default_picking_policy")
    public String getDefault_picking_policy(){
        return default_picking_policy ;
    }

    /**
     * 设置 [DEFAULT_PICKING_POLICY]
     */
    @JsonProperty("default_picking_policy")
    public void setDefault_picking_policy(String  default_picking_policy){
        this.default_picking_policy = default_picking_policy ;
        this.default_picking_policyDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_PICKING_POLICY]脏标记
     */
    @JsonIgnore
    public boolean getDefault_picking_policyDirtyFlag(){
        return default_picking_policyDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_YOUTUBE]
     */
    @JsonProperty("social_youtube")
    public String getSocial_youtube(){
        return social_youtube ;
    }

    /**
     * 设置 [SOCIAL_YOUTUBE]
     */
    @JsonProperty("social_youtube")
    public void setSocial_youtube(String  social_youtube){
        this.social_youtube = social_youtube ;
        this.social_youtubeDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_YOUTUBE]脏标记
     */
    @JsonIgnore
    public boolean getSocial_youtubeDirtyFlag(){
        return social_youtubeDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_COMPANY_ID]
     */
    @JsonProperty("website_company_id")
    public Integer getWebsite_company_id(){
        return website_company_id ;
    }

    /**
     * 设置 [WEBSITE_COMPANY_ID]
     */
    @JsonProperty("website_company_id")
    public void setWebsite_company_id(Integer  website_company_id){
        this.website_company_id = website_company_id ;
        this.website_company_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_company_idDirtyFlag(){
        return website_company_idDirtyFlag ;
    }

    /**
     * 获取 [MODULE_MRP_BYPRODUCT]
     */
    @JsonProperty("module_mrp_byproduct")
    public String getModule_mrp_byproduct(){
        return module_mrp_byproduct ;
    }

    /**
     * 设置 [MODULE_MRP_BYPRODUCT]
     */
    @JsonProperty("module_mrp_byproduct")
    public void setModule_mrp_byproduct(String  module_mrp_byproduct){
        this.module_mrp_byproduct = module_mrp_byproduct ;
        this.module_mrp_byproductDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_MRP_BYPRODUCT]脏标记
     */
    @JsonIgnore
    public boolean getModule_mrp_byproductDirtyFlag(){
        return module_mrp_byproductDirtyFlag ;
    }

    /**
     * 获取 [MODULE_DELIVERY_USPS]
     */
    @JsonProperty("module_delivery_usps")
    public String getModule_delivery_usps(){
        return module_delivery_usps ;
    }

    /**
     * 设置 [MODULE_DELIVERY_USPS]
     */
    @JsonProperty("module_delivery_usps")
    public void setModule_delivery_usps(String  module_delivery_usps){
        this.module_delivery_usps = module_delivery_usps ;
        this.module_delivery_uspsDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_DELIVERY_USPS]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_uspsDirtyFlag(){
        return module_delivery_uspsDirtyFlag ;
    }

    /**
     * 获取 [MODULE_DELIVERY_DHL]
     */
    @JsonProperty("module_delivery_dhl")
    public String getModule_delivery_dhl(){
        return module_delivery_dhl ;
    }

    /**
     * 设置 [MODULE_DELIVERY_DHL]
     */
    @JsonProperty("module_delivery_dhl")
    public void setModule_delivery_dhl(String  module_delivery_dhl){
        this.module_delivery_dhl = module_delivery_dhl ;
        this.module_delivery_dhlDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_DELIVERY_DHL]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_dhlDirtyFlag(){
        return module_delivery_dhlDirtyFlag ;
    }

    /**
     * 获取 [GROUP_PROJECT_RATING]
     */
    @JsonProperty("group_project_rating")
    public String getGroup_project_rating(){
        return group_project_rating ;
    }

    /**
     * 设置 [GROUP_PROJECT_RATING]
     */
    @JsonProperty("group_project_rating")
    public void setGroup_project_rating(String  group_project_rating){
        this.group_project_rating = group_project_rating ;
        this.group_project_ratingDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_PROJECT_RATING]脏标记
     */
    @JsonIgnore
    public boolean getGroup_project_ratingDirtyFlag(){
        return group_project_ratingDirtyFlag ;
    }

    /**
     * 获取 [GOOGLE_MAPS_API_KEY]
     */
    @JsonProperty("google_maps_api_key")
    public String getGoogle_maps_api_key(){
        return google_maps_api_key ;
    }

    /**
     * 设置 [GOOGLE_MAPS_API_KEY]
     */
    @JsonProperty("google_maps_api_key")
    public void setGoogle_maps_api_key(String  google_maps_api_key){
        this.google_maps_api_key = google_maps_api_key ;
        this.google_maps_api_keyDirtyFlag = true ;
    }

    /**
     * 获取 [GOOGLE_MAPS_API_KEY]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_maps_api_keyDirtyFlag(){
        return google_maps_api_keyDirtyFlag ;
    }

    /**
     * 获取 [GROUP_USE_LEAD]
     */
    @JsonProperty("group_use_lead")
    public String getGroup_use_lead(){
        return group_use_lead ;
    }

    /**
     * 设置 [GROUP_USE_LEAD]
     */
    @JsonProperty("group_use_lead")
    public void setGroup_use_lead(String  group_use_lead){
        this.group_use_lead = group_use_lead ;
        this.group_use_leadDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_USE_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getGroup_use_leadDirtyFlag(){
        return group_use_leadDirtyFlag ;
    }

    /**
     * 获取 [GROUP_STOCK_TRACKING_LOT]
     */
    @JsonProperty("group_stock_tracking_lot")
    public String getGroup_stock_tracking_lot(){
        return group_stock_tracking_lot ;
    }

    /**
     * 设置 [GROUP_STOCK_TRACKING_LOT]
     */
    @JsonProperty("group_stock_tracking_lot")
    public void setGroup_stock_tracking_lot(String  group_stock_tracking_lot){
        this.group_stock_tracking_lot = group_stock_tracking_lot ;
        this.group_stock_tracking_lotDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_STOCK_TRACKING_LOT]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_tracking_lotDirtyFlag(){
        return group_stock_tracking_lotDirtyFlag ;
    }

    /**
     * 获取 [GROUP_STOCK_ADV_LOCATION]
     */
    @JsonProperty("group_stock_adv_location")
    public String getGroup_stock_adv_location(){
        return group_stock_adv_location ;
    }

    /**
     * 设置 [GROUP_STOCK_ADV_LOCATION]
     */
    @JsonProperty("group_stock_adv_location")
    public void setGroup_stock_adv_location(String  group_stock_adv_location){
        this.group_stock_adv_location = group_stock_adv_location ;
        this.group_stock_adv_locationDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_STOCK_ADV_LOCATION]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_adv_locationDirtyFlag(){
        return group_stock_adv_locationDirtyFlag ;
    }

    /**
     * 获取 [POS_SALES_PRICE]
     */
    @JsonProperty("pos_sales_price")
    public String getPos_sales_price(){
        return pos_sales_price ;
    }

    /**
     * 设置 [POS_SALES_PRICE]
     */
    @JsonProperty("pos_sales_price")
    public void setPos_sales_price(String  pos_sales_price){
        this.pos_sales_price = pos_sales_price ;
        this.pos_sales_priceDirtyFlag = true ;
    }

    /**
     * 获取 [POS_SALES_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getPos_sales_priceDirtyFlag(){
        return pos_sales_priceDirtyFlag ;
    }

    /**
     * 获取 [HAS_CHART_OF_ACCOUNTS]
     */
    @JsonProperty("has_chart_of_accounts")
    public String getHas_chart_of_accounts(){
        return has_chart_of_accounts ;
    }

    /**
     * 设置 [HAS_CHART_OF_ACCOUNTS]
     */
    @JsonProperty("has_chart_of_accounts")
    public void setHas_chart_of_accounts(String  has_chart_of_accounts){
        this.has_chart_of_accounts = has_chart_of_accounts ;
        this.has_chart_of_accountsDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_CHART_OF_ACCOUNTS]脏标记
     */
    @JsonIgnore
    public boolean getHas_chart_of_accountsDirtyFlag(){
        return has_chart_of_accountsDirtyFlag ;
    }

    /**
     * 获取 [MULTI_SALES_PRICE_METHOD]
     */
    @JsonProperty("multi_sales_price_method")
    public String getMulti_sales_price_method(){
        return multi_sales_price_method ;
    }

    /**
     * 设置 [MULTI_SALES_PRICE_METHOD]
     */
    @JsonProperty("multi_sales_price_method")
    public void setMulti_sales_price_method(String  multi_sales_price_method){
        this.multi_sales_price_method = multi_sales_price_method ;
        this.multi_sales_price_methodDirtyFlag = true ;
    }

    /**
     * 获取 [MULTI_SALES_PRICE_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getMulti_sales_price_methodDirtyFlag(){
        return multi_sales_price_methodDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MODULE_PROCUREMENT_JIT]
     */
    @JsonProperty("module_procurement_jit")
    public String getModule_procurement_jit(){
        return module_procurement_jit ;
    }

    /**
     * 设置 [MODULE_PROCUREMENT_JIT]
     */
    @JsonProperty("module_procurement_jit")
    public void setModule_procurement_jit(String  module_procurement_jit){
        this.module_procurement_jit = module_procurement_jit ;
        this.module_procurement_jitDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_PROCUREMENT_JIT]脏标记
     */
    @JsonIgnore
    public boolean getModule_procurement_jitDirtyFlag(){
        return module_procurement_jitDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_ASSET]
     */
    @JsonProperty("module_account_asset")
    public String getModule_account_asset(){
        return module_account_asset ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_ASSET]
     */
    @JsonProperty("module_account_asset")
    public void setModule_account_asset(String  module_account_asset){
        this.module_account_asset = module_account_asset ;
        this.module_account_assetDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_ASSET]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_assetDirtyFlag(){
        return module_account_assetDirtyFlag ;
    }

    /**
     * 获取 [USE_MAILGATEWAY]
     */
    @JsonProperty("use_mailgateway")
    public String getUse_mailgateway(){
        return use_mailgateway ;
    }

    /**
     * 设置 [USE_MAILGATEWAY]
     */
    @JsonProperty("use_mailgateway")
    public void setUse_mailgateway(String  use_mailgateway){
        this.use_mailgateway = use_mailgateway ;
        this.use_mailgatewayDirtyFlag = true ;
    }

    /**
     * 获取 [USE_MAILGATEWAY]脏标记
     */
    @JsonIgnore
    public boolean getUse_mailgatewayDirtyFlag(){
        return use_mailgatewayDirtyFlag ;
    }

    /**
     * 获取 [GROUP_SALE_PRICELIST]
     */
    @JsonProperty("group_sale_pricelist")
    public String getGroup_sale_pricelist(){
        return group_sale_pricelist ;
    }

    /**
     * 设置 [GROUP_SALE_PRICELIST]
     */
    @JsonProperty("group_sale_pricelist")
    public void setGroup_sale_pricelist(String  group_sale_pricelist){
        this.group_sale_pricelist = group_sale_pricelist ;
        this.group_sale_pricelistDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_SALE_PRICELIST]脏标记
     */
    @JsonIgnore
    public boolean getGroup_sale_pricelistDirtyFlag(){
        return group_sale_pricelistDirtyFlag ;
    }

    /**
     * 获取 [CRM_DEFAULT_TEAM_ID]
     */
    @JsonProperty("crm_default_team_id")
    public Integer getCrm_default_team_id(){
        return crm_default_team_id ;
    }

    /**
     * 设置 [CRM_DEFAULT_TEAM_ID]
     */
    @JsonProperty("crm_default_team_id")
    public void setCrm_default_team_id(Integer  crm_default_team_id){
        this.crm_default_team_id = crm_default_team_id ;
        this.crm_default_team_idDirtyFlag = true ;
    }

    /**
     * 获取 [CRM_DEFAULT_TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getCrm_default_team_idDirtyFlag(){
        return crm_default_team_idDirtyFlag ;
    }

    /**
     * 获取 [GROUP_STOCK_MULTI_LOCATIONS]
     */
    @JsonProperty("group_stock_multi_locations")
    public String getGroup_stock_multi_locations(){
        return group_stock_multi_locations ;
    }

    /**
     * 设置 [GROUP_STOCK_MULTI_LOCATIONS]
     */
    @JsonProperty("group_stock_multi_locations")
    public void setGroup_stock_multi_locations(String  group_stock_multi_locations){
        this.group_stock_multi_locations = group_stock_multi_locations ;
        this.group_stock_multi_locationsDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_STOCK_MULTI_LOCATIONS]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_multi_locationsDirtyFlag(){
        return group_stock_multi_locationsDirtyFlag ;
    }

    /**
     * 获取 [USE_MANUFACTURING_LEAD]
     */
    @JsonProperty("use_manufacturing_lead")
    public String getUse_manufacturing_lead(){
        return use_manufacturing_lead ;
    }

    /**
     * 设置 [USE_MANUFACTURING_LEAD]
     */
    @JsonProperty("use_manufacturing_lead")
    public void setUse_manufacturing_lead(String  use_manufacturing_lead){
        this.use_manufacturing_lead = use_manufacturing_lead ;
        this.use_manufacturing_leadDirtyFlag = true ;
    }

    /**
     * 获取 [USE_MANUFACTURING_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getUse_manufacturing_leadDirtyFlag(){
        return use_manufacturing_leadDirtyFlag ;
    }

    /**
     * 获取 [MODULE_GOOGLE_SPREADSHEET]
     */
    @JsonProperty("module_google_spreadsheet")
    public String getModule_google_spreadsheet(){
        return module_google_spreadsheet ;
    }

    /**
     * 设置 [MODULE_GOOGLE_SPREADSHEET]
     */
    @JsonProperty("module_google_spreadsheet")
    public void setModule_google_spreadsheet(String  module_google_spreadsheet){
        this.module_google_spreadsheet = module_google_spreadsheet ;
        this.module_google_spreadsheetDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_GOOGLE_SPREADSHEET]脏标记
     */
    @JsonIgnore
    public boolean getModule_google_spreadsheetDirtyFlag(){
        return module_google_spreadsheetDirtyFlag ;
    }

    /**
     * 获取 [SHOW_LINE_SUBTOTALS_TAX_SELECTION]
     */
    @JsonProperty("show_line_subtotals_tax_selection")
    public String getShow_line_subtotals_tax_selection(){
        return show_line_subtotals_tax_selection ;
    }

    /**
     * 设置 [SHOW_LINE_SUBTOTALS_TAX_SELECTION]
     */
    @JsonProperty("show_line_subtotals_tax_selection")
    public void setShow_line_subtotals_tax_selection(String  show_line_subtotals_tax_selection){
        this.show_line_subtotals_tax_selection = show_line_subtotals_tax_selection ;
        this.show_line_subtotals_tax_selectionDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_LINE_SUBTOTALS_TAX_SELECTION]脏标记
     */
    @JsonIgnore
    public boolean getShow_line_subtotals_tax_selectionDirtyFlag(){
        return show_line_subtotals_tax_selectionDirtyFlag ;
    }

    /**
     * 获取 [LANGUAGE_IDS]
     */
    @JsonProperty("language_ids")
    public String getLanguage_ids(){
        return language_ids ;
    }

    /**
     * 设置 [LANGUAGE_IDS]
     */
    @JsonProperty("language_ids")
    public void setLanguage_ids(String  language_ids){
        this.language_ids = language_ids ;
        this.language_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LANGUAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLanguage_idsDirtyFlag(){
        return language_idsDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_SALE_DELIVERY]
     */
    @JsonProperty("module_website_sale_delivery")
    public String getModule_website_sale_delivery(){
        return module_website_sale_delivery ;
    }

    /**
     * 设置 [MODULE_WEBSITE_SALE_DELIVERY]
     */
    @JsonProperty("module_website_sale_delivery")
    public void setModule_website_sale_delivery(String  module_website_sale_delivery){
        this.module_website_sale_delivery = module_website_sale_delivery ;
        this.module_website_sale_deliveryDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_SALE_DELIVERY]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_sale_deliveryDirtyFlag(){
        return module_website_sale_deliveryDirtyFlag ;
    }

    /**
     * 获取 [MODULE_MRP_MPS]
     */
    @JsonProperty("module_mrp_mps")
    public String getModule_mrp_mps(){
        return module_mrp_mps ;
    }

    /**
     * 设置 [MODULE_MRP_MPS]
     */
    @JsonProperty("module_mrp_mps")
    public void setModule_mrp_mps(String  module_mrp_mps){
        this.module_mrp_mps = module_mrp_mps ;
        this.module_mrp_mpsDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_MRP_MPS]脏标记
     */
    @JsonIgnore
    public boolean getModule_mrp_mpsDirtyFlag(){
        return module_mrp_mpsDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_AUTOCOMPLETE_INSUFFICIENT_CREDIT]
     */
    @JsonProperty("partner_autocomplete_insufficient_credit")
    public String getPartner_autocomplete_insufficient_credit(){
        return partner_autocomplete_insufficient_credit ;
    }

    /**
     * 设置 [PARTNER_AUTOCOMPLETE_INSUFFICIENT_CREDIT]
     */
    @JsonProperty("partner_autocomplete_insufficient_credit")
    public void setPartner_autocomplete_insufficient_credit(String  partner_autocomplete_insufficient_credit){
        this.partner_autocomplete_insufficient_credit = partner_autocomplete_insufficient_credit ;
        this.partner_autocomplete_insufficient_creditDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_AUTOCOMPLETE_INSUFFICIENT_CREDIT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_autocomplete_insufficient_creditDirtyFlag(){
        return partner_autocomplete_insufficient_creditDirtyFlag ;
    }

    /**
     * 获取 [MODULE_HR_ORG_CHART]
     */
    @JsonProperty("module_hr_org_chart")
    public String getModule_hr_org_chart(){
        return module_hr_org_chart ;
    }

    /**
     * 设置 [MODULE_HR_ORG_CHART]
     */
    @JsonProperty("module_hr_org_chart")
    public void setModule_hr_org_chart(String  module_hr_org_chart){
        this.module_hr_org_chart = module_hr_org_chart ;
        this.module_hr_org_chartDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_HR_ORG_CHART]脏标记
     */
    @JsonIgnore
    public boolean getModule_hr_org_chartDirtyFlag(){
        return module_hr_org_chartDirtyFlag ;
    }

    /**
     * 获取 [MODULE_PRODUCT_EXPIRY]
     */
    @JsonProperty("module_product_expiry")
    public String getModule_product_expiry(){
        return module_product_expiry ;
    }

    /**
     * 设置 [MODULE_PRODUCT_EXPIRY]
     */
    @JsonProperty("module_product_expiry")
    public void setModule_product_expiry(String  module_product_expiry){
        this.module_product_expiry = module_product_expiry ;
        this.module_product_expiryDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_PRODUCT_EXPIRY]脏标记
     */
    @JsonIgnore
    public boolean getModule_product_expiryDirtyFlag(){
        return module_product_expiryDirtyFlag ;
    }

    /**
     * 获取 [MODULE_DELIVERY_BPOST]
     */
    @JsonProperty("module_delivery_bpost")
    public String getModule_delivery_bpost(){
        return module_delivery_bpost ;
    }

    /**
     * 设置 [MODULE_DELIVERY_BPOST]
     */
    @JsonProperty("module_delivery_bpost")
    public void setModule_delivery_bpost(String  module_delivery_bpost){
        this.module_delivery_bpost = module_delivery_bpost ;
        this.module_delivery_bpostDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_DELIVERY_BPOST]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_bpostDirtyFlag(){
        return module_delivery_bpostDirtyFlag ;
    }

    /**
     * 获取 [MODULE_STOCK_BARCODE]
     */
    @JsonProperty("module_stock_barcode")
    public String getModule_stock_barcode(){
        return module_stock_barcode ;
    }

    /**
     * 设置 [MODULE_STOCK_BARCODE]
     */
    @JsonProperty("module_stock_barcode")
    public void setModule_stock_barcode(String  module_stock_barcode){
        this.module_stock_barcode = module_stock_barcode ;
        this.module_stock_barcodeDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_STOCK_BARCODE]脏标记
     */
    @JsonIgnore
    public boolean getModule_stock_barcodeDirtyFlag(){
        return module_stock_barcodeDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_GITHUB]
     */
    @JsonProperty("social_github")
    public String getSocial_github(){
        return social_github ;
    }

    /**
     * 设置 [SOCIAL_GITHUB]
     */
    @JsonProperty("social_github")
    public void setSocial_github(String  social_github){
        this.social_github = social_github ;
        this.social_githubDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_GITHUB]脏标记
     */
    @JsonIgnore
    public boolean getSocial_githubDirtyFlag(){
        return social_githubDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_INTRASTAT]
     */
    @JsonProperty("module_account_intrastat")
    public String getModule_account_intrastat(){
        return module_account_intrastat ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_INTRASTAT]
     */
    @JsonProperty("module_account_intrastat")
    public void setModule_account_intrastat(String  module_account_intrastat){
        this.module_account_intrastat = module_account_intrastat ;
        this.module_account_intrastatDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_INTRASTAT]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_intrastatDirtyFlag(){
        return module_account_intrastatDirtyFlag ;
    }

    /**
     * 获取 [AUTO_DONE_SETTING]
     */
    @JsonProperty("auto_done_setting")
    public String getAuto_done_setting(){
        return auto_done_setting ;
    }

    /**
     * 设置 [AUTO_DONE_SETTING]
     */
    @JsonProperty("auto_done_setting")
    public void setAuto_done_setting(String  auto_done_setting){
        this.auto_done_setting = auto_done_setting ;
        this.auto_done_settingDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_DONE_SETTING]脏标记
     */
    @JsonIgnore
    public boolean getAuto_done_settingDirtyFlag(){
        return auto_done_settingDirtyFlag ;
    }

    /**
     * 获取 [AUTH_SIGNUP_UNINVITED]
     */
    @JsonProperty("auth_signup_uninvited")
    public String getAuth_signup_uninvited(){
        return auth_signup_uninvited ;
    }

    /**
     * 设置 [AUTH_SIGNUP_UNINVITED]
     */
    @JsonProperty("auth_signup_uninvited")
    public void setAuth_signup_uninvited(String  auth_signup_uninvited){
        this.auth_signup_uninvited = auth_signup_uninvited ;
        this.auth_signup_uninvitedDirtyFlag = true ;
    }

    /**
     * 获取 [AUTH_SIGNUP_UNINVITED]脏标记
     */
    @JsonIgnore
    public boolean getAuth_signup_uninvitedDirtyFlag(){
        return auth_signup_uninvitedDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_SHARE_PRODUCT]
     */
    @JsonProperty("company_share_product")
    public String getCompany_share_product(){
        return company_share_product ;
    }

    /**
     * 设置 [COMPANY_SHARE_PRODUCT]
     */
    @JsonProperty("company_share_product")
    public void setCompany_share_product(String  company_share_product){
        this.company_share_product = company_share_product ;
        this.company_share_productDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_SHARE_PRODUCT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_share_productDirtyFlag(){
        return company_share_productDirtyFlag ;
    }

    /**
     * 获取 [GROUP_SALE_ORDER_TEMPLATE]
     */
    @JsonProperty("group_sale_order_template")
    public String getGroup_sale_order_template(){
        return group_sale_order_template ;
    }

    /**
     * 设置 [GROUP_SALE_ORDER_TEMPLATE]
     */
    @JsonProperty("group_sale_order_template")
    public void setGroup_sale_order_template(String  group_sale_order_template){
        this.group_sale_order_template = group_sale_order_template ;
        this.group_sale_order_templateDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_SALE_ORDER_TEMPLATE]脏标记
     */
    @JsonIgnore
    public boolean getGroup_sale_order_templateDirtyFlag(){
        return group_sale_order_templateDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_SEPA_DIRECT_DEBIT]
     */
    @JsonProperty("module_account_sepa_direct_debit")
    public String getModule_account_sepa_direct_debit(){
        return module_account_sepa_direct_debit ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_SEPA_DIRECT_DEBIT]
     */
    @JsonProperty("module_account_sepa_direct_debit")
    public void setModule_account_sepa_direct_debit(String  module_account_sepa_direct_debit){
        this.module_account_sepa_direct_debit = module_account_sepa_direct_debit ;
        this.module_account_sepa_direct_debitDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_SEPA_DIRECT_DEBIT]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_sepa_direct_debitDirtyFlag(){
        return module_account_sepa_direct_debitDirtyFlag ;
    }

    /**
     * 获取 [USE_QUOTATION_VALIDITY_DAYS]
     */
    @JsonProperty("use_quotation_validity_days")
    public String getUse_quotation_validity_days(){
        return use_quotation_validity_days ;
    }

    /**
     * 设置 [USE_QUOTATION_VALIDITY_DAYS]
     */
    @JsonProperty("use_quotation_validity_days")
    public void setUse_quotation_validity_days(String  use_quotation_validity_days){
        this.use_quotation_validity_days = use_quotation_validity_days ;
        this.use_quotation_validity_daysDirtyFlag = true ;
    }

    /**
     * 获取 [USE_QUOTATION_VALIDITY_DAYS]脏标记
     */
    @JsonIgnore
    public boolean getUse_quotation_validity_daysDirtyFlag(){
        return use_quotation_validity_daysDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_REPORTS_FOLLOWUP]
     */
    @JsonProperty("module_account_reports_followup")
    public String getModule_account_reports_followup(){
        return module_account_reports_followup ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_REPORTS_FOLLOWUP]
     */
    @JsonProperty("module_account_reports_followup")
    public void setModule_account_reports_followup(String  module_account_reports_followup){
        this.module_account_reports_followup = module_account_reports_followup ;
        this.module_account_reports_followupDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_REPORTS_FOLLOWUP]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_reports_followupDirtyFlag(){
        return module_account_reports_followupDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BATCH_PAYMENT]
     */
    @JsonProperty("module_account_batch_payment")
    public String getModule_account_batch_payment(){
        return module_account_batch_payment ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_BATCH_PAYMENT]
     */
    @JsonProperty("module_account_batch_payment")
    public void setModule_account_batch_payment(String  module_account_batch_payment){
        this.module_account_batch_payment = module_account_batch_payment ;
        this.module_account_batch_paymentDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BATCH_PAYMENT]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_batch_paymentDirtyFlag(){
        return module_account_batch_paymentDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_TWITTER]
     */
    @JsonProperty("social_twitter")
    public String getSocial_twitter(){
        return social_twitter ;
    }

    /**
     * 设置 [SOCIAL_TWITTER]
     */
    @JsonProperty("social_twitter")
    public void setSocial_twitter(String  social_twitter){
        this.social_twitter = social_twitter ;
        this.social_twitterDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_TWITTER]脏标记
     */
    @JsonIgnore
    public boolean getSocial_twitterDirtyFlag(){
        return social_twitterDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BUDGET]
     */
    @JsonProperty("module_account_budget")
    public String getModule_account_budget(){
        return module_account_budget ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_BUDGET]
     */
    @JsonProperty("module_account_budget")
    public void setModule_account_budget(String  module_account_budget){
        this.module_account_budget = module_account_budget ;
        this.module_account_budgetDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BUDGET]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_budgetDirtyFlag(){
        return module_account_budgetDirtyFlag ;
    }

    /**
     * 获取 [GROUP_MRP_ROUTINGS]
     */
    @JsonProperty("group_mrp_routings")
    public String getGroup_mrp_routings(){
        return group_mrp_routings ;
    }

    /**
     * 设置 [GROUP_MRP_ROUTINGS]
     */
    @JsonProperty("group_mrp_routings")
    public void setGroup_mrp_routings(String  group_mrp_routings){
        this.group_mrp_routings = group_mrp_routings ;
        this.group_mrp_routingsDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_MRP_ROUTINGS]脏标记
     */
    @JsonIgnore
    public boolean getGroup_mrp_routingsDirtyFlag(){
        return group_mrp_routingsDirtyFlag ;
    }

    /**
     * 获取 [GROUP_CASH_ROUNDING]
     */
    @JsonProperty("group_cash_rounding")
    public String getGroup_cash_rounding(){
        return group_cash_rounding ;
    }

    /**
     * 设置 [GROUP_CASH_ROUNDING]
     */
    @JsonProperty("group_cash_rounding")
    public void setGroup_cash_rounding(String  group_cash_rounding){
        this.group_cash_rounding = group_cash_rounding ;
        this.group_cash_roundingDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_CASH_ROUNDING]脏标记
     */
    @JsonIgnore
    public boolean getGroup_cash_roundingDirtyFlag(){
        return group_cash_roundingDirtyFlag ;
    }

    /**
     * 获取 [MODULE_STOCK_LANDED_COSTS]
     */
    @JsonProperty("module_stock_landed_costs")
    public String getModule_stock_landed_costs(){
        return module_stock_landed_costs ;
    }

    /**
     * 设置 [MODULE_STOCK_LANDED_COSTS]
     */
    @JsonProperty("module_stock_landed_costs")
    public void setModule_stock_landed_costs(String  module_stock_landed_costs){
        this.module_stock_landed_costs = module_stock_landed_costs ;
        this.module_stock_landed_costsDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_STOCK_LANDED_COSTS]脏标记
     */
    @JsonIgnore
    public boolean getModule_stock_landed_costsDirtyFlag(){
        return module_stock_landed_costsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_NAME]
     */
    @JsonProperty("website_name")
    public String getWebsite_name(){
        return website_name ;
    }

    /**
     * 设置 [WEBSITE_NAME]
     */
    @JsonProperty("website_name")
    public void setWebsite_name(String  website_name){
        this.website_name = website_name ;
        this.website_nameDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_nameDirtyFlag(){
        return website_nameDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_SALE_STOCK]
     */
    @JsonProperty("module_website_sale_stock")
    public String getModule_website_sale_stock(){
        return module_website_sale_stock ;
    }

    /**
     * 设置 [MODULE_WEBSITE_SALE_STOCK]
     */
    @JsonProperty("module_website_sale_stock")
    public void setModule_website_sale_stock(String  module_website_sale_stock){
        this.module_website_sale_stock = module_website_sale_stock ;
        this.module_website_sale_stockDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_SALE_STOCK]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_sale_stockDirtyFlag(){
        return module_website_sale_stockDirtyFlag ;
    }

    /**
     * 获取 [GROUP_WEBSITE_POPUP_ON_EXIT]
     */
    @JsonProperty("group_website_popup_on_exit")
    public String getGroup_website_popup_on_exit(){
        return group_website_popup_on_exit ;
    }

    /**
     * 设置 [GROUP_WEBSITE_POPUP_ON_EXIT]
     */
    @JsonProperty("group_website_popup_on_exit")
    public void setGroup_website_popup_on_exit(String  group_website_popup_on_exit){
        this.group_website_popup_on_exit = group_website_popup_on_exit ;
        this.group_website_popup_on_exitDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_WEBSITE_POPUP_ON_EXIT]脏标记
     */
    @JsonIgnore
    public boolean getGroup_website_popup_on_exitDirtyFlag(){
        return group_website_popup_on_exitDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_EVENT_TRACK]
     */
    @JsonProperty("module_website_event_track")
    public String getModule_website_event_track(){
        return module_website_event_track ;
    }

    /**
     * 设置 [MODULE_WEBSITE_EVENT_TRACK]
     */
    @JsonProperty("module_website_event_track")
    public void setModule_website_event_track(String  module_website_event_track){
        this.module_website_event_track = module_website_event_track ;
        this.module_website_event_trackDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_EVENT_TRACK]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_event_trackDirtyFlag(){
        return module_website_event_trackDirtyFlag ;
    }

    /**
     * 获取 [GROUP_MANAGE_VENDOR_PRICE]
     */
    @JsonProperty("group_manage_vendor_price")
    public String getGroup_manage_vendor_price(){
        return group_manage_vendor_price ;
    }

    /**
     * 设置 [GROUP_MANAGE_VENDOR_PRICE]
     */
    @JsonProperty("group_manage_vendor_price")
    public void setGroup_manage_vendor_price(String  group_manage_vendor_price){
        this.group_manage_vendor_price = group_manage_vendor_price ;
        this.group_manage_vendor_priceDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_MANAGE_VENDOR_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getGroup_manage_vendor_priceDirtyFlag(){
        return group_manage_vendor_priceDirtyFlag ;
    }

    /**
     * 获取 [MODULE_DELIVERY]
     */
    @JsonProperty("module_delivery")
    public String getModule_delivery(){
        return module_delivery ;
    }

    /**
     * 设置 [MODULE_DELIVERY]
     */
    @JsonProperty("module_delivery")
    public void setModule_delivery(String  module_delivery){
        this.module_delivery = module_delivery ;
        this.module_deliveryDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_DELIVERY]脏标记
     */
    @JsonIgnore
    public boolean getModule_deliveryDirtyFlag(){
        return module_deliveryDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_INVOICE_EXTRACT]
     */
    @JsonProperty("module_account_invoice_extract")
    public String getModule_account_invoice_extract(){
        return module_account_invoice_extract ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_INVOICE_EXTRACT]
     */
    @JsonProperty("module_account_invoice_extract")
    public void setModule_account_invoice_extract(String  module_account_invoice_extract){
        this.module_account_invoice_extract = module_account_invoice_extract ;
        this.module_account_invoice_extractDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_INVOICE_EXTRACT]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_invoice_extractDirtyFlag(){
        return module_account_invoice_extractDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_ID]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return channel_id ;
    }

    /**
     * 设置 [CHANNEL_ID]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return channel_idDirtyFlag ;
    }

    /**
     * 获取 [GROUP_WARNING_SALE]
     */
    @JsonProperty("group_warning_sale")
    public String getGroup_warning_sale(){
        return group_warning_sale ;
    }

    /**
     * 设置 [GROUP_WARNING_SALE]
     */
    @JsonProperty("group_warning_sale")
    public void setGroup_warning_sale(String  group_warning_sale){
        this.group_warning_sale = group_warning_sale ;
        this.group_warning_saleDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_WARNING_SALE]脏标记
     */
    @JsonIgnore
    public boolean getGroup_warning_saleDirtyFlag(){
        return group_warning_saleDirtyFlag ;
    }

    /**
     * 获取 [CDN_URL]
     */
    @JsonProperty("cdn_url")
    public String getCdn_url(){
        return cdn_url ;
    }

    /**
     * 设置 [CDN_URL]
     */
    @JsonProperty("cdn_url")
    public void setCdn_url(String  cdn_url){
        this.cdn_url = cdn_url ;
        this.cdn_urlDirtyFlag = true ;
    }

    /**
     * 获取 [CDN_URL]脏标记
     */
    @JsonIgnore
    public boolean getCdn_urlDirtyFlag(){
        return cdn_urlDirtyFlag ;
    }

    /**
     * 获取 [MODULE_EVENT_BARCODE]
     */
    @JsonProperty("module_event_barcode")
    public String getModule_event_barcode(){
        return module_event_barcode ;
    }

    /**
     * 设置 [MODULE_EVENT_BARCODE]
     */
    @JsonProperty("module_event_barcode")
    public void setModule_event_barcode(String  module_event_barcode){
        this.module_event_barcode = module_event_barcode ;
        this.module_event_barcodeDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_EVENT_BARCODE]脏标记
     */
    @JsonIgnore
    public boolean getModule_event_barcodeDirtyFlag(){
        return module_event_barcodeDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return alias_domain ;
    }

    /**
     * 设置 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return alias_domainDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_LINKEDIN]
     */
    @JsonProperty("social_linkedin")
    public String getSocial_linkedin(){
        return social_linkedin ;
    }

    /**
     * 设置 [SOCIAL_LINKEDIN]
     */
    @JsonProperty("social_linkedin")
    public void setSocial_linkedin(String  social_linkedin){
        this.social_linkedin = social_linkedin ;
        this.social_linkedinDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_LINKEDIN]脏标记
     */
    @JsonIgnore
    public boolean getSocial_linkedinDirtyFlag(){
        return social_linkedinDirtyFlag ;
    }

    /**
     * 获取 [GROUP_STOCK_MULTI_WAREHOUSES]
     */
    @JsonProperty("group_stock_multi_warehouses")
    public String getGroup_stock_multi_warehouses(){
        return group_stock_multi_warehouses ;
    }

    /**
     * 设置 [GROUP_STOCK_MULTI_WAREHOUSES]
     */
    @JsonProperty("group_stock_multi_warehouses")
    public void setGroup_stock_multi_warehouses(String  group_stock_multi_warehouses){
        this.group_stock_multi_warehouses = group_stock_multi_warehouses ;
        this.group_stock_multi_warehousesDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_STOCK_MULTI_WAREHOUSES]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_multi_warehousesDirtyFlag(){
        return group_stock_multi_warehousesDirtyFlag ;
    }

    /**
     * 获取 [SALESPERSON_ID]
     */
    @JsonProperty("salesperson_id")
    public Integer getSalesperson_id(){
        return salesperson_id ;
    }

    /**
     * 设置 [SALESPERSON_ID]
     */
    @JsonProperty("salesperson_id")
    public void setSalesperson_id(Integer  salesperson_id){
        this.salesperson_id = salesperson_id ;
        this.salesperson_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALESPERSON_ID]脏标记
     */
    @JsonIgnore
    public boolean getSalesperson_idDirtyFlag(){
        return salesperson_idDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_REPORTS]
     */
    @JsonProperty("module_account_reports")
    public String getModule_account_reports(){
        return module_account_reports ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_REPORTS]
     */
    @JsonProperty("module_account_reports")
    public void setModule_account_reports(String  module_account_reports){
        this.module_account_reports = module_account_reports ;
        this.module_account_reportsDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_REPORTS]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_reportsDirtyFlag(){
        return module_account_reportsDirtyFlag ;
    }

    /**
     * 获取 [GROUP_PRODUCT_PRICELIST]
     */
    @JsonProperty("group_product_pricelist")
    public String getGroup_product_pricelist(){
        return group_product_pricelist ;
    }

    /**
     * 设置 [GROUP_PRODUCT_PRICELIST]
     */
    @JsonProperty("group_product_pricelist")
    public void setGroup_product_pricelist(String  group_product_pricelist){
        this.group_product_pricelist = group_product_pricelist ;
        this.group_product_pricelistDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_PRODUCT_PRICELIST]脏标记
     */
    @JsonIgnore
    public boolean getGroup_product_pricelistDirtyFlag(){
        return group_product_pricelistDirtyFlag ;
    }

    /**
     * 获取 [MODULE_CRM_PHONE_VALIDATION]
     */
    @JsonProperty("module_crm_phone_validation")
    public String getModule_crm_phone_validation(){
        return module_crm_phone_validation ;
    }

    /**
     * 设置 [MODULE_CRM_PHONE_VALIDATION]
     */
    @JsonProperty("module_crm_phone_validation")
    public void setModule_crm_phone_validation(String  module_crm_phone_validation){
        this.module_crm_phone_validation = module_crm_phone_validation ;
        this.module_crm_phone_validationDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_CRM_PHONE_VALIDATION]脏标记
     */
    @JsonIgnore
    public boolean getModule_crm_phone_validationDirtyFlag(){
        return module_crm_phone_validationDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_VERSION]
     */
    @JsonProperty("module_website_version")
    public String getModule_website_version(){
        return module_website_version ;
    }

    /**
     * 设置 [MODULE_WEBSITE_VERSION]
     */
    @JsonProperty("module_website_version")
    public void setModule_website_version(String  module_website_version){
        this.module_website_version = module_website_version ;
        this.module_website_versionDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_VERSION]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_versionDirtyFlag(){
        return module_website_versionDirtyFlag ;
    }

    /**
     * 获取 [MODULE_BASE_IMPORT]
     */
    @JsonProperty("module_base_import")
    public String getModule_base_import(){
        return module_base_import ;
    }

    /**
     * 设置 [MODULE_BASE_IMPORT]
     */
    @JsonProperty("module_base_import")
    public void setModule_base_import(String  module_base_import){
        this.module_base_import = module_base_import ;
        this.module_base_importDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_BASE_IMPORT]脏标记
     */
    @JsonIgnore
    public boolean getModule_base_importDirtyFlag(){
        return module_base_importDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CSV]
     */
    @JsonProperty("module_account_bank_statement_import_csv")
    public String getModule_account_bank_statement_import_csv(){
        return module_account_bank_statement_import_csv ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CSV]
     */
    @JsonProperty("module_account_bank_statement_import_csv")
    public void setModule_account_bank_statement_import_csv(String  module_account_bank_statement_import_csv){
        this.module_account_bank_statement_import_csv = module_account_bank_statement_import_csv ;
        this.module_account_bank_statement_import_csvDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CSV]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_bank_statement_import_csvDirtyFlag(){
        return module_account_bank_statement_import_csvDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_TAXCLOUD]
     */
    @JsonProperty("module_account_taxcloud")
    public String getModule_account_taxcloud(){
        return module_account_taxcloud ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_TAXCLOUD]
     */
    @JsonProperty("module_account_taxcloud")
    public void setModule_account_taxcloud(String  module_account_taxcloud){
        this.module_account_taxcloud = module_account_taxcloud ;
        this.module_account_taxcloudDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_TAXCLOUD]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_taxcloudDirtyFlag(){
        return module_account_taxcloudDirtyFlag ;
    }

    /**
     * 获取 [USE_SALE_NOTE]
     */
    @JsonProperty("use_sale_note")
    public String getUse_sale_note(){
        return use_sale_note ;
    }

    /**
     * 设置 [USE_SALE_NOTE]
     */
    @JsonProperty("use_sale_note")
    public void setUse_sale_note(String  use_sale_note){
        this.use_sale_note = use_sale_note ;
        this.use_sale_noteDirtyFlag = true ;
    }

    /**
     * 获取 [USE_SALE_NOTE]脏标记
     */
    @JsonIgnore
    public boolean getUse_sale_noteDirtyFlag(){
        return use_sale_noteDirtyFlag ;
    }

    /**
     * 获取 [CART_ABANDONED_DELAY]
     */
    @JsonProperty("cart_abandoned_delay")
    public Double getCart_abandoned_delay(){
        return cart_abandoned_delay ;
    }

    /**
     * 设置 [CART_ABANDONED_DELAY]
     */
    @JsonProperty("cart_abandoned_delay")
    public void setCart_abandoned_delay(Double  cart_abandoned_delay){
        this.cart_abandoned_delay = cart_abandoned_delay ;
        this.cart_abandoned_delayDirtyFlag = true ;
    }

    /**
     * 获取 [CART_ABANDONED_DELAY]脏标记
     */
    @JsonIgnore
    public boolean getCart_abandoned_delayDirtyFlag(){
        return cart_abandoned_delayDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_DOMAIN]
     */
    @JsonProperty("website_domain")
    public String getWebsite_domain(){
        return website_domain ;
    }

    /**
     * 设置 [WEBSITE_DOMAIN]
     */
    @JsonProperty("website_domain")
    public void setWebsite_domain(String  website_domain){
        this.website_domain = website_domain ;
        this.website_domainDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_domainDirtyFlag(){
        return website_domainDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_ACCOUNTANT]
     */
    @JsonProperty("module_account_accountant")
    public String getModule_account_accountant(){
        return module_account_accountant ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_ACCOUNTANT]
     */
    @JsonProperty("module_account_accountant")
    public void setModule_account_accountant(String  module_account_accountant){
        this.module_account_accountant = module_account_accountant ;
        this.module_account_accountantDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_ACCOUNTANT]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_accountantDirtyFlag(){
        return module_account_accountantDirtyFlag ;
    }

    /**
     * 获取 [MODULE_SALE_MARGIN]
     */
    @JsonProperty("module_sale_margin")
    public String getModule_sale_margin(){
        return module_sale_margin ;
    }

    /**
     * 设置 [MODULE_SALE_MARGIN]
     */
    @JsonProperty("module_sale_margin")
    public void setModule_sale_margin(String  module_sale_margin){
        this.module_sale_margin = module_sale_margin ;
        this.module_sale_marginDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_SALE_MARGIN]脏标记
     */
    @JsonIgnore
    public boolean getModule_sale_marginDirtyFlag(){
        return module_sale_marginDirtyFlag ;
    }

    /**
     * 获取 [DIGEST_EMAILS]
     */
    @JsonProperty("digest_emails")
    public String getDigest_emails(){
        return digest_emails ;
    }

    /**
     * 设置 [DIGEST_EMAILS]
     */
    @JsonProperty("digest_emails")
    public void setDigest_emails(String  digest_emails){
        this.digest_emails = digest_emails ;
        this.digest_emailsDirtyFlag = true ;
    }

    /**
     * 获取 [DIGEST_EMAILS]脏标记
     */
    @JsonIgnore
    public boolean getDigest_emailsDirtyFlag(){
        return digest_emailsDirtyFlag ;
    }

    /**
     * 获取 [MODULE_PAD]
     */
    @JsonProperty("module_pad")
    public String getModule_pad(){
        return module_pad ;
    }

    /**
     * 设置 [MODULE_PAD]
     */
    @JsonProperty("module_pad")
    public void setModule_pad(String  module_pad){
        this.module_pad = module_pad ;
        this.module_padDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_PAD]脏标记
     */
    @JsonIgnore
    public boolean getModule_padDirtyFlag(){
        return module_padDirtyFlag ;
    }

    /**
     * 获取 [GROUP_WARNING_ACCOUNT]
     */
    @JsonProperty("group_warning_account")
    public String getGroup_warning_account(){
        return group_warning_account ;
    }

    /**
     * 设置 [GROUP_WARNING_ACCOUNT]
     */
    @JsonProperty("group_warning_account")
    public void setGroup_warning_account(String  group_warning_account){
        this.group_warning_account = group_warning_account ;
        this.group_warning_accountDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_WARNING_ACCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getGroup_warning_accountDirtyFlag(){
        return group_warning_accountDirtyFlag ;
    }

    /**
     * 获取 [GROUP_DISPLAY_INCOTERM]
     */
    @JsonProperty("group_display_incoterm")
    public String getGroup_display_incoterm(){
        return group_display_incoterm ;
    }

    /**
     * 设置 [GROUP_DISPLAY_INCOTERM]
     */
    @JsonProperty("group_display_incoterm")
    public void setGroup_display_incoterm(String  group_display_incoterm){
        this.group_display_incoterm = group_display_incoterm ;
        this.group_display_incotermDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_DISPLAY_INCOTERM]脏标记
     */
    @JsonIgnore
    public boolean getGroup_display_incotermDirtyFlag(){
        return group_display_incotermDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_SALE_WISHLIST]
     */
    @JsonProperty("module_website_sale_wishlist")
    public String getModule_website_sale_wishlist(){
        return module_website_sale_wishlist ;
    }

    /**
     * 设置 [MODULE_WEBSITE_SALE_WISHLIST]
     */
    @JsonProperty("module_website_sale_wishlist")
    public void setModule_website_sale_wishlist(String  module_website_sale_wishlist){
        this.module_website_sale_wishlist = module_website_sale_wishlist ;
        this.module_website_sale_wishlistDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_SALE_WISHLIST]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_sale_wishlistDirtyFlag(){
        return module_website_sale_wishlistDirtyFlag ;
    }

    /**
     * 获取 [USER_DEFAULT_RIGHTS]
     */
    @JsonProperty("user_default_rights")
    public String getUser_default_rights(){
        return user_default_rights ;
    }

    /**
     * 设置 [USER_DEFAULT_RIGHTS]
     */
    @JsonProperty("user_default_rights")
    public void setUser_default_rights(String  user_default_rights){
        this.user_default_rights = user_default_rights ;
        this.user_default_rightsDirtyFlag = true ;
    }

    /**
     * 获取 [USER_DEFAULT_RIGHTS]脏标记
     */
    @JsonIgnore
    public boolean getUser_default_rightsDirtyFlag(){
        return user_default_rightsDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_PURCHASE_METHOD]
     */
    @JsonProperty("default_purchase_method")
    public String getDefault_purchase_method(){
        return default_purchase_method ;
    }

    /**
     * 设置 [DEFAULT_PURCHASE_METHOD]
     */
    @JsonProperty("default_purchase_method")
    public void setDefault_purchase_method(String  default_purchase_method){
        this.default_purchase_method = default_purchase_method ;
        this.default_purchase_methodDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_PURCHASE_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getDefault_purchase_methodDirtyFlag(){
        return default_purchase_methodDirtyFlag ;
    }

    /**
     * 获取 [GROUP_DELIVERY_INVOICE_ADDRESS]
     */
    @JsonProperty("group_delivery_invoice_address")
    public String getGroup_delivery_invoice_address(){
        return group_delivery_invoice_address ;
    }

    /**
     * 设置 [GROUP_DELIVERY_INVOICE_ADDRESS]
     */
    @JsonProperty("group_delivery_invoice_address")
    public void setGroup_delivery_invoice_address(String  group_delivery_invoice_address){
        this.group_delivery_invoice_address = group_delivery_invoice_address ;
        this.group_delivery_invoice_addressDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_DELIVERY_INVOICE_ADDRESS]脏标记
     */
    @JsonIgnore
    public boolean getGroup_delivery_invoice_addressDirtyFlag(){
        return group_delivery_invoice_addressDirtyFlag ;
    }

    /**
     * 获取 [GROUP_LOT_ON_DELIVERY_SLIP]
     */
    @JsonProperty("group_lot_on_delivery_slip")
    public String getGroup_lot_on_delivery_slip(){
        return group_lot_on_delivery_slip ;
    }

    /**
     * 设置 [GROUP_LOT_ON_DELIVERY_SLIP]
     */
    @JsonProperty("group_lot_on_delivery_slip")
    public void setGroup_lot_on_delivery_slip(String  group_lot_on_delivery_slip){
        this.group_lot_on_delivery_slip = group_lot_on_delivery_slip ;
        this.group_lot_on_delivery_slipDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_LOT_ON_DELIVERY_SLIP]脏标记
     */
    @JsonIgnore
    public boolean getGroup_lot_on_delivery_slipDirtyFlag(){
        return group_lot_on_delivery_slipDirtyFlag ;
    }

    /**
     * 获取 [MODULE_EVENT_SALE]
     */
    @JsonProperty("module_event_sale")
    public String getModule_event_sale(){
        return module_event_sale ;
    }

    /**
     * 设置 [MODULE_EVENT_SALE]
     */
    @JsonProperty("module_event_sale")
    public void setModule_event_sale(String  module_event_sale){
        this.module_event_sale = module_event_sale ;
        this.module_event_saleDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_EVENT_SALE]脏标记
     */
    @JsonIgnore
    public boolean getModule_event_saleDirtyFlag(){
        return module_event_saleDirtyFlag ;
    }

    /**
     * 获取 [GROUP_SHOW_LINE_SUBTOTALS_TAX_INCLUDED]
     */
    @JsonProperty("group_show_line_subtotals_tax_included")
    public String getGroup_show_line_subtotals_tax_included(){
        return group_show_line_subtotals_tax_included ;
    }

    /**
     * 设置 [GROUP_SHOW_LINE_SUBTOTALS_TAX_INCLUDED]
     */
    @JsonProperty("group_show_line_subtotals_tax_included")
    public void setGroup_show_line_subtotals_tax_included(String  group_show_line_subtotals_tax_included){
        this.group_show_line_subtotals_tax_included = group_show_line_subtotals_tax_included ;
        this.group_show_line_subtotals_tax_includedDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_SHOW_LINE_SUBTOTALS_TAX_INCLUDED]脏标记
     */
    @JsonIgnore
    public boolean getGroup_show_line_subtotals_tax_includedDirtyFlag(){
        return group_show_line_subtotals_tax_includedDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [GROUP_PRODUCT_VARIANT]
     */
    @JsonProperty("group_product_variant")
    public String getGroup_product_variant(){
        return group_product_variant ;
    }

    /**
     * 设置 [GROUP_PRODUCT_VARIANT]
     */
    @JsonProperty("group_product_variant")
    public void setGroup_product_variant(String  group_product_variant){
        this.group_product_variant = group_product_variant ;
        this.group_product_variantDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_PRODUCT_VARIANT]脏标记
     */
    @JsonIgnore
    public boolean getGroup_product_variantDirtyFlag(){
        return group_product_variantDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_SEPA]
     */
    @JsonProperty("module_account_sepa")
    public String getModule_account_sepa(){
        return module_account_sepa ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_SEPA]
     */
    @JsonProperty("module_account_sepa")
    public void setModule_account_sepa(String  module_account_sepa){
        this.module_account_sepa = module_account_sepa ;
        this.module_account_sepaDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_SEPA]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_sepaDirtyFlag(){
        return module_account_sepaDirtyFlag ;
    }

    /**
     * 获取 [GROUP_MULTI_CURRENCY]
     */
    @JsonProperty("group_multi_currency")
    public String getGroup_multi_currency(){
        return group_multi_currency ;
    }

    /**
     * 设置 [GROUP_MULTI_CURRENCY]
     */
    @JsonProperty("group_multi_currency")
    public void setGroup_multi_currency(String  group_multi_currency){
        this.group_multi_currency = group_multi_currency ;
        this.group_multi_currencyDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_MULTI_CURRENCY]脏标记
     */
    @JsonIgnore
    public boolean getGroup_multi_currencyDirtyFlag(){
        return group_multi_currencyDirtyFlag ;
    }

    /**
     * 获取 [GROUP_PRODUCTS_IN_BILLS]
     */
    @JsonProperty("group_products_in_bills")
    public String getGroup_products_in_bills(){
        return group_products_in_bills ;
    }

    /**
     * 设置 [GROUP_PRODUCTS_IN_BILLS]
     */
    @JsonProperty("group_products_in_bills")
    public void setGroup_products_in_bills(String  group_products_in_bills){
        this.group_products_in_bills = group_products_in_bills ;
        this.group_products_in_billsDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_PRODUCTS_IN_BILLS]脏标记
     */
    @JsonIgnore
    public boolean getGroup_products_in_billsDirtyFlag(){
        return group_products_in_billsDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ANALYTIC_ACCOUNTING]
     */
    @JsonProperty("group_analytic_accounting")
    public String getGroup_analytic_accounting(){
        return group_analytic_accounting ;
    }

    /**
     * 设置 [GROUP_ANALYTIC_ACCOUNTING]
     */
    @JsonProperty("group_analytic_accounting")
    public void setGroup_analytic_accounting(String  group_analytic_accounting){
        this.group_analytic_accounting = group_analytic_accounting ;
        this.group_analytic_accountingDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ANALYTIC_ACCOUNTING]脏标记
     */
    @JsonIgnore
    public boolean getGroup_analytic_accountingDirtyFlag(){
        return group_analytic_accountingDirtyFlag ;
    }

    /**
     * 获取 [GROUP_STOCK_PACKAGING]
     */
    @JsonProperty("group_stock_packaging")
    public String getGroup_stock_packaging(){
        return group_stock_packaging ;
    }

    /**
     * 设置 [GROUP_STOCK_PACKAGING]
     */
    @JsonProperty("group_stock_packaging")
    public void setGroup_stock_packaging(String  group_stock_packaging){
        this.group_stock_packaging = group_stock_packaging ;
        this.group_stock_packagingDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_STOCK_PACKAGING]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_packagingDirtyFlag(){
        return group_stock_packagingDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_SLIDE_GOOGLE_APP_KEY]
     */
    @JsonProperty("website_slide_google_app_key")
    public String getWebsite_slide_google_app_key(){
        return website_slide_google_app_key ;
    }

    /**
     * 设置 [WEBSITE_SLIDE_GOOGLE_APP_KEY]
     */
    @JsonProperty("website_slide_google_app_key")
    public void setWebsite_slide_google_app_key(String  website_slide_google_app_key){
        this.website_slide_google_app_key = website_slide_google_app_key ;
        this.website_slide_google_app_keyDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_SLIDE_GOOGLE_APP_KEY]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_slide_google_app_keyDirtyFlag(){
        return website_slide_google_app_keyDirtyFlag ;
    }

    /**
     * 获取 [PO_ORDER_APPROVAL]
     */
    @JsonProperty("po_order_approval")
    public String getPo_order_approval(){
        return po_order_approval ;
    }

    /**
     * 设置 [PO_ORDER_APPROVAL]
     */
    @JsonProperty("po_order_approval")
    public void setPo_order_approval(String  po_order_approval){
        this.po_order_approval = po_order_approval ;
        this.po_order_approvalDirtyFlag = true ;
    }

    /**
     * 获取 [PO_ORDER_APPROVAL]脏标记
     */
    @JsonIgnore
    public boolean getPo_order_approvalDirtyFlag(){
        return po_order_approvalDirtyFlag ;
    }

    /**
     * 获取 [IS_INSTALLED_SALE]
     */
    @JsonProperty("is_installed_sale")
    public String getIs_installed_sale(){
        return is_installed_sale ;
    }

    /**
     * 设置 [IS_INSTALLED_SALE]
     */
    @JsonProperty("is_installed_sale")
    public void setIs_installed_sale(String  is_installed_sale){
        this.is_installed_sale = is_installed_sale ;
        this.is_installed_saleDirtyFlag = true ;
    }

    /**
     * 获取 [IS_INSTALLED_SALE]脏标记
     */
    @JsonIgnore
    public boolean getIs_installed_saleDirtyFlag(){
        return is_installed_saleDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_PAYMENT]
     */
    @JsonProperty("module_account_payment")
    public String getModule_account_payment(){
        return module_account_payment ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_PAYMENT]
     */
    @JsonProperty("module_account_payment")
    public void setModule_account_payment(String  module_account_payment){
        this.module_account_payment = module_account_payment ;
        this.module_account_paymentDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_PAYMENT]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_paymentDirtyFlag(){
        return module_account_paymentDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ANALYTIC_TAGS]
     */
    @JsonProperty("group_analytic_tags")
    public String getGroup_analytic_tags(){
        return group_analytic_tags ;
    }

    /**
     * 设置 [GROUP_ANALYTIC_TAGS]
     */
    @JsonProperty("group_analytic_tags")
    public void setGroup_analytic_tags(String  group_analytic_tags){
        this.group_analytic_tags = group_analytic_tags ;
        this.group_analytic_tagsDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ANALYTIC_TAGS]脏标记
     */
    @JsonIgnore
    public boolean getGroup_analytic_tagsDirtyFlag(){
        return group_analytic_tagsDirtyFlag ;
    }

    /**
     * 获取 [GROUP_SALE_ORDER_DATES]
     */
    @JsonProperty("group_sale_order_dates")
    public String getGroup_sale_order_dates(){
        return group_sale_order_dates ;
    }

    /**
     * 设置 [GROUP_SALE_ORDER_DATES]
     */
    @JsonProperty("group_sale_order_dates")
    public void setGroup_sale_order_dates(String  group_sale_order_dates){
        this.group_sale_order_dates = group_sale_order_dates ;
        this.group_sale_order_datesDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_SALE_ORDER_DATES]脏标记
     */
    @JsonIgnore
    public boolean getGroup_sale_order_datesDirtyFlag(){
        return group_sale_order_datesDirtyFlag ;
    }

    /**
     * 获取 [MODULE_VOIP]
     */
    @JsonProperty("module_voip")
    public String getModule_voip(){
        return module_voip ;
    }

    /**
     * 设置 [MODULE_VOIP]
     */
    @JsonProperty("module_voip")
    public void setModule_voip(String  module_voip){
        this.module_voip = module_voip ;
        this.module_voipDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_VOIP]脏标记
     */
    @JsonIgnore
    public boolean getModule_voipDirtyFlag(){
        return module_voipDirtyFlag ;
    }

    /**
     * 获取 [CART_RECOVERY_MAIL_TEMPLATE]
     */
    @JsonProperty("cart_recovery_mail_template")
    public Integer getCart_recovery_mail_template(){
        return cart_recovery_mail_template ;
    }

    /**
     * 设置 [CART_RECOVERY_MAIL_TEMPLATE]
     */
    @JsonProperty("cart_recovery_mail_template")
    public void setCart_recovery_mail_template(Integer  cart_recovery_mail_template){
        this.cart_recovery_mail_template = cart_recovery_mail_template ;
        this.cart_recovery_mail_templateDirtyFlag = true ;
    }

    /**
     * 获取 [CART_RECOVERY_MAIL_TEMPLATE]脏标记
     */
    @JsonIgnore
    public boolean getCart_recovery_mail_templateDirtyFlag(){
        return cart_recovery_mail_templateDirtyFlag ;
    }

    /**
     * 获取 [GROUP_MULTI_WEBSITE]
     */
    @JsonProperty("group_multi_website")
    public String getGroup_multi_website(){
        return group_multi_website ;
    }

    /**
     * 设置 [GROUP_MULTI_WEBSITE]
     */
    @JsonProperty("group_multi_website")
    public void setGroup_multi_website(String  group_multi_website){
        this.group_multi_website = group_multi_website ;
        this.group_multi_websiteDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_MULTI_WEBSITE]脏标记
     */
    @JsonIgnore
    public boolean getGroup_multi_websiteDirtyFlag(){
        return group_multi_websiteDirtyFlag ;
    }

    /**
     * 获取 [MODULE_AUTH_OAUTH]
     */
    @JsonProperty("module_auth_oauth")
    public String getModule_auth_oauth(){
        return module_auth_oauth ;
    }

    /**
     * 设置 [MODULE_AUTH_OAUTH]
     */
    @JsonProperty("module_auth_oauth")
    public void setModule_auth_oauth(String  module_auth_oauth){
        this.module_auth_oauth = module_auth_oauth ;
        this.module_auth_oauthDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_AUTH_OAUTH]脏标记
     */
    @JsonIgnore
    public boolean getModule_auth_oauthDirtyFlag(){
        return module_auth_oauthDirtyFlag ;
    }

    /**
     * 获取 [SALE_DELIVERY_SETTINGS]
     */
    @JsonProperty("sale_delivery_settings")
    public String getSale_delivery_settings(){
        return sale_delivery_settings ;
    }

    /**
     * 设置 [SALE_DELIVERY_SETTINGS]
     */
    @JsonProperty("sale_delivery_settings")
    public void setSale_delivery_settings(String  sale_delivery_settings){
        this.sale_delivery_settings = sale_delivery_settings ;
        this.sale_delivery_settingsDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_DELIVERY_SETTINGS]脏标记
     */
    @JsonIgnore
    public boolean getSale_delivery_settingsDirtyFlag(){
        return sale_delivery_settingsDirtyFlag ;
    }

    /**
     * 获取 [MODULE_SALE_QUOTATION_BUILDER]
     */
    @JsonProperty("module_sale_quotation_builder")
    public String getModule_sale_quotation_builder(){
        return module_sale_quotation_builder ;
    }

    /**
     * 设置 [MODULE_SALE_QUOTATION_BUILDER]
     */
    @JsonProperty("module_sale_quotation_builder")
    public void setModule_sale_quotation_builder(String  module_sale_quotation_builder){
        this.module_sale_quotation_builder = module_sale_quotation_builder ;
        this.module_sale_quotation_builderDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_SALE_QUOTATION_BUILDER]脏标记
     */
    @JsonIgnore
    public boolean getModule_sale_quotation_builderDirtyFlag(){
        return module_sale_quotation_builderDirtyFlag ;
    }

    /**
     * 获取 [MODULE_INTER_COMPANY_RULES]
     */
    @JsonProperty("module_inter_company_rules")
    public String getModule_inter_company_rules(){
        return module_inter_company_rules ;
    }

    /**
     * 设置 [MODULE_INTER_COMPANY_RULES]
     */
    @JsonProperty("module_inter_company_rules")
    public void setModule_inter_company_rules(String  module_inter_company_rules){
        this.module_inter_company_rules = module_inter_company_rules ;
        this.module_inter_company_rulesDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_INTER_COMPANY_RULES]脏标记
     */
    @JsonIgnore
    public boolean getModule_inter_company_rulesDirtyFlag(){
        return module_inter_company_rulesDirtyFlag ;
    }

    /**
     * 获取 [USE_SECURITY_LEAD]
     */
    @JsonProperty("use_security_lead")
    public String getUse_security_lead(){
        return use_security_lead ;
    }

    /**
     * 设置 [USE_SECURITY_LEAD]
     */
    @JsonProperty("use_security_lead")
    public void setUse_security_lead(String  use_security_lead){
        this.use_security_lead = use_security_lead ;
        this.use_security_leadDirtyFlag = true ;
    }

    /**
     * 获取 [USE_SECURITY_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getUse_security_leadDirtyFlag(){
        return use_security_leadDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_INVOICE_POLICY]
     */
    @JsonProperty("default_invoice_policy")
    public String getDefault_invoice_policy(){
        return default_invoice_policy ;
    }

    /**
     * 设置 [DEFAULT_INVOICE_POLICY]
     */
    @JsonProperty("default_invoice_policy")
    public void setDefault_invoice_policy(String  default_invoice_policy){
        this.default_invoice_policy = default_invoice_policy ;
        this.default_invoice_policyDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_INVOICE_POLICY]脏标记
     */
    @JsonIgnore
    public boolean getDefault_invoice_policyDirtyFlag(){
        return default_invoice_policyDirtyFlag ;
    }

    /**
     * 获取 [MULTI_SALES_PRICE]
     */
    @JsonProperty("multi_sales_price")
    public String getMulti_sales_price(){
        return multi_sales_price ;
    }

    /**
     * 设置 [MULTI_SALES_PRICE]
     */
    @JsonProperty("multi_sales_price")
    public void setMulti_sales_price(String  multi_sales_price){
        this.multi_sales_price = multi_sales_price ;
        this.multi_sales_priceDirtyFlag = true ;
    }

    /**
     * 获取 [MULTI_SALES_PRICE]脏标记
     */
    @JsonIgnore
    public boolean getMulti_sales_priceDirtyFlag(){
        return multi_sales_priceDirtyFlag ;
    }

    /**
     * 获取 [LOCK_CONFIRMED_PO]
     */
    @JsonProperty("lock_confirmed_po")
    public String getLock_confirmed_po(){
        return lock_confirmed_po ;
    }

    /**
     * 设置 [LOCK_CONFIRMED_PO]
     */
    @JsonProperty("lock_confirmed_po")
    public void setLock_confirmed_po(String  lock_confirmed_po){
        this.lock_confirmed_po = lock_confirmed_po ;
        this.lock_confirmed_poDirtyFlag = true ;
    }

    /**
     * 获取 [LOCK_CONFIRMED_PO]脏标记
     */
    @JsonIgnore
    public boolean getLock_confirmed_poDirtyFlag(){
        return lock_confirmed_poDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_WEIGHT_IN_LBS]
     */
    @JsonProperty("product_weight_in_lbs")
    public String getProduct_weight_in_lbs(){
        return product_weight_in_lbs ;
    }

    /**
     * 设置 [PRODUCT_WEIGHT_IN_LBS]
     */
    @JsonProperty("product_weight_in_lbs")
    public void setProduct_weight_in_lbs(String  product_weight_in_lbs){
        this.product_weight_in_lbs = product_weight_in_lbs ;
        this.product_weight_in_lbsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_WEIGHT_IN_LBS]脏标记
     */
    @JsonIgnore
    public boolean getProduct_weight_in_lbsDirtyFlag(){
        return product_weight_in_lbsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [HAS_GOOGLE_ANALYTICS_DASHBOARD]
     */
    @JsonProperty("has_google_analytics_dashboard")
    public String getHas_google_analytics_dashboard(){
        return has_google_analytics_dashboard ;
    }

    /**
     * 设置 [HAS_GOOGLE_ANALYTICS_DASHBOARD]
     */
    @JsonProperty("has_google_analytics_dashboard")
    public void setHas_google_analytics_dashboard(String  has_google_analytics_dashboard){
        this.has_google_analytics_dashboard = has_google_analytics_dashboard ;
        this.has_google_analytics_dashboardDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_GOOGLE_ANALYTICS_DASHBOARD]脏标记
     */
    @JsonIgnore
    public boolean getHas_google_analytics_dashboardDirtyFlag(){
        return has_google_analytics_dashboardDirtyFlag ;
    }

    /**
     * 获取 [MODULE_STOCK_PICKING_BATCH]
     */
    @JsonProperty("module_stock_picking_batch")
    public String getModule_stock_picking_batch(){
        return module_stock_picking_batch ;
    }

    /**
     * 设置 [MODULE_STOCK_PICKING_BATCH]
     */
    @JsonProperty("module_stock_picking_batch")
    public void setModule_stock_picking_batch(String  module_stock_picking_batch){
        this.module_stock_picking_batch = module_stock_picking_batch ;
        this.module_stock_picking_batchDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_STOCK_PICKING_BATCH]脏标记
     */
    @JsonIgnore
    public boolean getModule_stock_picking_batchDirtyFlag(){
        return module_stock_picking_batchDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_ALIAS_PREFIX]
     */
    @JsonProperty("expense_alias_prefix")
    public String getExpense_alias_prefix(){
        return expense_alias_prefix ;
    }

    /**
     * 设置 [EXPENSE_ALIAS_PREFIX]
     */
    @JsonProperty("expense_alias_prefix")
    public void setExpense_alias_prefix(String  expense_alias_prefix){
        this.expense_alias_prefix = expense_alias_prefix ;
        this.expense_alias_prefixDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_ALIAS_PREFIX]脏标记
     */
    @JsonIgnore
    public boolean getExpense_alias_prefixDirtyFlag(){
        return expense_alias_prefixDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_DEFERRED_REVENUE]
     */
    @JsonProperty("module_account_deferred_revenue")
    public String getModule_account_deferred_revenue(){
        return module_account_deferred_revenue ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_DEFERRED_REVENUE]
     */
    @JsonProperty("module_account_deferred_revenue")
    public void setModule_account_deferred_revenue(String  module_account_deferred_revenue){
        this.module_account_deferred_revenue = module_account_deferred_revenue ;
        this.module_account_deferred_revenueDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_DEFERRED_REVENUE]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_deferred_revenueDirtyFlag(){
        return module_account_deferred_revenueDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_FACEBOOK]
     */
    @JsonProperty("social_facebook")
    public String getSocial_facebook(){
        return social_facebook ;
    }

    /**
     * 设置 [SOCIAL_FACEBOOK]
     */
    @JsonProperty("social_facebook")
    public void setSocial_facebook(String  social_facebook){
        this.social_facebook = social_facebook ;
        this.social_facebookDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_FACEBOOK]脏标记
     */
    @JsonIgnore
    public boolean getSocial_facebookDirtyFlag(){
        return social_facebookDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEB_UNSPLASH]
     */
    @JsonProperty("module_web_unsplash")
    public String getModule_web_unsplash(){
        return module_web_unsplash ;
    }

    /**
     * 设置 [MODULE_WEB_UNSPLASH]
     */
    @JsonProperty("module_web_unsplash")
    public void setModule_web_unsplash(String  module_web_unsplash){
        this.module_web_unsplash = module_web_unsplash ;
        this.module_web_unsplashDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEB_UNSPLASH]脏标记
     */
    @JsonIgnore
    public boolean getModule_web_unsplashDirtyFlag(){
        return module_web_unsplashDirtyFlag ;
    }

    /**
     * 获取 [GROUP_MASS_MAILING_CAMPAIGN]
     */
    @JsonProperty("group_mass_mailing_campaign")
    public String getGroup_mass_mailing_campaign(){
        return group_mass_mailing_campaign ;
    }

    /**
     * 设置 [GROUP_MASS_MAILING_CAMPAIGN]
     */
    @JsonProperty("group_mass_mailing_campaign")
    public void setGroup_mass_mailing_campaign(String  group_mass_mailing_campaign){
        this.group_mass_mailing_campaign = group_mass_mailing_campaign ;
        this.group_mass_mailing_campaignDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_MASS_MAILING_CAMPAIGN]脏标记
     */
    @JsonIgnore
    public boolean getGroup_mass_mailing_campaignDirtyFlag(){
        return group_mass_mailing_campaignDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CAMT]
     */
    @JsonProperty("module_account_bank_statement_import_camt")
    public String getModule_account_bank_statement_import_camt(){
        return module_account_bank_statement_import_camt ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CAMT]
     */
    @JsonProperty("module_account_bank_statement_import_camt")
    public void setModule_account_bank_statement_import_camt(String  module_account_bank_statement_import_camt){
        this.module_account_bank_statement_import_camt = module_account_bank_statement_import_camt ;
        this.module_account_bank_statement_import_camtDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_BANK_STATEMENT_IMPORT_CAMT]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_bank_statement_import_camtDirtyFlag(){
        return module_account_bank_statement_import_camtDirtyFlag ;
    }

    /**
     * 获取 [MODULE_PRODUCT_MARGIN]
     */
    @JsonProperty("module_product_margin")
    public String getModule_product_margin(){
        return module_product_margin ;
    }

    /**
     * 设置 [MODULE_PRODUCT_MARGIN]
     */
    @JsonProperty("module_product_margin")
    public void setModule_product_margin(String  module_product_margin){
        this.module_product_margin = module_product_margin ;
        this.module_product_marginDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_PRODUCT_MARGIN]脏标记
     */
    @JsonIgnore
    public boolean getModule_product_marginDirtyFlag(){
        return module_product_marginDirtyFlag ;
    }

    /**
     * 获取 [GROUP_SUBTASK_PROJECT]
     */
    @JsonProperty("group_subtask_project")
    public String getGroup_subtask_project(){
        return group_subtask_project ;
    }

    /**
     * 设置 [GROUP_SUBTASK_PROJECT]
     */
    @JsonProperty("group_subtask_project")
    public void setGroup_subtask_project(String  group_subtask_project){
        this.group_subtask_project = group_subtask_project ;
        this.group_subtask_projectDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_SUBTASK_PROJECT]脏标记
     */
    @JsonIgnore
    public boolean getGroup_subtask_projectDirtyFlag(){
        return group_subtask_projectDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_3WAY_MATCH]
     */
    @JsonProperty("module_account_3way_match")
    public String getModule_account_3way_match(){
        return module_account_3way_match ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_3WAY_MATCH]
     */
    @JsonProperty("module_account_3way_match")
    public void setModule_account_3way_match(String  module_account_3way_match){
        this.module_account_3way_match = module_account_3way_match ;
        this.module_account_3way_matchDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_3WAY_MATCH]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_3way_matchDirtyFlag(){
        return module_account_3way_matchDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_SALE_DIGITAL]
     */
    @JsonProperty("module_website_sale_digital")
    public String getModule_website_sale_digital(){
        return module_website_sale_digital ;
    }

    /**
     * 设置 [MODULE_WEBSITE_SALE_DIGITAL]
     */
    @JsonProperty("module_website_sale_digital")
    public void setModule_website_sale_digital(String  module_website_sale_digital){
        this.module_website_sale_digital = module_website_sale_digital ;
        this.module_website_sale_digitalDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_SALE_DIGITAL]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_sale_digitalDirtyFlag(){
        return module_website_sale_digitalDirtyFlag ;
    }

    /**
     * 获取 [MODULE_SALE_COUPON]
     */
    @JsonProperty("module_sale_coupon")
    public String getModule_sale_coupon(){
        return module_sale_coupon ;
    }

    /**
     * 设置 [MODULE_SALE_COUPON]
     */
    @JsonProperty("module_sale_coupon")
    public void setModule_sale_coupon(String  module_sale_coupon){
        this.module_sale_coupon = module_sale_coupon ;
        this.module_sale_couponDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_SALE_COUPON]脏标记
     */
    @JsonIgnore
    public boolean getModule_sale_couponDirtyFlag(){
        return module_sale_couponDirtyFlag ;
    }

    /**
     * 获取 [SHOW_BLACKLIST_BUTTONS]
     */
    @JsonProperty("show_blacklist_buttons")
    public String getShow_blacklist_buttons(){
        return show_blacklist_buttons ;
    }

    /**
     * 设置 [SHOW_BLACKLIST_BUTTONS]
     */
    @JsonProperty("show_blacklist_buttons")
    public void setShow_blacklist_buttons(String  show_blacklist_buttons){
        this.show_blacklist_buttons = show_blacklist_buttons ;
        this.show_blacklist_buttonsDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_BLACKLIST_BUTTONS]脏标记
     */
    @JsonIgnore
    public boolean getShow_blacklist_buttonsDirtyFlag(){
        return show_blacklist_buttonsDirtyFlag ;
    }

    /**
     * 获取 [GROUP_WARNING_STOCK]
     */
    @JsonProperty("group_warning_stock")
    public String getGroup_warning_stock(){
        return group_warning_stock ;
    }

    /**
     * 设置 [GROUP_WARNING_STOCK]
     */
    @JsonProperty("group_warning_stock")
    public void setGroup_warning_stock(String  group_warning_stock){
        this.group_warning_stock = group_warning_stock ;
        this.group_warning_stockDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_WARNING_STOCK]脏标记
     */
    @JsonIgnore
    public boolean getGroup_warning_stockDirtyFlag(){
        return group_warning_stockDirtyFlag ;
    }

    /**
     * 获取 [GROUP_MULTI_COMPANY]
     */
    @JsonProperty("group_multi_company")
    public String getGroup_multi_company(){
        return group_multi_company ;
    }

    /**
     * 设置 [GROUP_MULTI_COMPANY]
     */
    @JsonProperty("group_multi_company")
    public void setGroup_multi_company(String  group_multi_company){
        this.group_multi_company = group_multi_company ;
        this.group_multi_companyDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_MULTI_COMPANY]脏标记
     */
    @JsonIgnore
    public boolean getGroup_multi_companyDirtyFlag(){
        return group_multi_companyDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ATTENDANCE_USE_PIN]
     */
    @JsonProperty("group_attendance_use_pin")
    public String getGroup_attendance_use_pin(){
        return group_attendance_use_pin ;
    }

    /**
     * 设置 [GROUP_ATTENDANCE_USE_PIN]
     */
    @JsonProperty("group_attendance_use_pin")
    public void setGroup_attendance_use_pin(String  group_attendance_use_pin){
        this.group_attendance_use_pin = group_attendance_use_pin ;
        this.group_attendance_use_pinDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ATTENDANCE_USE_PIN]脏标记
     */
    @JsonIgnore
    public boolean getGroup_attendance_use_pinDirtyFlag(){
        return group_attendance_use_pinDirtyFlag ;
    }

    /**
     * 获取 [SALESTEAM_ID]
     */
    @JsonProperty("salesteam_id")
    public Integer getSalesteam_id(){
        return salesteam_id ;
    }

    /**
     * 设置 [SALESTEAM_ID]
     */
    @JsonProperty("salesteam_id")
    public void setSalesteam_id(Integer  salesteam_id){
        this.salesteam_id = salesteam_id ;
        this.salesteam_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALESTEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getSalesteam_idDirtyFlag(){
        return salesteam_idDirtyFlag ;
    }

    /**
     * 获取 [FAVICON]
     */
    @JsonProperty("favicon")
    public byte[] getFavicon(){
        return favicon ;
    }

    /**
     * 设置 [FAVICON]
     */
    @JsonProperty("favicon")
    public void setFavicon(byte[]  favicon){
        this.favicon = favicon ;
        this.faviconDirtyFlag = true ;
    }

    /**
     * 获取 [FAVICON]脏标记
     */
    @JsonIgnore
    public boolean getFaviconDirtyFlag(){
        return faviconDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_SALE_COMPARISON]
     */
    @JsonProperty("module_website_sale_comparison")
    public String getModule_website_sale_comparison(){
        return module_website_sale_comparison ;
    }

    /**
     * 设置 [MODULE_WEBSITE_SALE_COMPARISON]
     */
    @JsonProperty("module_website_sale_comparison")
    public void setModule_website_sale_comparison(String  module_website_sale_comparison){
        this.module_website_sale_comparison = module_website_sale_comparison ;
        this.module_website_sale_comparisonDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_SALE_COMPARISON]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_sale_comparisonDirtyFlag(){
        return module_website_sale_comparisonDirtyFlag ;
    }

    /**
     * 获取 [AVAILABLE_THRESHOLD]
     */
    @JsonProperty("available_threshold")
    public Double getAvailable_threshold(){
        return available_threshold ;
    }

    /**
     * 设置 [AVAILABLE_THRESHOLD]
     */
    @JsonProperty("available_threshold")
    public void setAvailable_threshold(Double  available_threshold){
        this.available_threshold = available_threshold ;
        this.available_thresholdDirtyFlag = true ;
    }

    /**
     * 获取 [AVAILABLE_THRESHOLD]脏标记
     */
    @JsonIgnore
    public boolean getAvailable_thresholdDirtyFlag(){
        return available_thresholdDirtyFlag ;
    }

    /**
     * 获取 [USE_PO_LEAD]
     */
    @JsonProperty("use_po_lead")
    public String getUse_po_lead(){
        return use_po_lead ;
    }

    /**
     * 设置 [USE_PO_LEAD]
     */
    @JsonProperty("use_po_lead")
    public void setUse_po_lead(String  use_po_lead){
        this.use_po_lead = use_po_lead ;
        this.use_po_leadDirtyFlag = true ;
    }

    /**
     * 获取 [USE_PO_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getUse_po_leadDirtyFlag(){
        return use_po_leadDirtyFlag ;
    }

    /**
     * 获取 [GROUP_FISCAL_YEAR]
     */
    @JsonProperty("group_fiscal_year")
    public String getGroup_fiscal_year(){
        return group_fiscal_year ;
    }

    /**
     * 设置 [GROUP_FISCAL_YEAR]
     */
    @JsonProperty("group_fiscal_year")
    public void setGroup_fiscal_year(String  group_fiscal_year){
        this.group_fiscal_year = group_fiscal_year ;
        this.group_fiscal_yearDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_FISCAL_YEAR]脏标记
     */
    @JsonIgnore
    public boolean getGroup_fiscal_yearDirtyFlag(){
        return group_fiscal_yearDirtyFlag ;
    }

    /**
     * 获取 [MODULE_HR_TIMESHEET]
     */
    @JsonProperty("module_hr_timesheet")
    public String getModule_hr_timesheet(){
        return module_hr_timesheet ;
    }

    /**
     * 设置 [MODULE_HR_TIMESHEET]
     */
    @JsonProperty("module_hr_timesheet")
    public void setModule_hr_timesheet(String  module_hr_timesheet){
        this.module_hr_timesheet = module_hr_timesheet ;
        this.module_hr_timesheetDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_HR_TIMESHEET]脏标记
     */
    @JsonIgnore
    public boolean getModule_hr_timesheetDirtyFlag(){
        return module_hr_timesheetDirtyFlag ;
    }

    /**
     * 获取 [MODULE_MRP_PLM]
     */
    @JsonProperty("module_mrp_plm")
    public String getModule_mrp_plm(){
        return module_mrp_plm ;
    }

    /**
     * 设置 [MODULE_MRP_PLM]
     */
    @JsonProperty("module_mrp_plm")
    public void setModule_mrp_plm(String  module_mrp_plm){
        this.module_mrp_plm = module_mrp_plm ;
        this.module_mrp_plmDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_MRP_PLM]脏标记
     */
    @JsonIgnore
    public boolean getModule_mrp_plmDirtyFlag(){
        return module_mrp_plmDirtyFlag ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_YODLEE]
     */
    @JsonProperty("module_account_yodlee")
    public String getModule_account_yodlee(){
        return module_account_yodlee ;
    }

    /**
     * 设置 [MODULE_ACCOUNT_YODLEE]
     */
    @JsonProperty("module_account_yodlee")
    public void setModule_account_yodlee(String  module_account_yodlee){
        this.module_account_yodlee = module_account_yodlee ;
        this.module_account_yodleeDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_ACCOUNT_YODLEE]脏标记
     */
    @JsonIgnore
    public boolean getModule_account_yodleeDirtyFlag(){
        return module_account_yodleeDirtyFlag ;
    }

    /**
     * 获取 [CDN_FILTERS]
     */
    @JsonProperty("cdn_filters")
    public String getCdn_filters(){
        return cdn_filters ;
    }

    /**
     * 设置 [CDN_FILTERS]
     */
    @JsonProperty("cdn_filters")
    public void setCdn_filters(String  cdn_filters){
        this.cdn_filters = cdn_filters ;
        this.cdn_filtersDirtyFlag = true ;
    }

    /**
     * 获取 [CDN_FILTERS]脏标记
     */
    @JsonIgnore
    public boolean getCdn_filtersDirtyFlag(){
        return cdn_filtersDirtyFlag ;
    }

    /**
     * 获取 [USE_PROPAGATION_MINIMUM_DELTA]
     */
    @JsonProperty("use_propagation_minimum_delta")
    public String getUse_propagation_minimum_delta(){
        return use_propagation_minimum_delta ;
    }

    /**
     * 设置 [USE_PROPAGATION_MINIMUM_DELTA]
     */
    @JsonProperty("use_propagation_minimum_delta")
    public void setUse_propagation_minimum_delta(String  use_propagation_minimum_delta){
        this.use_propagation_minimum_delta = use_propagation_minimum_delta ;
        this.use_propagation_minimum_deltaDirtyFlag = true ;
    }

    /**
     * 获取 [USE_PROPAGATION_MINIMUM_DELTA]脏标记
     */
    @JsonIgnore
    public boolean getUse_propagation_minimum_deltaDirtyFlag(){
        return use_propagation_minimum_deltaDirtyFlag ;
    }

    /**
     * 获取 [MODULE_DELIVERY_UPS]
     */
    @JsonProperty("module_delivery_ups")
    public String getModule_delivery_ups(){
        return module_delivery_ups ;
    }

    /**
     * 设置 [MODULE_DELIVERY_UPS]
     */
    @JsonProperty("module_delivery_ups")
    public void setModule_delivery_ups(String  module_delivery_ups){
        this.module_delivery_ups = module_delivery_ups ;
        this.module_delivery_upsDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_DELIVERY_UPS]脏标记
     */
    @JsonIgnore
    public boolean getModule_delivery_upsDirtyFlag(){
        return module_delivery_upsDirtyFlag ;
    }

    /**
     * 获取 [FAIL_COUNTER]
     */
    @JsonProperty("fail_counter")
    public Integer getFail_counter(){
        return fail_counter ;
    }

    /**
     * 设置 [FAIL_COUNTER]
     */
    @JsonProperty("fail_counter")
    public void setFail_counter(Integer  fail_counter){
        this.fail_counter = fail_counter ;
        this.fail_counterDirtyFlag = true ;
    }

    /**
     * 获取 [FAIL_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getFail_counterDirtyFlag(){
        return fail_counterDirtyFlag ;
    }

    /**
     * 获取 [GROUP_STOCK_PRODUCTION_LOT]
     */
    @JsonProperty("group_stock_production_lot")
    public String getGroup_stock_production_lot(){
        return group_stock_production_lot ;
    }

    /**
     * 设置 [GROUP_STOCK_PRODUCTION_LOT]
     */
    @JsonProperty("group_stock_production_lot")
    public void setGroup_stock_production_lot(String  group_stock_production_lot){
        this.group_stock_production_lot = group_stock_production_lot ;
        this.group_stock_production_lotDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_STOCK_PRODUCTION_LOT]脏标记
     */
    @JsonIgnore
    public boolean getGroup_stock_production_lotDirtyFlag(){
        return group_stock_production_lotDirtyFlag ;
    }

    /**
     * 获取 [HAS_ACCOUNTING_ENTRIES]
     */
    @JsonProperty("has_accounting_entries")
    public String getHas_accounting_entries(){
        return has_accounting_entries ;
    }

    /**
     * 设置 [HAS_ACCOUNTING_ENTRIES]
     */
    @JsonProperty("has_accounting_entries")
    public void setHas_accounting_entries(String  has_accounting_entries){
        this.has_accounting_entries = has_accounting_entries ;
        this.has_accounting_entriesDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_ACCOUNTING_ENTRIES]脏标记
     */
    @JsonIgnore
    public boolean getHas_accounting_entriesDirtyFlag(){
        return has_accounting_entriesDirtyFlag ;
    }

    /**
     * 获取 [GROUP_UOM]
     */
    @JsonProperty("group_uom")
    public String getGroup_uom(){
        return group_uom ;
    }

    /**
     * 设置 [GROUP_UOM]
     */
    @JsonProperty("group_uom")
    public void setGroup_uom(String  group_uom){
        this.group_uom = group_uom ;
        this.group_uomDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_UOM]脏标记
     */
    @JsonIgnore
    public boolean getGroup_uomDirtyFlag(){
        return group_uomDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_OUTGOING_MAIL_SERVER]
     */
    @JsonProperty("mass_mailing_outgoing_mail_server")
    public String getMass_mailing_outgoing_mail_server(){
        return mass_mailing_outgoing_mail_server ;
    }

    /**
     * 设置 [MASS_MAILING_OUTGOING_MAIL_SERVER]
     */
    @JsonProperty("mass_mailing_outgoing_mail_server")
    public void setMass_mailing_outgoing_mail_server(String  mass_mailing_outgoing_mail_server){
        this.mass_mailing_outgoing_mail_server = mass_mailing_outgoing_mail_server ;
        this.mass_mailing_outgoing_mail_serverDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_OUTGOING_MAIL_SERVER]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_outgoing_mail_serverDirtyFlag(){
        return mass_mailing_outgoing_mail_serverDirtyFlag ;
    }

    /**
     * 获取 [CRM_ALIAS_PREFIX]
     */
    @JsonProperty("crm_alias_prefix")
    public String getCrm_alias_prefix(){
        return crm_alias_prefix ;
    }

    /**
     * 设置 [CRM_ALIAS_PREFIX]
     */
    @JsonProperty("crm_alias_prefix")
    public void setCrm_alias_prefix(String  crm_alias_prefix){
        this.crm_alias_prefix = crm_alias_prefix ;
        this.crm_alias_prefixDirtyFlag = true ;
    }

    /**
     * 获取 [CRM_ALIAS_PREFIX]脏标记
     */
    @JsonIgnore
    public boolean getCrm_alias_prefixDirtyFlag(){
        return crm_alias_prefixDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_GOOGLEPLUS]
     */
    @JsonProperty("social_googleplus")
    public String getSocial_googleplus(){
        return social_googleplus ;
    }

    /**
     * 设置 [SOCIAL_GOOGLEPLUS]
     */
    @JsonProperty("social_googleplus")
    public void setSocial_googleplus(String  social_googleplus){
        this.social_googleplus = social_googleplus ;
        this.social_googleplusDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_GOOGLEPLUS]脏标记
     */
    @JsonIgnore
    public boolean getSocial_googleplusDirtyFlag(){
        return social_googleplusDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ROUTE_SO_LINES]
     */
    @JsonProperty("group_route_so_lines")
    public String getGroup_route_so_lines(){
        return group_route_so_lines ;
    }

    /**
     * 设置 [GROUP_ROUTE_SO_LINES]
     */
    @JsonProperty("group_route_so_lines")
    public void setGroup_route_so_lines(String  group_route_so_lines){
        this.group_route_so_lines = group_route_so_lines ;
        this.group_route_so_linesDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ROUTE_SO_LINES]脏标记
     */
    @JsonIgnore
    public boolean getGroup_route_so_linesDirtyFlag(){
        return group_route_so_linesDirtyFlag ;
    }

    /**
     * 获取 [GOOGLE_MANAGEMENT_CLIENT_SECRET]
     */
    @JsonProperty("google_management_client_secret")
    public String getGoogle_management_client_secret(){
        return google_management_client_secret ;
    }

    /**
     * 设置 [GOOGLE_MANAGEMENT_CLIENT_SECRET]
     */
    @JsonProperty("google_management_client_secret")
    public void setGoogle_management_client_secret(String  google_management_client_secret){
        this.google_management_client_secret = google_management_client_secret ;
        this.google_management_client_secretDirtyFlag = true ;
    }

    /**
     * 获取 [GOOGLE_MANAGEMENT_CLIENT_SECRET]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_management_client_secretDirtyFlag(){
        return google_management_client_secretDirtyFlag ;
    }

    /**
     * 获取 [SOCIAL_DEFAULT_IMAGE]
     */
    @JsonProperty("social_default_image")
    public byte[] getSocial_default_image(){
        return social_default_image ;
    }

    /**
     * 设置 [SOCIAL_DEFAULT_IMAGE]
     */
    @JsonProperty("social_default_image")
    public void setSocial_default_image(byte[]  social_default_image){
        this.social_default_image = social_default_image ;
        this.social_default_imageDirtyFlag = true ;
    }

    /**
     * 获取 [SOCIAL_DEFAULT_IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getSocial_default_imageDirtyFlag(){
        return social_default_imageDirtyFlag ;
    }

    /**
     * 获取 [HAS_GOOGLE_ANALYTICS]
     */
    @JsonProperty("has_google_analytics")
    public String getHas_google_analytics(){
        return has_google_analytics ;
    }

    /**
     * 设置 [HAS_GOOGLE_ANALYTICS]
     */
    @JsonProperty("has_google_analytics")
    public void setHas_google_analytics(String  has_google_analytics){
        this.has_google_analytics = has_google_analytics ;
        this.has_google_analyticsDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_GOOGLE_ANALYTICS]脏标记
     */
    @JsonIgnore
    public boolean getHas_google_analyticsDirtyFlag(){
        return has_google_analyticsDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_EVENT_QUESTIONS]
     */
    @JsonProperty("module_website_event_questions")
    public String getModule_website_event_questions(){
        return module_website_event_questions ;
    }

    /**
     * 设置 [MODULE_WEBSITE_EVENT_QUESTIONS]
     */
    @JsonProperty("module_website_event_questions")
    public void setModule_website_event_questions(String  module_website_event_questions){
        this.module_website_event_questions = module_website_event_questions ;
        this.module_website_event_questionsDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_EVENT_QUESTIONS]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_event_questionsDirtyFlag(){
        return module_website_event_questionsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_DEFAULT_LANG_ID]
     */
    @JsonProperty("website_default_lang_id")
    public Integer getWebsite_default_lang_id(){
        return website_default_lang_id ;
    }

    /**
     * 设置 [WEBSITE_DEFAULT_LANG_ID]
     */
    @JsonProperty("website_default_lang_id")
    public void setWebsite_default_lang_id(Integer  website_default_lang_id){
        this.website_default_lang_id = website_default_lang_id ;
        this.website_default_lang_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_DEFAULT_LANG_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_default_lang_idDirtyFlag(){
        return website_default_lang_idDirtyFlag ;
    }

    /**
     * 获取 [INVENTORY_AVAILABILITY]
     */
    @JsonProperty("inventory_availability")
    public String getInventory_availability(){
        return inventory_availability ;
    }

    /**
     * 设置 [INVENTORY_AVAILABILITY]
     */
    @JsonProperty("inventory_availability")
    public void setInventory_availability(String  inventory_availability){
        this.inventory_availability = inventory_availability ;
        this.inventory_availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [INVENTORY_AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getInventory_availabilityDirtyFlag(){
        return inventory_availabilityDirtyFlag ;
    }

    /**
     * 获取 [GROUP_WARNING_PURCHASE]
     */
    @JsonProperty("group_warning_purchase")
    public String getGroup_warning_purchase(){
        return group_warning_purchase ;
    }

    /**
     * 设置 [GROUP_WARNING_PURCHASE]
     */
    @JsonProperty("group_warning_purchase")
    public void setGroup_warning_purchase(String  group_warning_purchase){
        this.group_warning_purchase = group_warning_purchase ;
        this.group_warning_purchaseDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_WARNING_PURCHASE]脏标记
     */
    @JsonIgnore
    public boolean getGroup_warning_purchaseDirtyFlag(){
        return group_warning_purchaseDirtyFlag ;
    }

    /**
     * 获取 [MODULE_QUALITY_CONTROL]
     */
    @JsonProperty("module_quality_control")
    public String getModule_quality_control(){
        return module_quality_control ;
    }

    /**
     * 设置 [MODULE_QUALITY_CONTROL]
     */
    @JsonProperty("module_quality_control")
    public void setModule_quality_control(String  module_quality_control){
        this.module_quality_control = module_quality_control ;
        this.module_quality_controlDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_QUALITY_CONTROL]脏标记
     */
    @JsonIgnore
    public boolean getModule_quality_controlDirtyFlag(){
        return module_quality_controlDirtyFlag ;
    }

    /**
     * 获取 [GENERATE_LEAD_FROM_ALIAS]
     */
    @JsonProperty("generate_lead_from_alias")
    public String getGenerate_lead_from_alias(){
        return generate_lead_from_alias ;
    }

    /**
     * 设置 [GENERATE_LEAD_FROM_ALIAS]
     */
    @JsonProperty("generate_lead_from_alias")
    public void setGenerate_lead_from_alias(String  generate_lead_from_alias){
        this.generate_lead_from_alias = generate_lead_from_alias ;
        this.generate_lead_from_aliasDirtyFlag = true ;
    }

    /**
     * 获取 [GENERATE_LEAD_FROM_ALIAS]脏标记
     */
    @JsonIgnore
    public boolean getGenerate_lead_from_aliasDirtyFlag(){
        return generate_lead_from_aliasDirtyFlag ;
    }

    /**
     * 获取 [SALE_PRICELIST_SETTING]
     */
    @JsonProperty("sale_pricelist_setting")
    public String getSale_pricelist_setting(){
        return sale_pricelist_setting ;
    }

    /**
     * 设置 [SALE_PRICELIST_SETTING]
     */
    @JsonProperty("sale_pricelist_setting")
    public void setSale_pricelist_setting(String  sale_pricelist_setting){
        this.sale_pricelist_setting = sale_pricelist_setting ;
        this.sale_pricelist_settingDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_PRICELIST_SETTING]脏标记
     */
    @JsonIgnore
    public boolean getSale_pricelist_settingDirtyFlag(){
        return sale_pricelist_settingDirtyFlag ;
    }

    /**
     * 获取 [GROUP_PRICELIST_ITEM]
     */
    @JsonProperty("group_pricelist_item")
    public String getGroup_pricelist_item(){
        return group_pricelist_item ;
    }

    /**
     * 设置 [GROUP_PRICELIST_ITEM]
     */
    @JsonProperty("group_pricelist_item")
    public void setGroup_pricelist_item(String  group_pricelist_item){
        this.group_pricelist_item = group_pricelist_item ;
        this.group_pricelist_itemDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_PRICELIST_ITEM]脏标记
     */
    @JsonIgnore
    public boolean getGroup_pricelist_itemDirtyFlag(){
        return group_pricelist_itemDirtyFlag ;
    }

    /**
     * 获取 [EXTERNAL_EMAIL_SERVER_DEFAULT]
     */
    @JsonProperty("external_email_server_default")
    public String getExternal_email_server_default(){
        return external_email_server_default ;
    }

    /**
     * 设置 [EXTERNAL_EMAIL_SERVER_DEFAULT]
     */
    @JsonProperty("external_email_server_default")
    public void setExternal_email_server_default(String  external_email_server_default){
        this.external_email_server_default = external_email_server_default ;
        this.external_email_server_defaultDirtyFlag = true ;
    }

    /**
     * 获取 [EXTERNAL_EMAIL_SERVER_DEFAULT]脏标记
     */
    @JsonIgnore
    public boolean getExternal_email_server_defaultDirtyFlag(){
        return external_email_server_defaultDirtyFlag ;
    }

    /**
     * 获取 [UNSPLASH_ACCESS_KEY]
     */
    @JsonProperty("unsplash_access_key")
    public String getUnsplash_access_key(){
        return unsplash_access_key ;
    }

    /**
     * 设置 [UNSPLASH_ACCESS_KEY]
     */
    @JsonProperty("unsplash_access_key")
    public void setUnsplash_access_key(String  unsplash_access_key){
        this.unsplash_access_key = unsplash_access_key ;
        this.unsplash_access_keyDirtyFlag = true ;
    }

    /**
     * 获取 [UNSPLASH_ACCESS_KEY]脏标记
     */
    @JsonIgnore
    public boolean getUnsplash_access_keyDirtyFlag(){
        return unsplash_access_keyDirtyFlag ;
    }

    /**
     * 获取 [MODULE_BASE_GENGO]
     */
    @JsonProperty("module_base_gengo")
    public String getModule_base_gengo(){
        return module_base_gengo ;
    }

    /**
     * 设置 [MODULE_BASE_GENGO]
     */
    @JsonProperty("module_base_gengo")
    public void setModule_base_gengo(String  module_base_gengo){
        this.module_base_gengo = module_base_gengo ;
        this.module_base_gengoDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_BASE_GENGO]脏标记
     */
    @JsonIgnore
    public boolean getModule_base_gengoDirtyFlag(){
        return module_base_gengoDirtyFlag ;
    }

    /**
     * 获取 [MODULE_WEBSITE_EVENT_SALE]
     */
    @JsonProperty("module_website_event_sale")
    public String getModule_website_event_sale(){
        return module_website_event_sale ;
    }

    /**
     * 设置 [MODULE_WEBSITE_EVENT_SALE]
     */
    @JsonProperty("module_website_event_sale")
    public void setModule_website_event_sale(String  module_website_event_sale){
        this.module_website_event_sale = module_website_event_sale ;
        this.module_website_event_saleDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_WEBSITE_EVENT_SALE]脏标记
     */
    @JsonIgnore
    public boolean getModule_website_event_saleDirtyFlag(){
        return module_website_event_saleDirtyFlag ;
    }

    /**
     * 获取 [CRM_DEFAULT_USER_ID]
     */
    @JsonProperty("crm_default_user_id")
    public Integer getCrm_default_user_id(){
        return crm_default_user_id ;
    }

    /**
     * 设置 [CRM_DEFAULT_USER_ID]
     */
    @JsonProperty("crm_default_user_id")
    public void setCrm_default_user_id(Integer  crm_default_user_id){
        this.crm_default_user_id = crm_default_user_id ;
        this.crm_default_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [CRM_DEFAULT_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getCrm_default_user_idDirtyFlag(){
        return crm_default_user_idDirtyFlag ;
    }

    /**
     * 获取 [MODULE_STOCK_DROPSHIPPING]
     */
    @JsonProperty("module_stock_dropshipping")
    public String getModule_stock_dropshipping(){
        return module_stock_dropshipping ;
    }

    /**
     * 设置 [MODULE_STOCK_DROPSHIPPING]
     */
    @JsonProperty("module_stock_dropshipping")
    public void setModule_stock_dropshipping(String  module_stock_dropshipping){
        this.module_stock_dropshipping = module_stock_dropshipping ;
        this.module_stock_dropshippingDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_STOCK_DROPSHIPPING]脏标记
     */
    @JsonIgnore
    public boolean getModule_stock_dropshippingDirtyFlag(){
        return module_stock_dropshippingDirtyFlag ;
    }

    /**
     * 获取 [MODULE_CRM_REVEAL]
     */
    @JsonProperty("module_crm_reveal")
    public String getModule_crm_reveal(){
        return module_crm_reveal ;
    }

    /**
     * 设置 [MODULE_CRM_REVEAL]
     */
    @JsonProperty("module_crm_reveal")
    public void setModule_crm_reveal(String  module_crm_reveal){
        this.module_crm_reveal = module_crm_reveal ;
        this.module_crm_revealDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_CRM_REVEAL]脏标记
     */
    @JsonIgnore
    public boolean getModule_crm_revealDirtyFlag(){
        return module_crm_revealDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_MAIL_SERVER_ID]
     */
    @JsonProperty("mass_mailing_mail_server_id")
    public Integer getMass_mailing_mail_server_id(){
        return mass_mailing_mail_server_id ;
    }

    /**
     * 设置 [MASS_MAILING_MAIL_SERVER_ID]
     */
    @JsonProperty("mass_mailing_mail_server_id")
    public void setMass_mailing_mail_server_id(Integer  mass_mailing_mail_server_id){
        this.mass_mailing_mail_server_id = mass_mailing_mail_server_id ;
        this.mass_mailing_mail_server_idDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_MAIL_SERVER_ID]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_mail_server_idDirtyFlag(){
        return mass_mailing_mail_server_idDirtyFlag ;
    }

    /**
     * 获取 [GROUP_SHOW_LINE_SUBTOTALS_TAX_EXCLUDED]
     */
    @JsonProperty("group_show_line_subtotals_tax_excluded")
    public String getGroup_show_line_subtotals_tax_excluded(){
        return group_show_line_subtotals_tax_excluded ;
    }

    /**
     * 设置 [GROUP_SHOW_LINE_SUBTOTALS_TAX_EXCLUDED]
     */
    @JsonProperty("group_show_line_subtotals_tax_excluded")
    public void setGroup_show_line_subtotals_tax_excluded(String  group_show_line_subtotals_tax_excluded){
        this.group_show_line_subtotals_tax_excluded = group_show_line_subtotals_tax_excluded ;
        this.group_show_line_subtotals_tax_excludedDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_SHOW_LINE_SUBTOTALS_TAX_EXCLUDED]脏标记
     */
    @JsonIgnore
    public boolean getGroup_show_line_subtotals_tax_excludedDirtyFlag(){
        return group_show_line_subtotals_tax_excludedDirtyFlag ;
    }

    /**
     * 获取 [MODULE_HR_RECRUITMENT_SURVEY]
     */
    @JsonProperty("module_hr_recruitment_survey")
    public String getModule_hr_recruitment_survey(){
        return module_hr_recruitment_survey ;
    }

    /**
     * 设置 [MODULE_HR_RECRUITMENT_SURVEY]
     */
    @JsonProperty("module_hr_recruitment_survey")
    public void setModule_hr_recruitment_survey(String  module_hr_recruitment_survey){
        this.module_hr_recruitment_survey = module_hr_recruitment_survey ;
        this.module_hr_recruitment_surveyDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_HR_RECRUITMENT_SURVEY]脏标记
     */
    @JsonIgnore
    public boolean getModule_hr_recruitment_surveyDirtyFlag(){
        return module_hr_recruitment_surveyDirtyFlag ;
    }

    /**
     * 获取 [MODULE_MRP_WORKORDER]
     */
    @JsonProperty("module_mrp_workorder")
    public String getModule_mrp_workorder(){
        return module_mrp_workorder ;
    }

    /**
     * 设置 [MODULE_MRP_WORKORDER]
     */
    @JsonProperty("module_mrp_workorder")
    public void setModule_mrp_workorder(String  module_mrp_workorder){
        this.module_mrp_workorder = module_mrp_workorder ;
        this.module_mrp_workorderDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_MRP_WORKORDER]脏标记
     */
    @JsonIgnore
    public boolean getModule_mrp_workorderDirtyFlag(){
        return module_mrp_workorderDirtyFlag ;
    }

    /**
     * 获取 [MODULE_POS_MERCURY]
     */
    @JsonProperty("module_pos_mercury")
    public String getModule_pos_mercury(){
        return module_pos_mercury ;
    }

    /**
     * 设置 [MODULE_POS_MERCURY]
     */
    @JsonProperty("module_pos_mercury")
    public void setModule_pos_mercury(String  module_pos_mercury){
        this.module_pos_mercury = module_pos_mercury ;
        this.module_pos_mercuryDirtyFlag = true ;
    }

    /**
     * 获取 [MODULE_POS_MERCURY]脏标记
     */
    @JsonIgnore
    public boolean getModule_pos_mercuryDirtyFlag(){
        return module_pos_mercuryDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_REFERENCE_TYPE]
     */
    @JsonProperty("invoice_reference_type")
    public String getInvoice_reference_type(){
        return invoice_reference_type ;
    }

    /**
     * 设置 [INVOICE_REFERENCE_TYPE]
     */
    @JsonProperty("invoice_reference_type")
    public void setInvoice_reference_type(String  invoice_reference_type){
        this.invoice_reference_type = invoice_reference_type ;
        this.invoice_reference_typeDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_REFERENCE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_reference_typeDirtyFlag(){
        return invoice_reference_typeDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return company_currency_id ;
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return company_currency_idDirtyFlag ;
    }

    /**
     * 获取 [TAX_EXIGIBILITY]
     */
    @JsonProperty("tax_exigibility")
    public String getTax_exigibility(){
        return tax_exigibility ;
    }

    /**
     * 设置 [TAX_EXIGIBILITY]
     */
    @JsonProperty("tax_exigibility")
    public void setTax_exigibility(String  tax_exigibility){
        this.tax_exigibility = tax_exigibility ;
        this.tax_exigibilityDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_EXIGIBILITY]脏标记
     */
    @JsonIgnore
    public boolean getTax_exigibilityDirtyFlag(){
        return tax_exigibilityDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_BANK_RECONCILIATION_START]
     */
    @JsonProperty("account_bank_reconciliation_start")
    public Timestamp getAccount_bank_reconciliation_start(){
        return account_bank_reconciliation_start ;
    }

    /**
     * 设置 [ACCOUNT_BANK_RECONCILIATION_START]
     */
    @JsonProperty("account_bank_reconciliation_start")
    public void setAccount_bank_reconciliation_start(Timestamp  account_bank_reconciliation_start){
        this.account_bank_reconciliation_start = account_bank_reconciliation_start ;
        this.account_bank_reconciliation_startDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_BANK_RECONCILIATION_START]脏标记
     */
    @JsonIgnore
    public boolean getAccount_bank_reconciliation_startDirtyFlag(){
        return account_bank_reconciliation_startDirtyFlag ;
    }

    /**
     * 获取 [TAX_CASH_BASIS_JOURNAL_ID]
     */
    @JsonProperty("tax_cash_basis_journal_id")
    public Integer getTax_cash_basis_journal_id(){
        return tax_cash_basis_journal_id ;
    }

    /**
     * 设置 [TAX_CASH_BASIS_JOURNAL_ID]
     */
    @JsonProperty("tax_cash_basis_journal_id")
    public void setTax_cash_basis_journal_id(Integer  tax_cash_basis_journal_id){
        this.tax_cash_basis_journal_id = tax_cash_basis_journal_id ;
        this.tax_cash_basis_journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_CASH_BASIS_JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getTax_cash_basis_journal_idDirtyFlag(){
        return tax_cash_basis_journal_idDirtyFlag ;
    }

    /**
     * 获取 [PO_LEAD]
     */
    @JsonProperty("po_lead")
    public Double getPo_lead(){
        return po_lead ;
    }

    /**
     * 设置 [PO_LEAD]
     */
    @JsonProperty("po_lead")
    public void setPo_lead(Double  po_lead){
        this.po_lead = po_lead ;
        this.po_leadDirtyFlag = true ;
    }

    /**
     * 获取 [PO_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getPo_leadDirtyFlag(){
        return po_leadDirtyFlag ;
    }

    /**
     * 获取 [SNAILMAIL_COLOR]
     */
    @JsonProperty("snailmail_color")
    public String getSnailmail_color(){
        return snailmail_color ;
    }

    /**
     * 设置 [SNAILMAIL_COLOR]
     */
    @JsonProperty("snailmail_color")
    public void setSnailmail_color(String  snailmail_color){
        this.snailmail_color = snailmail_color ;
        this.snailmail_colorDirtyFlag = true ;
    }

    /**
     * 获取 [SNAILMAIL_COLOR]脏标记
     */
    @JsonIgnore
    public boolean getSnailmail_colorDirtyFlag(){
        return snailmail_colorDirtyFlag ;
    }

    /**
     * 获取 [PAPERFORMAT_ID]
     */
    @JsonProperty("paperformat_id")
    public Integer getPaperformat_id(){
        return paperformat_id ;
    }

    /**
     * 设置 [PAPERFORMAT_ID]
     */
    @JsonProperty("paperformat_id")
    public void setPaperformat_id(Integer  paperformat_id){
        this.paperformat_id = paperformat_id ;
        this.paperformat_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAPERFORMAT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPaperformat_idDirtyFlag(){
        return paperformat_idDirtyFlag ;
    }

    /**
     * 获取 [PORTAL_CONFIRMATION_SIGN]
     */
    @JsonProperty("portal_confirmation_sign")
    public String getPortal_confirmation_sign(){
        return portal_confirmation_sign ;
    }

    /**
     * 设置 [PORTAL_CONFIRMATION_SIGN]
     */
    @JsonProperty("portal_confirmation_sign")
    public void setPortal_confirmation_sign(String  portal_confirmation_sign){
        this.portal_confirmation_sign = portal_confirmation_sign ;
        this.portal_confirmation_signDirtyFlag = true ;
    }

    /**
     * 获取 [PORTAL_CONFIRMATION_SIGN]脏标记
     */
    @JsonIgnore
    public boolean getPortal_confirmation_signDirtyFlag(){
        return portal_confirmation_signDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [DIGEST_ID_TEXT]
     */
    @JsonProperty("digest_id_text")
    public String getDigest_id_text(){
        return digest_id_text ;
    }

    /**
     * 设置 [DIGEST_ID_TEXT]
     */
    @JsonProperty("digest_id_text")
    public void setDigest_id_text(String  digest_id_text){
        this.digest_id_text = digest_id_text ;
        this.digest_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DIGEST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDigest_id_textDirtyFlag(){
        return digest_id_textDirtyFlag ;
    }

    /**
     * 获取 [SALE_NOTE]
     */
    @JsonProperty("sale_note")
    public String getSale_note(){
        return sale_note ;
    }

    /**
     * 设置 [SALE_NOTE]
     */
    @JsonProperty("sale_note")
    public void setSale_note(String  sale_note){
        this.sale_note = sale_note ;
        this.sale_noteDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_NOTE]脏标记
     */
    @JsonIgnore
    public boolean getSale_noteDirtyFlag(){
        return sale_noteDirtyFlag ;
    }

    /**
     * 获取 [AUTH_SIGNUP_TEMPLATE_USER_ID_TEXT]
     */
    @JsonProperty("auth_signup_template_user_id_text")
    public String getAuth_signup_template_user_id_text(){
        return auth_signup_template_user_id_text ;
    }

    /**
     * 设置 [AUTH_SIGNUP_TEMPLATE_USER_ID_TEXT]
     */
    @JsonProperty("auth_signup_template_user_id_text")
    public void setAuth_signup_template_user_id_text(String  auth_signup_template_user_id_text){
        this.auth_signup_template_user_id_text = auth_signup_template_user_id_text ;
        this.auth_signup_template_user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [AUTH_SIGNUP_TEMPLATE_USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAuth_signup_template_user_id_textDirtyFlag(){
        return auth_signup_template_user_id_textDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IS_PRINT]
     */
    @JsonProperty("invoice_is_print")
    public String getInvoice_is_print(){
        return invoice_is_print ;
    }

    /**
     * 设置 [INVOICE_IS_PRINT]
     */
    @JsonProperty("invoice_is_print")
    public void setInvoice_is_print(String  invoice_is_print){
        this.invoice_is_print = invoice_is_print ;
        this.invoice_is_printDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IS_PRINT]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_printDirtyFlag(){
        return invoice_is_printDirtyFlag ;
    }

    /**
     * 获取 [DEPOSIT_DEFAULT_PRODUCT_ID_TEXT]
     */
    @JsonProperty("deposit_default_product_id_text")
    public String getDeposit_default_product_id_text(){
        return deposit_default_product_id_text ;
    }

    /**
     * 设置 [DEPOSIT_DEFAULT_PRODUCT_ID_TEXT]
     */
    @JsonProperty("deposit_default_product_id_text")
    public void setDeposit_default_product_id_text(String  deposit_default_product_id_text){
        this.deposit_default_product_id_text = deposit_default_product_id_text ;
        this.deposit_default_product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEPOSIT_DEFAULT_PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDeposit_default_product_id_textDirtyFlag(){
        return deposit_default_product_id_textDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return resource_calendar_id ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return resource_calendar_idDirtyFlag ;
    }

    /**
     * 获取 [PO_LOCK]
     */
    @JsonProperty("po_lock")
    public String getPo_lock(){
        return po_lock ;
    }

    /**
     * 设置 [PO_LOCK]
     */
    @JsonProperty("po_lock")
    public void setPo_lock(String  po_lock){
        this.po_lock = po_lock ;
        this.po_lockDirtyFlag = true ;
    }

    /**
     * 获取 [PO_LOCK]脏标记
     */
    @JsonIgnore
    public boolean getPo_lockDirtyFlag(){
        return po_lockDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IS_SNAILMAIL]
     */
    @JsonProperty("invoice_is_snailmail")
    public String getInvoice_is_snailmail(){
        return invoice_is_snailmail ;
    }

    /**
     * 设置 [INVOICE_IS_SNAILMAIL]
     */
    @JsonProperty("invoice_is_snailmail")
    public void setInvoice_is_snailmail(String  invoice_is_snailmail){
        this.invoice_is_snailmail = invoice_is_snailmail ;
        this.invoice_is_snailmailDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IS_SNAILMAIL]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_snailmailDirtyFlag(){
        return invoice_is_snailmailDirtyFlag ;
    }

    /**
     * 获取 [MANUFACTURING_LEAD]
     */
    @JsonProperty("manufacturing_lead")
    public Double getManufacturing_lead(){
        return manufacturing_lead ;
    }

    /**
     * 设置 [MANUFACTURING_LEAD]
     */
    @JsonProperty("manufacturing_lead")
    public void setManufacturing_lead(Double  manufacturing_lead){
        this.manufacturing_lead = manufacturing_lead ;
        this.manufacturing_leadDirtyFlag = true ;
    }

    /**
     * 获取 [MANUFACTURING_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getManufacturing_leadDirtyFlag(){
        return manufacturing_leadDirtyFlag ;
    }

    /**
     * 获取 [PO_DOUBLE_VALIDATION]
     */
    @JsonProperty("po_double_validation")
    public String getPo_double_validation(){
        return po_double_validation ;
    }

    /**
     * 设置 [PO_DOUBLE_VALIDATION]
     */
    @JsonProperty("po_double_validation")
    public void setPo_double_validation(String  po_double_validation){
        this.po_double_validation = po_double_validation ;
        this.po_double_validationDirtyFlag = true ;
    }

    /**
     * 获取 [PO_DOUBLE_VALIDATION]脏标记
     */
    @JsonIgnore
    public boolean getPo_double_validationDirtyFlag(){
        return po_double_validationDirtyFlag ;
    }

    /**
     * 获取 [REPORT_FOOTER]
     */
    @JsonProperty("report_footer")
    public String getReport_footer(){
        return report_footer ;
    }

    /**
     * 设置 [REPORT_FOOTER]
     */
    @JsonProperty("report_footer")
    public void setReport_footer(String  report_footer){
        this.report_footer = report_footer ;
        this.report_footerDirtyFlag = true ;
    }

    /**
     * 获取 [REPORT_FOOTER]脏标记
     */
    @JsonIgnore
    public boolean getReport_footerDirtyFlag(){
        return report_footerDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID_TEXT]
     */
    @JsonProperty("template_id_text")
    public String getTemplate_id_text(){
        return template_id_text ;
    }

    /**
     * 设置 [TEMPLATE_ID_TEXT]
     */
    @JsonProperty("template_id_text")
    public void setTemplate_id_text(String  template_id_text){
        this.template_id_text = template_id_text ;
        this.template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_id_textDirtyFlag(){
        return template_id_textDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IS_EMAIL]
     */
    @JsonProperty("invoice_is_email")
    public String getInvoice_is_email(){
        return invoice_is_email ;
    }

    /**
     * 设置 [INVOICE_IS_EMAIL]
     */
    @JsonProperty("invoice_is_email")
    public void setInvoice_is_email(String  invoice_is_email){
        this.invoice_is_email = invoice_is_email ;
        this.invoice_is_emailDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IS_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_is_emailDirtyFlag(){
        return invoice_is_emailDirtyFlag ;
    }

    /**
     * 获取 [PORTAL_CONFIRMATION_PAY]
     */
    @JsonProperty("portal_confirmation_pay")
    public String getPortal_confirmation_pay(){
        return portal_confirmation_pay ;
    }

    /**
     * 设置 [PORTAL_CONFIRMATION_PAY]
     */
    @JsonProperty("portal_confirmation_pay")
    public void setPortal_confirmation_pay(String  portal_confirmation_pay){
        this.portal_confirmation_pay = portal_confirmation_pay ;
        this.portal_confirmation_payDirtyFlag = true ;
    }

    /**
     * 获取 [PORTAL_CONFIRMATION_PAY]脏标记
     */
    @JsonIgnore
    public boolean getPortal_confirmation_payDirtyFlag(){
        return portal_confirmation_payDirtyFlag ;
    }

    /**
     * 获取 [QR_CODE]
     */
    @JsonProperty("qr_code")
    public String getQr_code(){
        return qr_code ;
    }

    /**
     * 设置 [QR_CODE]
     */
    @JsonProperty("qr_code")
    public void setQr_code(String  qr_code){
        this.qr_code = qr_code ;
        this.qr_codeDirtyFlag = true ;
    }

    /**
     * 获取 [QR_CODE]脏标记
     */
    @JsonIgnore
    public boolean getQr_codeDirtyFlag(){
        return qr_codeDirtyFlag ;
    }

    /**
     * 获取 [SNAILMAIL_DUPLEX]
     */
    @JsonProperty("snailmail_duplex")
    public String getSnailmail_duplex(){
        return snailmail_duplex ;
    }

    /**
     * 设置 [SNAILMAIL_DUPLEX]
     */
    @JsonProperty("snailmail_duplex")
    public void setSnailmail_duplex(String  snailmail_duplex){
        this.snailmail_duplex = snailmail_duplex ;
        this.snailmail_duplexDirtyFlag = true ;
    }

    /**
     * 获取 [SNAILMAIL_DUPLEX]脏标记
     */
    @JsonIgnore
    public boolean getSnailmail_duplexDirtyFlag(){
        return snailmail_duplexDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [SECURITY_LEAD]
     */
    @JsonProperty("security_lead")
    public Double getSecurity_lead(){
        return security_lead ;
    }

    /**
     * 设置 [SECURITY_LEAD]
     */
    @JsonProperty("security_lead")
    public void setSecurity_lead(Double  security_lead){
        this.security_lead = security_lead ;
        this.security_leadDirtyFlag = true ;
    }

    /**
     * 获取 [SECURITY_LEAD]脏标记
     */
    @JsonIgnore
    public boolean getSecurity_leadDirtyFlag(){
        return security_leadDirtyFlag ;
    }

    /**
     * 获取 [EXTERNAL_REPORT_LAYOUT_ID]
     */
    @JsonProperty("external_report_layout_id")
    public Integer getExternal_report_layout_id(){
        return external_report_layout_id ;
    }

    /**
     * 设置 [EXTERNAL_REPORT_LAYOUT_ID]
     */
    @JsonProperty("external_report_layout_id")
    public void setExternal_report_layout_id(Integer  external_report_layout_id){
        this.external_report_layout_id = external_report_layout_id ;
        this.external_report_layout_idDirtyFlag = true ;
    }

    /**
     * 获取 [EXTERNAL_REPORT_LAYOUT_ID]脏标记
     */
    @JsonIgnore
    public boolean getExternal_report_layout_idDirtyFlag(){
        return external_report_layout_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_TAX_ID]
     */
    @JsonProperty("purchase_tax_id")
    public Integer getPurchase_tax_id(){
        return purchase_tax_id ;
    }

    /**
     * 设置 [PURCHASE_TAX_ID]
     */
    @JsonProperty("purchase_tax_id")
    public void setPurchase_tax_id(Integer  purchase_tax_id){
        this.purchase_tax_id = purchase_tax_id ;
        this.purchase_tax_idDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_TAX_ID]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_tax_idDirtyFlag(){
        return purchase_tax_idDirtyFlag ;
    }

    /**
     * 获取 [PROPAGATION_MINIMUM_DELTA]
     */
    @JsonProperty("propagation_minimum_delta")
    public Integer getPropagation_minimum_delta(){
        return propagation_minimum_delta ;
    }

    /**
     * 设置 [PROPAGATION_MINIMUM_DELTA]
     */
    @JsonProperty("propagation_minimum_delta")
    public void setPropagation_minimum_delta(Integer  propagation_minimum_delta){
        this.propagation_minimum_delta = propagation_minimum_delta ;
        this.propagation_minimum_deltaDirtyFlag = true ;
    }

    /**
     * 获取 [PROPAGATION_MINIMUM_DELTA]脏标记
     */
    @JsonIgnore
    public boolean getPropagation_minimum_deltaDirtyFlag(){
        return propagation_minimum_deltaDirtyFlag ;
    }

    /**
     * 获取 [QUOTATION_VALIDITY_DAYS]
     */
    @JsonProperty("quotation_validity_days")
    public Integer getQuotation_validity_days(){
        return quotation_validity_days ;
    }

    /**
     * 设置 [QUOTATION_VALIDITY_DAYS]
     */
    @JsonProperty("quotation_validity_days")
    public void setQuotation_validity_days(Integer  quotation_validity_days){
        this.quotation_validity_days = quotation_validity_days ;
        this.quotation_validity_daysDirtyFlag = true ;
    }

    /**
     * 获取 [QUOTATION_VALIDITY_DAYS]脏标记
     */
    @JsonIgnore
    public boolean getQuotation_validity_daysDirtyFlag(){
        return quotation_validity_daysDirtyFlag ;
    }

    /**
     * 获取 [TAX_CALCULATION_ROUNDING_METHOD]
     */
    @JsonProperty("tax_calculation_rounding_method")
    public String getTax_calculation_rounding_method(){
        return tax_calculation_rounding_method ;
    }

    /**
     * 设置 [TAX_CALCULATION_ROUNDING_METHOD]
     */
    @JsonProperty("tax_calculation_rounding_method")
    public void setTax_calculation_rounding_method(String  tax_calculation_rounding_method){
        this.tax_calculation_rounding_method = tax_calculation_rounding_method ;
        this.tax_calculation_rounding_methodDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_CALCULATION_ROUNDING_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getTax_calculation_rounding_methodDirtyFlag(){
        return tax_calculation_rounding_methodDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_EXCHANGE_JOURNAL_ID]
     */
    @JsonProperty("currency_exchange_journal_id")
    public Integer getCurrency_exchange_journal_id(){
        return currency_exchange_journal_id ;
    }

    /**
     * 设置 [CURRENCY_EXCHANGE_JOURNAL_ID]
     */
    @JsonProperty("currency_exchange_journal_id")
    public void setCurrency_exchange_journal_id(Integer  currency_exchange_journal_id){
        this.currency_exchange_journal_id = currency_exchange_journal_id ;
        this.currency_exchange_journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_EXCHANGE_JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_exchange_journal_idDirtyFlag(){
        return currency_exchange_journal_idDirtyFlag ;
    }

    /**
     * 获取 [PO_DOUBLE_VALIDATION_AMOUNT]
     */
    @JsonProperty("po_double_validation_amount")
    public Double getPo_double_validation_amount(){
        return po_double_validation_amount ;
    }

    /**
     * 设置 [PO_DOUBLE_VALIDATION_AMOUNT]
     */
    @JsonProperty("po_double_validation_amount")
    public void setPo_double_validation_amount(Double  po_double_validation_amount){
        this.po_double_validation_amount = po_double_validation_amount ;
        this.po_double_validation_amountDirtyFlag = true ;
    }

    /**
     * 获取 [PO_DOUBLE_VALIDATION_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getPo_double_validation_amountDirtyFlag(){
        return po_double_validation_amountDirtyFlag ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("chart_template_id_text")
    public String getChart_template_id_text(){
        return chart_template_id_text ;
    }

    /**
     * 设置 [CHART_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("chart_template_id_text")
    public void setChart_template_id_text(String  chart_template_id_text){
        this.chart_template_id_text = chart_template_id_text ;
        this.chart_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_id_textDirtyFlag(){
        return chart_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [SALE_TAX_ID]
     */
    @JsonProperty("sale_tax_id")
    public Integer getSale_tax_id(){
        return sale_tax_id ;
    }

    /**
     * 设置 [SALE_TAX_ID]
     */
    @JsonProperty("sale_tax_id")
    public void setSale_tax_id(Integer  sale_tax_id){
        this.sale_tax_id = sale_tax_id ;
        this.sale_tax_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_TAX_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_tax_idDirtyFlag(){
        return sale_tax_idDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_SALE_ORDER_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("default_sale_order_template_id_text")
    public String getDefault_sale_order_template_id_text(){
        return default_sale_order_template_id_text ;
    }

    /**
     * 设置 [DEFAULT_SALE_ORDER_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("default_sale_order_template_id_text")
    public void setDefault_sale_order_template_id_text(String  default_sale_order_template_id_text){
        this.default_sale_order_template_id_text = default_sale_order_template_id_text ;
        this.default_sale_order_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_SALE_ORDER_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDefault_sale_order_template_id_textDirtyFlag(){
        return default_sale_order_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_SALE_ORDER_TEMPLATE_ID]
     */
    @JsonProperty("default_sale_order_template_id")
    public Integer getDefault_sale_order_template_id(){
        return default_sale_order_template_id ;
    }

    /**
     * 设置 [DEFAULT_SALE_ORDER_TEMPLATE_ID]
     */
    @JsonProperty("default_sale_order_template_id")
    public void setDefault_sale_order_template_id(Integer  default_sale_order_template_id){
        this.default_sale_order_template_id = default_sale_order_template_id ;
        this.default_sale_order_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_SALE_ORDER_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getDefault_sale_order_template_idDirtyFlag(){
        return default_sale_order_template_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [DIGEST_ID]
     */
    @JsonProperty("digest_id")
    public Integer getDigest_id(){
        return digest_id ;
    }

    /**
     * 设置 [DIGEST_ID]
     */
    @JsonProperty("digest_id")
    public void setDigest_id(Integer  digest_id){
        this.digest_id = digest_id ;
        this.digest_idDirtyFlag = true ;
    }

    /**
     * 获取 [DIGEST_ID]脏标记
     */
    @JsonIgnore
    public boolean getDigest_idDirtyFlag(){
        return digest_idDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return template_id ;
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return template_idDirtyFlag ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID]
     */
    @JsonProperty("chart_template_id")
    public Integer getChart_template_id(){
        return chart_template_id ;
    }

    /**
     * 设置 [CHART_TEMPLATE_ID]
     */
    @JsonProperty("chart_template_id")
    public void setChart_template_id(Integer  chart_template_id){
        this.chart_template_id = chart_template_id ;
        this.chart_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_idDirtyFlag(){
        return chart_template_idDirtyFlag ;
    }

    /**
     * 获取 [DEPOSIT_DEFAULT_PRODUCT_ID]
     */
    @JsonProperty("deposit_default_product_id")
    public Integer getDeposit_default_product_id(){
        return deposit_default_product_id ;
    }

    /**
     * 设置 [DEPOSIT_DEFAULT_PRODUCT_ID]
     */
    @JsonProperty("deposit_default_product_id")
    public void setDeposit_default_product_id(Integer  deposit_default_product_id){
        this.deposit_default_product_id = deposit_default_product_id ;
        this.deposit_default_product_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEPOSIT_DEFAULT_PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDeposit_default_product_idDirtyFlag(){
        return deposit_default_product_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [AUTH_SIGNUP_TEMPLATE_USER_ID]
     */
    @JsonProperty("auth_signup_template_user_id")
    public Integer getAuth_signup_template_user_id(){
        return auth_signup_template_user_id ;
    }

    /**
     * 设置 [AUTH_SIGNUP_TEMPLATE_USER_ID]
     */
    @JsonProperty("auth_signup_template_user_id")
    public void setAuth_signup_template_user_id(Integer  auth_signup_template_user_id){
        this.auth_signup_template_user_id = auth_signup_template_user_id ;
        this.auth_signup_template_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [AUTH_SIGNUP_TEMPLATE_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getAuth_signup_template_user_idDirtyFlag(){
        return auth_signup_template_user_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Res_config_settings toDO() {
        Res_config_settings srfdomain = new Res_config_settings();
        if(getModule_account_bank_statement_import_qifDirtyFlag())
            srfdomain.setModule_account_bank_statement_import_qif(module_account_bank_statement_import_qif);
        if(getModule_account_bank_statement_import_ofxDirtyFlag())
            srfdomain.setModule_account_bank_statement_import_ofx(module_account_bank_statement_import_ofx);
        if(getHas_google_mapsDirtyFlag())
            srfdomain.setHas_google_maps(has_google_maps);
        if(getModule_l10n_eu_serviceDirtyFlag())
            srfdomain.setModule_l10n_eu_service(module_l10n_eu_service);
        if(getCdn_activatedDirtyFlag())
            srfdomain.setCdn_activated(cdn_activated);
        if(getWebsite_default_lang_codeDirtyFlag())
            srfdomain.setWebsite_default_lang_code(website_default_lang_code);
        if(getAuth_signup_reset_passwordDirtyFlag())
            srfdomain.setAuth_signup_reset_password(auth_signup_reset_password);
        if(getWebsite_country_group_idsDirtyFlag())
            srfdomain.setWebsite_country_group_ids(website_country_group_ids);
        if(getSocial_instagramDirtyFlag())
            srfdomain.setSocial_instagram(social_instagram);
        if(getModule_purchase_requisitionDirtyFlag())
            srfdomain.setModule_purchase_requisition(module_purchase_requisition);
        if(getModule_delivery_easypostDirtyFlag())
            srfdomain.setModule_delivery_easypost(module_delivery_easypost);
        if(getGroup_discount_per_so_lineDirtyFlag())
            srfdomain.setGroup_discount_per_so_line(group_discount_per_so_line);
        if(getAutomatic_invoiceDirtyFlag())
            srfdomain.setAutomatic_invoice(automatic_invoice);
        if(getModule_account_plaidDirtyFlag())
            srfdomain.setModule_account_plaid(module_account_plaid);
        if(getGoogle_management_client_idDirtyFlag())
            srfdomain.setGoogle_management_client_id(google_management_client_id);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getModule_account_check_printingDirtyFlag())
            srfdomain.setModule_account_check_printing(module_account_check_printing);
        if(getModule_partner_autocompleteDirtyFlag())
            srfdomain.setModule_partner_autocomplete(module_partner_autocomplete);
        if(getLanguage_countDirtyFlag())
            srfdomain.setLanguage_count(language_count);
        if(getModule_website_linksDirtyFlag())
            srfdomain.setModule_website_links(module_website_links);
        if(getWebsite_form_enable_metadataDirtyFlag())
            srfdomain.setWebsite_form_enable_metadata(website_form_enable_metadata);
        if(getHas_social_networkDirtyFlag())
            srfdomain.setHas_social_network(has_social_network);
        if(getGroup_sale_delivery_addressDirtyFlag())
            srfdomain.setGroup_sale_delivery_address(group_sale_delivery_address);
        if(getGoogle_analytics_keyDirtyFlag())
            srfdomain.setGoogle_analytics_key(google_analytics_key);
        if(getModule_auth_ldapDirtyFlag())
            srfdomain.setModule_auth_ldap(module_auth_ldap);
        if(getSpecific_user_accountDirtyFlag())
            srfdomain.setSpecific_user_account(specific_user_account);
        if(getModule_website_hr_recruitmentDirtyFlag())
            srfdomain.setModule_website_hr_recruitment(module_website_hr_recruitment);
        if(getModule_project_forecastDirtyFlag())
            srfdomain.setModule_project_forecast(module_project_forecast);
        if(getGroup_stock_tracking_ownerDirtyFlag())
            srfdomain.setGroup_stock_tracking_owner(group_stock_tracking_owner);
        if(getModule_google_calendarDirtyFlag())
            srfdomain.setModule_google_calendar(module_google_calendar);
        if(getModule_accountDirtyFlag())
            srfdomain.setModule_account(module_account);
        if(getModule_google_driveDirtyFlag())
            srfdomain.setModule_google_drive(module_google_drive);
        if(getPos_pricelist_settingDirtyFlag())
            srfdomain.setPos_pricelist_setting(pos_pricelist_setting);
        if(getCompany_share_partnerDirtyFlag())
            srfdomain.setCompany_share_partner(company_share_partner);
        if(getModule_currency_rate_liveDirtyFlag())
            srfdomain.setModule_currency_rate_live(module_currency_rate_live);
        if(getGroup_proforma_salesDirtyFlag())
            srfdomain.setGroup_proforma_sales(group_proforma_sales);
        if(getModule_delivery_fedexDirtyFlag())
            srfdomain.setModule_delivery_fedex(module_delivery_fedex);
        if(getModule_product_email_templateDirtyFlag())
            srfdomain.setModule_product_email_template(module_product_email_template);
        if(getShow_effectDirtyFlag())
            srfdomain.setShow_effect(show_effect);
        if(getDefault_picking_policyDirtyFlag())
            srfdomain.setDefault_picking_policy(default_picking_policy);
        if(getSocial_youtubeDirtyFlag())
            srfdomain.setSocial_youtube(social_youtube);
        if(getWebsite_company_idDirtyFlag())
            srfdomain.setWebsite_company_id(website_company_id);
        if(getModule_mrp_byproductDirtyFlag())
            srfdomain.setModule_mrp_byproduct(module_mrp_byproduct);
        if(getModule_delivery_uspsDirtyFlag())
            srfdomain.setModule_delivery_usps(module_delivery_usps);
        if(getModule_delivery_dhlDirtyFlag())
            srfdomain.setModule_delivery_dhl(module_delivery_dhl);
        if(getGroup_project_ratingDirtyFlag())
            srfdomain.setGroup_project_rating(group_project_rating);
        if(getGoogle_maps_api_keyDirtyFlag())
            srfdomain.setGoogle_maps_api_key(google_maps_api_key);
        if(getGroup_use_leadDirtyFlag())
            srfdomain.setGroup_use_lead(group_use_lead);
        if(getGroup_stock_tracking_lotDirtyFlag())
            srfdomain.setGroup_stock_tracking_lot(group_stock_tracking_lot);
        if(getGroup_stock_adv_locationDirtyFlag())
            srfdomain.setGroup_stock_adv_location(group_stock_adv_location);
        if(getPos_sales_priceDirtyFlag())
            srfdomain.setPos_sales_price(pos_sales_price);
        if(getHas_chart_of_accountsDirtyFlag())
            srfdomain.setHas_chart_of_accounts(has_chart_of_accounts);
        if(getMulti_sales_price_methodDirtyFlag())
            srfdomain.setMulti_sales_price_method(multi_sales_price_method);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getModule_procurement_jitDirtyFlag())
            srfdomain.setModule_procurement_jit(module_procurement_jit);
        if(getModule_account_assetDirtyFlag())
            srfdomain.setModule_account_asset(module_account_asset);
        if(getUse_mailgatewayDirtyFlag())
            srfdomain.setUse_mailgateway(use_mailgateway);
        if(getGroup_sale_pricelistDirtyFlag())
            srfdomain.setGroup_sale_pricelist(group_sale_pricelist);
        if(getCrm_default_team_idDirtyFlag())
            srfdomain.setCrm_default_team_id(crm_default_team_id);
        if(getGroup_stock_multi_locationsDirtyFlag())
            srfdomain.setGroup_stock_multi_locations(group_stock_multi_locations);
        if(getUse_manufacturing_leadDirtyFlag())
            srfdomain.setUse_manufacturing_lead(use_manufacturing_lead);
        if(getModule_google_spreadsheetDirtyFlag())
            srfdomain.setModule_google_spreadsheet(module_google_spreadsheet);
        if(getShow_line_subtotals_tax_selectionDirtyFlag())
            srfdomain.setShow_line_subtotals_tax_selection(show_line_subtotals_tax_selection);
        if(getLanguage_idsDirtyFlag())
            srfdomain.setLanguage_ids(language_ids);
        if(getModule_website_sale_deliveryDirtyFlag())
            srfdomain.setModule_website_sale_delivery(module_website_sale_delivery);
        if(getModule_mrp_mpsDirtyFlag())
            srfdomain.setModule_mrp_mps(module_mrp_mps);
        if(getPartner_autocomplete_insufficient_creditDirtyFlag())
            srfdomain.setPartner_autocomplete_insufficient_credit(partner_autocomplete_insufficient_credit);
        if(getModule_hr_org_chartDirtyFlag())
            srfdomain.setModule_hr_org_chart(module_hr_org_chart);
        if(getModule_product_expiryDirtyFlag())
            srfdomain.setModule_product_expiry(module_product_expiry);
        if(getModule_delivery_bpostDirtyFlag())
            srfdomain.setModule_delivery_bpost(module_delivery_bpost);
        if(getModule_stock_barcodeDirtyFlag())
            srfdomain.setModule_stock_barcode(module_stock_barcode);
        if(getSocial_githubDirtyFlag())
            srfdomain.setSocial_github(social_github);
        if(getModule_account_intrastatDirtyFlag())
            srfdomain.setModule_account_intrastat(module_account_intrastat);
        if(getAuto_done_settingDirtyFlag())
            srfdomain.setAuto_done_setting(auto_done_setting);
        if(getAuth_signup_uninvitedDirtyFlag())
            srfdomain.setAuth_signup_uninvited(auth_signup_uninvited);
        if(getCompany_share_productDirtyFlag())
            srfdomain.setCompany_share_product(company_share_product);
        if(getGroup_sale_order_templateDirtyFlag())
            srfdomain.setGroup_sale_order_template(group_sale_order_template);
        if(getModule_account_sepa_direct_debitDirtyFlag())
            srfdomain.setModule_account_sepa_direct_debit(module_account_sepa_direct_debit);
        if(getUse_quotation_validity_daysDirtyFlag())
            srfdomain.setUse_quotation_validity_days(use_quotation_validity_days);
        if(getModule_account_reports_followupDirtyFlag())
            srfdomain.setModule_account_reports_followup(module_account_reports_followup);
        if(getModule_account_batch_paymentDirtyFlag())
            srfdomain.setModule_account_batch_payment(module_account_batch_payment);
        if(getSocial_twitterDirtyFlag())
            srfdomain.setSocial_twitter(social_twitter);
        if(getModule_account_budgetDirtyFlag())
            srfdomain.setModule_account_budget(module_account_budget);
        if(getGroup_mrp_routingsDirtyFlag())
            srfdomain.setGroup_mrp_routings(group_mrp_routings);
        if(getGroup_cash_roundingDirtyFlag())
            srfdomain.setGroup_cash_rounding(group_cash_rounding);
        if(getModule_stock_landed_costsDirtyFlag())
            srfdomain.setModule_stock_landed_costs(module_stock_landed_costs);
        if(getWebsite_nameDirtyFlag())
            srfdomain.setWebsite_name(website_name);
        if(getModule_website_sale_stockDirtyFlag())
            srfdomain.setModule_website_sale_stock(module_website_sale_stock);
        if(getGroup_website_popup_on_exitDirtyFlag())
            srfdomain.setGroup_website_popup_on_exit(group_website_popup_on_exit);
        if(getModule_website_event_trackDirtyFlag())
            srfdomain.setModule_website_event_track(module_website_event_track);
        if(getGroup_manage_vendor_priceDirtyFlag())
            srfdomain.setGroup_manage_vendor_price(group_manage_vendor_price);
        if(getModule_deliveryDirtyFlag())
            srfdomain.setModule_delivery(module_delivery);
        if(getModule_account_invoice_extractDirtyFlag())
            srfdomain.setModule_account_invoice_extract(module_account_invoice_extract);
        if(getChannel_idDirtyFlag())
            srfdomain.setChannel_id(channel_id);
        if(getGroup_warning_saleDirtyFlag())
            srfdomain.setGroup_warning_sale(group_warning_sale);
        if(getCdn_urlDirtyFlag())
            srfdomain.setCdn_url(cdn_url);
        if(getModule_event_barcodeDirtyFlag())
            srfdomain.setModule_event_barcode(module_event_barcode);
        if(getAlias_domainDirtyFlag())
            srfdomain.setAlias_domain(alias_domain);
        if(getSocial_linkedinDirtyFlag())
            srfdomain.setSocial_linkedin(social_linkedin);
        if(getGroup_stock_multi_warehousesDirtyFlag())
            srfdomain.setGroup_stock_multi_warehouses(group_stock_multi_warehouses);
        if(getSalesperson_idDirtyFlag())
            srfdomain.setSalesperson_id(salesperson_id);
        if(getModule_account_reportsDirtyFlag())
            srfdomain.setModule_account_reports(module_account_reports);
        if(getGroup_product_pricelistDirtyFlag())
            srfdomain.setGroup_product_pricelist(group_product_pricelist);
        if(getModule_crm_phone_validationDirtyFlag())
            srfdomain.setModule_crm_phone_validation(module_crm_phone_validation);
        if(getModule_website_versionDirtyFlag())
            srfdomain.setModule_website_version(module_website_version);
        if(getModule_base_importDirtyFlag())
            srfdomain.setModule_base_import(module_base_import);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getModule_account_bank_statement_import_csvDirtyFlag())
            srfdomain.setModule_account_bank_statement_import_csv(module_account_bank_statement_import_csv);
        if(getModule_account_taxcloudDirtyFlag())
            srfdomain.setModule_account_taxcloud(module_account_taxcloud);
        if(getUse_sale_noteDirtyFlag())
            srfdomain.setUse_sale_note(use_sale_note);
        if(getCart_abandoned_delayDirtyFlag())
            srfdomain.setCart_abandoned_delay(cart_abandoned_delay);
        if(getWebsite_domainDirtyFlag())
            srfdomain.setWebsite_domain(website_domain);
        if(getModule_account_accountantDirtyFlag())
            srfdomain.setModule_account_accountant(module_account_accountant);
        if(getModule_sale_marginDirtyFlag())
            srfdomain.setModule_sale_margin(module_sale_margin);
        if(getDigest_emailsDirtyFlag())
            srfdomain.setDigest_emails(digest_emails);
        if(getModule_padDirtyFlag())
            srfdomain.setModule_pad(module_pad);
        if(getGroup_warning_accountDirtyFlag())
            srfdomain.setGroup_warning_account(group_warning_account);
        if(getGroup_display_incotermDirtyFlag())
            srfdomain.setGroup_display_incoterm(group_display_incoterm);
        if(getModule_website_sale_wishlistDirtyFlag())
            srfdomain.setModule_website_sale_wishlist(module_website_sale_wishlist);
        if(getUser_default_rightsDirtyFlag())
            srfdomain.setUser_default_rights(user_default_rights);
        if(getDefault_purchase_methodDirtyFlag())
            srfdomain.setDefault_purchase_method(default_purchase_method);
        if(getGroup_delivery_invoice_addressDirtyFlag())
            srfdomain.setGroup_delivery_invoice_address(group_delivery_invoice_address);
        if(getGroup_lot_on_delivery_slipDirtyFlag())
            srfdomain.setGroup_lot_on_delivery_slip(group_lot_on_delivery_slip);
        if(getModule_event_saleDirtyFlag())
            srfdomain.setModule_event_sale(module_event_sale);
        if(getGroup_show_line_subtotals_tax_includedDirtyFlag())
            srfdomain.setGroup_show_line_subtotals_tax_included(group_show_line_subtotals_tax_included);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getGroup_product_variantDirtyFlag())
            srfdomain.setGroup_product_variant(group_product_variant);
        if(getModule_account_sepaDirtyFlag())
            srfdomain.setModule_account_sepa(module_account_sepa);
        if(getGroup_multi_currencyDirtyFlag())
            srfdomain.setGroup_multi_currency(group_multi_currency);
        if(getGroup_products_in_billsDirtyFlag())
            srfdomain.setGroup_products_in_bills(group_products_in_bills);
        if(getGroup_analytic_accountingDirtyFlag())
            srfdomain.setGroup_analytic_accounting(group_analytic_accounting);
        if(getGroup_stock_packagingDirtyFlag())
            srfdomain.setGroup_stock_packaging(group_stock_packaging);
        if(getWebsite_slide_google_app_keyDirtyFlag())
            srfdomain.setWebsite_slide_google_app_key(website_slide_google_app_key);
        if(getPo_order_approvalDirtyFlag())
            srfdomain.setPo_order_approval(po_order_approval);
        if(getIs_installed_saleDirtyFlag())
            srfdomain.setIs_installed_sale(is_installed_sale);
        if(getModule_account_paymentDirtyFlag())
            srfdomain.setModule_account_payment(module_account_payment);
        if(getGroup_analytic_tagsDirtyFlag())
            srfdomain.setGroup_analytic_tags(group_analytic_tags);
        if(getGroup_sale_order_datesDirtyFlag())
            srfdomain.setGroup_sale_order_dates(group_sale_order_dates);
        if(getModule_voipDirtyFlag())
            srfdomain.setModule_voip(module_voip);
        if(getCart_recovery_mail_templateDirtyFlag())
            srfdomain.setCart_recovery_mail_template(cart_recovery_mail_template);
        if(getGroup_multi_websiteDirtyFlag())
            srfdomain.setGroup_multi_website(group_multi_website);
        if(getModule_auth_oauthDirtyFlag())
            srfdomain.setModule_auth_oauth(module_auth_oauth);
        if(getSale_delivery_settingsDirtyFlag())
            srfdomain.setSale_delivery_settings(sale_delivery_settings);
        if(getModule_sale_quotation_builderDirtyFlag())
            srfdomain.setModule_sale_quotation_builder(module_sale_quotation_builder);
        if(getModule_inter_company_rulesDirtyFlag())
            srfdomain.setModule_inter_company_rules(module_inter_company_rules);
        if(getUse_security_leadDirtyFlag())
            srfdomain.setUse_security_lead(use_security_lead);
        if(getDefault_invoice_policyDirtyFlag())
            srfdomain.setDefault_invoice_policy(default_invoice_policy);
        if(getMulti_sales_priceDirtyFlag())
            srfdomain.setMulti_sales_price(multi_sales_price);
        if(getLock_confirmed_poDirtyFlag())
            srfdomain.setLock_confirmed_po(lock_confirmed_po);
        if(getProduct_weight_in_lbsDirtyFlag())
            srfdomain.setProduct_weight_in_lbs(product_weight_in_lbs);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getHas_google_analytics_dashboardDirtyFlag())
            srfdomain.setHas_google_analytics_dashboard(has_google_analytics_dashboard);
        if(getModule_stock_picking_batchDirtyFlag())
            srfdomain.setModule_stock_picking_batch(module_stock_picking_batch);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getExpense_alias_prefixDirtyFlag())
            srfdomain.setExpense_alias_prefix(expense_alias_prefix);
        if(getModule_account_deferred_revenueDirtyFlag())
            srfdomain.setModule_account_deferred_revenue(module_account_deferred_revenue);
        if(getSocial_facebookDirtyFlag())
            srfdomain.setSocial_facebook(social_facebook);
        if(getModule_web_unsplashDirtyFlag())
            srfdomain.setModule_web_unsplash(module_web_unsplash);
        if(getGroup_mass_mailing_campaignDirtyFlag())
            srfdomain.setGroup_mass_mailing_campaign(group_mass_mailing_campaign);
        if(getModule_account_bank_statement_import_camtDirtyFlag())
            srfdomain.setModule_account_bank_statement_import_camt(module_account_bank_statement_import_camt);
        if(getModule_product_marginDirtyFlag())
            srfdomain.setModule_product_margin(module_product_margin);
        if(getGroup_subtask_projectDirtyFlag())
            srfdomain.setGroup_subtask_project(group_subtask_project);
        if(getModule_account_3way_matchDirtyFlag())
            srfdomain.setModule_account_3way_match(module_account_3way_match);
        if(getModule_website_sale_digitalDirtyFlag())
            srfdomain.setModule_website_sale_digital(module_website_sale_digital);
        if(getModule_sale_couponDirtyFlag())
            srfdomain.setModule_sale_coupon(module_sale_coupon);
        if(getShow_blacklist_buttonsDirtyFlag())
            srfdomain.setShow_blacklist_buttons(show_blacklist_buttons);
        if(getGroup_warning_stockDirtyFlag())
            srfdomain.setGroup_warning_stock(group_warning_stock);
        if(getGroup_multi_companyDirtyFlag())
            srfdomain.setGroup_multi_company(group_multi_company);
        if(getGroup_attendance_use_pinDirtyFlag())
            srfdomain.setGroup_attendance_use_pin(group_attendance_use_pin);
        if(getSalesteam_idDirtyFlag())
            srfdomain.setSalesteam_id(salesteam_id);
        if(getFaviconDirtyFlag())
            srfdomain.setFavicon(favicon);
        if(getModule_website_sale_comparisonDirtyFlag())
            srfdomain.setModule_website_sale_comparison(module_website_sale_comparison);
        if(getAvailable_thresholdDirtyFlag())
            srfdomain.setAvailable_threshold(available_threshold);
        if(getUse_po_leadDirtyFlag())
            srfdomain.setUse_po_lead(use_po_lead);
        if(getGroup_fiscal_yearDirtyFlag())
            srfdomain.setGroup_fiscal_year(group_fiscal_year);
        if(getModule_hr_timesheetDirtyFlag())
            srfdomain.setModule_hr_timesheet(module_hr_timesheet);
        if(getModule_mrp_plmDirtyFlag())
            srfdomain.setModule_mrp_plm(module_mrp_plm);
        if(getModule_account_yodleeDirtyFlag())
            srfdomain.setModule_account_yodlee(module_account_yodlee);
        if(getCdn_filtersDirtyFlag())
            srfdomain.setCdn_filters(cdn_filters);
        if(getUse_propagation_minimum_deltaDirtyFlag())
            srfdomain.setUse_propagation_minimum_delta(use_propagation_minimum_delta);
        if(getModule_delivery_upsDirtyFlag())
            srfdomain.setModule_delivery_ups(module_delivery_ups);
        if(getFail_counterDirtyFlag())
            srfdomain.setFail_counter(fail_counter);
        if(getGroup_stock_production_lotDirtyFlag())
            srfdomain.setGroup_stock_production_lot(group_stock_production_lot);
        if(getHas_accounting_entriesDirtyFlag())
            srfdomain.setHas_accounting_entries(has_accounting_entries);
        if(getGroup_uomDirtyFlag())
            srfdomain.setGroup_uom(group_uom);
        if(getMass_mailing_outgoing_mail_serverDirtyFlag())
            srfdomain.setMass_mailing_outgoing_mail_server(mass_mailing_outgoing_mail_server);
        if(getCrm_alias_prefixDirtyFlag())
            srfdomain.setCrm_alias_prefix(crm_alias_prefix);
        if(getSocial_googleplusDirtyFlag())
            srfdomain.setSocial_googleplus(social_googleplus);
        if(getGroup_route_so_linesDirtyFlag())
            srfdomain.setGroup_route_so_lines(group_route_so_lines);
        if(getGoogle_management_client_secretDirtyFlag())
            srfdomain.setGoogle_management_client_secret(google_management_client_secret);
        if(getSocial_default_imageDirtyFlag())
            srfdomain.setSocial_default_image(social_default_image);
        if(getHas_google_analyticsDirtyFlag())
            srfdomain.setHas_google_analytics(has_google_analytics);
        if(getModule_website_event_questionsDirtyFlag())
            srfdomain.setModule_website_event_questions(module_website_event_questions);
        if(getWebsite_default_lang_idDirtyFlag())
            srfdomain.setWebsite_default_lang_id(website_default_lang_id);
        if(getInventory_availabilityDirtyFlag())
            srfdomain.setInventory_availability(inventory_availability);
        if(getGroup_warning_purchaseDirtyFlag())
            srfdomain.setGroup_warning_purchase(group_warning_purchase);
        if(getModule_quality_controlDirtyFlag())
            srfdomain.setModule_quality_control(module_quality_control);
        if(getGenerate_lead_from_aliasDirtyFlag())
            srfdomain.setGenerate_lead_from_alias(generate_lead_from_alias);
        if(getSale_pricelist_settingDirtyFlag())
            srfdomain.setSale_pricelist_setting(sale_pricelist_setting);
        if(getGroup_pricelist_itemDirtyFlag())
            srfdomain.setGroup_pricelist_item(group_pricelist_item);
        if(getExternal_email_server_defaultDirtyFlag())
            srfdomain.setExternal_email_server_default(external_email_server_default);
        if(getUnsplash_access_keyDirtyFlag())
            srfdomain.setUnsplash_access_key(unsplash_access_key);
        if(getModule_base_gengoDirtyFlag())
            srfdomain.setModule_base_gengo(module_base_gengo);
        if(getModule_website_event_saleDirtyFlag())
            srfdomain.setModule_website_event_sale(module_website_event_sale);
        if(getCrm_default_user_idDirtyFlag())
            srfdomain.setCrm_default_user_id(crm_default_user_id);
        if(getModule_stock_dropshippingDirtyFlag())
            srfdomain.setModule_stock_dropshipping(module_stock_dropshipping);
        if(getModule_crm_revealDirtyFlag())
            srfdomain.setModule_crm_reveal(module_crm_reveal);
        if(getMass_mailing_mail_server_idDirtyFlag())
            srfdomain.setMass_mailing_mail_server_id(mass_mailing_mail_server_id);
        if(getGroup_show_line_subtotals_tax_excludedDirtyFlag())
            srfdomain.setGroup_show_line_subtotals_tax_excluded(group_show_line_subtotals_tax_excluded);
        if(getModule_hr_recruitment_surveyDirtyFlag())
            srfdomain.setModule_hr_recruitment_survey(module_hr_recruitment_survey);
        if(getModule_mrp_workorderDirtyFlag())
            srfdomain.setModule_mrp_workorder(module_mrp_workorder);
        if(getModule_pos_mercuryDirtyFlag())
            srfdomain.setModule_pos_mercury(module_pos_mercury);
        if(getInvoice_reference_typeDirtyFlag())
            srfdomain.setInvoice_reference_type(invoice_reference_type);
        if(getCompany_currency_idDirtyFlag())
            srfdomain.setCompany_currency_id(company_currency_id);
        if(getTax_exigibilityDirtyFlag())
            srfdomain.setTax_exigibility(tax_exigibility);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getAccount_bank_reconciliation_startDirtyFlag())
            srfdomain.setAccount_bank_reconciliation_start(account_bank_reconciliation_start);
        if(getTax_cash_basis_journal_idDirtyFlag())
            srfdomain.setTax_cash_basis_journal_id(tax_cash_basis_journal_id);
        if(getPo_leadDirtyFlag())
            srfdomain.setPo_lead(po_lead);
        if(getSnailmail_colorDirtyFlag())
            srfdomain.setSnailmail_color(snailmail_color);
        if(getPaperformat_idDirtyFlag())
            srfdomain.setPaperformat_id(paperformat_id);
        if(getPortal_confirmation_signDirtyFlag())
            srfdomain.setPortal_confirmation_sign(portal_confirmation_sign);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getDigest_id_textDirtyFlag())
            srfdomain.setDigest_id_text(digest_id_text);
        if(getSale_noteDirtyFlag())
            srfdomain.setSale_note(sale_note);
        if(getAuth_signup_template_user_id_textDirtyFlag())
            srfdomain.setAuth_signup_template_user_id_text(auth_signup_template_user_id_text);
        if(getInvoice_is_printDirtyFlag())
            srfdomain.setInvoice_is_print(invoice_is_print);
        if(getDeposit_default_product_id_textDirtyFlag())
            srfdomain.setDeposit_default_product_id_text(deposit_default_product_id_text);
        if(getResource_calendar_idDirtyFlag())
            srfdomain.setResource_calendar_id(resource_calendar_id);
        if(getPo_lockDirtyFlag())
            srfdomain.setPo_lock(po_lock);
        if(getInvoice_is_snailmailDirtyFlag())
            srfdomain.setInvoice_is_snailmail(invoice_is_snailmail);
        if(getManufacturing_leadDirtyFlag())
            srfdomain.setManufacturing_lead(manufacturing_lead);
        if(getPo_double_validationDirtyFlag())
            srfdomain.setPo_double_validation(po_double_validation);
        if(getReport_footerDirtyFlag())
            srfdomain.setReport_footer(report_footer);
        if(getTemplate_id_textDirtyFlag())
            srfdomain.setTemplate_id_text(template_id_text);
        if(getInvoice_is_emailDirtyFlag())
            srfdomain.setInvoice_is_email(invoice_is_email);
        if(getPortal_confirmation_payDirtyFlag())
            srfdomain.setPortal_confirmation_pay(portal_confirmation_pay);
        if(getQr_codeDirtyFlag())
            srfdomain.setQr_code(qr_code);
        if(getSnailmail_duplexDirtyFlag())
            srfdomain.setSnailmail_duplex(snailmail_duplex);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getSecurity_leadDirtyFlag())
            srfdomain.setSecurity_lead(security_lead);
        if(getExternal_report_layout_idDirtyFlag())
            srfdomain.setExternal_report_layout_id(external_report_layout_id);
        if(getPurchase_tax_idDirtyFlag())
            srfdomain.setPurchase_tax_id(purchase_tax_id);
        if(getPropagation_minimum_deltaDirtyFlag())
            srfdomain.setPropagation_minimum_delta(propagation_minimum_delta);
        if(getQuotation_validity_daysDirtyFlag())
            srfdomain.setQuotation_validity_days(quotation_validity_days);
        if(getTax_calculation_rounding_methodDirtyFlag())
            srfdomain.setTax_calculation_rounding_method(tax_calculation_rounding_method);
        if(getCurrency_exchange_journal_idDirtyFlag())
            srfdomain.setCurrency_exchange_journal_id(currency_exchange_journal_id);
        if(getPo_double_validation_amountDirtyFlag())
            srfdomain.setPo_double_validation_amount(po_double_validation_amount);
        if(getChart_template_id_textDirtyFlag())
            srfdomain.setChart_template_id_text(chart_template_id_text);
        if(getSale_tax_idDirtyFlag())
            srfdomain.setSale_tax_id(sale_tax_id);
        if(getDefault_sale_order_template_id_textDirtyFlag())
            srfdomain.setDefault_sale_order_template_id_text(default_sale_order_template_id_text);
        if(getDefault_sale_order_template_idDirtyFlag())
            srfdomain.setDefault_sale_order_template_id(default_sale_order_template_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getDigest_idDirtyFlag())
            srfdomain.setDigest_id(digest_id);
        if(getTemplate_idDirtyFlag())
            srfdomain.setTemplate_id(template_id);
        if(getChart_template_idDirtyFlag())
            srfdomain.setChart_template_id(chart_template_id);
        if(getDeposit_default_product_idDirtyFlag())
            srfdomain.setDeposit_default_product_id(deposit_default_product_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getAuth_signup_template_user_idDirtyFlag())
            srfdomain.setAuth_signup_template_user_id(auth_signup_template_user_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Res_config_settings srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getModule_account_bank_statement_import_qifDirtyFlag())
            this.setModule_account_bank_statement_import_qif(srfdomain.getModule_account_bank_statement_import_qif());
        if(srfdomain.getModule_account_bank_statement_import_ofxDirtyFlag())
            this.setModule_account_bank_statement_import_ofx(srfdomain.getModule_account_bank_statement_import_ofx());
        if(srfdomain.getHas_google_mapsDirtyFlag())
            this.setHas_google_maps(srfdomain.getHas_google_maps());
        if(srfdomain.getModule_l10n_eu_serviceDirtyFlag())
            this.setModule_l10n_eu_service(srfdomain.getModule_l10n_eu_service());
        if(srfdomain.getCdn_activatedDirtyFlag())
            this.setCdn_activated(srfdomain.getCdn_activated());
        if(srfdomain.getWebsite_default_lang_codeDirtyFlag())
            this.setWebsite_default_lang_code(srfdomain.getWebsite_default_lang_code());
        if(srfdomain.getAuth_signup_reset_passwordDirtyFlag())
            this.setAuth_signup_reset_password(srfdomain.getAuth_signup_reset_password());
        if(srfdomain.getWebsite_country_group_idsDirtyFlag())
            this.setWebsite_country_group_ids(srfdomain.getWebsite_country_group_ids());
        if(srfdomain.getSocial_instagramDirtyFlag())
            this.setSocial_instagram(srfdomain.getSocial_instagram());
        if(srfdomain.getModule_purchase_requisitionDirtyFlag())
            this.setModule_purchase_requisition(srfdomain.getModule_purchase_requisition());
        if(srfdomain.getModule_delivery_easypostDirtyFlag())
            this.setModule_delivery_easypost(srfdomain.getModule_delivery_easypost());
        if(srfdomain.getGroup_discount_per_so_lineDirtyFlag())
            this.setGroup_discount_per_so_line(srfdomain.getGroup_discount_per_so_line());
        if(srfdomain.getAutomatic_invoiceDirtyFlag())
            this.setAutomatic_invoice(srfdomain.getAutomatic_invoice());
        if(srfdomain.getModule_account_plaidDirtyFlag())
            this.setModule_account_plaid(srfdomain.getModule_account_plaid());
        if(srfdomain.getGoogle_management_client_idDirtyFlag())
            this.setGoogle_management_client_id(srfdomain.getGoogle_management_client_id());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getModule_account_check_printingDirtyFlag())
            this.setModule_account_check_printing(srfdomain.getModule_account_check_printing());
        if(srfdomain.getModule_partner_autocompleteDirtyFlag())
            this.setModule_partner_autocomplete(srfdomain.getModule_partner_autocomplete());
        if(srfdomain.getLanguage_countDirtyFlag())
            this.setLanguage_count(srfdomain.getLanguage_count());
        if(srfdomain.getModule_website_linksDirtyFlag())
            this.setModule_website_links(srfdomain.getModule_website_links());
        if(srfdomain.getWebsite_form_enable_metadataDirtyFlag())
            this.setWebsite_form_enable_metadata(srfdomain.getWebsite_form_enable_metadata());
        if(srfdomain.getHas_social_networkDirtyFlag())
            this.setHas_social_network(srfdomain.getHas_social_network());
        if(srfdomain.getGroup_sale_delivery_addressDirtyFlag())
            this.setGroup_sale_delivery_address(srfdomain.getGroup_sale_delivery_address());
        if(srfdomain.getGoogle_analytics_keyDirtyFlag())
            this.setGoogle_analytics_key(srfdomain.getGoogle_analytics_key());
        if(srfdomain.getModule_auth_ldapDirtyFlag())
            this.setModule_auth_ldap(srfdomain.getModule_auth_ldap());
        if(srfdomain.getSpecific_user_accountDirtyFlag())
            this.setSpecific_user_account(srfdomain.getSpecific_user_account());
        if(srfdomain.getModule_website_hr_recruitmentDirtyFlag())
            this.setModule_website_hr_recruitment(srfdomain.getModule_website_hr_recruitment());
        if(srfdomain.getModule_project_forecastDirtyFlag())
            this.setModule_project_forecast(srfdomain.getModule_project_forecast());
        if(srfdomain.getGroup_stock_tracking_ownerDirtyFlag())
            this.setGroup_stock_tracking_owner(srfdomain.getGroup_stock_tracking_owner());
        if(srfdomain.getModule_google_calendarDirtyFlag())
            this.setModule_google_calendar(srfdomain.getModule_google_calendar());
        if(srfdomain.getModule_accountDirtyFlag())
            this.setModule_account(srfdomain.getModule_account());
        if(srfdomain.getModule_google_driveDirtyFlag())
            this.setModule_google_drive(srfdomain.getModule_google_drive());
        if(srfdomain.getPos_pricelist_settingDirtyFlag())
            this.setPos_pricelist_setting(srfdomain.getPos_pricelist_setting());
        if(srfdomain.getCompany_share_partnerDirtyFlag())
            this.setCompany_share_partner(srfdomain.getCompany_share_partner());
        if(srfdomain.getModule_currency_rate_liveDirtyFlag())
            this.setModule_currency_rate_live(srfdomain.getModule_currency_rate_live());
        if(srfdomain.getGroup_proforma_salesDirtyFlag())
            this.setGroup_proforma_sales(srfdomain.getGroup_proforma_sales());
        if(srfdomain.getModule_delivery_fedexDirtyFlag())
            this.setModule_delivery_fedex(srfdomain.getModule_delivery_fedex());
        if(srfdomain.getModule_product_email_templateDirtyFlag())
            this.setModule_product_email_template(srfdomain.getModule_product_email_template());
        if(srfdomain.getShow_effectDirtyFlag())
            this.setShow_effect(srfdomain.getShow_effect());
        if(srfdomain.getDefault_picking_policyDirtyFlag())
            this.setDefault_picking_policy(srfdomain.getDefault_picking_policy());
        if(srfdomain.getSocial_youtubeDirtyFlag())
            this.setSocial_youtube(srfdomain.getSocial_youtube());
        if(srfdomain.getWebsite_company_idDirtyFlag())
            this.setWebsite_company_id(srfdomain.getWebsite_company_id());
        if(srfdomain.getModule_mrp_byproductDirtyFlag())
            this.setModule_mrp_byproduct(srfdomain.getModule_mrp_byproduct());
        if(srfdomain.getModule_delivery_uspsDirtyFlag())
            this.setModule_delivery_usps(srfdomain.getModule_delivery_usps());
        if(srfdomain.getModule_delivery_dhlDirtyFlag())
            this.setModule_delivery_dhl(srfdomain.getModule_delivery_dhl());
        if(srfdomain.getGroup_project_ratingDirtyFlag())
            this.setGroup_project_rating(srfdomain.getGroup_project_rating());
        if(srfdomain.getGoogle_maps_api_keyDirtyFlag())
            this.setGoogle_maps_api_key(srfdomain.getGoogle_maps_api_key());
        if(srfdomain.getGroup_use_leadDirtyFlag())
            this.setGroup_use_lead(srfdomain.getGroup_use_lead());
        if(srfdomain.getGroup_stock_tracking_lotDirtyFlag())
            this.setGroup_stock_tracking_lot(srfdomain.getGroup_stock_tracking_lot());
        if(srfdomain.getGroup_stock_adv_locationDirtyFlag())
            this.setGroup_stock_adv_location(srfdomain.getGroup_stock_adv_location());
        if(srfdomain.getPos_sales_priceDirtyFlag())
            this.setPos_sales_price(srfdomain.getPos_sales_price());
        if(srfdomain.getHas_chart_of_accountsDirtyFlag())
            this.setHas_chart_of_accounts(srfdomain.getHas_chart_of_accounts());
        if(srfdomain.getMulti_sales_price_methodDirtyFlag())
            this.setMulti_sales_price_method(srfdomain.getMulti_sales_price_method());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getModule_procurement_jitDirtyFlag())
            this.setModule_procurement_jit(srfdomain.getModule_procurement_jit());
        if(srfdomain.getModule_account_assetDirtyFlag())
            this.setModule_account_asset(srfdomain.getModule_account_asset());
        if(srfdomain.getUse_mailgatewayDirtyFlag())
            this.setUse_mailgateway(srfdomain.getUse_mailgateway());
        if(srfdomain.getGroup_sale_pricelistDirtyFlag())
            this.setGroup_sale_pricelist(srfdomain.getGroup_sale_pricelist());
        if(srfdomain.getCrm_default_team_idDirtyFlag())
            this.setCrm_default_team_id(srfdomain.getCrm_default_team_id());
        if(srfdomain.getGroup_stock_multi_locationsDirtyFlag())
            this.setGroup_stock_multi_locations(srfdomain.getGroup_stock_multi_locations());
        if(srfdomain.getUse_manufacturing_leadDirtyFlag())
            this.setUse_manufacturing_lead(srfdomain.getUse_manufacturing_lead());
        if(srfdomain.getModule_google_spreadsheetDirtyFlag())
            this.setModule_google_spreadsheet(srfdomain.getModule_google_spreadsheet());
        if(srfdomain.getShow_line_subtotals_tax_selectionDirtyFlag())
            this.setShow_line_subtotals_tax_selection(srfdomain.getShow_line_subtotals_tax_selection());
        if(srfdomain.getLanguage_idsDirtyFlag())
            this.setLanguage_ids(srfdomain.getLanguage_ids());
        if(srfdomain.getModule_website_sale_deliveryDirtyFlag())
            this.setModule_website_sale_delivery(srfdomain.getModule_website_sale_delivery());
        if(srfdomain.getModule_mrp_mpsDirtyFlag())
            this.setModule_mrp_mps(srfdomain.getModule_mrp_mps());
        if(srfdomain.getPartner_autocomplete_insufficient_creditDirtyFlag())
            this.setPartner_autocomplete_insufficient_credit(srfdomain.getPartner_autocomplete_insufficient_credit());
        if(srfdomain.getModule_hr_org_chartDirtyFlag())
            this.setModule_hr_org_chart(srfdomain.getModule_hr_org_chart());
        if(srfdomain.getModule_product_expiryDirtyFlag())
            this.setModule_product_expiry(srfdomain.getModule_product_expiry());
        if(srfdomain.getModule_delivery_bpostDirtyFlag())
            this.setModule_delivery_bpost(srfdomain.getModule_delivery_bpost());
        if(srfdomain.getModule_stock_barcodeDirtyFlag())
            this.setModule_stock_barcode(srfdomain.getModule_stock_barcode());
        if(srfdomain.getSocial_githubDirtyFlag())
            this.setSocial_github(srfdomain.getSocial_github());
        if(srfdomain.getModule_account_intrastatDirtyFlag())
            this.setModule_account_intrastat(srfdomain.getModule_account_intrastat());
        if(srfdomain.getAuto_done_settingDirtyFlag())
            this.setAuto_done_setting(srfdomain.getAuto_done_setting());
        if(srfdomain.getAuth_signup_uninvitedDirtyFlag())
            this.setAuth_signup_uninvited(srfdomain.getAuth_signup_uninvited());
        if(srfdomain.getCompany_share_productDirtyFlag())
            this.setCompany_share_product(srfdomain.getCompany_share_product());
        if(srfdomain.getGroup_sale_order_templateDirtyFlag())
            this.setGroup_sale_order_template(srfdomain.getGroup_sale_order_template());
        if(srfdomain.getModule_account_sepa_direct_debitDirtyFlag())
            this.setModule_account_sepa_direct_debit(srfdomain.getModule_account_sepa_direct_debit());
        if(srfdomain.getUse_quotation_validity_daysDirtyFlag())
            this.setUse_quotation_validity_days(srfdomain.getUse_quotation_validity_days());
        if(srfdomain.getModule_account_reports_followupDirtyFlag())
            this.setModule_account_reports_followup(srfdomain.getModule_account_reports_followup());
        if(srfdomain.getModule_account_batch_paymentDirtyFlag())
            this.setModule_account_batch_payment(srfdomain.getModule_account_batch_payment());
        if(srfdomain.getSocial_twitterDirtyFlag())
            this.setSocial_twitter(srfdomain.getSocial_twitter());
        if(srfdomain.getModule_account_budgetDirtyFlag())
            this.setModule_account_budget(srfdomain.getModule_account_budget());
        if(srfdomain.getGroup_mrp_routingsDirtyFlag())
            this.setGroup_mrp_routings(srfdomain.getGroup_mrp_routings());
        if(srfdomain.getGroup_cash_roundingDirtyFlag())
            this.setGroup_cash_rounding(srfdomain.getGroup_cash_rounding());
        if(srfdomain.getModule_stock_landed_costsDirtyFlag())
            this.setModule_stock_landed_costs(srfdomain.getModule_stock_landed_costs());
        if(srfdomain.getWebsite_nameDirtyFlag())
            this.setWebsite_name(srfdomain.getWebsite_name());
        if(srfdomain.getModule_website_sale_stockDirtyFlag())
            this.setModule_website_sale_stock(srfdomain.getModule_website_sale_stock());
        if(srfdomain.getGroup_website_popup_on_exitDirtyFlag())
            this.setGroup_website_popup_on_exit(srfdomain.getGroup_website_popup_on_exit());
        if(srfdomain.getModule_website_event_trackDirtyFlag())
            this.setModule_website_event_track(srfdomain.getModule_website_event_track());
        if(srfdomain.getGroup_manage_vendor_priceDirtyFlag())
            this.setGroup_manage_vendor_price(srfdomain.getGroup_manage_vendor_price());
        if(srfdomain.getModule_deliveryDirtyFlag())
            this.setModule_delivery(srfdomain.getModule_delivery());
        if(srfdomain.getModule_account_invoice_extractDirtyFlag())
            this.setModule_account_invoice_extract(srfdomain.getModule_account_invoice_extract());
        if(srfdomain.getChannel_idDirtyFlag())
            this.setChannel_id(srfdomain.getChannel_id());
        if(srfdomain.getGroup_warning_saleDirtyFlag())
            this.setGroup_warning_sale(srfdomain.getGroup_warning_sale());
        if(srfdomain.getCdn_urlDirtyFlag())
            this.setCdn_url(srfdomain.getCdn_url());
        if(srfdomain.getModule_event_barcodeDirtyFlag())
            this.setModule_event_barcode(srfdomain.getModule_event_barcode());
        if(srfdomain.getAlias_domainDirtyFlag())
            this.setAlias_domain(srfdomain.getAlias_domain());
        if(srfdomain.getSocial_linkedinDirtyFlag())
            this.setSocial_linkedin(srfdomain.getSocial_linkedin());
        if(srfdomain.getGroup_stock_multi_warehousesDirtyFlag())
            this.setGroup_stock_multi_warehouses(srfdomain.getGroup_stock_multi_warehouses());
        if(srfdomain.getSalesperson_idDirtyFlag())
            this.setSalesperson_id(srfdomain.getSalesperson_id());
        if(srfdomain.getModule_account_reportsDirtyFlag())
            this.setModule_account_reports(srfdomain.getModule_account_reports());
        if(srfdomain.getGroup_product_pricelistDirtyFlag())
            this.setGroup_product_pricelist(srfdomain.getGroup_product_pricelist());
        if(srfdomain.getModule_crm_phone_validationDirtyFlag())
            this.setModule_crm_phone_validation(srfdomain.getModule_crm_phone_validation());
        if(srfdomain.getModule_website_versionDirtyFlag())
            this.setModule_website_version(srfdomain.getModule_website_version());
        if(srfdomain.getModule_base_importDirtyFlag())
            this.setModule_base_import(srfdomain.getModule_base_import());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getModule_account_bank_statement_import_csvDirtyFlag())
            this.setModule_account_bank_statement_import_csv(srfdomain.getModule_account_bank_statement_import_csv());
        if(srfdomain.getModule_account_taxcloudDirtyFlag())
            this.setModule_account_taxcloud(srfdomain.getModule_account_taxcloud());
        if(srfdomain.getUse_sale_noteDirtyFlag())
            this.setUse_sale_note(srfdomain.getUse_sale_note());
        if(srfdomain.getCart_abandoned_delayDirtyFlag())
            this.setCart_abandoned_delay(srfdomain.getCart_abandoned_delay());
        if(srfdomain.getWebsite_domainDirtyFlag())
            this.setWebsite_domain(srfdomain.getWebsite_domain());
        if(srfdomain.getModule_account_accountantDirtyFlag())
            this.setModule_account_accountant(srfdomain.getModule_account_accountant());
        if(srfdomain.getModule_sale_marginDirtyFlag())
            this.setModule_sale_margin(srfdomain.getModule_sale_margin());
        if(srfdomain.getDigest_emailsDirtyFlag())
            this.setDigest_emails(srfdomain.getDigest_emails());
        if(srfdomain.getModule_padDirtyFlag())
            this.setModule_pad(srfdomain.getModule_pad());
        if(srfdomain.getGroup_warning_accountDirtyFlag())
            this.setGroup_warning_account(srfdomain.getGroup_warning_account());
        if(srfdomain.getGroup_display_incotermDirtyFlag())
            this.setGroup_display_incoterm(srfdomain.getGroup_display_incoterm());
        if(srfdomain.getModule_website_sale_wishlistDirtyFlag())
            this.setModule_website_sale_wishlist(srfdomain.getModule_website_sale_wishlist());
        if(srfdomain.getUser_default_rightsDirtyFlag())
            this.setUser_default_rights(srfdomain.getUser_default_rights());
        if(srfdomain.getDefault_purchase_methodDirtyFlag())
            this.setDefault_purchase_method(srfdomain.getDefault_purchase_method());
        if(srfdomain.getGroup_delivery_invoice_addressDirtyFlag())
            this.setGroup_delivery_invoice_address(srfdomain.getGroup_delivery_invoice_address());
        if(srfdomain.getGroup_lot_on_delivery_slipDirtyFlag())
            this.setGroup_lot_on_delivery_slip(srfdomain.getGroup_lot_on_delivery_slip());
        if(srfdomain.getModule_event_saleDirtyFlag())
            this.setModule_event_sale(srfdomain.getModule_event_sale());
        if(srfdomain.getGroup_show_line_subtotals_tax_includedDirtyFlag())
            this.setGroup_show_line_subtotals_tax_included(srfdomain.getGroup_show_line_subtotals_tax_included());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getGroup_product_variantDirtyFlag())
            this.setGroup_product_variant(srfdomain.getGroup_product_variant());
        if(srfdomain.getModule_account_sepaDirtyFlag())
            this.setModule_account_sepa(srfdomain.getModule_account_sepa());
        if(srfdomain.getGroup_multi_currencyDirtyFlag())
            this.setGroup_multi_currency(srfdomain.getGroup_multi_currency());
        if(srfdomain.getGroup_products_in_billsDirtyFlag())
            this.setGroup_products_in_bills(srfdomain.getGroup_products_in_bills());
        if(srfdomain.getGroup_analytic_accountingDirtyFlag())
            this.setGroup_analytic_accounting(srfdomain.getGroup_analytic_accounting());
        if(srfdomain.getGroup_stock_packagingDirtyFlag())
            this.setGroup_stock_packaging(srfdomain.getGroup_stock_packaging());
        if(srfdomain.getWebsite_slide_google_app_keyDirtyFlag())
            this.setWebsite_slide_google_app_key(srfdomain.getWebsite_slide_google_app_key());
        if(srfdomain.getPo_order_approvalDirtyFlag())
            this.setPo_order_approval(srfdomain.getPo_order_approval());
        if(srfdomain.getIs_installed_saleDirtyFlag())
            this.setIs_installed_sale(srfdomain.getIs_installed_sale());
        if(srfdomain.getModule_account_paymentDirtyFlag())
            this.setModule_account_payment(srfdomain.getModule_account_payment());
        if(srfdomain.getGroup_analytic_tagsDirtyFlag())
            this.setGroup_analytic_tags(srfdomain.getGroup_analytic_tags());
        if(srfdomain.getGroup_sale_order_datesDirtyFlag())
            this.setGroup_sale_order_dates(srfdomain.getGroup_sale_order_dates());
        if(srfdomain.getModule_voipDirtyFlag())
            this.setModule_voip(srfdomain.getModule_voip());
        if(srfdomain.getCart_recovery_mail_templateDirtyFlag())
            this.setCart_recovery_mail_template(srfdomain.getCart_recovery_mail_template());
        if(srfdomain.getGroup_multi_websiteDirtyFlag())
            this.setGroup_multi_website(srfdomain.getGroup_multi_website());
        if(srfdomain.getModule_auth_oauthDirtyFlag())
            this.setModule_auth_oauth(srfdomain.getModule_auth_oauth());
        if(srfdomain.getSale_delivery_settingsDirtyFlag())
            this.setSale_delivery_settings(srfdomain.getSale_delivery_settings());
        if(srfdomain.getModule_sale_quotation_builderDirtyFlag())
            this.setModule_sale_quotation_builder(srfdomain.getModule_sale_quotation_builder());
        if(srfdomain.getModule_inter_company_rulesDirtyFlag())
            this.setModule_inter_company_rules(srfdomain.getModule_inter_company_rules());
        if(srfdomain.getUse_security_leadDirtyFlag())
            this.setUse_security_lead(srfdomain.getUse_security_lead());
        if(srfdomain.getDefault_invoice_policyDirtyFlag())
            this.setDefault_invoice_policy(srfdomain.getDefault_invoice_policy());
        if(srfdomain.getMulti_sales_priceDirtyFlag())
            this.setMulti_sales_price(srfdomain.getMulti_sales_price());
        if(srfdomain.getLock_confirmed_poDirtyFlag())
            this.setLock_confirmed_po(srfdomain.getLock_confirmed_po());
        if(srfdomain.getProduct_weight_in_lbsDirtyFlag())
            this.setProduct_weight_in_lbs(srfdomain.getProduct_weight_in_lbs());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getHas_google_analytics_dashboardDirtyFlag())
            this.setHas_google_analytics_dashboard(srfdomain.getHas_google_analytics_dashboard());
        if(srfdomain.getModule_stock_picking_batchDirtyFlag())
            this.setModule_stock_picking_batch(srfdomain.getModule_stock_picking_batch());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getExpense_alias_prefixDirtyFlag())
            this.setExpense_alias_prefix(srfdomain.getExpense_alias_prefix());
        if(srfdomain.getModule_account_deferred_revenueDirtyFlag())
            this.setModule_account_deferred_revenue(srfdomain.getModule_account_deferred_revenue());
        if(srfdomain.getSocial_facebookDirtyFlag())
            this.setSocial_facebook(srfdomain.getSocial_facebook());
        if(srfdomain.getModule_web_unsplashDirtyFlag())
            this.setModule_web_unsplash(srfdomain.getModule_web_unsplash());
        if(srfdomain.getGroup_mass_mailing_campaignDirtyFlag())
            this.setGroup_mass_mailing_campaign(srfdomain.getGroup_mass_mailing_campaign());
        if(srfdomain.getModule_account_bank_statement_import_camtDirtyFlag())
            this.setModule_account_bank_statement_import_camt(srfdomain.getModule_account_bank_statement_import_camt());
        if(srfdomain.getModule_product_marginDirtyFlag())
            this.setModule_product_margin(srfdomain.getModule_product_margin());
        if(srfdomain.getGroup_subtask_projectDirtyFlag())
            this.setGroup_subtask_project(srfdomain.getGroup_subtask_project());
        if(srfdomain.getModule_account_3way_matchDirtyFlag())
            this.setModule_account_3way_match(srfdomain.getModule_account_3way_match());
        if(srfdomain.getModule_website_sale_digitalDirtyFlag())
            this.setModule_website_sale_digital(srfdomain.getModule_website_sale_digital());
        if(srfdomain.getModule_sale_couponDirtyFlag())
            this.setModule_sale_coupon(srfdomain.getModule_sale_coupon());
        if(srfdomain.getShow_blacklist_buttonsDirtyFlag())
            this.setShow_blacklist_buttons(srfdomain.getShow_blacklist_buttons());
        if(srfdomain.getGroup_warning_stockDirtyFlag())
            this.setGroup_warning_stock(srfdomain.getGroup_warning_stock());
        if(srfdomain.getGroup_multi_companyDirtyFlag())
            this.setGroup_multi_company(srfdomain.getGroup_multi_company());
        if(srfdomain.getGroup_attendance_use_pinDirtyFlag())
            this.setGroup_attendance_use_pin(srfdomain.getGroup_attendance_use_pin());
        if(srfdomain.getSalesteam_idDirtyFlag())
            this.setSalesteam_id(srfdomain.getSalesteam_id());
        if(srfdomain.getFaviconDirtyFlag())
            this.setFavicon(srfdomain.getFavicon());
        if(srfdomain.getModule_website_sale_comparisonDirtyFlag())
            this.setModule_website_sale_comparison(srfdomain.getModule_website_sale_comparison());
        if(srfdomain.getAvailable_thresholdDirtyFlag())
            this.setAvailable_threshold(srfdomain.getAvailable_threshold());
        if(srfdomain.getUse_po_leadDirtyFlag())
            this.setUse_po_lead(srfdomain.getUse_po_lead());
        if(srfdomain.getGroup_fiscal_yearDirtyFlag())
            this.setGroup_fiscal_year(srfdomain.getGroup_fiscal_year());
        if(srfdomain.getModule_hr_timesheetDirtyFlag())
            this.setModule_hr_timesheet(srfdomain.getModule_hr_timesheet());
        if(srfdomain.getModule_mrp_plmDirtyFlag())
            this.setModule_mrp_plm(srfdomain.getModule_mrp_plm());
        if(srfdomain.getModule_account_yodleeDirtyFlag())
            this.setModule_account_yodlee(srfdomain.getModule_account_yodlee());
        if(srfdomain.getCdn_filtersDirtyFlag())
            this.setCdn_filters(srfdomain.getCdn_filters());
        if(srfdomain.getUse_propagation_minimum_deltaDirtyFlag())
            this.setUse_propagation_minimum_delta(srfdomain.getUse_propagation_minimum_delta());
        if(srfdomain.getModule_delivery_upsDirtyFlag())
            this.setModule_delivery_ups(srfdomain.getModule_delivery_ups());
        if(srfdomain.getFail_counterDirtyFlag())
            this.setFail_counter(srfdomain.getFail_counter());
        if(srfdomain.getGroup_stock_production_lotDirtyFlag())
            this.setGroup_stock_production_lot(srfdomain.getGroup_stock_production_lot());
        if(srfdomain.getHas_accounting_entriesDirtyFlag())
            this.setHas_accounting_entries(srfdomain.getHas_accounting_entries());
        if(srfdomain.getGroup_uomDirtyFlag())
            this.setGroup_uom(srfdomain.getGroup_uom());
        if(srfdomain.getMass_mailing_outgoing_mail_serverDirtyFlag())
            this.setMass_mailing_outgoing_mail_server(srfdomain.getMass_mailing_outgoing_mail_server());
        if(srfdomain.getCrm_alias_prefixDirtyFlag())
            this.setCrm_alias_prefix(srfdomain.getCrm_alias_prefix());
        if(srfdomain.getSocial_googleplusDirtyFlag())
            this.setSocial_googleplus(srfdomain.getSocial_googleplus());
        if(srfdomain.getGroup_route_so_linesDirtyFlag())
            this.setGroup_route_so_lines(srfdomain.getGroup_route_so_lines());
        if(srfdomain.getGoogle_management_client_secretDirtyFlag())
            this.setGoogle_management_client_secret(srfdomain.getGoogle_management_client_secret());
        if(srfdomain.getSocial_default_imageDirtyFlag())
            this.setSocial_default_image(srfdomain.getSocial_default_image());
        if(srfdomain.getHas_google_analyticsDirtyFlag())
            this.setHas_google_analytics(srfdomain.getHas_google_analytics());
        if(srfdomain.getModule_website_event_questionsDirtyFlag())
            this.setModule_website_event_questions(srfdomain.getModule_website_event_questions());
        if(srfdomain.getWebsite_default_lang_idDirtyFlag())
            this.setWebsite_default_lang_id(srfdomain.getWebsite_default_lang_id());
        if(srfdomain.getInventory_availabilityDirtyFlag())
            this.setInventory_availability(srfdomain.getInventory_availability());
        if(srfdomain.getGroup_warning_purchaseDirtyFlag())
            this.setGroup_warning_purchase(srfdomain.getGroup_warning_purchase());
        if(srfdomain.getModule_quality_controlDirtyFlag())
            this.setModule_quality_control(srfdomain.getModule_quality_control());
        if(srfdomain.getGenerate_lead_from_aliasDirtyFlag())
            this.setGenerate_lead_from_alias(srfdomain.getGenerate_lead_from_alias());
        if(srfdomain.getSale_pricelist_settingDirtyFlag())
            this.setSale_pricelist_setting(srfdomain.getSale_pricelist_setting());
        if(srfdomain.getGroup_pricelist_itemDirtyFlag())
            this.setGroup_pricelist_item(srfdomain.getGroup_pricelist_item());
        if(srfdomain.getExternal_email_server_defaultDirtyFlag())
            this.setExternal_email_server_default(srfdomain.getExternal_email_server_default());
        if(srfdomain.getUnsplash_access_keyDirtyFlag())
            this.setUnsplash_access_key(srfdomain.getUnsplash_access_key());
        if(srfdomain.getModule_base_gengoDirtyFlag())
            this.setModule_base_gengo(srfdomain.getModule_base_gengo());
        if(srfdomain.getModule_website_event_saleDirtyFlag())
            this.setModule_website_event_sale(srfdomain.getModule_website_event_sale());
        if(srfdomain.getCrm_default_user_idDirtyFlag())
            this.setCrm_default_user_id(srfdomain.getCrm_default_user_id());
        if(srfdomain.getModule_stock_dropshippingDirtyFlag())
            this.setModule_stock_dropshipping(srfdomain.getModule_stock_dropshipping());
        if(srfdomain.getModule_crm_revealDirtyFlag())
            this.setModule_crm_reveal(srfdomain.getModule_crm_reveal());
        if(srfdomain.getMass_mailing_mail_server_idDirtyFlag())
            this.setMass_mailing_mail_server_id(srfdomain.getMass_mailing_mail_server_id());
        if(srfdomain.getGroup_show_line_subtotals_tax_excludedDirtyFlag())
            this.setGroup_show_line_subtotals_tax_excluded(srfdomain.getGroup_show_line_subtotals_tax_excluded());
        if(srfdomain.getModule_hr_recruitment_surveyDirtyFlag())
            this.setModule_hr_recruitment_survey(srfdomain.getModule_hr_recruitment_survey());
        if(srfdomain.getModule_mrp_workorderDirtyFlag())
            this.setModule_mrp_workorder(srfdomain.getModule_mrp_workorder());
        if(srfdomain.getModule_pos_mercuryDirtyFlag())
            this.setModule_pos_mercury(srfdomain.getModule_pos_mercury());
        if(srfdomain.getInvoice_reference_typeDirtyFlag())
            this.setInvoice_reference_type(srfdomain.getInvoice_reference_type());
        if(srfdomain.getCompany_currency_idDirtyFlag())
            this.setCompany_currency_id(srfdomain.getCompany_currency_id());
        if(srfdomain.getTax_exigibilityDirtyFlag())
            this.setTax_exigibility(srfdomain.getTax_exigibility());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getAccount_bank_reconciliation_startDirtyFlag())
            this.setAccount_bank_reconciliation_start(srfdomain.getAccount_bank_reconciliation_start());
        if(srfdomain.getTax_cash_basis_journal_idDirtyFlag())
            this.setTax_cash_basis_journal_id(srfdomain.getTax_cash_basis_journal_id());
        if(srfdomain.getPo_leadDirtyFlag())
            this.setPo_lead(srfdomain.getPo_lead());
        if(srfdomain.getSnailmail_colorDirtyFlag())
            this.setSnailmail_color(srfdomain.getSnailmail_color());
        if(srfdomain.getPaperformat_idDirtyFlag())
            this.setPaperformat_id(srfdomain.getPaperformat_id());
        if(srfdomain.getPortal_confirmation_signDirtyFlag())
            this.setPortal_confirmation_sign(srfdomain.getPortal_confirmation_sign());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getDigest_id_textDirtyFlag())
            this.setDigest_id_text(srfdomain.getDigest_id_text());
        if(srfdomain.getSale_noteDirtyFlag())
            this.setSale_note(srfdomain.getSale_note());
        if(srfdomain.getAuth_signup_template_user_id_textDirtyFlag())
            this.setAuth_signup_template_user_id_text(srfdomain.getAuth_signup_template_user_id_text());
        if(srfdomain.getInvoice_is_printDirtyFlag())
            this.setInvoice_is_print(srfdomain.getInvoice_is_print());
        if(srfdomain.getDeposit_default_product_id_textDirtyFlag())
            this.setDeposit_default_product_id_text(srfdomain.getDeposit_default_product_id_text());
        if(srfdomain.getResource_calendar_idDirtyFlag())
            this.setResource_calendar_id(srfdomain.getResource_calendar_id());
        if(srfdomain.getPo_lockDirtyFlag())
            this.setPo_lock(srfdomain.getPo_lock());
        if(srfdomain.getInvoice_is_snailmailDirtyFlag())
            this.setInvoice_is_snailmail(srfdomain.getInvoice_is_snailmail());
        if(srfdomain.getManufacturing_leadDirtyFlag())
            this.setManufacturing_lead(srfdomain.getManufacturing_lead());
        if(srfdomain.getPo_double_validationDirtyFlag())
            this.setPo_double_validation(srfdomain.getPo_double_validation());
        if(srfdomain.getReport_footerDirtyFlag())
            this.setReport_footer(srfdomain.getReport_footer());
        if(srfdomain.getTemplate_id_textDirtyFlag())
            this.setTemplate_id_text(srfdomain.getTemplate_id_text());
        if(srfdomain.getInvoice_is_emailDirtyFlag())
            this.setInvoice_is_email(srfdomain.getInvoice_is_email());
        if(srfdomain.getPortal_confirmation_payDirtyFlag())
            this.setPortal_confirmation_pay(srfdomain.getPortal_confirmation_pay());
        if(srfdomain.getQr_codeDirtyFlag())
            this.setQr_code(srfdomain.getQr_code());
        if(srfdomain.getSnailmail_duplexDirtyFlag())
            this.setSnailmail_duplex(srfdomain.getSnailmail_duplex());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getSecurity_leadDirtyFlag())
            this.setSecurity_lead(srfdomain.getSecurity_lead());
        if(srfdomain.getExternal_report_layout_idDirtyFlag())
            this.setExternal_report_layout_id(srfdomain.getExternal_report_layout_id());
        if(srfdomain.getPurchase_tax_idDirtyFlag())
            this.setPurchase_tax_id(srfdomain.getPurchase_tax_id());
        if(srfdomain.getPropagation_minimum_deltaDirtyFlag())
            this.setPropagation_minimum_delta(srfdomain.getPropagation_minimum_delta());
        if(srfdomain.getQuotation_validity_daysDirtyFlag())
            this.setQuotation_validity_days(srfdomain.getQuotation_validity_days());
        if(srfdomain.getTax_calculation_rounding_methodDirtyFlag())
            this.setTax_calculation_rounding_method(srfdomain.getTax_calculation_rounding_method());
        if(srfdomain.getCurrency_exchange_journal_idDirtyFlag())
            this.setCurrency_exchange_journal_id(srfdomain.getCurrency_exchange_journal_id());
        if(srfdomain.getPo_double_validation_amountDirtyFlag())
            this.setPo_double_validation_amount(srfdomain.getPo_double_validation_amount());
        if(srfdomain.getChart_template_id_textDirtyFlag())
            this.setChart_template_id_text(srfdomain.getChart_template_id_text());
        if(srfdomain.getSale_tax_idDirtyFlag())
            this.setSale_tax_id(srfdomain.getSale_tax_id());
        if(srfdomain.getDefault_sale_order_template_id_textDirtyFlag())
            this.setDefault_sale_order_template_id_text(srfdomain.getDefault_sale_order_template_id_text());
        if(srfdomain.getDefault_sale_order_template_idDirtyFlag())
            this.setDefault_sale_order_template_id(srfdomain.getDefault_sale_order_template_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getDigest_idDirtyFlag())
            this.setDigest_id(srfdomain.getDigest_id());
        if(srfdomain.getTemplate_idDirtyFlag())
            this.setTemplate_id(srfdomain.getTemplate_id());
        if(srfdomain.getChart_template_idDirtyFlag())
            this.setChart_template_id(srfdomain.getChart_template_id());
        if(srfdomain.getDeposit_default_product_idDirtyFlag())
            this.setDeposit_default_product_id(srfdomain.getDeposit_default_product_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getAuth_signup_template_user_idDirtyFlag())
            this.setAuth_signup_template_user_id(srfdomain.getAuth_signup_template_user_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Res_config_settingsDTO> fromDOPage(List<Res_config_settings> poPage)   {
        if(poPage == null)
            return null;
        List<Res_config_settingsDTO> dtos=new ArrayList<Res_config_settingsDTO>();
        for(Res_config_settings domain : poPage) {
            Res_config_settingsDTO dto = new Res_config_settingsDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

