package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_automationDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_automationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_automation" })
@RestController
@RequestMapping("")
public class Base_automationResource {

    @Autowired
    private IBase_automationService base_automationService;

    public IBase_automationService getBase_automationService() {
        return this.base_automationService;
    }

    @ApiOperation(value = "获取数据", tags = {"Base_automation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automations/{base_automation_id}")
    public ResponseEntity<Base_automationDTO> get(@PathVariable("base_automation_id") Integer base_automation_id) {
        Base_automationDTO dto = new Base_automationDTO();
        Base_automation domain = base_automationService.get(base_automation_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_automation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_automations")

    public ResponseEntity<Base_automationDTO> create(@RequestBody Base_automationDTO base_automationdto) {
        Base_automationDTO dto = new Base_automationDTO();
        Base_automation domain = base_automationdto.toDO();
		base_automationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_automation" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_automations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_automationDTO> base_automationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_automation" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_automations/createBatch")
    public ResponseEntity<Boolean> createBatchBase_automation(@RequestBody List<Base_automationDTO> base_automationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_automation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_automations/{base_automation_id}")

    public ResponseEntity<Base_automationDTO> update(@PathVariable("base_automation_id") Integer base_automation_id, @RequestBody Base_automationDTO base_automationdto) {
		Base_automation domain = base_automationdto.toDO();
        domain.setId(base_automation_id);
		base_automationService.update(domain);
		Base_automationDTO dto = new Base_automationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_automation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_automations/{base_automation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_automation_id") Integer base_automation_id) {
        Base_automationDTO base_automationdto = new Base_automationDTO();
		Base_automation domain = new Base_automation();
		base_automationdto.setId(base_automation_id);
		domain.setId(base_automation_id);
        Boolean rst = base_automationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_automation" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_automations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_automationDTO> base_automationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_automation" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_automations/fetchdefault")
	public ResponseEntity<Page<Base_automationDTO>> fetchDefault(Base_automationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_automationDTO> list = new ArrayList<Base_automationDTO>();
        
        Page<Base_automation> domains = base_automationService.searchDefault(context) ;
        for(Base_automation base_automation : domains.getContent()){
            Base_automationDTO dto = new Base_automationDTO();
            dto.fromDO(base_automation);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
