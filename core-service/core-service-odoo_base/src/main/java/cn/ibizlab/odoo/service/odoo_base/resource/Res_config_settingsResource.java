package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_config_settingsDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_config_settingsService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_settingsSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_config_settings" })
@RestController
@RequestMapping("")
public class Res_config_settingsResource {

    @Autowired
    private IRes_config_settingsService res_config_settingsService;

    public IRes_config_settingsService getRes_config_settingsService() {
        return this.res_config_settingsService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_config_settings" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_config_settings/createBatch")
    public ResponseEntity<Boolean> createBatchRes_config_settings(@RequestBody List<Res_config_settingsDTO> res_config_settingsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_config_settings" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_config_settings/{res_config_settings_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_config_settings_id") Integer res_config_settings_id) {
        Res_config_settingsDTO res_config_settingsdto = new Res_config_settingsDTO();
		Res_config_settings domain = new Res_config_settings();
		res_config_settingsdto.setId(res_config_settings_id);
		domain.setId(res_config_settings_id);
        Boolean rst = res_config_settingsService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_config_settings" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_config_settings/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_config_settingsDTO> res_config_settingsdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_config_settings" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_config_settings/{res_config_settings_id}")
    public ResponseEntity<Res_config_settingsDTO> get(@PathVariable("res_config_settings_id") Integer res_config_settings_id) {
        Res_config_settingsDTO dto = new Res_config_settingsDTO();
        Res_config_settings domain = res_config_settingsService.get(res_config_settings_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_config_settings" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_config_settings/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_config_settingsDTO> res_config_settingsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_config_settings" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_config_settings/{res_config_settings_id}")

    public ResponseEntity<Res_config_settingsDTO> update(@PathVariable("res_config_settings_id") Integer res_config_settings_id, @RequestBody Res_config_settingsDTO res_config_settingsdto) {
		Res_config_settings domain = res_config_settingsdto.toDO();
        domain.setId(res_config_settings_id);
		res_config_settingsService.update(domain);
		Res_config_settingsDTO dto = new Res_config_settingsDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_config_settings" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_config_settings")

    public ResponseEntity<Res_config_settingsDTO> create(@RequestBody Res_config_settingsDTO res_config_settingsdto) {
        Res_config_settingsDTO dto = new Res_config_settingsDTO();
        Res_config_settings domain = res_config_settingsdto.toDO();
		res_config_settingsService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_config_settings" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_config_settings/fetchdefault")
	public ResponseEntity<Page<Res_config_settingsDTO>> fetchDefault(Res_config_settingsSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_config_settingsDTO> list = new ArrayList<Res_config_settingsDTO>();
        
        Page<Res_config_settings> domains = res_config_settingsService.searchDefault(context) ;
        for(Res_config_settings res_config_settings : domains.getContent()){
            Res_config_settingsDTO dto = new Res_config_settingsDTO();
            dto.fromDO(res_config_settings);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
