package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_users_logDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users_log;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_users_logService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_users_logSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_users_log" })
@RestController
@RequestMapping("")
public class Res_users_logResource {

    @Autowired
    private IRes_users_logService res_users_logService;

    public IRes_users_logService getRes_users_logService() {
        return this.res_users_logService;
    }

    @ApiOperation(value = "更新数据", tags = {"Res_users_log" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_users_logs/{res_users_log_id}")

    public ResponseEntity<Res_users_logDTO> update(@PathVariable("res_users_log_id") Integer res_users_log_id, @RequestBody Res_users_logDTO res_users_logdto) {
		Res_users_log domain = res_users_logdto.toDO();
        domain.setId(res_users_log_id);
		res_users_logService.update(domain);
		Res_users_logDTO dto = new Res_users_logDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_users_log" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_users_logs/{res_users_log_id}")
    public ResponseEntity<Res_users_logDTO> get(@PathVariable("res_users_log_id") Integer res_users_log_id) {
        Res_users_logDTO dto = new Res_users_logDTO();
        Res_users_log domain = res_users_logService.get(res_users_log_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_users_log" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_users_logs")

    public ResponseEntity<Res_users_logDTO> create(@RequestBody Res_users_logDTO res_users_logdto) {
        Res_users_logDTO dto = new Res_users_logDTO();
        Res_users_log domain = res_users_logdto.toDO();
		res_users_logService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_users_log" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_users_logs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_users_logDTO> res_users_logdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_users_log" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_users_logs/{res_users_log_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_users_log_id") Integer res_users_log_id) {
        Res_users_logDTO res_users_logdto = new Res_users_logDTO();
		Res_users_log domain = new Res_users_log();
		res_users_logdto.setId(res_users_log_id);
		domain.setId(res_users_log_id);
        Boolean rst = res_users_logService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_users_log" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_users_logs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_users_logDTO> res_users_logdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_users_log" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_users_logs/createBatch")
    public ResponseEntity<Boolean> createBatchRes_users_log(@RequestBody List<Res_users_logDTO> res_users_logdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_users_log" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_users_logs/fetchdefault")
	public ResponseEntity<Page<Res_users_logDTO>> fetchDefault(Res_users_logSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_users_logDTO> list = new ArrayList<Res_users_logDTO>();
        
        Page<Res_users_log> domains = res_users_logService.searchDefault(context) ;
        for(Res_users_log res_users_log : domains.getContent()){
            Res_users_logDTO dto = new Res_users_logDTO();
            dto.fromDO(res_users_log);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
