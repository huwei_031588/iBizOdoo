package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_partner_merge_automatic_wizardDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_partner_merge_automatic_wizardService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_partner_merge_automatic_wizard" })
@RestController
@RequestMapping("")
public class Base_partner_merge_automatic_wizardResource {

    @Autowired
    private IBase_partner_merge_automatic_wizardService base_partner_merge_automatic_wizardService;

    public IBase_partner_merge_automatic_wizardService getBase_partner_merge_automatic_wizardService() {
        return this.base_partner_merge_automatic_wizardService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_partner_merge_automatic_wizards/createBatch")
    public ResponseEntity<Boolean> createBatchBase_partner_merge_automatic_wizard(@RequestBody List<Base_partner_merge_automatic_wizardDTO> base_partner_merge_automatic_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_partner_merge_automatic_wizards/{base_partner_merge_automatic_wizard_id}")

    public ResponseEntity<Base_partner_merge_automatic_wizardDTO> update(@PathVariable("base_partner_merge_automatic_wizard_id") Integer base_partner_merge_automatic_wizard_id, @RequestBody Base_partner_merge_automatic_wizardDTO base_partner_merge_automatic_wizarddto) {
		Base_partner_merge_automatic_wizard domain = base_partner_merge_automatic_wizarddto.toDO();
        domain.setId(base_partner_merge_automatic_wizard_id);
		base_partner_merge_automatic_wizardService.update(domain);
		Base_partner_merge_automatic_wizardDTO dto = new Base_partner_merge_automatic_wizardDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_partner_merge_automatic_wizards/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_partner_merge_automatic_wizardDTO> base_partner_merge_automatic_wizarddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_partner_merge_automatic_wizards/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_partner_merge_automatic_wizardDTO> base_partner_merge_automatic_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_partner_merge_automatic_wizards")

    public ResponseEntity<Base_partner_merge_automatic_wizardDTO> create(@RequestBody Base_partner_merge_automatic_wizardDTO base_partner_merge_automatic_wizarddto) {
        Base_partner_merge_automatic_wizardDTO dto = new Base_partner_merge_automatic_wizardDTO();
        Base_partner_merge_automatic_wizard domain = base_partner_merge_automatic_wizarddto.toDO();
		base_partner_merge_automatic_wizardService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_partner_merge_automatic_wizards/{base_partner_merge_automatic_wizard_id}")
    public ResponseEntity<Base_partner_merge_automatic_wizardDTO> get(@PathVariable("base_partner_merge_automatic_wizard_id") Integer base_partner_merge_automatic_wizard_id) {
        Base_partner_merge_automatic_wizardDTO dto = new Base_partner_merge_automatic_wizardDTO();
        Base_partner_merge_automatic_wizard domain = base_partner_merge_automatic_wizardService.get(base_partner_merge_automatic_wizard_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_partner_merge_automatic_wizards/{base_partner_merge_automatic_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_partner_merge_automatic_wizard_id") Integer base_partner_merge_automatic_wizard_id) {
        Base_partner_merge_automatic_wizardDTO base_partner_merge_automatic_wizarddto = new Base_partner_merge_automatic_wizardDTO();
		Base_partner_merge_automatic_wizard domain = new Base_partner_merge_automatic_wizard();
		base_partner_merge_automatic_wizarddto.setId(base_partner_merge_automatic_wizard_id);
		domain.setId(base_partner_merge_automatic_wizard_id);
        Boolean rst = base_partner_merge_automatic_wizardService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_partner_merge_automatic_wizard" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_partner_merge_automatic_wizards/fetchdefault")
	public ResponseEntity<Page<Base_partner_merge_automatic_wizardDTO>> fetchDefault(Base_partner_merge_automatic_wizardSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_partner_merge_automatic_wizardDTO> list = new ArrayList<Base_partner_merge_automatic_wizardDTO>();
        
        Page<Base_partner_merge_automatic_wizard> domains = base_partner_merge_automatic_wizardService.searchDefault(context) ;
        for(Base_partner_merge_automatic_wizard base_partner_merge_automatic_wizard : domains.getContent()){
            Base_partner_merge_automatic_wizardDTO dto = new Base_partner_merge_automatic_wizardDTO();
            dto.fromDO(base_partner_merge_automatic_wizard);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
