package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_applicant_categoryDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant_category;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_applicant_categoryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicant_categorySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_applicant_category" })
@RestController
@RequestMapping("")
public class Hr_applicant_categoryResource {

    @Autowired
    private IHr_applicant_categoryService hr_applicant_categoryService;

    public IHr_applicant_categoryService getHr_applicant_categoryService() {
        return this.hr_applicant_categoryService;
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_applicant_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_applicant_categories/{hr_applicant_category_id}")

    public ResponseEntity<Hr_applicant_categoryDTO> update(@PathVariable("hr_applicant_category_id") Integer hr_applicant_category_id, @RequestBody Hr_applicant_categoryDTO hr_applicant_categorydto) {
		Hr_applicant_category domain = hr_applicant_categorydto.toDO();
        domain.setId(hr_applicant_category_id);
		hr_applicant_categoryService.update(domain);
		Hr_applicant_categoryDTO dto = new Hr_applicant_categoryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_applicant_category" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_applicant_categories/createBatch")
    public ResponseEntity<Boolean> createBatchHr_applicant_category(@RequestBody List<Hr_applicant_categoryDTO> hr_applicant_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_applicant_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_applicant_categories/{hr_applicant_category_id}")
    public ResponseEntity<Hr_applicant_categoryDTO> get(@PathVariable("hr_applicant_category_id") Integer hr_applicant_category_id) {
        Hr_applicant_categoryDTO dto = new Hr_applicant_categoryDTO();
        Hr_applicant_category domain = hr_applicant_categoryService.get(hr_applicant_category_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_applicant_category" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_applicant_categories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_applicant_categoryDTO> hr_applicant_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_applicant_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_applicant_categories")

    public ResponseEntity<Hr_applicant_categoryDTO> create(@RequestBody Hr_applicant_categoryDTO hr_applicant_categorydto) {
        Hr_applicant_categoryDTO dto = new Hr_applicant_categoryDTO();
        Hr_applicant_category domain = hr_applicant_categorydto.toDO();
		hr_applicant_categoryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_applicant_category" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_applicant_categories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_applicant_categoryDTO> hr_applicant_categorydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_applicant_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_applicant_categories/{hr_applicant_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_applicant_category_id") Integer hr_applicant_category_id) {
        Hr_applicant_categoryDTO hr_applicant_categorydto = new Hr_applicant_categoryDTO();
		Hr_applicant_category domain = new Hr_applicant_category();
		hr_applicant_categorydto.setId(hr_applicant_category_id);
		domain.setId(hr_applicant_category_id);
        Boolean rst = hr_applicant_categoryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_applicant_category" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_applicant_categories/fetchdefault")
	public ResponseEntity<Page<Hr_applicant_categoryDTO>> fetchDefault(Hr_applicant_categorySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_applicant_categoryDTO> list = new ArrayList<Hr_applicant_categoryDTO>();
        
        Page<Hr_applicant_category> domains = hr_applicant_categoryService.searchDefault(context) ;
        for(Hr_applicant_category hr_applicant_category : domains.getContent()){
            Hr_applicant_categoryDTO dto = new Hr_applicant_categoryDTO();
            dto.fromDO(hr_applicant_category);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
