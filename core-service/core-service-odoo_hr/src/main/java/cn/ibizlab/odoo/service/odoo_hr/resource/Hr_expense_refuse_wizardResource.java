package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_expense_refuse_wizardDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_refuse_wizard;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expense_refuse_wizardService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_refuse_wizardSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_expense_refuse_wizard" })
@RestController
@RequestMapping("")
public class Hr_expense_refuse_wizardResource {

    @Autowired
    private IHr_expense_refuse_wizardService hr_expense_refuse_wizardService;

    public IHr_expense_refuse_wizardService getHr_expense_refuse_wizardService() {
        return this.hr_expense_refuse_wizardService;
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_expense_refuse_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expense_refuse_wizards")

    public ResponseEntity<Hr_expense_refuse_wizardDTO> create(@RequestBody Hr_expense_refuse_wizardDTO hr_expense_refuse_wizarddto) {
        Hr_expense_refuse_wizardDTO dto = new Hr_expense_refuse_wizardDTO();
        Hr_expense_refuse_wizard domain = hr_expense_refuse_wizarddto.toDO();
		hr_expense_refuse_wizardService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_expense_refuse_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expense_refuse_wizards/{hr_expense_refuse_wizard_id}")

    public ResponseEntity<Hr_expense_refuse_wizardDTO> update(@PathVariable("hr_expense_refuse_wizard_id") Integer hr_expense_refuse_wizard_id, @RequestBody Hr_expense_refuse_wizardDTO hr_expense_refuse_wizarddto) {
		Hr_expense_refuse_wizard domain = hr_expense_refuse_wizarddto.toDO();
        domain.setId(hr_expense_refuse_wizard_id);
		hr_expense_refuse_wizardService.update(domain);
		Hr_expense_refuse_wizardDTO dto = new Hr_expense_refuse_wizardDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_expense_refuse_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_refuse_wizards/{hr_expense_refuse_wizard_id}")
    public ResponseEntity<Hr_expense_refuse_wizardDTO> get(@PathVariable("hr_expense_refuse_wizard_id") Integer hr_expense_refuse_wizard_id) {
        Hr_expense_refuse_wizardDTO dto = new Hr_expense_refuse_wizardDTO();
        Hr_expense_refuse_wizard domain = hr_expense_refuse_wizardService.get(hr_expense_refuse_wizard_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_expense_refuse_wizard" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expense_refuse_wizards/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_expense_refuse_wizardDTO> hr_expense_refuse_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_expense_refuse_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expense_refuse_wizards/{hr_expense_refuse_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_expense_refuse_wizard_id") Integer hr_expense_refuse_wizard_id) {
        Hr_expense_refuse_wizardDTO hr_expense_refuse_wizarddto = new Hr_expense_refuse_wizardDTO();
		Hr_expense_refuse_wizard domain = new Hr_expense_refuse_wizard();
		hr_expense_refuse_wizarddto.setId(hr_expense_refuse_wizard_id);
		domain.setId(hr_expense_refuse_wizard_id);
        Boolean rst = hr_expense_refuse_wizardService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_expense_refuse_wizard" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expense_refuse_wizards/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_expense_refuse_wizardDTO> hr_expense_refuse_wizarddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_expense_refuse_wizard" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expense_refuse_wizards/createBatch")
    public ResponseEntity<Boolean> createBatchHr_expense_refuse_wizard(@RequestBody List<Hr_expense_refuse_wizardDTO> hr_expense_refuse_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_expense_refuse_wizard" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_expense_refuse_wizards/fetchdefault")
	public ResponseEntity<Page<Hr_expense_refuse_wizardDTO>> fetchDefault(Hr_expense_refuse_wizardSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_expense_refuse_wizardDTO> list = new ArrayList<Hr_expense_refuse_wizardDTO>();
        
        Page<Hr_expense_refuse_wizard> domains = hr_expense_refuse_wizardService.searchDefault(context) ;
        for(Hr_expense_refuse_wizard hr_expense_refuse_wizard : domains.getContent()){
            Hr_expense_refuse_wizardDTO dto = new Hr_expense_refuse_wizardDTO();
            dto.fromDO(hr_expense_refuse_wizard);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
