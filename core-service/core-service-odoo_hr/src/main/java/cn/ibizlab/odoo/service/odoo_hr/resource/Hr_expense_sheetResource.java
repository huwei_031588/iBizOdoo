package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_expense_sheetDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expense_sheetService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheetSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_expense_sheet" })
@RestController
@RequestMapping("")
public class Hr_expense_sheetResource {

    @Autowired
    private IHr_expense_sheetService hr_expense_sheetService;

    public IHr_expense_sheetService getHr_expense_sheetService() {
        return this.hr_expense_sheetService;
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_expense_sheet" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expense_sheets/{hr_expense_sheet_id}")

    public ResponseEntity<Hr_expense_sheetDTO> update(@PathVariable("hr_expense_sheet_id") Integer hr_expense_sheet_id, @RequestBody Hr_expense_sheetDTO hr_expense_sheetdto) {
		Hr_expense_sheet domain = hr_expense_sheetdto.toDO();
        domain.setId(hr_expense_sheet_id);
		hr_expense_sheetService.update(domain);
		Hr_expense_sheetDTO dto = new Hr_expense_sheetDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_expense_sheet" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_sheets/{hr_expense_sheet_id}")
    public ResponseEntity<Hr_expense_sheetDTO> get(@PathVariable("hr_expense_sheet_id") Integer hr_expense_sheet_id) {
        Hr_expense_sheetDTO dto = new Hr_expense_sheetDTO();
        Hr_expense_sheet domain = hr_expense_sheetService.get(hr_expense_sheet_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_expense_sheet" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expense_sheets/createBatch")
    public ResponseEntity<Boolean> createBatchHr_expense_sheet(@RequestBody List<Hr_expense_sheetDTO> hr_expense_sheetdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_expense_sheet" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expense_sheets/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_expense_sheetDTO> hr_expense_sheetdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_expense_sheet" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expense_sheets")

    public ResponseEntity<Hr_expense_sheetDTO> create(@RequestBody Hr_expense_sheetDTO hr_expense_sheetdto) {
        Hr_expense_sheetDTO dto = new Hr_expense_sheetDTO();
        Hr_expense_sheet domain = hr_expense_sheetdto.toDO();
		hr_expense_sheetService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_expense_sheet" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expense_sheets/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_expense_sheetDTO> hr_expense_sheetdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_expense_sheet" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expense_sheets/{hr_expense_sheet_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_expense_sheet_id") Integer hr_expense_sheet_id) {
        Hr_expense_sheetDTO hr_expense_sheetdto = new Hr_expense_sheetDTO();
		Hr_expense_sheet domain = new Hr_expense_sheet();
		hr_expense_sheetdto.setId(hr_expense_sheet_id);
		domain.setId(hr_expense_sheet_id);
        Boolean rst = hr_expense_sheetService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_expense_sheet" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_expense_sheets/fetchdefault")
	public ResponseEntity<Page<Hr_expense_sheetDTO>> fetchDefault(Hr_expense_sheetSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_expense_sheetDTO> list = new ArrayList<Hr_expense_sheetDTO>();
        
        Page<Hr_expense_sheet> domains = hr_expense_sheetService.searchDefault(context) ;
        for(Hr_expense_sheet hr_expense_sheet : domains.getContent()){
            Hr_expense_sheetDTO dto = new Hr_expense_sheetDTO();
            dto.fromDO(hr_expense_sheet);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
