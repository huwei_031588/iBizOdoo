package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_employee_categoryDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee_category;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_employee_categoryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employee_categorySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_employee_category" })
@RestController
@RequestMapping("")
public class Hr_employee_categoryResource {

    @Autowired
    private IHr_employee_categoryService hr_employee_categoryService;

    public IHr_employee_categoryService getHr_employee_categoryService() {
        return this.hr_employee_categoryService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_employee_category" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_employee_categories/createBatch")
    public ResponseEntity<Boolean> createBatchHr_employee_category(@RequestBody List<Hr_employee_categoryDTO> hr_employee_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_employee_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_employee_categories")

    public ResponseEntity<Hr_employee_categoryDTO> create(@RequestBody Hr_employee_categoryDTO hr_employee_categorydto) {
        Hr_employee_categoryDTO dto = new Hr_employee_categoryDTO();
        Hr_employee_category domain = hr_employee_categorydto.toDO();
		hr_employee_categoryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_employee_category" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_employee_categories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_employee_categoryDTO> hr_employee_categorydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_employee_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_employee_categories/{hr_employee_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_employee_category_id") Integer hr_employee_category_id) {
        Hr_employee_categoryDTO hr_employee_categorydto = new Hr_employee_categoryDTO();
		Hr_employee_category domain = new Hr_employee_category();
		hr_employee_categorydto.setId(hr_employee_category_id);
		domain.setId(hr_employee_category_id);
        Boolean rst = hr_employee_categoryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_employee_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_employee_categories/{hr_employee_category_id}")

    public ResponseEntity<Hr_employee_categoryDTO> update(@PathVariable("hr_employee_category_id") Integer hr_employee_category_id, @RequestBody Hr_employee_categoryDTO hr_employee_categorydto) {
		Hr_employee_category domain = hr_employee_categorydto.toDO();
        domain.setId(hr_employee_category_id);
		hr_employee_categoryService.update(domain);
		Hr_employee_categoryDTO dto = new Hr_employee_categoryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_employee_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_employee_categories/{hr_employee_category_id}")
    public ResponseEntity<Hr_employee_categoryDTO> get(@PathVariable("hr_employee_category_id") Integer hr_employee_category_id) {
        Hr_employee_categoryDTO dto = new Hr_employee_categoryDTO();
        Hr_employee_category domain = hr_employee_categoryService.get(hr_employee_category_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_employee_category" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_employee_categories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_employee_categoryDTO> hr_employee_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_employee_category" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_employee_categories/fetchdefault")
	public ResponseEntity<Page<Hr_employee_categoryDTO>> fetchDefault(Hr_employee_categorySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_employee_categoryDTO> list = new ArrayList<Hr_employee_categoryDTO>();
        
        Page<Hr_employee_category> domains = hr_employee_categoryService.searchDefault(context) ;
        for(Hr_employee_category hr_employee_category : domains.getContent()){
            Hr_employee_categoryDTO dto = new Hr_employee_categoryDTO();
            dto.fromDO(hr_employee_category);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
