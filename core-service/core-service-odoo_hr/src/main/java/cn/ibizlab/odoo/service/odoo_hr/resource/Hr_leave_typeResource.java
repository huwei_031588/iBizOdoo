package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_leave_typeDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_typeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_typeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_leave_type" })
@RestController
@RequestMapping("")
public class Hr_leave_typeResource {

    @Autowired
    private IHr_leave_typeService hr_leave_typeService;

    public IHr_leave_typeService getHr_leave_typeService() {
        return this.hr_leave_typeService;
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_leave_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_types")

    public ResponseEntity<Hr_leave_typeDTO> create(@RequestBody Hr_leave_typeDTO hr_leave_typedto) {
        Hr_leave_typeDTO dto = new Hr_leave_typeDTO();
        Hr_leave_type domain = hr_leave_typedto.toDO();
		hr_leave_typeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_leave_type" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_types/createBatch")
    public ResponseEntity<Boolean> createBatchHr_leave_type(@RequestBody List<Hr_leave_typeDTO> hr_leave_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_leave_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_types/{hr_leave_type_id}")

    public ResponseEntity<Hr_leave_typeDTO> update(@PathVariable("hr_leave_type_id") Integer hr_leave_type_id, @RequestBody Hr_leave_typeDTO hr_leave_typedto) {
		Hr_leave_type domain = hr_leave_typedto.toDO();
        domain.setId(hr_leave_type_id);
		hr_leave_typeService.update(domain);
		Hr_leave_typeDTO dto = new Hr_leave_typeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_leave_type" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_types/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leave_typeDTO> hr_leave_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_leave_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_types/{hr_leave_type_id}")
    public ResponseEntity<Hr_leave_typeDTO> get(@PathVariable("hr_leave_type_id") Integer hr_leave_type_id) {
        Hr_leave_typeDTO dto = new Hr_leave_typeDTO();
        Hr_leave_type domain = hr_leave_typeService.get(hr_leave_type_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_leave_type" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_types/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_leave_typeDTO> hr_leave_typedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_leave_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_types/{hr_leave_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_type_id") Integer hr_leave_type_id) {
        Hr_leave_typeDTO hr_leave_typedto = new Hr_leave_typeDTO();
		Hr_leave_type domain = new Hr_leave_type();
		hr_leave_typedto.setId(hr_leave_type_id);
		domain.setId(hr_leave_type_id);
        Boolean rst = hr_leave_typeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_leave_type" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_leave_types/fetchdefault")
	public ResponseEntity<Page<Hr_leave_typeDTO>> fetchDefault(Hr_leave_typeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_leave_typeDTO> list = new ArrayList<Hr_leave_typeDTO>();
        
        Page<Hr_leave_type> domains = hr_leave_typeService.searchDefault(context) ;
        for(Hr_leave_type hr_leave_type : domains.getContent()){
            Hr_leave_typeDTO dto = new Hr_leave_typeDTO();
            dto.fromDO(hr_leave_type);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
