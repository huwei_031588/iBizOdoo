package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_leave_allocation.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_allocation;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_leave_allocationDTO]
 */
public class Hr_leave_allocationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Hr_leave_allocationMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Hr_leave_allocationNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DATE_TO]
     *
     */
    @Hr_leave_allocationDate_toDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_to;

    @JsonIgnore
    private boolean date_toDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Hr_leave_allocationMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [NUMBER_PER_INTERVAL]
     *
     */
    @Hr_leave_allocationNumber_per_intervalDefault(info = "默认规则")
    private Double number_per_interval;

    @JsonIgnore
    private boolean number_per_intervalDirtyFlag;

    /**
     * 属性 [NUMBER_OF_DAYS_DISPLAY]
     *
     */
    @Hr_leave_allocationNumber_of_days_displayDefault(info = "默认规则")
    private Double number_of_days_display;

    @JsonIgnore
    private boolean number_of_days_displayDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Hr_leave_allocationMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_leave_allocation__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [HOLIDAY_TYPE]
     *
     */
    @Hr_leave_allocationHoliday_typeDefault(info = "默认规则")
    private String holiday_type;

    @JsonIgnore
    private boolean holiday_typeDirtyFlag;

    /**
     * 属性 [INTERVAL_NUMBER]
     *
     */
    @Hr_leave_allocationInterval_numberDefault(info = "默认规则")
    private Integer interval_number;

    @JsonIgnore
    private boolean interval_numberDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Hr_leave_allocationMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [NUMBER_OF_HOURS_DISPLAY]
     *
     */
    @Hr_leave_allocationNumber_of_hours_displayDefault(info = "默认规则")
    private Double number_of_hours_display;

    @JsonIgnore
    private boolean number_of_hours_displayDirtyFlag;

    /**
     * 属性 [ACCRUAL]
     *
     */
    @Hr_leave_allocationAccrualDefault(info = "默认规则")
    private String accrual;

    @JsonIgnore
    private boolean accrualDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Hr_leave_allocationMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Hr_leave_allocationActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [LINKED_REQUEST_IDS]
     *
     */
    @Hr_leave_allocationLinked_request_idsDefault(info = "默认规则")
    private String linked_request_ids;

    @JsonIgnore
    private boolean linked_request_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Hr_leave_allocationCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Hr_leave_allocationActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Hr_leave_allocationMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Hr_leave_allocationActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [NEXTCALL]
     *
     */
    @Hr_leave_allocationNextcallDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp nextcall;

    @JsonIgnore
    private boolean nextcallDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Hr_leave_allocationMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Hr_leave_allocationMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Hr_leave_allocationActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [NUMBER_OF_DAYS]
     *
     */
    @Hr_leave_allocationNumber_of_daysDefault(info = "默认规则")
    private Double number_of_days;

    @JsonIgnore
    private boolean number_of_daysDirtyFlag;

    /**
     * 属性 [NOTES]
     *
     */
    @Hr_leave_allocationNotesDefault(info = "默认规则")
    private String notes;

    @JsonIgnore
    private boolean notesDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Hr_leave_allocationActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Hr_leave_allocationMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Hr_leave_allocationMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Hr_leave_allocationStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [INTERVAL_UNIT]
     *
     */
    @Hr_leave_allocationInterval_unitDefault(info = "默认规则")
    private String interval_unit;

    @JsonIgnore
    private boolean interval_unitDirtyFlag;

    /**
     * 属性 [DURATION_DISPLAY]
     *
     */
    @Hr_leave_allocationDuration_displayDefault(info = "默认规则")
    private String duration_display;

    @JsonIgnore
    private boolean duration_displayDirtyFlag;

    /**
     * 属性 [CAN_APPROVE]
     *
     */
    @Hr_leave_allocationCan_approveDefault(info = "默认规则")
    private String can_approve;

    @JsonIgnore
    private boolean can_approveDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Hr_leave_allocationIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CAN_RESET]
     *
     */
    @Hr_leave_allocationCan_resetDefault(info = "默认规则")
    private String can_reset;

    @JsonIgnore
    private boolean can_resetDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Hr_leave_allocationMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Hr_leave_allocationWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [UNIT_PER_INTERVAL]
     *
     */
    @Hr_leave_allocationUnit_per_intervalDefault(info = "默认规则")
    private String unit_per_interval;

    @JsonIgnore
    private boolean unit_per_intervalDirtyFlag;

    /**
     * 属性 [ACCRUAL_LIMIT]
     *
     */
    @Hr_leave_allocationAccrual_limitDefault(info = "默认规则")
    private Integer accrual_limit;

    @JsonIgnore
    private boolean accrual_limitDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Hr_leave_allocationWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Hr_leave_allocationMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_leave_allocationDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [DATE_FROM]
     *
     */
    @Hr_leave_allocationDate_fromDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_from;

    @JsonIgnore
    private boolean date_fromDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Hr_leave_allocationMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Hr_leave_allocationActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @Hr_leave_allocationDepartment_id_textDefault(info = "默认规则")
    private String department_id_text;

    @JsonIgnore
    private boolean department_id_textDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID_TEXT]
     *
     */
    @Hr_leave_allocationEmployee_id_textDefault(info = "默认规则")
    private String employee_id_text;

    @JsonIgnore
    private boolean employee_id_textDirtyFlag;

    /**
     * 属性 [VALIDATION_TYPE]
     *
     */
    @Hr_leave_allocationValidation_typeDefault(info = "默认规则")
    private String validation_type;

    @JsonIgnore
    private boolean validation_typeDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Hr_leave_allocationCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [MODE_COMPANY_ID_TEXT]
     *
     */
    @Hr_leave_allocationMode_company_id_textDefault(info = "默认规则")
    private String mode_company_id_text;

    @JsonIgnore
    private boolean mode_company_id_textDirtyFlag;

    /**
     * 属性 [FIRST_APPROVER_ID_TEXT]
     *
     */
    @Hr_leave_allocationFirst_approver_id_textDefault(info = "默认规则")
    private String first_approver_id_text;

    @JsonIgnore
    private boolean first_approver_id_textDirtyFlag;

    /**
     * 属性 [HOLIDAY_STATUS_ID_TEXT]
     *
     */
    @Hr_leave_allocationHoliday_status_id_textDefault(info = "默认规则")
    private String holiday_status_id_text;

    @JsonIgnore
    private boolean holiday_status_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Hr_leave_allocationWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [TYPE_REQUEST_UNIT]
     *
     */
    @Hr_leave_allocationType_request_unitDefault(info = "默认规则")
    private String type_request_unit;

    @JsonIgnore
    private boolean type_request_unitDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Hr_leave_allocationParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [SECOND_APPROVER_ID_TEXT]
     *
     */
    @Hr_leave_allocationSecond_approver_id_textDefault(info = "默认规则")
    private String second_approver_id_text;

    @JsonIgnore
    private boolean second_approver_id_textDirtyFlag;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @Hr_leave_allocationCategory_id_textDefault(info = "默认规则")
    private String category_id_text;

    @JsonIgnore
    private boolean category_id_textDirtyFlag;

    /**
     * 属性 [HOLIDAY_STATUS_ID]
     *
     */
    @Hr_leave_allocationHoliday_status_idDefault(info = "默认规则")
    private Integer holiday_status_id;

    @JsonIgnore
    private boolean holiday_status_idDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @Hr_leave_allocationDepartment_idDefault(info = "默认规则")
    private Integer department_id;

    @JsonIgnore
    private boolean department_idDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Hr_leave_allocationParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Hr_leave_allocationCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [SECOND_APPROVER_ID]
     *
     */
    @Hr_leave_allocationSecond_approver_idDefault(info = "默认规则")
    private Integer second_approver_id;

    @JsonIgnore
    private boolean second_approver_idDirtyFlag;

    /**
     * 属性 [MODE_COMPANY_ID]
     *
     */
    @Hr_leave_allocationMode_company_idDefault(info = "默认规则")
    private Integer mode_company_id;

    @JsonIgnore
    private boolean mode_company_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Hr_leave_allocationWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [FIRST_APPROVER_ID]
     *
     */
    @Hr_leave_allocationFirst_approver_idDefault(info = "默认规则")
    private Integer first_approver_id;

    @JsonIgnore
    private boolean first_approver_idDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Hr_leave_allocationCategory_idDefault(info = "默认规则")
    private Integer category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @Hr_leave_allocationEmployee_idDefault(info = "默认规则")
    private Integer employee_id;

    @JsonIgnore
    private boolean employee_idDirtyFlag;


    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DATE_TO]
     */
    @JsonProperty("date_to")
    public Timestamp getDate_to(){
        return date_to ;
    }

    /**
     * 设置 [DATE_TO]
     */
    @JsonProperty("date_to")
    public void setDate_to(Timestamp  date_to){
        this.date_to = date_to ;
        this.date_toDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_TO]脏标记
     */
    @JsonIgnore
    public boolean getDate_toDirtyFlag(){
        return date_toDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [NUMBER_PER_INTERVAL]
     */
    @JsonProperty("number_per_interval")
    public Double getNumber_per_interval(){
        return number_per_interval ;
    }

    /**
     * 设置 [NUMBER_PER_INTERVAL]
     */
    @JsonProperty("number_per_interval")
    public void setNumber_per_interval(Double  number_per_interval){
        this.number_per_interval = number_per_interval ;
        this.number_per_intervalDirtyFlag = true ;
    }

    /**
     * 获取 [NUMBER_PER_INTERVAL]脏标记
     */
    @JsonIgnore
    public boolean getNumber_per_intervalDirtyFlag(){
        return number_per_intervalDirtyFlag ;
    }

    /**
     * 获取 [NUMBER_OF_DAYS_DISPLAY]
     */
    @JsonProperty("number_of_days_display")
    public Double getNumber_of_days_display(){
        return number_of_days_display ;
    }

    /**
     * 设置 [NUMBER_OF_DAYS_DISPLAY]
     */
    @JsonProperty("number_of_days_display")
    public void setNumber_of_days_display(Double  number_of_days_display){
        this.number_of_days_display = number_of_days_display ;
        this.number_of_days_displayDirtyFlag = true ;
    }

    /**
     * 获取 [NUMBER_OF_DAYS_DISPLAY]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_days_displayDirtyFlag(){
        return number_of_days_displayDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [HOLIDAY_TYPE]
     */
    @JsonProperty("holiday_type")
    public String getHoliday_type(){
        return holiday_type ;
    }

    /**
     * 设置 [HOLIDAY_TYPE]
     */
    @JsonProperty("holiday_type")
    public void setHoliday_type(String  holiday_type){
        this.holiday_type = holiday_type ;
        this.holiday_typeDirtyFlag = true ;
    }

    /**
     * 获取 [HOLIDAY_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_typeDirtyFlag(){
        return holiday_typeDirtyFlag ;
    }

    /**
     * 获取 [INTERVAL_NUMBER]
     */
    @JsonProperty("interval_number")
    public Integer getInterval_number(){
        return interval_number ;
    }

    /**
     * 设置 [INTERVAL_NUMBER]
     */
    @JsonProperty("interval_number")
    public void setInterval_number(Integer  interval_number){
        this.interval_number = interval_number ;
        this.interval_numberDirtyFlag = true ;
    }

    /**
     * 获取 [INTERVAL_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getInterval_numberDirtyFlag(){
        return interval_numberDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [NUMBER_OF_HOURS_DISPLAY]
     */
    @JsonProperty("number_of_hours_display")
    public Double getNumber_of_hours_display(){
        return number_of_hours_display ;
    }

    /**
     * 设置 [NUMBER_OF_HOURS_DISPLAY]
     */
    @JsonProperty("number_of_hours_display")
    public void setNumber_of_hours_display(Double  number_of_hours_display){
        this.number_of_hours_display = number_of_hours_display ;
        this.number_of_hours_displayDirtyFlag = true ;
    }

    /**
     * 获取 [NUMBER_OF_HOURS_DISPLAY]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_hours_displayDirtyFlag(){
        return number_of_hours_displayDirtyFlag ;
    }

    /**
     * 获取 [ACCRUAL]
     */
    @JsonProperty("accrual")
    public String getAccrual(){
        return accrual ;
    }

    /**
     * 设置 [ACCRUAL]
     */
    @JsonProperty("accrual")
    public void setAccrual(String  accrual){
        this.accrual = accrual ;
        this.accrualDirtyFlag = true ;
    }

    /**
     * 获取 [ACCRUAL]脏标记
     */
    @JsonIgnore
    public boolean getAccrualDirtyFlag(){
        return accrualDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [LINKED_REQUEST_IDS]
     */
    @JsonProperty("linked_request_ids")
    public String getLinked_request_ids(){
        return linked_request_ids ;
    }

    /**
     * 设置 [LINKED_REQUEST_IDS]
     */
    @JsonProperty("linked_request_ids")
    public void setLinked_request_ids(String  linked_request_ids){
        this.linked_request_ids = linked_request_ids ;
        this.linked_request_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LINKED_REQUEST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLinked_request_idsDirtyFlag(){
        return linked_request_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [NEXTCALL]
     */
    @JsonProperty("nextcall")
    public Timestamp getNextcall(){
        return nextcall ;
    }

    /**
     * 设置 [NEXTCALL]
     */
    @JsonProperty("nextcall")
    public void setNextcall(Timestamp  nextcall){
        this.nextcall = nextcall ;
        this.nextcallDirtyFlag = true ;
    }

    /**
     * 获取 [NEXTCALL]脏标记
     */
    @JsonIgnore
    public boolean getNextcallDirtyFlag(){
        return nextcallDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [NUMBER_OF_DAYS]
     */
    @JsonProperty("number_of_days")
    public Double getNumber_of_days(){
        return number_of_days ;
    }

    /**
     * 设置 [NUMBER_OF_DAYS]
     */
    @JsonProperty("number_of_days")
    public void setNumber_of_days(Double  number_of_days){
        this.number_of_days = number_of_days ;
        this.number_of_daysDirtyFlag = true ;
    }

    /**
     * 获取 [NUMBER_OF_DAYS]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_daysDirtyFlag(){
        return number_of_daysDirtyFlag ;
    }

    /**
     * 获取 [NOTES]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return notes ;
    }

    /**
     * 设置 [NOTES]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

    /**
     * 获取 [NOTES]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return notesDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [INTERVAL_UNIT]
     */
    @JsonProperty("interval_unit")
    public String getInterval_unit(){
        return interval_unit ;
    }

    /**
     * 设置 [INTERVAL_UNIT]
     */
    @JsonProperty("interval_unit")
    public void setInterval_unit(String  interval_unit){
        this.interval_unit = interval_unit ;
        this.interval_unitDirtyFlag = true ;
    }

    /**
     * 获取 [INTERVAL_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getInterval_unitDirtyFlag(){
        return interval_unitDirtyFlag ;
    }

    /**
     * 获取 [DURATION_DISPLAY]
     */
    @JsonProperty("duration_display")
    public String getDuration_display(){
        return duration_display ;
    }

    /**
     * 设置 [DURATION_DISPLAY]
     */
    @JsonProperty("duration_display")
    public void setDuration_display(String  duration_display){
        this.duration_display = duration_display ;
        this.duration_displayDirtyFlag = true ;
    }

    /**
     * 获取 [DURATION_DISPLAY]脏标记
     */
    @JsonIgnore
    public boolean getDuration_displayDirtyFlag(){
        return duration_displayDirtyFlag ;
    }

    /**
     * 获取 [CAN_APPROVE]
     */
    @JsonProperty("can_approve")
    public String getCan_approve(){
        return can_approve ;
    }

    /**
     * 设置 [CAN_APPROVE]
     */
    @JsonProperty("can_approve")
    public void setCan_approve(String  can_approve){
        this.can_approve = can_approve ;
        this.can_approveDirtyFlag = true ;
    }

    /**
     * 获取 [CAN_APPROVE]脏标记
     */
    @JsonIgnore
    public boolean getCan_approveDirtyFlag(){
        return can_approveDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CAN_RESET]
     */
    @JsonProperty("can_reset")
    public String getCan_reset(){
        return can_reset ;
    }

    /**
     * 设置 [CAN_RESET]
     */
    @JsonProperty("can_reset")
    public void setCan_reset(String  can_reset){
        this.can_reset = can_reset ;
        this.can_resetDirtyFlag = true ;
    }

    /**
     * 获取 [CAN_RESET]脏标记
     */
    @JsonIgnore
    public boolean getCan_resetDirtyFlag(){
        return can_resetDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [UNIT_PER_INTERVAL]
     */
    @JsonProperty("unit_per_interval")
    public String getUnit_per_interval(){
        return unit_per_interval ;
    }

    /**
     * 设置 [UNIT_PER_INTERVAL]
     */
    @JsonProperty("unit_per_interval")
    public void setUnit_per_interval(String  unit_per_interval){
        this.unit_per_interval = unit_per_interval ;
        this.unit_per_intervalDirtyFlag = true ;
    }

    /**
     * 获取 [UNIT_PER_INTERVAL]脏标记
     */
    @JsonIgnore
    public boolean getUnit_per_intervalDirtyFlag(){
        return unit_per_intervalDirtyFlag ;
    }

    /**
     * 获取 [ACCRUAL_LIMIT]
     */
    @JsonProperty("accrual_limit")
    public Integer getAccrual_limit(){
        return accrual_limit ;
    }

    /**
     * 设置 [ACCRUAL_LIMIT]
     */
    @JsonProperty("accrual_limit")
    public void setAccrual_limit(Integer  accrual_limit){
        this.accrual_limit = accrual_limit ;
        this.accrual_limitDirtyFlag = true ;
    }

    /**
     * 获取 [ACCRUAL_LIMIT]脏标记
     */
    @JsonIgnore
    public boolean getAccrual_limitDirtyFlag(){
        return accrual_limitDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [DATE_FROM]
     */
    @JsonProperty("date_from")
    public Timestamp getDate_from(){
        return date_from ;
    }

    /**
     * 设置 [DATE_FROM]
     */
    @JsonProperty("date_from")
    public void setDate_from(Timestamp  date_from){
        this.date_from = date_from ;
        this.date_fromDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_FROM]脏标记
     */
    @JsonIgnore
    public boolean getDate_fromDirtyFlag(){
        return date_fromDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return department_id_text ;
    }

    /**
     * 设置 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return department_id_textDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return employee_id_text ;
    }

    /**
     * 设置 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return employee_id_textDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_TYPE]
     */
    @JsonProperty("validation_type")
    public String getValidation_type(){
        return validation_type ;
    }

    /**
     * 设置 [VALIDATION_TYPE]
     */
    @JsonProperty("validation_type")
    public void setValidation_type(String  validation_type){
        this.validation_type = validation_type ;
        this.validation_typeDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getValidation_typeDirtyFlag(){
        return validation_typeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MODE_COMPANY_ID_TEXT]
     */
    @JsonProperty("mode_company_id_text")
    public String getMode_company_id_text(){
        return mode_company_id_text ;
    }

    /**
     * 设置 [MODE_COMPANY_ID_TEXT]
     */
    @JsonProperty("mode_company_id_text")
    public void setMode_company_id_text(String  mode_company_id_text){
        this.mode_company_id_text = mode_company_id_text ;
        this.mode_company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MODE_COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMode_company_id_textDirtyFlag(){
        return mode_company_id_textDirtyFlag ;
    }

    /**
     * 获取 [FIRST_APPROVER_ID_TEXT]
     */
    @JsonProperty("first_approver_id_text")
    public String getFirst_approver_id_text(){
        return first_approver_id_text ;
    }

    /**
     * 设置 [FIRST_APPROVER_ID_TEXT]
     */
    @JsonProperty("first_approver_id_text")
    public void setFirst_approver_id_text(String  first_approver_id_text){
        this.first_approver_id_text = first_approver_id_text ;
        this.first_approver_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [FIRST_APPROVER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getFirst_approver_id_textDirtyFlag(){
        return first_approver_id_textDirtyFlag ;
    }

    /**
     * 获取 [HOLIDAY_STATUS_ID_TEXT]
     */
    @JsonProperty("holiday_status_id_text")
    public String getHoliday_status_id_text(){
        return holiday_status_id_text ;
    }

    /**
     * 设置 [HOLIDAY_STATUS_ID_TEXT]
     */
    @JsonProperty("holiday_status_id_text")
    public void setHoliday_status_id_text(String  holiday_status_id_text){
        this.holiday_status_id_text = holiday_status_id_text ;
        this.holiday_status_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [HOLIDAY_STATUS_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_status_id_textDirtyFlag(){
        return holiday_status_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TYPE_REQUEST_UNIT]
     */
    @JsonProperty("type_request_unit")
    public String getType_request_unit(){
        return type_request_unit ;
    }

    /**
     * 设置 [TYPE_REQUEST_UNIT]
     */
    @JsonProperty("type_request_unit")
    public void setType_request_unit(String  type_request_unit){
        this.type_request_unit = type_request_unit ;
        this.type_request_unitDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE_REQUEST_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getType_request_unitDirtyFlag(){
        return type_request_unitDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [SECOND_APPROVER_ID_TEXT]
     */
    @JsonProperty("second_approver_id_text")
    public String getSecond_approver_id_text(){
        return second_approver_id_text ;
    }

    /**
     * 设置 [SECOND_APPROVER_ID_TEXT]
     */
    @JsonProperty("second_approver_id_text")
    public void setSecond_approver_id_text(String  second_approver_id_text){
        this.second_approver_id_text = second_approver_id_text ;
        this.second_approver_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_APPROVER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSecond_approver_id_textDirtyFlag(){
        return second_approver_id_textDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return category_id_text ;
    }

    /**
     * 设置 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return category_id_textDirtyFlag ;
    }

    /**
     * 获取 [HOLIDAY_STATUS_ID]
     */
    @JsonProperty("holiday_status_id")
    public Integer getHoliday_status_id(){
        return holiday_status_id ;
    }

    /**
     * 设置 [HOLIDAY_STATUS_ID]
     */
    @JsonProperty("holiday_status_id")
    public void setHoliday_status_id(Integer  holiday_status_id){
        this.holiday_status_id = holiday_status_id ;
        this.holiday_status_idDirtyFlag = true ;
    }

    /**
     * 获取 [HOLIDAY_STATUS_ID]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_status_idDirtyFlag(){
        return holiday_status_idDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return department_id ;
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return department_idDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [SECOND_APPROVER_ID]
     */
    @JsonProperty("second_approver_id")
    public Integer getSecond_approver_id(){
        return second_approver_id ;
    }

    /**
     * 设置 [SECOND_APPROVER_ID]
     */
    @JsonProperty("second_approver_id")
    public void setSecond_approver_id(Integer  second_approver_id){
        this.second_approver_id = second_approver_id ;
        this.second_approver_idDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_APPROVER_ID]脏标记
     */
    @JsonIgnore
    public boolean getSecond_approver_idDirtyFlag(){
        return second_approver_idDirtyFlag ;
    }

    /**
     * 获取 [MODE_COMPANY_ID]
     */
    @JsonProperty("mode_company_id")
    public Integer getMode_company_id(){
        return mode_company_id ;
    }

    /**
     * 设置 [MODE_COMPANY_ID]
     */
    @JsonProperty("mode_company_id")
    public void setMode_company_id(Integer  mode_company_id){
        this.mode_company_id = mode_company_id ;
        this.mode_company_idDirtyFlag = true ;
    }

    /**
     * 获取 [MODE_COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getMode_company_idDirtyFlag(){
        return mode_company_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [FIRST_APPROVER_ID]
     */
    @JsonProperty("first_approver_id")
    public Integer getFirst_approver_id(){
        return first_approver_id ;
    }

    /**
     * 设置 [FIRST_APPROVER_ID]
     */
    @JsonProperty("first_approver_id")
    public void setFirst_approver_id(Integer  first_approver_id){
        this.first_approver_id = first_approver_id ;
        this.first_approver_idDirtyFlag = true ;
    }

    /**
     * 获取 [FIRST_APPROVER_ID]脏标记
     */
    @JsonIgnore
    public boolean getFirst_approver_idDirtyFlag(){
        return first_approver_idDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return employee_id ;
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return employee_idDirtyFlag ;
    }



    public Hr_leave_allocation toDO() {
        Hr_leave_allocation srfdomain = new Hr_leave_allocation();
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDate_toDirtyFlag())
            srfdomain.setDate_to(date_to);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getNumber_per_intervalDirtyFlag())
            srfdomain.setNumber_per_interval(number_per_interval);
        if(getNumber_of_days_displayDirtyFlag())
            srfdomain.setNumber_of_days_display(number_of_days_display);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getHoliday_typeDirtyFlag())
            srfdomain.setHoliday_type(holiday_type);
        if(getInterval_numberDirtyFlag())
            srfdomain.setInterval_number(interval_number);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getNumber_of_hours_displayDirtyFlag())
            srfdomain.setNumber_of_hours_display(number_of_hours_display);
        if(getAccrualDirtyFlag())
            srfdomain.setAccrual(accrual);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getLinked_request_idsDirtyFlag())
            srfdomain.setLinked_request_ids(linked_request_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getNextcallDirtyFlag())
            srfdomain.setNextcall(nextcall);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getNumber_of_daysDirtyFlag())
            srfdomain.setNumber_of_days(number_of_days);
        if(getNotesDirtyFlag())
            srfdomain.setNotes(notes);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getInterval_unitDirtyFlag())
            srfdomain.setInterval_unit(interval_unit);
        if(getDuration_displayDirtyFlag())
            srfdomain.setDuration_display(duration_display);
        if(getCan_approveDirtyFlag())
            srfdomain.setCan_approve(can_approve);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCan_resetDirtyFlag())
            srfdomain.setCan_reset(can_reset);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getUnit_per_intervalDirtyFlag())
            srfdomain.setUnit_per_interval(unit_per_interval);
        if(getAccrual_limitDirtyFlag())
            srfdomain.setAccrual_limit(accrual_limit);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getDate_fromDirtyFlag())
            srfdomain.setDate_from(date_from);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getDepartment_id_textDirtyFlag())
            srfdomain.setDepartment_id_text(department_id_text);
        if(getEmployee_id_textDirtyFlag())
            srfdomain.setEmployee_id_text(employee_id_text);
        if(getValidation_typeDirtyFlag())
            srfdomain.setValidation_type(validation_type);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getMode_company_id_textDirtyFlag())
            srfdomain.setMode_company_id_text(mode_company_id_text);
        if(getFirst_approver_id_textDirtyFlag())
            srfdomain.setFirst_approver_id_text(first_approver_id_text);
        if(getHoliday_status_id_textDirtyFlag())
            srfdomain.setHoliday_status_id_text(holiday_status_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getType_request_unitDirtyFlag())
            srfdomain.setType_request_unit(type_request_unit);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getSecond_approver_id_textDirtyFlag())
            srfdomain.setSecond_approver_id_text(second_approver_id_text);
        if(getCategory_id_textDirtyFlag())
            srfdomain.setCategory_id_text(category_id_text);
        if(getHoliday_status_idDirtyFlag())
            srfdomain.setHoliday_status_id(holiday_status_id);
        if(getDepartment_idDirtyFlag())
            srfdomain.setDepartment_id(department_id);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getSecond_approver_idDirtyFlag())
            srfdomain.setSecond_approver_id(second_approver_id);
        if(getMode_company_idDirtyFlag())
            srfdomain.setMode_company_id(mode_company_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getFirst_approver_idDirtyFlag())
            srfdomain.setFirst_approver_id(first_approver_id);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getEmployee_idDirtyFlag())
            srfdomain.setEmployee_id(employee_id);

        return srfdomain;
    }

    public void fromDO(Hr_leave_allocation srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDate_toDirtyFlag())
            this.setDate_to(srfdomain.getDate_to());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getNumber_per_intervalDirtyFlag())
            this.setNumber_per_interval(srfdomain.getNumber_per_interval());
        if(srfdomain.getNumber_of_days_displayDirtyFlag())
            this.setNumber_of_days_display(srfdomain.getNumber_of_days_display());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getHoliday_typeDirtyFlag())
            this.setHoliday_type(srfdomain.getHoliday_type());
        if(srfdomain.getInterval_numberDirtyFlag())
            this.setInterval_number(srfdomain.getInterval_number());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getNumber_of_hours_displayDirtyFlag())
            this.setNumber_of_hours_display(srfdomain.getNumber_of_hours_display());
        if(srfdomain.getAccrualDirtyFlag())
            this.setAccrual(srfdomain.getAccrual());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getLinked_request_idsDirtyFlag())
            this.setLinked_request_ids(srfdomain.getLinked_request_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getNextcallDirtyFlag())
            this.setNextcall(srfdomain.getNextcall());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getNumber_of_daysDirtyFlag())
            this.setNumber_of_days(srfdomain.getNumber_of_days());
        if(srfdomain.getNotesDirtyFlag())
            this.setNotes(srfdomain.getNotes());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getInterval_unitDirtyFlag())
            this.setInterval_unit(srfdomain.getInterval_unit());
        if(srfdomain.getDuration_displayDirtyFlag())
            this.setDuration_display(srfdomain.getDuration_display());
        if(srfdomain.getCan_approveDirtyFlag())
            this.setCan_approve(srfdomain.getCan_approve());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCan_resetDirtyFlag())
            this.setCan_reset(srfdomain.getCan_reset());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getUnit_per_intervalDirtyFlag())
            this.setUnit_per_interval(srfdomain.getUnit_per_interval());
        if(srfdomain.getAccrual_limitDirtyFlag())
            this.setAccrual_limit(srfdomain.getAccrual_limit());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getDate_fromDirtyFlag())
            this.setDate_from(srfdomain.getDate_from());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getDepartment_id_textDirtyFlag())
            this.setDepartment_id_text(srfdomain.getDepartment_id_text());
        if(srfdomain.getEmployee_id_textDirtyFlag())
            this.setEmployee_id_text(srfdomain.getEmployee_id_text());
        if(srfdomain.getValidation_typeDirtyFlag())
            this.setValidation_type(srfdomain.getValidation_type());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getMode_company_id_textDirtyFlag())
            this.setMode_company_id_text(srfdomain.getMode_company_id_text());
        if(srfdomain.getFirst_approver_id_textDirtyFlag())
            this.setFirst_approver_id_text(srfdomain.getFirst_approver_id_text());
        if(srfdomain.getHoliday_status_id_textDirtyFlag())
            this.setHoliday_status_id_text(srfdomain.getHoliday_status_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getType_request_unitDirtyFlag())
            this.setType_request_unit(srfdomain.getType_request_unit());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getSecond_approver_id_textDirtyFlag())
            this.setSecond_approver_id_text(srfdomain.getSecond_approver_id_text());
        if(srfdomain.getCategory_id_textDirtyFlag())
            this.setCategory_id_text(srfdomain.getCategory_id_text());
        if(srfdomain.getHoliday_status_idDirtyFlag())
            this.setHoliday_status_id(srfdomain.getHoliday_status_id());
        if(srfdomain.getDepartment_idDirtyFlag())
            this.setDepartment_id(srfdomain.getDepartment_id());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getSecond_approver_idDirtyFlag())
            this.setSecond_approver_id(srfdomain.getSecond_approver_id());
        if(srfdomain.getMode_company_idDirtyFlag())
            this.setMode_company_id(srfdomain.getMode_company_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getFirst_approver_idDirtyFlag())
            this.setFirst_approver_id(srfdomain.getFirst_approver_id());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getEmployee_idDirtyFlag())
            this.setEmployee_id(srfdomain.getEmployee_id());

    }

    public List<Hr_leave_allocationDTO> fromDOPage(List<Hr_leave_allocation> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_leave_allocationDTO> dtos=new ArrayList<Hr_leave_allocationDTO>();
        for(Hr_leave_allocation domain : poPage) {
            Hr_leave_allocationDTO dto = new Hr_leave_allocationDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

