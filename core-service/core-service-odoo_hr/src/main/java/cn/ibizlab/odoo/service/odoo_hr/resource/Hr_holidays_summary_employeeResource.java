package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_holidays_summary_employeeDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_employee;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_holidays_summary_employeeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_employeeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_holidays_summary_employee" })
@RestController
@RequestMapping("")
public class Hr_holidays_summary_employeeResource {

    @Autowired
    private IHr_holidays_summary_employeeService hr_holidays_summary_employeeService;

    public IHr_holidays_summary_employeeService getHr_holidays_summary_employeeService() {
        return this.hr_holidays_summary_employeeService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_holidays_summary_employee" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_holidays_summary_employees/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_holidays_summary_employeeDTO> hr_holidays_summary_employeedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_holidays_summary_employee" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_holidays_summary_employees/{hr_holidays_summary_employee_id}")

    public ResponseEntity<Hr_holidays_summary_employeeDTO> update(@PathVariable("hr_holidays_summary_employee_id") Integer hr_holidays_summary_employee_id, @RequestBody Hr_holidays_summary_employeeDTO hr_holidays_summary_employeedto) {
		Hr_holidays_summary_employee domain = hr_holidays_summary_employeedto.toDO();
        domain.setId(hr_holidays_summary_employee_id);
		hr_holidays_summary_employeeService.update(domain);
		Hr_holidays_summary_employeeDTO dto = new Hr_holidays_summary_employeeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_holidays_summary_employee" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_holidays_summary_employees/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_holidays_summary_employeeDTO> hr_holidays_summary_employeedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_holidays_summary_employee" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_holidays_summary_employees/createBatch")
    public ResponseEntity<Boolean> createBatchHr_holidays_summary_employee(@RequestBody List<Hr_holidays_summary_employeeDTO> hr_holidays_summary_employeedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_holidays_summary_employee" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_holidays_summary_employees/{hr_holidays_summary_employee_id}")
    public ResponseEntity<Hr_holidays_summary_employeeDTO> get(@PathVariable("hr_holidays_summary_employee_id") Integer hr_holidays_summary_employee_id) {
        Hr_holidays_summary_employeeDTO dto = new Hr_holidays_summary_employeeDTO();
        Hr_holidays_summary_employee domain = hr_holidays_summary_employeeService.get(hr_holidays_summary_employee_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_holidays_summary_employee" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_holidays_summary_employees")

    public ResponseEntity<Hr_holidays_summary_employeeDTO> create(@RequestBody Hr_holidays_summary_employeeDTO hr_holidays_summary_employeedto) {
        Hr_holidays_summary_employeeDTO dto = new Hr_holidays_summary_employeeDTO();
        Hr_holidays_summary_employee domain = hr_holidays_summary_employeedto.toDO();
		hr_holidays_summary_employeeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_holidays_summary_employee" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_holidays_summary_employees/{hr_holidays_summary_employee_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_holidays_summary_employee_id") Integer hr_holidays_summary_employee_id) {
        Hr_holidays_summary_employeeDTO hr_holidays_summary_employeedto = new Hr_holidays_summary_employeeDTO();
		Hr_holidays_summary_employee domain = new Hr_holidays_summary_employee();
		hr_holidays_summary_employeedto.setId(hr_holidays_summary_employee_id);
		domain.setId(hr_holidays_summary_employee_id);
        Boolean rst = hr_holidays_summary_employeeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_holidays_summary_employee" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_holidays_summary_employees/fetchdefault")
	public ResponseEntity<Page<Hr_holidays_summary_employeeDTO>> fetchDefault(Hr_holidays_summary_employeeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_holidays_summary_employeeDTO> list = new ArrayList<Hr_holidays_summary_employeeDTO>();
        
        Page<Hr_holidays_summary_employee> domains = hr_holidays_summary_employeeService.searchDefault(context) ;
        for(Hr_holidays_summary_employee hr_holidays_summary_employee : domains.getContent()){
            Hr_holidays_summary_employeeDTO dto = new Hr_holidays_summary_employeeDTO();
            dto.fromDO(hr_holidays_summary_employee);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
