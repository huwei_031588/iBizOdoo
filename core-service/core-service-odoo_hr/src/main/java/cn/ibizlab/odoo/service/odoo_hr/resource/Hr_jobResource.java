package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_jobDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_jobService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_jobSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_job" })
@RestController
@RequestMapping("")
public class Hr_jobResource {

    @Autowired
    private IHr_jobService hr_jobService;

    public IHr_jobService getHr_jobService() {
        return this.hr_jobService;
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_job" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_jobs/{hr_job_id}")

    public ResponseEntity<Hr_jobDTO> update(@PathVariable("hr_job_id") Integer hr_job_id, @RequestBody Hr_jobDTO hr_jobdto) {
		Hr_job domain = hr_jobdto.toDO();
        domain.setId(hr_job_id);
		hr_jobService.update(domain);
		Hr_jobDTO dto = new Hr_jobDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_job" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_jobs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_jobDTO> hr_jobdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_job" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_jobs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_jobDTO> hr_jobdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_job" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_jobs")

    public ResponseEntity<Hr_jobDTO> create(@RequestBody Hr_jobDTO hr_jobdto) {
        Hr_jobDTO dto = new Hr_jobDTO();
        Hr_job domain = hr_jobdto.toDO();
		hr_jobService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_job" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_jobs/{hr_job_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_job_id") Integer hr_job_id) {
        Hr_jobDTO hr_jobdto = new Hr_jobDTO();
		Hr_job domain = new Hr_job();
		hr_jobdto.setId(hr_job_id);
		domain.setId(hr_job_id);
        Boolean rst = hr_jobService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_job" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_jobs/{hr_job_id}")
    public ResponseEntity<Hr_jobDTO> get(@PathVariable("hr_job_id") Integer hr_job_id) {
        Hr_jobDTO dto = new Hr_jobDTO();
        Hr_job domain = hr_jobService.get(hr_job_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_job" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_jobs/createBatch")
    public ResponseEntity<Boolean> createBatchHr_job(@RequestBody List<Hr_jobDTO> hr_jobdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_job" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_jobs/fetchdefault")
	public ResponseEntity<Page<Hr_jobDTO>> fetchDefault(Hr_jobSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_jobDTO> list = new ArrayList<Hr_jobDTO>();
        
        Page<Hr_job> domains = hr_jobService.searchDefault(context) ;
        for(Hr_job hr_job : domains.getContent()){
            Hr_jobDTO dto = new Hr_jobDTO();
            dto.fromDO(hr_job);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
