package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_holidays_summary_deptDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_dept;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_holidays_summary_deptService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_deptSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_holidays_summary_dept" })
@RestController
@RequestMapping("")
public class Hr_holidays_summary_deptResource {

    @Autowired
    private IHr_holidays_summary_deptService hr_holidays_summary_deptService;

    public IHr_holidays_summary_deptService getHr_holidays_summary_deptService() {
        return this.hr_holidays_summary_deptService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_holidays_summary_dept" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_holidays_summary_depts/createBatch")
    public ResponseEntity<Boolean> createBatchHr_holidays_summary_dept(@RequestBody List<Hr_holidays_summary_deptDTO> hr_holidays_summary_deptdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_holidays_summary_dept" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_holidays_summary_depts/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_holidays_summary_deptDTO> hr_holidays_summary_deptdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_holidays_summary_dept" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_holidays_summary_depts")

    public ResponseEntity<Hr_holidays_summary_deptDTO> create(@RequestBody Hr_holidays_summary_deptDTO hr_holidays_summary_deptdto) {
        Hr_holidays_summary_deptDTO dto = new Hr_holidays_summary_deptDTO();
        Hr_holidays_summary_dept domain = hr_holidays_summary_deptdto.toDO();
		hr_holidays_summary_deptService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_holidays_summary_dept" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_holidays_summary_depts/{hr_holidays_summary_dept_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_holidays_summary_dept_id") Integer hr_holidays_summary_dept_id) {
        Hr_holidays_summary_deptDTO hr_holidays_summary_deptdto = new Hr_holidays_summary_deptDTO();
		Hr_holidays_summary_dept domain = new Hr_holidays_summary_dept();
		hr_holidays_summary_deptdto.setId(hr_holidays_summary_dept_id);
		domain.setId(hr_holidays_summary_dept_id);
        Boolean rst = hr_holidays_summary_deptService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_holidays_summary_dept" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_holidays_summary_depts/{hr_holidays_summary_dept_id}")

    public ResponseEntity<Hr_holidays_summary_deptDTO> update(@PathVariable("hr_holidays_summary_dept_id") Integer hr_holidays_summary_dept_id, @RequestBody Hr_holidays_summary_deptDTO hr_holidays_summary_deptdto) {
		Hr_holidays_summary_dept domain = hr_holidays_summary_deptdto.toDO();
        domain.setId(hr_holidays_summary_dept_id);
		hr_holidays_summary_deptService.update(domain);
		Hr_holidays_summary_deptDTO dto = new Hr_holidays_summary_deptDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_holidays_summary_dept" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_holidays_summary_depts/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_holidays_summary_deptDTO> hr_holidays_summary_deptdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_holidays_summary_dept" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_holidays_summary_depts/{hr_holidays_summary_dept_id}")
    public ResponseEntity<Hr_holidays_summary_deptDTO> get(@PathVariable("hr_holidays_summary_dept_id") Integer hr_holidays_summary_dept_id) {
        Hr_holidays_summary_deptDTO dto = new Hr_holidays_summary_deptDTO();
        Hr_holidays_summary_dept domain = hr_holidays_summary_deptService.get(hr_holidays_summary_dept_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_holidays_summary_dept" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_holidays_summary_depts/fetchdefault")
	public ResponseEntity<Page<Hr_holidays_summary_deptDTO>> fetchDefault(Hr_holidays_summary_deptSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_holidays_summary_deptDTO> list = new ArrayList<Hr_holidays_summary_deptDTO>();
        
        Page<Hr_holidays_summary_dept> domains = hr_holidays_summary_deptService.searchDefault(context) ;
        for(Hr_holidays_summary_dept hr_holidays_summary_dept : domains.getContent()){
            Hr_holidays_summary_deptDTO dto = new Hr_holidays_summary_deptDTO();
            dto.fromDO(hr_holidays_summary_dept);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
