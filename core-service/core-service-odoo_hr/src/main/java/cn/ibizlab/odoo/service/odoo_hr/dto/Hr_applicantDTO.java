package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_applicant.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_applicantDTO]
 */
public class Hr_applicantDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Hr_applicantPriorityDefault(info = "默认规则")
    private String priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [SALARY_EXPECTED]
     *
     */
    @Hr_applicantSalary_expectedDefault(info = "默认规则")
    private Double salary_expected;

    @JsonIgnore
    private boolean salary_expectedDirtyFlag;

    /**
     * 属性 [CATEG_IDS]
     *
     */
    @Hr_applicantCateg_idsDefault(info = "默认规则")
    private String categ_ids;

    @JsonIgnore
    private boolean categ_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Hr_applicantMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [SALARY_PROPOSED]
     *
     */
    @Hr_applicantSalary_proposedDefault(info = "默认规则")
    private Double salary_proposed;

    @JsonIgnore
    private boolean salary_proposedDirtyFlag;

    /**
     * 属性 [ATTACHMENT_NUMBER]
     *
     */
    @Hr_applicantAttachment_numberDefault(info = "默认规则")
    private Integer attachment_number;

    @JsonIgnore
    private boolean attachment_numberDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_applicantDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Hr_applicantMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Hr_applicantDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [PARTNER_PHONE]
     *
     */
    @Hr_applicantPartner_phoneDefault(info = "默认规则")
    private String partner_phone;

    @JsonIgnore
    private boolean partner_phoneDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Hr_applicantMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [KANBAN_STATE]
     *
     */
    @Hr_applicantKanban_stateDefault(info = "默认规则")
    private String kanban_state;

    @JsonIgnore
    private boolean kanban_stateDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Hr_applicantMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Hr_applicantActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [DATE_OPEN]
     *
     */
    @Hr_applicantDate_openDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_open;

    @JsonIgnore
    private boolean date_openDirtyFlag;

    /**
     * 属性 [DAY_OPEN]
     *
     */
    @Hr_applicantDay_openDefault(info = "默认规则")
    private Double day_open;

    @JsonIgnore
    private boolean day_openDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Hr_applicantMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Hr_applicantActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [AVAILABILITY]
     *
     */
    @Hr_applicantAvailabilityDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp availability;

    @JsonIgnore
    private boolean availabilityDirtyFlag;

    /**
     * 属性 [SALARY_EXPECTED_EXTRA]
     *
     */
    @Hr_applicantSalary_expected_extraDefault(info = "默认规则")
    private String salary_expected_extra;

    @JsonIgnore
    private boolean salary_expected_extraDirtyFlag;

    /**
     * 属性 [DATE_CLOSED]
     *
     */
    @Hr_applicantDate_closedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_closed;

    @JsonIgnore
    private boolean date_closedDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Hr_applicantMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Hr_applicantMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [PARTNER_MOBILE]
     *
     */
    @Hr_applicantPartner_mobileDefault(info = "默认规则")
    private String partner_mobile;

    @JsonIgnore
    private boolean partner_mobileDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Hr_applicantMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [SALARY_PROPOSED_EXTRA]
     *
     */
    @Hr_applicantSalary_proposed_extraDefault(info = "默认规则")
    private String salary_proposed_extra;

    @JsonIgnore
    private boolean salary_proposed_extraDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Hr_applicantMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Hr_applicantMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @Hr_applicantEmail_fromDefault(info = "默认规则")
    private String email_from;

    @JsonIgnore
    private boolean email_fromDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Hr_applicantWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Hr_applicantNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_applicant__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Hr_applicantMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [DATE_LAST_STAGE_UPDATE]
     *
     */
    @Hr_applicantDate_last_stage_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_last_stage_update;

    @JsonIgnore
    private boolean date_last_stage_updateDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Hr_applicantActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Hr_applicantMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Hr_applicantMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Hr_applicantActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Hr_applicantActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [REFERENCE]
     *
     */
    @Hr_applicantReferenceDefault(info = "默认规则")
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @Hr_applicantAttachment_idsDefault(info = "默认规则")
    private String attachment_ids;

    @JsonIgnore
    private boolean attachment_idsDirtyFlag;

    /**
     * 属性 [EMAIL_CC]
     *
     */
    @Hr_applicantEmail_ccDefault(info = "默认规则")
    private String email_cc;

    @JsonIgnore
    private boolean email_ccDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Hr_applicantWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Hr_applicantCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [PARTNER_NAME]
     *
     */
    @Hr_applicantPartner_nameDefault(info = "默认规则")
    private String partner_name;

    @JsonIgnore
    private boolean partner_nameDirtyFlag;

    /**
     * 属性 [DAY_CLOSE]
     *
     */
    @Hr_applicantDay_closeDefault(info = "默认规则")
    private Double day_close;

    @JsonIgnore
    private boolean day_closeDirtyFlag;

    /**
     * 属性 [PROBABILITY]
     *
     */
    @Hr_applicantProbabilityDefault(info = "默认规则")
    private Double probability;

    @JsonIgnore
    private boolean probabilityDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Hr_applicantColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Hr_applicantActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [DELAY_CLOSE]
     *
     */
    @Hr_applicantDelay_closeDefault(info = "默认规则")
    private Double delay_close;

    @JsonIgnore
    private boolean delay_closeDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Hr_applicantIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Hr_applicantActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @Hr_applicantSource_id_textDefault(info = "默认规则")
    private String source_id_text;

    @JsonIgnore
    private boolean source_id_textDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @Hr_applicantCampaign_id_textDefault(info = "默认规则")
    private String campaign_id_text;

    @JsonIgnore
    private boolean campaign_id_textDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @Hr_applicantDepartment_id_textDefault(info = "默认规则")
    private String department_id_text;

    @JsonIgnore
    private boolean department_id_textDirtyFlag;

    /**
     * 属性 [LAST_STAGE_ID_TEXT]
     *
     */
    @Hr_applicantLast_stage_id_textDefault(info = "默认规则")
    private String last_stage_id_text;

    @JsonIgnore
    private boolean last_stage_id_textDirtyFlag;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @Hr_applicantStage_id_textDefault(info = "默认规则")
    private String stage_id_text;

    @JsonIgnore
    private boolean stage_id_textDirtyFlag;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @Hr_applicantMedium_id_textDefault(info = "默认规则")
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;

    /**
     * 属性 [LEGEND_NORMAL]
     *
     */
    @Hr_applicantLegend_normalDefault(info = "默认规则")
    private String legend_normal;

    @JsonIgnore
    private boolean legend_normalDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Hr_applicantUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Hr_applicantCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [TYPE_ID_TEXT]
     *
     */
    @Hr_applicantType_id_textDefault(info = "默认规则")
    private String type_id_text;

    @JsonIgnore
    private boolean type_id_textDirtyFlag;

    /**
     * 属性 [JOB_ID_TEXT]
     *
     */
    @Hr_applicantJob_id_textDefault(info = "默认规则")
    private String job_id_text;

    @JsonIgnore
    private boolean job_id_textDirtyFlag;

    /**
     * 属性 [LEGEND_DONE]
     *
     */
    @Hr_applicantLegend_doneDefault(info = "默认规则")
    private String legend_done;

    @JsonIgnore
    private boolean legend_doneDirtyFlag;

    /**
     * 属性 [LEGEND_BLOCKED]
     *
     */
    @Hr_applicantLegend_blockedDefault(info = "默认规则")
    private String legend_blocked;

    @JsonIgnore
    private boolean legend_blockedDirtyFlag;

    /**
     * 属性 [EMPLOYEE_NAME]
     *
     */
    @Hr_applicantEmployee_nameDefault(info = "默认规则")
    private String employee_name;

    @JsonIgnore
    private boolean employee_nameDirtyFlag;

    /**
     * 属性 [USER_EMAIL]
     *
     */
    @Hr_applicantUser_emailDefault(info = "默认规则")
    private String user_email;

    @JsonIgnore
    private boolean user_emailDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Hr_applicantWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Hr_applicantPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Hr_applicantCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [JOB_ID]
     *
     */
    @Hr_applicantJob_idDefault(info = "默认规则")
    private Integer job_id;

    @JsonIgnore
    private boolean job_idDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @Hr_applicantCampaign_idDefault(info = "默认规则")
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;

    /**
     * 属性 [LAST_STAGE_ID]
     *
     */
    @Hr_applicantLast_stage_idDefault(info = "默认规则")
    private Integer last_stage_id;

    @JsonIgnore
    private boolean last_stage_idDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @Hr_applicantDepartment_idDefault(info = "默认规则")
    private Integer department_id;

    @JsonIgnore
    private boolean department_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Hr_applicantPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Hr_applicantCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @Hr_applicantMedium_idDefault(info = "默认规则")
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;

    /**
     * 属性 [EMP_ID]
     *
     */
    @Hr_applicantEmp_idDefault(info = "默认规则")
    private Integer emp_id;

    @JsonIgnore
    private boolean emp_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Hr_applicantWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [TYPE_ID]
     *
     */
    @Hr_applicantType_idDefault(info = "默认规则")
    private Integer type_id;

    @JsonIgnore
    private boolean type_idDirtyFlag;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @Hr_applicantSource_idDefault(info = "默认规则")
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @Hr_applicantStage_idDefault(info = "默认规则")
    private Integer stage_id;

    @JsonIgnore
    private boolean stage_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Hr_applicantCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Hr_applicantUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;


    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [SALARY_EXPECTED]
     */
    @JsonProperty("salary_expected")
    public Double getSalary_expected(){
        return salary_expected ;
    }

    /**
     * 设置 [SALARY_EXPECTED]
     */
    @JsonProperty("salary_expected")
    public void setSalary_expected(Double  salary_expected){
        this.salary_expected = salary_expected ;
        this.salary_expectedDirtyFlag = true ;
    }

    /**
     * 获取 [SALARY_EXPECTED]脏标记
     */
    @JsonIgnore
    public boolean getSalary_expectedDirtyFlag(){
        return salary_expectedDirtyFlag ;
    }

    /**
     * 获取 [CATEG_IDS]
     */
    @JsonProperty("categ_ids")
    public String getCateg_ids(){
        return categ_ids ;
    }

    /**
     * 设置 [CATEG_IDS]
     */
    @JsonProperty("categ_ids")
    public void setCateg_ids(String  categ_ids){
        this.categ_ids = categ_ids ;
        this.categ_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idsDirtyFlag(){
        return categ_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [SALARY_PROPOSED]
     */
    @JsonProperty("salary_proposed")
    public Double getSalary_proposed(){
        return salary_proposed ;
    }

    /**
     * 设置 [SALARY_PROPOSED]
     */
    @JsonProperty("salary_proposed")
    public void setSalary_proposed(Double  salary_proposed){
        this.salary_proposed = salary_proposed ;
        this.salary_proposedDirtyFlag = true ;
    }

    /**
     * 获取 [SALARY_PROPOSED]脏标记
     */
    @JsonIgnore
    public boolean getSalary_proposedDirtyFlag(){
        return salary_proposedDirtyFlag ;
    }

    /**
     * 获取 [ATTACHMENT_NUMBER]
     */
    @JsonProperty("attachment_number")
    public Integer getAttachment_number(){
        return attachment_number ;
    }

    /**
     * 设置 [ATTACHMENT_NUMBER]
     */
    @JsonProperty("attachment_number")
    public void setAttachment_number(Integer  attachment_number){
        this.attachment_number = attachment_number ;
        this.attachment_numberDirtyFlag = true ;
    }

    /**
     * 获取 [ATTACHMENT_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_numberDirtyFlag(){
        return attachment_numberDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_PHONE]
     */
    @JsonProperty("partner_phone")
    public String getPartner_phone(){
        return partner_phone ;
    }

    /**
     * 设置 [PARTNER_PHONE]
     */
    @JsonProperty("partner_phone")
    public void setPartner_phone(String  partner_phone){
        this.partner_phone = partner_phone ;
        this.partner_phoneDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_PHONE]脏标记
     */
    @JsonIgnore
    public boolean getPartner_phoneDirtyFlag(){
        return partner_phoneDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [KANBAN_STATE]
     */
    @JsonProperty("kanban_state")
    public String getKanban_state(){
        return kanban_state ;
    }

    /**
     * 设置 [KANBAN_STATE]
     */
    @JsonProperty("kanban_state")
    public void setKanban_state(String  kanban_state){
        this.kanban_state = kanban_state ;
        this.kanban_stateDirtyFlag = true ;
    }

    /**
     * 获取 [KANBAN_STATE]脏标记
     */
    @JsonIgnore
    public boolean getKanban_stateDirtyFlag(){
        return kanban_stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [DATE_OPEN]
     */
    @JsonProperty("date_open")
    public Timestamp getDate_open(){
        return date_open ;
    }

    /**
     * 设置 [DATE_OPEN]
     */
    @JsonProperty("date_open")
    public void setDate_open(Timestamp  date_open){
        this.date_open = date_open ;
        this.date_openDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_OPEN]脏标记
     */
    @JsonIgnore
    public boolean getDate_openDirtyFlag(){
        return date_openDirtyFlag ;
    }

    /**
     * 获取 [DAY_OPEN]
     */
    @JsonProperty("day_open")
    public Double getDay_open(){
        return day_open ;
    }

    /**
     * 设置 [DAY_OPEN]
     */
    @JsonProperty("day_open")
    public void setDay_open(Double  day_open){
        this.day_open = day_open ;
        this.day_openDirtyFlag = true ;
    }

    /**
     * 获取 [DAY_OPEN]脏标记
     */
    @JsonIgnore
    public boolean getDay_openDirtyFlag(){
        return day_openDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [AVAILABILITY]
     */
    @JsonProperty("availability")
    public Timestamp getAvailability(){
        return availability ;
    }

    /**
     * 设置 [AVAILABILITY]
     */
    @JsonProperty("availability")
    public void setAvailability(Timestamp  availability){
        this.availability = availability ;
        this.availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getAvailabilityDirtyFlag(){
        return availabilityDirtyFlag ;
    }

    /**
     * 获取 [SALARY_EXPECTED_EXTRA]
     */
    @JsonProperty("salary_expected_extra")
    public String getSalary_expected_extra(){
        return salary_expected_extra ;
    }

    /**
     * 设置 [SALARY_EXPECTED_EXTRA]
     */
    @JsonProperty("salary_expected_extra")
    public void setSalary_expected_extra(String  salary_expected_extra){
        this.salary_expected_extra = salary_expected_extra ;
        this.salary_expected_extraDirtyFlag = true ;
    }

    /**
     * 获取 [SALARY_EXPECTED_EXTRA]脏标记
     */
    @JsonIgnore
    public boolean getSalary_expected_extraDirtyFlag(){
        return salary_expected_extraDirtyFlag ;
    }

    /**
     * 获取 [DATE_CLOSED]
     */
    @JsonProperty("date_closed")
    public Timestamp getDate_closed(){
        return date_closed ;
    }

    /**
     * 设置 [DATE_CLOSED]
     */
    @JsonProperty("date_closed")
    public void setDate_closed(Timestamp  date_closed){
        this.date_closed = date_closed ;
        this.date_closedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_CLOSED]脏标记
     */
    @JsonIgnore
    public boolean getDate_closedDirtyFlag(){
        return date_closedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_MOBILE]
     */
    @JsonProperty("partner_mobile")
    public String getPartner_mobile(){
        return partner_mobile ;
    }

    /**
     * 设置 [PARTNER_MOBILE]
     */
    @JsonProperty("partner_mobile")
    public void setPartner_mobile(String  partner_mobile){
        this.partner_mobile = partner_mobile ;
        this.partner_mobileDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_MOBILE]脏标记
     */
    @JsonIgnore
    public boolean getPartner_mobileDirtyFlag(){
        return partner_mobileDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [SALARY_PROPOSED_EXTRA]
     */
    @JsonProperty("salary_proposed_extra")
    public String getSalary_proposed_extra(){
        return salary_proposed_extra ;
    }

    /**
     * 设置 [SALARY_PROPOSED_EXTRA]
     */
    @JsonProperty("salary_proposed_extra")
    public void setSalary_proposed_extra(String  salary_proposed_extra){
        this.salary_proposed_extra = salary_proposed_extra ;
        this.salary_proposed_extraDirtyFlag = true ;
    }

    /**
     * 获取 [SALARY_PROPOSED_EXTRA]脏标记
     */
    @JsonIgnore
    public boolean getSalary_proposed_extraDirtyFlag(){
        return salary_proposed_extraDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return email_from ;
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_FROM]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return email_fromDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [DATE_LAST_STAGE_UPDATE]
     */
    @JsonProperty("date_last_stage_update")
    public Timestamp getDate_last_stage_update(){
        return date_last_stage_update ;
    }

    /**
     * 设置 [DATE_LAST_STAGE_UPDATE]
     */
    @JsonProperty("date_last_stage_update")
    public void setDate_last_stage_update(Timestamp  date_last_stage_update){
        this.date_last_stage_update = date_last_stage_update ;
        this.date_last_stage_updateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_LAST_STAGE_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean getDate_last_stage_updateDirtyFlag(){
        return date_last_stage_updateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [REFERENCE]
     */
    @JsonProperty("reference")
    public String getReference(){
        return reference ;
    }

    /**
     * 设置 [REFERENCE]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return referenceDirtyFlag ;
    }

    /**
     * 获取 [ATTACHMENT_IDS]
     */
    @JsonProperty("attachment_ids")
    public String getAttachment_ids(){
        return attachment_ids ;
    }

    /**
     * 设置 [ATTACHMENT_IDS]
     */
    @JsonProperty("attachment_ids")
    public void setAttachment_ids(String  attachment_ids){
        this.attachment_ids = attachment_ids ;
        this.attachment_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTACHMENT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_idsDirtyFlag(){
        return attachment_idsDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_CC]
     */
    @JsonProperty("email_cc")
    public String getEmail_cc(){
        return email_cc ;
    }

    /**
     * 设置 [EMAIL_CC]
     */
    @JsonProperty("email_cc")
    public void setEmail_cc(String  email_cc){
        this.email_cc = email_cc ;
        this.email_ccDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_CC]脏标记
     */
    @JsonIgnore
    public boolean getEmail_ccDirtyFlag(){
        return email_ccDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_NAME]
     */
    @JsonProperty("partner_name")
    public String getPartner_name(){
        return partner_name ;
    }

    /**
     * 设置 [PARTNER_NAME]
     */
    @JsonProperty("partner_name")
    public void setPartner_name(String  partner_name){
        this.partner_name = partner_name ;
        this.partner_nameDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_NAME]脏标记
     */
    @JsonIgnore
    public boolean getPartner_nameDirtyFlag(){
        return partner_nameDirtyFlag ;
    }

    /**
     * 获取 [DAY_CLOSE]
     */
    @JsonProperty("day_close")
    public Double getDay_close(){
        return day_close ;
    }

    /**
     * 设置 [DAY_CLOSE]
     */
    @JsonProperty("day_close")
    public void setDay_close(Double  day_close){
        this.day_close = day_close ;
        this.day_closeDirtyFlag = true ;
    }

    /**
     * 获取 [DAY_CLOSE]脏标记
     */
    @JsonIgnore
    public boolean getDay_closeDirtyFlag(){
        return day_closeDirtyFlag ;
    }

    /**
     * 获取 [PROBABILITY]
     */
    @JsonProperty("probability")
    public Double getProbability(){
        return probability ;
    }

    /**
     * 设置 [PROBABILITY]
     */
    @JsonProperty("probability")
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.probabilityDirtyFlag = true ;
    }

    /**
     * 获取 [PROBABILITY]脏标记
     */
    @JsonIgnore
    public boolean getProbabilityDirtyFlag(){
        return probabilityDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [DELAY_CLOSE]
     */
    @JsonProperty("delay_close")
    public Double getDelay_close(){
        return delay_close ;
    }

    /**
     * 设置 [DELAY_CLOSE]
     */
    @JsonProperty("delay_close")
    public void setDelay_close(Double  delay_close){
        this.delay_close = delay_close ;
        this.delay_closeDirtyFlag = true ;
    }

    /**
     * 获取 [DELAY_CLOSE]脏标记
     */
    @JsonIgnore
    public boolean getDelay_closeDirtyFlag(){
        return delay_closeDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return source_id_text ;
    }

    /**
     * 设置 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return source_id_textDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return campaign_id_text ;
    }

    /**
     * 设置 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return department_id_text ;
    }

    /**
     * 设置 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return department_id_textDirtyFlag ;
    }

    /**
     * 获取 [LAST_STAGE_ID_TEXT]
     */
    @JsonProperty("last_stage_id_text")
    public String getLast_stage_id_text(){
        return last_stage_id_text ;
    }

    /**
     * 设置 [LAST_STAGE_ID_TEXT]
     */
    @JsonProperty("last_stage_id_text")
    public void setLast_stage_id_text(String  last_stage_id_text){
        this.last_stage_id_text = last_stage_id_text ;
        this.last_stage_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_STAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLast_stage_id_textDirtyFlag(){
        return last_stage_id_textDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return stage_id_text ;
    }

    /**
     * 设置 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return stage_id_textDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return medium_id_text ;
    }

    /**
     * 设置 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_NORMAL]
     */
    @JsonProperty("legend_normal")
    public String getLegend_normal(){
        return legend_normal ;
    }

    /**
     * 设置 [LEGEND_NORMAL]
     */
    @JsonProperty("legend_normal")
    public void setLegend_normal(String  legend_normal){
        this.legend_normal = legend_normal ;
        this.legend_normalDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_NORMAL]脏标记
     */
    @JsonIgnore
    public boolean getLegend_normalDirtyFlag(){
        return legend_normalDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TYPE_ID_TEXT]
     */
    @JsonProperty("type_id_text")
    public String getType_id_text(){
        return type_id_text ;
    }

    /**
     * 设置 [TYPE_ID_TEXT]
     */
    @JsonProperty("type_id_text")
    public void setType_id_text(String  type_id_text){
        this.type_id_text = type_id_text ;
        this.type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getType_id_textDirtyFlag(){
        return type_id_textDirtyFlag ;
    }

    /**
     * 获取 [JOB_ID_TEXT]
     */
    @JsonProperty("job_id_text")
    public String getJob_id_text(){
        return job_id_text ;
    }

    /**
     * 设置 [JOB_ID_TEXT]
     */
    @JsonProperty("job_id_text")
    public void setJob_id_text(String  job_id_text){
        this.job_id_text = job_id_text ;
        this.job_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOB_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJob_id_textDirtyFlag(){
        return job_id_textDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_DONE]
     */
    @JsonProperty("legend_done")
    public String getLegend_done(){
        return legend_done ;
    }

    /**
     * 设置 [LEGEND_DONE]
     */
    @JsonProperty("legend_done")
    public void setLegend_done(String  legend_done){
        this.legend_done = legend_done ;
        this.legend_doneDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_DONE]脏标记
     */
    @JsonIgnore
    public boolean getLegend_doneDirtyFlag(){
        return legend_doneDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_BLOCKED]
     */
    @JsonProperty("legend_blocked")
    public String getLegend_blocked(){
        return legend_blocked ;
    }

    /**
     * 设置 [LEGEND_BLOCKED]
     */
    @JsonProperty("legend_blocked")
    public void setLegend_blocked(String  legend_blocked){
        this.legend_blocked = legend_blocked ;
        this.legend_blockedDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_BLOCKED]脏标记
     */
    @JsonIgnore
    public boolean getLegend_blockedDirtyFlag(){
        return legend_blockedDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_NAME]
     */
    @JsonProperty("employee_name")
    public String getEmployee_name(){
        return employee_name ;
    }

    /**
     * 设置 [EMPLOYEE_NAME]
     */
    @JsonProperty("employee_name")
    public void setEmployee_name(String  employee_name){
        this.employee_name = employee_name ;
        this.employee_nameDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_nameDirtyFlag(){
        return employee_nameDirtyFlag ;
    }

    /**
     * 获取 [USER_EMAIL]
     */
    @JsonProperty("user_email")
    public String getUser_email(){
        return user_email ;
    }

    /**
     * 设置 [USER_EMAIL]
     */
    @JsonProperty("user_email")
    public void setUser_email(String  user_email){
        this.user_email = user_email ;
        this.user_emailDirtyFlag = true ;
    }

    /**
     * 获取 [USER_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getUser_emailDirtyFlag(){
        return user_emailDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [JOB_ID]
     */
    @JsonProperty("job_id")
    public Integer getJob_id(){
        return job_id ;
    }

    /**
     * 设置 [JOB_ID]
     */
    @JsonProperty("job_id")
    public void setJob_id(Integer  job_id){
        this.job_id = job_id ;
        this.job_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOB_ID]脏标记
     */
    @JsonIgnore
    public boolean getJob_idDirtyFlag(){
        return job_idDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return campaign_id ;
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return campaign_idDirtyFlag ;
    }

    /**
     * 获取 [LAST_STAGE_ID]
     */
    @JsonProperty("last_stage_id")
    public Integer getLast_stage_id(){
        return last_stage_id ;
    }

    /**
     * 设置 [LAST_STAGE_ID]
     */
    @JsonProperty("last_stage_id")
    public void setLast_stage_id(Integer  last_stage_id){
        this.last_stage_id = last_stage_id ;
        this.last_stage_idDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_STAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getLast_stage_idDirtyFlag(){
        return last_stage_idDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return department_id ;
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return department_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return medium_id ;
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return medium_idDirtyFlag ;
    }

    /**
     * 获取 [EMP_ID]
     */
    @JsonProperty("emp_id")
    public Integer getEmp_id(){
        return emp_id ;
    }

    /**
     * 设置 [EMP_ID]
     */
    @JsonProperty("emp_id")
    public void setEmp_id(Integer  emp_id){
        this.emp_id = emp_id ;
        this.emp_idDirtyFlag = true ;
    }

    /**
     * 获取 [EMP_ID]脏标记
     */
    @JsonIgnore
    public boolean getEmp_idDirtyFlag(){
        return emp_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [TYPE_ID]
     */
    @JsonProperty("type_id")
    public Integer getType_id(){
        return type_id ;
    }

    /**
     * 设置 [TYPE_ID]
     */
    @JsonProperty("type_id")
    public void setType_id(Integer  type_id){
        this.type_id = type_id ;
        this.type_idDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getType_idDirtyFlag(){
        return type_idDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return source_id ;
    }

    /**
     * 设置 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return source_idDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return stage_id ;
    }

    /**
     * 设置 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return stage_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }



    public Hr_applicant toDO() {
        Hr_applicant srfdomain = new Hr_applicant();
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(getSalary_expectedDirtyFlag())
            srfdomain.setSalary_expected(salary_expected);
        if(getCateg_idsDirtyFlag())
            srfdomain.setCateg_ids(categ_ids);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getSalary_proposedDirtyFlag())
            srfdomain.setSalary_proposed(salary_proposed);
        if(getAttachment_numberDirtyFlag())
            srfdomain.setAttachment_number(attachment_number);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getPartner_phoneDirtyFlag())
            srfdomain.setPartner_phone(partner_phone);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getKanban_stateDirtyFlag())
            srfdomain.setKanban_state(kanban_state);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getDate_openDirtyFlag())
            srfdomain.setDate_open(date_open);
        if(getDay_openDirtyFlag())
            srfdomain.setDay_open(day_open);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getAvailabilityDirtyFlag())
            srfdomain.setAvailability(availability);
        if(getSalary_expected_extraDirtyFlag())
            srfdomain.setSalary_expected_extra(salary_expected_extra);
        if(getDate_closedDirtyFlag())
            srfdomain.setDate_closed(date_closed);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getPartner_mobileDirtyFlag())
            srfdomain.setPartner_mobile(partner_mobile);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getSalary_proposed_extraDirtyFlag())
            srfdomain.setSalary_proposed_extra(salary_proposed_extra);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getEmail_fromDirtyFlag())
            srfdomain.setEmail_from(email_from);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getDate_last_stage_updateDirtyFlag())
            srfdomain.setDate_last_stage_update(date_last_stage_update);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getReferenceDirtyFlag())
            srfdomain.setReference(reference);
        if(getAttachment_idsDirtyFlag())
            srfdomain.setAttachment_ids(attachment_ids);
        if(getEmail_ccDirtyFlag())
            srfdomain.setEmail_cc(email_cc);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getPartner_nameDirtyFlag())
            srfdomain.setPartner_name(partner_name);
        if(getDay_closeDirtyFlag())
            srfdomain.setDay_close(day_close);
        if(getProbabilityDirtyFlag())
            srfdomain.setProbability(probability);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getDelay_closeDirtyFlag())
            srfdomain.setDelay_close(delay_close);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getSource_id_textDirtyFlag())
            srfdomain.setSource_id_text(source_id_text);
        if(getCampaign_id_textDirtyFlag())
            srfdomain.setCampaign_id_text(campaign_id_text);
        if(getDepartment_id_textDirtyFlag())
            srfdomain.setDepartment_id_text(department_id_text);
        if(getLast_stage_id_textDirtyFlag())
            srfdomain.setLast_stage_id_text(last_stage_id_text);
        if(getStage_id_textDirtyFlag())
            srfdomain.setStage_id_text(stage_id_text);
        if(getMedium_id_textDirtyFlag())
            srfdomain.setMedium_id_text(medium_id_text);
        if(getLegend_normalDirtyFlag())
            srfdomain.setLegend_normal(legend_normal);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getType_id_textDirtyFlag())
            srfdomain.setType_id_text(type_id_text);
        if(getJob_id_textDirtyFlag())
            srfdomain.setJob_id_text(job_id_text);
        if(getLegend_doneDirtyFlag())
            srfdomain.setLegend_done(legend_done);
        if(getLegend_blockedDirtyFlag())
            srfdomain.setLegend_blocked(legend_blocked);
        if(getEmployee_nameDirtyFlag())
            srfdomain.setEmployee_name(employee_name);
        if(getUser_emailDirtyFlag())
            srfdomain.setUser_email(user_email);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getJob_idDirtyFlag())
            srfdomain.setJob_id(job_id);
        if(getCampaign_idDirtyFlag())
            srfdomain.setCampaign_id(campaign_id);
        if(getLast_stage_idDirtyFlag())
            srfdomain.setLast_stage_id(last_stage_id);
        if(getDepartment_idDirtyFlag())
            srfdomain.setDepartment_id(department_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getMedium_idDirtyFlag())
            srfdomain.setMedium_id(medium_id);
        if(getEmp_idDirtyFlag())
            srfdomain.setEmp_id(emp_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getType_idDirtyFlag())
            srfdomain.setType_id(type_id);
        if(getSource_idDirtyFlag())
            srfdomain.setSource_id(source_id);
        if(getStage_idDirtyFlag())
            srfdomain.setStage_id(stage_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);

        return srfdomain;
    }

    public void fromDO(Hr_applicant srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.getSalary_expectedDirtyFlag())
            this.setSalary_expected(srfdomain.getSalary_expected());
        if(srfdomain.getCateg_idsDirtyFlag())
            this.setCateg_ids(srfdomain.getCateg_ids());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getSalary_proposedDirtyFlag())
            this.setSalary_proposed(srfdomain.getSalary_proposed());
        if(srfdomain.getAttachment_numberDirtyFlag())
            this.setAttachment_number(srfdomain.getAttachment_number());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getPartner_phoneDirtyFlag())
            this.setPartner_phone(srfdomain.getPartner_phone());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getKanban_stateDirtyFlag())
            this.setKanban_state(srfdomain.getKanban_state());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getDate_openDirtyFlag())
            this.setDate_open(srfdomain.getDate_open());
        if(srfdomain.getDay_openDirtyFlag())
            this.setDay_open(srfdomain.getDay_open());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getAvailabilityDirtyFlag())
            this.setAvailability(srfdomain.getAvailability());
        if(srfdomain.getSalary_expected_extraDirtyFlag())
            this.setSalary_expected_extra(srfdomain.getSalary_expected_extra());
        if(srfdomain.getDate_closedDirtyFlag())
            this.setDate_closed(srfdomain.getDate_closed());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getPartner_mobileDirtyFlag())
            this.setPartner_mobile(srfdomain.getPartner_mobile());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getSalary_proposed_extraDirtyFlag())
            this.setSalary_proposed_extra(srfdomain.getSalary_proposed_extra());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getEmail_fromDirtyFlag())
            this.setEmail_from(srfdomain.getEmail_from());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getDate_last_stage_updateDirtyFlag())
            this.setDate_last_stage_update(srfdomain.getDate_last_stage_update());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getReferenceDirtyFlag())
            this.setReference(srfdomain.getReference());
        if(srfdomain.getAttachment_idsDirtyFlag())
            this.setAttachment_ids(srfdomain.getAttachment_ids());
        if(srfdomain.getEmail_ccDirtyFlag())
            this.setEmail_cc(srfdomain.getEmail_cc());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getPartner_nameDirtyFlag())
            this.setPartner_name(srfdomain.getPartner_name());
        if(srfdomain.getDay_closeDirtyFlag())
            this.setDay_close(srfdomain.getDay_close());
        if(srfdomain.getProbabilityDirtyFlag())
            this.setProbability(srfdomain.getProbability());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getDelay_closeDirtyFlag())
            this.setDelay_close(srfdomain.getDelay_close());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getSource_id_textDirtyFlag())
            this.setSource_id_text(srfdomain.getSource_id_text());
        if(srfdomain.getCampaign_id_textDirtyFlag())
            this.setCampaign_id_text(srfdomain.getCampaign_id_text());
        if(srfdomain.getDepartment_id_textDirtyFlag())
            this.setDepartment_id_text(srfdomain.getDepartment_id_text());
        if(srfdomain.getLast_stage_id_textDirtyFlag())
            this.setLast_stage_id_text(srfdomain.getLast_stage_id_text());
        if(srfdomain.getStage_id_textDirtyFlag())
            this.setStage_id_text(srfdomain.getStage_id_text());
        if(srfdomain.getMedium_id_textDirtyFlag())
            this.setMedium_id_text(srfdomain.getMedium_id_text());
        if(srfdomain.getLegend_normalDirtyFlag())
            this.setLegend_normal(srfdomain.getLegend_normal());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getType_id_textDirtyFlag())
            this.setType_id_text(srfdomain.getType_id_text());
        if(srfdomain.getJob_id_textDirtyFlag())
            this.setJob_id_text(srfdomain.getJob_id_text());
        if(srfdomain.getLegend_doneDirtyFlag())
            this.setLegend_done(srfdomain.getLegend_done());
        if(srfdomain.getLegend_blockedDirtyFlag())
            this.setLegend_blocked(srfdomain.getLegend_blocked());
        if(srfdomain.getEmployee_nameDirtyFlag())
            this.setEmployee_name(srfdomain.getEmployee_name());
        if(srfdomain.getUser_emailDirtyFlag())
            this.setUser_email(srfdomain.getUser_email());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getJob_idDirtyFlag())
            this.setJob_id(srfdomain.getJob_id());
        if(srfdomain.getCampaign_idDirtyFlag())
            this.setCampaign_id(srfdomain.getCampaign_id());
        if(srfdomain.getLast_stage_idDirtyFlag())
            this.setLast_stage_id(srfdomain.getLast_stage_id());
        if(srfdomain.getDepartment_idDirtyFlag())
            this.setDepartment_id(srfdomain.getDepartment_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getMedium_idDirtyFlag())
            this.setMedium_id(srfdomain.getMedium_id());
        if(srfdomain.getEmp_idDirtyFlag())
            this.setEmp_id(srfdomain.getEmp_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getType_idDirtyFlag())
            this.setType_id(srfdomain.getType_id());
        if(srfdomain.getSource_idDirtyFlag())
            this.setSource_id(srfdomain.getSource_id());
        if(srfdomain.getStage_idDirtyFlag())
            this.setStage_id(srfdomain.getStage_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());

    }

    public List<Hr_applicantDTO> fromDOPage(List<Hr_applicant> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_applicantDTO> dtos=new ArrayList<Hr_applicantDTO>();
        for(Hr_applicant domain : poPage) {
            Hr_applicantDTO dto = new Hr_applicantDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

