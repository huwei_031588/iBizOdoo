package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_leave_type.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_leave_typeDTO]
 */
public class Hr_leave_typeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [VALID]
     *
     */
    @Hr_leave_typeValidDefault(info = "默认规则")
    private String valid;

    @JsonIgnore
    private boolean validDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Hr_leave_typeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [VIRTUAL_REMAINING_LEAVES]
     *
     */
    @Hr_leave_typeVirtual_remaining_leavesDefault(info = "默认规则")
    private Double virtual_remaining_leaves;

    @JsonIgnore
    private boolean virtual_remaining_leavesDirtyFlag;

    /**
     * 属性 [LEAVES_TAKEN]
     *
     */
    @Hr_leave_typeLeaves_takenDefault(info = "默认规则")
    private Double leaves_taken;

    @JsonIgnore
    private boolean leaves_takenDirtyFlag;

    /**
     * 属性 [GROUP_DAYS_ALLOCATION]
     *
     */
    @Hr_leave_typeGroup_days_allocationDefault(info = "默认规则")
    private Double group_days_allocation;

    @JsonIgnore
    private boolean group_days_allocationDirtyFlag;

    /**
     * 属性 [DOUBLE_VALIDATION]
     *
     */
    @Hr_leave_typeDouble_validationDefault(info = "默认规则")
    private String double_validation;

    @JsonIgnore
    private boolean double_validationDirtyFlag;

    /**
     * 属性 [ALLOCATION_TYPE]
     *
     */
    @Hr_leave_typeAllocation_typeDefault(info = "默认规则")
    private String allocation_type;

    @JsonIgnore
    private boolean allocation_typeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_leave_typeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Hr_leave_typeSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [UNPAID]
     *
     */
    @Hr_leave_typeUnpaidDefault(info = "默认规则")
    private String unpaid;

    @JsonIgnore
    private boolean unpaidDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Hr_leave_typeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MAX_LEAVES]
     *
     */
    @Hr_leave_typeMax_leavesDefault(info = "默认规则")
    private Double max_leaves;

    @JsonIgnore
    private boolean max_leavesDirtyFlag;

    /**
     * 属性 [VALIDITY_STOP]
     *
     */
    @Hr_leave_typeValidity_stopDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp validity_stop;

    @JsonIgnore
    private boolean validity_stopDirtyFlag;

    /**
     * 属性 [VALIDATION_TYPE]
     *
     */
    @Hr_leave_typeValidation_typeDefault(info = "默认规则")
    private String validation_type;

    @JsonIgnore
    private boolean validation_typeDirtyFlag;

    /**
     * 属性 [TIME_TYPE]
     *
     */
    @Hr_leave_typeTime_typeDefault(info = "默认规则")
    private String time_type;

    @JsonIgnore
    private boolean time_typeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_leave_type__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [REQUEST_UNIT]
     *
     */
    @Hr_leave_typeRequest_unitDefault(info = "默认规则")
    private String request_unit;

    @JsonIgnore
    private boolean request_unitDirtyFlag;

    /**
     * 属性 [GROUP_DAYS_LEAVE]
     *
     */
    @Hr_leave_typeGroup_days_leaveDefault(info = "默认规则")
    private Double group_days_leave;

    @JsonIgnore
    private boolean group_days_leaveDirtyFlag;

    /**
     * 属性 [COLOR_NAME]
     *
     */
    @Hr_leave_typeColor_nameDefault(info = "默认规则")
    private String color_name;

    @JsonIgnore
    private boolean color_nameDirtyFlag;

    /**
     * 属性 [REMAINING_LEAVES]
     *
     */
    @Hr_leave_typeRemaining_leavesDefault(info = "默认规则")
    private Double remaining_leaves;

    @JsonIgnore
    private boolean remaining_leavesDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Hr_leave_typeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [VALIDITY_START]
     *
     */
    @Hr_leave_typeValidity_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp validity_start;

    @JsonIgnore
    private boolean validity_startDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Hr_leave_typeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Hr_leave_typeActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [CATEG_ID_TEXT]
     *
     */
    @Hr_leave_typeCateg_id_textDefault(info = "默认规则")
    private String categ_id_text;

    @JsonIgnore
    private boolean categ_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Hr_leave_typeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Hr_leave_typeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Hr_leave_typeCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CATEG_ID]
     *
     */
    @Hr_leave_typeCateg_idDefault(info = "默认规则")
    private Integer categ_id;

    @JsonIgnore
    private boolean categ_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Hr_leave_typeCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Hr_leave_typeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Hr_leave_typeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [VALID]
     */
    @JsonProperty("valid")
    public String getValid(){
        return valid ;
    }

    /**
     * 设置 [VALID]
     */
    @JsonProperty("valid")
    public void setValid(String  valid){
        this.valid = valid ;
        this.validDirtyFlag = true ;
    }

    /**
     * 获取 [VALID]脏标记
     */
    @JsonIgnore
    public boolean getValidDirtyFlag(){
        return validDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [VIRTUAL_REMAINING_LEAVES]
     */
    @JsonProperty("virtual_remaining_leaves")
    public Double getVirtual_remaining_leaves(){
        return virtual_remaining_leaves ;
    }

    /**
     * 设置 [VIRTUAL_REMAINING_LEAVES]
     */
    @JsonProperty("virtual_remaining_leaves")
    public void setVirtual_remaining_leaves(Double  virtual_remaining_leaves){
        this.virtual_remaining_leaves = virtual_remaining_leaves ;
        this.virtual_remaining_leavesDirtyFlag = true ;
    }

    /**
     * 获取 [VIRTUAL_REMAINING_LEAVES]脏标记
     */
    @JsonIgnore
    public boolean getVirtual_remaining_leavesDirtyFlag(){
        return virtual_remaining_leavesDirtyFlag ;
    }

    /**
     * 获取 [LEAVES_TAKEN]
     */
    @JsonProperty("leaves_taken")
    public Double getLeaves_taken(){
        return leaves_taken ;
    }

    /**
     * 设置 [LEAVES_TAKEN]
     */
    @JsonProperty("leaves_taken")
    public void setLeaves_taken(Double  leaves_taken){
        this.leaves_taken = leaves_taken ;
        this.leaves_takenDirtyFlag = true ;
    }

    /**
     * 获取 [LEAVES_TAKEN]脏标记
     */
    @JsonIgnore
    public boolean getLeaves_takenDirtyFlag(){
        return leaves_takenDirtyFlag ;
    }

    /**
     * 获取 [GROUP_DAYS_ALLOCATION]
     */
    @JsonProperty("group_days_allocation")
    public Double getGroup_days_allocation(){
        return group_days_allocation ;
    }

    /**
     * 设置 [GROUP_DAYS_ALLOCATION]
     */
    @JsonProperty("group_days_allocation")
    public void setGroup_days_allocation(Double  group_days_allocation){
        this.group_days_allocation = group_days_allocation ;
        this.group_days_allocationDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_DAYS_ALLOCATION]脏标记
     */
    @JsonIgnore
    public boolean getGroup_days_allocationDirtyFlag(){
        return group_days_allocationDirtyFlag ;
    }

    /**
     * 获取 [DOUBLE_VALIDATION]
     */
    @JsonProperty("double_validation")
    public String getDouble_validation(){
        return double_validation ;
    }

    /**
     * 设置 [DOUBLE_VALIDATION]
     */
    @JsonProperty("double_validation")
    public void setDouble_validation(String  double_validation){
        this.double_validation = double_validation ;
        this.double_validationDirtyFlag = true ;
    }

    /**
     * 获取 [DOUBLE_VALIDATION]脏标记
     */
    @JsonIgnore
    public boolean getDouble_validationDirtyFlag(){
        return double_validationDirtyFlag ;
    }

    /**
     * 获取 [ALLOCATION_TYPE]
     */
    @JsonProperty("allocation_type")
    public String getAllocation_type(){
        return allocation_type ;
    }

    /**
     * 设置 [ALLOCATION_TYPE]
     */
    @JsonProperty("allocation_type")
    public void setAllocation_type(String  allocation_type){
        this.allocation_type = allocation_type ;
        this.allocation_typeDirtyFlag = true ;
    }

    /**
     * 获取 [ALLOCATION_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getAllocation_typeDirtyFlag(){
        return allocation_typeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [UNPAID]
     */
    @JsonProperty("unpaid")
    public String getUnpaid(){
        return unpaid ;
    }

    /**
     * 设置 [UNPAID]
     */
    @JsonProperty("unpaid")
    public void setUnpaid(String  unpaid){
        this.unpaid = unpaid ;
        this.unpaidDirtyFlag = true ;
    }

    /**
     * 获取 [UNPAID]脏标记
     */
    @JsonIgnore
    public boolean getUnpaidDirtyFlag(){
        return unpaidDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MAX_LEAVES]
     */
    @JsonProperty("max_leaves")
    public Double getMax_leaves(){
        return max_leaves ;
    }

    /**
     * 设置 [MAX_LEAVES]
     */
    @JsonProperty("max_leaves")
    public void setMax_leaves(Double  max_leaves){
        this.max_leaves = max_leaves ;
        this.max_leavesDirtyFlag = true ;
    }

    /**
     * 获取 [MAX_LEAVES]脏标记
     */
    @JsonIgnore
    public boolean getMax_leavesDirtyFlag(){
        return max_leavesDirtyFlag ;
    }

    /**
     * 获取 [VALIDITY_STOP]
     */
    @JsonProperty("validity_stop")
    public Timestamp getValidity_stop(){
        return validity_stop ;
    }

    /**
     * 设置 [VALIDITY_STOP]
     */
    @JsonProperty("validity_stop")
    public void setValidity_stop(Timestamp  validity_stop){
        this.validity_stop = validity_stop ;
        this.validity_stopDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDITY_STOP]脏标记
     */
    @JsonIgnore
    public boolean getValidity_stopDirtyFlag(){
        return validity_stopDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_TYPE]
     */
    @JsonProperty("validation_type")
    public String getValidation_type(){
        return validation_type ;
    }

    /**
     * 设置 [VALIDATION_TYPE]
     */
    @JsonProperty("validation_type")
    public void setValidation_type(String  validation_type){
        this.validation_type = validation_type ;
        this.validation_typeDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getValidation_typeDirtyFlag(){
        return validation_typeDirtyFlag ;
    }

    /**
     * 获取 [TIME_TYPE]
     */
    @JsonProperty("time_type")
    public String getTime_type(){
        return time_type ;
    }

    /**
     * 设置 [TIME_TYPE]
     */
    @JsonProperty("time_type")
    public void setTime_type(String  time_type){
        this.time_type = time_type ;
        this.time_typeDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTime_typeDirtyFlag(){
        return time_typeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [REQUEST_UNIT]
     */
    @JsonProperty("request_unit")
    public String getRequest_unit(){
        return request_unit ;
    }

    /**
     * 设置 [REQUEST_UNIT]
     */
    @JsonProperty("request_unit")
    public void setRequest_unit(String  request_unit){
        this.request_unit = request_unit ;
        this.request_unitDirtyFlag = true ;
    }

    /**
     * 获取 [REQUEST_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getRequest_unitDirtyFlag(){
        return request_unitDirtyFlag ;
    }

    /**
     * 获取 [GROUP_DAYS_LEAVE]
     */
    @JsonProperty("group_days_leave")
    public Double getGroup_days_leave(){
        return group_days_leave ;
    }

    /**
     * 设置 [GROUP_DAYS_LEAVE]
     */
    @JsonProperty("group_days_leave")
    public void setGroup_days_leave(Double  group_days_leave){
        this.group_days_leave = group_days_leave ;
        this.group_days_leaveDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_DAYS_LEAVE]脏标记
     */
    @JsonIgnore
    public boolean getGroup_days_leaveDirtyFlag(){
        return group_days_leaveDirtyFlag ;
    }

    /**
     * 获取 [COLOR_NAME]
     */
    @JsonProperty("color_name")
    public String getColor_name(){
        return color_name ;
    }

    /**
     * 设置 [COLOR_NAME]
     */
    @JsonProperty("color_name")
    public void setColor_name(String  color_name){
        this.color_name = color_name ;
        this.color_nameDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR_NAME]脏标记
     */
    @JsonIgnore
    public boolean getColor_nameDirtyFlag(){
        return color_nameDirtyFlag ;
    }

    /**
     * 获取 [REMAINING_LEAVES]
     */
    @JsonProperty("remaining_leaves")
    public Double getRemaining_leaves(){
        return remaining_leaves ;
    }

    /**
     * 设置 [REMAINING_LEAVES]
     */
    @JsonProperty("remaining_leaves")
    public void setRemaining_leaves(Double  remaining_leaves){
        this.remaining_leaves = remaining_leaves ;
        this.remaining_leavesDirtyFlag = true ;
    }

    /**
     * 获取 [REMAINING_LEAVES]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_leavesDirtyFlag(){
        return remaining_leavesDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [VALIDITY_START]
     */
    @JsonProperty("validity_start")
    public Timestamp getValidity_start(){
        return validity_start ;
    }

    /**
     * 设置 [VALIDITY_START]
     */
    @JsonProperty("validity_start")
    public void setValidity_start(Timestamp  validity_start){
        this.validity_start = validity_start ;
        this.validity_startDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDITY_START]脏标记
     */
    @JsonIgnore
    public boolean getValidity_startDirtyFlag(){
        return validity_startDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [CATEG_ID_TEXT]
     */
    @JsonProperty("categ_id_text")
    public String getCateg_id_text(){
        return categ_id_text ;
    }

    /**
     * 设置 [CATEG_ID_TEXT]
     */
    @JsonProperty("categ_id_text")
    public void setCateg_id_text(String  categ_id_text){
        this.categ_id_text = categ_id_text ;
        this.categ_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCateg_id_textDirtyFlag(){
        return categ_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CATEG_ID]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return categ_id ;
    }

    /**
     * 设置 [CATEG_ID]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return categ_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Hr_leave_type toDO() {
        Hr_leave_type srfdomain = new Hr_leave_type();
        if(getValidDirtyFlag())
            srfdomain.setValid(valid);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getVirtual_remaining_leavesDirtyFlag())
            srfdomain.setVirtual_remaining_leaves(virtual_remaining_leaves);
        if(getLeaves_takenDirtyFlag())
            srfdomain.setLeaves_taken(leaves_taken);
        if(getGroup_days_allocationDirtyFlag())
            srfdomain.setGroup_days_allocation(group_days_allocation);
        if(getDouble_validationDirtyFlag())
            srfdomain.setDouble_validation(double_validation);
        if(getAllocation_typeDirtyFlag())
            srfdomain.setAllocation_type(allocation_type);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getUnpaidDirtyFlag())
            srfdomain.setUnpaid(unpaid);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMax_leavesDirtyFlag())
            srfdomain.setMax_leaves(max_leaves);
        if(getValidity_stopDirtyFlag())
            srfdomain.setValidity_stop(validity_stop);
        if(getValidation_typeDirtyFlag())
            srfdomain.setValidation_type(validation_type);
        if(getTime_typeDirtyFlag())
            srfdomain.setTime_type(time_type);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getRequest_unitDirtyFlag())
            srfdomain.setRequest_unit(request_unit);
        if(getGroup_days_leaveDirtyFlag())
            srfdomain.setGroup_days_leave(group_days_leave);
        if(getColor_nameDirtyFlag())
            srfdomain.setColor_name(color_name);
        if(getRemaining_leavesDirtyFlag())
            srfdomain.setRemaining_leaves(remaining_leaves);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getValidity_startDirtyFlag())
            srfdomain.setValidity_start(validity_start);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getCateg_id_textDirtyFlag())
            srfdomain.setCateg_id_text(categ_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCateg_idDirtyFlag())
            srfdomain.setCateg_id(categ_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Hr_leave_type srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getValidDirtyFlag())
            this.setValid(srfdomain.getValid());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getVirtual_remaining_leavesDirtyFlag())
            this.setVirtual_remaining_leaves(srfdomain.getVirtual_remaining_leaves());
        if(srfdomain.getLeaves_takenDirtyFlag())
            this.setLeaves_taken(srfdomain.getLeaves_taken());
        if(srfdomain.getGroup_days_allocationDirtyFlag())
            this.setGroup_days_allocation(srfdomain.getGroup_days_allocation());
        if(srfdomain.getDouble_validationDirtyFlag())
            this.setDouble_validation(srfdomain.getDouble_validation());
        if(srfdomain.getAllocation_typeDirtyFlag())
            this.setAllocation_type(srfdomain.getAllocation_type());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getUnpaidDirtyFlag())
            this.setUnpaid(srfdomain.getUnpaid());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMax_leavesDirtyFlag())
            this.setMax_leaves(srfdomain.getMax_leaves());
        if(srfdomain.getValidity_stopDirtyFlag())
            this.setValidity_stop(srfdomain.getValidity_stop());
        if(srfdomain.getValidation_typeDirtyFlag())
            this.setValidation_type(srfdomain.getValidation_type());
        if(srfdomain.getTime_typeDirtyFlag())
            this.setTime_type(srfdomain.getTime_type());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getRequest_unitDirtyFlag())
            this.setRequest_unit(srfdomain.getRequest_unit());
        if(srfdomain.getGroup_days_leaveDirtyFlag())
            this.setGroup_days_leave(srfdomain.getGroup_days_leave());
        if(srfdomain.getColor_nameDirtyFlag())
            this.setColor_name(srfdomain.getColor_name());
        if(srfdomain.getRemaining_leavesDirtyFlag())
            this.setRemaining_leaves(srfdomain.getRemaining_leaves());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getValidity_startDirtyFlag())
            this.setValidity_start(srfdomain.getValidity_start());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getCateg_id_textDirtyFlag())
            this.setCateg_id_text(srfdomain.getCateg_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCateg_idDirtyFlag())
            this.setCateg_id(srfdomain.getCateg_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Hr_leave_typeDTO> fromDOPage(List<Hr_leave_type> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_leave_typeDTO> dtos=new ArrayList<Hr_leave_typeDTO>();
        for(Hr_leave_type domain : poPage) {
            Hr_leave_typeDTO dto = new Hr_leave_typeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

