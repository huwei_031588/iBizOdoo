package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_recruitment_stageDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_stageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_recruitment_stage" })
@RestController
@RequestMapping("")
public class Hr_recruitment_stageResource {

    @Autowired
    private IHr_recruitment_stageService hr_recruitment_stageService;

    public IHr_recruitment_stageService getHr_recruitment_stageService() {
        return this.hr_recruitment_stageService;
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_recruitment_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_stages/{hr_recruitment_stage_id}")
    public ResponseEntity<Hr_recruitment_stageDTO> get(@PathVariable("hr_recruitment_stage_id") Integer hr_recruitment_stage_id) {
        Hr_recruitment_stageDTO dto = new Hr_recruitment_stageDTO();
        Hr_recruitment_stage domain = hr_recruitment_stageService.get(hr_recruitment_stage_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_recruitment_stage" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_stages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_recruitment_stageDTO> hr_recruitment_stagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_recruitment_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_stages")

    public ResponseEntity<Hr_recruitment_stageDTO> create(@RequestBody Hr_recruitment_stageDTO hr_recruitment_stagedto) {
        Hr_recruitment_stageDTO dto = new Hr_recruitment_stageDTO();
        Hr_recruitment_stage domain = hr_recruitment_stagedto.toDO();
		hr_recruitment_stageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_recruitment_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_stages/{hr_recruitment_stage_id}")

    public ResponseEntity<Hr_recruitment_stageDTO> update(@PathVariable("hr_recruitment_stage_id") Integer hr_recruitment_stage_id, @RequestBody Hr_recruitment_stageDTO hr_recruitment_stagedto) {
		Hr_recruitment_stage domain = hr_recruitment_stagedto.toDO();
        domain.setId(hr_recruitment_stage_id);
		hr_recruitment_stageService.update(domain);
		Hr_recruitment_stageDTO dto = new Hr_recruitment_stageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_recruitment_stage" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_stages/createBatch")
    public ResponseEntity<Boolean> createBatchHr_recruitment_stage(@RequestBody List<Hr_recruitment_stageDTO> hr_recruitment_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_recruitment_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_stages/{hr_recruitment_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_recruitment_stage_id") Integer hr_recruitment_stage_id) {
        Hr_recruitment_stageDTO hr_recruitment_stagedto = new Hr_recruitment_stageDTO();
		Hr_recruitment_stage domain = new Hr_recruitment_stage();
		hr_recruitment_stagedto.setId(hr_recruitment_stage_id);
		domain.setId(hr_recruitment_stage_id);
        Boolean rst = hr_recruitment_stageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_recruitment_stage" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_stages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_recruitment_stageDTO> hr_recruitment_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_recruitment_stage" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_recruitment_stages/fetchdefault")
	public ResponseEntity<Page<Hr_recruitment_stageDTO>> fetchDefault(Hr_recruitment_stageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_recruitment_stageDTO> list = new ArrayList<Hr_recruitment_stageDTO>();
        
        Page<Hr_recruitment_stage> domains = hr_recruitment_stageService.searchDefault(context) ;
        for(Hr_recruitment_stage hr_recruitment_stage : domains.getContent()){
            Hr_recruitment_stageDTO dto = new Hr_recruitment_stageDTO();
            dto.fromDO(hr_recruitment_stage);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
