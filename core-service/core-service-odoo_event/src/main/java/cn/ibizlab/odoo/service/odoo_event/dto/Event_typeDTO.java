package cn.ibizlab.odoo.service.odoo_event.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_event.valuerule.anno.event_type.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Event_typeDTO]
 */
public class Event_typeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DEFAULT_REGISTRATION_MAX]
     *
     */
    @Event_typeDefault_registration_maxDefault(info = "默认规则")
    private Integer default_registration_max;

    @JsonIgnore
    private boolean default_registration_maxDirtyFlag;

    /**
     * 属性 [EVENT_TYPE_MAIL_IDS]
     *
     */
    @Event_typeEvent_type_mail_idsDefault(info = "默认规则")
    private String event_type_mail_ids;

    @JsonIgnore
    private boolean event_type_mail_idsDirtyFlag;

    /**
     * 属性 [DEFAULT_HASHTAG]
     *
     */
    @Event_typeDefault_hashtagDefault(info = "默认规则")
    private String default_hashtag;

    @JsonIgnore
    private boolean default_hashtagDirtyFlag;

    /**
     * 属性 [DEFAULT_TIMEZONE]
     *
     */
    @Event_typeDefault_timezoneDefault(info = "默认规则")
    private String default_timezone;

    @JsonIgnore
    private boolean default_timezoneDirtyFlag;

    /**
     * 属性 [DEFAULT_REGISTRATION_MIN]
     *
     */
    @Event_typeDefault_registration_minDefault(info = "默认规则")
    private Integer default_registration_min;

    @JsonIgnore
    private boolean default_registration_minDirtyFlag;

    /**
     * 属性 [EVENT_TICKET_IDS]
     *
     */
    @Event_typeEvent_ticket_idsDefault(info = "默认规则")
    private String event_ticket_ids;

    @JsonIgnore
    private boolean event_ticket_idsDirtyFlag;

    /**
     * 属性 [USE_TICKETING]
     *
     */
    @Event_typeUse_ticketingDefault(info = "默认规则")
    private String use_ticketing;

    @JsonIgnore
    private boolean use_ticketingDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Event_typeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [IS_ONLINE]
     *
     */
    @Event_typeIs_onlineDefault(info = "默认规则")
    private String is_online;

    @JsonIgnore
    private boolean is_onlineDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Event_typeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Event_typeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Event_typeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [AUTO_CONFIRM]
     *
     */
    @Event_typeAuto_confirmDefault(info = "默认规则")
    private String auto_confirm;

    @JsonIgnore
    private boolean auto_confirmDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Event_type__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WEBSITE_MENU]
     *
     */
    @Event_typeWebsite_menuDefault(info = "默认规则")
    private String website_menu;

    @JsonIgnore
    private boolean website_menuDirtyFlag;

    /**
     * 属性 [USE_HASHTAG]
     *
     */
    @Event_typeUse_hashtagDefault(info = "默认规则")
    private String use_hashtag;

    @JsonIgnore
    private boolean use_hashtagDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Event_typeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [HAS_SEATS_LIMITATION]
     *
     */
    @Event_typeHas_seats_limitationDefault(info = "默认规则")
    private String has_seats_limitation;

    @JsonIgnore
    private boolean has_seats_limitationDirtyFlag;

    /**
     * 属性 [USE_MAIL_SCHEDULE]
     *
     */
    @Event_typeUse_mail_scheduleDefault(info = "默认规则")
    private String use_mail_schedule;

    @JsonIgnore
    private boolean use_mail_scheduleDirtyFlag;

    /**
     * 属性 [USE_TIMEZONE]
     *
     */
    @Event_typeUse_timezoneDefault(info = "默认规则")
    private String use_timezone;

    @JsonIgnore
    private boolean use_timezoneDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Event_typeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Event_typeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Event_typeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Event_typeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [DEFAULT_REGISTRATION_MAX]
     */
    @JsonProperty("default_registration_max")
    public Integer getDefault_registration_max(){
        return default_registration_max ;
    }

    /**
     * 设置 [DEFAULT_REGISTRATION_MAX]
     */
    @JsonProperty("default_registration_max")
    public void setDefault_registration_max(Integer  default_registration_max){
        this.default_registration_max = default_registration_max ;
        this.default_registration_maxDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_REGISTRATION_MAX]脏标记
     */
    @JsonIgnore
    public boolean getDefault_registration_maxDirtyFlag(){
        return default_registration_maxDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TYPE_MAIL_IDS]
     */
    @JsonProperty("event_type_mail_ids")
    public String getEvent_type_mail_ids(){
        return event_type_mail_ids ;
    }

    /**
     * 设置 [EVENT_TYPE_MAIL_IDS]
     */
    @JsonProperty("event_type_mail_ids")
    public void setEvent_type_mail_ids(String  event_type_mail_ids){
        this.event_type_mail_ids = event_type_mail_ids ;
        this.event_type_mail_idsDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TYPE_MAIL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getEvent_type_mail_idsDirtyFlag(){
        return event_type_mail_idsDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_HASHTAG]
     */
    @JsonProperty("default_hashtag")
    public String getDefault_hashtag(){
        return default_hashtag ;
    }

    /**
     * 设置 [DEFAULT_HASHTAG]
     */
    @JsonProperty("default_hashtag")
    public void setDefault_hashtag(String  default_hashtag){
        this.default_hashtag = default_hashtag ;
        this.default_hashtagDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_HASHTAG]脏标记
     */
    @JsonIgnore
    public boolean getDefault_hashtagDirtyFlag(){
        return default_hashtagDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_TIMEZONE]
     */
    @JsonProperty("default_timezone")
    public String getDefault_timezone(){
        return default_timezone ;
    }

    /**
     * 设置 [DEFAULT_TIMEZONE]
     */
    @JsonProperty("default_timezone")
    public void setDefault_timezone(String  default_timezone){
        this.default_timezone = default_timezone ;
        this.default_timezoneDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_TIMEZONE]脏标记
     */
    @JsonIgnore
    public boolean getDefault_timezoneDirtyFlag(){
        return default_timezoneDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_REGISTRATION_MIN]
     */
    @JsonProperty("default_registration_min")
    public Integer getDefault_registration_min(){
        return default_registration_min ;
    }

    /**
     * 设置 [DEFAULT_REGISTRATION_MIN]
     */
    @JsonProperty("default_registration_min")
    public void setDefault_registration_min(Integer  default_registration_min){
        this.default_registration_min = default_registration_min ;
        this.default_registration_minDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_REGISTRATION_MIN]脏标记
     */
    @JsonIgnore
    public boolean getDefault_registration_minDirtyFlag(){
        return default_registration_minDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TICKET_IDS]
     */
    @JsonProperty("event_ticket_ids")
    public String getEvent_ticket_ids(){
        return event_ticket_ids ;
    }

    /**
     * 设置 [EVENT_TICKET_IDS]
     */
    @JsonProperty("event_ticket_ids")
    public void setEvent_ticket_ids(String  event_ticket_ids){
        this.event_ticket_ids = event_ticket_ids ;
        this.event_ticket_idsDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TICKET_IDS]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_idsDirtyFlag(){
        return event_ticket_idsDirtyFlag ;
    }

    /**
     * 获取 [USE_TICKETING]
     */
    @JsonProperty("use_ticketing")
    public String getUse_ticketing(){
        return use_ticketing ;
    }

    /**
     * 设置 [USE_TICKETING]
     */
    @JsonProperty("use_ticketing")
    public void setUse_ticketing(String  use_ticketing){
        this.use_ticketing = use_ticketing ;
        this.use_ticketingDirtyFlag = true ;
    }

    /**
     * 获取 [USE_TICKETING]脏标记
     */
    @JsonIgnore
    public boolean getUse_ticketingDirtyFlag(){
        return use_ticketingDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [IS_ONLINE]
     */
    @JsonProperty("is_online")
    public String getIs_online(){
        return is_online ;
    }

    /**
     * 设置 [IS_ONLINE]
     */
    @JsonProperty("is_online")
    public void setIs_online(String  is_online){
        this.is_online = is_online ;
        this.is_onlineDirtyFlag = true ;
    }

    /**
     * 获取 [IS_ONLINE]脏标记
     */
    @JsonIgnore
    public boolean getIs_onlineDirtyFlag(){
        return is_onlineDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [AUTO_CONFIRM]
     */
    @JsonProperty("auto_confirm")
    public String getAuto_confirm(){
        return auto_confirm ;
    }

    /**
     * 设置 [AUTO_CONFIRM]
     */
    @JsonProperty("auto_confirm")
    public void setAuto_confirm(String  auto_confirm){
        this.auto_confirm = auto_confirm ;
        this.auto_confirmDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_CONFIRM]脏标记
     */
    @JsonIgnore
    public boolean getAuto_confirmDirtyFlag(){
        return auto_confirmDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MENU]
     */
    @JsonProperty("website_menu")
    public String getWebsite_menu(){
        return website_menu ;
    }

    /**
     * 设置 [WEBSITE_MENU]
     */
    @JsonProperty("website_menu")
    public void setWebsite_menu(String  website_menu){
        this.website_menu = website_menu ;
        this.website_menuDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MENU]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_menuDirtyFlag(){
        return website_menuDirtyFlag ;
    }

    /**
     * 获取 [USE_HASHTAG]
     */
    @JsonProperty("use_hashtag")
    public String getUse_hashtag(){
        return use_hashtag ;
    }

    /**
     * 设置 [USE_HASHTAG]
     */
    @JsonProperty("use_hashtag")
    public void setUse_hashtag(String  use_hashtag){
        this.use_hashtag = use_hashtag ;
        this.use_hashtagDirtyFlag = true ;
    }

    /**
     * 获取 [USE_HASHTAG]脏标记
     */
    @JsonIgnore
    public boolean getUse_hashtagDirtyFlag(){
        return use_hashtagDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [HAS_SEATS_LIMITATION]
     */
    @JsonProperty("has_seats_limitation")
    public String getHas_seats_limitation(){
        return has_seats_limitation ;
    }

    /**
     * 设置 [HAS_SEATS_LIMITATION]
     */
    @JsonProperty("has_seats_limitation")
    public void setHas_seats_limitation(String  has_seats_limitation){
        this.has_seats_limitation = has_seats_limitation ;
        this.has_seats_limitationDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_SEATS_LIMITATION]脏标记
     */
    @JsonIgnore
    public boolean getHas_seats_limitationDirtyFlag(){
        return has_seats_limitationDirtyFlag ;
    }

    /**
     * 获取 [USE_MAIL_SCHEDULE]
     */
    @JsonProperty("use_mail_schedule")
    public String getUse_mail_schedule(){
        return use_mail_schedule ;
    }

    /**
     * 设置 [USE_MAIL_SCHEDULE]
     */
    @JsonProperty("use_mail_schedule")
    public void setUse_mail_schedule(String  use_mail_schedule){
        this.use_mail_schedule = use_mail_schedule ;
        this.use_mail_scheduleDirtyFlag = true ;
    }

    /**
     * 获取 [USE_MAIL_SCHEDULE]脏标记
     */
    @JsonIgnore
    public boolean getUse_mail_scheduleDirtyFlag(){
        return use_mail_scheduleDirtyFlag ;
    }

    /**
     * 获取 [USE_TIMEZONE]
     */
    @JsonProperty("use_timezone")
    public String getUse_timezone(){
        return use_timezone ;
    }

    /**
     * 设置 [USE_TIMEZONE]
     */
    @JsonProperty("use_timezone")
    public void setUse_timezone(String  use_timezone){
        this.use_timezone = use_timezone ;
        this.use_timezoneDirtyFlag = true ;
    }

    /**
     * 获取 [USE_TIMEZONE]脏标记
     */
    @JsonIgnore
    public boolean getUse_timezoneDirtyFlag(){
        return use_timezoneDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Event_type toDO() {
        Event_type srfdomain = new Event_type();
        if(getDefault_registration_maxDirtyFlag())
            srfdomain.setDefault_registration_max(default_registration_max);
        if(getEvent_type_mail_idsDirtyFlag())
            srfdomain.setEvent_type_mail_ids(event_type_mail_ids);
        if(getDefault_hashtagDirtyFlag())
            srfdomain.setDefault_hashtag(default_hashtag);
        if(getDefault_timezoneDirtyFlag())
            srfdomain.setDefault_timezone(default_timezone);
        if(getDefault_registration_minDirtyFlag())
            srfdomain.setDefault_registration_min(default_registration_min);
        if(getEvent_ticket_idsDirtyFlag())
            srfdomain.setEvent_ticket_ids(event_ticket_ids);
        if(getUse_ticketingDirtyFlag())
            srfdomain.setUse_ticketing(use_ticketing);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getIs_onlineDirtyFlag())
            srfdomain.setIs_online(is_online);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getAuto_confirmDirtyFlag())
            srfdomain.setAuto_confirm(auto_confirm);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWebsite_menuDirtyFlag())
            srfdomain.setWebsite_menu(website_menu);
        if(getUse_hashtagDirtyFlag())
            srfdomain.setUse_hashtag(use_hashtag);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getHas_seats_limitationDirtyFlag())
            srfdomain.setHas_seats_limitation(has_seats_limitation);
        if(getUse_mail_scheduleDirtyFlag())
            srfdomain.setUse_mail_schedule(use_mail_schedule);
        if(getUse_timezoneDirtyFlag())
            srfdomain.setUse_timezone(use_timezone);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Event_type srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDefault_registration_maxDirtyFlag())
            this.setDefault_registration_max(srfdomain.getDefault_registration_max());
        if(srfdomain.getEvent_type_mail_idsDirtyFlag())
            this.setEvent_type_mail_ids(srfdomain.getEvent_type_mail_ids());
        if(srfdomain.getDefault_hashtagDirtyFlag())
            this.setDefault_hashtag(srfdomain.getDefault_hashtag());
        if(srfdomain.getDefault_timezoneDirtyFlag())
            this.setDefault_timezone(srfdomain.getDefault_timezone());
        if(srfdomain.getDefault_registration_minDirtyFlag())
            this.setDefault_registration_min(srfdomain.getDefault_registration_min());
        if(srfdomain.getEvent_ticket_idsDirtyFlag())
            this.setEvent_ticket_ids(srfdomain.getEvent_ticket_ids());
        if(srfdomain.getUse_ticketingDirtyFlag())
            this.setUse_ticketing(srfdomain.getUse_ticketing());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getIs_onlineDirtyFlag())
            this.setIs_online(srfdomain.getIs_online());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getAuto_confirmDirtyFlag())
            this.setAuto_confirm(srfdomain.getAuto_confirm());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWebsite_menuDirtyFlag())
            this.setWebsite_menu(srfdomain.getWebsite_menu());
        if(srfdomain.getUse_hashtagDirtyFlag())
            this.setUse_hashtag(srfdomain.getUse_hashtag());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getHas_seats_limitationDirtyFlag())
            this.setHas_seats_limitation(srfdomain.getHas_seats_limitation());
        if(srfdomain.getUse_mail_scheduleDirtyFlag())
            this.setUse_mail_schedule(srfdomain.getUse_mail_schedule());
        if(srfdomain.getUse_timezoneDirtyFlag())
            this.setUse_timezone(srfdomain.getUse_timezone());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Event_typeDTO> fromDOPage(List<Event_type> poPage)   {
        if(poPage == null)
            return null;
        List<Event_typeDTO> dtos=new ArrayList<Event_typeDTO>();
        for(Event_type domain : poPage) {
            Event_typeDTO dto = new Event_typeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

