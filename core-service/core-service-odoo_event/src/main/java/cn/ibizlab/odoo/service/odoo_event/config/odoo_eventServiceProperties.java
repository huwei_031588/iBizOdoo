package cn.ibizlab.odoo.service.odoo_event.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.event")
@Data
public class odoo_eventServiceProperties {

	private boolean enabled;

	private boolean auth;


}