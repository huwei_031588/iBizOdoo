package cn.ibizlab.odoo.service.odoo_event.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_event.valuerule.anno.event_event_ticket.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event_ticket;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Event_event_ticketDTO]
 */
public class Event_event_ticketDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Event_event_ticketCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Event_event_ticketWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [SEATS_AVAILABLE]
     *
     */
    @Event_event_ticketSeats_availableDefault(info = "默认规则")
    private Integer seats_available;

    @JsonIgnore
    private boolean seats_availableDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Event_event_ticketIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Event_event_ticketNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PRICE_REDUCE_TAXINC]
     *
     */
    @Event_event_ticketPrice_reduce_taxincDefault(info = "默认规则")
    private Double price_reduce_taxinc;

    @JsonIgnore
    private boolean price_reduce_taxincDirtyFlag;

    /**
     * 属性 [PRICE_REDUCE]
     *
     */
    @Event_event_ticketPrice_reduceDefault(info = "默认规则")
    private Double price_reduce;

    @JsonIgnore
    private boolean price_reduceDirtyFlag;

    /**
     * 属性 [REGISTRATION_IDS]
     *
     */
    @Event_event_ticketRegistration_idsDefault(info = "默认规则")
    private String registration_ids;

    @JsonIgnore
    private boolean registration_idsDirtyFlag;

    /**
     * 属性 [SEATS_UNCONFIRMED]
     *
     */
    @Event_event_ticketSeats_unconfirmedDefault(info = "默认规则")
    private Integer seats_unconfirmed;

    @JsonIgnore
    private boolean seats_unconfirmedDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Event_event_ticketDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [SEATS_RESERVED]
     *
     */
    @Event_event_ticketSeats_reservedDefault(info = "默认规则")
    private Integer seats_reserved;

    @JsonIgnore
    private boolean seats_reservedDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Event_event_ticket__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PRICE]
     *
     */
    @Event_event_ticketPriceDefault(info = "默认规则")
    private Double price;

    @JsonIgnore
    private boolean priceDirtyFlag;

    /**
     * 属性 [SEATS_USED]
     *
     */
    @Event_event_ticketSeats_usedDefault(info = "默认规则")
    private Integer seats_used;

    @JsonIgnore
    private boolean seats_usedDirtyFlag;

    /**
     * 属性 [DEADLINE]
     *
     */
    @Event_event_ticketDeadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp deadline;

    @JsonIgnore
    private boolean deadlineDirtyFlag;

    /**
     * 属性 [SEATS_AVAILABILITY]
     *
     */
    @Event_event_ticketSeats_availabilityDefault(info = "默认规则")
    private String seats_availability;

    @JsonIgnore
    private boolean seats_availabilityDirtyFlag;

    /**
     * 属性 [IS_EXPIRED]
     *
     */
    @Event_event_ticketIs_expiredDefault(info = "默认规则")
    private String is_expired;

    @JsonIgnore
    private boolean is_expiredDirtyFlag;

    /**
     * 属性 [SEATS_MAX]
     *
     */
    @Event_event_ticketSeats_maxDefault(info = "默认规则")
    private Integer seats_max;

    @JsonIgnore
    private boolean seats_maxDirtyFlag;

    /**
     * 属性 [EVENT_TYPE_ID_TEXT]
     *
     */
    @Event_event_ticketEvent_type_id_textDefault(info = "默认规则")
    private String event_type_id_text;

    @JsonIgnore
    private boolean event_type_id_textDirtyFlag;

    /**
     * 属性 [EVENT_ID_TEXT]
     *
     */
    @Event_event_ticketEvent_id_textDefault(info = "默认规则")
    private String event_id_text;

    @JsonIgnore
    private boolean event_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Event_event_ticketCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Event_event_ticketWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Event_event_ticketProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Event_event_ticketProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Event_event_ticketCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [EVENT_ID]
     *
     */
    @Event_event_ticketEvent_idDefault(info = "默认规则")
    private Integer event_id;

    @JsonIgnore
    private boolean event_idDirtyFlag;

    /**
     * 属性 [EVENT_TYPE_ID]
     *
     */
    @Event_event_ticketEvent_type_idDefault(info = "默认规则")
    private Integer event_type_id;

    @JsonIgnore
    private boolean event_type_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Event_event_ticketWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [SEATS_AVAILABLE]
     */
    @JsonProperty("seats_available")
    public Integer getSeats_available(){
        return seats_available ;
    }

    /**
     * 设置 [SEATS_AVAILABLE]
     */
    @JsonProperty("seats_available")
    public void setSeats_available(Integer  seats_available){
        this.seats_available = seats_available ;
        this.seats_availableDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_AVAILABLE]脏标记
     */
    @JsonIgnore
    public boolean getSeats_availableDirtyFlag(){
        return seats_availableDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PRICE_REDUCE_TAXINC]
     */
    @JsonProperty("price_reduce_taxinc")
    public Double getPrice_reduce_taxinc(){
        return price_reduce_taxinc ;
    }

    /**
     * 设置 [PRICE_REDUCE_TAXINC]
     */
    @JsonProperty("price_reduce_taxinc")
    public void setPrice_reduce_taxinc(Double  price_reduce_taxinc){
        this.price_reduce_taxinc = price_reduce_taxinc ;
        this.price_reduce_taxincDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_REDUCE_TAXINC]脏标记
     */
    @JsonIgnore
    public boolean getPrice_reduce_taxincDirtyFlag(){
        return price_reduce_taxincDirtyFlag ;
    }

    /**
     * 获取 [PRICE_REDUCE]
     */
    @JsonProperty("price_reduce")
    public Double getPrice_reduce(){
        return price_reduce ;
    }

    /**
     * 设置 [PRICE_REDUCE]
     */
    @JsonProperty("price_reduce")
    public void setPrice_reduce(Double  price_reduce){
        this.price_reduce = price_reduce ;
        this.price_reduceDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_REDUCE]脏标记
     */
    @JsonIgnore
    public boolean getPrice_reduceDirtyFlag(){
        return price_reduceDirtyFlag ;
    }

    /**
     * 获取 [REGISTRATION_IDS]
     */
    @JsonProperty("registration_ids")
    public String getRegistration_ids(){
        return registration_ids ;
    }

    /**
     * 设置 [REGISTRATION_IDS]
     */
    @JsonProperty("registration_ids")
    public void setRegistration_ids(String  registration_ids){
        this.registration_ids = registration_ids ;
        this.registration_idsDirtyFlag = true ;
    }

    /**
     * 获取 [REGISTRATION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRegistration_idsDirtyFlag(){
        return registration_idsDirtyFlag ;
    }

    /**
     * 获取 [SEATS_UNCONFIRMED]
     */
    @JsonProperty("seats_unconfirmed")
    public Integer getSeats_unconfirmed(){
        return seats_unconfirmed ;
    }

    /**
     * 设置 [SEATS_UNCONFIRMED]
     */
    @JsonProperty("seats_unconfirmed")
    public void setSeats_unconfirmed(Integer  seats_unconfirmed){
        this.seats_unconfirmed = seats_unconfirmed ;
        this.seats_unconfirmedDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_UNCONFIRMED]脏标记
     */
    @JsonIgnore
    public boolean getSeats_unconfirmedDirtyFlag(){
        return seats_unconfirmedDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [SEATS_RESERVED]
     */
    @JsonProperty("seats_reserved")
    public Integer getSeats_reserved(){
        return seats_reserved ;
    }

    /**
     * 设置 [SEATS_RESERVED]
     */
    @JsonProperty("seats_reserved")
    public void setSeats_reserved(Integer  seats_reserved){
        this.seats_reserved = seats_reserved ;
        this.seats_reservedDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_RESERVED]脏标记
     */
    @JsonIgnore
    public boolean getSeats_reservedDirtyFlag(){
        return seats_reservedDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PRICE]
     */
    @JsonProperty("price")
    public Double getPrice(){
        return price ;
    }

    /**
     * 设置 [PRICE]
     */
    @JsonProperty("price")
    public void setPrice(Double  price){
        this.price = price ;
        this.priceDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE]脏标记
     */
    @JsonIgnore
    public boolean getPriceDirtyFlag(){
        return priceDirtyFlag ;
    }

    /**
     * 获取 [SEATS_USED]
     */
    @JsonProperty("seats_used")
    public Integer getSeats_used(){
        return seats_used ;
    }

    /**
     * 设置 [SEATS_USED]
     */
    @JsonProperty("seats_used")
    public void setSeats_used(Integer  seats_used){
        this.seats_used = seats_used ;
        this.seats_usedDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_USED]脏标记
     */
    @JsonIgnore
    public boolean getSeats_usedDirtyFlag(){
        return seats_usedDirtyFlag ;
    }

    /**
     * 获取 [DEADLINE]
     */
    @JsonProperty("deadline")
    public Timestamp getDeadline(){
        return deadline ;
    }

    /**
     * 设置 [DEADLINE]
     */
    @JsonProperty("deadline")
    public void setDeadline(Timestamp  deadline){
        this.deadline = deadline ;
        this.deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getDeadlineDirtyFlag(){
        return deadlineDirtyFlag ;
    }

    /**
     * 获取 [SEATS_AVAILABILITY]
     */
    @JsonProperty("seats_availability")
    public String getSeats_availability(){
        return seats_availability ;
    }

    /**
     * 设置 [SEATS_AVAILABILITY]
     */
    @JsonProperty("seats_availability")
    public void setSeats_availability(String  seats_availability){
        this.seats_availability = seats_availability ;
        this.seats_availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getSeats_availabilityDirtyFlag(){
        return seats_availabilityDirtyFlag ;
    }

    /**
     * 获取 [IS_EXPIRED]
     */
    @JsonProperty("is_expired")
    public String getIs_expired(){
        return is_expired ;
    }

    /**
     * 设置 [IS_EXPIRED]
     */
    @JsonProperty("is_expired")
    public void setIs_expired(String  is_expired){
        this.is_expired = is_expired ;
        this.is_expiredDirtyFlag = true ;
    }

    /**
     * 获取 [IS_EXPIRED]脏标记
     */
    @JsonIgnore
    public boolean getIs_expiredDirtyFlag(){
        return is_expiredDirtyFlag ;
    }

    /**
     * 获取 [SEATS_MAX]
     */
    @JsonProperty("seats_max")
    public Integer getSeats_max(){
        return seats_max ;
    }

    /**
     * 设置 [SEATS_MAX]
     */
    @JsonProperty("seats_max")
    public void setSeats_max(Integer  seats_max){
        this.seats_max = seats_max ;
        this.seats_maxDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_MAX]脏标记
     */
    @JsonIgnore
    public boolean getSeats_maxDirtyFlag(){
        return seats_maxDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TYPE_ID_TEXT]
     */
    @JsonProperty("event_type_id_text")
    public String getEvent_type_id_text(){
        return event_type_id_text ;
    }

    /**
     * 设置 [EVENT_TYPE_ID_TEXT]
     */
    @JsonProperty("event_type_id_text")
    public void setEvent_type_id_text(String  event_type_id_text){
        this.event_type_id_text = event_type_id_text ;
        this.event_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEvent_type_id_textDirtyFlag(){
        return event_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [EVENT_ID_TEXT]
     */
    @JsonProperty("event_id_text")
    public String getEvent_id_text(){
        return event_id_text ;
    }

    /**
     * 设置 [EVENT_ID_TEXT]
     */
    @JsonProperty("event_id_text")
    public void setEvent_id_text(String  event_id_text){
        this.event_id_text = event_id_text ;
        this.event_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEvent_id_textDirtyFlag(){
        return event_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [EVENT_ID]
     */
    @JsonProperty("event_id")
    public Integer getEvent_id(){
        return event_id ;
    }

    /**
     * 设置 [EVENT_ID]
     */
    @JsonProperty("event_id")
    public void setEvent_id(Integer  event_id){
        this.event_id = event_id ;
        this.event_idDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getEvent_idDirtyFlag(){
        return event_idDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TYPE_ID]
     */
    @JsonProperty("event_type_id")
    public Integer getEvent_type_id(){
        return event_type_id ;
    }

    /**
     * 设置 [EVENT_TYPE_ID]
     */
    @JsonProperty("event_type_id")
    public void setEvent_type_id(Integer  event_type_id){
        this.event_type_id = event_type_id ;
        this.event_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getEvent_type_idDirtyFlag(){
        return event_type_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Event_event_ticket toDO() {
        Event_event_ticket srfdomain = new Event_event_ticket();
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getSeats_availableDirtyFlag())
            srfdomain.setSeats_available(seats_available);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getPrice_reduce_taxincDirtyFlag())
            srfdomain.setPrice_reduce_taxinc(price_reduce_taxinc);
        if(getPrice_reduceDirtyFlag())
            srfdomain.setPrice_reduce(price_reduce);
        if(getRegistration_idsDirtyFlag())
            srfdomain.setRegistration_ids(registration_ids);
        if(getSeats_unconfirmedDirtyFlag())
            srfdomain.setSeats_unconfirmed(seats_unconfirmed);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getSeats_reservedDirtyFlag())
            srfdomain.setSeats_reserved(seats_reserved);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getPriceDirtyFlag())
            srfdomain.setPrice(price);
        if(getSeats_usedDirtyFlag())
            srfdomain.setSeats_used(seats_used);
        if(getDeadlineDirtyFlag())
            srfdomain.setDeadline(deadline);
        if(getSeats_availabilityDirtyFlag())
            srfdomain.setSeats_availability(seats_availability);
        if(getIs_expiredDirtyFlag())
            srfdomain.setIs_expired(is_expired);
        if(getSeats_maxDirtyFlag())
            srfdomain.setSeats_max(seats_max);
        if(getEvent_type_id_textDirtyFlag())
            srfdomain.setEvent_type_id_text(event_type_id_text);
        if(getEvent_id_textDirtyFlag())
            srfdomain.setEvent_id_text(event_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getEvent_idDirtyFlag())
            srfdomain.setEvent_id(event_id);
        if(getEvent_type_idDirtyFlag())
            srfdomain.setEvent_type_id(event_type_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Event_event_ticket srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getSeats_availableDirtyFlag())
            this.setSeats_available(srfdomain.getSeats_available());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getPrice_reduce_taxincDirtyFlag())
            this.setPrice_reduce_taxinc(srfdomain.getPrice_reduce_taxinc());
        if(srfdomain.getPrice_reduceDirtyFlag())
            this.setPrice_reduce(srfdomain.getPrice_reduce());
        if(srfdomain.getRegistration_idsDirtyFlag())
            this.setRegistration_ids(srfdomain.getRegistration_ids());
        if(srfdomain.getSeats_unconfirmedDirtyFlag())
            this.setSeats_unconfirmed(srfdomain.getSeats_unconfirmed());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getSeats_reservedDirtyFlag())
            this.setSeats_reserved(srfdomain.getSeats_reserved());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getPriceDirtyFlag())
            this.setPrice(srfdomain.getPrice());
        if(srfdomain.getSeats_usedDirtyFlag())
            this.setSeats_used(srfdomain.getSeats_used());
        if(srfdomain.getDeadlineDirtyFlag())
            this.setDeadline(srfdomain.getDeadline());
        if(srfdomain.getSeats_availabilityDirtyFlag())
            this.setSeats_availability(srfdomain.getSeats_availability());
        if(srfdomain.getIs_expiredDirtyFlag())
            this.setIs_expired(srfdomain.getIs_expired());
        if(srfdomain.getSeats_maxDirtyFlag())
            this.setSeats_max(srfdomain.getSeats_max());
        if(srfdomain.getEvent_type_id_textDirtyFlag())
            this.setEvent_type_id_text(srfdomain.getEvent_type_id_text());
        if(srfdomain.getEvent_id_textDirtyFlag())
            this.setEvent_id_text(srfdomain.getEvent_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getEvent_idDirtyFlag())
            this.setEvent_id(srfdomain.getEvent_id());
        if(srfdomain.getEvent_type_idDirtyFlag())
            this.setEvent_type_id(srfdomain.getEvent_type_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Event_event_ticketDTO> fromDOPage(List<Event_event_ticket> poPage)   {
        if(poPage == null)
            return null;
        List<Event_event_ticketDTO> dtos=new ArrayList<Event_event_ticketDTO>();
        for(Event_event_ticket domain : poPage) {
            Event_event_ticketDTO dto = new Event_event_ticketDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

