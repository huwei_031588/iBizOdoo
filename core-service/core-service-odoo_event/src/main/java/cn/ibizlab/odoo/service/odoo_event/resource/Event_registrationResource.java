package cn.ibizlab.odoo.service.odoo_event.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_event.dto.Event_registrationDTO;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_registration;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_registrationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_registrationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Event_registration" })
@RestController
@RequestMapping("")
public class Event_registrationResource {

    @Autowired
    private IEvent_registrationService event_registrationService;

    public IEvent_registrationService getEvent_registrationService() {
        return this.event_registrationService;
    }

    @ApiOperation(value = "建立数据", tags = {"Event_registration" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_registrations")

    public ResponseEntity<Event_registrationDTO> create(@RequestBody Event_registrationDTO event_registrationdto) {
        Event_registrationDTO dto = new Event_registrationDTO();
        Event_registration domain = event_registrationdto.toDO();
		event_registrationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Event_registration" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_registrations/createBatch")
    public ResponseEntity<Boolean> createBatchEvent_registration(@RequestBody List<Event_registrationDTO> event_registrationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Event_registration" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_registrations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Event_registrationDTO> event_registrationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Event_registration" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_registrations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_registrationDTO> event_registrationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Event_registration" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_registrations/{event_registration_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_registration_id") Integer event_registration_id) {
        Event_registrationDTO event_registrationdto = new Event_registrationDTO();
		Event_registration domain = new Event_registration();
		event_registrationdto.setId(event_registration_id);
		domain.setId(event_registration_id);
        Boolean rst = event_registrationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Event_registration" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_registrations/{event_registration_id}")

    public ResponseEntity<Event_registrationDTO> update(@PathVariable("event_registration_id") Integer event_registration_id, @RequestBody Event_registrationDTO event_registrationdto) {
		Event_registration domain = event_registrationdto.toDO();
        domain.setId(event_registration_id);
		event_registrationService.update(domain);
		Event_registrationDTO dto = new Event_registrationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Event_registration" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_registrations/{event_registration_id}")
    public ResponseEntity<Event_registrationDTO> get(@PathVariable("event_registration_id") Integer event_registration_id) {
        Event_registrationDTO dto = new Event_registrationDTO();
        Event_registration domain = event_registrationService.get(event_registration_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Event_registration" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_event/event_registrations/fetchdefault")
	public ResponseEntity<Page<Event_registrationDTO>> fetchDefault(Event_registrationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Event_registrationDTO> list = new ArrayList<Event_registrationDTO>();
        
        Page<Event_registration> domains = event_registrationService.searchDefault(context) ;
        for(Event_registration event_registration : domains.getContent()){
            Event_registrationDTO dto = new Event_registrationDTO();
            dto.fromDO(event_registration);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
