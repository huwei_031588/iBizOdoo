package cn.ibizlab.odoo.service.odoo_event.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_event.dto.Event_mailDTO;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_mailService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mailSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Event_mail" })
@RestController
@RequestMapping("")
public class Event_mailResource {

    @Autowired
    private IEvent_mailService event_mailService;

    public IEvent_mailService getEvent_mailService() {
        return this.event_mailService;
    }

    @ApiOperation(value = "建立数据", tags = {"Event_mail" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_mails")

    public ResponseEntity<Event_mailDTO> create(@RequestBody Event_mailDTO event_maildto) {
        Event_mailDTO dto = new Event_mailDTO();
        Event_mail domain = event_maildto.toDO();
		event_mailService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Event_mail" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_mails/{event_mail_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_mail_id") Integer event_mail_id) {
        Event_mailDTO event_maildto = new Event_mailDTO();
		Event_mail domain = new Event_mail();
		event_maildto.setId(event_mail_id);
		domain.setId(event_mail_id);
        Boolean rst = event_mailService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Event_mail" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_mails/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Event_mailDTO> event_maildtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Event_mail" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_mails/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_mailDTO> event_maildtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Event_mail" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_mails/createBatch")
    public ResponseEntity<Boolean> createBatchEvent_mail(@RequestBody List<Event_mailDTO> event_maildtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Event_mail" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_mails/{event_mail_id}")

    public ResponseEntity<Event_mailDTO> update(@PathVariable("event_mail_id") Integer event_mail_id, @RequestBody Event_mailDTO event_maildto) {
		Event_mail domain = event_maildto.toDO();
        domain.setId(event_mail_id);
		event_mailService.update(domain);
		Event_mailDTO dto = new Event_mailDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Event_mail" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_mails/{event_mail_id}")
    public ResponseEntity<Event_mailDTO> get(@PathVariable("event_mail_id") Integer event_mail_id) {
        Event_mailDTO dto = new Event_mailDTO();
        Event_mail domain = event_mailService.get(event_mail_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Event_mail" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_event/event_mails/fetchdefault")
	public ResponseEntity<Page<Event_mailDTO>> fetchDefault(Event_mailSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Event_mailDTO> list = new ArrayList<Event_mailDTO>();
        
        Page<Event_mail> domains = event_mailService.searchDefault(context) ;
        for(Event_mail event_mail : domains.getContent()){
            Event_mailDTO dto = new Event_mailDTO();
            dto.fromDO(event_mail);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
