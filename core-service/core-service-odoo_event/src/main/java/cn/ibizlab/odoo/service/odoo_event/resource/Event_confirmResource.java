package cn.ibizlab.odoo.service.odoo_event.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_event.dto.Event_confirmDTO;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_confirm;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_confirmService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_confirmSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Event_confirm" })
@RestController
@RequestMapping("")
public class Event_confirmResource {

    @Autowired
    private IEvent_confirmService event_confirmService;

    public IEvent_confirmService getEvent_confirmService() {
        return this.event_confirmService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Event_confirm" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_confirms/createBatch")
    public ResponseEntity<Boolean> createBatchEvent_confirm(@RequestBody List<Event_confirmDTO> event_confirmdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Event_confirm" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_confirms/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_confirmDTO> event_confirmdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Event_confirm" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_confirms/{event_confirm_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_confirm_id") Integer event_confirm_id) {
        Event_confirmDTO event_confirmdto = new Event_confirmDTO();
		Event_confirm domain = new Event_confirm();
		event_confirmdto.setId(event_confirm_id);
		domain.setId(event_confirm_id);
        Boolean rst = event_confirmService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Event_confirm" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_confirms/{event_confirm_id}")

    public ResponseEntity<Event_confirmDTO> update(@PathVariable("event_confirm_id") Integer event_confirm_id, @RequestBody Event_confirmDTO event_confirmdto) {
		Event_confirm domain = event_confirmdto.toDO();
        domain.setId(event_confirm_id);
		event_confirmService.update(domain);
		Event_confirmDTO dto = new Event_confirmDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Event_confirm" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_confirms/{event_confirm_id}")
    public ResponseEntity<Event_confirmDTO> get(@PathVariable("event_confirm_id") Integer event_confirm_id) {
        Event_confirmDTO dto = new Event_confirmDTO();
        Event_confirm domain = event_confirmService.get(event_confirm_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Event_confirm" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_confirms/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Event_confirmDTO> event_confirmdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Event_confirm" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_confirms")

    public ResponseEntity<Event_confirmDTO> create(@RequestBody Event_confirmDTO event_confirmdto) {
        Event_confirmDTO dto = new Event_confirmDTO();
        Event_confirm domain = event_confirmdto.toDO();
		event_confirmService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Event_confirm" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_event/event_confirms/fetchdefault")
	public ResponseEntity<Page<Event_confirmDTO>> fetchDefault(Event_confirmSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Event_confirmDTO> list = new ArrayList<Event_confirmDTO>();
        
        Page<Event_confirm> domains = event_confirmService.searchDefault(context) ;
        for(Event_confirm event_confirm : domains.getContent()){
            Event_confirmDTO dto = new Event_confirmDTO();
            dto.fromDO(event_confirm);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
