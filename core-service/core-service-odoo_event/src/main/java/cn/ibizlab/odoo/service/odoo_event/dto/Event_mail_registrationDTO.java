package cn.ibizlab.odoo.service.odoo_event.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_event.valuerule.anno.event_mail_registration.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail_registration;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Event_mail_registrationDTO]
 */
public class Event_mail_registrationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Event_mail_registrationDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Event_mail_registration__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MAIL_SENT]
     *
     */
    @Event_mail_registrationMail_sentDefault(info = "默认规则")
    private String mail_sent;

    @JsonIgnore
    private boolean mail_sentDirtyFlag;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @Event_mail_registrationScheduled_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp scheduled_date;

    @JsonIgnore
    private boolean scheduled_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Event_mail_registrationCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Event_mail_registrationIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Event_mail_registrationWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Event_mail_registrationWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Event_mail_registrationCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [REGISTRATION_ID_TEXT]
     *
     */
    @Event_mail_registrationRegistration_id_textDefault(info = "默认规则")
    private String registration_id_text;

    @JsonIgnore
    private boolean registration_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Event_mail_registrationWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [REGISTRATION_ID]
     *
     */
    @Event_mail_registrationRegistration_idDefault(info = "默认规则")
    private Integer registration_id;

    @JsonIgnore
    private boolean registration_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Event_mail_registrationCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [SCHEDULER_ID]
     *
     */
    @Event_mail_registrationScheduler_idDefault(info = "默认规则")
    private Integer scheduler_id;

    @JsonIgnore
    private boolean scheduler_idDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MAIL_SENT]
     */
    @JsonProperty("mail_sent")
    public String getMail_sent(){
        return mail_sent ;
    }

    /**
     * 设置 [MAIL_SENT]
     */
    @JsonProperty("mail_sent")
    public void setMail_sent(String  mail_sent){
        this.mail_sent = mail_sent ;
        this.mail_sentDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_SENT]脏标记
     */
    @JsonIgnore
    public boolean getMail_sentDirtyFlag(){
        return mail_sentDirtyFlag ;
    }

    /**
     * 获取 [SCHEDULED_DATE]
     */
    @JsonProperty("scheduled_date")
    public Timestamp getScheduled_date(){
        return scheduled_date ;
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(Timestamp  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULED_DATE]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return scheduled_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [REGISTRATION_ID_TEXT]
     */
    @JsonProperty("registration_id_text")
    public String getRegistration_id_text(){
        return registration_id_text ;
    }

    /**
     * 设置 [REGISTRATION_ID_TEXT]
     */
    @JsonProperty("registration_id_text")
    public void setRegistration_id_text(String  registration_id_text){
        this.registration_id_text = registration_id_text ;
        this.registration_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REGISTRATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRegistration_id_textDirtyFlag(){
        return registration_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [REGISTRATION_ID]
     */
    @JsonProperty("registration_id")
    public Integer getRegistration_id(){
        return registration_id ;
    }

    /**
     * 设置 [REGISTRATION_ID]
     */
    @JsonProperty("registration_id")
    public void setRegistration_id(Integer  registration_id){
        this.registration_id = registration_id ;
        this.registration_idDirtyFlag = true ;
    }

    /**
     * 获取 [REGISTRATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getRegistration_idDirtyFlag(){
        return registration_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [SCHEDULER_ID]
     */
    @JsonProperty("scheduler_id")
    public Integer getScheduler_id(){
        return scheduler_id ;
    }

    /**
     * 设置 [SCHEDULER_ID]
     */
    @JsonProperty("scheduler_id")
    public void setScheduler_id(Integer  scheduler_id){
        this.scheduler_id = scheduler_id ;
        this.scheduler_idDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULER_ID]脏标记
     */
    @JsonIgnore
    public boolean getScheduler_idDirtyFlag(){
        return scheduler_idDirtyFlag ;
    }



    public Event_mail_registration toDO() {
        Event_mail_registration srfdomain = new Event_mail_registration();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMail_sentDirtyFlag())
            srfdomain.setMail_sent(mail_sent);
        if(getScheduled_dateDirtyFlag())
            srfdomain.setScheduled_date(scheduled_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getRegistration_id_textDirtyFlag())
            srfdomain.setRegistration_id_text(registration_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getRegistration_idDirtyFlag())
            srfdomain.setRegistration_id(registration_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getScheduler_idDirtyFlag())
            srfdomain.setScheduler_id(scheduler_id);

        return srfdomain;
    }

    public void fromDO(Event_mail_registration srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMail_sentDirtyFlag())
            this.setMail_sent(srfdomain.getMail_sent());
        if(srfdomain.getScheduled_dateDirtyFlag())
            this.setScheduled_date(srfdomain.getScheduled_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getRegistration_id_textDirtyFlag())
            this.setRegistration_id_text(srfdomain.getRegistration_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getRegistration_idDirtyFlag())
            this.setRegistration_id(srfdomain.getRegistration_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getScheduler_idDirtyFlag())
            this.setScheduler_id(srfdomain.getScheduler_id());

    }

    public List<Event_mail_registrationDTO> fromDOPage(List<Event_mail_registration> poPage)   {
        if(poPage == null)
            return null;
        List<Event_mail_registrationDTO> dtos=new ArrayList<Event_mail_registrationDTO>();
        for(Event_mail_registration domain : poPage) {
            Event_mail_registrationDTO dto = new Event_mail_registrationDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

