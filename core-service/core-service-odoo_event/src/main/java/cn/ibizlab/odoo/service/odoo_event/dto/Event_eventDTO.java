package cn.ibizlab.odoo.service.odoo_event.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_event.valuerule.anno.event_event.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Event_eventDTO]
 */
public class Event_eventDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SEATS_USED]
     *
     */
    @Event_eventSeats_usedDefault(info = "默认规则")
    private Integer seats_used;

    @JsonIgnore
    private boolean seats_usedDirtyFlag;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @Event_eventIs_seo_optimizedDefault(info = "默认规则")
    private String is_seo_optimized;

    @JsonIgnore
    private boolean is_seo_optimizedDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Event_eventIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [BADGE_FRONT]
     *
     */
    @Event_eventBadge_frontDefault(info = "默认规则")
    private String badge_front;

    @JsonIgnore
    private boolean badge_frontDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Event_eventStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [EVENT_MAIL_IDS]
     *
     */
    @Event_eventEvent_mail_idsDefault(info = "默认规则")
    private String event_mail_ids;

    @JsonIgnore
    private boolean event_mail_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Event_eventCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [SEATS_EXPECTED]
     *
     */
    @Event_eventSeats_expectedDefault(info = "默认规则")
    private Integer seats_expected;

    @JsonIgnore
    private boolean seats_expectedDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Event_eventMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Event_eventMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [SEATS_AVAILABILITY]
     *
     */
    @Event_eventSeats_availabilityDefault(info = "默认规则")
    private String seats_availability;

    @JsonIgnore
    private boolean seats_availabilityDirtyFlag;

    /**
     * 属性 [EVENT_TICKET_IDS]
     *
     */
    @Event_eventEvent_ticket_idsDefault(info = "默认规则")
    private String event_ticket_ids;

    @JsonIgnore
    private boolean event_ticket_idsDirtyFlag;

    /**
     * 属性 [SEATS_UNCONFIRMED]
     *
     */
    @Event_eventSeats_unconfirmedDefault(info = "默认规则")
    private Integer seats_unconfirmed;

    @JsonIgnore
    private boolean seats_unconfirmedDirtyFlag;

    /**
     * 属性 [IS_PARTICIPATING]
     *
     */
    @Event_eventIs_participatingDefault(info = "默认规则")
    private String is_participating;

    @JsonIgnore
    private boolean is_participatingDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Event_eventMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [TWITTER_HASHTAG]
     *
     */
    @Event_eventTwitter_hashtagDefault(info = "默认规则")
    private String twitter_hashtag;

    @JsonIgnore
    private boolean twitter_hashtagDirtyFlag;

    /**
     * 属性 [SEATS_MAX]
     *
     */
    @Event_eventSeats_maxDefault(info = "默认规则")
    private Integer seats_max;

    @JsonIgnore
    private boolean seats_maxDirtyFlag;

    /**
     * 属性 [DATE_BEGIN]
     *
     */
    @Event_eventDate_beginDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_begin;

    @JsonIgnore
    private boolean date_beginDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Event_eventMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @Event_eventWebsite_meta_og_imgDefault(info = "默认规则")
    private String website_meta_og_img;

    @JsonIgnore
    private boolean website_meta_og_imgDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Event_eventWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Event_eventMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @Event_eventWebsite_meta_titleDefault(info = "默认规则")
    private String website_meta_title;

    @JsonIgnore
    private boolean website_meta_titleDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Event_eventMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [REGISTRATION_IDS]
     *
     */
    @Event_eventRegistration_idsDefault(info = "默认规则")
    private String registration_ids;

    @JsonIgnore
    private boolean registration_idsDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Event_eventDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [BADGE_INNERRIGHT]
     *
     */
    @Event_eventBadge_innerrightDefault(info = "默认规则")
    private String badge_innerright;

    @JsonIgnore
    private boolean badge_innerrightDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Event_eventMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @Event_eventWebsite_meta_descriptionDefault(info = "默认规则")
    private String website_meta_description;

    @JsonIgnore
    private boolean website_meta_descriptionDirtyFlag;

    /**
     * 属性 [SEATS_MIN]
     *
     */
    @Event_eventSeats_minDefault(info = "默认规则")
    private Integer seats_min;

    @JsonIgnore
    private boolean seats_minDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Event_eventMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [BADGE_BACK]
     *
     */
    @Event_eventBadge_backDefault(info = "默认规则")
    private String badge_back;

    @JsonIgnore
    private boolean badge_backDirtyFlag;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @Event_eventWebsite_meta_keywordsDefault(info = "默认规则")
    private String website_meta_keywords;

    @JsonIgnore
    private boolean website_meta_keywordsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Event_eventMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [AUTO_CONFIRM]
     *
     */
    @Event_eventAuto_confirmDefault(info = "默认规则")
    private String auto_confirm;

    @JsonIgnore
    private boolean auto_confirmDirtyFlag;

    /**
     * 属性 [DATE_END]
     *
     */
    @Event_eventDate_endDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_end;

    @JsonIgnore
    private boolean date_endDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Event_eventNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [BADGE_INNERLEFT]
     *
     */
    @Event_eventBadge_innerleftDefault(info = "默认规则")
    private String badge_innerleft;

    @JsonIgnore
    private boolean badge_innerleftDirtyFlag;

    /**
     * 属性 [SEATS_RESERVED]
     *
     */
    @Event_eventSeats_reservedDefault(info = "默认规则")
    private Integer seats_reserved;

    @JsonIgnore
    private boolean seats_reservedDirtyFlag;

    /**
     * 属性 [WEBSITE_MENU]
     *
     */
    @Event_eventWebsite_menuDefault(info = "默认规则")
    private String website_menu;

    @JsonIgnore
    private boolean website_menuDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Event_eventMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Event_eventWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Event_eventActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [DATE_END_LOCATED]
     *
     */
    @Event_eventDate_end_locatedDefault(info = "默认规则")
    private String date_end_located;

    @JsonIgnore
    private boolean date_end_locatedDirtyFlag;

    /**
     * 属性 [SEATS_AVAILABLE]
     *
     */
    @Event_eventSeats_availableDefault(info = "默认规则")
    private Integer seats_available;

    @JsonIgnore
    private boolean seats_availableDirtyFlag;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @Event_eventIs_publishedDefault(info = "默认规则")
    private String is_published;

    @JsonIgnore
    private boolean is_publishedDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Event_eventMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [IS_ONLINE]
     *
     */
    @Event_eventIs_onlineDefault(info = "默认规则")
    private String is_online;

    @JsonIgnore
    private boolean is_onlineDirtyFlag;

    /**
     * 属性 [DATE_TZ]
     *
     */
    @Event_eventDate_tzDefault(info = "默认规则")
    private String date_tz;

    @JsonIgnore
    private boolean date_tzDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Event_eventDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Event_eventMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Event_event__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Event_eventMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Event_eventWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Event_eventWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [DATE_BEGIN_LOCATED]
     *
     */
    @Event_eventDate_begin_locatedDefault(info = "默认规则")
    private String date_begin_located;

    @JsonIgnore
    private boolean date_begin_locatedDirtyFlag;

    /**
     * 属性 [EVENT_LOGO]
     *
     */
    @Event_eventEvent_logoDefault(info = "默认规则")
    private String event_logo;

    @JsonIgnore
    private boolean event_logoDirtyFlag;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @Event_eventWebsite_urlDefault(info = "默认规则")
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Event_eventColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Event_eventWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [EVENT_TYPE_ID_TEXT]
     *
     */
    @Event_eventEvent_type_id_textDefault(info = "默认规则")
    private String event_type_id_text;

    @JsonIgnore
    private boolean event_type_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Event_eventCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ADDRESS_ID_TEXT]
     *
     */
    @Event_eventAddress_id_textDefault(info = "默认规则")
    private String address_id_text;

    @JsonIgnore
    private boolean address_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @Event_eventCountry_id_textDefault(info = "默认规则")
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;

    /**
     * 属性 [MENU_ID_TEXT]
     *
     */
    @Event_eventMenu_id_textDefault(info = "默认规则")
    private String menu_id_text;

    @JsonIgnore
    private boolean menu_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Event_eventCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Event_eventUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [ORGANIZER_ID_TEXT]
     *
     */
    @Event_eventOrganizer_id_textDefault(info = "默认规则")
    private String organizer_id_text;

    @JsonIgnore
    private boolean organizer_id_textDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Event_eventUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Event_eventCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Event_eventCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [ORGANIZER_ID]
     *
     */
    @Event_eventOrganizer_idDefault(info = "默认规则")
    private Integer organizer_id;

    @JsonIgnore
    private boolean organizer_idDirtyFlag;

    /**
     * 属性 [MENU_ID]
     *
     */
    @Event_eventMenu_idDefault(info = "默认规则")
    private Integer menu_id;

    @JsonIgnore
    private boolean menu_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Event_eventCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [ADDRESS_ID]
     *
     */
    @Event_eventAddress_idDefault(info = "默认规则")
    private Integer address_id;

    @JsonIgnore
    private boolean address_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Event_eventWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [EVENT_TYPE_ID]
     *
     */
    @Event_eventEvent_type_idDefault(info = "默认规则")
    private Integer event_type_id;

    @JsonIgnore
    private boolean event_type_idDirtyFlag;


    /**
     * 获取 [SEATS_USED]
     */
    @JsonProperty("seats_used")
    public Integer getSeats_used(){
        return seats_used ;
    }

    /**
     * 设置 [SEATS_USED]
     */
    @JsonProperty("seats_used")
    public void setSeats_used(Integer  seats_used){
        this.seats_used = seats_used ;
        this.seats_usedDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_USED]脏标记
     */
    @JsonIgnore
    public boolean getSeats_usedDirtyFlag(){
        return seats_usedDirtyFlag ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return is_seo_optimized ;
    }

    /**
     * 设置 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return is_seo_optimizedDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [BADGE_FRONT]
     */
    @JsonProperty("badge_front")
    public String getBadge_front(){
        return badge_front ;
    }

    /**
     * 设置 [BADGE_FRONT]
     */
    @JsonProperty("badge_front")
    public void setBadge_front(String  badge_front){
        this.badge_front = badge_front ;
        this.badge_frontDirtyFlag = true ;
    }

    /**
     * 获取 [BADGE_FRONT]脏标记
     */
    @JsonIgnore
    public boolean getBadge_frontDirtyFlag(){
        return badge_frontDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [EVENT_MAIL_IDS]
     */
    @JsonProperty("event_mail_ids")
    public String getEvent_mail_ids(){
        return event_mail_ids ;
    }

    /**
     * 设置 [EVENT_MAIL_IDS]
     */
    @JsonProperty("event_mail_ids")
    public void setEvent_mail_ids(String  event_mail_ids){
        this.event_mail_ids = event_mail_ids ;
        this.event_mail_idsDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_MAIL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getEvent_mail_idsDirtyFlag(){
        return event_mail_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [SEATS_EXPECTED]
     */
    @JsonProperty("seats_expected")
    public Integer getSeats_expected(){
        return seats_expected ;
    }

    /**
     * 设置 [SEATS_EXPECTED]
     */
    @JsonProperty("seats_expected")
    public void setSeats_expected(Integer  seats_expected){
        this.seats_expected = seats_expected ;
        this.seats_expectedDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_EXPECTED]脏标记
     */
    @JsonIgnore
    public boolean getSeats_expectedDirtyFlag(){
        return seats_expectedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [SEATS_AVAILABILITY]
     */
    @JsonProperty("seats_availability")
    public String getSeats_availability(){
        return seats_availability ;
    }

    /**
     * 设置 [SEATS_AVAILABILITY]
     */
    @JsonProperty("seats_availability")
    public void setSeats_availability(String  seats_availability){
        this.seats_availability = seats_availability ;
        this.seats_availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getSeats_availabilityDirtyFlag(){
        return seats_availabilityDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TICKET_IDS]
     */
    @JsonProperty("event_ticket_ids")
    public String getEvent_ticket_ids(){
        return event_ticket_ids ;
    }

    /**
     * 设置 [EVENT_TICKET_IDS]
     */
    @JsonProperty("event_ticket_ids")
    public void setEvent_ticket_ids(String  event_ticket_ids){
        this.event_ticket_ids = event_ticket_ids ;
        this.event_ticket_idsDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TICKET_IDS]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_idsDirtyFlag(){
        return event_ticket_idsDirtyFlag ;
    }

    /**
     * 获取 [SEATS_UNCONFIRMED]
     */
    @JsonProperty("seats_unconfirmed")
    public Integer getSeats_unconfirmed(){
        return seats_unconfirmed ;
    }

    /**
     * 设置 [SEATS_UNCONFIRMED]
     */
    @JsonProperty("seats_unconfirmed")
    public void setSeats_unconfirmed(Integer  seats_unconfirmed){
        this.seats_unconfirmed = seats_unconfirmed ;
        this.seats_unconfirmedDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_UNCONFIRMED]脏标记
     */
    @JsonIgnore
    public boolean getSeats_unconfirmedDirtyFlag(){
        return seats_unconfirmedDirtyFlag ;
    }

    /**
     * 获取 [IS_PARTICIPATING]
     */
    @JsonProperty("is_participating")
    public String getIs_participating(){
        return is_participating ;
    }

    /**
     * 设置 [IS_PARTICIPATING]
     */
    @JsonProperty("is_participating")
    public void setIs_participating(String  is_participating){
        this.is_participating = is_participating ;
        this.is_participatingDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PARTICIPATING]脏标记
     */
    @JsonIgnore
    public boolean getIs_participatingDirtyFlag(){
        return is_participatingDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [TWITTER_HASHTAG]
     */
    @JsonProperty("twitter_hashtag")
    public String getTwitter_hashtag(){
        return twitter_hashtag ;
    }

    /**
     * 设置 [TWITTER_HASHTAG]
     */
    @JsonProperty("twitter_hashtag")
    public void setTwitter_hashtag(String  twitter_hashtag){
        this.twitter_hashtag = twitter_hashtag ;
        this.twitter_hashtagDirtyFlag = true ;
    }

    /**
     * 获取 [TWITTER_HASHTAG]脏标记
     */
    @JsonIgnore
    public boolean getTwitter_hashtagDirtyFlag(){
        return twitter_hashtagDirtyFlag ;
    }

    /**
     * 获取 [SEATS_MAX]
     */
    @JsonProperty("seats_max")
    public Integer getSeats_max(){
        return seats_max ;
    }

    /**
     * 设置 [SEATS_MAX]
     */
    @JsonProperty("seats_max")
    public void setSeats_max(Integer  seats_max){
        this.seats_max = seats_max ;
        this.seats_maxDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_MAX]脏标记
     */
    @JsonIgnore
    public boolean getSeats_maxDirtyFlag(){
        return seats_maxDirtyFlag ;
    }

    /**
     * 获取 [DATE_BEGIN]
     */
    @JsonProperty("date_begin")
    public Timestamp getDate_begin(){
        return date_begin ;
    }

    /**
     * 设置 [DATE_BEGIN]
     */
    @JsonProperty("date_begin")
    public void setDate_begin(Timestamp  date_begin){
        this.date_begin = date_begin ;
        this.date_beginDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_BEGIN]脏标记
     */
    @JsonIgnore
    public boolean getDate_beginDirtyFlag(){
        return date_beginDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return website_meta_og_img ;
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return website_meta_og_imgDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return website_meta_title ;
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return website_meta_titleDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [REGISTRATION_IDS]
     */
    @JsonProperty("registration_ids")
    public String getRegistration_ids(){
        return registration_ids ;
    }

    /**
     * 设置 [REGISTRATION_IDS]
     */
    @JsonProperty("registration_ids")
    public void setRegistration_ids(String  registration_ids){
        this.registration_ids = registration_ids ;
        this.registration_idsDirtyFlag = true ;
    }

    /**
     * 获取 [REGISTRATION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRegistration_idsDirtyFlag(){
        return registration_idsDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [BADGE_INNERRIGHT]
     */
    @JsonProperty("badge_innerright")
    public String getBadge_innerright(){
        return badge_innerright ;
    }

    /**
     * 设置 [BADGE_INNERRIGHT]
     */
    @JsonProperty("badge_innerright")
    public void setBadge_innerright(String  badge_innerright){
        this.badge_innerright = badge_innerright ;
        this.badge_innerrightDirtyFlag = true ;
    }

    /**
     * 获取 [BADGE_INNERRIGHT]脏标记
     */
    @JsonIgnore
    public boolean getBadge_innerrightDirtyFlag(){
        return badge_innerrightDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return website_meta_description ;
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return website_meta_descriptionDirtyFlag ;
    }

    /**
     * 获取 [SEATS_MIN]
     */
    @JsonProperty("seats_min")
    public Integer getSeats_min(){
        return seats_min ;
    }

    /**
     * 设置 [SEATS_MIN]
     */
    @JsonProperty("seats_min")
    public void setSeats_min(Integer  seats_min){
        this.seats_min = seats_min ;
        this.seats_minDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_MIN]脏标记
     */
    @JsonIgnore
    public boolean getSeats_minDirtyFlag(){
        return seats_minDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [BADGE_BACK]
     */
    @JsonProperty("badge_back")
    public String getBadge_back(){
        return badge_back ;
    }

    /**
     * 设置 [BADGE_BACK]
     */
    @JsonProperty("badge_back")
    public void setBadge_back(String  badge_back){
        this.badge_back = badge_back ;
        this.badge_backDirtyFlag = true ;
    }

    /**
     * 获取 [BADGE_BACK]脏标记
     */
    @JsonIgnore
    public boolean getBadge_backDirtyFlag(){
        return badge_backDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return website_meta_keywords ;
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return website_meta_keywordsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [AUTO_CONFIRM]
     */
    @JsonProperty("auto_confirm")
    public String getAuto_confirm(){
        return auto_confirm ;
    }

    /**
     * 设置 [AUTO_CONFIRM]
     */
    @JsonProperty("auto_confirm")
    public void setAuto_confirm(String  auto_confirm){
        this.auto_confirm = auto_confirm ;
        this.auto_confirmDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_CONFIRM]脏标记
     */
    @JsonIgnore
    public boolean getAuto_confirmDirtyFlag(){
        return auto_confirmDirtyFlag ;
    }

    /**
     * 获取 [DATE_END]
     */
    @JsonProperty("date_end")
    public Timestamp getDate_end(){
        return date_end ;
    }

    /**
     * 设置 [DATE_END]
     */
    @JsonProperty("date_end")
    public void setDate_end(Timestamp  date_end){
        this.date_end = date_end ;
        this.date_endDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_END]脏标记
     */
    @JsonIgnore
    public boolean getDate_endDirtyFlag(){
        return date_endDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [BADGE_INNERLEFT]
     */
    @JsonProperty("badge_innerleft")
    public String getBadge_innerleft(){
        return badge_innerleft ;
    }

    /**
     * 设置 [BADGE_INNERLEFT]
     */
    @JsonProperty("badge_innerleft")
    public void setBadge_innerleft(String  badge_innerleft){
        this.badge_innerleft = badge_innerleft ;
        this.badge_innerleftDirtyFlag = true ;
    }

    /**
     * 获取 [BADGE_INNERLEFT]脏标记
     */
    @JsonIgnore
    public boolean getBadge_innerleftDirtyFlag(){
        return badge_innerleftDirtyFlag ;
    }

    /**
     * 获取 [SEATS_RESERVED]
     */
    @JsonProperty("seats_reserved")
    public Integer getSeats_reserved(){
        return seats_reserved ;
    }

    /**
     * 设置 [SEATS_RESERVED]
     */
    @JsonProperty("seats_reserved")
    public void setSeats_reserved(Integer  seats_reserved){
        this.seats_reserved = seats_reserved ;
        this.seats_reservedDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_RESERVED]脏标记
     */
    @JsonIgnore
    public boolean getSeats_reservedDirtyFlag(){
        return seats_reservedDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MENU]
     */
    @JsonProperty("website_menu")
    public String getWebsite_menu(){
        return website_menu ;
    }

    /**
     * 设置 [WEBSITE_MENU]
     */
    @JsonProperty("website_menu")
    public void setWebsite_menu(String  website_menu){
        this.website_menu = website_menu ;
        this.website_menuDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MENU]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_menuDirtyFlag(){
        return website_menuDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [DATE_END_LOCATED]
     */
    @JsonProperty("date_end_located")
    public String getDate_end_located(){
        return date_end_located ;
    }

    /**
     * 设置 [DATE_END_LOCATED]
     */
    @JsonProperty("date_end_located")
    public void setDate_end_located(String  date_end_located){
        this.date_end_located = date_end_located ;
        this.date_end_locatedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_END_LOCATED]脏标记
     */
    @JsonIgnore
    public boolean getDate_end_locatedDirtyFlag(){
        return date_end_locatedDirtyFlag ;
    }

    /**
     * 获取 [SEATS_AVAILABLE]
     */
    @JsonProperty("seats_available")
    public Integer getSeats_available(){
        return seats_available ;
    }

    /**
     * 设置 [SEATS_AVAILABLE]
     */
    @JsonProperty("seats_available")
    public void setSeats_available(Integer  seats_available){
        this.seats_available = seats_available ;
        this.seats_availableDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS_AVAILABLE]脏标记
     */
    @JsonIgnore
    public boolean getSeats_availableDirtyFlag(){
        return seats_availableDirtyFlag ;
    }

    /**
     * 获取 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return is_published ;
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return is_publishedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [IS_ONLINE]
     */
    @JsonProperty("is_online")
    public String getIs_online(){
        return is_online ;
    }

    /**
     * 设置 [IS_ONLINE]
     */
    @JsonProperty("is_online")
    public void setIs_online(String  is_online){
        this.is_online = is_online ;
        this.is_onlineDirtyFlag = true ;
    }

    /**
     * 获取 [IS_ONLINE]脏标记
     */
    @JsonIgnore
    public boolean getIs_onlineDirtyFlag(){
        return is_onlineDirtyFlag ;
    }

    /**
     * 获取 [DATE_TZ]
     */
    @JsonProperty("date_tz")
    public String getDate_tz(){
        return date_tz ;
    }

    /**
     * 设置 [DATE_TZ]
     */
    @JsonProperty("date_tz")
    public void setDate_tz(String  date_tz){
        this.date_tz = date_tz ;
        this.date_tzDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_TZ]脏标记
     */
    @JsonIgnore
    public boolean getDate_tzDirtyFlag(){
        return date_tzDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [DATE_BEGIN_LOCATED]
     */
    @JsonProperty("date_begin_located")
    public String getDate_begin_located(){
        return date_begin_located ;
    }

    /**
     * 设置 [DATE_BEGIN_LOCATED]
     */
    @JsonProperty("date_begin_located")
    public void setDate_begin_located(String  date_begin_located){
        this.date_begin_located = date_begin_located ;
        this.date_begin_locatedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_BEGIN_LOCATED]脏标记
     */
    @JsonIgnore
    public boolean getDate_begin_locatedDirtyFlag(){
        return date_begin_locatedDirtyFlag ;
    }

    /**
     * 获取 [EVENT_LOGO]
     */
    @JsonProperty("event_logo")
    public String getEvent_logo(){
        return event_logo ;
    }

    /**
     * 设置 [EVENT_LOGO]
     */
    @JsonProperty("event_logo")
    public void setEvent_logo(String  event_logo){
        this.event_logo = event_logo ;
        this.event_logoDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_LOGO]脏标记
     */
    @JsonIgnore
    public boolean getEvent_logoDirtyFlag(){
        return event_logoDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return website_url ;
    }

    /**
     * 设置 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_URL]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return website_urlDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TYPE_ID_TEXT]
     */
    @JsonProperty("event_type_id_text")
    public String getEvent_type_id_text(){
        return event_type_id_text ;
    }

    /**
     * 设置 [EVENT_TYPE_ID_TEXT]
     */
    @JsonProperty("event_type_id_text")
    public void setEvent_type_id_text(String  event_type_id_text){
        this.event_type_id_text = event_type_id_text ;
        this.event_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEvent_type_id_textDirtyFlag(){
        return event_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_ID_TEXT]
     */
    @JsonProperty("address_id_text")
    public String getAddress_id_text(){
        return address_id_text ;
    }

    /**
     * 设置 [ADDRESS_ID_TEXT]
     */
    @JsonProperty("address_id_text")
    public void setAddress_id_text(String  address_id_text){
        this.address_id_text = address_id_text ;
        this.address_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAddress_id_textDirtyFlag(){
        return address_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return country_id_text ;
    }

    /**
     * 设置 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return country_id_textDirtyFlag ;
    }

    /**
     * 获取 [MENU_ID_TEXT]
     */
    @JsonProperty("menu_id_text")
    public String getMenu_id_text(){
        return menu_id_text ;
    }

    /**
     * 设置 [MENU_ID_TEXT]
     */
    @JsonProperty("menu_id_text")
    public void setMenu_id_text(String  menu_id_text){
        this.menu_id_text = menu_id_text ;
        this.menu_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MENU_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMenu_id_textDirtyFlag(){
        return menu_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [ORGANIZER_ID_TEXT]
     */
    @JsonProperty("organizer_id_text")
    public String getOrganizer_id_text(){
        return organizer_id_text ;
    }

    /**
     * 设置 [ORGANIZER_ID_TEXT]
     */
    @JsonProperty("organizer_id_text")
    public void setOrganizer_id_text(String  organizer_id_text){
        this.organizer_id_text = organizer_id_text ;
        this.organizer_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ORGANIZER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOrganizer_id_textDirtyFlag(){
        return organizer_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [ORGANIZER_ID]
     */
    @JsonProperty("organizer_id")
    public Integer getOrganizer_id(){
        return organizer_id ;
    }

    /**
     * 设置 [ORGANIZER_ID]
     */
    @JsonProperty("organizer_id")
    public void setOrganizer_id(Integer  organizer_id){
        this.organizer_id = organizer_id ;
        this.organizer_idDirtyFlag = true ;
    }

    /**
     * 获取 [ORGANIZER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOrganizer_idDirtyFlag(){
        return organizer_idDirtyFlag ;
    }

    /**
     * 获取 [MENU_ID]
     */
    @JsonProperty("menu_id")
    public Integer getMenu_id(){
        return menu_id ;
    }

    /**
     * 设置 [MENU_ID]
     */
    @JsonProperty("menu_id")
    public void setMenu_id(Integer  menu_id){
        this.menu_id = menu_id ;
        this.menu_idDirtyFlag = true ;
    }

    /**
     * 获取 [MENU_ID]脏标记
     */
    @JsonIgnore
    public boolean getMenu_idDirtyFlag(){
        return menu_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_ID]
     */
    @JsonProperty("address_id")
    public Integer getAddress_id(){
        return address_id ;
    }

    /**
     * 设置 [ADDRESS_ID]
     */
    @JsonProperty("address_id")
    public void setAddress_id(Integer  address_id){
        this.address_id = address_id ;
        this.address_idDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_ID]脏标记
     */
    @JsonIgnore
    public boolean getAddress_idDirtyFlag(){
        return address_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TYPE_ID]
     */
    @JsonProperty("event_type_id")
    public Integer getEvent_type_id(){
        return event_type_id ;
    }

    /**
     * 设置 [EVENT_TYPE_ID]
     */
    @JsonProperty("event_type_id")
    public void setEvent_type_id(Integer  event_type_id){
        this.event_type_id = event_type_id ;
        this.event_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getEvent_type_idDirtyFlag(){
        return event_type_idDirtyFlag ;
    }



    public Event_event toDO() {
        Event_event srfdomain = new Event_event();
        if(getSeats_usedDirtyFlag())
            srfdomain.setSeats_used(seats_used);
        if(getIs_seo_optimizedDirtyFlag())
            srfdomain.setIs_seo_optimized(is_seo_optimized);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getBadge_frontDirtyFlag())
            srfdomain.setBadge_front(badge_front);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getEvent_mail_idsDirtyFlag())
            srfdomain.setEvent_mail_ids(event_mail_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getSeats_expectedDirtyFlag())
            srfdomain.setSeats_expected(seats_expected);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getSeats_availabilityDirtyFlag())
            srfdomain.setSeats_availability(seats_availability);
        if(getEvent_ticket_idsDirtyFlag())
            srfdomain.setEvent_ticket_ids(event_ticket_ids);
        if(getSeats_unconfirmedDirtyFlag())
            srfdomain.setSeats_unconfirmed(seats_unconfirmed);
        if(getIs_participatingDirtyFlag())
            srfdomain.setIs_participating(is_participating);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getTwitter_hashtagDirtyFlag())
            srfdomain.setTwitter_hashtag(twitter_hashtag);
        if(getSeats_maxDirtyFlag())
            srfdomain.setSeats_max(seats_max);
        if(getDate_beginDirtyFlag())
            srfdomain.setDate_begin(date_begin);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getWebsite_meta_og_imgDirtyFlag())
            srfdomain.setWebsite_meta_og_img(website_meta_og_img);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getWebsite_meta_titleDirtyFlag())
            srfdomain.setWebsite_meta_title(website_meta_title);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getRegistration_idsDirtyFlag())
            srfdomain.setRegistration_ids(registration_ids);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getBadge_innerrightDirtyFlag())
            srfdomain.setBadge_innerright(badge_innerright);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getWebsite_meta_descriptionDirtyFlag())
            srfdomain.setWebsite_meta_description(website_meta_description);
        if(getSeats_minDirtyFlag())
            srfdomain.setSeats_min(seats_min);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getBadge_backDirtyFlag())
            srfdomain.setBadge_back(badge_back);
        if(getWebsite_meta_keywordsDirtyFlag())
            srfdomain.setWebsite_meta_keywords(website_meta_keywords);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getAuto_confirmDirtyFlag())
            srfdomain.setAuto_confirm(auto_confirm);
        if(getDate_endDirtyFlag())
            srfdomain.setDate_end(date_end);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getBadge_innerleftDirtyFlag())
            srfdomain.setBadge_innerleft(badge_innerleft);
        if(getSeats_reservedDirtyFlag())
            srfdomain.setSeats_reserved(seats_reserved);
        if(getWebsite_menuDirtyFlag())
            srfdomain.setWebsite_menu(website_menu);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getDate_end_locatedDirtyFlag())
            srfdomain.setDate_end_located(date_end_located);
        if(getSeats_availableDirtyFlag())
            srfdomain.setSeats_available(seats_available);
        if(getIs_publishedDirtyFlag())
            srfdomain.setIs_published(is_published);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getIs_onlineDirtyFlag())
            srfdomain.setIs_online(is_online);
        if(getDate_tzDirtyFlag())
            srfdomain.setDate_tz(date_tz);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getDate_begin_locatedDirtyFlag())
            srfdomain.setDate_begin_located(date_begin_located);
        if(getEvent_logoDirtyFlag())
            srfdomain.setEvent_logo(event_logo);
        if(getWebsite_urlDirtyFlag())
            srfdomain.setWebsite_url(website_url);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getEvent_type_id_textDirtyFlag())
            srfdomain.setEvent_type_id_text(event_type_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getAddress_id_textDirtyFlag())
            srfdomain.setAddress_id_text(address_id_text);
        if(getCountry_id_textDirtyFlag())
            srfdomain.setCountry_id_text(country_id_text);
        if(getMenu_id_textDirtyFlag())
            srfdomain.setMenu_id_text(menu_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getOrganizer_id_textDirtyFlag())
            srfdomain.setOrganizer_id_text(organizer_id_text);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getOrganizer_idDirtyFlag())
            srfdomain.setOrganizer_id(organizer_id);
        if(getMenu_idDirtyFlag())
            srfdomain.setMenu_id(menu_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getAddress_idDirtyFlag())
            srfdomain.setAddress_id(address_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getEvent_type_idDirtyFlag())
            srfdomain.setEvent_type_id(event_type_id);

        return srfdomain;
    }

    public void fromDO(Event_event srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getSeats_usedDirtyFlag())
            this.setSeats_used(srfdomain.getSeats_used());
        if(srfdomain.getIs_seo_optimizedDirtyFlag())
            this.setIs_seo_optimized(srfdomain.getIs_seo_optimized());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getBadge_frontDirtyFlag())
            this.setBadge_front(srfdomain.getBadge_front());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getEvent_mail_idsDirtyFlag())
            this.setEvent_mail_ids(srfdomain.getEvent_mail_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getSeats_expectedDirtyFlag())
            this.setSeats_expected(srfdomain.getSeats_expected());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getSeats_availabilityDirtyFlag())
            this.setSeats_availability(srfdomain.getSeats_availability());
        if(srfdomain.getEvent_ticket_idsDirtyFlag())
            this.setEvent_ticket_ids(srfdomain.getEvent_ticket_ids());
        if(srfdomain.getSeats_unconfirmedDirtyFlag())
            this.setSeats_unconfirmed(srfdomain.getSeats_unconfirmed());
        if(srfdomain.getIs_participatingDirtyFlag())
            this.setIs_participating(srfdomain.getIs_participating());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getTwitter_hashtagDirtyFlag())
            this.setTwitter_hashtag(srfdomain.getTwitter_hashtag());
        if(srfdomain.getSeats_maxDirtyFlag())
            this.setSeats_max(srfdomain.getSeats_max());
        if(srfdomain.getDate_beginDirtyFlag())
            this.setDate_begin(srfdomain.getDate_begin());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getWebsite_meta_og_imgDirtyFlag())
            this.setWebsite_meta_og_img(srfdomain.getWebsite_meta_og_img());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getWebsite_meta_titleDirtyFlag())
            this.setWebsite_meta_title(srfdomain.getWebsite_meta_title());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getRegistration_idsDirtyFlag())
            this.setRegistration_ids(srfdomain.getRegistration_ids());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getBadge_innerrightDirtyFlag())
            this.setBadge_innerright(srfdomain.getBadge_innerright());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getWebsite_meta_descriptionDirtyFlag())
            this.setWebsite_meta_description(srfdomain.getWebsite_meta_description());
        if(srfdomain.getSeats_minDirtyFlag())
            this.setSeats_min(srfdomain.getSeats_min());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getBadge_backDirtyFlag())
            this.setBadge_back(srfdomain.getBadge_back());
        if(srfdomain.getWebsite_meta_keywordsDirtyFlag())
            this.setWebsite_meta_keywords(srfdomain.getWebsite_meta_keywords());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getAuto_confirmDirtyFlag())
            this.setAuto_confirm(srfdomain.getAuto_confirm());
        if(srfdomain.getDate_endDirtyFlag())
            this.setDate_end(srfdomain.getDate_end());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getBadge_innerleftDirtyFlag())
            this.setBadge_innerleft(srfdomain.getBadge_innerleft());
        if(srfdomain.getSeats_reservedDirtyFlag())
            this.setSeats_reserved(srfdomain.getSeats_reserved());
        if(srfdomain.getWebsite_menuDirtyFlag())
            this.setWebsite_menu(srfdomain.getWebsite_menu());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getDate_end_locatedDirtyFlag())
            this.setDate_end_located(srfdomain.getDate_end_located());
        if(srfdomain.getSeats_availableDirtyFlag())
            this.setSeats_available(srfdomain.getSeats_available());
        if(srfdomain.getIs_publishedDirtyFlag())
            this.setIs_published(srfdomain.getIs_published());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getIs_onlineDirtyFlag())
            this.setIs_online(srfdomain.getIs_online());
        if(srfdomain.getDate_tzDirtyFlag())
            this.setDate_tz(srfdomain.getDate_tz());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getDate_begin_locatedDirtyFlag())
            this.setDate_begin_located(srfdomain.getDate_begin_located());
        if(srfdomain.getEvent_logoDirtyFlag())
            this.setEvent_logo(srfdomain.getEvent_logo());
        if(srfdomain.getWebsite_urlDirtyFlag())
            this.setWebsite_url(srfdomain.getWebsite_url());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getEvent_type_id_textDirtyFlag())
            this.setEvent_type_id_text(srfdomain.getEvent_type_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getAddress_id_textDirtyFlag())
            this.setAddress_id_text(srfdomain.getAddress_id_text());
        if(srfdomain.getCountry_id_textDirtyFlag())
            this.setCountry_id_text(srfdomain.getCountry_id_text());
        if(srfdomain.getMenu_id_textDirtyFlag())
            this.setMenu_id_text(srfdomain.getMenu_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getOrganizer_id_textDirtyFlag())
            this.setOrganizer_id_text(srfdomain.getOrganizer_id_text());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getOrganizer_idDirtyFlag())
            this.setOrganizer_id(srfdomain.getOrganizer_id());
        if(srfdomain.getMenu_idDirtyFlag())
            this.setMenu_id(srfdomain.getMenu_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getAddress_idDirtyFlag())
            this.setAddress_id(srfdomain.getAddress_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getEvent_type_idDirtyFlag())
            this.setEvent_type_id(srfdomain.getEvent_type_id());

    }

    public List<Event_eventDTO> fromDOPage(List<Event_event> poPage)   {
        if(poPage == null)
            return null;
        List<Event_eventDTO> dtos=new ArrayList<Event_eventDTO>();
        for(Event_event domain : poPage) {
            Event_eventDTO dto = new Event_eventDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

