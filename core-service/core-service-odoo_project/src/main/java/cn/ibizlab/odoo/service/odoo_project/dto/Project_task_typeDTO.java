package cn.ibizlab.odoo.service.odoo_project.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_project.valuerule.anno.project_task_type.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Project_task_typeDTO]
 */
public class Project_task_typeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @Project_task_typeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [AUTO_VALIDATION_KANBAN_STATE]
     *
     */
    @Project_task_typeAuto_validation_kanban_stateDefault(info = "默认规则")
    private String auto_validation_kanban_state;

    @JsonIgnore
    private boolean auto_validation_kanban_stateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Project_task_typeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Project_task_typeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Project_task_typeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PROJECT_IDS]
     *
     */
    @Project_task_typeProject_idsDefault(info = "默认规则")
    private String project_ids;

    @JsonIgnore
    private boolean project_idsDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Project_task_typeSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Project_task_typeDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [LEGEND_NORMAL]
     *
     */
    @Project_task_typeLegend_normalDefault(info = "默认规则")
    private String legend_normal;

    @JsonIgnore
    private boolean legend_normalDirtyFlag;

    /**
     * 属性 [FOLD]
     *
     */
    @Project_task_typeFoldDefault(info = "默认规则")
    private String fold;

    @JsonIgnore
    private boolean foldDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Project_task_typeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [LEGEND_BLOCKED]
     *
     */
    @Project_task_typeLegend_blockedDefault(info = "默认规则")
    private String legend_blocked;

    @JsonIgnore
    private boolean legend_blockedDirtyFlag;

    /**
     * 属性 [LEGEND_PRIORITY]
     *
     */
    @Project_task_typeLegend_priorityDefault(info = "默认规则")
    private String legend_priority;

    @JsonIgnore
    private boolean legend_priorityDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Project_task_type__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [LEGEND_DONE]
     *
     */
    @Project_task_typeLegend_doneDefault(info = "默认规则")
    private String legend_done;

    @JsonIgnore
    private boolean legend_doneDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Project_task_typeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [RATING_TEMPLATE_ID_TEXT]
     *
     */
    @Project_task_typeRating_template_id_textDefault(info = "默认规则")
    private String rating_template_id_text;

    @JsonIgnore
    private boolean rating_template_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Project_task_typeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MAIL_TEMPLATE_ID_TEXT]
     *
     */
    @Project_task_typeMail_template_id_textDefault(info = "默认规则")
    private String mail_template_id_text;

    @JsonIgnore
    private boolean mail_template_id_textDirtyFlag;

    /**
     * 属性 [MAIL_TEMPLATE_ID]
     *
     */
    @Project_task_typeMail_template_idDefault(info = "默认规则")
    private Integer mail_template_id;

    @JsonIgnore
    private boolean mail_template_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Project_task_typeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [RATING_TEMPLATE_ID]
     *
     */
    @Project_task_typeRating_template_idDefault(info = "默认规则")
    private Integer rating_template_id;

    @JsonIgnore
    private boolean rating_template_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Project_task_typeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [AUTO_VALIDATION_KANBAN_STATE]
     */
    @JsonProperty("auto_validation_kanban_state")
    public String getAuto_validation_kanban_state(){
        return auto_validation_kanban_state ;
    }

    /**
     * 设置 [AUTO_VALIDATION_KANBAN_STATE]
     */
    @JsonProperty("auto_validation_kanban_state")
    public void setAuto_validation_kanban_state(String  auto_validation_kanban_state){
        this.auto_validation_kanban_state = auto_validation_kanban_state ;
        this.auto_validation_kanban_stateDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_VALIDATION_KANBAN_STATE]脏标记
     */
    @JsonIgnore
    public boolean getAuto_validation_kanban_stateDirtyFlag(){
        return auto_validation_kanban_stateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PROJECT_IDS]
     */
    @JsonProperty("project_ids")
    public String getProject_ids(){
        return project_ids ;
    }

    /**
     * 设置 [PROJECT_IDS]
     */
    @JsonProperty("project_ids")
    public void setProject_ids(String  project_ids){
        this.project_ids = project_ids ;
        this.project_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PROJECT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getProject_idsDirtyFlag(){
        return project_idsDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_NORMAL]
     */
    @JsonProperty("legend_normal")
    public String getLegend_normal(){
        return legend_normal ;
    }

    /**
     * 设置 [LEGEND_NORMAL]
     */
    @JsonProperty("legend_normal")
    public void setLegend_normal(String  legend_normal){
        this.legend_normal = legend_normal ;
        this.legend_normalDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_NORMAL]脏标记
     */
    @JsonIgnore
    public boolean getLegend_normalDirtyFlag(){
        return legend_normalDirtyFlag ;
    }

    /**
     * 获取 [FOLD]
     */
    @JsonProperty("fold")
    public String getFold(){
        return fold ;
    }

    /**
     * 设置 [FOLD]
     */
    @JsonProperty("fold")
    public void setFold(String  fold){
        this.fold = fold ;
        this.foldDirtyFlag = true ;
    }

    /**
     * 获取 [FOLD]脏标记
     */
    @JsonIgnore
    public boolean getFoldDirtyFlag(){
        return foldDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_BLOCKED]
     */
    @JsonProperty("legend_blocked")
    public String getLegend_blocked(){
        return legend_blocked ;
    }

    /**
     * 设置 [LEGEND_BLOCKED]
     */
    @JsonProperty("legend_blocked")
    public void setLegend_blocked(String  legend_blocked){
        this.legend_blocked = legend_blocked ;
        this.legend_blockedDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_BLOCKED]脏标记
     */
    @JsonIgnore
    public boolean getLegend_blockedDirtyFlag(){
        return legend_blockedDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_PRIORITY]
     */
    @JsonProperty("legend_priority")
    public String getLegend_priority(){
        return legend_priority ;
    }

    /**
     * 设置 [LEGEND_PRIORITY]
     */
    @JsonProperty("legend_priority")
    public void setLegend_priority(String  legend_priority){
        this.legend_priority = legend_priority ;
        this.legend_priorityDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getLegend_priorityDirtyFlag(){
        return legend_priorityDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_DONE]
     */
    @JsonProperty("legend_done")
    public String getLegend_done(){
        return legend_done ;
    }

    /**
     * 设置 [LEGEND_DONE]
     */
    @JsonProperty("legend_done")
    public void setLegend_done(String  legend_done){
        this.legend_done = legend_done ;
        this.legend_doneDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_DONE]脏标记
     */
    @JsonIgnore
    public boolean getLegend_doneDirtyFlag(){
        return legend_doneDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [RATING_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("rating_template_id_text")
    public String getRating_template_id_text(){
        return rating_template_id_text ;
    }

    /**
     * 设置 [RATING_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("rating_template_id_text")
    public void setRating_template_id_text(String  rating_template_id_text){
        this.rating_template_id_text = rating_template_id_text ;
        this.rating_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRating_template_id_textDirtyFlag(){
        return rating_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("mail_template_id_text")
    public String getMail_template_id_text(){
        return mail_template_id_text ;
    }

    /**
     * 设置 [MAIL_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("mail_template_id_text")
    public void setMail_template_id_text(String  mail_template_id_text){
        this.mail_template_id_text = mail_template_id_text ;
        this.mail_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_id_textDirtyFlag(){
        return mail_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_ID]
     */
    @JsonProperty("mail_template_id")
    public Integer getMail_template_id(){
        return mail_template_id ;
    }

    /**
     * 设置 [MAIL_TEMPLATE_ID]
     */
    @JsonProperty("mail_template_id")
    public void setMail_template_id(Integer  mail_template_id){
        this.mail_template_id = mail_template_id ;
        this.mail_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_idDirtyFlag(){
        return mail_template_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [RATING_TEMPLATE_ID]
     */
    @JsonProperty("rating_template_id")
    public Integer getRating_template_id(){
        return rating_template_id ;
    }

    /**
     * 设置 [RATING_TEMPLATE_ID]
     */
    @JsonProperty("rating_template_id")
    public void setRating_template_id(Integer  rating_template_id){
        this.rating_template_id = rating_template_id ;
        this.rating_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getRating_template_idDirtyFlag(){
        return rating_template_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Project_task_type toDO() {
        Project_task_type srfdomain = new Project_task_type();
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getAuto_validation_kanban_stateDirtyFlag())
            srfdomain.setAuto_validation_kanban_state(auto_validation_kanban_state);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getProject_idsDirtyFlag())
            srfdomain.setProject_ids(project_ids);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getLegend_normalDirtyFlag())
            srfdomain.setLegend_normal(legend_normal);
        if(getFoldDirtyFlag())
            srfdomain.setFold(fold);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getLegend_blockedDirtyFlag())
            srfdomain.setLegend_blocked(legend_blocked);
        if(getLegend_priorityDirtyFlag())
            srfdomain.setLegend_priority(legend_priority);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getLegend_doneDirtyFlag())
            srfdomain.setLegend_done(legend_done);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getRating_template_id_textDirtyFlag())
            srfdomain.setRating_template_id_text(rating_template_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getMail_template_id_textDirtyFlag())
            srfdomain.setMail_template_id_text(mail_template_id_text);
        if(getMail_template_idDirtyFlag())
            srfdomain.setMail_template_id(mail_template_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getRating_template_idDirtyFlag())
            srfdomain.setRating_template_id(rating_template_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Project_task_type srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getAuto_validation_kanban_stateDirtyFlag())
            this.setAuto_validation_kanban_state(srfdomain.getAuto_validation_kanban_state());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getProject_idsDirtyFlag())
            this.setProject_ids(srfdomain.getProject_ids());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getLegend_normalDirtyFlag())
            this.setLegend_normal(srfdomain.getLegend_normal());
        if(srfdomain.getFoldDirtyFlag())
            this.setFold(srfdomain.getFold());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getLegend_blockedDirtyFlag())
            this.setLegend_blocked(srfdomain.getLegend_blocked());
        if(srfdomain.getLegend_priorityDirtyFlag())
            this.setLegend_priority(srfdomain.getLegend_priority());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getLegend_doneDirtyFlag())
            this.setLegend_done(srfdomain.getLegend_done());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getRating_template_id_textDirtyFlag())
            this.setRating_template_id_text(srfdomain.getRating_template_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getMail_template_id_textDirtyFlag())
            this.setMail_template_id_text(srfdomain.getMail_template_id_text());
        if(srfdomain.getMail_template_idDirtyFlag())
            this.setMail_template_id(srfdomain.getMail_template_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getRating_template_idDirtyFlag())
            this.setRating_template_id(srfdomain.getRating_template_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Project_task_typeDTO> fromDOPage(List<Project_task_type> poPage)   {
        if(poPage == null)
            return null;
        List<Project_task_typeDTO> dtos=new ArrayList<Project_task_typeDTO>();
        for(Project_task_type domain : poPage) {
            Project_task_typeDTO dto = new Project_task_typeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

