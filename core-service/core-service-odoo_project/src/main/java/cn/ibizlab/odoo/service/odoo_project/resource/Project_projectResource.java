package cn.ibizlab.odoo.service.odoo_project.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_project.dto.Project_projectDTO;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_project;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_projectService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_projectSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Project_project" })
@RestController
@RequestMapping("")
public class Project_projectResource {

    @Autowired
    private IProject_projectService project_projectService;

    public IProject_projectService getProject_projectService() {
        return this.project_projectService;
    }

    @ApiOperation(value = "获取数据", tags = {"Project_project" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_projects/{project_project_id}")
    public ResponseEntity<Project_projectDTO> get(@PathVariable("project_project_id") Integer project_project_id) {
        Project_projectDTO dto = new Project_projectDTO();
        Project_project domain = project_projectService.get(project_project_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Project_project" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_projects/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_projectDTO> project_projectdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Project_project" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_projects/{project_project_id}")

    public ResponseEntity<Project_projectDTO> update(@PathVariable("project_project_id") Integer project_project_id, @RequestBody Project_projectDTO project_projectdto) {
		Project_project domain = project_projectdto.toDO();
        domain.setId(project_project_id);
		project_projectService.update(domain);
		Project_projectDTO dto = new Project_projectDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Project_project" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_projects")

    public ResponseEntity<Project_projectDTO> create(@RequestBody Project_projectDTO project_projectdto) {
        Project_projectDTO dto = new Project_projectDTO();
        Project_project domain = project_projectdto.toDO();
		project_projectService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Project_project" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_projects/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Project_projectDTO> project_projectdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Project_project" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_projects/{project_project_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("project_project_id") Integer project_project_id) {
        Project_projectDTO project_projectdto = new Project_projectDTO();
		Project_project domain = new Project_project();
		project_projectdto.setId(project_project_id);
		domain.setId(project_project_id);
        Boolean rst = project_projectService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Project_project" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_projects/createBatch")
    public ResponseEntity<Boolean> createBatchProject_project(@RequestBody List<Project_projectDTO> project_projectdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Project_project" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_project/project_projects/fetchdefault")
	public ResponseEntity<Page<Project_projectDTO>> fetchDefault(Project_projectSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Project_projectDTO> list = new ArrayList<Project_projectDTO>();
        
        Page<Project_project> domains = project_projectService.searchDefault(context) ;
        for(Project_project project_project : domains.getContent()){
            Project_projectDTO dto = new Project_projectDTO();
            dto.fromDO(project_project);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
