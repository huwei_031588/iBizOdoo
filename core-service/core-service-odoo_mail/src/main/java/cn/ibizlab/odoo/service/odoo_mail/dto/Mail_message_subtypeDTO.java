package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_message_subtype.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_message_subtypeDTO]
 */
public class Mail_message_subtypeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Mail_message_subtypeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mail_message_subtypeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_message_subtype__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [RELATION_FIELD]
     *
     */
    @Mail_message_subtypeRelation_fieldDefault(info = "默认规则")
    private String relation_field;

    @JsonIgnore
    private boolean relation_fieldDirtyFlag;

    /**
     * 属性 [INTERNAL]
     *
     */
    @Mail_message_subtypeInternalDefault(info = "默认规则")
    private String internal;

    @JsonIgnore
    private boolean internalDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Mail_message_subtypeSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_message_subtypeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DEFAULT]
     *
     */
    @Mail_message_subtypeIbizdefaultDefault(info = "默认规则")
    private String ibizdefault;

    @JsonIgnore
    private boolean ibizdefaultDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Mail_message_subtypeDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_message_subtypeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [HIDDEN]
     *
     */
    @Mail_message_subtypeHiddenDefault(info = "默认规则")
    private String hidden;

    @JsonIgnore
    private boolean hiddenDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_message_subtypeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @Mail_message_subtypeRes_modelDefault(info = "默认规则")
    private String res_model;

    @JsonIgnore
    private boolean res_modelDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_message_subtypeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_message_subtypeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Mail_message_subtypeParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_message_subtypeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_message_subtypeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Mail_message_subtypeParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [RELATION_FIELD]
     */
    @JsonProperty("relation_field")
    public String getRelation_field(){
        return relation_field ;
    }

    /**
     * 设置 [RELATION_FIELD]
     */
    @JsonProperty("relation_field")
    public void setRelation_field(String  relation_field){
        this.relation_field = relation_field ;
        this.relation_fieldDirtyFlag = true ;
    }

    /**
     * 获取 [RELATION_FIELD]脏标记
     */
    @JsonIgnore
    public boolean getRelation_fieldDirtyFlag(){
        return relation_fieldDirtyFlag ;
    }

    /**
     * 获取 [INTERNAL]
     */
    @JsonProperty("internal")
    public String getInternal(){
        return internal ;
    }

    /**
     * 设置 [INTERNAL]
     */
    @JsonProperty("internal")
    public void setInternal(String  internal){
        this.internal = internal ;
        this.internalDirtyFlag = true ;
    }

    /**
     * 获取 [INTERNAL]脏标记
     */
    @JsonIgnore
    public boolean getInternalDirtyFlag(){
        return internalDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT]
     */
    @JsonProperty("ibizdefault")
    public String getIbizdefault(){
        return ibizdefault ;
    }

    /**
     * 设置 [DEFAULT]
     */
    @JsonProperty("ibizdefault")
    public void setIbizdefault(String  ibizdefault){
        this.ibizdefault = ibizdefault ;
        this.ibizdefaultDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT]脏标记
     */
    @JsonIgnore
    public boolean getIbizdefaultDirtyFlag(){
        return ibizdefaultDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [HIDDEN]
     */
    @JsonProperty("hidden")
    public String getHidden(){
        return hidden ;
    }

    /**
     * 设置 [HIDDEN]
     */
    @JsonProperty("hidden")
    public void setHidden(String  hidden){
        this.hidden = hidden ;
        this.hiddenDirtyFlag = true ;
    }

    /**
     * 获取 [HIDDEN]脏标记
     */
    @JsonIgnore
    public boolean getHiddenDirtyFlag(){
        return hiddenDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return res_model ;
    }

    /**
     * 设置 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return res_modelDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }



    public Mail_message_subtype toDO() {
        Mail_message_subtype srfdomain = new Mail_message_subtype();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getRelation_fieldDirtyFlag())
            srfdomain.setRelation_field(relation_field);
        if(getInternalDirtyFlag())
            srfdomain.setInternal(internal);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIbizdefaultDirtyFlag())
            srfdomain.setIbizdefault(ibizdefault);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getHiddenDirtyFlag())
            srfdomain.setHidden(hidden);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getRes_modelDirtyFlag())
            srfdomain.setRes_model(res_model);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);

        return srfdomain;
    }

    public void fromDO(Mail_message_subtype srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getRelation_fieldDirtyFlag())
            this.setRelation_field(srfdomain.getRelation_field());
        if(srfdomain.getInternalDirtyFlag())
            this.setInternal(srfdomain.getInternal());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIbizdefaultDirtyFlag())
            this.setIbizdefault(srfdomain.getIbizdefault());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getHiddenDirtyFlag())
            this.setHidden(srfdomain.getHidden());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getRes_modelDirtyFlag())
            this.setRes_model(srfdomain.getRes_model());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());

    }

    public List<Mail_message_subtypeDTO> fromDOPage(List<Mail_message_subtype> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_message_subtypeDTO> dtos=new ArrayList<Mail_message_subtypeDTO>();
        for(Mail_message_subtype domain : poPage) {
            Mail_message_subtypeDTO dto = new Mail_message_subtypeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

