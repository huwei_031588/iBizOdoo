package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_compose_messageDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_compose_message;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_compose_messageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_compose_messageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_compose_message" })
@RestController
@RequestMapping("")
public class Mail_compose_messageResource {

    @Autowired
    private IMail_compose_messageService mail_compose_messageService;

    public IMail_compose_messageService getMail_compose_messageService() {
        return this.mail_compose_messageService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_compose_message" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_compose_messages/createBatch")
    public ResponseEntity<Boolean> createBatchMail_compose_message(@RequestBody List<Mail_compose_messageDTO> mail_compose_messagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_compose_message" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_compose_messages/{mail_compose_message_id}")
    public ResponseEntity<Mail_compose_messageDTO> get(@PathVariable("mail_compose_message_id") Integer mail_compose_message_id) {
        Mail_compose_messageDTO dto = new Mail_compose_messageDTO();
        Mail_compose_message domain = mail_compose_messageService.get(mail_compose_message_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_compose_message" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_compose_messages/{mail_compose_message_id}")

    public ResponseEntity<Mail_compose_messageDTO> update(@PathVariable("mail_compose_message_id") Integer mail_compose_message_id, @RequestBody Mail_compose_messageDTO mail_compose_messagedto) {
		Mail_compose_message domain = mail_compose_messagedto.toDO();
        domain.setId(mail_compose_message_id);
		mail_compose_messageService.update(domain);
		Mail_compose_messageDTO dto = new Mail_compose_messageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_compose_message" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_compose_messages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_compose_messageDTO> mail_compose_messagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_compose_message" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_compose_messages")

    public ResponseEntity<Mail_compose_messageDTO> create(@RequestBody Mail_compose_messageDTO mail_compose_messagedto) {
        Mail_compose_messageDTO dto = new Mail_compose_messageDTO();
        Mail_compose_message domain = mail_compose_messagedto.toDO();
		mail_compose_messageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_compose_message" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_compose_messages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_compose_messageDTO> mail_compose_messagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_compose_message" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_compose_messages/{mail_compose_message_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_compose_message_id") Integer mail_compose_message_id) {
        Mail_compose_messageDTO mail_compose_messagedto = new Mail_compose_messageDTO();
		Mail_compose_message domain = new Mail_compose_message();
		mail_compose_messagedto.setId(mail_compose_message_id);
		domain.setId(mail_compose_message_id);
        Boolean rst = mail_compose_messageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_compose_message" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_compose_messages/fetchdefault")
	public ResponseEntity<Page<Mail_compose_messageDTO>> fetchDefault(Mail_compose_messageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_compose_messageDTO> list = new ArrayList<Mail_compose_messageDTO>();
        
        Page<Mail_compose_message> domains = mail_compose_messageService.searchDefault(context) ;
        for(Mail_compose_message mail_compose_message : domains.getContent()){
            Mail_compose_messageDTO dto = new Mail_compose_messageDTO();
            dto.fromDO(mail_compose_message);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
