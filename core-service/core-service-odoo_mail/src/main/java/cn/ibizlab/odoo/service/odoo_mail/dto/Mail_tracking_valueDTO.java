package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_tracking_value.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_tracking_value;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_tracking_valueDTO]
 */
public class Mail_tracking_valueDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [FIELD_DESC]
     *
     */
    @Mail_tracking_valueField_descDefault(info = "默认规则")
    private String field_desc;

    @JsonIgnore
    private boolean field_descDirtyFlag;

    /**
     * 属性 [OLD_VALUE_CHAR]
     *
     */
    @Mail_tracking_valueOld_value_charDefault(info = "默认规则")
    private String old_value_char;

    @JsonIgnore
    private boolean old_value_charDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_tracking_valueWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [NEW_VALUE_DATETIME]
     *
     */
    @Mail_tracking_valueNew_value_datetimeDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp new_value_datetime;

    @JsonIgnore
    private boolean new_value_datetimeDirtyFlag;

    /**
     * 属性 [OLD_VALUE_MONETARY]
     *
     */
    @Mail_tracking_valueOld_value_monetaryDefault(info = "默认规则")
    private Double old_value_monetary;

    @JsonIgnore
    private boolean old_value_monetaryDirtyFlag;

    /**
     * 属性 [NEW_VALUE_CHAR]
     *
     */
    @Mail_tracking_valueNew_value_charDefault(info = "默认规则")
    private String new_value_char;

    @JsonIgnore
    private boolean new_value_charDirtyFlag;

    /**
     * 属性 [NEW_VALUE_TEXT]
     *
     */
    @Mail_tracking_valueNew_value_textDefault(info = "默认规则")
    private String new_value_text;

    @JsonIgnore
    private boolean new_value_textDirtyFlag;

    /**
     * 属性 [TRACK_SEQUENCE]
     *
     */
    @Mail_tracking_valueTrack_sequenceDefault(info = "默认规则")
    private Integer track_sequence;

    @JsonIgnore
    private boolean track_sequenceDirtyFlag;

    /**
     * 属性 [NEW_VALUE_MONETARY]
     *
     */
    @Mail_tracking_valueNew_value_monetaryDefault(info = "默认规则")
    private Double new_value_monetary;

    @JsonIgnore
    private boolean new_value_monetaryDirtyFlag;

    /**
     * 属性 [OLD_VALUE_DATETIME]
     *
     */
    @Mail_tracking_valueOld_value_datetimeDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp old_value_datetime;

    @JsonIgnore
    private boolean old_value_datetimeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_tracking_value__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_tracking_valueCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [OLD_VALUE_INTEGER]
     *
     */
    @Mail_tracking_valueOld_value_integerDefault(info = "默认规则")
    private Integer old_value_integer;

    @JsonIgnore
    private boolean old_value_integerDirtyFlag;

    /**
     * 属性 [OLD_VALUE_TEXT]
     *
     */
    @Mail_tracking_valueOld_value_textDefault(info = "默认规则")
    private String old_value_text;

    @JsonIgnore
    private boolean old_value_textDirtyFlag;

    /**
     * 属性 [FIELD_TYPE]
     *
     */
    @Mail_tracking_valueField_typeDefault(info = "默认规则")
    private String field_type;

    @JsonIgnore
    private boolean field_typeDirtyFlag;

    /**
     * 属性 [NEW_VALUE_INTEGER]
     *
     */
    @Mail_tracking_valueNew_value_integerDefault(info = "默认规则")
    private Integer new_value_integer;

    @JsonIgnore
    private boolean new_value_integerDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_tracking_valueDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [NEW_VALUE_FLOAT]
     *
     */
    @Mail_tracking_valueNew_value_floatDefault(info = "默认规则")
    private Double new_value_float;

    @JsonIgnore
    private boolean new_value_floatDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_tracking_valueIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [FIELD]
     *
     */
    @Mail_tracking_valueFieldDefault(info = "默认规则")
    private String field;

    @JsonIgnore
    private boolean fieldDirtyFlag;

    /**
     * 属性 [OLD_VALUE_FLOAT]
     *
     */
    @Mail_tracking_valueOld_value_floatDefault(info = "默认规则")
    private Double old_value_float;

    @JsonIgnore
    private boolean old_value_floatDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_tracking_valueWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_tracking_valueCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_tracking_valueWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_tracking_valueCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [MAIL_MESSAGE_ID]
     *
     */
    @Mail_tracking_valueMail_message_idDefault(info = "默认规则")
    private Integer mail_message_id;

    @JsonIgnore
    private boolean mail_message_idDirtyFlag;


    /**
     * 获取 [FIELD_DESC]
     */
    @JsonProperty("field_desc")
    public String getField_desc(){
        return field_desc ;
    }

    /**
     * 设置 [FIELD_DESC]
     */
    @JsonProperty("field_desc")
    public void setField_desc(String  field_desc){
        this.field_desc = field_desc ;
        this.field_descDirtyFlag = true ;
    }

    /**
     * 获取 [FIELD_DESC]脏标记
     */
    @JsonIgnore
    public boolean getField_descDirtyFlag(){
        return field_descDirtyFlag ;
    }

    /**
     * 获取 [OLD_VALUE_CHAR]
     */
    @JsonProperty("old_value_char")
    public String getOld_value_char(){
        return old_value_char ;
    }

    /**
     * 设置 [OLD_VALUE_CHAR]
     */
    @JsonProperty("old_value_char")
    public void setOld_value_char(String  old_value_char){
        this.old_value_char = old_value_char ;
        this.old_value_charDirtyFlag = true ;
    }

    /**
     * 获取 [OLD_VALUE_CHAR]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_charDirtyFlag(){
        return old_value_charDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [NEW_VALUE_DATETIME]
     */
    @JsonProperty("new_value_datetime")
    public Timestamp getNew_value_datetime(){
        return new_value_datetime ;
    }

    /**
     * 设置 [NEW_VALUE_DATETIME]
     */
    @JsonProperty("new_value_datetime")
    public void setNew_value_datetime(Timestamp  new_value_datetime){
        this.new_value_datetime = new_value_datetime ;
        this.new_value_datetimeDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_VALUE_DATETIME]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_datetimeDirtyFlag(){
        return new_value_datetimeDirtyFlag ;
    }

    /**
     * 获取 [OLD_VALUE_MONETARY]
     */
    @JsonProperty("old_value_monetary")
    public Double getOld_value_monetary(){
        return old_value_monetary ;
    }

    /**
     * 设置 [OLD_VALUE_MONETARY]
     */
    @JsonProperty("old_value_monetary")
    public void setOld_value_monetary(Double  old_value_monetary){
        this.old_value_monetary = old_value_monetary ;
        this.old_value_monetaryDirtyFlag = true ;
    }

    /**
     * 获取 [OLD_VALUE_MONETARY]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_monetaryDirtyFlag(){
        return old_value_monetaryDirtyFlag ;
    }

    /**
     * 获取 [NEW_VALUE_CHAR]
     */
    @JsonProperty("new_value_char")
    public String getNew_value_char(){
        return new_value_char ;
    }

    /**
     * 设置 [NEW_VALUE_CHAR]
     */
    @JsonProperty("new_value_char")
    public void setNew_value_char(String  new_value_char){
        this.new_value_char = new_value_char ;
        this.new_value_charDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_VALUE_CHAR]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_charDirtyFlag(){
        return new_value_charDirtyFlag ;
    }

    /**
     * 获取 [NEW_VALUE_TEXT]
     */
    @JsonProperty("new_value_text")
    public String getNew_value_text(){
        return new_value_text ;
    }

    /**
     * 设置 [NEW_VALUE_TEXT]
     */
    @JsonProperty("new_value_text")
    public void setNew_value_text(String  new_value_text){
        this.new_value_text = new_value_text ;
        this.new_value_textDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_VALUE_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_textDirtyFlag(){
        return new_value_textDirtyFlag ;
    }

    /**
     * 获取 [TRACK_SEQUENCE]
     */
    @JsonProperty("track_sequence")
    public Integer getTrack_sequence(){
        return track_sequence ;
    }

    /**
     * 设置 [TRACK_SEQUENCE]
     */
    @JsonProperty("track_sequence")
    public void setTrack_sequence(Integer  track_sequence){
        this.track_sequence = track_sequence ;
        this.track_sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [TRACK_SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getTrack_sequenceDirtyFlag(){
        return track_sequenceDirtyFlag ;
    }

    /**
     * 获取 [NEW_VALUE_MONETARY]
     */
    @JsonProperty("new_value_monetary")
    public Double getNew_value_monetary(){
        return new_value_monetary ;
    }

    /**
     * 设置 [NEW_VALUE_MONETARY]
     */
    @JsonProperty("new_value_monetary")
    public void setNew_value_monetary(Double  new_value_monetary){
        this.new_value_monetary = new_value_monetary ;
        this.new_value_monetaryDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_VALUE_MONETARY]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_monetaryDirtyFlag(){
        return new_value_monetaryDirtyFlag ;
    }

    /**
     * 获取 [OLD_VALUE_DATETIME]
     */
    @JsonProperty("old_value_datetime")
    public Timestamp getOld_value_datetime(){
        return old_value_datetime ;
    }

    /**
     * 设置 [OLD_VALUE_DATETIME]
     */
    @JsonProperty("old_value_datetime")
    public void setOld_value_datetime(Timestamp  old_value_datetime){
        this.old_value_datetime = old_value_datetime ;
        this.old_value_datetimeDirtyFlag = true ;
    }

    /**
     * 获取 [OLD_VALUE_DATETIME]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_datetimeDirtyFlag(){
        return old_value_datetimeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [OLD_VALUE_INTEGER]
     */
    @JsonProperty("old_value_integer")
    public Integer getOld_value_integer(){
        return old_value_integer ;
    }

    /**
     * 设置 [OLD_VALUE_INTEGER]
     */
    @JsonProperty("old_value_integer")
    public void setOld_value_integer(Integer  old_value_integer){
        this.old_value_integer = old_value_integer ;
        this.old_value_integerDirtyFlag = true ;
    }

    /**
     * 获取 [OLD_VALUE_INTEGER]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_integerDirtyFlag(){
        return old_value_integerDirtyFlag ;
    }

    /**
     * 获取 [OLD_VALUE_TEXT]
     */
    @JsonProperty("old_value_text")
    public String getOld_value_text(){
        return old_value_text ;
    }

    /**
     * 设置 [OLD_VALUE_TEXT]
     */
    @JsonProperty("old_value_text")
    public void setOld_value_text(String  old_value_text){
        this.old_value_text = old_value_text ;
        this.old_value_textDirtyFlag = true ;
    }

    /**
     * 获取 [OLD_VALUE_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_textDirtyFlag(){
        return old_value_textDirtyFlag ;
    }

    /**
     * 获取 [FIELD_TYPE]
     */
    @JsonProperty("field_type")
    public String getField_type(){
        return field_type ;
    }

    /**
     * 设置 [FIELD_TYPE]
     */
    @JsonProperty("field_type")
    public void setField_type(String  field_type){
        this.field_type = field_type ;
        this.field_typeDirtyFlag = true ;
    }

    /**
     * 获取 [FIELD_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getField_typeDirtyFlag(){
        return field_typeDirtyFlag ;
    }

    /**
     * 获取 [NEW_VALUE_INTEGER]
     */
    @JsonProperty("new_value_integer")
    public Integer getNew_value_integer(){
        return new_value_integer ;
    }

    /**
     * 设置 [NEW_VALUE_INTEGER]
     */
    @JsonProperty("new_value_integer")
    public void setNew_value_integer(Integer  new_value_integer){
        this.new_value_integer = new_value_integer ;
        this.new_value_integerDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_VALUE_INTEGER]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_integerDirtyFlag(){
        return new_value_integerDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [NEW_VALUE_FLOAT]
     */
    @JsonProperty("new_value_float")
    public Double getNew_value_float(){
        return new_value_float ;
    }

    /**
     * 设置 [NEW_VALUE_FLOAT]
     */
    @JsonProperty("new_value_float")
    public void setNew_value_float(Double  new_value_float){
        this.new_value_float = new_value_float ;
        this.new_value_floatDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_VALUE_FLOAT]脏标记
     */
    @JsonIgnore
    public boolean getNew_value_floatDirtyFlag(){
        return new_value_floatDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [FIELD]
     */
    @JsonProperty("field")
    public String getField(){
        return field ;
    }

    /**
     * 设置 [FIELD]
     */
    @JsonProperty("field")
    public void setField(String  field){
        this.field = field ;
        this.fieldDirtyFlag = true ;
    }

    /**
     * 获取 [FIELD]脏标记
     */
    @JsonIgnore
    public boolean getFieldDirtyFlag(){
        return fieldDirtyFlag ;
    }

    /**
     * 获取 [OLD_VALUE_FLOAT]
     */
    @JsonProperty("old_value_float")
    public Double getOld_value_float(){
        return old_value_float ;
    }

    /**
     * 设置 [OLD_VALUE_FLOAT]
     */
    @JsonProperty("old_value_float")
    public void setOld_value_float(Double  old_value_float){
        this.old_value_float = old_value_float ;
        this.old_value_floatDirtyFlag = true ;
    }

    /**
     * 获取 [OLD_VALUE_FLOAT]脏标记
     */
    @JsonIgnore
    public boolean getOld_value_floatDirtyFlag(){
        return old_value_floatDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [MAIL_MESSAGE_ID]
     */
    @JsonProperty("mail_message_id")
    public Integer getMail_message_id(){
        return mail_message_id ;
    }

    /**
     * 设置 [MAIL_MESSAGE_ID]
     */
    @JsonProperty("mail_message_id")
    public void setMail_message_id(Integer  mail_message_id){
        this.mail_message_id = mail_message_id ;
        this.mail_message_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_MESSAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_message_idDirtyFlag(){
        return mail_message_idDirtyFlag ;
    }



    public Mail_tracking_value toDO() {
        Mail_tracking_value srfdomain = new Mail_tracking_value();
        if(getField_descDirtyFlag())
            srfdomain.setField_desc(field_desc);
        if(getOld_value_charDirtyFlag())
            srfdomain.setOld_value_char(old_value_char);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getNew_value_datetimeDirtyFlag())
            srfdomain.setNew_value_datetime(new_value_datetime);
        if(getOld_value_monetaryDirtyFlag())
            srfdomain.setOld_value_monetary(old_value_monetary);
        if(getNew_value_charDirtyFlag())
            srfdomain.setNew_value_char(new_value_char);
        if(getNew_value_textDirtyFlag())
            srfdomain.setNew_value_text(new_value_text);
        if(getTrack_sequenceDirtyFlag())
            srfdomain.setTrack_sequence(track_sequence);
        if(getNew_value_monetaryDirtyFlag())
            srfdomain.setNew_value_monetary(new_value_monetary);
        if(getOld_value_datetimeDirtyFlag())
            srfdomain.setOld_value_datetime(old_value_datetime);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getOld_value_integerDirtyFlag())
            srfdomain.setOld_value_integer(old_value_integer);
        if(getOld_value_textDirtyFlag())
            srfdomain.setOld_value_text(old_value_text);
        if(getField_typeDirtyFlag())
            srfdomain.setField_type(field_type);
        if(getNew_value_integerDirtyFlag())
            srfdomain.setNew_value_integer(new_value_integer);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getNew_value_floatDirtyFlag())
            srfdomain.setNew_value_float(new_value_float);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getFieldDirtyFlag())
            srfdomain.setField(field);
        if(getOld_value_floatDirtyFlag())
            srfdomain.setOld_value_float(old_value_float);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getMail_message_idDirtyFlag())
            srfdomain.setMail_message_id(mail_message_id);

        return srfdomain;
    }

    public void fromDO(Mail_tracking_value srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getField_descDirtyFlag())
            this.setField_desc(srfdomain.getField_desc());
        if(srfdomain.getOld_value_charDirtyFlag())
            this.setOld_value_char(srfdomain.getOld_value_char());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getNew_value_datetimeDirtyFlag())
            this.setNew_value_datetime(srfdomain.getNew_value_datetime());
        if(srfdomain.getOld_value_monetaryDirtyFlag())
            this.setOld_value_monetary(srfdomain.getOld_value_monetary());
        if(srfdomain.getNew_value_charDirtyFlag())
            this.setNew_value_char(srfdomain.getNew_value_char());
        if(srfdomain.getNew_value_textDirtyFlag())
            this.setNew_value_text(srfdomain.getNew_value_text());
        if(srfdomain.getTrack_sequenceDirtyFlag())
            this.setTrack_sequence(srfdomain.getTrack_sequence());
        if(srfdomain.getNew_value_monetaryDirtyFlag())
            this.setNew_value_monetary(srfdomain.getNew_value_monetary());
        if(srfdomain.getOld_value_datetimeDirtyFlag())
            this.setOld_value_datetime(srfdomain.getOld_value_datetime());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getOld_value_integerDirtyFlag())
            this.setOld_value_integer(srfdomain.getOld_value_integer());
        if(srfdomain.getOld_value_textDirtyFlag())
            this.setOld_value_text(srfdomain.getOld_value_text());
        if(srfdomain.getField_typeDirtyFlag())
            this.setField_type(srfdomain.getField_type());
        if(srfdomain.getNew_value_integerDirtyFlag())
            this.setNew_value_integer(srfdomain.getNew_value_integer());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getNew_value_floatDirtyFlag())
            this.setNew_value_float(srfdomain.getNew_value_float());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getFieldDirtyFlag())
            this.setField(srfdomain.getField());
        if(srfdomain.getOld_value_floatDirtyFlag())
            this.setOld_value_float(srfdomain.getOld_value_float());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getMail_message_idDirtyFlag())
            this.setMail_message_id(srfdomain.getMail_message_id());

    }

    public List<Mail_tracking_valueDTO> fromDOPage(List<Mail_tracking_value> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_tracking_valueDTO> dtos=new ArrayList<Mail_tracking_valueDTO>();
        for(Mail_tracking_value domain : poPage) {
            Mail_tracking_valueDTO dto = new Mail_tracking_valueDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

