package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_notification.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_notification;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_notificationDTO]
 */
public class Mail_notificationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_notification__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_notificationIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [FAILURE_REASON]
     *
     */
    @Mail_notificationFailure_reasonDefault(info = "默认规则")
    private String failure_reason;

    @JsonIgnore
    private boolean failure_reasonDirtyFlag;

    /**
     * 属性 [IS_EMAIL]
     *
     */
    @Mail_notificationIs_emailDefault(info = "默认规则")
    private String is_email;

    @JsonIgnore
    private boolean is_emailDirtyFlag;

    /**
     * 属性 [IS_READ]
     *
     */
    @Mail_notificationIs_readDefault(info = "默认规则")
    private String is_read;

    @JsonIgnore
    private boolean is_readDirtyFlag;

    /**
     * 属性 [EMAIL_STATUS]
     *
     */
    @Mail_notificationEmail_statusDefault(info = "默认规则")
    private String email_status;

    @JsonIgnore
    private boolean email_statusDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_notificationDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [FAILURE_TYPE]
     *
     */
    @Mail_notificationFailure_typeDefault(info = "默认规则")
    private String failure_type;

    @JsonIgnore
    private boolean failure_typeDirtyFlag;

    /**
     * 属性 [RES_PARTNER_ID_TEXT]
     *
     */
    @Mail_notificationRes_partner_id_textDefault(info = "默认规则")
    private String res_partner_id_text;

    @JsonIgnore
    private boolean res_partner_id_textDirtyFlag;

    /**
     * 属性 [MAIL_ID]
     *
     */
    @Mail_notificationMail_idDefault(info = "默认规则")
    private Integer mail_id;

    @JsonIgnore
    private boolean mail_idDirtyFlag;

    /**
     * 属性 [MAIL_MESSAGE_ID]
     *
     */
    @Mail_notificationMail_message_idDefault(info = "默认规则")
    private Integer mail_message_id;

    @JsonIgnore
    private boolean mail_message_idDirtyFlag;

    /**
     * 属性 [RES_PARTNER_ID]
     *
     */
    @Mail_notificationRes_partner_idDefault(info = "默认规则")
    private Integer res_partner_id;

    @JsonIgnore
    private boolean res_partner_idDirtyFlag;


    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [FAILURE_REASON]
     */
    @JsonProperty("failure_reason")
    public String getFailure_reason(){
        return failure_reason ;
    }

    /**
     * 设置 [FAILURE_REASON]
     */
    @JsonProperty("failure_reason")
    public void setFailure_reason(String  failure_reason){
        this.failure_reason = failure_reason ;
        this.failure_reasonDirtyFlag = true ;
    }

    /**
     * 获取 [FAILURE_REASON]脏标记
     */
    @JsonIgnore
    public boolean getFailure_reasonDirtyFlag(){
        return failure_reasonDirtyFlag ;
    }

    /**
     * 获取 [IS_EMAIL]
     */
    @JsonProperty("is_email")
    public String getIs_email(){
        return is_email ;
    }

    /**
     * 设置 [IS_EMAIL]
     */
    @JsonProperty("is_email")
    public void setIs_email(String  is_email){
        this.is_email = is_email ;
        this.is_emailDirtyFlag = true ;
    }

    /**
     * 获取 [IS_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getIs_emailDirtyFlag(){
        return is_emailDirtyFlag ;
    }

    /**
     * 获取 [IS_READ]
     */
    @JsonProperty("is_read")
    public String getIs_read(){
        return is_read ;
    }

    /**
     * 设置 [IS_READ]
     */
    @JsonProperty("is_read")
    public void setIs_read(String  is_read){
        this.is_read = is_read ;
        this.is_readDirtyFlag = true ;
    }

    /**
     * 获取 [IS_READ]脏标记
     */
    @JsonIgnore
    public boolean getIs_readDirtyFlag(){
        return is_readDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_STATUS]
     */
    @JsonProperty("email_status")
    public String getEmail_status(){
        return email_status ;
    }

    /**
     * 设置 [EMAIL_STATUS]
     */
    @JsonProperty("email_status")
    public void setEmail_status(String  email_status){
        this.email_status = email_status ;
        this.email_statusDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_STATUS]脏标记
     */
    @JsonIgnore
    public boolean getEmail_statusDirtyFlag(){
        return email_statusDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [FAILURE_TYPE]
     */
    @JsonProperty("failure_type")
    public String getFailure_type(){
        return failure_type ;
    }

    /**
     * 设置 [FAILURE_TYPE]
     */
    @JsonProperty("failure_type")
    public void setFailure_type(String  failure_type){
        this.failure_type = failure_type ;
        this.failure_typeDirtyFlag = true ;
    }

    /**
     * 获取 [FAILURE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getFailure_typeDirtyFlag(){
        return failure_typeDirtyFlag ;
    }

    /**
     * 获取 [RES_PARTNER_ID_TEXT]
     */
    @JsonProperty("res_partner_id_text")
    public String getRes_partner_id_text(){
        return res_partner_id_text ;
    }

    /**
     * 设置 [RES_PARTNER_ID_TEXT]
     */
    @JsonProperty("res_partner_id_text")
    public void setRes_partner_id_text(String  res_partner_id_text){
        this.res_partner_id_text = res_partner_id_text ;
        this.res_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RES_PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRes_partner_id_textDirtyFlag(){
        return res_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [MAIL_ID]
     */
    @JsonProperty("mail_id")
    public Integer getMail_id(){
        return mail_id ;
    }

    /**
     * 设置 [MAIL_ID]
     */
    @JsonProperty("mail_id")
    public void setMail_id(Integer  mail_id){
        this.mail_id = mail_id ;
        this.mail_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_idDirtyFlag(){
        return mail_idDirtyFlag ;
    }

    /**
     * 获取 [MAIL_MESSAGE_ID]
     */
    @JsonProperty("mail_message_id")
    public Integer getMail_message_id(){
        return mail_message_id ;
    }

    /**
     * 设置 [MAIL_MESSAGE_ID]
     */
    @JsonProperty("mail_message_id")
    public void setMail_message_id(Integer  mail_message_id){
        this.mail_message_id = mail_message_id ;
        this.mail_message_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_MESSAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_message_idDirtyFlag(){
        return mail_message_idDirtyFlag ;
    }

    /**
     * 获取 [RES_PARTNER_ID]
     */
    @JsonProperty("res_partner_id")
    public Integer getRes_partner_id(){
        return res_partner_id ;
    }

    /**
     * 设置 [RES_PARTNER_ID]
     */
    @JsonProperty("res_partner_id")
    public void setRes_partner_id(Integer  res_partner_id){
        this.res_partner_id = res_partner_id ;
        this.res_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_partner_idDirtyFlag(){
        return res_partner_idDirtyFlag ;
    }



    public Mail_notification toDO() {
        Mail_notification srfdomain = new Mail_notification();
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getFailure_reasonDirtyFlag())
            srfdomain.setFailure_reason(failure_reason);
        if(getIs_emailDirtyFlag())
            srfdomain.setIs_email(is_email);
        if(getIs_readDirtyFlag())
            srfdomain.setIs_read(is_read);
        if(getEmail_statusDirtyFlag())
            srfdomain.setEmail_status(email_status);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getFailure_typeDirtyFlag())
            srfdomain.setFailure_type(failure_type);
        if(getRes_partner_id_textDirtyFlag())
            srfdomain.setRes_partner_id_text(res_partner_id_text);
        if(getMail_idDirtyFlag())
            srfdomain.setMail_id(mail_id);
        if(getMail_message_idDirtyFlag())
            srfdomain.setMail_message_id(mail_message_id);
        if(getRes_partner_idDirtyFlag())
            srfdomain.setRes_partner_id(res_partner_id);

        return srfdomain;
    }

    public void fromDO(Mail_notification srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getFailure_reasonDirtyFlag())
            this.setFailure_reason(srfdomain.getFailure_reason());
        if(srfdomain.getIs_emailDirtyFlag())
            this.setIs_email(srfdomain.getIs_email());
        if(srfdomain.getIs_readDirtyFlag())
            this.setIs_read(srfdomain.getIs_read());
        if(srfdomain.getEmail_statusDirtyFlag())
            this.setEmail_status(srfdomain.getEmail_status());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getFailure_typeDirtyFlag())
            this.setFailure_type(srfdomain.getFailure_type());
        if(srfdomain.getRes_partner_id_textDirtyFlag())
            this.setRes_partner_id_text(srfdomain.getRes_partner_id_text());
        if(srfdomain.getMail_idDirtyFlag())
            this.setMail_id(srfdomain.getMail_id());
        if(srfdomain.getMail_message_idDirtyFlag())
            this.setMail_message_id(srfdomain.getMail_message_id());
        if(srfdomain.getRes_partner_idDirtyFlag())
            this.setRes_partner_id(srfdomain.getRes_partner_id());

    }

    public List<Mail_notificationDTO> fromDOPage(List<Mail_notification> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_notificationDTO> dtos=new ArrayList<Mail_notificationDTO>();
        for(Mail_notification domain : poPage) {
            Mail_notificationDTO dto = new Mail_notificationDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

