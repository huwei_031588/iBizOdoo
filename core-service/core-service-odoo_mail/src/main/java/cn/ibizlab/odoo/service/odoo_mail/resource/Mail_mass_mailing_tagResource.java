package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_mass_mailing_tagDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_tag;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_tagService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_tagSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_mass_mailing_tag" })
@RestController
@RequestMapping("")
public class Mail_mass_mailing_tagResource {

    @Autowired
    private IMail_mass_mailing_tagService mail_mass_mailing_tagService;

    public IMail_mass_mailing_tagService getMail_mass_mailing_tagService() {
        return this.mail_mass_mailing_tagService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_tag" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_tags/{mail_mass_mailing_tag_id}")

    public ResponseEntity<Mail_mass_mailing_tagDTO> update(@PathVariable("mail_mass_mailing_tag_id") Integer mail_mass_mailing_tag_id, @RequestBody Mail_mass_mailing_tagDTO mail_mass_mailing_tagdto) {
		Mail_mass_mailing_tag domain = mail_mass_mailing_tagdto.toDO();
        domain.setId(mail_mass_mailing_tag_id);
		mail_mass_mailing_tagService.update(domain);
		Mail_mass_mailing_tagDTO dto = new Mail_mass_mailing_tagDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_mass_mailing_tag" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_tags/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_tagDTO> mail_mass_mailing_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_mass_mailing_tag" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_tags/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_mass_mailing_tagDTO> mail_mass_mailing_tagdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_tag" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_tags")

    public ResponseEntity<Mail_mass_mailing_tagDTO> create(@RequestBody Mail_mass_mailing_tagDTO mail_mass_mailing_tagdto) {
        Mail_mass_mailing_tagDTO dto = new Mail_mass_mailing_tagDTO();
        Mail_mass_mailing_tag domain = mail_mass_mailing_tagdto.toDO();
		mail_mass_mailing_tagService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_mass_mailing_tag" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_tags/createBatch")
    public ResponseEntity<Boolean> createBatchMail_mass_mailing_tag(@RequestBody List<Mail_mass_mailing_tagDTO> mail_mass_mailing_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_tag" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_tags/{mail_mass_mailing_tag_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_tag_id") Integer mail_mass_mailing_tag_id) {
        Mail_mass_mailing_tagDTO mail_mass_mailing_tagdto = new Mail_mass_mailing_tagDTO();
		Mail_mass_mailing_tag domain = new Mail_mass_mailing_tag();
		mail_mass_mailing_tagdto.setId(mail_mass_mailing_tag_id);
		domain.setId(mail_mass_mailing_tag_id);
        Boolean rst = mail_mass_mailing_tagService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_tag" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_tags/{mail_mass_mailing_tag_id}")
    public ResponseEntity<Mail_mass_mailing_tagDTO> get(@PathVariable("mail_mass_mailing_tag_id") Integer mail_mass_mailing_tag_id) {
        Mail_mass_mailing_tagDTO dto = new Mail_mass_mailing_tagDTO();
        Mail_mass_mailing_tag domain = mail_mass_mailing_tagService.get(mail_mass_mailing_tag_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_mass_mailing_tag" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_mass_mailing_tags/fetchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_tagDTO>> fetchDefault(Mail_mass_mailing_tagSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_mass_mailing_tagDTO> list = new ArrayList<Mail_mass_mailing_tagDTO>();
        
        Page<Mail_mass_mailing_tag> domains = mail_mass_mailing_tagService.searchDefault(context) ;
        for(Mail_mass_mailing_tag mail_mass_mailing_tag : domains.getContent()){
            Mail_mass_mailing_tagDTO dto = new Mail_mass_mailing_tagDTO();
            dto.fromDO(mail_mass_mailing_tag);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
