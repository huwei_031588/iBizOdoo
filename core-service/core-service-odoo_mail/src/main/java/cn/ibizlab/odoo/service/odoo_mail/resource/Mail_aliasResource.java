package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_aliasDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_aliasService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_aliasSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_alias" })
@RestController
@RequestMapping("")
public class Mail_aliasResource {

    @Autowired
    private IMail_aliasService mail_aliasService;

    public IMail_aliasService getMail_aliasService() {
        return this.mail_aliasService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_alias" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_aliases/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_aliasDTO> mail_aliasdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_alias" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_aliases/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_aliasDTO> mail_aliasdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_alias" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_aliases")

    public ResponseEntity<Mail_aliasDTO> create(@RequestBody Mail_aliasDTO mail_aliasdto) {
        Mail_aliasDTO dto = new Mail_aliasDTO();
        Mail_alias domain = mail_aliasdto.toDO();
		mail_aliasService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_alias" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_aliases/{mail_alias_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_alias_id") Integer mail_alias_id) {
        Mail_aliasDTO mail_aliasdto = new Mail_aliasDTO();
		Mail_alias domain = new Mail_alias();
		mail_aliasdto.setId(mail_alias_id);
		domain.setId(mail_alias_id);
        Boolean rst = mail_aliasService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_alias" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_aliases/{mail_alias_id}")

    public ResponseEntity<Mail_aliasDTO> update(@PathVariable("mail_alias_id") Integer mail_alias_id, @RequestBody Mail_aliasDTO mail_aliasdto) {
		Mail_alias domain = mail_aliasdto.toDO();
        domain.setId(mail_alias_id);
		mail_aliasService.update(domain);
		Mail_aliasDTO dto = new Mail_aliasDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_alias" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_aliases/{mail_alias_id}")
    public ResponseEntity<Mail_aliasDTO> get(@PathVariable("mail_alias_id") Integer mail_alias_id) {
        Mail_aliasDTO dto = new Mail_aliasDTO();
        Mail_alias domain = mail_aliasService.get(mail_alias_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_alias" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_aliases/createBatch")
    public ResponseEntity<Boolean> createBatchMail_alias(@RequestBody List<Mail_aliasDTO> mail_aliasdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_alias" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_aliases/fetchdefault")
	public ResponseEntity<Page<Mail_aliasDTO>> fetchDefault(Mail_aliasSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_aliasDTO> list = new ArrayList<Mail_aliasDTO>();
        
        Page<Mail_alias> domains = mail_aliasService.searchDefault(context) ;
        for(Mail_alias mail_alias : domains.getContent()){
            Mail_aliasDTO dto = new Mail_aliasDTO();
            dto.fromDO(mail_alias);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
