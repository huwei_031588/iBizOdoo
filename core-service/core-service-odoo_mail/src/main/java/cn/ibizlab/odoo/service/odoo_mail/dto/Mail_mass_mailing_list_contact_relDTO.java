package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_mass_mailing_list_contact_rel.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_mass_mailing_list_contact_relDTO]
 */
public class Mail_mass_mailing_list_contact_relDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_mass_mailing_list_contact_relDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [UNSUBSCRIPTION_DATE]
     *
     */
    @Mail_mass_mailing_list_contact_relUnsubscription_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp unsubscription_date;

    @JsonIgnore
    private boolean unsubscription_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_mass_mailing_list_contact_relCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_mass_mailing_list_contact_relWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_mass_mailing_list_contact_relIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [OPT_OUT]
     *
     */
    @Mail_mass_mailing_list_contact_relOpt_outDefault(info = "默认规则")
    private String opt_out;

    @JsonIgnore
    private boolean opt_outDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_mass_mailing_list_contact_rel__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MESSAGE_BOUNCE]
     *
     */
    @Mail_mass_mailing_list_contact_relMessage_bounceDefault(info = "默认规则")
    private Integer message_bounce;

    @JsonIgnore
    private boolean message_bounceDirtyFlag;

    /**
     * 属性 [IS_BLACKLISTED]
     *
     */
    @Mail_mass_mailing_list_contact_relIs_blacklistedDefault(info = "默认规则")
    private String is_blacklisted;

    @JsonIgnore
    private boolean is_blacklistedDirtyFlag;

    /**
     * 属性 [LIST_ID_TEXT]
     *
     */
    @Mail_mass_mailing_list_contact_relList_id_textDefault(info = "默认规则")
    private String list_id_text;

    @JsonIgnore
    private boolean list_id_textDirtyFlag;

    /**
     * 属性 [CONTACT_ID_TEXT]
     *
     */
    @Mail_mass_mailing_list_contact_relContact_id_textDefault(info = "默认规则")
    private String contact_id_text;

    @JsonIgnore
    private boolean contact_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_mass_mailing_list_contact_relCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CONTACT_COUNT]
     *
     */
    @Mail_mass_mailing_list_contact_relContact_countDefault(info = "默认规则")
    private Integer contact_count;

    @JsonIgnore
    private boolean contact_countDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_mass_mailing_list_contact_relWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_mass_mailing_list_contact_relWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_mass_mailing_list_contact_relCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [CONTACT_ID]
     *
     */
    @Mail_mass_mailing_list_contact_relContact_idDefault(info = "默认规则")
    private Integer contact_id;

    @JsonIgnore
    private boolean contact_idDirtyFlag;

    /**
     * 属性 [LIST_ID]
     *
     */
    @Mail_mass_mailing_list_contact_relList_idDefault(info = "默认规则")
    private Integer list_id;

    @JsonIgnore
    private boolean list_idDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [UNSUBSCRIPTION_DATE]
     */
    @JsonProperty("unsubscription_date")
    public Timestamp getUnsubscription_date(){
        return unsubscription_date ;
    }

    /**
     * 设置 [UNSUBSCRIPTION_DATE]
     */
    @JsonProperty("unsubscription_date")
    public void setUnsubscription_date(Timestamp  unsubscription_date){
        this.unsubscription_date = unsubscription_date ;
        this.unsubscription_dateDirtyFlag = true ;
    }

    /**
     * 获取 [UNSUBSCRIPTION_DATE]脏标记
     */
    @JsonIgnore
    public boolean getUnsubscription_dateDirtyFlag(){
        return unsubscription_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [OPT_OUT]
     */
    @JsonProperty("opt_out")
    public String getOpt_out(){
        return opt_out ;
    }

    /**
     * 设置 [OPT_OUT]
     */
    @JsonProperty("opt_out")
    public void setOpt_out(String  opt_out){
        this.opt_out = opt_out ;
        this.opt_outDirtyFlag = true ;
    }

    /**
     * 获取 [OPT_OUT]脏标记
     */
    @JsonIgnore
    public boolean getOpt_outDirtyFlag(){
        return opt_outDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_BOUNCE]
     */
    @JsonProperty("message_bounce")
    public Integer getMessage_bounce(){
        return message_bounce ;
    }

    /**
     * 设置 [MESSAGE_BOUNCE]
     */
    @JsonProperty("message_bounce")
    public void setMessage_bounce(Integer  message_bounce){
        this.message_bounce = message_bounce ;
        this.message_bounceDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_BOUNCE]脏标记
     */
    @JsonIgnore
    public boolean getMessage_bounceDirtyFlag(){
        return message_bounceDirtyFlag ;
    }

    /**
     * 获取 [IS_BLACKLISTED]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return is_blacklisted ;
    }

    /**
     * 设置 [IS_BLACKLISTED]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_BLACKLISTED]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return is_blacklistedDirtyFlag ;
    }

    /**
     * 获取 [LIST_ID_TEXT]
     */
    @JsonProperty("list_id_text")
    public String getList_id_text(){
        return list_id_text ;
    }

    /**
     * 设置 [LIST_ID_TEXT]
     */
    @JsonProperty("list_id_text")
    public void setList_id_text(String  list_id_text){
        this.list_id_text = list_id_text ;
        this.list_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LIST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getList_id_textDirtyFlag(){
        return list_id_textDirtyFlag ;
    }

    /**
     * 获取 [CONTACT_ID_TEXT]
     */
    @JsonProperty("contact_id_text")
    public String getContact_id_text(){
        return contact_id_text ;
    }

    /**
     * 设置 [CONTACT_ID_TEXT]
     */
    @JsonProperty("contact_id_text")
    public void setContact_id_text(String  contact_id_text){
        this.contact_id_text = contact_id_text ;
        this.contact_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CONTACT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getContact_id_textDirtyFlag(){
        return contact_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CONTACT_COUNT]
     */
    @JsonProperty("contact_count")
    public Integer getContact_count(){
        return contact_count ;
    }

    /**
     * 设置 [CONTACT_COUNT]
     */
    @JsonProperty("contact_count")
    public void setContact_count(Integer  contact_count){
        this.contact_count = contact_count ;
        this.contact_countDirtyFlag = true ;
    }

    /**
     * 获取 [CONTACT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getContact_countDirtyFlag(){
        return contact_countDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [CONTACT_ID]
     */
    @JsonProperty("contact_id")
    public Integer getContact_id(){
        return contact_id ;
    }

    /**
     * 设置 [CONTACT_ID]
     */
    @JsonProperty("contact_id")
    public void setContact_id(Integer  contact_id){
        this.contact_id = contact_id ;
        this.contact_idDirtyFlag = true ;
    }

    /**
     * 获取 [CONTACT_ID]脏标记
     */
    @JsonIgnore
    public boolean getContact_idDirtyFlag(){
        return contact_idDirtyFlag ;
    }

    /**
     * 获取 [LIST_ID]
     */
    @JsonProperty("list_id")
    public Integer getList_id(){
        return list_id ;
    }

    /**
     * 设置 [LIST_ID]
     */
    @JsonProperty("list_id")
    public void setList_id(Integer  list_id){
        this.list_id = list_id ;
        this.list_idDirtyFlag = true ;
    }

    /**
     * 获取 [LIST_ID]脏标记
     */
    @JsonIgnore
    public boolean getList_idDirtyFlag(){
        return list_idDirtyFlag ;
    }



    public Mail_mass_mailing_list_contact_rel toDO() {
        Mail_mass_mailing_list_contact_rel srfdomain = new Mail_mass_mailing_list_contact_rel();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getUnsubscription_dateDirtyFlag())
            srfdomain.setUnsubscription_date(unsubscription_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getOpt_outDirtyFlag())
            srfdomain.setOpt_out(opt_out);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMessage_bounceDirtyFlag())
            srfdomain.setMessage_bounce(message_bounce);
        if(getIs_blacklistedDirtyFlag())
            srfdomain.setIs_blacklisted(is_blacklisted);
        if(getList_id_textDirtyFlag())
            srfdomain.setList_id_text(list_id_text);
        if(getContact_id_textDirtyFlag())
            srfdomain.setContact_id_text(contact_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getContact_countDirtyFlag())
            srfdomain.setContact_count(contact_count);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getContact_idDirtyFlag())
            srfdomain.setContact_id(contact_id);
        if(getList_idDirtyFlag())
            srfdomain.setList_id(list_id);

        return srfdomain;
    }

    public void fromDO(Mail_mass_mailing_list_contact_rel srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getUnsubscription_dateDirtyFlag())
            this.setUnsubscription_date(srfdomain.getUnsubscription_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getOpt_outDirtyFlag())
            this.setOpt_out(srfdomain.getOpt_out());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMessage_bounceDirtyFlag())
            this.setMessage_bounce(srfdomain.getMessage_bounce());
        if(srfdomain.getIs_blacklistedDirtyFlag())
            this.setIs_blacklisted(srfdomain.getIs_blacklisted());
        if(srfdomain.getList_id_textDirtyFlag())
            this.setList_id_text(srfdomain.getList_id_text());
        if(srfdomain.getContact_id_textDirtyFlag())
            this.setContact_id_text(srfdomain.getContact_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getContact_countDirtyFlag())
            this.setContact_count(srfdomain.getContact_count());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getContact_idDirtyFlag())
            this.setContact_id(srfdomain.getContact_id());
        if(srfdomain.getList_idDirtyFlag())
            this.setList_id(srfdomain.getList_id());

    }

    public List<Mail_mass_mailing_list_contact_relDTO> fromDOPage(List<Mail_mass_mailing_list_contact_rel> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_mass_mailing_list_contact_relDTO> dtos=new ArrayList<Mail_mass_mailing_list_contact_relDTO>();
        for(Mail_mass_mailing_list_contact_rel domain : poPage) {
            Mail_mass_mailing_list_contact_relDTO dto = new Mail_mass_mailing_list_contact_relDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

