package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_mass_mailing_testDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_test;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_testService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_testSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_mass_mailing_test" })
@RestController
@RequestMapping("")
public class Mail_mass_mailing_testResource {

    @Autowired
    private IMail_mass_mailing_testService mail_mass_mailing_testService;

    public IMail_mass_mailing_testService getMail_mass_mailing_testService() {
        return this.mail_mass_mailing_testService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_test" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_tests/{mail_mass_mailing_test_id}")

    public ResponseEntity<Mail_mass_mailing_testDTO> update(@PathVariable("mail_mass_mailing_test_id") Integer mail_mass_mailing_test_id, @RequestBody Mail_mass_mailing_testDTO mail_mass_mailing_testdto) {
		Mail_mass_mailing_test domain = mail_mass_mailing_testdto.toDO();
        domain.setId(mail_mass_mailing_test_id);
		mail_mass_mailing_testService.update(domain);
		Mail_mass_mailing_testDTO dto = new Mail_mass_mailing_testDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_test" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_tests")

    public ResponseEntity<Mail_mass_mailing_testDTO> create(@RequestBody Mail_mass_mailing_testDTO mail_mass_mailing_testdto) {
        Mail_mass_mailing_testDTO dto = new Mail_mass_mailing_testDTO();
        Mail_mass_mailing_test domain = mail_mass_mailing_testdto.toDO();
		mail_mass_mailing_testService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_mass_mailing_test" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_tests/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_testDTO> mail_mass_mailing_testdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_mass_mailing_test" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_tests/createBatch")
    public ResponseEntity<Boolean> createBatchMail_mass_mailing_test(@RequestBody List<Mail_mass_mailing_testDTO> mail_mass_mailing_testdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_test" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_tests/{mail_mass_mailing_test_id}")
    public ResponseEntity<Mail_mass_mailing_testDTO> get(@PathVariable("mail_mass_mailing_test_id") Integer mail_mass_mailing_test_id) {
        Mail_mass_mailing_testDTO dto = new Mail_mass_mailing_testDTO();
        Mail_mass_mailing_test domain = mail_mass_mailing_testService.get(mail_mass_mailing_test_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_test" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_tests/{mail_mass_mailing_test_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_test_id") Integer mail_mass_mailing_test_id) {
        Mail_mass_mailing_testDTO mail_mass_mailing_testdto = new Mail_mass_mailing_testDTO();
		Mail_mass_mailing_test domain = new Mail_mass_mailing_test();
		mail_mass_mailing_testdto.setId(mail_mass_mailing_test_id);
		domain.setId(mail_mass_mailing_test_id);
        Boolean rst = mail_mass_mailing_testService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_mass_mailing_test" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_tests/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_mass_mailing_testDTO> mail_mass_mailing_testdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_mass_mailing_test" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_mass_mailing_tests/fetchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_testDTO>> fetchDefault(Mail_mass_mailing_testSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_mass_mailing_testDTO> list = new ArrayList<Mail_mass_mailing_testDTO>();
        
        Page<Mail_mass_mailing_test> domains = mail_mass_mailing_testService.searchDefault(context) ;
        for(Mail_mass_mailing_test mail_mass_mailing_test : domains.getContent()){
            Mail_mass_mailing_testDTO dto = new Mail_mass_mailing_testDTO();
            dto.fromDO(mail_mass_mailing_test);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
