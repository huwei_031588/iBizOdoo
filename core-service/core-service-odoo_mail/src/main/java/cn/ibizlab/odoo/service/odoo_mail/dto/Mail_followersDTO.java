package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_followers.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_followers;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_followersDTO]
 */
public class Mail_followersDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_followersDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_followers__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_followersIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [RES_ID]
     *
     */
    @Mail_followersRes_idDefault(info = "默认规则")
    private Integer res_id;

    @JsonIgnore
    private boolean res_idDirtyFlag;

    /**
     * 属性 [SUBTYPE_IDS]
     *
     */
    @Mail_followersSubtype_idsDefault(info = "默认规则")
    private String subtype_ids;

    @JsonIgnore
    private boolean subtype_idsDirtyFlag;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @Mail_followersRes_modelDefault(info = "默认规则")
    private String res_model;

    @JsonIgnore
    private boolean res_modelDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Mail_followersPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CHANNEL_ID_TEXT]
     *
     */
    @Mail_followersChannel_id_textDefault(info = "默认规则")
    private String channel_id_text;

    @JsonIgnore
    private boolean channel_id_textDirtyFlag;

    /**
     * 属性 [CHANNEL_ID]
     *
     */
    @Mail_followersChannel_idDefault(info = "默认规则")
    private Integer channel_id;

    @JsonIgnore
    private boolean channel_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Mail_followersPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [RES_ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return res_id ;
    }

    /**
     * 设置 [RES_ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return res_idDirtyFlag ;
    }

    /**
     * 获取 [SUBTYPE_IDS]
     */
    @JsonProperty("subtype_ids")
    public String getSubtype_ids(){
        return subtype_ids ;
    }

    /**
     * 设置 [SUBTYPE_IDS]
     */
    @JsonProperty("subtype_ids")
    public void setSubtype_ids(String  subtype_ids){
        this.subtype_ids = subtype_ids ;
        this.subtype_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTYPE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_idsDirtyFlag(){
        return subtype_idsDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return res_model ;
    }

    /**
     * 设置 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return res_modelDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_ID_TEXT]
     */
    @JsonProperty("channel_id_text")
    public String getChannel_id_text(){
        return channel_id_text ;
    }

    /**
     * 设置 [CHANNEL_ID_TEXT]
     */
    @JsonProperty("channel_id_text")
    public void setChannel_id_text(String  channel_id_text){
        this.channel_id_text = channel_id_text ;
        this.channel_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChannel_id_textDirtyFlag(){
        return channel_id_textDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_ID]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return channel_id ;
    }

    /**
     * 设置 [CHANNEL_ID]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return channel_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }



    public Mail_followers toDO() {
        Mail_followers srfdomain = new Mail_followers();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getRes_idDirtyFlag())
            srfdomain.setRes_id(res_id);
        if(getSubtype_idsDirtyFlag())
            srfdomain.setSubtype_ids(subtype_ids);
        if(getRes_modelDirtyFlag())
            srfdomain.setRes_model(res_model);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getChannel_id_textDirtyFlag())
            srfdomain.setChannel_id_text(channel_id_text);
        if(getChannel_idDirtyFlag())
            srfdomain.setChannel_id(channel_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);

        return srfdomain;
    }

    public void fromDO(Mail_followers srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getRes_idDirtyFlag())
            this.setRes_id(srfdomain.getRes_id());
        if(srfdomain.getSubtype_idsDirtyFlag())
            this.setSubtype_ids(srfdomain.getSubtype_ids());
        if(srfdomain.getRes_modelDirtyFlag())
            this.setRes_model(srfdomain.getRes_model());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getChannel_id_textDirtyFlag())
            this.setChannel_id_text(srfdomain.getChannel_id_text());
        if(srfdomain.getChannel_idDirtyFlag())
            this.setChannel_id(srfdomain.getChannel_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());

    }

    public List<Mail_followersDTO> fromDOPage(List<Mail_followers> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_followersDTO> dtos=new ArrayList<Mail_followersDTO>();
        for(Mail_followers domain : poPage) {
            Mail_followersDTO dto = new Mail_followersDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

