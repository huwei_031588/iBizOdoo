package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_mail_statistics.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_mail_statisticsDTO]
 */
public class Mail_mail_statisticsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SCHEDULED]
     *
     */
    @Mail_mail_statisticsScheduledDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp scheduled;

    @JsonIgnore
    private boolean scheduledDirtyFlag;

    /**
     * 属性 [CLICKED]
     *
     */
    @Mail_mail_statisticsClickedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp clicked;

    @JsonIgnore
    private boolean clickedDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_mail_statisticsDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [EMAIL]
     *
     */
    @Mail_mail_statisticsEmailDefault(info = "默认规则")
    private String email;

    @JsonIgnore
    private boolean emailDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_mail_statisticsCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [STATE_UPDATE]
     *
     */
    @Mail_mail_statisticsState_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp state_update;

    @JsonIgnore
    private boolean state_updateDirtyFlag;

    /**
     * 属性 [REPLIED]
     *
     */
    @Mail_mail_statisticsRepliedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp replied;

    @JsonIgnore
    private boolean repliedDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_mail_statisticsWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MODEL]
     *
     */
    @Mail_mail_statisticsModelDefault(info = "默认规则")
    private String model;

    @JsonIgnore
    private boolean modelDirtyFlag;

    /**
     * 属性 [MESSAGE_ID]
     *
     */
    @Mail_mail_statisticsMessage_idDefault(info = "默认规则")
    private String message_id;

    @JsonIgnore
    private boolean message_idDirtyFlag;

    /**
     * 属性 [EXCEPTION]
     *
     */
    @Mail_mail_statisticsExceptionDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp exception;

    @JsonIgnore
    private boolean exceptionDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_mail_statistics__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [BOUNCED]
     *
     */
    @Mail_mail_statisticsBouncedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp bounced;

    @JsonIgnore
    private boolean bouncedDirtyFlag;

    /**
     * 属性 [IGNORED]
     *
     */
    @Mail_mail_statisticsIgnoredDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp ignored;

    @JsonIgnore
    private boolean ignoredDirtyFlag;

    /**
     * 属性 [RES_ID]
     *
     */
    @Mail_mail_statisticsRes_idDefault(info = "默认规则")
    private Integer res_id;

    @JsonIgnore
    private boolean res_idDirtyFlag;

    /**
     * 属性 [SENT]
     *
     */
    @Mail_mail_statisticsSentDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp sent;

    @JsonIgnore
    private boolean sentDirtyFlag;

    /**
     * 属性 [MAIL_MAIL_ID_INT]
     *
     */
    @Mail_mail_statisticsMail_mail_id_intDefault(info = "默认规则")
    private Integer mail_mail_id_int;

    @JsonIgnore
    private boolean mail_mail_id_intDirtyFlag;

    /**
     * 属性 [LINKS_CLICK_IDS]
     *
     */
    @Mail_mail_statisticsLinks_click_idsDefault(info = "默认规则")
    private String links_click_ids;

    @JsonIgnore
    private boolean links_click_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_mail_statisticsIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Mail_mail_statisticsStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [OPENED]
     *
     */
    @Mail_mail_statisticsOpenedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp opened;

    @JsonIgnore
    private boolean openedDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_mail_statisticsCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_mail_statisticsWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     *
     */
    @Mail_mail_statisticsMass_mailing_campaign_id_textDefault(info = "默认规则")
    private String mass_mailing_campaign_id_text;

    @JsonIgnore
    private boolean mass_mailing_campaign_id_textDirtyFlag;

    /**
     * 属性 [MASS_MAILING_ID_TEXT]
     *
     */
    @Mail_mail_statisticsMass_mailing_id_textDefault(info = "默认规则")
    private String mass_mailing_id_text;

    @JsonIgnore
    private boolean mass_mailing_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_mail_statisticsWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID]
     *
     */
    @Mail_mail_statisticsMass_mailing_campaign_idDefault(info = "默认规则")
    private Integer mass_mailing_campaign_id;

    @JsonIgnore
    private boolean mass_mailing_campaign_idDirtyFlag;

    /**
     * 属性 [MAIL_MAIL_ID]
     *
     */
    @Mail_mail_statisticsMail_mail_idDefault(info = "默认规则")
    private Integer mail_mail_id;

    @JsonIgnore
    private boolean mail_mail_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_mail_statisticsCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [MASS_MAILING_ID]
     *
     */
    @Mail_mail_statisticsMass_mailing_idDefault(info = "默认规则")
    private Integer mass_mailing_id;

    @JsonIgnore
    private boolean mass_mailing_idDirtyFlag;


    /**
     * 获取 [SCHEDULED]
     */
    @JsonProperty("scheduled")
    public Timestamp getScheduled(){
        return scheduled ;
    }

    /**
     * 设置 [SCHEDULED]
     */
    @JsonProperty("scheduled")
    public void setScheduled(Timestamp  scheduled){
        this.scheduled = scheduled ;
        this.scheduledDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULED]脏标记
     */
    @JsonIgnore
    public boolean getScheduledDirtyFlag(){
        return scheduledDirtyFlag ;
    }

    /**
     * 获取 [CLICKED]
     */
    @JsonProperty("clicked")
    public Timestamp getClicked(){
        return clicked ;
    }

    /**
     * 设置 [CLICKED]
     */
    @JsonProperty("clicked")
    public void setClicked(Timestamp  clicked){
        this.clicked = clicked ;
        this.clickedDirtyFlag = true ;
    }

    /**
     * 获取 [CLICKED]脏标记
     */
    @JsonIgnore
    public boolean getClickedDirtyFlag(){
        return clickedDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [EMAIL]
     */
    @JsonProperty("email")
    public String getEmail(){
        return email ;
    }

    /**
     * 设置 [EMAIL]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return emailDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [STATE_UPDATE]
     */
    @JsonProperty("state_update")
    public Timestamp getState_update(){
        return state_update ;
    }

    /**
     * 设置 [STATE_UPDATE]
     */
    @JsonProperty("state_update")
    public void setState_update(Timestamp  state_update){
        this.state_update = state_update ;
        this.state_updateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean getState_updateDirtyFlag(){
        return state_updateDirtyFlag ;
    }

    /**
     * 获取 [REPLIED]
     */
    @JsonProperty("replied")
    public Timestamp getReplied(){
        return replied ;
    }

    /**
     * 设置 [REPLIED]
     */
    @JsonProperty("replied")
    public void setReplied(Timestamp  replied){
        this.replied = replied ;
        this.repliedDirtyFlag = true ;
    }

    /**
     * 获取 [REPLIED]脏标记
     */
    @JsonIgnore
    public boolean getRepliedDirtyFlag(){
        return repliedDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MODEL]
     */
    @JsonProperty("model")
    public String getModel(){
        return model ;
    }

    /**
     * 设置 [MODEL]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return modelDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ID]
     */
    @JsonProperty("message_id")
    public String getMessage_id(){
        return message_id ;
    }

    /**
     * 设置 [MESSAGE_ID]
     */
    @JsonProperty("message_id")
    public void setMessage_id(String  message_id){
        this.message_id = message_id ;
        this.message_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idDirtyFlag(){
        return message_idDirtyFlag ;
    }

    /**
     * 获取 [EXCEPTION]
     */
    @JsonProperty("exception")
    public Timestamp getException(){
        return exception ;
    }

    /**
     * 设置 [EXCEPTION]
     */
    @JsonProperty("exception")
    public void setException(Timestamp  exception){
        this.exception = exception ;
        this.exceptionDirtyFlag = true ;
    }

    /**
     * 获取 [EXCEPTION]脏标记
     */
    @JsonIgnore
    public boolean getExceptionDirtyFlag(){
        return exceptionDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [BOUNCED]
     */
    @JsonProperty("bounced")
    public Timestamp getBounced(){
        return bounced ;
    }

    /**
     * 设置 [BOUNCED]
     */
    @JsonProperty("bounced")
    public void setBounced(Timestamp  bounced){
        this.bounced = bounced ;
        this.bouncedDirtyFlag = true ;
    }

    /**
     * 获取 [BOUNCED]脏标记
     */
    @JsonIgnore
    public boolean getBouncedDirtyFlag(){
        return bouncedDirtyFlag ;
    }

    /**
     * 获取 [IGNORED]
     */
    @JsonProperty("ignored")
    public Timestamp getIgnored(){
        return ignored ;
    }

    /**
     * 设置 [IGNORED]
     */
    @JsonProperty("ignored")
    public void setIgnored(Timestamp  ignored){
        this.ignored = ignored ;
        this.ignoredDirtyFlag = true ;
    }

    /**
     * 获取 [IGNORED]脏标记
     */
    @JsonIgnore
    public boolean getIgnoredDirtyFlag(){
        return ignoredDirtyFlag ;
    }

    /**
     * 获取 [RES_ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return res_id ;
    }

    /**
     * 设置 [RES_ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return res_idDirtyFlag ;
    }

    /**
     * 获取 [SENT]
     */
    @JsonProperty("sent")
    public Timestamp getSent(){
        return sent ;
    }

    /**
     * 设置 [SENT]
     */
    @JsonProperty("sent")
    public void setSent(Timestamp  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

    /**
     * 获取 [SENT]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return sentDirtyFlag ;
    }

    /**
     * 获取 [MAIL_MAIL_ID_INT]
     */
    @JsonProperty("mail_mail_id_int")
    public Integer getMail_mail_id_int(){
        return mail_mail_id_int ;
    }

    /**
     * 设置 [MAIL_MAIL_ID_INT]
     */
    @JsonProperty("mail_mail_id_int")
    public void setMail_mail_id_int(Integer  mail_mail_id_int){
        this.mail_mail_id_int = mail_mail_id_int ;
        this.mail_mail_id_intDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_MAIL_ID_INT]脏标记
     */
    @JsonIgnore
    public boolean getMail_mail_id_intDirtyFlag(){
        return mail_mail_id_intDirtyFlag ;
    }

    /**
     * 获取 [LINKS_CLICK_IDS]
     */
    @JsonProperty("links_click_ids")
    public String getLinks_click_ids(){
        return links_click_ids ;
    }

    /**
     * 设置 [LINKS_CLICK_IDS]
     */
    @JsonProperty("links_click_ids")
    public void setLinks_click_ids(String  links_click_ids){
        this.links_click_ids = links_click_ids ;
        this.links_click_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LINKS_CLICK_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLinks_click_idsDirtyFlag(){
        return links_click_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [OPENED]
     */
    @JsonProperty("opened")
    public Timestamp getOpened(){
        return opened ;
    }

    /**
     * 设置 [OPENED]
     */
    @JsonProperty("opened")
    public void setOpened(Timestamp  opened){
        this.opened = opened ;
        this.openedDirtyFlag = true ;
    }

    /**
     * 获取 [OPENED]脏标记
     */
    @JsonIgnore
    public boolean getOpenedDirtyFlag(){
        return openedDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("mass_mailing_campaign_id_text")
    public String getMass_mailing_campaign_id_text(){
        return mass_mailing_campaign_id_text ;
    }

    /**
     * 设置 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("mass_mailing_campaign_id_text")
    public void setMass_mailing_campaign_id_text(String  mass_mailing_campaign_id_text){
        this.mass_mailing_campaign_id_text = mass_mailing_campaign_id_text ;
        this.mass_mailing_campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_id_textDirtyFlag(){
        return mass_mailing_campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_ID_TEXT]
     */
    @JsonProperty("mass_mailing_id_text")
    public String getMass_mailing_id_text(){
        return mass_mailing_id_text ;
    }

    /**
     * 设置 [MASS_MAILING_ID_TEXT]
     */
    @JsonProperty("mass_mailing_id_text")
    public void setMass_mailing_id_text(String  mass_mailing_id_text){
        this.mass_mailing_id_text = mass_mailing_id_text ;
        this.mass_mailing_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_id_textDirtyFlag(){
        return mass_mailing_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public Integer getMass_mailing_campaign_id(){
        return mass_mailing_campaign_id ;
    }

    /**
     * 设置 [MASS_MAILING_CAMPAIGN_ID]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public void setMass_mailing_campaign_id(Integer  mass_mailing_campaign_id){
        this.mass_mailing_campaign_id = mass_mailing_campaign_id ;
        this.mass_mailing_campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_idDirtyFlag(){
        return mass_mailing_campaign_idDirtyFlag ;
    }

    /**
     * 获取 [MAIL_MAIL_ID]
     */
    @JsonProperty("mail_mail_id")
    public Integer getMail_mail_id(){
        return mail_mail_id ;
    }

    /**
     * 设置 [MAIL_MAIL_ID]
     */
    @JsonProperty("mail_mail_id")
    public void setMail_mail_id(Integer  mail_mail_id){
        this.mail_mail_id = mail_mail_id ;
        this.mail_mail_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_MAIL_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_mail_idDirtyFlag(){
        return mail_mail_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_ID]
     */
    @JsonProperty("mass_mailing_id")
    public Integer getMass_mailing_id(){
        return mass_mailing_id ;
    }

    /**
     * 设置 [MASS_MAILING_ID]
     */
    @JsonProperty("mass_mailing_id")
    public void setMass_mailing_id(Integer  mass_mailing_id){
        this.mass_mailing_id = mass_mailing_id ;
        this.mass_mailing_idDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_ID]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_idDirtyFlag(){
        return mass_mailing_idDirtyFlag ;
    }



    public Mail_mail_statistics toDO() {
        Mail_mail_statistics srfdomain = new Mail_mail_statistics();
        if(getScheduledDirtyFlag())
            srfdomain.setScheduled(scheduled);
        if(getClickedDirtyFlag())
            srfdomain.setClicked(clicked);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getEmailDirtyFlag())
            srfdomain.setEmail(email);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getState_updateDirtyFlag())
            srfdomain.setState_update(state_update);
        if(getRepliedDirtyFlag())
            srfdomain.setReplied(replied);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getModelDirtyFlag())
            srfdomain.setModel(model);
        if(getMessage_idDirtyFlag())
            srfdomain.setMessage_id(message_id);
        if(getExceptionDirtyFlag())
            srfdomain.setException(exception);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getBouncedDirtyFlag())
            srfdomain.setBounced(bounced);
        if(getIgnoredDirtyFlag())
            srfdomain.setIgnored(ignored);
        if(getRes_idDirtyFlag())
            srfdomain.setRes_id(res_id);
        if(getSentDirtyFlag())
            srfdomain.setSent(sent);
        if(getMail_mail_id_intDirtyFlag())
            srfdomain.setMail_mail_id_int(mail_mail_id_int);
        if(getLinks_click_idsDirtyFlag())
            srfdomain.setLinks_click_ids(links_click_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getOpenedDirtyFlag())
            srfdomain.setOpened(opened);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getMass_mailing_campaign_id_textDirtyFlag())
            srfdomain.setMass_mailing_campaign_id_text(mass_mailing_campaign_id_text);
        if(getMass_mailing_id_textDirtyFlag())
            srfdomain.setMass_mailing_id_text(mass_mailing_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getMass_mailing_campaign_idDirtyFlag())
            srfdomain.setMass_mailing_campaign_id(mass_mailing_campaign_id);
        if(getMail_mail_idDirtyFlag())
            srfdomain.setMail_mail_id(mail_mail_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getMass_mailing_idDirtyFlag())
            srfdomain.setMass_mailing_id(mass_mailing_id);

        return srfdomain;
    }

    public void fromDO(Mail_mail_statistics srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getScheduledDirtyFlag())
            this.setScheduled(srfdomain.getScheduled());
        if(srfdomain.getClickedDirtyFlag())
            this.setClicked(srfdomain.getClicked());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getEmailDirtyFlag())
            this.setEmail(srfdomain.getEmail());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getState_updateDirtyFlag())
            this.setState_update(srfdomain.getState_update());
        if(srfdomain.getRepliedDirtyFlag())
            this.setReplied(srfdomain.getReplied());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getModelDirtyFlag())
            this.setModel(srfdomain.getModel());
        if(srfdomain.getMessage_idDirtyFlag())
            this.setMessage_id(srfdomain.getMessage_id());
        if(srfdomain.getExceptionDirtyFlag())
            this.setException(srfdomain.getException());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getBouncedDirtyFlag())
            this.setBounced(srfdomain.getBounced());
        if(srfdomain.getIgnoredDirtyFlag())
            this.setIgnored(srfdomain.getIgnored());
        if(srfdomain.getRes_idDirtyFlag())
            this.setRes_id(srfdomain.getRes_id());
        if(srfdomain.getSentDirtyFlag())
            this.setSent(srfdomain.getSent());
        if(srfdomain.getMail_mail_id_intDirtyFlag())
            this.setMail_mail_id_int(srfdomain.getMail_mail_id_int());
        if(srfdomain.getLinks_click_idsDirtyFlag())
            this.setLinks_click_ids(srfdomain.getLinks_click_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getOpenedDirtyFlag())
            this.setOpened(srfdomain.getOpened());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getMass_mailing_campaign_id_textDirtyFlag())
            this.setMass_mailing_campaign_id_text(srfdomain.getMass_mailing_campaign_id_text());
        if(srfdomain.getMass_mailing_id_textDirtyFlag())
            this.setMass_mailing_id_text(srfdomain.getMass_mailing_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getMass_mailing_campaign_idDirtyFlag())
            this.setMass_mailing_campaign_id(srfdomain.getMass_mailing_campaign_id());
        if(srfdomain.getMail_mail_idDirtyFlag())
            this.setMail_mail_id(srfdomain.getMail_mail_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getMass_mailing_idDirtyFlag())
            this.setMass_mailing_id(srfdomain.getMass_mailing_id());

    }

    public List<Mail_mail_statisticsDTO> fromDOPage(List<Mail_mail_statistics> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_mail_statisticsDTO> dtos=new ArrayList<Mail_mail_statisticsDTO>();
        for(Mail_mail_statistics domain : poPage) {
            Mail_mail_statisticsDTO dto = new Mail_mail_statisticsDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

