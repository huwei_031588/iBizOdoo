package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_resend_message.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_message;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_resend_messageDTO]
 */
public class Mail_resend_messageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Mail_resend_messageIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_resend_messageDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [NOTIFICATION_IDS]
     *
     */
    @Mail_resend_messageNotification_idsDefault(info = "默认规则")
    private String notification_ids;

    @JsonIgnore
    private boolean notification_idsDirtyFlag;

    /**
     * 属性 [HAS_CANCEL]
     *
     */
    @Mail_resend_messageHas_cancelDefault(info = "默认规则")
    private String has_cancel;

    @JsonIgnore
    private boolean has_cancelDirtyFlag;

    /**
     * 属性 [PARTNER_READONLY]
     *
     */
    @Mail_resend_messagePartner_readonlyDefault(info = "默认规则")
    private String partner_readonly;

    @JsonIgnore
    private boolean partner_readonlyDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_resend_messageCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @Mail_resend_messagePartner_idsDefault(info = "默认规则")
    private String partner_ids;

    @JsonIgnore
    private boolean partner_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_resend_messageWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_resend_message__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_resend_messageWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_resend_messageCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_resend_messageWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_resend_messageCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [MAIL_MESSAGE_ID]
     *
     */
    @Mail_resend_messageMail_message_idDefault(info = "默认规则")
    private Integer mail_message_id;

    @JsonIgnore
    private boolean mail_message_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [NOTIFICATION_IDS]
     */
    @JsonProperty("notification_ids")
    public String getNotification_ids(){
        return notification_ids ;
    }

    /**
     * 设置 [NOTIFICATION_IDS]
     */
    @JsonProperty("notification_ids")
    public void setNotification_ids(String  notification_ids){
        this.notification_ids = notification_ids ;
        this.notification_idsDirtyFlag = true ;
    }

    /**
     * 获取 [NOTIFICATION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getNotification_idsDirtyFlag(){
        return notification_idsDirtyFlag ;
    }

    /**
     * 获取 [HAS_CANCEL]
     */
    @JsonProperty("has_cancel")
    public String getHas_cancel(){
        return has_cancel ;
    }

    /**
     * 设置 [HAS_CANCEL]
     */
    @JsonProperty("has_cancel")
    public void setHas_cancel(String  has_cancel){
        this.has_cancel = has_cancel ;
        this.has_cancelDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_CANCEL]脏标记
     */
    @JsonIgnore
    public boolean getHas_cancelDirtyFlag(){
        return has_cancelDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_READONLY]
     */
    @JsonProperty("partner_readonly")
    public String getPartner_readonly(){
        return partner_readonly ;
    }

    /**
     * 设置 [PARTNER_READONLY]
     */
    @JsonProperty("partner_readonly")
    public void setPartner_readonly(String  partner_readonly){
        this.partner_readonly = partner_readonly ;
        this.partner_readonlyDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_READONLY]脏标记
     */
    @JsonIgnore
    public boolean getPartner_readonlyDirtyFlag(){
        return partner_readonlyDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_IDS]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return partner_ids ;
    }

    /**
     * 设置 [PARTNER_IDS]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return partner_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [MAIL_MESSAGE_ID]
     */
    @JsonProperty("mail_message_id")
    public Integer getMail_message_id(){
        return mail_message_id ;
    }

    /**
     * 设置 [MAIL_MESSAGE_ID]
     */
    @JsonProperty("mail_message_id")
    public void setMail_message_id(Integer  mail_message_id){
        this.mail_message_id = mail_message_id ;
        this.mail_message_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_MESSAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_message_idDirtyFlag(){
        return mail_message_idDirtyFlag ;
    }



    public Mail_resend_message toDO() {
        Mail_resend_message srfdomain = new Mail_resend_message();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getNotification_idsDirtyFlag())
            srfdomain.setNotification_ids(notification_ids);
        if(getHas_cancelDirtyFlag())
            srfdomain.setHas_cancel(has_cancel);
        if(getPartner_readonlyDirtyFlag())
            srfdomain.setPartner_readonly(partner_readonly);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getPartner_idsDirtyFlag())
            srfdomain.setPartner_ids(partner_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getMail_message_idDirtyFlag())
            srfdomain.setMail_message_id(mail_message_id);

        return srfdomain;
    }

    public void fromDO(Mail_resend_message srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getNotification_idsDirtyFlag())
            this.setNotification_ids(srfdomain.getNotification_ids());
        if(srfdomain.getHas_cancelDirtyFlag())
            this.setHas_cancel(srfdomain.getHas_cancel());
        if(srfdomain.getPartner_readonlyDirtyFlag())
            this.setPartner_readonly(srfdomain.getPartner_readonly());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getPartner_idsDirtyFlag())
            this.setPartner_ids(srfdomain.getPartner_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getMail_message_idDirtyFlag())
            this.setMail_message_id(srfdomain.getMail_message_id());

    }

    public List<Mail_resend_messageDTO> fromDOPage(List<Mail_resend_message> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_resend_messageDTO> dtos=new ArrayList<Mail_resend_messageDTO>();
        for(Mail_resend_message domain : poPage) {
            Mail_resend_messageDTO dto = new Mail_resend_messageDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

