package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_statistics_report.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_statistics_report;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_statistics_reportDTO]
 */
public class Mail_statistics_reportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CLICKED]
     *
     */
    @Mail_statistics_reportClickedDefault(info = "默认规则")
    private Integer clicked;

    @JsonIgnore
    private boolean clickedDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_statistics_report__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_statistics_reportIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_statistics_reportDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [BOUNCED]
     *
     */
    @Mail_statistics_reportBouncedDefault(info = "默认规则")
    private Integer bounced;

    @JsonIgnore
    private boolean bouncedDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Mail_statistics_reportStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [REPLIED]
     *
     */
    @Mail_statistics_reportRepliedDefault(info = "默认规则")
    private Integer replied;

    @JsonIgnore
    private boolean repliedDirtyFlag;

    /**
     * 属性 [CAMPAIGN]
     *
     */
    @Mail_statistics_reportCampaignDefault(info = "默认规则")
    private String campaign;

    @JsonIgnore
    private boolean campaignDirtyFlag;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @Mail_statistics_reportScheduled_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp scheduled_date;

    @JsonIgnore
    private boolean scheduled_dateDirtyFlag;

    /**
     * 属性 [SENT]
     *
     */
    @Mail_statistics_reportSentDefault(info = "默认规则")
    private Integer sent;

    @JsonIgnore
    private boolean sentDirtyFlag;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @Mail_statistics_reportEmail_fromDefault(info = "默认规则")
    private String email_from;

    @JsonIgnore
    private boolean email_fromDirtyFlag;

    /**
     * 属性 [DELIVERED]
     *
     */
    @Mail_statistics_reportDeliveredDefault(info = "默认规则")
    private Integer delivered;

    @JsonIgnore
    private boolean deliveredDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mail_statistics_reportNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [OPENED]
     *
     */
    @Mail_statistics_reportOpenedDefault(info = "默认规则")
    private Integer opened;

    @JsonIgnore
    private boolean openedDirtyFlag;


    /**
     * 获取 [CLICKED]
     */
    @JsonProperty("clicked")
    public Integer getClicked(){
        return clicked ;
    }

    /**
     * 设置 [CLICKED]
     */
    @JsonProperty("clicked")
    public void setClicked(Integer  clicked){
        this.clicked = clicked ;
        this.clickedDirtyFlag = true ;
    }

    /**
     * 获取 [CLICKED]脏标记
     */
    @JsonIgnore
    public boolean getClickedDirtyFlag(){
        return clickedDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [BOUNCED]
     */
    @JsonProperty("bounced")
    public Integer getBounced(){
        return bounced ;
    }

    /**
     * 设置 [BOUNCED]
     */
    @JsonProperty("bounced")
    public void setBounced(Integer  bounced){
        this.bounced = bounced ;
        this.bouncedDirtyFlag = true ;
    }

    /**
     * 获取 [BOUNCED]脏标记
     */
    @JsonIgnore
    public boolean getBouncedDirtyFlag(){
        return bouncedDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [REPLIED]
     */
    @JsonProperty("replied")
    public Integer getReplied(){
        return replied ;
    }

    /**
     * 设置 [REPLIED]
     */
    @JsonProperty("replied")
    public void setReplied(Integer  replied){
        this.replied = replied ;
        this.repliedDirtyFlag = true ;
    }

    /**
     * 获取 [REPLIED]脏标记
     */
    @JsonIgnore
    public boolean getRepliedDirtyFlag(){
        return repliedDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN]
     */
    @JsonProperty("campaign")
    public String getCampaign(){
        return campaign ;
    }

    /**
     * 设置 [CAMPAIGN]
     */
    @JsonProperty("campaign")
    public void setCampaign(String  campaign){
        this.campaign = campaign ;
        this.campaignDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN]脏标记
     */
    @JsonIgnore
    public boolean getCampaignDirtyFlag(){
        return campaignDirtyFlag ;
    }

    /**
     * 获取 [SCHEDULED_DATE]
     */
    @JsonProperty("scheduled_date")
    public Timestamp getScheduled_date(){
        return scheduled_date ;
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(Timestamp  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULED_DATE]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return scheduled_dateDirtyFlag ;
    }

    /**
     * 获取 [SENT]
     */
    @JsonProperty("sent")
    public Integer getSent(){
        return sent ;
    }

    /**
     * 设置 [SENT]
     */
    @JsonProperty("sent")
    public void setSent(Integer  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

    /**
     * 获取 [SENT]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return sentDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return email_from ;
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_FROM]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return email_fromDirtyFlag ;
    }

    /**
     * 获取 [DELIVERED]
     */
    @JsonProperty("delivered")
    public Integer getDelivered(){
        return delivered ;
    }

    /**
     * 设置 [DELIVERED]
     */
    @JsonProperty("delivered")
    public void setDelivered(Integer  delivered){
        this.delivered = delivered ;
        this.deliveredDirtyFlag = true ;
    }

    /**
     * 获取 [DELIVERED]脏标记
     */
    @JsonIgnore
    public boolean getDeliveredDirtyFlag(){
        return deliveredDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [OPENED]
     */
    @JsonProperty("opened")
    public Integer getOpened(){
        return opened ;
    }

    /**
     * 设置 [OPENED]
     */
    @JsonProperty("opened")
    public void setOpened(Integer  opened){
        this.opened = opened ;
        this.openedDirtyFlag = true ;
    }

    /**
     * 获取 [OPENED]脏标记
     */
    @JsonIgnore
    public boolean getOpenedDirtyFlag(){
        return openedDirtyFlag ;
    }



    public Mail_statistics_report toDO() {
        Mail_statistics_report srfdomain = new Mail_statistics_report();
        if(getClickedDirtyFlag())
            srfdomain.setClicked(clicked);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getBouncedDirtyFlag())
            srfdomain.setBounced(bounced);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getRepliedDirtyFlag())
            srfdomain.setReplied(replied);
        if(getCampaignDirtyFlag())
            srfdomain.setCampaign(campaign);
        if(getScheduled_dateDirtyFlag())
            srfdomain.setScheduled_date(scheduled_date);
        if(getSentDirtyFlag())
            srfdomain.setSent(sent);
        if(getEmail_fromDirtyFlag())
            srfdomain.setEmail_from(email_from);
        if(getDeliveredDirtyFlag())
            srfdomain.setDelivered(delivered);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getOpenedDirtyFlag())
            srfdomain.setOpened(opened);

        return srfdomain;
    }

    public void fromDO(Mail_statistics_report srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getClickedDirtyFlag())
            this.setClicked(srfdomain.getClicked());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getBouncedDirtyFlag())
            this.setBounced(srfdomain.getBounced());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getRepliedDirtyFlag())
            this.setReplied(srfdomain.getReplied());
        if(srfdomain.getCampaignDirtyFlag())
            this.setCampaign(srfdomain.getCampaign());
        if(srfdomain.getScheduled_dateDirtyFlag())
            this.setScheduled_date(srfdomain.getScheduled_date());
        if(srfdomain.getSentDirtyFlag())
            this.setSent(srfdomain.getSent());
        if(srfdomain.getEmail_fromDirtyFlag())
            this.setEmail_from(srfdomain.getEmail_from());
        if(srfdomain.getDeliveredDirtyFlag())
            this.setDelivered(srfdomain.getDelivered());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getOpenedDirtyFlag())
            this.setOpened(srfdomain.getOpened());

    }

    public List<Mail_statistics_reportDTO> fromDOPage(List<Mail_statistics_report> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_statistics_reportDTO> dtos=new ArrayList<Mail_statistics_reportDTO>();
        for(Mail_statistics_report domain : poPage) {
            Mail_statistics_reportDTO dto = new Mail_statistics_reportDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

