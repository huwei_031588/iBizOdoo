package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_tracking_valueDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_tracking_value;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_tracking_valueService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_tracking_valueSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_tracking_value" })
@RestController
@RequestMapping("")
public class Mail_tracking_valueResource {

    @Autowired
    private IMail_tracking_valueService mail_tracking_valueService;

    public IMail_tracking_valueService getMail_tracking_valueService() {
        return this.mail_tracking_valueService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_tracking_value" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_tracking_values/{mail_tracking_value_id}")

    public ResponseEntity<Mail_tracking_valueDTO> update(@PathVariable("mail_tracking_value_id") Integer mail_tracking_value_id, @RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
		Mail_tracking_value domain = mail_tracking_valuedto.toDO();
        domain.setId(mail_tracking_value_id);
		mail_tracking_valueService.update(domain);
		Mail_tracking_valueDTO dto = new Mail_tracking_valueDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_tracking_value" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_tracking_values/createBatch")
    public ResponseEntity<Boolean> createBatchMail_tracking_value(@RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_tracking_value" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_tracking_values")

    public ResponseEntity<Mail_tracking_valueDTO> create(@RequestBody Mail_tracking_valueDTO mail_tracking_valuedto) {
        Mail_tracking_valueDTO dto = new Mail_tracking_valueDTO();
        Mail_tracking_value domain = mail_tracking_valuedto.toDO();
		mail_tracking_valueService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_tracking_value" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_tracking_values/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_tracking_value" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_tracking_values/{mail_tracking_value_id}")
    public ResponseEntity<Mail_tracking_valueDTO> get(@PathVariable("mail_tracking_value_id") Integer mail_tracking_value_id) {
        Mail_tracking_valueDTO dto = new Mail_tracking_valueDTO();
        Mail_tracking_value domain = mail_tracking_valueService.get(mail_tracking_value_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_tracking_value" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_tracking_values/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_tracking_valueDTO> mail_tracking_valuedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_tracking_value" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_tracking_values/{mail_tracking_value_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_tracking_value_id") Integer mail_tracking_value_id) {
        Mail_tracking_valueDTO mail_tracking_valuedto = new Mail_tracking_valueDTO();
		Mail_tracking_value domain = new Mail_tracking_value();
		mail_tracking_valuedto.setId(mail_tracking_value_id);
		domain.setId(mail_tracking_value_id);
        Boolean rst = mail_tracking_valueService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_tracking_value" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_tracking_values/fetchdefault")
	public ResponseEntity<Page<Mail_tracking_valueDTO>> fetchDefault(Mail_tracking_valueSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_tracking_valueDTO> list = new ArrayList<Mail_tracking_valueDTO>();
        
        Page<Mail_tracking_value> domains = mail_tracking_valueService.searchDefault(context) ;
        for(Mail_tracking_value mail_tracking_value : domains.getContent()){
            Mail_tracking_valueDTO dto = new Mail_tracking_valueDTO();
            dto.fromDO(mail_tracking_value);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
