package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_threadDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_thread;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_threadService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_threadSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_thread" })
@RestController
@RequestMapping("")
public class Mail_threadResource {

    @Autowired
    private IMail_threadService mail_threadService;

    public IMail_threadService getMail_threadService() {
        return this.mail_threadService;
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_thread" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_threads/{mail_thread_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_thread_id") Integer mail_thread_id) {
        Mail_threadDTO mail_threaddto = new Mail_threadDTO();
		Mail_thread domain = new Mail_thread();
		mail_threaddto.setId(mail_thread_id);
		domain.setId(mail_thread_id);
        Boolean rst = mail_threadService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_thread" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_threads")

    public ResponseEntity<Mail_threadDTO> create(@RequestBody Mail_threadDTO mail_threaddto) {
        Mail_threadDTO dto = new Mail_threadDTO();
        Mail_thread domain = mail_threaddto.toDO();
		mail_threadService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_thread" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_threads/{mail_thread_id}")
    public ResponseEntity<Mail_threadDTO> get(@PathVariable("mail_thread_id") Integer mail_thread_id) {
        Mail_threadDTO dto = new Mail_threadDTO();
        Mail_thread domain = mail_threadService.get(mail_thread_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_thread" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_threads/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_threadDTO> mail_threaddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_thread" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_threads/createBatch")
    public ResponseEntity<Boolean> createBatchMail_thread(@RequestBody List<Mail_threadDTO> mail_threaddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_thread" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_threads/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_threadDTO> mail_threaddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_thread" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_threads/{mail_thread_id}")

    public ResponseEntity<Mail_threadDTO> update(@PathVariable("mail_thread_id") Integer mail_thread_id, @RequestBody Mail_threadDTO mail_threaddto) {
		Mail_thread domain = mail_threaddto.toDO();
        domain.setId(mail_thread_id);
		mail_threadService.update(domain);
		Mail_threadDTO dto = new Mail_threadDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_thread" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_threads/fetchdefault")
	public ResponseEntity<Page<Mail_threadDTO>> fetchDefault(Mail_threadSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_threadDTO> list = new ArrayList<Mail_threadDTO>();
        
        Page<Mail_thread> domains = mail_threadService.searchDefault(context) ;
        for(Mail_thread mail_thread : domains.getContent()){
            Mail_threadDTO dto = new Mail_threadDTO();
            dto.fromDO(mail_thread);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
