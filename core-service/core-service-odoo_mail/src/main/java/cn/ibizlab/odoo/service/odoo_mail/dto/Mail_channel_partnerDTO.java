package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_channel_partner.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_channel_partnerDTO]
 */
public class Mail_channel_partnerDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Mail_channel_partnerIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_channel_partnerDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [IS_BLACKLISTED]
     *
     */
    @Mail_channel_partnerIs_blacklistedDefault(info = "默认规则")
    private String is_blacklisted;

    @JsonIgnore
    private boolean is_blacklistedDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_channel_partner__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_channel_partnerCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [IS_PINNED]
     *
     */
    @Mail_channel_partnerIs_pinnedDefault(info = "默认规则")
    private String is_pinned;

    @JsonIgnore
    private boolean is_pinnedDirtyFlag;

    /**
     * 属性 [FOLD_STATE]
     *
     */
    @Mail_channel_partnerFold_stateDefault(info = "默认规则")
    private String fold_state;

    @JsonIgnore
    private boolean fold_stateDirtyFlag;

    /**
     * 属性 [IS_MINIMIZED]
     *
     */
    @Mail_channel_partnerIs_minimizedDefault(info = "默认规则")
    private String is_minimized;

    @JsonIgnore
    private boolean is_minimizedDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_channel_partnerWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Mail_channel_partnerPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CHANNEL_ID_TEXT]
     *
     */
    @Mail_channel_partnerChannel_id_textDefault(info = "默认规则")
    private String channel_id_text;

    @JsonIgnore
    private boolean channel_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_EMAIL]
     *
     */
    @Mail_channel_partnerPartner_emailDefault(info = "默认规则")
    private String partner_email;

    @JsonIgnore
    private boolean partner_emailDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_channel_partnerCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_channel_partnerWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_channel_partnerWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CHANNEL_ID]
     *
     */
    @Mail_channel_partnerChannel_idDefault(info = "默认规则")
    private Integer channel_id;

    @JsonIgnore
    private boolean channel_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_channel_partnerCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Mail_channel_partnerPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [SEEN_MESSAGE_ID]
     *
     */
    @Mail_channel_partnerSeen_message_idDefault(info = "默认规则")
    private Integer seen_message_id;

    @JsonIgnore
    private boolean seen_message_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [IS_BLACKLISTED]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return is_blacklisted ;
    }

    /**
     * 设置 [IS_BLACKLISTED]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_BLACKLISTED]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return is_blacklistedDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [IS_PINNED]
     */
    @JsonProperty("is_pinned")
    public String getIs_pinned(){
        return is_pinned ;
    }

    /**
     * 设置 [IS_PINNED]
     */
    @JsonProperty("is_pinned")
    public void setIs_pinned(String  is_pinned){
        this.is_pinned = is_pinned ;
        this.is_pinnedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PINNED]脏标记
     */
    @JsonIgnore
    public boolean getIs_pinnedDirtyFlag(){
        return is_pinnedDirtyFlag ;
    }

    /**
     * 获取 [FOLD_STATE]
     */
    @JsonProperty("fold_state")
    public String getFold_state(){
        return fold_state ;
    }

    /**
     * 设置 [FOLD_STATE]
     */
    @JsonProperty("fold_state")
    public void setFold_state(String  fold_state){
        this.fold_state = fold_state ;
        this.fold_stateDirtyFlag = true ;
    }

    /**
     * 获取 [FOLD_STATE]脏标记
     */
    @JsonIgnore
    public boolean getFold_stateDirtyFlag(){
        return fold_stateDirtyFlag ;
    }

    /**
     * 获取 [IS_MINIMIZED]
     */
    @JsonProperty("is_minimized")
    public String getIs_minimized(){
        return is_minimized ;
    }

    /**
     * 设置 [IS_MINIMIZED]
     */
    @JsonProperty("is_minimized")
    public void setIs_minimized(String  is_minimized){
        this.is_minimized = is_minimized ;
        this.is_minimizedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_MINIMIZED]脏标记
     */
    @JsonIgnore
    public boolean getIs_minimizedDirtyFlag(){
        return is_minimizedDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_ID_TEXT]
     */
    @JsonProperty("channel_id_text")
    public String getChannel_id_text(){
        return channel_id_text ;
    }

    /**
     * 设置 [CHANNEL_ID_TEXT]
     */
    @JsonProperty("channel_id_text")
    public void setChannel_id_text(String  channel_id_text){
        this.channel_id_text = channel_id_text ;
        this.channel_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChannel_id_textDirtyFlag(){
        return channel_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_EMAIL]
     */
    @JsonProperty("partner_email")
    public String getPartner_email(){
        return partner_email ;
    }

    /**
     * 设置 [PARTNER_EMAIL]
     */
    @JsonProperty("partner_email")
    public void setPartner_email(String  partner_email){
        this.partner_email = partner_email ;
        this.partner_emailDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getPartner_emailDirtyFlag(){
        return partner_emailDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_ID]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return channel_id ;
    }

    /**
     * 设置 [CHANNEL_ID]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return channel_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [SEEN_MESSAGE_ID]
     */
    @JsonProperty("seen_message_id")
    public Integer getSeen_message_id(){
        return seen_message_id ;
    }

    /**
     * 设置 [SEEN_MESSAGE_ID]
     */
    @JsonProperty("seen_message_id")
    public void setSeen_message_id(Integer  seen_message_id){
        this.seen_message_id = seen_message_id ;
        this.seen_message_idDirtyFlag = true ;
    }

    /**
     * 获取 [SEEN_MESSAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSeen_message_idDirtyFlag(){
        return seen_message_idDirtyFlag ;
    }



    public Mail_channel_partner toDO() {
        Mail_channel_partner srfdomain = new Mail_channel_partner();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIs_blacklistedDirtyFlag())
            srfdomain.setIs_blacklisted(is_blacklisted);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIs_pinnedDirtyFlag())
            srfdomain.setIs_pinned(is_pinned);
        if(getFold_stateDirtyFlag())
            srfdomain.setFold_state(fold_state);
        if(getIs_minimizedDirtyFlag())
            srfdomain.setIs_minimized(is_minimized);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getChannel_id_textDirtyFlag())
            srfdomain.setChannel_id_text(channel_id_text);
        if(getPartner_emailDirtyFlag())
            srfdomain.setPartner_email(partner_email);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getChannel_idDirtyFlag())
            srfdomain.setChannel_id(channel_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getSeen_message_idDirtyFlag())
            srfdomain.setSeen_message_id(seen_message_id);

        return srfdomain;
    }

    public void fromDO(Mail_channel_partner srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIs_blacklistedDirtyFlag())
            this.setIs_blacklisted(srfdomain.getIs_blacklisted());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIs_pinnedDirtyFlag())
            this.setIs_pinned(srfdomain.getIs_pinned());
        if(srfdomain.getFold_stateDirtyFlag())
            this.setFold_state(srfdomain.getFold_state());
        if(srfdomain.getIs_minimizedDirtyFlag())
            this.setIs_minimized(srfdomain.getIs_minimized());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getChannel_id_textDirtyFlag())
            this.setChannel_id_text(srfdomain.getChannel_id_text());
        if(srfdomain.getPartner_emailDirtyFlag())
            this.setPartner_email(srfdomain.getPartner_email());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getChannel_idDirtyFlag())
            this.setChannel_id(srfdomain.getChannel_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getSeen_message_idDirtyFlag())
            this.setSeen_message_id(srfdomain.getSeen_message_id());

    }

    public List<Mail_channel_partnerDTO> fromDOPage(List<Mail_channel_partner> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_channel_partnerDTO> dtos=new ArrayList<Mail_channel_partnerDTO>();
        for(Mail_channel_partner domain : poPage) {
            Mail_channel_partnerDTO dto = new Mail_channel_partnerDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

