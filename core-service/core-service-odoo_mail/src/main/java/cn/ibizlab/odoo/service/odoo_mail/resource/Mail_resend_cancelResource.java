package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_resend_cancelDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_resend_cancelService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_cancelSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_resend_cancel" })
@RestController
@RequestMapping("")
public class Mail_resend_cancelResource {

    @Autowired
    private IMail_resend_cancelService mail_resend_cancelService;

    public IMail_resend_cancelService getMail_resend_cancelService() {
        return this.mail_resend_cancelService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_resend_cancel" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_resend_cancels/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_resend_cancelDTO> mail_resend_canceldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_resend_cancel" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_resend_cancels/createBatch")
    public ResponseEntity<Boolean> createBatchMail_resend_cancel(@RequestBody List<Mail_resend_cancelDTO> mail_resend_canceldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_resend_cancel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_resend_cancels/{mail_resend_cancel_id}")

    public ResponseEntity<Mail_resend_cancelDTO> update(@PathVariable("mail_resend_cancel_id") Integer mail_resend_cancel_id, @RequestBody Mail_resend_cancelDTO mail_resend_canceldto) {
		Mail_resend_cancel domain = mail_resend_canceldto.toDO();
        domain.setId(mail_resend_cancel_id);
		mail_resend_cancelService.update(domain);
		Mail_resend_cancelDTO dto = new Mail_resend_cancelDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_resend_cancel" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_resend_cancels/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_resend_cancelDTO> mail_resend_canceldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_resend_cancel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_resend_cancels/{mail_resend_cancel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_resend_cancel_id") Integer mail_resend_cancel_id) {
        Mail_resend_cancelDTO mail_resend_canceldto = new Mail_resend_cancelDTO();
		Mail_resend_cancel domain = new Mail_resend_cancel();
		mail_resend_canceldto.setId(mail_resend_cancel_id);
		domain.setId(mail_resend_cancel_id);
        Boolean rst = mail_resend_cancelService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_resend_cancel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_resend_cancels")

    public ResponseEntity<Mail_resend_cancelDTO> create(@RequestBody Mail_resend_cancelDTO mail_resend_canceldto) {
        Mail_resend_cancelDTO dto = new Mail_resend_cancelDTO();
        Mail_resend_cancel domain = mail_resend_canceldto.toDO();
		mail_resend_cancelService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_resend_cancel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_cancels/{mail_resend_cancel_id}")
    public ResponseEntity<Mail_resend_cancelDTO> get(@PathVariable("mail_resend_cancel_id") Integer mail_resend_cancel_id) {
        Mail_resend_cancelDTO dto = new Mail_resend_cancelDTO();
        Mail_resend_cancel domain = mail_resend_cancelService.get(mail_resend_cancel_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_resend_cancel" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_resend_cancels/fetchdefault")
	public ResponseEntity<Page<Mail_resend_cancelDTO>> fetchDefault(Mail_resend_cancelSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_resend_cancelDTO> list = new ArrayList<Mail_resend_cancelDTO>();
        
        Page<Mail_resend_cancel> domains = mail_resend_cancelService.searchDefault(context) ;
        for(Mail_resend_cancel mail_resend_cancel : domains.getContent()){
            Mail_resend_cancelDTO dto = new Mail_resend_cancelDTO();
            dto.fromDO(mail_resend_cancel);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
