package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_botDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_botService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_botSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_bot" })
@RestController
@RequestMapping("")
public class Mail_botResource {

    @Autowired
    private IMail_botService mail_botService;

    public IMail_botService getMail_botService() {
        return this.mail_botService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_bot" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_bots/{mail_bot_id}")

    public ResponseEntity<Mail_botDTO> update(@PathVariable("mail_bot_id") Integer mail_bot_id, @RequestBody Mail_botDTO mail_botdto) {
		Mail_bot domain = mail_botdto.toDO();
        domain.setId(mail_bot_id);
		mail_botService.update(domain);
		Mail_botDTO dto = new Mail_botDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_bot" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_bots/{mail_bot_id}")
    public ResponseEntity<Mail_botDTO> get(@PathVariable("mail_bot_id") Integer mail_bot_id) {
        Mail_botDTO dto = new Mail_botDTO();
        Mail_bot domain = mail_botService.get(mail_bot_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_bot" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_bots")

    public ResponseEntity<Mail_botDTO> create(@RequestBody Mail_botDTO mail_botdto) {
        Mail_botDTO dto = new Mail_botDTO();
        Mail_bot domain = mail_botdto.toDO();
		mail_botService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_bot" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_bots/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_botDTO> mail_botdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_bot" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_bots/{mail_bot_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_bot_id") Integer mail_bot_id) {
        Mail_botDTO mail_botdto = new Mail_botDTO();
		Mail_bot domain = new Mail_bot();
		mail_botdto.setId(mail_bot_id);
		domain.setId(mail_bot_id);
        Boolean rst = mail_botService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_bot" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_bots/createBatch")
    public ResponseEntity<Boolean> createBatchMail_bot(@RequestBody List<Mail_botDTO> mail_botdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_bot" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_bots/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_botDTO> mail_botdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_bot" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_bots/fetchdefault")
	public ResponseEntity<Page<Mail_botDTO>> fetchDefault(Mail_botSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_botDTO> list = new ArrayList<Mail_botDTO>();
        
        Page<Mail_bot> domains = mail_botService.searchDefault(context) ;
        for(Mail_bot mail_bot : domains.getContent()){
            Mail_botDTO dto = new Mail_botDTO();
            dto.fromDO(mail_bot);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
