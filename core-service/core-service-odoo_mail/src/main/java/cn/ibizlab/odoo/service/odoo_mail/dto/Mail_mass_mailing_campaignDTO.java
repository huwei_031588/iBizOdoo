package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_mass_mailing_campaign.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_mass_mailing_campaignDTO]
 */
public class Mail_mass_mailing_campaignDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TOTAL]
     *
     */
    @Mail_mass_mailing_campaignTotalDefault(info = "默认规则")
    private Integer total;

    @JsonIgnore
    private boolean totalDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_mass_mailing_campaign__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Mail_mass_mailing_campaignColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [MASS_MAILING_IDS]
     *
     */
    @Mail_mass_mailing_campaignMass_mailing_idsDefault(info = "默认规则")
    private String mass_mailing_ids;

    @JsonIgnore
    private boolean mass_mailing_idsDirtyFlag;

    /**
     * 属性 [OPENED_RATIO]
     *
     */
    @Mail_mass_mailing_campaignOpened_ratioDefault(info = "默认规则")
    private Integer opened_ratio;

    @JsonIgnore
    private boolean opened_ratioDirtyFlag;

    /**
     * 属性 [FAILED]
     *
     */
    @Mail_mass_mailing_campaignFailedDefault(info = "默认规则")
    private Integer failed;

    @JsonIgnore
    private boolean failedDirtyFlag;

    /**
     * 属性 [SCHEDULED]
     *
     */
    @Mail_mass_mailing_campaignScheduledDefault(info = "默认规则")
    private Integer scheduled;

    @JsonIgnore
    private boolean scheduledDirtyFlag;

    /**
     * 属性 [BOUNCED]
     *
     */
    @Mail_mass_mailing_campaignBouncedDefault(info = "默认规则")
    private Integer bounced;

    @JsonIgnore
    private boolean bouncedDirtyFlag;

    /**
     * 属性 [CLICKS_RATIO]
     *
     */
    @Mail_mass_mailing_campaignClicks_ratioDefault(info = "默认规则")
    private Integer clicks_ratio;

    @JsonIgnore
    private boolean clicks_ratioDirtyFlag;

    /**
     * 属性 [SENT]
     *
     */
    @Mail_mass_mailing_campaignSentDefault(info = "默认规则")
    private Integer sent;

    @JsonIgnore
    private boolean sentDirtyFlag;

    /**
     * 属性 [RECEIVED_RATIO]
     *
     */
    @Mail_mass_mailing_campaignReceived_ratioDefault(info = "默认规则")
    private Integer received_ratio;

    @JsonIgnore
    private boolean received_ratioDirtyFlag;

    /**
     * 属性 [IGNORED]
     *
     */
    @Mail_mass_mailing_campaignIgnoredDefault(info = "默认规则")
    private Integer ignored;

    @JsonIgnore
    private boolean ignoredDirtyFlag;

    /**
     * 属性 [UNIQUE_AB_TESTING]
     *
     */
    @Mail_mass_mailing_campaignUnique_ab_testingDefault(info = "默认规则")
    private String unique_ab_testing;

    @JsonIgnore
    private boolean unique_ab_testingDirtyFlag;

    /**
     * 属性 [REPLIED]
     *
     */
    @Mail_mass_mailing_campaignRepliedDefault(info = "默认规则")
    private Integer replied;

    @JsonIgnore
    private boolean repliedDirtyFlag;

    /**
     * 属性 [TOTAL_MAILINGS]
     *
     */
    @Mail_mass_mailing_campaignTotal_mailingsDefault(info = "默认规则")
    private Integer total_mailings;

    @JsonIgnore
    private boolean total_mailingsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_mass_mailing_campaignDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [BOUNCED_RATIO]
     *
     */
    @Mail_mass_mailing_campaignBounced_ratioDefault(info = "默认规则")
    private Integer bounced_ratio;

    @JsonIgnore
    private boolean bounced_ratioDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_mass_mailing_campaignIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @Mail_mass_mailing_campaignTag_idsDefault(info = "默认规则")
    private String tag_ids;

    @JsonIgnore
    private boolean tag_idsDirtyFlag;

    /**
     * 属性 [OPENED]
     *
     */
    @Mail_mass_mailing_campaignOpenedDefault(info = "默认规则")
    private Integer opened;

    @JsonIgnore
    private boolean openedDirtyFlag;

    /**
     * 属性 [REPLIED_RATIO]
     *
     */
    @Mail_mass_mailing_campaignReplied_ratioDefault(info = "默认规则")
    private Integer replied_ratio;

    @JsonIgnore
    private boolean replied_ratioDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_mass_mailing_campaignWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DELIVERED]
     *
     */
    @Mail_mass_mailing_campaignDeliveredDefault(info = "默认规则")
    private Integer delivered;

    @JsonIgnore
    private boolean deliveredDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_mass_mailing_campaignCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @Mail_mass_mailing_campaignMedium_id_textDefault(info = "默认规则")
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @Mail_mass_mailing_campaignStage_id_textDefault(info = "默认规则")
    private String stage_id_text;

    @JsonIgnore
    private boolean stage_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Mail_mass_mailing_campaignUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_mass_mailing_campaignWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_mass_mailing_campaignCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @Mail_mass_mailing_campaignSource_id_textDefault(info = "默认规则")
    private String source_id_text;

    @JsonIgnore
    private boolean source_id_textDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mail_mass_mailing_campaignNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Mail_mass_mailing_campaignUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_mass_mailing_campaignCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @Mail_mass_mailing_campaignCampaign_idDefault(info = "默认规则")
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_mass_mailing_campaignWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @Mail_mass_mailing_campaignStage_idDefault(info = "默认规则")
    private Integer stage_id;

    @JsonIgnore
    private boolean stage_idDirtyFlag;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @Mail_mass_mailing_campaignSource_idDefault(info = "默认规则")
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @Mail_mass_mailing_campaignMedium_idDefault(info = "默认规则")
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;


    /**
     * 获取 [TOTAL]
     */
    @JsonProperty("total")
    public Integer getTotal(){
        return total ;
    }

    /**
     * 设置 [TOTAL]
     */
    @JsonProperty("total")
    public void setTotal(Integer  total){
        this.total = total ;
        this.totalDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getTotalDirtyFlag(){
        return totalDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_IDS]
     */
    @JsonProperty("mass_mailing_ids")
    public String getMass_mailing_ids(){
        return mass_mailing_ids ;
    }

    /**
     * 设置 [MASS_MAILING_IDS]
     */
    @JsonProperty("mass_mailing_ids")
    public void setMass_mailing_ids(String  mass_mailing_ids){
        this.mass_mailing_ids = mass_mailing_ids ;
        this.mass_mailing_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_idsDirtyFlag(){
        return mass_mailing_idsDirtyFlag ;
    }

    /**
     * 获取 [OPENED_RATIO]
     */
    @JsonProperty("opened_ratio")
    public Integer getOpened_ratio(){
        return opened_ratio ;
    }

    /**
     * 设置 [OPENED_RATIO]
     */
    @JsonProperty("opened_ratio")
    public void setOpened_ratio(Integer  opened_ratio){
        this.opened_ratio = opened_ratio ;
        this.opened_ratioDirtyFlag = true ;
    }

    /**
     * 获取 [OPENED_RATIO]脏标记
     */
    @JsonIgnore
    public boolean getOpened_ratioDirtyFlag(){
        return opened_ratioDirtyFlag ;
    }

    /**
     * 获取 [FAILED]
     */
    @JsonProperty("failed")
    public Integer getFailed(){
        return failed ;
    }

    /**
     * 设置 [FAILED]
     */
    @JsonProperty("failed")
    public void setFailed(Integer  failed){
        this.failed = failed ;
        this.failedDirtyFlag = true ;
    }

    /**
     * 获取 [FAILED]脏标记
     */
    @JsonIgnore
    public boolean getFailedDirtyFlag(){
        return failedDirtyFlag ;
    }

    /**
     * 获取 [SCHEDULED]
     */
    @JsonProperty("scheduled")
    public Integer getScheduled(){
        return scheduled ;
    }

    /**
     * 设置 [SCHEDULED]
     */
    @JsonProperty("scheduled")
    public void setScheduled(Integer  scheduled){
        this.scheduled = scheduled ;
        this.scheduledDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULED]脏标记
     */
    @JsonIgnore
    public boolean getScheduledDirtyFlag(){
        return scheduledDirtyFlag ;
    }

    /**
     * 获取 [BOUNCED]
     */
    @JsonProperty("bounced")
    public Integer getBounced(){
        return bounced ;
    }

    /**
     * 设置 [BOUNCED]
     */
    @JsonProperty("bounced")
    public void setBounced(Integer  bounced){
        this.bounced = bounced ;
        this.bouncedDirtyFlag = true ;
    }

    /**
     * 获取 [BOUNCED]脏标记
     */
    @JsonIgnore
    public boolean getBouncedDirtyFlag(){
        return bouncedDirtyFlag ;
    }

    /**
     * 获取 [CLICKS_RATIO]
     */
    @JsonProperty("clicks_ratio")
    public Integer getClicks_ratio(){
        return clicks_ratio ;
    }

    /**
     * 设置 [CLICKS_RATIO]
     */
    @JsonProperty("clicks_ratio")
    public void setClicks_ratio(Integer  clicks_ratio){
        this.clicks_ratio = clicks_ratio ;
        this.clicks_ratioDirtyFlag = true ;
    }

    /**
     * 获取 [CLICKS_RATIO]脏标记
     */
    @JsonIgnore
    public boolean getClicks_ratioDirtyFlag(){
        return clicks_ratioDirtyFlag ;
    }

    /**
     * 获取 [SENT]
     */
    @JsonProperty("sent")
    public Integer getSent(){
        return sent ;
    }

    /**
     * 设置 [SENT]
     */
    @JsonProperty("sent")
    public void setSent(Integer  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

    /**
     * 获取 [SENT]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return sentDirtyFlag ;
    }

    /**
     * 获取 [RECEIVED_RATIO]
     */
    @JsonProperty("received_ratio")
    public Integer getReceived_ratio(){
        return received_ratio ;
    }

    /**
     * 设置 [RECEIVED_RATIO]
     */
    @JsonProperty("received_ratio")
    public void setReceived_ratio(Integer  received_ratio){
        this.received_ratio = received_ratio ;
        this.received_ratioDirtyFlag = true ;
    }

    /**
     * 获取 [RECEIVED_RATIO]脏标记
     */
    @JsonIgnore
    public boolean getReceived_ratioDirtyFlag(){
        return received_ratioDirtyFlag ;
    }

    /**
     * 获取 [IGNORED]
     */
    @JsonProperty("ignored")
    public Integer getIgnored(){
        return ignored ;
    }

    /**
     * 设置 [IGNORED]
     */
    @JsonProperty("ignored")
    public void setIgnored(Integer  ignored){
        this.ignored = ignored ;
        this.ignoredDirtyFlag = true ;
    }

    /**
     * 获取 [IGNORED]脏标记
     */
    @JsonIgnore
    public boolean getIgnoredDirtyFlag(){
        return ignoredDirtyFlag ;
    }

    /**
     * 获取 [UNIQUE_AB_TESTING]
     */
    @JsonProperty("unique_ab_testing")
    public String getUnique_ab_testing(){
        return unique_ab_testing ;
    }

    /**
     * 设置 [UNIQUE_AB_TESTING]
     */
    @JsonProperty("unique_ab_testing")
    public void setUnique_ab_testing(String  unique_ab_testing){
        this.unique_ab_testing = unique_ab_testing ;
        this.unique_ab_testingDirtyFlag = true ;
    }

    /**
     * 获取 [UNIQUE_AB_TESTING]脏标记
     */
    @JsonIgnore
    public boolean getUnique_ab_testingDirtyFlag(){
        return unique_ab_testingDirtyFlag ;
    }

    /**
     * 获取 [REPLIED]
     */
    @JsonProperty("replied")
    public Integer getReplied(){
        return replied ;
    }

    /**
     * 设置 [REPLIED]
     */
    @JsonProperty("replied")
    public void setReplied(Integer  replied){
        this.replied = replied ;
        this.repliedDirtyFlag = true ;
    }

    /**
     * 获取 [REPLIED]脏标记
     */
    @JsonIgnore
    public boolean getRepliedDirtyFlag(){
        return repliedDirtyFlag ;
    }

    /**
     * 获取 [TOTAL_MAILINGS]
     */
    @JsonProperty("total_mailings")
    public Integer getTotal_mailings(){
        return total_mailings ;
    }

    /**
     * 设置 [TOTAL_MAILINGS]
     */
    @JsonProperty("total_mailings")
    public void setTotal_mailings(Integer  total_mailings){
        this.total_mailings = total_mailings ;
        this.total_mailingsDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL_MAILINGS]脏标记
     */
    @JsonIgnore
    public boolean getTotal_mailingsDirtyFlag(){
        return total_mailingsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [BOUNCED_RATIO]
     */
    @JsonProperty("bounced_ratio")
    public Integer getBounced_ratio(){
        return bounced_ratio ;
    }

    /**
     * 设置 [BOUNCED_RATIO]
     */
    @JsonProperty("bounced_ratio")
    public void setBounced_ratio(Integer  bounced_ratio){
        this.bounced_ratio = bounced_ratio ;
        this.bounced_ratioDirtyFlag = true ;
    }

    /**
     * 获取 [BOUNCED_RATIO]脏标记
     */
    @JsonIgnore
    public boolean getBounced_ratioDirtyFlag(){
        return bounced_ratioDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return tag_ids ;
    }

    /**
     * 设置 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return tag_idsDirtyFlag ;
    }

    /**
     * 获取 [OPENED]
     */
    @JsonProperty("opened")
    public Integer getOpened(){
        return opened ;
    }

    /**
     * 设置 [OPENED]
     */
    @JsonProperty("opened")
    public void setOpened(Integer  opened){
        this.opened = opened ;
        this.openedDirtyFlag = true ;
    }

    /**
     * 获取 [OPENED]脏标记
     */
    @JsonIgnore
    public boolean getOpenedDirtyFlag(){
        return openedDirtyFlag ;
    }

    /**
     * 获取 [REPLIED_RATIO]
     */
    @JsonProperty("replied_ratio")
    public Integer getReplied_ratio(){
        return replied_ratio ;
    }

    /**
     * 设置 [REPLIED_RATIO]
     */
    @JsonProperty("replied_ratio")
    public void setReplied_ratio(Integer  replied_ratio){
        this.replied_ratio = replied_ratio ;
        this.replied_ratioDirtyFlag = true ;
    }

    /**
     * 获取 [REPLIED_RATIO]脏标记
     */
    @JsonIgnore
    public boolean getReplied_ratioDirtyFlag(){
        return replied_ratioDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DELIVERED]
     */
    @JsonProperty("delivered")
    public Integer getDelivered(){
        return delivered ;
    }

    /**
     * 设置 [DELIVERED]
     */
    @JsonProperty("delivered")
    public void setDelivered(Integer  delivered){
        this.delivered = delivered ;
        this.deliveredDirtyFlag = true ;
    }

    /**
     * 获取 [DELIVERED]脏标记
     */
    @JsonIgnore
    public boolean getDeliveredDirtyFlag(){
        return deliveredDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return medium_id_text ;
    }

    /**
     * 设置 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return stage_id_text ;
    }

    /**
     * 设置 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return stage_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return source_id_text ;
    }

    /**
     * 设置 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return source_id_textDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return campaign_id ;
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return campaign_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return stage_id ;
    }

    /**
     * 设置 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return stage_idDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return source_id ;
    }

    /**
     * 设置 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return source_idDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return medium_id ;
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return medium_idDirtyFlag ;
    }



    public Mail_mass_mailing_campaign toDO() {
        Mail_mass_mailing_campaign srfdomain = new Mail_mass_mailing_campaign();
        if(getTotalDirtyFlag())
            srfdomain.setTotal(total);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getMass_mailing_idsDirtyFlag())
            srfdomain.setMass_mailing_ids(mass_mailing_ids);
        if(getOpened_ratioDirtyFlag())
            srfdomain.setOpened_ratio(opened_ratio);
        if(getFailedDirtyFlag())
            srfdomain.setFailed(failed);
        if(getScheduledDirtyFlag())
            srfdomain.setScheduled(scheduled);
        if(getBouncedDirtyFlag())
            srfdomain.setBounced(bounced);
        if(getClicks_ratioDirtyFlag())
            srfdomain.setClicks_ratio(clicks_ratio);
        if(getSentDirtyFlag())
            srfdomain.setSent(sent);
        if(getReceived_ratioDirtyFlag())
            srfdomain.setReceived_ratio(received_ratio);
        if(getIgnoredDirtyFlag())
            srfdomain.setIgnored(ignored);
        if(getUnique_ab_testingDirtyFlag())
            srfdomain.setUnique_ab_testing(unique_ab_testing);
        if(getRepliedDirtyFlag())
            srfdomain.setReplied(replied);
        if(getTotal_mailingsDirtyFlag())
            srfdomain.setTotal_mailings(total_mailings);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getBounced_ratioDirtyFlag())
            srfdomain.setBounced_ratio(bounced_ratio);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getTag_idsDirtyFlag())
            srfdomain.setTag_ids(tag_ids);
        if(getOpenedDirtyFlag())
            srfdomain.setOpened(opened);
        if(getReplied_ratioDirtyFlag())
            srfdomain.setReplied_ratio(replied_ratio);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDeliveredDirtyFlag())
            srfdomain.setDelivered(delivered);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMedium_id_textDirtyFlag())
            srfdomain.setMedium_id_text(medium_id_text);
        if(getStage_id_textDirtyFlag())
            srfdomain.setStage_id_text(stage_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getSource_id_textDirtyFlag())
            srfdomain.setSource_id_text(source_id_text);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCampaign_idDirtyFlag())
            srfdomain.setCampaign_id(campaign_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getStage_idDirtyFlag())
            srfdomain.setStage_id(stage_id);
        if(getSource_idDirtyFlag())
            srfdomain.setSource_id(source_id);
        if(getMedium_idDirtyFlag())
            srfdomain.setMedium_id(medium_id);

        return srfdomain;
    }

    public void fromDO(Mail_mass_mailing_campaign srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getTotalDirtyFlag())
            this.setTotal(srfdomain.getTotal());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getMass_mailing_idsDirtyFlag())
            this.setMass_mailing_ids(srfdomain.getMass_mailing_ids());
        if(srfdomain.getOpened_ratioDirtyFlag())
            this.setOpened_ratio(srfdomain.getOpened_ratio());
        if(srfdomain.getFailedDirtyFlag())
            this.setFailed(srfdomain.getFailed());
        if(srfdomain.getScheduledDirtyFlag())
            this.setScheduled(srfdomain.getScheduled());
        if(srfdomain.getBouncedDirtyFlag())
            this.setBounced(srfdomain.getBounced());
        if(srfdomain.getClicks_ratioDirtyFlag())
            this.setClicks_ratio(srfdomain.getClicks_ratio());
        if(srfdomain.getSentDirtyFlag())
            this.setSent(srfdomain.getSent());
        if(srfdomain.getReceived_ratioDirtyFlag())
            this.setReceived_ratio(srfdomain.getReceived_ratio());
        if(srfdomain.getIgnoredDirtyFlag())
            this.setIgnored(srfdomain.getIgnored());
        if(srfdomain.getUnique_ab_testingDirtyFlag())
            this.setUnique_ab_testing(srfdomain.getUnique_ab_testing());
        if(srfdomain.getRepliedDirtyFlag())
            this.setReplied(srfdomain.getReplied());
        if(srfdomain.getTotal_mailingsDirtyFlag())
            this.setTotal_mailings(srfdomain.getTotal_mailings());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getBounced_ratioDirtyFlag())
            this.setBounced_ratio(srfdomain.getBounced_ratio());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getTag_idsDirtyFlag())
            this.setTag_ids(srfdomain.getTag_ids());
        if(srfdomain.getOpenedDirtyFlag())
            this.setOpened(srfdomain.getOpened());
        if(srfdomain.getReplied_ratioDirtyFlag())
            this.setReplied_ratio(srfdomain.getReplied_ratio());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDeliveredDirtyFlag())
            this.setDelivered(srfdomain.getDelivered());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMedium_id_textDirtyFlag())
            this.setMedium_id_text(srfdomain.getMedium_id_text());
        if(srfdomain.getStage_id_textDirtyFlag())
            this.setStage_id_text(srfdomain.getStage_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getSource_id_textDirtyFlag())
            this.setSource_id_text(srfdomain.getSource_id_text());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCampaign_idDirtyFlag())
            this.setCampaign_id(srfdomain.getCampaign_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getStage_idDirtyFlag())
            this.setStage_id(srfdomain.getStage_id());
        if(srfdomain.getSource_idDirtyFlag())
            this.setSource_id(srfdomain.getSource_id());
        if(srfdomain.getMedium_idDirtyFlag())
            this.setMedium_id(srfdomain.getMedium_id());

    }

    public List<Mail_mass_mailing_campaignDTO> fromDOPage(List<Mail_mass_mailing_campaign> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_mass_mailing_campaignDTO> dtos=new ArrayList<Mail_mass_mailing_campaignDTO>();
        for(Mail_mass_mailing_campaign domain : poPage) {
            Mail_mass_mailing_campaignDTO dto = new Mail_mass_mailing_campaignDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

