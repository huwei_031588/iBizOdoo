package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_mass_mailing.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_mass_mailingDTO]
 */
public class Mail_mass_mailingDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SENT_DATE]
     *
     */
    @Mail_mass_mailingSent_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp sent_date;

    @JsonIgnore
    private boolean sent_dateDirtyFlag;

    /**
     * 属性 [SALE_QUOTATION_COUNT]
     *
     */
    @Mail_mass_mailingSale_quotation_countDefault(info = "默认规则")
    private Integer sale_quotation_count;

    @JsonIgnore
    private boolean sale_quotation_countDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_mass_mailingCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_mass_mailing__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MAILING_DOMAIN]
     *
     */
    @Mail_mass_mailingMailing_domainDefault(info = "默认规则")
    private String mailing_domain;

    @JsonIgnore
    private boolean mailing_domainDirtyFlag;

    /**
     * 属性 [OPENED]
     *
     */
    @Mail_mass_mailingOpenedDefault(info = "默认规则")
    private Integer opened;

    @JsonIgnore
    private boolean openedDirtyFlag;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @Mail_mass_mailingReply_toDefault(info = "默认规则")
    private String reply_to;

    @JsonIgnore
    private boolean reply_toDirtyFlag;

    /**
     * 属性 [REPLIED]
     *
     */
    @Mail_mass_mailingRepliedDefault(info = "默认规则")
    private Integer replied;

    @JsonIgnore
    private boolean repliedDirtyFlag;

    /**
     * 属性 [CRM_LEAD_COUNT]
     *
     */
    @Mail_mass_mailingCrm_lead_countDefault(info = "默认规则")
    private Integer crm_lead_count;

    @JsonIgnore
    private boolean crm_lead_countDirtyFlag;

    /**
     * 属性 [MAILING_MODEL_REAL]
     *
     */
    @Mail_mass_mailingMailing_model_realDefault(info = "默认规则")
    private String mailing_model_real;

    @JsonIgnore
    private boolean mailing_model_realDirtyFlag;

    /**
     * 属性 [CLICKS_RATIO]
     *
     */
    @Mail_mass_mailingClicks_ratioDefault(info = "默认规则")
    private Integer clicks_ratio;

    @JsonIgnore
    private boolean clicks_ratioDirtyFlag;

    /**
     * 属性 [DELIVERED]
     *
     */
    @Mail_mass_mailingDeliveredDefault(info = "默认规则")
    private Integer delivered;

    @JsonIgnore
    private boolean deliveredDirtyFlag;

    /**
     * 属性 [MAILING_MODEL_ID]
     *
     */
    @Mail_mass_mailingMailing_model_idDefault(info = "默认规则")
    private Integer mailing_model_id;

    @JsonIgnore
    private boolean mailing_model_idDirtyFlag;

    /**
     * 属性 [REPLY_TO_MODE]
     *
     */
    @Mail_mass_mailingReply_to_modeDefault(info = "默认规则")
    private String reply_to_mode;

    @JsonIgnore
    private boolean reply_to_modeDirtyFlag;

    /**
     * 属性 [SCHEDULE_DATE]
     *
     */
    @Mail_mass_mailingSchedule_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp schedule_date;

    @JsonIgnore
    private boolean schedule_dateDirtyFlag;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @Mail_mass_mailingEmail_fromDefault(info = "默认规则")
    private String email_from;

    @JsonIgnore
    private boolean email_fromDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_mass_mailingDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [SALE_INVOICED_AMOUNT]
     *
     */
    @Mail_mass_mailingSale_invoiced_amountDefault(info = "默认规则")
    private Integer sale_invoiced_amount;

    @JsonIgnore
    private boolean sale_invoiced_amountDirtyFlag;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @Mail_mass_mailingAttachment_idsDefault(info = "默认规则")
    private String attachment_ids;

    @JsonIgnore
    private boolean attachment_idsDirtyFlag;

    /**
     * 属性 [TOTAL]
     *
     */
    @Mail_mass_mailingTotalDefault(info = "默认规则")
    private Integer total;

    @JsonIgnore
    private boolean totalDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Mail_mass_mailingActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [BOUNCED_RATIO]
     *
     */
    @Mail_mass_mailingBounced_ratioDefault(info = "默认规则")
    private Integer bounced_ratio;

    @JsonIgnore
    private boolean bounced_ratioDirtyFlag;

    /**
     * 属性 [NEXT_DEPARTURE]
     *
     */
    @Mail_mass_mailingNext_departureDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp next_departure;

    @JsonIgnore
    private boolean next_departureDirtyFlag;

    /**
     * 属性 [MAIL_SERVER_ID]
     *
     */
    @Mail_mass_mailingMail_server_idDefault(info = "默认规则")
    private Integer mail_server_id;

    @JsonIgnore
    private boolean mail_server_idDirtyFlag;

    /**
     * 属性 [IGNORED]
     *
     */
    @Mail_mass_mailingIgnoredDefault(info = "默认规则")
    private Integer ignored;

    @JsonIgnore
    private boolean ignoredDirtyFlag;

    /**
     * 属性 [SCHEDULED]
     *
     */
    @Mail_mass_mailingScheduledDefault(info = "默认规则")
    private Integer scheduled;

    @JsonIgnore
    private boolean scheduledDirtyFlag;

    /**
     * 属性 [CRM_LEAD_ACTIVATED]
     *
     */
    @Mail_mass_mailingCrm_lead_activatedDefault(info = "默认规则")
    private String crm_lead_activated;

    @JsonIgnore
    private boolean crm_lead_activatedDirtyFlag;

    /**
     * 属性 [CLICKED]
     *
     */
    @Mail_mass_mailingClickedDefault(info = "默认规则")
    private Integer clicked;

    @JsonIgnore
    private boolean clickedDirtyFlag;

    /**
     * 属性 [REPLIED_RATIO]
     *
     */
    @Mail_mass_mailingReplied_ratioDefault(info = "默认规则")
    private Integer replied_ratio;

    @JsonIgnore
    private boolean replied_ratioDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_mass_mailingWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Mail_mass_mailingStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [STATISTICS_IDS]
     *
     */
    @Mail_mass_mailingStatistics_idsDefault(info = "默认规则")
    private String statistics_ids;

    @JsonIgnore
    private boolean statistics_idsDirtyFlag;

    /**
     * 属性 [OPENED_RATIO]
     *
     */
    @Mail_mass_mailingOpened_ratioDefault(info = "默认规则")
    private Integer opened_ratio;

    @JsonIgnore
    private boolean opened_ratioDirtyFlag;

    /**
     * 属性 [BODY_HTML]
     *
     */
    @Mail_mass_mailingBody_htmlDefault(info = "默认规则")
    private String body_html;

    @JsonIgnore
    private boolean body_htmlDirtyFlag;

    /**
     * 属性 [BOUNCED]
     *
     */
    @Mail_mass_mailingBouncedDefault(info = "默认规则")
    private Integer bounced;

    @JsonIgnore
    private boolean bouncedDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Mail_mass_mailingColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [MAILING_MODEL_NAME]
     *
     */
    @Mail_mass_mailingMailing_model_nameDefault(info = "默认规则")
    private String mailing_model_name;

    @JsonIgnore
    private boolean mailing_model_nameDirtyFlag;

    /**
     * 属性 [CONTACT_LIST_IDS]
     *
     */
    @Mail_mass_mailingContact_list_idsDefault(info = "默认规则")
    private String contact_list_ids;

    @JsonIgnore
    private boolean contact_list_idsDirtyFlag;

    /**
     * 属性 [EXPECTED]
     *
     */
    @Mail_mass_mailingExpectedDefault(info = "默认规则")
    private Integer expected;

    @JsonIgnore
    private boolean expectedDirtyFlag;

    /**
     * 属性 [FAILED]
     *
     */
    @Mail_mass_mailingFailedDefault(info = "默认规则")
    private Integer failed;

    @JsonIgnore
    private boolean failedDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_mass_mailingIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [RECEIVED_RATIO]
     *
     */
    @Mail_mass_mailingReceived_ratioDefault(info = "默认规则")
    private Integer received_ratio;

    @JsonIgnore
    private boolean received_ratioDirtyFlag;

    /**
     * 属性 [CRM_OPPORTUNITIES_COUNT]
     *
     */
    @Mail_mass_mailingCrm_opportunities_countDefault(info = "默认规则")
    private Integer crm_opportunities_count;

    @JsonIgnore
    private boolean crm_opportunities_countDirtyFlag;

    /**
     * 属性 [KEEP_ARCHIVES]
     *
     */
    @Mail_mass_mailingKeep_archivesDefault(info = "默认规则")
    private String keep_archives;

    @JsonIgnore
    private boolean keep_archivesDirtyFlag;

    /**
     * 属性 [CONTACT_AB_PC]
     *
     */
    @Mail_mass_mailingContact_ab_pcDefault(info = "默认规则")
    private Integer contact_ab_pc;

    @JsonIgnore
    private boolean contact_ab_pcDirtyFlag;

    /**
     * 属性 [SENT]
     *
     */
    @Mail_mass_mailingSentDefault(info = "默认规则")
    private Integer sent;

    @JsonIgnore
    private boolean sentDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @Mail_mass_mailingCampaign_id_textDefault(info = "默认规则")
    private String campaign_id_text;

    @JsonIgnore
    private boolean campaign_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Mail_mass_mailingUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mail_mass_mailingNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_mass_mailingWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     *
     */
    @Mail_mass_mailingMass_mailing_campaign_id_textDefault(info = "默认规则")
    private String mass_mailing_campaign_id_text;

    @JsonIgnore
    private boolean mass_mailing_campaign_id_textDirtyFlag;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @Mail_mass_mailingMedium_id_textDefault(info = "默认规则")
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_mass_mailingCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_mass_mailingCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Mail_mass_mailingUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID]
     *
     */
    @Mail_mass_mailingMass_mailing_campaign_idDefault(info = "默认规则")
    private Integer mass_mailing_campaign_id;

    @JsonIgnore
    private boolean mass_mailing_campaign_idDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @Mail_mass_mailingCampaign_idDefault(info = "默认规则")
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_mass_mailingWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @Mail_mass_mailingMedium_idDefault(info = "默认规则")
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @Mail_mass_mailingSource_idDefault(info = "默认规则")
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;


    /**
     * 获取 [SENT_DATE]
     */
    @JsonProperty("sent_date")
    public Timestamp getSent_date(){
        return sent_date ;
    }

    /**
     * 设置 [SENT_DATE]
     */
    @JsonProperty("sent_date")
    public void setSent_date(Timestamp  sent_date){
        this.sent_date = sent_date ;
        this.sent_dateDirtyFlag = true ;
    }

    /**
     * 获取 [SENT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getSent_dateDirtyFlag(){
        return sent_dateDirtyFlag ;
    }

    /**
     * 获取 [SALE_QUOTATION_COUNT]
     */
    @JsonProperty("sale_quotation_count")
    public Integer getSale_quotation_count(){
        return sale_quotation_count ;
    }

    /**
     * 设置 [SALE_QUOTATION_COUNT]
     */
    @JsonProperty("sale_quotation_count")
    public void setSale_quotation_count(Integer  sale_quotation_count){
        this.sale_quotation_count = sale_quotation_count ;
        this.sale_quotation_countDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_QUOTATION_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getSale_quotation_countDirtyFlag(){
        return sale_quotation_countDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MAILING_DOMAIN]
     */
    @JsonProperty("mailing_domain")
    public String getMailing_domain(){
        return mailing_domain ;
    }

    /**
     * 设置 [MAILING_DOMAIN]
     */
    @JsonProperty("mailing_domain")
    public void setMailing_domain(String  mailing_domain){
        this.mailing_domain = mailing_domain ;
        this.mailing_domainDirtyFlag = true ;
    }

    /**
     * 获取 [MAILING_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getMailing_domainDirtyFlag(){
        return mailing_domainDirtyFlag ;
    }

    /**
     * 获取 [OPENED]
     */
    @JsonProperty("opened")
    public Integer getOpened(){
        return opened ;
    }

    /**
     * 设置 [OPENED]
     */
    @JsonProperty("opened")
    public void setOpened(Integer  opened){
        this.opened = opened ;
        this.openedDirtyFlag = true ;
    }

    /**
     * 获取 [OPENED]脏标记
     */
    @JsonIgnore
    public boolean getOpenedDirtyFlag(){
        return openedDirtyFlag ;
    }

    /**
     * 获取 [REPLY_TO]
     */
    @JsonProperty("reply_to")
    public String getReply_to(){
        return reply_to ;
    }

    /**
     * 设置 [REPLY_TO]
     */
    @JsonProperty("reply_to")
    public void setReply_to(String  reply_to){
        this.reply_to = reply_to ;
        this.reply_toDirtyFlag = true ;
    }

    /**
     * 获取 [REPLY_TO]脏标记
     */
    @JsonIgnore
    public boolean getReply_toDirtyFlag(){
        return reply_toDirtyFlag ;
    }

    /**
     * 获取 [REPLIED]
     */
    @JsonProperty("replied")
    public Integer getReplied(){
        return replied ;
    }

    /**
     * 设置 [REPLIED]
     */
    @JsonProperty("replied")
    public void setReplied(Integer  replied){
        this.replied = replied ;
        this.repliedDirtyFlag = true ;
    }

    /**
     * 获取 [REPLIED]脏标记
     */
    @JsonIgnore
    public boolean getRepliedDirtyFlag(){
        return repliedDirtyFlag ;
    }

    /**
     * 获取 [CRM_LEAD_COUNT]
     */
    @JsonProperty("crm_lead_count")
    public Integer getCrm_lead_count(){
        return crm_lead_count ;
    }

    /**
     * 设置 [CRM_LEAD_COUNT]
     */
    @JsonProperty("crm_lead_count")
    public void setCrm_lead_count(Integer  crm_lead_count){
        this.crm_lead_count = crm_lead_count ;
        this.crm_lead_countDirtyFlag = true ;
    }

    /**
     * 获取 [CRM_LEAD_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getCrm_lead_countDirtyFlag(){
        return crm_lead_countDirtyFlag ;
    }

    /**
     * 获取 [MAILING_MODEL_REAL]
     */
    @JsonProperty("mailing_model_real")
    public String getMailing_model_real(){
        return mailing_model_real ;
    }

    /**
     * 设置 [MAILING_MODEL_REAL]
     */
    @JsonProperty("mailing_model_real")
    public void setMailing_model_real(String  mailing_model_real){
        this.mailing_model_real = mailing_model_real ;
        this.mailing_model_realDirtyFlag = true ;
    }

    /**
     * 获取 [MAILING_MODEL_REAL]脏标记
     */
    @JsonIgnore
    public boolean getMailing_model_realDirtyFlag(){
        return mailing_model_realDirtyFlag ;
    }

    /**
     * 获取 [CLICKS_RATIO]
     */
    @JsonProperty("clicks_ratio")
    public Integer getClicks_ratio(){
        return clicks_ratio ;
    }

    /**
     * 设置 [CLICKS_RATIO]
     */
    @JsonProperty("clicks_ratio")
    public void setClicks_ratio(Integer  clicks_ratio){
        this.clicks_ratio = clicks_ratio ;
        this.clicks_ratioDirtyFlag = true ;
    }

    /**
     * 获取 [CLICKS_RATIO]脏标记
     */
    @JsonIgnore
    public boolean getClicks_ratioDirtyFlag(){
        return clicks_ratioDirtyFlag ;
    }

    /**
     * 获取 [DELIVERED]
     */
    @JsonProperty("delivered")
    public Integer getDelivered(){
        return delivered ;
    }

    /**
     * 设置 [DELIVERED]
     */
    @JsonProperty("delivered")
    public void setDelivered(Integer  delivered){
        this.delivered = delivered ;
        this.deliveredDirtyFlag = true ;
    }

    /**
     * 获取 [DELIVERED]脏标记
     */
    @JsonIgnore
    public boolean getDeliveredDirtyFlag(){
        return deliveredDirtyFlag ;
    }

    /**
     * 获取 [MAILING_MODEL_ID]
     */
    @JsonProperty("mailing_model_id")
    public Integer getMailing_model_id(){
        return mailing_model_id ;
    }

    /**
     * 设置 [MAILING_MODEL_ID]
     */
    @JsonProperty("mailing_model_id")
    public void setMailing_model_id(Integer  mailing_model_id){
        this.mailing_model_id = mailing_model_id ;
        this.mailing_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAILING_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getMailing_model_idDirtyFlag(){
        return mailing_model_idDirtyFlag ;
    }

    /**
     * 获取 [REPLY_TO_MODE]
     */
    @JsonProperty("reply_to_mode")
    public String getReply_to_mode(){
        return reply_to_mode ;
    }

    /**
     * 设置 [REPLY_TO_MODE]
     */
    @JsonProperty("reply_to_mode")
    public void setReply_to_mode(String  reply_to_mode){
        this.reply_to_mode = reply_to_mode ;
        this.reply_to_modeDirtyFlag = true ;
    }

    /**
     * 获取 [REPLY_TO_MODE]脏标记
     */
    @JsonIgnore
    public boolean getReply_to_modeDirtyFlag(){
        return reply_to_modeDirtyFlag ;
    }

    /**
     * 获取 [SCHEDULE_DATE]
     */
    @JsonProperty("schedule_date")
    public Timestamp getSchedule_date(){
        return schedule_date ;
    }

    /**
     * 设置 [SCHEDULE_DATE]
     */
    @JsonProperty("schedule_date")
    public void setSchedule_date(Timestamp  schedule_date){
        this.schedule_date = schedule_date ;
        this.schedule_dateDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getSchedule_dateDirtyFlag(){
        return schedule_dateDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return email_from ;
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_FROM]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return email_fromDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [SALE_INVOICED_AMOUNT]
     */
    @JsonProperty("sale_invoiced_amount")
    public Integer getSale_invoiced_amount(){
        return sale_invoiced_amount ;
    }

    /**
     * 设置 [SALE_INVOICED_AMOUNT]
     */
    @JsonProperty("sale_invoiced_amount")
    public void setSale_invoiced_amount(Integer  sale_invoiced_amount){
        this.sale_invoiced_amount = sale_invoiced_amount ;
        this.sale_invoiced_amountDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_INVOICED_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getSale_invoiced_amountDirtyFlag(){
        return sale_invoiced_amountDirtyFlag ;
    }

    /**
     * 获取 [ATTACHMENT_IDS]
     */
    @JsonProperty("attachment_ids")
    public String getAttachment_ids(){
        return attachment_ids ;
    }

    /**
     * 设置 [ATTACHMENT_IDS]
     */
    @JsonProperty("attachment_ids")
    public void setAttachment_ids(String  attachment_ids){
        this.attachment_ids = attachment_ids ;
        this.attachment_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTACHMENT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_idsDirtyFlag(){
        return attachment_idsDirtyFlag ;
    }

    /**
     * 获取 [TOTAL]
     */
    @JsonProperty("total")
    public Integer getTotal(){
        return total ;
    }

    /**
     * 设置 [TOTAL]
     */
    @JsonProperty("total")
    public void setTotal(Integer  total){
        this.total = total ;
        this.totalDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getTotalDirtyFlag(){
        return totalDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [BOUNCED_RATIO]
     */
    @JsonProperty("bounced_ratio")
    public Integer getBounced_ratio(){
        return bounced_ratio ;
    }

    /**
     * 设置 [BOUNCED_RATIO]
     */
    @JsonProperty("bounced_ratio")
    public void setBounced_ratio(Integer  bounced_ratio){
        this.bounced_ratio = bounced_ratio ;
        this.bounced_ratioDirtyFlag = true ;
    }

    /**
     * 获取 [BOUNCED_RATIO]脏标记
     */
    @JsonIgnore
    public boolean getBounced_ratioDirtyFlag(){
        return bounced_ratioDirtyFlag ;
    }

    /**
     * 获取 [NEXT_DEPARTURE]
     */
    @JsonProperty("next_departure")
    public Timestamp getNext_departure(){
        return next_departure ;
    }

    /**
     * 设置 [NEXT_DEPARTURE]
     */
    @JsonProperty("next_departure")
    public void setNext_departure(Timestamp  next_departure){
        this.next_departure = next_departure ;
        this.next_departureDirtyFlag = true ;
    }

    /**
     * 获取 [NEXT_DEPARTURE]脏标记
     */
    @JsonIgnore
    public boolean getNext_departureDirtyFlag(){
        return next_departureDirtyFlag ;
    }

    /**
     * 获取 [MAIL_SERVER_ID]
     */
    @JsonProperty("mail_server_id")
    public Integer getMail_server_id(){
        return mail_server_id ;
    }

    /**
     * 设置 [MAIL_SERVER_ID]
     */
    @JsonProperty("mail_server_id")
    public void setMail_server_id(Integer  mail_server_id){
        this.mail_server_id = mail_server_id ;
        this.mail_server_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_SERVER_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_server_idDirtyFlag(){
        return mail_server_idDirtyFlag ;
    }

    /**
     * 获取 [IGNORED]
     */
    @JsonProperty("ignored")
    public Integer getIgnored(){
        return ignored ;
    }

    /**
     * 设置 [IGNORED]
     */
    @JsonProperty("ignored")
    public void setIgnored(Integer  ignored){
        this.ignored = ignored ;
        this.ignoredDirtyFlag = true ;
    }

    /**
     * 获取 [IGNORED]脏标记
     */
    @JsonIgnore
    public boolean getIgnoredDirtyFlag(){
        return ignoredDirtyFlag ;
    }

    /**
     * 获取 [SCHEDULED]
     */
    @JsonProperty("scheduled")
    public Integer getScheduled(){
        return scheduled ;
    }

    /**
     * 设置 [SCHEDULED]
     */
    @JsonProperty("scheduled")
    public void setScheduled(Integer  scheduled){
        this.scheduled = scheduled ;
        this.scheduledDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULED]脏标记
     */
    @JsonIgnore
    public boolean getScheduledDirtyFlag(){
        return scheduledDirtyFlag ;
    }

    /**
     * 获取 [CRM_LEAD_ACTIVATED]
     */
    @JsonProperty("crm_lead_activated")
    public String getCrm_lead_activated(){
        return crm_lead_activated ;
    }

    /**
     * 设置 [CRM_LEAD_ACTIVATED]
     */
    @JsonProperty("crm_lead_activated")
    public void setCrm_lead_activated(String  crm_lead_activated){
        this.crm_lead_activated = crm_lead_activated ;
        this.crm_lead_activatedDirtyFlag = true ;
    }

    /**
     * 获取 [CRM_LEAD_ACTIVATED]脏标记
     */
    @JsonIgnore
    public boolean getCrm_lead_activatedDirtyFlag(){
        return crm_lead_activatedDirtyFlag ;
    }

    /**
     * 获取 [CLICKED]
     */
    @JsonProperty("clicked")
    public Integer getClicked(){
        return clicked ;
    }

    /**
     * 设置 [CLICKED]
     */
    @JsonProperty("clicked")
    public void setClicked(Integer  clicked){
        this.clicked = clicked ;
        this.clickedDirtyFlag = true ;
    }

    /**
     * 获取 [CLICKED]脏标记
     */
    @JsonIgnore
    public boolean getClickedDirtyFlag(){
        return clickedDirtyFlag ;
    }

    /**
     * 获取 [REPLIED_RATIO]
     */
    @JsonProperty("replied_ratio")
    public Integer getReplied_ratio(){
        return replied_ratio ;
    }

    /**
     * 设置 [REPLIED_RATIO]
     */
    @JsonProperty("replied_ratio")
    public void setReplied_ratio(Integer  replied_ratio){
        this.replied_ratio = replied_ratio ;
        this.replied_ratioDirtyFlag = true ;
    }

    /**
     * 获取 [REPLIED_RATIO]脏标记
     */
    @JsonIgnore
    public boolean getReplied_ratioDirtyFlag(){
        return replied_ratioDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [STATISTICS_IDS]
     */
    @JsonProperty("statistics_ids")
    public String getStatistics_ids(){
        return statistics_ids ;
    }

    /**
     * 设置 [STATISTICS_IDS]
     */
    @JsonProperty("statistics_ids")
    public void setStatistics_ids(String  statistics_ids){
        this.statistics_ids = statistics_ids ;
        this.statistics_idsDirtyFlag = true ;
    }

    /**
     * 获取 [STATISTICS_IDS]脏标记
     */
    @JsonIgnore
    public boolean getStatistics_idsDirtyFlag(){
        return statistics_idsDirtyFlag ;
    }

    /**
     * 获取 [OPENED_RATIO]
     */
    @JsonProperty("opened_ratio")
    public Integer getOpened_ratio(){
        return opened_ratio ;
    }

    /**
     * 设置 [OPENED_RATIO]
     */
    @JsonProperty("opened_ratio")
    public void setOpened_ratio(Integer  opened_ratio){
        this.opened_ratio = opened_ratio ;
        this.opened_ratioDirtyFlag = true ;
    }

    /**
     * 获取 [OPENED_RATIO]脏标记
     */
    @JsonIgnore
    public boolean getOpened_ratioDirtyFlag(){
        return opened_ratioDirtyFlag ;
    }

    /**
     * 获取 [BODY_HTML]
     */
    @JsonProperty("body_html")
    public String getBody_html(){
        return body_html ;
    }

    /**
     * 设置 [BODY_HTML]
     */
    @JsonProperty("body_html")
    public void setBody_html(String  body_html){
        this.body_html = body_html ;
        this.body_htmlDirtyFlag = true ;
    }

    /**
     * 获取 [BODY_HTML]脏标记
     */
    @JsonIgnore
    public boolean getBody_htmlDirtyFlag(){
        return body_htmlDirtyFlag ;
    }

    /**
     * 获取 [BOUNCED]
     */
    @JsonProperty("bounced")
    public Integer getBounced(){
        return bounced ;
    }

    /**
     * 设置 [BOUNCED]
     */
    @JsonProperty("bounced")
    public void setBounced(Integer  bounced){
        this.bounced = bounced ;
        this.bouncedDirtyFlag = true ;
    }

    /**
     * 获取 [BOUNCED]脏标记
     */
    @JsonIgnore
    public boolean getBouncedDirtyFlag(){
        return bouncedDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [MAILING_MODEL_NAME]
     */
    @JsonProperty("mailing_model_name")
    public String getMailing_model_name(){
        return mailing_model_name ;
    }

    /**
     * 设置 [MAILING_MODEL_NAME]
     */
    @JsonProperty("mailing_model_name")
    public void setMailing_model_name(String  mailing_model_name){
        this.mailing_model_name = mailing_model_name ;
        this.mailing_model_nameDirtyFlag = true ;
    }

    /**
     * 获取 [MAILING_MODEL_NAME]脏标记
     */
    @JsonIgnore
    public boolean getMailing_model_nameDirtyFlag(){
        return mailing_model_nameDirtyFlag ;
    }

    /**
     * 获取 [CONTACT_LIST_IDS]
     */
    @JsonProperty("contact_list_ids")
    public String getContact_list_ids(){
        return contact_list_ids ;
    }

    /**
     * 设置 [CONTACT_LIST_IDS]
     */
    @JsonProperty("contact_list_ids")
    public void setContact_list_ids(String  contact_list_ids){
        this.contact_list_ids = contact_list_ids ;
        this.contact_list_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CONTACT_LIST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getContact_list_idsDirtyFlag(){
        return contact_list_idsDirtyFlag ;
    }

    /**
     * 获取 [EXPECTED]
     */
    @JsonProperty("expected")
    public Integer getExpected(){
        return expected ;
    }

    /**
     * 设置 [EXPECTED]
     */
    @JsonProperty("expected")
    public void setExpected(Integer  expected){
        this.expected = expected ;
        this.expectedDirtyFlag = true ;
    }

    /**
     * 获取 [EXPECTED]脏标记
     */
    @JsonIgnore
    public boolean getExpectedDirtyFlag(){
        return expectedDirtyFlag ;
    }

    /**
     * 获取 [FAILED]
     */
    @JsonProperty("failed")
    public Integer getFailed(){
        return failed ;
    }

    /**
     * 设置 [FAILED]
     */
    @JsonProperty("failed")
    public void setFailed(Integer  failed){
        this.failed = failed ;
        this.failedDirtyFlag = true ;
    }

    /**
     * 获取 [FAILED]脏标记
     */
    @JsonIgnore
    public boolean getFailedDirtyFlag(){
        return failedDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [RECEIVED_RATIO]
     */
    @JsonProperty("received_ratio")
    public Integer getReceived_ratio(){
        return received_ratio ;
    }

    /**
     * 设置 [RECEIVED_RATIO]
     */
    @JsonProperty("received_ratio")
    public void setReceived_ratio(Integer  received_ratio){
        this.received_ratio = received_ratio ;
        this.received_ratioDirtyFlag = true ;
    }

    /**
     * 获取 [RECEIVED_RATIO]脏标记
     */
    @JsonIgnore
    public boolean getReceived_ratioDirtyFlag(){
        return received_ratioDirtyFlag ;
    }

    /**
     * 获取 [CRM_OPPORTUNITIES_COUNT]
     */
    @JsonProperty("crm_opportunities_count")
    public Integer getCrm_opportunities_count(){
        return crm_opportunities_count ;
    }

    /**
     * 设置 [CRM_OPPORTUNITIES_COUNT]
     */
    @JsonProperty("crm_opportunities_count")
    public void setCrm_opportunities_count(Integer  crm_opportunities_count){
        this.crm_opportunities_count = crm_opportunities_count ;
        this.crm_opportunities_countDirtyFlag = true ;
    }

    /**
     * 获取 [CRM_OPPORTUNITIES_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getCrm_opportunities_countDirtyFlag(){
        return crm_opportunities_countDirtyFlag ;
    }

    /**
     * 获取 [KEEP_ARCHIVES]
     */
    @JsonProperty("keep_archives")
    public String getKeep_archives(){
        return keep_archives ;
    }

    /**
     * 设置 [KEEP_ARCHIVES]
     */
    @JsonProperty("keep_archives")
    public void setKeep_archives(String  keep_archives){
        this.keep_archives = keep_archives ;
        this.keep_archivesDirtyFlag = true ;
    }

    /**
     * 获取 [KEEP_ARCHIVES]脏标记
     */
    @JsonIgnore
    public boolean getKeep_archivesDirtyFlag(){
        return keep_archivesDirtyFlag ;
    }

    /**
     * 获取 [CONTACT_AB_PC]
     */
    @JsonProperty("contact_ab_pc")
    public Integer getContact_ab_pc(){
        return contact_ab_pc ;
    }

    /**
     * 设置 [CONTACT_AB_PC]
     */
    @JsonProperty("contact_ab_pc")
    public void setContact_ab_pc(Integer  contact_ab_pc){
        this.contact_ab_pc = contact_ab_pc ;
        this.contact_ab_pcDirtyFlag = true ;
    }

    /**
     * 获取 [CONTACT_AB_PC]脏标记
     */
    @JsonIgnore
    public boolean getContact_ab_pcDirtyFlag(){
        return contact_ab_pcDirtyFlag ;
    }

    /**
     * 获取 [SENT]
     */
    @JsonProperty("sent")
    public Integer getSent(){
        return sent ;
    }

    /**
     * 设置 [SENT]
     */
    @JsonProperty("sent")
    public void setSent(Integer  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

    /**
     * 获取 [SENT]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return sentDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return campaign_id_text ;
    }

    /**
     * 设置 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("mass_mailing_campaign_id_text")
    public String getMass_mailing_campaign_id_text(){
        return mass_mailing_campaign_id_text ;
    }

    /**
     * 设置 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("mass_mailing_campaign_id_text")
    public void setMass_mailing_campaign_id_text(String  mass_mailing_campaign_id_text){
        this.mass_mailing_campaign_id_text = mass_mailing_campaign_id_text ;
        this.mass_mailing_campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_id_textDirtyFlag(){
        return mass_mailing_campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return medium_id_text ;
    }

    /**
     * 设置 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public Integer getMass_mailing_campaign_id(){
        return mass_mailing_campaign_id ;
    }

    /**
     * 设置 [MASS_MAILING_CAMPAIGN_ID]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public void setMass_mailing_campaign_id(Integer  mass_mailing_campaign_id){
        this.mass_mailing_campaign_id = mass_mailing_campaign_id ;
        this.mass_mailing_campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_idDirtyFlag(){
        return mass_mailing_campaign_idDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return campaign_id ;
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return campaign_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return medium_id ;
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return medium_idDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return source_id ;
    }

    /**
     * 设置 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return source_idDirtyFlag ;
    }



    public Mail_mass_mailing toDO() {
        Mail_mass_mailing srfdomain = new Mail_mass_mailing();
        if(getSent_dateDirtyFlag())
            srfdomain.setSent_date(sent_date);
        if(getSale_quotation_countDirtyFlag())
            srfdomain.setSale_quotation_count(sale_quotation_count);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMailing_domainDirtyFlag())
            srfdomain.setMailing_domain(mailing_domain);
        if(getOpenedDirtyFlag())
            srfdomain.setOpened(opened);
        if(getReply_toDirtyFlag())
            srfdomain.setReply_to(reply_to);
        if(getRepliedDirtyFlag())
            srfdomain.setReplied(replied);
        if(getCrm_lead_countDirtyFlag())
            srfdomain.setCrm_lead_count(crm_lead_count);
        if(getMailing_model_realDirtyFlag())
            srfdomain.setMailing_model_real(mailing_model_real);
        if(getClicks_ratioDirtyFlag())
            srfdomain.setClicks_ratio(clicks_ratio);
        if(getDeliveredDirtyFlag())
            srfdomain.setDelivered(delivered);
        if(getMailing_model_idDirtyFlag())
            srfdomain.setMailing_model_id(mailing_model_id);
        if(getReply_to_modeDirtyFlag())
            srfdomain.setReply_to_mode(reply_to_mode);
        if(getSchedule_dateDirtyFlag())
            srfdomain.setSchedule_date(schedule_date);
        if(getEmail_fromDirtyFlag())
            srfdomain.setEmail_from(email_from);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getSale_invoiced_amountDirtyFlag())
            srfdomain.setSale_invoiced_amount(sale_invoiced_amount);
        if(getAttachment_idsDirtyFlag())
            srfdomain.setAttachment_ids(attachment_ids);
        if(getTotalDirtyFlag())
            srfdomain.setTotal(total);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getBounced_ratioDirtyFlag())
            srfdomain.setBounced_ratio(bounced_ratio);
        if(getNext_departureDirtyFlag())
            srfdomain.setNext_departure(next_departure);
        if(getMail_server_idDirtyFlag())
            srfdomain.setMail_server_id(mail_server_id);
        if(getIgnoredDirtyFlag())
            srfdomain.setIgnored(ignored);
        if(getScheduledDirtyFlag())
            srfdomain.setScheduled(scheduled);
        if(getCrm_lead_activatedDirtyFlag())
            srfdomain.setCrm_lead_activated(crm_lead_activated);
        if(getClickedDirtyFlag())
            srfdomain.setClicked(clicked);
        if(getReplied_ratioDirtyFlag())
            srfdomain.setReplied_ratio(replied_ratio);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getStatistics_idsDirtyFlag())
            srfdomain.setStatistics_ids(statistics_ids);
        if(getOpened_ratioDirtyFlag())
            srfdomain.setOpened_ratio(opened_ratio);
        if(getBody_htmlDirtyFlag())
            srfdomain.setBody_html(body_html);
        if(getBouncedDirtyFlag())
            srfdomain.setBounced(bounced);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getMailing_model_nameDirtyFlag())
            srfdomain.setMailing_model_name(mailing_model_name);
        if(getContact_list_idsDirtyFlag())
            srfdomain.setContact_list_ids(contact_list_ids);
        if(getExpectedDirtyFlag())
            srfdomain.setExpected(expected);
        if(getFailedDirtyFlag())
            srfdomain.setFailed(failed);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getReceived_ratioDirtyFlag())
            srfdomain.setReceived_ratio(received_ratio);
        if(getCrm_opportunities_countDirtyFlag())
            srfdomain.setCrm_opportunities_count(crm_opportunities_count);
        if(getKeep_archivesDirtyFlag())
            srfdomain.setKeep_archives(keep_archives);
        if(getContact_ab_pcDirtyFlag())
            srfdomain.setContact_ab_pc(contact_ab_pc);
        if(getSentDirtyFlag())
            srfdomain.setSent(sent);
        if(getCampaign_id_textDirtyFlag())
            srfdomain.setCampaign_id_text(campaign_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getMass_mailing_campaign_id_textDirtyFlag())
            srfdomain.setMass_mailing_campaign_id_text(mass_mailing_campaign_id_text);
        if(getMedium_id_textDirtyFlag())
            srfdomain.setMedium_id_text(medium_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getMass_mailing_campaign_idDirtyFlag())
            srfdomain.setMass_mailing_campaign_id(mass_mailing_campaign_id);
        if(getCampaign_idDirtyFlag())
            srfdomain.setCampaign_id(campaign_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getMedium_idDirtyFlag())
            srfdomain.setMedium_id(medium_id);
        if(getSource_idDirtyFlag())
            srfdomain.setSource_id(source_id);

        return srfdomain;
    }

    public void fromDO(Mail_mass_mailing srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getSent_dateDirtyFlag())
            this.setSent_date(srfdomain.getSent_date());
        if(srfdomain.getSale_quotation_countDirtyFlag())
            this.setSale_quotation_count(srfdomain.getSale_quotation_count());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMailing_domainDirtyFlag())
            this.setMailing_domain(srfdomain.getMailing_domain());
        if(srfdomain.getOpenedDirtyFlag())
            this.setOpened(srfdomain.getOpened());
        if(srfdomain.getReply_toDirtyFlag())
            this.setReply_to(srfdomain.getReply_to());
        if(srfdomain.getRepliedDirtyFlag())
            this.setReplied(srfdomain.getReplied());
        if(srfdomain.getCrm_lead_countDirtyFlag())
            this.setCrm_lead_count(srfdomain.getCrm_lead_count());
        if(srfdomain.getMailing_model_realDirtyFlag())
            this.setMailing_model_real(srfdomain.getMailing_model_real());
        if(srfdomain.getClicks_ratioDirtyFlag())
            this.setClicks_ratio(srfdomain.getClicks_ratio());
        if(srfdomain.getDeliveredDirtyFlag())
            this.setDelivered(srfdomain.getDelivered());
        if(srfdomain.getMailing_model_idDirtyFlag())
            this.setMailing_model_id(srfdomain.getMailing_model_id());
        if(srfdomain.getReply_to_modeDirtyFlag())
            this.setReply_to_mode(srfdomain.getReply_to_mode());
        if(srfdomain.getSchedule_dateDirtyFlag())
            this.setSchedule_date(srfdomain.getSchedule_date());
        if(srfdomain.getEmail_fromDirtyFlag())
            this.setEmail_from(srfdomain.getEmail_from());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getSale_invoiced_amountDirtyFlag())
            this.setSale_invoiced_amount(srfdomain.getSale_invoiced_amount());
        if(srfdomain.getAttachment_idsDirtyFlag())
            this.setAttachment_ids(srfdomain.getAttachment_ids());
        if(srfdomain.getTotalDirtyFlag())
            this.setTotal(srfdomain.getTotal());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getBounced_ratioDirtyFlag())
            this.setBounced_ratio(srfdomain.getBounced_ratio());
        if(srfdomain.getNext_departureDirtyFlag())
            this.setNext_departure(srfdomain.getNext_departure());
        if(srfdomain.getMail_server_idDirtyFlag())
            this.setMail_server_id(srfdomain.getMail_server_id());
        if(srfdomain.getIgnoredDirtyFlag())
            this.setIgnored(srfdomain.getIgnored());
        if(srfdomain.getScheduledDirtyFlag())
            this.setScheduled(srfdomain.getScheduled());
        if(srfdomain.getCrm_lead_activatedDirtyFlag())
            this.setCrm_lead_activated(srfdomain.getCrm_lead_activated());
        if(srfdomain.getClickedDirtyFlag())
            this.setClicked(srfdomain.getClicked());
        if(srfdomain.getReplied_ratioDirtyFlag())
            this.setReplied_ratio(srfdomain.getReplied_ratio());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getStatistics_idsDirtyFlag())
            this.setStatistics_ids(srfdomain.getStatistics_ids());
        if(srfdomain.getOpened_ratioDirtyFlag())
            this.setOpened_ratio(srfdomain.getOpened_ratio());
        if(srfdomain.getBody_htmlDirtyFlag())
            this.setBody_html(srfdomain.getBody_html());
        if(srfdomain.getBouncedDirtyFlag())
            this.setBounced(srfdomain.getBounced());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getMailing_model_nameDirtyFlag())
            this.setMailing_model_name(srfdomain.getMailing_model_name());
        if(srfdomain.getContact_list_idsDirtyFlag())
            this.setContact_list_ids(srfdomain.getContact_list_ids());
        if(srfdomain.getExpectedDirtyFlag())
            this.setExpected(srfdomain.getExpected());
        if(srfdomain.getFailedDirtyFlag())
            this.setFailed(srfdomain.getFailed());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getReceived_ratioDirtyFlag())
            this.setReceived_ratio(srfdomain.getReceived_ratio());
        if(srfdomain.getCrm_opportunities_countDirtyFlag())
            this.setCrm_opportunities_count(srfdomain.getCrm_opportunities_count());
        if(srfdomain.getKeep_archivesDirtyFlag())
            this.setKeep_archives(srfdomain.getKeep_archives());
        if(srfdomain.getContact_ab_pcDirtyFlag())
            this.setContact_ab_pc(srfdomain.getContact_ab_pc());
        if(srfdomain.getSentDirtyFlag())
            this.setSent(srfdomain.getSent());
        if(srfdomain.getCampaign_id_textDirtyFlag())
            this.setCampaign_id_text(srfdomain.getCampaign_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getMass_mailing_campaign_id_textDirtyFlag())
            this.setMass_mailing_campaign_id_text(srfdomain.getMass_mailing_campaign_id_text());
        if(srfdomain.getMedium_id_textDirtyFlag())
            this.setMedium_id_text(srfdomain.getMedium_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getMass_mailing_campaign_idDirtyFlag())
            this.setMass_mailing_campaign_id(srfdomain.getMass_mailing_campaign_id());
        if(srfdomain.getCampaign_idDirtyFlag())
            this.setCampaign_id(srfdomain.getCampaign_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getMedium_idDirtyFlag())
            this.setMedium_id(srfdomain.getMedium_id());
        if(srfdomain.getSource_idDirtyFlag())
            this.setSource_id(srfdomain.getSource_id());

    }

    public List<Mail_mass_mailingDTO> fromDOPage(List<Mail_mass_mailing> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_mass_mailingDTO> dtos=new ArrayList<Mail_mass_mailingDTO>();
        for(Mail_mass_mailing domain : poPage) {
            Mail_mass_mailingDTO dto = new Mail_mass_mailingDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

