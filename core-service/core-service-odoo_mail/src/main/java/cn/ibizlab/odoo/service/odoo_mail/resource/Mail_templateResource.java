package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_templateDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_templateService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_templateSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_template" })
@RestController
@RequestMapping("")
public class Mail_templateResource {

    @Autowired
    private IMail_templateService mail_templateService;

    public IMail_templateService getMail_templateService() {
        return this.mail_templateService;
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_template" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_templates/{mail_template_id}")
    public ResponseEntity<Mail_templateDTO> get(@PathVariable("mail_template_id") Integer mail_template_id) {
        Mail_templateDTO dto = new Mail_templateDTO();
        Mail_template domain = mail_templateService.get(mail_template_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_template" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_templates/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_templateDTO> mail_templatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_template" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_templates/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_templateDTO> mail_templatedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_template" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_templates/{mail_template_id}")

    public ResponseEntity<Mail_templateDTO> update(@PathVariable("mail_template_id") Integer mail_template_id, @RequestBody Mail_templateDTO mail_templatedto) {
		Mail_template domain = mail_templatedto.toDO();
        domain.setId(mail_template_id);
		mail_templateService.update(domain);
		Mail_templateDTO dto = new Mail_templateDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_template" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_templates")

    public ResponseEntity<Mail_templateDTO> create(@RequestBody Mail_templateDTO mail_templatedto) {
        Mail_templateDTO dto = new Mail_templateDTO();
        Mail_template domain = mail_templatedto.toDO();
		mail_templateService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_template" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_templates/{mail_template_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_template_id") Integer mail_template_id) {
        Mail_templateDTO mail_templatedto = new Mail_templateDTO();
		Mail_template domain = new Mail_template();
		mail_templatedto.setId(mail_template_id);
		domain.setId(mail_template_id);
        Boolean rst = mail_templateService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_template" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_templates/createBatch")
    public ResponseEntity<Boolean> createBatchMail_template(@RequestBody List<Mail_templateDTO> mail_templatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_template" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_templates/fetchdefault")
	public ResponseEntity<Page<Mail_templateDTO>> fetchDefault(Mail_templateSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_templateDTO> list = new ArrayList<Mail_templateDTO>();
        
        Page<Mail_template> domains = mail_templateService.searchDefault(context) ;
        for(Mail_template mail_template : domains.getContent()){
            Mail_templateDTO dto = new Mail_templateDTO();
            dto.fromDO(mail_template);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
