package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_shortcodeDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_shortcode;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_shortcodeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_shortcodeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_shortcode" })
@RestController
@RequestMapping("")
public class Mail_shortcodeResource {

    @Autowired
    private IMail_shortcodeService mail_shortcodeService;

    public IMail_shortcodeService getMail_shortcodeService() {
        return this.mail_shortcodeService;
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_shortcode" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_shortcodes/{mail_shortcode_id}")
    public ResponseEntity<Mail_shortcodeDTO> get(@PathVariable("mail_shortcode_id") Integer mail_shortcode_id) {
        Mail_shortcodeDTO dto = new Mail_shortcodeDTO();
        Mail_shortcode domain = mail_shortcodeService.get(mail_shortcode_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_shortcode" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_shortcodes")

    public ResponseEntity<Mail_shortcodeDTO> create(@RequestBody Mail_shortcodeDTO mail_shortcodedto) {
        Mail_shortcodeDTO dto = new Mail_shortcodeDTO();
        Mail_shortcode domain = mail_shortcodedto.toDO();
		mail_shortcodeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_shortcode" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_shortcodes/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_shortcodeDTO> mail_shortcodedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_shortcode" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_shortcodes/{mail_shortcode_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_shortcode_id") Integer mail_shortcode_id) {
        Mail_shortcodeDTO mail_shortcodedto = new Mail_shortcodeDTO();
		Mail_shortcode domain = new Mail_shortcode();
		mail_shortcodedto.setId(mail_shortcode_id);
		domain.setId(mail_shortcode_id);
        Boolean rst = mail_shortcodeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_shortcode" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_shortcodes/createBatch")
    public ResponseEntity<Boolean> createBatchMail_shortcode(@RequestBody List<Mail_shortcodeDTO> mail_shortcodedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_shortcode" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_shortcodes/{mail_shortcode_id}")

    public ResponseEntity<Mail_shortcodeDTO> update(@PathVariable("mail_shortcode_id") Integer mail_shortcode_id, @RequestBody Mail_shortcodeDTO mail_shortcodedto) {
		Mail_shortcode domain = mail_shortcodedto.toDO();
        domain.setId(mail_shortcode_id);
		mail_shortcodeService.update(domain);
		Mail_shortcodeDTO dto = new Mail_shortcodeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_shortcode" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_shortcodes/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_shortcodeDTO> mail_shortcodedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_shortcode" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_shortcodes/fetchdefault")
	public ResponseEntity<Page<Mail_shortcodeDTO>> fetchDefault(Mail_shortcodeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_shortcodeDTO> list = new ArrayList<Mail_shortcodeDTO>();
        
        Page<Mail_shortcode> domains = mail_shortcodeService.searchDefault(context) ;
        for(Mail_shortcode mail_shortcode : domains.getContent()){
            Mail_shortcodeDTO dto = new Mail_shortcodeDTO();
            dto.fromDO(mail_shortcode);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
