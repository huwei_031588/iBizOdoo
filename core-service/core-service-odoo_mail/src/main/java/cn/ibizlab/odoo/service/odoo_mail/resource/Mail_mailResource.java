package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_mailDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mailService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mailSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_mail" })
@RestController
@RequestMapping("")
public class Mail_mailResource {

    @Autowired
    private IMail_mailService mail_mailService;

    public IMail_mailService getMail_mailService() {
        return this.mail_mailService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_mail" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mails/createBatch")
    public ResponseEntity<Boolean> createBatchMail_mail(@RequestBody List<Mail_mailDTO> mail_maildtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_mail" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mails/{mail_mail_id}")

    public ResponseEntity<Mail_mailDTO> update(@PathVariable("mail_mail_id") Integer mail_mail_id, @RequestBody Mail_mailDTO mail_maildto) {
		Mail_mail domain = mail_maildto.toDO();
        domain.setId(mail_mail_id);
		mail_mailService.update(domain);
		Mail_mailDTO dto = new Mail_mailDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_mail" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mails/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mailDTO> mail_maildtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_mail" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mails/{mail_mail_id}")
    public ResponseEntity<Mail_mailDTO> get(@PathVariable("mail_mail_id") Integer mail_mail_id) {
        Mail_mailDTO dto = new Mail_mailDTO();
        Mail_mail domain = mail_mailService.get(mail_mail_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_mail" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mails/{mail_mail_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mail_id") Integer mail_mail_id) {
        Mail_mailDTO mail_maildto = new Mail_mailDTO();
		Mail_mail domain = new Mail_mail();
		mail_maildto.setId(mail_mail_id);
		domain.setId(mail_mail_id);
        Boolean rst = mail_mailService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_mail" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mails")

    public ResponseEntity<Mail_mailDTO> create(@RequestBody Mail_mailDTO mail_maildto) {
        Mail_mailDTO dto = new Mail_mailDTO();
        Mail_mail domain = mail_maildto.toDO();
		mail_mailService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_mail" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mails/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_mailDTO> mail_maildtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_mail" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_mails/fetchdefault")
	public ResponseEntity<Page<Mail_mailDTO>> fetchDefault(Mail_mailSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_mailDTO> list = new ArrayList<Mail_mailDTO>();
        
        Page<Mail_mail> domains = mail_mailService.searchDefault(context) ;
        for(Mail_mail mail_mail : domains.getContent()){
            Mail_mailDTO dto = new Mail_mailDTO();
            dto.fromDO(mail_mail);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
