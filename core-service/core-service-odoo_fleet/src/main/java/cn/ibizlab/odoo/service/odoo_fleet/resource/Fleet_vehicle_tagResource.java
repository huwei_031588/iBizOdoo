package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicle_tagDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_tag;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_tagService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_tagSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle_tag" })
@RestController
@RequestMapping("")
public class Fleet_vehicle_tagResource {

    @Autowired
    private IFleet_vehicle_tagService fleet_vehicle_tagService;

    public IFleet_vehicle_tagService getFleet_vehicle_tagService() {
        return this.fleet_vehicle_tagService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle_tag" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_tags/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle_tag(@RequestBody List<Fleet_vehicle_tagDTO> fleet_vehicle_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle_tag" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_tags/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicle_tagDTO> fleet_vehicle_tagdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle_tag" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_tags/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_tagDTO> fleet_vehicle_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_tag" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_tags/{fleet_vehicle_tag_id}")
    public ResponseEntity<Fleet_vehicle_tagDTO> get(@PathVariable("fleet_vehicle_tag_id") Integer fleet_vehicle_tag_id) {
        Fleet_vehicle_tagDTO dto = new Fleet_vehicle_tagDTO();
        Fleet_vehicle_tag domain = fleet_vehicle_tagService.get(fleet_vehicle_tag_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_tag" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_tags/{fleet_vehicle_tag_id}")

    public ResponseEntity<Fleet_vehicle_tagDTO> update(@PathVariable("fleet_vehicle_tag_id") Integer fleet_vehicle_tag_id, @RequestBody Fleet_vehicle_tagDTO fleet_vehicle_tagdto) {
		Fleet_vehicle_tag domain = fleet_vehicle_tagdto.toDO();
        domain.setId(fleet_vehicle_tag_id);
		fleet_vehicle_tagService.update(domain);
		Fleet_vehicle_tagDTO dto = new Fleet_vehicle_tagDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_tag" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_tags")

    public ResponseEntity<Fleet_vehicle_tagDTO> create(@RequestBody Fleet_vehicle_tagDTO fleet_vehicle_tagdto) {
        Fleet_vehicle_tagDTO dto = new Fleet_vehicle_tagDTO();
        Fleet_vehicle_tag domain = fleet_vehicle_tagdto.toDO();
		fleet_vehicle_tagService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_tag" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_tags/{fleet_vehicle_tag_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_tag_id") Integer fleet_vehicle_tag_id) {
        Fleet_vehicle_tagDTO fleet_vehicle_tagdto = new Fleet_vehicle_tagDTO();
		Fleet_vehicle_tag domain = new Fleet_vehicle_tag();
		fleet_vehicle_tagdto.setId(fleet_vehicle_tag_id);
		domain.setId(fleet_vehicle_tag_id);
        Boolean rst = fleet_vehicle_tagService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle_tag" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicle_tags/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicle_tagDTO>> fetchDefault(Fleet_vehicle_tagSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicle_tagDTO> list = new ArrayList<Fleet_vehicle_tagDTO>();
        
        Page<Fleet_vehicle_tag> domains = fleet_vehicle_tagService.searchDefault(context) ;
        for(Fleet_vehicle_tag fleet_vehicle_tag : domains.getContent()){
            Fleet_vehicle_tagDTO dto = new Fleet_vehicle_tagDTO();
            dto.fromDO(fleet_vehicle_tag);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
