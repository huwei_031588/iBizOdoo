package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicle_modelDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_modelService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_modelSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle_model" })
@RestController
@RequestMapping("")
public class Fleet_vehicle_modelResource {

    @Autowired
    private IFleet_vehicle_modelService fleet_vehicle_modelService;

    public IFleet_vehicle_modelService getFleet_vehicle_modelService() {
        return this.fleet_vehicle_modelService;
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_model" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_models/{fleet_vehicle_model_id}")

    public ResponseEntity<Fleet_vehicle_modelDTO> update(@PathVariable("fleet_vehicle_model_id") Integer fleet_vehicle_model_id, @RequestBody Fleet_vehicle_modelDTO fleet_vehicle_modeldto) {
		Fleet_vehicle_model domain = fleet_vehicle_modeldto.toDO();
        domain.setId(fleet_vehicle_model_id);
		fleet_vehicle_modelService.update(domain);
		Fleet_vehicle_modelDTO dto = new Fleet_vehicle_modelDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_model" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_models/{fleet_vehicle_model_id}")
    public ResponseEntity<Fleet_vehicle_modelDTO> get(@PathVariable("fleet_vehicle_model_id") Integer fleet_vehicle_model_id) {
        Fleet_vehicle_modelDTO dto = new Fleet_vehicle_modelDTO();
        Fleet_vehicle_model domain = fleet_vehicle_modelService.get(fleet_vehicle_model_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle_model" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_models/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicle_modelDTO> fleet_vehicle_modeldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle_model" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_models/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_modelDTO> fleet_vehicle_modeldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_model" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_models/{fleet_vehicle_model_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_model_id") Integer fleet_vehicle_model_id) {
        Fleet_vehicle_modelDTO fleet_vehicle_modeldto = new Fleet_vehicle_modelDTO();
		Fleet_vehicle_model domain = new Fleet_vehicle_model();
		fleet_vehicle_modeldto.setId(fleet_vehicle_model_id);
		domain.setId(fleet_vehicle_model_id);
        Boolean rst = fleet_vehicle_modelService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle_model" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_models/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle_model(@RequestBody List<Fleet_vehicle_modelDTO> fleet_vehicle_modeldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_model" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_models")

    public ResponseEntity<Fleet_vehicle_modelDTO> create(@RequestBody Fleet_vehicle_modelDTO fleet_vehicle_modeldto) {
        Fleet_vehicle_modelDTO dto = new Fleet_vehicle_modelDTO();
        Fleet_vehicle_model domain = fleet_vehicle_modeldto.toDO();
		fleet_vehicle_modelService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle_model" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicle_models/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicle_modelDTO>> fetchDefault(Fleet_vehicle_modelSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicle_modelDTO> list = new ArrayList<Fleet_vehicle_modelDTO>();
        
        Page<Fleet_vehicle_model> domains = fleet_vehicle_modelService.searchDefault(context) ;
        for(Fleet_vehicle_model fleet_vehicle_model : domains.getContent()){
            Fleet_vehicle_modelDTO dto = new Fleet_vehicle_modelDTO();
            dto.fromDO(fleet_vehicle_model);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
