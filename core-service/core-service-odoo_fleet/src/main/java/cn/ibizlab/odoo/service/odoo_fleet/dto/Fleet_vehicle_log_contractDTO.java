package cn.ibizlab.odoo.service.odoo_fleet.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_fleet.valuerule.anno.fleet_vehicle_log_contract.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Fleet_vehicle_log_contractDTO]
 */
public class Fleet_vehicle_log_contractDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Fleet_vehicle_log_contractActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [INS_REF]
     *
     */
    @Fleet_vehicle_log_contractIns_refDefault(info = "默认规则")
    private String ins_ref;

    @JsonIgnore
    private boolean ins_refDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Fleet_vehicle_log_contractActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [SUM_COST]
     *
     */
    @Fleet_vehicle_log_contractSum_costDefault(info = "默认规则")
    private Double sum_cost;

    @JsonIgnore
    private boolean sum_costDirtyFlag;

    /**
     * 属性 [NOTES]
     *
     */
    @Fleet_vehicle_log_contractNotesDefault(info = "默认规则")
    private String notes;

    @JsonIgnore
    private boolean notesDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Fleet_vehicle_log_contractDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Fleet_vehicle_log_contractMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [EXPIRATION_DATE]
     *
     */
    @Fleet_vehicle_log_contractExpiration_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp expiration_date;

    @JsonIgnore
    private boolean expiration_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Fleet_vehicle_log_contractMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Fleet_vehicle_log_contractMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Fleet_vehicle_log_contractActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Fleet_vehicle_log_contractMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Fleet_vehicle_log_contractActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [DAYS_LEFT]
     *
     */
    @Fleet_vehicle_log_contractDays_leftDefault(info = "默认规则")
    private Integer days_left;

    @JsonIgnore
    private boolean days_leftDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Fleet_vehicle_log_contractMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Fleet_vehicle_log_contractMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [ODOMETER]
     *
     */
    @Fleet_vehicle_log_contractOdometerDefault(info = "默认规则")
    private Double odometer;

    @JsonIgnore
    private boolean odometerDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Fleet_vehicle_log_contractStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [COST_IDS]
     *
     */
    @Fleet_vehicle_log_contractCost_idsDefault(info = "默认规则")
    private String cost_ids;

    @JsonIgnore
    private boolean cost_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Fleet_vehicle_log_contractMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [COST_GENERATED]
     *
     */
    @Fleet_vehicle_log_contractCost_generatedDefault(info = "默认规则")
    private Double cost_generated;

    @JsonIgnore
    private boolean cost_generatedDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Fleet_vehicle_log_contract__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Fleet_vehicle_log_contractWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Fleet_vehicle_log_contractCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Fleet_vehicle_log_contractMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Fleet_vehicle_log_contractMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Fleet_vehicle_log_contractIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Fleet_vehicle_log_contractWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Fleet_vehicle_log_contractMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Fleet_vehicle_log_contractMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Fleet_vehicle_log_contractMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [GENERATED_COST_IDS]
     *
     */
    @Fleet_vehicle_log_contractGenerated_cost_idsDefault(info = "默认规则")
    private String generated_cost_ids;

    @JsonIgnore
    private boolean generated_cost_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Fleet_vehicle_log_contractNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [COST_FREQUENCY]
     *
     */
    @Fleet_vehicle_log_contractCost_frequencyDefault(info = "默认规则")
    private String cost_frequency;

    @JsonIgnore
    private boolean cost_frequencyDirtyFlag;

    /**
     * 属性 [START_DATE]
     *
     */
    @Fleet_vehicle_log_contractStart_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp start_date;

    @JsonIgnore
    private boolean start_dateDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Fleet_vehicle_log_contractActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Fleet_vehicle_log_contractMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Fleet_vehicle_log_contractActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Fleet_vehicle_log_contractActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [VEHICLE_ID]
     *
     */
    @Fleet_vehicle_log_contractVehicle_idDefault(info = "默认规则")
    private Integer vehicle_id;

    @JsonIgnore
    private boolean vehicle_idDirtyFlag;

    /**
     * 属性 [AUTO_GENERATED]
     *
     */
    @Fleet_vehicle_log_contractAuto_generatedDefault(info = "默认规则")
    private String auto_generated;

    @JsonIgnore
    private boolean auto_generatedDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Fleet_vehicle_log_contractCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ODOMETER_ID]
     *
     */
    @Fleet_vehicle_log_contractOdometer_idDefault(info = "默认规则")
    private Integer odometer_id;

    @JsonIgnore
    private boolean odometer_idDirtyFlag;

    /**
     * 属性 [COST_TYPE]
     *
     */
    @Fleet_vehicle_log_contractCost_typeDefault(info = "默认规则")
    private String cost_type;

    @JsonIgnore
    private boolean cost_typeDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Fleet_vehicle_log_contractParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [PURCHASER_ID_TEXT]
     *
     */
    @Fleet_vehicle_log_contractPurchaser_id_textDefault(info = "默认规则")
    private String purchaser_id_text;

    @JsonIgnore
    private boolean purchaser_id_textDirtyFlag;

    /**
     * 属性 [COST_SUBTYPE_ID]
     *
     */
    @Fleet_vehicle_log_contractCost_subtype_idDefault(info = "默认规则")
    private Integer cost_subtype_id;

    @JsonIgnore
    private boolean cost_subtype_idDirtyFlag;

    /**
     * 属性 [COST_AMOUNT]
     *
     */
    @Fleet_vehicle_log_contractCost_amountDefault(info = "默认规则")
    private Double cost_amount;

    @JsonIgnore
    private boolean cost_amountDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Fleet_vehicle_log_contractAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [INSURER_ID_TEXT]
     *
     */
    @Fleet_vehicle_log_contractInsurer_id_textDefault(info = "默认规则")
    private String insurer_id_text;

    @JsonIgnore
    private boolean insurer_id_textDirtyFlag;

    /**
     * 属性 [COST_ID_TEXT]
     *
     */
    @Fleet_vehicle_log_contractCost_id_textDefault(info = "默认规则")
    private String cost_id_text;

    @JsonIgnore
    private boolean cost_id_textDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Fleet_vehicle_log_contractDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Fleet_vehicle_log_contractUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Fleet_vehicle_log_contractDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [ODOMETER_UNIT]
     *
     */
    @Fleet_vehicle_log_contractOdometer_unitDefault(info = "默认规则")
    private String odometer_unit;

    @JsonIgnore
    private boolean odometer_unitDirtyFlag;

    /**
     * 属性 [CONTRACT_ID]
     *
     */
    @Fleet_vehicle_log_contractContract_idDefault(info = "默认规则")
    private Integer contract_id;

    @JsonIgnore
    private boolean contract_idDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Fleet_vehicle_log_contractWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Fleet_vehicle_log_contractWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COST_ID]
     *
     */
    @Fleet_vehicle_log_contractCost_idDefault(info = "默认规则")
    private Integer cost_id;

    @JsonIgnore
    private boolean cost_idDirtyFlag;

    /**
     * 属性 [INSURER_ID]
     *
     */
    @Fleet_vehicle_log_contractInsurer_idDefault(info = "默认规则")
    private Integer insurer_id;

    @JsonIgnore
    private boolean insurer_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Fleet_vehicle_log_contractUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Fleet_vehicle_log_contractCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PURCHASER_ID]
     *
     */
    @Fleet_vehicle_log_contractPurchaser_idDefault(info = "默认规则")
    private Integer purchaser_id;

    @JsonIgnore
    private boolean purchaser_idDirtyFlag;


    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [INS_REF]
     */
    @JsonProperty("ins_ref")
    public String getIns_ref(){
        return ins_ref ;
    }

    /**
     * 设置 [INS_REF]
     */
    @JsonProperty("ins_ref")
    public void setIns_ref(String  ins_ref){
        this.ins_ref = ins_ref ;
        this.ins_refDirtyFlag = true ;
    }

    /**
     * 获取 [INS_REF]脏标记
     */
    @JsonIgnore
    public boolean getIns_refDirtyFlag(){
        return ins_refDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [SUM_COST]
     */
    @JsonProperty("sum_cost")
    public Double getSum_cost(){
        return sum_cost ;
    }

    /**
     * 设置 [SUM_COST]
     */
    @JsonProperty("sum_cost")
    public void setSum_cost(Double  sum_cost){
        this.sum_cost = sum_cost ;
        this.sum_costDirtyFlag = true ;
    }

    /**
     * 获取 [SUM_COST]脏标记
     */
    @JsonIgnore
    public boolean getSum_costDirtyFlag(){
        return sum_costDirtyFlag ;
    }

    /**
     * 获取 [NOTES]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return notes ;
    }

    /**
     * 设置 [NOTES]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

    /**
     * 获取 [NOTES]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return notesDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [EXPIRATION_DATE]
     */
    @JsonProperty("expiration_date")
    public Timestamp getExpiration_date(){
        return expiration_date ;
    }

    /**
     * 设置 [EXPIRATION_DATE]
     */
    @JsonProperty("expiration_date")
    public void setExpiration_date(Timestamp  expiration_date){
        this.expiration_date = expiration_date ;
        this.expiration_dateDirtyFlag = true ;
    }

    /**
     * 获取 [EXPIRATION_DATE]脏标记
     */
    @JsonIgnore
    public boolean getExpiration_dateDirtyFlag(){
        return expiration_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [DAYS_LEFT]
     */
    @JsonProperty("days_left")
    public Integer getDays_left(){
        return days_left ;
    }

    /**
     * 设置 [DAYS_LEFT]
     */
    @JsonProperty("days_left")
    public void setDays_left(Integer  days_left){
        this.days_left = days_left ;
        this.days_leftDirtyFlag = true ;
    }

    /**
     * 获取 [DAYS_LEFT]脏标记
     */
    @JsonIgnore
    public boolean getDays_leftDirtyFlag(){
        return days_leftDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER]
     */
    @JsonProperty("odometer")
    public Double getOdometer(){
        return odometer ;
    }

    /**
     * 设置 [ODOMETER]
     */
    @JsonProperty("odometer")
    public void setOdometer(Double  odometer){
        this.odometer = odometer ;
        this.odometerDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER]脏标记
     */
    @JsonIgnore
    public boolean getOdometerDirtyFlag(){
        return odometerDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [COST_IDS]
     */
    @JsonProperty("cost_ids")
    public String getCost_ids(){
        return cost_ids ;
    }

    /**
     * 设置 [COST_IDS]
     */
    @JsonProperty("cost_ids")
    public void setCost_ids(String  cost_ids){
        this.cost_ids = cost_ids ;
        this.cost_idsDirtyFlag = true ;
    }

    /**
     * 获取 [COST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCost_idsDirtyFlag(){
        return cost_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [COST_GENERATED]
     */
    @JsonProperty("cost_generated")
    public Double getCost_generated(){
        return cost_generated ;
    }

    /**
     * 设置 [COST_GENERATED]
     */
    @JsonProperty("cost_generated")
    public void setCost_generated(Double  cost_generated){
        this.cost_generated = cost_generated ;
        this.cost_generatedDirtyFlag = true ;
    }

    /**
     * 获取 [COST_GENERATED]脏标记
     */
    @JsonIgnore
    public boolean getCost_generatedDirtyFlag(){
        return cost_generatedDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [GENERATED_COST_IDS]
     */
    @JsonProperty("generated_cost_ids")
    public String getGenerated_cost_ids(){
        return generated_cost_ids ;
    }

    /**
     * 设置 [GENERATED_COST_IDS]
     */
    @JsonProperty("generated_cost_ids")
    public void setGenerated_cost_ids(String  generated_cost_ids){
        this.generated_cost_ids = generated_cost_ids ;
        this.generated_cost_idsDirtyFlag = true ;
    }

    /**
     * 获取 [GENERATED_COST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getGenerated_cost_idsDirtyFlag(){
        return generated_cost_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [COST_FREQUENCY]
     */
    @JsonProperty("cost_frequency")
    public String getCost_frequency(){
        return cost_frequency ;
    }

    /**
     * 设置 [COST_FREQUENCY]
     */
    @JsonProperty("cost_frequency")
    public void setCost_frequency(String  cost_frequency){
        this.cost_frequency = cost_frequency ;
        this.cost_frequencyDirtyFlag = true ;
    }

    /**
     * 获取 [COST_FREQUENCY]脏标记
     */
    @JsonIgnore
    public boolean getCost_frequencyDirtyFlag(){
        return cost_frequencyDirtyFlag ;
    }

    /**
     * 获取 [START_DATE]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return start_date ;
    }

    /**
     * 设置 [START_DATE]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

    /**
     * 获取 [START_DATE]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return start_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [VEHICLE_ID]
     */
    @JsonProperty("vehicle_id")
    public Integer getVehicle_id(){
        return vehicle_id ;
    }

    /**
     * 设置 [VEHICLE_ID]
     */
    @JsonProperty("vehicle_id")
    public void setVehicle_id(Integer  vehicle_id){
        this.vehicle_id = vehicle_id ;
        this.vehicle_idDirtyFlag = true ;
    }

    /**
     * 获取 [VEHICLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getVehicle_idDirtyFlag(){
        return vehicle_idDirtyFlag ;
    }

    /**
     * 获取 [AUTO_GENERATED]
     */
    @JsonProperty("auto_generated")
    public String getAuto_generated(){
        return auto_generated ;
    }

    /**
     * 设置 [AUTO_GENERATED]
     */
    @JsonProperty("auto_generated")
    public void setAuto_generated(String  auto_generated){
        this.auto_generated = auto_generated ;
        this.auto_generatedDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_GENERATED]脏标记
     */
    @JsonIgnore
    public boolean getAuto_generatedDirtyFlag(){
        return auto_generatedDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER_ID]
     */
    @JsonProperty("odometer_id")
    public Integer getOdometer_id(){
        return odometer_id ;
    }

    /**
     * 设置 [ODOMETER_ID]
     */
    @JsonProperty("odometer_id")
    public void setOdometer_id(Integer  odometer_id){
        this.odometer_id = odometer_id ;
        this.odometer_idDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_idDirtyFlag(){
        return odometer_idDirtyFlag ;
    }

    /**
     * 获取 [COST_TYPE]
     */
    @JsonProperty("cost_type")
    public String getCost_type(){
        return cost_type ;
    }

    /**
     * 设置 [COST_TYPE]
     */
    @JsonProperty("cost_type")
    public void setCost_type(String  cost_type){
        this.cost_type = cost_type ;
        this.cost_typeDirtyFlag = true ;
    }

    /**
     * 获取 [COST_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getCost_typeDirtyFlag(){
        return cost_typeDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASER_ID_TEXT]
     */
    @JsonProperty("purchaser_id_text")
    public String getPurchaser_id_text(){
        return purchaser_id_text ;
    }

    /**
     * 设置 [PURCHASER_ID_TEXT]
     */
    @JsonProperty("purchaser_id_text")
    public void setPurchaser_id_text(String  purchaser_id_text){
        this.purchaser_id_text = purchaser_id_text ;
        this.purchaser_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPurchaser_id_textDirtyFlag(){
        return purchaser_id_textDirtyFlag ;
    }

    /**
     * 获取 [COST_SUBTYPE_ID]
     */
    @JsonProperty("cost_subtype_id")
    public Integer getCost_subtype_id(){
        return cost_subtype_id ;
    }

    /**
     * 设置 [COST_SUBTYPE_ID]
     */
    @JsonProperty("cost_subtype_id")
    public void setCost_subtype_id(Integer  cost_subtype_id){
        this.cost_subtype_id = cost_subtype_id ;
        this.cost_subtype_idDirtyFlag = true ;
    }

    /**
     * 获取 [COST_SUBTYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getCost_subtype_idDirtyFlag(){
        return cost_subtype_idDirtyFlag ;
    }

    /**
     * 获取 [COST_AMOUNT]
     */
    @JsonProperty("cost_amount")
    public Double getCost_amount(){
        return cost_amount ;
    }

    /**
     * 设置 [COST_AMOUNT]
     */
    @JsonProperty("cost_amount")
    public void setCost_amount(Double  cost_amount){
        this.cost_amount = cost_amount ;
        this.cost_amountDirtyFlag = true ;
    }

    /**
     * 获取 [COST_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getCost_amountDirtyFlag(){
        return cost_amountDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [INSURER_ID_TEXT]
     */
    @JsonProperty("insurer_id_text")
    public String getInsurer_id_text(){
        return insurer_id_text ;
    }

    /**
     * 设置 [INSURER_ID_TEXT]
     */
    @JsonProperty("insurer_id_text")
    public void setInsurer_id_text(String  insurer_id_text){
        this.insurer_id_text = insurer_id_text ;
        this.insurer_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INSURER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getInsurer_id_textDirtyFlag(){
        return insurer_id_textDirtyFlag ;
    }

    /**
     * 获取 [COST_ID_TEXT]
     */
    @JsonProperty("cost_id_text")
    public String getCost_id_text(){
        return cost_id_text ;
    }

    /**
     * 设置 [COST_ID_TEXT]
     */
    @JsonProperty("cost_id_text")
    public void setCost_id_text(String  cost_id_text){
        this.cost_id_text = cost_id_text ;
        this.cost_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCost_id_textDirtyFlag(){
        return cost_id_textDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER_UNIT]
     */
    @JsonProperty("odometer_unit")
    public String getOdometer_unit(){
        return odometer_unit ;
    }

    /**
     * 设置 [ODOMETER_UNIT]
     */
    @JsonProperty("odometer_unit")
    public void setOdometer_unit(String  odometer_unit){
        this.odometer_unit = odometer_unit ;
        this.odometer_unitDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_unitDirtyFlag(){
        return odometer_unitDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_ID]
     */
    @JsonProperty("contract_id")
    public Integer getContract_id(){
        return contract_id ;
    }

    /**
     * 设置 [CONTRACT_ID]
     */
    @JsonProperty("contract_id")
    public void setContract_id(Integer  contract_id){
        this.contract_id = contract_id ;
        this.contract_idDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_ID]脏标记
     */
    @JsonIgnore
    public boolean getContract_idDirtyFlag(){
        return contract_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COST_ID]
     */
    @JsonProperty("cost_id")
    public Integer getCost_id(){
        return cost_id ;
    }

    /**
     * 设置 [COST_ID]
     */
    @JsonProperty("cost_id")
    public void setCost_id(Integer  cost_id){
        this.cost_id = cost_id ;
        this.cost_idDirtyFlag = true ;
    }

    /**
     * 获取 [COST_ID]脏标记
     */
    @JsonIgnore
    public boolean getCost_idDirtyFlag(){
        return cost_idDirtyFlag ;
    }

    /**
     * 获取 [INSURER_ID]
     */
    @JsonProperty("insurer_id")
    public Integer getInsurer_id(){
        return insurer_id ;
    }

    /**
     * 设置 [INSURER_ID]
     */
    @JsonProperty("insurer_id")
    public void setInsurer_id(Integer  insurer_id){
        this.insurer_id = insurer_id ;
        this.insurer_idDirtyFlag = true ;
    }

    /**
     * 获取 [INSURER_ID]脏标记
     */
    @JsonIgnore
    public boolean getInsurer_idDirtyFlag(){
        return insurer_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PURCHASER_ID]
     */
    @JsonProperty("purchaser_id")
    public Integer getPurchaser_id(){
        return purchaser_id ;
    }

    /**
     * 设置 [PURCHASER_ID]
     */
    @JsonProperty("purchaser_id")
    public void setPurchaser_id(Integer  purchaser_id){
        this.purchaser_id = purchaser_id ;
        this.purchaser_idDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPurchaser_idDirtyFlag(){
        return purchaser_idDirtyFlag ;
    }



    public Fleet_vehicle_log_contract toDO() {
        Fleet_vehicle_log_contract srfdomain = new Fleet_vehicle_log_contract();
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getIns_refDirtyFlag())
            srfdomain.setIns_ref(ins_ref);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getSum_costDirtyFlag())
            srfdomain.setSum_cost(sum_cost);
        if(getNotesDirtyFlag())
            srfdomain.setNotes(notes);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getExpiration_dateDirtyFlag())
            srfdomain.setExpiration_date(expiration_date);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getDays_leftDirtyFlag())
            srfdomain.setDays_left(days_left);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getOdometerDirtyFlag())
            srfdomain.setOdometer(odometer);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getCost_idsDirtyFlag())
            srfdomain.setCost_ids(cost_ids);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getCost_generatedDirtyFlag())
            srfdomain.setCost_generated(cost_generated);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getGenerated_cost_idsDirtyFlag())
            srfdomain.setGenerated_cost_ids(generated_cost_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCost_frequencyDirtyFlag())
            srfdomain.setCost_frequency(cost_frequency);
        if(getStart_dateDirtyFlag())
            srfdomain.setStart_date(start_date);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getVehicle_idDirtyFlag())
            srfdomain.setVehicle_id(vehicle_id);
        if(getAuto_generatedDirtyFlag())
            srfdomain.setAuto_generated(auto_generated);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getOdometer_idDirtyFlag())
            srfdomain.setOdometer_id(odometer_id);
        if(getCost_typeDirtyFlag())
            srfdomain.setCost_type(cost_type);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getPurchaser_id_textDirtyFlag())
            srfdomain.setPurchaser_id_text(purchaser_id_text);
        if(getCost_subtype_idDirtyFlag())
            srfdomain.setCost_subtype_id(cost_subtype_id);
        if(getCost_amountDirtyFlag())
            srfdomain.setCost_amount(cost_amount);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getInsurer_id_textDirtyFlag())
            srfdomain.setInsurer_id_text(insurer_id_text);
        if(getCost_id_textDirtyFlag())
            srfdomain.setCost_id_text(cost_id_text);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getOdometer_unitDirtyFlag())
            srfdomain.setOdometer_unit(odometer_unit);
        if(getContract_idDirtyFlag())
            srfdomain.setContract_id(contract_id);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCost_idDirtyFlag())
            srfdomain.setCost_id(cost_id);
        if(getInsurer_idDirtyFlag())
            srfdomain.setInsurer_id(insurer_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPurchaser_idDirtyFlag())
            srfdomain.setPurchaser_id(purchaser_id);

        return srfdomain;
    }

    public void fromDO(Fleet_vehicle_log_contract srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getIns_refDirtyFlag())
            this.setIns_ref(srfdomain.getIns_ref());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getSum_costDirtyFlag())
            this.setSum_cost(srfdomain.getSum_cost());
        if(srfdomain.getNotesDirtyFlag())
            this.setNotes(srfdomain.getNotes());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getExpiration_dateDirtyFlag())
            this.setExpiration_date(srfdomain.getExpiration_date());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getDays_leftDirtyFlag())
            this.setDays_left(srfdomain.getDays_left());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getOdometerDirtyFlag())
            this.setOdometer(srfdomain.getOdometer());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getCost_idsDirtyFlag())
            this.setCost_ids(srfdomain.getCost_ids());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getCost_generatedDirtyFlag())
            this.setCost_generated(srfdomain.getCost_generated());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getGenerated_cost_idsDirtyFlag())
            this.setGenerated_cost_ids(srfdomain.getGenerated_cost_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCost_frequencyDirtyFlag())
            this.setCost_frequency(srfdomain.getCost_frequency());
        if(srfdomain.getStart_dateDirtyFlag())
            this.setStart_date(srfdomain.getStart_date());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getVehicle_idDirtyFlag())
            this.setVehicle_id(srfdomain.getVehicle_id());
        if(srfdomain.getAuto_generatedDirtyFlag())
            this.setAuto_generated(srfdomain.getAuto_generated());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getOdometer_idDirtyFlag())
            this.setOdometer_id(srfdomain.getOdometer_id());
        if(srfdomain.getCost_typeDirtyFlag())
            this.setCost_type(srfdomain.getCost_type());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getPurchaser_id_textDirtyFlag())
            this.setPurchaser_id_text(srfdomain.getPurchaser_id_text());
        if(srfdomain.getCost_subtype_idDirtyFlag())
            this.setCost_subtype_id(srfdomain.getCost_subtype_id());
        if(srfdomain.getCost_amountDirtyFlag())
            this.setCost_amount(srfdomain.getCost_amount());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getInsurer_id_textDirtyFlag())
            this.setInsurer_id_text(srfdomain.getInsurer_id_text());
        if(srfdomain.getCost_id_textDirtyFlag())
            this.setCost_id_text(srfdomain.getCost_id_text());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getOdometer_unitDirtyFlag())
            this.setOdometer_unit(srfdomain.getOdometer_unit());
        if(srfdomain.getContract_idDirtyFlag())
            this.setContract_id(srfdomain.getContract_id());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCost_idDirtyFlag())
            this.setCost_id(srfdomain.getCost_id());
        if(srfdomain.getInsurer_idDirtyFlag())
            this.setInsurer_id(srfdomain.getInsurer_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPurchaser_idDirtyFlag())
            this.setPurchaser_id(srfdomain.getPurchaser_id());

    }

    public List<Fleet_vehicle_log_contractDTO> fromDOPage(List<Fleet_vehicle_log_contract> poPage)   {
        if(poPage == null)
            return null;
        List<Fleet_vehicle_log_contractDTO> dtos=new ArrayList<Fleet_vehicle_log_contractDTO>();
        for(Fleet_vehicle_log_contract domain : poPage) {
            Fleet_vehicle_log_contractDTO dto = new Fleet_vehicle_log_contractDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

