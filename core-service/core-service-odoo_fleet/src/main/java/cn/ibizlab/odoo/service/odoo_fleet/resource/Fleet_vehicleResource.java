package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicleDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicleService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicleSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle" })
@RestController
@RequestMapping("")
public class Fleet_vehicleResource {

    @Autowired
    private IFleet_vehicleService fleet_vehicleService;

    public IFleet_vehicleService getFleet_vehicleService() {
        return this.fleet_vehicleService;
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicles/{fleet_vehicle_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_id") Integer fleet_vehicle_id) {
        Fleet_vehicleDTO fleet_vehicledto = new Fleet_vehicleDTO();
		Fleet_vehicle domain = new Fleet_vehicle();
		fleet_vehicledto.setId(fleet_vehicle_id);
		domain.setId(fleet_vehicle_id);
        Boolean rst = fleet_vehicleService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicles/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicleDTO> fleet_vehicledtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicles")

    public ResponseEntity<Fleet_vehicleDTO> create(@RequestBody Fleet_vehicleDTO fleet_vehicledto) {
        Fleet_vehicleDTO dto = new Fleet_vehicleDTO();
        Fleet_vehicle domain = fleet_vehicledto.toDO();
		fleet_vehicleService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicles/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle(@RequestBody List<Fleet_vehicleDTO> fleet_vehicledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicles/{fleet_vehicle_id}")

    public ResponseEntity<Fleet_vehicleDTO> update(@PathVariable("fleet_vehicle_id") Integer fleet_vehicle_id, @RequestBody Fleet_vehicleDTO fleet_vehicledto) {
		Fleet_vehicle domain = fleet_vehicledto.toDO();
        domain.setId(fleet_vehicle_id);
		fleet_vehicleService.update(domain);
		Fleet_vehicleDTO dto = new Fleet_vehicleDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicles/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicleDTO> fleet_vehicledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicles/{fleet_vehicle_id}")
    public ResponseEntity<Fleet_vehicleDTO> get(@PathVariable("fleet_vehicle_id") Integer fleet_vehicle_id) {
        Fleet_vehicleDTO dto = new Fleet_vehicleDTO();
        Fleet_vehicle domain = fleet_vehicleService.get(fleet_vehicle_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicles/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicleDTO>> fetchDefault(Fleet_vehicleSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicleDTO> list = new ArrayList<Fleet_vehicleDTO>();
        
        Page<Fleet_vehicle> domains = fleet_vehicleService.searchDefault(context) ;
        for(Fleet_vehicle fleet_vehicle : domains.getContent()){
            Fleet_vehicleDTO dto = new Fleet_vehicleDTO();
            dto.fromDO(fleet_vehicle);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
