package cn.ibizlab.odoo.service.odoo_fleet.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.fleet")
@Data
public class odoo_fleetServiceProperties {

	private boolean enabled;

	private boolean auth;


}