package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_service_typeDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_service_typeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_service_typeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_service_type" })
@RestController
@RequestMapping("")
public class Fleet_service_typeResource {

    @Autowired
    private IFleet_service_typeService fleet_service_typeService;

    public IFleet_service_typeService getFleet_service_typeService() {
        return this.fleet_service_typeService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_service_type" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_service_types/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_service_type(@RequestBody List<Fleet_service_typeDTO> fleet_service_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_service_type" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_service_types/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_service_typeDTO> fleet_service_typedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_service_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_service_types/{fleet_service_type_id}")

    public ResponseEntity<Fleet_service_typeDTO> update(@PathVariable("fleet_service_type_id") Integer fleet_service_type_id, @RequestBody Fleet_service_typeDTO fleet_service_typedto) {
		Fleet_service_type domain = fleet_service_typedto.toDO();
        domain.setId(fleet_service_type_id);
		fleet_service_typeService.update(domain);
		Fleet_service_typeDTO dto = new Fleet_service_typeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_service_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_service_types/{fleet_service_type_id}")
    public ResponseEntity<Fleet_service_typeDTO> get(@PathVariable("fleet_service_type_id") Integer fleet_service_type_id) {
        Fleet_service_typeDTO dto = new Fleet_service_typeDTO();
        Fleet_service_type domain = fleet_service_typeService.get(fleet_service_type_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_service_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_service_types")

    public ResponseEntity<Fleet_service_typeDTO> create(@RequestBody Fleet_service_typeDTO fleet_service_typedto) {
        Fleet_service_typeDTO dto = new Fleet_service_typeDTO();
        Fleet_service_type domain = fleet_service_typedto.toDO();
		fleet_service_typeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_service_type" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_service_types/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_service_typeDTO> fleet_service_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_service_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_service_types/{fleet_service_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_service_type_id") Integer fleet_service_type_id) {
        Fleet_service_typeDTO fleet_service_typedto = new Fleet_service_typeDTO();
		Fleet_service_type domain = new Fleet_service_type();
		fleet_service_typedto.setId(fleet_service_type_id);
		domain.setId(fleet_service_type_id);
        Boolean rst = fleet_service_typeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_service_type" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_service_types/fetchdefault")
	public ResponseEntity<Page<Fleet_service_typeDTO>> fetchDefault(Fleet_service_typeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_service_typeDTO> list = new ArrayList<Fleet_service_typeDTO>();
        
        Page<Fleet_service_type> domains = fleet_service_typeService.searchDefault(context) ;
        for(Fleet_service_type fleet_service_type : domains.getContent()){
            Fleet_service_typeDTO dto = new Fleet_service_typeDTO();
            dto.fromDO(fleet_service_type);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
