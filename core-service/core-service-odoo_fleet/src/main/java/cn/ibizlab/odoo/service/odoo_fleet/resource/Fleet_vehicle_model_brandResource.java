package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicle_model_brandDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model_brand;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_model_brandService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_model_brandSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle_model_brand" })
@RestController
@RequestMapping("")
public class Fleet_vehicle_model_brandResource {

    @Autowired
    private IFleet_vehicle_model_brandService fleet_vehicle_model_brandService;

    public IFleet_vehicle_model_brandService getFleet_vehicle_model_brandService() {
        return this.fleet_vehicle_model_brandService;
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_model_brand" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_model_brands")

    public ResponseEntity<Fleet_vehicle_model_brandDTO> create(@RequestBody Fleet_vehicle_model_brandDTO fleet_vehicle_model_branddto) {
        Fleet_vehicle_model_brandDTO dto = new Fleet_vehicle_model_brandDTO();
        Fleet_vehicle_model_brand domain = fleet_vehicle_model_branddto.toDO();
		fleet_vehicle_model_brandService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle_model_brand" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_model_brands/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicle_model_brandDTO> fleet_vehicle_model_branddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_model_brand" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_model_brands/{fleet_vehicle_model_brand_id}")

    public ResponseEntity<Fleet_vehicle_model_brandDTO> update(@PathVariable("fleet_vehicle_model_brand_id") Integer fleet_vehicle_model_brand_id, @RequestBody Fleet_vehicle_model_brandDTO fleet_vehicle_model_branddto) {
		Fleet_vehicle_model_brand domain = fleet_vehicle_model_branddto.toDO();
        domain.setId(fleet_vehicle_model_brand_id);
		fleet_vehicle_model_brandService.update(domain);
		Fleet_vehicle_model_brandDTO dto = new Fleet_vehicle_model_brandDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle_model_brand" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_model_brands/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle_model_brand(@RequestBody List<Fleet_vehicle_model_brandDTO> fleet_vehicle_model_branddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_model_brand" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_model_brands/{fleet_vehicle_model_brand_id}")
    public ResponseEntity<Fleet_vehicle_model_brandDTO> get(@PathVariable("fleet_vehicle_model_brand_id") Integer fleet_vehicle_model_brand_id) {
        Fleet_vehicle_model_brandDTO dto = new Fleet_vehicle_model_brandDTO();
        Fleet_vehicle_model_brand domain = fleet_vehicle_model_brandService.get(fleet_vehicle_model_brand_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_model_brand" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_model_brands/{fleet_vehicle_model_brand_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_model_brand_id") Integer fleet_vehicle_model_brand_id) {
        Fleet_vehicle_model_brandDTO fleet_vehicle_model_branddto = new Fleet_vehicle_model_brandDTO();
		Fleet_vehicle_model_brand domain = new Fleet_vehicle_model_brand();
		fleet_vehicle_model_branddto.setId(fleet_vehicle_model_brand_id);
		domain.setId(fleet_vehicle_model_brand_id);
        Boolean rst = fleet_vehicle_model_brandService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle_model_brand" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_model_brands/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_model_brandDTO> fleet_vehicle_model_branddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle_model_brand" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicle_model_brands/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicle_model_brandDTO>> fetchDefault(Fleet_vehicle_model_brandSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicle_model_brandDTO> list = new ArrayList<Fleet_vehicle_model_brandDTO>();
        
        Page<Fleet_vehicle_model_brand> domains = fleet_vehicle_model_brandService.searchDefault(context) ;
        for(Fleet_vehicle_model_brand fleet_vehicle_model_brand : domains.getContent()){
            Fleet_vehicle_model_brandDTO dto = new Fleet_vehicle_model_brandDTO();
            dto.fromDO(fleet_vehicle_model_brand);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
