package cn.ibizlab.odoo.service.odoo_im_livechat.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.im.livechat")
@Data
public class odoo_im_livechatServiceProperties {

	private boolean enabled;

	private boolean auth;


}