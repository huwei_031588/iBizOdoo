package cn.ibizlab.odoo.service.odoo_im_livechat.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_im_livechat.dto.Im_livechat_report_channelDTO;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_channel;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_report_channelService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_report_channelSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Im_livechat_report_channel" })
@RestController
@RequestMapping("")
public class Im_livechat_report_channelResource {

    @Autowired
    private IIm_livechat_report_channelService im_livechat_report_channelService;

    public IIm_livechat_report_channelService getIm_livechat_report_channelService() {
        return this.im_livechat_report_channelService;
    }

    @ApiOperation(value = "建立数据", tags = {"Im_livechat_report_channel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_report_channels")

    public ResponseEntity<Im_livechat_report_channelDTO> create(@RequestBody Im_livechat_report_channelDTO im_livechat_report_channeldto) {
        Im_livechat_report_channelDTO dto = new Im_livechat_report_channelDTO();
        Im_livechat_report_channel domain = im_livechat_report_channeldto.toDO();
		im_livechat_report_channelService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Im_livechat_report_channel" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_report_channels/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Im_livechat_report_channelDTO> im_livechat_report_channeldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Im_livechat_report_channel" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_report_channels/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Im_livechat_report_channelDTO> im_livechat_report_channeldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Im_livechat_report_channel" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_report_channels/createBatch")
    public ResponseEntity<Boolean> createBatchIm_livechat_report_channel(@RequestBody List<Im_livechat_report_channelDTO> im_livechat_report_channeldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Im_livechat_report_channel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_report_channels/{im_livechat_report_channel_id}")
    public ResponseEntity<Im_livechat_report_channelDTO> get(@PathVariable("im_livechat_report_channel_id") Integer im_livechat_report_channel_id) {
        Im_livechat_report_channelDTO dto = new Im_livechat_report_channelDTO();
        Im_livechat_report_channel domain = im_livechat_report_channelService.get(im_livechat_report_channel_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Im_livechat_report_channel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_report_channels/{im_livechat_report_channel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("im_livechat_report_channel_id") Integer im_livechat_report_channel_id) {
        Im_livechat_report_channelDTO im_livechat_report_channeldto = new Im_livechat_report_channelDTO();
		Im_livechat_report_channel domain = new Im_livechat_report_channel();
		im_livechat_report_channeldto.setId(im_livechat_report_channel_id);
		domain.setId(im_livechat_report_channel_id);
        Boolean rst = im_livechat_report_channelService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Im_livechat_report_channel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_report_channels/{im_livechat_report_channel_id}")

    public ResponseEntity<Im_livechat_report_channelDTO> update(@PathVariable("im_livechat_report_channel_id") Integer im_livechat_report_channel_id, @RequestBody Im_livechat_report_channelDTO im_livechat_report_channeldto) {
		Im_livechat_report_channel domain = im_livechat_report_channeldto.toDO();
        domain.setId(im_livechat_report_channel_id);
		im_livechat_report_channelService.update(domain);
		Im_livechat_report_channelDTO dto = new Im_livechat_report_channelDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Im_livechat_report_channel" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_im_livechat/im_livechat_report_channels/fetchdefault")
	public ResponseEntity<Page<Im_livechat_report_channelDTO>> fetchDefault(Im_livechat_report_channelSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Im_livechat_report_channelDTO> list = new ArrayList<Im_livechat_report_channelDTO>();
        
        Page<Im_livechat_report_channel> domains = im_livechat_report_channelService.searchDefault(context) ;
        for(Im_livechat_report_channel im_livechat_report_channel : domains.getContent()){
            Im_livechat_report_channelDTO dto = new Im_livechat_report_channelDTO();
            dto.fromDO(im_livechat_report_channel);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
