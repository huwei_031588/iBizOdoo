package cn.ibizlab.odoo.service.odoo_im_livechat.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_im_livechat.dto.Im_livechat_report_operatorDTO;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_report_operatorService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_report_operatorSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Im_livechat_report_operator" })
@RestController
@RequestMapping("")
public class Im_livechat_report_operatorResource {

    @Autowired
    private IIm_livechat_report_operatorService im_livechat_report_operatorService;

    public IIm_livechat_report_operatorService getIm_livechat_report_operatorService() {
        return this.im_livechat_report_operatorService;
    }

    @ApiOperation(value = "建立数据", tags = {"Im_livechat_report_operator" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_report_operators")

    public ResponseEntity<Im_livechat_report_operatorDTO> create(@RequestBody Im_livechat_report_operatorDTO im_livechat_report_operatordto) {
        Im_livechat_report_operatorDTO dto = new Im_livechat_report_operatorDTO();
        Im_livechat_report_operator domain = im_livechat_report_operatordto.toDO();
		im_livechat_report_operatorService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Im_livechat_report_operator" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_report_operators/{im_livechat_report_operator_id}")
    public ResponseEntity<Im_livechat_report_operatorDTO> get(@PathVariable("im_livechat_report_operator_id") Integer im_livechat_report_operator_id) {
        Im_livechat_report_operatorDTO dto = new Im_livechat_report_operatorDTO();
        Im_livechat_report_operator domain = im_livechat_report_operatorService.get(im_livechat_report_operator_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Im_livechat_report_operator" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_report_operators/{im_livechat_report_operator_id}")

    public ResponseEntity<Im_livechat_report_operatorDTO> update(@PathVariable("im_livechat_report_operator_id") Integer im_livechat_report_operator_id, @RequestBody Im_livechat_report_operatorDTO im_livechat_report_operatordto) {
		Im_livechat_report_operator domain = im_livechat_report_operatordto.toDO();
        domain.setId(im_livechat_report_operator_id);
		im_livechat_report_operatorService.update(domain);
		Im_livechat_report_operatorDTO dto = new Im_livechat_report_operatorDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Im_livechat_report_operator" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_report_operators/createBatch")
    public ResponseEntity<Boolean> createBatchIm_livechat_report_operator(@RequestBody List<Im_livechat_report_operatorDTO> im_livechat_report_operatordtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Im_livechat_report_operator" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_report_operators/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Im_livechat_report_operatorDTO> im_livechat_report_operatordtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Im_livechat_report_operator" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_report_operators/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Im_livechat_report_operatorDTO> im_livechat_report_operatordtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Im_livechat_report_operator" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_report_operators/{im_livechat_report_operator_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("im_livechat_report_operator_id") Integer im_livechat_report_operator_id) {
        Im_livechat_report_operatorDTO im_livechat_report_operatordto = new Im_livechat_report_operatorDTO();
		Im_livechat_report_operator domain = new Im_livechat_report_operator();
		im_livechat_report_operatordto.setId(im_livechat_report_operator_id);
		domain.setId(im_livechat_report_operator_id);
        Boolean rst = im_livechat_report_operatorService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Im_livechat_report_operator" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_im_livechat/im_livechat_report_operators/fetchdefault")
	public ResponseEntity<Page<Im_livechat_report_operatorDTO>> fetchDefault(Im_livechat_report_operatorSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Im_livechat_report_operatorDTO> list = new ArrayList<Im_livechat_report_operatorDTO>();
        
        Page<Im_livechat_report_operator> domains = im_livechat_report_operatorService.searchDefault(context) ;
        for(Im_livechat_report_operator im_livechat_report_operator : domains.getContent()){
            Im_livechat_report_operatorDTO dto = new Im_livechat_report_operatorDTO();
            dto.fromDO(im_livechat_report_operator);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
