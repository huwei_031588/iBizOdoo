package cn.ibizlab.odoo.service.odoo_im_livechat.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_im_livechat.valuerule.anno.im_livechat_report_operator.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Im_livechat_report_operatorDTO]
 */
public class Im_livechat_report_operatorDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TIME_TO_ANSWER]
     *
     */
    @Im_livechat_report_operatorTime_to_answerDefault(info = "默认规则")
    private Double time_to_answer;

    @JsonIgnore
    private boolean time_to_answerDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Im_livechat_report_operator__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [START_DATE]
     *
     */
    @Im_livechat_report_operatorStart_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp start_date;

    @JsonIgnore
    private boolean start_dateDirtyFlag;

    /**
     * 属性 [NBR_CHANNEL]
     *
     */
    @Im_livechat_report_operatorNbr_channelDefault(info = "默认规则")
    private Integer nbr_channel;

    @JsonIgnore
    private boolean nbr_channelDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Im_livechat_report_operatorIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DURATION]
     *
     */
    @Im_livechat_report_operatorDurationDefault(info = "默认规则")
    private Double duration;

    @JsonIgnore
    private boolean durationDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Im_livechat_report_operatorDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [LIVECHAT_CHANNEL_ID_TEXT]
     *
     */
    @Im_livechat_report_operatorLivechat_channel_id_textDefault(info = "默认规则")
    private String livechat_channel_id_text;

    @JsonIgnore
    private boolean livechat_channel_id_textDirtyFlag;

    /**
     * 属性 [CHANNEL_ID_TEXT]
     *
     */
    @Im_livechat_report_operatorChannel_id_textDefault(info = "默认规则")
    private String channel_id_text;

    @JsonIgnore
    private boolean channel_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Im_livechat_report_operatorPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CHANNEL_ID]
     *
     */
    @Im_livechat_report_operatorChannel_idDefault(info = "默认规则")
    private Integer channel_id;

    @JsonIgnore
    private boolean channel_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Im_livechat_report_operatorPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [LIVECHAT_CHANNEL_ID]
     *
     */
    @Im_livechat_report_operatorLivechat_channel_idDefault(info = "默认规则")
    private Integer livechat_channel_id;

    @JsonIgnore
    private boolean livechat_channel_idDirtyFlag;


    /**
     * 获取 [TIME_TO_ANSWER]
     */
    @JsonProperty("time_to_answer")
    public Double getTime_to_answer(){
        return time_to_answer ;
    }

    /**
     * 设置 [TIME_TO_ANSWER]
     */
    @JsonProperty("time_to_answer")
    public void setTime_to_answer(Double  time_to_answer){
        this.time_to_answer = time_to_answer ;
        this.time_to_answerDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_TO_ANSWER]脏标记
     */
    @JsonIgnore
    public boolean getTime_to_answerDirtyFlag(){
        return time_to_answerDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [START_DATE]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return start_date ;
    }

    /**
     * 设置 [START_DATE]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

    /**
     * 获取 [START_DATE]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return start_dateDirtyFlag ;
    }

    /**
     * 获取 [NBR_CHANNEL]
     */
    @JsonProperty("nbr_channel")
    public Integer getNbr_channel(){
        return nbr_channel ;
    }

    /**
     * 设置 [NBR_CHANNEL]
     */
    @JsonProperty("nbr_channel")
    public void setNbr_channel(Integer  nbr_channel){
        this.nbr_channel = nbr_channel ;
        this.nbr_channelDirtyFlag = true ;
    }

    /**
     * 获取 [NBR_CHANNEL]脏标记
     */
    @JsonIgnore
    public boolean getNbr_channelDirtyFlag(){
        return nbr_channelDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DURATION]
     */
    @JsonProperty("duration")
    public Double getDuration(){
        return duration ;
    }

    /**
     * 设置 [DURATION]
     */
    @JsonProperty("duration")
    public void setDuration(Double  duration){
        this.duration = duration ;
        this.durationDirtyFlag = true ;
    }

    /**
     * 获取 [DURATION]脏标记
     */
    @JsonIgnore
    public boolean getDurationDirtyFlag(){
        return durationDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [LIVECHAT_CHANNEL_ID_TEXT]
     */
    @JsonProperty("livechat_channel_id_text")
    public String getLivechat_channel_id_text(){
        return livechat_channel_id_text ;
    }

    /**
     * 设置 [LIVECHAT_CHANNEL_ID_TEXT]
     */
    @JsonProperty("livechat_channel_id_text")
    public void setLivechat_channel_id_text(String  livechat_channel_id_text){
        this.livechat_channel_id_text = livechat_channel_id_text ;
        this.livechat_channel_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LIVECHAT_CHANNEL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLivechat_channel_id_textDirtyFlag(){
        return livechat_channel_id_textDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_ID_TEXT]
     */
    @JsonProperty("channel_id_text")
    public String getChannel_id_text(){
        return channel_id_text ;
    }

    /**
     * 设置 [CHANNEL_ID_TEXT]
     */
    @JsonProperty("channel_id_text")
    public void setChannel_id_text(String  channel_id_text){
        this.channel_id_text = channel_id_text ;
        this.channel_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChannel_id_textDirtyFlag(){
        return channel_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_ID]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return channel_id ;
    }

    /**
     * 设置 [CHANNEL_ID]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return channel_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [LIVECHAT_CHANNEL_ID]
     */
    @JsonProperty("livechat_channel_id")
    public Integer getLivechat_channel_id(){
        return livechat_channel_id ;
    }

    /**
     * 设置 [LIVECHAT_CHANNEL_ID]
     */
    @JsonProperty("livechat_channel_id")
    public void setLivechat_channel_id(Integer  livechat_channel_id){
        this.livechat_channel_id = livechat_channel_id ;
        this.livechat_channel_idDirtyFlag = true ;
    }

    /**
     * 获取 [LIVECHAT_CHANNEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getLivechat_channel_idDirtyFlag(){
        return livechat_channel_idDirtyFlag ;
    }



    public Im_livechat_report_operator toDO() {
        Im_livechat_report_operator srfdomain = new Im_livechat_report_operator();
        if(getTime_to_answerDirtyFlag())
            srfdomain.setTime_to_answer(time_to_answer);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getStart_dateDirtyFlag())
            srfdomain.setStart_date(start_date);
        if(getNbr_channelDirtyFlag())
            srfdomain.setNbr_channel(nbr_channel);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDurationDirtyFlag())
            srfdomain.setDuration(duration);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getLivechat_channel_id_textDirtyFlag())
            srfdomain.setLivechat_channel_id_text(livechat_channel_id_text);
        if(getChannel_id_textDirtyFlag())
            srfdomain.setChannel_id_text(channel_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getChannel_idDirtyFlag())
            srfdomain.setChannel_id(channel_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getLivechat_channel_idDirtyFlag())
            srfdomain.setLivechat_channel_id(livechat_channel_id);

        return srfdomain;
    }

    public void fromDO(Im_livechat_report_operator srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getTime_to_answerDirtyFlag())
            this.setTime_to_answer(srfdomain.getTime_to_answer());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getStart_dateDirtyFlag())
            this.setStart_date(srfdomain.getStart_date());
        if(srfdomain.getNbr_channelDirtyFlag())
            this.setNbr_channel(srfdomain.getNbr_channel());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDurationDirtyFlag())
            this.setDuration(srfdomain.getDuration());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getLivechat_channel_id_textDirtyFlag())
            this.setLivechat_channel_id_text(srfdomain.getLivechat_channel_id_text());
        if(srfdomain.getChannel_id_textDirtyFlag())
            this.setChannel_id_text(srfdomain.getChannel_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getChannel_idDirtyFlag())
            this.setChannel_id(srfdomain.getChannel_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getLivechat_channel_idDirtyFlag())
            this.setLivechat_channel_id(srfdomain.getLivechat_channel_id());

    }

    public List<Im_livechat_report_operatorDTO> fromDOPage(List<Im_livechat_report_operator> poPage)   {
        if(poPage == null)
            return null;
        List<Im_livechat_report_operatorDTO> dtos=new ArrayList<Im_livechat_report_operatorDTO>();
        for(Im_livechat_report_operator domain : poPage) {
            Im_livechat_report_operatorDTO dto = new Im_livechat_report_operatorDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

