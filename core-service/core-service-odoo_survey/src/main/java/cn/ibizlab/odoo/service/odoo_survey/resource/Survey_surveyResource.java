package cn.ibizlab.odoo.service.odoo_survey.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_survey.dto.Survey_surveyDTO;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_surveyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_surveySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Survey_survey" })
@RestController
@RequestMapping("")
public class Survey_surveyResource {

    @Autowired
    private ISurvey_surveyService survey_surveyService;

    public ISurvey_surveyService getSurvey_surveyService() {
        return this.survey_surveyService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Survey_survey" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_surveys/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Survey_surveyDTO> survey_surveydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Survey_survey" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_surveys/{survey_survey_id}")
    public ResponseEntity<Survey_surveyDTO> get(@PathVariable("survey_survey_id") Integer survey_survey_id) {
        Survey_surveyDTO dto = new Survey_surveyDTO();
        Survey_survey domain = survey_surveyService.get(survey_survey_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Survey_survey" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_surveys")

    public ResponseEntity<Survey_surveyDTO> create(@RequestBody Survey_surveyDTO survey_surveydto) {
        Survey_surveyDTO dto = new Survey_surveyDTO();
        Survey_survey domain = survey_surveydto.toDO();
		survey_surveyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Survey_survey" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_surveys/{survey_survey_id}")

    public ResponseEntity<Survey_surveyDTO> update(@PathVariable("survey_survey_id") Integer survey_survey_id, @RequestBody Survey_surveyDTO survey_surveydto) {
		Survey_survey domain = survey_surveydto.toDO();
        domain.setId(survey_survey_id);
		survey_surveyService.update(domain);
		Survey_surveyDTO dto = new Survey_surveyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Survey_survey" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_surveys/{survey_survey_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_survey_id") Integer survey_survey_id) {
        Survey_surveyDTO survey_surveydto = new Survey_surveyDTO();
		Survey_survey domain = new Survey_survey();
		survey_surveydto.setId(survey_survey_id);
		domain.setId(survey_survey_id);
        Boolean rst = survey_surveyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Survey_survey" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_surveys/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_surveyDTO> survey_surveydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Survey_survey" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_surveys/createBatch")
    public ResponseEntity<Boolean> createBatchSurvey_survey(@RequestBody List<Survey_surveyDTO> survey_surveydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Survey_survey" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_survey/survey_surveys/fetchdefault")
	public ResponseEntity<Page<Survey_surveyDTO>> fetchDefault(Survey_surveySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Survey_surveyDTO> list = new ArrayList<Survey_surveyDTO>();
        
        Page<Survey_survey> domains = survey_surveyService.searchDefault(context) ;
        for(Survey_survey survey_survey : domains.getContent()){
            Survey_surveyDTO dto = new Survey_surveyDTO();
            dto.fromDO(survey_survey);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
