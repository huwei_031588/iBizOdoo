package cn.ibizlab.odoo.service.odoo_survey.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_survey.valuerule.anno.survey_survey.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_survey;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Survey_surveyDTO]
 */
public class Survey_surveyDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Survey_surveyMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Survey_surveyCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Survey_surveyDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Survey_surveyWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [USERS_CAN_GO_BACK]
     *
     */
    @Survey_surveyUsers_can_go_backDefault(info = "默认规则")
    private String users_can_go_back;

    @JsonIgnore
    private boolean users_can_go_backDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Survey_surveyMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Survey_surveyMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Survey_surveyMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Survey_surveyMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Survey_surveyIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [TOT_START_SURVEY]
     *
     */
    @Survey_surveyTot_start_surveyDefault(info = "默认规则")
    private Integer tot_start_survey;

    @JsonIgnore
    private boolean tot_start_surveyDirtyFlag;

    /**
     * 属性 [RESULT_URL]
     *
     */
    @Survey_surveyResult_urlDefault(info = "默认规则")
    private String result_url;

    @JsonIgnore
    private boolean result_urlDirtyFlag;

    /**
     * 属性 [PRINT_URL]
     *
     */
    @Survey_surveyPrint_urlDefault(info = "默认规则")
    private String print_url;

    @JsonIgnore
    private boolean print_urlDirtyFlag;

    /**
     * 属性 [QUIZZ_MODE]
     *
     */
    @Survey_surveyQuizz_modeDefault(info = "默认规则")
    private String quizz_mode;

    @JsonIgnore
    private boolean quizz_modeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Survey_survey__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [TOT_COMP_SURVEY]
     *
     */
    @Survey_surveyTot_comp_surveyDefault(info = "默认规则")
    private Integer tot_comp_survey;

    @JsonIgnore
    private boolean tot_comp_surveyDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Survey_surveyWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Survey_surveyMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Survey_surveyActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [PUBLIC_URL_HTML]
     *
     */
    @Survey_surveyPublic_url_htmlDefault(info = "默认规则")
    private String public_url_html;

    @JsonIgnore
    private boolean public_url_htmlDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Survey_surveyActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Survey_surveyMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Survey_surveyMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Survey_surveyMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [USER_INPUT_IDS]
     *
     */
    @Survey_surveyUser_input_idsDefault(info = "默认规则")
    private String user_input_ids;

    @JsonIgnore
    private boolean user_input_idsDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Survey_surveyColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [PAGE_IDS]
     *
     */
    @Survey_surveyPage_idsDefault(info = "默认规则")
    private String page_ids;

    @JsonIgnore
    private boolean page_idsDirtyFlag;

    /**
     * 属性 [AUTH_REQUIRED]
     *
     */
    @Survey_surveyAuth_requiredDefault(info = "默认规则")
    private String auth_required;

    @JsonIgnore
    private boolean auth_requiredDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Survey_surveyDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Survey_surveyMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [TOT_SENT_SURVEY]
     *
     */
    @Survey_surveyTot_sent_surveyDefault(info = "默认规则")
    private Integer tot_sent_survey;

    @JsonIgnore
    private boolean tot_sent_surveyDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Survey_surveyActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Survey_surveyActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Survey_surveyMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [DESIGNED]
     *
     */
    @Survey_surveyDesignedDefault(info = "默认规则")
    private String designed;

    @JsonIgnore
    private boolean designedDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Survey_surveyActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Survey_surveyMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [TITLE]
     *
     */
    @Survey_surveyTitleDefault(info = "默认规则")
    private String title;

    @JsonIgnore
    private boolean titleDirtyFlag;

    /**
     * 属性 [THANK_YOU_MESSAGE]
     *
     */
    @Survey_surveyThank_you_messageDefault(info = "默认规则")
    private String thank_you_message;

    @JsonIgnore
    private boolean thank_you_messageDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Survey_surveyActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [PUBLIC_URL]
     *
     */
    @Survey_surveyPublic_urlDefault(info = "默认规则")
    private String public_url;

    @JsonIgnore
    private boolean public_urlDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Survey_surveyActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Survey_surveyMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Survey_surveyWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [IS_CLOSED]
     *
     */
    @Survey_surveyIs_closedDefault(info = "默认规则")
    private String is_closed;

    @JsonIgnore
    private boolean is_closedDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Survey_surveyCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @Survey_surveyStage_id_textDefault(info = "默认规则")
    private String stage_id_text;

    @JsonIgnore
    private boolean stage_id_textDirtyFlag;

    /**
     * 属性 [EMAIL_TEMPLATE_ID_TEXT]
     *
     */
    @Survey_surveyEmail_template_id_textDefault(info = "默认规则")
    private String email_template_id_text;

    @JsonIgnore
    private boolean email_template_id_textDirtyFlag;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @Survey_surveyStage_idDefault(info = "默认规则")
    private Integer stage_id;

    @JsonIgnore
    private boolean stage_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Survey_surveyCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [EMAIL_TEMPLATE_ID]
     *
     */
    @Survey_surveyEmail_template_idDefault(info = "默认规则")
    private Integer email_template_id;

    @JsonIgnore
    private boolean email_template_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Survey_surveyWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [USERS_CAN_GO_BACK]
     */
    @JsonProperty("users_can_go_back")
    public String getUsers_can_go_back(){
        return users_can_go_back ;
    }

    /**
     * 设置 [USERS_CAN_GO_BACK]
     */
    @JsonProperty("users_can_go_back")
    public void setUsers_can_go_back(String  users_can_go_back){
        this.users_can_go_back = users_can_go_back ;
        this.users_can_go_backDirtyFlag = true ;
    }

    /**
     * 获取 [USERS_CAN_GO_BACK]脏标记
     */
    @JsonIgnore
    public boolean getUsers_can_go_backDirtyFlag(){
        return users_can_go_backDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [TOT_START_SURVEY]
     */
    @JsonProperty("tot_start_survey")
    public Integer getTot_start_survey(){
        return tot_start_survey ;
    }

    /**
     * 设置 [TOT_START_SURVEY]
     */
    @JsonProperty("tot_start_survey")
    public void setTot_start_survey(Integer  tot_start_survey){
        this.tot_start_survey = tot_start_survey ;
        this.tot_start_surveyDirtyFlag = true ;
    }

    /**
     * 获取 [TOT_START_SURVEY]脏标记
     */
    @JsonIgnore
    public boolean getTot_start_surveyDirtyFlag(){
        return tot_start_surveyDirtyFlag ;
    }

    /**
     * 获取 [RESULT_URL]
     */
    @JsonProperty("result_url")
    public String getResult_url(){
        return result_url ;
    }

    /**
     * 设置 [RESULT_URL]
     */
    @JsonProperty("result_url")
    public void setResult_url(String  result_url){
        this.result_url = result_url ;
        this.result_urlDirtyFlag = true ;
    }

    /**
     * 获取 [RESULT_URL]脏标记
     */
    @JsonIgnore
    public boolean getResult_urlDirtyFlag(){
        return result_urlDirtyFlag ;
    }

    /**
     * 获取 [PRINT_URL]
     */
    @JsonProperty("print_url")
    public String getPrint_url(){
        return print_url ;
    }

    /**
     * 设置 [PRINT_URL]
     */
    @JsonProperty("print_url")
    public void setPrint_url(String  print_url){
        this.print_url = print_url ;
        this.print_urlDirtyFlag = true ;
    }

    /**
     * 获取 [PRINT_URL]脏标记
     */
    @JsonIgnore
    public boolean getPrint_urlDirtyFlag(){
        return print_urlDirtyFlag ;
    }

    /**
     * 获取 [QUIZZ_MODE]
     */
    @JsonProperty("quizz_mode")
    public String getQuizz_mode(){
        return quizz_mode ;
    }

    /**
     * 设置 [QUIZZ_MODE]
     */
    @JsonProperty("quizz_mode")
    public void setQuizz_mode(String  quizz_mode){
        this.quizz_mode = quizz_mode ;
        this.quizz_modeDirtyFlag = true ;
    }

    /**
     * 获取 [QUIZZ_MODE]脏标记
     */
    @JsonIgnore
    public boolean getQuizz_modeDirtyFlag(){
        return quizz_modeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [TOT_COMP_SURVEY]
     */
    @JsonProperty("tot_comp_survey")
    public Integer getTot_comp_survey(){
        return tot_comp_survey ;
    }

    /**
     * 设置 [TOT_COMP_SURVEY]
     */
    @JsonProperty("tot_comp_survey")
    public void setTot_comp_survey(Integer  tot_comp_survey){
        this.tot_comp_survey = tot_comp_survey ;
        this.tot_comp_surveyDirtyFlag = true ;
    }

    /**
     * 获取 [TOT_COMP_SURVEY]脏标记
     */
    @JsonIgnore
    public boolean getTot_comp_surveyDirtyFlag(){
        return tot_comp_surveyDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [PUBLIC_URL_HTML]
     */
    @JsonProperty("public_url_html")
    public String getPublic_url_html(){
        return public_url_html ;
    }

    /**
     * 设置 [PUBLIC_URL_HTML]
     */
    @JsonProperty("public_url_html")
    public void setPublic_url_html(String  public_url_html){
        this.public_url_html = public_url_html ;
        this.public_url_htmlDirtyFlag = true ;
    }

    /**
     * 获取 [PUBLIC_URL_HTML]脏标记
     */
    @JsonIgnore
    public boolean getPublic_url_htmlDirtyFlag(){
        return public_url_htmlDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [USER_INPUT_IDS]
     */
    @JsonProperty("user_input_ids")
    public String getUser_input_ids(){
        return user_input_ids ;
    }

    /**
     * 设置 [USER_INPUT_IDS]
     */
    @JsonProperty("user_input_ids")
    public void setUser_input_ids(String  user_input_ids){
        this.user_input_ids = user_input_ids ;
        this.user_input_idsDirtyFlag = true ;
    }

    /**
     * 获取 [USER_INPUT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getUser_input_idsDirtyFlag(){
        return user_input_idsDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [PAGE_IDS]
     */
    @JsonProperty("page_ids")
    public String getPage_ids(){
        return page_ids ;
    }

    /**
     * 设置 [PAGE_IDS]
     */
    @JsonProperty("page_ids")
    public void setPage_ids(String  page_ids){
        this.page_ids = page_ids ;
        this.page_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPage_idsDirtyFlag(){
        return page_idsDirtyFlag ;
    }

    /**
     * 获取 [AUTH_REQUIRED]
     */
    @JsonProperty("auth_required")
    public String getAuth_required(){
        return auth_required ;
    }

    /**
     * 设置 [AUTH_REQUIRED]
     */
    @JsonProperty("auth_required")
    public void setAuth_required(String  auth_required){
        this.auth_required = auth_required ;
        this.auth_requiredDirtyFlag = true ;
    }

    /**
     * 获取 [AUTH_REQUIRED]脏标记
     */
    @JsonIgnore
    public boolean getAuth_requiredDirtyFlag(){
        return auth_requiredDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [TOT_SENT_SURVEY]
     */
    @JsonProperty("tot_sent_survey")
    public Integer getTot_sent_survey(){
        return tot_sent_survey ;
    }

    /**
     * 设置 [TOT_SENT_SURVEY]
     */
    @JsonProperty("tot_sent_survey")
    public void setTot_sent_survey(Integer  tot_sent_survey){
        this.tot_sent_survey = tot_sent_survey ;
        this.tot_sent_surveyDirtyFlag = true ;
    }

    /**
     * 获取 [TOT_SENT_SURVEY]脏标记
     */
    @JsonIgnore
    public boolean getTot_sent_surveyDirtyFlag(){
        return tot_sent_surveyDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [DESIGNED]
     */
    @JsonProperty("designed")
    public String getDesigned(){
        return designed ;
    }

    /**
     * 设置 [DESIGNED]
     */
    @JsonProperty("designed")
    public void setDesigned(String  designed){
        this.designed = designed ;
        this.designedDirtyFlag = true ;
    }

    /**
     * 获取 [DESIGNED]脏标记
     */
    @JsonIgnore
    public boolean getDesignedDirtyFlag(){
        return designedDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [TITLE]
     */
    @JsonProperty("title")
    public String getTitle(){
        return title ;
    }

    /**
     * 设置 [TITLE]
     */
    @JsonProperty("title")
    public void setTitle(String  title){
        this.title = title ;
        this.titleDirtyFlag = true ;
    }

    /**
     * 获取 [TITLE]脏标记
     */
    @JsonIgnore
    public boolean getTitleDirtyFlag(){
        return titleDirtyFlag ;
    }

    /**
     * 获取 [THANK_YOU_MESSAGE]
     */
    @JsonProperty("thank_you_message")
    public String getThank_you_message(){
        return thank_you_message ;
    }

    /**
     * 设置 [THANK_YOU_MESSAGE]
     */
    @JsonProperty("thank_you_message")
    public void setThank_you_message(String  thank_you_message){
        this.thank_you_message = thank_you_message ;
        this.thank_you_messageDirtyFlag = true ;
    }

    /**
     * 获取 [THANK_YOU_MESSAGE]脏标记
     */
    @JsonIgnore
    public boolean getThank_you_messageDirtyFlag(){
        return thank_you_messageDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [PUBLIC_URL]
     */
    @JsonProperty("public_url")
    public String getPublic_url(){
        return public_url ;
    }

    /**
     * 设置 [PUBLIC_URL]
     */
    @JsonProperty("public_url")
    public void setPublic_url(String  public_url){
        this.public_url = public_url ;
        this.public_urlDirtyFlag = true ;
    }

    /**
     * 获取 [PUBLIC_URL]脏标记
     */
    @JsonIgnore
    public boolean getPublic_urlDirtyFlag(){
        return public_urlDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [IS_CLOSED]
     */
    @JsonProperty("is_closed")
    public String getIs_closed(){
        return is_closed ;
    }

    /**
     * 设置 [IS_CLOSED]
     */
    @JsonProperty("is_closed")
    public void setIs_closed(String  is_closed){
        this.is_closed = is_closed ;
        this.is_closedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_CLOSED]脏标记
     */
    @JsonIgnore
    public boolean getIs_closedDirtyFlag(){
        return is_closedDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return stage_id_text ;
    }

    /**
     * 设置 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return stage_id_textDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("email_template_id_text")
    public String getEmail_template_id_text(){
        return email_template_id_text ;
    }

    /**
     * 设置 [EMAIL_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("email_template_id_text")
    public void setEmail_template_id_text(String  email_template_id_text){
        this.email_template_id_text = email_template_id_text ;
        this.email_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEmail_template_id_textDirtyFlag(){
        return email_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return stage_id ;
    }

    /**
     * 设置 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return stage_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_TEMPLATE_ID]
     */
    @JsonProperty("email_template_id")
    public Integer getEmail_template_id(){
        return email_template_id ;
    }

    /**
     * 设置 [EMAIL_TEMPLATE_ID]
     */
    @JsonProperty("email_template_id")
    public void setEmail_template_id(Integer  email_template_id){
        this.email_template_id = email_template_id ;
        this.email_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getEmail_template_idDirtyFlag(){
        return email_template_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Survey_survey toDO() {
        Survey_survey srfdomain = new Survey_survey();
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getUsers_can_go_backDirtyFlag())
            srfdomain.setUsers_can_go_back(users_can_go_back);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getTot_start_surveyDirtyFlag())
            srfdomain.setTot_start_survey(tot_start_survey);
        if(getResult_urlDirtyFlag())
            srfdomain.setResult_url(result_url);
        if(getPrint_urlDirtyFlag())
            srfdomain.setPrint_url(print_url);
        if(getQuizz_modeDirtyFlag())
            srfdomain.setQuizz_mode(quizz_mode);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getTot_comp_surveyDirtyFlag())
            srfdomain.setTot_comp_survey(tot_comp_survey);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getPublic_url_htmlDirtyFlag())
            srfdomain.setPublic_url_html(public_url_html);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getUser_input_idsDirtyFlag())
            srfdomain.setUser_input_ids(user_input_ids);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getPage_idsDirtyFlag())
            srfdomain.setPage_ids(page_ids);
        if(getAuth_requiredDirtyFlag())
            srfdomain.setAuth_required(auth_required);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getTot_sent_surveyDirtyFlag())
            srfdomain.setTot_sent_survey(tot_sent_survey);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getDesignedDirtyFlag())
            srfdomain.setDesigned(designed);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getTitleDirtyFlag())
            srfdomain.setTitle(title);
        if(getThank_you_messageDirtyFlag())
            srfdomain.setThank_you_message(thank_you_message);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getPublic_urlDirtyFlag())
            srfdomain.setPublic_url(public_url);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getIs_closedDirtyFlag())
            srfdomain.setIs_closed(is_closed);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getStage_id_textDirtyFlag())
            srfdomain.setStage_id_text(stage_id_text);
        if(getEmail_template_id_textDirtyFlag())
            srfdomain.setEmail_template_id_text(email_template_id_text);
        if(getStage_idDirtyFlag())
            srfdomain.setStage_id(stage_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getEmail_template_idDirtyFlag())
            srfdomain.setEmail_template_id(email_template_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Survey_survey srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getUsers_can_go_backDirtyFlag())
            this.setUsers_can_go_back(srfdomain.getUsers_can_go_back());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getTot_start_surveyDirtyFlag())
            this.setTot_start_survey(srfdomain.getTot_start_survey());
        if(srfdomain.getResult_urlDirtyFlag())
            this.setResult_url(srfdomain.getResult_url());
        if(srfdomain.getPrint_urlDirtyFlag())
            this.setPrint_url(srfdomain.getPrint_url());
        if(srfdomain.getQuizz_modeDirtyFlag())
            this.setQuizz_mode(srfdomain.getQuizz_mode());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getTot_comp_surveyDirtyFlag())
            this.setTot_comp_survey(srfdomain.getTot_comp_survey());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getPublic_url_htmlDirtyFlag())
            this.setPublic_url_html(srfdomain.getPublic_url_html());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getUser_input_idsDirtyFlag())
            this.setUser_input_ids(srfdomain.getUser_input_ids());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getPage_idsDirtyFlag())
            this.setPage_ids(srfdomain.getPage_ids());
        if(srfdomain.getAuth_requiredDirtyFlag())
            this.setAuth_required(srfdomain.getAuth_required());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getTot_sent_surveyDirtyFlag())
            this.setTot_sent_survey(srfdomain.getTot_sent_survey());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getDesignedDirtyFlag())
            this.setDesigned(srfdomain.getDesigned());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getTitleDirtyFlag())
            this.setTitle(srfdomain.getTitle());
        if(srfdomain.getThank_you_messageDirtyFlag())
            this.setThank_you_message(srfdomain.getThank_you_message());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getPublic_urlDirtyFlag())
            this.setPublic_url(srfdomain.getPublic_url());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getIs_closedDirtyFlag())
            this.setIs_closed(srfdomain.getIs_closed());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getStage_id_textDirtyFlag())
            this.setStage_id_text(srfdomain.getStage_id_text());
        if(srfdomain.getEmail_template_id_textDirtyFlag())
            this.setEmail_template_id_text(srfdomain.getEmail_template_id_text());
        if(srfdomain.getStage_idDirtyFlag())
            this.setStage_id(srfdomain.getStage_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getEmail_template_idDirtyFlag())
            this.setEmail_template_id(srfdomain.getEmail_template_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Survey_surveyDTO> fromDOPage(List<Survey_survey> poPage)   {
        if(poPage == null)
            return null;
        List<Survey_surveyDTO> dtos=new ArrayList<Survey_surveyDTO>();
        for(Survey_survey domain : poPage) {
            Survey_surveyDTO dto = new Survey_surveyDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

