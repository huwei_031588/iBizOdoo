package cn.ibizlab.odoo.service.odoo_survey.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_survey.valuerule.anno.survey_mail_compose_message.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_mail_compose_message;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Survey_mail_compose_messageDTO]
 */
public class Survey_mail_compose_messageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Survey_mail_compose_messageIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [AUTO_DELETE]
     *
     */
    @Survey_mail_compose_messageAuto_deleteDefault(info = "默认规则")
    private String auto_delete;

    @JsonIgnore
    private boolean auto_deleteDirtyFlag;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @Survey_mail_compose_messageRating_idsDefault(info = "默认规则")
    private String rating_ids;

    @JsonIgnore
    private boolean rating_idsDirtyFlag;

    /**
     * 属性 [NEEDACTION_PARTNER_IDS]
     *
     */
    @Survey_mail_compose_messageNeedaction_partner_idsDefault(info = "默认规则")
    private String needaction_partner_ids;

    @JsonIgnore
    private boolean needaction_partner_idsDirtyFlag;

    /**
     * 属性 [RATING_VALUE]
     *
     */
    @Survey_mail_compose_messageRating_valueDefault(info = "默认规则")
    private Double rating_value;

    @JsonIgnore
    private boolean rating_valueDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Survey_mail_compose_messageCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_ID]
     *
     */
    @Survey_mail_compose_messageMessage_idDefault(info = "默认规则")
    private String message_id;

    @JsonIgnore
    private boolean message_idDirtyFlag;

    /**
     * 属性 [MAIL_SERVER_ID]
     *
     */
    @Survey_mail_compose_messageMail_server_idDefault(info = "默认规则")
    private Integer mail_server_id;

    @JsonIgnore
    private boolean mail_server_idDirtyFlag;

    /**
     * 属性 [BODY]
     *
     */
    @Survey_mail_compose_messageBodyDefault(info = "默认规则")
    private String body;

    @JsonIgnore
    private boolean bodyDirtyFlag;

    /**
     * 属性 [STARRED_PARTNER_IDS]
     *
     */
    @Survey_mail_compose_messageStarred_partner_idsDefault(info = "默认规则")
    private String starred_partner_ids;

    @JsonIgnore
    private boolean starred_partner_idsDirtyFlag;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @Survey_mail_compose_messageChannel_idsDefault(info = "默认规则")
    private String channel_ids;

    @JsonIgnore
    private boolean channel_idsDirtyFlag;

    /**
     * 属性 [STARRED]
     *
     */
    @Survey_mail_compose_messageStarredDefault(info = "默认规则")
    private String starred;

    @JsonIgnore
    private boolean starredDirtyFlag;

    /**
     * 属性 [NEED_MODERATION]
     *
     */
    @Survey_mail_compose_messageNeed_moderationDefault(info = "默认规则")
    private String need_moderation;

    @JsonIgnore
    private boolean need_moderationDirtyFlag;

    /**
     * 属性 [TRACKING_VALUE_IDS]
     *
     */
    @Survey_mail_compose_messageTracking_value_idsDefault(info = "默认规则")
    private String tracking_value_ids;

    @JsonIgnore
    private boolean tracking_value_idsDirtyFlag;

    /**
     * 属性 [DATE_DEADLINE]
     *
     */
    @Survey_mail_compose_messageDate_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_deadline;

    @JsonIgnore
    private boolean date_deadlineDirtyFlag;

    /**
     * 属性 [NO_AUTO_THREAD]
     *
     */
    @Survey_mail_compose_messageNo_auto_threadDefault(info = "默认规则")
    private String no_auto_thread;

    @JsonIgnore
    private boolean no_auto_threadDirtyFlag;

    /**
     * 属性 [SUBJECT]
     *
     */
    @Survey_mail_compose_messageSubjectDefault(info = "默认规则")
    private String subject;

    @JsonIgnore
    private boolean subjectDirtyFlag;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @Survey_mail_compose_messageReply_toDefault(info = "默认规则")
    private String reply_to;

    @JsonIgnore
    private boolean reply_toDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Survey_mail_compose_messageDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @Survey_mail_compose_messagePartner_idsDefault(info = "默认规则")
    private String partner_ids;

    @JsonIgnore
    private boolean partner_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Survey_mail_compose_messageDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [HAS_ERROR]
     *
     */
    @Survey_mail_compose_messageHas_errorDefault(info = "默认规则")
    private String has_error;

    @JsonIgnore
    private boolean has_errorDirtyFlag;

    /**
     * 属性 [MULTI_EMAIL]
     *
     */
    @Survey_mail_compose_messageMulti_emailDefault(info = "默认规则")
    private String multi_email;

    @JsonIgnore
    private boolean multi_emailDirtyFlag;

    /**
     * 属性 [USE_ACTIVE_DOMAIN]
     *
     */
    @Survey_mail_compose_messageUse_active_domainDefault(info = "默认规则")
    private String use_active_domain;

    @JsonIgnore
    private boolean use_active_domainDirtyFlag;

    /**
     * 属性 [MODEL]
     *
     */
    @Survey_mail_compose_messageModelDefault(info = "默认规则")
    private String model;

    @JsonIgnore
    private boolean modelDirtyFlag;

    /**
     * 属性 [NEEDACTION]
     *
     */
    @Survey_mail_compose_messageNeedactionDefault(info = "默认规则")
    private String needaction;

    @JsonIgnore
    private boolean needactionDirtyFlag;

    /**
     * 属性 [LAYOUT]
     *
     */
    @Survey_mail_compose_messageLayoutDefault(info = "默认规则")
    private String layout;

    @JsonIgnore
    private boolean layoutDirtyFlag;

    /**
     * 属性 [MASS_MAILING_NAME]
     *
     */
    @Survey_mail_compose_messageMass_mailing_nameDefault(info = "默认规则")
    private String mass_mailing_name;

    @JsonIgnore
    private boolean mass_mailing_nameDirtyFlag;

    /**
     * 属性 [AUTO_DELETE_MESSAGE]
     *
     */
    @Survey_mail_compose_messageAuto_delete_messageDefault(info = "默认规则")
    private String auto_delete_message;

    @JsonIgnore
    private boolean auto_delete_messageDirtyFlag;

    /**
     * 属性 [PUBLIC_URL_HTML]
     *
     */
    @Survey_mail_compose_messagePublic_url_htmlDefault(info = "默认规则")
    private String public_url_html;

    @JsonIgnore
    private boolean public_url_htmlDirtyFlag;

    /**
     * 属性 [RECORD_NAME]
     *
     */
    @Survey_mail_compose_messageRecord_nameDefault(info = "默认规则")
    private String record_name;

    @JsonIgnore
    private boolean record_nameDirtyFlag;

    /**
     * 属性 [PUBLIC_URL]
     *
     */
    @Survey_mail_compose_messagePublic_urlDefault(info = "默认规则")
    private String public_url;

    @JsonIgnore
    private boolean public_urlDirtyFlag;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @Survey_mail_compose_messageChild_idsDefault(info = "默认规则")
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;

    /**
     * 属性 [NOTIFICATION_IDS]
     *
     */
    @Survey_mail_compose_messageNotification_idsDefault(info = "默认规则")
    private String notification_ids;

    @JsonIgnore
    private boolean notification_idsDirtyFlag;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @Survey_mail_compose_messageEmail_fromDefault(info = "默认规则")
    private String email_from;

    @JsonIgnore
    private boolean email_fromDirtyFlag;

    /**
     * 属性 [IS_LOG]
     *
     */
    @Survey_mail_compose_messageIs_logDefault(info = "默认规则")
    private String is_log;

    @JsonIgnore
    private boolean is_logDirtyFlag;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @Survey_mail_compose_messageAttachment_idsDefault(info = "默认规则")
    private String attachment_ids;

    @JsonIgnore
    private boolean attachment_idsDirtyFlag;

    /**
     * 属性 [RES_ID]
     *
     */
    @Survey_mail_compose_messageRes_idDefault(info = "默认规则")
    private Integer res_id;

    @JsonIgnore
    private boolean res_idDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Survey_mail_compose_messageDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [ADD_SIGN]
     *
     */
    @Survey_mail_compose_messageAdd_signDefault(info = "默认规则")
    private String add_sign;

    @JsonIgnore
    private boolean add_signDirtyFlag;

    /**
     * 属性 [MODERATION_STATUS]
     *
     */
    @Survey_mail_compose_messageModeration_statusDefault(info = "默认规则")
    private String moderation_status;

    @JsonIgnore
    private boolean moderation_statusDirtyFlag;

    /**
     * 属性 [MESSAGE_TYPE]
     *
     */
    @Survey_mail_compose_messageMessage_typeDefault(info = "默认规则")
    private String message_type;

    @JsonIgnore
    private boolean message_typeDirtyFlag;

    /**
     * 属性 [IBIZPUBLIC]
     *
     */
    @Survey_mail_compose_messageIbizpublicDefault(info = "默认规则")
    private String ibizpublic;

    @JsonIgnore
    private boolean ibizpublicDirtyFlag;

    /**
     * 属性 [ACTIVE_DOMAIN]
     *
     */
    @Survey_mail_compose_messageActive_domainDefault(info = "默认规则")
    private String active_domain;

    @JsonIgnore
    private boolean active_domainDirtyFlag;

    /**
     * 属性 [NOTIFY]
     *
     */
    @Survey_mail_compose_messageNotifyDefault(info = "默认规则")
    private String notify;

    @JsonIgnore
    private boolean notifyDirtyFlag;

    /**
     * 属性 [COMPOSITION_MODE]
     *
     */
    @Survey_mail_compose_messageComposition_modeDefault(info = "默认规则")
    private String composition_mode;

    @JsonIgnore
    private boolean composition_modeDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Survey_mail_compose_messageWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Survey_mail_compose_message__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Survey_mail_compose_messageWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [MAILING_LIST_IDS]
     *
     */
    @Survey_mail_compose_messageMailing_list_idsDefault(info = "默认规则")
    private String mailing_list_ids;

    @JsonIgnore
    private boolean mailing_list_idsDirtyFlag;

    /**
     * 属性 [AUTHOR_ID_TEXT]
     *
     */
    @Survey_mail_compose_messageAuthor_id_textDefault(info = "默认规则")
    private String author_id_text;

    @JsonIgnore
    private boolean author_id_textDirtyFlag;

    /**
     * 属性 [SUBTYPE_ID_TEXT]
     *
     */
    @Survey_mail_compose_messageSubtype_id_textDefault(info = "默认规则")
    private String subtype_id_text;

    @JsonIgnore
    private boolean subtype_id_textDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @Survey_mail_compose_messageTemplate_id_textDefault(info = "默认规则")
    private String template_id_text;

    @JsonIgnore
    private boolean template_id_textDirtyFlag;

    /**
     * 属性 [AUTHOR_AVATAR]
     *
     */
    @Survey_mail_compose_messageAuthor_avatarDefault(info = "默认规则")
    private byte[] author_avatar;

    @JsonIgnore
    private boolean author_avatarDirtyFlag;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @Survey_mail_compose_messageMail_activity_type_id_textDefault(info = "默认规则")
    private String mail_activity_type_id_text;

    @JsonIgnore
    private boolean mail_activity_type_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Survey_mail_compose_messageWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MODERATOR_ID_TEXT]
     *
     */
    @Survey_mail_compose_messageModerator_id_textDefault(info = "默认规则")
    private String moderator_id_text;

    @JsonIgnore
    private boolean moderator_id_textDirtyFlag;

    /**
     * 属性 [MASS_MAILING_ID_TEXT]
     *
     */
    @Survey_mail_compose_messageMass_mailing_id_textDefault(info = "默认规则")
    private String mass_mailing_id_text;

    @JsonIgnore
    private boolean mass_mailing_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Survey_mail_compose_messageCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     *
     */
    @Survey_mail_compose_messageMass_mailing_campaign_id_textDefault(info = "默认规则")
    private String mass_mailing_campaign_id_text;

    @JsonIgnore
    private boolean mass_mailing_campaign_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Survey_mail_compose_messageCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [SURVEY_ID]
     *
     */
    @Survey_mail_compose_messageSurvey_idDefault(info = "默认规则")
    private Integer survey_id;

    @JsonIgnore
    private boolean survey_idDirtyFlag;

    /**
     * 属性 [AUTHOR_ID]
     *
     */
    @Survey_mail_compose_messageAuthor_idDefault(info = "默认规则")
    private Integer author_id;

    @JsonIgnore
    private boolean author_idDirtyFlag;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID]
     *
     */
    @Survey_mail_compose_messageMass_mailing_campaign_idDefault(info = "默认规则")
    private Integer mass_mailing_campaign_id;

    @JsonIgnore
    private boolean mass_mailing_campaign_idDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Survey_mail_compose_messageParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [SUBTYPE_ID]
     *
     */
    @Survey_mail_compose_messageSubtype_idDefault(info = "默认规则")
    private Integer subtype_id;

    @JsonIgnore
    private boolean subtype_idDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @Survey_mail_compose_messageTemplate_idDefault(info = "默认规则")
    private Integer template_id;

    @JsonIgnore
    private boolean template_idDirtyFlag;

    /**
     * 属性 [MASS_MAILING_ID]
     *
     */
    @Survey_mail_compose_messageMass_mailing_idDefault(info = "默认规则")
    private Integer mass_mailing_id;

    @JsonIgnore
    private boolean mass_mailing_idDirtyFlag;

    /**
     * 属性 [MODERATOR_ID]
     *
     */
    @Survey_mail_compose_messageModerator_idDefault(info = "默认规则")
    private Integer moderator_id;

    @JsonIgnore
    private boolean moderator_idDirtyFlag;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID]
     *
     */
    @Survey_mail_compose_messageMail_activity_type_idDefault(info = "默认规则")
    private Integer mail_activity_type_id;

    @JsonIgnore
    private boolean mail_activity_type_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Survey_mail_compose_messageWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [AUTO_DELETE]
     */
    @JsonProperty("auto_delete")
    public String getAuto_delete(){
        return auto_delete ;
    }

    /**
     * 设置 [AUTO_DELETE]
     */
    @JsonProperty("auto_delete")
    public void setAuto_delete(String  auto_delete){
        this.auto_delete = auto_delete ;
        this.auto_deleteDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_DELETE]脏标记
     */
    @JsonIgnore
    public boolean getAuto_deleteDirtyFlag(){
        return auto_deleteDirtyFlag ;
    }

    /**
     * 获取 [RATING_IDS]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return rating_ids ;
    }

    /**
     * 设置 [RATING_IDS]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return rating_idsDirtyFlag ;
    }

    /**
     * 获取 [NEEDACTION_PARTNER_IDS]
     */
    @JsonProperty("needaction_partner_ids")
    public String getNeedaction_partner_ids(){
        return needaction_partner_ids ;
    }

    /**
     * 设置 [NEEDACTION_PARTNER_IDS]
     */
    @JsonProperty("needaction_partner_ids")
    public void setNeedaction_partner_ids(String  needaction_partner_ids){
        this.needaction_partner_ids = needaction_partner_ids ;
        this.needaction_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [NEEDACTION_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getNeedaction_partner_idsDirtyFlag(){
        return needaction_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [RATING_VALUE]
     */
    @JsonProperty("rating_value")
    public Double getRating_value(){
        return rating_value ;
    }

    /**
     * 设置 [RATING_VALUE]
     */
    @JsonProperty("rating_value")
    public void setRating_value(Double  rating_value){
        this.rating_value = rating_value ;
        this.rating_valueDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getRating_valueDirtyFlag(){
        return rating_valueDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ID]
     */
    @JsonProperty("message_id")
    public String getMessage_id(){
        return message_id ;
    }

    /**
     * 设置 [MESSAGE_ID]
     */
    @JsonProperty("message_id")
    public void setMessage_id(String  message_id){
        this.message_id = message_id ;
        this.message_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idDirtyFlag(){
        return message_idDirtyFlag ;
    }

    /**
     * 获取 [MAIL_SERVER_ID]
     */
    @JsonProperty("mail_server_id")
    public Integer getMail_server_id(){
        return mail_server_id ;
    }

    /**
     * 设置 [MAIL_SERVER_ID]
     */
    @JsonProperty("mail_server_id")
    public void setMail_server_id(Integer  mail_server_id){
        this.mail_server_id = mail_server_id ;
        this.mail_server_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_SERVER_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_server_idDirtyFlag(){
        return mail_server_idDirtyFlag ;
    }

    /**
     * 获取 [BODY]
     */
    @JsonProperty("body")
    public String getBody(){
        return body ;
    }

    /**
     * 设置 [BODY]
     */
    @JsonProperty("body")
    public void setBody(String  body){
        this.body = body ;
        this.bodyDirtyFlag = true ;
    }

    /**
     * 获取 [BODY]脏标记
     */
    @JsonIgnore
    public boolean getBodyDirtyFlag(){
        return bodyDirtyFlag ;
    }

    /**
     * 获取 [STARRED_PARTNER_IDS]
     */
    @JsonProperty("starred_partner_ids")
    public String getStarred_partner_ids(){
        return starred_partner_ids ;
    }

    /**
     * 设置 [STARRED_PARTNER_IDS]
     */
    @JsonProperty("starred_partner_ids")
    public void setStarred_partner_ids(String  starred_partner_ids){
        this.starred_partner_ids = starred_partner_ids ;
        this.starred_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [STARRED_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getStarred_partner_idsDirtyFlag(){
        return starred_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_IDS]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return channel_ids ;
    }

    /**
     * 设置 [CHANNEL_IDS]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return channel_idsDirtyFlag ;
    }

    /**
     * 获取 [STARRED]
     */
    @JsonProperty("starred")
    public String getStarred(){
        return starred ;
    }

    /**
     * 设置 [STARRED]
     */
    @JsonProperty("starred")
    public void setStarred(String  starred){
        this.starred = starred ;
        this.starredDirtyFlag = true ;
    }

    /**
     * 获取 [STARRED]脏标记
     */
    @JsonIgnore
    public boolean getStarredDirtyFlag(){
        return starredDirtyFlag ;
    }

    /**
     * 获取 [NEED_MODERATION]
     */
    @JsonProperty("need_moderation")
    public String getNeed_moderation(){
        return need_moderation ;
    }

    /**
     * 设置 [NEED_MODERATION]
     */
    @JsonProperty("need_moderation")
    public void setNeed_moderation(String  need_moderation){
        this.need_moderation = need_moderation ;
        this.need_moderationDirtyFlag = true ;
    }

    /**
     * 获取 [NEED_MODERATION]脏标记
     */
    @JsonIgnore
    public boolean getNeed_moderationDirtyFlag(){
        return need_moderationDirtyFlag ;
    }

    /**
     * 获取 [TRACKING_VALUE_IDS]
     */
    @JsonProperty("tracking_value_ids")
    public String getTracking_value_ids(){
        return tracking_value_ids ;
    }

    /**
     * 设置 [TRACKING_VALUE_IDS]
     */
    @JsonProperty("tracking_value_ids")
    public void setTracking_value_ids(String  tracking_value_ids){
        this.tracking_value_ids = tracking_value_ids ;
        this.tracking_value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TRACKING_VALUE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTracking_value_idsDirtyFlag(){
        return tracking_value_idsDirtyFlag ;
    }

    /**
     * 获取 [DATE_DEADLINE]
     */
    @JsonProperty("date_deadline")
    public Timestamp getDate_deadline(){
        return date_deadline ;
    }

    /**
     * 设置 [DATE_DEADLINE]
     */
    @JsonProperty("date_deadline")
    public void setDate_deadline(Timestamp  date_deadline){
        this.date_deadline = date_deadline ;
        this.date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getDate_deadlineDirtyFlag(){
        return date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [NO_AUTO_THREAD]
     */
    @JsonProperty("no_auto_thread")
    public String getNo_auto_thread(){
        return no_auto_thread ;
    }

    /**
     * 设置 [NO_AUTO_THREAD]
     */
    @JsonProperty("no_auto_thread")
    public void setNo_auto_thread(String  no_auto_thread){
        this.no_auto_thread = no_auto_thread ;
        this.no_auto_threadDirtyFlag = true ;
    }

    /**
     * 获取 [NO_AUTO_THREAD]脏标记
     */
    @JsonIgnore
    public boolean getNo_auto_threadDirtyFlag(){
        return no_auto_threadDirtyFlag ;
    }

    /**
     * 获取 [SUBJECT]
     */
    @JsonProperty("subject")
    public String getSubject(){
        return subject ;
    }

    /**
     * 设置 [SUBJECT]
     */
    @JsonProperty("subject")
    public void setSubject(String  subject){
        this.subject = subject ;
        this.subjectDirtyFlag = true ;
    }

    /**
     * 获取 [SUBJECT]脏标记
     */
    @JsonIgnore
    public boolean getSubjectDirtyFlag(){
        return subjectDirtyFlag ;
    }

    /**
     * 获取 [REPLY_TO]
     */
    @JsonProperty("reply_to")
    public String getReply_to(){
        return reply_to ;
    }

    /**
     * 设置 [REPLY_TO]
     */
    @JsonProperty("reply_to")
    public void setReply_to(String  reply_to){
        this.reply_to = reply_to ;
        this.reply_toDirtyFlag = true ;
    }

    /**
     * 获取 [REPLY_TO]脏标记
     */
    @JsonIgnore
    public boolean getReply_toDirtyFlag(){
        return reply_toDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_IDS]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return partner_ids ;
    }

    /**
     * 设置 [PARTNER_IDS]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return partner_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [HAS_ERROR]
     */
    @JsonProperty("has_error")
    public String getHas_error(){
        return has_error ;
    }

    /**
     * 设置 [HAS_ERROR]
     */
    @JsonProperty("has_error")
    public void setHas_error(String  has_error){
        this.has_error = has_error ;
        this.has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getHas_errorDirtyFlag(){
        return has_errorDirtyFlag ;
    }

    /**
     * 获取 [MULTI_EMAIL]
     */
    @JsonProperty("multi_email")
    public String getMulti_email(){
        return multi_email ;
    }

    /**
     * 设置 [MULTI_EMAIL]
     */
    @JsonProperty("multi_email")
    public void setMulti_email(String  multi_email){
        this.multi_email = multi_email ;
        this.multi_emailDirtyFlag = true ;
    }

    /**
     * 获取 [MULTI_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getMulti_emailDirtyFlag(){
        return multi_emailDirtyFlag ;
    }

    /**
     * 获取 [USE_ACTIVE_DOMAIN]
     */
    @JsonProperty("use_active_domain")
    public String getUse_active_domain(){
        return use_active_domain ;
    }

    /**
     * 设置 [USE_ACTIVE_DOMAIN]
     */
    @JsonProperty("use_active_domain")
    public void setUse_active_domain(String  use_active_domain){
        this.use_active_domain = use_active_domain ;
        this.use_active_domainDirtyFlag = true ;
    }

    /**
     * 获取 [USE_ACTIVE_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getUse_active_domainDirtyFlag(){
        return use_active_domainDirtyFlag ;
    }

    /**
     * 获取 [MODEL]
     */
    @JsonProperty("model")
    public String getModel(){
        return model ;
    }

    /**
     * 设置 [MODEL]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return modelDirtyFlag ;
    }

    /**
     * 获取 [NEEDACTION]
     */
    @JsonProperty("needaction")
    public String getNeedaction(){
        return needaction ;
    }

    /**
     * 设置 [NEEDACTION]
     */
    @JsonProperty("needaction")
    public void setNeedaction(String  needaction){
        this.needaction = needaction ;
        this.needactionDirtyFlag = true ;
    }

    /**
     * 获取 [NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getNeedactionDirtyFlag(){
        return needactionDirtyFlag ;
    }

    /**
     * 获取 [LAYOUT]
     */
    @JsonProperty("layout")
    public String getLayout(){
        return layout ;
    }

    /**
     * 设置 [LAYOUT]
     */
    @JsonProperty("layout")
    public void setLayout(String  layout){
        this.layout = layout ;
        this.layoutDirtyFlag = true ;
    }

    /**
     * 获取 [LAYOUT]脏标记
     */
    @JsonIgnore
    public boolean getLayoutDirtyFlag(){
        return layoutDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_NAME]
     */
    @JsonProperty("mass_mailing_name")
    public String getMass_mailing_name(){
        return mass_mailing_name ;
    }

    /**
     * 设置 [MASS_MAILING_NAME]
     */
    @JsonProperty("mass_mailing_name")
    public void setMass_mailing_name(String  mass_mailing_name){
        this.mass_mailing_name = mass_mailing_name ;
        this.mass_mailing_nameDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_NAME]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_nameDirtyFlag(){
        return mass_mailing_nameDirtyFlag ;
    }

    /**
     * 获取 [AUTO_DELETE_MESSAGE]
     */
    @JsonProperty("auto_delete_message")
    public String getAuto_delete_message(){
        return auto_delete_message ;
    }

    /**
     * 设置 [AUTO_DELETE_MESSAGE]
     */
    @JsonProperty("auto_delete_message")
    public void setAuto_delete_message(String  auto_delete_message){
        this.auto_delete_message = auto_delete_message ;
        this.auto_delete_messageDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_DELETE_MESSAGE]脏标记
     */
    @JsonIgnore
    public boolean getAuto_delete_messageDirtyFlag(){
        return auto_delete_messageDirtyFlag ;
    }

    /**
     * 获取 [PUBLIC_URL_HTML]
     */
    @JsonProperty("public_url_html")
    public String getPublic_url_html(){
        return public_url_html ;
    }

    /**
     * 设置 [PUBLIC_URL_HTML]
     */
    @JsonProperty("public_url_html")
    public void setPublic_url_html(String  public_url_html){
        this.public_url_html = public_url_html ;
        this.public_url_htmlDirtyFlag = true ;
    }

    /**
     * 获取 [PUBLIC_URL_HTML]脏标记
     */
    @JsonIgnore
    public boolean getPublic_url_htmlDirtyFlag(){
        return public_url_htmlDirtyFlag ;
    }

    /**
     * 获取 [RECORD_NAME]
     */
    @JsonProperty("record_name")
    public String getRecord_name(){
        return record_name ;
    }

    /**
     * 设置 [RECORD_NAME]
     */
    @JsonProperty("record_name")
    public void setRecord_name(String  record_name){
        this.record_name = record_name ;
        this.record_nameDirtyFlag = true ;
    }

    /**
     * 获取 [RECORD_NAME]脏标记
     */
    @JsonIgnore
    public boolean getRecord_nameDirtyFlag(){
        return record_nameDirtyFlag ;
    }

    /**
     * 获取 [PUBLIC_URL]
     */
    @JsonProperty("public_url")
    public String getPublic_url(){
        return public_url ;
    }

    /**
     * 设置 [PUBLIC_URL]
     */
    @JsonProperty("public_url")
    public void setPublic_url(String  public_url){
        this.public_url = public_url ;
        this.public_urlDirtyFlag = true ;
    }

    /**
     * 获取 [PUBLIC_URL]脏标记
     */
    @JsonIgnore
    public boolean getPublic_urlDirtyFlag(){
        return public_urlDirtyFlag ;
    }

    /**
     * 获取 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return child_ids ;
    }

    /**
     * 设置 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return child_idsDirtyFlag ;
    }

    /**
     * 获取 [NOTIFICATION_IDS]
     */
    @JsonProperty("notification_ids")
    public String getNotification_ids(){
        return notification_ids ;
    }

    /**
     * 设置 [NOTIFICATION_IDS]
     */
    @JsonProperty("notification_ids")
    public void setNotification_ids(String  notification_ids){
        this.notification_ids = notification_ids ;
        this.notification_idsDirtyFlag = true ;
    }

    /**
     * 获取 [NOTIFICATION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getNotification_idsDirtyFlag(){
        return notification_idsDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return email_from ;
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_FROM]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return email_fromDirtyFlag ;
    }

    /**
     * 获取 [IS_LOG]
     */
    @JsonProperty("is_log")
    public String getIs_log(){
        return is_log ;
    }

    /**
     * 设置 [IS_LOG]
     */
    @JsonProperty("is_log")
    public void setIs_log(String  is_log){
        this.is_log = is_log ;
        this.is_logDirtyFlag = true ;
    }

    /**
     * 获取 [IS_LOG]脏标记
     */
    @JsonIgnore
    public boolean getIs_logDirtyFlag(){
        return is_logDirtyFlag ;
    }

    /**
     * 获取 [ATTACHMENT_IDS]
     */
    @JsonProperty("attachment_ids")
    public String getAttachment_ids(){
        return attachment_ids ;
    }

    /**
     * 设置 [ATTACHMENT_IDS]
     */
    @JsonProperty("attachment_ids")
    public void setAttachment_ids(String  attachment_ids){
        this.attachment_ids = attachment_ids ;
        this.attachment_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTACHMENT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_idsDirtyFlag(){
        return attachment_idsDirtyFlag ;
    }

    /**
     * 获取 [RES_ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return res_id ;
    }

    /**
     * 设置 [RES_ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return res_idDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [ADD_SIGN]
     */
    @JsonProperty("add_sign")
    public String getAdd_sign(){
        return add_sign ;
    }

    /**
     * 设置 [ADD_SIGN]
     */
    @JsonProperty("add_sign")
    public void setAdd_sign(String  add_sign){
        this.add_sign = add_sign ;
        this.add_signDirtyFlag = true ;
    }

    /**
     * 获取 [ADD_SIGN]脏标记
     */
    @JsonIgnore
    public boolean getAdd_signDirtyFlag(){
        return add_signDirtyFlag ;
    }

    /**
     * 获取 [MODERATION_STATUS]
     */
    @JsonProperty("moderation_status")
    public String getModeration_status(){
        return moderation_status ;
    }

    /**
     * 设置 [MODERATION_STATUS]
     */
    @JsonProperty("moderation_status")
    public void setModeration_status(String  moderation_status){
        this.moderation_status = moderation_status ;
        this.moderation_statusDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATION_STATUS]脏标记
     */
    @JsonIgnore
    public boolean getModeration_statusDirtyFlag(){
        return moderation_statusDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_TYPE]
     */
    @JsonProperty("message_type")
    public String getMessage_type(){
        return message_type ;
    }

    /**
     * 设置 [MESSAGE_TYPE]
     */
    @JsonProperty("message_type")
    public void setMessage_type(String  message_type){
        this.message_type = message_type ;
        this.message_typeDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getMessage_typeDirtyFlag(){
        return message_typeDirtyFlag ;
    }

    /**
     * 获取 [IBIZPUBLIC]
     */
    @JsonProperty("ibizpublic")
    public String getIbizpublic(){
        return ibizpublic ;
    }

    /**
     * 设置 [IBIZPUBLIC]
     */
    @JsonProperty("ibizpublic")
    public void setIbizpublic(String  ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.ibizpublicDirtyFlag = true ;
    }

    /**
     * 获取 [IBIZPUBLIC]脏标记
     */
    @JsonIgnore
    public boolean getIbizpublicDirtyFlag(){
        return ibizpublicDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE_DOMAIN]
     */
    @JsonProperty("active_domain")
    public String getActive_domain(){
        return active_domain ;
    }

    /**
     * 设置 [ACTIVE_DOMAIN]
     */
    @JsonProperty("active_domain")
    public void setActive_domain(String  active_domain){
        this.active_domain = active_domain ;
        this.active_domainDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getActive_domainDirtyFlag(){
        return active_domainDirtyFlag ;
    }

    /**
     * 获取 [NOTIFY]
     */
    @JsonProperty("notify")
    public String getNotify(){
        return notify ;
    }

    /**
     * 设置 [NOTIFY]
     */
    @JsonProperty("notify")
    public void setNotify(String  notify){
        this.notify = notify ;
        this.notifyDirtyFlag = true ;
    }

    /**
     * 获取 [NOTIFY]脏标记
     */
    @JsonIgnore
    public boolean getNotifyDirtyFlag(){
        return notifyDirtyFlag ;
    }

    /**
     * 获取 [COMPOSITION_MODE]
     */
    @JsonProperty("composition_mode")
    public String getComposition_mode(){
        return composition_mode ;
    }

    /**
     * 设置 [COMPOSITION_MODE]
     */
    @JsonProperty("composition_mode")
    public void setComposition_mode(String  composition_mode){
        this.composition_mode = composition_mode ;
        this.composition_modeDirtyFlag = true ;
    }

    /**
     * 获取 [COMPOSITION_MODE]脏标记
     */
    @JsonIgnore
    public boolean getComposition_modeDirtyFlag(){
        return composition_modeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [MAILING_LIST_IDS]
     */
    @JsonProperty("mailing_list_ids")
    public String getMailing_list_ids(){
        return mailing_list_ids ;
    }

    /**
     * 设置 [MAILING_LIST_IDS]
     */
    @JsonProperty("mailing_list_ids")
    public void setMailing_list_ids(String  mailing_list_ids){
        this.mailing_list_ids = mailing_list_ids ;
        this.mailing_list_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MAILING_LIST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMailing_list_idsDirtyFlag(){
        return mailing_list_idsDirtyFlag ;
    }

    /**
     * 获取 [AUTHOR_ID_TEXT]
     */
    @JsonProperty("author_id_text")
    public String getAuthor_id_text(){
        return author_id_text ;
    }

    /**
     * 设置 [AUTHOR_ID_TEXT]
     */
    @JsonProperty("author_id_text")
    public void setAuthor_id_text(String  author_id_text){
        this.author_id_text = author_id_text ;
        this.author_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [AUTHOR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_id_textDirtyFlag(){
        return author_id_textDirtyFlag ;
    }

    /**
     * 获取 [SUBTYPE_ID_TEXT]
     */
    @JsonProperty("subtype_id_text")
    public String getSubtype_id_text(){
        return subtype_id_text ;
    }

    /**
     * 设置 [SUBTYPE_ID_TEXT]
     */
    @JsonProperty("subtype_id_text")
    public void setSubtype_id_text(String  subtype_id_text){
        this.subtype_id_text = subtype_id_text ;
        this.subtype_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_id_textDirtyFlag(){
        return subtype_id_textDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID_TEXT]
     */
    @JsonProperty("template_id_text")
    public String getTemplate_id_text(){
        return template_id_text ;
    }

    /**
     * 设置 [TEMPLATE_ID_TEXT]
     */
    @JsonProperty("template_id_text")
    public void setTemplate_id_text(String  template_id_text){
        this.template_id_text = template_id_text ;
        this.template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_id_textDirtyFlag(){
        return template_id_textDirtyFlag ;
    }

    /**
     * 获取 [AUTHOR_AVATAR]
     */
    @JsonProperty("author_avatar")
    public byte[] getAuthor_avatar(){
        return author_avatar ;
    }

    /**
     * 设置 [AUTHOR_AVATAR]
     */
    @JsonProperty("author_avatar")
    public void setAuthor_avatar(byte[]  author_avatar){
        this.author_avatar = author_avatar ;
        this.author_avatarDirtyFlag = true ;
    }

    /**
     * 获取 [AUTHOR_AVATAR]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_avatarDirtyFlag(){
        return author_avatarDirtyFlag ;
    }

    /**
     * 获取 [MAIL_ACTIVITY_TYPE_ID_TEXT]
     */
    @JsonProperty("mail_activity_type_id_text")
    public String getMail_activity_type_id_text(){
        return mail_activity_type_id_text ;
    }

    /**
     * 设置 [MAIL_ACTIVITY_TYPE_ID_TEXT]
     */
    @JsonProperty("mail_activity_type_id_text")
    public void setMail_activity_type_id_text(String  mail_activity_type_id_text){
        this.mail_activity_type_id_text = mail_activity_type_id_text ;
        this.mail_activity_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_ACTIVITY_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_id_textDirtyFlag(){
        return mail_activity_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MODERATOR_ID_TEXT]
     */
    @JsonProperty("moderator_id_text")
    public String getModerator_id_text(){
        return moderator_id_text ;
    }

    /**
     * 设置 [MODERATOR_ID_TEXT]
     */
    @JsonProperty("moderator_id_text")
    public void setModerator_id_text(String  moderator_id_text){
        this.moderator_id_text = moderator_id_text ;
        this.moderator_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATOR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getModerator_id_textDirtyFlag(){
        return moderator_id_textDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_ID_TEXT]
     */
    @JsonProperty("mass_mailing_id_text")
    public String getMass_mailing_id_text(){
        return mass_mailing_id_text ;
    }

    /**
     * 设置 [MASS_MAILING_ID_TEXT]
     */
    @JsonProperty("mass_mailing_id_text")
    public void setMass_mailing_id_text(String  mass_mailing_id_text){
        this.mass_mailing_id_text = mass_mailing_id_text ;
        this.mass_mailing_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_id_textDirtyFlag(){
        return mass_mailing_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("mass_mailing_campaign_id_text")
    public String getMass_mailing_campaign_id_text(){
        return mass_mailing_campaign_id_text ;
    }

    /**
     * 设置 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("mass_mailing_campaign_id_text")
    public void setMass_mailing_campaign_id_text(String  mass_mailing_campaign_id_text){
        this.mass_mailing_campaign_id_text = mass_mailing_campaign_id_text ;
        this.mass_mailing_campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_id_textDirtyFlag(){
        return mass_mailing_campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [SURVEY_ID]
     */
    @JsonProperty("survey_id")
    public Integer getSurvey_id(){
        return survey_id ;
    }

    /**
     * 设置 [SURVEY_ID]
     */
    @JsonProperty("survey_id")
    public void setSurvey_id(Integer  survey_id){
        this.survey_id = survey_id ;
        this.survey_idDirtyFlag = true ;
    }

    /**
     * 获取 [SURVEY_ID]脏标记
     */
    @JsonIgnore
    public boolean getSurvey_idDirtyFlag(){
        return survey_idDirtyFlag ;
    }

    /**
     * 获取 [AUTHOR_ID]
     */
    @JsonProperty("author_id")
    public Integer getAuthor_id(){
        return author_id ;
    }

    /**
     * 设置 [AUTHOR_ID]
     */
    @JsonProperty("author_id")
    public void setAuthor_id(Integer  author_id){
        this.author_id = author_id ;
        this.author_idDirtyFlag = true ;
    }

    /**
     * 获取 [AUTHOR_ID]脏标记
     */
    @JsonIgnore
    public boolean getAuthor_idDirtyFlag(){
        return author_idDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public Integer getMass_mailing_campaign_id(){
        return mass_mailing_campaign_id ;
    }

    /**
     * 设置 [MASS_MAILING_CAMPAIGN_ID]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public void setMass_mailing_campaign_id(Integer  mass_mailing_campaign_id){
        this.mass_mailing_campaign_id = mass_mailing_campaign_id ;
        this.mass_mailing_campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_idDirtyFlag(){
        return mass_mailing_campaign_idDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [SUBTYPE_ID]
     */
    @JsonProperty("subtype_id")
    public Integer getSubtype_id(){
        return subtype_id ;
    }

    /**
     * 设置 [SUBTYPE_ID]
     */
    @JsonProperty("subtype_id")
    public void setSubtype_id(Integer  subtype_id){
        this.subtype_id = subtype_id ;
        this.subtype_idDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSubtype_idDirtyFlag(){
        return subtype_idDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return template_id ;
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return template_idDirtyFlag ;
    }

    /**
     * 获取 [MASS_MAILING_ID]
     */
    @JsonProperty("mass_mailing_id")
    public Integer getMass_mailing_id(){
        return mass_mailing_id ;
    }

    /**
     * 设置 [MASS_MAILING_ID]
     */
    @JsonProperty("mass_mailing_id")
    public void setMass_mailing_id(Integer  mass_mailing_id){
        this.mass_mailing_id = mass_mailing_id ;
        this.mass_mailing_idDirtyFlag = true ;
    }

    /**
     * 获取 [MASS_MAILING_ID]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_idDirtyFlag(){
        return mass_mailing_idDirtyFlag ;
    }

    /**
     * 获取 [MODERATOR_ID]
     */
    @JsonProperty("moderator_id")
    public Integer getModerator_id(){
        return moderator_id ;
    }

    /**
     * 设置 [MODERATOR_ID]
     */
    @JsonProperty("moderator_id")
    public void setModerator_id(Integer  moderator_id){
        this.moderator_id = moderator_id ;
        this.moderator_idDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATOR_ID]脏标记
     */
    @JsonIgnore
    public boolean getModerator_idDirtyFlag(){
        return moderator_idDirtyFlag ;
    }

    /**
     * 获取 [MAIL_ACTIVITY_TYPE_ID]
     */
    @JsonProperty("mail_activity_type_id")
    public Integer getMail_activity_type_id(){
        return mail_activity_type_id ;
    }

    /**
     * 设置 [MAIL_ACTIVITY_TYPE_ID]
     */
    @JsonProperty("mail_activity_type_id")
    public void setMail_activity_type_id(Integer  mail_activity_type_id){
        this.mail_activity_type_id = mail_activity_type_id ;
        this.mail_activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_activity_type_idDirtyFlag(){
        return mail_activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Survey_mail_compose_message toDO() {
        Survey_mail_compose_message srfdomain = new Survey_mail_compose_message();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getAuto_deleteDirtyFlag())
            srfdomain.setAuto_delete(auto_delete);
        if(getRating_idsDirtyFlag())
            srfdomain.setRating_ids(rating_ids);
        if(getNeedaction_partner_idsDirtyFlag())
            srfdomain.setNeedaction_partner_ids(needaction_partner_ids);
        if(getRating_valueDirtyFlag())
            srfdomain.setRating_value(rating_value);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMessage_idDirtyFlag())
            srfdomain.setMessage_id(message_id);
        if(getMail_server_idDirtyFlag())
            srfdomain.setMail_server_id(mail_server_id);
        if(getBodyDirtyFlag())
            srfdomain.setBody(body);
        if(getStarred_partner_idsDirtyFlag())
            srfdomain.setStarred_partner_ids(starred_partner_ids);
        if(getChannel_idsDirtyFlag())
            srfdomain.setChannel_ids(channel_ids);
        if(getStarredDirtyFlag())
            srfdomain.setStarred(starred);
        if(getNeed_moderationDirtyFlag())
            srfdomain.setNeed_moderation(need_moderation);
        if(getTracking_value_idsDirtyFlag())
            srfdomain.setTracking_value_ids(tracking_value_ids);
        if(getDate_deadlineDirtyFlag())
            srfdomain.setDate_deadline(date_deadline);
        if(getNo_auto_threadDirtyFlag())
            srfdomain.setNo_auto_thread(no_auto_thread);
        if(getSubjectDirtyFlag())
            srfdomain.setSubject(subject);
        if(getReply_toDirtyFlag())
            srfdomain.setReply_to(reply_to);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getPartner_idsDirtyFlag())
            srfdomain.setPartner_ids(partner_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getHas_errorDirtyFlag())
            srfdomain.setHas_error(has_error);
        if(getMulti_emailDirtyFlag())
            srfdomain.setMulti_email(multi_email);
        if(getUse_active_domainDirtyFlag())
            srfdomain.setUse_active_domain(use_active_domain);
        if(getModelDirtyFlag())
            srfdomain.setModel(model);
        if(getNeedactionDirtyFlag())
            srfdomain.setNeedaction(needaction);
        if(getLayoutDirtyFlag())
            srfdomain.setLayout(layout);
        if(getMass_mailing_nameDirtyFlag())
            srfdomain.setMass_mailing_name(mass_mailing_name);
        if(getAuto_delete_messageDirtyFlag())
            srfdomain.setAuto_delete_message(auto_delete_message);
        if(getPublic_url_htmlDirtyFlag())
            srfdomain.setPublic_url_html(public_url_html);
        if(getRecord_nameDirtyFlag())
            srfdomain.setRecord_name(record_name);
        if(getPublic_urlDirtyFlag())
            srfdomain.setPublic_url(public_url);
        if(getChild_idsDirtyFlag())
            srfdomain.setChild_ids(child_ids);
        if(getNotification_idsDirtyFlag())
            srfdomain.setNotification_ids(notification_ids);
        if(getEmail_fromDirtyFlag())
            srfdomain.setEmail_from(email_from);
        if(getIs_logDirtyFlag())
            srfdomain.setIs_log(is_log);
        if(getAttachment_idsDirtyFlag())
            srfdomain.setAttachment_ids(attachment_ids);
        if(getRes_idDirtyFlag())
            srfdomain.setRes_id(res_id);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getAdd_signDirtyFlag())
            srfdomain.setAdd_sign(add_sign);
        if(getModeration_statusDirtyFlag())
            srfdomain.setModeration_status(moderation_status);
        if(getMessage_typeDirtyFlag())
            srfdomain.setMessage_type(message_type);
        if(getIbizpublicDirtyFlag())
            srfdomain.setIbizpublic(ibizpublic);
        if(getActive_domainDirtyFlag())
            srfdomain.setActive_domain(active_domain);
        if(getNotifyDirtyFlag())
            srfdomain.setNotify(notify);
        if(getComposition_modeDirtyFlag())
            srfdomain.setComposition_mode(composition_mode);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(getMailing_list_idsDirtyFlag())
            srfdomain.setMailing_list_ids(mailing_list_ids);
        if(getAuthor_id_textDirtyFlag())
            srfdomain.setAuthor_id_text(author_id_text);
        if(getSubtype_id_textDirtyFlag())
            srfdomain.setSubtype_id_text(subtype_id_text);
        if(getTemplate_id_textDirtyFlag())
            srfdomain.setTemplate_id_text(template_id_text);
        if(getAuthor_avatarDirtyFlag())
            srfdomain.setAuthor_avatar(author_avatar);
        if(getMail_activity_type_id_textDirtyFlag())
            srfdomain.setMail_activity_type_id_text(mail_activity_type_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getModerator_id_textDirtyFlag())
            srfdomain.setModerator_id_text(moderator_id_text);
        if(getMass_mailing_id_textDirtyFlag())
            srfdomain.setMass_mailing_id_text(mass_mailing_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getMass_mailing_campaign_id_textDirtyFlag())
            srfdomain.setMass_mailing_campaign_id_text(mass_mailing_campaign_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getSurvey_idDirtyFlag())
            srfdomain.setSurvey_id(survey_id);
        if(getAuthor_idDirtyFlag())
            srfdomain.setAuthor_id(author_id);
        if(getMass_mailing_campaign_idDirtyFlag())
            srfdomain.setMass_mailing_campaign_id(mass_mailing_campaign_id);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getSubtype_idDirtyFlag())
            srfdomain.setSubtype_id(subtype_id);
        if(getTemplate_idDirtyFlag())
            srfdomain.setTemplate_id(template_id);
        if(getMass_mailing_idDirtyFlag())
            srfdomain.setMass_mailing_id(mass_mailing_id);
        if(getModerator_idDirtyFlag())
            srfdomain.setModerator_id(moderator_id);
        if(getMail_activity_type_idDirtyFlag())
            srfdomain.setMail_activity_type_id(mail_activity_type_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Survey_mail_compose_message srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getAuto_deleteDirtyFlag())
            this.setAuto_delete(srfdomain.getAuto_delete());
        if(srfdomain.getRating_idsDirtyFlag())
            this.setRating_ids(srfdomain.getRating_ids());
        if(srfdomain.getNeedaction_partner_idsDirtyFlag())
            this.setNeedaction_partner_ids(srfdomain.getNeedaction_partner_ids());
        if(srfdomain.getRating_valueDirtyFlag())
            this.setRating_value(srfdomain.getRating_value());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMessage_idDirtyFlag())
            this.setMessage_id(srfdomain.getMessage_id());
        if(srfdomain.getMail_server_idDirtyFlag())
            this.setMail_server_id(srfdomain.getMail_server_id());
        if(srfdomain.getBodyDirtyFlag())
            this.setBody(srfdomain.getBody());
        if(srfdomain.getStarred_partner_idsDirtyFlag())
            this.setStarred_partner_ids(srfdomain.getStarred_partner_ids());
        if(srfdomain.getChannel_idsDirtyFlag())
            this.setChannel_ids(srfdomain.getChannel_ids());
        if(srfdomain.getStarredDirtyFlag())
            this.setStarred(srfdomain.getStarred());
        if(srfdomain.getNeed_moderationDirtyFlag())
            this.setNeed_moderation(srfdomain.getNeed_moderation());
        if(srfdomain.getTracking_value_idsDirtyFlag())
            this.setTracking_value_ids(srfdomain.getTracking_value_ids());
        if(srfdomain.getDate_deadlineDirtyFlag())
            this.setDate_deadline(srfdomain.getDate_deadline());
        if(srfdomain.getNo_auto_threadDirtyFlag())
            this.setNo_auto_thread(srfdomain.getNo_auto_thread());
        if(srfdomain.getSubjectDirtyFlag())
            this.setSubject(srfdomain.getSubject());
        if(srfdomain.getReply_toDirtyFlag())
            this.setReply_to(srfdomain.getReply_to());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getPartner_idsDirtyFlag())
            this.setPartner_ids(srfdomain.getPartner_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getHas_errorDirtyFlag())
            this.setHas_error(srfdomain.getHas_error());
        if(srfdomain.getMulti_emailDirtyFlag())
            this.setMulti_email(srfdomain.getMulti_email());
        if(srfdomain.getUse_active_domainDirtyFlag())
            this.setUse_active_domain(srfdomain.getUse_active_domain());
        if(srfdomain.getModelDirtyFlag())
            this.setModel(srfdomain.getModel());
        if(srfdomain.getNeedactionDirtyFlag())
            this.setNeedaction(srfdomain.getNeedaction());
        if(srfdomain.getLayoutDirtyFlag())
            this.setLayout(srfdomain.getLayout());
        if(srfdomain.getMass_mailing_nameDirtyFlag())
            this.setMass_mailing_name(srfdomain.getMass_mailing_name());
        if(srfdomain.getAuto_delete_messageDirtyFlag())
            this.setAuto_delete_message(srfdomain.getAuto_delete_message());
        if(srfdomain.getPublic_url_htmlDirtyFlag())
            this.setPublic_url_html(srfdomain.getPublic_url_html());
        if(srfdomain.getRecord_nameDirtyFlag())
            this.setRecord_name(srfdomain.getRecord_name());
        if(srfdomain.getPublic_urlDirtyFlag())
            this.setPublic_url(srfdomain.getPublic_url());
        if(srfdomain.getChild_idsDirtyFlag())
            this.setChild_ids(srfdomain.getChild_ids());
        if(srfdomain.getNotification_idsDirtyFlag())
            this.setNotification_ids(srfdomain.getNotification_ids());
        if(srfdomain.getEmail_fromDirtyFlag())
            this.setEmail_from(srfdomain.getEmail_from());
        if(srfdomain.getIs_logDirtyFlag())
            this.setIs_log(srfdomain.getIs_log());
        if(srfdomain.getAttachment_idsDirtyFlag())
            this.setAttachment_ids(srfdomain.getAttachment_ids());
        if(srfdomain.getRes_idDirtyFlag())
            this.setRes_id(srfdomain.getRes_id());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getAdd_signDirtyFlag())
            this.setAdd_sign(srfdomain.getAdd_sign());
        if(srfdomain.getModeration_statusDirtyFlag())
            this.setModeration_status(srfdomain.getModeration_status());
        if(srfdomain.getMessage_typeDirtyFlag())
            this.setMessage_type(srfdomain.getMessage_type());
        if(srfdomain.getIbizpublicDirtyFlag())
            this.setIbizpublic(srfdomain.getIbizpublic());
        if(srfdomain.getActive_domainDirtyFlag())
            this.setActive_domain(srfdomain.getActive_domain());
        if(srfdomain.getNotifyDirtyFlag())
            this.setNotify(srfdomain.getNotify());
        if(srfdomain.getComposition_modeDirtyFlag())
            this.setComposition_mode(srfdomain.getComposition_mode());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.getMailing_list_idsDirtyFlag())
            this.setMailing_list_ids(srfdomain.getMailing_list_ids());
        if(srfdomain.getAuthor_id_textDirtyFlag())
            this.setAuthor_id_text(srfdomain.getAuthor_id_text());
        if(srfdomain.getSubtype_id_textDirtyFlag())
            this.setSubtype_id_text(srfdomain.getSubtype_id_text());
        if(srfdomain.getTemplate_id_textDirtyFlag())
            this.setTemplate_id_text(srfdomain.getTemplate_id_text());
        if(srfdomain.getAuthor_avatarDirtyFlag())
            this.setAuthor_avatar(srfdomain.getAuthor_avatar());
        if(srfdomain.getMail_activity_type_id_textDirtyFlag())
            this.setMail_activity_type_id_text(srfdomain.getMail_activity_type_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getModerator_id_textDirtyFlag())
            this.setModerator_id_text(srfdomain.getModerator_id_text());
        if(srfdomain.getMass_mailing_id_textDirtyFlag())
            this.setMass_mailing_id_text(srfdomain.getMass_mailing_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getMass_mailing_campaign_id_textDirtyFlag())
            this.setMass_mailing_campaign_id_text(srfdomain.getMass_mailing_campaign_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getSurvey_idDirtyFlag())
            this.setSurvey_id(srfdomain.getSurvey_id());
        if(srfdomain.getAuthor_idDirtyFlag())
            this.setAuthor_id(srfdomain.getAuthor_id());
        if(srfdomain.getMass_mailing_campaign_idDirtyFlag())
            this.setMass_mailing_campaign_id(srfdomain.getMass_mailing_campaign_id());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getSubtype_idDirtyFlag())
            this.setSubtype_id(srfdomain.getSubtype_id());
        if(srfdomain.getTemplate_idDirtyFlag())
            this.setTemplate_id(srfdomain.getTemplate_id());
        if(srfdomain.getMass_mailing_idDirtyFlag())
            this.setMass_mailing_id(srfdomain.getMass_mailing_id());
        if(srfdomain.getModerator_idDirtyFlag())
            this.setModerator_id(srfdomain.getModerator_id());
        if(srfdomain.getMail_activity_type_idDirtyFlag())
            this.setMail_activity_type_id(srfdomain.getMail_activity_type_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Survey_mail_compose_messageDTO> fromDOPage(List<Survey_mail_compose_message> poPage)   {
        if(poPage == null)
            return null;
        List<Survey_mail_compose_messageDTO> dtos=new ArrayList<Survey_mail_compose_messageDTO>();
        for(Survey_mail_compose_message domain : poPage) {
            Survey_mail_compose_messageDTO dto = new Survey_mail_compose_messageDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

