package cn.ibizlab.odoo.service.odoo_survey.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_survey.dto.Survey_stageDTO;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_stageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_stageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Survey_stage" })
@RestController
@RequestMapping("")
public class Survey_stageResource {

    @Autowired
    private ISurvey_stageService survey_stageService;

    public ISurvey_stageService getSurvey_stageService() {
        return this.survey_stageService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Survey_stage" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_stages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Survey_stageDTO> survey_stagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Survey_stage" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_stages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_stageDTO> survey_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Survey_stage" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_stages/createBatch")
    public ResponseEntity<Boolean> createBatchSurvey_stage(@RequestBody List<Survey_stageDTO> survey_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Survey_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_stages/{survey_stage_id}")

    public ResponseEntity<Survey_stageDTO> update(@PathVariable("survey_stage_id") Integer survey_stage_id, @RequestBody Survey_stageDTO survey_stagedto) {
		Survey_stage domain = survey_stagedto.toDO();
        domain.setId(survey_stage_id);
		survey_stageService.update(domain);
		Survey_stageDTO dto = new Survey_stageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Survey_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_stages/{survey_stage_id}")
    public ResponseEntity<Survey_stageDTO> get(@PathVariable("survey_stage_id") Integer survey_stage_id) {
        Survey_stageDTO dto = new Survey_stageDTO();
        Survey_stage domain = survey_stageService.get(survey_stage_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Survey_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_stages")

    public ResponseEntity<Survey_stageDTO> create(@RequestBody Survey_stageDTO survey_stagedto) {
        Survey_stageDTO dto = new Survey_stageDTO();
        Survey_stage domain = survey_stagedto.toDO();
		survey_stageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Survey_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_stages/{survey_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_stage_id") Integer survey_stage_id) {
        Survey_stageDTO survey_stagedto = new Survey_stageDTO();
		Survey_stage domain = new Survey_stage();
		survey_stagedto.setId(survey_stage_id);
		domain.setId(survey_stage_id);
        Boolean rst = survey_stageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Survey_stage" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_survey/survey_stages/fetchdefault")
	public ResponseEntity<Page<Survey_stageDTO>> fetchDefault(Survey_stageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Survey_stageDTO> list = new ArrayList<Survey_stageDTO>();
        
        Page<Survey_stage> domains = survey_stageService.searchDefault(context) ;
        for(Survey_stage survey_stage : domains.getContent()){
            Survey_stageDTO dto = new Survey_stageDTO();
            dto.fromDO(survey_stage);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
