package cn.ibizlab.odoo.service.odoo_survey.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_survey.valuerule.anno.survey_user_input.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Survey_user_inputDTO]
 */
public class Survey_user_inputDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [USER_INPUT_LINE_IDS]
     *
     */
    @Survey_user_inputUser_input_line_idsDefault(info = "默认规则")
    private String user_input_line_ids;

    @JsonIgnore
    private boolean user_input_line_idsDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Survey_user_inputStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Survey_user_inputIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [QUIZZ_SCORE]
     *
     */
    @Survey_user_inputQuizz_scoreDefault(info = "默认规则")
    private Double quizz_score;

    @JsonIgnore
    private boolean quizz_scoreDirtyFlag;

    /**
     * 属性 [TOKEN]
     *
     */
    @Survey_user_inputTokenDefault(info = "默认规则")
    private String token;

    @JsonIgnore
    private boolean tokenDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Survey_user_inputWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Survey_user_inputCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Survey_user_inputDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [EMAIL]
     *
     */
    @Survey_user_inputEmailDefault(info = "默认规则")
    private String email;

    @JsonIgnore
    private boolean emailDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Survey_user_input__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [TEST_ENTRY]
     *
     */
    @Survey_user_inputTest_entryDefault(info = "默认规则")
    private String test_entry;

    @JsonIgnore
    private boolean test_entryDirtyFlag;

    /**
     * 属性 [DEADLINE]
     *
     */
    @Survey_user_inputDeadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp deadline;

    @JsonIgnore
    private boolean deadlineDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Survey_user_inputTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [DATE_CREATE]
     *
     */
    @Survey_user_inputDate_createDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_create;

    @JsonIgnore
    private boolean date_createDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Survey_user_inputWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Survey_user_inputCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PRINT_URL]
     *
     */
    @Survey_user_inputPrint_urlDefault(info = "默认规则")
    private String print_url;

    @JsonIgnore
    private boolean print_urlDirtyFlag;

    /**
     * 属性 [RESULT_URL]
     *
     */
    @Survey_user_inputResult_urlDefault(info = "默认规则")
    private String result_url;

    @JsonIgnore
    private boolean result_urlDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Survey_user_inputPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Survey_user_inputPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [SURVEY_ID]
     *
     */
    @Survey_user_inputSurvey_idDefault(info = "默认规则")
    private Integer survey_id;

    @JsonIgnore
    private boolean survey_idDirtyFlag;

    /**
     * 属性 [LAST_DISPLAYED_PAGE_ID]
     *
     */
    @Survey_user_inputLast_displayed_page_idDefault(info = "默认规则")
    private Integer last_displayed_page_id;

    @JsonIgnore
    private boolean last_displayed_page_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Survey_user_inputWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Survey_user_inputCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [USER_INPUT_LINE_IDS]
     */
    @JsonProperty("user_input_line_ids")
    public String getUser_input_line_ids(){
        return user_input_line_ids ;
    }

    /**
     * 设置 [USER_INPUT_LINE_IDS]
     */
    @JsonProperty("user_input_line_ids")
    public void setUser_input_line_ids(String  user_input_line_ids){
        this.user_input_line_ids = user_input_line_ids ;
        this.user_input_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [USER_INPUT_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getUser_input_line_idsDirtyFlag(){
        return user_input_line_idsDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [QUIZZ_SCORE]
     */
    @JsonProperty("quizz_score")
    public Double getQuizz_score(){
        return quizz_score ;
    }

    /**
     * 设置 [QUIZZ_SCORE]
     */
    @JsonProperty("quizz_score")
    public void setQuizz_score(Double  quizz_score){
        this.quizz_score = quizz_score ;
        this.quizz_scoreDirtyFlag = true ;
    }

    /**
     * 获取 [QUIZZ_SCORE]脏标记
     */
    @JsonIgnore
    public boolean getQuizz_scoreDirtyFlag(){
        return quizz_scoreDirtyFlag ;
    }

    /**
     * 获取 [TOKEN]
     */
    @JsonProperty("token")
    public String getToken(){
        return token ;
    }

    /**
     * 设置 [TOKEN]
     */
    @JsonProperty("token")
    public void setToken(String  token){
        this.token = token ;
        this.tokenDirtyFlag = true ;
    }

    /**
     * 获取 [TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getTokenDirtyFlag(){
        return tokenDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [EMAIL]
     */
    @JsonProperty("email")
    public String getEmail(){
        return email ;
    }

    /**
     * 设置 [EMAIL]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return emailDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [TEST_ENTRY]
     */
    @JsonProperty("test_entry")
    public String getTest_entry(){
        return test_entry ;
    }

    /**
     * 设置 [TEST_ENTRY]
     */
    @JsonProperty("test_entry")
    public void setTest_entry(String  test_entry){
        this.test_entry = test_entry ;
        this.test_entryDirtyFlag = true ;
    }

    /**
     * 获取 [TEST_ENTRY]脏标记
     */
    @JsonIgnore
    public boolean getTest_entryDirtyFlag(){
        return test_entryDirtyFlag ;
    }

    /**
     * 获取 [DEADLINE]
     */
    @JsonProperty("deadline")
    public Timestamp getDeadline(){
        return deadline ;
    }

    /**
     * 设置 [DEADLINE]
     */
    @JsonProperty("deadline")
    public void setDeadline(Timestamp  deadline){
        this.deadline = deadline ;
        this.deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getDeadlineDirtyFlag(){
        return deadlineDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [DATE_CREATE]
     */
    @JsonProperty("date_create")
    public Timestamp getDate_create(){
        return date_create ;
    }

    /**
     * 设置 [DATE_CREATE]
     */
    @JsonProperty("date_create")
    public void setDate_create(Timestamp  date_create){
        this.date_create = date_create ;
        this.date_createDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_CREATE]脏标记
     */
    @JsonIgnore
    public boolean getDate_createDirtyFlag(){
        return date_createDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRINT_URL]
     */
    @JsonProperty("print_url")
    public String getPrint_url(){
        return print_url ;
    }

    /**
     * 设置 [PRINT_URL]
     */
    @JsonProperty("print_url")
    public void setPrint_url(String  print_url){
        this.print_url = print_url ;
        this.print_urlDirtyFlag = true ;
    }

    /**
     * 获取 [PRINT_URL]脏标记
     */
    @JsonIgnore
    public boolean getPrint_urlDirtyFlag(){
        return print_urlDirtyFlag ;
    }

    /**
     * 获取 [RESULT_URL]
     */
    @JsonProperty("result_url")
    public String getResult_url(){
        return result_url ;
    }

    /**
     * 设置 [RESULT_URL]
     */
    @JsonProperty("result_url")
    public void setResult_url(String  result_url){
        this.result_url = result_url ;
        this.result_urlDirtyFlag = true ;
    }

    /**
     * 获取 [RESULT_URL]脏标记
     */
    @JsonIgnore
    public boolean getResult_urlDirtyFlag(){
        return result_urlDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [SURVEY_ID]
     */
    @JsonProperty("survey_id")
    public Integer getSurvey_id(){
        return survey_id ;
    }

    /**
     * 设置 [SURVEY_ID]
     */
    @JsonProperty("survey_id")
    public void setSurvey_id(Integer  survey_id){
        this.survey_id = survey_id ;
        this.survey_idDirtyFlag = true ;
    }

    /**
     * 获取 [SURVEY_ID]脏标记
     */
    @JsonIgnore
    public boolean getSurvey_idDirtyFlag(){
        return survey_idDirtyFlag ;
    }

    /**
     * 获取 [LAST_DISPLAYED_PAGE_ID]
     */
    @JsonProperty("last_displayed_page_id")
    public Integer getLast_displayed_page_id(){
        return last_displayed_page_id ;
    }

    /**
     * 设置 [LAST_DISPLAYED_PAGE_ID]
     */
    @JsonProperty("last_displayed_page_id")
    public void setLast_displayed_page_id(Integer  last_displayed_page_id){
        this.last_displayed_page_id = last_displayed_page_id ;
        this.last_displayed_page_idDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_DISPLAYED_PAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getLast_displayed_page_idDirtyFlag(){
        return last_displayed_page_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Survey_user_input toDO() {
        Survey_user_input srfdomain = new Survey_user_input();
        if(getUser_input_line_idsDirtyFlag())
            srfdomain.setUser_input_line_ids(user_input_line_ids);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getQuizz_scoreDirtyFlag())
            srfdomain.setQuizz_score(quizz_score);
        if(getTokenDirtyFlag())
            srfdomain.setToken(token);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getEmailDirtyFlag())
            srfdomain.setEmail(email);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getTest_entryDirtyFlag())
            srfdomain.setTest_entry(test_entry);
        if(getDeadlineDirtyFlag())
            srfdomain.setDeadline(deadline);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getDate_createDirtyFlag())
            srfdomain.setDate_create(date_create);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getPrint_urlDirtyFlag())
            srfdomain.setPrint_url(print_url);
        if(getResult_urlDirtyFlag())
            srfdomain.setResult_url(result_url);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getSurvey_idDirtyFlag())
            srfdomain.setSurvey_id(survey_id);
        if(getLast_displayed_page_idDirtyFlag())
            srfdomain.setLast_displayed_page_id(last_displayed_page_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Survey_user_input srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getUser_input_line_idsDirtyFlag())
            this.setUser_input_line_ids(srfdomain.getUser_input_line_ids());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getQuizz_scoreDirtyFlag())
            this.setQuizz_score(srfdomain.getQuizz_score());
        if(srfdomain.getTokenDirtyFlag())
            this.setToken(srfdomain.getToken());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getEmailDirtyFlag())
            this.setEmail(srfdomain.getEmail());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getTest_entryDirtyFlag())
            this.setTest_entry(srfdomain.getTest_entry());
        if(srfdomain.getDeadlineDirtyFlag())
            this.setDeadline(srfdomain.getDeadline());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getDate_createDirtyFlag())
            this.setDate_create(srfdomain.getDate_create());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getPrint_urlDirtyFlag())
            this.setPrint_url(srfdomain.getPrint_url());
        if(srfdomain.getResult_urlDirtyFlag())
            this.setResult_url(srfdomain.getResult_url());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getSurvey_idDirtyFlag())
            this.setSurvey_id(srfdomain.getSurvey_id());
        if(srfdomain.getLast_displayed_page_idDirtyFlag())
            this.setLast_displayed_page_id(srfdomain.getLast_displayed_page_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Survey_user_inputDTO> fromDOPage(List<Survey_user_input> poPage)   {
        if(poPage == null)
            return null;
        List<Survey_user_inputDTO> dtos=new ArrayList<Survey_user_inputDTO>();
        for(Survey_user_input domain : poPage) {
            Survey_user_inputDTO dto = new Survey_user_inputDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

