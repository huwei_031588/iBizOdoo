package cn.ibizlab.odoo.service.odoo_survey.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_survey.dto.Survey_user_inputDTO;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_user_inputService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_inputSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Survey_user_input" })
@RestController
@RequestMapping("")
public class Survey_user_inputResource {

    @Autowired
    private ISurvey_user_inputService survey_user_inputService;

    public ISurvey_user_inputService getSurvey_user_inputService() {
        return this.survey_user_inputService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Survey_user_input" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_user_inputs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Survey_user_inputDTO> survey_user_inputdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Survey_user_input" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_user_inputs/{survey_user_input_id}")

    public ResponseEntity<Survey_user_inputDTO> update(@PathVariable("survey_user_input_id") Integer survey_user_input_id, @RequestBody Survey_user_inputDTO survey_user_inputdto) {
		Survey_user_input domain = survey_user_inputdto.toDO();
        domain.setId(survey_user_input_id);
		survey_user_inputService.update(domain);
		Survey_user_inputDTO dto = new Survey_user_inputDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Survey_user_input" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_user_inputs")

    public ResponseEntity<Survey_user_inputDTO> create(@RequestBody Survey_user_inputDTO survey_user_inputdto) {
        Survey_user_inputDTO dto = new Survey_user_inputDTO();
        Survey_user_input domain = survey_user_inputdto.toDO();
		survey_user_inputService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Survey_user_input" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_user_inputs/{survey_user_input_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_user_input_id") Integer survey_user_input_id) {
        Survey_user_inputDTO survey_user_inputdto = new Survey_user_inputDTO();
		Survey_user_input domain = new Survey_user_input();
		survey_user_inputdto.setId(survey_user_input_id);
		domain.setId(survey_user_input_id);
        Boolean rst = survey_user_inputService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Survey_user_input" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_user_inputs/createBatch")
    public ResponseEntity<Boolean> createBatchSurvey_user_input(@RequestBody List<Survey_user_inputDTO> survey_user_inputdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Survey_user_input" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_user_inputs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_user_inputDTO> survey_user_inputdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Survey_user_input" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_user_inputs/{survey_user_input_id}")
    public ResponseEntity<Survey_user_inputDTO> get(@PathVariable("survey_user_input_id") Integer survey_user_input_id) {
        Survey_user_inputDTO dto = new Survey_user_inputDTO();
        Survey_user_input domain = survey_user_inputService.get(survey_user_input_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Survey_user_input" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_survey/survey_user_inputs/fetchdefault")
	public ResponseEntity<Page<Survey_user_inputDTO>> fetchDefault(Survey_user_inputSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Survey_user_inputDTO> list = new ArrayList<Survey_user_inputDTO>();
        
        Page<Survey_user_input> domains = survey_user_inputService.searchDefault(context) ;
        for(Survey_user_input survey_user_input : domains.getContent()){
            Survey_user_inputDTO dto = new Survey_user_inputDTO();
            dto.fromDO(survey_user_input);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
