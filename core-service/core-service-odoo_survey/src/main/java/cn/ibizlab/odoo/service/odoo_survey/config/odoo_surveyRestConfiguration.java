package cn.ibizlab.odoo.service.odoo_survey.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_survey")
public class odoo_surveyRestConfiguration {

}
