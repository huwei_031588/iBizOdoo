package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_pm_rule_lineDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule_line;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_rule_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_rule_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_pm_rule_line" })
@RestController
@RequestMapping("")
public class Mro_pm_rule_lineResource {

    @Autowired
    private IMro_pm_rule_lineService mro_pm_rule_lineService;

    public IMro_pm_rule_lineService getMro_pm_rule_lineService() {
        return this.mro_pm_rule_lineService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_pm_rule_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_rule_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_rule_lineDTO> mro_pm_rule_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_pm_rule_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_rule_lines/{mro_pm_rule_line_id}")
    public ResponseEntity<Mro_pm_rule_lineDTO> get(@PathVariable("mro_pm_rule_line_id") Integer mro_pm_rule_line_id) {
        Mro_pm_rule_lineDTO dto = new Mro_pm_rule_lineDTO();
        Mro_pm_rule_line domain = mro_pm_rule_lineService.get(mro_pm_rule_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_pm_rule_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_rule_lines/{mro_pm_rule_line_id}")

    public ResponseEntity<Mro_pm_rule_lineDTO> update(@PathVariable("mro_pm_rule_line_id") Integer mro_pm_rule_line_id, @RequestBody Mro_pm_rule_lineDTO mro_pm_rule_linedto) {
		Mro_pm_rule_line domain = mro_pm_rule_linedto.toDO();
        domain.setId(mro_pm_rule_line_id);
		mro_pm_rule_lineService.update(domain);
		Mro_pm_rule_lineDTO dto = new Mro_pm_rule_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_pm_rule_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_rule_lines/{mro_pm_rule_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_rule_line_id") Integer mro_pm_rule_line_id) {
        Mro_pm_rule_lineDTO mro_pm_rule_linedto = new Mro_pm_rule_lineDTO();
		Mro_pm_rule_line domain = new Mro_pm_rule_line();
		mro_pm_rule_linedto.setId(mro_pm_rule_line_id);
		domain.setId(mro_pm_rule_line_id);
        Boolean rst = mro_pm_rule_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_pm_rule_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_rule_lines")

    public ResponseEntity<Mro_pm_rule_lineDTO> create(@RequestBody Mro_pm_rule_lineDTO mro_pm_rule_linedto) {
        Mro_pm_rule_lineDTO dto = new Mro_pm_rule_lineDTO();
        Mro_pm_rule_line domain = mro_pm_rule_linedto.toDO();
		mro_pm_rule_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_pm_rule_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_rule_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_pm_rule_lineDTO> mro_pm_rule_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_pm_rule_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_rule_lines/createBatch")
    public ResponseEntity<Boolean> createBatchMro_pm_rule_line(@RequestBody List<Mro_pm_rule_lineDTO> mro_pm_rule_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_pm_rule_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_pm_rule_lines/fetchdefault")
	public ResponseEntity<Page<Mro_pm_rule_lineDTO>> fetchDefault(Mro_pm_rule_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_pm_rule_lineDTO> list = new ArrayList<Mro_pm_rule_lineDTO>();
        
        Page<Mro_pm_rule_line> domains = mro_pm_rule_lineService.searchDefault(context) ;
        for(Mro_pm_rule_line mro_pm_rule_line : domains.getContent()){
            Mro_pm_rule_lineDTO dto = new Mro_pm_rule_lineDTO();
            dto.fromDO(mro_pm_rule_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
