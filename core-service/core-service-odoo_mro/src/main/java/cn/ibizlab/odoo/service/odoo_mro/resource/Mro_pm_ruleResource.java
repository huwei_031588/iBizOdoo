package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_pm_ruleDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_ruleService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_ruleSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_pm_rule" })
@RestController
@RequestMapping("")
public class Mro_pm_ruleResource {

    @Autowired
    private IMro_pm_ruleService mro_pm_ruleService;

    public IMro_pm_ruleService getMro_pm_ruleService() {
        return this.mro_pm_ruleService;
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_pm_rule" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_rules/{mro_pm_rule_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_rule_id") Integer mro_pm_rule_id) {
        Mro_pm_ruleDTO mro_pm_ruledto = new Mro_pm_ruleDTO();
		Mro_pm_rule domain = new Mro_pm_rule();
		mro_pm_ruledto.setId(mro_pm_rule_id);
		domain.setId(mro_pm_rule_id);
        Boolean rst = mro_pm_ruleService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_pm_rule" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_rules")

    public ResponseEntity<Mro_pm_ruleDTO> create(@RequestBody Mro_pm_ruleDTO mro_pm_ruledto) {
        Mro_pm_ruleDTO dto = new Mro_pm_ruleDTO();
        Mro_pm_rule domain = mro_pm_ruledto.toDO();
		mro_pm_ruleService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_pm_rule" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_rules/{mro_pm_rule_id}")
    public ResponseEntity<Mro_pm_ruleDTO> get(@PathVariable("mro_pm_rule_id") Integer mro_pm_rule_id) {
        Mro_pm_ruleDTO dto = new Mro_pm_ruleDTO();
        Mro_pm_rule domain = mro_pm_ruleService.get(mro_pm_rule_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_pm_rule" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_rules/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_ruleDTO> mro_pm_ruledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_pm_rule" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_rules/{mro_pm_rule_id}")

    public ResponseEntity<Mro_pm_ruleDTO> update(@PathVariable("mro_pm_rule_id") Integer mro_pm_rule_id, @RequestBody Mro_pm_ruleDTO mro_pm_ruledto) {
		Mro_pm_rule domain = mro_pm_ruledto.toDO();
        domain.setId(mro_pm_rule_id);
		mro_pm_ruleService.update(domain);
		Mro_pm_ruleDTO dto = new Mro_pm_ruleDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_pm_rule" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_rules/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_pm_ruleDTO> mro_pm_ruledtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_pm_rule" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_rules/createBatch")
    public ResponseEntity<Boolean> createBatchMro_pm_rule(@RequestBody List<Mro_pm_ruleDTO> mro_pm_ruledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_pm_rule" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_pm_rules/fetchdefault")
	public ResponseEntity<Page<Mro_pm_ruleDTO>> fetchDefault(Mro_pm_ruleSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_pm_ruleDTO> list = new ArrayList<Mro_pm_ruleDTO>();
        
        Page<Mro_pm_rule> domains = mro_pm_ruleService.searchDefault(context) ;
        for(Mro_pm_rule mro_pm_rule : domains.getContent()){
            Mro_pm_ruleDTO dto = new Mro_pm_ruleDTO();
            dto.fromDO(mro_pm_rule);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
