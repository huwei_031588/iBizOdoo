package cn.ibizlab.odoo.service.odoo_barcodes.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_barcodes.dto.Barcodes_barcode_events_mixinDTO;
import cn.ibizlab.odoo.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.odoo.core.odoo_barcodes.service.IBarcodes_barcode_events_mixinService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Barcodes_barcode_events_mixin" })
@RestController
@RequestMapping("")
public class Barcodes_barcode_events_mixinResource {

    @Autowired
    private IBarcodes_barcode_events_mixinService barcodes_barcode_events_mixinService;

    public IBarcodes_barcode_events_mixinService getBarcodes_barcode_events_mixinService() {
        return this.barcodes_barcode_events_mixinService;
    }

    @ApiOperation(value = "建立数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_barcodes/barcodes_barcode_events_mixins")

    public ResponseEntity<Barcodes_barcode_events_mixinDTO> create(@RequestBody Barcodes_barcode_events_mixinDTO barcodes_barcode_events_mixindto) {
        Barcodes_barcode_events_mixinDTO dto = new Barcodes_barcode_events_mixinDTO();
        Barcodes_barcode_events_mixin domain = barcodes_barcode_events_mixindto.toDO();
		barcodes_barcode_events_mixinService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_barcodes/barcodes_barcode_events_mixins/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Barcodes_barcode_events_mixinDTO> barcodes_barcode_events_mixindtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_barcodes/barcodes_barcode_events_mixins/{barcodes_barcode_events_mixin_id}")

    public ResponseEntity<Barcodes_barcode_events_mixinDTO> update(@PathVariable("barcodes_barcode_events_mixin_id") Integer barcodes_barcode_events_mixin_id, @RequestBody Barcodes_barcode_events_mixinDTO barcodes_barcode_events_mixindto) {
		Barcodes_barcode_events_mixin domain = barcodes_barcode_events_mixindto.toDO();
        domain.setId(barcodes_barcode_events_mixin_id);
		barcodes_barcode_events_mixinService.update(domain);
		Barcodes_barcode_events_mixinDTO dto = new Barcodes_barcode_events_mixinDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_barcodes/barcodes_barcode_events_mixins/{barcodes_barcode_events_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("barcodes_barcode_events_mixin_id") Integer barcodes_barcode_events_mixin_id) {
        Barcodes_barcode_events_mixinDTO barcodes_barcode_events_mixindto = new Barcodes_barcode_events_mixinDTO();
		Barcodes_barcode_events_mixin domain = new Barcodes_barcode_events_mixin();
		barcodes_barcode_events_mixindto.setId(barcodes_barcode_events_mixin_id);
		domain.setId(barcodes_barcode_events_mixin_id);
        Boolean rst = barcodes_barcode_events_mixinService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_barcodes/barcodes_barcode_events_mixins/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Barcodes_barcode_events_mixinDTO> barcodes_barcode_events_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_barcodes/barcodes_barcode_events_mixins/{barcodes_barcode_events_mixin_id}")
    public ResponseEntity<Barcodes_barcode_events_mixinDTO> get(@PathVariable("barcodes_barcode_events_mixin_id") Integer barcodes_barcode_events_mixin_id) {
        Barcodes_barcode_events_mixinDTO dto = new Barcodes_barcode_events_mixinDTO();
        Barcodes_barcode_events_mixin domain = barcodes_barcode_events_mixinService.get(barcodes_barcode_events_mixin_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_barcodes/barcodes_barcode_events_mixins/createBatch")
    public ResponseEntity<Boolean> createBatchBarcodes_barcode_events_mixin(@RequestBody List<Barcodes_barcode_events_mixinDTO> barcodes_barcode_events_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Barcodes_barcode_events_mixin" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_barcodes/barcodes_barcode_events_mixins/fetchdefault")
	public ResponseEntity<Page<Barcodes_barcode_events_mixinDTO>> fetchDefault(Barcodes_barcode_events_mixinSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Barcodes_barcode_events_mixinDTO> list = new ArrayList<Barcodes_barcode_events_mixinDTO>();
        
        Page<Barcodes_barcode_events_mixin> domains = barcodes_barcode_events_mixinService.searchDefault(context) ;
        for(Barcodes_barcode_events_mixin barcodes_barcode_events_mixin : domains.getContent()){
            Barcodes_barcode_events_mixinDTO dto = new Barcodes_barcode_events_mixinDTO();
            dto.fromDO(barcodes_barcode_events_mixin);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
