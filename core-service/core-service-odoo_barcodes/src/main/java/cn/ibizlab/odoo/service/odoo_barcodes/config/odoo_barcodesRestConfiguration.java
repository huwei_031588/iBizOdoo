package cn.ibizlab.odoo.service.odoo_barcodes.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_barcodes")
public class odoo_barcodesRestConfiguration {

}
