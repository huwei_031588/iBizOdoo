package cn.ibizlab.odoo.service.odoo_barcodes.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_barcodes.valuerule.anno.barcodes_barcode_events_mixin.*;
import cn.ibizlab.odoo.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Barcodes_barcode_events_mixinDTO]
 */
public class Barcodes_barcode_events_mixinDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Barcodes_barcode_events_mixinIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Barcodes_barcode_events_mixinDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Barcodes_barcode_events_mixin__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [_BARCODE_SCANNED]
     *
     */
    @Barcodes_barcode_events_mixin_barcode_scannedDefault(info = "默认规则")
    private String _barcode_scanned;

    @JsonIgnore
    private boolean _barcode_scannedDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [_BARCODE_SCANNED]
     */
    @JsonProperty("_barcode_scanned")
    public String get_barcode_scanned(){
        return _barcode_scanned ;
    }

    /**
     * 设置 [_BARCODE_SCANNED]
     */
    @JsonProperty("_barcode_scanned")
    public void set_barcode_scanned(String  _barcode_scanned){
        this._barcode_scanned = _barcode_scanned ;
        this._barcode_scannedDirtyFlag = true ;
    }

    /**
     * 获取 [_BARCODE_SCANNED]脏标记
     */
    @JsonIgnore
    public boolean get_barcode_scannedDirtyFlag(){
        return _barcode_scannedDirtyFlag ;
    }



    public Barcodes_barcode_events_mixin toDO() {
        Barcodes_barcode_events_mixin srfdomain = new Barcodes_barcode_events_mixin();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(get_barcode_scannedDirtyFlag())
            srfdomain.set_barcode_scanned(_barcode_scanned);

        return srfdomain;
    }

    public void fromDO(Barcodes_barcode_events_mixin srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.get_barcode_scannedDirtyFlag())
            this.set_barcode_scanned(srfdomain.get_barcode_scanned());

    }

    public List<Barcodes_barcode_events_mixinDTO> fromDOPage(List<Barcodes_barcode_events_mixin> poPage)   {
        if(poPage == null)
            return null;
        List<Barcodes_barcode_events_mixinDTO> dtos=new ArrayList<Barcodes_barcode_events_mixinDTO>();
        for(Barcodes_barcode_events_mixin domain : poPage) {
            Barcodes_barcode_events_mixinDTO dto = new Barcodes_barcode_events_mixinDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

