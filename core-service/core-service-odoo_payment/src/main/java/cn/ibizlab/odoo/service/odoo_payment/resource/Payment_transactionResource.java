package cn.ibizlab.odoo.service.odoo_payment.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_payment.dto.Payment_transactionDTO;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_transactionService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_transactionSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Payment_transaction" })
@RestController
@RequestMapping("")
public class Payment_transactionResource {

    @Autowired
    private IPayment_transactionService payment_transactionService;

    public IPayment_transactionService getPayment_transactionService() {
        return this.payment_transactionService;
    }

    @ApiOperation(value = "建立数据", tags = {"Payment_transaction" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_transactions")

    public ResponseEntity<Payment_transactionDTO> create(@RequestBody Payment_transactionDTO payment_transactiondto) {
        Payment_transactionDTO dto = new Payment_transactionDTO();
        Payment_transaction domain = payment_transactiondto.toDO();
		payment_transactionService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Payment_transaction" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_transactions/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Payment_transactionDTO> payment_transactiondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Payment_transaction" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_transactions/{payment_transaction_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("payment_transaction_id") Integer payment_transaction_id) {
        Payment_transactionDTO payment_transactiondto = new Payment_transactionDTO();
		Payment_transaction domain = new Payment_transaction();
		payment_transactiondto.setId(payment_transaction_id);
		domain.setId(payment_transaction_id);
        Boolean rst = payment_transactionService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Payment_transaction" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_transactions/createBatch")
    public ResponseEntity<Boolean> createBatchPayment_transaction(@RequestBody List<Payment_transactionDTO> payment_transactiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Payment_transaction" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_transactions/{payment_transaction_id}")
    public ResponseEntity<Payment_transactionDTO> get(@PathVariable("payment_transaction_id") Integer payment_transaction_id) {
        Payment_transactionDTO dto = new Payment_transactionDTO();
        Payment_transaction domain = payment_transactionService.get(payment_transaction_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Payment_transaction" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_transactions/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_transactionDTO> payment_transactiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Payment_transaction" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_transactions/{payment_transaction_id}")

    public ResponseEntity<Payment_transactionDTO> update(@PathVariable("payment_transaction_id") Integer payment_transaction_id, @RequestBody Payment_transactionDTO payment_transactiondto) {
		Payment_transaction domain = payment_transactiondto.toDO();
        domain.setId(payment_transaction_id);
		payment_transactionService.update(domain);
		Payment_transactionDTO dto = new Payment_transactionDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Payment_transaction" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_payment/payment_transactions/fetchdefault")
	public ResponseEntity<Page<Payment_transactionDTO>> fetchDefault(Payment_transactionSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Payment_transactionDTO> list = new ArrayList<Payment_transactionDTO>();
        
        Page<Payment_transaction> domains = payment_transactionService.searchDefault(context) ;
        for(Payment_transaction payment_transaction : domains.getContent()){
            Payment_transactionDTO dto = new Payment_transactionDTO();
            dto.fromDO(payment_transaction);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
