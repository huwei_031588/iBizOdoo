package cn.ibizlab.odoo.service.odoo_payment.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_payment.dto.Payment_acquirer_onboarding_wizardDTO;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_acquirer_onboarding_wizardService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Payment_acquirer_onboarding_wizard" })
@RestController
@RequestMapping("")
public class Payment_acquirer_onboarding_wizardResource {

    @Autowired
    private IPayment_acquirer_onboarding_wizardService payment_acquirer_onboarding_wizardService;

    public IPayment_acquirer_onboarding_wizardService getPayment_acquirer_onboarding_wizardService() {
        return this.payment_acquirer_onboarding_wizardService;
    }

    @ApiOperation(value = "删除数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_acquirer_onboarding_wizards/{payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("payment_acquirer_onboarding_wizard_id") Integer payment_acquirer_onboarding_wizard_id) {
        Payment_acquirer_onboarding_wizardDTO payment_acquirer_onboarding_wizarddto = new Payment_acquirer_onboarding_wizardDTO();
		Payment_acquirer_onboarding_wizard domain = new Payment_acquirer_onboarding_wizard();
		payment_acquirer_onboarding_wizarddto.setId(payment_acquirer_onboarding_wizard_id);
		domain.setId(payment_acquirer_onboarding_wizard_id);
        Boolean rst = payment_acquirer_onboarding_wizardService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_acquirer_onboarding_wizards")

    public ResponseEntity<Payment_acquirer_onboarding_wizardDTO> create(@RequestBody Payment_acquirer_onboarding_wizardDTO payment_acquirer_onboarding_wizarddto) {
        Payment_acquirer_onboarding_wizardDTO dto = new Payment_acquirer_onboarding_wizardDTO();
        Payment_acquirer_onboarding_wizard domain = payment_acquirer_onboarding_wizarddto.toDO();
		payment_acquirer_onboarding_wizardService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_acquirer_onboarding_wizards/createBatch")
    public ResponseEntity<Boolean> createBatchPayment_acquirer_onboarding_wizard(@RequestBody List<Payment_acquirer_onboarding_wizardDTO> payment_acquirer_onboarding_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_acquirer_onboarding_wizards/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Payment_acquirer_onboarding_wizardDTO> payment_acquirer_onboarding_wizarddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_acquirer_onboarding_wizards/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_acquirer_onboarding_wizardDTO> payment_acquirer_onboarding_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_acquirer_onboarding_wizards/{payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Payment_acquirer_onboarding_wizardDTO> get(@PathVariable("payment_acquirer_onboarding_wizard_id") Integer payment_acquirer_onboarding_wizard_id) {
        Payment_acquirer_onboarding_wizardDTO dto = new Payment_acquirer_onboarding_wizardDTO();
        Payment_acquirer_onboarding_wizard domain = payment_acquirer_onboarding_wizardService.get(payment_acquirer_onboarding_wizard_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_acquirer_onboarding_wizards/{payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Payment_acquirer_onboarding_wizardDTO> update(@PathVariable("payment_acquirer_onboarding_wizard_id") Integer payment_acquirer_onboarding_wizard_id, @RequestBody Payment_acquirer_onboarding_wizardDTO payment_acquirer_onboarding_wizarddto) {
		Payment_acquirer_onboarding_wizard domain = payment_acquirer_onboarding_wizarddto.toDO();
        domain.setId(payment_acquirer_onboarding_wizard_id);
		payment_acquirer_onboarding_wizardService.update(domain);
		Payment_acquirer_onboarding_wizardDTO dto = new Payment_acquirer_onboarding_wizardDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Payment_acquirer_onboarding_wizard" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_payment/payment_acquirer_onboarding_wizards/fetchdefault")
	public ResponseEntity<Page<Payment_acquirer_onboarding_wizardDTO>> fetchDefault(Payment_acquirer_onboarding_wizardSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Payment_acquirer_onboarding_wizardDTO> list = new ArrayList<Payment_acquirer_onboarding_wizardDTO>();
        
        Page<Payment_acquirer_onboarding_wizard> domains = payment_acquirer_onboarding_wizardService.searchDefault(context) ;
        for(Payment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard : domains.getContent()){
            Payment_acquirer_onboarding_wizardDTO dto = new Payment_acquirer_onboarding_wizardDTO();
            dto.fromDO(payment_acquirer_onboarding_wizard);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
