package cn.ibizlab.odoo.service.odoo_payment.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_payment")
public class odoo_paymentRestConfiguration {

}
