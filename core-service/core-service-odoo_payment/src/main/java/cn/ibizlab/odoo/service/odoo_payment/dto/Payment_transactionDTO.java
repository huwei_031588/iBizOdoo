package cn.ibizlab.odoo.service.odoo_payment.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_payment.valuerule.anno.payment_transaction.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Payment_transactionDTO]
 */
public class Payment_transactionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Payment_transactionIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Payment_transactionAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [PARTNER_PHONE]
     *
     */
    @Payment_transactionPartner_phoneDefault(info = "默认规则")
    private String partner_phone;

    @JsonIgnore
    private boolean partner_phoneDirtyFlag;

    /**
     * 属性 [PARTNER_EMAIL]
     *
     */
    @Payment_transactionPartner_emailDefault(info = "默认规则")
    private String partner_email;

    @JsonIgnore
    private boolean partner_emailDirtyFlag;

    /**
     * 属性 [CALLBACK_METHOD]
     *
     */
    @Payment_transactionCallback_methodDefault(info = "默认规则")
    private String callback_method;

    @JsonIgnore
    private boolean callback_methodDirtyFlag;

    /**
     * 属性 [RETURN_URL]
     *
     */
    @Payment_transactionReturn_urlDefault(info = "默认规则")
    private String return_url;

    @JsonIgnore
    private boolean return_urlDirtyFlag;

    /**
     * 属性 [SALE_ORDER_IDS_NBR]
     *
     */
    @Payment_transactionSale_order_ids_nbrDefault(info = "默认规则")
    private Integer sale_order_ids_nbr;

    @JsonIgnore
    private boolean sale_order_ids_nbrDirtyFlag;

    /**
     * 属性 [ACQUIRER_REFERENCE]
     *
     */
    @Payment_transactionAcquirer_referenceDefault(info = "默认规则")
    private String acquirer_reference;

    @JsonIgnore
    private boolean acquirer_referenceDirtyFlag;

    /**
     * 属性 [PARTNER_LANG]
     *
     */
    @Payment_transactionPartner_langDefault(info = "默认规则")
    private String partner_lang;

    @JsonIgnore
    private boolean partner_langDirtyFlag;

    /**
     * 属性 [PARTNER_ZIP]
     *
     */
    @Payment_transactionPartner_zipDefault(info = "默认规则")
    private String partner_zip;

    @JsonIgnore
    private boolean partner_zipDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Payment_transactionDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Payment_transactionStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Payment_transactionDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [CALLBACK_MODEL_ID]
     *
     */
    @Payment_transactionCallback_model_idDefault(info = "默认规则")
    private Integer callback_model_id;

    @JsonIgnore
    private boolean callback_model_idDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Payment_transaction__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @Payment_transactionInvoice_idsDefault(info = "默认规则")
    private String invoice_ids;

    @JsonIgnore
    private boolean invoice_idsDirtyFlag;

    /**
     * 属性 [SALE_ORDER_IDS]
     *
     */
    @Payment_transactionSale_order_idsDefault(info = "默认规则")
    private String sale_order_ids;

    @JsonIgnore
    private boolean sale_order_idsDirtyFlag;

    /**
     * 属性 [FEES]
     *
     */
    @Payment_transactionFeesDefault(info = "默认规则")
    private Double fees;

    @JsonIgnore
    private boolean feesDirtyFlag;

    /**
     * 属性 [STATE_MESSAGE]
     *
     */
    @Payment_transactionState_messageDefault(info = "默认规则")
    private String state_message;

    @JsonIgnore
    private boolean state_messageDirtyFlag;

    /**
     * 属性 [CALLBACK_RES_ID]
     *
     */
    @Payment_transactionCallback_res_idDefault(info = "默认规则")
    private Integer callback_res_id;

    @JsonIgnore
    private boolean callback_res_idDirtyFlag;

    /**
     * 属性 [PARTNER_CITY]
     *
     */
    @Payment_transactionPartner_cityDefault(info = "默认规则")
    private String partner_city;

    @JsonIgnore
    private boolean partner_cityDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Payment_transactionWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Payment_transactionCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [INVOICE_IDS_NBR]
     *
     */
    @Payment_transactionInvoice_ids_nbrDefault(info = "默认规则")
    private Integer invoice_ids_nbr;

    @JsonIgnore
    private boolean invoice_ids_nbrDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Payment_transactionTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [HTML_3DS]
     *
     */
    @Payment_transactionHtml_3dsDefault(info = "默认规则")
    private String html_3ds;

    @JsonIgnore
    private boolean html_3dsDirtyFlag;

    /**
     * 属性 [PARTNER_NAME]
     *
     */
    @Payment_transactionPartner_nameDefault(info = "默认规则")
    private String partner_name;

    @JsonIgnore
    private boolean partner_nameDirtyFlag;

    /**
     * 属性 [REFERENCE]
     *
     */
    @Payment_transactionReferenceDefault(info = "默认规则")
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;

    /**
     * 属性 [PARTNER_ADDRESS]
     *
     */
    @Payment_transactionPartner_addressDefault(info = "默认规则")
    private String partner_address;

    @JsonIgnore
    private boolean partner_addressDirtyFlag;

    /**
     * 属性 [IS_PROCESSED]
     *
     */
    @Payment_transactionIs_processedDefault(info = "默认规则")
    private String is_processed;

    @JsonIgnore
    private boolean is_processedDirtyFlag;

    /**
     * 属性 [CALLBACK_HASH]
     *
     */
    @Payment_transactionCallback_hashDefault(info = "默认规则")
    private String callback_hash;

    @JsonIgnore
    private boolean callback_hashDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Payment_transactionCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Payment_transactionWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Payment_transactionCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PAYMENT_ID_TEXT]
     *
     */
    @Payment_transactionPayment_id_textDefault(info = "默认规则")
    private String payment_id_text;

    @JsonIgnore
    private boolean payment_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Payment_transactionPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [PROVIDER]
     *
     */
    @Payment_transactionProviderDefault(info = "默认规则")
    private String provider;

    @JsonIgnore
    private boolean providerDirtyFlag;

    /**
     * 属性 [ACQUIRER_ID_TEXT]
     *
     */
    @Payment_transactionAcquirer_id_textDefault(info = "默认规则")
    private String acquirer_id_text;

    @JsonIgnore
    private boolean acquirer_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_COUNTRY_ID_TEXT]
     *
     */
    @Payment_transactionPartner_country_id_textDefault(info = "默认规则")
    private String partner_country_id_text;

    @JsonIgnore
    private boolean partner_country_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_TOKEN_ID_TEXT]
     *
     */
    @Payment_transactionPayment_token_id_textDefault(info = "默认规则")
    private String payment_token_id_text;

    @JsonIgnore
    private boolean payment_token_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Payment_transactionCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Payment_transactionWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PARTNER_COUNTRY_ID]
     *
     */
    @Payment_transactionPartner_country_idDefault(info = "默认规则")
    private Integer partner_country_id;

    @JsonIgnore
    private boolean partner_country_idDirtyFlag;

    /**
     * 属性 [PAYMENT_TOKEN_ID]
     *
     */
    @Payment_transactionPayment_token_idDefault(info = "默认规则")
    private Integer payment_token_id;

    @JsonIgnore
    private boolean payment_token_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Payment_transactionPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Payment_transactionCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PAYMENT_ID]
     *
     */
    @Payment_transactionPayment_idDefault(info = "默认规则")
    private Integer payment_id;

    @JsonIgnore
    private boolean payment_idDirtyFlag;

    /**
     * 属性 [ACQUIRER_ID]
     *
     */
    @Payment_transactionAcquirer_idDefault(info = "默认规则")
    private Integer acquirer_id;

    @JsonIgnore
    private boolean acquirer_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_PHONE]
     */
    @JsonProperty("partner_phone")
    public String getPartner_phone(){
        return partner_phone ;
    }

    /**
     * 设置 [PARTNER_PHONE]
     */
    @JsonProperty("partner_phone")
    public void setPartner_phone(String  partner_phone){
        this.partner_phone = partner_phone ;
        this.partner_phoneDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_PHONE]脏标记
     */
    @JsonIgnore
    public boolean getPartner_phoneDirtyFlag(){
        return partner_phoneDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_EMAIL]
     */
    @JsonProperty("partner_email")
    public String getPartner_email(){
        return partner_email ;
    }

    /**
     * 设置 [PARTNER_EMAIL]
     */
    @JsonProperty("partner_email")
    public void setPartner_email(String  partner_email){
        this.partner_email = partner_email ;
        this.partner_emailDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getPartner_emailDirtyFlag(){
        return partner_emailDirtyFlag ;
    }

    /**
     * 获取 [CALLBACK_METHOD]
     */
    @JsonProperty("callback_method")
    public String getCallback_method(){
        return callback_method ;
    }

    /**
     * 设置 [CALLBACK_METHOD]
     */
    @JsonProperty("callback_method")
    public void setCallback_method(String  callback_method){
        this.callback_method = callback_method ;
        this.callback_methodDirtyFlag = true ;
    }

    /**
     * 获取 [CALLBACK_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getCallback_methodDirtyFlag(){
        return callback_methodDirtyFlag ;
    }

    /**
     * 获取 [RETURN_URL]
     */
    @JsonProperty("return_url")
    public String getReturn_url(){
        return return_url ;
    }

    /**
     * 设置 [RETURN_URL]
     */
    @JsonProperty("return_url")
    public void setReturn_url(String  return_url){
        this.return_url = return_url ;
        this.return_urlDirtyFlag = true ;
    }

    /**
     * 获取 [RETURN_URL]脏标记
     */
    @JsonIgnore
    public boolean getReturn_urlDirtyFlag(){
        return return_urlDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_IDS_NBR]
     */
    @JsonProperty("sale_order_ids_nbr")
    public Integer getSale_order_ids_nbr(){
        return sale_order_ids_nbr ;
    }

    /**
     * 设置 [SALE_ORDER_IDS_NBR]
     */
    @JsonProperty("sale_order_ids_nbr")
    public void setSale_order_ids_nbr(Integer  sale_order_ids_nbr){
        this.sale_order_ids_nbr = sale_order_ids_nbr ;
        this.sale_order_ids_nbrDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_IDS_NBR]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_ids_nbrDirtyFlag(){
        return sale_order_ids_nbrDirtyFlag ;
    }

    /**
     * 获取 [ACQUIRER_REFERENCE]
     */
    @JsonProperty("acquirer_reference")
    public String getAcquirer_reference(){
        return acquirer_reference ;
    }

    /**
     * 设置 [ACQUIRER_REFERENCE]
     */
    @JsonProperty("acquirer_reference")
    public void setAcquirer_reference(String  acquirer_reference){
        this.acquirer_reference = acquirer_reference ;
        this.acquirer_referenceDirtyFlag = true ;
    }

    /**
     * 获取 [ACQUIRER_REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_referenceDirtyFlag(){
        return acquirer_referenceDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_LANG]
     */
    @JsonProperty("partner_lang")
    public String getPartner_lang(){
        return partner_lang ;
    }

    /**
     * 设置 [PARTNER_LANG]
     */
    @JsonProperty("partner_lang")
    public void setPartner_lang(String  partner_lang){
        this.partner_lang = partner_lang ;
        this.partner_langDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_LANG]脏标记
     */
    @JsonIgnore
    public boolean getPartner_langDirtyFlag(){
        return partner_langDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ZIP]
     */
    @JsonProperty("partner_zip")
    public String getPartner_zip(){
        return partner_zip ;
    }

    /**
     * 设置 [PARTNER_ZIP]
     */
    @JsonProperty("partner_zip")
    public void setPartner_zip(String  partner_zip){
        this.partner_zip = partner_zip ;
        this.partner_zipDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ZIP]脏标记
     */
    @JsonIgnore
    public boolean getPartner_zipDirtyFlag(){
        return partner_zipDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [CALLBACK_MODEL_ID]
     */
    @JsonProperty("callback_model_id")
    public Integer getCallback_model_id(){
        return callback_model_id ;
    }

    /**
     * 设置 [CALLBACK_MODEL_ID]
     */
    @JsonProperty("callback_model_id")
    public void setCallback_model_id(Integer  callback_model_id){
        this.callback_model_id = callback_model_id ;
        this.callback_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [CALLBACK_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getCallback_model_idDirtyFlag(){
        return callback_model_idDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return invoice_ids ;
    }

    /**
     * 设置 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_IDS]
     */
    @JsonProperty("sale_order_ids")
    public String getSale_order_ids(){
        return sale_order_ids ;
    }

    /**
     * 设置 [SALE_ORDER_IDS]
     */
    @JsonProperty("sale_order_ids")
    public void setSale_order_ids(String  sale_order_ids){
        this.sale_order_ids = sale_order_ids ;
        this.sale_order_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_idsDirtyFlag(){
        return sale_order_idsDirtyFlag ;
    }

    /**
     * 获取 [FEES]
     */
    @JsonProperty("fees")
    public Double getFees(){
        return fees ;
    }

    /**
     * 设置 [FEES]
     */
    @JsonProperty("fees")
    public void setFees(Double  fees){
        this.fees = fees ;
        this.feesDirtyFlag = true ;
    }

    /**
     * 获取 [FEES]脏标记
     */
    @JsonIgnore
    public boolean getFeesDirtyFlag(){
        return feesDirtyFlag ;
    }

    /**
     * 获取 [STATE_MESSAGE]
     */
    @JsonProperty("state_message")
    public String getState_message(){
        return state_message ;
    }

    /**
     * 设置 [STATE_MESSAGE]
     */
    @JsonProperty("state_message")
    public void setState_message(String  state_message){
        this.state_message = state_message ;
        this.state_messageDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_MESSAGE]脏标记
     */
    @JsonIgnore
    public boolean getState_messageDirtyFlag(){
        return state_messageDirtyFlag ;
    }

    /**
     * 获取 [CALLBACK_RES_ID]
     */
    @JsonProperty("callback_res_id")
    public Integer getCallback_res_id(){
        return callback_res_id ;
    }

    /**
     * 设置 [CALLBACK_RES_ID]
     */
    @JsonProperty("callback_res_id")
    public void setCallback_res_id(Integer  callback_res_id){
        this.callback_res_id = callback_res_id ;
        this.callback_res_idDirtyFlag = true ;
    }

    /**
     * 获取 [CALLBACK_RES_ID]脏标记
     */
    @JsonIgnore
    public boolean getCallback_res_idDirtyFlag(){
        return callback_res_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_CITY]
     */
    @JsonProperty("partner_city")
    public String getPartner_city(){
        return partner_city ;
    }

    /**
     * 设置 [PARTNER_CITY]
     */
    @JsonProperty("partner_city")
    public void setPartner_city(String  partner_city){
        this.partner_city = partner_city ;
        this.partner_cityDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_CITY]脏标记
     */
    @JsonIgnore
    public boolean getPartner_cityDirtyFlag(){
        return partner_cityDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IDS_NBR]
     */
    @JsonProperty("invoice_ids_nbr")
    public Integer getInvoice_ids_nbr(){
        return invoice_ids_nbr ;
    }

    /**
     * 设置 [INVOICE_IDS_NBR]
     */
    @JsonProperty("invoice_ids_nbr")
    public void setInvoice_ids_nbr(Integer  invoice_ids_nbr){
        this.invoice_ids_nbr = invoice_ids_nbr ;
        this.invoice_ids_nbrDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IDS_NBR]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_ids_nbrDirtyFlag(){
        return invoice_ids_nbrDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [HTML_3DS]
     */
    @JsonProperty("html_3ds")
    public String getHtml_3ds(){
        return html_3ds ;
    }

    /**
     * 设置 [HTML_3DS]
     */
    @JsonProperty("html_3ds")
    public void setHtml_3ds(String  html_3ds){
        this.html_3ds = html_3ds ;
        this.html_3dsDirtyFlag = true ;
    }

    /**
     * 获取 [HTML_3DS]脏标记
     */
    @JsonIgnore
    public boolean getHtml_3dsDirtyFlag(){
        return html_3dsDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_NAME]
     */
    @JsonProperty("partner_name")
    public String getPartner_name(){
        return partner_name ;
    }

    /**
     * 设置 [PARTNER_NAME]
     */
    @JsonProperty("partner_name")
    public void setPartner_name(String  partner_name){
        this.partner_name = partner_name ;
        this.partner_nameDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_NAME]脏标记
     */
    @JsonIgnore
    public boolean getPartner_nameDirtyFlag(){
        return partner_nameDirtyFlag ;
    }

    /**
     * 获取 [REFERENCE]
     */
    @JsonProperty("reference")
    public String getReference(){
        return reference ;
    }

    /**
     * 设置 [REFERENCE]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return referenceDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ADDRESS]
     */
    @JsonProperty("partner_address")
    public String getPartner_address(){
        return partner_address ;
    }

    /**
     * 设置 [PARTNER_ADDRESS]
     */
    @JsonProperty("partner_address")
    public void setPartner_address(String  partner_address){
        this.partner_address = partner_address ;
        this.partner_addressDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ADDRESS]脏标记
     */
    @JsonIgnore
    public boolean getPartner_addressDirtyFlag(){
        return partner_addressDirtyFlag ;
    }

    /**
     * 获取 [IS_PROCESSED]
     */
    @JsonProperty("is_processed")
    public String getIs_processed(){
        return is_processed ;
    }

    /**
     * 设置 [IS_PROCESSED]
     */
    @JsonProperty("is_processed")
    public void setIs_processed(String  is_processed){
        this.is_processed = is_processed ;
        this.is_processedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PROCESSED]脏标记
     */
    @JsonIgnore
    public boolean getIs_processedDirtyFlag(){
        return is_processedDirtyFlag ;
    }

    /**
     * 获取 [CALLBACK_HASH]
     */
    @JsonProperty("callback_hash")
    public String getCallback_hash(){
        return callback_hash ;
    }

    /**
     * 设置 [CALLBACK_HASH]
     */
    @JsonProperty("callback_hash")
    public void setCallback_hash(String  callback_hash){
        this.callback_hash = callback_hash ;
        this.callback_hashDirtyFlag = true ;
    }

    /**
     * 获取 [CALLBACK_HASH]脏标记
     */
    @JsonIgnore
    public boolean getCallback_hashDirtyFlag(){
        return callback_hashDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_ID_TEXT]
     */
    @JsonProperty("payment_id_text")
    public String getPayment_id_text(){
        return payment_id_text ;
    }

    /**
     * 设置 [PAYMENT_ID_TEXT]
     */
    @JsonProperty("payment_id_text")
    public void setPayment_id_text(String  payment_id_text){
        this.payment_id_text = payment_id_text ;
        this.payment_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_id_textDirtyFlag(){
        return payment_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROVIDER]
     */
    @JsonProperty("provider")
    public String getProvider(){
        return provider ;
    }

    /**
     * 设置 [PROVIDER]
     */
    @JsonProperty("provider")
    public void setProvider(String  provider){
        this.provider = provider ;
        this.providerDirtyFlag = true ;
    }

    /**
     * 获取 [PROVIDER]脏标记
     */
    @JsonIgnore
    public boolean getProviderDirtyFlag(){
        return providerDirtyFlag ;
    }

    /**
     * 获取 [ACQUIRER_ID_TEXT]
     */
    @JsonProperty("acquirer_id_text")
    public String getAcquirer_id_text(){
        return acquirer_id_text ;
    }

    /**
     * 设置 [ACQUIRER_ID_TEXT]
     */
    @JsonProperty("acquirer_id_text")
    public void setAcquirer_id_text(String  acquirer_id_text){
        this.acquirer_id_text = acquirer_id_text ;
        this.acquirer_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACQUIRER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_id_textDirtyFlag(){
        return acquirer_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_COUNTRY_ID_TEXT]
     */
    @JsonProperty("partner_country_id_text")
    public String getPartner_country_id_text(){
        return partner_country_id_text ;
    }

    /**
     * 设置 [PARTNER_COUNTRY_ID_TEXT]
     */
    @JsonProperty("partner_country_id_text")
    public void setPartner_country_id_text(String  partner_country_id_text){
        this.partner_country_id_text = partner_country_id_text ;
        this.partner_country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_COUNTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_country_id_textDirtyFlag(){
        return partner_country_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_ID_TEXT]
     */
    @JsonProperty("payment_token_id_text")
    public String getPayment_token_id_text(){
        return payment_token_id_text ;
    }

    /**
     * 设置 [PAYMENT_TOKEN_ID_TEXT]
     */
    @JsonProperty("payment_token_id_text")
    public void setPayment_token_id_text(String  payment_token_id_text){
        this.payment_token_id_text = payment_token_id_text ;
        this.payment_token_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_id_textDirtyFlag(){
        return payment_token_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_COUNTRY_ID]
     */
    @JsonProperty("partner_country_id")
    public Integer getPartner_country_id(){
        return partner_country_id ;
    }

    /**
     * 设置 [PARTNER_COUNTRY_ID]
     */
    @JsonProperty("partner_country_id")
    public void setPartner_country_id(Integer  partner_country_id){
        this.partner_country_id = partner_country_id ;
        this.partner_country_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_country_idDirtyFlag(){
        return partner_country_idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_ID]
     */
    @JsonProperty("payment_token_id")
    public Integer getPayment_token_id(){
        return payment_token_id ;
    }

    /**
     * 设置 [PAYMENT_TOKEN_ID]
     */
    @JsonProperty("payment_token_id")
    public void setPayment_token_id(Integer  payment_token_id){
        this.payment_token_id = payment_token_id ;
        this.payment_token_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TOKEN_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_token_idDirtyFlag(){
        return payment_token_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_ID]
     */
    @JsonProperty("payment_id")
    public Integer getPayment_id(){
        return payment_id ;
    }

    /**
     * 设置 [PAYMENT_ID]
     */
    @JsonProperty("payment_id")
    public void setPayment_id(Integer  payment_id){
        this.payment_id = payment_id ;
        this.payment_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idDirtyFlag(){
        return payment_idDirtyFlag ;
    }

    /**
     * 获取 [ACQUIRER_ID]
     */
    @JsonProperty("acquirer_id")
    public Integer getAcquirer_id(){
        return acquirer_id ;
    }

    /**
     * 设置 [ACQUIRER_ID]
     */
    @JsonProperty("acquirer_id")
    public void setAcquirer_id(Integer  acquirer_id){
        this.acquirer_id = acquirer_id ;
        this.acquirer_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACQUIRER_ID]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_idDirtyFlag(){
        return acquirer_idDirtyFlag ;
    }



    public Payment_transaction toDO() {
        Payment_transaction srfdomain = new Payment_transaction();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getPartner_phoneDirtyFlag())
            srfdomain.setPartner_phone(partner_phone);
        if(getPartner_emailDirtyFlag())
            srfdomain.setPartner_email(partner_email);
        if(getCallback_methodDirtyFlag())
            srfdomain.setCallback_method(callback_method);
        if(getReturn_urlDirtyFlag())
            srfdomain.setReturn_url(return_url);
        if(getSale_order_ids_nbrDirtyFlag())
            srfdomain.setSale_order_ids_nbr(sale_order_ids_nbr);
        if(getAcquirer_referenceDirtyFlag())
            srfdomain.setAcquirer_reference(acquirer_reference);
        if(getPartner_langDirtyFlag())
            srfdomain.setPartner_lang(partner_lang);
        if(getPartner_zipDirtyFlag())
            srfdomain.setPartner_zip(partner_zip);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getCallback_model_idDirtyFlag())
            srfdomain.setCallback_model_id(callback_model_id);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getInvoice_idsDirtyFlag())
            srfdomain.setInvoice_ids(invoice_ids);
        if(getSale_order_idsDirtyFlag())
            srfdomain.setSale_order_ids(sale_order_ids);
        if(getFeesDirtyFlag())
            srfdomain.setFees(fees);
        if(getState_messageDirtyFlag())
            srfdomain.setState_message(state_message);
        if(getCallback_res_idDirtyFlag())
            srfdomain.setCallback_res_id(callback_res_id);
        if(getPartner_cityDirtyFlag())
            srfdomain.setPartner_city(partner_city);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getInvoice_ids_nbrDirtyFlag())
            srfdomain.setInvoice_ids_nbr(invoice_ids_nbr);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getHtml_3dsDirtyFlag())
            srfdomain.setHtml_3ds(html_3ds);
        if(getPartner_nameDirtyFlag())
            srfdomain.setPartner_name(partner_name);
        if(getReferenceDirtyFlag())
            srfdomain.setReference(reference);
        if(getPartner_addressDirtyFlag())
            srfdomain.setPartner_address(partner_address);
        if(getIs_processedDirtyFlag())
            srfdomain.setIs_processed(is_processed);
        if(getCallback_hashDirtyFlag())
            srfdomain.setCallback_hash(callback_hash);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getPayment_id_textDirtyFlag())
            srfdomain.setPayment_id_text(payment_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getProviderDirtyFlag())
            srfdomain.setProvider(provider);
        if(getAcquirer_id_textDirtyFlag())
            srfdomain.setAcquirer_id_text(acquirer_id_text);
        if(getPartner_country_id_textDirtyFlag())
            srfdomain.setPartner_country_id_text(partner_country_id_text);
        if(getPayment_token_id_textDirtyFlag())
            srfdomain.setPayment_token_id_text(payment_token_id_text);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPartner_country_idDirtyFlag())
            srfdomain.setPartner_country_id(partner_country_id);
        if(getPayment_token_idDirtyFlag())
            srfdomain.setPayment_token_id(payment_token_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPayment_idDirtyFlag())
            srfdomain.setPayment_id(payment_id);
        if(getAcquirer_idDirtyFlag())
            srfdomain.setAcquirer_id(acquirer_id);

        return srfdomain;
    }

    public void fromDO(Payment_transaction srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getPartner_phoneDirtyFlag())
            this.setPartner_phone(srfdomain.getPartner_phone());
        if(srfdomain.getPartner_emailDirtyFlag())
            this.setPartner_email(srfdomain.getPartner_email());
        if(srfdomain.getCallback_methodDirtyFlag())
            this.setCallback_method(srfdomain.getCallback_method());
        if(srfdomain.getReturn_urlDirtyFlag())
            this.setReturn_url(srfdomain.getReturn_url());
        if(srfdomain.getSale_order_ids_nbrDirtyFlag())
            this.setSale_order_ids_nbr(srfdomain.getSale_order_ids_nbr());
        if(srfdomain.getAcquirer_referenceDirtyFlag())
            this.setAcquirer_reference(srfdomain.getAcquirer_reference());
        if(srfdomain.getPartner_langDirtyFlag())
            this.setPartner_lang(srfdomain.getPartner_lang());
        if(srfdomain.getPartner_zipDirtyFlag())
            this.setPartner_zip(srfdomain.getPartner_zip());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getCallback_model_idDirtyFlag())
            this.setCallback_model_id(srfdomain.getCallback_model_id());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getInvoice_idsDirtyFlag())
            this.setInvoice_ids(srfdomain.getInvoice_ids());
        if(srfdomain.getSale_order_idsDirtyFlag())
            this.setSale_order_ids(srfdomain.getSale_order_ids());
        if(srfdomain.getFeesDirtyFlag())
            this.setFees(srfdomain.getFees());
        if(srfdomain.getState_messageDirtyFlag())
            this.setState_message(srfdomain.getState_message());
        if(srfdomain.getCallback_res_idDirtyFlag())
            this.setCallback_res_id(srfdomain.getCallback_res_id());
        if(srfdomain.getPartner_cityDirtyFlag())
            this.setPartner_city(srfdomain.getPartner_city());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getInvoice_ids_nbrDirtyFlag())
            this.setInvoice_ids_nbr(srfdomain.getInvoice_ids_nbr());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getHtml_3dsDirtyFlag())
            this.setHtml_3ds(srfdomain.getHtml_3ds());
        if(srfdomain.getPartner_nameDirtyFlag())
            this.setPartner_name(srfdomain.getPartner_name());
        if(srfdomain.getReferenceDirtyFlag())
            this.setReference(srfdomain.getReference());
        if(srfdomain.getPartner_addressDirtyFlag())
            this.setPartner_address(srfdomain.getPartner_address());
        if(srfdomain.getIs_processedDirtyFlag())
            this.setIs_processed(srfdomain.getIs_processed());
        if(srfdomain.getCallback_hashDirtyFlag())
            this.setCallback_hash(srfdomain.getCallback_hash());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getPayment_id_textDirtyFlag())
            this.setPayment_id_text(srfdomain.getPayment_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getProviderDirtyFlag())
            this.setProvider(srfdomain.getProvider());
        if(srfdomain.getAcquirer_id_textDirtyFlag())
            this.setAcquirer_id_text(srfdomain.getAcquirer_id_text());
        if(srfdomain.getPartner_country_id_textDirtyFlag())
            this.setPartner_country_id_text(srfdomain.getPartner_country_id_text());
        if(srfdomain.getPayment_token_id_textDirtyFlag())
            this.setPayment_token_id_text(srfdomain.getPayment_token_id_text());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPartner_country_idDirtyFlag())
            this.setPartner_country_id(srfdomain.getPartner_country_id());
        if(srfdomain.getPayment_token_idDirtyFlag())
            this.setPayment_token_id(srfdomain.getPayment_token_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPayment_idDirtyFlag())
            this.setPayment_id(srfdomain.getPayment_id());
        if(srfdomain.getAcquirer_idDirtyFlag())
            this.setAcquirer_id(srfdomain.getAcquirer_id());

    }

    public List<Payment_transactionDTO> fromDOPage(List<Payment_transaction> poPage)   {
        if(poPage == null)
            return null;
        List<Payment_transactionDTO> dtos=new ArrayList<Payment_transactionDTO>();
        for(Payment_transaction domain : poPage) {
            Payment_transactionDTO dto = new Payment_transactionDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

