package cn.ibizlab.odoo.service.odoo_payment.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_payment.dto.Payment_tokenDTO;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_tokenService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_tokenSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Payment_token" })
@RestController
@RequestMapping("")
public class Payment_tokenResource {

    @Autowired
    private IPayment_tokenService payment_tokenService;

    public IPayment_tokenService getPayment_tokenService() {
        return this.payment_tokenService;
    }

    @ApiOperation(value = "建立数据", tags = {"Payment_token" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_tokens")

    public ResponseEntity<Payment_tokenDTO> create(@RequestBody Payment_tokenDTO payment_tokendto) {
        Payment_tokenDTO dto = new Payment_tokenDTO();
        Payment_token domain = payment_tokendto.toDO();
		payment_tokenService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Payment_token" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_tokens/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Payment_tokenDTO> payment_tokendtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Payment_token" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_tokens/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_tokenDTO> payment_tokendtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Payment_token" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_tokens/{payment_token_id}")

    public ResponseEntity<Payment_tokenDTO> update(@PathVariable("payment_token_id") Integer payment_token_id, @RequestBody Payment_tokenDTO payment_tokendto) {
		Payment_token domain = payment_tokendto.toDO();
        domain.setId(payment_token_id);
		payment_tokenService.update(domain);
		Payment_tokenDTO dto = new Payment_tokenDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Payment_token" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_tokens/createBatch")
    public ResponseEntity<Boolean> createBatchPayment_token(@RequestBody List<Payment_tokenDTO> payment_tokendtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Payment_token" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_tokens/{payment_token_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("payment_token_id") Integer payment_token_id) {
        Payment_tokenDTO payment_tokendto = new Payment_tokenDTO();
		Payment_token domain = new Payment_token();
		payment_tokendto.setId(payment_token_id);
		domain.setId(payment_token_id);
        Boolean rst = payment_tokenService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Payment_token" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_tokens/{payment_token_id}")
    public ResponseEntity<Payment_tokenDTO> get(@PathVariable("payment_token_id") Integer payment_token_id) {
        Payment_tokenDTO dto = new Payment_tokenDTO();
        Payment_token domain = payment_tokenService.get(payment_token_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Payment_token" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_payment/payment_tokens/fetchdefault")
	public ResponseEntity<Page<Payment_tokenDTO>> fetchDefault(Payment_tokenSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Payment_tokenDTO> list = new ArrayList<Payment_tokenDTO>();
        
        Page<Payment_token> domains = payment_tokenService.searchDefault(context) ;
        for(Payment_token payment_token : domains.getContent()){
            Payment_tokenDTO dto = new Payment_tokenDTO();
            dto.fromDO(payment_token);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
