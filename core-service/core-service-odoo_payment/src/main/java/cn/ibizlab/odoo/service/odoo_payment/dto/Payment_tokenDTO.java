package cn.ibizlab.odoo.service.odoo_payment.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_payment.valuerule.anno.payment_token.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Payment_tokenDTO]
 */
public class Payment_tokenDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Payment_tokenIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Payment_tokenCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ACQUIRER_REF]
     *
     */
    @Payment_tokenAcquirer_refDefault(info = "默认规则")
    private String acquirer_ref;

    @JsonIgnore
    private boolean acquirer_refDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Payment_tokenActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [VERIFIED]
     *
     */
    @Payment_tokenVerifiedDefault(info = "默认规则")
    private String verified;

    @JsonIgnore
    private boolean verifiedDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Payment_token__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [SHORT_NAME]
     *
     */
    @Payment_tokenShort_nameDefault(info = "默认规则")
    private String short_name;

    @JsonIgnore
    private boolean short_nameDirtyFlag;

    /**
     * 属性 [PAYMENT_IDS]
     *
     */
    @Payment_tokenPayment_idsDefault(info = "默认规则")
    private String payment_ids;

    @JsonIgnore
    private boolean payment_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Payment_tokenNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Payment_tokenWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Payment_tokenDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Payment_tokenCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ACQUIRER_ID_TEXT]
     *
     */
    @Payment_tokenAcquirer_id_textDefault(info = "默认规则")
    private String acquirer_id_text;

    @JsonIgnore
    private boolean acquirer_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Payment_tokenPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Payment_tokenWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Payment_tokenWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Payment_tokenPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Payment_tokenCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [ACQUIRER_ID]
     *
     */
    @Payment_tokenAcquirer_idDefault(info = "默认规则")
    private Integer acquirer_id;

    @JsonIgnore
    private boolean acquirer_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ACQUIRER_REF]
     */
    @JsonProperty("acquirer_ref")
    public String getAcquirer_ref(){
        return acquirer_ref ;
    }

    /**
     * 设置 [ACQUIRER_REF]
     */
    @JsonProperty("acquirer_ref")
    public void setAcquirer_ref(String  acquirer_ref){
        this.acquirer_ref = acquirer_ref ;
        this.acquirer_refDirtyFlag = true ;
    }

    /**
     * 获取 [ACQUIRER_REF]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_refDirtyFlag(){
        return acquirer_refDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [VERIFIED]
     */
    @JsonProperty("verified")
    public String getVerified(){
        return verified ;
    }

    /**
     * 设置 [VERIFIED]
     */
    @JsonProperty("verified")
    public void setVerified(String  verified){
        this.verified = verified ;
        this.verifiedDirtyFlag = true ;
    }

    /**
     * 获取 [VERIFIED]脏标记
     */
    @JsonIgnore
    public boolean getVerifiedDirtyFlag(){
        return verifiedDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [SHORT_NAME]
     */
    @JsonProperty("short_name")
    public String getShort_name(){
        return short_name ;
    }

    /**
     * 设置 [SHORT_NAME]
     */
    @JsonProperty("short_name")
    public void setShort_name(String  short_name){
        this.short_name = short_name ;
        this.short_nameDirtyFlag = true ;
    }

    /**
     * 获取 [SHORT_NAME]脏标记
     */
    @JsonIgnore
    public boolean getShort_nameDirtyFlag(){
        return short_nameDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_IDS]
     */
    @JsonProperty("payment_ids")
    public String getPayment_ids(){
        return payment_ids ;
    }

    /**
     * 设置 [PAYMENT_IDS]
     */
    @JsonProperty("payment_ids")
    public void setPayment_ids(String  payment_ids){
        this.payment_ids = payment_ids ;
        this.payment_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idsDirtyFlag(){
        return payment_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ACQUIRER_ID_TEXT]
     */
    @JsonProperty("acquirer_id_text")
    public String getAcquirer_id_text(){
        return acquirer_id_text ;
    }

    /**
     * 设置 [ACQUIRER_ID_TEXT]
     */
    @JsonProperty("acquirer_id_text")
    public void setAcquirer_id_text(String  acquirer_id_text){
        this.acquirer_id_text = acquirer_id_text ;
        this.acquirer_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACQUIRER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_id_textDirtyFlag(){
        return acquirer_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [ACQUIRER_ID]
     */
    @JsonProperty("acquirer_id")
    public Integer getAcquirer_id(){
        return acquirer_id ;
    }

    /**
     * 设置 [ACQUIRER_ID]
     */
    @JsonProperty("acquirer_id")
    public void setAcquirer_id(Integer  acquirer_id){
        this.acquirer_id = acquirer_id ;
        this.acquirer_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACQUIRER_ID]脏标记
     */
    @JsonIgnore
    public boolean getAcquirer_idDirtyFlag(){
        return acquirer_idDirtyFlag ;
    }



    public Payment_token toDO() {
        Payment_token srfdomain = new Payment_token();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getAcquirer_refDirtyFlag())
            srfdomain.setAcquirer_ref(acquirer_ref);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getVerifiedDirtyFlag())
            srfdomain.setVerified(verified);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getShort_nameDirtyFlag())
            srfdomain.setShort_name(short_name);
        if(getPayment_idsDirtyFlag())
            srfdomain.setPayment_ids(payment_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getAcquirer_id_textDirtyFlag())
            srfdomain.setAcquirer_id_text(acquirer_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getAcquirer_idDirtyFlag())
            srfdomain.setAcquirer_id(acquirer_id);

        return srfdomain;
    }

    public void fromDO(Payment_token srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getAcquirer_refDirtyFlag())
            this.setAcquirer_ref(srfdomain.getAcquirer_ref());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getVerifiedDirtyFlag())
            this.setVerified(srfdomain.getVerified());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getShort_nameDirtyFlag())
            this.setShort_name(srfdomain.getShort_name());
        if(srfdomain.getPayment_idsDirtyFlag())
            this.setPayment_ids(srfdomain.getPayment_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getAcquirer_id_textDirtyFlag())
            this.setAcquirer_id_text(srfdomain.getAcquirer_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getAcquirer_idDirtyFlag())
            this.setAcquirer_id(srfdomain.getAcquirer_id());

    }

    public List<Payment_tokenDTO> fromDOPage(List<Payment_token> poPage)   {
        if(poPage == null)
            return null;
        List<Payment_tokenDTO> dtos=new ArrayList<Payment_tokenDTO>();
        for(Payment_token domain : poPage) {
            Payment_tokenDTO dto = new Payment_tokenDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

