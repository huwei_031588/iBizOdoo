package cn.ibizlab.odoo.service.odoo_utm.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_utm.dto.Utm_mixinDTO;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_mixinService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mixinSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Utm_mixin" })
@RestController
@RequestMapping("")
public class Utm_mixinResource {

    @Autowired
    private IUtm_mixinService utm_mixinService;

    public IUtm_mixinService getUtm_mixinService() {
        return this.utm_mixinService;
    }

    @ApiOperation(value = "更新数据", tags = {"Utm_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_mixins/{utm_mixin_id}")

    public ResponseEntity<Utm_mixinDTO> update(@PathVariable("utm_mixin_id") Integer utm_mixin_id, @RequestBody Utm_mixinDTO utm_mixindto) {
		Utm_mixin domain = utm_mixindto.toDO();
        domain.setId(utm_mixin_id);
		utm_mixinService.update(domain);
		Utm_mixinDTO dto = new Utm_mixinDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Utm_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_mixins/{utm_mixin_id}")
    public ResponseEntity<Utm_mixinDTO> get(@PathVariable("utm_mixin_id") Integer utm_mixin_id) {
        Utm_mixinDTO dto = new Utm_mixinDTO();
        Utm_mixin domain = utm_mixinService.get(utm_mixin_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Utm_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_mixins")

    public ResponseEntity<Utm_mixinDTO> create(@RequestBody Utm_mixinDTO utm_mixindto) {
        Utm_mixinDTO dto = new Utm_mixinDTO();
        Utm_mixin domain = utm_mixindto.toDO();
		utm_mixinService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Utm_mixin" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_mixins/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Utm_mixinDTO> utm_mixindtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Utm_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_mixins/{utm_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("utm_mixin_id") Integer utm_mixin_id) {
        Utm_mixinDTO utm_mixindto = new Utm_mixinDTO();
		Utm_mixin domain = new Utm_mixin();
		utm_mixindto.setId(utm_mixin_id);
		domain.setId(utm_mixin_id);
        Boolean rst = utm_mixinService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Utm_mixin" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_mixins/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_mixinDTO> utm_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Utm_mixin" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_mixins/createBatch")
    public ResponseEntity<Boolean> createBatchUtm_mixin(@RequestBody List<Utm_mixinDTO> utm_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Utm_mixin" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_utm/utm_mixins/fetchdefault")
	public ResponseEntity<Page<Utm_mixinDTO>> fetchDefault(Utm_mixinSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Utm_mixinDTO> list = new ArrayList<Utm_mixinDTO>();
        
        Page<Utm_mixin> domains = utm_mixinService.searchDefault(context) ;
        for(Utm_mixin utm_mixin : domains.getContent()){
            Utm_mixinDTO dto = new Utm_mixinDTO();
            dto.fromDO(utm_mixin);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
