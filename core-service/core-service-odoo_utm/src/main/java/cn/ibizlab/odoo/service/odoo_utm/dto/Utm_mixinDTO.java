package cn.ibizlab.odoo.service.odoo_utm.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_utm.valuerule.anno.utm_mixin.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_mixin;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Utm_mixinDTO]
 */
public class Utm_mixinDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Utm_mixinDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Utm_mixin__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Utm_mixinIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @Utm_mixinMedium_id_textDefault(info = "默认规则")
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @Utm_mixinSource_id_textDefault(info = "默认规则")
    private String source_id_text;

    @JsonIgnore
    private boolean source_id_textDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @Utm_mixinCampaign_id_textDefault(info = "默认规则")
    private String campaign_id_text;

    @JsonIgnore
    private boolean campaign_id_textDirtyFlag;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @Utm_mixinMedium_idDefault(info = "默认规则")
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @Utm_mixinSource_idDefault(info = "默认规则")
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @Utm_mixinCampaign_idDefault(info = "默认规则")
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return medium_id_text ;
    }

    /**
     * 设置 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return source_id_text ;
    }

    /**
     * 设置 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return source_id_textDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return campaign_id_text ;
    }

    /**
     * 设置 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return medium_id ;
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return medium_idDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return source_id ;
    }

    /**
     * 设置 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return source_idDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return campaign_id ;
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return campaign_idDirtyFlag ;
    }



    public Utm_mixin toDO() {
        Utm_mixin srfdomain = new Utm_mixin();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMedium_id_textDirtyFlag())
            srfdomain.setMedium_id_text(medium_id_text);
        if(getSource_id_textDirtyFlag())
            srfdomain.setSource_id_text(source_id_text);
        if(getCampaign_id_textDirtyFlag())
            srfdomain.setCampaign_id_text(campaign_id_text);
        if(getMedium_idDirtyFlag())
            srfdomain.setMedium_id(medium_id);
        if(getSource_idDirtyFlag())
            srfdomain.setSource_id(source_id);
        if(getCampaign_idDirtyFlag())
            srfdomain.setCampaign_id(campaign_id);

        return srfdomain;
    }

    public void fromDO(Utm_mixin srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMedium_id_textDirtyFlag())
            this.setMedium_id_text(srfdomain.getMedium_id_text());
        if(srfdomain.getSource_id_textDirtyFlag())
            this.setSource_id_text(srfdomain.getSource_id_text());
        if(srfdomain.getCampaign_id_textDirtyFlag())
            this.setCampaign_id_text(srfdomain.getCampaign_id_text());
        if(srfdomain.getMedium_idDirtyFlag())
            this.setMedium_id(srfdomain.getMedium_id());
        if(srfdomain.getSource_idDirtyFlag())
            this.setSource_id(srfdomain.getSource_id());
        if(srfdomain.getCampaign_idDirtyFlag())
            this.setCampaign_id(srfdomain.getCampaign_id());

    }

    public List<Utm_mixinDTO> fromDOPage(List<Utm_mixin> poPage)   {
        if(poPage == null)
            return null;
        List<Utm_mixinDTO> dtos=new ArrayList<Utm_mixinDTO>();
        for(Utm_mixin domain : poPage) {
            Utm_mixinDTO dto = new Utm_mixinDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

