package cn.ibizlab.odoo.service.odoo_utm.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_utm.dto.Utm_mediumDTO;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_mediumService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mediumSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Utm_medium" })
@RestController
@RequestMapping("")
public class Utm_mediumResource {

    @Autowired
    private IUtm_mediumService utm_mediumService;

    public IUtm_mediumService getUtm_mediumService() {
        return this.utm_mediumService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Utm_medium" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_media/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Utm_mediumDTO> utm_mediumdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Utm_medium" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_media/{utm_medium_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("utm_medium_id") Integer utm_medium_id) {
        Utm_mediumDTO utm_mediumdto = new Utm_mediumDTO();
		Utm_medium domain = new Utm_medium();
		utm_mediumdto.setId(utm_medium_id);
		domain.setId(utm_medium_id);
        Boolean rst = utm_mediumService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Utm_medium" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_media")

    public ResponseEntity<Utm_mediumDTO> create(@RequestBody Utm_mediumDTO utm_mediumdto) {
        Utm_mediumDTO dto = new Utm_mediumDTO();
        Utm_medium domain = utm_mediumdto.toDO();
		utm_mediumService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Utm_medium" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_media/{utm_medium_id}")

    public ResponseEntity<Utm_mediumDTO> update(@PathVariable("utm_medium_id") Integer utm_medium_id, @RequestBody Utm_mediumDTO utm_mediumdto) {
		Utm_medium domain = utm_mediumdto.toDO();
        domain.setId(utm_medium_id);
		utm_mediumService.update(domain);
		Utm_mediumDTO dto = new Utm_mediumDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Utm_medium" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_media/{utm_medium_id}")
    public ResponseEntity<Utm_mediumDTO> get(@PathVariable("utm_medium_id") Integer utm_medium_id) {
        Utm_mediumDTO dto = new Utm_mediumDTO();
        Utm_medium domain = utm_mediumService.get(utm_medium_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Utm_medium" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_media/createBatch")
    public ResponseEntity<Boolean> createBatchUtm_medium(@RequestBody List<Utm_mediumDTO> utm_mediumdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Utm_medium" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_media/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_mediumDTO> utm_mediumdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Utm_medium" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_utm/utm_media/fetchdefault")
	public ResponseEntity<Page<Utm_mediumDTO>> fetchDefault(Utm_mediumSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Utm_mediumDTO> list = new ArrayList<Utm_mediumDTO>();
        
        Page<Utm_medium> domains = utm_mediumService.searchDefault(context) ;
        for(Utm_medium utm_medium : domains.getContent()){
            Utm_mediumDTO dto = new Utm_mediumDTO();
            dto.fromDO(utm_medium);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
