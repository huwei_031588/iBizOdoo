package cn.ibizlab.odoo.service.odoo_resource.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_resource.valuerule.anno.resource_resource.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Resource_resourceDTO]
 */
public class Resource_resourceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Resource_resource__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Resource_resourceIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Resource_resourceNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [TIME_EFFICIENCY]
     *
     */
    @Resource_resourceTime_efficiencyDefault(info = "默认规则")
    private Double time_efficiency;

    @JsonIgnore
    private boolean time_efficiencyDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Resource_resourceCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Resource_resourceActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [RESOURCE_TYPE]
     *
     */
    @Resource_resourceResource_typeDefault(info = "默认规则")
    private String resource_type;

    @JsonIgnore
    private boolean resource_typeDirtyFlag;

    /**
     * 属性 [TZ]
     *
     */
    @Resource_resourceTzDefault(info = "默认规则")
    private String tz;

    @JsonIgnore
    private boolean tzDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Resource_resourceWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Resource_resourceDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CALENDAR_ID_TEXT]
     *
     */
    @Resource_resourceCalendar_id_textDefault(info = "默认规则")
    private String calendar_id_text;

    @JsonIgnore
    private boolean calendar_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Resource_resourceWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Resource_resourceCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Resource_resourceUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Resource_resourceCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Resource_resourceCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [CALENDAR_ID]
     *
     */
    @Resource_resourceCalendar_idDefault(info = "默认规则")
    private Integer calendar_id;

    @JsonIgnore
    private boolean calendar_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Resource_resourceCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Resource_resourceUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Resource_resourceWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [TIME_EFFICIENCY]
     */
    @JsonProperty("time_efficiency")
    public Double getTime_efficiency(){
        return time_efficiency ;
    }

    /**
     * 设置 [TIME_EFFICIENCY]
     */
    @JsonProperty("time_efficiency")
    public void setTime_efficiency(Double  time_efficiency){
        this.time_efficiency = time_efficiency ;
        this.time_efficiencyDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_EFFICIENCY]脏标记
     */
    @JsonIgnore
    public boolean getTime_efficiencyDirtyFlag(){
        return time_efficiencyDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_TYPE]
     */
    @JsonProperty("resource_type")
    public String getResource_type(){
        return resource_type ;
    }

    /**
     * 设置 [RESOURCE_TYPE]
     */
    @JsonProperty("resource_type")
    public void setResource_type(String  resource_type){
        this.resource_type = resource_type ;
        this.resource_typeDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getResource_typeDirtyFlag(){
        return resource_typeDirtyFlag ;
    }

    /**
     * 获取 [TZ]
     */
    @JsonProperty("tz")
    public String getTz(){
        return tz ;
    }

    /**
     * 设置 [TZ]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

    /**
     * 获取 [TZ]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return tzDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CALENDAR_ID_TEXT]
     */
    @JsonProperty("calendar_id_text")
    public String getCalendar_id_text(){
        return calendar_id_text ;
    }

    /**
     * 设置 [CALENDAR_ID_TEXT]
     */
    @JsonProperty("calendar_id_text")
    public void setCalendar_id_text(String  calendar_id_text){
        this.calendar_id_text = calendar_id_text ;
        this.calendar_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CALENDAR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_id_textDirtyFlag(){
        return calendar_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [CALENDAR_ID]
     */
    @JsonProperty("calendar_id")
    public Integer getCalendar_id(){
        return calendar_id ;
    }

    /**
     * 设置 [CALENDAR_ID]
     */
    @JsonProperty("calendar_id")
    public void setCalendar_id(Integer  calendar_id){
        this.calendar_id = calendar_id ;
        this.calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_idDirtyFlag(){
        return calendar_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Resource_resource toDO() {
        Resource_resource srfdomain = new Resource_resource();
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getTime_efficiencyDirtyFlag())
            srfdomain.setTime_efficiency(time_efficiency);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getResource_typeDirtyFlag())
            srfdomain.setResource_type(resource_type);
        if(getTzDirtyFlag())
            srfdomain.setTz(tz);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCalendar_id_textDirtyFlag())
            srfdomain.setCalendar_id_text(calendar_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCalendar_idDirtyFlag())
            srfdomain.setCalendar_id(calendar_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Resource_resource srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getTime_efficiencyDirtyFlag())
            this.setTime_efficiency(srfdomain.getTime_efficiency());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getResource_typeDirtyFlag())
            this.setResource_type(srfdomain.getResource_type());
        if(srfdomain.getTzDirtyFlag())
            this.setTz(srfdomain.getTz());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCalendar_id_textDirtyFlag())
            this.setCalendar_id_text(srfdomain.getCalendar_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCalendar_idDirtyFlag())
            this.setCalendar_id(srfdomain.getCalendar_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Resource_resourceDTO> fromDOPage(List<Resource_resource> poPage)   {
        if(poPage == null)
            return null;
        List<Resource_resourceDTO> dtos=new ArrayList<Resource_resourceDTO>();
        for(Resource_resource domain : poPage) {
            Resource_resourceDTO dto = new Resource_resourceDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

