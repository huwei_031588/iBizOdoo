package cn.ibizlab.odoo.service.odoo_resource.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_resource.dto.Resource_mixinDTO;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_mixinService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_mixinSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Resource_mixin" })
@RestController
@RequestMapping("")
public class Resource_mixinResource {

    @Autowired
    private IResource_mixinService resource_mixinService;

    public IResource_mixinService getResource_mixinService() {
        return this.resource_mixinService;
    }

    @ApiOperation(value = "更新数据", tags = {"Resource_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_mixins/{resource_mixin_id}")

    public ResponseEntity<Resource_mixinDTO> update(@PathVariable("resource_mixin_id") Integer resource_mixin_id, @RequestBody Resource_mixinDTO resource_mixindto) {
		Resource_mixin domain = resource_mixindto.toDO();
        domain.setId(resource_mixin_id);
		resource_mixinService.update(domain);
		Resource_mixinDTO dto = new Resource_mixinDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Resource_mixin" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_mixins/createBatch")
    public ResponseEntity<Boolean> createBatchResource_mixin(@RequestBody List<Resource_mixinDTO> resource_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Resource_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_mixins/{resource_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_mixin_id") Integer resource_mixin_id) {
        Resource_mixinDTO resource_mixindto = new Resource_mixinDTO();
		Resource_mixin domain = new Resource_mixin();
		resource_mixindto.setId(resource_mixin_id);
		domain.setId(resource_mixin_id);
        Boolean rst = resource_mixinService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Resource_mixin" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_mixins/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Resource_mixinDTO> resource_mixindtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Resource_mixin" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_mixins/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_mixinDTO> resource_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Resource_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_mixins/{resource_mixin_id}")
    public ResponseEntity<Resource_mixinDTO> get(@PathVariable("resource_mixin_id") Integer resource_mixin_id) {
        Resource_mixinDTO dto = new Resource_mixinDTO();
        Resource_mixin domain = resource_mixinService.get(resource_mixin_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Resource_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_mixins")

    public ResponseEntity<Resource_mixinDTO> create(@RequestBody Resource_mixinDTO resource_mixindto) {
        Resource_mixinDTO dto = new Resource_mixinDTO();
        Resource_mixin domain = resource_mixindto.toDO();
		resource_mixinService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Resource_mixin" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_resource/resource_mixins/fetchdefault")
	public ResponseEntity<Page<Resource_mixinDTO>> fetchDefault(Resource_mixinSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Resource_mixinDTO> list = new ArrayList<Resource_mixinDTO>();
        
        Page<Resource_mixin> domains = resource_mixinService.searchDefault(context) ;
        for(Resource_mixin resource_mixin : domains.getContent()){
            Resource_mixinDTO dto = new Resource_mixinDTO();
            dto.fromDO(resource_mixin);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
