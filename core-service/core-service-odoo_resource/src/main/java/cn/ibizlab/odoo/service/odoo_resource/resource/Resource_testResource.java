package cn.ibizlab.odoo.service.odoo_resource.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_resource.dto.Resource_testDTO;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_test;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_testService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_testSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Resource_test" })
@RestController
@RequestMapping("")
public class Resource_testResource {

    @Autowired
    private IResource_testService resource_testService;

    public IResource_testService getResource_testService() {
        return this.resource_testService;
    }

    @ApiOperation(value = "删除数据", tags = {"Resource_test" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_tests/{resource_test_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_test_id") Integer resource_test_id) {
        Resource_testDTO resource_testdto = new Resource_testDTO();
		Resource_test domain = new Resource_test();
		resource_testdto.setId(resource_test_id);
		domain.setId(resource_test_id);
        Boolean rst = resource_testService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Resource_test" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_tests/{resource_test_id}")

    public ResponseEntity<Resource_testDTO> update(@PathVariable("resource_test_id") Integer resource_test_id, @RequestBody Resource_testDTO resource_testdto) {
		Resource_test domain = resource_testdto.toDO();
        domain.setId(resource_test_id);
		resource_testService.update(domain);
		Resource_testDTO dto = new Resource_testDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Resource_test" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_tests/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Resource_testDTO> resource_testdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Resource_test" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_tests")

    public ResponseEntity<Resource_testDTO> create(@RequestBody Resource_testDTO resource_testdto) {
        Resource_testDTO dto = new Resource_testDTO();
        Resource_test domain = resource_testdto.toDO();
		resource_testService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Resource_test" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_tests/createBatch")
    public ResponseEntity<Boolean> createBatchResource_test(@RequestBody List<Resource_testDTO> resource_testdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Resource_test" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_tests/{resource_test_id}")
    public ResponseEntity<Resource_testDTO> get(@PathVariable("resource_test_id") Integer resource_test_id) {
        Resource_testDTO dto = new Resource_testDTO();
        Resource_test domain = resource_testService.get(resource_test_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Resource_test" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_tests/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_testDTO> resource_testdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Resource_test" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_resource/resource_tests/fetchdefault")
	public ResponseEntity<Page<Resource_testDTO>> fetchDefault(Resource_testSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Resource_testDTO> list = new ArrayList<Resource_testDTO>();
        
        Page<Resource_test> domains = resource_testService.searchDefault(context) ;
        for(Resource_test resource_test : domains.getContent()){
            Resource_testDTO dto = new Resource_testDTO();
            dto.fromDO(resource_test);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
