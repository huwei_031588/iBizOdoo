package cn.ibizlab.odoo.service.odoo_resource.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_resource.valuerule.anno.resource_calendar.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Resource_calendarDTO]
 */
public class Resource_calendarDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TZ]
     *
     */
    @Resource_calendarTzDefault(info = "默认规则")
    private String tz;

    @JsonIgnore
    private boolean tzDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Resource_calendarNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Resource_calendar__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Resource_calendarWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Resource_calendarIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [GLOBAL_LEAVE_IDS]
     *
     */
    @Resource_calendarGlobal_leave_idsDefault(info = "默认规则")
    private String global_leave_ids;

    @JsonIgnore
    private boolean global_leave_idsDirtyFlag;

    /**
     * 属性 [ATTENDANCE_IDS]
     *
     */
    @Resource_calendarAttendance_idsDefault(info = "默认规则")
    private String attendance_ids;

    @JsonIgnore
    private boolean attendance_idsDirtyFlag;

    /**
     * 属性 [HOURS_PER_DAY]
     *
     */
    @Resource_calendarHours_per_dayDefault(info = "默认规则")
    private Double hours_per_day;

    @JsonIgnore
    private boolean hours_per_dayDirtyFlag;

    /**
     * 属性 [LEAVE_IDS]
     *
     */
    @Resource_calendarLeave_idsDefault(info = "默认规则")
    private String leave_ids;

    @JsonIgnore
    private boolean leave_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Resource_calendarDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Resource_calendarCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Resource_calendarCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Resource_calendarWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Resource_calendarCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Resource_calendarCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Resource_calendarCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Resource_calendarWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [TZ]
     */
    @JsonProperty("tz")
    public String getTz(){
        return tz ;
    }

    /**
     * 设置 [TZ]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

    /**
     * 获取 [TZ]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return tzDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [GLOBAL_LEAVE_IDS]
     */
    @JsonProperty("global_leave_ids")
    public String getGlobal_leave_ids(){
        return global_leave_ids ;
    }

    /**
     * 设置 [GLOBAL_LEAVE_IDS]
     */
    @JsonProperty("global_leave_ids")
    public void setGlobal_leave_ids(String  global_leave_ids){
        this.global_leave_ids = global_leave_ids ;
        this.global_leave_idsDirtyFlag = true ;
    }

    /**
     * 获取 [GLOBAL_LEAVE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getGlobal_leave_idsDirtyFlag(){
        return global_leave_idsDirtyFlag ;
    }

    /**
     * 获取 [ATTENDANCE_IDS]
     */
    @JsonProperty("attendance_ids")
    public String getAttendance_ids(){
        return attendance_ids ;
    }

    /**
     * 设置 [ATTENDANCE_IDS]
     */
    @JsonProperty("attendance_ids")
    public void setAttendance_ids(String  attendance_ids){
        this.attendance_ids = attendance_ids ;
        this.attendance_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTENDANCE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttendance_idsDirtyFlag(){
        return attendance_idsDirtyFlag ;
    }

    /**
     * 获取 [HOURS_PER_DAY]
     */
    @JsonProperty("hours_per_day")
    public Double getHours_per_day(){
        return hours_per_day ;
    }

    /**
     * 设置 [HOURS_PER_DAY]
     */
    @JsonProperty("hours_per_day")
    public void setHours_per_day(Double  hours_per_day){
        this.hours_per_day = hours_per_day ;
        this.hours_per_dayDirtyFlag = true ;
    }

    /**
     * 获取 [HOURS_PER_DAY]脏标记
     */
    @JsonIgnore
    public boolean getHours_per_dayDirtyFlag(){
        return hours_per_dayDirtyFlag ;
    }

    /**
     * 获取 [LEAVE_IDS]
     */
    @JsonProperty("leave_ids")
    public String getLeave_ids(){
        return leave_ids ;
    }

    /**
     * 设置 [LEAVE_IDS]
     */
    @JsonProperty("leave_ids")
    public void setLeave_ids(String  leave_ids){
        this.leave_ids = leave_ids ;
        this.leave_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LEAVE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLeave_idsDirtyFlag(){
        return leave_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Resource_calendar toDO() {
        Resource_calendar srfdomain = new Resource_calendar();
        if(getTzDirtyFlag())
            srfdomain.setTz(tz);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getGlobal_leave_idsDirtyFlag())
            srfdomain.setGlobal_leave_ids(global_leave_ids);
        if(getAttendance_idsDirtyFlag())
            srfdomain.setAttendance_ids(attendance_ids);
        if(getHours_per_dayDirtyFlag())
            srfdomain.setHours_per_day(hours_per_day);
        if(getLeave_idsDirtyFlag())
            srfdomain.setLeave_ids(leave_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Resource_calendar srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getTzDirtyFlag())
            this.setTz(srfdomain.getTz());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getGlobal_leave_idsDirtyFlag())
            this.setGlobal_leave_ids(srfdomain.getGlobal_leave_ids());
        if(srfdomain.getAttendance_idsDirtyFlag())
            this.setAttendance_ids(srfdomain.getAttendance_ids());
        if(srfdomain.getHours_per_dayDirtyFlag())
            this.setHours_per_day(srfdomain.getHours_per_day());
        if(srfdomain.getLeave_idsDirtyFlag())
            this.setLeave_ids(srfdomain.getLeave_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Resource_calendarDTO> fromDOPage(List<Resource_calendar> poPage)   {
        if(poPage == null)
            return null;
        List<Resource_calendarDTO> dtos=new ArrayList<Resource_calendarDTO>();
        for(Resource_calendar domain : poPage) {
            Resource_calendarDTO dto = new Resource_calendarDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

