package cn.ibizlab.odoo.service.odoo_resource.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_resource.dto.Resource_calendar_attendanceDTO;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendar_attendanceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Resource_calendar_attendance" })
@RestController
@RequestMapping("")
public class Resource_calendar_attendanceResource {

    @Autowired
    private IResource_calendar_attendanceService resource_calendar_attendanceService;

    public IResource_calendar_attendanceService getResource_calendar_attendanceService() {
        return this.resource_calendar_attendanceService;
    }

    @ApiOperation(value = "删除数据", tags = {"Resource_calendar_attendance" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendar_attendances/{resource_calendar_attendance_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_calendar_attendance_id") Integer resource_calendar_attendance_id) {
        Resource_calendar_attendanceDTO resource_calendar_attendancedto = new Resource_calendar_attendanceDTO();
		Resource_calendar_attendance domain = new Resource_calendar_attendance();
		resource_calendar_attendancedto.setId(resource_calendar_attendance_id);
		domain.setId(resource_calendar_attendance_id);
        Boolean rst = resource_calendar_attendanceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Resource_calendar_attendance" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendar_attendances/{resource_calendar_attendance_id}")
    public ResponseEntity<Resource_calendar_attendanceDTO> get(@PathVariable("resource_calendar_attendance_id") Integer resource_calendar_attendance_id) {
        Resource_calendar_attendanceDTO dto = new Resource_calendar_attendanceDTO();
        Resource_calendar_attendance domain = resource_calendar_attendanceService.get(resource_calendar_attendance_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Resource_calendar_attendance" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendar_attendances/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Resource_calendar_attendanceDTO> resource_calendar_attendancedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Resource_calendar_attendance" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendar_attendances/{resource_calendar_attendance_id}")

    public ResponseEntity<Resource_calendar_attendanceDTO> update(@PathVariable("resource_calendar_attendance_id") Integer resource_calendar_attendance_id, @RequestBody Resource_calendar_attendanceDTO resource_calendar_attendancedto) {
		Resource_calendar_attendance domain = resource_calendar_attendancedto.toDO();
        domain.setId(resource_calendar_attendance_id);
		resource_calendar_attendanceService.update(domain);
		Resource_calendar_attendanceDTO dto = new Resource_calendar_attendanceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Resource_calendar_attendance" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendar_attendances/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_calendar_attendanceDTO> resource_calendar_attendancedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Resource_calendar_attendance" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendar_attendances")

    public ResponseEntity<Resource_calendar_attendanceDTO> create(@RequestBody Resource_calendar_attendanceDTO resource_calendar_attendancedto) {
        Resource_calendar_attendanceDTO dto = new Resource_calendar_attendanceDTO();
        Resource_calendar_attendance domain = resource_calendar_attendancedto.toDO();
		resource_calendar_attendanceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Resource_calendar_attendance" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendar_attendances/createBatch")
    public ResponseEntity<Boolean> createBatchResource_calendar_attendance(@RequestBody List<Resource_calendar_attendanceDTO> resource_calendar_attendancedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Resource_calendar_attendance" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_resource/resource_calendar_attendances/fetchdefault")
	public ResponseEntity<Page<Resource_calendar_attendanceDTO>> fetchDefault(Resource_calendar_attendanceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Resource_calendar_attendanceDTO> list = new ArrayList<Resource_calendar_attendanceDTO>();
        
        Page<Resource_calendar_attendance> domains = resource_calendar_attendanceService.searchDefault(context) ;
        for(Resource_calendar_attendance resource_calendar_attendance : domains.getContent()){
            Resource_calendar_attendanceDTO dto = new Resource_calendar_attendanceDTO();
            dto.fromDO(resource_calendar_attendance);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
