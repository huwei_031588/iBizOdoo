package cn.ibizlab.odoo.service.odoo_resource.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_resource.valuerule.anno.resource_mixin.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_mixin;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Resource_mixinDTO]
 */
public class Resource_mixinDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Resource_mixin__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Resource_mixinDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Resource_mixinIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [TZ]
     *
     */
    @Resource_mixinTzDefault(info = "默认规则")
    private String tz;

    @JsonIgnore
    private boolean tzDirtyFlag;

    /**
     * 属性 [RESOURCE_ID_TEXT]
     *
     */
    @Resource_mixinResource_id_textDefault(info = "默认规则")
    private String resource_id_text;

    @JsonIgnore
    private boolean resource_id_textDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID_TEXT]
     *
     */
    @Resource_mixinResource_calendar_id_textDefault(info = "默认规则")
    private String resource_calendar_id_text;

    @JsonIgnore
    private boolean resource_calendar_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Resource_mixinCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @Resource_mixinResource_calendar_idDefault(info = "默认规则")
    private Integer resource_calendar_id;

    @JsonIgnore
    private boolean resource_calendar_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Resource_mixinCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [RESOURCE_ID]
     *
     */
    @Resource_mixinResource_idDefault(info = "默认规则")
    private Integer resource_id;

    @JsonIgnore
    private boolean resource_idDirtyFlag;


    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [TZ]
     */
    @JsonProperty("tz")
    public String getTz(){
        return tz ;
    }

    /**
     * 设置 [TZ]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

    /**
     * 获取 [TZ]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return tzDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_ID_TEXT]
     */
    @JsonProperty("resource_id_text")
    public String getResource_id_text(){
        return resource_id_text ;
    }

    /**
     * 设置 [RESOURCE_ID_TEXT]
     */
    @JsonProperty("resource_id_text")
    public void setResource_id_text(String  resource_id_text){
        this.resource_id_text = resource_id_text ;
        this.resource_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getResource_id_textDirtyFlag(){
        return resource_id_textDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("resource_calendar_id_text")
    public String getResource_calendar_id_text(){
        return resource_calendar_id_text ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("resource_calendar_id_text")
    public void setResource_calendar_id_text(String  resource_calendar_id_text){
        this.resource_calendar_id_text = resource_calendar_id_text ;
        this.resource_calendar_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_id_textDirtyFlag(){
        return resource_calendar_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return resource_calendar_id ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return resource_calendar_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_ID]
     */
    @JsonProperty("resource_id")
    public Integer getResource_id(){
        return resource_id ;
    }

    /**
     * 设置 [RESOURCE_ID]
     */
    @JsonProperty("resource_id")
    public void setResource_id(Integer  resource_id){
        this.resource_id = resource_id ;
        this.resource_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_idDirtyFlag(){
        return resource_idDirtyFlag ;
    }



    public Resource_mixin toDO() {
        Resource_mixin srfdomain = new Resource_mixin();
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getTzDirtyFlag())
            srfdomain.setTz(tz);
        if(getResource_id_textDirtyFlag())
            srfdomain.setResource_id_text(resource_id_text);
        if(getResource_calendar_id_textDirtyFlag())
            srfdomain.setResource_calendar_id_text(resource_calendar_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getResource_calendar_idDirtyFlag())
            srfdomain.setResource_calendar_id(resource_calendar_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getResource_idDirtyFlag())
            srfdomain.setResource_id(resource_id);

        return srfdomain;
    }

    public void fromDO(Resource_mixin srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getTzDirtyFlag())
            this.setTz(srfdomain.getTz());
        if(srfdomain.getResource_id_textDirtyFlag())
            this.setResource_id_text(srfdomain.getResource_id_text());
        if(srfdomain.getResource_calendar_id_textDirtyFlag())
            this.setResource_calendar_id_text(srfdomain.getResource_calendar_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getResource_calendar_idDirtyFlag())
            this.setResource_calendar_id(srfdomain.getResource_calendar_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getResource_idDirtyFlag())
            this.setResource_id(srfdomain.getResource_id());

    }

    public List<Resource_mixinDTO> fromDOPage(List<Resource_mixin> poPage)   {
        if(poPage == null)
            return null;
        List<Resource_mixinDTO> dtos=new ArrayList<Resource_mixinDTO>();
        for(Resource_mixin domain : poPage) {
            Resource_mixinDTO dto = new Resource_mixinDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

