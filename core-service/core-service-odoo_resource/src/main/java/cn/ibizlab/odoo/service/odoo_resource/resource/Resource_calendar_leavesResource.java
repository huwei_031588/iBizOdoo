package cn.ibizlab.odoo.service.odoo_resource.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_resource.dto.Resource_calendar_leavesDTO;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_leaves;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendar_leavesService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Resource_calendar_leaves" })
@RestController
@RequestMapping("")
public class Resource_calendar_leavesResource {

    @Autowired
    private IResource_calendar_leavesService resource_calendar_leavesService;

    public IResource_calendar_leavesService getResource_calendar_leavesService() {
        return this.resource_calendar_leavesService;
    }

    @ApiOperation(value = "建立数据", tags = {"Resource_calendar_leaves" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendar_leaves")

    public ResponseEntity<Resource_calendar_leavesDTO> create(@RequestBody Resource_calendar_leavesDTO resource_calendar_leavesdto) {
        Resource_calendar_leavesDTO dto = new Resource_calendar_leavesDTO();
        Resource_calendar_leaves domain = resource_calendar_leavesdto.toDO();
		resource_calendar_leavesService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Resource_calendar_leaves" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendar_leaves/createBatch")
    public ResponseEntity<Boolean> createBatchResource_calendar_leaves(@RequestBody List<Resource_calendar_leavesDTO> resource_calendar_leavesdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Resource_calendar_leaves" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendar_leaves/{resource_calendar_leaves_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_calendar_leaves_id") Integer resource_calendar_leaves_id) {
        Resource_calendar_leavesDTO resource_calendar_leavesdto = new Resource_calendar_leavesDTO();
		Resource_calendar_leaves domain = new Resource_calendar_leaves();
		resource_calendar_leavesdto.setId(resource_calendar_leaves_id);
		domain.setId(resource_calendar_leaves_id);
        Boolean rst = resource_calendar_leavesService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Resource_calendar_leaves" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendar_leaves/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_calendar_leavesDTO> resource_calendar_leavesdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Resource_calendar_leaves" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendar_leaves/{resource_calendar_leaves_id}")

    public ResponseEntity<Resource_calendar_leavesDTO> update(@PathVariable("resource_calendar_leaves_id") Integer resource_calendar_leaves_id, @RequestBody Resource_calendar_leavesDTO resource_calendar_leavesdto) {
		Resource_calendar_leaves domain = resource_calendar_leavesdto.toDO();
        domain.setId(resource_calendar_leaves_id);
		resource_calendar_leavesService.update(domain);
		Resource_calendar_leavesDTO dto = new Resource_calendar_leavesDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Resource_calendar_leaves" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendar_leaves/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Resource_calendar_leavesDTO> resource_calendar_leavesdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Resource_calendar_leaves" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendar_leaves/{resource_calendar_leaves_id}")
    public ResponseEntity<Resource_calendar_leavesDTO> get(@PathVariable("resource_calendar_leaves_id") Integer resource_calendar_leaves_id) {
        Resource_calendar_leavesDTO dto = new Resource_calendar_leavesDTO();
        Resource_calendar_leaves domain = resource_calendar_leavesService.get(resource_calendar_leaves_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Resource_calendar_leaves" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_resource/resource_calendar_leaves/fetchdefault")
	public ResponseEntity<Page<Resource_calendar_leavesDTO>> fetchDefault(Resource_calendar_leavesSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Resource_calendar_leavesDTO> list = new ArrayList<Resource_calendar_leavesDTO>();
        
        Page<Resource_calendar_leaves> domains = resource_calendar_leavesService.searchDefault(context) ;
        for(Resource_calendar_leaves resource_calendar_leaves : domains.getContent()){
            Resource_calendar_leavesDTO dto = new Resource_calendar_leavesDTO();
            dto.fromDO(resource_calendar_leaves);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
