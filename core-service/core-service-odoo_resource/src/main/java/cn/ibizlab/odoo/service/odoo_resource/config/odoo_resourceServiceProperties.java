package cn.ibizlab.odoo.service.odoo_resource.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.resource")
@Data
public class odoo_resourceServiceProperties {

	private boolean enabled;

	private boolean auth;


}