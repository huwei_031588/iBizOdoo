package cn.ibizlab.odoo.service.odoo_resource.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_resource.valuerule.anno.resource_calendar_leaves.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_leaves;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Resource_calendar_leavesDTO]
 */
public class Resource_calendar_leavesDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Resource_calendar_leaves__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Resource_calendar_leavesDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Resource_calendar_leavesCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Resource_calendar_leavesWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Resource_calendar_leavesNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [TIME_TYPE]
     *
     */
    @Resource_calendar_leavesTime_typeDefault(info = "默认规则")
    private String time_type;

    @JsonIgnore
    private boolean time_typeDirtyFlag;

    /**
     * 属性 [DATE_TO]
     *
     */
    @Resource_calendar_leavesDate_toDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_to;

    @JsonIgnore
    private boolean date_toDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Resource_calendar_leavesIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DATE_FROM]
     *
     */
    @Resource_calendar_leavesDate_fromDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_from;

    @JsonIgnore
    private boolean date_fromDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Resource_calendar_leavesCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CALENDAR_ID_TEXT]
     *
     */
    @Resource_calendar_leavesCalendar_id_textDefault(info = "默认规则")
    private String calendar_id_text;

    @JsonIgnore
    private boolean calendar_id_textDirtyFlag;

    /**
     * 属性 [HOLIDAY_ID_TEXT]
     *
     */
    @Resource_calendar_leavesHoliday_id_textDefault(info = "默认规则")
    private String holiday_id_text;

    @JsonIgnore
    private boolean holiday_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Resource_calendar_leavesWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Resource_calendar_leavesCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [RESOURCE_ID_TEXT]
     *
     */
    @Resource_calendar_leavesResource_id_textDefault(info = "默认规则")
    private String resource_id_text;

    @JsonIgnore
    private boolean resource_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Resource_calendar_leavesWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Resource_calendar_leavesCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Resource_calendar_leavesCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [HOLIDAY_ID]
     *
     */
    @Resource_calendar_leavesHoliday_idDefault(info = "默认规则")
    private Integer holiday_id;

    @JsonIgnore
    private boolean holiday_idDirtyFlag;

    /**
     * 属性 [CALENDAR_ID]
     *
     */
    @Resource_calendar_leavesCalendar_idDefault(info = "默认规则")
    private Integer calendar_id;

    @JsonIgnore
    private boolean calendar_idDirtyFlag;

    /**
     * 属性 [RESOURCE_ID]
     *
     */
    @Resource_calendar_leavesResource_idDefault(info = "默认规则")
    private Integer resource_id;

    @JsonIgnore
    private boolean resource_idDirtyFlag;


    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [TIME_TYPE]
     */
    @JsonProperty("time_type")
    public String getTime_type(){
        return time_type ;
    }

    /**
     * 设置 [TIME_TYPE]
     */
    @JsonProperty("time_type")
    public void setTime_type(String  time_type){
        this.time_type = time_type ;
        this.time_typeDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTime_typeDirtyFlag(){
        return time_typeDirtyFlag ;
    }

    /**
     * 获取 [DATE_TO]
     */
    @JsonProperty("date_to")
    public Timestamp getDate_to(){
        return date_to ;
    }

    /**
     * 设置 [DATE_TO]
     */
    @JsonProperty("date_to")
    public void setDate_to(Timestamp  date_to){
        this.date_to = date_to ;
        this.date_toDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_TO]脏标记
     */
    @JsonIgnore
    public boolean getDate_toDirtyFlag(){
        return date_toDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DATE_FROM]
     */
    @JsonProperty("date_from")
    public Timestamp getDate_from(){
        return date_from ;
    }

    /**
     * 设置 [DATE_FROM]
     */
    @JsonProperty("date_from")
    public void setDate_from(Timestamp  date_from){
        this.date_from = date_from ;
        this.date_fromDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_FROM]脏标记
     */
    @JsonIgnore
    public boolean getDate_fromDirtyFlag(){
        return date_fromDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CALENDAR_ID_TEXT]
     */
    @JsonProperty("calendar_id_text")
    public String getCalendar_id_text(){
        return calendar_id_text ;
    }

    /**
     * 设置 [CALENDAR_ID_TEXT]
     */
    @JsonProperty("calendar_id_text")
    public void setCalendar_id_text(String  calendar_id_text){
        this.calendar_id_text = calendar_id_text ;
        this.calendar_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CALENDAR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_id_textDirtyFlag(){
        return calendar_id_textDirtyFlag ;
    }

    /**
     * 获取 [HOLIDAY_ID_TEXT]
     */
    @JsonProperty("holiday_id_text")
    public String getHoliday_id_text(){
        return holiday_id_text ;
    }

    /**
     * 设置 [HOLIDAY_ID_TEXT]
     */
    @JsonProperty("holiday_id_text")
    public void setHoliday_id_text(String  holiday_id_text){
        this.holiday_id_text = holiday_id_text ;
        this.holiday_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [HOLIDAY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_id_textDirtyFlag(){
        return holiday_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_ID_TEXT]
     */
    @JsonProperty("resource_id_text")
    public String getResource_id_text(){
        return resource_id_text ;
    }

    /**
     * 设置 [RESOURCE_ID_TEXT]
     */
    @JsonProperty("resource_id_text")
    public void setResource_id_text(String  resource_id_text){
        this.resource_id_text = resource_id_text ;
        this.resource_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getResource_id_textDirtyFlag(){
        return resource_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [HOLIDAY_ID]
     */
    @JsonProperty("holiday_id")
    public Integer getHoliday_id(){
        return holiday_id ;
    }

    /**
     * 设置 [HOLIDAY_ID]
     */
    @JsonProperty("holiday_id")
    public void setHoliday_id(Integer  holiday_id){
        this.holiday_id = holiday_id ;
        this.holiday_idDirtyFlag = true ;
    }

    /**
     * 获取 [HOLIDAY_ID]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_idDirtyFlag(){
        return holiday_idDirtyFlag ;
    }

    /**
     * 获取 [CALENDAR_ID]
     */
    @JsonProperty("calendar_id")
    public Integer getCalendar_id(){
        return calendar_id ;
    }

    /**
     * 设置 [CALENDAR_ID]
     */
    @JsonProperty("calendar_id")
    public void setCalendar_id(Integer  calendar_id){
        this.calendar_id = calendar_id ;
        this.calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_idDirtyFlag(){
        return calendar_idDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_ID]
     */
    @JsonProperty("resource_id")
    public Integer getResource_id(){
        return resource_id ;
    }

    /**
     * 设置 [RESOURCE_ID]
     */
    @JsonProperty("resource_id")
    public void setResource_id(Integer  resource_id){
        this.resource_id = resource_id ;
        this.resource_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_idDirtyFlag(){
        return resource_idDirtyFlag ;
    }



    public Resource_calendar_leaves toDO() {
        Resource_calendar_leaves srfdomain = new Resource_calendar_leaves();
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getTime_typeDirtyFlag())
            srfdomain.setTime_type(time_type);
        if(getDate_toDirtyFlag())
            srfdomain.setDate_to(date_to);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDate_fromDirtyFlag())
            srfdomain.setDate_from(date_from);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCalendar_id_textDirtyFlag())
            srfdomain.setCalendar_id_text(calendar_id_text);
        if(getHoliday_id_textDirtyFlag())
            srfdomain.setHoliday_id_text(holiday_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getResource_id_textDirtyFlag())
            srfdomain.setResource_id_text(resource_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getHoliday_idDirtyFlag())
            srfdomain.setHoliday_id(holiday_id);
        if(getCalendar_idDirtyFlag())
            srfdomain.setCalendar_id(calendar_id);
        if(getResource_idDirtyFlag())
            srfdomain.setResource_id(resource_id);

        return srfdomain;
    }

    public void fromDO(Resource_calendar_leaves srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getTime_typeDirtyFlag())
            this.setTime_type(srfdomain.getTime_type());
        if(srfdomain.getDate_toDirtyFlag())
            this.setDate_to(srfdomain.getDate_to());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDate_fromDirtyFlag())
            this.setDate_from(srfdomain.getDate_from());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCalendar_id_textDirtyFlag())
            this.setCalendar_id_text(srfdomain.getCalendar_id_text());
        if(srfdomain.getHoliday_id_textDirtyFlag())
            this.setHoliday_id_text(srfdomain.getHoliday_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getResource_id_textDirtyFlag())
            this.setResource_id_text(srfdomain.getResource_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getHoliday_idDirtyFlag())
            this.setHoliday_id(srfdomain.getHoliday_id());
        if(srfdomain.getCalendar_idDirtyFlag())
            this.setCalendar_id(srfdomain.getCalendar_id());
        if(srfdomain.getResource_idDirtyFlag())
            this.setResource_id(srfdomain.getResource_id());

    }

    public List<Resource_calendar_leavesDTO> fromDOPage(List<Resource_calendar_leaves> poPage)   {
        if(poPage == null)
            return null;
        List<Resource_calendar_leavesDTO> dtos=new ArrayList<Resource_calendar_leavesDTO>();
        for(Resource_calendar_leaves domain : poPage) {
            Resource_calendar_leavesDTO dto = new Resource_calendar_leavesDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

