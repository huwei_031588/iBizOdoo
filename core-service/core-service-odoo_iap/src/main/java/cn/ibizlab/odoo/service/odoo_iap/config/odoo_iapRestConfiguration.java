package cn.ibizlab.odoo.service.odoo_iap.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_iap")
public class odoo_iapRestConfiguration {

}
