package cn.ibizlab.odoo.service.odoo_sale.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_sale.dto.Sale_reportDTO;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_reportService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_reportSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Sale_report" })
@RestController
@RequestMapping("")
public class Sale_reportResource {

    @Autowired
    private ISale_reportService sale_reportService;

    public ISale_reportService getSale_reportService() {
        return this.sale_reportService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Sale_report" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_reports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sale_reportDTO> sale_reportdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Sale_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_reports/{sale_report_id}")
    public ResponseEntity<Sale_reportDTO> get(@PathVariable("sale_report_id") Integer sale_report_id) {
        Sale_reportDTO dto = new Sale_reportDTO();
        Sale_report domain = sale_reportService.get(sale_report_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Sale_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_reports")

    public ResponseEntity<Sale_reportDTO> create(@RequestBody Sale_reportDTO sale_reportdto) {
        Sale_reportDTO dto = new Sale_reportDTO();
        Sale_report domain = sale_reportdto.toDO();
		sale_reportService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Sale_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_reports/{sale_report_id}")

    public ResponseEntity<Sale_reportDTO> update(@PathVariable("sale_report_id") Integer sale_report_id, @RequestBody Sale_reportDTO sale_reportdto) {
		Sale_report domain = sale_reportdto.toDO();
        domain.setId(sale_report_id);
		sale_reportService.update(domain);
		Sale_reportDTO dto = new Sale_reportDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Sale_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_reports/{sale_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_report_id") Integer sale_report_id) {
        Sale_reportDTO sale_reportdto = new Sale_reportDTO();
		Sale_report domain = new Sale_report();
		sale_reportdto.setId(sale_report_id);
		domain.setId(sale_report_id);
        Boolean rst = sale_reportService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Sale_report" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_reports/createBatch")
    public ResponseEntity<Boolean> createBatchSale_report(@RequestBody List<Sale_reportDTO> sale_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Sale_report" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_reports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_reportDTO> sale_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sale_report" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_sale/sale_reports/fetchdefault")
	public ResponseEntity<Page<Sale_reportDTO>> fetchDefault(Sale_reportSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sale_reportDTO> list = new ArrayList<Sale_reportDTO>();
        
        Page<Sale_report> domains = sale_reportService.searchDefault(context) ;
        for(Sale_report sale_report : domains.getContent()){
            Sale_reportDTO dto = new Sale_reportDTO();
            dto.fromDO(sale_report);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
