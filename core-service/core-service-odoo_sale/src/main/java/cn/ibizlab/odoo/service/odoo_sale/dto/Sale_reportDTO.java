package cn.ibizlab.odoo.service.odoo_sale.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_report.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_report;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Sale_reportDTO]
 */
public class Sale_reportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRICE_SUBTOTAL]
     *
     */
    @Sale_reportPrice_subtotalDefault(info = "默认规则")
    private Double price_subtotal;

    @JsonIgnore
    private boolean price_subtotalDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Sale_reportIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISCOUNT]
     *
     */
    @Sale_reportDiscountDefault(info = "默认规则")
    private Double discount;

    @JsonIgnore
    private boolean discountDirtyFlag;

    /**
     * 属性 [DISCOUNT_AMOUNT]
     *
     */
    @Sale_reportDiscount_amountDefault(info = "默认规则")
    private Double discount_amount;

    @JsonIgnore
    private boolean discount_amountDirtyFlag;

    /**
     * 属性 [VOLUME]
     *
     */
    @Sale_reportVolumeDefault(info = "默认规则")
    private Double volume;

    @JsonIgnore
    private boolean volumeDirtyFlag;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @Sale_reportPrice_totalDefault(info = "默认规则")
    private Double price_total;

    @JsonIgnore
    private boolean price_totalDirtyFlag;

    /**
     * 属性 [CONFIRMATION_DATE]
     *
     */
    @Sale_reportConfirmation_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp confirmation_date;

    @JsonIgnore
    private boolean confirmation_dateDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Sale_reportDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Sale_report__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Sale_reportNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Sale_reportDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Sale_reportStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [QTY_TO_INVOICE]
     *
     */
    @Sale_reportQty_to_invoiceDefault(info = "默认规则")
    private Double qty_to_invoice;

    @JsonIgnore
    private boolean qty_to_invoiceDirtyFlag;

    /**
     * 属性 [NBR]
     *
     */
    @Sale_reportNbrDefault(info = "默认规则")
    private Integer nbr;

    @JsonIgnore
    private boolean nbrDirtyFlag;

    /**
     * 属性 [WEIGHT]
     *
     */
    @Sale_reportWeightDefault(info = "默认规则")
    private Double weight;

    @JsonIgnore
    private boolean weightDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @Sale_reportProduct_uom_qtyDefault(info = "默认规则")
    private Double product_uom_qty;

    @JsonIgnore
    private boolean product_uom_qtyDirtyFlag;

    /**
     * 属性 [UNTAXED_AMOUNT_TO_INVOICE]
     *
     */
    @Sale_reportUntaxed_amount_to_invoiceDefault(info = "默认规则")
    private Double untaxed_amount_to_invoice;

    @JsonIgnore
    private boolean untaxed_amount_to_invoiceDirtyFlag;

    /**
     * 属性 [UNTAXED_AMOUNT_INVOICED]
     *
     */
    @Sale_reportUntaxed_amount_invoicedDefault(info = "默认规则")
    private Double untaxed_amount_invoiced;

    @JsonIgnore
    private boolean untaxed_amount_invoicedDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Sale_reportWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [QTY_DELIVERED]
     *
     */
    @Sale_reportQty_deliveredDefault(info = "默认规则")
    private Double qty_delivered;

    @JsonIgnore
    private boolean qty_deliveredDirtyFlag;

    /**
     * 属性 [QTY_INVOICED]
     *
     */
    @Sale_reportQty_invoicedDefault(info = "默认规则")
    private Double qty_invoiced;

    @JsonIgnore
    private boolean qty_invoicedDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID_TEXT]
     *
     */
    @Sale_reportProduct_tmpl_id_textDefault(info = "默认规则")
    private String product_tmpl_id_text;

    @JsonIgnore
    private boolean product_tmpl_id_textDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @Sale_reportWarehouse_id_textDefault(info = "默认规则")
    private String warehouse_id_text;

    @JsonIgnore
    private boolean warehouse_id_textDirtyFlag;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID_TEXT]
     *
     */
    @Sale_reportAnalytic_account_id_textDefault(info = "默认规则")
    private String analytic_account_id_text;

    @JsonIgnore
    private boolean analytic_account_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Sale_reportProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @Sale_reportCountry_id_textDefault(info = "默认规则")
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;

    /**
     * 属性 [CATEG_ID_TEXT]
     *
     */
    @Sale_reportCateg_id_textDefault(info = "默认规则")
    private String categ_id_text;

    @JsonIgnore
    private boolean categ_id_textDirtyFlag;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @Sale_reportSource_id_textDefault(info = "默认规则")
    private String source_id_text;

    @JsonIgnore
    private boolean source_id_textDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @Sale_reportCampaign_id_textDefault(info = "默认规则")
    private String campaign_id_text;

    @JsonIgnore
    private boolean campaign_id_textDirtyFlag;

    /**
     * 属性 [ORDER_ID_TEXT]
     *
     */
    @Sale_reportOrder_id_textDefault(info = "默认规则")
    private String order_id_text;

    @JsonIgnore
    private boolean order_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Sale_reportUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID_TEXT]
     *
     */
    @Sale_reportCommercial_partner_id_textDefault(info = "默认规则")
    private String commercial_partner_id_text;

    @JsonIgnore
    private boolean commercial_partner_id_textDirtyFlag;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @Sale_reportMedium_id_textDefault(info = "默认规则")
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @Sale_reportProduct_uom_textDefault(info = "默认规则")
    private String product_uom_text;

    @JsonIgnore
    private boolean product_uom_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Sale_reportCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @Sale_reportTeam_id_textDefault(info = "默认规则")
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;

    /**
     * 属性 [PRICELIST_ID_TEXT]
     *
     */
    @Sale_reportPricelist_id_textDefault(info = "默认规则")
    private String pricelist_id_text;

    @JsonIgnore
    private boolean pricelist_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Sale_reportPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @Sale_reportTeam_idDefault(info = "默认规则")
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @Sale_reportCommercial_partner_idDefault(info = "默认规则")
    private Integer commercial_partner_id;

    @JsonIgnore
    private boolean commercial_partner_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Sale_reportUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @Sale_reportProduct_tmpl_idDefault(info = "默认规则")
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID]
     *
     */
    @Sale_reportAnalytic_account_idDefault(info = "默认规则")
    private Integer analytic_account_id;

    @JsonIgnore
    private boolean analytic_account_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Sale_reportCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @Sale_reportMedium_idDefault(info = "默认规则")
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;

    /**
     * 属性 [ORDER_ID]
     *
     */
    @Sale_reportOrder_idDefault(info = "默认规则")
    private Integer order_id;

    @JsonIgnore
    private boolean order_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Sale_reportPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @Sale_reportSource_idDefault(info = "默认规则")
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @Sale_reportCampaign_idDefault(info = "默认规则")
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;

    /**
     * 属性 [CATEG_ID]
     *
     */
    @Sale_reportCateg_idDefault(info = "默认规则")
    private Integer categ_id;

    @JsonIgnore
    private boolean categ_idDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @Sale_reportWarehouse_idDefault(info = "默认规则")
    private Integer warehouse_id;

    @JsonIgnore
    private boolean warehouse_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Sale_reportProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [PRICELIST_ID]
     *
     */
    @Sale_reportPricelist_idDefault(info = "默认规则")
    private Integer pricelist_id;

    @JsonIgnore
    private boolean pricelist_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @Sale_reportProduct_uomDefault(info = "默认规则")
    private Integer product_uom;

    @JsonIgnore
    private boolean product_uomDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Sale_reportCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;


    /**
     * 获取 [PRICE_SUBTOTAL]
     */
    @JsonProperty("price_subtotal")
    public Double getPrice_subtotal(){
        return price_subtotal ;
    }

    /**
     * 设置 [PRICE_SUBTOTAL]
     */
    @JsonProperty("price_subtotal")
    public void setPrice_subtotal(Double  price_subtotal){
        this.price_subtotal = price_subtotal ;
        this.price_subtotalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotalDirtyFlag(){
        return price_subtotalDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISCOUNT]
     */
    @JsonProperty("discount")
    public Double getDiscount(){
        return discount ;
    }

    /**
     * 设置 [DISCOUNT]
     */
    @JsonProperty("discount")
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.discountDirtyFlag = true ;
    }

    /**
     * 获取 [DISCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getDiscountDirtyFlag(){
        return discountDirtyFlag ;
    }

    /**
     * 获取 [DISCOUNT_AMOUNT]
     */
    @JsonProperty("discount_amount")
    public Double getDiscount_amount(){
        return discount_amount ;
    }

    /**
     * 设置 [DISCOUNT_AMOUNT]
     */
    @JsonProperty("discount_amount")
    public void setDiscount_amount(Double  discount_amount){
        this.discount_amount = discount_amount ;
        this.discount_amountDirtyFlag = true ;
    }

    /**
     * 获取 [DISCOUNT_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getDiscount_amountDirtyFlag(){
        return discount_amountDirtyFlag ;
    }

    /**
     * 获取 [VOLUME]
     */
    @JsonProperty("volume")
    public Double getVolume(){
        return volume ;
    }

    /**
     * 设置 [VOLUME]
     */
    @JsonProperty("volume")
    public void setVolume(Double  volume){
        this.volume = volume ;
        this.volumeDirtyFlag = true ;
    }

    /**
     * 获取 [VOLUME]脏标记
     */
    @JsonIgnore
    public boolean getVolumeDirtyFlag(){
        return volumeDirtyFlag ;
    }

    /**
     * 获取 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return price_total ;
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return price_totalDirtyFlag ;
    }

    /**
     * 获取 [CONFIRMATION_DATE]
     */
    @JsonProperty("confirmation_date")
    public Timestamp getConfirmation_date(){
        return confirmation_date ;
    }

    /**
     * 设置 [CONFIRMATION_DATE]
     */
    @JsonProperty("confirmation_date")
    public void setConfirmation_date(Timestamp  confirmation_date){
        this.confirmation_date = confirmation_date ;
        this.confirmation_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CONFIRMATION_DATE]脏标记
     */
    @JsonIgnore
    public boolean getConfirmation_dateDirtyFlag(){
        return confirmation_dateDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [QTY_TO_INVOICE]
     */
    @JsonProperty("qty_to_invoice")
    public Double getQty_to_invoice(){
        return qty_to_invoice ;
    }

    /**
     * 设置 [QTY_TO_INVOICE]
     */
    @JsonProperty("qty_to_invoice")
    public void setQty_to_invoice(Double  qty_to_invoice){
        this.qty_to_invoice = qty_to_invoice ;
        this.qty_to_invoiceDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_TO_INVOICE]脏标记
     */
    @JsonIgnore
    public boolean getQty_to_invoiceDirtyFlag(){
        return qty_to_invoiceDirtyFlag ;
    }

    /**
     * 获取 [NBR]
     */
    @JsonProperty("nbr")
    public Integer getNbr(){
        return nbr ;
    }

    /**
     * 设置 [NBR]
     */
    @JsonProperty("nbr")
    public void setNbr(Integer  nbr){
        this.nbr = nbr ;
        this.nbrDirtyFlag = true ;
    }

    /**
     * 获取 [NBR]脏标记
     */
    @JsonIgnore
    public boolean getNbrDirtyFlag(){
        return nbrDirtyFlag ;
    }

    /**
     * 获取 [WEIGHT]
     */
    @JsonProperty("weight")
    public Double getWeight(){
        return weight ;
    }

    /**
     * 设置 [WEIGHT]
     */
    @JsonProperty("weight")
    public void setWeight(Double  weight){
        this.weight = weight ;
        this.weightDirtyFlag = true ;
    }

    /**
     * 获取 [WEIGHT]脏标记
     */
    @JsonIgnore
    public boolean getWeightDirtyFlag(){
        return weightDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return product_uom_qty ;
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return product_uom_qtyDirtyFlag ;
    }

    /**
     * 获取 [UNTAXED_AMOUNT_TO_INVOICE]
     */
    @JsonProperty("untaxed_amount_to_invoice")
    public Double getUntaxed_amount_to_invoice(){
        return untaxed_amount_to_invoice ;
    }

    /**
     * 设置 [UNTAXED_AMOUNT_TO_INVOICE]
     */
    @JsonProperty("untaxed_amount_to_invoice")
    public void setUntaxed_amount_to_invoice(Double  untaxed_amount_to_invoice){
        this.untaxed_amount_to_invoice = untaxed_amount_to_invoice ;
        this.untaxed_amount_to_invoiceDirtyFlag = true ;
    }

    /**
     * 获取 [UNTAXED_AMOUNT_TO_INVOICE]脏标记
     */
    @JsonIgnore
    public boolean getUntaxed_amount_to_invoiceDirtyFlag(){
        return untaxed_amount_to_invoiceDirtyFlag ;
    }

    /**
     * 获取 [UNTAXED_AMOUNT_INVOICED]
     */
    @JsonProperty("untaxed_amount_invoiced")
    public Double getUntaxed_amount_invoiced(){
        return untaxed_amount_invoiced ;
    }

    /**
     * 设置 [UNTAXED_AMOUNT_INVOICED]
     */
    @JsonProperty("untaxed_amount_invoiced")
    public void setUntaxed_amount_invoiced(Double  untaxed_amount_invoiced){
        this.untaxed_amount_invoiced = untaxed_amount_invoiced ;
        this.untaxed_amount_invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [UNTAXED_AMOUNT_INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getUntaxed_amount_invoicedDirtyFlag(){
        return untaxed_amount_invoicedDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [QTY_DELIVERED]
     */
    @JsonProperty("qty_delivered")
    public Double getQty_delivered(){
        return qty_delivered ;
    }

    /**
     * 设置 [QTY_DELIVERED]
     */
    @JsonProperty("qty_delivered")
    public void setQty_delivered(Double  qty_delivered){
        this.qty_delivered = qty_delivered ;
        this.qty_deliveredDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_DELIVERED]脏标记
     */
    @JsonIgnore
    public boolean getQty_deliveredDirtyFlag(){
        return qty_deliveredDirtyFlag ;
    }

    /**
     * 获取 [QTY_INVOICED]
     */
    @JsonProperty("qty_invoiced")
    public Double getQty_invoiced(){
        return qty_invoiced ;
    }

    /**
     * 设置 [QTY_INVOICED]
     */
    @JsonProperty("qty_invoiced")
    public void setQty_invoiced(Double  qty_invoiced){
        this.qty_invoiced = qty_invoiced ;
        this.qty_invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getQty_invoicedDirtyFlag(){
        return qty_invoicedDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return product_tmpl_id_text ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return product_tmpl_id_textDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return warehouse_id_text ;
    }

    /**
     * 设置 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return warehouse_id_textDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("analytic_account_id_text")
    public String getAnalytic_account_id_text(){
        return analytic_account_id_text ;
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("analytic_account_id_text")
    public void setAnalytic_account_id_text(String  analytic_account_id_text){
        this.analytic_account_id_text = analytic_account_id_text ;
        this.analytic_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_id_textDirtyFlag(){
        return analytic_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return country_id_text ;
    }

    /**
     * 设置 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return country_id_textDirtyFlag ;
    }

    /**
     * 获取 [CATEG_ID_TEXT]
     */
    @JsonProperty("categ_id_text")
    public String getCateg_id_text(){
        return categ_id_text ;
    }

    /**
     * 设置 [CATEG_ID_TEXT]
     */
    @JsonProperty("categ_id_text")
    public void setCateg_id_text(String  categ_id_text){
        this.categ_id_text = categ_id_text ;
        this.categ_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCateg_id_textDirtyFlag(){
        return categ_id_textDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return source_id_text ;
    }

    /**
     * 设置 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return source_id_textDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return campaign_id_text ;
    }

    /**
     * 设置 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [ORDER_ID_TEXT]
     */
    @JsonProperty("order_id_text")
    public String getOrder_id_text(){
        return order_id_text ;
    }

    /**
     * 设置 [ORDER_ID_TEXT]
     */
    @JsonProperty("order_id_text")
    public void setOrder_id_text(String  order_id_text){
        this.order_id_text = order_id_text ;
        this.order_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOrder_id_textDirtyFlag(){
        return order_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID_TEXT]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return commercial_partner_id_text ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID_TEXT]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return commercial_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return medium_id_text ;
    }

    /**
     * 设置 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return product_uom_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return product_uom_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return team_id_text ;
    }

    /**
     * 设置 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return team_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRICELIST_ID_TEXT]
     */
    @JsonProperty("pricelist_id_text")
    public String getPricelist_id_text(){
        return pricelist_id_text ;
    }

    /**
     * 设置 [PRICELIST_ID_TEXT]
     */
    @JsonProperty("pricelist_id_text")
    public void setPricelist_id_text(String  pricelist_id_text){
        this.pricelist_id_text = pricelist_id_text ;
        this.pricelist_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRICELIST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_id_textDirtyFlag(){
        return pricelist_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return team_id ;
    }

    /**
     * 设置 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return team_idDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return commercial_partner_id ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return commercial_partner_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return product_tmpl_id ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return product_tmpl_idDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("analytic_account_id")
    public Integer getAnalytic_account_id(){
        return analytic_account_id ;
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("analytic_account_id")
    public void setAnalytic_account_id(Integer  analytic_account_id){
        this.analytic_account_id = analytic_account_id ;
        this.analytic_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_idDirtyFlag(){
        return analytic_account_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return medium_id ;
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return medium_idDirtyFlag ;
    }

    /**
     * 获取 [ORDER_ID]
     */
    @JsonProperty("order_id")
    public Integer getOrder_id(){
        return order_id ;
    }

    /**
     * 设置 [ORDER_ID]
     */
    @JsonProperty("order_id")
    public void setOrder_id(Integer  order_id){
        this.order_id = order_id ;
        this.order_idDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idDirtyFlag(){
        return order_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return source_id ;
    }

    /**
     * 设置 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return source_idDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return campaign_id ;
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return campaign_idDirtyFlag ;
    }

    /**
     * 获取 [CATEG_ID]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return categ_id ;
    }

    /**
     * 设置 [CATEG_ID]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return categ_idDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return warehouse_id ;
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return warehouse_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [PRICELIST_ID]
     */
    @JsonProperty("pricelist_id")
    public Integer getPricelist_id(){
        return pricelist_id ;
    }

    /**
     * 设置 [PRICELIST_ID]
     */
    @JsonProperty("pricelist_id")
    public void setPricelist_id(Integer  pricelist_id){
        this.pricelist_id = pricelist_id ;
        this.pricelist_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRICELIST_ID]脏标记
     */
    @JsonIgnore
    public boolean getPricelist_idDirtyFlag(){
        return pricelist_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return product_uom ;
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return product_uomDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }



    public Sale_report toDO() {
        Sale_report srfdomain = new Sale_report();
        if(getPrice_subtotalDirtyFlag())
            srfdomain.setPrice_subtotal(price_subtotal);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDiscountDirtyFlag())
            srfdomain.setDiscount(discount);
        if(getDiscount_amountDirtyFlag())
            srfdomain.setDiscount_amount(discount_amount);
        if(getVolumeDirtyFlag())
            srfdomain.setVolume(volume);
        if(getPrice_totalDirtyFlag())
            srfdomain.setPrice_total(price_total);
        if(getConfirmation_dateDirtyFlag())
            srfdomain.setConfirmation_date(confirmation_date);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getQty_to_invoiceDirtyFlag())
            srfdomain.setQty_to_invoice(qty_to_invoice);
        if(getNbrDirtyFlag())
            srfdomain.setNbr(nbr);
        if(getWeightDirtyFlag())
            srfdomain.setWeight(weight);
        if(getProduct_uom_qtyDirtyFlag())
            srfdomain.setProduct_uom_qty(product_uom_qty);
        if(getUntaxed_amount_to_invoiceDirtyFlag())
            srfdomain.setUntaxed_amount_to_invoice(untaxed_amount_to_invoice);
        if(getUntaxed_amount_invoicedDirtyFlag())
            srfdomain.setUntaxed_amount_invoiced(untaxed_amount_invoiced);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getQty_deliveredDirtyFlag())
            srfdomain.setQty_delivered(qty_delivered);
        if(getQty_invoicedDirtyFlag())
            srfdomain.setQty_invoiced(qty_invoiced);
        if(getProduct_tmpl_id_textDirtyFlag())
            srfdomain.setProduct_tmpl_id_text(product_tmpl_id_text);
        if(getWarehouse_id_textDirtyFlag())
            srfdomain.setWarehouse_id_text(warehouse_id_text);
        if(getAnalytic_account_id_textDirtyFlag())
            srfdomain.setAnalytic_account_id_text(analytic_account_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getCountry_id_textDirtyFlag())
            srfdomain.setCountry_id_text(country_id_text);
        if(getCateg_id_textDirtyFlag())
            srfdomain.setCateg_id_text(categ_id_text);
        if(getSource_id_textDirtyFlag())
            srfdomain.setSource_id_text(source_id_text);
        if(getCampaign_id_textDirtyFlag())
            srfdomain.setCampaign_id_text(campaign_id_text);
        if(getOrder_id_textDirtyFlag())
            srfdomain.setOrder_id_text(order_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getCommercial_partner_id_textDirtyFlag())
            srfdomain.setCommercial_partner_id_text(commercial_partner_id_text);
        if(getMedium_id_textDirtyFlag())
            srfdomain.setMedium_id_text(medium_id_text);
        if(getProduct_uom_textDirtyFlag())
            srfdomain.setProduct_uom_text(product_uom_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getTeam_id_textDirtyFlag())
            srfdomain.setTeam_id_text(team_id_text);
        if(getPricelist_id_textDirtyFlag())
            srfdomain.setPricelist_id_text(pricelist_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getTeam_idDirtyFlag())
            srfdomain.setTeam_id(team_id);
        if(getCommercial_partner_idDirtyFlag())
            srfdomain.setCommercial_partner_id(commercial_partner_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getProduct_tmpl_idDirtyFlag())
            srfdomain.setProduct_tmpl_id(product_tmpl_id);
        if(getAnalytic_account_idDirtyFlag())
            srfdomain.setAnalytic_account_id(analytic_account_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getMedium_idDirtyFlag())
            srfdomain.setMedium_id(medium_id);
        if(getOrder_idDirtyFlag())
            srfdomain.setOrder_id(order_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getSource_idDirtyFlag())
            srfdomain.setSource_id(source_id);
        if(getCampaign_idDirtyFlag())
            srfdomain.setCampaign_id(campaign_id);
        if(getCateg_idDirtyFlag())
            srfdomain.setCateg_id(categ_id);
        if(getWarehouse_idDirtyFlag())
            srfdomain.setWarehouse_id(warehouse_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getPricelist_idDirtyFlag())
            srfdomain.setPricelist_id(pricelist_id);
        if(getProduct_uomDirtyFlag())
            srfdomain.setProduct_uom(product_uom);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);

        return srfdomain;
    }

    public void fromDO(Sale_report srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getPrice_subtotalDirtyFlag())
            this.setPrice_subtotal(srfdomain.getPrice_subtotal());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDiscountDirtyFlag())
            this.setDiscount(srfdomain.getDiscount());
        if(srfdomain.getDiscount_amountDirtyFlag())
            this.setDiscount_amount(srfdomain.getDiscount_amount());
        if(srfdomain.getVolumeDirtyFlag())
            this.setVolume(srfdomain.getVolume());
        if(srfdomain.getPrice_totalDirtyFlag())
            this.setPrice_total(srfdomain.getPrice_total());
        if(srfdomain.getConfirmation_dateDirtyFlag())
            this.setConfirmation_date(srfdomain.getConfirmation_date());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getQty_to_invoiceDirtyFlag())
            this.setQty_to_invoice(srfdomain.getQty_to_invoice());
        if(srfdomain.getNbrDirtyFlag())
            this.setNbr(srfdomain.getNbr());
        if(srfdomain.getWeightDirtyFlag())
            this.setWeight(srfdomain.getWeight());
        if(srfdomain.getProduct_uom_qtyDirtyFlag())
            this.setProduct_uom_qty(srfdomain.getProduct_uom_qty());
        if(srfdomain.getUntaxed_amount_to_invoiceDirtyFlag())
            this.setUntaxed_amount_to_invoice(srfdomain.getUntaxed_amount_to_invoice());
        if(srfdomain.getUntaxed_amount_invoicedDirtyFlag())
            this.setUntaxed_amount_invoiced(srfdomain.getUntaxed_amount_invoiced());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getQty_deliveredDirtyFlag())
            this.setQty_delivered(srfdomain.getQty_delivered());
        if(srfdomain.getQty_invoicedDirtyFlag())
            this.setQty_invoiced(srfdomain.getQty_invoiced());
        if(srfdomain.getProduct_tmpl_id_textDirtyFlag())
            this.setProduct_tmpl_id_text(srfdomain.getProduct_tmpl_id_text());
        if(srfdomain.getWarehouse_id_textDirtyFlag())
            this.setWarehouse_id_text(srfdomain.getWarehouse_id_text());
        if(srfdomain.getAnalytic_account_id_textDirtyFlag())
            this.setAnalytic_account_id_text(srfdomain.getAnalytic_account_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getCountry_id_textDirtyFlag())
            this.setCountry_id_text(srfdomain.getCountry_id_text());
        if(srfdomain.getCateg_id_textDirtyFlag())
            this.setCateg_id_text(srfdomain.getCateg_id_text());
        if(srfdomain.getSource_id_textDirtyFlag())
            this.setSource_id_text(srfdomain.getSource_id_text());
        if(srfdomain.getCampaign_id_textDirtyFlag())
            this.setCampaign_id_text(srfdomain.getCampaign_id_text());
        if(srfdomain.getOrder_id_textDirtyFlag())
            this.setOrder_id_text(srfdomain.getOrder_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getCommercial_partner_id_textDirtyFlag())
            this.setCommercial_partner_id_text(srfdomain.getCommercial_partner_id_text());
        if(srfdomain.getMedium_id_textDirtyFlag())
            this.setMedium_id_text(srfdomain.getMedium_id_text());
        if(srfdomain.getProduct_uom_textDirtyFlag())
            this.setProduct_uom_text(srfdomain.getProduct_uom_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getTeam_id_textDirtyFlag())
            this.setTeam_id_text(srfdomain.getTeam_id_text());
        if(srfdomain.getPricelist_id_textDirtyFlag())
            this.setPricelist_id_text(srfdomain.getPricelist_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getTeam_idDirtyFlag())
            this.setTeam_id(srfdomain.getTeam_id());
        if(srfdomain.getCommercial_partner_idDirtyFlag())
            this.setCommercial_partner_id(srfdomain.getCommercial_partner_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getProduct_tmpl_idDirtyFlag())
            this.setProduct_tmpl_id(srfdomain.getProduct_tmpl_id());
        if(srfdomain.getAnalytic_account_idDirtyFlag())
            this.setAnalytic_account_id(srfdomain.getAnalytic_account_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getMedium_idDirtyFlag())
            this.setMedium_id(srfdomain.getMedium_id());
        if(srfdomain.getOrder_idDirtyFlag())
            this.setOrder_id(srfdomain.getOrder_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getSource_idDirtyFlag())
            this.setSource_id(srfdomain.getSource_id());
        if(srfdomain.getCampaign_idDirtyFlag())
            this.setCampaign_id(srfdomain.getCampaign_id());
        if(srfdomain.getCateg_idDirtyFlag())
            this.setCateg_id(srfdomain.getCateg_id());
        if(srfdomain.getWarehouse_idDirtyFlag())
            this.setWarehouse_id(srfdomain.getWarehouse_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getPricelist_idDirtyFlag())
            this.setPricelist_id(srfdomain.getPricelist_id());
        if(srfdomain.getProduct_uomDirtyFlag())
            this.setProduct_uom(srfdomain.getProduct_uom());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());

    }

    public List<Sale_reportDTO> fromDOPage(List<Sale_report> poPage)   {
        if(poPage == null)
            return null;
        List<Sale_reportDTO> dtos=new ArrayList<Sale_reportDTO>();
        for(Sale_report domain : poPage) {
            Sale_reportDTO dto = new Sale_reportDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

