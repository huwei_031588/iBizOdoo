package cn.ibizlab.odoo.service.odoo_sale.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_sale.dto.Sale_payment_acquirer_onboarding_wizardDTO;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_payment_acquirer_onboarding_wizardService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_payment_acquirer_onboarding_wizardSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Sale_payment_acquirer_onboarding_wizard" })
@RestController
@RequestMapping("")
public class Sale_payment_acquirer_onboarding_wizardResource {

    @Autowired
    private ISale_payment_acquirer_onboarding_wizardService sale_payment_acquirer_onboarding_wizardService;

    public ISale_payment_acquirer_onboarding_wizardService getSale_payment_acquirer_onboarding_wizardService() {
        return this.sale_payment_acquirer_onboarding_wizardService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_payment_acquirer_onboarding_wizards/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_payment_acquirer_onboarding_wizardDTO> sale_payment_acquirer_onboarding_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_payment_acquirer_onboarding_wizards/createBatch")
    public ResponseEntity<Boolean> createBatchSale_payment_acquirer_onboarding_wizard(@RequestBody List<Sale_payment_acquirer_onboarding_wizardDTO> sale_payment_acquirer_onboarding_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_payment_acquirer_onboarding_wizards/{sale_payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Sale_payment_acquirer_onboarding_wizardDTO> get(@PathVariable("sale_payment_acquirer_onboarding_wizard_id") Integer sale_payment_acquirer_onboarding_wizard_id) {
        Sale_payment_acquirer_onboarding_wizardDTO dto = new Sale_payment_acquirer_onboarding_wizardDTO();
        Sale_payment_acquirer_onboarding_wizard domain = sale_payment_acquirer_onboarding_wizardService.get(sale_payment_acquirer_onboarding_wizard_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_payment_acquirer_onboarding_wizards/{sale_payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_payment_acquirer_onboarding_wizard_id") Integer sale_payment_acquirer_onboarding_wizard_id) {
        Sale_payment_acquirer_onboarding_wizardDTO sale_payment_acquirer_onboarding_wizarddto = new Sale_payment_acquirer_onboarding_wizardDTO();
		Sale_payment_acquirer_onboarding_wizard domain = new Sale_payment_acquirer_onboarding_wizard();
		sale_payment_acquirer_onboarding_wizarddto.setId(sale_payment_acquirer_onboarding_wizard_id);
		domain.setId(sale_payment_acquirer_onboarding_wizard_id);
        Boolean rst = sale_payment_acquirer_onboarding_wizardService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_payment_acquirer_onboarding_wizards/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sale_payment_acquirer_onboarding_wizardDTO> sale_payment_acquirer_onboarding_wizarddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_payment_acquirer_onboarding_wizards/{sale_payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Sale_payment_acquirer_onboarding_wizardDTO> update(@PathVariable("sale_payment_acquirer_onboarding_wizard_id") Integer sale_payment_acquirer_onboarding_wizard_id, @RequestBody Sale_payment_acquirer_onboarding_wizardDTO sale_payment_acquirer_onboarding_wizarddto) {
		Sale_payment_acquirer_onboarding_wizard domain = sale_payment_acquirer_onboarding_wizarddto.toDO();
        domain.setId(sale_payment_acquirer_onboarding_wizard_id);
		sale_payment_acquirer_onboarding_wizardService.update(domain);
		Sale_payment_acquirer_onboarding_wizardDTO dto = new Sale_payment_acquirer_onboarding_wizardDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_payment_acquirer_onboarding_wizards")

    public ResponseEntity<Sale_payment_acquirer_onboarding_wizardDTO> create(@RequestBody Sale_payment_acquirer_onboarding_wizardDTO sale_payment_acquirer_onboarding_wizarddto) {
        Sale_payment_acquirer_onboarding_wizardDTO dto = new Sale_payment_acquirer_onboarding_wizardDTO();
        Sale_payment_acquirer_onboarding_wizard domain = sale_payment_acquirer_onboarding_wizarddto.toDO();
		sale_payment_acquirer_onboarding_wizardService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sale_payment_acquirer_onboarding_wizard" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_sale/sale_payment_acquirer_onboarding_wizards/fetchdefault")
	public ResponseEntity<Page<Sale_payment_acquirer_onboarding_wizardDTO>> fetchDefault(Sale_payment_acquirer_onboarding_wizardSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sale_payment_acquirer_onboarding_wizardDTO> list = new ArrayList<Sale_payment_acquirer_onboarding_wizardDTO>();
        
        Page<Sale_payment_acquirer_onboarding_wizard> domains = sale_payment_acquirer_onboarding_wizardService.searchDefault(context) ;
        for(Sale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard : domains.getContent()){
            Sale_payment_acquirer_onboarding_wizardDTO dto = new Sale_payment_acquirer_onboarding_wizardDTO();
            dto.fromDO(sale_payment_acquirer_onboarding_wizard);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
