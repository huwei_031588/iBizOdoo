package cn.ibizlab.odoo.service.odoo_sale.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_sale.dto.Sale_order_optionDTO;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_option;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_optionService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_optionSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Sale_order_option" })
@RestController
@RequestMapping("")
public class Sale_order_optionResource {

    @Autowired
    private ISale_order_optionService sale_order_optionService;

    public ISale_order_optionService getSale_order_optionService() {
        return this.sale_order_optionService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Sale_order_option" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_options/createBatch")
    public ResponseEntity<Boolean> createBatchSale_order_option(@RequestBody List<Sale_order_optionDTO> sale_order_optiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Sale_order_option" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_options/{sale_order_option_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_option_id") Integer sale_order_option_id) {
        Sale_order_optionDTO sale_order_optiondto = new Sale_order_optionDTO();
		Sale_order_option domain = new Sale_order_option();
		sale_order_optiondto.setId(sale_order_option_id);
		domain.setId(sale_order_option_id);
        Boolean rst = sale_order_optionService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Sale_order_option" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_options/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_order_optionDTO> sale_order_optiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Sale_order_option" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_options/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sale_order_optionDTO> sale_order_optiondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Sale_order_option" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_options/{sale_order_option_id}")
    public ResponseEntity<Sale_order_optionDTO> get(@PathVariable("sale_order_option_id") Integer sale_order_option_id) {
        Sale_order_optionDTO dto = new Sale_order_optionDTO();
        Sale_order_option domain = sale_order_optionService.get(sale_order_option_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Sale_order_option" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_options")

    public ResponseEntity<Sale_order_optionDTO> create(@RequestBody Sale_order_optionDTO sale_order_optiondto) {
        Sale_order_optionDTO dto = new Sale_order_optionDTO();
        Sale_order_option domain = sale_order_optiondto.toDO();
		sale_order_optionService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Sale_order_option" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_options/{sale_order_option_id}")

    public ResponseEntity<Sale_order_optionDTO> update(@PathVariable("sale_order_option_id") Integer sale_order_option_id, @RequestBody Sale_order_optionDTO sale_order_optiondto) {
		Sale_order_option domain = sale_order_optiondto.toDO();
        domain.setId(sale_order_option_id);
		sale_order_optionService.update(domain);
		Sale_order_optionDTO dto = new Sale_order_optionDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sale_order_option" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_sale/sale_order_options/fetchdefault")
	public ResponseEntity<Page<Sale_order_optionDTO>> fetchDefault(Sale_order_optionSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sale_order_optionDTO> list = new ArrayList<Sale_order_optionDTO>();
        
        Page<Sale_order_option> domains = sale_order_optionService.searchDefault(context) ;
        for(Sale_order_option sale_order_option : domains.getContent()){
            Sale_order_optionDTO dto = new Sale_order_optionDTO();
            dto.fromDO(sale_order_option);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
