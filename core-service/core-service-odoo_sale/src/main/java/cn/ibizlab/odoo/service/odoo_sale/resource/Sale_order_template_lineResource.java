package cn.ibizlab.odoo.service.odoo_sale.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_sale.dto.Sale_order_template_lineDTO;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_line;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_template_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_template_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Sale_order_template_line" })
@RestController
@RequestMapping("")
public class Sale_order_template_lineResource {

    @Autowired
    private ISale_order_template_lineService sale_order_template_lineService;

    public ISale_order_template_lineService getSale_order_template_lineService() {
        return this.sale_order_template_lineService;
    }

    @ApiOperation(value = "删除数据", tags = {"Sale_order_template_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_template_lines/{sale_order_template_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_template_line_id") Integer sale_order_template_line_id) {
        Sale_order_template_lineDTO sale_order_template_linedto = new Sale_order_template_lineDTO();
		Sale_order_template_line domain = new Sale_order_template_line();
		sale_order_template_linedto.setId(sale_order_template_line_id);
		domain.setId(sale_order_template_line_id);
        Boolean rst = sale_order_template_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Sale_order_template_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_template_lines/createBatch")
    public ResponseEntity<Boolean> createBatchSale_order_template_line(@RequestBody List<Sale_order_template_lineDTO> sale_order_template_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Sale_order_template_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_template_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_order_template_lineDTO> sale_order_template_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Sale_order_template_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_template_lines")

    public ResponseEntity<Sale_order_template_lineDTO> create(@RequestBody Sale_order_template_lineDTO sale_order_template_linedto) {
        Sale_order_template_lineDTO dto = new Sale_order_template_lineDTO();
        Sale_order_template_line domain = sale_order_template_linedto.toDO();
		sale_order_template_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Sale_order_template_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_template_lines/{sale_order_template_line_id}")
    public ResponseEntity<Sale_order_template_lineDTO> get(@PathVariable("sale_order_template_line_id") Integer sale_order_template_line_id) {
        Sale_order_template_lineDTO dto = new Sale_order_template_lineDTO();
        Sale_order_template_line domain = sale_order_template_lineService.get(sale_order_template_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Sale_order_template_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_template_lines/{sale_order_template_line_id}")

    public ResponseEntity<Sale_order_template_lineDTO> update(@PathVariable("sale_order_template_line_id") Integer sale_order_template_line_id, @RequestBody Sale_order_template_lineDTO sale_order_template_linedto) {
		Sale_order_template_line domain = sale_order_template_linedto.toDO();
        domain.setId(sale_order_template_line_id);
		sale_order_template_lineService.update(domain);
		Sale_order_template_lineDTO dto = new Sale_order_template_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Sale_order_template_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_template_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sale_order_template_lineDTO> sale_order_template_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sale_order_template_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_sale/sale_order_template_lines/fetchdefault")
	public ResponseEntity<Page<Sale_order_template_lineDTO>> fetchDefault(Sale_order_template_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sale_order_template_lineDTO> list = new ArrayList<Sale_order_template_lineDTO>();
        
        Page<Sale_order_template_line> domains = sale_order_template_lineService.searchDefault(context) ;
        for(Sale_order_template_line sale_order_template_line : domains.getContent()){
            Sale_order_template_lineDTO dto = new Sale_order_template_lineDTO();
            dto.fromDO(sale_order_template_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
