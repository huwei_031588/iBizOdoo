package cn.ibizlab.odoo.service.odoo_sale.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_sale.dto.Sale_order_templateDTO;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_templateService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_templateSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Sale_order_template" })
@RestController
@RequestMapping("")
public class Sale_order_templateResource {

    @Autowired
    private ISale_order_templateService sale_order_templateService;

    public ISale_order_templateService getSale_order_templateService() {
        return this.sale_order_templateService;
    }

    @ApiOperation(value = "建立数据", tags = {"Sale_order_template" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_templates")

    public ResponseEntity<Sale_order_templateDTO> create(@RequestBody Sale_order_templateDTO sale_order_templatedto) {
        Sale_order_templateDTO dto = new Sale_order_templateDTO();
        Sale_order_template domain = sale_order_templatedto.toDO();
		sale_order_templateService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Sale_order_template" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_templates/{sale_order_template_id}")

    public ResponseEntity<Sale_order_templateDTO> update(@PathVariable("sale_order_template_id") Integer sale_order_template_id, @RequestBody Sale_order_templateDTO sale_order_templatedto) {
		Sale_order_template domain = sale_order_templatedto.toDO();
        domain.setId(sale_order_template_id);
		sale_order_templateService.update(domain);
		Sale_order_templateDTO dto = new Sale_order_templateDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Sale_order_template" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_order_templates/{sale_order_template_id}")
    public ResponseEntity<Sale_order_templateDTO> get(@PathVariable("sale_order_template_id") Integer sale_order_template_id) {
        Sale_order_templateDTO dto = new Sale_order_templateDTO();
        Sale_order_template domain = sale_order_templateService.get(sale_order_template_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Sale_order_template" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_templates/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sale_order_templateDTO> sale_order_templatedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Sale_order_template" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_order_templates/createBatch")
    public ResponseEntity<Boolean> createBatchSale_order_template(@RequestBody List<Sale_order_templateDTO> sale_order_templatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Sale_order_template" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_order_templates/{sale_order_template_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_template_id") Integer sale_order_template_id) {
        Sale_order_templateDTO sale_order_templatedto = new Sale_order_templateDTO();
		Sale_order_template domain = new Sale_order_template();
		sale_order_templatedto.setId(sale_order_template_id);
		domain.setId(sale_order_template_id);
        Boolean rst = sale_order_templateService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Sale_order_template" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_order_templates/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_order_templateDTO> sale_order_templatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sale_order_template" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_sale/sale_order_templates/fetchdefault")
	public ResponseEntity<Page<Sale_order_templateDTO>> fetchDefault(Sale_order_templateSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sale_order_templateDTO> list = new ArrayList<Sale_order_templateDTO>();
        
        Page<Sale_order_template> domains = sale_order_templateService.searchDefault(context) ;
        for(Sale_order_template sale_order_template : domains.getContent()){
            Sale_order_templateDTO dto = new Sale_order_templateDTO();
            dto.fromDO(sale_order_template);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
