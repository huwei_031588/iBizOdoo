package cn.ibizlab.odoo.service.odoo_sale.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_order_template.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Sale_order_templateDTO]
 */
public class Sale_order_templateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @Sale_order_templateNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [REQUIRE_SIGNATURE]
     *
     */
    @Sale_order_templateRequire_signatureDefault(info = "默认规则")
    private String require_signature;

    @JsonIgnore
    private boolean require_signatureDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Sale_order_templateDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [REQUIRE_PAYMENT]
     *
     */
    @Sale_order_templateRequire_paymentDefault(info = "默认规则")
    private String require_payment;

    @JsonIgnore
    private boolean require_paymentDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Sale_order_templateCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [NUMBER_OF_DAYS]
     *
     */
    @Sale_order_templateNumber_of_daysDefault(info = "默认规则")
    private Integer number_of_days;

    @JsonIgnore
    private boolean number_of_daysDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Sale_order_template__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Sale_order_templateNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Sale_order_templateIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_OPTION_IDS]
     *
     */
    @Sale_order_templateSale_order_template_option_idsDefault(info = "默认规则")
    private String sale_order_template_option_ids;

    @JsonIgnore
    private boolean sale_order_template_option_idsDirtyFlag;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_LINE_IDS]
     *
     */
    @Sale_order_templateSale_order_template_line_idsDefault(info = "默认规则")
    private String sale_order_template_line_ids;

    @JsonIgnore
    private boolean sale_order_template_line_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Sale_order_templateWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Sale_order_templateActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Sale_order_templateCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [MAIL_TEMPLATE_ID_TEXT]
     *
     */
    @Sale_order_templateMail_template_id_textDefault(info = "默认规则")
    private String mail_template_id_text;

    @JsonIgnore
    private boolean mail_template_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Sale_order_templateWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Sale_order_templateWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [MAIL_TEMPLATE_ID]
     *
     */
    @Sale_order_templateMail_template_idDefault(info = "默认规则")
    private Integer mail_template_id;

    @JsonIgnore
    private boolean mail_template_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Sale_order_templateCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [REQUIRE_SIGNATURE]
     */
    @JsonProperty("require_signature")
    public String getRequire_signature(){
        return require_signature ;
    }

    /**
     * 设置 [REQUIRE_SIGNATURE]
     */
    @JsonProperty("require_signature")
    public void setRequire_signature(String  require_signature){
        this.require_signature = require_signature ;
        this.require_signatureDirtyFlag = true ;
    }

    /**
     * 获取 [REQUIRE_SIGNATURE]脏标记
     */
    @JsonIgnore
    public boolean getRequire_signatureDirtyFlag(){
        return require_signatureDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [REQUIRE_PAYMENT]
     */
    @JsonProperty("require_payment")
    public String getRequire_payment(){
        return require_payment ;
    }

    /**
     * 设置 [REQUIRE_PAYMENT]
     */
    @JsonProperty("require_payment")
    public void setRequire_payment(String  require_payment){
        this.require_payment = require_payment ;
        this.require_paymentDirtyFlag = true ;
    }

    /**
     * 获取 [REQUIRE_PAYMENT]脏标记
     */
    @JsonIgnore
    public boolean getRequire_paymentDirtyFlag(){
        return require_paymentDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [NUMBER_OF_DAYS]
     */
    @JsonProperty("number_of_days")
    public Integer getNumber_of_days(){
        return number_of_days ;
    }

    /**
     * 设置 [NUMBER_OF_DAYS]
     */
    @JsonProperty("number_of_days")
    public void setNumber_of_days(Integer  number_of_days){
        this.number_of_days = number_of_days ;
        this.number_of_daysDirtyFlag = true ;
    }

    /**
     * 获取 [NUMBER_OF_DAYS]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_daysDirtyFlag(){
        return number_of_daysDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_OPTION_IDS]
     */
    @JsonProperty("sale_order_template_option_ids")
    public String getSale_order_template_option_ids(){
        return sale_order_template_option_ids ;
    }

    /**
     * 设置 [SALE_ORDER_TEMPLATE_OPTION_IDS]
     */
    @JsonProperty("sale_order_template_option_ids")
    public void setSale_order_template_option_ids(String  sale_order_template_option_ids){
        this.sale_order_template_option_ids = sale_order_template_option_ids ;
        this.sale_order_template_option_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_OPTION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_option_idsDirtyFlag(){
        return sale_order_template_option_idsDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_LINE_IDS]
     */
    @JsonProperty("sale_order_template_line_ids")
    public String getSale_order_template_line_ids(){
        return sale_order_template_line_ids ;
    }

    /**
     * 设置 [SALE_ORDER_TEMPLATE_LINE_IDS]
     */
    @JsonProperty("sale_order_template_line_ids")
    public void setSale_order_template_line_ids(String  sale_order_template_line_ids){
        this.sale_order_template_line_ids = sale_order_template_line_ids ;
        this.sale_order_template_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_line_idsDirtyFlag(){
        return sale_order_template_line_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("mail_template_id_text")
    public String getMail_template_id_text(){
        return mail_template_id_text ;
    }

    /**
     * 设置 [MAIL_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("mail_template_id_text")
    public void setMail_template_id_text(String  mail_template_id_text){
        this.mail_template_id_text = mail_template_id_text ;
        this.mail_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_id_textDirtyFlag(){
        return mail_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_ID]
     */
    @JsonProperty("mail_template_id")
    public Integer getMail_template_id(){
        return mail_template_id ;
    }

    /**
     * 设置 [MAIL_TEMPLATE_ID]
     */
    @JsonProperty("mail_template_id")
    public void setMail_template_id(Integer  mail_template_id){
        this.mail_template_id = mail_template_id ;
        this.mail_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_idDirtyFlag(){
        return mail_template_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Sale_order_template toDO() {
        Sale_order_template srfdomain = new Sale_order_template();
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getRequire_signatureDirtyFlag())
            srfdomain.setRequire_signature(require_signature);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getRequire_paymentDirtyFlag())
            srfdomain.setRequire_payment(require_payment);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getNumber_of_daysDirtyFlag())
            srfdomain.setNumber_of_days(number_of_days);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getSale_order_template_option_idsDirtyFlag())
            srfdomain.setSale_order_template_option_ids(sale_order_template_option_ids);
        if(getSale_order_template_line_idsDirtyFlag())
            srfdomain.setSale_order_template_line_ids(sale_order_template_line_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getMail_template_id_textDirtyFlag())
            srfdomain.setMail_template_id_text(mail_template_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getMail_template_idDirtyFlag())
            srfdomain.setMail_template_id(mail_template_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Sale_order_template srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getRequire_signatureDirtyFlag())
            this.setRequire_signature(srfdomain.getRequire_signature());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getRequire_paymentDirtyFlag())
            this.setRequire_payment(srfdomain.getRequire_payment());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getNumber_of_daysDirtyFlag())
            this.setNumber_of_days(srfdomain.getNumber_of_days());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getSale_order_template_option_idsDirtyFlag())
            this.setSale_order_template_option_ids(srfdomain.getSale_order_template_option_ids());
        if(srfdomain.getSale_order_template_line_idsDirtyFlag())
            this.setSale_order_template_line_ids(srfdomain.getSale_order_template_line_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getMail_template_id_textDirtyFlag())
            this.setMail_template_id_text(srfdomain.getMail_template_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getMail_template_idDirtyFlag())
            this.setMail_template_id(srfdomain.getMail_template_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Sale_order_templateDTO> fromDOPage(List<Sale_order_template> poPage)   {
        if(poPage == null)
            return null;
        List<Sale_order_templateDTO> dtos=new ArrayList<Sale_order_templateDTO>();
        for(Sale_order_template domain : poPage) {
            Sale_order_templateDTO dto = new Sale_order_templateDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

