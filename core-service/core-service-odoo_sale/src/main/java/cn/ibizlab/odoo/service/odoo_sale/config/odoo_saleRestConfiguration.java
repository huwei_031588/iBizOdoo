package cn.ibizlab.odoo.service.odoo_sale.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_sale")
public class odoo_saleRestConfiguration {

}
