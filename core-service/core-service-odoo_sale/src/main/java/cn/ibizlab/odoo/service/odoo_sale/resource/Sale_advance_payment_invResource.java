package cn.ibizlab.odoo.service.odoo_sale.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_sale.dto.Sale_advance_payment_invDTO;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_advance_payment_inv;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_advance_payment_invService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Sale_advance_payment_inv" })
@RestController
@RequestMapping("")
public class Sale_advance_payment_invResource {

    @Autowired
    private ISale_advance_payment_invService sale_advance_payment_invService;

    public ISale_advance_payment_invService getSale_advance_payment_invService() {
        return this.sale_advance_payment_invService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Sale_advance_payment_inv" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_advance_payment_invs/createBatch")
    public ResponseEntity<Boolean> createBatchSale_advance_payment_inv(@RequestBody List<Sale_advance_payment_invDTO> sale_advance_payment_invdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Sale_advance_payment_inv" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_advance_payment_invs/{sale_advance_payment_inv_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_advance_payment_inv_id") Integer sale_advance_payment_inv_id) {
        Sale_advance_payment_invDTO sale_advance_payment_invdto = new Sale_advance_payment_invDTO();
		Sale_advance_payment_inv domain = new Sale_advance_payment_inv();
		sale_advance_payment_invdto.setId(sale_advance_payment_inv_id);
		domain.setId(sale_advance_payment_inv_id);
        Boolean rst = sale_advance_payment_invService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Sale_advance_payment_inv" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_advance_payment_invs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_advance_payment_invDTO> sale_advance_payment_invdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Sale_advance_payment_inv" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_advance_payment_invs/{sale_advance_payment_inv_id}")

    public ResponseEntity<Sale_advance_payment_invDTO> update(@PathVariable("sale_advance_payment_inv_id") Integer sale_advance_payment_inv_id, @RequestBody Sale_advance_payment_invDTO sale_advance_payment_invdto) {
		Sale_advance_payment_inv domain = sale_advance_payment_invdto.toDO();
        domain.setId(sale_advance_payment_inv_id);
		sale_advance_payment_invService.update(domain);
		Sale_advance_payment_invDTO dto = new Sale_advance_payment_invDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Sale_advance_payment_inv" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_advance_payment_invs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sale_advance_payment_invDTO> sale_advance_payment_invdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Sale_advance_payment_inv" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_advance_payment_invs/{sale_advance_payment_inv_id}")
    public ResponseEntity<Sale_advance_payment_invDTO> get(@PathVariable("sale_advance_payment_inv_id") Integer sale_advance_payment_inv_id) {
        Sale_advance_payment_invDTO dto = new Sale_advance_payment_invDTO();
        Sale_advance_payment_inv domain = sale_advance_payment_invService.get(sale_advance_payment_inv_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Sale_advance_payment_inv" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_advance_payment_invs")

    public ResponseEntity<Sale_advance_payment_invDTO> create(@RequestBody Sale_advance_payment_invDTO sale_advance_payment_invdto) {
        Sale_advance_payment_invDTO dto = new Sale_advance_payment_invDTO();
        Sale_advance_payment_inv domain = sale_advance_payment_invdto.toDO();
		sale_advance_payment_invService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sale_advance_payment_inv" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_sale/sale_advance_payment_invs/fetchdefault")
	public ResponseEntity<Page<Sale_advance_payment_invDTO>> fetchDefault(Sale_advance_payment_invSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sale_advance_payment_invDTO> list = new ArrayList<Sale_advance_payment_invDTO>();
        
        Page<Sale_advance_payment_inv> domains = sale_advance_payment_invService.searchDefault(context) ;
        for(Sale_advance_payment_inv sale_advance_payment_inv : domains.getContent()){
            Sale_advance_payment_invDTO dto = new Sale_advance_payment_invDTO();
            dto.fromDO(sale_advance_payment_inv);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
