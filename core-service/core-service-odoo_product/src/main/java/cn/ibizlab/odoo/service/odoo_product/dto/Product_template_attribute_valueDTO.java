package cn.ibizlab.odoo.service.odoo_product.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_template_attribute_value.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_value;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Product_template_attribute_valueDTO]
 */
public class Product_template_attribute_valueDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Product_template_attribute_valueCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [EXCLUDE_FOR]
     *
     */
    @Product_template_attribute_valueExclude_forDefault(info = "默认规则")
    private String exclude_for;

    @JsonIgnore
    private boolean exclude_forDirtyFlag;

    /**
     * 属性 [PRICE_EXTRA]
     *
     */
    @Product_template_attribute_valuePrice_extraDefault(info = "默认规则")
    private Double price_extra;

    @JsonIgnore
    private boolean price_extraDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Product_template_attribute_value__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Product_template_attribute_valueDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Product_template_attribute_valueIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Product_template_attribute_valueWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Product_template_attribute_valueNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ATTRIBUTE_ID]
     *
     */
    @Product_template_attribute_valueAttribute_idDefault(info = "默认规则")
    private Integer attribute_id;

    @JsonIgnore
    private boolean attribute_idDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Product_template_attribute_valueSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID_TEXT]
     *
     */
    @Product_template_attribute_valueProduct_tmpl_id_textDefault(info = "默认规则")
    private String product_tmpl_id_text;

    @JsonIgnore
    private boolean product_tmpl_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Product_template_attribute_valueCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [HTML_COLOR]
     *
     */
    @Product_template_attribute_valueHtml_colorDefault(info = "默认规则")
    private String html_color;

    @JsonIgnore
    private boolean html_colorDirtyFlag;

    /**
     * 属性 [IS_CUSTOM]
     *
     */
    @Product_template_attribute_valueIs_customDefault(info = "默认规则")
    private String is_custom;

    @JsonIgnore
    private boolean is_customDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Product_template_attribute_valueWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ATTRIBUTE_VALUE_ID]
     *
     */
    @Product_template_attribute_valueProduct_attribute_value_idDefault(info = "默认规则")
    private Integer product_attribute_value_id;

    @JsonIgnore
    private boolean product_attribute_value_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Product_template_attribute_valueCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @Product_template_attribute_valueProduct_tmpl_idDefault(info = "默认规则")
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Product_template_attribute_valueWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [EXCLUDE_FOR]
     */
    @JsonProperty("exclude_for")
    public String getExclude_for(){
        return exclude_for ;
    }

    /**
     * 设置 [EXCLUDE_FOR]
     */
    @JsonProperty("exclude_for")
    public void setExclude_for(String  exclude_for){
        this.exclude_for = exclude_for ;
        this.exclude_forDirtyFlag = true ;
    }

    /**
     * 获取 [EXCLUDE_FOR]脏标记
     */
    @JsonIgnore
    public boolean getExclude_forDirtyFlag(){
        return exclude_forDirtyFlag ;
    }

    /**
     * 获取 [PRICE_EXTRA]
     */
    @JsonProperty("price_extra")
    public Double getPrice_extra(){
        return price_extra ;
    }

    /**
     * 设置 [PRICE_EXTRA]
     */
    @JsonProperty("price_extra")
    public void setPrice_extra(Double  price_extra){
        this.price_extra = price_extra ;
        this.price_extraDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_EXTRA]脏标记
     */
    @JsonIgnore
    public boolean getPrice_extraDirtyFlag(){
        return price_extraDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ATTRIBUTE_ID]
     */
    @JsonProperty("attribute_id")
    public Integer getAttribute_id(){
        return attribute_id ;
    }

    /**
     * 设置 [ATTRIBUTE_ID]
     */
    @JsonProperty("attribute_id")
    public void setAttribute_id(Integer  attribute_id){
        this.attribute_id = attribute_id ;
        this.attribute_idDirtyFlag = true ;
    }

    /**
     * 获取 [ATTRIBUTE_ID]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_idDirtyFlag(){
        return attribute_idDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return product_tmpl_id_text ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return product_tmpl_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [HTML_COLOR]
     */
    @JsonProperty("html_color")
    public String getHtml_color(){
        return html_color ;
    }

    /**
     * 设置 [HTML_COLOR]
     */
    @JsonProperty("html_color")
    public void setHtml_color(String  html_color){
        this.html_color = html_color ;
        this.html_colorDirtyFlag = true ;
    }

    /**
     * 获取 [HTML_COLOR]脏标记
     */
    @JsonIgnore
    public boolean getHtml_colorDirtyFlag(){
        return html_colorDirtyFlag ;
    }

    /**
     * 获取 [IS_CUSTOM]
     */
    @JsonProperty("is_custom")
    public String getIs_custom(){
        return is_custom ;
    }

    /**
     * 设置 [IS_CUSTOM]
     */
    @JsonProperty("is_custom")
    public void setIs_custom(String  is_custom){
        this.is_custom = is_custom ;
        this.is_customDirtyFlag = true ;
    }

    /**
     * 获取 [IS_CUSTOM]脏标记
     */
    @JsonIgnore
    public boolean getIs_customDirtyFlag(){
        return is_customDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ATTRIBUTE_VALUE_ID]
     */
    @JsonProperty("product_attribute_value_id")
    public Integer getProduct_attribute_value_id(){
        return product_attribute_value_id ;
    }

    /**
     * 设置 [PRODUCT_ATTRIBUTE_VALUE_ID]
     */
    @JsonProperty("product_attribute_value_id")
    public void setProduct_attribute_value_id(Integer  product_attribute_value_id){
        this.product_attribute_value_id = product_attribute_value_id ;
        this.product_attribute_value_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ATTRIBUTE_VALUE_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_attribute_value_idDirtyFlag(){
        return product_attribute_value_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return product_tmpl_id ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return product_tmpl_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Product_template_attribute_value toDO() {
        Product_template_attribute_value srfdomain = new Product_template_attribute_value();
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getExclude_forDirtyFlag())
            srfdomain.setExclude_for(exclude_for);
        if(getPrice_extraDirtyFlag())
            srfdomain.setPrice_extra(price_extra);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getAttribute_idDirtyFlag())
            srfdomain.setAttribute_id(attribute_id);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getProduct_tmpl_id_textDirtyFlag())
            srfdomain.setProduct_tmpl_id_text(product_tmpl_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getHtml_colorDirtyFlag())
            srfdomain.setHtml_color(html_color);
        if(getIs_customDirtyFlag())
            srfdomain.setIs_custom(is_custom);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getProduct_attribute_value_idDirtyFlag())
            srfdomain.setProduct_attribute_value_id(product_attribute_value_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getProduct_tmpl_idDirtyFlag())
            srfdomain.setProduct_tmpl_id(product_tmpl_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Product_template_attribute_value srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getExclude_forDirtyFlag())
            this.setExclude_for(srfdomain.getExclude_for());
        if(srfdomain.getPrice_extraDirtyFlag())
            this.setPrice_extra(srfdomain.getPrice_extra());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getAttribute_idDirtyFlag())
            this.setAttribute_id(srfdomain.getAttribute_id());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getProduct_tmpl_id_textDirtyFlag())
            this.setProduct_tmpl_id_text(srfdomain.getProduct_tmpl_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getHtml_colorDirtyFlag())
            this.setHtml_color(srfdomain.getHtml_color());
        if(srfdomain.getIs_customDirtyFlag())
            this.setIs_custom(srfdomain.getIs_custom());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getProduct_attribute_value_idDirtyFlag())
            this.setProduct_attribute_value_id(srfdomain.getProduct_attribute_value_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getProduct_tmpl_idDirtyFlag())
            this.setProduct_tmpl_id(srfdomain.getProduct_tmpl_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Product_template_attribute_valueDTO> fromDOPage(List<Product_template_attribute_value> poPage)   {
        if(poPage == null)
            return null;
        List<Product_template_attribute_valueDTO> dtos=new ArrayList<Product_template_attribute_valueDTO>();
        for(Product_template_attribute_value domain : poPage) {
            Product_template_attribute_valueDTO dto = new Product_template_attribute_valueDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

