package cn.ibizlab.odoo.service.odoo_product.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_attribute.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Product_attributeDTO]
 */
public class Product_attributeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ATTRIBUTE_LINE_IDS]
     *
     */
    @Product_attributeAttribute_line_idsDefault(info = "默认规则")
    private String attribute_line_ids;

    @JsonIgnore
    private boolean attribute_line_idsDirtyFlag;

    /**
     * 属性 [CREATE_VARIANT]
     *
     */
    @Product_attributeCreate_variantDefault(info = "默认规则")
    private String create_variant;

    @JsonIgnore
    private boolean create_variantDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Product_attributeTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Product_attributeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Product_attributeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [VALUE_IDS]
     *
     */
    @Product_attributeValue_idsDefault(info = "默认规则")
    private String value_ids;

    @JsonIgnore
    private boolean value_idsDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Product_attributeSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Product_attribute__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Product_attributeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Product_attributeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Product_attributeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Product_attributeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Product_attributeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Product_attributeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Product_attributeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [ATTRIBUTE_LINE_IDS]
     */
    @JsonProperty("attribute_line_ids")
    public String getAttribute_line_ids(){
        return attribute_line_ids ;
    }

    /**
     * 设置 [ATTRIBUTE_LINE_IDS]
     */
    @JsonProperty("attribute_line_ids")
    public void setAttribute_line_ids(String  attribute_line_ids){
        this.attribute_line_ids = attribute_line_ids ;
        this.attribute_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTRIBUTE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttribute_line_idsDirtyFlag(){
        return attribute_line_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_VARIANT]
     */
    @JsonProperty("create_variant")
    public String getCreate_variant(){
        return create_variant ;
    }

    /**
     * 设置 [CREATE_VARIANT]
     */
    @JsonProperty("create_variant")
    public void setCreate_variant(String  create_variant){
        this.create_variant = create_variant ;
        this.create_variantDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_VARIANT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_variantDirtyFlag(){
        return create_variantDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [VALUE_IDS]
     */
    @JsonProperty("value_ids")
    public String getValue_ids(){
        return value_ids ;
    }

    /**
     * 设置 [VALUE_IDS]
     */
    @JsonProperty("value_ids")
    public void setValue_ids(String  value_ids){
        this.value_ids = value_ids ;
        this.value_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getValue_idsDirtyFlag(){
        return value_idsDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Product_attribute toDO() {
        Product_attribute srfdomain = new Product_attribute();
        if(getAttribute_line_idsDirtyFlag())
            srfdomain.setAttribute_line_ids(attribute_line_ids);
        if(getCreate_variantDirtyFlag())
            srfdomain.setCreate_variant(create_variant);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getValue_idsDirtyFlag())
            srfdomain.setValue_ids(value_ids);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Product_attribute srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getAttribute_line_idsDirtyFlag())
            this.setAttribute_line_ids(srfdomain.getAttribute_line_ids());
        if(srfdomain.getCreate_variantDirtyFlag())
            this.setCreate_variant(srfdomain.getCreate_variant());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getValue_idsDirtyFlag())
            this.setValue_ids(srfdomain.getValue_ids());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Product_attributeDTO> fromDOPage(List<Product_attribute> poPage)   {
        if(poPage == null)
            return null;
        List<Product_attributeDTO> dtos=new ArrayList<Product_attributeDTO>();
        for(Product_attribute domain : poPage) {
            Product_attributeDTO dto = new Product_attributeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

