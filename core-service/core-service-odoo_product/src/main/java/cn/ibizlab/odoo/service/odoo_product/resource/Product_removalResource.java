package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_removalDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_removal;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_removalService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_removalSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_removal" })
@RestController
@RequestMapping("")
public class Product_removalResource {

    @Autowired
    private IProduct_removalService product_removalService;

    public IProduct_removalService getProduct_removalService() {
        return this.product_removalService;
    }

    @ApiOperation(value = "删除数据", tags = {"Product_removal" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_removals/{product_removal_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_removal_id") Integer product_removal_id) {
        Product_removalDTO product_removaldto = new Product_removalDTO();
		Product_removal domain = new Product_removal();
		product_removaldto.setId(product_removal_id);
		domain.setId(product_removal_id);
        Boolean rst = product_removalService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_removal" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_removals/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_removal(@RequestBody List<Product_removalDTO> product_removaldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_removal" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_removals/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_removalDTO> product_removaldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_removal" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_removals/{product_removal_id}")

    public ResponseEntity<Product_removalDTO> update(@PathVariable("product_removal_id") Integer product_removal_id, @RequestBody Product_removalDTO product_removaldto) {
		Product_removal domain = product_removaldto.toDO();
        domain.setId(product_removal_id);
		product_removalService.update(domain);
		Product_removalDTO dto = new Product_removalDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_removal" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_removals")

    public ResponseEntity<Product_removalDTO> create(@RequestBody Product_removalDTO product_removaldto) {
        Product_removalDTO dto = new Product_removalDTO();
        Product_removal domain = product_removaldto.toDO();
		product_removalService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_removal" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_removals/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_removalDTO> product_removaldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_removal" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_removals/{product_removal_id}")
    public ResponseEntity<Product_removalDTO> get(@PathVariable("product_removal_id") Integer product_removal_id) {
        Product_removalDTO dto = new Product_removalDTO();
        Product_removal domain = product_removalService.get(product_removal_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_removal" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_removals/fetchdefault")
	public ResponseEntity<Page<Product_removalDTO>> fetchDefault(Product_removalSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_removalDTO> list = new ArrayList<Product_removalDTO>();
        
        Page<Product_removal> domains = product_removalService.searchDefault(context) ;
        for(Product_removal product_removal : domains.getContent()){
            Product_removalDTO dto = new Product_removalDTO();
            dto.fromDO(product_removal);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
