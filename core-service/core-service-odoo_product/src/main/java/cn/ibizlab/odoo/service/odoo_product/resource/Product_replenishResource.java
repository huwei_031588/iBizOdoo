package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_replenishDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_replenishService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_replenishSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_replenish" })
@RestController
@RequestMapping("")
public class Product_replenishResource {

    @Autowired
    private IProduct_replenishService product_replenishService;

    public IProduct_replenishService getProduct_replenishService() {
        return this.product_replenishService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_replenish" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_replenishes/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_replenish(@RequestBody List<Product_replenishDTO> product_replenishdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_replenish" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_replenishes/{product_replenish_id}")
    public ResponseEntity<Product_replenishDTO> get(@PathVariable("product_replenish_id") Integer product_replenish_id) {
        Product_replenishDTO dto = new Product_replenishDTO();
        Product_replenish domain = product_replenishService.get(product_replenish_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_replenish" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_replenishes/{product_replenish_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_replenish_id") Integer product_replenish_id) {
        Product_replenishDTO product_replenishdto = new Product_replenishDTO();
		Product_replenish domain = new Product_replenish();
		product_replenishdto.setId(product_replenish_id);
		domain.setId(product_replenish_id);
        Boolean rst = product_replenishService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Product_replenish" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_replenishes")

    public ResponseEntity<Product_replenishDTO> create(@RequestBody Product_replenishDTO product_replenishdto) {
        Product_replenishDTO dto = new Product_replenishDTO();
        Product_replenish domain = product_replenishdto.toDO();
		product_replenishService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_replenish" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_replenishes/{product_replenish_id}")

    public ResponseEntity<Product_replenishDTO> update(@PathVariable("product_replenish_id") Integer product_replenish_id, @RequestBody Product_replenishDTO product_replenishdto) {
		Product_replenish domain = product_replenishdto.toDO();
        domain.setId(product_replenish_id);
		product_replenishService.update(domain);
		Product_replenishDTO dto = new Product_replenishDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_replenish" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_replenishes/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_replenishDTO> product_replenishdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_replenish" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_replenishes/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_replenishDTO> product_replenishdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_replenish" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_replenishes/fetchdefault")
	public ResponseEntity<Page<Product_replenishDTO>> fetchDefault(Product_replenishSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_replenishDTO> list = new ArrayList<Product_replenishDTO>();
        
        Page<Product_replenish> domains = product_replenishService.searchDefault(context) ;
        for(Product_replenish product_replenish : domains.getContent()){
            Product_replenishDTO dto = new Product_replenishDTO();
            dto.fromDO(product_replenish);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
