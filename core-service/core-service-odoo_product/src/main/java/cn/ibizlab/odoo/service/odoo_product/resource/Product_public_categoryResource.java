package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_public_categoryDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_public_categoryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_public_categorySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_public_category" })
@RestController
@RequestMapping("")
public class Product_public_categoryResource {

    @Autowired
    private IProduct_public_categoryService product_public_categoryService;

    public IProduct_public_categoryService getProduct_public_categoryService() {
        return this.product_public_categoryService;
    }

    @ApiOperation(value = "获取数据", tags = {"Product_public_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_public_categories/{product_public_category_id}")
    public ResponseEntity<Product_public_categoryDTO> get(@PathVariable("product_public_category_id") Integer product_public_category_id) {
        Product_public_categoryDTO dto = new Product_public_categoryDTO();
        Product_public_category domain = product_public_categoryService.get(product_public_category_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_public_category" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_public_categories/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_public_category(@RequestBody List<Product_public_categoryDTO> product_public_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_public_category" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_public_categories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_public_categoryDTO> product_public_categorydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_public_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_public_categories")

    public ResponseEntity<Product_public_categoryDTO> create(@RequestBody Product_public_categoryDTO product_public_categorydto) {
        Product_public_categoryDTO dto = new Product_public_categoryDTO();
        Product_public_category domain = product_public_categorydto.toDO();
		product_public_categoryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_public_category" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_public_categories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_public_categoryDTO> product_public_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_public_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_public_categories/{product_public_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_public_category_id") Integer product_public_category_id) {
        Product_public_categoryDTO product_public_categorydto = new Product_public_categoryDTO();
		Product_public_category domain = new Product_public_category();
		product_public_categorydto.setId(product_public_category_id);
		domain.setId(product_public_category_id);
        Boolean rst = product_public_categoryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Product_public_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_public_categories/{product_public_category_id}")

    public ResponseEntity<Product_public_categoryDTO> update(@PathVariable("product_public_category_id") Integer product_public_category_id, @RequestBody Product_public_categoryDTO product_public_categorydto) {
		Product_public_category domain = product_public_categorydto.toDO();
        domain.setId(product_public_category_id);
		product_public_categoryService.update(domain);
		Product_public_categoryDTO dto = new Product_public_categoryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_public_category" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_public_categories/fetchdefault")
	public ResponseEntity<Page<Product_public_categoryDTO>> fetchDefault(Product_public_categorySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_public_categoryDTO> list = new ArrayList<Product_public_categoryDTO>();
        
        Page<Product_public_category> domains = product_public_categoryService.searchDefault(context) ;
        for(Product_public_category product_public_category : domains.getContent()){
            Product_public_categoryDTO dto = new Product_public_categoryDTO();
            dto.fromDO(product_public_category);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
