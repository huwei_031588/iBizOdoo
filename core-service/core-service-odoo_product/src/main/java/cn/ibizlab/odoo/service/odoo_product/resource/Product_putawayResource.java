package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_putawayDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_putawayService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_putawaySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_putaway" })
@RestController
@RequestMapping("")
public class Product_putawayResource {

    @Autowired
    private IProduct_putawayService product_putawayService;

    public IProduct_putawayService getProduct_putawayService() {
        return this.product_putawayService;
    }

    @ApiOperation(value = "删除数据", tags = {"Product_putaway" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_putaways/{product_putaway_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_putaway_id") Integer product_putaway_id) {
        Product_putawayDTO product_putawaydto = new Product_putawayDTO();
		Product_putaway domain = new Product_putaway();
		product_putawaydto.setId(product_putaway_id);
		domain.setId(product_putaway_id);
        Boolean rst = product_putawayService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_putaway" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_putaways/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_putawayDTO> product_putawaydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_putaway" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_putaways/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_putaway(@RequestBody List<Product_putawayDTO> product_putawaydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_putaway" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_putaways")

    public ResponseEntity<Product_putawayDTO> create(@RequestBody Product_putawayDTO product_putawaydto) {
        Product_putawayDTO dto = new Product_putawayDTO();
        Product_putaway domain = product_putawaydto.toDO();
		product_putawayService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_putaway" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_putaways/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_putawayDTO> product_putawaydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_putaway" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_putaways/{product_putaway_id}")
    public ResponseEntity<Product_putawayDTO> get(@PathVariable("product_putaway_id") Integer product_putaway_id) {
        Product_putawayDTO dto = new Product_putawayDTO();
        Product_putaway domain = product_putawayService.get(product_putaway_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_putaway" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_putaways/{product_putaway_id}")

    public ResponseEntity<Product_putawayDTO> update(@PathVariable("product_putaway_id") Integer product_putaway_id, @RequestBody Product_putawayDTO product_putawaydto) {
		Product_putaway domain = product_putawaydto.toDO();
        domain.setId(product_putaway_id);
		product_putawayService.update(domain);
		Product_putawayDTO dto = new Product_putawayDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_putaway" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_putaways/fetchdefault")
	public ResponseEntity<Page<Product_putawayDTO>> fetchDefault(Product_putawaySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_putawayDTO> list = new ArrayList<Product_putawayDTO>();
        
        Page<Product_putaway> domains = product_putawayService.searchDefault(context) ;
        for(Product_putaway product_putaway : domains.getContent()){
            Product_putawayDTO dto = new Product_putawayDTO();
            dto.fromDO(product_putaway);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
