package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_supplierinfoDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_supplierinfo;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_supplierinfoService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_supplierinfoSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_supplierinfo" })
@RestController
@RequestMapping("")
public class Product_supplierinfoResource {

    @Autowired
    private IProduct_supplierinfoService product_supplierinfoService;

    public IProduct_supplierinfoService getProduct_supplierinfoService() {
        return this.product_supplierinfoService;
    }

    @ApiOperation(value = "删除数据", tags = {"Product_supplierinfo" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_supplierinfos/{product_supplierinfo_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_supplierinfo_id") Integer product_supplierinfo_id) {
        Product_supplierinfoDTO product_supplierinfodto = new Product_supplierinfoDTO();
		Product_supplierinfo domain = new Product_supplierinfo();
		product_supplierinfodto.setId(product_supplierinfo_id);
		domain.setId(product_supplierinfo_id);
        Boolean rst = product_supplierinfoService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_supplierinfo" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_supplierinfos/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_supplierinfo" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_supplierinfos")

    public ResponseEntity<Product_supplierinfoDTO> create(@RequestBody Product_supplierinfoDTO product_supplierinfodto) {
        Product_supplierinfoDTO dto = new Product_supplierinfoDTO();
        Product_supplierinfo domain = product_supplierinfodto.toDO();
		product_supplierinfoService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_supplierinfo" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_supplierinfos/{product_supplierinfo_id}")
    public ResponseEntity<Product_supplierinfoDTO> get(@PathVariable("product_supplierinfo_id") Integer product_supplierinfo_id) {
        Product_supplierinfoDTO dto = new Product_supplierinfoDTO();
        Product_supplierinfo domain = product_supplierinfoService.get(product_supplierinfo_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_supplierinfo" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_supplierinfos/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_supplierinfo" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_supplierinfos/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_supplierinfo(@RequestBody List<Product_supplierinfoDTO> product_supplierinfodtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_supplierinfo" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_supplierinfos/{product_supplierinfo_id}")

    public ResponseEntity<Product_supplierinfoDTO> update(@PathVariable("product_supplierinfo_id") Integer product_supplierinfo_id, @RequestBody Product_supplierinfoDTO product_supplierinfodto) {
		Product_supplierinfo domain = product_supplierinfodto.toDO();
        domain.setId(product_supplierinfo_id);
		product_supplierinfoService.update(domain);
		Product_supplierinfoDTO dto = new Product_supplierinfoDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_supplierinfo" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_supplierinfos/fetchdefault")
	public ResponseEntity<Page<Product_supplierinfoDTO>> fetchDefault(Product_supplierinfoSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_supplierinfoDTO> list = new ArrayList<Product_supplierinfoDTO>();
        
        Page<Product_supplierinfo> domains = product_supplierinfoService.searchDefault(context) ;
        for(Product_supplierinfo product_supplierinfo : domains.getContent()){
            Product_supplierinfoDTO dto = new Product_supplierinfoDTO();
            dto.fromDO(product_supplierinfo);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
