package cn.ibizlab.odoo.service.odoo_product.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_public_category.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Product_public_categoryDTO]
 */
public class Product_public_categoryDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @Product_public_categoryImage_mediumDefault(info = "默认规则")
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @Product_public_categoryImage_smallDefault(info = "默认规则")
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Product_public_categoryIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @Product_public_categoryWebsite_meta_titleDefault(info = "默认规则")
    private String website_meta_title;

    @JsonIgnore
    private boolean website_meta_titleDirtyFlag;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @Product_public_categoryIs_seo_optimizedDefault(info = "默认规则")
    private String is_seo_optimized;

    @JsonIgnore
    private boolean is_seo_optimizedDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Product_public_categorySequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @Product_public_categoryWebsite_meta_descriptionDefault(info = "默认规则")
    private String website_meta_description;

    @JsonIgnore
    private boolean website_meta_descriptionDirtyFlag;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @Product_public_categoryWebsite_meta_og_imgDefault(info = "默认规则")
    private String website_meta_og_img;

    @JsonIgnore
    private boolean website_meta_og_imgDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Product_public_categoryDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @Product_public_categoryWebsite_meta_keywordsDefault(info = "默认规则")
    private String website_meta_keywords;

    @JsonIgnore
    private boolean website_meta_keywordsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Product_public_categoryCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Product_public_categoryWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Product_public_category__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Product_public_categoryImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Product_public_categoryWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [CHILD_ID]
     *
     */
    @Product_public_categoryChild_idDefault(info = "默认规则")
    private String child_id;

    @JsonIgnore
    private boolean child_idDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Product_public_categoryNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Product_public_categoryCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Product_public_categoryWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Product_public_categoryParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Product_public_categoryWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Product_public_categoryParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Product_public_categoryCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return image_medium ;
    }

    /**
     * 设置 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return image_mediumDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return image_small ;
    }

    /**
     * 设置 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_SMALL]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return image_smallDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return website_meta_title ;
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return website_meta_titleDirtyFlag ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return is_seo_optimized ;
    }

    /**
     * 设置 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return is_seo_optimizedDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return website_meta_description ;
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return website_meta_descriptionDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return website_meta_og_img ;
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return website_meta_og_imgDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return website_meta_keywords ;
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return website_meta_keywordsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [CHILD_ID]
     */
    @JsonProperty("child_id")
    public String getChild_id(){
        return child_id ;
    }

    /**
     * 设置 [CHILD_ID]
     */
    @JsonProperty("child_id")
    public void setChild_id(String  child_id){
        this.child_id = child_id ;
        this.child_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_ID]脏标记
     */
    @JsonIgnore
    public boolean getChild_idDirtyFlag(){
        return child_idDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Product_public_category toDO() {
        Product_public_category srfdomain = new Product_public_category();
        if(getImage_mediumDirtyFlag())
            srfdomain.setImage_medium(image_medium);
        if(getImage_smallDirtyFlag())
            srfdomain.setImage_small(image_small);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWebsite_meta_titleDirtyFlag())
            srfdomain.setWebsite_meta_title(website_meta_title);
        if(getIs_seo_optimizedDirtyFlag())
            srfdomain.setIs_seo_optimized(is_seo_optimized);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getWebsite_meta_descriptionDirtyFlag())
            srfdomain.setWebsite_meta_description(website_meta_description);
        if(getWebsite_meta_og_imgDirtyFlag())
            srfdomain.setWebsite_meta_og_img(website_meta_og_img);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWebsite_meta_keywordsDirtyFlag())
            srfdomain.setWebsite_meta_keywords(website_meta_keywords);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getChild_idDirtyFlag())
            srfdomain.setChild_id(child_id);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Product_public_category srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getImage_mediumDirtyFlag())
            this.setImage_medium(srfdomain.getImage_medium());
        if(srfdomain.getImage_smallDirtyFlag())
            this.setImage_small(srfdomain.getImage_small());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWebsite_meta_titleDirtyFlag())
            this.setWebsite_meta_title(srfdomain.getWebsite_meta_title());
        if(srfdomain.getIs_seo_optimizedDirtyFlag())
            this.setIs_seo_optimized(srfdomain.getIs_seo_optimized());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getWebsite_meta_descriptionDirtyFlag())
            this.setWebsite_meta_description(srfdomain.getWebsite_meta_description());
        if(srfdomain.getWebsite_meta_og_imgDirtyFlag())
            this.setWebsite_meta_og_img(srfdomain.getWebsite_meta_og_img());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWebsite_meta_keywordsDirtyFlag())
            this.setWebsite_meta_keywords(srfdomain.getWebsite_meta_keywords());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getChild_idDirtyFlag())
            this.setChild_id(srfdomain.getChild_id());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Product_public_categoryDTO> fromDOPage(List<Product_public_category> poPage)   {
        if(poPage == null)
            return null;
        List<Product_public_categoryDTO> dtos=new ArrayList<Product_public_categoryDTO>();
        for(Product_public_category domain : poPage) {
            Product_public_categoryDTO dto = new Product_public_categoryDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

