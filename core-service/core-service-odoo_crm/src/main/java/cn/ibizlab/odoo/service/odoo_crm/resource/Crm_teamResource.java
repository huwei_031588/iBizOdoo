package cn.ibizlab.odoo.service.odoo_crm.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_crm.dto.Crm_teamDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_teamService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_teamSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_team" })
@RestController
@RequestMapping("")
public class Crm_teamResource {

    @Autowired
    private ICrm_teamService crm_teamService;

    public ICrm_teamService getCrm_teamService() {
        return this.crm_teamService;
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_team" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_teams/{crm_team_id}")
    public ResponseEntity<Crm_teamDTO> get(@PathVariable("crm_team_id") Integer crm_team_id) {
        Crm_teamDTO dto = new Crm_teamDTO();
        Crm_team domain = crm_teamService.get(crm_team_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_team" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_teams/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_teamDTO> crm_teamdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_team" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_teams/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_team(@RequestBody List<Crm_teamDTO> crm_teamdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_team" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_teams/{crm_team_id}")

    public ResponseEntity<Crm_teamDTO> update(@PathVariable("crm_team_id") Integer crm_team_id, @RequestBody Crm_teamDTO crm_teamdto) {
		Crm_team domain = crm_teamdto.toDO();
        domain.setId(crm_team_id);
		crm_teamService.update(domain);
		Crm_teamDTO dto = new Crm_teamDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_team" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_teams/{crm_team_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_team_id") Integer crm_team_id) {
        Crm_teamDTO crm_teamdto = new Crm_teamDTO();
		Crm_team domain = new Crm_team();
		crm_teamdto.setId(crm_team_id);
		domain.setId(crm_team_id);
        Boolean rst = crm_teamService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_team" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_teams")

    public ResponseEntity<Crm_teamDTO> create(@RequestBody Crm_teamDTO crm_teamdto) {
        Crm_teamDTO dto = new Crm_teamDTO();
        Crm_team domain = crm_teamdto.toDO();
		crm_teamService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_team" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_teams/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_teamDTO> crm_teamdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_team" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_crm/crm_teams/fetchdefault")
	public ResponseEntity<Page<Crm_teamDTO>> fetchDefault(Crm_teamSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_teamDTO> list = new ArrayList<Crm_teamDTO>();
        
        Page<Crm_team> domains = crm_teamService.searchDefault(context) ;
        for(Crm_team crm_team : domains.getContent()){
            Crm_teamDTO dto = new Crm_teamDTO();
            dto.fromDO(crm_team);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
