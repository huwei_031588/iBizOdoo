package cn.ibizlab.odoo.service.odoo_crm.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_crm.dto.Crm_leadDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_leadService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_leadSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_lead" })
@RestController
@RequestMapping("")
public class Crm_leadResource {

    @Autowired
    private ICrm_leadService crm_leadService;

    public ICrm_leadService getCrm_leadService() {
        return this.crm_leadService;
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_lead" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_leads/{crm_lead_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_id") Integer crm_lead_id) {
        Crm_leadDTO crm_leaddto = new Crm_leadDTO();
		Crm_lead domain = new Crm_lead();
		crm_leaddto.setId(crm_lead_id);
		domain.setId(crm_lead_id);
        Boolean rst = crm_leadService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_lead" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_leads/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_leadDTO> crm_leaddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_lead" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_leads/{crm_lead_id}")
    public ResponseEntity<Crm_leadDTO> get(@PathVariable("crm_lead_id") Integer crm_lead_id) {
        Crm_leadDTO dto = new Crm_leadDTO();
        Crm_lead domain = crm_leadService.get(crm_lead_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_lead" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_leads/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_lead(@RequestBody List<Crm_leadDTO> crm_leaddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_lead" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_leads/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_leadDTO> crm_leaddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_lead" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_leads")

    public ResponseEntity<Crm_leadDTO> create(@RequestBody Crm_leadDTO crm_leaddto) {
        Crm_leadDTO dto = new Crm_leadDTO();
        Crm_lead domain = crm_leaddto.toDO();
		crm_leadService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_lead" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_leads/{crm_lead_id}")

    public ResponseEntity<Crm_leadDTO> update(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_leadDTO crm_leaddto) {
		Crm_lead domain = crm_leaddto.toDO();
        domain.setId(crm_lead_id);
		crm_leadService.update(domain);
		Crm_leadDTO dto = new Crm_leadDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_lead" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_crm/crm_leads/fetchdefault")
	public ResponseEntity<Page<Crm_leadDTO>> fetchDefault(Crm_leadSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_leadDTO> list = new ArrayList<Crm_leadDTO>();
        
        Page<Crm_lead> domains = crm_leadService.searchDefault(context) ;
        for(Crm_lead crm_lead : domains.getContent()){
            Crm_leadDTO dto = new Crm_leadDTO();
            dto.fromDO(crm_lead);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
