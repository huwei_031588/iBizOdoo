package cn.ibizlab.odoo.service.odoo_crm.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_crm.dto.Crm_partner_bindingDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_partner_bindingService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_partner_bindingSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_partner_binding" })
@RestController
@RequestMapping("")
public class Crm_partner_bindingResource {

    @Autowired
    private ICrm_partner_bindingService crm_partner_bindingService;

    public ICrm_partner_bindingService getCrm_partner_bindingService() {
        return this.crm_partner_bindingService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_partner_binding" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_partner_bindings/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_partner_bindingDTO> crm_partner_bindingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_partner_binding" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_partner_bindings/{crm_partner_binding_id}")
    public ResponseEntity<Crm_partner_bindingDTO> get(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id) {
        Crm_partner_bindingDTO dto = new Crm_partner_bindingDTO();
        Crm_partner_binding domain = crm_partner_bindingService.get(crm_partner_binding_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_partner_binding" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_partner_bindings/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_partner_binding(@RequestBody List<Crm_partner_bindingDTO> crm_partner_bindingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_partner_binding" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_partner_bindings/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_partner_bindingDTO> crm_partner_bindingdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_partner_binding" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_partner_bindings/{crm_partner_binding_id}")

    public ResponseEntity<Crm_partner_bindingDTO> update(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id, @RequestBody Crm_partner_bindingDTO crm_partner_bindingdto) {
		Crm_partner_binding domain = crm_partner_bindingdto.toDO();
        domain.setId(crm_partner_binding_id);
		crm_partner_bindingService.update(domain);
		Crm_partner_bindingDTO dto = new Crm_partner_bindingDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_partner_binding" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_partner_bindings")

    public ResponseEntity<Crm_partner_bindingDTO> create(@RequestBody Crm_partner_bindingDTO crm_partner_bindingdto) {
        Crm_partner_bindingDTO dto = new Crm_partner_bindingDTO();
        Crm_partner_binding domain = crm_partner_bindingdto.toDO();
		crm_partner_bindingService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_partner_binding" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_partner_bindings/{crm_partner_binding_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_partner_binding_id") Integer crm_partner_binding_id) {
        Crm_partner_bindingDTO crm_partner_bindingdto = new Crm_partner_bindingDTO();
		Crm_partner_binding domain = new Crm_partner_binding();
		crm_partner_bindingdto.setId(crm_partner_binding_id);
		domain.setId(crm_partner_binding_id);
        Boolean rst = crm_partner_bindingService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_partner_binding" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_crm/crm_partner_bindings/fetchdefault")
	public ResponseEntity<Page<Crm_partner_bindingDTO>> fetchDefault(Crm_partner_bindingSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_partner_bindingDTO> list = new ArrayList<Crm_partner_bindingDTO>();
        
        Page<Crm_partner_binding> domains = crm_partner_bindingService.searchDefault(context) ;
        for(Crm_partner_binding crm_partner_binding : domains.getContent()){
            Crm_partner_bindingDTO dto = new Crm_partner_bindingDTO();
            dto.fromDO(crm_partner_binding);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
