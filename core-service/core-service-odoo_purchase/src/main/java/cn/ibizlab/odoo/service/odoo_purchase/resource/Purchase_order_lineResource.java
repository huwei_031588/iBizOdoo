package cn.ibizlab.odoo.service.odoo_purchase.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_purchase.dto.Purchase_order_lineDTO;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_order_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_order_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Purchase_order_line" })
@RestController
@RequestMapping("")
public class Purchase_order_lineResource {

    @Autowired
    private IPurchase_order_lineService purchase_order_lineService;

    public IPurchase_order_lineService getPurchase_order_lineService() {
        return this.purchase_order_lineService;
    }

    @ApiOperation(value = "建立数据", tags = {"Purchase_order_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_order_lines")

    public ResponseEntity<Purchase_order_lineDTO> create(@RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_lineDTO dto = new Purchase_order_lineDTO();
        Purchase_order_line domain = purchase_order_linedto.toDO();
		purchase_order_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Purchase_order_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_order_lines/{purchase_order_line_id}")

    public ResponseEntity<Purchase_order_lineDTO> update(@PathVariable("purchase_order_line_id") Integer purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
		Purchase_order_line domain = purchase_order_linedto.toDO();
        domain.setId(purchase_order_line_id);
		purchase_order_lineService.update(domain);
		Purchase_order_lineDTO dto = new Purchase_order_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Purchase_order_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_order_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Purchase_order_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_order_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Purchase_order_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> get(@PathVariable("purchase_order_line_id") Integer purchase_order_line_id) {
        Purchase_order_lineDTO dto = new Purchase_order_lineDTO();
        Purchase_order_line domain = purchase_order_lineService.get(purchase_order_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Purchase_order_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_order_lines/{purchase_order_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("purchase_order_line_id") Integer purchase_order_line_id) {
        Purchase_order_lineDTO purchase_order_linedto = new Purchase_order_lineDTO();
		Purchase_order_line domain = new Purchase_order_line();
		purchase_order_linedto.setId(purchase_order_line_id);
		domain.setId(purchase_order_line_id);
        Boolean rst = purchase_order_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Purchase_order_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_order_lines/createBatch")
    public ResponseEntity<Boolean> createBatchPurchase_order_line(@RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Purchase_order_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_purchase/purchase_order_lines/fetchdefault")
	public ResponseEntity<Page<Purchase_order_lineDTO>> fetchDefault(Purchase_order_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Purchase_order_lineDTO> list = new ArrayList<Purchase_order_lineDTO>();
        
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
        for(Purchase_order_line purchase_order_line : domains.getContent()){
            Purchase_order_lineDTO dto = new Purchase_order_lineDTO();
            dto.fromDO(purchase_order_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
