package cn.ibizlab.odoo.service.odoo_purchase.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_purchase.valuerule.anno.purchase_report.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Purchase_reportDTO]
 */
public class Purchase_reportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DELAY_PASS]
     *
     */
    @Purchase_reportDelay_passDefault(info = "默认规则")
    private Double delay_pass;

    @JsonIgnore
    private boolean delay_passDirtyFlag;

    /**
     * 属性 [DATE_APPROVE]
     *
     */
    @Purchase_reportDate_approveDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_approve;

    @JsonIgnore
    private boolean date_approveDirtyFlag;

    /**
     * 属性 [VOLUME]
     *
     */
    @Purchase_reportVolumeDefault(info = "默认规则")
    private Double volume;

    @JsonIgnore
    private boolean volumeDirtyFlag;

    /**
     * 属性 [NBR_LINES]
     *
     */
    @Purchase_reportNbr_linesDefault(info = "默认规则")
    private Integer nbr_lines;

    @JsonIgnore
    private boolean nbr_linesDirtyFlag;

    /**
     * 属性 [NEGOCIATION]
     *
     */
    @Purchase_reportNegociationDefault(info = "默认规则")
    private Double negociation;

    @JsonIgnore
    private boolean negociationDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Purchase_reportStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Purchase_reportDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @Purchase_reportPrice_totalDefault(info = "默认规则")
    private Double price_total;

    @JsonIgnore
    private boolean price_totalDirtyFlag;

    /**
     * 属性 [WEIGHT]
     *
     */
    @Purchase_reportWeightDefault(info = "默认规则")
    private Double weight;

    @JsonIgnore
    private boolean weightDirtyFlag;

    /**
     * 属性 [DATE_ORDER]
     *
     */
    @Purchase_reportDate_orderDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_order;

    @JsonIgnore
    private boolean date_orderDirtyFlag;

    /**
     * 属性 [PRICE_AVERAGE]
     *
     */
    @Purchase_reportPrice_averageDefault(info = "默认规则")
    private Double price_average;

    @JsonIgnore
    private boolean price_averageDirtyFlag;

    /**
     * 属性 [DELAY]
     *
     */
    @Purchase_reportDelayDefault(info = "默认规则")
    private Double delay;

    @JsonIgnore
    private boolean delayDirtyFlag;

    /**
     * 属性 [UNIT_QUANTITY]
     *
     */
    @Purchase_reportUnit_quantityDefault(info = "默认规则")
    private Double unit_quantity;

    @JsonIgnore
    private boolean unit_quantityDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Purchase_reportIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Purchase_report__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PRICE_STANDARD]
     *
     */
    @Purchase_reportPrice_standardDefault(info = "默认规则")
    private Double price_standard;

    @JsonIgnore
    private boolean price_standardDirtyFlag;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @Purchase_reportCategory_id_textDefault(info = "默认规则")
    private String category_id_text;

    @JsonIgnore
    private boolean category_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Purchase_reportCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Purchase_reportPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Purchase_reportCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID_TEXT]
     *
     */
    @Purchase_reportProduct_tmpl_id_textDefault(info = "默认规则")
    private String product_tmpl_id_text;

    @JsonIgnore
    private boolean product_tmpl_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @Purchase_reportCountry_id_textDefault(info = "默认规则")
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @Purchase_reportPicking_type_id_textDefault(info = "默认规则")
    private String picking_type_id_text;

    @JsonIgnore
    private boolean picking_type_id_textDirtyFlag;

    /**
     * 属性 [FISCAL_POSITION_ID_TEXT]
     *
     */
    @Purchase_reportFiscal_position_id_textDefault(info = "默认规则")
    private String fiscal_position_id_text;

    @JsonIgnore
    private boolean fiscal_position_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID_TEXT]
     *
     */
    @Purchase_reportAccount_analytic_id_textDefault(info = "默认规则")
    private String account_analytic_id_text;

    @JsonIgnore
    private boolean account_analytic_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Purchase_reportUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID_TEXT]
     *
     */
    @Purchase_reportCommercial_partner_id_textDefault(info = "默认规则")
    private String commercial_partner_id_text;

    @JsonIgnore
    private boolean commercial_partner_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @Purchase_reportProduct_uom_textDefault(info = "默认规则")
    private String product_uom_text;

    @JsonIgnore
    private boolean product_uom_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Purchase_reportProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Purchase_reportCategory_idDefault(info = "默认规则")
    private Integer category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Purchase_reportCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Purchase_reportCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @Purchase_reportPicking_type_idDefault(info = "默认规则")
    private Integer picking_type_id;

    @JsonIgnore
    private boolean picking_type_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Purchase_reportUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @Purchase_reportCommercial_partner_idDefault(info = "默认规则")
    private Integer commercial_partner_id;

    @JsonIgnore
    private boolean commercial_partner_idDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @Purchase_reportProduct_tmpl_idDefault(info = "默认规则")
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Purchase_reportPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Purchase_reportProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @Purchase_reportProduct_uomDefault(info = "默认规则")
    private Integer product_uom;

    @JsonIgnore
    private boolean product_uomDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Purchase_reportCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;

    /**
     * 属性 [FISCAL_POSITION_ID]
     *
     */
    @Purchase_reportFiscal_position_idDefault(info = "默认规则")
    private Integer fiscal_position_id;

    @JsonIgnore
    private boolean fiscal_position_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID]
     *
     */
    @Purchase_reportAccount_analytic_idDefault(info = "默认规则")
    private Integer account_analytic_id;

    @JsonIgnore
    private boolean account_analytic_idDirtyFlag;


    /**
     * 获取 [DELAY_PASS]
     */
    @JsonProperty("delay_pass")
    public Double getDelay_pass(){
        return delay_pass ;
    }

    /**
     * 设置 [DELAY_PASS]
     */
    @JsonProperty("delay_pass")
    public void setDelay_pass(Double  delay_pass){
        this.delay_pass = delay_pass ;
        this.delay_passDirtyFlag = true ;
    }

    /**
     * 获取 [DELAY_PASS]脏标记
     */
    @JsonIgnore
    public boolean getDelay_passDirtyFlag(){
        return delay_passDirtyFlag ;
    }

    /**
     * 获取 [DATE_APPROVE]
     */
    @JsonProperty("date_approve")
    public Timestamp getDate_approve(){
        return date_approve ;
    }

    /**
     * 设置 [DATE_APPROVE]
     */
    @JsonProperty("date_approve")
    public void setDate_approve(Timestamp  date_approve){
        this.date_approve = date_approve ;
        this.date_approveDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_APPROVE]脏标记
     */
    @JsonIgnore
    public boolean getDate_approveDirtyFlag(){
        return date_approveDirtyFlag ;
    }

    /**
     * 获取 [VOLUME]
     */
    @JsonProperty("volume")
    public Double getVolume(){
        return volume ;
    }

    /**
     * 设置 [VOLUME]
     */
    @JsonProperty("volume")
    public void setVolume(Double  volume){
        this.volume = volume ;
        this.volumeDirtyFlag = true ;
    }

    /**
     * 获取 [VOLUME]脏标记
     */
    @JsonIgnore
    public boolean getVolumeDirtyFlag(){
        return volumeDirtyFlag ;
    }

    /**
     * 获取 [NBR_LINES]
     */
    @JsonProperty("nbr_lines")
    public Integer getNbr_lines(){
        return nbr_lines ;
    }

    /**
     * 设置 [NBR_LINES]
     */
    @JsonProperty("nbr_lines")
    public void setNbr_lines(Integer  nbr_lines){
        this.nbr_lines = nbr_lines ;
        this.nbr_linesDirtyFlag = true ;
    }

    /**
     * 获取 [NBR_LINES]脏标记
     */
    @JsonIgnore
    public boolean getNbr_linesDirtyFlag(){
        return nbr_linesDirtyFlag ;
    }

    /**
     * 获取 [NEGOCIATION]
     */
    @JsonProperty("negociation")
    public Double getNegociation(){
        return negociation ;
    }

    /**
     * 设置 [NEGOCIATION]
     */
    @JsonProperty("negociation")
    public void setNegociation(Double  negociation){
        this.negociation = negociation ;
        this.negociationDirtyFlag = true ;
    }

    /**
     * 获取 [NEGOCIATION]脏标记
     */
    @JsonIgnore
    public boolean getNegociationDirtyFlag(){
        return negociationDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return price_total ;
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return price_totalDirtyFlag ;
    }

    /**
     * 获取 [WEIGHT]
     */
    @JsonProperty("weight")
    public Double getWeight(){
        return weight ;
    }

    /**
     * 设置 [WEIGHT]
     */
    @JsonProperty("weight")
    public void setWeight(Double  weight){
        this.weight = weight ;
        this.weightDirtyFlag = true ;
    }

    /**
     * 获取 [WEIGHT]脏标记
     */
    @JsonIgnore
    public boolean getWeightDirtyFlag(){
        return weightDirtyFlag ;
    }

    /**
     * 获取 [DATE_ORDER]
     */
    @JsonProperty("date_order")
    public Timestamp getDate_order(){
        return date_order ;
    }

    /**
     * 设置 [DATE_ORDER]
     */
    @JsonProperty("date_order")
    public void setDate_order(Timestamp  date_order){
        this.date_order = date_order ;
        this.date_orderDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_ORDER]脏标记
     */
    @JsonIgnore
    public boolean getDate_orderDirtyFlag(){
        return date_orderDirtyFlag ;
    }

    /**
     * 获取 [PRICE_AVERAGE]
     */
    @JsonProperty("price_average")
    public Double getPrice_average(){
        return price_average ;
    }

    /**
     * 设置 [PRICE_AVERAGE]
     */
    @JsonProperty("price_average")
    public void setPrice_average(Double  price_average){
        this.price_average = price_average ;
        this.price_averageDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_AVERAGE]脏标记
     */
    @JsonIgnore
    public boolean getPrice_averageDirtyFlag(){
        return price_averageDirtyFlag ;
    }

    /**
     * 获取 [DELAY]
     */
    @JsonProperty("delay")
    public Double getDelay(){
        return delay ;
    }

    /**
     * 设置 [DELAY]
     */
    @JsonProperty("delay")
    public void setDelay(Double  delay){
        this.delay = delay ;
        this.delayDirtyFlag = true ;
    }

    /**
     * 获取 [DELAY]脏标记
     */
    @JsonIgnore
    public boolean getDelayDirtyFlag(){
        return delayDirtyFlag ;
    }

    /**
     * 获取 [UNIT_QUANTITY]
     */
    @JsonProperty("unit_quantity")
    public Double getUnit_quantity(){
        return unit_quantity ;
    }

    /**
     * 设置 [UNIT_QUANTITY]
     */
    @JsonProperty("unit_quantity")
    public void setUnit_quantity(Double  unit_quantity){
        this.unit_quantity = unit_quantity ;
        this.unit_quantityDirtyFlag = true ;
    }

    /**
     * 获取 [UNIT_QUANTITY]脏标记
     */
    @JsonIgnore
    public boolean getUnit_quantityDirtyFlag(){
        return unit_quantityDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PRICE_STANDARD]
     */
    @JsonProperty("price_standard")
    public Double getPrice_standard(){
        return price_standard ;
    }

    /**
     * 设置 [PRICE_STANDARD]
     */
    @JsonProperty("price_standard")
    public void setPrice_standard(Double  price_standard){
        this.price_standard = price_standard ;
        this.price_standardDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_STANDARD]脏标记
     */
    @JsonIgnore
    public boolean getPrice_standardDirtyFlag(){
        return price_standardDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return category_id_text ;
    }

    /**
     * 设置 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return category_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return product_tmpl_id_text ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return product_tmpl_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return country_id_text ;
    }

    /**
     * 设置 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return country_id_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return picking_type_id_text ;
    }

    /**
     * 设置 [PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return picking_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID_TEXT]
     */
    @JsonProperty("fiscal_position_id_text")
    public String getFiscal_position_id_text(){
        return fiscal_position_id_text ;
    }

    /**
     * 设置 [FISCAL_POSITION_ID_TEXT]
     */
    @JsonProperty("fiscal_position_id_text")
    public void setFiscal_position_id_text(String  fiscal_position_id_text){
        this.fiscal_position_id_text = fiscal_position_id_text ;
        this.fiscal_position_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_id_textDirtyFlag(){
        return fiscal_position_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID_TEXT]
     */
    @JsonProperty("account_analytic_id_text")
    public String getAccount_analytic_id_text(){
        return account_analytic_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID_TEXT]
     */
    @JsonProperty("account_analytic_id_text")
    public void setAccount_analytic_id_text(String  account_analytic_id_text){
        this.account_analytic_id_text = account_analytic_id_text ;
        this.account_analytic_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_id_textDirtyFlag(){
        return account_analytic_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID_TEXT]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return commercial_partner_id_text ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID_TEXT]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return commercial_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return product_uom_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return product_uom_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ID]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return picking_type_id ;
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return picking_type_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return commercial_partner_id ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return commercial_partner_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return product_tmpl_id ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return product_tmpl_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return product_uom ;
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return product_uomDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID]
     */
    @JsonProperty("fiscal_position_id")
    public Integer getFiscal_position_id(){
        return fiscal_position_id ;
    }

    /**
     * 设置 [FISCAL_POSITION_ID]
     */
    @JsonProperty("fiscal_position_id")
    public void setFiscal_position_id(Integer  fiscal_position_id){
        this.fiscal_position_id = fiscal_position_id ;
        this.fiscal_position_idDirtyFlag = true ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_idDirtyFlag(){
        return fiscal_position_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID]
     */
    @JsonProperty("account_analytic_id")
    public Integer getAccount_analytic_id(){
        return account_analytic_id ;
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID]
     */
    @JsonProperty("account_analytic_id")
    public void setAccount_analytic_id(Integer  account_analytic_id){
        this.account_analytic_id = account_analytic_id ;
        this.account_analytic_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_idDirtyFlag(){
        return account_analytic_idDirtyFlag ;
    }



    public Purchase_report toDO() {
        Purchase_report srfdomain = new Purchase_report();
        if(getDelay_passDirtyFlag())
            srfdomain.setDelay_pass(delay_pass);
        if(getDate_approveDirtyFlag())
            srfdomain.setDate_approve(date_approve);
        if(getVolumeDirtyFlag())
            srfdomain.setVolume(volume);
        if(getNbr_linesDirtyFlag())
            srfdomain.setNbr_lines(nbr_lines);
        if(getNegociationDirtyFlag())
            srfdomain.setNegociation(negociation);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getPrice_totalDirtyFlag())
            srfdomain.setPrice_total(price_total);
        if(getWeightDirtyFlag())
            srfdomain.setWeight(weight);
        if(getDate_orderDirtyFlag())
            srfdomain.setDate_order(date_order);
        if(getPrice_averageDirtyFlag())
            srfdomain.setPrice_average(price_average);
        if(getDelayDirtyFlag())
            srfdomain.setDelay(delay);
        if(getUnit_quantityDirtyFlag())
            srfdomain.setUnit_quantity(unit_quantity);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getPrice_standardDirtyFlag())
            srfdomain.setPrice_standard(price_standard);
        if(getCategory_id_textDirtyFlag())
            srfdomain.setCategory_id_text(category_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getProduct_tmpl_id_textDirtyFlag())
            srfdomain.setProduct_tmpl_id_text(product_tmpl_id_text);
        if(getCountry_id_textDirtyFlag())
            srfdomain.setCountry_id_text(country_id_text);
        if(getPicking_type_id_textDirtyFlag())
            srfdomain.setPicking_type_id_text(picking_type_id_text);
        if(getFiscal_position_id_textDirtyFlag())
            srfdomain.setFiscal_position_id_text(fiscal_position_id_text);
        if(getAccount_analytic_id_textDirtyFlag())
            srfdomain.setAccount_analytic_id_text(account_analytic_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getCommercial_partner_id_textDirtyFlag())
            srfdomain.setCommercial_partner_id_text(commercial_partner_id_text);
        if(getProduct_uom_textDirtyFlag())
            srfdomain.setProduct_uom_text(product_uom_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getPicking_type_idDirtyFlag())
            srfdomain.setPicking_type_id(picking_type_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getCommercial_partner_idDirtyFlag())
            srfdomain.setCommercial_partner_id(commercial_partner_id);
        if(getProduct_tmpl_idDirtyFlag())
            srfdomain.setProduct_tmpl_id(product_tmpl_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getProduct_uomDirtyFlag())
            srfdomain.setProduct_uom(product_uom);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);
        if(getFiscal_position_idDirtyFlag())
            srfdomain.setFiscal_position_id(fiscal_position_id);
        if(getAccount_analytic_idDirtyFlag())
            srfdomain.setAccount_analytic_id(account_analytic_id);

        return srfdomain;
    }

    public void fromDO(Purchase_report srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDelay_passDirtyFlag())
            this.setDelay_pass(srfdomain.getDelay_pass());
        if(srfdomain.getDate_approveDirtyFlag())
            this.setDate_approve(srfdomain.getDate_approve());
        if(srfdomain.getVolumeDirtyFlag())
            this.setVolume(srfdomain.getVolume());
        if(srfdomain.getNbr_linesDirtyFlag())
            this.setNbr_lines(srfdomain.getNbr_lines());
        if(srfdomain.getNegociationDirtyFlag())
            this.setNegociation(srfdomain.getNegociation());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getPrice_totalDirtyFlag())
            this.setPrice_total(srfdomain.getPrice_total());
        if(srfdomain.getWeightDirtyFlag())
            this.setWeight(srfdomain.getWeight());
        if(srfdomain.getDate_orderDirtyFlag())
            this.setDate_order(srfdomain.getDate_order());
        if(srfdomain.getPrice_averageDirtyFlag())
            this.setPrice_average(srfdomain.getPrice_average());
        if(srfdomain.getDelayDirtyFlag())
            this.setDelay(srfdomain.getDelay());
        if(srfdomain.getUnit_quantityDirtyFlag())
            this.setUnit_quantity(srfdomain.getUnit_quantity());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getPrice_standardDirtyFlag())
            this.setPrice_standard(srfdomain.getPrice_standard());
        if(srfdomain.getCategory_id_textDirtyFlag())
            this.setCategory_id_text(srfdomain.getCategory_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getProduct_tmpl_id_textDirtyFlag())
            this.setProduct_tmpl_id_text(srfdomain.getProduct_tmpl_id_text());
        if(srfdomain.getCountry_id_textDirtyFlag())
            this.setCountry_id_text(srfdomain.getCountry_id_text());
        if(srfdomain.getPicking_type_id_textDirtyFlag())
            this.setPicking_type_id_text(srfdomain.getPicking_type_id_text());
        if(srfdomain.getFiscal_position_id_textDirtyFlag())
            this.setFiscal_position_id_text(srfdomain.getFiscal_position_id_text());
        if(srfdomain.getAccount_analytic_id_textDirtyFlag())
            this.setAccount_analytic_id_text(srfdomain.getAccount_analytic_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getCommercial_partner_id_textDirtyFlag())
            this.setCommercial_partner_id_text(srfdomain.getCommercial_partner_id_text());
        if(srfdomain.getProduct_uom_textDirtyFlag())
            this.setProduct_uom_text(srfdomain.getProduct_uom_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getPicking_type_idDirtyFlag())
            this.setPicking_type_id(srfdomain.getPicking_type_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getCommercial_partner_idDirtyFlag())
            this.setCommercial_partner_id(srfdomain.getCommercial_partner_id());
        if(srfdomain.getProduct_tmpl_idDirtyFlag())
            this.setProduct_tmpl_id(srfdomain.getProduct_tmpl_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getProduct_uomDirtyFlag())
            this.setProduct_uom(srfdomain.getProduct_uom());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());
        if(srfdomain.getFiscal_position_idDirtyFlag())
            this.setFiscal_position_id(srfdomain.getFiscal_position_id());
        if(srfdomain.getAccount_analytic_idDirtyFlag())
            this.setAccount_analytic_id(srfdomain.getAccount_analytic_id());

    }

    public List<Purchase_reportDTO> fromDOPage(List<Purchase_report> poPage)   {
        if(poPage == null)
            return null;
        List<Purchase_reportDTO> dtos=new ArrayList<Purchase_reportDTO>();
        for(Purchase_report domain : poPage) {
            Purchase_reportDTO dto = new Purchase_reportDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

