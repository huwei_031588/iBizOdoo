package cn.ibizlab.odoo.service.odoo_purchase.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_purchase.dto.Purchase_orderDTO;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_orderService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_orderSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Purchase_order" })
@RestController
@RequestMapping("")
public class Purchase_orderResource {

    @Autowired
    private IPurchase_orderService purchase_orderService;

    public IPurchase_orderService getPurchase_orderService() {
        return this.purchase_orderService;
    }

    @ApiOperation(value = "获取数据", tags = {"Purchase_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_orders/{purchase_order_id}")
    public ResponseEntity<Purchase_orderDTO> get(@PathVariable("purchase_order_id") Integer purchase_order_id) {
        Purchase_orderDTO dto = new Purchase_orderDTO();
        Purchase_order domain = purchase_orderService.get(purchase_order_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Purchase_order" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_orders/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Purchase_order" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_orders/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Purchase_orderDTO> purchase_orderdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Purchase_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_orders/{purchase_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("purchase_order_id") Integer purchase_order_id) {
        Purchase_orderDTO purchase_orderdto = new Purchase_orderDTO();
		Purchase_order domain = new Purchase_order();
		purchase_orderdto.setId(purchase_order_id);
		domain.setId(purchase_order_id);
        Boolean rst = purchase_orderService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Purchase_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_orders/{purchase_order_id}")

    public ResponseEntity<Purchase_orderDTO> update(@PathVariable("purchase_order_id") Integer purchase_order_id, @RequestBody Purchase_orderDTO purchase_orderdto) {
		Purchase_order domain = purchase_orderdto.toDO();
        domain.setId(purchase_order_id);
		purchase_orderService.update(domain);
		Purchase_orderDTO dto = new Purchase_orderDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Purchase_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_orders")

    public ResponseEntity<Purchase_orderDTO> create(@RequestBody Purchase_orderDTO purchase_orderdto) {
        Purchase_orderDTO dto = new Purchase_orderDTO();
        Purchase_order domain = purchase_orderdto.toDO();
		purchase_orderService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Purchase_order" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_orders/createBatch")
    public ResponseEntity<Boolean> createBatchPurchase_order(@RequestBody List<Purchase_orderDTO> purchase_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Purchase_order" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_purchase/purchase_orders/fetchdefault")
	public ResponseEntity<Page<Purchase_orderDTO>> fetchDefault(Purchase_orderSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Purchase_orderDTO> list = new ArrayList<Purchase_orderDTO>();
        
        Page<Purchase_order> domains = purchase_orderService.searchDefault(context) ;
        for(Purchase_order purchase_order : domains.getContent()){
            Purchase_orderDTO dto = new Purchase_orderDTO();
            dto.fromDO(purchase_order);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
