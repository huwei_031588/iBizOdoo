package cn.ibizlab.odoo.service.odoo_purchase.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_purchase.dto.Purchase_bill_unionDTO;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_bill_unionService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Purchase_bill_union" })
@RestController
@RequestMapping("")
public class Purchase_bill_unionResource {

    @Autowired
    private IPurchase_bill_unionService purchase_bill_unionService;

    public IPurchase_bill_unionService getPurchase_bill_unionService() {
        return this.purchase_bill_unionService;
    }

    @ApiOperation(value = "建立数据", tags = {"Purchase_bill_union" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_bill_unions")

    public ResponseEntity<Purchase_bill_unionDTO> create(@RequestBody Purchase_bill_unionDTO purchase_bill_uniondto) {
        Purchase_bill_unionDTO dto = new Purchase_bill_unionDTO();
        Purchase_bill_union domain = purchase_bill_uniondto.toDO();
		purchase_bill_unionService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Purchase_bill_union" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_bill_unions/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_bill_unionDTO> purchase_bill_uniondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Purchase_bill_union" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_bill_unions/{purchase_bill_union_id}")
    public ResponseEntity<Purchase_bill_unionDTO> get(@PathVariable("purchase_bill_union_id") Integer purchase_bill_union_id) {
        Purchase_bill_unionDTO dto = new Purchase_bill_unionDTO();
        Purchase_bill_union domain = purchase_bill_unionService.get(purchase_bill_union_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Purchase_bill_union" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_bill_unions/createBatch")
    public ResponseEntity<Boolean> createBatchPurchase_bill_union(@RequestBody List<Purchase_bill_unionDTO> purchase_bill_uniondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Purchase_bill_union" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_bill_unions/{purchase_bill_union_id}")

    public ResponseEntity<Purchase_bill_unionDTO> update(@PathVariable("purchase_bill_union_id") Integer purchase_bill_union_id, @RequestBody Purchase_bill_unionDTO purchase_bill_uniondto) {
		Purchase_bill_union domain = purchase_bill_uniondto.toDO();
        domain.setId(purchase_bill_union_id);
		purchase_bill_unionService.update(domain);
		Purchase_bill_unionDTO dto = new Purchase_bill_unionDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Purchase_bill_union" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_bill_unions/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Purchase_bill_unionDTO> purchase_bill_uniondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Purchase_bill_union" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_bill_unions/{purchase_bill_union_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("purchase_bill_union_id") Integer purchase_bill_union_id) {
        Purchase_bill_unionDTO purchase_bill_uniondto = new Purchase_bill_unionDTO();
		Purchase_bill_union domain = new Purchase_bill_union();
		purchase_bill_uniondto.setId(purchase_bill_union_id);
		domain.setId(purchase_bill_union_id);
        Boolean rst = purchase_bill_unionService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Purchase_bill_union" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_purchase/purchase_bill_unions/fetchdefault")
	public ResponseEntity<Page<Purchase_bill_unionDTO>> fetchDefault(Purchase_bill_unionSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Purchase_bill_unionDTO> list = new ArrayList<Purchase_bill_unionDTO>();
        
        Page<Purchase_bill_union> domains = purchase_bill_unionService.searchDefault(context) ;
        for(Purchase_bill_union purchase_bill_union : domains.getContent()){
            Purchase_bill_unionDTO dto = new Purchase_bill_unionDTO();
            dto.fromDO(purchase_bill_union);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
