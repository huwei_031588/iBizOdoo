package cn.ibizlab.odoo.service.odoo_fetchmail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_fetchmail.valuerule.anno.fetchmail_server.*;
import cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Fetchmail_serverDTO]
 */
public class Fetchmail_serverDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [USER]
     *
     */
    @Fetchmail_serverUserDefault(info = "默认规则")
    private String user;

    @JsonIgnore
    private boolean userDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Fetchmail_serverDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [SCRIPT]
     *
     */
    @Fetchmail_serverScriptDefault(info = "默认规则")
    private String script;

    @JsonIgnore
    private boolean scriptDirtyFlag;

    /**
     * 属性 [CONFIGURATION]
     *
     */
    @Fetchmail_serverConfigurationDefault(info = "默认规则")
    private String configuration;

    @JsonIgnore
    private boolean configurationDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Fetchmail_serverDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Fetchmail_serverTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [PASSWORD]
     *
     */
    @Fetchmail_serverPasswordDefault(info = "默认规则")
    private String password;

    @JsonIgnore
    private boolean passwordDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Fetchmail_serverMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Fetchmail_serverCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [SERVER]
     *
     */
    @Fetchmail_serverServerDefault(info = "默认规则")
    private String server;

    @JsonIgnore
    private boolean serverDirtyFlag;

    /**
     * 属性 [PORT]
     *
     */
    @Fetchmail_serverPortDefault(info = "默认规则")
    private Integer port;

    @JsonIgnore
    private boolean portDirtyFlag;

    /**
     * 属性 [IS_SSL]
     *
     */
    @Fetchmail_serverIs_sslDefault(info = "默认规则")
    private String is_ssl;

    @JsonIgnore
    private boolean is_sslDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Fetchmail_serverNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Fetchmail_serverPriorityDefault(info = "默认规则")
    private Integer priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Fetchmail_server__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [OBJECT_ID]
     *
     */
    @Fetchmail_serverObject_idDefault(info = "默认规则")
    private Integer object_id;

    @JsonIgnore
    private boolean object_idDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Fetchmail_serverIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Fetchmail_serverStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Fetchmail_serverActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Fetchmail_serverWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ATTACH]
     *
     */
    @Fetchmail_serverAttachDefault(info = "默认规则")
    private String attach;

    @JsonIgnore
    private boolean attachDirtyFlag;

    /**
     * 属性 [ORIGINAL]
     *
     */
    @Fetchmail_serverOriginalDefault(info = "默认规则")
    private String original;

    @JsonIgnore
    private boolean originalDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Fetchmail_serverCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Fetchmail_serverWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Fetchmail_serverCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Fetchmail_serverWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [USER]
     */
    @JsonProperty("user")
    public String getUser(){
        return user ;
    }

    /**
     * 设置 [USER]
     */
    @JsonProperty("user")
    public void setUser(String  user){
        this.user = user ;
        this.userDirtyFlag = true ;
    }

    /**
     * 获取 [USER]脏标记
     */
    @JsonIgnore
    public boolean getUserDirtyFlag(){
        return userDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [SCRIPT]
     */
    @JsonProperty("script")
    public String getScript(){
        return script ;
    }

    /**
     * 设置 [SCRIPT]
     */
    @JsonProperty("script")
    public void setScript(String  script){
        this.script = script ;
        this.scriptDirtyFlag = true ;
    }

    /**
     * 获取 [SCRIPT]脏标记
     */
    @JsonIgnore
    public boolean getScriptDirtyFlag(){
        return scriptDirtyFlag ;
    }

    /**
     * 获取 [CONFIGURATION]
     */
    @JsonProperty("configuration")
    public String getConfiguration(){
        return configuration ;
    }

    /**
     * 设置 [CONFIGURATION]
     */
    @JsonProperty("configuration")
    public void setConfiguration(String  configuration){
        this.configuration = configuration ;
        this.configurationDirtyFlag = true ;
    }

    /**
     * 获取 [CONFIGURATION]脏标记
     */
    @JsonIgnore
    public boolean getConfigurationDirtyFlag(){
        return configurationDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [PASSWORD]
     */
    @JsonProperty("password")
    public String getPassword(){
        return password ;
    }

    /**
     * 设置 [PASSWORD]
     */
    @JsonProperty("password")
    public void setPassword(String  password){
        this.password = password ;
        this.passwordDirtyFlag = true ;
    }

    /**
     * 获取 [PASSWORD]脏标记
     */
    @JsonIgnore
    public boolean getPasswordDirtyFlag(){
        return passwordDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [SERVER]
     */
    @JsonProperty("server")
    public String getServer(){
        return server ;
    }

    /**
     * 设置 [SERVER]
     */
    @JsonProperty("server")
    public void setServer(String  server){
        this.server = server ;
        this.serverDirtyFlag = true ;
    }

    /**
     * 获取 [SERVER]脏标记
     */
    @JsonIgnore
    public boolean getServerDirtyFlag(){
        return serverDirtyFlag ;
    }

    /**
     * 获取 [PORT]
     */
    @JsonProperty("port")
    public Integer getPort(){
        return port ;
    }

    /**
     * 设置 [PORT]
     */
    @JsonProperty("port")
    public void setPort(Integer  port){
        this.port = port ;
        this.portDirtyFlag = true ;
    }

    /**
     * 获取 [PORT]脏标记
     */
    @JsonIgnore
    public boolean getPortDirtyFlag(){
        return portDirtyFlag ;
    }

    /**
     * 获取 [IS_SSL]
     */
    @JsonProperty("is_ssl")
    public String getIs_ssl(){
        return is_ssl ;
    }

    /**
     * 设置 [IS_SSL]
     */
    @JsonProperty("is_ssl")
    public void setIs_ssl(String  is_ssl){
        this.is_ssl = is_ssl ;
        this.is_sslDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SSL]脏标记
     */
    @JsonIgnore
    public boolean getIs_sslDirtyFlag(){
        return is_sslDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public Integer getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(Integer  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [OBJECT_ID]
     */
    @JsonProperty("object_id")
    public Integer getObject_id(){
        return object_id ;
    }

    /**
     * 设置 [OBJECT_ID]
     */
    @JsonProperty("object_id")
    public void setObject_id(Integer  object_id){
        this.object_id = object_id ;
        this.object_idDirtyFlag = true ;
    }

    /**
     * 获取 [OBJECT_ID]脏标记
     */
    @JsonIgnore
    public boolean getObject_idDirtyFlag(){
        return object_idDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ATTACH]
     */
    @JsonProperty("attach")
    public String getAttach(){
        return attach ;
    }

    /**
     * 设置 [ATTACH]
     */
    @JsonProperty("attach")
    public void setAttach(String  attach){
        this.attach = attach ;
        this.attachDirtyFlag = true ;
    }

    /**
     * 获取 [ATTACH]脏标记
     */
    @JsonIgnore
    public boolean getAttachDirtyFlag(){
        return attachDirtyFlag ;
    }

    /**
     * 获取 [ORIGINAL]
     */
    @JsonProperty("original")
    public String getOriginal(){
        return original ;
    }

    /**
     * 设置 [ORIGINAL]
     */
    @JsonProperty("original")
    public void setOriginal(String  original){
        this.original = original ;
        this.originalDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGINAL]脏标记
     */
    @JsonIgnore
    public boolean getOriginalDirtyFlag(){
        return originalDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Fetchmail_server toDO() {
        Fetchmail_server srfdomain = new Fetchmail_server();
        if(getUserDirtyFlag())
            srfdomain.setUser(user);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getScriptDirtyFlag())
            srfdomain.setScript(script);
        if(getConfigurationDirtyFlag())
            srfdomain.setConfiguration(configuration);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getPasswordDirtyFlag())
            srfdomain.setPassword(password);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getServerDirtyFlag())
            srfdomain.setServer(server);
        if(getPortDirtyFlag())
            srfdomain.setPort(port);
        if(getIs_sslDirtyFlag())
            srfdomain.setIs_ssl(is_ssl);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getObject_idDirtyFlag())
            srfdomain.setObject_id(object_id);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getAttachDirtyFlag())
            srfdomain.setAttach(attach);
        if(getOriginalDirtyFlag())
            srfdomain.setOriginal(original);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Fetchmail_server srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getUserDirtyFlag())
            this.setUser(srfdomain.getUser());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getScriptDirtyFlag())
            this.setScript(srfdomain.getScript());
        if(srfdomain.getConfigurationDirtyFlag())
            this.setConfiguration(srfdomain.getConfiguration());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getPasswordDirtyFlag())
            this.setPassword(srfdomain.getPassword());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getServerDirtyFlag())
            this.setServer(srfdomain.getServer());
        if(srfdomain.getPortDirtyFlag())
            this.setPort(srfdomain.getPort());
        if(srfdomain.getIs_sslDirtyFlag())
            this.setIs_ssl(srfdomain.getIs_ssl());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getObject_idDirtyFlag())
            this.setObject_id(srfdomain.getObject_id());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getAttachDirtyFlag())
            this.setAttach(srfdomain.getAttach());
        if(srfdomain.getOriginalDirtyFlag())
            this.setOriginal(srfdomain.getOriginal());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Fetchmail_serverDTO> fromDOPage(List<Fetchmail_server> poPage)   {
        if(poPage == null)
            return null;
        List<Fetchmail_serverDTO> dtos=new ArrayList<Fetchmail_serverDTO>();
        for(Fetchmail_server domain : poPage) {
            Fetchmail_serverDTO dto = new Fetchmail_serverDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

