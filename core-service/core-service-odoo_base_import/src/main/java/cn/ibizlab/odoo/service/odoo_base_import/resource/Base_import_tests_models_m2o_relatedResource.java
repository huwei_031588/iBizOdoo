package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_m2o_relatedDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o_related;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_m2o_relatedService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2o_relatedSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_m2o_related" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_m2o_relatedResource {

    @Autowired
    private IBase_import_tests_models_m2o_relatedService base_import_tests_models_m2o_relatedService;

    public IBase_import_tests_models_m2o_relatedService getBase_import_tests_models_m2o_relatedService() {
        return this.base_import_tests_models_m2o_relatedService;
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/{base_import_tests_models_m2o_related_id}")

    public ResponseEntity<Base_import_tests_models_m2o_relatedDTO> update(@PathVariable("base_import_tests_models_m2o_related_id") Integer base_import_tests_models_m2o_related_id, @RequestBody Base_import_tests_models_m2o_relatedDTO base_import_tests_models_m2o_relateddto) {
		Base_import_tests_models_m2o_related domain = base_import_tests_models_m2o_relateddto.toDO();
        domain.setId(base_import_tests_models_m2o_related_id);
		base_import_tests_models_m2o_relatedService.update(domain);
		Base_import_tests_models_m2o_relatedDTO dto = new Base_import_tests_models_m2o_relatedDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_m2o_relatedDTO> base_import_tests_models_m2o_relateddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_m2o_related(@RequestBody List<Base_import_tests_models_m2o_relatedDTO> base_import_tests_models_m2o_relateddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/{base_import_tests_models_m2o_related_id}")
    public ResponseEntity<Base_import_tests_models_m2o_relatedDTO> get(@PathVariable("base_import_tests_models_m2o_related_id") Integer base_import_tests_models_m2o_related_id) {
        Base_import_tests_models_m2o_relatedDTO dto = new Base_import_tests_models_m2o_relatedDTO();
        Base_import_tests_models_m2o_related domain = base_import_tests_models_m2o_relatedService.get(base_import_tests_models_m2o_related_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_m2o_relatedDTO> base_import_tests_models_m2o_relateddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_m2o_relateds")

    public ResponseEntity<Base_import_tests_models_m2o_relatedDTO> create(@RequestBody Base_import_tests_models_m2o_relatedDTO base_import_tests_models_m2o_relateddto) {
        Base_import_tests_models_m2o_relatedDTO dto = new Base_import_tests_models_m2o_relatedDTO();
        Base_import_tests_models_m2o_related domain = base_import_tests_models_m2o_relateddto.toDO();
		base_import_tests_models_m2o_relatedService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_m2o_related" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_m2o_relateds/{base_import_tests_models_m2o_related_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_m2o_related_id") Integer base_import_tests_models_m2o_related_id) {
        Base_import_tests_models_m2o_relatedDTO base_import_tests_models_m2o_relateddto = new Base_import_tests_models_m2o_relatedDTO();
		Base_import_tests_models_m2o_related domain = new Base_import_tests_models_m2o_related();
		base_import_tests_models_m2o_relateddto.setId(base_import_tests_models_m2o_related_id);
		domain.setId(base_import_tests_models_m2o_related_id);
        Boolean rst = base_import_tests_models_m2o_relatedService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_m2o_related" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_m2o_relateds/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_m2o_relatedDTO>> fetchDefault(Base_import_tests_models_m2o_relatedSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_m2o_relatedDTO> list = new ArrayList<Base_import_tests_models_m2o_relatedDTO>();
        
        Page<Base_import_tests_models_m2o_related> domains = base_import_tests_models_m2o_relatedService.searchDefault(context) ;
        for(Base_import_tests_models_m2o_related base_import_tests_models_m2o_related : domains.getContent()){
            Base_import_tests_models_m2o_relatedDTO dto = new Base_import_tests_models_m2o_relatedDTO();
            dto.fromDO(base_import_tests_models_m2o_related);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
