package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_importDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_import;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_importService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_importSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_import" })
@RestController
@RequestMapping("")
public class Base_import_importResource {

    @Autowired
    private IBase_import_importService base_import_importService;

    public IBase_import_importService getBase_import_importService() {
        return this.base_import_importService;
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_import" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_imports/{base_import_import_id}")
    public ResponseEntity<Base_import_importDTO> get(@PathVariable("base_import_import_id") Integer base_import_import_id) {
        Base_import_importDTO dto = new Base_import_importDTO();
        Base_import_import domain = base_import_importService.get(base_import_import_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_import" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_imports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_importDTO> base_import_importdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_import" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_imports/{base_import_import_id}")

    public ResponseEntity<Base_import_importDTO> update(@PathVariable("base_import_import_id") Integer base_import_import_id, @RequestBody Base_import_importDTO base_import_importdto) {
		Base_import_import domain = base_import_importdto.toDO();
        domain.setId(base_import_import_id);
		base_import_importService.update(domain);
		Base_import_importDTO dto = new Base_import_importDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_import" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_imports")

    public ResponseEntity<Base_import_importDTO> create(@RequestBody Base_import_importDTO base_import_importdto) {
        Base_import_importDTO dto = new Base_import_importDTO();
        Base_import_import domain = base_import_importdto.toDO();
		base_import_importService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_import" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_imports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_importDTO> base_import_importdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_import" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_imports/{base_import_import_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_import_id") Integer base_import_import_id) {
        Base_import_importDTO base_import_importdto = new Base_import_importDTO();
		Base_import_import domain = new Base_import_import();
		base_import_importdto.setId(base_import_import_id);
		domain.setId(base_import_import_id);
        Boolean rst = base_import_importService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_import" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_imports/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_import(@RequestBody List<Base_import_importDTO> base_import_importdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_import" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_imports/fetchdefault")
	public ResponseEntity<Page<Base_import_importDTO>> fetchDefault(Base_import_importSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_importDTO> list = new ArrayList<Base_import_importDTO>();
        
        Page<Base_import_import> domains = base_import_importService.searchDefault(context) ;
        for(Base_import_import base_import_import : domains.getContent()){
            Base_import_importDTO dto = new Base_import_importDTO();
            dto.fromDO(base_import_import);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
