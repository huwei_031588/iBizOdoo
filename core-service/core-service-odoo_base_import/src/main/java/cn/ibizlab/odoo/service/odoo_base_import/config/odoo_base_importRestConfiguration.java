package cn.ibizlab.odoo.service.odoo_base_import.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_base_import")
public class odoo_base_importRestConfiguration {

}
