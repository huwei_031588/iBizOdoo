package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_char_readonlyDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_readonly;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_char_readonlyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_readonlySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_char_readonly" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_char_readonlyResource {

    @Autowired
    private IBase_import_tests_models_char_readonlyService base_import_tests_models_char_readonlyService;

    public IBase_import_tests_models_char_readonlyService getBase_import_tests_models_char_readonlyService() {
        return this.base_import_tests_models_char_readonlyService;
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_char_readonly" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_readonlies/{base_import_tests_models_char_readonly_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_readonly_id") Integer base_import_tests_models_char_readonly_id) {
        Base_import_tests_models_char_readonlyDTO base_import_tests_models_char_readonlydto = new Base_import_tests_models_char_readonlyDTO();
		Base_import_tests_models_char_readonly domain = new Base_import_tests_models_char_readonly();
		base_import_tests_models_char_readonlydto.setId(base_import_tests_models_char_readonly_id);
		domain.setId(base_import_tests_models_char_readonly_id);
        Boolean rst = base_import_tests_models_char_readonlyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_char_readonly" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_readonlies/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_char_readonly(@RequestBody List<Base_import_tests_models_char_readonlyDTO> base_import_tests_models_char_readonlydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_char_readonly" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_readonlies/{base_import_tests_models_char_readonly_id}")

    public ResponseEntity<Base_import_tests_models_char_readonlyDTO> update(@PathVariable("base_import_tests_models_char_readonly_id") Integer base_import_tests_models_char_readonly_id, @RequestBody Base_import_tests_models_char_readonlyDTO base_import_tests_models_char_readonlydto) {
		Base_import_tests_models_char_readonly domain = base_import_tests_models_char_readonlydto.toDO();
        domain.setId(base_import_tests_models_char_readonly_id);
		base_import_tests_models_char_readonlyService.update(domain);
		Base_import_tests_models_char_readonlyDTO dto = new Base_import_tests_models_char_readonlyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_char_readonly" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_readonlies")

    public ResponseEntity<Base_import_tests_models_char_readonlyDTO> create(@RequestBody Base_import_tests_models_char_readonlyDTO base_import_tests_models_char_readonlydto) {
        Base_import_tests_models_char_readonlyDTO dto = new Base_import_tests_models_char_readonlyDTO();
        Base_import_tests_models_char_readonly domain = base_import_tests_models_char_readonlydto.toDO();
		base_import_tests_models_char_readonlyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_char_readonly" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_readonlies/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_char_readonlyDTO> base_import_tests_models_char_readonlydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_char_readonly" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_readonlies/{base_import_tests_models_char_readonly_id}")
    public ResponseEntity<Base_import_tests_models_char_readonlyDTO> get(@PathVariable("base_import_tests_models_char_readonly_id") Integer base_import_tests_models_char_readonly_id) {
        Base_import_tests_models_char_readonlyDTO dto = new Base_import_tests_models_char_readonlyDTO();
        Base_import_tests_models_char_readonly domain = base_import_tests_models_char_readonlyService.get(base_import_tests_models_char_readonly_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_char_readonly" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_readonlies/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_char_readonlyDTO> base_import_tests_models_char_readonlydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_char_readonly" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_char_readonlies/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_char_readonlyDTO>> fetchDefault(Base_import_tests_models_char_readonlySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_char_readonlyDTO> list = new ArrayList<Base_import_tests_models_char_readonlyDTO>();
        
        Page<Base_import_tests_models_char_readonly> domains = base_import_tests_models_char_readonlyService.searchDefault(context) ;
        for(Base_import_tests_models_char_readonly base_import_tests_models_char_readonly : domains.getContent()){
            Base_import_tests_models_char_readonlyDTO dto = new Base_import_tests_models_char_readonlyDTO();
            dto.fromDO(base_import_tests_models_char_readonly);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
