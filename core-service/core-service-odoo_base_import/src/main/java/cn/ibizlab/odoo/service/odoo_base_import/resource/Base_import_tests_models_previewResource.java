package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_previewDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_preview;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_previewService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_previewSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_preview" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_previewResource {

    @Autowired
    private IBase_import_tests_models_previewService base_import_tests_models_previewService;

    public IBase_import_tests_models_previewService getBase_import_tests_models_previewService() {
        return this.base_import_tests_models_previewService;
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_preview" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_previews/{base_import_tests_models_preview_id}")
    public ResponseEntity<Base_import_tests_models_previewDTO> get(@PathVariable("base_import_tests_models_preview_id") Integer base_import_tests_models_preview_id) {
        Base_import_tests_models_previewDTO dto = new Base_import_tests_models_previewDTO();
        Base_import_tests_models_preview domain = base_import_tests_models_previewService.get(base_import_tests_models_preview_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_preview" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_previews/{base_import_tests_models_preview_id}")

    public ResponseEntity<Base_import_tests_models_previewDTO> update(@PathVariable("base_import_tests_models_preview_id") Integer base_import_tests_models_preview_id, @RequestBody Base_import_tests_models_previewDTO base_import_tests_models_previewdto) {
		Base_import_tests_models_preview domain = base_import_tests_models_previewdto.toDO();
        domain.setId(base_import_tests_models_preview_id);
		base_import_tests_models_previewService.update(domain);
		Base_import_tests_models_previewDTO dto = new Base_import_tests_models_previewDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_preview" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_previews/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_preview(@RequestBody List<Base_import_tests_models_previewDTO> base_import_tests_models_previewdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_preview" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_previews/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_previewDTO> base_import_tests_models_previewdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_preview" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_previews")

    public ResponseEntity<Base_import_tests_models_previewDTO> create(@RequestBody Base_import_tests_models_previewDTO base_import_tests_models_previewdto) {
        Base_import_tests_models_previewDTO dto = new Base_import_tests_models_previewDTO();
        Base_import_tests_models_preview domain = base_import_tests_models_previewdto.toDO();
		base_import_tests_models_previewService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_preview" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_previews/{base_import_tests_models_preview_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_preview_id") Integer base_import_tests_models_preview_id) {
        Base_import_tests_models_previewDTO base_import_tests_models_previewdto = new Base_import_tests_models_previewDTO();
		Base_import_tests_models_preview domain = new Base_import_tests_models_preview();
		base_import_tests_models_previewdto.setId(base_import_tests_models_preview_id);
		domain.setId(base_import_tests_models_preview_id);
        Boolean rst = base_import_tests_models_previewService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_preview" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_previews/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_previewDTO> base_import_tests_models_previewdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_preview" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_previews/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_previewDTO>> fetchDefault(Base_import_tests_models_previewSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_previewDTO> list = new ArrayList<Base_import_tests_models_previewDTO>();
        
        Page<Base_import_tests_models_preview> domains = base_import_tests_models_previewService.searchDefault(context) ;
        for(Base_import_tests_models_preview base_import_tests_models_preview : domains.getContent()){
            Base_import_tests_models_previewDTO dto = new Base_import_tests_models_previewDTO();
            dto.fromDO(base_import_tests_models_preview);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
