package cn.ibizlab.odoo.service.odoo_lunch.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_lunch.dto.Lunch_orderDTO;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_orderService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_orderSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Lunch_order" })
@RestController
@RequestMapping("")
public class Lunch_orderResource {

    @Autowired
    private ILunch_orderService lunch_orderService;

    public ILunch_orderService getLunch_orderService() {
        return this.lunch_orderService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Lunch_order" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_orders/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_orderDTO> lunch_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Lunch_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_orders/{lunch_order_id}")
    public ResponseEntity<Lunch_orderDTO> get(@PathVariable("lunch_order_id") Integer lunch_order_id) {
        Lunch_orderDTO dto = new Lunch_orderDTO();
        Lunch_order domain = lunch_orderService.get(lunch_order_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Lunch_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_orders")

    public ResponseEntity<Lunch_orderDTO> create(@RequestBody Lunch_orderDTO lunch_orderdto) {
        Lunch_orderDTO dto = new Lunch_orderDTO();
        Lunch_order domain = lunch_orderdto.toDO();
		lunch_orderService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Lunch_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_orders/{lunch_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_order_id") Integer lunch_order_id) {
        Lunch_orderDTO lunch_orderdto = new Lunch_orderDTO();
		Lunch_order domain = new Lunch_order();
		lunch_orderdto.setId(lunch_order_id);
		domain.setId(lunch_order_id);
        Boolean rst = lunch_orderService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Lunch_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_orders/{lunch_order_id}")

    public ResponseEntity<Lunch_orderDTO> update(@PathVariable("lunch_order_id") Integer lunch_order_id, @RequestBody Lunch_orderDTO lunch_orderdto) {
		Lunch_order domain = lunch_orderdto.toDO();
        domain.setId(lunch_order_id);
		lunch_orderService.update(domain);
		Lunch_orderDTO dto = new Lunch_orderDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Lunch_order" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_orders/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Lunch_orderDTO> lunch_orderdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Lunch_order" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_orders/createBatch")
    public ResponseEntity<Boolean> createBatchLunch_order(@RequestBody List<Lunch_orderDTO> lunch_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Lunch_order" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_lunch/lunch_orders/fetchdefault")
	public ResponseEntity<Page<Lunch_orderDTO>> fetchDefault(Lunch_orderSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Lunch_orderDTO> list = new ArrayList<Lunch_orderDTO>();
        
        Page<Lunch_order> domains = lunch_orderService.searchDefault(context) ;
        for(Lunch_order lunch_order : domains.getContent()){
            Lunch_orderDTO dto = new Lunch_orderDTO();
            dto.fromDO(lunch_order);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
