package cn.ibizlab.odoo.service.odoo_gamification.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_gamification")
public class odoo_gamificationRestConfiguration {

}
