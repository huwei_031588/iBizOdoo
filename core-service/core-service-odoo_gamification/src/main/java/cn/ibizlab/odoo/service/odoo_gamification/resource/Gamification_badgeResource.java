package cn.ibizlab.odoo.service.odoo_gamification.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_gamification.dto.Gamification_badgeDTO;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_badgeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badgeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Gamification_badge" })
@RestController
@RequestMapping("")
public class Gamification_badgeResource {

    @Autowired
    private IGamification_badgeService gamification_badgeService;

    public IGamification_badgeService getGamification_badgeService() {
        return this.gamification_badgeService;
    }

    @ApiOperation(value = "删除数据", tags = {"Gamification_badge" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_badges/{gamification_badge_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_badge_id") Integer gamification_badge_id) {
        Gamification_badgeDTO gamification_badgedto = new Gamification_badgeDTO();
		Gamification_badge domain = new Gamification_badge();
		gamification_badgedto.setId(gamification_badge_id);
		domain.setId(gamification_badge_id);
        Boolean rst = gamification_badgeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Gamification_badge" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_badges/createBatch")
    public ResponseEntity<Boolean> createBatchGamification_badge(@RequestBody List<Gamification_badgeDTO> gamification_badgedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Gamification_badge" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_badges/{gamification_badge_id}")

    public ResponseEntity<Gamification_badgeDTO> update(@PathVariable("gamification_badge_id") Integer gamification_badge_id, @RequestBody Gamification_badgeDTO gamification_badgedto) {
		Gamification_badge domain = gamification_badgedto.toDO();
        domain.setId(gamification_badge_id);
		gamification_badgeService.update(domain);
		Gamification_badgeDTO dto = new Gamification_badgeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Gamification_badge" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_badges/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_badgeDTO> gamification_badgedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Gamification_badge" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_badges/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Gamification_badgeDTO> gamification_badgedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Gamification_badge" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_badges")

    public ResponseEntity<Gamification_badgeDTO> create(@RequestBody Gamification_badgeDTO gamification_badgedto) {
        Gamification_badgeDTO dto = new Gamification_badgeDTO();
        Gamification_badge domain = gamification_badgedto.toDO();
		gamification_badgeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Gamification_badge" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badges/{gamification_badge_id}")
    public ResponseEntity<Gamification_badgeDTO> get(@PathVariable("gamification_badge_id") Integer gamification_badge_id) {
        Gamification_badgeDTO dto = new Gamification_badgeDTO();
        Gamification_badge domain = gamification_badgeService.get(gamification_badge_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Gamification_badge" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_gamification/gamification_badges/fetchdefault")
	public ResponseEntity<Page<Gamification_badgeDTO>> fetchDefault(Gamification_badgeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Gamification_badgeDTO> list = new ArrayList<Gamification_badgeDTO>();
        
        Page<Gamification_badge> domains = gamification_badgeService.searchDefault(context) ;
        for(Gamification_badge gamification_badge : domains.getContent()){
            Gamification_badgeDTO dto = new Gamification_badgeDTO();
            dto.fromDO(gamification_badge);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
