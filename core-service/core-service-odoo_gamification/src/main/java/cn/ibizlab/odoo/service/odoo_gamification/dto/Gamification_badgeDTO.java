package cn.ibizlab.odoo.service.odoo_gamification.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_badge.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Gamification_badgeDTO]
 */
public class Gamification_badgeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RULE_AUTH]
     *
     */
    @Gamification_badgeRule_authDefault(info = "默认规则")
    private String rule_auth;

    @JsonIgnore
    private boolean rule_authDirtyFlag;

    /**
     * 属性 [RULE_MAX_NUMBER]
     *
     */
    @Gamification_badgeRule_max_numberDefault(info = "默认规则")
    private Integer rule_max_number;

    @JsonIgnore
    private boolean rule_max_numberDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Gamification_badgeDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [RULE_MAX]
     *
     */
    @Gamification_badgeRule_maxDefault(info = "默认规则")
    private String rule_max;

    @JsonIgnore
    private boolean rule_maxDirtyFlag;

    /**
     * 属性 [RULE_AUTH_USER_IDS]
     *
     */
    @Gamification_badgeRule_auth_user_idsDefault(info = "默认规则")
    private String rule_auth_user_ids;

    @JsonIgnore
    private boolean rule_auth_user_idsDirtyFlag;

    /**
     * 属性 [RULE_AUTH_BADGE_IDS]
     *
     */
    @Gamification_badgeRule_auth_badge_idsDefault(info = "默认规则")
    private String rule_auth_badge_ids;

    @JsonIgnore
    private boolean rule_auth_badge_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Gamification_badgeMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Gamification_badgeMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [REMAINING_SENDING]
     *
     */
    @Gamification_badgeRemaining_sendingDefault(info = "默认规则")
    private Integer remaining_sending;

    @JsonIgnore
    private boolean remaining_sendingDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Gamification_badgeMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Gamification_badgeMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Gamification_badgeMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [STAT_MY]
     *
     */
    @Gamification_badgeStat_myDefault(info = "默认规则")
    private Integer stat_my;

    @JsonIgnore
    private boolean stat_myDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Gamification_badgeImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Gamification_badgeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Gamification_badgeMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Gamification_badgeMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Gamification_badgeMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [UNIQUE_OWNER_IDS]
     *
     */
    @Gamification_badgeUnique_owner_idsDefault(info = "默认规则")
    private String unique_owner_ids;

    @JsonIgnore
    private boolean unique_owner_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Gamification_badgeWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Gamification_badge__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Gamification_badgeMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Gamification_badgeMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [LEVEL]
     *
     */
    @Gamification_badgeLevelDefault(info = "默认规则")
    private String level;

    @JsonIgnore
    private boolean levelDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Gamification_badgeActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [OWNER_IDS]
     *
     */
    @Gamification_badgeOwner_idsDefault(info = "默认规则")
    private String owner_ids;

    @JsonIgnore
    private boolean owner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Gamification_badgeMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Gamification_badgeMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [GOAL_DEFINITION_IDS]
     *
     */
    @Gamification_badgeGoal_definition_idsDefault(info = "默认规则")
    private String goal_definition_ids;

    @JsonIgnore
    private boolean goal_definition_idsDirtyFlag;

    /**
     * 属性 [STAT_MY_MONTHLY_SENDING]
     *
     */
    @Gamification_badgeStat_my_monthly_sendingDefault(info = "默认规则")
    private Integer stat_my_monthly_sending;

    @JsonIgnore
    private boolean stat_my_monthly_sendingDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Gamification_badgeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [STAT_COUNT]
     *
     */
    @Gamification_badgeStat_countDefault(info = "默认规则")
    private Integer stat_count;

    @JsonIgnore
    private boolean stat_countDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Gamification_badgeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [STAT_THIS_MONTH]
     *
     */
    @Gamification_badgeStat_this_monthDefault(info = "默认规则")
    private Integer stat_this_month;

    @JsonIgnore
    private boolean stat_this_monthDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Gamification_badgeMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Gamification_badgeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Gamification_badgeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [STAT_COUNT_DISTINCT]
     *
     */
    @Gamification_badgeStat_count_distinctDefault(info = "默认规则")
    private Integer stat_count_distinct;

    @JsonIgnore
    private boolean stat_count_distinctDirtyFlag;

    /**
     * 属性 [STAT_MY_THIS_MONTH]
     *
     */
    @Gamification_badgeStat_my_this_monthDefault(info = "默认规则")
    private Integer stat_my_this_month;

    @JsonIgnore
    private boolean stat_my_this_monthDirtyFlag;

    /**
     * 属性 [CHALLENGE_IDS]
     *
     */
    @Gamification_badgeChallenge_idsDefault(info = "默认规则")
    private String challenge_ids;

    @JsonIgnore
    private boolean challenge_idsDirtyFlag;

    /**
     * 属性 [GRANTED_EMPLOYEES_COUNT]
     *
     */
    @Gamification_badgeGranted_employees_countDefault(info = "默认规则")
    private Integer granted_employees_count;

    @JsonIgnore
    private boolean granted_employees_countDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Gamification_badgeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Gamification_badgeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Gamification_badgeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Gamification_badgeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [RULE_AUTH]
     */
    @JsonProperty("rule_auth")
    public String getRule_auth(){
        return rule_auth ;
    }

    /**
     * 设置 [RULE_AUTH]
     */
    @JsonProperty("rule_auth")
    public void setRule_auth(String  rule_auth){
        this.rule_auth = rule_auth ;
        this.rule_authDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_AUTH]脏标记
     */
    @JsonIgnore
    public boolean getRule_authDirtyFlag(){
        return rule_authDirtyFlag ;
    }

    /**
     * 获取 [RULE_MAX_NUMBER]
     */
    @JsonProperty("rule_max_number")
    public Integer getRule_max_number(){
        return rule_max_number ;
    }

    /**
     * 设置 [RULE_MAX_NUMBER]
     */
    @JsonProperty("rule_max_number")
    public void setRule_max_number(Integer  rule_max_number){
        this.rule_max_number = rule_max_number ;
        this.rule_max_numberDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_MAX_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getRule_max_numberDirtyFlag(){
        return rule_max_numberDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [RULE_MAX]
     */
    @JsonProperty("rule_max")
    public String getRule_max(){
        return rule_max ;
    }

    /**
     * 设置 [RULE_MAX]
     */
    @JsonProperty("rule_max")
    public void setRule_max(String  rule_max){
        this.rule_max = rule_max ;
        this.rule_maxDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_MAX]脏标记
     */
    @JsonIgnore
    public boolean getRule_maxDirtyFlag(){
        return rule_maxDirtyFlag ;
    }

    /**
     * 获取 [RULE_AUTH_USER_IDS]
     */
    @JsonProperty("rule_auth_user_ids")
    public String getRule_auth_user_ids(){
        return rule_auth_user_ids ;
    }

    /**
     * 设置 [RULE_AUTH_USER_IDS]
     */
    @JsonProperty("rule_auth_user_ids")
    public void setRule_auth_user_ids(String  rule_auth_user_ids){
        this.rule_auth_user_ids = rule_auth_user_ids ;
        this.rule_auth_user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_AUTH_USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRule_auth_user_idsDirtyFlag(){
        return rule_auth_user_idsDirtyFlag ;
    }

    /**
     * 获取 [RULE_AUTH_BADGE_IDS]
     */
    @JsonProperty("rule_auth_badge_ids")
    public String getRule_auth_badge_ids(){
        return rule_auth_badge_ids ;
    }

    /**
     * 设置 [RULE_AUTH_BADGE_IDS]
     */
    @JsonProperty("rule_auth_badge_ids")
    public void setRule_auth_badge_ids(String  rule_auth_badge_ids){
        this.rule_auth_badge_ids = rule_auth_badge_ids ;
        this.rule_auth_badge_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_AUTH_BADGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRule_auth_badge_idsDirtyFlag(){
        return rule_auth_badge_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [REMAINING_SENDING]
     */
    @JsonProperty("remaining_sending")
    public Integer getRemaining_sending(){
        return remaining_sending ;
    }

    /**
     * 设置 [REMAINING_SENDING]
     */
    @JsonProperty("remaining_sending")
    public void setRemaining_sending(Integer  remaining_sending){
        this.remaining_sending = remaining_sending ;
        this.remaining_sendingDirtyFlag = true ;
    }

    /**
     * 获取 [REMAINING_SENDING]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_sendingDirtyFlag(){
        return remaining_sendingDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [STAT_MY]
     */
    @JsonProperty("stat_my")
    public Integer getStat_my(){
        return stat_my ;
    }

    /**
     * 设置 [STAT_MY]
     */
    @JsonProperty("stat_my")
    public void setStat_my(Integer  stat_my){
        this.stat_my = stat_my ;
        this.stat_myDirtyFlag = true ;
    }

    /**
     * 获取 [STAT_MY]脏标记
     */
    @JsonIgnore
    public boolean getStat_myDirtyFlag(){
        return stat_myDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [UNIQUE_OWNER_IDS]
     */
    @JsonProperty("unique_owner_ids")
    public String getUnique_owner_ids(){
        return unique_owner_ids ;
    }

    /**
     * 设置 [UNIQUE_OWNER_IDS]
     */
    @JsonProperty("unique_owner_ids")
    public void setUnique_owner_ids(String  unique_owner_ids){
        this.unique_owner_ids = unique_owner_ids ;
        this.unique_owner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [UNIQUE_OWNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getUnique_owner_idsDirtyFlag(){
        return unique_owner_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [LEVEL]
     */
    @JsonProperty("level")
    public String getLevel(){
        return level ;
    }

    /**
     * 设置 [LEVEL]
     */
    @JsonProperty("level")
    public void setLevel(String  level){
        this.level = level ;
        this.levelDirtyFlag = true ;
    }

    /**
     * 获取 [LEVEL]脏标记
     */
    @JsonIgnore
    public boolean getLevelDirtyFlag(){
        return levelDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [OWNER_IDS]
     */
    @JsonProperty("owner_ids")
    public String getOwner_ids(){
        return owner_ids ;
    }

    /**
     * 设置 [OWNER_IDS]
     */
    @JsonProperty("owner_ids")
    public void setOwner_ids(String  owner_ids){
        this.owner_ids = owner_ids ;
        this.owner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOwner_idsDirtyFlag(){
        return owner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [GOAL_DEFINITION_IDS]
     */
    @JsonProperty("goal_definition_ids")
    public String getGoal_definition_ids(){
        return goal_definition_ids ;
    }

    /**
     * 设置 [GOAL_DEFINITION_IDS]
     */
    @JsonProperty("goal_definition_ids")
    public void setGoal_definition_ids(String  goal_definition_ids){
        this.goal_definition_ids = goal_definition_ids ;
        this.goal_definition_idsDirtyFlag = true ;
    }

    /**
     * 获取 [GOAL_DEFINITION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getGoal_definition_idsDirtyFlag(){
        return goal_definition_idsDirtyFlag ;
    }

    /**
     * 获取 [STAT_MY_MONTHLY_SENDING]
     */
    @JsonProperty("stat_my_monthly_sending")
    public Integer getStat_my_monthly_sending(){
        return stat_my_monthly_sending ;
    }

    /**
     * 设置 [STAT_MY_MONTHLY_SENDING]
     */
    @JsonProperty("stat_my_monthly_sending")
    public void setStat_my_monthly_sending(Integer  stat_my_monthly_sending){
        this.stat_my_monthly_sending = stat_my_monthly_sending ;
        this.stat_my_monthly_sendingDirtyFlag = true ;
    }

    /**
     * 获取 [STAT_MY_MONTHLY_SENDING]脏标记
     */
    @JsonIgnore
    public boolean getStat_my_monthly_sendingDirtyFlag(){
        return stat_my_monthly_sendingDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [STAT_COUNT]
     */
    @JsonProperty("stat_count")
    public Integer getStat_count(){
        return stat_count ;
    }

    /**
     * 设置 [STAT_COUNT]
     */
    @JsonProperty("stat_count")
    public void setStat_count(Integer  stat_count){
        this.stat_count = stat_count ;
        this.stat_countDirtyFlag = true ;
    }

    /**
     * 获取 [STAT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getStat_countDirtyFlag(){
        return stat_countDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [STAT_THIS_MONTH]
     */
    @JsonProperty("stat_this_month")
    public Integer getStat_this_month(){
        return stat_this_month ;
    }

    /**
     * 设置 [STAT_THIS_MONTH]
     */
    @JsonProperty("stat_this_month")
    public void setStat_this_month(Integer  stat_this_month){
        this.stat_this_month = stat_this_month ;
        this.stat_this_monthDirtyFlag = true ;
    }

    /**
     * 获取 [STAT_THIS_MONTH]脏标记
     */
    @JsonIgnore
    public boolean getStat_this_monthDirtyFlag(){
        return stat_this_monthDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [STAT_COUNT_DISTINCT]
     */
    @JsonProperty("stat_count_distinct")
    public Integer getStat_count_distinct(){
        return stat_count_distinct ;
    }

    /**
     * 设置 [STAT_COUNT_DISTINCT]
     */
    @JsonProperty("stat_count_distinct")
    public void setStat_count_distinct(Integer  stat_count_distinct){
        this.stat_count_distinct = stat_count_distinct ;
        this.stat_count_distinctDirtyFlag = true ;
    }

    /**
     * 获取 [STAT_COUNT_DISTINCT]脏标记
     */
    @JsonIgnore
    public boolean getStat_count_distinctDirtyFlag(){
        return stat_count_distinctDirtyFlag ;
    }

    /**
     * 获取 [STAT_MY_THIS_MONTH]
     */
    @JsonProperty("stat_my_this_month")
    public Integer getStat_my_this_month(){
        return stat_my_this_month ;
    }

    /**
     * 设置 [STAT_MY_THIS_MONTH]
     */
    @JsonProperty("stat_my_this_month")
    public void setStat_my_this_month(Integer  stat_my_this_month){
        this.stat_my_this_month = stat_my_this_month ;
        this.stat_my_this_monthDirtyFlag = true ;
    }

    /**
     * 获取 [STAT_MY_THIS_MONTH]脏标记
     */
    @JsonIgnore
    public boolean getStat_my_this_monthDirtyFlag(){
        return stat_my_this_monthDirtyFlag ;
    }

    /**
     * 获取 [CHALLENGE_IDS]
     */
    @JsonProperty("challenge_ids")
    public String getChallenge_ids(){
        return challenge_ids ;
    }

    /**
     * 设置 [CHALLENGE_IDS]
     */
    @JsonProperty("challenge_ids")
    public void setChallenge_ids(String  challenge_ids){
        this.challenge_ids = challenge_ids ;
        this.challenge_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHALLENGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_idsDirtyFlag(){
        return challenge_idsDirtyFlag ;
    }

    /**
     * 获取 [GRANTED_EMPLOYEES_COUNT]
     */
    @JsonProperty("granted_employees_count")
    public Integer getGranted_employees_count(){
        return granted_employees_count ;
    }

    /**
     * 设置 [GRANTED_EMPLOYEES_COUNT]
     */
    @JsonProperty("granted_employees_count")
    public void setGranted_employees_count(Integer  granted_employees_count){
        this.granted_employees_count = granted_employees_count ;
        this.granted_employees_countDirtyFlag = true ;
    }

    /**
     * 获取 [GRANTED_EMPLOYEES_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getGranted_employees_countDirtyFlag(){
        return granted_employees_countDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Gamification_badge toDO() {
        Gamification_badge srfdomain = new Gamification_badge();
        if(getRule_authDirtyFlag())
            srfdomain.setRule_auth(rule_auth);
        if(getRule_max_numberDirtyFlag())
            srfdomain.setRule_max_number(rule_max_number);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getRule_maxDirtyFlag())
            srfdomain.setRule_max(rule_max);
        if(getRule_auth_user_idsDirtyFlag())
            srfdomain.setRule_auth_user_ids(rule_auth_user_ids);
        if(getRule_auth_badge_idsDirtyFlag())
            srfdomain.setRule_auth_badge_ids(rule_auth_badge_ids);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getRemaining_sendingDirtyFlag())
            srfdomain.setRemaining_sending(remaining_sending);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getStat_myDirtyFlag())
            srfdomain.setStat_my(stat_my);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getUnique_owner_idsDirtyFlag())
            srfdomain.setUnique_owner_ids(unique_owner_ids);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getLevelDirtyFlag())
            srfdomain.setLevel(level);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getOwner_idsDirtyFlag())
            srfdomain.setOwner_ids(owner_ids);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getGoal_definition_idsDirtyFlag())
            srfdomain.setGoal_definition_ids(goal_definition_ids);
        if(getStat_my_monthly_sendingDirtyFlag())
            srfdomain.setStat_my_monthly_sending(stat_my_monthly_sending);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getStat_countDirtyFlag())
            srfdomain.setStat_count(stat_count);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getStat_this_monthDirtyFlag())
            srfdomain.setStat_this_month(stat_this_month);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getStat_count_distinctDirtyFlag())
            srfdomain.setStat_count_distinct(stat_count_distinct);
        if(getStat_my_this_monthDirtyFlag())
            srfdomain.setStat_my_this_month(stat_my_this_month);
        if(getChallenge_idsDirtyFlag())
            srfdomain.setChallenge_ids(challenge_ids);
        if(getGranted_employees_countDirtyFlag())
            srfdomain.setGranted_employees_count(granted_employees_count);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Gamification_badge srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getRule_authDirtyFlag())
            this.setRule_auth(srfdomain.getRule_auth());
        if(srfdomain.getRule_max_numberDirtyFlag())
            this.setRule_max_number(srfdomain.getRule_max_number());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getRule_maxDirtyFlag())
            this.setRule_max(srfdomain.getRule_max());
        if(srfdomain.getRule_auth_user_idsDirtyFlag())
            this.setRule_auth_user_ids(srfdomain.getRule_auth_user_ids());
        if(srfdomain.getRule_auth_badge_idsDirtyFlag())
            this.setRule_auth_badge_ids(srfdomain.getRule_auth_badge_ids());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getRemaining_sendingDirtyFlag())
            this.setRemaining_sending(srfdomain.getRemaining_sending());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getStat_myDirtyFlag())
            this.setStat_my(srfdomain.getStat_my());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getUnique_owner_idsDirtyFlag())
            this.setUnique_owner_ids(srfdomain.getUnique_owner_ids());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getLevelDirtyFlag())
            this.setLevel(srfdomain.getLevel());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getOwner_idsDirtyFlag())
            this.setOwner_ids(srfdomain.getOwner_ids());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getGoal_definition_idsDirtyFlag())
            this.setGoal_definition_ids(srfdomain.getGoal_definition_ids());
        if(srfdomain.getStat_my_monthly_sendingDirtyFlag())
            this.setStat_my_monthly_sending(srfdomain.getStat_my_monthly_sending());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getStat_countDirtyFlag())
            this.setStat_count(srfdomain.getStat_count());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getStat_this_monthDirtyFlag())
            this.setStat_this_month(srfdomain.getStat_this_month());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getStat_count_distinctDirtyFlag())
            this.setStat_count_distinct(srfdomain.getStat_count_distinct());
        if(srfdomain.getStat_my_this_monthDirtyFlag())
            this.setStat_my_this_month(srfdomain.getStat_my_this_month());
        if(srfdomain.getChallenge_idsDirtyFlag())
            this.setChallenge_ids(srfdomain.getChallenge_ids());
        if(srfdomain.getGranted_employees_countDirtyFlag())
            this.setGranted_employees_count(srfdomain.getGranted_employees_count());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Gamification_badgeDTO> fromDOPage(List<Gamification_badge> poPage)   {
        if(poPage == null)
            return null;
        List<Gamification_badgeDTO> dtos=new ArrayList<Gamification_badgeDTO>();
        for(Gamification_badge domain : poPage) {
            Gamification_badgeDTO dto = new Gamification_badgeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

