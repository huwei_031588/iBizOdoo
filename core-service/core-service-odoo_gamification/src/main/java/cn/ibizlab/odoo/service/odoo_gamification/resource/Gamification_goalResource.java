package cn.ibizlab.odoo.service.odoo_gamification.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_gamification.dto.Gamification_goalDTO;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_goalService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goalSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Gamification_goal" })
@RestController
@RequestMapping("")
public class Gamification_goalResource {

    @Autowired
    private IGamification_goalService gamification_goalService;

    public IGamification_goalService getGamification_goalService() {
        return this.gamification_goalService;
    }

    @ApiOperation(value = "建立数据", tags = {"Gamification_goal" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_goals")

    public ResponseEntity<Gamification_goalDTO> create(@RequestBody Gamification_goalDTO gamification_goaldto) {
        Gamification_goalDTO dto = new Gamification_goalDTO();
        Gamification_goal domain = gamification_goaldto.toDO();
		gamification_goalService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Gamification_goal" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_goals/{gamification_goal_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_goal_id") Integer gamification_goal_id) {
        Gamification_goalDTO gamification_goaldto = new Gamification_goalDTO();
		Gamification_goal domain = new Gamification_goal();
		gamification_goaldto.setId(gamification_goal_id);
		domain.setId(gamification_goal_id);
        Boolean rst = gamification_goalService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Gamification_goal" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_goals/createBatch")
    public ResponseEntity<Boolean> createBatchGamification_goal(@RequestBody List<Gamification_goalDTO> gamification_goaldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Gamification_goal" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_goals/{gamification_goal_id}")

    public ResponseEntity<Gamification_goalDTO> update(@PathVariable("gamification_goal_id") Integer gamification_goal_id, @RequestBody Gamification_goalDTO gamification_goaldto) {
		Gamification_goal domain = gamification_goaldto.toDO();
        domain.setId(gamification_goal_id);
		gamification_goalService.update(domain);
		Gamification_goalDTO dto = new Gamification_goalDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Gamification_goal" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_goals/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_goalDTO> gamification_goaldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Gamification_goal" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goals/{gamification_goal_id}")
    public ResponseEntity<Gamification_goalDTO> get(@PathVariable("gamification_goal_id") Integer gamification_goal_id) {
        Gamification_goalDTO dto = new Gamification_goalDTO();
        Gamification_goal domain = gamification_goalService.get(gamification_goal_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Gamification_goal" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_goals/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Gamification_goalDTO> gamification_goaldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Gamification_goal" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_gamification/gamification_goals/fetchdefault")
	public ResponseEntity<Page<Gamification_goalDTO>> fetchDefault(Gamification_goalSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Gamification_goalDTO> list = new ArrayList<Gamification_goalDTO>();
        
        Page<Gamification_goal> domains = gamification_goalService.searchDefault(context) ;
        for(Gamification_goal gamification_goal : domains.getContent()){
            Gamification_goalDTO dto = new Gamification_goalDTO();
            dto.fromDO(gamification_goal);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
