package cn.ibizlab.odoo.service.odoo_gamification.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_gamification.dto.Gamification_challenge_lineDTO;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_challenge_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challenge_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Gamification_challenge_line" })
@RestController
@RequestMapping("")
public class Gamification_challenge_lineResource {

    @Autowired
    private IGamification_challenge_lineService gamification_challenge_lineService;

    public IGamification_challenge_lineService getGamification_challenge_lineService() {
        return this.gamification_challenge_lineService;
    }

    @ApiOperation(value = "删除数据", tags = {"Gamification_challenge_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_challenge_lines/{gamification_challenge_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_challenge_line_id") Integer gamification_challenge_line_id) {
        Gamification_challenge_lineDTO gamification_challenge_linedto = new Gamification_challenge_lineDTO();
		Gamification_challenge_line domain = new Gamification_challenge_line();
		gamification_challenge_linedto.setId(gamification_challenge_line_id);
		domain.setId(gamification_challenge_line_id);
        Boolean rst = gamification_challenge_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Gamification_challenge_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_challenge_lines")

    public ResponseEntity<Gamification_challenge_lineDTO> create(@RequestBody Gamification_challenge_lineDTO gamification_challenge_linedto) {
        Gamification_challenge_lineDTO dto = new Gamification_challenge_lineDTO();
        Gamification_challenge_line domain = gamification_challenge_linedto.toDO();
		gamification_challenge_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Gamification_challenge_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_challenge_lines/{gamification_challenge_line_id}")
    public ResponseEntity<Gamification_challenge_lineDTO> get(@PathVariable("gamification_challenge_line_id") Integer gamification_challenge_line_id) {
        Gamification_challenge_lineDTO dto = new Gamification_challenge_lineDTO();
        Gamification_challenge_line domain = gamification_challenge_lineService.get(gamification_challenge_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Gamification_challenge_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_challenge_lines/{gamification_challenge_line_id}")

    public ResponseEntity<Gamification_challenge_lineDTO> update(@PathVariable("gamification_challenge_line_id") Integer gamification_challenge_line_id, @RequestBody Gamification_challenge_lineDTO gamification_challenge_linedto) {
		Gamification_challenge_line domain = gamification_challenge_linedto.toDO();
        domain.setId(gamification_challenge_line_id);
		gamification_challenge_lineService.update(domain);
		Gamification_challenge_lineDTO dto = new Gamification_challenge_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Gamification_challenge_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_challenge_lines/createBatch")
    public ResponseEntity<Boolean> createBatchGamification_challenge_line(@RequestBody List<Gamification_challenge_lineDTO> gamification_challenge_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Gamification_challenge_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_challenge_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Gamification_challenge_lineDTO> gamification_challenge_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Gamification_challenge_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_challenge_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_challenge_lineDTO> gamification_challenge_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Gamification_challenge_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_gamification/gamification_challenge_lines/fetchdefault")
	public ResponseEntity<Page<Gamification_challenge_lineDTO>> fetchDefault(Gamification_challenge_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Gamification_challenge_lineDTO> list = new ArrayList<Gamification_challenge_lineDTO>();
        
        Page<Gamification_challenge_line> domains = gamification_challenge_lineService.searchDefault(context) ;
        for(Gamification_challenge_line gamification_challenge_line : domains.getContent()){
            Gamification_challenge_lineDTO dto = new Gamification_challenge_lineDTO();
            dto.fromDO(gamification_challenge_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
