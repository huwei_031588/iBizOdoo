package cn.ibizlab.odoo.service.odoo_gamification.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_challenge_line.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Gamification_challenge_lineDTO]
 */
public class Gamification_challenge_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Gamification_challenge_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [TARGET_GOAL]
     *
     */
    @Gamification_challenge_lineTarget_goalDefault(info = "默认规则")
    private Double target_goal;

    @JsonIgnore
    private boolean target_goalDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Gamification_challenge_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Gamification_challenge_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Gamification_challenge_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Gamification_challenge_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Gamification_challenge_lineSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [DEFINITION_MONETARY]
     *
     */
    @Gamification_challenge_lineDefinition_monetaryDefault(info = "默认规则")
    private String definition_monetary;

    @JsonIgnore
    private boolean definition_monetaryDirtyFlag;

    /**
     * 属性 [CONDITION]
     *
     */
    @Gamification_challenge_lineConditionDefault(info = "默认规则")
    private String condition;

    @JsonIgnore
    private boolean conditionDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Gamification_challenge_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Gamification_challenge_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [DEFINITION_FULL_SUFFIX]
     *
     */
    @Gamification_challenge_lineDefinition_full_suffixDefault(info = "默认规则")
    private String definition_full_suffix;

    @JsonIgnore
    private boolean definition_full_suffixDirtyFlag;

    /**
     * 属性 [CHALLENGE_ID_TEXT]
     *
     */
    @Gamification_challenge_lineChallenge_id_textDefault(info = "默认规则")
    private String challenge_id_text;

    @JsonIgnore
    private boolean challenge_id_textDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Gamification_challenge_lineNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DEFINITION_SUFFIX]
     *
     */
    @Gamification_challenge_lineDefinition_suffixDefault(info = "默认规则")
    private String definition_suffix;

    @JsonIgnore
    private boolean definition_suffixDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Gamification_challenge_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CHALLENGE_ID]
     *
     */
    @Gamification_challenge_lineChallenge_idDefault(info = "默认规则")
    private Integer challenge_id;

    @JsonIgnore
    private boolean challenge_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Gamification_challenge_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [DEFINITION_ID]
     *
     */
    @Gamification_challenge_lineDefinition_idDefault(info = "默认规则")
    private Integer definition_id;

    @JsonIgnore
    private boolean definition_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [TARGET_GOAL]
     */
    @JsonProperty("target_goal")
    public Double getTarget_goal(){
        return target_goal ;
    }

    /**
     * 设置 [TARGET_GOAL]
     */
    @JsonProperty("target_goal")
    public void setTarget_goal(Double  target_goal){
        this.target_goal = target_goal ;
        this.target_goalDirtyFlag = true ;
    }

    /**
     * 获取 [TARGET_GOAL]脏标记
     */
    @JsonIgnore
    public boolean getTarget_goalDirtyFlag(){
        return target_goalDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [DEFINITION_MONETARY]
     */
    @JsonProperty("definition_monetary")
    public String getDefinition_monetary(){
        return definition_monetary ;
    }

    /**
     * 设置 [DEFINITION_MONETARY]
     */
    @JsonProperty("definition_monetary")
    public void setDefinition_monetary(String  definition_monetary){
        this.definition_monetary = definition_monetary ;
        this.definition_monetaryDirtyFlag = true ;
    }

    /**
     * 获取 [DEFINITION_MONETARY]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_monetaryDirtyFlag(){
        return definition_monetaryDirtyFlag ;
    }

    /**
     * 获取 [CONDITION]
     */
    @JsonProperty("condition")
    public String getCondition(){
        return condition ;
    }

    /**
     * 设置 [CONDITION]
     */
    @JsonProperty("condition")
    public void setCondition(String  condition){
        this.condition = condition ;
        this.conditionDirtyFlag = true ;
    }

    /**
     * 获取 [CONDITION]脏标记
     */
    @JsonIgnore
    public boolean getConditionDirtyFlag(){
        return conditionDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [DEFINITION_FULL_SUFFIX]
     */
    @JsonProperty("definition_full_suffix")
    public String getDefinition_full_suffix(){
        return definition_full_suffix ;
    }

    /**
     * 设置 [DEFINITION_FULL_SUFFIX]
     */
    @JsonProperty("definition_full_suffix")
    public void setDefinition_full_suffix(String  definition_full_suffix){
        this.definition_full_suffix = definition_full_suffix ;
        this.definition_full_suffixDirtyFlag = true ;
    }

    /**
     * 获取 [DEFINITION_FULL_SUFFIX]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_full_suffixDirtyFlag(){
        return definition_full_suffixDirtyFlag ;
    }

    /**
     * 获取 [CHALLENGE_ID_TEXT]
     */
    @JsonProperty("challenge_id_text")
    public String getChallenge_id_text(){
        return challenge_id_text ;
    }

    /**
     * 设置 [CHALLENGE_ID_TEXT]
     */
    @JsonProperty("challenge_id_text")
    public void setChallenge_id_text(String  challenge_id_text){
        this.challenge_id_text = challenge_id_text ;
        this.challenge_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHALLENGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_id_textDirtyFlag(){
        return challenge_id_textDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DEFINITION_SUFFIX]
     */
    @JsonProperty("definition_suffix")
    public String getDefinition_suffix(){
        return definition_suffix ;
    }

    /**
     * 设置 [DEFINITION_SUFFIX]
     */
    @JsonProperty("definition_suffix")
    public void setDefinition_suffix(String  definition_suffix){
        this.definition_suffix = definition_suffix ;
        this.definition_suffixDirtyFlag = true ;
    }

    /**
     * 获取 [DEFINITION_SUFFIX]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_suffixDirtyFlag(){
        return definition_suffixDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CHALLENGE_ID]
     */
    @JsonProperty("challenge_id")
    public Integer getChallenge_id(){
        return challenge_id ;
    }

    /**
     * 设置 [CHALLENGE_ID]
     */
    @JsonProperty("challenge_id")
    public void setChallenge_id(Integer  challenge_id){
        this.challenge_id = challenge_id ;
        this.challenge_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHALLENGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_idDirtyFlag(){
        return challenge_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [DEFINITION_ID]
     */
    @JsonProperty("definition_id")
    public Integer getDefinition_id(){
        return definition_id ;
    }

    /**
     * 设置 [DEFINITION_ID]
     */
    @JsonProperty("definition_id")
    public void setDefinition_id(Integer  definition_id){
        this.definition_id = definition_id ;
        this.definition_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEFINITION_ID]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_idDirtyFlag(){
        return definition_idDirtyFlag ;
    }



    public Gamification_challenge_line toDO() {
        Gamification_challenge_line srfdomain = new Gamification_challenge_line();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getTarget_goalDirtyFlag())
            srfdomain.setTarget_goal(target_goal);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getDefinition_monetaryDirtyFlag())
            srfdomain.setDefinition_monetary(definition_monetary);
        if(getConditionDirtyFlag())
            srfdomain.setCondition(condition);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getDefinition_full_suffixDirtyFlag())
            srfdomain.setDefinition_full_suffix(definition_full_suffix);
        if(getChallenge_id_textDirtyFlag())
            srfdomain.setChallenge_id_text(challenge_id_text);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDefinition_suffixDirtyFlag())
            srfdomain.setDefinition_suffix(definition_suffix);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getChallenge_idDirtyFlag())
            srfdomain.setChallenge_id(challenge_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getDefinition_idDirtyFlag())
            srfdomain.setDefinition_id(definition_id);

        return srfdomain;
    }

    public void fromDO(Gamification_challenge_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getTarget_goalDirtyFlag())
            this.setTarget_goal(srfdomain.getTarget_goal());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getDefinition_monetaryDirtyFlag())
            this.setDefinition_monetary(srfdomain.getDefinition_monetary());
        if(srfdomain.getConditionDirtyFlag())
            this.setCondition(srfdomain.getCondition());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getDefinition_full_suffixDirtyFlag())
            this.setDefinition_full_suffix(srfdomain.getDefinition_full_suffix());
        if(srfdomain.getChallenge_id_textDirtyFlag())
            this.setChallenge_id_text(srfdomain.getChallenge_id_text());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDefinition_suffixDirtyFlag())
            this.setDefinition_suffix(srfdomain.getDefinition_suffix());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getChallenge_idDirtyFlag())
            this.setChallenge_id(srfdomain.getChallenge_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getDefinition_idDirtyFlag())
            this.setDefinition_id(srfdomain.getDefinition_id());

    }

    public List<Gamification_challenge_lineDTO> fromDOPage(List<Gamification_challenge_line> poPage)   {
        if(poPage == null)
            return null;
        List<Gamification_challenge_lineDTO> dtos=new ArrayList<Gamification_challenge_lineDTO>();
        for(Gamification_challenge_line domain : poPage) {
            Gamification_challenge_lineDTO dto = new Gamification_challenge_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

