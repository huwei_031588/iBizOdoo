package cn.ibizlab.odoo.service.odoo_gamification.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.gamification")
@Data
public class odoo_gamificationServiceProperties {

	private boolean enabled;

	private boolean auth;


}