package cn.ibizlab.odoo.service.odoo_board.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.board")
@Data
public class odoo_boardServiceProperties {

	private boolean enabled;

	private boolean auth;


}