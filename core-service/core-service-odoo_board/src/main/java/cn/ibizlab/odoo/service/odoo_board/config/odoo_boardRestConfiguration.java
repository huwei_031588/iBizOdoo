package cn.ibizlab.odoo.service.odoo_board.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_board")
public class odoo_boardRestConfiguration {

}
