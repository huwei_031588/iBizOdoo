package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_analytic_accountDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_accountService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_accountSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_analytic_account" })
@RestController
@RequestMapping("")
public class Account_analytic_accountResource {

    @Autowired
    private IAccount_analytic_accountService account_analytic_accountService;

    public IAccount_analytic_accountService getAccount_analytic_accountService() {
        return this.account_analytic_accountService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_analytic_account" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_accounts/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_accountDTO> account_analytic_accountdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_analytic_account" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_accounts/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_analytic_account(@RequestBody List<Account_analytic_accountDTO> account_analytic_accountdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_analytic_account" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_accounts/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_analytic_accountDTO> account_analytic_accountdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_analytic_account" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_accounts/{account_analytic_account_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_account_id") Integer account_analytic_account_id) {
        Account_analytic_accountDTO account_analytic_accountdto = new Account_analytic_accountDTO();
		Account_analytic_account domain = new Account_analytic_account();
		account_analytic_accountdto.setId(account_analytic_account_id);
		domain.setId(account_analytic_account_id);
        Boolean rst = account_analytic_accountService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Account_analytic_account" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_accounts")

    public ResponseEntity<Account_analytic_accountDTO> create(@RequestBody Account_analytic_accountDTO account_analytic_accountdto) {
        Account_analytic_accountDTO dto = new Account_analytic_accountDTO();
        Account_analytic_account domain = account_analytic_accountdto.toDO();
		account_analytic_accountService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_analytic_account" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_accounts/{account_analytic_account_id}")

    public ResponseEntity<Account_analytic_accountDTO> update(@PathVariable("account_analytic_account_id") Integer account_analytic_account_id, @RequestBody Account_analytic_accountDTO account_analytic_accountdto) {
		Account_analytic_account domain = account_analytic_accountdto.toDO();
        domain.setId(account_analytic_account_id);
		account_analytic_accountService.update(domain);
		Account_analytic_accountDTO dto = new Account_analytic_accountDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_analytic_account" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_accounts/{account_analytic_account_id}")
    public ResponseEntity<Account_analytic_accountDTO> get(@PathVariable("account_analytic_account_id") Integer account_analytic_account_id) {
        Account_analytic_accountDTO dto = new Account_analytic_accountDTO();
        Account_analytic_account domain = account_analytic_accountService.get(account_analytic_account_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_analytic_account" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_analytic_accounts/fetchdefault")
	public ResponseEntity<Page<Account_analytic_accountDTO>> fetchDefault(Account_analytic_accountSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_analytic_accountDTO> list = new ArrayList<Account_analytic_accountDTO>();
        
        Page<Account_analytic_account> domains = account_analytic_accountService.searchDefault(context) ;
        for(Account_analytic_account account_analytic_account : domains.getContent()){
            Account_analytic_accountDTO dto = new Account_analytic_accountDTO();
            dto.fromDO(account_analytic_account);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
