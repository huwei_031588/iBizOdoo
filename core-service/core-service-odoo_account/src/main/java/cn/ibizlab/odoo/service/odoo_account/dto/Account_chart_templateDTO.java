package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_chart_template.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_chart_templateDTO]
 */
public class Account_chart_templateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CODE_DIGITS]
     *
     */
    @Account_chart_templateCode_digitsDefault(info = "默认规则")
    private Integer code_digits;

    @JsonIgnore
    private boolean code_digitsDirtyFlag;

    /**
     * 属性 [TRANSFER_ACCOUNT_CODE_PREFIX]
     *
     */
    @Account_chart_templateTransfer_account_code_prefixDefault(info = "默认规则")
    private String transfer_account_code_prefix;

    @JsonIgnore
    private boolean transfer_account_code_prefixDirtyFlag;

    /**
     * 属性 [BANK_ACCOUNT_CODE_PREFIX]
     *
     */
    @Account_chart_templateBank_account_code_prefixDefault(info = "默认规则")
    private String bank_account_code_prefix;

    @JsonIgnore
    private boolean bank_account_code_prefixDirtyFlag;

    /**
     * 属性 [SPOKEN_LANGUAGES]
     *
     */
    @Account_chart_templateSpoken_languagesDefault(info = "默认规则")
    private String spoken_languages;

    @JsonIgnore
    private boolean spoken_languagesDirtyFlag;

    /**
     * 属性 [TAX_TEMPLATE_IDS]
     *
     */
    @Account_chart_templateTax_template_idsDefault(info = "默认规则")
    private String tax_template_ids;

    @JsonIgnore
    private boolean tax_template_idsDirtyFlag;

    /**
     * 属性 [VISIBLE]
     *
     */
    @Account_chart_templateVisibleDefault(info = "默认规则")
    private String visible;

    @JsonIgnore
    private boolean visibleDirtyFlag;

    /**
     * 属性 [ACCOUNT_IDS]
     *
     */
    @Account_chart_templateAccount_idsDefault(info = "默认规则")
    private String account_ids;

    @JsonIgnore
    private boolean account_idsDirtyFlag;

    /**
     * 属性 [COMPLETE_TAX_SET]
     *
     */
    @Account_chart_templateComplete_tax_setDefault(info = "默认规则")
    private String complete_tax_set;

    @JsonIgnore
    private boolean complete_tax_setDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_chart_templateIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CASH_ACCOUNT_CODE_PREFIX]
     *
     */
    @Account_chart_templateCash_account_code_prefixDefault(info = "默认规则")
    private String cash_account_code_prefix;

    @JsonIgnore
    private boolean cash_account_code_prefixDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_chart_templateDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_chart_templateCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [USE_ANGLO_SAXON]
     *
     */
    @Account_chart_templateUse_anglo_saxonDefault(info = "默认规则")
    private String use_anglo_saxon;

    @JsonIgnore
    private boolean use_anglo_saxonDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_chart_template__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_chart_templateWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_chart_templateNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID_TEXT]
     *
     */
    @Account_chart_templateExpense_currency_exchange_account_id_textDefault(info = "默认规则")
    private String expense_currency_exchange_account_id_text;

    @JsonIgnore
    private boolean expense_currency_exchange_account_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_RECEIVABLE_ID_TEXT]
     *
     */
    @Account_chart_templateProperty_account_receivable_id_textDefault(info = "默认规则")
    private String property_account_receivable_id_text;

    @JsonIgnore
    private boolean property_account_receivable_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_chart_templateCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_ID_TEXT]
     *
     */
    @Account_chart_templateProperty_account_expense_id_textDefault(info = "默认规则")
    private String property_account_expense_id_text;

    @JsonIgnore
    private boolean property_account_expense_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_chart_templateCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Account_chart_templateParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID_TEXT]
     *
     */
    @Account_chart_templateProperty_stock_account_input_categ_id_textDefault(info = "默认规则")
    private String property_stock_account_input_categ_id_text;

    @JsonIgnore
    private boolean property_stock_account_input_categ_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_ID_TEXT]
     *
     */
    @Account_chart_templateProperty_account_income_id_textDefault(info = "默认规则")
    private String property_account_income_id_text;

    @JsonIgnore
    private boolean property_account_income_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID_TEXT]
     *
     */
    @Account_chart_templateProperty_account_expense_categ_id_textDefault(info = "默认规则")
    private String property_account_expense_categ_id_text;

    @JsonIgnore
    private boolean property_account_expense_categ_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_chart_templateWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID_TEXT]
     *
     */
    @Account_chart_templateIncome_currency_exchange_account_id_textDefault(info = "默认规则")
    private String income_currency_exchange_account_id_text;

    @JsonIgnore
    private boolean income_currency_exchange_account_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_PAYABLE_ID_TEXT]
     *
     */
    @Account_chart_templateProperty_account_payable_id_textDefault(info = "默认规则")
    private String property_account_payable_id_text;

    @JsonIgnore
    private boolean property_account_payable_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_CATEG_ID_TEXT]
     *
     */
    @Account_chart_templateProperty_account_income_categ_id_textDefault(info = "默认规则")
    private String property_account_income_categ_id_text;

    @JsonIgnore
    private boolean property_account_income_categ_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID_TEXT]
     *
     */
    @Account_chart_templateProperty_stock_valuation_account_id_textDefault(info = "默认规则")
    private String property_stock_valuation_account_id_text;

    @JsonIgnore
    private boolean property_stock_valuation_account_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID_TEXT]
     *
     */
    @Account_chart_templateProperty_stock_account_output_categ_id_textDefault(info = "默认规则")
    private String property_stock_account_output_categ_id_text;

    @JsonIgnore
    private boolean property_stock_account_output_categ_id_textDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     *
     */
    @Account_chart_templateProperty_stock_valuation_account_idDefault(info = "默认规则")
    private Integer property_stock_valuation_account_id;

    @JsonIgnore
    private boolean property_stock_valuation_account_idDirtyFlag;

    /**
     * 属性 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]
     *
     */
    @Account_chart_templateExpense_currency_exchange_account_idDefault(info = "默认规则")
    private Integer expense_currency_exchange_account_id;

    @JsonIgnore
    private boolean expense_currency_exchange_account_idDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_PAYABLE_ID]
     *
     */
    @Account_chart_templateProperty_account_payable_idDefault(info = "默认规则")
    private Integer property_account_payable_id;

    @JsonIgnore
    private boolean property_account_payable_idDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]
     *
     */
    @Account_chart_templateProperty_account_income_categ_idDefault(info = "默认规则")
    private Integer property_account_income_categ_id;

    @JsonIgnore
    private boolean property_account_income_categ_idDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     *
     */
    @Account_chart_templateProperty_stock_account_output_categ_idDefault(info = "默认规则")
    private Integer property_stock_account_output_categ_id;

    @JsonIgnore
    private boolean property_stock_account_output_categ_idDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_ID]
     *
     */
    @Account_chart_templateProperty_account_expense_idDefault(info = "默认规则")
    private Integer property_account_expense_id;

    @JsonIgnore
    private boolean property_account_expense_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_chart_templateCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Account_chart_templateParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     *
     */
    @Account_chart_templateProperty_account_receivable_idDefault(info = "默认规则")
    private Integer property_account_receivable_id;

    @JsonIgnore
    private boolean property_account_receivable_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_chart_templateWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]
     *
     */
    @Account_chart_templateProperty_account_expense_categ_idDefault(info = "默认规则")
    private Integer property_account_expense_categ_id;

    @JsonIgnore
    private boolean property_account_expense_categ_idDirtyFlag;

    /**
     * 属性 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]
     *
     */
    @Account_chart_templateIncome_currency_exchange_account_idDefault(info = "默认规则")
    private Integer income_currency_exchange_account_id;

    @JsonIgnore
    private boolean income_currency_exchange_account_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_chart_templateCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     *
     */
    @Account_chart_templateProperty_stock_account_input_categ_idDefault(info = "默认规则")
    private Integer property_stock_account_input_categ_id;

    @JsonIgnore
    private boolean property_stock_account_input_categ_idDirtyFlag;

    /**
     * 属性 [PROPERTY_ACCOUNT_INCOME_ID]
     *
     */
    @Account_chart_templateProperty_account_income_idDefault(info = "默认规则")
    private Integer property_account_income_id;

    @JsonIgnore
    private boolean property_account_income_idDirtyFlag;


    /**
     * 获取 [CODE_DIGITS]
     */
    @JsonProperty("code_digits")
    public Integer getCode_digits(){
        return code_digits ;
    }

    /**
     * 设置 [CODE_DIGITS]
     */
    @JsonProperty("code_digits")
    public void setCode_digits(Integer  code_digits){
        this.code_digits = code_digits ;
        this.code_digitsDirtyFlag = true ;
    }

    /**
     * 获取 [CODE_DIGITS]脏标记
     */
    @JsonIgnore
    public boolean getCode_digitsDirtyFlag(){
        return code_digitsDirtyFlag ;
    }

    /**
     * 获取 [TRANSFER_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("transfer_account_code_prefix")
    public String getTransfer_account_code_prefix(){
        return transfer_account_code_prefix ;
    }

    /**
     * 设置 [TRANSFER_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("transfer_account_code_prefix")
    public void setTransfer_account_code_prefix(String  transfer_account_code_prefix){
        this.transfer_account_code_prefix = transfer_account_code_prefix ;
        this.transfer_account_code_prefixDirtyFlag = true ;
    }

    /**
     * 获取 [TRANSFER_ACCOUNT_CODE_PREFIX]脏标记
     */
    @JsonIgnore
    public boolean getTransfer_account_code_prefixDirtyFlag(){
        return transfer_account_code_prefixDirtyFlag ;
    }

    /**
     * 获取 [BANK_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("bank_account_code_prefix")
    public String getBank_account_code_prefix(){
        return bank_account_code_prefix ;
    }

    /**
     * 设置 [BANK_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("bank_account_code_prefix")
    public void setBank_account_code_prefix(String  bank_account_code_prefix){
        this.bank_account_code_prefix = bank_account_code_prefix ;
        this.bank_account_code_prefixDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ACCOUNT_CODE_PREFIX]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_code_prefixDirtyFlag(){
        return bank_account_code_prefixDirtyFlag ;
    }

    /**
     * 获取 [SPOKEN_LANGUAGES]
     */
    @JsonProperty("spoken_languages")
    public String getSpoken_languages(){
        return spoken_languages ;
    }

    /**
     * 设置 [SPOKEN_LANGUAGES]
     */
    @JsonProperty("spoken_languages")
    public void setSpoken_languages(String  spoken_languages){
        this.spoken_languages = spoken_languages ;
        this.spoken_languagesDirtyFlag = true ;
    }

    /**
     * 获取 [SPOKEN_LANGUAGES]脏标记
     */
    @JsonIgnore
    public boolean getSpoken_languagesDirtyFlag(){
        return spoken_languagesDirtyFlag ;
    }

    /**
     * 获取 [TAX_TEMPLATE_IDS]
     */
    @JsonProperty("tax_template_ids")
    public String getTax_template_ids(){
        return tax_template_ids ;
    }

    /**
     * 设置 [TAX_TEMPLATE_IDS]
     */
    @JsonProperty("tax_template_ids")
    public void setTax_template_ids(String  tax_template_ids){
        this.tax_template_ids = tax_template_ids ;
        this.tax_template_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_TEMPLATE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTax_template_idsDirtyFlag(){
        return tax_template_idsDirtyFlag ;
    }

    /**
     * 获取 [VISIBLE]
     */
    @JsonProperty("visible")
    public String getVisible(){
        return visible ;
    }

    /**
     * 设置 [VISIBLE]
     */
    @JsonProperty("visible")
    public void setVisible(String  visible){
        this.visible = visible ;
        this.visibleDirtyFlag = true ;
    }

    /**
     * 获取 [VISIBLE]脏标记
     */
    @JsonIgnore
    public boolean getVisibleDirtyFlag(){
        return visibleDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_IDS]
     */
    @JsonProperty("account_ids")
    public String getAccount_ids(){
        return account_ids ;
    }

    /**
     * 设置 [ACCOUNT_IDS]
     */
    @JsonProperty("account_ids")
    public void setAccount_ids(String  account_ids){
        this.account_ids = account_ids ;
        this.account_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idsDirtyFlag(){
        return account_idsDirtyFlag ;
    }

    /**
     * 获取 [COMPLETE_TAX_SET]
     */
    @JsonProperty("complete_tax_set")
    public String getComplete_tax_set(){
        return complete_tax_set ;
    }

    /**
     * 设置 [COMPLETE_TAX_SET]
     */
    @JsonProperty("complete_tax_set")
    public void setComplete_tax_set(String  complete_tax_set){
        this.complete_tax_set = complete_tax_set ;
        this.complete_tax_setDirtyFlag = true ;
    }

    /**
     * 获取 [COMPLETE_TAX_SET]脏标记
     */
    @JsonIgnore
    public boolean getComplete_tax_setDirtyFlag(){
        return complete_tax_setDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CASH_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("cash_account_code_prefix")
    public String getCash_account_code_prefix(){
        return cash_account_code_prefix ;
    }

    /**
     * 设置 [CASH_ACCOUNT_CODE_PREFIX]
     */
    @JsonProperty("cash_account_code_prefix")
    public void setCash_account_code_prefix(String  cash_account_code_prefix){
        this.cash_account_code_prefix = cash_account_code_prefix ;
        this.cash_account_code_prefixDirtyFlag = true ;
    }

    /**
     * 获取 [CASH_ACCOUNT_CODE_PREFIX]脏标记
     */
    @JsonIgnore
    public boolean getCash_account_code_prefixDirtyFlag(){
        return cash_account_code_prefixDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [USE_ANGLO_SAXON]
     */
    @JsonProperty("use_anglo_saxon")
    public String getUse_anglo_saxon(){
        return use_anglo_saxon ;
    }

    /**
     * 设置 [USE_ANGLO_SAXON]
     */
    @JsonProperty("use_anglo_saxon")
    public void setUse_anglo_saxon(String  use_anglo_saxon){
        this.use_anglo_saxon = use_anglo_saxon ;
        this.use_anglo_saxonDirtyFlag = true ;
    }

    /**
     * 获取 [USE_ANGLO_SAXON]脏标记
     */
    @JsonIgnore
    public boolean getUse_anglo_saxonDirtyFlag(){
        return use_anglo_saxonDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("expense_currency_exchange_account_id_text")
    public String getExpense_currency_exchange_account_id_text(){
        return expense_currency_exchange_account_id_text ;
    }

    /**
     * 设置 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("expense_currency_exchange_account_id_text")
    public void setExpense_currency_exchange_account_id_text(String  expense_currency_exchange_account_id_text){
        this.expense_currency_exchange_account_id_text = expense_currency_exchange_account_id_text ;
        this.expense_currency_exchange_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getExpense_currency_exchange_account_id_textDirtyFlag(){
        return expense_currency_exchange_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_RECEIVABLE_ID_TEXT]
     */
    @JsonProperty("property_account_receivable_id_text")
    public String getProperty_account_receivable_id_text(){
        return property_account_receivable_id_text ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_RECEIVABLE_ID_TEXT]
     */
    @JsonProperty("property_account_receivable_id_text")
    public void setProperty_account_receivable_id_text(String  property_account_receivable_id_text){
        this.property_account_receivable_id_text = property_account_receivable_id_text ;
        this.property_account_receivable_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_RECEIVABLE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_receivable_id_textDirtyFlag(){
        return property_account_receivable_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_ID_TEXT]
     */
    @JsonProperty("property_account_expense_id_text")
    public String getProperty_account_expense_id_text(){
        return property_account_expense_id_text ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_EXPENSE_ID_TEXT]
     */
    @JsonProperty("property_account_expense_id_text")
    public void setProperty_account_expense_id_text(String  property_account_expense_id_text){
        this.property_account_expense_id_text = property_account_expense_id_text ;
        this.property_account_expense_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_id_textDirtyFlag(){
        return property_account_expense_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID_TEXT]
     */
    @JsonProperty("property_stock_account_input_categ_id_text")
    public String getProperty_stock_account_input_categ_id_text(){
        return property_stock_account_input_categ_id_text ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID_TEXT]
     */
    @JsonProperty("property_stock_account_input_categ_id_text")
    public void setProperty_stock_account_input_categ_id_text(String  property_stock_account_input_categ_id_text){
        this.property_stock_account_input_categ_id_text = property_stock_account_input_categ_id_text ;
        this.property_stock_account_input_categ_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_id_textDirtyFlag(){
        return property_stock_account_input_categ_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_ID_TEXT]
     */
    @JsonProperty("property_account_income_id_text")
    public String getProperty_account_income_id_text(){
        return property_account_income_id_text ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_INCOME_ID_TEXT]
     */
    @JsonProperty("property_account_income_id_text")
    public void setProperty_account_income_id_text(String  property_account_income_id_text){
        this.property_account_income_id_text = property_account_income_id_text ;
        this.property_account_income_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_id_textDirtyFlag(){
        return property_account_income_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID_TEXT]
     */
    @JsonProperty("property_account_expense_categ_id_text")
    public String getProperty_account_expense_categ_id_text(){
        return property_account_expense_categ_id_text ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID_TEXT]
     */
    @JsonProperty("property_account_expense_categ_id_text")
    public void setProperty_account_expense_categ_id_text(String  property_account_expense_categ_id_text){
        this.property_account_expense_categ_id_text = property_account_expense_categ_id_text ;
        this.property_account_expense_categ_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_categ_id_textDirtyFlag(){
        return property_account_expense_categ_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("income_currency_exchange_account_id_text")
    public String getIncome_currency_exchange_account_id_text(){
        return income_currency_exchange_account_id_text ;
    }

    /**
     * 设置 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("income_currency_exchange_account_id_text")
    public void setIncome_currency_exchange_account_id_text(String  income_currency_exchange_account_id_text){
        this.income_currency_exchange_account_id_text = income_currency_exchange_account_id_text ;
        this.income_currency_exchange_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getIncome_currency_exchange_account_id_textDirtyFlag(){
        return income_currency_exchange_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_PAYABLE_ID_TEXT]
     */
    @JsonProperty("property_account_payable_id_text")
    public String getProperty_account_payable_id_text(){
        return property_account_payable_id_text ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_PAYABLE_ID_TEXT]
     */
    @JsonProperty("property_account_payable_id_text")
    public void setProperty_account_payable_id_text(String  property_account_payable_id_text){
        this.property_account_payable_id_text = property_account_payable_id_text ;
        this.property_account_payable_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_PAYABLE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_payable_id_textDirtyFlag(){
        return property_account_payable_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_CATEG_ID_TEXT]
     */
    @JsonProperty("property_account_income_categ_id_text")
    public String getProperty_account_income_categ_id_text(){
        return property_account_income_categ_id_text ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_INCOME_CATEG_ID_TEXT]
     */
    @JsonProperty("property_account_income_categ_id_text")
    public void setProperty_account_income_categ_id_text(String  property_account_income_categ_id_text){
        this.property_account_income_categ_id_text = property_account_income_categ_id_text ;
        this.property_account_income_categ_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_CATEG_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_categ_id_textDirtyFlag(){
        return property_account_income_categ_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("property_stock_valuation_account_id_text")
    public String getProperty_stock_valuation_account_id_text(){
        return property_stock_valuation_account_id_text ;
    }

    /**
     * 设置 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("property_stock_valuation_account_id_text")
    public void setProperty_stock_valuation_account_id_text(String  property_stock_valuation_account_id_text){
        this.property_stock_valuation_account_id_text = property_stock_valuation_account_id_text ;
        this.property_stock_valuation_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_id_textDirtyFlag(){
        return property_stock_valuation_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID_TEXT]
     */
    @JsonProperty("property_stock_account_output_categ_id_text")
    public String getProperty_stock_account_output_categ_id_text(){
        return property_stock_account_output_categ_id_text ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID_TEXT]
     */
    @JsonProperty("property_stock_account_output_categ_id_text")
    public void setProperty_stock_account_output_categ_id_text(String  property_stock_account_output_categ_id_text){
        this.property_stock_account_output_categ_id_text = property_stock_account_output_categ_id_text ;
        this.property_stock_account_output_categ_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_id_textDirtyFlag(){
        return property_stock_account_output_categ_id_textDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public Integer getProperty_stock_valuation_account_id(){
        return property_stock_valuation_account_id ;
    }

    /**
     * 设置 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public void setProperty_stock_valuation_account_id(Integer  property_stock_valuation_account_id){
        this.property_stock_valuation_account_id = property_stock_valuation_account_id ;
        this.property_stock_valuation_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_VALUATION_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_idDirtyFlag(){
        return property_stock_valuation_account_idDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]
     */
    @JsonProperty("expense_currency_exchange_account_id")
    public Integer getExpense_currency_exchange_account_id(){
        return expense_currency_exchange_account_id ;
    }

    /**
     * 设置 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]
     */
    @JsonProperty("expense_currency_exchange_account_id")
    public void setExpense_currency_exchange_account_id(Integer  expense_currency_exchange_account_id){
        this.expense_currency_exchange_account_id = expense_currency_exchange_account_id ;
        this.expense_currency_exchange_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_CURRENCY_EXCHANGE_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getExpense_currency_exchange_account_idDirtyFlag(){
        return expense_currency_exchange_account_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_PAYABLE_ID]
     */
    @JsonProperty("property_account_payable_id")
    public Integer getProperty_account_payable_id(){
        return property_account_payable_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_PAYABLE_ID]
     */
    @JsonProperty("property_account_payable_id")
    public void setProperty_account_payable_id(Integer  property_account_payable_id){
        this.property_account_payable_id = property_account_payable_id ;
        this.property_account_payable_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_PAYABLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_payable_idDirtyFlag(){
        return property_account_payable_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]
     */
    @JsonProperty("property_account_income_categ_id")
    public Integer getProperty_account_income_categ_id(){
        return property_account_income_categ_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]
     */
    @JsonProperty("property_account_income_categ_id")
    public void setProperty_account_income_categ_id(Integer  property_account_income_categ_id){
        this.property_account_income_categ_id = property_account_income_categ_id ;
        this.property_account_income_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_categ_idDirtyFlag(){
        return property_account_income_categ_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public Integer getProperty_stock_account_output_categ_id(){
        return property_stock_account_output_categ_id ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public void setProperty_stock_account_output_categ_id(Integer  property_stock_account_output_categ_id){
        this.property_stock_account_output_categ_id = property_stock_account_output_categ_id ;
        this.property_stock_account_output_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_OUTPUT_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_idDirtyFlag(){
        return property_stock_account_output_categ_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_ID]
     */
    @JsonProperty("property_account_expense_id")
    public Integer getProperty_account_expense_id(){
        return property_account_expense_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_EXPENSE_ID]
     */
    @JsonProperty("property_account_expense_id")
    public void setProperty_account_expense_id(Integer  property_account_expense_id){
        this.property_account_expense_id = property_account_expense_id ;
        this.property_account_expense_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_idDirtyFlag(){
        return property_account_expense_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     */
    @JsonProperty("property_account_receivable_id")
    public Integer getProperty_account_receivable_id(){
        return property_account_receivable_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_RECEIVABLE_ID]
     */
    @JsonProperty("property_account_receivable_id")
    public void setProperty_account_receivable_id(Integer  property_account_receivable_id){
        this.property_account_receivable_id = property_account_receivable_id ;
        this.property_account_receivable_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_RECEIVABLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_receivable_idDirtyFlag(){
        return property_account_receivable_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]
     */
    @JsonProperty("property_account_expense_categ_id")
    public Integer getProperty_account_expense_categ_id(){
        return property_account_expense_categ_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]
     */
    @JsonProperty("property_account_expense_categ_id")
    public void setProperty_account_expense_categ_id(Integer  property_account_expense_categ_id){
        this.property_account_expense_categ_id = property_account_expense_categ_id ;
        this.property_account_expense_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_EXPENSE_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_categ_idDirtyFlag(){
        return property_account_expense_categ_idDirtyFlag ;
    }

    /**
     * 获取 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]
     */
    @JsonProperty("income_currency_exchange_account_id")
    public Integer getIncome_currency_exchange_account_id(){
        return income_currency_exchange_account_id ;
    }

    /**
     * 设置 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]
     */
    @JsonProperty("income_currency_exchange_account_id")
    public void setIncome_currency_exchange_account_id(Integer  income_currency_exchange_account_id){
        this.income_currency_exchange_account_id = income_currency_exchange_account_id ;
        this.income_currency_exchange_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [INCOME_CURRENCY_EXCHANGE_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getIncome_currency_exchange_account_idDirtyFlag(){
        return income_currency_exchange_account_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public Integer getProperty_stock_account_input_categ_id(){
        return property_stock_account_input_categ_id ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public void setProperty_stock_account_input_categ_id(Integer  property_stock_account_input_categ_id){
        this.property_stock_account_input_categ_id = property_stock_account_input_categ_id ;
        this.property_stock_account_input_categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ACCOUNT_INPUT_CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_idDirtyFlag(){
        return property_stock_account_input_categ_idDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_ID]
     */
    @JsonProperty("property_account_income_id")
    public Integer getProperty_account_income_id(){
        return property_account_income_id ;
    }

    /**
     * 设置 [PROPERTY_ACCOUNT_INCOME_ID]
     */
    @JsonProperty("property_account_income_id")
    public void setProperty_account_income_id(Integer  property_account_income_id){
        this.property_account_income_id = property_account_income_id ;
        this.property_account_income_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_ACCOUNT_INCOME_ID]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_idDirtyFlag(){
        return property_account_income_idDirtyFlag ;
    }



    public Account_chart_template toDO() {
        Account_chart_template srfdomain = new Account_chart_template();
        if(getCode_digitsDirtyFlag())
            srfdomain.setCode_digits(code_digits);
        if(getTransfer_account_code_prefixDirtyFlag())
            srfdomain.setTransfer_account_code_prefix(transfer_account_code_prefix);
        if(getBank_account_code_prefixDirtyFlag())
            srfdomain.setBank_account_code_prefix(bank_account_code_prefix);
        if(getSpoken_languagesDirtyFlag())
            srfdomain.setSpoken_languages(spoken_languages);
        if(getTax_template_idsDirtyFlag())
            srfdomain.setTax_template_ids(tax_template_ids);
        if(getVisibleDirtyFlag())
            srfdomain.setVisible(visible);
        if(getAccount_idsDirtyFlag())
            srfdomain.setAccount_ids(account_ids);
        if(getComplete_tax_setDirtyFlag())
            srfdomain.setComplete_tax_set(complete_tax_set);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCash_account_code_prefixDirtyFlag())
            srfdomain.setCash_account_code_prefix(cash_account_code_prefix);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getUse_anglo_saxonDirtyFlag())
            srfdomain.setUse_anglo_saxon(use_anglo_saxon);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getExpense_currency_exchange_account_id_textDirtyFlag())
            srfdomain.setExpense_currency_exchange_account_id_text(expense_currency_exchange_account_id_text);
        if(getProperty_account_receivable_id_textDirtyFlag())
            srfdomain.setProperty_account_receivable_id_text(property_account_receivable_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getProperty_account_expense_id_textDirtyFlag())
            srfdomain.setProperty_account_expense_id_text(property_account_expense_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getProperty_stock_account_input_categ_id_textDirtyFlag())
            srfdomain.setProperty_stock_account_input_categ_id_text(property_stock_account_input_categ_id_text);
        if(getProperty_account_income_id_textDirtyFlag())
            srfdomain.setProperty_account_income_id_text(property_account_income_id_text);
        if(getProperty_account_expense_categ_id_textDirtyFlag())
            srfdomain.setProperty_account_expense_categ_id_text(property_account_expense_categ_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getIncome_currency_exchange_account_id_textDirtyFlag())
            srfdomain.setIncome_currency_exchange_account_id_text(income_currency_exchange_account_id_text);
        if(getProperty_account_payable_id_textDirtyFlag())
            srfdomain.setProperty_account_payable_id_text(property_account_payable_id_text);
        if(getProperty_account_income_categ_id_textDirtyFlag())
            srfdomain.setProperty_account_income_categ_id_text(property_account_income_categ_id_text);
        if(getProperty_stock_valuation_account_id_textDirtyFlag())
            srfdomain.setProperty_stock_valuation_account_id_text(property_stock_valuation_account_id_text);
        if(getProperty_stock_account_output_categ_id_textDirtyFlag())
            srfdomain.setProperty_stock_account_output_categ_id_text(property_stock_account_output_categ_id_text);
        if(getProperty_stock_valuation_account_idDirtyFlag())
            srfdomain.setProperty_stock_valuation_account_id(property_stock_valuation_account_id);
        if(getExpense_currency_exchange_account_idDirtyFlag())
            srfdomain.setExpense_currency_exchange_account_id(expense_currency_exchange_account_id);
        if(getProperty_account_payable_idDirtyFlag())
            srfdomain.setProperty_account_payable_id(property_account_payable_id);
        if(getProperty_account_income_categ_idDirtyFlag())
            srfdomain.setProperty_account_income_categ_id(property_account_income_categ_id);
        if(getProperty_stock_account_output_categ_idDirtyFlag())
            srfdomain.setProperty_stock_account_output_categ_id(property_stock_account_output_categ_id);
        if(getProperty_account_expense_idDirtyFlag())
            srfdomain.setProperty_account_expense_id(property_account_expense_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getProperty_account_receivable_idDirtyFlag())
            srfdomain.setProperty_account_receivable_id(property_account_receivable_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getProperty_account_expense_categ_idDirtyFlag())
            srfdomain.setProperty_account_expense_categ_id(property_account_expense_categ_id);
        if(getIncome_currency_exchange_account_idDirtyFlag())
            srfdomain.setIncome_currency_exchange_account_id(income_currency_exchange_account_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getProperty_stock_account_input_categ_idDirtyFlag())
            srfdomain.setProperty_stock_account_input_categ_id(property_stock_account_input_categ_id);
        if(getProperty_account_income_idDirtyFlag())
            srfdomain.setProperty_account_income_id(property_account_income_id);

        return srfdomain;
    }

    public void fromDO(Account_chart_template srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCode_digitsDirtyFlag())
            this.setCode_digits(srfdomain.getCode_digits());
        if(srfdomain.getTransfer_account_code_prefixDirtyFlag())
            this.setTransfer_account_code_prefix(srfdomain.getTransfer_account_code_prefix());
        if(srfdomain.getBank_account_code_prefixDirtyFlag())
            this.setBank_account_code_prefix(srfdomain.getBank_account_code_prefix());
        if(srfdomain.getSpoken_languagesDirtyFlag())
            this.setSpoken_languages(srfdomain.getSpoken_languages());
        if(srfdomain.getTax_template_idsDirtyFlag())
            this.setTax_template_ids(srfdomain.getTax_template_ids());
        if(srfdomain.getVisibleDirtyFlag())
            this.setVisible(srfdomain.getVisible());
        if(srfdomain.getAccount_idsDirtyFlag())
            this.setAccount_ids(srfdomain.getAccount_ids());
        if(srfdomain.getComplete_tax_setDirtyFlag())
            this.setComplete_tax_set(srfdomain.getComplete_tax_set());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCash_account_code_prefixDirtyFlag())
            this.setCash_account_code_prefix(srfdomain.getCash_account_code_prefix());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getUse_anglo_saxonDirtyFlag())
            this.setUse_anglo_saxon(srfdomain.getUse_anglo_saxon());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getExpense_currency_exchange_account_id_textDirtyFlag())
            this.setExpense_currency_exchange_account_id_text(srfdomain.getExpense_currency_exchange_account_id_text());
        if(srfdomain.getProperty_account_receivable_id_textDirtyFlag())
            this.setProperty_account_receivable_id_text(srfdomain.getProperty_account_receivable_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getProperty_account_expense_id_textDirtyFlag())
            this.setProperty_account_expense_id_text(srfdomain.getProperty_account_expense_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getProperty_stock_account_input_categ_id_textDirtyFlag())
            this.setProperty_stock_account_input_categ_id_text(srfdomain.getProperty_stock_account_input_categ_id_text());
        if(srfdomain.getProperty_account_income_id_textDirtyFlag())
            this.setProperty_account_income_id_text(srfdomain.getProperty_account_income_id_text());
        if(srfdomain.getProperty_account_expense_categ_id_textDirtyFlag())
            this.setProperty_account_expense_categ_id_text(srfdomain.getProperty_account_expense_categ_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getIncome_currency_exchange_account_id_textDirtyFlag())
            this.setIncome_currency_exchange_account_id_text(srfdomain.getIncome_currency_exchange_account_id_text());
        if(srfdomain.getProperty_account_payable_id_textDirtyFlag())
            this.setProperty_account_payable_id_text(srfdomain.getProperty_account_payable_id_text());
        if(srfdomain.getProperty_account_income_categ_id_textDirtyFlag())
            this.setProperty_account_income_categ_id_text(srfdomain.getProperty_account_income_categ_id_text());
        if(srfdomain.getProperty_stock_valuation_account_id_textDirtyFlag())
            this.setProperty_stock_valuation_account_id_text(srfdomain.getProperty_stock_valuation_account_id_text());
        if(srfdomain.getProperty_stock_account_output_categ_id_textDirtyFlag())
            this.setProperty_stock_account_output_categ_id_text(srfdomain.getProperty_stock_account_output_categ_id_text());
        if(srfdomain.getProperty_stock_valuation_account_idDirtyFlag())
            this.setProperty_stock_valuation_account_id(srfdomain.getProperty_stock_valuation_account_id());
        if(srfdomain.getExpense_currency_exchange_account_idDirtyFlag())
            this.setExpense_currency_exchange_account_id(srfdomain.getExpense_currency_exchange_account_id());
        if(srfdomain.getProperty_account_payable_idDirtyFlag())
            this.setProperty_account_payable_id(srfdomain.getProperty_account_payable_id());
        if(srfdomain.getProperty_account_income_categ_idDirtyFlag())
            this.setProperty_account_income_categ_id(srfdomain.getProperty_account_income_categ_id());
        if(srfdomain.getProperty_stock_account_output_categ_idDirtyFlag())
            this.setProperty_stock_account_output_categ_id(srfdomain.getProperty_stock_account_output_categ_id());
        if(srfdomain.getProperty_account_expense_idDirtyFlag())
            this.setProperty_account_expense_id(srfdomain.getProperty_account_expense_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getProperty_account_receivable_idDirtyFlag())
            this.setProperty_account_receivable_id(srfdomain.getProperty_account_receivable_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getProperty_account_expense_categ_idDirtyFlag())
            this.setProperty_account_expense_categ_id(srfdomain.getProperty_account_expense_categ_id());
        if(srfdomain.getIncome_currency_exchange_account_idDirtyFlag())
            this.setIncome_currency_exchange_account_id(srfdomain.getIncome_currency_exchange_account_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getProperty_stock_account_input_categ_idDirtyFlag())
            this.setProperty_stock_account_input_categ_id(srfdomain.getProperty_stock_account_input_categ_id());
        if(srfdomain.getProperty_account_income_idDirtyFlag())
            this.setProperty_account_income_id(srfdomain.getProperty_account_income_id());

    }

    public List<Account_chart_templateDTO> fromDOPage(List<Account_chart_template> poPage)   {
        if(poPage == null)
            return null;
        List<Account_chart_templateDTO> dtos=new ArrayList<Account_chart_templateDTO>();
        for(Account_chart_template domain : poPage) {
            Account_chart_templateDTO dto = new Account_chart_templateDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

