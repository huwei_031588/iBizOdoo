package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_reconcile_model_templateDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconcile_model_templateService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_reconcile_model_template" })
@RestController
@RequestMapping("")
public class Account_reconcile_model_templateResource {

    @Autowired
    private IAccount_reconcile_model_templateService account_reconcile_model_templateService;

    public IAccount_reconcile_model_templateService getAccount_reconcile_model_templateService() {
        return this.account_reconcile_model_templateService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_reconcile_model_template" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconcile_model_templates/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_reconcile_model_templateDTO> account_reconcile_model_templatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_reconcile_model_template" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconcile_model_templates/{account_reconcile_model_template_id}")
    public ResponseEntity<Account_reconcile_model_templateDTO> get(@PathVariable("account_reconcile_model_template_id") Integer account_reconcile_model_template_id) {
        Account_reconcile_model_templateDTO dto = new Account_reconcile_model_templateDTO();
        Account_reconcile_model_template domain = account_reconcile_model_templateService.get(account_reconcile_model_template_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_reconcile_model_template" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconcile_model_templates/{account_reconcile_model_template_id}")

    public ResponseEntity<Account_reconcile_model_templateDTO> update(@PathVariable("account_reconcile_model_template_id") Integer account_reconcile_model_template_id, @RequestBody Account_reconcile_model_templateDTO account_reconcile_model_templatedto) {
		Account_reconcile_model_template domain = account_reconcile_model_templatedto.toDO();
        domain.setId(account_reconcile_model_template_id);
		account_reconcile_model_templateService.update(domain);
		Account_reconcile_model_templateDTO dto = new Account_reconcile_model_templateDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_reconcile_model_template" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconcile_model_templates/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_reconcile_model_template(@RequestBody List<Account_reconcile_model_templateDTO> account_reconcile_model_templatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_reconcile_model_template" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconcile_model_templates/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_reconcile_model_templateDTO> account_reconcile_model_templatedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_reconcile_model_template" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconcile_model_templates/{account_reconcile_model_template_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_reconcile_model_template_id") Integer account_reconcile_model_template_id) {
        Account_reconcile_model_templateDTO account_reconcile_model_templatedto = new Account_reconcile_model_templateDTO();
		Account_reconcile_model_template domain = new Account_reconcile_model_template();
		account_reconcile_model_templatedto.setId(account_reconcile_model_template_id);
		domain.setId(account_reconcile_model_template_id);
        Boolean rst = account_reconcile_model_templateService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Account_reconcile_model_template" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconcile_model_templates")

    public ResponseEntity<Account_reconcile_model_templateDTO> create(@RequestBody Account_reconcile_model_templateDTO account_reconcile_model_templatedto) {
        Account_reconcile_model_templateDTO dto = new Account_reconcile_model_templateDTO();
        Account_reconcile_model_template domain = account_reconcile_model_templatedto.toDO();
		account_reconcile_model_templateService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_reconcile_model_template" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_reconcile_model_templates/fetchdefault")
	public ResponseEntity<Page<Account_reconcile_model_templateDTO>> fetchDefault(Account_reconcile_model_templateSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_reconcile_model_templateDTO> list = new ArrayList<Account_reconcile_model_templateDTO>();
        
        Page<Account_reconcile_model_template> domains = account_reconcile_model_templateService.searchDefault(context) ;
        for(Account_reconcile_model_template account_reconcile_model_template : domains.getContent()){
            Account_reconcile_model_templateDTO dto = new Account_reconcile_model_templateDTO();
            dto.fromDO(account_reconcile_model_template);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
