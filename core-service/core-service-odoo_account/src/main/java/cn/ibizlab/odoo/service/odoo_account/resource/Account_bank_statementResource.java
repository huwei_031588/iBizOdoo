package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_bank_statementDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statementService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statementSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_bank_statement" })
@RestController
@RequestMapping("")
public class Account_bank_statementResource {

    @Autowired
    private IAccount_bank_statementService account_bank_statementService;

    public IAccount_bank_statementService getAccount_bank_statementService() {
        return this.account_bank_statementService;
    }

    @ApiOperation(value = "删除数据", tags = {"Account_bank_statement" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statements/{account_bank_statement_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_id") Integer account_bank_statement_id) {
        Account_bank_statementDTO account_bank_statementdto = new Account_bank_statementDTO();
		Account_bank_statement domain = new Account_bank_statement();
		account_bank_statementdto.setId(account_bank_statement_id);
		domain.setId(account_bank_statement_id);
        Boolean rst = account_bank_statementService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Account_bank_statement" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statements")

    public ResponseEntity<Account_bank_statementDTO> create(@RequestBody Account_bank_statementDTO account_bank_statementdto) {
        Account_bank_statementDTO dto = new Account_bank_statementDTO();
        Account_bank_statement domain = account_bank_statementdto.toDO();
		account_bank_statementService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_bank_statement" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statements/{account_bank_statement_id}")
    public ResponseEntity<Account_bank_statementDTO> get(@PathVariable("account_bank_statement_id") Integer account_bank_statement_id) {
        Account_bank_statementDTO dto = new Account_bank_statementDTO();
        Account_bank_statement domain = account_bank_statementService.get(account_bank_statement_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_bank_statement" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statements/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_bank_statementDTO> account_bank_statementdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_bank_statement" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statements/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statementDTO> account_bank_statementdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_bank_statement" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statements/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_bank_statement(@RequestBody List<Account_bank_statementDTO> account_bank_statementdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_bank_statement" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statements/{account_bank_statement_id}")

    public ResponseEntity<Account_bank_statementDTO> update(@PathVariable("account_bank_statement_id") Integer account_bank_statement_id, @RequestBody Account_bank_statementDTO account_bank_statementdto) {
		Account_bank_statement domain = account_bank_statementdto.toDO();
        domain.setId(account_bank_statement_id);
		account_bank_statementService.update(domain);
		Account_bank_statementDTO dto = new Account_bank_statementDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_bank_statement" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_bank_statements/fetchdefault")
	public ResponseEntity<Page<Account_bank_statementDTO>> fetchDefault(Account_bank_statementSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_bank_statementDTO> list = new ArrayList<Account_bank_statementDTO>();
        
        Page<Account_bank_statement> domains = account_bank_statementService.searchDefault(context) ;
        for(Account_bank_statement account_bank_statement : domains.getContent()){
            Account_bank_statementDTO dto = new Account_bank_statementDTO();
            dto.fromDO(account_bank_statement);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
