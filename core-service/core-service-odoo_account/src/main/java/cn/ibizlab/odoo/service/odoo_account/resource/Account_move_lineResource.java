package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_move_lineDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_move_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_move_line" })
@RestController
@RequestMapping("")
public class Account_move_lineResource {

    @Autowired
    private IAccount_move_lineService account_move_lineService;

    public IAccount_move_lineService getAccount_move_lineService() {
        return this.account_move_lineService;
    }

    @ApiOperation(value = "更新数据", tags = {"Account_move_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_move_lines/{account_move_line_id}")

    public ResponseEntity<Account_move_lineDTO> update(@PathVariable("account_move_line_id") Integer account_move_line_id, @RequestBody Account_move_lineDTO account_move_linedto) {
		Account_move_line domain = account_move_linedto.toDO();
        domain.setId(account_move_line_id);
		account_move_lineService.update(domain);
		Account_move_lineDTO dto = new Account_move_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_move_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_move_lines")

    public ResponseEntity<Account_move_lineDTO> create(@RequestBody Account_move_lineDTO account_move_linedto) {
        Account_move_lineDTO dto = new Account_move_lineDTO();
        Account_move_line domain = account_move_linedto.toDO();
		account_move_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_move_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_move_lines/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_move_line(@RequestBody List<Account_move_lineDTO> account_move_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_move_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_move_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_move_lineDTO> account_move_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_move_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_move_lines/{account_move_line_id}")
    public ResponseEntity<Account_move_lineDTO> get(@PathVariable("account_move_line_id") Integer account_move_line_id) {
        Account_move_lineDTO dto = new Account_move_lineDTO();
        Account_move_line domain = account_move_lineService.get(account_move_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_move_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_move_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_move_lineDTO> account_move_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_move_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_move_lines/{account_move_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_move_line_id") Integer account_move_line_id) {
        Account_move_lineDTO account_move_linedto = new Account_move_lineDTO();
		Account_move_line domain = new Account_move_line();
		account_move_linedto.setId(account_move_line_id);
		domain.setId(account_move_line_id);
        Boolean rst = account_move_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_move_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_move_lines/fetchdefault")
	public ResponseEntity<Page<Account_move_lineDTO>> fetchDefault(Account_move_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_move_lineDTO> list = new ArrayList<Account_move_lineDTO>();
        
        Page<Account_move_line> domains = account_move_lineService.searchDefault(context) ;
        for(Account_move_line account_move_line : domains.getContent()){
            Account_move_lineDTO dto = new Account_move_lineDTO();
            dto.fromDO(account_move_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
