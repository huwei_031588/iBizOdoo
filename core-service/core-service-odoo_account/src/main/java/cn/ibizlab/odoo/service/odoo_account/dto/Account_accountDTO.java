package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_account.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_accountDTO]
 */
public class Account_accountDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Account_accountIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [OPENING_DEBIT]
     *
     */
    @Account_accountOpening_debitDefault(info = "默认规则")
    private Double opening_debit;

    @JsonIgnore
    private boolean opening_debitDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Account_accountNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [OPENING_CREDIT]
     *
     */
    @Account_accountOpening_creditDefault(info = "默认规则")
    private Double opening_credit;

    @JsonIgnore
    private boolean opening_creditDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_accountCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [CODE]
     *
     */
    @Account_accountCodeDefault(info = "默认规则")
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;

    /**
     * 属性 [TAX_IDS]
     *
     */
    @Account_accountTax_idsDefault(info = "默认规则")
    private String tax_ids;

    @JsonIgnore
    private boolean tax_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_accountWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DEPRECATED]
     *
     */
    @Account_accountDeprecatedDefault(info = "默认规则")
    private String deprecated;

    @JsonIgnore
    private boolean deprecatedDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_accountDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [LAST_TIME_ENTRIES_CHECKED]
     *
     */
    @Account_accountLast_time_entries_checkedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp last_time_entries_checked;

    @JsonIgnore
    private boolean last_time_entries_checkedDirtyFlag;

    /**
     * 属性 [RECONCILE]
     *
     */
    @Account_accountReconcileDefault(info = "默认规则")
    private String reconcile;

    @JsonIgnore
    private boolean reconcileDirtyFlag;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @Account_accountTag_idsDefault(info = "默认规则")
    private String tag_ids;

    @JsonIgnore
    private boolean tag_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_accountNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_account__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [INTERNAL_TYPE]
     *
     */
    @Account_accountInternal_typeDefault(info = "默认规则")
    private String internal_type;

    @JsonIgnore
    private boolean internal_typeDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_accountCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_accountCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [INTERNAL_GROUP]
     *
     */
    @Account_accountInternal_groupDefault(info = "默认规则")
    private String internal_group;

    @JsonIgnore
    private boolean internal_groupDirtyFlag;

    /**
     * 属性 [GROUP_ID_TEXT]
     *
     */
    @Account_accountGroup_id_textDefault(info = "默认规则")
    private String group_id_text;

    @JsonIgnore
    private boolean group_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_accountWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_accountCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [USER_TYPE_ID_TEXT]
     *
     */
    @Account_accountUser_type_id_textDefault(info = "默认规则")
    private String user_type_id_text;

    @JsonIgnore
    private boolean user_type_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_accountCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @Account_accountGroup_idDefault(info = "默认规则")
    private Integer group_id;

    @JsonIgnore
    private boolean group_idDirtyFlag;

    /**
     * 属性 [USER_TYPE_ID]
     *
     */
    @Account_accountUser_type_idDefault(info = "默认规则")
    private Integer user_type_id;

    @JsonIgnore
    private boolean user_type_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_accountCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_accountWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_accountCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [OPENING_DEBIT]
     */
    @JsonProperty("opening_debit")
    public Double getOpening_debit(){
        return opening_debit ;
    }

    /**
     * 设置 [OPENING_DEBIT]
     */
    @JsonProperty("opening_debit")
    public void setOpening_debit(Double  opening_debit){
        this.opening_debit = opening_debit ;
        this.opening_debitDirtyFlag = true ;
    }

    /**
     * 获取 [OPENING_DEBIT]脏标记
     */
    @JsonIgnore
    public boolean getOpening_debitDirtyFlag(){
        return opening_debitDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [OPENING_CREDIT]
     */
    @JsonProperty("opening_credit")
    public Double getOpening_credit(){
        return opening_credit ;
    }

    /**
     * 设置 [OPENING_CREDIT]
     */
    @JsonProperty("opening_credit")
    public void setOpening_credit(Double  opening_credit){
        this.opening_credit = opening_credit ;
        this.opening_creditDirtyFlag = true ;
    }

    /**
     * 获取 [OPENING_CREDIT]脏标记
     */
    @JsonIgnore
    public boolean getOpening_creditDirtyFlag(){
        return opening_creditDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [CODE]
     */
    @JsonProperty("code")
    public String getCode(){
        return code ;
    }

    /**
     * 设置 [CODE]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [CODE]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return codeDirtyFlag ;
    }

    /**
     * 获取 [TAX_IDS]
     */
    @JsonProperty("tax_ids")
    public String getTax_ids(){
        return tax_ids ;
    }

    /**
     * 设置 [TAX_IDS]
     */
    @JsonProperty("tax_ids")
    public void setTax_ids(String  tax_ids){
        this.tax_ids = tax_ids ;
        this.tax_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTax_idsDirtyFlag(){
        return tax_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DEPRECATED]
     */
    @JsonProperty("deprecated")
    public String getDeprecated(){
        return deprecated ;
    }

    /**
     * 设置 [DEPRECATED]
     */
    @JsonProperty("deprecated")
    public void setDeprecated(String  deprecated){
        this.deprecated = deprecated ;
        this.deprecatedDirtyFlag = true ;
    }

    /**
     * 获取 [DEPRECATED]脏标记
     */
    @JsonIgnore
    public boolean getDeprecatedDirtyFlag(){
        return deprecatedDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [LAST_TIME_ENTRIES_CHECKED]
     */
    @JsonProperty("last_time_entries_checked")
    public Timestamp getLast_time_entries_checked(){
        return last_time_entries_checked ;
    }

    /**
     * 设置 [LAST_TIME_ENTRIES_CHECKED]
     */
    @JsonProperty("last_time_entries_checked")
    public void setLast_time_entries_checked(Timestamp  last_time_entries_checked){
        this.last_time_entries_checked = last_time_entries_checked ;
        this.last_time_entries_checkedDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_TIME_ENTRIES_CHECKED]脏标记
     */
    @JsonIgnore
    public boolean getLast_time_entries_checkedDirtyFlag(){
        return last_time_entries_checkedDirtyFlag ;
    }

    /**
     * 获取 [RECONCILE]
     */
    @JsonProperty("reconcile")
    public String getReconcile(){
        return reconcile ;
    }

    /**
     * 设置 [RECONCILE]
     */
    @JsonProperty("reconcile")
    public void setReconcile(String  reconcile){
        this.reconcile = reconcile ;
        this.reconcileDirtyFlag = true ;
    }

    /**
     * 获取 [RECONCILE]脏标记
     */
    @JsonIgnore
    public boolean getReconcileDirtyFlag(){
        return reconcileDirtyFlag ;
    }

    /**
     * 获取 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return tag_ids ;
    }

    /**
     * 设置 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return tag_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [INTERNAL_TYPE]
     */
    @JsonProperty("internal_type")
    public String getInternal_type(){
        return internal_type ;
    }

    /**
     * 设置 [INTERNAL_TYPE]
     */
    @JsonProperty("internal_type")
    public void setInternal_type(String  internal_type){
        this.internal_type = internal_type ;
        this.internal_typeDirtyFlag = true ;
    }

    /**
     * 获取 [INTERNAL_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getInternal_typeDirtyFlag(){
        return internal_typeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [INTERNAL_GROUP]
     */
    @JsonProperty("internal_group")
    public String getInternal_group(){
        return internal_group ;
    }

    /**
     * 设置 [INTERNAL_GROUP]
     */
    @JsonProperty("internal_group")
    public void setInternal_group(String  internal_group){
        this.internal_group = internal_group ;
        this.internal_groupDirtyFlag = true ;
    }

    /**
     * 获取 [INTERNAL_GROUP]脏标记
     */
    @JsonIgnore
    public boolean getInternal_groupDirtyFlag(){
        return internal_groupDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ID_TEXT]
     */
    @JsonProperty("group_id_text")
    public String getGroup_id_text(){
        return group_id_text ;
    }

    /**
     * 设置 [GROUP_ID_TEXT]
     */
    @JsonProperty("group_id_text")
    public void setGroup_id_text(String  group_id_text){
        this.group_id_text = group_id_text ;
        this.group_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getGroup_id_textDirtyFlag(){
        return group_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_TYPE_ID_TEXT]
     */
    @JsonProperty("user_type_id_text")
    public String getUser_type_id_text(){
        return user_type_id_text ;
    }

    /**
     * 设置 [USER_TYPE_ID_TEXT]
     */
    @JsonProperty("user_type_id_text")
    public void setUser_type_id_text(String  user_type_id_text){
        this.user_type_id_text = user_type_id_text ;
        this.user_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_type_id_textDirtyFlag(){
        return user_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return group_id ;
    }

    /**
     * 设置 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return group_idDirtyFlag ;
    }

    /**
     * 获取 [USER_TYPE_ID]
     */
    @JsonProperty("user_type_id")
    public Integer getUser_type_id(){
        return user_type_id ;
    }

    /**
     * 设置 [USER_TYPE_ID]
     */
    @JsonProperty("user_type_id")
    public void setUser_type_id(Integer  user_type_id){
        this.user_type_id = user_type_id ;
        this.user_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_type_idDirtyFlag(){
        return user_type_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Account_account toDO() {
        Account_account srfdomain = new Account_account();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getOpening_debitDirtyFlag())
            srfdomain.setOpening_debit(opening_debit);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getOpening_creditDirtyFlag())
            srfdomain.setOpening_credit(opening_credit);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getCodeDirtyFlag())
            srfdomain.setCode(code);
        if(getTax_idsDirtyFlag())
            srfdomain.setTax_ids(tax_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDeprecatedDirtyFlag())
            srfdomain.setDeprecated(deprecated);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getLast_time_entries_checkedDirtyFlag())
            srfdomain.setLast_time_entries_checked(last_time_entries_checked);
        if(getReconcileDirtyFlag())
            srfdomain.setReconcile(reconcile);
        if(getTag_idsDirtyFlag())
            srfdomain.setTag_ids(tag_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getInternal_typeDirtyFlag())
            srfdomain.setInternal_type(internal_type);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getInternal_groupDirtyFlag())
            srfdomain.setInternal_group(internal_group);
        if(getGroup_id_textDirtyFlag())
            srfdomain.setGroup_id_text(group_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getUser_type_id_textDirtyFlag())
            srfdomain.setUser_type_id_text(user_type_id_text);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getGroup_idDirtyFlag())
            srfdomain.setGroup_id(group_id);
        if(getUser_type_idDirtyFlag())
            srfdomain.setUser_type_id(user_type_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Account_account srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getOpening_debitDirtyFlag())
            this.setOpening_debit(srfdomain.getOpening_debit());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getOpening_creditDirtyFlag())
            this.setOpening_credit(srfdomain.getOpening_credit());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getCodeDirtyFlag())
            this.setCode(srfdomain.getCode());
        if(srfdomain.getTax_idsDirtyFlag())
            this.setTax_ids(srfdomain.getTax_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDeprecatedDirtyFlag())
            this.setDeprecated(srfdomain.getDeprecated());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getLast_time_entries_checkedDirtyFlag())
            this.setLast_time_entries_checked(srfdomain.getLast_time_entries_checked());
        if(srfdomain.getReconcileDirtyFlag())
            this.setReconcile(srfdomain.getReconcile());
        if(srfdomain.getTag_idsDirtyFlag())
            this.setTag_ids(srfdomain.getTag_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getInternal_typeDirtyFlag())
            this.setInternal_type(srfdomain.getInternal_type());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getInternal_groupDirtyFlag())
            this.setInternal_group(srfdomain.getInternal_group());
        if(srfdomain.getGroup_id_textDirtyFlag())
            this.setGroup_id_text(srfdomain.getGroup_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getUser_type_id_textDirtyFlag())
            this.setUser_type_id_text(srfdomain.getUser_type_id_text());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getGroup_idDirtyFlag())
            this.setGroup_id(srfdomain.getGroup_id());
        if(srfdomain.getUser_type_idDirtyFlag())
            this.setUser_type_id(srfdomain.getUser_type_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Account_accountDTO> fromDOPage(List<Account_account> poPage)   {
        if(poPage == null)
            return null;
        List<Account_accountDTO> dtos=new ArrayList<Account_accountDTO>();
        for(Account_account domain : poPage) {
            Account_accountDTO dto = new Account_accountDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

