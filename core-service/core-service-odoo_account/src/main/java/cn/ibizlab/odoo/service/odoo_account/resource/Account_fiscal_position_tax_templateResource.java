package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_fiscal_position_tax_templateDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax_template;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_position_tax_templateService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_tax_templateSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_fiscal_position_tax_template" })
@RestController
@RequestMapping("")
public class Account_fiscal_position_tax_templateResource {

    @Autowired
    private IAccount_fiscal_position_tax_templateService account_fiscal_position_tax_templateService;

    public IAccount_fiscal_position_tax_templateService getAccount_fiscal_position_tax_templateService() {
        return this.account_fiscal_position_tax_templateService;
    }

    @ApiOperation(value = "获取数据", tags = {"Account_fiscal_position_tax_template" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_position_tax_templates/{account_fiscal_position_tax_template_id}")
    public ResponseEntity<Account_fiscal_position_tax_templateDTO> get(@PathVariable("account_fiscal_position_tax_template_id") Integer account_fiscal_position_tax_template_id) {
        Account_fiscal_position_tax_templateDTO dto = new Account_fiscal_position_tax_templateDTO();
        Account_fiscal_position_tax_template domain = account_fiscal_position_tax_templateService.get(account_fiscal_position_tax_template_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_fiscal_position_tax_template" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_position_tax_templates/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_fiscal_position_tax_templateDTO> account_fiscal_position_tax_templatedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_fiscal_position_tax_template" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_position_tax_templates/{account_fiscal_position_tax_template_id}")

    public ResponseEntity<Account_fiscal_position_tax_templateDTO> update(@PathVariable("account_fiscal_position_tax_template_id") Integer account_fiscal_position_tax_template_id, @RequestBody Account_fiscal_position_tax_templateDTO account_fiscal_position_tax_templatedto) {
		Account_fiscal_position_tax_template domain = account_fiscal_position_tax_templatedto.toDO();
        domain.setId(account_fiscal_position_tax_template_id);
		account_fiscal_position_tax_templateService.update(domain);
		Account_fiscal_position_tax_templateDTO dto = new Account_fiscal_position_tax_templateDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_fiscal_position_tax_template" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_position_tax_templates/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_fiscal_position_tax_templateDTO> account_fiscal_position_tax_templatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_fiscal_position_tax_template" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_position_tax_templates/{account_fiscal_position_tax_template_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_fiscal_position_tax_template_id") Integer account_fiscal_position_tax_template_id) {
        Account_fiscal_position_tax_templateDTO account_fiscal_position_tax_templatedto = new Account_fiscal_position_tax_templateDTO();
		Account_fiscal_position_tax_template domain = new Account_fiscal_position_tax_template();
		account_fiscal_position_tax_templatedto.setId(account_fiscal_position_tax_template_id);
		domain.setId(account_fiscal_position_tax_template_id);
        Boolean rst = account_fiscal_position_tax_templateService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_fiscal_position_tax_template" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_position_tax_templates/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_fiscal_position_tax_template(@RequestBody List<Account_fiscal_position_tax_templateDTO> account_fiscal_position_tax_templatedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_fiscal_position_tax_template" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_position_tax_templates")

    public ResponseEntity<Account_fiscal_position_tax_templateDTO> create(@RequestBody Account_fiscal_position_tax_templateDTO account_fiscal_position_tax_templatedto) {
        Account_fiscal_position_tax_templateDTO dto = new Account_fiscal_position_tax_templateDTO();
        Account_fiscal_position_tax_template domain = account_fiscal_position_tax_templatedto.toDO();
		account_fiscal_position_tax_templateService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_fiscal_position_tax_template" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_fiscal_position_tax_templates/fetchdefault")
	public ResponseEntity<Page<Account_fiscal_position_tax_templateDTO>> fetchDefault(Account_fiscal_position_tax_templateSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_fiscal_position_tax_templateDTO> list = new ArrayList<Account_fiscal_position_tax_templateDTO>();
        
        Page<Account_fiscal_position_tax_template> domains = account_fiscal_position_tax_templateService.searchDefault(context) ;
        for(Account_fiscal_position_tax_template account_fiscal_position_tax_template : domains.getContent()){
            Account_fiscal_position_tax_templateDTO dto = new Account_fiscal_position_tax_templateDTO();
            dto.fromDO(account_fiscal_position_tax_template);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
