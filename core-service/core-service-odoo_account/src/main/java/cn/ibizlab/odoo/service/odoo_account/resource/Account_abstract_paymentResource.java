package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_abstract_paymentDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_abstract_paymentService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_abstract_paymentSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_abstract_payment" })
@RestController
@RequestMapping("")
public class Account_abstract_paymentResource {

    @Autowired
    private IAccount_abstract_paymentService account_abstract_paymentService;

    public IAccount_abstract_paymentService getAccount_abstract_paymentService() {
        return this.account_abstract_paymentService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_abstract_payment" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_abstract_payments/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_abstract_payment(@RequestBody List<Account_abstract_paymentDTO> account_abstract_paymentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_abstract_payment" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_abstract_payments/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_abstract_paymentDTO> account_abstract_paymentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_abstract_payment" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_abstract_payments/{account_abstract_payment_id}")

    public ResponseEntity<Account_abstract_paymentDTO> update(@PathVariable("account_abstract_payment_id") Integer account_abstract_payment_id, @RequestBody Account_abstract_paymentDTO account_abstract_paymentdto) {
		Account_abstract_payment domain = account_abstract_paymentdto.toDO();
        domain.setId(account_abstract_payment_id);
		account_abstract_paymentService.update(domain);
		Account_abstract_paymentDTO dto = new Account_abstract_paymentDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_abstract_payment" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_abstract_payments/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_abstract_paymentDTO> account_abstract_paymentdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_abstract_payment" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_abstract_payments/{account_abstract_payment_id}")
    public ResponseEntity<Account_abstract_paymentDTO> get(@PathVariable("account_abstract_payment_id") Integer account_abstract_payment_id) {
        Account_abstract_paymentDTO dto = new Account_abstract_paymentDTO();
        Account_abstract_payment domain = account_abstract_paymentService.get(account_abstract_payment_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_abstract_payment" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_abstract_payments/{account_abstract_payment_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_abstract_payment_id") Integer account_abstract_payment_id) {
        Account_abstract_paymentDTO account_abstract_paymentdto = new Account_abstract_paymentDTO();
		Account_abstract_payment domain = new Account_abstract_payment();
		account_abstract_paymentdto.setId(account_abstract_payment_id);
		domain.setId(account_abstract_payment_id);
        Boolean rst = account_abstract_paymentService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Account_abstract_payment" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_abstract_payments")

    public ResponseEntity<Account_abstract_paymentDTO> create(@RequestBody Account_abstract_paymentDTO account_abstract_paymentdto) {
        Account_abstract_paymentDTO dto = new Account_abstract_paymentDTO();
        Account_abstract_payment domain = account_abstract_paymentdto.toDO();
		account_abstract_paymentService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_abstract_payment" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_abstract_payments/fetchdefault")
	public ResponseEntity<Page<Account_abstract_paymentDTO>> fetchDefault(Account_abstract_paymentSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_abstract_paymentDTO> list = new ArrayList<Account_abstract_paymentDTO>();
        
        Page<Account_abstract_payment> domains = account_abstract_paymentService.searchDefault(context) ;
        for(Account_abstract_payment account_abstract_payment : domains.getContent()){
            Account_abstract_paymentDTO dto = new Account_abstract_paymentDTO();
            dto.fromDO(account_abstract_payment);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
