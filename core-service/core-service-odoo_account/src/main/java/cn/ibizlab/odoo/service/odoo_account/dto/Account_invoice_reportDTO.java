package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_invoice_report.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_report;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_invoice_reportDTO]
 */
public class Account_invoice_reportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NUMBER]
     *
     */
    @Account_invoice_reportNumberDefault(info = "默认规则")
    private String number;

    @JsonIgnore
    private boolean numberDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_invoice_report__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @Account_invoice_reportProduct_qtyDefault(info = "默认规则")
    private Double product_qty;

    @JsonIgnore
    private boolean product_qtyDirtyFlag;

    /**
     * 属性 [USER_CURRENCY_PRICE_TOTAL]
     *
     */
    @Account_invoice_reportUser_currency_price_totalDefault(info = "默认规则")
    private Double user_currency_price_total;

    @JsonIgnore
    private boolean user_currency_price_totalDirtyFlag;

    /**
     * 属性 [RESIDUAL]
     *
     */
    @Account_invoice_reportResidualDefault(info = "默认规则")
    private Double residual;

    @JsonIgnore
    private boolean residualDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Account_invoice_reportStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [DATE_DUE]
     *
     */
    @Account_invoice_reportDate_dueDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_due;

    @JsonIgnore
    private boolean date_dueDirtyFlag;

    /**
     * 属性 [NBR]
     *
     */
    @Account_invoice_reportNbrDefault(info = "默认规则")
    private Integer nbr;

    @JsonIgnore
    private boolean nbrDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_invoice_reportIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Account_invoice_reportDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [AMOUNT_TOTAL]
     *
     */
    @Account_invoice_reportAmount_totalDefault(info = "默认规则")
    private Double amount_total;

    @JsonIgnore
    private boolean amount_totalDirtyFlag;

    /**
     * 属性 [UOM_NAME]
     *
     */
    @Account_invoice_reportUom_nameDefault(info = "默认规则")
    private String uom_name;

    @JsonIgnore
    private boolean uom_nameDirtyFlag;

    /**
     * 属性 [PRICE_AVERAGE]
     *
     */
    @Account_invoice_reportPrice_averageDefault(info = "默认规则")
    private Double price_average;

    @JsonIgnore
    private boolean price_averageDirtyFlag;

    /**
     * 属性 [CURRENCY_RATE]
     *
     */
    @Account_invoice_reportCurrency_rateDefault(info = "默认规则")
    private Double currency_rate;

    @JsonIgnore
    private boolean currency_rateDirtyFlag;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @Account_invoice_reportPrice_totalDefault(info = "默认规则")
    private Double price_total;

    @JsonIgnore
    private boolean price_totalDirtyFlag;

    /**
     * 属性 [USER_CURRENCY_PRICE_AVERAGE]
     *
     */
    @Account_invoice_reportUser_currency_price_averageDefault(info = "默认规则")
    private Double user_currency_price_average;

    @JsonIgnore
    private boolean user_currency_price_averageDirtyFlag;

    /**
     * 属性 [USER_CURRENCY_RESIDUAL]
     *
     */
    @Account_invoice_reportUser_currency_residualDefault(info = "默认规则")
    private Double user_currency_residual;

    @JsonIgnore
    private boolean user_currency_residualDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Account_invoice_reportTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_invoice_reportDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @Account_invoice_reportTeam_id_textDefault(info = "默认规则")
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID_TEXT]
     *
     */
    @Account_invoice_reportAccount_analytic_id_textDefault(info = "默认规则")
    private String account_analytic_id_text;

    @JsonIgnore
    private boolean account_analytic_id_textDirtyFlag;

    /**
     * 属性 [CATEG_ID_TEXT]
     *
     */
    @Account_invoice_reportCateg_id_textDefault(info = "默认规则")
    private String categ_id_text;

    @JsonIgnore
    private boolean categ_id_textDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Account_invoice_reportJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [FISCAL_POSITION_ID_TEXT]
     *
     */
    @Account_invoice_reportFiscal_position_id_textDefault(info = "默认规则")
    private String fiscal_position_id_text;

    @JsonIgnore
    private boolean fiscal_position_id_textDirtyFlag;

    /**
     * 属性 [INVOICE_ID_TEXT]
     *
     */
    @Account_invoice_reportInvoice_id_textDefault(info = "默认规则")
    private String invoice_id_text;

    @JsonIgnore
    private boolean invoice_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @Account_invoice_reportCountry_id_textDefault(info = "默认规则")
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_invoice_reportCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_TERM_ID_TEXT]
     *
     */
    @Account_invoice_reportPayment_term_id_textDefault(info = "默认规则")
    private String payment_term_id_text;

    @JsonIgnore
    private boolean payment_term_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Account_invoice_reportPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_LINE_ID_TEXT]
     *
     */
    @Account_invoice_reportAccount_line_id_textDefault(info = "默认规则")
    private String account_line_id_text;

    @JsonIgnore
    private boolean account_line_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_invoice_reportCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID_TEXT]
     *
     */
    @Account_invoice_reportCommercial_partner_id_textDefault(info = "默认规则")
    private String commercial_partner_id_text;

    @JsonIgnore
    private boolean commercial_partner_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Account_invoice_reportProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Account_invoice_reportUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @Account_invoice_reportAccount_id_textDefault(info = "默认规则")
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Account_invoice_reportProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Account_invoice_reportUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Account_invoice_reportCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;

    /**
     * 属性 [PARTNER_BANK_ID]
     *
     */
    @Account_invoice_reportPartner_bank_idDefault(info = "默认规则")
    private Integer partner_bank_id;

    @JsonIgnore
    private boolean partner_bank_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_invoice_reportCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CATEG_ID]
     *
     */
    @Account_invoice_reportCateg_idDefault(info = "默认规则")
    private Integer categ_id;

    @JsonIgnore
    private boolean categ_idDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @Account_invoice_reportCommercial_partner_idDefault(info = "默认规则")
    private Integer commercial_partner_id;

    @JsonIgnore
    private boolean commercial_partner_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @Account_invoice_reportAccount_idDefault(info = "默认规则")
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Account_invoice_reportPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [PAYMENT_TERM_ID]
     *
     */
    @Account_invoice_reportPayment_term_idDefault(info = "默认规则")
    private Integer payment_term_id;

    @JsonIgnore
    private boolean payment_term_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID]
     *
     */
    @Account_invoice_reportAccount_analytic_idDefault(info = "默认规则")
    private Integer account_analytic_id;

    @JsonIgnore
    private boolean account_analytic_idDirtyFlag;

    /**
     * 属性 [FISCAL_POSITION_ID]
     *
     */
    @Account_invoice_reportFiscal_position_idDefault(info = "默认规则")
    private Integer fiscal_position_id;

    @JsonIgnore
    private boolean fiscal_position_idDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_invoice_reportJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_LINE_ID]
     *
     */
    @Account_invoice_reportAccount_line_idDefault(info = "默认规则")
    private Integer account_line_id;

    @JsonIgnore
    private boolean account_line_idDirtyFlag;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @Account_invoice_reportTeam_idDefault(info = "默认规则")
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_invoice_reportCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [INVOICE_ID]
     *
     */
    @Account_invoice_reportInvoice_idDefault(info = "默认规则")
    private Integer invoice_id;

    @JsonIgnore
    private boolean invoice_idDirtyFlag;


    /**
     * 获取 [NUMBER]
     */
    @JsonProperty("number")
    public String getNumber(){
        return number ;
    }

    /**
     * 设置 [NUMBER]
     */
    @JsonProperty("number")
    public void setNumber(String  number){
        this.number = number ;
        this.numberDirtyFlag = true ;
    }

    /**
     * 获取 [NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getNumberDirtyFlag(){
        return numberDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return product_qty ;
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return product_qtyDirtyFlag ;
    }

    /**
     * 获取 [USER_CURRENCY_PRICE_TOTAL]
     */
    @JsonProperty("user_currency_price_total")
    public Double getUser_currency_price_total(){
        return user_currency_price_total ;
    }

    /**
     * 设置 [USER_CURRENCY_PRICE_TOTAL]
     */
    @JsonProperty("user_currency_price_total")
    public void setUser_currency_price_total(Double  user_currency_price_total){
        this.user_currency_price_total = user_currency_price_total ;
        this.user_currency_price_totalDirtyFlag = true ;
    }

    /**
     * 获取 [USER_CURRENCY_PRICE_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getUser_currency_price_totalDirtyFlag(){
        return user_currency_price_totalDirtyFlag ;
    }

    /**
     * 获取 [RESIDUAL]
     */
    @JsonProperty("residual")
    public Double getResidual(){
        return residual ;
    }

    /**
     * 设置 [RESIDUAL]
     */
    @JsonProperty("residual")
    public void setResidual(Double  residual){
        this.residual = residual ;
        this.residualDirtyFlag = true ;
    }

    /**
     * 获取 [RESIDUAL]脏标记
     */
    @JsonIgnore
    public boolean getResidualDirtyFlag(){
        return residualDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [DATE_DUE]
     */
    @JsonProperty("date_due")
    public Timestamp getDate_due(){
        return date_due ;
    }

    /**
     * 设置 [DATE_DUE]
     */
    @JsonProperty("date_due")
    public void setDate_due(Timestamp  date_due){
        this.date_due = date_due ;
        this.date_dueDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_DUE]脏标记
     */
    @JsonIgnore
    public boolean getDate_dueDirtyFlag(){
        return date_dueDirtyFlag ;
    }

    /**
     * 获取 [NBR]
     */
    @JsonProperty("nbr")
    public Integer getNbr(){
        return nbr ;
    }

    /**
     * 设置 [NBR]
     */
    @JsonProperty("nbr")
    public void setNbr(Integer  nbr){
        this.nbr = nbr ;
        this.nbrDirtyFlag = true ;
    }

    /**
     * 获取 [NBR]脏标记
     */
    @JsonIgnore
    public boolean getNbrDirtyFlag(){
        return nbrDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TOTAL]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return amount_total ;
    }

    /**
     * 设置 [AMOUNT_TOTAL]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return amount_totalDirtyFlag ;
    }

    /**
     * 获取 [UOM_NAME]
     */
    @JsonProperty("uom_name")
    public String getUom_name(){
        return uom_name ;
    }

    /**
     * 设置 [UOM_NAME]
     */
    @JsonProperty("uom_name")
    public void setUom_name(String  uom_name){
        this.uom_name = uom_name ;
        this.uom_nameDirtyFlag = true ;
    }

    /**
     * 获取 [UOM_NAME]脏标记
     */
    @JsonIgnore
    public boolean getUom_nameDirtyFlag(){
        return uom_nameDirtyFlag ;
    }

    /**
     * 获取 [PRICE_AVERAGE]
     */
    @JsonProperty("price_average")
    public Double getPrice_average(){
        return price_average ;
    }

    /**
     * 设置 [PRICE_AVERAGE]
     */
    @JsonProperty("price_average")
    public void setPrice_average(Double  price_average){
        this.price_average = price_average ;
        this.price_averageDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_AVERAGE]脏标记
     */
    @JsonIgnore
    public boolean getPrice_averageDirtyFlag(){
        return price_averageDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_RATE]
     */
    @JsonProperty("currency_rate")
    public Double getCurrency_rate(){
        return currency_rate ;
    }

    /**
     * 设置 [CURRENCY_RATE]
     */
    @JsonProperty("currency_rate")
    public void setCurrency_rate(Double  currency_rate){
        this.currency_rate = currency_rate ;
        this.currency_rateDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_RATE]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_rateDirtyFlag(){
        return currency_rateDirtyFlag ;
    }

    /**
     * 获取 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return price_total ;
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return price_totalDirtyFlag ;
    }

    /**
     * 获取 [USER_CURRENCY_PRICE_AVERAGE]
     */
    @JsonProperty("user_currency_price_average")
    public Double getUser_currency_price_average(){
        return user_currency_price_average ;
    }

    /**
     * 设置 [USER_CURRENCY_PRICE_AVERAGE]
     */
    @JsonProperty("user_currency_price_average")
    public void setUser_currency_price_average(Double  user_currency_price_average){
        this.user_currency_price_average = user_currency_price_average ;
        this.user_currency_price_averageDirtyFlag = true ;
    }

    /**
     * 获取 [USER_CURRENCY_PRICE_AVERAGE]脏标记
     */
    @JsonIgnore
    public boolean getUser_currency_price_averageDirtyFlag(){
        return user_currency_price_averageDirtyFlag ;
    }

    /**
     * 获取 [USER_CURRENCY_RESIDUAL]
     */
    @JsonProperty("user_currency_residual")
    public Double getUser_currency_residual(){
        return user_currency_residual ;
    }

    /**
     * 设置 [USER_CURRENCY_RESIDUAL]
     */
    @JsonProperty("user_currency_residual")
    public void setUser_currency_residual(Double  user_currency_residual){
        this.user_currency_residual = user_currency_residual ;
        this.user_currency_residualDirtyFlag = true ;
    }

    /**
     * 获取 [USER_CURRENCY_RESIDUAL]脏标记
     */
    @JsonIgnore
    public boolean getUser_currency_residualDirtyFlag(){
        return user_currency_residualDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return team_id_text ;
    }

    /**
     * 设置 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return team_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID_TEXT]
     */
    @JsonProperty("account_analytic_id_text")
    public String getAccount_analytic_id_text(){
        return account_analytic_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID_TEXT]
     */
    @JsonProperty("account_analytic_id_text")
    public void setAccount_analytic_id_text(String  account_analytic_id_text){
        this.account_analytic_id_text = account_analytic_id_text ;
        this.account_analytic_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_id_textDirtyFlag(){
        return account_analytic_id_textDirtyFlag ;
    }

    /**
     * 获取 [CATEG_ID_TEXT]
     */
    @JsonProperty("categ_id_text")
    public String getCateg_id_text(){
        return categ_id_text ;
    }

    /**
     * 设置 [CATEG_ID_TEXT]
     */
    @JsonProperty("categ_id_text")
    public void setCateg_id_text(String  categ_id_text){
        this.categ_id_text = categ_id_text ;
        this.categ_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCateg_id_textDirtyFlag(){
        return categ_id_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID_TEXT]
     */
    @JsonProperty("fiscal_position_id_text")
    public String getFiscal_position_id_text(){
        return fiscal_position_id_text ;
    }

    /**
     * 设置 [FISCAL_POSITION_ID_TEXT]
     */
    @JsonProperty("fiscal_position_id_text")
    public void setFiscal_position_id_text(String  fiscal_position_id_text){
        this.fiscal_position_id_text = fiscal_position_id_text ;
        this.fiscal_position_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_id_textDirtyFlag(){
        return fiscal_position_id_textDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_ID_TEXT]
     */
    @JsonProperty("invoice_id_text")
    public String getInvoice_id_text(){
        return invoice_id_text ;
    }

    /**
     * 设置 [INVOICE_ID_TEXT]
     */
    @JsonProperty("invoice_id_text")
    public void setInvoice_id_text(String  invoice_id_text){
        this.invoice_id_text = invoice_id_text ;
        this.invoice_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_id_textDirtyFlag(){
        return invoice_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return country_id_text ;
    }

    /**
     * 设置 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return country_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID_TEXT]
     */
    @JsonProperty("payment_term_id_text")
    public String getPayment_term_id_text(){
        return payment_term_id_text ;
    }

    /**
     * 设置 [PAYMENT_TERM_ID_TEXT]
     */
    @JsonProperty("payment_term_id_text")
    public void setPayment_term_id_text(String  payment_term_id_text){
        this.payment_term_id_text = payment_term_id_text ;
        this.payment_term_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_id_textDirtyFlag(){
        return payment_term_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_LINE_ID_TEXT]
     */
    @JsonProperty("account_line_id_text")
    public String getAccount_line_id_text(){
        return account_line_id_text ;
    }

    /**
     * 设置 [ACCOUNT_LINE_ID_TEXT]
     */
    @JsonProperty("account_line_id_text")
    public void setAccount_line_id_text(String  account_line_id_text){
        this.account_line_id_text = account_line_id_text ;
        this.account_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_line_id_textDirtyFlag(){
        return account_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID_TEXT]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return commercial_partner_id_text ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID_TEXT]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return commercial_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return account_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return account_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_BANK_ID]
     */
    @JsonProperty("partner_bank_id")
    public Integer getPartner_bank_id(){
        return partner_bank_id ;
    }

    /**
     * 设置 [PARTNER_BANK_ID]
     */
    @JsonProperty("partner_bank_id")
    public void setPartner_bank_id(Integer  partner_bank_id){
        this.partner_bank_id = partner_bank_id ;
        this.partner_bank_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_BANK_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_idDirtyFlag(){
        return partner_bank_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CATEG_ID]
     */
    @JsonProperty("categ_id")
    public Integer getCateg_id(){
        return categ_id ;
    }

    /**
     * 设置 [CATEG_ID]
     */
    @JsonProperty("categ_id")
    public void setCateg_id(Integer  categ_id){
        this.categ_id = categ_id ;
        this.categ_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_ID]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idDirtyFlag(){
        return categ_idDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return commercial_partner_id ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return commercial_partner_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return account_id ;
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return account_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID]
     */
    @JsonProperty("payment_term_id")
    public Integer getPayment_term_id(){
        return payment_term_id ;
    }

    /**
     * 设置 [PAYMENT_TERM_ID]
     */
    @JsonProperty("payment_term_id")
    public void setPayment_term_id(Integer  payment_term_id){
        this.payment_term_id = payment_term_id ;
        this.payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_idDirtyFlag(){
        return payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID]
     */
    @JsonProperty("account_analytic_id")
    public Integer getAccount_analytic_id(){
        return account_analytic_id ;
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID]
     */
    @JsonProperty("account_analytic_id")
    public void setAccount_analytic_id(Integer  account_analytic_id){
        this.account_analytic_id = account_analytic_id ;
        this.account_analytic_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_idDirtyFlag(){
        return account_analytic_idDirtyFlag ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID]
     */
    @JsonProperty("fiscal_position_id")
    public Integer getFiscal_position_id(){
        return fiscal_position_id ;
    }

    /**
     * 设置 [FISCAL_POSITION_ID]
     */
    @JsonProperty("fiscal_position_id")
    public void setFiscal_position_id(Integer  fiscal_position_id){
        this.fiscal_position_id = fiscal_position_id ;
        this.fiscal_position_idDirtyFlag = true ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_idDirtyFlag(){
        return fiscal_position_idDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_LINE_ID]
     */
    @JsonProperty("account_line_id")
    public Integer getAccount_line_id(){
        return account_line_id ;
    }

    /**
     * 设置 [ACCOUNT_LINE_ID]
     */
    @JsonProperty("account_line_id")
    public void setAccount_line_id(Integer  account_line_id){
        this.account_line_id = account_line_id ;
        this.account_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_line_idDirtyFlag(){
        return account_line_idDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return team_id ;
    }

    /**
     * 设置 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return team_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_ID]
     */
    @JsonProperty("invoice_id")
    public Integer getInvoice_id(){
        return invoice_id ;
    }

    /**
     * 设置 [INVOICE_ID]
     */
    @JsonProperty("invoice_id")
    public void setInvoice_id(Integer  invoice_id){
        this.invoice_id = invoice_id ;
        this.invoice_idDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_ID]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idDirtyFlag(){
        return invoice_idDirtyFlag ;
    }



    public Account_invoice_report toDO() {
        Account_invoice_report srfdomain = new Account_invoice_report();
        if(getNumberDirtyFlag())
            srfdomain.setNumber(number);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getProduct_qtyDirtyFlag())
            srfdomain.setProduct_qty(product_qty);
        if(getUser_currency_price_totalDirtyFlag())
            srfdomain.setUser_currency_price_total(user_currency_price_total);
        if(getResidualDirtyFlag())
            srfdomain.setResidual(residual);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getDate_dueDirtyFlag())
            srfdomain.setDate_due(date_due);
        if(getNbrDirtyFlag())
            srfdomain.setNbr(nbr);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getAmount_totalDirtyFlag())
            srfdomain.setAmount_total(amount_total);
        if(getUom_nameDirtyFlag())
            srfdomain.setUom_name(uom_name);
        if(getPrice_averageDirtyFlag())
            srfdomain.setPrice_average(price_average);
        if(getCurrency_rateDirtyFlag())
            srfdomain.setCurrency_rate(currency_rate);
        if(getPrice_totalDirtyFlag())
            srfdomain.setPrice_total(price_total);
        if(getUser_currency_price_averageDirtyFlag())
            srfdomain.setUser_currency_price_average(user_currency_price_average);
        if(getUser_currency_residualDirtyFlag())
            srfdomain.setUser_currency_residual(user_currency_residual);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getTeam_id_textDirtyFlag())
            srfdomain.setTeam_id_text(team_id_text);
        if(getAccount_analytic_id_textDirtyFlag())
            srfdomain.setAccount_analytic_id_text(account_analytic_id_text);
        if(getCateg_id_textDirtyFlag())
            srfdomain.setCateg_id_text(categ_id_text);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getFiscal_position_id_textDirtyFlag())
            srfdomain.setFiscal_position_id_text(fiscal_position_id_text);
        if(getInvoice_id_textDirtyFlag())
            srfdomain.setInvoice_id_text(invoice_id_text);
        if(getCountry_id_textDirtyFlag())
            srfdomain.setCountry_id_text(country_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getPayment_term_id_textDirtyFlag())
            srfdomain.setPayment_term_id_text(payment_term_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getAccount_line_id_textDirtyFlag())
            srfdomain.setAccount_line_id_text(account_line_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCommercial_partner_id_textDirtyFlag())
            srfdomain.setCommercial_partner_id_text(commercial_partner_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getAccount_id_textDirtyFlag())
            srfdomain.setAccount_id_text(account_id_text);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);
        if(getPartner_bank_idDirtyFlag())
            srfdomain.setPartner_bank_id(partner_bank_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCateg_idDirtyFlag())
            srfdomain.setCateg_id(categ_id);
        if(getCommercial_partner_idDirtyFlag())
            srfdomain.setCommercial_partner_id(commercial_partner_id);
        if(getAccount_idDirtyFlag())
            srfdomain.setAccount_id(account_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getPayment_term_idDirtyFlag())
            srfdomain.setPayment_term_id(payment_term_id);
        if(getAccount_analytic_idDirtyFlag())
            srfdomain.setAccount_analytic_id(account_analytic_id);
        if(getFiscal_position_idDirtyFlag())
            srfdomain.setFiscal_position_id(fiscal_position_id);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getAccount_line_idDirtyFlag())
            srfdomain.setAccount_line_id(account_line_id);
        if(getTeam_idDirtyFlag())
            srfdomain.setTeam_id(team_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getInvoice_idDirtyFlag())
            srfdomain.setInvoice_id(invoice_id);

        return srfdomain;
    }

    public void fromDO(Account_invoice_report srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getNumberDirtyFlag())
            this.setNumber(srfdomain.getNumber());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getProduct_qtyDirtyFlag())
            this.setProduct_qty(srfdomain.getProduct_qty());
        if(srfdomain.getUser_currency_price_totalDirtyFlag())
            this.setUser_currency_price_total(srfdomain.getUser_currency_price_total());
        if(srfdomain.getResidualDirtyFlag())
            this.setResidual(srfdomain.getResidual());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getDate_dueDirtyFlag())
            this.setDate_due(srfdomain.getDate_due());
        if(srfdomain.getNbrDirtyFlag())
            this.setNbr(srfdomain.getNbr());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getAmount_totalDirtyFlag())
            this.setAmount_total(srfdomain.getAmount_total());
        if(srfdomain.getUom_nameDirtyFlag())
            this.setUom_name(srfdomain.getUom_name());
        if(srfdomain.getPrice_averageDirtyFlag())
            this.setPrice_average(srfdomain.getPrice_average());
        if(srfdomain.getCurrency_rateDirtyFlag())
            this.setCurrency_rate(srfdomain.getCurrency_rate());
        if(srfdomain.getPrice_totalDirtyFlag())
            this.setPrice_total(srfdomain.getPrice_total());
        if(srfdomain.getUser_currency_price_averageDirtyFlag())
            this.setUser_currency_price_average(srfdomain.getUser_currency_price_average());
        if(srfdomain.getUser_currency_residualDirtyFlag())
            this.setUser_currency_residual(srfdomain.getUser_currency_residual());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getTeam_id_textDirtyFlag())
            this.setTeam_id_text(srfdomain.getTeam_id_text());
        if(srfdomain.getAccount_analytic_id_textDirtyFlag())
            this.setAccount_analytic_id_text(srfdomain.getAccount_analytic_id_text());
        if(srfdomain.getCateg_id_textDirtyFlag())
            this.setCateg_id_text(srfdomain.getCateg_id_text());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getFiscal_position_id_textDirtyFlag())
            this.setFiscal_position_id_text(srfdomain.getFiscal_position_id_text());
        if(srfdomain.getInvoice_id_textDirtyFlag())
            this.setInvoice_id_text(srfdomain.getInvoice_id_text());
        if(srfdomain.getCountry_id_textDirtyFlag())
            this.setCountry_id_text(srfdomain.getCountry_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getPayment_term_id_textDirtyFlag())
            this.setPayment_term_id_text(srfdomain.getPayment_term_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getAccount_line_id_textDirtyFlag())
            this.setAccount_line_id_text(srfdomain.getAccount_line_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCommercial_partner_id_textDirtyFlag())
            this.setCommercial_partner_id_text(srfdomain.getCommercial_partner_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getAccount_id_textDirtyFlag())
            this.setAccount_id_text(srfdomain.getAccount_id_text());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());
        if(srfdomain.getPartner_bank_idDirtyFlag())
            this.setPartner_bank_id(srfdomain.getPartner_bank_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCateg_idDirtyFlag())
            this.setCateg_id(srfdomain.getCateg_id());
        if(srfdomain.getCommercial_partner_idDirtyFlag())
            this.setCommercial_partner_id(srfdomain.getCommercial_partner_id());
        if(srfdomain.getAccount_idDirtyFlag())
            this.setAccount_id(srfdomain.getAccount_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getPayment_term_idDirtyFlag())
            this.setPayment_term_id(srfdomain.getPayment_term_id());
        if(srfdomain.getAccount_analytic_idDirtyFlag())
            this.setAccount_analytic_id(srfdomain.getAccount_analytic_id());
        if(srfdomain.getFiscal_position_idDirtyFlag())
            this.setFiscal_position_id(srfdomain.getFiscal_position_id());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getAccount_line_idDirtyFlag())
            this.setAccount_line_id(srfdomain.getAccount_line_id());
        if(srfdomain.getTeam_idDirtyFlag())
            this.setTeam_id(srfdomain.getTeam_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getInvoice_idDirtyFlag())
            this.setInvoice_id(srfdomain.getInvoice_id());

    }

    public List<Account_invoice_reportDTO> fromDOPage(List<Account_invoice_report> poPage)   {
        if(poPage == null)
            return null;
        List<Account_invoice_reportDTO> dtos=new ArrayList<Account_invoice_reportDTO>();
        for(Account_invoice_report domain : poPage) {
            Account_invoice_reportDTO dto = new Account_invoice_reportDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

