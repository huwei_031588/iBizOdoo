package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_full_reconcileDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_full_reconcile;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_full_reconcileService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_full_reconcileSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_full_reconcile" })
@RestController
@RequestMapping("")
public class Account_full_reconcileResource {

    @Autowired
    private IAccount_full_reconcileService account_full_reconcileService;

    public IAccount_full_reconcileService getAccount_full_reconcileService() {
        return this.account_full_reconcileService;
    }

    @ApiOperation(value = "建立数据", tags = {"Account_full_reconcile" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_full_reconciles")

    public ResponseEntity<Account_full_reconcileDTO> create(@RequestBody Account_full_reconcileDTO account_full_reconciledto) {
        Account_full_reconcileDTO dto = new Account_full_reconcileDTO();
        Account_full_reconcile domain = account_full_reconciledto.toDO();
		account_full_reconcileService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_full_reconcile" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_full_reconciles/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_full_reconcileDTO> account_full_reconciledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_full_reconcile" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_full_reconciles/{account_full_reconcile_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_full_reconcile_id") Integer account_full_reconcile_id) {
        Account_full_reconcileDTO account_full_reconciledto = new Account_full_reconcileDTO();
		Account_full_reconcile domain = new Account_full_reconcile();
		account_full_reconciledto.setId(account_full_reconcile_id);
		domain.setId(account_full_reconcile_id);
        Boolean rst = account_full_reconcileService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_full_reconcile" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_full_reconciles/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_full_reconcile(@RequestBody List<Account_full_reconcileDTO> account_full_reconciledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_full_reconcile" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_full_reconciles/{account_full_reconcile_id}")
    public ResponseEntity<Account_full_reconcileDTO> get(@PathVariable("account_full_reconcile_id") Integer account_full_reconcile_id) {
        Account_full_reconcileDTO dto = new Account_full_reconcileDTO();
        Account_full_reconcile domain = account_full_reconcileService.get(account_full_reconcile_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_full_reconcile" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_full_reconciles/{account_full_reconcile_id}")

    public ResponseEntity<Account_full_reconcileDTO> update(@PathVariable("account_full_reconcile_id") Integer account_full_reconcile_id, @RequestBody Account_full_reconcileDTO account_full_reconciledto) {
		Account_full_reconcile domain = account_full_reconciledto.toDO();
        domain.setId(account_full_reconcile_id);
		account_full_reconcileService.update(domain);
		Account_full_reconcileDTO dto = new Account_full_reconcileDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_full_reconcile" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_full_reconciles/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_full_reconcileDTO> account_full_reconciledtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_full_reconcile" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_full_reconciles/fetchdefault")
	public ResponseEntity<Page<Account_full_reconcileDTO>> fetchDefault(Account_full_reconcileSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_full_reconcileDTO> list = new ArrayList<Account_full_reconcileDTO>();
        
        Page<Account_full_reconcile> domains = account_full_reconcileService.searchDefault(context) ;
        for(Account_full_reconcile account_full_reconcile : domains.getContent()){
            Account_full_reconcileDTO dto = new Account_full_reconcileDTO();
            dto.fromDO(account_full_reconcile);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
