package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_account_tagDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_tag;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_account_tagService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_tagSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_account_tag" })
@RestController
@RequestMapping("")
public class Account_account_tagResource {

    @Autowired
    private IAccount_account_tagService account_account_tagService;

    public IAccount_account_tagService getAccount_account_tagService() {
        return this.account_account_tagService;
    }

    @ApiOperation(value = "更新数据", tags = {"Account_account_tag" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_account_tags/{account_account_tag_id}")

    public ResponseEntity<Account_account_tagDTO> update(@PathVariable("account_account_tag_id") Integer account_account_tag_id, @RequestBody Account_account_tagDTO account_account_tagdto) {
		Account_account_tag domain = account_account_tagdto.toDO();
        domain.setId(account_account_tag_id);
		account_account_tagService.update(domain);
		Account_account_tagDTO dto = new Account_account_tagDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_account_tag" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_account_tags/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_account_tagDTO> account_account_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_account_tag" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_account_tags")

    public ResponseEntity<Account_account_tagDTO> create(@RequestBody Account_account_tagDTO account_account_tagdto) {
        Account_account_tagDTO dto = new Account_account_tagDTO();
        Account_account_tag domain = account_account_tagdto.toDO();
		account_account_tagService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_account_tag" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_account_tags/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_account_tagDTO> account_account_tagdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_account_tag" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_account_tags/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_account_tag(@RequestBody List<Account_account_tagDTO> account_account_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_account_tag" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_account_tags/{account_account_tag_id}")
    public ResponseEntity<Account_account_tagDTO> get(@PathVariable("account_account_tag_id") Integer account_account_tag_id) {
        Account_account_tagDTO dto = new Account_account_tagDTO();
        Account_account_tag domain = account_account_tagService.get(account_account_tag_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_account_tag" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_account_tags/{account_account_tag_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_account_tag_id") Integer account_account_tag_id) {
        Account_account_tagDTO account_account_tagdto = new Account_account_tagDTO();
		Account_account_tag domain = new Account_account_tag();
		account_account_tagdto.setId(account_account_tag_id);
		domain.setId(account_account_tag_id);
        Boolean rst = account_account_tagService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_account_tag" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_account_tags/fetchdefault")
	public ResponseEntity<Page<Account_account_tagDTO>> fetchDefault(Account_account_tagSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_account_tagDTO> list = new ArrayList<Account_account_tagDTO>();
        
        Page<Account_account_tag> domains = account_account_tagService.searchDefault(context) ;
        for(Account_account_tag account_account_tag : domains.getContent()){
            Account_account_tagDTO dto = new Account_account_tagDTO();
            dto.fromDO(account_account_tag);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
