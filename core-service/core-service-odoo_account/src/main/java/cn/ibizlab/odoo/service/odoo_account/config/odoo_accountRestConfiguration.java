package cn.ibizlab.odoo.service.odoo_account.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_account")
public class odoo_accountRestConfiguration {

}
