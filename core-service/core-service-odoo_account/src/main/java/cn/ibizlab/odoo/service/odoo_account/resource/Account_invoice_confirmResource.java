package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_invoice_confirmDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_confirm;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_confirmService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_confirmSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_invoice_confirm" })
@RestController
@RequestMapping("")
public class Account_invoice_confirmResource {

    @Autowired
    private IAccount_invoice_confirmService account_invoice_confirmService;

    public IAccount_invoice_confirmService getAccount_invoice_confirmService() {
        return this.account_invoice_confirmService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_invoice_confirm" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_confirms/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_confirmDTO> account_invoice_confirmdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_invoice_confirm" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_confirms")

    public ResponseEntity<Account_invoice_confirmDTO> create(@RequestBody Account_invoice_confirmDTO account_invoice_confirmdto) {
        Account_invoice_confirmDTO dto = new Account_invoice_confirmDTO();
        Account_invoice_confirm domain = account_invoice_confirmdto.toDO();
		account_invoice_confirmService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_invoice_confirm" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_confirms/{account_invoice_confirm_id}")
    public ResponseEntity<Account_invoice_confirmDTO> get(@PathVariable("account_invoice_confirm_id") Integer account_invoice_confirm_id) {
        Account_invoice_confirmDTO dto = new Account_invoice_confirmDTO();
        Account_invoice_confirm domain = account_invoice_confirmService.get(account_invoice_confirm_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_invoice_confirm" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_confirms/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_invoice_confirmDTO> account_invoice_confirmdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_invoice_confirm" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_confirms/{account_invoice_confirm_id}")

    public ResponseEntity<Account_invoice_confirmDTO> update(@PathVariable("account_invoice_confirm_id") Integer account_invoice_confirm_id, @RequestBody Account_invoice_confirmDTO account_invoice_confirmdto) {
		Account_invoice_confirm domain = account_invoice_confirmdto.toDO();
        domain.setId(account_invoice_confirm_id);
		account_invoice_confirmService.update(domain);
		Account_invoice_confirmDTO dto = new Account_invoice_confirmDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_invoice_confirm" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_confirms/{account_invoice_confirm_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_confirm_id") Integer account_invoice_confirm_id) {
        Account_invoice_confirmDTO account_invoice_confirmdto = new Account_invoice_confirmDTO();
		Account_invoice_confirm domain = new Account_invoice_confirm();
		account_invoice_confirmdto.setId(account_invoice_confirm_id);
		domain.setId(account_invoice_confirm_id);
        Boolean rst = account_invoice_confirmService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_invoice_confirm" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_confirms/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_invoice_confirm(@RequestBody List<Account_invoice_confirmDTO> account_invoice_confirmdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_invoice_confirm" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_invoice_confirms/fetchdefault")
	public ResponseEntity<Page<Account_invoice_confirmDTO>> fetchDefault(Account_invoice_confirmSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_invoice_confirmDTO> list = new ArrayList<Account_invoice_confirmDTO>();
        
        Page<Account_invoice_confirm> domains = account_invoice_confirmService.searchDefault(context) ;
        for(Account_invoice_confirm account_invoice_confirm : domains.getContent()){
            Account_invoice_confirmDTO dto = new Account_invoice_confirmDTO();
            dto.fromDO(account_invoice_confirm);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
