package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_bank_statement_importDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_importService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_importSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_bank_statement_import" })
@RestController
@RequestMapping("")
public class Account_bank_statement_importResource {

    @Autowired
    private IAccount_bank_statement_importService account_bank_statement_importService;

    public IAccount_bank_statement_importService getAccount_bank_statement_importService() {
        return this.account_bank_statement_importService;
    }

    @ApiOperation(value = "删除数据", tags = {"Account_bank_statement_import" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_imports/{account_bank_statement_import_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_import_id") Integer account_bank_statement_import_id) {
        Account_bank_statement_importDTO account_bank_statement_importdto = new Account_bank_statement_importDTO();
		Account_bank_statement_import domain = new Account_bank_statement_import();
		account_bank_statement_importdto.setId(account_bank_statement_import_id);
		domain.setId(account_bank_statement_import_id);
        Boolean rst = account_bank_statement_importService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Account_bank_statement_import" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_imports/{account_bank_statement_import_id}")
    public ResponseEntity<Account_bank_statement_importDTO> get(@PathVariable("account_bank_statement_import_id") Integer account_bank_statement_import_id) {
        Account_bank_statement_importDTO dto = new Account_bank_statement_importDTO();
        Account_bank_statement_import domain = account_bank_statement_importService.get(account_bank_statement_import_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_bank_statement_import" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_imports")

    public ResponseEntity<Account_bank_statement_importDTO> create(@RequestBody Account_bank_statement_importDTO account_bank_statement_importdto) {
        Account_bank_statement_importDTO dto = new Account_bank_statement_importDTO();
        Account_bank_statement_import domain = account_bank_statement_importdto.toDO();
		account_bank_statement_importService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_bank_statement_import" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_imports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_importDTO> account_bank_statement_importdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_bank_statement_import" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_imports/{account_bank_statement_import_id}")

    public ResponseEntity<Account_bank_statement_importDTO> update(@PathVariable("account_bank_statement_import_id") Integer account_bank_statement_import_id, @RequestBody Account_bank_statement_importDTO account_bank_statement_importdto) {
		Account_bank_statement_import domain = account_bank_statement_importdto.toDO();
        domain.setId(account_bank_statement_import_id);
		account_bank_statement_importService.update(domain);
		Account_bank_statement_importDTO dto = new Account_bank_statement_importDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_bank_statement_import" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_imports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_bank_statement_importDTO> account_bank_statement_importdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_bank_statement_import" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_imports/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_bank_statement_import(@RequestBody List<Account_bank_statement_importDTO> account_bank_statement_importdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_bank_statement_import" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_bank_statement_imports/fetchdefault")
	public ResponseEntity<Page<Account_bank_statement_importDTO>> fetchDefault(Account_bank_statement_importSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_bank_statement_importDTO> list = new ArrayList<Account_bank_statement_importDTO>();
        
        Page<Account_bank_statement_import> domains = account_bank_statement_importService.searchDefault(context) ;
        for(Account_bank_statement_import account_bank_statement_import : domains.getContent()){
            Account_bank_statement_importDTO dto = new Account_bank_statement_importDTO();
            dto.fromDO(account_bank_statement_import);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
