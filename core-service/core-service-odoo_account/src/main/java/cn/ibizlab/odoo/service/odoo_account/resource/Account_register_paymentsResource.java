package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_register_paymentsDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_register_paymentsService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_register_paymentsSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_register_payments" })
@RestController
@RequestMapping("")
public class Account_register_paymentsResource {

    @Autowired
    private IAccount_register_paymentsService account_register_paymentsService;

    public IAccount_register_paymentsService getAccount_register_paymentsService() {
        return this.account_register_paymentsService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_register_payments" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_register_payments/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_register_paymentsDTO> account_register_paymentsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_register_payments" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_register_payments/{account_register_payments_id}")
    public ResponseEntity<Account_register_paymentsDTO> get(@PathVariable("account_register_payments_id") Integer account_register_payments_id) {
        Account_register_paymentsDTO dto = new Account_register_paymentsDTO();
        Account_register_payments domain = account_register_paymentsService.get(account_register_payments_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_register_payments" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_register_payments/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_register_paymentsDTO> account_register_paymentsdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_register_payments" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_register_payments")

    public ResponseEntity<Account_register_paymentsDTO> create(@RequestBody Account_register_paymentsDTO account_register_paymentsdto) {
        Account_register_paymentsDTO dto = new Account_register_paymentsDTO();
        Account_register_payments domain = account_register_paymentsdto.toDO();
		account_register_paymentsService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_register_payments" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_register_payments/{account_register_payments_id}")

    public ResponseEntity<Account_register_paymentsDTO> update(@PathVariable("account_register_payments_id") Integer account_register_payments_id, @RequestBody Account_register_paymentsDTO account_register_paymentsdto) {
		Account_register_payments domain = account_register_paymentsdto.toDO();
        domain.setId(account_register_payments_id);
		account_register_paymentsService.update(domain);
		Account_register_paymentsDTO dto = new Account_register_paymentsDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_register_payments" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_register_payments/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_register_payments(@RequestBody List<Account_register_paymentsDTO> account_register_paymentsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_register_payments" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_register_payments/{account_register_payments_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_register_payments_id") Integer account_register_payments_id) {
        Account_register_paymentsDTO account_register_paymentsdto = new Account_register_paymentsDTO();
		Account_register_payments domain = new Account_register_payments();
		account_register_paymentsdto.setId(account_register_payments_id);
		domain.setId(account_register_payments_id);
        Boolean rst = account_register_paymentsService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_register_payments" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_register_payments/fetchdefault")
	public ResponseEntity<Page<Account_register_paymentsDTO>> fetchDefault(Account_register_paymentsSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_register_paymentsDTO> list = new ArrayList<Account_register_paymentsDTO>();
        
        Page<Account_register_payments> domains = account_register_paymentsService.searchDefault(context) ;
        for(Account_register_payments account_register_payments : domains.getContent()){
            Account_register_paymentsDTO dto = new Account_register_paymentsDTO();
            dto.fromDO(account_register_payments);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
