package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_invoice_import_wizardDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_import_wizard;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_import_wizardService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_import_wizardSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_invoice_import_wizard" })
@RestController
@RequestMapping("")
public class Account_invoice_import_wizardResource {

    @Autowired
    private IAccount_invoice_import_wizardService account_invoice_import_wizardService;

    public IAccount_invoice_import_wizardService getAccount_invoice_import_wizardService() {
        return this.account_invoice_import_wizardService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_invoice_import_wizard" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_import_wizards/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_invoice_import_wizardDTO> account_invoice_import_wizarddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_invoice_import_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_import_wizards/{account_invoice_import_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_import_wizard_id") Integer account_invoice_import_wizard_id) {
        Account_invoice_import_wizardDTO account_invoice_import_wizarddto = new Account_invoice_import_wizardDTO();
		Account_invoice_import_wizard domain = new Account_invoice_import_wizard();
		account_invoice_import_wizarddto.setId(account_invoice_import_wizard_id);
		domain.setId(account_invoice_import_wizard_id);
        Boolean rst = account_invoice_import_wizardService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Account_invoice_import_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_import_wizards/{account_invoice_import_wizard_id}")

    public ResponseEntity<Account_invoice_import_wizardDTO> update(@PathVariable("account_invoice_import_wizard_id") Integer account_invoice_import_wizard_id, @RequestBody Account_invoice_import_wizardDTO account_invoice_import_wizarddto) {
		Account_invoice_import_wizard domain = account_invoice_import_wizarddto.toDO();
        domain.setId(account_invoice_import_wizard_id);
		account_invoice_import_wizardService.update(domain);
		Account_invoice_import_wizardDTO dto = new Account_invoice_import_wizardDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_invoice_import_wizard" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_import_wizards/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_invoice_import_wizard(@RequestBody List<Account_invoice_import_wizardDTO> account_invoice_import_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_invoice_import_wizard" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_import_wizards/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_import_wizardDTO> account_invoice_import_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_invoice_import_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_import_wizards/{account_invoice_import_wizard_id}")
    public ResponseEntity<Account_invoice_import_wizardDTO> get(@PathVariable("account_invoice_import_wizard_id") Integer account_invoice_import_wizard_id) {
        Account_invoice_import_wizardDTO dto = new Account_invoice_import_wizardDTO();
        Account_invoice_import_wizard domain = account_invoice_import_wizardService.get(account_invoice_import_wizard_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_invoice_import_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_import_wizards")

    public ResponseEntity<Account_invoice_import_wizardDTO> create(@RequestBody Account_invoice_import_wizardDTO account_invoice_import_wizarddto) {
        Account_invoice_import_wizardDTO dto = new Account_invoice_import_wizardDTO();
        Account_invoice_import_wizard domain = account_invoice_import_wizarddto.toDO();
		account_invoice_import_wizardService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_invoice_import_wizard" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_invoice_import_wizards/fetchdefault")
	public ResponseEntity<Page<Account_invoice_import_wizardDTO>> fetchDefault(Account_invoice_import_wizardSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_invoice_import_wizardDTO> list = new ArrayList<Account_invoice_import_wizardDTO>();
        
        Page<Account_invoice_import_wizard> domains = account_invoice_import_wizardService.searchDefault(context) ;
        for(Account_invoice_import_wizard account_invoice_import_wizard : domains.getContent()){
            Account_invoice_import_wizardDTO dto = new Account_invoice_import_wizardDTO();
            dto.fromDO(account_invoice_import_wizard);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
