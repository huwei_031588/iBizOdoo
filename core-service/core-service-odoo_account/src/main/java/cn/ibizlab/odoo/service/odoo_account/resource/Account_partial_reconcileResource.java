package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_partial_reconcileDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_partial_reconcileService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_partial_reconcileSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_partial_reconcile" })
@RestController
@RequestMapping("")
public class Account_partial_reconcileResource {

    @Autowired
    private IAccount_partial_reconcileService account_partial_reconcileService;

    public IAccount_partial_reconcileService getAccount_partial_reconcileService() {
        return this.account_partial_reconcileService;
    }

    @ApiOperation(value = "建立数据", tags = {"Account_partial_reconcile" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_partial_reconciles")

    public ResponseEntity<Account_partial_reconcileDTO> create(@RequestBody Account_partial_reconcileDTO account_partial_reconciledto) {
        Account_partial_reconcileDTO dto = new Account_partial_reconcileDTO();
        Account_partial_reconcile domain = account_partial_reconciledto.toDO();
		account_partial_reconcileService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_partial_reconcile" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_partial_reconciles/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_partial_reconcileDTO> account_partial_reconciledtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_partial_reconcile" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_partial_reconciles/{account_partial_reconcile_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_partial_reconcile_id") Integer account_partial_reconcile_id) {
        Account_partial_reconcileDTO account_partial_reconciledto = new Account_partial_reconcileDTO();
		Account_partial_reconcile domain = new Account_partial_reconcile();
		account_partial_reconciledto.setId(account_partial_reconcile_id);
		domain.setId(account_partial_reconcile_id);
        Boolean rst = account_partial_reconcileService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Account_partial_reconcile" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_partial_reconciles/{account_partial_reconcile_id}")

    public ResponseEntity<Account_partial_reconcileDTO> update(@PathVariable("account_partial_reconcile_id") Integer account_partial_reconcile_id, @RequestBody Account_partial_reconcileDTO account_partial_reconciledto) {
		Account_partial_reconcile domain = account_partial_reconciledto.toDO();
        domain.setId(account_partial_reconcile_id);
		account_partial_reconcileService.update(domain);
		Account_partial_reconcileDTO dto = new Account_partial_reconcileDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_partial_reconcile" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_partial_reconciles/{account_partial_reconcile_id}")
    public ResponseEntity<Account_partial_reconcileDTO> get(@PathVariable("account_partial_reconcile_id") Integer account_partial_reconcile_id) {
        Account_partial_reconcileDTO dto = new Account_partial_reconcileDTO();
        Account_partial_reconcile domain = account_partial_reconcileService.get(account_partial_reconcile_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_partial_reconcile" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_partial_reconciles/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_partial_reconcile(@RequestBody List<Account_partial_reconcileDTO> account_partial_reconciledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_partial_reconcile" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_partial_reconciles/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_partial_reconcileDTO> account_partial_reconciledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_partial_reconcile" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_partial_reconciles/fetchdefault")
	public ResponseEntity<Page<Account_partial_reconcileDTO>> fetchDefault(Account_partial_reconcileSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_partial_reconcileDTO> list = new ArrayList<Account_partial_reconcileDTO>();
        
        Page<Account_partial_reconcile> domains = account_partial_reconcileService.searchDefault(context) ;
        for(Account_partial_reconcile account_partial_reconcile : domains.getContent()){
            Account_partial_reconcileDTO dto = new Account_partial_reconcileDTO();
            dto.fromDO(account_partial_reconcile);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
