package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_fiscal_position_template.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_template;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_fiscal_position_templateDTO]
 */
public class Account_fiscal_position_templateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @Account_fiscal_position_templateNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [STATE_IDS]
     *
     */
    @Account_fiscal_position_templateState_idsDefault(info = "默认规则")
    private String state_ids;

    @JsonIgnore
    private boolean state_idsDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Account_fiscal_position_templateNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Account_fiscal_position_templateSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [AUTO_APPLY]
     *
     */
    @Account_fiscal_position_templateAuto_applyDefault(info = "默认规则")
    private String auto_apply;

    @JsonIgnore
    private boolean auto_applyDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_fiscal_position_template__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_fiscal_position_templateWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ACCOUNT_IDS]
     *
     */
    @Account_fiscal_position_templateAccount_idsDefault(info = "默认规则")
    private String account_ids;

    @JsonIgnore
    private boolean account_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_fiscal_position_templateCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_fiscal_position_templateIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [ZIP_TO]
     *
     */
    @Account_fiscal_position_templateZip_toDefault(info = "默认规则")
    private Integer zip_to;

    @JsonIgnore
    private boolean zip_toDirtyFlag;

    /**
     * 属性 [ZIP_FROM]
     *
     */
    @Account_fiscal_position_templateZip_fromDefault(info = "默认规则")
    private Integer zip_from;

    @JsonIgnore
    private boolean zip_fromDirtyFlag;

    /**
     * 属性 [TAX_IDS]
     *
     */
    @Account_fiscal_position_templateTax_idsDefault(info = "默认规则")
    private String tax_ids;

    @JsonIgnore
    private boolean tax_idsDirtyFlag;

    /**
     * 属性 [VAT_REQUIRED]
     *
     */
    @Account_fiscal_position_templateVat_requiredDefault(info = "默认规则")
    private String vat_required;

    @JsonIgnore
    private boolean vat_requiredDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_fiscal_position_templateDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_fiscal_position_templateWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CHART_TEMPLATE_ID_TEXT]
     *
     */
    @Account_fiscal_position_templateChart_template_id_textDefault(info = "默认规则")
    private String chart_template_id_text;

    @JsonIgnore
    private boolean chart_template_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @Account_fiscal_position_templateCountry_id_textDefault(info = "默认规则")
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_fiscal_position_templateCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COUNTRY_GROUP_ID_TEXT]
     *
     */
    @Account_fiscal_position_templateCountry_group_id_textDefault(info = "默认规则")
    private String country_group_id_text;

    @JsonIgnore
    private boolean country_group_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_GROUP_ID]
     *
     */
    @Account_fiscal_position_templateCountry_group_idDefault(info = "默认规则")
    private Integer country_group_id;

    @JsonIgnore
    private boolean country_group_idDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Account_fiscal_position_templateCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_fiscal_position_templateCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_fiscal_position_templateWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CHART_TEMPLATE_ID]
     *
     */
    @Account_fiscal_position_templateChart_template_idDefault(info = "默认规则")
    private Integer chart_template_id;

    @JsonIgnore
    private boolean chart_template_idDirtyFlag;


    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [STATE_IDS]
     */
    @JsonProperty("state_ids")
    public String getState_ids(){
        return state_ids ;
    }

    /**
     * 设置 [STATE_IDS]
     */
    @JsonProperty("state_ids")
    public void setState_ids(String  state_ids){
        this.state_ids = state_ids ;
        this.state_idsDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getState_idsDirtyFlag(){
        return state_idsDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [AUTO_APPLY]
     */
    @JsonProperty("auto_apply")
    public String getAuto_apply(){
        return auto_apply ;
    }

    /**
     * 设置 [AUTO_APPLY]
     */
    @JsonProperty("auto_apply")
    public void setAuto_apply(String  auto_apply){
        this.auto_apply = auto_apply ;
        this.auto_applyDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_APPLY]脏标记
     */
    @JsonIgnore
    public boolean getAuto_applyDirtyFlag(){
        return auto_applyDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_IDS]
     */
    @JsonProperty("account_ids")
    public String getAccount_ids(){
        return account_ids ;
    }

    /**
     * 设置 [ACCOUNT_IDS]
     */
    @JsonProperty("account_ids")
    public void setAccount_ids(String  account_ids){
        this.account_ids = account_ids ;
        this.account_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idsDirtyFlag(){
        return account_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [ZIP_TO]
     */
    @JsonProperty("zip_to")
    public Integer getZip_to(){
        return zip_to ;
    }

    /**
     * 设置 [ZIP_TO]
     */
    @JsonProperty("zip_to")
    public void setZip_to(Integer  zip_to){
        this.zip_to = zip_to ;
        this.zip_toDirtyFlag = true ;
    }

    /**
     * 获取 [ZIP_TO]脏标记
     */
    @JsonIgnore
    public boolean getZip_toDirtyFlag(){
        return zip_toDirtyFlag ;
    }

    /**
     * 获取 [ZIP_FROM]
     */
    @JsonProperty("zip_from")
    public Integer getZip_from(){
        return zip_from ;
    }

    /**
     * 设置 [ZIP_FROM]
     */
    @JsonProperty("zip_from")
    public void setZip_from(Integer  zip_from){
        this.zip_from = zip_from ;
        this.zip_fromDirtyFlag = true ;
    }

    /**
     * 获取 [ZIP_FROM]脏标记
     */
    @JsonIgnore
    public boolean getZip_fromDirtyFlag(){
        return zip_fromDirtyFlag ;
    }

    /**
     * 获取 [TAX_IDS]
     */
    @JsonProperty("tax_ids")
    public String getTax_ids(){
        return tax_ids ;
    }

    /**
     * 设置 [TAX_IDS]
     */
    @JsonProperty("tax_ids")
    public void setTax_ids(String  tax_ids){
        this.tax_ids = tax_ids ;
        this.tax_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTax_idsDirtyFlag(){
        return tax_idsDirtyFlag ;
    }

    /**
     * 获取 [VAT_REQUIRED]
     */
    @JsonProperty("vat_required")
    public String getVat_required(){
        return vat_required ;
    }

    /**
     * 设置 [VAT_REQUIRED]
     */
    @JsonProperty("vat_required")
    public void setVat_required(String  vat_required){
        this.vat_required = vat_required ;
        this.vat_requiredDirtyFlag = true ;
    }

    /**
     * 获取 [VAT_REQUIRED]脏标记
     */
    @JsonIgnore
    public boolean getVat_requiredDirtyFlag(){
        return vat_requiredDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("chart_template_id_text")
    public String getChart_template_id_text(){
        return chart_template_id_text ;
    }

    /**
     * 设置 [CHART_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("chart_template_id_text")
    public void setChart_template_id_text(String  chart_template_id_text){
        this.chart_template_id_text = chart_template_id_text ;
        this.chart_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_id_textDirtyFlag(){
        return chart_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return country_id_text ;
    }

    /**
     * 设置 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return country_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_GROUP_ID_TEXT]
     */
    @JsonProperty("country_group_id_text")
    public String getCountry_group_id_text(){
        return country_group_id_text ;
    }

    /**
     * 设置 [COUNTRY_GROUP_ID_TEXT]
     */
    @JsonProperty("country_group_id_text")
    public void setCountry_group_id_text(String  country_group_id_text){
        this.country_group_id_text = country_group_id_text ;
        this.country_group_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_GROUP_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_group_id_textDirtyFlag(){
        return country_group_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_GROUP_ID]
     */
    @JsonProperty("country_group_id")
    public Integer getCountry_group_id(){
        return country_group_id ;
    }

    /**
     * 设置 [COUNTRY_GROUP_ID]
     */
    @JsonProperty("country_group_id")
    public void setCountry_group_id(Integer  country_group_id){
        this.country_group_id = country_group_id ;
        this.country_group_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_group_idDirtyFlag(){
        return country_group_idDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID]
     */
    @JsonProperty("chart_template_id")
    public Integer getChart_template_id(){
        return chart_template_id ;
    }

    /**
     * 设置 [CHART_TEMPLATE_ID]
     */
    @JsonProperty("chart_template_id")
    public void setChart_template_id(Integer  chart_template_id){
        this.chart_template_id = chart_template_id ;
        this.chart_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_idDirtyFlag(){
        return chart_template_idDirtyFlag ;
    }



    public Account_fiscal_position_template toDO() {
        Account_fiscal_position_template srfdomain = new Account_fiscal_position_template();
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getState_idsDirtyFlag())
            srfdomain.setState_ids(state_ids);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getAuto_applyDirtyFlag())
            srfdomain.setAuto_apply(auto_apply);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getAccount_idsDirtyFlag())
            srfdomain.setAccount_ids(account_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getZip_toDirtyFlag())
            srfdomain.setZip_to(zip_to);
        if(getZip_fromDirtyFlag())
            srfdomain.setZip_from(zip_from);
        if(getTax_idsDirtyFlag())
            srfdomain.setTax_ids(tax_ids);
        if(getVat_requiredDirtyFlag())
            srfdomain.setVat_required(vat_required);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getChart_template_id_textDirtyFlag())
            srfdomain.setChart_template_id_text(chart_template_id_text);
        if(getCountry_id_textDirtyFlag())
            srfdomain.setCountry_id_text(country_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCountry_group_id_textDirtyFlag())
            srfdomain.setCountry_group_id_text(country_group_id_text);
        if(getCountry_group_idDirtyFlag())
            srfdomain.setCountry_group_id(country_group_id);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getChart_template_idDirtyFlag())
            srfdomain.setChart_template_id(chart_template_id);

        return srfdomain;
    }

    public void fromDO(Account_fiscal_position_template srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getState_idsDirtyFlag())
            this.setState_ids(srfdomain.getState_ids());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getAuto_applyDirtyFlag())
            this.setAuto_apply(srfdomain.getAuto_apply());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getAccount_idsDirtyFlag())
            this.setAccount_ids(srfdomain.getAccount_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getZip_toDirtyFlag())
            this.setZip_to(srfdomain.getZip_to());
        if(srfdomain.getZip_fromDirtyFlag())
            this.setZip_from(srfdomain.getZip_from());
        if(srfdomain.getTax_idsDirtyFlag())
            this.setTax_ids(srfdomain.getTax_ids());
        if(srfdomain.getVat_requiredDirtyFlag())
            this.setVat_required(srfdomain.getVat_required());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getChart_template_id_textDirtyFlag())
            this.setChart_template_id_text(srfdomain.getChart_template_id_text());
        if(srfdomain.getCountry_id_textDirtyFlag())
            this.setCountry_id_text(srfdomain.getCountry_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCountry_group_id_textDirtyFlag())
            this.setCountry_group_id_text(srfdomain.getCountry_group_id_text());
        if(srfdomain.getCountry_group_idDirtyFlag())
            this.setCountry_group_id(srfdomain.getCountry_group_id());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getChart_template_idDirtyFlag())
            this.setChart_template_id(srfdomain.getChart_template_id());

    }

    public List<Account_fiscal_position_templateDTO> fromDOPage(List<Account_fiscal_position_template> poPage)   {
        if(poPage == null)
            return null;
        List<Account_fiscal_position_templateDTO> dtos=new ArrayList<Account_fiscal_position_templateDTO>();
        for(Account_fiscal_position_template domain : poPage) {
            Account_fiscal_position_templateDTO dto = new Account_fiscal_position_templateDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

