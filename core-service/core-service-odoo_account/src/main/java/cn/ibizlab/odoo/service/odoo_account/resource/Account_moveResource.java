package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_moveDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_moveService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_moveSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_move" })
@RestController
@RequestMapping("")
public class Account_moveResource {

    @Autowired
    private IAccount_moveService account_moveService;

    public IAccount_moveService getAccount_moveService() {
        return this.account_moveService;
    }

    @ApiOperation(value = "更新数据", tags = {"Account_move" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_moves/{account_move_id}")

    public ResponseEntity<Account_moveDTO> update(@PathVariable("account_move_id") Integer account_move_id, @RequestBody Account_moveDTO account_movedto) {
		Account_move domain = account_movedto.toDO();
        domain.setId(account_move_id);
		account_moveService.update(domain);
		Account_moveDTO dto = new Account_moveDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_move" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_moves/{account_move_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_move_id") Integer account_move_id) {
        Account_moveDTO account_movedto = new Account_moveDTO();
		Account_move domain = new Account_move();
		account_movedto.setId(account_move_id);
		domain.setId(account_move_id);
        Boolean rst = account_moveService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_move" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_moves/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_moveDTO> account_movedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_move" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_moves/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_moveDTO> account_movedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_move" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_moves/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_move(@RequestBody List<Account_moveDTO> account_movedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_move" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_moves/{account_move_id}")
    public ResponseEntity<Account_moveDTO> get(@PathVariable("account_move_id") Integer account_move_id) {
        Account_moveDTO dto = new Account_moveDTO();
        Account_move domain = account_moveService.get(account_move_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_move" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_moves")

    public ResponseEntity<Account_moveDTO> create(@RequestBody Account_moveDTO account_movedto) {
        Account_moveDTO dto = new Account_moveDTO();
        Account_move domain = account_movedto.toDO();
		account_moveService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_move" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_moves/fetchdefault")
	public ResponseEntity<Page<Account_moveDTO>> fetchDefault(Account_moveSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_moveDTO> list = new ArrayList<Account_moveDTO>();
        
        Page<Account_move> domains = account_moveService.searchDefault(context) ;
        for(Account_move account_move : domains.getContent()){
            Account_moveDTO dto = new Account_moveDTO();
            dto.fromDO(account_move);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
