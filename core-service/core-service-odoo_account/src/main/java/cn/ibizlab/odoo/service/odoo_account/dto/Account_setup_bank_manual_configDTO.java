package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_setup_bank_manual_config.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_setup_bank_manual_configDTO]
 */
public class Account_setup_bank_manual_configDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_setup_bank_manual_configJournal_idDefault(info = "默认规则")
    private String journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [LINKED_JOURNAL_ID]
     *
     */
    @Account_setup_bank_manual_configLinked_journal_idDefault(info = "默认规则")
    private Integer linked_journal_id;

    @JsonIgnore
    private boolean linked_journal_idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_setup_bank_manual_configDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_setup_bank_manual_configWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [NEW_JOURNAL_NAME]
     *
     */
    @Account_setup_bank_manual_configNew_journal_nameDefault(info = "默认规则")
    private String new_journal_name;

    @JsonIgnore
    private boolean new_journal_nameDirtyFlag;

    /**
     * 属性 [NEW_JOURNAL_CODE]
     *
     */
    @Account_setup_bank_manual_configNew_journal_codeDefault(info = "默认规则")
    private String new_journal_code;

    @JsonIgnore
    private boolean new_journal_codeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_setup_bank_manual_config__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_OR_LINK_OPTION]
     *
     */
    @Account_setup_bank_manual_configCreate_or_link_optionDefault(info = "默认规则")
    private String create_or_link_option;

    @JsonIgnore
    private boolean create_or_link_optionDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_setup_bank_manual_configCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [RELATED_ACC_TYPE]
     *
     */
    @Account_setup_bank_manual_configRelated_acc_typeDefault(info = "默认规则")
    private String related_acc_type;

    @JsonIgnore
    private boolean related_acc_typeDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_setup_bank_manual_configIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Account_setup_bank_manual_configPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [BANK_NAME]
     *
     */
    @Account_setup_bank_manual_configBank_nameDefault(info = "默认规则")
    private String bank_name;

    @JsonIgnore
    private boolean bank_nameDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_setup_bank_manual_configWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [BANK_ID]
     *
     */
    @Account_setup_bank_manual_configBank_idDefault(info = "默认规则")
    private Integer bank_id;

    @JsonIgnore
    private boolean bank_idDirtyFlag;

    /**
     * 属性 [QR_CODE_VALID]
     *
     */
    @Account_setup_bank_manual_configQr_code_validDefault(info = "默认规则")
    private String qr_code_valid;

    @JsonIgnore
    private boolean qr_code_validDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_setup_bank_manual_configCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [SANITIZED_ACC_NUMBER]
     *
     */
    @Account_setup_bank_manual_configSanitized_acc_numberDefault(info = "默认规则")
    private String sanitized_acc_number;

    @JsonIgnore
    private boolean sanitized_acc_numberDirtyFlag;

    /**
     * 属性 [ACC_TYPE]
     *
     */
    @Account_setup_bank_manual_configAcc_typeDefault(info = "默认规则")
    private String acc_type;

    @JsonIgnore
    private boolean acc_typeDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Account_setup_bank_manual_configSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_setup_bank_manual_configCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_setup_bank_manual_configCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ACC_NUMBER]
     *
     */
    @Account_setup_bank_manual_configAcc_numberDefault(info = "默认规则")
    private String acc_number;

    @JsonIgnore
    private boolean acc_numberDirtyFlag;

    /**
     * 属性 [BANK_BIC]
     *
     */
    @Account_setup_bank_manual_configBank_bicDefault(info = "默认规则")
    private String bank_bic;

    @JsonIgnore
    private boolean bank_bicDirtyFlag;

    /**
     * 属性 [ACC_HOLDER_NAME]
     *
     */
    @Account_setup_bank_manual_configAcc_holder_nameDefault(info = "默认规则")
    private String acc_holder_name;

    @JsonIgnore
    private boolean acc_holder_nameDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_setup_bank_manual_configWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_setup_bank_manual_configCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [RES_PARTNER_BANK_ID]
     *
     */
    @Account_setup_bank_manual_configRes_partner_bank_idDefault(info = "默认规则")
    private Integer res_partner_bank_id;

    @JsonIgnore
    private boolean res_partner_bank_idDirtyFlag;


    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public String getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(String  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [LINKED_JOURNAL_ID]
     */
    @JsonProperty("linked_journal_id")
    public Integer getLinked_journal_id(){
        return linked_journal_id ;
    }

    /**
     * 设置 [LINKED_JOURNAL_ID]
     */
    @JsonProperty("linked_journal_id")
    public void setLinked_journal_id(Integer  linked_journal_id){
        this.linked_journal_id = linked_journal_id ;
        this.linked_journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [LINKED_JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getLinked_journal_idDirtyFlag(){
        return linked_journal_idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [NEW_JOURNAL_NAME]
     */
    @JsonProperty("new_journal_name")
    public String getNew_journal_name(){
        return new_journal_name ;
    }

    /**
     * 设置 [NEW_JOURNAL_NAME]
     */
    @JsonProperty("new_journal_name")
    public void setNew_journal_name(String  new_journal_name){
        this.new_journal_name = new_journal_name ;
        this.new_journal_nameDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_JOURNAL_NAME]脏标记
     */
    @JsonIgnore
    public boolean getNew_journal_nameDirtyFlag(){
        return new_journal_nameDirtyFlag ;
    }

    /**
     * 获取 [NEW_JOURNAL_CODE]
     */
    @JsonProperty("new_journal_code")
    public String getNew_journal_code(){
        return new_journal_code ;
    }

    /**
     * 设置 [NEW_JOURNAL_CODE]
     */
    @JsonProperty("new_journal_code")
    public void setNew_journal_code(String  new_journal_code){
        this.new_journal_code = new_journal_code ;
        this.new_journal_codeDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_JOURNAL_CODE]脏标记
     */
    @JsonIgnore
    public boolean getNew_journal_codeDirtyFlag(){
        return new_journal_codeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_OR_LINK_OPTION]
     */
    @JsonProperty("create_or_link_option")
    public String getCreate_or_link_option(){
        return create_or_link_option ;
    }

    /**
     * 设置 [CREATE_OR_LINK_OPTION]
     */
    @JsonProperty("create_or_link_option")
    public void setCreate_or_link_option(String  create_or_link_option){
        this.create_or_link_option = create_or_link_option ;
        this.create_or_link_optionDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_OR_LINK_OPTION]脏标记
     */
    @JsonIgnore
    public boolean getCreate_or_link_optionDirtyFlag(){
        return create_or_link_optionDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [RELATED_ACC_TYPE]
     */
    @JsonProperty("related_acc_type")
    public String getRelated_acc_type(){
        return related_acc_type ;
    }

    /**
     * 设置 [RELATED_ACC_TYPE]
     */
    @JsonProperty("related_acc_type")
    public void setRelated_acc_type(String  related_acc_type){
        this.related_acc_type = related_acc_type ;
        this.related_acc_typeDirtyFlag = true ;
    }

    /**
     * 获取 [RELATED_ACC_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getRelated_acc_typeDirtyFlag(){
        return related_acc_typeDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [BANK_NAME]
     */
    @JsonProperty("bank_name")
    public String getBank_name(){
        return bank_name ;
    }

    /**
     * 设置 [BANK_NAME]
     */
    @JsonProperty("bank_name")
    public void setBank_name(String  bank_name){
        this.bank_name = bank_name ;
        this.bank_nameDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_NAME]脏标记
     */
    @JsonIgnore
    public boolean getBank_nameDirtyFlag(){
        return bank_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [BANK_ID]
     */
    @JsonProperty("bank_id")
    public Integer getBank_id(){
        return bank_id ;
    }

    /**
     * 设置 [BANK_ID]
     */
    @JsonProperty("bank_id")
    public void setBank_id(Integer  bank_id){
        this.bank_id = bank_id ;
        this.bank_idDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ID]脏标记
     */
    @JsonIgnore
    public boolean getBank_idDirtyFlag(){
        return bank_idDirtyFlag ;
    }

    /**
     * 获取 [QR_CODE_VALID]
     */
    @JsonProperty("qr_code_valid")
    public String getQr_code_valid(){
        return qr_code_valid ;
    }

    /**
     * 设置 [QR_CODE_VALID]
     */
    @JsonProperty("qr_code_valid")
    public void setQr_code_valid(String  qr_code_valid){
        this.qr_code_valid = qr_code_valid ;
        this.qr_code_validDirtyFlag = true ;
    }

    /**
     * 获取 [QR_CODE_VALID]脏标记
     */
    @JsonIgnore
    public boolean getQr_code_validDirtyFlag(){
        return qr_code_validDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [SANITIZED_ACC_NUMBER]
     */
    @JsonProperty("sanitized_acc_number")
    public String getSanitized_acc_number(){
        return sanitized_acc_number ;
    }

    /**
     * 设置 [SANITIZED_ACC_NUMBER]
     */
    @JsonProperty("sanitized_acc_number")
    public void setSanitized_acc_number(String  sanitized_acc_number){
        this.sanitized_acc_number = sanitized_acc_number ;
        this.sanitized_acc_numberDirtyFlag = true ;
    }

    /**
     * 获取 [SANITIZED_ACC_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getSanitized_acc_numberDirtyFlag(){
        return sanitized_acc_numberDirtyFlag ;
    }

    /**
     * 获取 [ACC_TYPE]
     */
    @JsonProperty("acc_type")
    public String getAcc_type(){
        return acc_type ;
    }

    /**
     * 设置 [ACC_TYPE]
     */
    @JsonProperty("acc_type")
    public void setAcc_type(String  acc_type){
        this.acc_type = acc_type ;
        this.acc_typeDirtyFlag = true ;
    }

    /**
     * 获取 [ACC_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getAcc_typeDirtyFlag(){
        return acc_typeDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ACC_NUMBER]
     */
    @JsonProperty("acc_number")
    public String getAcc_number(){
        return acc_number ;
    }

    /**
     * 设置 [ACC_NUMBER]
     */
    @JsonProperty("acc_number")
    public void setAcc_number(String  acc_number){
        this.acc_number = acc_number ;
        this.acc_numberDirtyFlag = true ;
    }

    /**
     * 获取 [ACC_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getAcc_numberDirtyFlag(){
        return acc_numberDirtyFlag ;
    }

    /**
     * 获取 [BANK_BIC]
     */
    @JsonProperty("bank_bic")
    public String getBank_bic(){
        return bank_bic ;
    }

    /**
     * 设置 [BANK_BIC]
     */
    @JsonProperty("bank_bic")
    public void setBank_bic(String  bank_bic){
        this.bank_bic = bank_bic ;
        this.bank_bicDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_BIC]脏标记
     */
    @JsonIgnore
    public boolean getBank_bicDirtyFlag(){
        return bank_bicDirtyFlag ;
    }

    /**
     * 获取 [ACC_HOLDER_NAME]
     */
    @JsonProperty("acc_holder_name")
    public String getAcc_holder_name(){
        return acc_holder_name ;
    }

    /**
     * 设置 [ACC_HOLDER_NAME]
     */
    @JsonProperty("acc_holder_name")
    public void setAcc_holder_name(String  acc_holder_name){
        this.acc_holder_name = acc_holder_name ;
        this.acc_holder_nameDirtyFlag = true ;
    }

    /**
     * 获取 [ACC_HOLDER_NAME]脏标记
     */
    @JsonIgnore
    public boolean getAcc_holder_nameDirtyFlag(){
        return acc_holder_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [RES_PARTNER_BANK_ID]
     */
    @JsonProperty("res_partner_bank_id")
    public Integer getRes_partner_bank_id(){
        return res_partner_bank_id ;
    }

    /**
     * 设置 [RES_PARTNER_BANK_ID]
     */
    @JsonProperty("res_partner_bank_id")
    public void setRes_partner_bank_id(Integer  res_partner_bank_id){
        this.res_partner_bank_id = res_partner_bank_id ;
        this.res_partner_bank_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_PARTNER_BANK_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_partner_bank_idDirtyFlag(){
        return res_partner_bank_idDirtyFlag ;
    }



    public Account_setup_bank_manual_config toDO() {
        Account_setup_bank_manual_config srfdomain = new Account_setup_bank_manual_config();
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getLinked_journal_idDirtyFlag())
            srfdomain.setLinked_journal_id(linked_journal_id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getNew_journal_nameDirtyFlag())
            srfdomain.setNew_journal_name(new_journal_name);
        if(getNew_journal_codeDirtyFlag())
            srfdomain.setNew_journal_code(new_journal_code);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_or_link_optionDirtyFlag())
            srfdomain.setCreate_or_link_option(create_or_link_option);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getRelated_acc_typeDirtyFlag())
            srfdomain.setRelated_acc_type(related_acc_type);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getBank_nameDirtyFlag())
            srfdomain.setBank_name(bank_name);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getBank_idDirtyFlag())
            srfdomain.setBank_id(bank_id);
        if(getQr_code_validDirtyFlag())
            srfdomain.setQr_code_valid(qr_code_valid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getSanitized_acc_numberDirtyFlag())
            srfdomain.setSanitized_acc_number(sanitized_acc_number);
        if(getAcc_typeDirtyFlag())
            srfdomain.setAcc_type(acc_type);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getAcc_numberDirtyFlag())
            srfdomain.setAcc_number(acc_number);
        if(getBank_bicDirtyFlag())
            srfdomain.setBank_bic(bank_bic);
        if(getAcc_holder_nameDirtyFlag())
            srfdomain.setAcc_holder_name(acc_holder_name);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getRes_partner_bank_idDirtyFlag())
            srfdomain.setRes_partner_bank_id(res_partner_bank_id);

        return srfdomain;
    }

    public void fromDO(Account_setup_bank_manual_config srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getLinked_journal_idDirtyFlag())
            this.setLinked_journal_id(srfdomain.getLinked_journal_id());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getNew_journal_nameDirtyFlag())
            this.setNew_journal_name(srfdomain.getNew_journal_name());
        if(srfdomain.getNew_journal_codeDirtyFlag())
            this.setNew_journal_code(srfdomain.getNew_journal_code());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_or_link_optionDirtyFlag())
            this.setCreate_or_link_option(srfdomain.getCreate_or_link_option());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getRelated_acc_typeDirtyFlag())
            this.setRelated_acc_type(srfdomain.getRelated_acc_type());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getBank_nameDirtyFlag())
            this.setBank_name(srfdomain.getBank_name());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getBank_idDirtyFlag())
            this.setBank_id(srfdomain.getBank_id());
        if(srfdomain.getQr_code_validDirtyFlag())
            this.setQr_code_valid(srfdomain.getQr_code_valid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getSanitized_acc_numberDirtyFlag())
            this.setSanitized_acc_number(srfdomain.getSanitized_acc_number());
        if(srfdomain.getAcc_typeDirtyFlag())
            this.setAcc_type(srfdomain.getAcc_type());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getAcc_numberDirtyFlag())
            this.setAcc_number(srfdomain.getAcc_number());
        if(srfdomain.getBank_bicDirtyFlag())
            this.setBank_bic(srfdomain.getBank_bic());
        if(srfdomain.getAcc_holder_nameDirtyFlag())
            this.setAcc_holder_name(srfdomain.getAcc_holder_name());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getRes_partner_bank_idDirtyFlag())
            this.setRes_partner_bank_id(srfdomain.getRes_partner_bank_id());

    }

    public List<Account_setup_bank_manual_configDTO> fromDOPage(List<Account_setup_bank_manual_config> poPage)   {
        if(poPage == null)
            return null;
        List<Account_setup_bank_manual_configDTO> dtos=new ArrayList<Account_setup_bank_manual_configDTO>();
        for(Account_setup_bank_manual_config domain : poPage) {
            Account_setup_bank_manual_configDTO dto = new Account_setup_bank_manual_configDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

