package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_reconciliation_widgetDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconciliation_widgetService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_reconciliation_widget" })
@RestController
@RequestMapping("")
public class Account_reconciliation_widgetResource {

    @Autowired
    private IAccount_reconciliation_widgetService account_reconciliation_widgetService;

    public IAccount_reconciliation_widgetService getAccount_reconciliation_widgetService() {
        return this.account_reconciliation_widgetService;
    }

    @ApiOperation(value = "删除数据", tags = {"Account_reconciliation_widget" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconciliation_widgets/{account_reconciliation_widget_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_reconciliation_widget_id") Integer account_reconciliation_widget_id) {
        Account_reconciliation_widgetDTO account_reconciliation_widgetdto = new Account_reconciliation_widgetDTO();
		Account_reconciliation_widget domain = new Account_reconciliation_widget();
		account_reconciliation_widgetdto.setId(account_reconciliation_widget_id);
		domain.setId(account_reconciliation_widget_id);
        Boolean rst = account_reconciliation_widgetService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_reconciliation_widget" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconciliation_widgets/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_reconciliation_widget(@RequestBody List<Account_reconciliation_widgetDTO> account_reconciliation_widgetdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_reconciliation_widget" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconciliation_widgets")

    public ResponseEntity<Account_reconciliation_widgetDTO> create(@RequestBody Account_reconciliation_widgetDTO account_reconciliation_widgetdto) {
        Account_reconciliation_widgetDTO dto = new Account_reconciliation_widgetDTO();
        Account_reconciliation_widget domain = account_reconciliation_widgetdto.toDO();
		account_reconciliation_widgetService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_reconciliation_widget" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconciliation_widgets/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_reconciliation_widgetDTO> account_reconciliation_widgetdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_reconciliation_widget" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconciliation_widgets/{account_reconciliation_widget_id}")

    public ResponseEntity<Account_reconciliation_widgetDTO> update(@PathVariable("account_reconciliation_widget_id") Integer account_reconciliation_widget_id, @RequestBody Account_reconciliation_widgetDTO account_reconciliation_widgetdto) {
		Account_reconciliation_widget domain = account_reconciliation_widgetdto.toDO();
        domain.setId(account_reconciliation_widget_id);
		account_reconciliation_widgetService.update(domain);
		Account_reconciliation_widgetDTO dto = new Account_reconciliation_widgetDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_reconciliation_widget" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconciliation_widgets/{account_reconciliation_widget_id}")
    public ResponseEntity<Account_reconciliation_widgetDTO> get(@PathVariable("account_reconciliation_widget_id") Integer account_reconciliation_widget_id) {
        Account_reconciliation_widgetDTO dto = new Account_reconciliation_widgetDTO();
        Account_reconciliation_widget domain = account_reconciliation_widgetService.get(account_reconciliation_widget_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_reconciliation_widget" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconciliation_widgets/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_reconciliation_widgetDTO> account_reconciliation_widgetdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_reconciliation_widget" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_reconciliation_widgets/fetchdefault")
	public ResponseEntity<Page<Account_reconciliation_widgetDTO>> fetchDefault(Account_reconciliation_widgetSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_reconciliation_widgetDTO> list = new ArrayList<Account_reconciliation_widgetDTO>();
        
        Page<Account_reconciliation_widget> domains = account_reconciliation_widgetService.searchDefault(context) ;
        for(Account_reconciliation_widget account_reconciliation_widget : domains.getContent()){
            Account_reconciliation_widgetDTO dto = new Account_reconciliation_widgetDTO();
            dto.fromDO(account_reconciliation_widget);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
