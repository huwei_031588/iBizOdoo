package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_journalDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_journal;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_journalService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_journalSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_journal" })
@RestController
@RequestMapping("")
public class Account_journalResource {

    @Autowired
    private IAccount_journalService account_journalService;

    public IAccount_journalService getAccount_journalService() {
        return this.account_journalService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_journal" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_journals/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_journal(@RequestBody List<Account_journalDTO> account_journaldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_journal" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_journals/{account_journal_id}")
    public ResponseEntity<Account_journalDTO> get(@PathVariable("account_journal_id") Integer account_journal_id) {
        Account_journalDTO dto = new Account_journalDTO();
        Account_journal domain = account_journalService.get(account_journal_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_journal" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_journals")

    public ResponseEntity<Account_journalDTO> create(@RequestBody Account_journalDTO account_journaldto) {
        Account_journalDTO dto = new Account_journalDTO();
        Account_journal domain = account_journaldto.toDO();
		account_journalService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_journal" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_journals/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_journalDTO> account_journaldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_journal" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_journals/{account_journal_id}")

    public ResponseEntity<Account_journalDTO> update(@PathVariable("account_journal_id") Integer account_journal_id, @RequestBody Account_journalDTO account_journaldto) {
		Account_journal domain = account_journaldto.toDO();
        domain.setId(account_journal_id);
		account_journalService.update(domain);
		Account_journalDTO dto = new Account_journalDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_journal" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_journals/{account_journal_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_journal_id") Integer account_journal_id) {
        Account_journalDTO account_journaldto = new Account_journalDTO();
		Account_journal domain = new Account_journal();
		account_journaldto.setId(account_journal_id);
		domain.setId(account_journal_id);
        Boolean rst = account_journalService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_journal" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_journals/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_journalDTO> account_journaldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_journal" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_journals/fetchdefault")
	public ResponseEntity<Page<Account_journalDTO>> fetchDefault(Account_journalSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_journalDTO> list = new ArrayList<Account_journalDTO>();
        
        Page<Account_journal> domains = account_journalService.searchDefault(context) ;
        for(Account_journal account_journal : domains.getContent()){
            Account_journalDTO dto = new Account_journalDTO();
            dto.fromDO(account_journal);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
