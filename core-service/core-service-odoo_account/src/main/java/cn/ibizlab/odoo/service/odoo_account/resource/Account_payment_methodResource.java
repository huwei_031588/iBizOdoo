package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_payment_methodDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_methodService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_methodSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_payment_method" })
@RestController
@RequestMapping("")
public class Account_payment_methodResource {

    @Autowired
    private IAccount_payment_methodService account_payment_methodService;

    public IAccount_payment_methodService getAccount_payment_methodService() {
        return this.account_payment_methodService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_payment_method" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payment_methods/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_payment_methodDTO> account_payment_methoddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_payment_method" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payment_methods/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_payment_method(@RequestBody List<Account_payment_methodDTO> account_payment_methoddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_payment_method" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payment_methods/{account_payment_method_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_method_id") Integer account_payment_method_id) {
        Account_payment_methodDTO account_payment_methoddto = new Account_payment_methodDTO();
		Account_payment_method domain = new Account_payment_method();
		account_payment_methoddto.setId(account_payment_method_id);
		domain.setId(account_payment_method_id);
        Boolean rst = account_payment_methodService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_payment_method" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payment_methods/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_payment_methodDTO> account_payment_methoddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_payment_method" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payment_methods/{account_payment_method_id}")

    public ResponseEntity<Account_payment_methodDTO> update(@PathVariable("account_payment_method_id") Integer account_payment_method_id, @RequestBody Account_payment_methodDTO account_payment_methoddto) {
		Account_payment_method domain = account_payment_methoddto.toDO();
        domain.setId(account_payment_method_id);
		account_payment_methodService.update(domain);
		Account_payment_methodDTO dto = new Account_payment_methodDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_payment_method" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payment_methods")

    public ResponseEntity<Account_payment_methodDTO> create(@RequestBody Account_payment_methodDTO account_payment_methoddto) {
        Account_payment_methodDTO dto = new Account_payment_methodDTO();
        Account_payment_method domain = account_payment_methoddto.toDO();
		account_payment_methodService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_payment_method" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_methods/{account_payment_method_id}")
    public ResponseEntity<Account_payment_methodDTO> get(@PathVariable("account_payment_method_id") Integer account_payment_method_id) {
        Account_payment_methodDTO dto = new Account_payment_methodDTO();
        Account_payment_method domain = account_payment_methodService.get(account_payment_method_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_payment_method" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_payment_methods/fetchdefault")
	public ResponseEntity<Page<Account_payment_methodDTO>> fetchDefault(Account_payment_methodSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_payment_methodDTO> list = new ArrayList<Account_payment_methodDTO>();
        
        Page<Account_payment_method> domains = account_payment_methodService.searchDefault(context) ;
        for(Account_payment_method account_payment_method : domains.getContent()){
            Account_payment_methodDTO dto = new Account_payment_methodDTO();
            dto.fromDO(account_payment_method);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
