package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_cash_roundingDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_cash_roundingService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cash_roundingSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_cash_rounding" })
@RestController
@RequestMapping("")
public class Account_cash_roundingResource {

    @Autowired
    private IAccount_cash_roundingService account_cash_roundingService;

    public IAccount_cash_roundingService getAccount_cash_roundingService() {
        return this.account_cash_roundingService;
    }

    @ApiOperation(value = "建立数据", tags = {"Account_cash_rounding" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_cash_roundings")

    public ResponseEntity<Account_cash_roundingDTO> create(@RequestBody Account_cash_roundingDTO account_cash_roundingdto) {
        Account_cash_roundingDTO dto = new Account_cash_roundingDTO();
        Account_cash_rounding domain = account_cash_roundingdto.toDO();
		account_cash_roundingService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_cash_rounding" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_cash_roundings/{account_cash_rounding_id}")
    public ResponseEntity<Account_cash_roundingDTO> get(@PathVariable("account_cash_rounding_id") Integer account_cash_rounding_id) {
        Account_cash_roundingDTO dto = new Account_cash_roundingDTO();
        Account_cash_rounding domain = account_cash_roundingService.get(account_cash_rounding_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_cash_rounding" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_cash_roundings/{account_cash_rounding_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_cash_rounding_id") Integer account_cash_rounding_id) {
        Account_cash_roundingDTO account_cash_roundingdto = new Account_cash_roundingDTO();
		Account_cash_rounding domain = new Account_cash_rounding();
		account_cash_roundingdto.setId(account_cash_rounding_id);
		domain.setId(account_cash_rounding_id);
        Boolean rst = account_cash_roundingService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_cash_rounding" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_cash_roundings/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_cash_roundingDTO> account_cash_roundingdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_cash_rounding" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_cash_roundings/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_cash_rounding(@RequestBody List<Account_cash_roundingDTO> account_cash_roundingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_cash_rounding" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_cash_roundings/{account_cash_rounding_id}")

    public ResponseEntity<Account_cash_roundingDTO> update(@PathVariable("account_cash_rounding_id") Integer account_cash_rounding_id, @RequestBody Account_cash_roundingDTO account_cash_roundingdto) {
		Account_cash_rounding domain = account_cash_roundingdto.toDO();
        domain.setId(account_cash_rounding_id);
		account_cash_roundingService.update(domain);
		Account_cash_roundingDTO dto = new Account_cash_roundingDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_cash_rounding" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_cash_roundings/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_cash_roundingDTO> account_cash_roundingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_cash_rounding" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_cash_roundings/fetchdefault")
	public ResponseEntity<Page<Account_cash_roundingDTO>> fetchDefault(Account_cash_roundingSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_cash_roundingDTO> list = new ArrayList<Account_cash_roundingDTO>();
        
        Page<Account_cash_rounding> domains = account_cash_roundingService.searchDefault(context) ;
        for(Account_cash_rounding account_cash_rounding : domains.getContent()){
            Account_cash_roundingDTO dto = new Account_cash_roundingDTO();
            dto.fromDO(account_cash_rounding);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
