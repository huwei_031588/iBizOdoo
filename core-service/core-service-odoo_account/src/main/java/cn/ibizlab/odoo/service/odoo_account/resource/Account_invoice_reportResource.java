package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_invoice_reportDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_reportService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_reportSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_invoice_report" })
@RestController
@RequestMapping("")
public class Account_invoice_reportResource {

    @Autowired
    private IAccount_invoice_reportService account_invoice_reportService;

    public IAccount_invoice_reportService getAccount_invoice_reportService() {
        return this.account_invoice_reportService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_invoice_report" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_reports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_reportDTO> account_invoice_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_invoice_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_reports/{account_invoice_report_id}")
    public ResponseEntity<Account_invoice_reportDTO> get(@PathVariable("account_invoice_report_id") Integer account_invoice_report_id) {
        Account_invoice_reportDTO dto = new Account_invoice_reportDTO();
        Account_invoice_report domain = account_invoice_reportService.get(account_invoice_report_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_invoice_report" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_reports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_invoice_reportDTO> account_invoice_reportdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_invoice_report" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_reports/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_invoice_report(@RequestBody List<Account_invoice_reportDTO> account_invoice_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_invoice_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_reports")

    public ResponseEntity<Account_invoice_reportDTO> create(@RequestBody Account_invoice_reportDTO account_invoice_reportdto) {
        Account_invoice_reportDTO dto = new Account_invoice_reportDTO();
        Account_invoice_report domain = account_invoice_reportdto.toDO();
		account_invoice_reportService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_invoice_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_reports/{account_invoice_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_report_id") Integer account_invoice_report_id) {
        Account_invoice_reportDTO account_invoice_reportdto = new Account_invoice_reportDTO();
		Account_invoice_report domain = new Account_invoice_report();
		account_invoice_reportdto.setId(account_invoice_report_id);
		domain.setId(account_invoice_report_id);
        Boolean rst = account_invoice_reportService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Account_invoice_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_reports/{account_invoice_report_id}")

    public ResponseEntity<Account_invoice_reportDTO> update(@PathVariable("account_invoice_report_id") Integer account_invoice_report_id, @RequestBody Account_invoice_reportDTO account_invoice_reportdto) {
		Account_invoice_report domain = account_invoice_reportdto.toDO();
        domain.setId(account_invoice_report_id);
		account_invoice_reportService.update(domain);
		Account_invoice_reportDTO dto = new Account_invoice_reportDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_invoice_report" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_invoice_reports/fetchdefault")
	public ResponseEntity<Page<Account_invoice_reportDTO>> fetchDefault(Account_invoice_reportSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_invoice_reportDTO> list = new ArrayList<Account_invoice_reportDTO>();
        
        Page<Account_invoice_report> domains = account_invoice_reportService.searchDefault(context) ;
        for(Account_invoice_report account_invoice_report : domains.getContent()){
            Account_invoice_reportDTO dto = new Account_invoice_reportDTO();
            dto.fromDO(account_invoice_report);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
