package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_tax_groupDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_tax_groupService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_groupSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_tax_group" })
@RestController
@RequestMapping("")
public class Account_tax_groupResource {

    @Autowired
    private IAccount_tax_groupService account_tax_groupService;

    public IAccount_tax_groupService getAccount_tax_groupService() {
        return this.account_tax_groupService;
    }

    @ApiOperation(value = "更新数据", tags = {"Account_tax_group" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_tax_groups/{account_tax_group_id}")

    public ResponseEntity<Account_tax_groupDTO> update(@PathVariable("account_tax_group_id") Integer account_tax_group_id, @RequestBody Account_tax_groupDTO account_tax_groupdto) {
		Account_tax_group domain = account_tax_groupdto.toDO();
        domain.setId(account_tax_group_id);
		account_tax_groupService.update(domain);
		Account_tax_groupDTO dto = new Account_tax_groupDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_tax_group" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_tax_groups/{account_tax_group_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_tax_group_id") Integer account_tax_group_id) {
        Account_tax_groupDTO account_tax_groupdto = new Account_tax_groupDTO();
		Account_tax_group domain = new Account_tax_group();
		account_tax_groupdto.setId(account_tax_group_id);
		domain.setId(account_tax_group_id);
        Boolean rst = account_tax_groupService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_tax_group" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_tax_groups/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_tax_groupDTO> account_tax_groupdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_tax_group" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_tax_groups/{account_tax_group_id}")
    public ResponseEntity<Account_tax_groupDTO> get(@PathVariable("account_tax_group_id") Integer account_tax_group_id) {
        Account_tax_groupDTO dto = new Account_tax_groupDTO();
        Account_tax_group domain = account_tax_groupService.get(account_tax_group_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_tax_group" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_tax_groups/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_tax_groupDTO> account_tax_groupdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_tax_group" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_tax_groups/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_tax_group(@RequestBody List<Account_tax_groupDTO> account_tax_groupdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_tax_group" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_tax_groups")

    public ResponseEntity<Account_tax_groupDTO> create(@RequestBody Account_tax_groupDTO account_tax_groupdto) {
        Account_tax_groupDTO dto = new Account_tax_groupDTO();
        Account_tax_group domain = account_tax_groupdto.toDO();
		account_tax_groupService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_tax_group" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_tax_groups/fetchdefault")
	public ResponseEntity<Page<Account_tax_groupDTO>> fetchDefault(Account_tax_groupSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_tax_groupDTO> list = new ArrayList<Account_tax_groupDTO>();
        
        Page<Account_tax_group> domains = account_tax_groupService.searchDefault(context) ;
        for(Account_tax_group account_tax_group : domains.getContent()){
            Account_tax_groupDTO dto = new Account_tax_groupDTO();
            dto.fromDO(account_tax_group);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
