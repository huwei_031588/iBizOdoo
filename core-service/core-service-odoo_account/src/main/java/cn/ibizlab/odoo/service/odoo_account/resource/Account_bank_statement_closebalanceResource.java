package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_bank_statement_closebalanceDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_closebalance;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_closebalanceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_closebalanceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_bank_statement_closebalance" })
@RestController
@RequestMapping("")
public class Account_bank_statement_closebalanceResource {

    @Autowired
    private IAccount_bank_statement_closebalanceService account_bank_statement_closebalanceService;

    public IAccount_bank_statement_closebalanceService getAccount_bank_statement_closebalanceService() {
        return this.account_bank_statement_closebalanceService;
    }

    @ApiOperation(value = "删除数据", tags = {"Account_bank_statement_closebalance" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_closebalances/{account_bank_statement_closebalance_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_closebalance_id") Integer account_bank_statement_closebalance_id) {
        Account_bank_statement_closebalanceDTO account_bank_statement_closebalancedto = new Account_bank_statement_closebalanceDTO();
		Account_bank_statement_closebalance domain = new Account_bank_statement_closebalance();
		account_bank_statement_closebalancedto.setId(account_bank_statement_closebalance_id);
		domain.setId(account_bank_statement_closebalance_id);
        Boolean rst = account_bank_statement_closebalanceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_bank_statement_closebalance" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_closebalances/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_bank_statement_closebalanceDTO> account_bank_statement_closebalancedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_bank_statement_closebalance" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_closebalances")

    public ResponseEntity<Account_bank_statement_closebalanceDTO> create(@RequestBody Account_bank_statement_closebalanceDTO account_bank_statement_closebalancedto) {
        Account_bank_statement_closebalanceDTO dto = new Account_bank_statement_closebalanceDTO();
        Account_bank_statement_closebalance domain = account_bank_statement_closebalancedto.toDO();
		account_bank_statement_closebalanceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_bank_statement_closebalance" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_closebalances/{account_bank_statement_closebalance_id}")
    public ResponseEntity<Account_bank_statement_closebalanceDTO> get(@PathVariable("account_bank_statement_closebalance_id") Integer account_bank_statement_closebalance_id) {
        Account_bank_statement_closebalanceDTO dto = new Account_bank_statement_closebalanceDTO();
        Account_bank_statement_closebalance domain = account_bank_statement_closebalanceService.get(account_bank_statement_closebalance_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_bank_statement_closebalance" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_closebalances/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_bank_statement_closebalance(@RequestBody List<Account_bank_statement_closebalanceDTO> account_bank_statement_closebalancedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_bank_statement_closebalance" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_closebalances/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_closebalanceDTO> account_bank_statement_closebalancedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_bank_statement_closebalance" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_closebalances/{account_bank_statement_closebalance_id}")

    public ResponseEntity<Account_bank_statement_closebalanceDTO> update(@PathVariable("account_bank_statement_closebalance_id") Integer account_bank_statement_closebalance_id, @RequestBody Account_bank_statement_closebalanceDTO account_bank_statement_closebalancedto) {
		Account_bank_statement_closebalance domain = account_bank_statement_closebalancedto.toDO();
        domain.setId(account_bank_statement_closebalance_id);
		account_bank_statement_closebalanceService.update(domain);
		Account_bank_statement_closebalanceDTO dto = new Account_bank_statement_closebalanceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_bank_statement_closebalance" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_bank_statement_closebalances/fetchdefault")
	public ResponseEntity<Page<Account_bank_statement_closebalanceDTO>> fetchDefault(Account_bank_statement_closebalanceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_bank_statement_closebalanceDTO> list = new ArrayList<Account_bank_statement_closebalanceDTO>();
        
        Page<Account_bank_statement_closebalance> domains = account_bank_statement_closebalanceService.searchDefault(context) ;
        for(Account_bank_statement_closebalance account_bank_statement_closebalance : domains.getContent()){
            Account_bank_statement_closebalanceDTO dto = new Account_bank_statement_closebalanceDTO();
            dto.fromDO(account_bank_statement_closebalance);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
