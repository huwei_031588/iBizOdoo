package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_accountDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_accountService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_accountSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_account" })
@RestController
@RequestMapping("")
public class Account_accountResource {

    @Autowired
    private IAccount_accountService account_accountService;

    public IAccount_accountService getAccount_accountService() {
        return this.account_accountService;
    }

    @ApiOperation(value = "获取数据", tags = {"Account_account" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_accounts/{account_account_id}")
    public ResponseEntity<Account_accountDTO> get(@PathVariable("account_account_id") Integer account_account_id) {
        Account_accountDTO dto = new Account_accountDTO();
        Account_account domain = account_accountService.get(account_account_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_account" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_accounts/{account_account_id}")

    public ResponseEntity<Account_accountDTO> update(@PathVariable("account_account_id") Integer account_account_id, @RequestBody Account_accountDTO account_accountdto) {
		Account_account domain = account_accountdto.toDO();
        domain.setId(account_account_id);
		account_accountService.update(domain);
		Account_accountDTO dto = new Account_accountDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_account" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_accounts/{account_account_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_account_id") Integer account_account_id) {
        Account_accountDTO account_accountdto = new Account_accountDTO();
		Account_account domain = new Account_account();
		account_accountdto.setId(account_account_id);
		domain.setId(account_account_id);
        Boolean rst = account_accountService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_account" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_accounts/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_accountDTO> account_accountdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_account" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_accounts")

    public ResponseEntity<Account_accountDTO> create(@RequestBody Account_accountDTO account_accountdto) {
        Account_accountDTO dto = new Account_accountDTO();
        Account_account domain = account_accountdto.toDO();
		account_accountService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_account" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_accounts/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_account(@RequestBody List<Account_accountDTO> account_accountdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_account" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_accounts/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_accountDTO> account_accountdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_account" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_accounts/fetchdefault")
	public ResponseEntity<Page<Account_accountDTO>> fetchDefault(Account_accountSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_accountDTO> list = new ArrayList<Account_accountDTO>();
        
        Page<Account_account> domains = account_accountService.searchDefault(context) ;
        for(Account_account account_account : domains.getContent()){
            Account_accountDTO dto = new Account_accountDTO();
            dto.fromDO(account_account);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
