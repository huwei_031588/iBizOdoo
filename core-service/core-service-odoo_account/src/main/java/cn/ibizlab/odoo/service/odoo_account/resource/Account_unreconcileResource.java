package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_unreconcileDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_unreconcile;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_unreconcileService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_unreconcileSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_unreconcile" })
@RestController
@RequestMapping("")
public class Account_unreconcileResource {

    @Autowired
    private IAccount_unreconcileService account_unreconcileService;

    public IAccount_unreconcileService getAccount_unreconcileService() {
        return this.account_unreconcileService;
    }

    @ApiOperation(value = "删除数据", tags = {"Account_unreconcile" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_unreconciles/{account_unreconcile_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_unreconcile_id") Integer account_unreconcile_id) {
        Account_unreconcileDTO account_unreconciledto = new Account_unreconcileDTO();
		Account_unreconcile domain = new Account_unreconcile();
		account_unreconciledto.setId(account_unreconcile_id);
		domain.setId(account_unreconcile_id);
        Boolean rst = account_unreconcileService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_unreconcile" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_unreconciles/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_unreconcile(@RequestBody List<Account_unreconcileDTO> account_unreconciledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_unreconcile" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_unreconciles/{account_unreconcile_id}")
    public ResponseEntity<Account_unreconcileDTO> get(@PathVariable("account_unreconcile_id") Integer account_unreconcile_id) {
        Account_unreconcileDTO dto = new Account_unreconcileDTO();
        Account_unreconcile domain = account_unreconcileService.get(account_unreconcile_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_unreconcile" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_unreconciles/{account_unreconcile_id}")

    public ResponseEntity<Account_unreconcileDTO> update(@PathVariable("account_unreconcile_id") Integer account_unreconcile_id, @RequestBody Account_unreconcileDTO account_unreconciledto) {
		Account_unreconcile domain = account_unreconciledto.toDO();
        domain.setId(account_unreconcile_id);
		account_unreconcileService.update(domain);
		Account_unreconcileDTO dto = new Account_unreconcileDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_unreconcile" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_unreconciles")

    public ResponseEntity<Account_unreconcileDTO> create(@RequestBody Account_unreconcileDTO account_unreconciledto) {
        Account_unreconcileDTO dto = new Account_unreconcileDTO();
        Account_unreconcile domain = account_unreconciledto.toDO();
		account_unreconcileService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_unreconcile" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_unreconciles/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_unreconcileDTO> account_unreconciledtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_unreconcile" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_unreconciles/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_unreconcileDTO> account_unreconciledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_unreconcile" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_unreconciles/fetchdefault")
	public ResponseEntity<Page<Account_unreconcileDTO>> fetchDefault(Account_unreconcileSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_unreconcileDTO> list = new ArrayList<Account_unreconcileDTO>();
        
        Page<Account_unreconcile> domains = account_unreconcileService.searchDefault(context) ;
        for(Account_unreconcile account_unreconcile : domains.getContent()){
            Account_unreconcileDTO dto = new Account_unreconcileDTO();
            dto.fromDO(account_unreconcile);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
