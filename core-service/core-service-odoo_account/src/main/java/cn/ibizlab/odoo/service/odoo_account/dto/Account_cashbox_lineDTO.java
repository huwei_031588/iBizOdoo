package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_cashbox_line.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cashbox_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_cashbox_lineDTO]
 */
public class Account_cashbox_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DEFAULT_POS_ID]
     *
     */
    @Account_cashbox_lineDefault_pos_idDefault(info = "默认规则")
    private Integer default_pos_id;

    @JsonIgnore
    private boolean default_pos_idDirtyFlag;

    /**
     * 属性 [SUBTOTAL]
     *
     */
    @Account_cashbox_lineSubtotalDefault(info = "默认规则")
    private Double subtotal;

    @JsonIgnore
    private boolean subtotalDirtyFlag;

    /**
     * 属性 [COIN_VALUE]
     *
     */
    @Account_cashbox_lineCoin_valueDefault(info = "默认规则")
    private Double coin_value;

    @JsonIgnore
    private boolean coin_valueDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_cashbox_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_cashbox_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_cashbox_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_cashbox_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_cashbox_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [NUMBER]
     *
     */
    @Account_cashbox_lineNumberDefault(info = "默认规则")
    private Integer number;

    @JsonIgnore
    private boolean numberDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_cashbox_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_cashbox_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CASHBOX_ID]
     *
     */
    @Account_cashbox_lineCashbox_idDefault(info = "默认规则")
    private Integer cashbox_id;

    @JsonIgnore
    private boolean cashbox_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_cashbox_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_cashbox_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [DEFAULT_POS_ID]
     */
    @JsonProperty("default_pos_id")
    public Integer getDefault_pos_id(){
        return default_pos_id ;
    }

    /**
     * 设置 [DEFAULT_POS_ID]
     */
    @JsonProperty("default_pos_id")
    public void setDefault_pos_id(Integer  default_pos_id){
        this.default_pos_id = default_pos_id ;
        this.default_pos_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_POS_ID]脏标记
     */
    @JsonIgnore
    public boolean getDefault_pos_idDirtyFlag(){
        return default_pos_idDirtyFlag ;
    }

    /**
     * 获取 [SUBTOTAL]
     */
    @JsonProperty("subtotal")
    public Double getSubtotal(){
        return subtotal ;
    }

    /**
     * 设置 [SUBTOTAL]
     */
    @JsonProperty("subtotal")
    public void setSubtotal(Double  subtotal){
        this.subtotal = subtotal ;
        this.subtotalDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTOTAL]脏标记
     */
    @JsonIgnore
    public boolean getSubtotalDirtyFlag(){
        return subtotalDirtyFlag ;
    }

    /**
     * 获取 [COIN_VALUE]
     */
    @JsonProperty("coin_value")
    public Double getCoin_value(){
        return coin_value ;
    }

    /**
     * 设置 [COIN_VALUE]
     */
    @JsonProperty("coin_value")
    public void setCoin_value(Double  coin_value){
        this.coin_value = coin_value ;
        this.coin_valueDirtyFlag = true ;
    }

    /**
     * 获取 [COIN_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getCoin_valueDirtyFlag(){
        return coin_valueDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [NUMBER]
     */
    @JsonProperty("number")
    public Integer getNumber(){
        return number ;
    }

    /**
     * 设置 [NUMBER]
     */
    @JsonProperty("number")
    public void setNumber(Integer  number){
        this.number = number ;
        this.numberDirtyFlag = true ;
    }

    /**
     * 获取 [NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getNumberDirtyFlag(){
        return numberDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CASHBOX_ID]
     */
    @JsonProperty("cashbox_id")
    public Integer getCashbox_id(){
        return cashbox_id ;
    }

    /**
     * 设置 [CASHBOX_ID]
     */
    @JsonProperty("cashbox_id")
    public void setCashbox_id(Integer  cashbox_id){
        this.cashbox_id = cashbox_id ;
        this.cashbox_idDirtyFlag = true ;
    }

    /**
     * 获取 [CASHBOX_ID]脏标记
     */
    @JsonIgnore
    public boolean getCashbox_idDirtyFlag(){
        return cashbox_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Account_cashbox_line toDO() {
        Account_cashbox_line srfdomain = new Account_cashbox_line();
        if(getDefault_pos_idDirtyFlag())
            srfdomain.setDefault_pos_id(default_pos_id);
        if(getSubtotalDirtyFlag())
            srfdomain.setSubtotal(subtotal);
        if(getCoin_valueDirtyFlag())
            srfdomain.setCoin_value(coin_value);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getNumberDirtyFlag())
            srfdomain.setNumber(number);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCashbox_idDirtyFlag())
            srfdomain.setCashbox_id(cashbox_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Account_cashbox_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDefault_pos_idDirtyFlag())
            this.setDefault_pos_id(srfdomain.getDefault_pos_id());
        if(srfdomain.getSubtotalDirtyFlag())
            this.setSubtotal(srfdomain.getSubtotal());
        if(srfdomain.getCoin_valueDirtyFlag())
            this.setCoin_value(srfdomain.getCoin_value());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getNumberDirtyFlag())
            this.setNumber(srfdomain.getNumber());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCashbox_idDirtyFlag())
            this.setCashbox_id(srfdomain.getCashbox_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Account_cashbox_lineDTO> fromDOPage(List<Account_cashbox_line> poPage)   {
        if(poPage == null)
            return null;
        List<Account_cashbox_lineDTO> dtos=new ArrayList<Account_cashbox_lineDTO>();
        for(Account_cashbox_line domain : poPage) {
            Account_cashbox_lineDTO dto = new Account_cashbox_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

