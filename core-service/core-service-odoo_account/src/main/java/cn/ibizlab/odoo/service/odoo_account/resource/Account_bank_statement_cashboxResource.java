package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_bank_statement_cashboxDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_cashbox;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_cashboxService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_cashboxSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_bank_statement_cashbox" })
@RestController
@RequestMapping("")
public class Account_bank_statement_cashboxResource {

    @Autowired
    private IAccount_bank_statement_cashboxService account_bank_statement_cashboxService;

    public IAccount_bank_statement_cashboxService getAccount_bank_statement_cashboxService() {
        return this.account_bank_statement_cashboxService;
    }

    @ApiOperation(value = "删除数据", tags = {"Account_bank_statement_cashbox" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_cashboxes/{account_bank_statement_cashbox_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_cashbox_id") Integer account_bank_statement_cashbox_id) {
        Account_bank_statement_cashboxDTO account_bank_statement_cashboxdto = new Account_bank_statement_cashboxDTO();
		Account_bank_statement_cashbox domain = new Account_bank_statement_cashbox();
		account_bank_statement_cashboxdto.setId(account_bank_statement_cashbox_id);
		domain.setId(account_bank_statement_cashbox_id);
        Boolean rst = account_bank_statement_cashboxService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Account_bank_statement_cashbox" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_cashboxes/{account_bank_statement_cashbox_id}")

    public ResponseEntity<Account_bank_statement_cashboxDTO> update(@PathVariable("account_bank_statement_cashbox_id") Integer account_bank_statement_cashbox_id, @RequestBody Account_bank_statement_cashboxDTO account_bank_statement_cashboxdto) {
		Account_bank_statement_cashbox domain = account_bank_statement_cashboxdto.toDO();
        domain.setId(account_bank_statement_cashbox_id);
		account_bank_statement_cashboxService.update(domain);
		Account_bank_statement_cashboxDTO dto = new Account_bank_statement_cashboxDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_bank_statement_cashbox" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_cashboxes/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_cashboxDTO> account_bank_statement_cashboxdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_bank_statement_cashbox" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_cashboxes")

    public ResponseEntity<Account_bank_statement_cashboxDTO> create(@RequestBody Account_bank_statement_cashboxDTO account_bank_statement_cashboxdto) {
        Account_bank_statement_cashboxDTO dto = new Account_bank_statement_cashboxDTO();
        Account_bank_statement_cashbox domain = account_bank_statement_cashboxdto.toDO();
		account_bank_statement_cashboxService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_bank_statement_cashbox" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_cashboxes/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_bank_statement_cashbox(@RequestBody List<Account_bank_statement_cashboxDTO> account_bank_statement_cashboxdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_bank_statement_cashbox" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_cashboxes/{account_bank_statement_cashbox_id}")
    public ResponseEntity<Account_bank_statement_cashboxDTO> get(@PathVariable("account_bank_statement_cashbox_id") Integer account_bank_statement_cashbox_id) {
        Account_bank_statement_cashboxDTO dto = new Account_bank_statement_cashboxDTO();
        Account_bank_statement_cashbox domain = account_bank_statement_cashboxService.get(account_bank_statement_cashbox_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_bank_statement_cashbox" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_cashboxes/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_bank_statement_cashboxDTO> account_bank_statement_cashboxdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_bank_statement_cashbox" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_bank_statement_cashboxes/fetchdefault")
	public ResponseEntity<Page<Account_bank_statement_cashboxDTO>> fetchDefault(Account_bank_statement_cashboxSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_bank_statement_cashboxDTO> list = new ArrayList<Account_bank_statement_cashboxDTO>();
        
        Page<Account_bank_statement_cashbox> domains = account_bank_statement_cashboxService.searchDefault(context) ;
        for(Account_bank_statement_cashbox account_bank_statement_cashbox : domains.getContent()){
            Account_bank_statement_cashboxDTO dto = new Account_bank_statement_cashboxDTO();
            dto.fromDO(account_bank_statement_cashbox);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
