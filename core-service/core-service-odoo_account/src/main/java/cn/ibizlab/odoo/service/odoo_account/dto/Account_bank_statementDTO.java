package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_bank_statement.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_bank_statementDTO]
 */
public class Account_bank_statementDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Account_bank_statementMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @Account_bank_statementMove_line_idsDefault(info = "默认规则")
    private String move_line_ids;

    @JsonIgnore
    private boolean move_line_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_bank_statementNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_bank_statementDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [BALANCE_END_REAL]
     *
     */
    @Account_bank_statementBalance_end_realDefault(info = "默认规则")
    private Double balance_end_real;

    @JsonIgnore
    private boolean balance_end_realDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_bank_statement__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [POS_SESSION_ID]
     *
     */
    @Account_bank_statementPos_session_idDefault(info = "默认规则")
    private Integer pos_session_id;

    @JsonIgnore
    private boolean pos_session_idDirtyFlag;

    /**
     * 属性 [BALANCE_END]
     *
     */
    @Account_bank_statementBalance_endDefault(info = "默认规则")
    private Double balance_end;

    @JsonIgnore
    private boolean balance_endDirtyFlag;

    /**
     * 属性 [TOTAL_ENTRY_ENCODING]
     *
     */
    @Account_bank_statementTotal_entry_encodingDefault(info = "默认规则")
    private Double total_entry_encoding;

    @JsonIgnore
    private boolean total_entry_encodingDirtyFlag;

    /**
     * 属性 [DATE_DONE]
     *
     */
    @Account_bank_statementDate_doneDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_done;

    @JsonIgnore
    private boolean date_doneDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Account_bank_statementMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_bank_statementIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [ALL_LINES_RECONCILED]
     *
     */
    @Account_bank_statementAll_lines_reconciledDefault(info = "默认规则")
    private String all_lines_reconciled;

    @JsonIgnore
    private boolean all_lines_reconciledDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Account_bank_statementMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Account_bank_statementWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Account_bank_statementMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @Account_bank_statementLine_idsDefault(info = "默认规则")
    private String line_ids;

    @JsonIgnore
    private boolean line_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Account_bank_statementMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Account_bank_statementMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Account_bank_statementMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MOVE_LINE_COUNT]
     *
     */
    @Account_bank_statementMove_line_countDefault(info = "默认规则")
    private Integer move_line_count;

    @JsonIgnore
    private boolean move_line_countDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Account_bank_statementStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_bank_statementCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Account_bank_statementDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [IS_DIFFERENCE_ZERO]
     *
     */
    @Account_bank_statementIs_difference_zeroDefault(info = "默认规则")
    private String is_difference_zero;

    @JsonIgnore
    private boolean is_difference_zeroDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Account_bank_statementMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_bank_statementCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Account_bank_statementMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Account_bank_statementMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Account_bank_statementMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Account_bank_statementMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Account_bank_statementMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [DIFFERENCE]
     *
     */
    @Account_bank_statementDifferenceDefault(info = "默认规则")
    private Double difference;

    @JsonIgnore
    private boolean differenceDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_bank_statementWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [REFERENCE]
     *
     */
    @Account_bank_statementReferenceDefault(info = "默认规则")
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;

    /**
     * 属性 [BALANCE_START]
     *
     */
    @Account_bank_statementBalance_startDefault(info = "默认规则")
    private Double balance_start;

    @JsonIgnore
    private boolean balance_startDirtyFlag;

    /**
     * 属性 [ACCOUNTING_DATE]
     *
     */
    @Account_bank_statementAccounting_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp accounting_date;

    @JsonIgnore
    private boolean accounting_dateDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Account_bank_statementUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_bank_statementWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_bank_statementCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Account_bank_statementJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [JOURNAL_TYPE]
     *
     */
    @Account_bank_statementJournal_typeDefault(info = "默认规则")
    private String journal_type;

    @JsonIgnore
    private boolean journal_typeDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_bank_statementCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @Account_bank_statementAccount_idDefault(info = "默认规则")
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;

    /**
     * 属性 [CASHBOX_START_ID]
     *
     */
    @Account_bank_statementCashbox_start_idDefault(info = "默认规则")
    private Integer cashbox_start_id;

    @JsonIgnore
    private boolean cashbox_start_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_bank_statementCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_bank_statementJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_bank_statementCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CASHBOX_END_ID]
     *
     */
    @Account_bank_statementCashbox_end_idDefault(info = "默认规则")
    private Integer cashbox_end_id;

    @JsonIgnore
    private boolean cashbox_end_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Account_bank_statementUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_bank_statementWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return move_line_ids ;
    }

    /**
     * 设置 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [BALANCE_END_REAL]
     */
    @JsonProperty("balance_end_real")
    public Double getBalance_end_real(){
        return balance_end_real ;
    }

    /**
     * 设置 [BALANCE_END_REAL]
     */
    @JsonProperty("balance_end_real")
    public void setBalance_end_real(Double  balance_end_real){
        this.balance_end_real = balance_end_real ;
        this.balance_end_realDirtyFlag = true ;
    }

    /**
     * 获取 [BALANCE_END_REAL]脏标记
     */
    @JsonIgnore
    public boolean getBalance_end_realDirtyFlag(){
        return balance_end_realDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [POS_SESSION_ID]
     */
    @JsonProperty("pos_session_id")
    public Integer getPos_session_id(){
        return pos_session_id ;
    }

    /**
     * 设置 [POS_SESSION_ID]
     */
    @JsonProperty("pos_session_id")
    public void setPos_session_id(Integer  pos_session_id){
        this.pos_session_id = pos_session_id ;
        this.pos_session_idDirtyFlag = true ;
    }

    /**
     * 获取 [POS_SESSION_ID]脏标记
     */
    @JsonIgnore
    public boolean getPos_session_idDirtyFlag(){
        return pos_session_idDirtyFlag ;
    }

    /**
     * 获取 [BALANCE_END]
     */
    @JsonProperty("balance_end")
    public Double getBalance_end(){
        return balance_end ;
    }

    /**
     * 设置 [BALANCE_END]
     */
    @JsonProperty("balance_end")
    public void setBalance_end(Double  balance_end){
        this.balance_end = balance_end ;
        this.balance_endDirtyFlag = true ;
    }

    /**
     * 获取 [BALANCE_END]脏标记
     */
    @JsonIgnore
    public boolean getBalance_endDirtyFlag(){
        return balance_endDirtyFlag ;
    }

    /**
     * 获取 [TOTAL_ENTRY_ENCODING]
     */
    @JsonProperty("total_entry_encoding")
    public Double getTotal_entry_encoding(){
        return total_entry_encoding ;
    }

    /**
     * 设置 [TOTAL_ENTRY_ENCODING]
     */
    @JsonProperty("total_entry_encoding")
    public void setTotal_entry_encoding(Double  total_entry_encoding){
        this.total_entry_encoding = total_entry_encoding ;
        this.total_entry_encodingDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL_ENTRY_ENCODING]脏标记
     */
    @JsonIgnore
    public boolean getTotal_entry_encodingDirtyFlag(){
        return total_entry_encodingDirtyFlag ;
    }

    /**
     * 获取 [DATE_DONE]
     */
    @JsonProperty("date_done")
    public Timestamp getDate_done(){
        return date_done ;
    }

    /**
     * 设置 [DATE_DONE]
     */
    @JsonProperty("date_done")
    public void setDate_done(Timestamp  date_done){
        this.date_done = date_done ;
        this.date_doneDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_DONE]脏标记
     */
    @JsonIgnore
    public boolean getDate_doneDirtyFlag(){
        return date_doneDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [ALL_LINES_RECONCILED]
     */
    @JsonProperty("all_lines_reconciled")
    public String getAll_lines_reconciled(){
        return all_lines_reconciled ;
    }

    /**
     * 设置 [ALL_LINES_RECONCILED]
     */
    @JsonProperty("all_lines_reconciled")
    public void setAll_lines_reconciled(String  all_lines_reconciled){
        this.all_lines_reconciled = all_lines_reconciled ;
        this.all_lines_reconciledDirtyFlag = true ;
    }

    /**
     * 获取 [ALL_LINES_RECONCILED]脏标记
     */
    @JsonIgnore
    public boolean getAll_lines_reconciledDirtyFlag(){
        return all_lines_reconciledDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [LINE_IDS]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return line_ids ;
    }

    /**
     * 设置 [LINE_IDS]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return line_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MOVE_LINE_COUNT]
     */
    @JsonProperty("move_line_count")
    public Integer getMove_line_count(){
        return move_line_count ;
    }

    /**
     * 设置 [MOVE_LINE_COUNT]
     */
    @JsonProperty("move_line_count")
    public void setMove_line_count(Integer  move_line_count){
        this.move_line_count = move_line_count ;
        this.move_line_countDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_countDirtyFlag(){
        return move_line_countDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [IS_DIFFERENCE_ZERO]
     */
    @JsonProperty("is_difference_zero")
    public String getIs_difference_zero(){
        return is_difference_zero ;
    }

    /**
     * 设置 [IS_DIFFERENCE_ZERO]
     */
    @JsonProperty("is_difference_zero")
    public void setIs_difference_zero(String  is_difference_zero){
        this.is_difference_zero = is_difference_zero ;
        this.is_difference_zeroDirtyFlag = true ;
    }

    /**
     * 获取 [IS_DIFFERENCE_ZERO]脏标记
     */
    @JsonIgnore
    public boolean getIs_difference_zeroDirtyFlag(){
        return is_difference_zeroDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [DIFFERENCE]
     */
    @JsonProperty("difference")
    public Double getDifference(){
        return difference ;
    }

    /**
     * 设置 [DIFFERENCE]
     */
    @JsonProperty("difference")
    public void setDifference(Double  difference){
        this.difference = difference ;
        this.differenceDirtyFlag = true ;
    }

    /**
     * 获取 [DIFFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getDifferenceDirtyFlag(){
        return differenceDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [REFERENCE]
     */
    @JsonProperty("reference")
    public String getReference(){
        return reference ;
    }

    /**
     * 设置 [REFERENCE]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return referenceDirtyFlag ;
    }

    /**
     * 获取 [BALANCE_START]
     */
    @JsonProperty("balance_start")
    public Double getBalance_start(){
        return balance_start ;
    }

    /**
     * 设置 [BALANCE_START]
     */
    @JsonProperty("balance_start")
    public void setBalance_start(Double  balance_start){
        this.balance_start = balance_start ;
        this.balance_startDirtyFlag = true ;
    }

    /**
     * 获取 [BALANCE_START]脏标记
     */
    @JsonIgnore
    public boolean getBalance_startDirtyFlag(){
        return balance_startDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNTING_DATE]
     */
    @JsonProperty("accounting_date")
    public Timestamp getAccounting_date(){
        return accounting_date ;
    }

    /**
     * 设置 [ACCOUNTING_DATE]
     */
    @JsonProperty("accounting_date")
    public void setAccounting_date(Timestamp  accounting_date){
        this.accounting_date = accounting_date ;
        this.accounting_dateDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNTING_DATE]脏标记
     */
    @JsonIgnore
    public boolean getAccounting_dateDirtyFlag(){
        return accounting_dateDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_TYPE]
     */
    @JsonProperty("journal_type")
    public String getJournal_type(){
        return journal_type ;
    }

    /**
     * 设置 [JOURNAL_TYPE]
     */
    @JsonProperty("journal_type")
    public void setJournal_type(String  journal_type){
        this.journal_type = journal_type ;
        this.journal_typeDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getJournal_typeDirtyFlag(){
        return journal_typeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return account_id ;
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return account_idDirtyFlag ;
    }

    /**
     * 获取 [CASHBOX_START_ID]
     */
    @JsonProperty("cashbox_start_id")
    public Integer getCashbox_start_id(){
        return cashbox_start_id ;
    }

    /**
     * 设置 [CASHBOX_START_ID]
     */
    @JsonProperty("cashbox_start_id")
    public void setCashbox_start_id(Integer  cashbox_start_id){
        this.cashbox_start_id = cashbox_start_id ;
        this.cashbox_start_idDirtyFlag = true ;
    }

    /**
     * 获取 [CASHBOX_START_ID]脏标记
     */
    @JsonIgnore
    public boolean getCashbox_start_idDirtyFlag(){
        return cashbox_start_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CASHBOX_END_ID]
     */
    @JsonProperty("cashbox_end_id")
    public Integer getCashbox_end_id(){
        return cashbox_end_id ;
    }

    /**
     * 设置 [CASHBOX_END_ID]
     */
    @JsonProperty("cashbox_end_id")
    public void setCashbox_end_id(Integer  cashbox_end_id){
        this.cashbox_end_id = cashbox_end_id ;
        this.cashbox_end_idDirtyFlag = true ;
    }

    /**
     * 获取 [CASHBOX_END_ID]脏标记
     */
    @JsonIgnore
    public boolean getCashbox_end_idDirtyFlag(){
        return cashbox_end_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Account_bank_statement toDO() {
        Account_bank_statement srfdomain = new Account_bank_statement();
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getMove_line_idsDirtyFlag())
            srfdomain.setMove_line_ids(move_line_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getBalance_end_realDirtyFlag())
            srfdomain.setBalance_end_real(balance_end_real);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getPos_session_idDirtyFlag())
            srfdomain.setPos_session_id(pos_session_id);
        if(getBalance_endDirtyFlag())
            srfdomain.setBalance_end(balance_end);
        if(getTotal_entry_encodingDirtyFlag())
            srfdomain.setTotal_entry_encoding(total_entry_encoding);
        if(getDate_doneDirtyFlag())
            srfdomain.setDate_done(date_done);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getAll_lines_reconciledDirtyFlag())
            srfdomain.setAll_lines_reconciled(all_lines_reconciled);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getLine_idsDirtyFlag())
            srfdomain.setLine_ids(line_ids);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMove_line_countDirtyFlag())
            srfdomain.setMove_line_count(move_line_count);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getIs_difference_zeroDirtyFlag())
            srfdomain.setIs_difference_zero(is_difference_zero);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getDifferenceDirtyFlag())
            srfdomain.setDifference(difference);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getReferenceDirtyFlag())
            srfdomain.setReference(reference);
        if(getBalance_startDirtyFlag())
            srfdomain.setBalance_start(balance_start);
        if(getAccounting_dateDirtyFlag())
            srfdomain.setAccounting_date(accounting_date);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getJournal_typeDirtyFlag())
            srfdomain.setJournal_type(journal_type);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getAccount_idDirtyFlag())
            srfdomain.setAccount_id(account_id);
        if(getCashbox_start_idDirtyFlag())
            srfdomain.setCashbox_start_id(cashbox_start_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCashbox_end_idDirtyFlag())
            srfdomain.setCashbox_end_id(cashbox_end_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Account_bank_statement srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getMove_line_idsDirtyFlag())
            this.setMove_line_ids(srfdomain.getMove_line_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getBalance_end_realDirtyFlag())
            this.setBalance_end_real(srfdomain.getBalance_end_real());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getPos_session_idDirtyFlag())
            this.setPos_session_id(srfdomain.getPos_session_id());
        if(srfdomain.getBalance_endDirtyFlag())
            this.setBalance_end(srfdomain.getBalance_end());
        if(srfdomain.getTotal_entry_encodingDirtyFlag())
            this.setTotal_entry_encoding(srfdomain.getTotal_entry_encoding());
        if(srfdomain.getDate_doneDirtyFlag())
            this.setDate_done(srfdomain.getDate_done());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getAll_lines_reconciledDirtyFlag())
            this.setAll_lines_reconciled(srfdomain.getAll_lines_reconciled());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getLine_idsDirtyFlag())
            this.setLine_ids(srfdomain.getLine_ids());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMove_line_countDirtyFlag())
            this.setMove_line_count(srfdomain.getMove_line_count());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getIs_difference_zeroDirtyFlag())
            this.setIs_difference_zero(srfdomain.getIs_difference_zero());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getDifferenceDirtyFlag())
            this.setDifference(srfdomain.getDifference());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getReferenceDirtyFlag())
            this.setReference(srfdomain.getReference());
        if(srfdomain.getBalance_startDirtyFlag())
            this.setBalance_start(srfdomain.getBalance_start());
        if(srfdomain.getAccounting_dateDirtyFlag())
            this.setAccounting_date(srfdomain.getAccounting_date());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getJournal_typeDirtyFlag())
            this.setJournal_type(srfdomain.getJournal_type());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getAccount_idDirtyFlag())
            this.setAccount_id(srfdomain.getAccount_id());
        if(srfdomain.getCashbox_start_idDirtyFlag())
            this.setCashbox_start_id(srfdomain.getCashbox_start_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCashbox_end_idDirtyFlag())
            this.setCashbox_end_id(srfdomain.getCashbox_end_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Account_bank_statementDTO> fromDOPage(List<Account_bank_statement> poPage)   {
        if(poPage == null)
            return null;
        List<Account_bank_statementDTO> dtos=new ArrayList<Account_bank_statementDTO>();
        for(Account_bank_statement domain : poPage) {
            Account_bank_statementDTO dto = new Account_bank_statementDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

