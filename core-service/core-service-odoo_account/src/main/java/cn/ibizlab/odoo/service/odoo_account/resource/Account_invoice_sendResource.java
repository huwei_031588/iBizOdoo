package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_invoice_sendDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_send;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_sendService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_sendSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_invoice_send" })
@RestController
@RequestMapping("")
public class Account_invoice_sendResource {

    @Autowired
    private IAccount_invoice_sendService account_invoice_sendService;

    public IAccount_invoice_sendService getAccount_invoice_sendService() {
        return this.account_invoice_sendService;
    }

    @ApiOperation(value = "获取数据", tags = {"Account_invoice_send" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_sends/{account_invoice_send_id}")
    public ResponseEntity<Account_invoice_sendDTO> get(@PathVariable("account_invoice_send_id") Integer account_invoice_send_id) {
        Account_invoice_sendDTO dto = new Account_invoice_sendDTO();
        Account_invoice_send domain = account_invoice_sendService.get(account_invoice_send_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_invoice_send" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_sends/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_sendDTO> account_invoice_senddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_invoice_send" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_sends/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_invoice_sendDTO> account_invoice_senddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_invoice_send" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_sends/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_invoice_send(@RequestBody List<Account_invoice_sendDTO> account_invoice_senddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_invoice_send" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_sends/{account_invoice_send_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_send_id") Integer account_invoice_send_id) {
        Account_invoice_sendDTO account_invoice_senddto = new Account_invoice_sendDTO();
		Account_invoice_send domain = new Account_invoice_send();
		account_invoice_senddto.setId(account_invoice_send_id);
		domain.setId(account_invoice_send_id);
        Boolean rst = account_invoice_sendService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Account_invoice_send" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_sends/{account_invoice_send_id}")

    public ResponseEntity<Account_invoice_sendDTO> update(@PathVariable("account_invoice_send_id") Integer account_invoice_send_id, @RequestBody Account_invoice_sendDTO account_invoice_senddto) {
		Account_invoice_send domain = account_invoice_senddto.toDO();
        domain.setId(account_invoice_send_id);
		account_invoice_sendService.update(domain);
		Account_invoice_sendDTO dto = new Account_invoice_sendDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_invoice_send" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_sends")

    public ResponseEntity<Account_invoice_sendDTO> create(@RequestBody Account_invoice_sendDTO account_invoice_senddto) {
        Account_invoice_sendDTO dto = new Account_invoice_sendDTO();
        Account_invoice_send domain = account_invoice_senddto.toDO();
		account_invoice_sendService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_invoice_send" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_invoice_sends/fetchdefault")
	public ResponseEntity<Page<Account_invoice_sendDTO>> fetchDefault(Account_invoice_sendSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_invoice_sendDTO> list = new ArrayList<Account_invoice_sendDTO>();
        
        Page<Account_invoice_send> domains = account_invoice_sendService.searchDefault(context) ;
        for(Account_invoice_send account_invoice_send : domains.getContent()){
            Account_invoice_sendDTO dto = new Account_invoice_sendDTO();
            dto.fromDO(account_invoice_send);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
