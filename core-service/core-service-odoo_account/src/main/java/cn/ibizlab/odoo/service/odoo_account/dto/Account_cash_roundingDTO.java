package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_cash_rounding.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_cash_roundingDTO]
 */
public class Account_cash_roundingDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ROUNDING_METHOD]
     *
     */
    @Account_cash_roundingRounding_methodDefault(info = "默认规则")
    private String rounding_method;

    @JsonIgnore
    private boolean rounding_methodDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_cash_roundingWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [STRATEGY]
     *
     */
    @Account_cash_roundingStrategyDefault(info = "默认规则")
    private String strategy;

    @JsonIgnore
    private boolean strategyDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_cash_roundingDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_cash_rounding__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_cash_roundingNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ROUNDING]
     *
     */
    @Account_cash_roundingRoundingDefault(info = "默认规则")
    private Double rounding;

    @JsonIgnore
    private boolean roundingDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_cash_roundingCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_cash_roundingIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_cash_roundingCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @Account_cash_roundingAccount_id_textDefault(info = "默认规则")
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_cash_roundingWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @Account_cash_roundingAccount_idDefault(info = "默认规则")
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_cash_roundingCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_cash_roundingWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [ROUNDING_METHOD]
     */
    @JsonProperty("rounding_method")
    public String getRounding_method(){
        return rounding_method ;
    }

    /**
     * 设置 [ROUNDING_METHOD]
     */
    @JsonProperty("rounding_method")
    public void setRounding_method(String  rounding_method){
        this.rounding_method = rounding_method ;
        this.rounding_methodDirtyFlag = true ;
    }

    /**
     * 获取 [ROUNDING_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getRounding_methodDirtyFlag(){
        return rounding_methodDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [STRATEGY]
     */
    @JsonProperty("strategy")
    public String getStrategy(){
        return strategy ;
    }

    /**
     * 设置 [STRATEGY]
     */
    @JsonProperty("strategy")
    public void setStrategy(String  strategy){
        this.strategy = strategy ;
        this.strategyDirtyFlag = true ;
    }

    /**
     * 获取 [STRATEGY]脏标记
     */
    @JsonIgnore
    public boolean getStrategyDirtyFlag(){
        return strategyDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ROUNDING]
     */
    @JsonProperty("rounding")
    public Double getRounding(){
        return rounding ;
    }

    /**
     * 设置 [ROUNDING]
     */
    @JsonProperty("rounding")
    public void setRounding(Double  rounding){
        this.rounding = rounding ;
        this.roundingDirtyFlag = true ;
    }

    /**
     * 获取 [ROUNDING]脏标记
     */
    @JsonIgnore
    public boolean getRoundingDirtyFlag(){
        return roundingDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return account_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return account_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return account_id ;
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return account_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Account_cash_rounding toDO() {
        Account_cash_rounding srfdomain = new Account_cash_rounding();
        if(getRounding_methodDirtyFlag())
            srfdomain.setRounding_method(rounding_method);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getStrategyDirtyFlag())
            srfdomain.setStrategy(strategy);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getRoundingDirtyFlag())
            srfdomain.setRounding(rounding);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getAccount_id_textDirtyFlag())
            srfdomain.setAccount_id_text(account_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getAccount_idDirtyFlag())
            srfdomain.setAccount_id(account_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Account_cash_rounding srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getRounding_methodDirtyFlag())
            this.setRounding_method(srfdomain.getRounding_method());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getStrategyDirtyFlag())
            this.setStrategy(srfdomain.getStrategy());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getRoundingDirtyFlag())
            this.setRounding(srfdomain.getRounding());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getAccount_id_textDirtyFlag())
            this.setAccount_id_text(srfdomain.getAccount_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getAccount_idDirtyFlag())
            this.setAccount_id(srfdomain.getAccount_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Account_cash_roundingDTO> fromDOPage(List<Account_cash_rounding> poPage)   {
        if(poPage == null)
            return null;
        List<Account_cash_roundingDTO> dtos=new ArrayList<Account_cash_roundingDTO>();
        for(Account_cash_rounding domain : poPage) {
            Account_cash_roundingDTO dto = new Account_cash_roundingDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

