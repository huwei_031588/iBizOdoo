package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_partial_reconcile.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_partial_reconcileDTO]
 */
public class Account_partial_reconcileDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [AMOUNT_CURRENCY]
     *
     */
    @Account_partial_reconcileAmount_currencyDefault(info = "默认规则")
    private Double amount_currency;

    @JsonIgnore
    private boolean amount_currencyDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Account_partial_reconcileAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_partial_reconcileWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_partial_reconcileIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_partial_reconcile__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_partial_reconcileDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_partial_reconcileCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MAX_DATE]
     *
     */
    @Account_partial_reconcileMax_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp max_date;

    @JsonIgnore
    private boolean max_dateDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_partial_reconcileCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CREDIT_MOVE_ID_TEXT]
     *
     */
    @Account_partial_reconcileCredit_move_id_textDefault(info = "默认规则")
    private String credit_move_id_text;

    @JsonIgnore
    private boolean credit_move_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @Account_partial_reconcileCompany_currency_idDefault(info = "默认规则")
    private Integer company_currency_id;

    @JsonIgnore
    private boolean company_currency_idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_partial_reconcileCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [DEBIT_MOVE_ID_TEXT]
     *
     */
    @Account_partial_reconcileDebit_move_id_textDefault(info = "默认规则")
    private String debit_move_id_text;

    @JsonIgnore
    private boolean debit_move_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_partial_reconcileCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_partial_reconcileWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [FULL_RECONCILE_ID_TEXT]
     *
     */
    @Account_partial_reconcileFull_reconcile_id_textDefault(info = "默认规则")
    private String full_reconcile_id_text;

    @JsonIgnore
    private boolean full_reconcile_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_partial_reconcileCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_partial_reconcileCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [FULL_RECONCILE_ID]
     *
     */
    @Account_partial_reconcileFull_reconcile_idDefault(info = "默认规则")
    private Integer full_reconcile_id;

    @JsonIgnore
    private boolean full_reconcile_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_partial_reconcileCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [DEBIT_MOVE_ID]
     *
     */
    @Account_partial_reconcileDebit_move_idDefault(info = "默认规则")
    private Integer debit_move_id;

    @JsonIgnore
    private boolean debit_move_idDirtyFlag;

    /**
     * 属性 [CREDIT_MOVE_ID]
     *
     */
    @Account_partial_reconcileCredit_move_idDefault(info = "默认规则")
    private Integer credit_move_id;

    @JsonIgnore
    private boolean credit_move_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_partial_reconcileWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [AMOUNT_CURRENCY]
     */
    @JsonProperty("amount_currency")
    public Double getAmount_currency(){
        return amount_currency ;
    }

    /**
     * 设置 [AMOUNT_CURRENCY]
     */
    @JsonProperty("amount_currency")
    public void setAmount_currency(Double  amount_currency){
        this.amount_currency = amount_currency ;
        this.amount_currencyDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_CURRENCY]脏标记
     */
    @JsonIgnore
    public boolean getAmount_currencyDirtyFlag(){
        return amount_currencyDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MAX_DATE]
     */
    @JsonProperty("max_date")
    public Timestamp getMax_date(){
        return max_date ;
    }

    /**
     * 设置 [MAX_DATE]
     */
    @JsonProperty("max_date")
    public void setMax_date(Timestamp  max_date){
        this.max_date = max_date ;
        this.max_dateDirtyFlag = true ;
    }

    /**
     * 获取 [MAX_DATE]脏标记
     */
    @JsonIgnore
    public boolean getMax_dateDirtyFlag(){
        return max_dateDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREDIT_MOVE_ID_TEXT]
     */
    @JsonProperty("credit_move_id_text")
    public String getCredit_move_id_text(){
        return credit_move_id_text ;
    }

    /**
     * 设置 [CREDIT_MOVE_ID_TEXT]
     */
    @JsonProperty("credit_move_id_text")
    public void setCredit_move_id_text(String  credit_move_id_text){
        this.credit_move_id_text = credit_move_id_text ;
        this.credit_move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREDIT_MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCredit_move_id_textDirtyFlag(){
        return credit_move_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return company_currency_id ;
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return company_currency_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [DEBIT_MOVE_ID_TEXT]
     */
    @JsonProperty("debit_move_id_text")
    public String getDebit_move_id_text(){
        return debit_move_id_text ;
    }

    /**
     * 设置 [DEBIT_MOVE_ID_TEXT]
     */
    @JsonProperty("debit_move_id_text")
    public void setDebit_move_id_text(String  debit_move_id_text){
        this.debit_move_id_text = debit_move_id_text ;
        this.debit_move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEBIT_MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDebit_move_id_textDirtyFlag(){
        return debit_move_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [FULL_RECONCILE_ID_TEXT]
     */
    @JsonProperty("full_reconcile_id_text")
    public String getFull_reconcile_id_text(){
        return full_reconcile_id_text ;
    }

    /**
     * 设置 [FULL_RECONCILE_ID_TEXT]
     */
    @JsonProperty("full_reconcile_id_text")
    public void setFull_reconcile_id_text(String  full_reconcile_id_text){
        this.full_reconcile_id_text = full_reconcile_id_text ;
        this.full_reconcile_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [FULL_RECONCILE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getFull_reconcile_id_textDirtyFlag(){
        return full_reconcile_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [FULL_RECONCILE_ID]
     */
    @JsonProperty("full_reconcile_id")
    public Integer getFull_reconcile_id(){
        return full_reconcile_id ;
    }

    /**
     * 设置 [FULL_RECONCILE_ID]
     */
    @JsonProperty("full_reconcile_id")
    public void setFull_reconcile_id(Integer  full_reconcile_id){
        this.full_reconcile_id = full_reconcile_id ;
        this.full_reconcile_idDirtyFlag = true ;
    }

    /**
     * 获取 [FULL_RECONCILE_ID]脏标记
     */
    @JsonIgnore
    public boolean getFull_reconcile_idDirtyFlag(){
        return full_reconcile_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [DEBIT_MOVE_ID]
     */
    @JsonProperty("debit_move_id")
    public Integer getDebit_move_id(){
        return debit_move_id ;
    }

    /**
     * 设置 [DEBIT_MOVE_ID]
     */
    @JsonProperty("debit_move_id")
    public void setDebit_move_id(Integer  debit_move_id){
        this.debit_move_id = debit_move_id ;
        this.debit_move_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEBIT_MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getDebit_move_idDirtyFlag(){
        return debit_move_idDirtyFlag ;
    }

    /**
     * 获取 [CREDIT_MOVE_ID]
     */
    @JsonProperty("credit_move_id")
    public Integer getCredit_move_id(){
        return credit_move_id ;
    }

    /**
     * 设置 [CREDIT_MOVE_ID]
     */
    @JsonProperty("credit_move_id")
    public void setCredit_move_id(Integer  credit_move_id){
        this.credit_move_id = credit_move_id ;
        this.credit_move_idDirtyFlag = true ;
    }

    /**
     * 获取 [CREDIT_MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getCredit_move_idDirtyFlag(){
        return credit_move_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Account_partial_reconcile toDO() {
        Account_partial_reconcile srfdomain = new Account_partial_reconcile();
        if(getAmount_currencyDirtyFlag())
            srfdomain.setAmount_currency(amount_currency);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMax_dateDirtyFlag())
            srfdomain.setMax_date(max_date);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCredit_move_id_textDirtyFlag())
            srfdomain.setCredit_move_id_text(credit_move_id_text);
        if(getCompany_currency_idDirtyFlag())
            srfdomain.setCompany_currency_id(company_currency_id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getDebit_move_id_textDirtyFlag())
            srfdomain.setDebit_move_id_text(debit_move_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getFull_reconcile_id_textDirtyFlag())
            srfdomain.setFull_reconcile_id_text(full_reconcile_id_text);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getFull_reconcile_idDirtyFlag())
            srfdomain.setFull_reconcile_id(full_reconcile_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getDebit_move_idDirtyFlag())
            srfdomain.setDebit_move_id(debit_move_id);
        if(getCredit_move_idDirtyFlag())
            srfdomain.setCredit_move_id(credit_move_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Account_partial_reconcile srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getAmount_currencyDirtyFlag())
            this.setAmount_currency(srfdomain.getAmount_currency());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMax_dateDirtyFlag())
            this.setMax_date(srfdomain.getMax_date());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCredit_move_id_textDirtyFlag())
            this.setCredit_move_id_text(srfdomain.getCredit_move_id_text());
        if(srfdomain.getCompany_currency_idDirtyFlag())
            this.setCompany_currency_id(srfdomain.getCompany_currency_id());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getDebit_move_id_textDirtyFlag())
            this.setDebit_move_id_text(srfdomain.getDebit_move_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getFull_reconcile_id_textDirtyFlag())
            this.setFull_reconcile_id_text(srfdomain.getFull_reconcile_id_text());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getFull_reconcile_idDirtyFlag())
            this.setFull_reconcile_id(srfdomain.getFull_reconcile_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getDebit_move_idDirtyFlag())
            this.setDebit_move_id(srfdomain.getDebit_move_id());
        if(srfdomain.getCredit_move_idDirtyFlag())
            this.setCredit_move_id(srfdomain.getCredit_move_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Account_partial_reconcileDTO> fromDOPage(List<Account_partial_reconcile> poPage)   {
        if(poPage == null)
            return null;
        List<Account_partial_reconcileDTO> dtos=new ArrayList<Account_partial_reconcileDTO>();
        for(Account_partial_reconcile domain : poPage) {
            Account_partial_reconcileDTO dto = new Account_partial_reconcileDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

