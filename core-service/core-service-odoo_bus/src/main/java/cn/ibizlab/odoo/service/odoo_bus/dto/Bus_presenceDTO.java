package cn.ibizlab.odoo.service.odoo_bus.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_bus.valuerule.anno.bus_presence.*;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Bus_presenceDTO]
 */
public class Bus_presenceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Bus_presenceDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [LAST_PRESENCE]
     *
     */
    @Bus_presenceLast_presenceDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp last_presence;

    @JsonIgnore
    private boolean last_presenceDirtyFlag;

    /**
     * 属性 [LAST_POLL]
     *
     */
    @Bus_presenceLast_pollDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp last_poll;

    @JsonIgnore
    private boolean last_pollDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Bus_presenceIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [STATUS]
     *
     */
    @Bus_presenceStatusDefault(info = "默认规则")
    private String status;

    @JsonIgnore
    private boolean statusDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Bus_presence__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Bus_presenceUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Bus_presenceUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [LAST_PRESENCE]
     */
    @JsonProperty("last_presence")
    public Timestamp getLast_presence(){
        return last_presence ;
    }

    /**
     * 设置 [LAST_PRESENCE]
     */
    @JsonProperty("last_presence")
    public void setLast_presence(Timestamp  last_presence){
        this.last_presence = last_presence ;
        this.last_presenceDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_PRESENCE]脏标记
     */
    @JsonIgnore
    public boolean getLast_presenceDirtyFlag(){
        return last_presenceDirtyFlag ;
    }

    /**
     * 获取 [LAST_POLL]
     */
    @JsonProperty("last_poll")
    public Timestamp getLast_poll(){
        return last_poll ;
    }

    /**
     * 设置 [LAST_POLL]
     */
    @JsonProperty("last_poll")
    public void setLast_poll(Timestamp  last_poll){
        this.last_poll = last_poll ;
        this.last_pollDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_POLL]脏标记
     */
    @JsonIgnore
    public boolean getLast_pollDirtyFlag(){
        return last_pollDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [STATUS]
     */
    @JsonProperty("status")
    public String getStatus(){
        return status ;
    }

    /**
     * 设置 [STATUS]
     */
    @JsonProperty("status")
    public void setStatus(String  status){
        this.status = status ;
        this.statusDirtyFlag = true ;
    }

    /**
     * 获取 [STATUS]脏标记
     */
    @JsonIgnore
    public boolean getStatusDirtyFlag(){
        return statusDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }



    public Bus_presence toDO() {
        Bus_presence srfdomain = new Bus_presence();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getLast_presenceDirtyFlag())
            srfdomain.setLast_presence(last_presence);
        if(getLast_pollDirtyFlag())
            srfdomain.setLast_poll(last_poll);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getStatusDirtyFlag())
            srfdomain.setStatus(status);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);

        return srfdomain;
    }

    public void fromDO(Bus_presence srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getLast_presenceDirtyFlag())
            this.setLast_presence(srfdomain.getLast_presence());
        if(srfdomain.getLast_pollDirtyFlag())
            this.setLast_poll(srfdomain.getLast_poll());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getStatusDirtyFlag())
            this.setStatus(srfdomain.getStatus());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());

    }

    public List<Bus_presenceDTO> fromDOPage(List<Bus_presence> poPage)   {
        if(poPage == null)
            return null;
        List<Bus_presenceDTO> dtos=new ArrayList<Bus_presenceDTO>();
        for(Bus_presence domain : poPage) {
            Bus_presenceDTO dto = new Bus_presenceDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

