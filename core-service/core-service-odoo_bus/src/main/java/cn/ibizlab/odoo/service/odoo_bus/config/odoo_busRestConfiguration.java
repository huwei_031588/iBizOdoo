package cn.ibizlab.odoo.service.odoo_bus.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_bus")
public class odoo_busRestConfiguration {

}
