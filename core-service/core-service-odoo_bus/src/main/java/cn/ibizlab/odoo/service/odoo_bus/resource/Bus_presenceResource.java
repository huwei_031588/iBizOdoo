package cn.ibizlab.odoo.service.odoo_bus.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_bus.dto.Bus_presenceDTO;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence;
import cn.ibizlab.odoo.core.odoo_bus.service.IBus_presenceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_presenceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Bus_presence" })
@RestController
@RequestMapping("")
public class Bus_presenceResource {

    @Autowired
    private IBus_presenceService bus_presenceService;

    public IBus_presenceService getBus_presenceService() {
        return this.bus_presenceService;
    }

    @ApiOperation(value = "获取数据", tags = {"Bus_presence" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_bus/bus_presences/{bus_presence_id}")
    public ResponseEntity<Bus_presenceDTO> get(@PathVariable("bus_presence_id") Integer bus_presence_id) {
        Bus_presenceDTO dto = new Bus_presenceDTO();
        Bus_presence domain = bus_presenceService.get(bus_presence_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Bus_presence" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_bus/bus_presences/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Bus_presenceDTO> bus_presencedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Bus_presence" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_bus/bus_presences/{bus_presence_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("bus_presence_id") Integer bus_presence_id) {
        Bus_presenceDTO bus_presencedto = new Bus_presenceDTO();
		Bus_presence domain = new Bus_presence();
		bus_presencedto.setId(bus_presence_id);
		domain.setId(bus_presence_id);
        Boolean rst = bus_presenceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Bus_presence" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_bus/bus_presences/createBatch")
    public ResponseEntity<Boolean> createBatchBus_presence(@RequestBody List<Bus_presenceDTO> bus_presencedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Bus_presence" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_bus/bus_presences/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Bus_presenceDTO> bus_presencedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Bus_presence" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_bus/bus_presences/{bus_presence_id}")

    public ResponseEntity<Bus_presenceDTO> update(@PathVariable("bus_presence_id") Integer bus_presence_id, @RequestBody Bus_presenceDTO bus_presencedto) {
		Bus_presence domain = bus_presencedto.toDO();
        domain.setId(bus_presence_id);
		bus_presenceService.update(domain);
		Bus_presenceDTO dto = new Bus_presenceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Bus_presence" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_bus/bus_presences")

    public ResponseEntity<Bus_presenceDTO> create(@RequestBody Bus_presenceDTO bus_presencedto) {
        Bus_presenceDTO dto = new Bus_presenceDTO();
        Bus_presence domain = bus_presencedto.toDO();
		bus_presenceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Bus_presence" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_bus/bus_presences/fetchdefault")
	public ResponseEntity<Page<Bus_presenceDTO>> fetchDefault(Bus_presenceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Bus_presenceDTO> list = new ArrayList<Bus_presenceDTO>();
        
        Page<Bus_presence> domains = bus_presenceService.searchDefault(context) ;
        for(Bus_presence bus_presence : domains.getContent()){
            Bus_presenceDTO dto = new Bus_presenceDTO();
            dto.fromDO(bus_presence);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
