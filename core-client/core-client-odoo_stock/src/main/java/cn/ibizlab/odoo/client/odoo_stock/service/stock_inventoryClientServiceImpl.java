package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_inventory;
import cn.ibizlab.odoo.core.client.service.Istock_inventoryClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_inventoryImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_inventoryOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_inventoryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_inventory] 服务对象接口
 */
@Service
public class stock_inventoryClientServiceImpl implements Istock_inventoryClientService {
    @Autowired
    private  Istock_inventoryOdooClient  stock_inventoryOdooClient;

    public Istock_inventory createModel() {		
		return new stock_inventoryImpl();
	}


        public void update(Istock_inventory stock_inventory){
this.stock_inventoryOdooClient.update(stock_inventory) ;
        }
        
        public Page<Istock_inventory> search(SearchContext context){
            return this.stock_inventoryOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Istock_inventory> stock_inventories){
            
        }
        
        public void remove(Istock_inventory stock_inventory){
this.stock_inventoryOdooClient.remove(stock_inventory) ;
        }
        
        public void create(Istock_inventory stock_inventory){
this.stock_inventoryOdooClient.create(stock_inventory) ;
        }
        
        public void removeBatch(List<Istock_inventory> stock_inventories){
            
        }
        
        public void createBatch(List<Istock_inventory> stock_inventories){
            
        }
        
        public void get(Istock_inventory stock_inventory){
            this.stock_inventoryOdooClient.get(stock_inventory) ;
        }
        
        public Page<Istock_inventory> select(SearchContext context){
            return null ;
        }
        

}

